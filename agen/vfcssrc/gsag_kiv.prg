* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kiv                                                        *
*              Importa profilo                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-09                                                      *
* Last revis.: 2014-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kiv",oParentObject))

* --- Class definition
define class tgsag_kiv as StdForm
  Top    = 2
  Left   = 9

  * --- Standard Properties
  Width  = 625
  Height = 177
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-19"
  HelpContextID=267003753
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  PRA_ENTI_IDX = 0
  PRA_UFFI_IDX = 0
  CPUSERS_IDX = 0
  OFF_NOMI_IDX = 0
  CAT_SOGG_IDX = 0
  OFFTIPAT_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  CENCOST_IDX = 0
  DES_DIVE_IDX = 0
  GEST_ATTI_IDX = 0
  TIP_RISE_IDX = 0
  PAR_AGEN_IDX = 0
  CAUMATTI_IDX = 0
  CON_TRAS_IDX = 0
  MOD_ELEM_IDX = 0
  CONTI_IDX = 0
  PRO_EXPO_IDX = 0
  cpusers_IDX = 0
  cPrg = "gsag_kiv"
  cComment = "Importa profilo"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_LAYOUT = .F.
  w_READAZI = space(5)
  o_READAZI = space(5)
  w_FLTPART = space(5)
  o_FLTPART = space(5)
  w_DATPAR = 0
  w_DATULT = ctod('  /  /  ')
  o_DATULT = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_GestDataFin = space(1)
  o_GestDataFin = space(1)
  w_INCIGN = space(10)
  o_INCIGN = space(10)
  w_PRPATHIM = space(254)
  w_MININI = space(2)
  o_MININI = space(2)
  w_FLESGR = space(1)
  w_GRUPART = space(5)
  w_PRIORITA = 0
  w_DATA_INI = ctot('')
  w_COD_UTE = space(5)
  w_NOMEPART = space(40)
  w_VARLOG = .F.
  w_OBTEST = ctod('  /  /  ')
  w_COMODO = space(30)
  w_DPTIPRIS = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_TIPOGRUP = space(1)
  w_FLVISI = space(1)
  w_DatiAttivita = space(10)
  w_SERPRO = space(10)
  o_SERPRO = space(10)
  w_TIPDAT = space(1)
  w_FILEXP = space(60)
  w_PATHEX = space(254)
  w_PR__TIPO = space(1)
  w_PRDESCRI = space(50)
  w_CODPART = space(5)
  o_CODPART = space(5)
  w_PATIPRIS = space(1)
  w_DENOM_PART = space(48)
  w_RISERVE = space(1)
  w_DESCRPART = space(40)
  w_COGNPART = space(40)
  w_ESCIGN = space(1)
  w_BTNQRY = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_kiv
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kivPag1","gsag_kiv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATULT_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_BTNQRY = this.oPgFrm.Pages(1).oPag.BTNQRY
    DoDefault()
    proc Destroy()
      this.w_BTNQRY = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[21]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='PRA_ENTI'
    this.cWorkTables[4]='PRA_UFFI'
    this.cWorkTables[5]='CPUSERS'
    this.cWorkTables[6]='OFF_NOMI'
    this.cWorkTables[7]='CAT_SOGG'
    this.cWorkTables[8]='OFFTIPAT'
    this.cWorkTables[9]='IMP_MAST'
    this.cWorkTables[10]='IMP_DETT'
    this.cWorkTables[11]='CENCOST'
    this.cWorkTables[12]='DES_DIVE'
    this.cWorkTables[13]='GEST_ATTI'
    this.cWorkTables[14]='TIP_RISE'
    this.cWorkTables[15]='PAR_AGEN'
    this.cWorkTables[16]='CAUMATTI'
    this.cWorkTables[17]='CON_TRAS'
    this.cWorkTables[18]='MOD_ELEM'
    this.cWorkTables[19]='CONTI'
    this.cWorkTables[20]='PRO_EXPO'
    this.cWorkTables[21]='cpusers'
    return(this.OpenAllTables(21))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      gsag_biS('I')
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LAYOUT=.f.
      .w_READAZI=space(5)
      .w_FLTPART=space(5)
      .w_DATPAR=0
      .w_DATULT=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_GestDataFin=space(1)
      .w_INCIGN=space(10)
      .w_PRPATHIM=space(254)
      .w_MININI=space(2)
      .w_FLESGR=space(1)
      .w_GRUPART=space(5)
      .w_PRIORITA=0
      .w_DATA_INI=ctot("")
      .w_COD_UTE=space(5)
      .w_NOMEPART=space(40)
      .w_VARLOG=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_COMODO=space(30)
      .w_DPTIPRIS=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_TIPOGRUP=space(1)
      .w_FLVISI=space(1)
      .w_DatiAttivita=space(10)
      .w_SERPRO=space(10)
      .w_TIPDAT=space(1)
      .w_FILEXP=space(60)
      .w_PATHEX=space(254)
      .w_PR__TIPO=space(1)
      .w_PRDESCRI=space(50)
      .w_CODPART=space(5)
      .w_PATIPRIS=space(1)
      .w_DENOM_PART=space(48)
      .w_RISERVE=space(1)
      .w_DESCRPART=space(40)
      .w_COGNPART=space(40)
      .w_ESCIGN=space(1)
        .w_LAYOUT = Not Empty(  IIF(VARTYPE(this.oParentObject)='C',this.oParentObject,'') )
        .w_READAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_READAZI))
          .link_1_2('Full')
        endif
        .w_FLTPART = readdipend( i_CodUte, 'C')
        .w_DATPAR = ICASE(.w_GestDataFin='A',12,.w_GestDataFin='O',0,Val(.w_GestDataFin))
        .w_DATULT = iif(.w_GestDataFin='O',I_DATSYS,GOMONTH(I_DATSYS,-.w_DATPAR))
        .w_OREINI = '00'
        .w_GestDataFin = 'O'
        .w_INCIGN = ' '
          .DoRTCalc(9,9,.f.)
        .w_MININI = '00'
        .w_FLESGR = 'N'
        .w_GRUPART = Space(5)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_GRUPART))
          .link_1_13('Full')
        endif
        .w_PRIORITA = 0
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATULT),  ALLTR(STR(DAY(.w_DATULT)))+'-'+ALLTR(STR(MONTH(.w_DATULT)))+'-'+ALLTR(STR(YEAR(.w_DATULT)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
          .DoRTCalc(16,17,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(19,20,.f.)
        .w_OB_TEST = i_INIDAT
          .DoRTCalc(22,24,.f.)
        .w_SERPRO = iif(Vartype(oparentobject)='C',oparentobject,'')
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_SERPRO))
          .link_1_28('Full')
        endif
          .DoRTCalc(26,28,.f.)
        .w_PR__TIPO = iif(JUSTEXT(.w_PRPATHIM)='ICS','I','V')
        .DoRTCalc(30,31,.f.)
        if not(empty(.w_CODPART))
          .link_1_35('Full')
        endif
        .w_PATIPRIS = 'P'
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        .w_RISERVE = 'L'
          .DoRTCalc(35,36,.f.)
        .w_ESCIGN = iif(.w_INCIGN='S',' ','S')
      .oPgFrm.Page1.oPag.BTNQRY.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_READAZI<>.w_READAZI
            .w_READAZI = i_CODAZI
          .link_1_2('Full')
        endif
        if .o_FLTPART<>.w_FLTPART
            .w_FLTPART = readdipend( i_CodUte, 'C')
        endif
        if .o_GestDataFin<>.w_GestDataFin
            .w_DATPAR = ICASE(.w_GestDataFin='A',12,.w_GestDataFin='O',0,Val(.w_GestDataFin))
        endif
        if .o_GestDataFin<>.w_GestDataFin
            .w_DATULT = iif(.w_GestDataFin='O',I_DATSYS,GOMONTH(I_DATSYS,-.w_DATPAR))
        endif
        if .o_OREINI<>.w_OREINI.or. .o_SERPRO<>.w_SERPRO.or. .o_DATULT<>.w_DATULT
            .w_OREINI = '00'
        endif
        .DoRTCalc(7,9,.t.)
        if .o_MININI<>.w_MININI.or. .o_SERPRO<>.w_SERPRO.or. .o_DATULT<>.w_DATULT
            .w_MININI = '00'
        endif
        if .o_CODPART<>.w_CODPART
            .w_FLESGR = 'N'
        endif
        if .o_CODPART<>.w_CODPART
            .w_GRUPART = Space(5)
          .link_1_13('Full')
        endif
        .DoRTCalc(13,13,.t.)
        if .o_DATULT<>.w_DATULT.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATULT),  ALLTR(STR(DAY(.w_DATULT)))+'-'+ALLTR(STR(MONTH(.w_DATULT)))+'-'+ALLTR(STR(YEAR(.w_DATULT)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_CODPART<>.w_CODPART
            .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
        endif
        .DoRTCalc(16,24,.t.)
        if .o_READAZI<>.w_READAZI
            .w_SERPRO = iif(Vartype(oparentobject)='C',oparentobject,'')
          .link_1_28('Full')
        endif
        .DoRTCalc(26,28,.t.)
            .w_PR__TIPO = iif(JUSTEXT(.w_PRPATHIM)='ICS','I','V')
        .DoRTCalc(30,30,.t.)
        if .o_SERPRO<>.w_SERPRO
          .link_1_35('Full')
        endif
        .DoRTCalc(32,32,.t.)
        if .o_CODPART<>.w_CODPART.or. .o_SERPRO<>.w_SERPRO
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        if .o_SERPRO<>.w_SERPRO
          .Calculate_NHGZPPOKMU()
        endif
        .DoRTCalc(34,36,.t.)
        if .o_INCIGN<>.w_INCIGN
            .w_ESCIGN = iif(.w_INCIGN='S',' ','S')
        endif
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .w_CODPART = .w_CODPART
          .link_1_35('Full')
          .cComment = IIF(EMPTY(.w_CodPart), IIF(.w_LAYOUT, AH_MSGFORMAT("Importa profilo"),AH_MSGFORMAT("Importa profilo")),IIF(.w_LAYOUT, AH_MSGFORMAT("Importa profilo %1",alltrim(.w_DENOM_PART)),AH_MSGFORMAT("Importa profilo di %1",alltrim(.w_DENOM_PART))))
          .Caption = .cComment
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPRPATHIM_1_10.enabled = this.oPgFrm.Page1.oPag.oPRPATHIM_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.BTNQRY.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kiv
    if Cevent='w_SERPRO Changed' or cevent='Blank'
       This.Notifyevent('Assegna')
       This.Notifyevent('AggUlt')
    
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLVISI";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PAFLVISI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_FLVISI = NVL(_Link_.PAFLVISI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_FLVISI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPART
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPART)
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPART = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPOGRUP = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPART = space(5)
      endif
      this.w_TIPOGRUP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
        endif
        this.w_GRUPART = space(5)
        this.w_TIPOGRUP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SERPRO
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRO_EXPO_IDX,3]
    i_lTable = "PRO_EXPO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2], .t., this.PRO_EXPO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_APE',True,'PRO_EXPO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PRSERIAL like "+cp_ToStrODBC(trim(this.w_SERPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select PRSERIAL,PRDESCRI,PRPATHIM,PRTIPDAT,PRCODUTE,PRPATHEX";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PRSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PRSERIAL',trim(this.w_SERPRO))
          select PRSERIAL,PRDESCRI,PRPATHIM,PRTIPDAT,PRCODUTE,PRPATHEX;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PRSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SERPRO)==trim(_Link_.PRSERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PRDESCRI like "+cp_ToStrODBC(trim(this.w_SERPRO)+"%");

            i_ret=cp_SQL(i_nConn,"select PRSERIAL,PRDESCRI,PRPATHIM,PRTIPDAT,PRCODUTE,PRPATHEX";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PRDESCRI like "+cp_ToStr(trim(this.w_SERPRO)+"%");

            select PRSERIAL,PRDESCRI,PRPATHIM,PRTIPDAT,PRCODUTE,PRPATHEX;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SERPRO) and !this.bDontReportError
            deferred_cp_zoom('PRO_EXPO','*','PRSERIAL',cp_AbsName(oSource.parent,'oSERPRO_1_28'),i_cWhere,'GSAG_APE',"Profili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRSERIAL,PRDESCRI,PRPATHIM,PRTIPDAT,PRCODUTE,PRPATHEX";
                     +" from "+i_cTable+" "+i_lTable+" where PRSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRSERIAL',oSource.xKey(1))
            select PRSERIAL,PRDESCRI,PRPATHIM,PRTIPDAT,PRCODUTE,PRPATHEX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRSERIAL,PRDESCRI,PRPATHIM,PRTIPDAT,PRCODUTE,PRPATHEX";
                   +" from "+i_cTable+" "+i_lTable+" where PRSERIAL="+cp_ToStrODBC(this.w_SERPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRSERIAL',this.w_SERPRO)
            select PRSERIAL,PRDESCRI,PRPATHIM,PRTIPDAT,PRCODUTE,PRPATHEX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERPRO = NVL(_Link_.PRSERIAL,space(10))
      this.w_PRDESCRI = NVL(_Link_.PRDESCRI,space(50))
      this.w_PRPATHIM = NVL(_Link_.PRPATHIM,space(254))
      this.w_TIPDAT = NVL(_Link_.PRTIPDAT,space(1))
      this.w_CODPART = NVL(_Link_.PRCODUTE,space(5))
      this.w_FILEXP = NVL(_Link_.PRPATHEX,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_SERPRO = space(10)
      endif
      this.w_PRDESCRI = space(50)
      this.w_PRPATHIM = space(254)
      this.w_TIPDAT = space(1)
      this.w_CODPART = space(5)
      this.w_FILEXP = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])+'\'+cp_ToStr(_Link_.PRSERIAL,1)
      cp_ShowWarn(i_cKey,this.PRO_EXPO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPART
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODPART)
            select DPCODICE,DPTIPRIS,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPART = NVL(_Link_.DPCODICE,space(5))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODPART = space(5)
      endif
      this.w_DPTIPRIS = space(1)
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODPART = space(5)
        this.w_DPTIPRIS = space(1)
        this.w_COGNPART = space(40)
        this.w_NOMEPART = space(40)
        this.w_DESCRPART = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATULT_1_6.value==this.w_DATULT)
      this.oPgFrm.Page1.oPag.oDATULT_1_6.value=this.w_DATULT
    endif
    if not(this.oPgFrm.Page1.oPag.oGestDataFin_1_8.RadioValue()==this.w_GestDataFin)
      this.oPgFrm.Page1.oPag.oGestDataFin_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINCIGN_1_9.RadioValue()==this.w_INCIGN)
      this.oPgFrm.Page1.oPag.oINCIGN_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPATHIM_1_10.value==this.w_PRPATHIM)
      this.oPgFrm.Page1.oPag.oPRPATHIM_1_10.value=this.w_PRPATHIM
    endif
    if not(this.oPgFrm.Page1.oPag.oSERPRO_1_28.value==this.w_SERPRO)
      this.oPgFrm.Page1.oPag.oSERPRO_1_28.value=this.w_SERPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDESCRI_1_34.value==this.w_PRDESCRI)
      this.oPgFrm.Page1.oPag.oPRDESCRI_1_34.value=this.w_PRDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATULT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATULT_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DATULT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(DIRECTORY(JUSTPATH(.w_PRPATHIM)) and Not Empty(justfname(.w_PRPATHIM)))  and (Not Empty(.w_SERPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRPATHIM_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il file indicato non � valido")
          case   not(.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART))  and not(empty(.w_GRUPART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUPART_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
          case   (empty(.w_SERPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERPRO_1_28.SetFocus()
            i_bnoObbl = !empty(.w_SERPRO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS)  and not(empty(.w_CODPART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPART_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_READAZI = this.w_READAZI
    this.o_FLTPART = this.w_FLTPART
    this.o_DATULT = this.w_DATULT
    this.o_OREINI = this.w_OREINI
    this.o_GestDataFin = this.w_GestDataFin
    this.o_INCIGN = this.w_INCIGN
    this.o_MININI = this.w_MININI
    this.o_SERPRO = this.w_SERPRO
    this.o_CODPART = this.w_CODPART
    return

enddefine

* --- Define pages as container
define class tgsag_kivPag1 as StdContainer
  Width  = 621
  height = 177
  stdWidth  = 621
  stdheight = 177
  resizeXpos=266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATULT_1_6 as StdField with uid="VAJTUNAFFS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATULT", cQueryName = "DATULT",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data a partire dalla quale verr� eseguita la comparazione",;
    HelpContextID = 154164790,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=63, Top=53


  add object oGestDataFin_1_8 as StdCombo with uid="GTYLCCGUSH",rtseq=7,rtrep=.f.,left=153,top=53,width=118,height=21;
    , ToolTipText = "Valorizza il campo data rispetto alla data odierna";
    , HelpContextID = 98091047;
    , cFormVar="w_GestDataFin",RowSource=""+"Oggi,"+"Un mese,"+"Due mesi,"+"Tre mesi,"+"Sei mesi,"+"Un anno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGestDataFin_1_8.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'6',;
    iif(this.value =6,'A',;
    space(1))))))))
  endfunc
  func oGestDataFin_1_8.GetRadio()
    this.Parent.oContained.w_GestDataFin = this.RadioValue()
    return .t.
  endfunc

  func oGestDataFin_1_8.SetRadio()
    this.Parent.oContained.w_GestDataFin=trim(this.Parent.oContained.w_GestDataFin)
    this.value = ;
      iif(this.Parent.oContained.w_GestDataFin=='O',1,;
      iif(this.Parent.oContained.w_GestDataFin=='1',2,;
      iif(this.Parent.oContained.w_GestDataFin=='2',3,;
      iif(this.Parent.oContained.w_GestDataFin=='3',4,;
      iif(this.Parent.oContained.w_GestDataFin=='6',5,;
      iif(this.Parent.oContained.w_GestDataFin=='A',6,;
      0))))))
  endfunc

  add object oINCIGN_1_9 as StdCheck with uid="TGWFNOGLSQ",rtseq=8,rtrep=.f.,left=507, top=53, caption="Includi ignorati",;
    ToolTipText = "Se attivo, include eventi precedentemente ignorati ",;
    HelpContextID = 47405958,;
    cFormVar="w_INCIGN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINCIGN_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oINCIGN_1_9.GetRadio()
    this.Parent.oContained.w_INCIGN = this.RadioValue()
    return .t.
  endfunc

  func oINCIGN_1_9.SetRadio()
    this.Parent.oContained.w_INCIGN=trim(this.Parent.oContained.w_INCIGN)
    this.value = ;
      iif(this.Parent.oContained.w_INCIGN=='S',1,;
      0)
  endfunc

  add object oPRPATHIM_1_10 as StdField with uid="VIYSQOEWNY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PRPATHIM", cQueryName = "PRPATHIM",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il file indicato non � valido",;
    ToolTipText = "Percorso di import",;
    HelpContextID = 40095677,;
   bGlobalFont=.t.,;
    Height=21, Width=531, Left=65, Top=86, InputMask=replicate('X',254)

  func oPRPATHIM_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_SERPRO))
    endwith
   endif
  endfunc

  func oPRPATHIM_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (DIRECTORY(JUSTPATH(.w_PRPATHIM)) and Not Empty(justfname(.w_PRPATHIM)))
    endwith
    return bRes
  endfunc


  add object oBtn_1_21 as StdButton with uid="TKCMXZXILU",left=561, top=124, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 259686330;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="XKRYZTPXZE",left=510, top=124, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per importare attivit�";
    , HelpContextID = 195840646;
    , Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        gsag_biS(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSERPRO_1_28 as StdField with uid="AZKYDFMGSL",rtseq=25,rtrep=.f.,;
    cFormVar = "w_SERPRO", cQueryName = "SERPRO",nZero=10,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Profilo utente",;
    HelpContextID = 76235558,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=63, Top=19, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRO_EXPO", cZoomOnZoom="GSAG_APE", oKey_1_1="PRSERIAL", oKey_1_2="this.w_SERPRO"

  func oSERPRO_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oSERPRO_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSERPRO_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRO_EXPO','*','PRSERIAL',cp_AbsName(this.parent,'oSERPRO_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_APE',"Profili",'',this.parent.oContained
  endproc
  proc oSERPRO_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAG_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PRSERIAL=this.parent.oContained.w_SERPRO
     i_obj.ecpSave()
  endproc

  add object oPRDESCRI_1_34 as StdField with uid="TKTEUHTOMC",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PRDESCRI", cQueryName = "PRDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 143618111,;
   bGlobalFont=.t.,;
    Height=21, Width=438, Left=172, Top=19, InputMask=replicate('X',50)


  add object BTNQRY as cp_askfile with uid="SVNANQOYOT",left=597, top=87, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_PRPATHIM",cext="ICS,VCS",;
    nPag=1;
    , ToolTipText = "Premere per selezionare il file di import";
    , HelpContextID = 266802730

  add object oStr_1_5 as StdString with uid="KLEZMLKSOB",Visible=.t., Left=24, Top=55,;
    Alignment=1, Width=37, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="EKUQLQGTAB",Visible=.t., Left=25, Top=19,;
    Alignment=0, Width=38, Height=18,;
    Caption="Profilo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="ILOUNTOCKB",Visible=.t., Left=10, Top=89,;
    Alignment=1, Width=50, Height=18,;
    Caption="File :"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kiv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
