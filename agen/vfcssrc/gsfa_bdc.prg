* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bdc                                                        *
*              Import eventi da file csv                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-02                                                      *
* Last revis.: 2010-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPath,pCodPer,pCodGru,pErr_Log,pEventType
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bdc",oParentObject,m.pPath,m.pCodPer,m.pCodGru,m.pErr_Log,m.pEventType)
return(i_retval)

define class tgsfa_bdc as StdBatch
  * --- Local variables
  pPath = space(254)
  pCodPer = space(5)
  pCodGru = space(5)
  pErr_Log = .NULL.
  pEventType = space(10)
  w_TETIPDIR = space(1)
  w_FLERRORS = .f.
  w_NUM_ITEM = 0
  w_EVCURNAM = space(10)
  w_FILEHAND = 0
  w_LSTFIELDS = space(254)
  w_FLDINDEX = 0
  w_LSTVALUES = space(254)
  w_VALINDEX = 0
  w_ATTFLDID = 0
  w_NSER_ATT = 0
  w_CUR_FILE = space(254)
  w_IDFLDTBL = 0
  w_FLD_TYPE = space(1)
  w_INSVALUE = .NULL.
  * --- WorkFile variables
  TIPEVENT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Read from TIPEVENT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIPEVENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIPEVENT_idx,2],.t.,this.TIPEVENT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TETIPDIR"+;
        " from "+i_cTable+" TIPEVENT where ";
            +"TETIPEVE = "+cp_ToStrODBC(this.pEventType);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TETIPDIR;
        from (i_cTable) where;
            TETIPEVE = this.pEventType;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TETIPDIR = NVL(cp_ToDate(_read_.TETIPDIR),cp_NullValue(_read_.TETIPDIR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_FLERRORS = .F.
    this.w_NSER_ATT = 0
    LOCAL L_OLDERR, L_Err, L_MACRO, L_LSTFIELDS
    L_OLDERR=ON("ERROR")
    L_Err=.F.
    ON ERROR L_Err=.T.
    private L_OldPath
    L_OldPath = Sys(5)+Sys(2003)
    CD (this.pPath)
    if L_Err
      this.pErr_Log.AddMsgLog("Percorso inesistente o non raggiungibile")     
      this.w_FLERRORS = .T.
    else
      this.w_NUM_ITEM = ADIR(L_ArrFile, "*.csv")
    endif
    CD (L_OldPath)
    release L_OldPath
    if this.w_NUM_ITEM>0
      if !this.w_FLERRORS
        this.w_EVCURNAM = SYS(2015)
        vq_exec("..\agen\exe\QUERY\GSFA_BIM.VQR",this,this.w_EVCURNAM)
        * --- Metto in un array i campi della tabella per determinare successivamente
        *     il tipo nella creazione dinamica della frase di insert
        AFIELDS(L_TableFld)
      endif
    endif
    if !this.w_FLERRORS
      * --- Analisi file da importare
      do while this.w_NUM_ITEM>0
        this.w_CUR_FILE = ADDBS(ALLTRIM(this.pPath))+ALLTRIM(L_ArrFile(this.w_NUM_ITEM,1))
        * --- Apertura file in lettura per determinazione campi inclusi e lettura dati
        this.w_FILEHAND = FOPEN(this.w_CUR_FILE)
        if this.w_FILEHAND>0
          * --- Leggo nella prima riga separati da ; i nomi delle colonne (dato obbligatorio)
          this.w_LSTFIELDS = FGETS(this.w_FILEHAND, 8192)
          * --- Metto nell'array dei campi quanto appena letto
          this.w_FLDINDEX = ALINES(L_ArrFlds, this.w_LSTFIELDS, 1, ";")
          L_INSERT = "INSERT INTO '"+this.w_EVCURNAM+"' ("
          this.w_ATTFLDID = this.w_FLDINDEX
          do while this.w_ATTFLDID>0
            L_INSERT = L_INSERT + ALLTRIM( L_ArrFlds(this.w_ATTFLDID) ) + IIF(this.w_ATTFLDID>1, ", ", " ) VALUES ( ")
            this.w_ATTFLDID = this.w_ATTFLDID - 1
          enddo
          do while !FEOF(this.w_FILEHAND)
            * --- Lettura riga dei dati e relativa insert nel cursore
            this.w_LSTVALUES = FGETS(this.w_FILEHAND, 8192)
            if !EMPTY(this.w_LSTVALUES)
              * --- Metto nell'array dei valori quanto appena letto
              this.w_VALINDEX = ALINES(L_ArrValues, this.w_LSTVALUES, 1, ";")
              if this.w_VALINDEX<this.w_FLDINDEX
                * --- Ultimo campo vuoto lo aggiungo manualmente
                DIMENSION L_Arrvalues( ALEN(L_ArrValues, 1) + 1)
                L_Arrvalues( ALEN(L_ArrValues, 1) ) = ""
                this.w_VALINDEX = this.w_VALINDEX + 1
              endif
              if this.w_VALINDEX=this.w_FLDINDEX
                * --- Preparo la frase d'insert nel cursore
                L_MACRO = L_INSERT
                do while this.w_VALINDEX > 0
                  * --- Gestione campi particolari (Es. Seriale e tipo evento)
                  do case
                    case L_ArrFlds(this.w_VALINDEX)="EVSERIAL"
                      local L_VerSerial
                      L_VerSerial = ALLTRIM( L_ArrValues(this.w_VALINDEX) )
                      this.w_NSER_ATT = this.w_NSER_ATT + 1
                      L_ArrValues(this.w_VALINDEX) =IIF(EMPTY(NVL(L_VerSerial , " ")), "E"+RIGHT(REPL("0",10)+ALLTRIM(STR(this.w_NSER_ATT)),9), ALLTRIM( L_ArrValues(this.w_VALINDEX) ) )
                      release L_VerSerial
                    case L_ArrFlds(this.w_VALINDEX)="EVTIPEVE"
                      local L_TipEve
                      L_TipEve = ALLTRIM( L_ArrValues(this.w_VALINDEX) )
                      L_ArrValues(this.w_VALINDEX) = IIF(EMPTY(NVL(L_TipEve , " ")), this.pEventType, L_TipEve )
                      release L_TipEve
                    case L_ArrFlds(this.w_VALINDEX)="EVDIREVE"
                      local L_DirEve
                      L_DirEve = ALLTRIM( L_ArrValues(this.w_VALINDEX) )
                      L_ArrValues(this.w_VALINDEX) = IIF(EMPTY(NVL(L_DirEve , " ")), this.w_TETIPDIR , ALLTRIM( L_ArrValues(this.w_VALINDEX) ))
                      release L_DirEve
                    case L_ArrFlds(this.w_VALINDEX)="EVPERSON"
                      local L_CodPer
                      L_CodPer = ALLTRIM( L_ArrValues(this.w_VALINDEX) )
                      L_ArrValues(this.w_VALINDEX) = IIF(EMPTY(NVL(L_CodPer , " ")), ALLTRIM(this.pCodPer) , ALLTRIM( L_ArrValues(this.w_VALINDEX) ))
                      release L_CodPer
                    case L_ArrFlds(this.w_VALINDEX)="EVGRUPPO"
                      local L_CodGru
                      L_CodGru = ALLTRIM( L_ArrValues(this.w_VALINDEX) )
                      L_ArrValues(this.w_VALINDEX) = IIF(EMPTY(NVL(L_CodGru , " ")), ALLTRIM(this.pCodGru) , ALLTRIM( L_ArrValues(this.w_VALINDEX) ))
                      release L_CodGru
                  endcase
                  * --- Determino il tipo di campo per impostare correttamente il tipo per l'insert
                  this.w_IDFLDTBL = ASCAN( L_TableFld , ALLTRIM( L_ArrFlds(this.w_VALINDEX) ), 1, ALEN(L_TableFld,1), 1, 15)
                  this.w_FLD_TYPE = L_TableFld(this.w_IDFLDTBL, 2)
                  do case
                    case this.w_FLD_TYPE="C" OR this.w_FLD_TYPE="G" OR this.w_FLD_TYPE="M" OR this.w_FLD_TYPE="V"
                      this.w_INSVALUE = "'"+ALLTRIM( L_ArrValues(this.w_VALINDEX) )+"'"
                    case this.w_FLD_TYPE="D"
                      this.w_INSVALUE = "cp_CharToDate('"+ ALLTRIM( L_ArrValues(this.w_VALINDEX) ) +"')"
                    case this.w_FLD_TYPE="T"
                      this.w_INSVALUE = "CTOT('"+ CHRTRAN(ALLTRIM( L_ArrValues(this.w_VALINDEX) ), ".",":") +"')"
                    case this.w_FLD_TYPE="L"
                      this.w_INSVALUE = "'" + STR(EMPTY( L_ArrValues(this.w_VALINDEX) ) ) + "'"
                    case this.w_FLD_TYPE="N" OR this.w_FLD_TYPE="I"
                      this.w_INSVALUE = IIF(EMPTY( ALLTRIM( L_ArrValues(this.w_VALINDEX) ) ) , "0", ALLTRIM( L_ArrValues(this.w_VALINDEX) ) )
                    otherwise
                      this.w_INSVALUE = "'"+ALLTRIM( L_ArrValues(this.w_VALINDEX) )+"'"
                  endcase
                  L_MACRO = L_MACRO + this.w_INSVALUE + IIF(this.w_VALINDEX>1, ", ", ")" )
                  this.w_VALINDEX = this.w_VALINDEX - 1
                enddo
                * --- Inserisco i dati nel cursore
                &L_MACRO
                Release L_ArrValues
              else
                this.pErr_Log.AddMsgLog("I dati presenti nelle righe non coincidono con il numero dei campi indicati nel file")     
              endif
            endif
          enddo
          Release L_ArrFlds
        else
          this.pErr_Log.AddMsgLog("Impossibile aprire file %1%0%2", this.w_CUR_FILE, MESSAGE())     
        endif
        FCLOSE(this.w_FILEHAND)
        this.w_NUM_ITEM = this.w_NUM_ITEM - 1
      enddo
      if Used((this.w_EVCURNAM))
        * --- Analizzo gli eventi da inserire, i seriali vuoti vanno riempiti
        SELECT (this.w_EVCURNAM)
        GO TOP
        SCAN FOR EMPTY(NVL(EVSERIAL, " "))
        this.w_NSER_ATT = this.w_NSER_ATT + 1
        REPLACE EVSERIAL WITH "E"+RIGHT(REPL("0",10)+ALLTRIM(STR(this.w_NSER_ATT)),9)
        ENDSCAN
        * --- Elimino dal cursore tutte le righe con direzione non congruente 
        *     al tipo evento passato come parametro
        DELETE FROM (this.w_EVCURNAM) WHERE NVL(EVDIREVE, " ")<>this.w_TETIPDIR OR NVL(EVTIPEVE, " ")<>this.pEventType
        * --- Chiamata a routine standard d'inserimento eventi
        this.w_FLERRORS = this.w_FLERRORS OR !GSFA_BEV(this, this.w_EVCURNAM , "", this.pErr_Log, "X")
      else
        this.pErr_Log.AddMsgLog("Non esiste nessun file congruente alle tipologia eventi %1 " ,this.pEventType)     
      endif
    endif
    release L_ArrFile
    ON ERROR &L_OLDERR
    i_retcode = 'stop'
    i_retval = IIF(this.w_FLERRORS, "E", "")
    return
  endproc


  proc Init(oParentObject,pPath,pCodPer,pCodGru,pErr_Log,pEventType)
    this.pPath=pPath
    this.pCodPer=pCodPer
    this.pCodGru=pCodGru
    this.pErr_Log=pErr_Log
    this.pEventType=pEventType
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TIPEVENT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPath,pCodPer,pCodGru,pErr_Log,pEventType"
endproc
