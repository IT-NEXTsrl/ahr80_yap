* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bic                                                        *
*              Inserimento impianti/componenti                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-17                                                      *
* Last revis.: 2012-03-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bic",oParentObject)
return(i_retval)

define class tgsag_bic as StdBatch
  * --- Local variables
  w_Mask = .NULL.
  w_zoom_imp = space(1)
  w_ObjMCP = .NULL.
  w_NUMRIG = 0
  w_IMCODICE = space(10)
  w_IMDESCON = space(50)
  w_RIGA = 0
  w_PADRE = .NULL.
  CONTA_PREST = 0
  w_IMDTOBSO = ctod("  /  /  ")
  w_IMDTOINI = ctod("  /  /  ")
  * --- WorkFile variables
  IMP_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento multiplo impianti sull'attivit�
    this.w_PADRE = this.oParentObject.oParentObject
    this.w_Mask = this.oParentObject
    this.w_zoom_imp = this.w_Mask.w_zoom_imp.ccursor
    select (this.w_zoom_imp)
    COUNT FOR XCHK=1 TO this.CONTA_PREST
    if this.CONTA_PREST = 0
      ah_ErrorMsg("Non ci sono impianti selezionati")
      this.oParentObject.w_RESCHK = -1
      i_retcode = 'stop'
      return
    endif
    this.w_ObjMCP = iif(UPPER(this.w_PADRE.class)="TGSAG_AAT",this.w_PADRE.GSAG_MCP,this.w_PADRE)
     
 Select * from (this.w_zoom_imp) into cursor Temp 
 this.w_Mask.Ecpquit()
    * --- Cicla sul cursore dello zoom relativo all'impianto ed esamina i records selezionati
    SELECT TEMP 
 Go Top 
 scan for XCHK=1
    * --- Legge sul cursore dello zoom il codice dell'impianto
    this.w_IMCODICE = IMCODICE
    this.w_IMDESCON = IMDESCON
    this.w_RIGA = CPROWNUM
    this.w_IMDTOBSO = IMDTOBSO
    this.w_IMDTOINI = IMDTOINI
    * --- Aggiunge un nuovo record nel dettaglio impianti
    if this.w_ObjMCP.Search("NVL(t_CACODIMP, SPACE(10))= " + cp_ToStrODBC(this.w_IMCODICE) +" AND t_CACODCOM=" +cp_ToStrODBC(this.w_RIGA)+ " AND NOT DELETED()") = -1 
      this.w_ObjMCP.AddRow()     
      this.w_ObjMCP.w_CACODIMP = this.w_IMCODICE
      =setvaluelinked("D", this.w_ObjMCP,"w_CACODIMP", this.w_IMCODICE)
      this.w_ObjMCP.w_CACODCOM = this.w_RIGA
      this.w_ObjMCP.w_COCOM = this.w_RIGA
      this.w_ObjMCP.w_DESCOM = this.w_IMDESCON
      this.w_ObjMCP.w_DTOBSO = CP_TODATE(this.w_IMDTOBSO)
      this.w_ObjMCP.w_DTOINI = CP_TODATE(this.w_IMDTOINI)
      this.w_ObjMCP.SaveRow()     
      this.w_ObjMCP.Refresh()     
      this.w_ObjMCP.bUpdated = .T.
    endif
    SELECT TEMP
    endscan
    * --- Chiudo il cursore
     
 Use in temp
    this.w_Mask = NULL
    this.w_ObjMCP = NULL
    this.w_PADRE = NULL
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='IMP_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
