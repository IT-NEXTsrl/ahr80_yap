* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kit                                                        *
*              Nuove attivit� collegate                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-02-22                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kit",oParentObject))

* --- Class definition
define class tgsag_kit as StdForm
  Top    = 18
  Left   = 79

  * --- Standard Properties
  Width  = 660
  Height = 551
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-13"
  HelpContextID=267003753
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=73

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  PRO_ITER_IDX = 0
  CENCOST_IDX = 0
  OFF_NOMI_IDX = 0
  PAR_AGEN_IDX = 0
  VOC_COST_IDX = 0
  ATTIVITA_IDX = 0
  CAUMATTI_IDX = 0
  PRA_MATE_IDX = 0
  cPrg = "gsag_kit"
  cComment = "Nuove attivit� collegate"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CambiaPrat = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_PAR_AGEN = space(5)
  w_CODITER = space(10)
  w_CC_CONTO = space(15)
  w_CODPRAT = space(15)
  o_CODPRAT = space(15)
  w_ATTCOS = space(15)
  w_CENRIC = space(15)
  w_COMRIC = space(15)
  w_ATTRIC = space(15)
  w_DESPRAT = space(55)
  w_CODNOM = space(15)
  o_CODNOM = space(15)
  w_PERSON = space(60)
  w_DESCRITER = space(40)
  w_DATRIF = ctod('  /  /  ')
  o_DATRIF = ctod('  /  /  ')
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_MININI = space(2)
  w_ORARIF = space(2)
  w_MINRIF = space(2)
  w_DPTIPRIS = space(1)
  w_LOCALI = space(25)
  w_ENTE = space(10)
  w_UFFICIO = space(10)
  w_VALPRA = space(3)
  w_LISPRA = space(5)
  w_RESCHK = 0
  w_CNFLAPON = space(1)
  w_CNFLVALO = space(1)
  w_CNIMPORT = 0
  w_CNCALDIR = space(1)
  w_CNCOECAL = 0
  w_PARASS = 0
  w_FLAMPA = space(1)
  w_RIGA = 0
  w_DATOBSO = ctod('  /  /  ')
  w_RICALCOLA = 0
  w_GIORIF = space(3)
  w_CCDESPIA = space(40)
  w_CAUATT = space(5)
  w_CACHKINI = space(1)
  o_CACHKINI = space(1)
  w_ONLYFIRST = space(1)
  w_MatPra = space(10)
  w_CACHKFOR = space(1)
  w_COMODO = space(10)
  w_DATOBSONOM = ctod('  /  /  ')
  w_OFDATDOC = ctod('  /  /  ')
  w_PACHKFES = space(1)
  w_CODCOS = space(5)
  w_VOCOBSO = ctod('  /  /  ')
  w_TIPVOR = space(1)
  w_InviaMail = .F.
  w_ATTELEFO = space(18)
  w_AT___FAX = space(18)
  w_ATCELLUL = space(18)
  w_AT_EMAIL = space(0)
  w_AT_EMPEC = space(0)
  w_LOC_PRA = space(25)
  w_LOC_NOM = space(25)
  w_OB_TEST = ctod('  /  /  ')
  w_CCDESRIC = space(40)
  w_TIPATT = space(10)
  w_CENOBSO = ctod('  /  /  ')
  w_TIPOPRAT = space(10)
  w_TIPOEVENTO = space(10)
  w_PACALDAT = space(1)
  w_PACHKATT = 0
  w_VISALLPART = space(1)
  w_MODAPP = space(1)
  w_NOBFQ = .F.
  w_NOSAVE = .F.
  w_TipPra = space(10)
  w_CNDATFIN = ctod('  /  /  ')
  w_MPFLSOSP = space(1)
  w_ITER_ZOOM = .NULL.
  w_AGKIT_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_kit
  * Nome cursore eventi
  w_CUR_EVE=''
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kitPag1","gsag_kit",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODITER_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ITER_ZOOM = this.oPgFrm.Pages(1).oPag.ITER_ZOOM
    this.w_AGKIT_ZOOM = this.oPgFrm.Pages(1).oPag.AGKIT_ZOOM
    DoDefault()
    proc Destroy()
      this.w_ITER_ZOOM = .NULL.
      this.w_AGKIT_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='PRO_ITER'
    this.cWorkTables[3]='CENCOST'
    this.cWorkTables[4]='OFF_NOMI'
    this.cWorkTables[5]='PAR_AGEN'
    this.cWorkTables[6]='VOC_COST'
    this.cWorkTables[7]='ATTIVITA'
    this.cWorkTables[8]='CAUMATTI'
    this.cWorkTables[9]='PRA_MATE'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        AggioProvv(this,"S")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CambiaPrat=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_PAR_AGEN=space(5)
      .w_CODITER=space(10)
      .w_CC_CONTO=space(15)
      .w_CODPRAT=space(15)
      .w_ATTCOS=space(15)
      .w_CENRIC=space(15)
      .w_COMRIC=space(15)
      .w_ATTRIC=space(15)
      .w_DESPRAT=space(55)
      .w_CODNOM=space(15)
      .w_PERSON=space(60)
      .w_DESCRITER=space(40)
      .w_DATRIF=ctod("  /  /  ")
      .w_ORAINI=space(2)
      .w_MININI=space(2)
      .w_ORARIF=space(2)
      .w_MINRIF=space(2)
      .w_DPTIPRIS=space(1)
      .w_LOCALI=space(25)
      .w_ENTE=space(10)
      .w_UFFICIO=space(10)
      .w_VALPRA=space(3)
      .w_LISPRA=space(5)
      .w_RESCHK=0
      .w_CNFLAPON=space(1)
      .w_CNFLVALO=space(1)
      .w_CNIMPORT=0
      .w_CNCALDIR=space(1)
      .w_CNCOECAL=0
      .w_PARASS=0
      .w_FLAMPA=space(1)
      .w_RIGA=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_RICALCOLA=0
      .w_GIORIF=space(3)
      .w_CCDESPIA=space(40)
      .w_CAUATT=space(5)
      .w_CACHKINI=space(1)
      .w_ONLYFIRST=space(1)
      .w_MatPra=space(10)
      .w_CACHKFOR=space(1)
      .w_COMODO=space(10)
      .w_DATOBSONOM=ctod("  /  /  ")
      .w_OFDATDOC=ctod("  /  /  ")
      .w_PACHKFES=space(1)
      .w_CODCOS=space(5)
      .w_VOCOBSO=ctod("  /  /  ")
      .w_TIPVOR=space(1)
      .w_InviaMail=.f.
      .w_ATTELEFO=space(18)
      .w_AT___FAX=space(18)
      .w_ATCELLUL=space(18)
      .w_AT_EMAIL=space(0)
      .w_AT_EMPEC=space(0)
      .w_LOC_PRA=space(25)
      .w_LOC_NOM=space(25)
      .w_OB_TEST=ctod("  /  /  ")
      .w_CCDESRIC=space(40)
      .w_TIPATT=space(10)
      .w_CENOBSO=ctod("  /  /  ")
      .w_TIPOPRAT=space(10)
      .w_TIPOEVENTO=space(10)
      .w_PACALDAT=space(1)
      .w_PACHKATT=0
      .w_VISALLPART=space(1)
      .w_MODAPP=space(1)
      .w_NOBFQ=.f.
      .w_NOSAVE=.f.
      .w_TipPra=space(10)
      .w_CNDATFIN=ctod("  /  /  ")
      .w_MPFLSOSP=space(1)
        .w_CambiaPrat = IIF(ISALT(), .w_CODPRAT, '')
        .w_OBTEST = i_datsys
        .w_PAR_AGEN = i_CodAzi
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PAR_AGEN))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODITER))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CC_CONTO))
          .link_1_6('Full')
        endif
        .w_CODPRAT = iif(Vartype(this.oParentObject)='O' AND upper(this.oParentObject.cPrg)='GSAG_KRA', this.oParentObject.w_CODPRA, space(15) )
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODPRAT))
          .link_1_7('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_ATTCOS))
          .link_1_8('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CENRIC))
          .link_1_9('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_COMRIC))
          .link_1_10('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_ATTRIC))
          .link_1_11('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_CODNOM))
          .link_1_13('Full')
        endif
          .DoRTCalc(13,14,.f.)
        .w_DATRIF = i_datsys
        .w_ORAINI = ' ' 
        .w_MININI = ' ' 
        .w_ORARIF = '00'
        .w_MINRIF = '00'
        .w_DPTIPRIS = 'Q'
        .w_LOCALI = IIF(ISALT(), .w_LOC_PRA, .w_LOC_NOM)
          .DoRTCalc(22,25,.f.)
        .w_RESCHK = 0
      .oPgFrm.Page1.oPag.ITER_ZOOM.Calculate(.w_Cambiaprat)
          .DoRTCalc(27,33,.f.)
        .w_RIGA = .w_ITER_ZOOM.GETVAR('RIGAPAD')
          .DoRTCalc(35,35,.f.)
        .w_RICALCOLA = 0
        .w_GIORIF = LEFT(g_GIORNO[DOW(.w_DATRIF)],3)
          .DoRTCalc(38,38,.f.)
        .w_CAUATT = .w_ITER_ZOOM.GETVAR('IPCODCAU')
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_CAUATT))
          .link_1_49('Full')
        endif
        .w_CACHKINI = .w_PACALDAT
        .w_ONLYFIRST = IIF(Empty(.w_ORAINI) or !Isalt(),'N','P')
      .oPgFrm.Page1.oPag.AGKIT_ZOOM.Calculate(.w_RIGA)
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_MatPra))
          .link_1_54('Full')
        endif
        .w_CACHKFOR = iif( IsAlt() AND (.w_MPFLSOSP='S' OR EMPTY(.w_MPFLSOSP)), 'S' , ' ')
          .DoRTCalc(44,45,.f.)
        .w_OFDATDOC = i_datsys
          .DoRTCalc(47,58,.f.)
        .w_OB_TEST = i_datsys
          .DoRTCalc(60,60,.f.)
        .w_TIPATT = 'A'
      .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
          .DoRTCalc(62,66,.f.)
        .w_VISALLPART = 'S'
      .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
          .DoRTCalc(68,68,.f.)
        .w_NOBFQ = .F.
        .w_NOSAVE = .f.
    endwith
    this.DoRTCalc(71,73,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_90.enabled = this.oPgFrm.Page1.oPag.oBtn_1_90.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsag_kit
    *this.opgfrm.page1.opag.oBOX_1_22.visible=isalt()
    Local l_obj
    l_obj=this.getctrl('LOIZABPOHX')
    l_obj.visible=isalt()
    l_obj=.null.
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CambiaPrat = IIF(ISALT(), .w_CODPRAT, '')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,16,.t.)
        if .o_ORAINI<>.w_ORAINI
            .w_MININI = ' ' 
        endif
        .DoRTCalc(18,20,.t.)
        if .o_CODPRAT<>.w_CODPRAT.or. .o_CODNOM<>.w_CODNOM
            .w_LOCALI = IIF(ISALT(), .w_LOC_PRA, .w_LOC_NOM)
        endif
        .oPgFrm.Page1.oPag.ITER_ZOOM.Calculate(.w_Cambiaprat)
        .DoRTCalc(22,33,.t.)
            .w_RIGA = .w_ITER_ZOOM.GETVAR('RIGAPAD')
        .DoRTCalc(35,35,.t.)
        if .o_CODPRAT<>.w_CODPRAT
            .w_RICALCOLA = 0
        endif
        if .o_DATRIF<>.w_DATRIF
            .w_GIORIF = LEFT(g_GIORNO[DOW(.w_DATRIF)],3)
        endif
        .DoRTCalc(38,38,.t.)
            .w_CAUATT = .w_ITER_ZOOM.GETVAR('IPCODCAU')
          .link_1_49('Full')
        .DoRTCalc(40,40,.t.)
        if .o_ORAINI<>.w_ORAINI
            .w_ONLYFIRST = IIF(Empty(.w_ORAINI) or !Isalt(),'N','P')
        endif
        .oPgFrm.Page1.oPag.AGKIT_ZOOM.Calculate(.w_RIGA)
          .link_1_54('Full')
        if .o_CODPRAT<>.w_CODPRAT
            .w_CACHKFOR = iif( IsAlt() AND (.w_MPFLSOSP='S' OR EMPTY(.w_MPFLSOSP)), 'S' , ' ')
        endif
        .DoRTCalc(44,60,.t.)
            .w_TIPATT = 'A'
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        if .o_CACHKINI<>.w_CACHKINI
          .Calculate_ZXFSNGXOLS()
        endif
        .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(62,73,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ITER_ZOOM.Calculate(.w_Cambiaprat)
        .oPgFrm.Page1.oPag.AGKIT_ZOOM.Calculate(.w_RIGA)
        .oPgFrm.Page1.oPag.oObj_1_87.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
    endwith
  return

  proc Calculate_BNMKOBVVDZ()
    with this
          * --- Controlli a checkform
          GSAG_BIT(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_PSWLDQHWYO()
    with this
          * --- Seleziona i partecipanti
          GSAG_BIT(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_HXUEPKMSGY()
    with this
          * --- Aggiorna check Visualizza pers/ris dello studio
          .w_VISALLPART = IIF(EMPTY(.w_CODPRAT) or Not IsAlt(), 'S', 'N')
    endwith
  endproc
  proc Calculate_MDLTXGFHSW()
    with this
          * --- Controlli a checkform
          GSAG_BIT(this;
              ,'A';
             )
    endwith
  endproc
  proc Calculate_LNRIFBEQEI()
    with this
          * --- Apre tipo attivit�
          .a = opengest('A','GSAG_MCA','CACODICE',.w_CAUATT)
    endwith
  endproc
  proc Calculate_ATRHLQSOIB()
    with this
          * --- DaCostoaRicavo
          .w_CENRIC = IIF(Empty(.w_CENRIC) AND (g_PERCCM='S' OR IsAhr()),.w_CC_CONTO,.w_CENRIC)
          .link_1_9('Full')
          .w_COMRIC = IIF(Empty(.w_COMRIC),.w_CODPRAT,.w_COMRIC)
          .link_1_10('Full')
          .w_ATTRIC = IIF(Empty(.w_ATTRIC) AND .w_COMRIC=.w_CODPRAT AND g_PERCAN='S',.w_ATTCOS,.w_ATTRIC)
          .link_1_11('Full')
    endwith
  endproc
  proc Calculate_LKWTWVRCIM()
    with this
          * --- DaRicavoaCosto
          .w_CC_CONTO = IIF(Empty(.w_CC_CONTO) AND g_COAN = 'S',.w_CENRIC,.w_CC_CONTO)
          .link_1_6('Full')
          .w_CODPRAT = IIF(Empty(.w_CODPRAT),.w_COMRIC,.w_CODPRAT)
          .link_1_7('Full')
          .w_ATTCOS = IIF(Empty(.w_ATTCOS) AND .w_COMRIC=.w_CODPRAT AND g_PERCAN='S',.w_ATTRIC,.w_ATTCOS)
          .link_1_8('Full')
    endwith
  endproc
  proc Calculate_ZXFSNGXOLS()
    with this
          * --- Azzera ora e minuti
          .w_ORAINI = IIF(.w_CACHKINI='T', space(2), .w_ORAINI)
          .w_MININI = IIF(.w_CACHKINI='T', space(2), .w_MININI)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATTCOS_1_8.enabled = this.oPgFrm.Page1.oPag.oATTCOS_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCENRIC_1_9.enabled = this.oPgFrm.Page1.oPag.oCENRIC_1_9.mCond()
    this.oPgFrm.Page1.oPag.oATTRIC_1_11.enabled = this.oPgFrm.Page1.oPag.oATTRIC_1_11.mCond()
    this.oPgFrm.Page1.oPag.oMININI_1_20.enabled = this.oPgFrm.Page1.oPag.oMININI_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCACHKINI_1_51.enabled = this.oPgFrm.Page1.oPag.oCACHKINI_1_51.mCond()
    this.oPgFrm.Page1.oPag.oCACHKFOR_1_55.enabled = this.oPgFrm.Page1.oPag.oCACHKFOR_1_55.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCC_CONTO_1_6.visible=!this.oPgFrm.Page1.oPag.oCC_CONTO_1_6.mHide()
    this.oPgFrm.Page1.oPag.oATTCOS_1_8.visible=!this.oPgFrm.Page1.oPag.oATTCOS_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCENRIC_1_9.visible=!this.oPgFrm.Page1.oPag.oCENRIC_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCOMRIC_1_10.visible=!this.oPgFrm.Page1.oPag.oCOMRIC_1_10.mHide()
    this.oPgFrm.Page1.oPag.oATTRIC_1_11.visible=!this.oPgFrm.Page1.oPag.oATTRIC_1_11.mHide()
    this.oPgFrm.Page1.oPag.oDESPRAT_1_12.visible=!this.oPgFrm.Page1.oPag.oDESPRAT_1_12.mHide()
    this.oPgFrm.Page1.oPag.oORAINI_1_19.visible=!this.oPgFrm.Page1.oPag.oORAINI_1_19.mHide()
    this.oPgFrm.Page1.oPag.oMININI_1_20.visible=!this.oPgFrm.Page1.oPag.oMININI_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCCDESPIA_1_48.visible=!this.oPgFrm.Page1.oPag.oCCDESPIA_1_48.mHide()
    this.oPgFrm.Page1.oPag.oONLYFIRST_1_52.visible=!this.oPgFrm.Page1.oPag.oONLYFIRST_1_52.mHide()
    this.oPgFrm.Page1.oPag.oCACHKFOR_1_55.visible=!this.oPgFrm.Page1.oPag.oCACHKFOR_1_55.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oCCDESRIC_1_77.visible=!this.oPgFrm.Page1.oPag.oCCDESRIC_1_77.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_78.visible=!this.oPgFrm.Page1.oPag.oStr_1_78.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_79.visible=!this.oPgFrm.Page1.oPag.oStr_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_81.visible=!this.oPgFrm.Page1.oPag.oStr_1_81.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_85.visible=!this.oPgFrm.Page1.oPag.oStr_1_85.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_97.visible=!this.oPgFrm.Page1.oPag.oStr_1_97.mHide()
    this.oPgFrm.Page1.oPag.oVISALLPART_1_98.visible=!this.oPgFrm.Page1.oPag.oVISALLPART_1_98.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_99.visible=!this.oPgFrm.Page1.oPag.oStr_1_99.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_kit
    if Cevent='Done' and Used('RigheUte') 
       Use in RigheUte
    endif
    if Cevent='w_CODPRAT Changed' and Isalt()
       * aggiorno partecipanti al cambiare della pratica
       This.Notifyevent('AggPart')
       This.w_NOBFQ=.t.
    endif
    IF Cevent='w_CODITER Changed'
       This.w_NOBFQ=.t.
    Endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_BNMKOBVVDZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_AGKIT_ZOOM after query")
          .Calculate_PSWLDQHWYO()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ITER_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_CODPRAT Changed")
          .Calculate_HXUEPKMSGY()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.AGKIT_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("Aggiorna")
          .Calculate_MDLTXGFHSW()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_87.Event(cEvent)
        if lower(cEvent)==lower("w_ITER_ZOOM selected")
          .Calculate_LNRIFBEQEI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaCostoaRicavo")
          .Calculate_ATRHLQSOIB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaRicavoaCosto")
          .Calculate_LKWTWVRCIM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_101.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_102.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kit
    * Valorizzo dati analitica
    If Cevent='w_ATTRIC Changed' or  Cevent='w_CENRIC Changed' or Cevent='w_COMRIC Changed' and !Isalt()
       This.Notifyevent('DaRicavoaCosto')
    Endif
    If Cevent='w_CODPRAT Changed' or  Cevent='w_ATTCOS Changed' or Cevent='w_CC_CONTO Changed' and !Isalt()
       This.Notifyevent('DaCostoaRicavo')
    Endif
    IF Cevent='w_CODITER Changed' or (Cevent='w_CODPRAT Changed' and Isalt())
       This.w_NOBFQ=.f.
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PAR_AGEN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAR_AGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAR_AGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACHKFES,PACALDAT,PACHKATT";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PAR_AGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PAR_AGEN)
            select PACODAZI,PACHKFES,PACALDAT,PACHKATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAR_AGEN = NVL(_Link_.PACODAZI,space(5))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
      this.w_PACALDAT = NVL(_Link_.PACALDAT,space(1))
      this.w_PACHKATT = NVL(_Link_.PACHKATT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PAR_AGEN = space(5)
      endif
      this.w_PACHKFES = space(1)
      this.w_PACALDAT = space(1)
      this.w_PACHKATT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAR_AGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODITER
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
    i_lTable = "PRO_ITER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2], .t., this.PRO_ITER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODITER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIP',True,'PRO_ITER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IPCODICE like "+cp_ToStrODBC(trim(this.w_CODITER)+"%");

          i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IPCODICE',trim(this.w_CODITER))
          select IPCODICE,IPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODITER)==trim(_Link_.IPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IPDESCRI like "+cp_ToStrODBC(trim(this.w_CODITER)+"%");

            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IPDESCRI like "+cp_ToStr(trim(this.w_CODITER)+"%");

            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODITER) and !this.bDontReportError
            deferred_cp_zoom('PRO_ITER','*','IPCODICE',cp_AbsName(oSource.parent,'oCODITER_1_4'),i_cWhere,'GSAG_MIP',"Raggruppamenti di attivit�",''+iif(EMPTY(i_curform.w_TIPOEVENTO),"GSAG_MIP", "GSAG_KIT")+'.PRO_ITER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IPCODICE',oSource.xKey(1))
            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODITER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IPCODICE,IPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IPCODICE="+cp_ToStrODBC(this.w_CODITER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IPCODICE',this.w_CODITER)
            select IPCODICE,IPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODITER = NVL(_Link_.IPCODICE,space(10))
      this.w_DESCRITER = NVL(_Link_.IPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODITER = space(10)
      endif
      this.w_DESCRITER = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])+'\'+cp_ToStr(_Link_.IPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRO_ITER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODITER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CC_CONTO
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CC_CONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CC_CONTO)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CC_CONTO))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CC_CONTO)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CC_CONTO) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCC_CONTO_1_6'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CC_CONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CC_CONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CC_CONTO)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CC_CONTO = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CC_CONTO = space(15)
      endif
      this.w_CCDESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CC_CONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRAT
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZZ',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRAT))
          select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRAT)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRAT)+"%");

            select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNLOCALI like "+cp_ToStrODBC(trim(this.w_CODPRAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNLOCALI like "+cp_ToStr(trim(this.w_CODPRAT)+"%");

            select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CN__ENTE like "+cp_ToStrODBC(trim(this.w_CODPRAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CN__ENTE like "+cp_ToStr(trim(this.w_CODPRAT)+"%");

            select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRAT) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRAT_1_7'),i_cWhere,'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRAT)
            select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA,CNMATPRA,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRAT = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRAT = NVL(_Link_.CNDESCAN,space(55))
      this.w_LOC_PRA = NVL(_Link_.CNLOCALI,space(25))
      this.w_ENTE = NVL(_Link_.CN__ENTE,space(10))
      this.w_UFFICIO = NVL(_Link_.CNUFFICI,space(10))
      this.w_VALPRA = NVL(_Link_.CNCODVAL,space(3))
      this.w_LISPRA = NVL(_Link_.CNCODLIS,space(5))
      this.w_CNFLAPON = NVL(_Link_.CNFLAPON,space(1))
      this.w_CNFLVALO = NVL(_Link_.CNFLVALO,space(1))
      this.w_CNIMPORT = NVL(_Link_.CNIMPORT,0)
      this.w_CNCALDIR = NVL(_Link_.CNCALDIR,space(1))
      this.w_CNCOECAL = NVL(_Link_.CNCOECAL,0)
      this.w_PARASS = NVL(_Link_.CNPARASS,0)
      this.w_FLAMPA = NVL(_Link_.CNFLAMPA,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_TIPOPRAT = NVL(_Link_.CNTIPPRA,space(10))
      this.w_TipPra = NVL(_Link_.CNTIPPRA,space(10))
      this.w_MatPra = NVL(_Link_.CNMATPRA,space(10))
      this.w_CNDATFIN = NVL(cp_ToDate(_Link_.CNDATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRAT = space(15)
      endif
      this.w_DESPRAT = space(55)
      this.w_LOC_PRA = space(25)
      this.w_ENTE = space(10)
      this.w_UFFICIO = space(10)
      this.w_VALPRA = space(3)
      this.w_LISPRA = space(5)
      this.w_CNFLAPON = space(1)
      this.w_CNFLVALO = space(1)
      this.w_CNIMPORT = 0
      this.w_CNCALDIR = space(1)
      this.w_CNCOECAL = 0
      this.w_PARASS = 0
      this.w_FLAMPA = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPOPRAT = space(10)
      this.w_TipPra = space(10)
      this.w_MatPra = space(10)
      this.w_CNDATFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODPRAT = space(15)
        this.w_DESPRAT = space(55)
        this.w_LOC_PRA = space(25)
        this.w_ENTE = space(10)
        this.w_UFFICIO = space(10)
        this.w_VALPRA = space(3)
        this.w_LISPRA = space(5)
        this.w_CNFLAPON = space(1)
        this.w_CNFLVALO = space(1)
        this.w_CNIMPORT = 0
        this.w_CNCALDIR = space(1)
        this.w_CNCOECAL = 0
        this.w_PARASS = 0
        this.w_FLAMPA = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPOPRAT = space(10)
        this.w_TipPra = space(10)
        this.w_MatPra = space(10)
        this.w_CNDATFIN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_bRes and i_cCtrl='Drop'
      with this
        if i_bRes and !( ((.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) and (.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))) OR ! Isalt())
          i_bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
          if not(i_bRes)
            this.w_CODPRAT = space(15)
            this.w_DESPRAT = space(55)
            this.w_LOC_PRA = space(25)
            this.w_ENTE = space(10)
            this.w_UFFICIO = space(10)
            this.w_VALPRA = space(3)
            this.w_LISPRA = space(5)
            this.w_CNFLAPON = space(1)
            this.w_CNFLVALO = space(1)
            this.w_CNIMPORT = 0
            this.w_CNCALDIR = space(1)
            this.w_CNCOECAL = 0
            this.w_PARASS = 0
            this.w_FLAMPA = space(1)
            this.w_DATOBSO = ctod("  /  /  ")
            this.w_TIPOPRAT = space(10)
            this.w_TipPra = space(10)
            this.w_MatPra = space(10)
            this.w_CNDATFIN = ctod("  /  /  ")
          endif
        endif
      endwith
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTCOS
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTCOS)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODPRAT);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODPRAT;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_ATTCOS))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTCOS)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTCOS) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATTCOS_1_8'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODPRAT<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODPRAT);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTCOS);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODPRAT);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODPRAT;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_ATTCOS)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTCOS = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ATTCOS = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENRIC
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CENRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CENRIC))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CENRIC)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CENRIC) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCENRIC_1_9'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENRIC)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENRIC = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESRIC = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CENRIC = space(15)
      endif
      this.w_CCDESRIC = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CENRIC = space(15)
        this.w_CCDESRIC = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMRIC
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMRIC))
          select CNCODCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMRIC)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMRIC) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMRIC_1_10'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMRIC)
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMRIC = NVL(_Link_.CNCODCAN,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COMRIC = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa ricavi incongruente o obsoleto")
        endif
        this.w_COMRIC = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTRIC
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTRIC)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_COMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_COMRIC;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_ATTRIC))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTRIC)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTRIC) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATTRIC_1_11'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COMRIC<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_COMRIC);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTRIC);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_COMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_COMRIC;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_ATTRIC)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTRIC = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ATTRIC = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_1_13'),i_cWhere,'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_PERSON = NVL(_Link_.NODESCRI,space(60))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(10))
      this.w_DATOBSONOM = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_ATTELEFO = NVL(_Link_.NOTELEFO,space(18))
      this.w_AT___FAX = NVL(_Link_.NOTELFAX,space(18))
      this.w_AT_EMAIL = NVL(_Link_.NO_EMAIL,space(0))
      this.w_AT_EMPEC = NVL(_Link_.NO_EMPEC,space(0))
      this.w_ATCELLUL = NVL(_Link_.NONUMCEL,space(18))
      this.w_LOC_NOM = NVL(_Link_.NOLOCALI,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_PERSON = space(60)
      this.w_COMODO = space(10)
      this.w_DATOBSONOM = ctod("  /  /  ")
      this.w_ATTELEFO = space(18)
      this.w_AT___FAX = space(18)
      this.w_AT_EMAIL = space(0)
      this.w_AT_EMPEC = space(0)
      this.w_ATCELLUL = space(18)
      this.w_LOC_NOM = space(25)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSONOM>.w_OFDATDOC OR EMPTY(.w_DATOBSONOM)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM = space(15)
        this.w_PERSON = space(60)
        this.w_COMODO = space(10)
        this.w_DATOBSONOM = ctod("  /  /  ")
        this.w_ATTELEFO = space(18)
        this.w_AT___FAX = space(18)
        this.w_AT_EMAIL = space(0)
        this.w_AT_EMPEC = space(0)
        this.w_ATCELLUL = space(18)
        this.w_LOC_NOM = space(25)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUATT
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CAMODAPP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CAUATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CAUATT)
            select CACODICE,CAMODAPP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUATT = NVL(_Link_.CACODICE,space(5))
      this.w_MODAPP = NVL(_Link_.CAMODAPP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUATT = space(5)
      endif
      this.w_MODAPP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MatPra
  func Link_1_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_MATE_IDX,3]
    i_lTable = "PRA_MATE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_MATE_IDX,2], .t., this.PRA_MATE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_MATE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MatPra) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MatPra)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPFLSOSP";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_MatPra);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_MatPra)
            select MPCODICE,MPFLSOSP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MatPra = NVL(_Link_.MPCODICE,space(10))
      this.w_MPFLSOSP = NVL(_Link_.MPFLSOSP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MatPra = space(10)
      endif
      this.w_MPFLSOSP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_MATE_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_MATE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MatPra Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODITER_1_4.value==this.w_CODITER)
      this.oPgFrm.Page1.oPag.oCODITER_1_4.value=this.w_CODITER
    endif
    if not(this.oPgFrm.Page1.oPag.oCC_CONTO_1_6.value==this.w_CC_CONTO)
      this.oPgFrm.Page1.oPag.oCC_CONTO_1_6.value=this.w_CC_CONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRAT_1_7.value==this.w_CODPRAT)
      this.oPgFrm.Page1.oPag.oCODPRAT_1_7.value=this.w_CODPRAT
    endif
    if not(this.oPgFrm.Page1.oPag.oATTCOS_1_8.value==this.w_ATTCOS)
      this.oPgFrm.Page1.oPag.oATTCOS_1_8.value=this.w_ATTCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oCENRIC_1_9.value==this.w_CENRIC)
      this.oPgFrm.Page1.oPag.oCENRIC_1_9.value=this.w_CENRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMRIC_1_10.value==this.w_COMRIC)
      this.oPgFrm.Page1.oPag.oCOMRIC_1_10.value=this.w_COMRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oATTRIC_1_11.value==this.w_ATTRIC)
      this.oPgFrm.Page1.oPag.oATTRIC_1_11.value=this.w_ATTRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRAT_1_12.value==this.w_DESPRAT)
      this.oPgFrm.Page1.oPag.oDESPRAT_1_12.value=this.w_DESPRAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNOM_1_13.value==this.w_CODNOM)
      this.oPgFrm.Page1.oPag.oCODNOM_1_13.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oPERSON_1_14.value==this.w_PERSON)
      this.oPgFrm.Page1.oPag.oPERSON_1_14.value=this.w_PERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRITER_1_16.value==this.w_DESCRITER)
      this.oPgFrm.Page1.oPag.oDESCRITER_1_16.value=this.w_DESCRITER
    endif
    if not(this.oPgFrm.Page1.oPag.oDATRIF_1_18.value==this.w_DATRIF)
      this.oPgFrm.Page1.oPag.oDATRIF_1_18.value=this.w_DATRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_19.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_19.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_20.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_20.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oGIORIF_1_47.value==this.w_GIORIF)
      this.oPgFrm.Page1.oPag.oGIORIF_1_47.value=this.w_GIORIF
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA_1_48.value==this.w_CCDESPIA)
      this.oPgFrm.Page1.oPag.oCCDESPIA_1_48.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oCACHKINI_1_51.RadioValue()==this.w_CACHKINI)
      this.oPgFrm.Page1.oPag.oCACHKINI_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oONLYFIRST_1_52.RadioValue()==this.w_ONLYFIRST)
      this.oPgFrm.Page1.oPag.oONLYFIRST_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACHKFOR_1_55.RadioValue()==this.w_CACHKFOR)
      this.oPgFrm.Page1.oPag.oCACHKFOR_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESRIC_1_77.value==this.w_CCDESRIC)
      this.oPgFrm.Page1.oPag.oCCDESRIC_1_77.value=this.w_CCDESRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oVISALLPART_1_98.RadioValue()==this.w_VISALLPART)
      this.oPgFrm.Page1.oPag.oVISALLPART_1_98.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODITER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODITER_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CODITER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(empty(.w_CODPRAT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPRAT_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO))  and not( Isalt())  and ((g_PERCCM='S' OR IsAhr()))  and not(empty(.w_CENRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCENRIC_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(Isalt())  and not(empty(.w_COMRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMRIC_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa ricavi incongruente o obsoleto")
          case   not(.w_DATOBSONOM>.w_OFDATDOC OR EMPTY(.w_DATOBSONOM))  and not(empty(.w_CODNOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODNOM_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATRIF)) or not(.w_PACHKFES='N' OR Chkfestivi(.w_DATRIF, .w_PACHKFES, this)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATRIF_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DATRIF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_ORAINI) < 24)  and not(!Isalt() )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAINI_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   ((empty(.w_MININI)) or not(VAL(.w_MININI) < 60))  and not(!Isalt() )  and (Not empty(.w_ORAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_20.SetFocus()
            i_bnoObbl = !empty(.w_MININI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_kit
      * --- Controlli Finali
      IF i_bRes=.t.
         .w_RESCHK=0
         .NotifyEvent('ChkFinali')
         WAIT CLEAR
         IF .w_RESCHK<>0
           i_bRes=.f.
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODPRAT = this.w_CODPRAT
    this.o_CODNOM = this.w_CODNOM
    this.o_DATRIF = this.w_DATRIF
    this.o_ORAINI = this.w_ORAINI
    this.o_CACHKINI = this.w_CACHKINI
    return

enddefine

* --- Define pages as container
define class tgsag_kitPag1 as StdContainer
  Width  = 656
  height = 551
  stdWidth  = 656
  stdheight = 551
  resizeXpos=453
  resizeYpos=433
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODITER_1_4 as StdField with uid="LUIWKXTMHR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODITER", cQueryName = "CODITER",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice raggruppamento",;
    HelpContextID = 178482214,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=117, Top=12, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRO_ITER", cZoomOnZoom="GSAG_MIP", oKey_1_1="IPCODICE", oKey_1_2="this.w_CODITER"

  func oCODITER_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODITER_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODITER_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRO_ITER','*','IPCODICE',cp_AbsName(this.parent,'oCODITER_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIP',"Raggruppamenti di attivit�",''+iif(EMPTY(i_curform.w_TIPOEVENTO),"GSAG_MIP", "GSAG_KIT")+'.PRO_ITER_VZM',this.parent.oContained
  endproc
  proc oCODITER_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IPCODICE=this.parent.oContained.w_CODITER
     i_obj.ecpSave()
  endproc

  add object oCC_CONTO_1_6 as StdField with uid="ZYMECVJAQV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CC_CONTO", cQueryName = "CC_CONTO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice centro di costo/ricavo",;
    HelpContextID = 55513205,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=117, Top=38, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CC_CONTO"

  func oCC_CONTO_1_6.mHide()
    with this.Parent.oContained
      return (g_COAN <> 'S')
    endwith
  endfunc

  func oCC_CONTO_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCC_CONTO_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCC_CONTO_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCC_CONTO_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCC_CONTO_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CC_CONTO
     i_obj.ecpSave()
  endproc

  add object oCODPRAT_1_7 as StdField with uid="LNZCMMZIKS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODPRAT", cQueryName = "CODPRAT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 109734950,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=117, Top=64, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_BZZ", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRAT"

  func oCODPRAT_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
      if .not. empty(.w_ATTCOS)
        bRes2=.link_1_8('Full')
      endif
      if bRes and !( ((.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) and (.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))) OR ! Isalt())
         bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oCODPRAT_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRAT_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRAT_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODPRAT_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRAT
     i_obj.ecpSave()
  endproc

  add object oATTCOS_1_8 as StdField with uid="VVSXSZJDQD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ATTCOS", cQueryName = "ATTCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa costi",;
    HelpContextID = 139358470,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=529, Top=64, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODPRAT", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_ATTCOS"

  func oATTCOS_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_codprat))
    endwith
   endif
  endfunc

  func oATTCOS_1_8.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oATTCOS_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTCOS_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTCOS_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODPRAT)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODPRAT)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATTCOS_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oATTCOS_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODPRAT
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_ATTCOS
     i_obj.ecpSave()
  endproc

  add object oCENRIC_1_9 as StdField with uid="GWCBQUFVPT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CENRIC", cQueryName = "CENRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 134413786,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=117, Top=91, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENRIC"

  func oCENRIC_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCM='S' OR IsAhr()))
    endwith
   endif
  endfunc

  func oCENRIC_1_9.mHide()
    with this.Parent.oContained
      return ( Isalt())
    endwith
  endfunc

  func oCENRIC_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCENRIC_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCENRIC_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCENRIC_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCENRIC_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CENRIC
     i_obj.ecpSave()
  endproc

  add object oCOMRIC_1_10 as StdField with uid="CCYDSSPMTJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_COMRIC", cQueryName = "COMRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa ricavi incongruente o obsoleto",;
    ToolTipText = "Codice commessa ricavi",;
    HelpContextID = 134415322,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=117, Top=118, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMRIC"

  func oCOMRIC_1_10.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oCOMRIC_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
      if .not. empty(.w_ATTRIC)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCOMRIC_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMRIC_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMRIC_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCOMRIC_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_COMRIC
     i_obj.ecpSave()
  endproc

  add object oATTRIC_1_11 as StdField with uid="LTBYOWJTQX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ATTRIC", cQueryName = "ATTRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 134385402,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=529, Top=118, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_COMRIC", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_ATTRIC"

  func oATTRIC_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_COMRIC))
    endwith
   endif
  endfunc

  func oATTRIC_1_11.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oATTRIC_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTRIC_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTRIC_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_COMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_COMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATTRIC_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oATTRIC_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_COMRIC
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_ATTRIC
     i_obj.ecpSave()
  endproc

  add object oDESPRAT_1_12 as StdField with uid="DHZPCSQKFH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESPRAT", cQueryName = "DESPRAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(55), bMultilanguage =  .f.,;
    HelpContextID = 109793846,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=256, Top=64, InputMask=replicate('X',55)

  func oDESPRAT_1_12.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oCODNOM_1_13 as StdField with uid="JVWHREDOHG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 39349286,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=117, Top=156, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc

  add object oPERSON_1_14 as StdField with uid="TQJYTGWRDG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PERSON", cQueryName = "PERSON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione nominativo",;
    HelpContextID = 56509174,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=256, Top=156, InputMask=replicate('X',60)

  add object oDESCRITER_1_16 as StdField with uid="HIHAVWGZOZ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCRITER", cQueryName = "DESCRITER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 243160987,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=204, Top=12, InputMask=replicate('X',40)

  add object oDATRIF_1_18 as StdField with uid="DOFQIEHNYM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATRIF", cQueryName = "DATRIF",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento per il calcolo dei giorni",;
    HelpContextID = 84058570,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=170, Top=185

  func oDATRIF_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PACHKFES='N' OR Chkfestivi(.w_DATRIF, .w_PACHKFES, this))
    endwith
    return bRes
  endfunc

  add object oORAINI_1_19 as StdField with uid="VGJIHEDZAE",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di inizio delle attivit� collegate",;
    HelpContextID = 29147162,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=314, Top=185, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI_1_19.mHide()
    with this.Parent.oContained
      return (!Isalt() )
    endwith
  endfunc

  func oORAINI_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_20 as StdField with uid="NXNGNCXJWL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio delle attivit� collegate",;
    HelpContextID = 29096250,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=356, Top=185, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not empty(.w_ORAINI))
    endwith
   endif
  endfunc

  func oMININI_1_20.mHide()
    with this.Parent.oContained
      return (!Isalt() )
    endwith
  endfunc

  func oMININI_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc


  add object oBtn_1_25 as StdButton with uid="ERHOXVEEGI",left=539, top=501, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 266975002;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        AggioProvv(this.Parent.oContained,"B")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="TKCMXZXILU",left=593, top=501, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 259686330;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ITER_ZOOM as cp_zoombox with uid="PGJGGUCAWR",left=22, top=237, width=627,height=116,;
    caption='ITER_ZOOM',;
   bGlobalFont=.t.,;
    cTable="PRO_ITER",cZoomFile="GSAGIKIT",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bRetriveAllRows=.f.,;
    cEvent = "Blank,w_CODITER Changed",;
    nPag=1;
    , HelpContextID = 6063781

  add object oGIORIF_1_47 as StdField with uid="NUWQBJKVOI",rtseq=37,rtrep=.f.,;
    cFormVar = "w_GIORIF", cQueryName = "GIORIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 84076954,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=117, Top=185, InputMask=replicate('X',3)

  add object oCCDESPIA_1_48 as StdField with uid="JMXGVHYZRV",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 175153049,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=256, Top=38, InputMask=replicate('X',40)

  func oCCDESPIA_1_48.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S')
    endwith
  endfunc


  add object oCACHKINI_1_51 as StdCombo with uid="JDBQLUXVZW",rtseq=40,rtrep=.f.,left=499,top=186,width=133,height=21;
    , ToolTipText = "Metodo di calcolo della data inizio attivit�";
    , HelpContextID = 32354705;
    , cFormVar="w_CACHKINI",RowSource=""+"Da tipo attivit�,"+"Con conferma,"+"Automatica,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCACHKINI_1_51.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    iif(this.value =3,'A',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oCACHKINI_1_51.GetRadio()
    this.Parent.oContained.w_CACHKINI = this.RadioValue()
    return .t.
  endfunc

  func oCACHKINI_1_51.SetRadio()
    this.Parent.oContained.w_CACHKINI=trim(this.Parent.oContained.w_CACHKINI)
    this.value = ;
      iif(this.Parent.oContained.w_CACHKINI=='T',1,;
      iif(this.Parent.oContained.w_CACHKINI=='C',2,;
      iif(this.Parent.oContained.w_CACHKINI=='A',3,;
      iif(this.Parent.oContained.w_CACHKINI=='N',4,;
      0))))
  endfunc

  func oCACHKINI_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PACHKFES<>'N')
    endwith
   endif
  endfunc


  add object oONLYFIRST_1_52 as StdCombo with uid="HOAEJUOZCZ",rtseq=41,rtrep=.f.,left=314,top=211,width=136,height=21;
    , ToolTipText = "Applica ora impostata alle attivit� indicate del raggruppamento";
    , HelpContextID = 231993721;
    , cFormVar="w_ONLYFIRST",RowSource=""+"Solo alla prima,"+"Solo alla prima udienza,"+"Tutte,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oONLYFIRST_1_52.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'U',;
    iif(this.value =3,'T',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oONLYFIRST_1_52.GetRadio()
    this.Parent.oContained.w_ONLYFIRST = this.RadioValue()
    return .t.
  endfunc

  func oONLYFIRST_1_52.SetRadio()
    this.Parent.oContained.w_ONLYFIRST=trim(this.Parent.oContained.w_ONLYFIRST)
    this.value = ;
      iif(this.Parent.oContained.w_ONLYFIRST=='P',1,;
      iif(this.Parent.oContained.w_ONLYFIRST=='U',2,;
      iif(this.Parent.oContained.w_ONLYFIRST=='T',3,;
      iif(this.Parent.oContained.w_ONLYFIRST=='N',4,;
      0))))
  endfunc

  func oONLYFIRST_1_52.mHide()
    with this.Parent.oContained
      return (!Isalt() or Empty(.w_ORAINI))
    endwith
  endfunc


  add object AGKIT_ZOOM as cp_szoombox with uid="WODZOSNAJX",left=22, top=371, width=628,height=127,;
    caption='AGKIT_ZOOM',;
   bGlobalFont=.t.,;
    cTable="DIPENDEN",cZoomFile="GSAG_KIT",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,;
    cEvent = "Ricalcola,w_VISALLPART Changed",;
    nPag=1;
    , HelpContextID = 190568891

  add object oCACHKFOR_1_55 as StdCheck with uid="SZVHUJOINL",rtseq=43,rtrep=.f.,left=465, top=210, caption="Sospensioni forensi",;
    ToolTipText = "Se attivo nella determinazione della data inizio delle attivit� tiene conto anche della sospensione forense",;
    HelpContextID = 82686344,;
    cFormVar="w_CACHKFOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCACHKFOR_1_55.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCACHKFOR_1_55.GetRadio()
    this.Parent.oContained.w_CACHKFOR = this.RadioValue()
    return .t.
  endfunc

  func oCACHKFOR_1_55.SetRadio()
    this.Parent.oContained.w_CACHKFOR=trim(this.Parent.oContained.w_CACHKFOR)
    this.value = ;
      iif(this.Parent.oContained.w_CACHKFOR=='S',1,;
      0)
  endfunc

  func oCACHKFOR_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PACHKFES<>'N' And .w_PACHKFES<>'T')
    endwith
   endif
  endfunc

  func oCACHKFOR_1_55.mHide()
    with this.Parent.oContained
      return (not IsAlt() Or .w_CACHKINI='N' Or .w_CACHKINI='T' Or .w_PACHKFES<>'C')
    endwith
  endfunc

  add object oCCDESRIC_1_77 as StdField with uid="OQBJBQEOLA",rtseq=60,rtrep=.t.,;
    cFormVar = "w_CCDESRIC", cQueryName = "CCDESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 141598615,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=256, Top=92, InputMask=replicate('X',40)

  func oCCDESRIC_1_77.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oObj_1_87 as cp_runprogram with uid="XLZUQUWMJN",left=-3, top=589, width=274,height=18,;
    caption='GSAG_BIT(Q)',;
   bGlobalFont=.t.,;
    prg="GSAG_BIT('Q')",;
    cEvent = "w_AGKIT_ZOOM before query,SalvaImp",;
    nPag=1;
    , HelpContextID = 128703430


  add object oBtn_1_90 as StdButton with uid="VDSDBBLRZY",left=32, top=501, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Applica a tutti i tipi attivit� le impostazioni dei partecipanti";
    , HelpContextID = 169430278;
    , Caption='\<Applica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_90.Click()
      this.parent.oContained.NotifyEvent("Applica")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oVISALLPART_1_98 as StdCheck with uid="XRQFQZLGCR",rtseq=67,rtrep=.f.,left=244, top=360, caption="Visualizza tutte le persone oltre a quelle della pratica",;
    ToolTipText = "Se attivo, nell'elenco dei partecipanti alle singole attivit� sara possile inserire persone anche se non presenti fra i soggetti interni della pratica",;
    HelpContextID = 18657463,;
    cFormVar="w_VISALLPART", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVISALLPART_1_98.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVISALLPART_1_98.GetRadio()
    this.Parent.oContained.w_VISALLPART = this.RadioValue()
    return .t.
  endfunc

  func oVISALLPART_1_98.SetRadio()
    this.Parent.oContained.w_VISALLPART=trim(this.Parent.oContained.w_VISALLPART)
    this.value = ;
      iif(this.Parent.oContained.w_VISALLPART=='S',1,;
      0)
  endfunc

  func oVISALLPART_1_98.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODPRAT) OR NOT ISALT() or .w_MODAPP='D')
    endwith
  endfunc


  add object oObj_1_101 as cp_runprogram with uid="KYFUSRFRIA",left=-4, top=604, width=274,height=21,;
    caption='GSAG_BIT(P)',;
   bGlobalFont=.t.,;
    prg="GSAG_BIT('P')",;
    cEvent = "Applica",;
    nPag=1;
    , HelpContextID = 128703686


  add object oObj_1_102 as cp_runprogram with uid="CUKJOQXHZB",left=471, top=706, width=274,height=21,;
    caption='GSAG_BIT(K)',;
   bGlobalFont=.t.,;
    prg="GSAG_BIT('K')",;
    cEvent = "w_CODITER Changed,AggPart",;
    nPag=1;
    , HelpContextID = 128704966

  add object oStr_1_5 as StdString with uid="OQMLWZDYWU",Visible=.t., Left=39, Top=64,;
    Alignment=1, Width=71, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="DWBPIVPSUP",Visible=.t., Left=24, Top=12,;
    Alignment=1, Width=86, Height=18,;
    Caption="Codice raggr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="HAGHVISNFI",Visible=.t., Left=6, Top=185,;
    Alignment=1, Width=104, Height=18,;
    Caption="Data di partenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="OZATNEHMGU",Visible=.t., Left=21, Top=361,;
    Alignment=0, Width=178, Height=18,;
    Caption="Partecipanti alle singole attivit�"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="EBUADEBZKR",Visible=.t., Left=15, Top=40,;
    Alignment=1, Width=95, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="ORILRVBARV",Visible=.t., Left=30, Top=159,;
    Alignment=1, Width=80, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="OEALKSNEIW",Visible=.t., Left=27, Top=64,;
    Alignment=1, Width=83, Height=18,;
    Caption="Comm. costi:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_1_75 as StdString with uid="ASITDWTOLS",Visible=.t., Left=387, Top=186,;
    Alignment=1, Width=108, Height=18,;
    Caption="Calcolo data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="NDNECIPGFN",Visible=.t., Left=6, Top=91,;
    Alignment=1, Width=104, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_1_78.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_79 as StdString with uid="HTOLFADPTS",Visible=.t., Left=25, Top=118,;
    Alignment=1, Width=85, Height=18,;
    Caption="Comm. ricavi:"  ;
  , bGlobalFont=.t.

  func oStr_1_79.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="THZJZYTFBJ",Visible=.t., Left=435, Top=119,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_81 as StdString with uid="VRUOKTUIHY",Visible=.t., Left=435, Top=65,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_81.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_84 as StdString with uid="BYZPFCFPKB",Visible=.t., Left=255, Top=185,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_85 as StdString with uid="FXUDOJAZEE",Visible=.t., Left=342, Top=186,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_85.mHide()
    with this.Parent.oContained
      return (!Isalt() or .w_CACHKINI='T')
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="GDYXYXDDGK",Visible=.t., Left=24, Top=224,;
    Alignment=0, Width=154, Height=18,;
    Caption="Dettaglio raggruppamento"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="IJDBSTNCIJ",Visible=.t., Left=231, Top=212,;
    Alignment=1, Width=77, Height=18,;
    Caption="Applica ora a :"  ;
  , bGlobalFont=.t.

  func oStr_1_97.mHide()
    with this.Parent.oContained
      return (!Isalt() or Empty(.w_ORAINI))
    endwith
  endfunc

  add object oStr_1_99 as StdString with uid="DEYNVFGTOR",Visible=.t., Left=-3, Top=735,;
    Alignment=0, Width=1048, Height=18,;
    Caption="Attenzione! Aggiorna check Visualizza pers/ris dello studio deve avere sequenza minore dello zoom AGKIT_ZOOM per fare s� che il valore di w_VisAllPart che questo riceve come par. sia corretto"  ;
  , bGlobalFont=.t.

  func oStr_1_99.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oBox_1_29 as StdBox with uid="LOIZABPOHX",left=114, top=62, width=137,height=27
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kit','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_kit
proc AggioProvv(obj,pAzione)
   If pAzione='B'
     OBJ.NotifyEvent('ChkFinali')
   Endif
  if obj.w_RESCHK=0
   OBJ.NotifyEvent('Aggiorna')
  endif
endproc
* --- Fine Area Manuale
