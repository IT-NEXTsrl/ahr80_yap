* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bnp                                                        *
*              Lancia caricamento attivita da menu                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-21                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipoAtt
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bnp",oParentObject,m.pTipoAtt)
return(i_retval)

define class tgsag_bnp as StdBatch
  * --- Local variables
  pTipoAtt = space(5)
  w_OBJECT = .NULL.
  w_OBJ = .NULL.
  w_TIPOATTIV = space(20)
  * --- WorkFile variables
  PAR_AGEN_idx=0
  PAR_ATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                                                  Lancia da men� il caricamento di un'attivit�
    * --- Parametro passato da men�
    this.w_TIPOATTIV = space(20)
    * --- Legge da tabella Parametri Attivit� il tipo per caricamento di nuova attivit�
    * --- Read from PAR_ATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_ATTI_idx,2],.t.,this.PAR_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PTTIPATT"+;
        " from "+i_cTable+" PAR_ATTI where ";
            +"PTCODAZI = "+cp_ToStrODBC(i_codazi);
            +" and PTKEYMEN = "+cp_ToStrODBC(this.pTipoAtt);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PTTIPATT;
        from (i_cTable) where;
            PTCODAZI = i_codazi;
            and PTKEYMEN = this.pTipoAtt;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPOATTIV = NVL(cp_ToDate(_read_.PTTIPATT),cp_NullValue(_read_.PTTIPATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se � fallita la lettura 
    if i_Rows = 0 AND NOT EMPTY(this.pTipoAtt)
      ah_ErrorMsg("Attenzione: il tipo attivit� non � configurato correttamente")
    endif
    * --- Istanzio l'anagrafica
    this.w_OBJECT = GSAG_AAT()
    * --- Controllo se ha passato il test di accesso
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- Vado in caricamento
    this.w_OBJECT.ECPLOAD()     
    this.w_OBJECT.w_ATCAUATT = this.w_TIPOATTIV
    * --- Viene attivato il controllo sul campo Tipo attivit� e su di esso eseguito il check
    this.w_OBJ = this.w_OBJECT.GetcTRL("w_ATCAUATT")
    this.w_OBJ.Check()     
    * --- Aggiornamento variabili nell'anagrafica
    this.w_OBJECT.SetControlsValue()     
    this.w_OBJECT.mCalc(.T.)     
  endproc


  proc Init(oParentObject,pTipoAtt)
    this.pTipoAtt=pTipoAtt
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_AGEN'
    this.cWorkTables[2]='PAR_ATTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipoAtt"
endproc
