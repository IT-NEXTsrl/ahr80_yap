* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kia                                                        *
*              Generazione attivit�                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-14                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kia",oParentObject))

* --- Class definition
define class tgsag_kia as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 661
  Height = 444
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-13"
  HelpContextID=267003753
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=47

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  CENCOST_IDX = 0
  CAUMATTI_IDX = 0
  ATTIVITA_IDX = 0
  OFF_NOMI_IDX = 0
  cPrg = "gsag_kia"
  cComment = "Generazione attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CATEGO = space(1)
  w_CACODICE = space(20)
  w_TIPATT = space(10)
  w_CC_CONTO = space(15)
  w_CODPRAT = space(15)
  o_CODPRAT = space(15)
  w_ATTCOS = space(15)
  w_CENRIC = space(15)
  w_COMRIC = space(15)
  w_ATTRIC = space(15)
  w_DESPRAT = space(55)
  w_CODNOM = space(15)
  o_CODNOM = space(15)
  w_PERSON = space(60)
  w_CCDESPIA = space(40)
  w_CCDESRIC = space(40)
  w_CADESCRI = space(254)
  w_NOM_OBBL = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_OB_TEST = ctod('  /  /  ')
  w_CENOBSO = ctod('  /  /  ')
  w_DATOBSONOM = ctod('  /  /  ')
  w_OFDATDOC = ctod('  /  /  ')
  w_COMODO = space(10)
  w_ATTELEFO = space(18)
  w_AT___FAX = space(18)
  w_ATCELLUL = space(18)
  w_AT_EMAIL = space(0)
  w_AT_EMPEC = space(0)
  w_LOCALI = space(25)
  w_LOC_PRA = space(25)
  w_LOC_NOM = space(25)
  w_ENTE = space(10)
  w_UFFICIO = space(10)
  w_VALPRA = space(3)
  w_LISPRA = space(5)
  w_CNFLAPON = space(1)
  w_CNFLVALO = space(1)
  w_CNIMPORT = 0
  w_CNCALDIR = space(1)
  w_CNCOECAL = 0
  w_PARASS = 0
  w_FLAMPA = space(1)
  w_TIPOPRAT = space(10)
  w_PATIPRIS = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_VOCOBSO = ctod('  /  /  ')
  w_OKGEN = space(10)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_ZOOMPART = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kiaPag1","gsag_kia",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCACODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMPART = this.oPgFrm.Pages(1).oPag.ZOOMPART
    DoDefault()
    proc Destroy()
      this.w_ZOOMPART = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CENCOST'
    this.cWorkTables[3]='CAUMATTI'
    this.cWorkTables[4]='ATTIVITA'
    this.cWorkTables[5]='OFF_NOMI'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CATEGO=space(1)
      .w_CACODICE=space(20)
      .w_TIPATT=space(10)
      .w_CC_CONTO=space(15)
      .w_CODPRAT=space(15)
      .w_ATTCOS=space(15)
      .w_CENRIC=space(15)
      .w_COMRIC=space(15)
      .w_ATTRIC=space(15)
      .w_DESPRAT=space(55)
      .w_CODNOM=space(15)
      .w_PERSON=space(60)
      .w_CCDESPIA=space(40)
      .w_CCDESRIC=space(40)
      .w_CADESCRI=space(254)
      .w_NOM_OBBL=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OB_TEST=ctod("  /  /  ")
      .w_CENOBSO=ctod("  /  /  ")
      .w_DATOBSONOM=ctod("  /  /  ")
      .w_OFDATDOC=ctod("  /  /  ")
      .w_COMODO=space(10)
      .w_ATTELEFO=space(18)
      .w_AT___FAX=space(18)
      .w_ATCELLUL=space(18)
      .w_AT_EMAIL=space(0)
      .w_AT_EMPEC=space(0)
      .w_LOCALI=space(25)
      .w_LOC_PRA=space(25)
      .w_LOC_NOM=space(25)
      .w_ENTE=space(10)
      .w_UFFICIO=space(10)
      .w_VALPRA=space(3)
      .w_LISPRA=space(5)
      .w_CNFLAPON=space(1)
      .w_CNFLVALO=space(1)
      .w_CNIMPORT=0
      .w_CNCALDIR=space(1)
      .w_CNCOECAL=0
      .w_PARASS=0
      .w_FLAMPA=space(1)
      .w_TIPOPRAT=space(10)
      .w_PATIPRIS=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_VOCOBSO=ctod("  /  /  ")
      .w_OKGEN=space(10)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      .w_CATEGO=oParentObject.w_CATEGO
      .w_CACODICE=oParentObject.w_CACODICE
      .w_CC_CONTO=oParentObject.w_CC_CONTO
      .w_CODPRAT=oParentObject.w_CODPRAT
      .w_ATTCOS=oParentObject.w_ATTCOS
      .w_CENRIC=oParentObject.w_CENRIC
      .w_COMRIC=oParentObject.w_COMRIC
      .w_ATTRIC=oParentObject.w_ATTRIC
      .w_CODNOM=oParentObject.w_CODNOM
      .w_OKGEN=oParentObject.w_OKGEN
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_CACODICE))
          .link_1_2('Full')
        endif
        .w_TIPATT = 'A'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CC_CONTO))
          .link_1_5('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODPRAT))
          .link_1_6('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ATTCOS))
          .link_1_7('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CENRIC))
          .link_1_8('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_COMRIC))
          .link_1_9('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_ATTRIC))
          .link_1_10('Full')
        endif
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_CODNOM))
          .link_1_12('Full')
        endif
          .DoRTCalc(12,17,.f.)
        .w_OB_TEST = i_datsys
          .DoRTCalc(19,20,.f.)
        .w_OFDATDOC = i_datsys
          .DoRTCalc(22,27,.f.)
        .w_LOCALI = IIF(ISALT(), .w_LOC_PRA, .w_LOC_NOM)
      .oPgFrm.Page1.oPag.ZOOMPART.Calculate()
          .DoRTCalc(29,42,.f.)
        .w_PATIPRIS = 'P'
        .w_OBTEST = i_datsys
          .DoRTCalc(45,45,.f.)
        .w_OKGEN = .t.
        .w_DTBSO_CAUMATTI = i_DatSys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_55.enabled = this.oPgFrm.Page1.oPag.oBtn_1_55.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_56.enabled = this.oPgFrm.Page1.oPag.oBtn_1_56.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CATEGO=.w_CATEGO
      .oParentObject.w_CACODICE=.w_CACODICE
      .oParentObject.w_CC_CONTO=.w_CC_CONTO
      .oParentObject.w_CODPRAT=.w_CODPRAT
      .oParentObject.w_ATTCOS=.w_ATTCOS
      .oParentObject.w_CENRIC=.w_CENRIC
      .oParentObject.w_COMRIC=.w_COMRIC
      .oParentObject.w_ATTRIC=.w_ATTRIC
      .oParentObject.w_CODNOM=.w_CODNOM
      .oParentObject.w_OKGEN=.w_OKGEN
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_TIPATT = 'A'
        .DoRTCalc(4,27,.t.)
        if .o_CODPRAT<>.w_CODPRAT.or. .o_CODNOM<>.w_CODNOM
            .w_LOCALI = IIF(ISALT(), .w_LOC_PRA, .w_LOC_NOM)
        endif
        .oPgFrm.Page1.oPag.ZOOMPART.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(29,47,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMPART.Calculate()
    endwith
  return

  proc Calculate_ATRHLQSOIB()
    with this
          * --- DaCostoaRicavo
          .w_CENRIC = IIF(Empty(.w_CENRIC) AND (g_PERCCM='S' OR IsAhr()),.w_CC_CONTO,.w_CENRIC)
          .link_1_8('Full')
          .w_COMRIC = IIF(Empty(.w_COMRIC),.w_CODPRAT,.w_COMRIC)
          .link_1_9('Full')
          .w_ATTRIC = IIF(Empty(.w_ATTRIC) AND .w_COMRIC=.w_CODPRAT AND g_PERCAN='S',.w_ATTCOS,.w_ATTRIC)
          .link_1_10('Full')
    endwith
  endproc
  proc Calculate_LKWTWVRCIM()
    with this
          * --- DaRicavoaCosto
          .w_CC_CONTO = IIF(Empty(.w_CC_CONTO) AND g_COAN = 'S',.w_CENRIC,.w_CC_CONTO)
          .link_1_5('Full')
          .w_CODPRAT = IIF(Empty(.w_CODPRAT),.w_COMRIC,.w_CODPRAT)
          .link_1_6('Full')
          .w_ATTCOS = IIF(Empty(.w_ATTCOS) AND .w_COMRIC=.w_CODPRAT AND g_PERCAN='S',.w_ATTRIC,.w_ATTCOS)
          .link_1_7('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATTCOS_1_7.enabled = this.oPgFrm.Page1.oPag.oATTCOS_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCENRIC_1_8.enabled = this.oPgFrm.Page1.oPag.oCENRIC_1_8.mCond()
    this.oPgFrm.Page1.oPag.oATTRIC_1_10.enabled = this.oPgFrm.Page1.oPag.oATTRIC_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCC_CONTO_1_5.visible=!this.oPgFrm.Page1.oPag.oCC_CONTO_1_5.mHide()
    this.oPgFrm.Page1.oPag.oATTCOS_1_7.visible=!this.oPgFrm.Page1.oPag.oATTCOS_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCENRIC_1_8.visible=!this.oPgFrm.Page1.oPag.oCENRIC_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCOMRIC_1_9.visible=!this.oPgFrm.Page1.oPag.oCOMRIC_1_9.mHide()
    this.oPgFrm.Page1.oPag.oATTRIC_1_10.visible=!this.oPgFrm.Page1.oPag.oATTRIC_1_10.mHide()
    this.oPgFrm.Page1.oPag.oDESPRAT_1_11.visible=!this.oPgFrm.Page1.oPag.oDESPRAT_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCCDESPIA_1_14.visible=!this.oPgFrm.Page1.oPag.oCCDESPIA_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCCDESRIC_1_18.visible=!this.oPgFrm.Page1.oPag.oCCDESRIC_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMPART.Event(cEvent)
        if lower(cEvent)==lower("DaCostoaRicavo")
          .Calculate_ATRHLQSOIB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaRicavoaCosto")
          .Calculate_LKWTWVRCIM()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kia
    If cevent ='Blank' OR cevent='w_PATIPRIS Changed'
       Select (This.w_ZOOMPART.ccursor)
       Go top
       Update (This.w_ZOOMPART.ccursor) set XCHK=1 where  This.oparentobject.oparentobject.w_prcodute=DPCODICE
       Select (This.w_ZOOMPART.ccursor)
       Go top
    Endif
    * Valorizzo dati analitica
    If Cevent='w_ATTRIC Changed' or  Cevent='w_CENRIC Changed' or Cevent='w_COMRIC Changed' and !Isalt()
       This.Notifyevent('DaRicavoaCosto')
    Endif
    If Cevent='w_CODPRAT Changed' or  Cevent='w_ATTCOS Changed' or Cevent='w_CC_CONTO Changed' and !Isalt()
       This.Notifyevent('DaCostoaRicavo')
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CACODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CACODICE))
          select CACODICE,CADESCRI,CACHKNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_CACODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_CACODICE)+"%");

            select CACODICE,CADESCRI,CACHKNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACODICE) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oCACODICE_1_2'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'GSAGZKII.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKNOM";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CACHKNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKNOM";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODICE)
            select CACODICE,CADESCRI,CACHKNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODICE = NVL(_Link_.CACODICE,space(20))
      this.w_CADESCRI = NVL(_Link_.CADESCRI,space(254))
      this.w_NOM_OBBL = NVL(_Link_.CACHKNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACODICE = space(20)
      endif
      this.w_CADESCRI = space(254)
      this.w_NOM_OBBL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CC_CONTO
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CC_CONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CC_CONTO)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CC_CONTO))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CC_CONTO)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CC_CONTO) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCC_CONTO_1_5'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CC_CONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CC_CONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CC_CONTO)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CC_CONTO = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CC_CONTO = space(15)
      endif
      this.w_CCDESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CC_CONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRAT
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZZ',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRAT))
          select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRAT)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRAT)+"%");

            select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNLOCALI like "+cp_ToStrODBC(trim(this.w_CODPRAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNLOCALI like "+cp_ToStr(trim(this.w_CODPRAT)+"%");

            select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CN__ENTE like "+cp_ToStrODBC(trim(this.w_CODPRAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CN__ENTE like "+cp_ToStr(trim(this.w_CODPRAT)+"%");

            select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRAT) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRAT_1_6'),i_cWhere,'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRAT)
            select CNCODCAN,CNDESCAN,CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNDTOBSO,CNTIPPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRAT = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRAT = NVL(_Link_.CNDESCAN,space(55))
      this.w_LOC_PRA = NVL(_Link_.CNLOCALI,space(25))
      this.w_ENTE = NVL(_Link_.CN__ENTE,space(10))
      this.w_UFFICIO = NVL(_Link_.CNUFFICI,space(10))
      this.w_VALPRA = NVL(_Link_.CNCODVAL,space(3))
      this.w_LISPRA = NVL(_Link_.CNCODLIS,space(5))
      this.w_CNFLAPON = NVL(_Link_.CNFLAPON,space(1))
      this.w_CNFLVALO = NVL(_Link_.CNFLVALO,space(1))
      this.w_CNIMPORT = NVL(_Link_.CNIMPORT,0)
      this.w_CNCALDIR = NVL(_Link_.CNCALDIR,space(1))
      this.w_CNCOECAL = NVL(_Link_.CNCOECAL,0)
      this.w_PARASS = NVL(_Link_.CNPARASS,0)
      this.w_FLAMPA = NVL(_Link_.CNFLAMPA,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_TIPOPRAT = NVL(_Link_.CNTIPPRA,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRAT = space(15)
      endif
      this.w_DESPRAT = space(55)
      this.w_LOC_PRA = space(25)
      this.w_ENTE = space(10)
      this.w_UFFICIO = space(10)
      this.w_VALPRA = space(3)
      this.w_LISPRA = space(5)
      this.w_CNFLAPON = space(1)
      this.w_CNFLVALO = space(1)
      this.w_CNIMPORT = 0
      this.w_CNCALDIR = space(1)
      this.w_CNCOECAL = 0
      this.w_PARASS = 0
      this.w_FLAMPA = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPOPRAT = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODPRAT = space(15)
        this.w_DESPRAT = space(55)
        this.w_LOC_PRA = space(25)
        this.w_ENTE = space(10)
        this.w_UFFICIO = space(10)
        this.w_VALPRA = space(3)
        this.w_LISPRA = space(5)
        this.w_CNFLAPON = space(1)
        this.w_CNFLVALO = space(1)
        this.w_CNIMPORT = 0
        this.w_CNCALDIR = space(1)
        this.w_CNCOECAL = 0
        this.w_PARASS = 0
        this.w_FLAMPA = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPOPRAT = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTCOS
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTCOS)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODPRAT);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODPRAT;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_ATTCOS))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTCOS)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTCOS) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATTCOS_1_7'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODPRAT<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODPRAT);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTCOS);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODPRAT);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODPRAT;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_ATTCOS)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTCOS = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ATTCOS = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENRIC
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CENRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CENRIC))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CENRIC)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CENRIC) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCENRIC_1_8'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENRIC)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENRIC = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESRIC = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CENRIC = space(15)
      endif
      this.w_CCDESRIC = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CENRIC = space(15)
        this.w_CCDESRIC = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMRIC
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMRIC))
          select CNCODCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMRIC)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMRIC) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMRIC_1_9'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMRIC)
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMRIC = NVL(_Link_.CNCODCAN,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COMRIC = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa ricavi incongruente o obsoleto")
        endif
        this.w_COMRIC = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTRIC
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATTRIC)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_COMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_COMRIC;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_ATTRIC))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTRIC)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTRIC) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATTRIC_1_10'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COMRIC<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_COMRIC);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATTRIC);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_COMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_COMRIC;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_ATTRIC)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTRIC = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ATTRIC = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_1_12'),i_cWhere,'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NODTOBSO,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOLOCALI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_PERSON = NVL(_Link_.NODESCRI,space(60))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(10))
      this.w_DATOBSONOM = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_ATTELEFO = NVL(_Link_.NOTELEFO,space(18))
      this.w_AT___FAX = NVL(_Link_.NOTELFAX,space(18))
      this.w_AT_EMAIL = NVL(_Link_.NO_EMAIL,space(0))
      this.w_AT_EMPEC = NVL(_Link_.NO_EMPEC,space(0))
      this.w_ATCELLUL = NVL(_Link_.NONUMCEL,space(18))
      this.w_LOC_NOM = NVL(_Link_.NOLOCALI,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_PERSON = space(60)
      this.w_COMODO = space(10)
      this.w_DATOBSONOM = ctod("  /  /  ")
      this.w_ATTELEFO = space(18)
      this.w_AT___FAX = space(18)
      this.w_AT_EMAIL = space(0)
      this.w_AT_EMPEC = space(0)
      this.w_ATCELLUL = space(18)
      this.w_LOC_NOM = space(25)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DATOBSONOM>.w_OFDATDOC OR EMPTY(.w_DATOBSONOM))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM = space(15)
        this.w_PERSON = space(60)
        this.w_COMODO = space(10)
        this.w_DATOBSONOM = ctod("  /  /  ")
        this.w_ATTELEFO = space(18)
        this.w_AT___FAX = space(18)
        this.w_AT_EMAIL = space(0)
        this.w_AT_EMPEC = space(0)
        this.w_ATCELLUL = space(18)
        this.w_LOC_NOM = space(25)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCACODICE_1_2.value==this.w_CACODICE)
      this.oPgFrm.Page1.oPag.oCACODICE_1_2.value=this.w_CACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCC_CONTO_1_5.value==this.w_CC_CONTO)
      this.oPgFrm.Page1.oPag.oCC_CONTO_1_5.value=this.w_CC_CONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRAT_1_6.value==this.w_CODPRAT)
      this.oPgFrm.Page1.oPag.oCODPRAT_1_6.value=this.w_CODPRAT
    endif
    if not(this.oPgFrm.Page1.oPag.oATTCOS_1_7.value==this.w_ATTCOS)
      this.oPgFrm.Page1.oPag.oATTCOS_1_7.value=this.w_ATTCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oCENRIC_1_8.value==this.w_CENRIC)
      this.oPgFrm.Page1.oPag.oCENRIC_1_8.value=this.w_CENRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMRIC_1_9.value==this.w_COMRIC)
      this.oPgFrm.Page1.oPag.oCOMRIC_1_9.value=this.w_COMRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oATTRIC_1_10.value==this.w_ATTRIC)
      this.oPgFrm.Page1.oPag.oATTRIC_1_10.value=this.w_ATTRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRAT_1_11.value==this.w_DESPRAT)
      this.oPgFrm.Page1.oPag.oDESPRAT_1_11.value=this.w_DESPRAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNOM_1_12.value==this.w_CODNOM)
      this.oPgFrm.Page1.oPag.oCODNOM_1_12.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oPERSON_1_13.value==this.w_PERSON)
      this.oPgFrm.Page1.oPag.oPERSON_1_13.value=this.w_PERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA_1_14.value==this.w_CCDESPIA)
      this.oPgFrm.Page1.oPag.oCCDESPIA_1_14.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESRIC_1_18.value==this.w_CCDESRIC)
      this.oPgFrm.Page1.oPag.oCCDESRIC_1_18.value=this.w_CCDESRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESCRI_1_24.value==this.w_CADESCRI)
      this.oPgFrm.Page1.oPag.oCADESCRI_1_24.value=this.w_CADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPATIPRIS_1_53.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page1.oPag.oPATIPRIS_1_53.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CACODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CACODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(empty(.w_CODPRAT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPRAT_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO))  and not( Isalt())  and ((g_PERCCM='S' OR IsAhr()))  and not(empty(.w_CENRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCENRIC_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(Isalt())  and not(empty(.w_COMRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMRIC_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa ricavi incongruente o obsoleto")
          case   not((.w_DATOBSONOM>.w_OFDATDOC OR EMPTY(.w_DATOBSONOM)))  and not(empty(.w_CODNOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODNOM_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_kia
      IF EMPTY(This.w_CODPRAT) AND Isalt()
       i_bnoChk=.F.
       i_bRes=.F.
       i_cErrorMsg=Ah_MsgFormat("Inserire codice pratica ")
      ENDIF
      if i_bRes
         Select * from (This.w_ZOOMPART.ccursor) where XCHK=1 into cursor TMP_PART 
      Endif
      if This.w_NOM_OBBL="P" AND EMPTY(this.w_CODNOM)
         i_bnoChk=.F.
         i_bRes=.F.
         i_cErrorMsg=Ah_MsgFormat("Per il tipo attivit� selezionato � necessario inserire il nominativo ")
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODPRAT = this.w_CODPRAT
    this.o_CODNOM = this.w_CODNOM
    return

enddefine

* --- Define pages as container
define class tgsag_kiaPag1 as StdContainer
  Width  = 657
  height = 444
  stdWidth  = 657
  stdheight = 444
  resizeXpos=423
  resizeYpos=317
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODICE_1_2 as StdField with uid="MBGICDMGLD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CACODICE", cQueryName = "CACODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 39235989,;
   bGlobalFont=.t.,;
    Height=21, Width=171, Left=114, Top=15, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_CACODICE"

  func oCACODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODICE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODICE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oCACODICE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'GSAGZKII.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oCACODICE_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CACODICE
     i_obj.ecpSave()
  endproc

  add object oCC_CONTO_1_5 as StdField with uid="ZYMECVJAQV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CC_CONTO", cQueryName = "CC_CONTO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice centro di costo/ricavo",;
    HelpContextID = 55513205,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=114, Top=44, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CC_CONTO"

  func oCC_CONTO_1_5.mHide()
    with this.Parent.oContained
      return (g_COAN <> 'S')
    endwith
  endfunc

  func oCC_CONTO_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCC_CONTO_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCC_CONTO_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCC_CONTO_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCC_CONTO_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CC_CONTO
     i_obj.ecpSave()
  endproc

  add object oCODPRAT_1_6 as StdField with uid="LNZCMMZIKS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODPRAT", cQueryName = "CODPRAT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 109734950,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=114, Top=70, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_BZZ", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRAT"

  func oCODPRAT_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
      if .not. empty(.w_ATTCOS)
        bRes2=.link_1_7('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODPRAT_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRAT_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRAT_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODPRAT_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRAT
     i_obj.ecpSave()
  endproc

  add object oATTCOS_1_7 as StdField with uid="VVSXSZJDQD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ATTCOS", cQueryName = "ATTCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa costi",;
    HelpContextID = 129076986,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=526, Top=70, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODPRAT", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_ATTCOS"

  func oATTCOS_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_codprat))
    endwith
   endif
  endfunc

  func oATTCOS_1_7.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oATTCOS_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTCOS_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTCOS_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODPRAT)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODPRAT)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATTCOS_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oATTCOS_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODPRAT
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_ATTCOS
     i_obj.ecpSave()
  endproc

  add object oCENRIC_1_8 as StdField with uid="GWCBQUFVPT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CENRIC", cQueryName = "CENRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 134413786,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=114, Top=97, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENRIC"

  func oCENRIC_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCM='S' OR IsAhr()))
    endwith
   endif
  endfunc

  func oCENRIC_1_8.mHide()
    with this.Parent.oContained
      return ( Isalt())
    endwith
  endfunc

  func oCENRIC_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCENRIC_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCENRIC_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCENRIC_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCENRIC_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CENRIC
     i_obj.ecpSave()
  endproc

  add object oCOMRIC_1_9 as StdField with uid="CCYDSSPMTJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_COMRIC", cQueryName = "COMRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa ricavi incongruente o obsoleto",;
    ToolTipText = "Codice commessa ricavi",;
    HelpContextID = 134415322,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=114, Top=124, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMRIC"

  func oCOMRIC_1_9.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oCOMRIC_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
      if .not. empty(.w_ATTRIC)
        bRes2=.link_1_10('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCOMRIC_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMRIC_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMRIC_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCOMRIC_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_COMRIC
     i_obj.ecpSave()
  endproc

  add object oATTRIC_1_10 as StdField with uid="LTBYOWJTQX",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ATTRIC", cQueryName = "ATTRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 134385402,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=526, Top=124, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_COMRIC", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_ATTRIC"

  func oATTRIC_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_COMRIC))
    endwith
   endif
  endfunc

  func oATTRIC_1_10.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oATTRIC_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTRIC_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTRIC_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_COMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_COMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATTRIC_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oATTRIC_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_COMRIC
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_ATTRIC
     i_obj.ecpSave()
  endproc

  add object oDESPRAT_1_11 as StdField with uid="DHZPCSQKFH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESPRAT", cQueryName = "DESPRAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(55), bMultilanguage =  .f.,;
    HelpContextID = 109793846,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=253, Top=71, InputMask=replicate('X',55)

  func oDESPRAT_1_11.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oCODNOM_1_12 as StdField with uid="JVWHREDOHG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 229086170,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=114, Top=150, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc

  add object oPERSON_1_13 as StdField with uid="TQJYTGWRDG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PERSON", cQueryName = "PERSON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione nominativo",;
    HelpContextID = 211926282,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=253, Top=150, InputMask=replicate('X',60)

  add object oCCDESPIA_1_14 as StdField with uid="JMXGVHYZRV",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 93282407,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=253, Top=44, InputMask=replicate('X',40)

  func oCCDESPIA_1_14.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S')
    endwith
  endfunc

  add object oCCDESRIC_1_18 as StdField with uid="OQBJBQEOLA",rtseq=14,rtrep=.t.,;
    cFormVar = "w_CCDESRIC", cQueryName = "CCDESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 126836841,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=253, Top=98, InputMask=replicate('X',40)

  func oCCDESRIC_1_18.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCADESCRI_1_24 as StdField with uid="WIYVDUCLML",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CADESCRI", cQueryName = "CADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 124821905,;
   bGlobalFont=.t.,;
    Height=21, Width=355, Left=289, Top=15, InputMask=replicate('X',254)


  add object ZOOMPART as cp_szoombox with uid="WODZOSNAJX",left=18, top=198, width=627,height=188,;
    caption='ZOOMPART',;
   bGlobalFont=.t.,;
    cTable="DIPENDEN",cZoomFile="GSAG_MPP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,;
    cEvent = "Blank,w_PATIPRIS Changed",;
    nPag=1;
    , HelpContextID = 160948758


  add object oPATIPRIS_1_53 as StdCombo with uid="GTJVRKATPA",value=4,rtseq=43,rtrep=.f.,left=114,top=177,width=76,height=21;
    , HelpContextID = 124018505;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPATIPRIS_1_53.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    iif(this.value =3,'R',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oPATIPRIS_1_53.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_1_53.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',3,;
      iif(this.Parent.oContained.w_PATIPRIS=='',4,;
      0))))
  endfunc


  add object oBtn_1_55 as StdButton with uid="ERHOXVEEGI",left=545, top=393, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 266975002;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_55.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_56 as StdButton with uid="TKCMXZXILU",left=597, top=393, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 259686330;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_56.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="OQMLWZDYWU",Visible=.t., Left=36, Top=70,;
    Alignment=1, Width=71, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="EBUADEBZKR",Visible=.t., Left=12, Top=46,;
    Alignment=1, Width=95, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (g_COAN<>'S')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="ORILRVBARV",Visible=.t., Left=27, Top=153,;
    Alignment=1, Width=80, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="OEALKSNEIW",Visible=.t., Left=24, Top=70,;
    Alignment=1, Width=83, Height=18,;
    Caption="Comm. costi:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="NDNECIPGFN",Visible=.t., Left=3, Top=97,;
    Alignment=1, Width=104, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="HTOLFADPTS",Visible=.t., Left=22, Top=124,;
    Alignment=1, Width=85, Height=18,;
    Caption="Comm. ricavi:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="THZJZYTFBJ",Visible=.t., Left=432, Top=125,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (g_PERCAN <>'S' or Isalt())
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="VRUOKTUIHY",Visible=.t., Left=432, Top=71,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (g_PERCAN <>'S' or Isalt())
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="QVTFQRDCTZ",Visible=.t., Left=24, Top=17,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="IIPQFPVHAG",Visible=.t., Left=30, Top=178,;
    Alignment=1, Width=77, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kia','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
