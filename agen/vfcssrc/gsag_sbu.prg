* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_sbu                                                        *
*              Stampa nominativi e buste                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-10                                                      *
* Last revis.: 2014-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_sbu",oParentObject))

* --- Class definition
define class tgsag_sbu as StdForm
  Top    = 7
  Left   = 9

  * --- Standard Properties
  Width  = 794
  Height = 454+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-10"
  HelpContextID=107620201
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=68

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  GRU_NOMI_IDX = 0
  ORI_NOMI_IDX = 0
  cpusers_IDX = 0
  DIPENDEN_IDX = 0
  CAT_ATTR_IDX = 0
  TIP_CATT_IDX = 0
  SAL_NOMI_IDX = 0
  NAZIONI_IDX = 0
  cPrg = "gsag_sbu"
  cComment = "Stampa nominativi e buste"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DESNOM = space(40)
  w_Associa = space(1)
  o_Associa = space(1)
  w_DESCAN = space(100)
  w_CODCAN = space(15)
  w_FLAPE = space(1)
  w_FLARCH = space(1)
  w_GRUPSTA = space(8)
  o_GRUPSTA = space(8)
  w_TipoPersona = space(1)
  w_INDIRI = space(50)
  w_CAP = space(8)
  w_LOCALI = space(30)
  w_PROVIN = space(2)
  w_NOTE = space(0)
  w_PDATINIZ = ctod('  /  /  ')
  o_PDATINIZ = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_obsodat1 = space(1)
  w_TIPONOME = space(1)
  w_InsMitt = space(10)
  w_RAGSTU = space(100)
  w_INDSTU = space(100)
  w_LOCSTU = space(100)
  w_CAPSTU = space(9)
  w_PROVSTU = space(2)
  w_MODSPED = space(50)
  w_NOTEBUSTA = space(50)
  w_AziAttiva = space(5)
  w_DimScheda = 0
  w_TIPONOME = space(1)
  w_PERSONE = space(1)
  w_NOCODGRU = space(5)
  w_NORIFINT = space(5)
  o_NORIFINT = space(5)
  w_NOCODORI = space(5)
  w_CodUte = 0
  w_CONS_DATI = space(1)
  w_TIPNOM = space(1)
  w_DESOGG = space(60)
  w_DESGRU = space(35)
  w_DESUTE = space(20)
  w_DESORI = space(35)
  w_DESRIFINT = space(35)
  w_COGNOME = space(10)
  w_NOME = space(10)
  w_DESCAT1 = space(35)
  w_DESATT1 = space(35)
  w_DESCAT2 = space(35)
  w_DESATT2 = space(35)
  w_DESCAT3 = space(35)
  w_DESATT3 = space(35)
  w_RACODAT1 = space(10)
  w_RACODAT2 = space(10)
  w_RACODAT3 = space(10)
  w_TIPCAT1 = space(5)
  w_TIPCAT2 = space(5)
  w_TIPCAT3 = space(5)
  w_SELEZI = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_ForzaNom = space(10)
  o_ForzaNom = space(10)
  w_SAL_Dest = space(5)
  w_RAGSOC_Dest = space(100)
  w_INDIRI_Dest = space(100)
  w_LOCALI_Dest = space(100)
  w_CAP_Dest = space(9)
  w_PROV_Dest = space(2)
  w_NAZ_Dest = space(3)
  w_CortAtt_Dest = space(100)
  w_CodiceFittizio = space(20)
  w_NADESNAZ = space(35)
  w_AGSBU_OUTPUT = .NULL.
  w_AGSBU_NOM = .NULL.
  w_AGSBU_PRA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_sbuPag1","gsag_sbu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsag_sbuPag2","gsag_sbu",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettagli")
      .Pages(3).addobject("oPag","tgsag_sbuPag3","gsag_sbu",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Opzioni stampa buste")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDESNOM_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGSBU_OUTPUT = this.oPgFrm.Pages(1).oPag.AGSBU_OUTPUT
    this.w_AGSBU_NOM = this.oPgFrm.Pages(1).oPag.AGSBU_NOM
    this.w_AGSBU_PRA = this.oPgFrm.Pages(1).oPag.AGSBU_PRA
    DoDefault()
    proc Destroy()
      this.w_AGSBU_OUTPUT = .NULL.
      this.w_AGSBU_NOM = .NULL.
      this.w_AGSBU_PRA = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='GRU_NOMI'
    this.cWorkTables[3]='ORI_NOMI'
    this.cWorkTables[4]='cpusers'
    this.cWorkTables[5]='DIPENDEN'
    this.cWorkTables[6]='CAT_ATTR'
    this.cWorkTables[7]='TIP_CATT'
    this.cWorkTables[8]='SAL_NOMI'
    this.cWorkTables[9]='NAZIONI'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSAG_BSB(this,"STAMPA")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsag_sbu
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DESNOM=space(40)
      .w_Associa=space(1)
      .w_DESCAN=space(100)
      .w_CODCAN=space(15)
      .w_FLAPE=space(1)
      .w_FLARCH=space(1)
      .w_GRUPSTA=space(8)
      .w_TipoPersona=space(1)
      .w_INDIRI=space(50)
      .w_CAP=space(8)
      .w_LOCALI=space(30)
      .w_PROVIN=space(2)
      .w_NOTE=space(0)
      .w_PDATINIZ=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_obsodat1=space(1)
      .w_TIPONOME=space(1)
      .w_InsMitt=space(10)
      .w_RAGSTU=space(100)
      .w_INDSTU=space(100)
      .w_LOCSTU=space(100)
      .w_CAPSTU=space(9)
      .w_PROVSTU=space(2)
      .w_MODSPED=space(50)
      .w_NOTEBUSTA=space(50)
      .w_AziAttiva=space(5)
      .w_DimScheda=0
      .w_TIPONOME=space(1)
      .w_PERSONE=space(1)
      .w_NOCODGRU=space(5)
      .w_NORIFINT=space(5)
      .w_NOCODORI=space(5)
      .w_CodUte=0
      .w_CONS_DATI=space(1)
      .w_TIPNOM=space(1)
      .w_DESOGG=space(60)
      .w_DESGRU=space(35)
      .w_DESUTE=space(20)
      .w_DESORI=space(35)
      .w_DESRIFINT=space(35)
      .w_COGNOME=space(10)
      .w_NOME=space(10)
      .w_DESCAT1=space(35)
      .w_DESATT1=space(35)
      .w_DESCAT2=space(35)
      .w_DESATT2=space(35)
      .w_DESCAT3=space(35)
      .w_DESATT3=space(35)
      .w_RACODAT1=space(10)
      .w_RACODAT2=space(10)
      .w_RACODAT3=space(10)
      .w_TIPCAT1=space(5)
      .w_TIPCAT2=space(5)
      .w_TIPCAT3=space(5)
      .w_SELEZI=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_ForzaNom=space(10)
      .w_RAGSOC_Dest=space(100)
      .w_INDIRI_Dest=space(100)
      .w_LOCALI_Dest=space(100)
      .w_CAP_Dest=space(9)
      .w_PROV_Dest=space(2)
      .w_NAZ_Dest=space(3)
      .w_CortAtt_Dest=space(100)
      .w_CodiceFittizio=space(20)
      .w_NADESNAZ=space(35)
        .w_DESNOM = ''
        .w_Associa = ' '
        .w_DESCAN = ''
          .DoRTCalc(4,4,.f.)
        .w_FLAPE = 'A'
        .w_FLARCH = 'T'
        .w_GRUPSTA = 'GSAG_SNO'
      .oPgFrm.Page1.oPag.AGSBU_OUTPUT.Calculate(.w_GRUPSTA)
      .oPgFrm.Page1.oPag.AGSBU_NOM.Calculate()
        .w_TipoPersona = 'P'
        .w_INDIRI = ''
          .DoRTCalc(10,10,.f.)
        .w_LOCALI = ''
          .DoRTCalc(12,13,.f.)
        .w_PDATINIZ = i_datsys
        .w_OBTEST = .w_PDATINIZ
          .DoRTCalc(16,16,.f.)
        .w_obsodat1 = 'N'
        .w_TIPONOME = 'X'
        .w_InsMitt = 'N'
        .DoRTCalc(20,27,.f.)
        if not(empty(.w_AziAttiva))
          .link_3_9('Full')
        endif
      .oPgFrm.Page1.oPag.AGSBU_PRA.Calculate()
        .w_DimScheda = 0
        .w_TIPONOME = 'X'
        .w_PERSONE = 'N'
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_NOCODGRU))
          .link_2_18('Full')
        endif
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_NORIFINT))
          .link_2_19('Full')
        endif
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_NOCODORI))
          .link_2_20('Full')
        endif
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_CodUte))
          .link_2_21('Full')
        endif
        .w_CONS_DATI = ''
        .w_TIPNOM = IIF(.w_TIPONOME='X','',.w_TIPONOME)
          .DoRTCalc(37,40,.f.)
        .w_DESRIFINT = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .DoRTCalc(42,50,.f.)
        if not(empty(.w_RACODAT1))
          .link_2_45('Full')
        endif
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_RACODAT2))
          .link_2_46('Full')
        endif
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_RACODAT3))
          .link_2_47('Full')
        endif
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_TIPCAT1))
          .link_2_48('Full')
        endif
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_TIPCAT2))
          .link_2_49('Full')
        endif
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_TIPCAT3))
          .link_2_50('Full')
        endif
        .w_SELEZI = 'D'
        .w_OB_TEST = i_INIDAT
        .w_ForzaNom = 'N'
        .DoRTCalc(59,64,.f.)
        if not(empty(.w_NAZ_Dest))
          .link_3_27('Full')
        endif
          .DoRTCalc(65,65,.f.)
        .w_CodiceFittizio = 'X*?-X*?-X*?-X*?-X*?-'
    endwith
    this.DoRTCalc(67,67,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_10.enabled = this.oPgFrm.Page3.oPag.oBtn_3_10.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_11.enabled = this.oPgFrm.Page3.oPag.oBtn_3_11.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_51.enabled = this.oPgFrm.Page2.oPag.oBtn_2_51.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.AGSBU_OUTPUT.Calculate(.w_GRUPSTA)
        .oPgFrm.Page1.oPag.AGSBU_NOM.Calculate()
        .DoRTCalc(1,14,.t.)
        if .o_PDATINIZ<>.w_PDATINIZ
            .w_OBTEST = .w_PDATINIZ
        endif
        .DoRTCalc(16,26,.t.)
          .link_3_9('Full')
        .oPgFrm.Page1.oPag.AGSBU_PRA.Calculate()
        if .o_Associa<>.w_Associa
          .Calculate_VVAMCROZDR()
        endif
        .DoRTCalc(28,35,.t.)
            .w_TIPNOM = IIF(.w_TIPONOME='X','',.w_TIPONOME)
        .DoRTCalc(37,40,.t.)
        if .o_NORIFINT<>.w_NORIFINT
            .w_DESRIFINT = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        endif
        .DoRTCalc(42,52,.t.)
          .link_2_48('Full')
          .link_2_49('Full')
          .link_2_50('Full')
        if .o_ForzaNom<>.w_ForzaNom
          .Calculate_NSTEVRMXUL()
        endif
        if .o_GRUPSTA<>.w_GRUPSTA
          .Calculate_BUJKYHVLTG()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(56,68,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AGSBU_OUTPUT.Calculate(.w_GRUPSTA)
        .oPgFrm.Page1.oPag.AGSBU_NOM.Calculate()
        .oPgFrm.Page1.oPag.AGSBU_PRA.Calculate()
    endwith
  return

  proc Calculate_MIJIOEJQSQ()
    with this
          * --- init azienda
          .w_AziAttiva = i_CodAzi
    endwith
  endproc
  proc Calculate_HALNTOGBAZ()
    with this
          * --- Ingrandisce la scheda
          GSAG_BSB(this;
              ,"INGRANDISCI";
             )
    endwith
  endproc
  proc Calculate_ZSDRQRGJJG()
    with this
          * --- Riduce la scheda
          GSAG_BSB(this;
              ,"RIDUCI";
             )
    endwith
  endproc
  proc Calculate_VVAMCROZDR()
    with this
          * --- Visualizza o meno lo zoom delle pratiche
          GSAG_BSB(this;
              ,'VIS_PRAT';
             )
    endwith
  endproc
  proc Calculate_PONEEIJXXP()
    with this
          * --- All'init nasconde lo zoom di selez. pratiche
          GSAG_BSB(this;
              ,'INIT';
             )
    endwith
  endproc
  proc Calculate_BNJOWHDEIU()
    with this
          * --- Selezione/deseleziona pratica
          gsag_bsb(this;
              ,'RICERCANOMI';
             )
    endwith
  endproc
  proc Calculate_VLOICQMGQC()
    with this
          * --- Aggiorna le anagrafiche
          GSAG_BSB(this;
              ,'RICERCANOMI';
             )
    endwith
  endproc
  proc Calculate_SSRRRZDUKL()
    with this
          * --- Aggiorna le pratiche
          GSAG_BSB(this;
              ,'AGGIORNAPRATICHE';
             )
    endwith
  endproc
  proc Calculate_IXCVRPHEDJ()
    with this
          * --- Seleziona/deseleziona nominativi
          GSAG_BSB(this;
              ,IIF(.w_SELEZI='S', 'SELEZ_NOM', 'DESEL_NOM');
             )
    endwith
  endproc
  proc Calculate_RPPUZTBOJK()
    with this
          * --- RIlancio zoom
          GSAG_BSB(this;
              ,IIF(.w_ASSOCIA='A','AGGIORNAPRATICHE','RICERCANOMI');
             )
    endwith
  endproc
  proc Calculate_SCLNZSHZFG()
    with this
          * --- Seleziona nominativi
          GSAG_BSB(this;
              ,'SELEZ_NOM';
             )
    endwith
  endproc
  proc Calculate_NSTEVRMXUL()
    with this
          * --- Legge dati nominativo selezionato
          GSAG_BSB(this;
              ,'COPIA_DATI';
             )
    endwith
  endproc
  proc Calculate_BUJKYHVLTG()
    with this
          * --- Azzera variabili
          .w_ForzaNom = IIF(.w_GRUPSTA<>'GSAG_SBU', 'N', .w_ForzaNom)
          .w_RAGSOC_Dest = IIF(.w_GRUPSTA<>'GSAG_SBU', ' ', .w_RAGSOC_Dest)
          .w_INDIRI_Dest = IIF(.w_GRUPSTA<>'GSAG_SBU', ' ', .w_INDIRI_Dest)
          .w_LOCALI_Dest = IIF(.w_GRUPSTA<>'GSAG_SBU', ' ', .w_LOCALI_Dest)
          .w_CAP_Dest = IIF(.w_GRUPSTA<>'GSAG_SBU', ' ', .w_CAP_Dest)
          .w_PROV_Dest = IIF(.w_GRUPSTA<>'GSAG_SBU', ' ', .w_PROV_Dest)
          .w_NAZ_Dest = IIF(.w_GRUPSTA<>'GSAG_SBU', ' ', .w_NAZ_Dest)
          .w_NADESNAZ = IIF(.w_GRUPSTA<>'GSAG_SBU', ' ', .w_NADESNAZ)
          .w_SAL_Dest = IIF(.w_GRUPSTA<>'GSAG_SBU', ' ', .w_SAL_Dest)
          .w_CortAtt_Dest = IIF(.w_GRUPSTA<>'GSAG_SBU', ' ', .w_CortAtt_Dest)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDESCAN_1_5.enabled = this.oPgFrm.Page1.oPag.oDESCAN_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCODCAN_1_6.enabled = this.oPgFrm.Page1.oPag.oCODCAN_1_6.mCond()
    this.oPgFrm.Page1.oPag.oFLAPE_1_7.enabled = this.oPgFrm.Page1.oPag.oFLAPE_1_7.mCond()
    this.oPgFrm.Page1.oPag.oFLARCH_1_8.enabled = this.oPgFrm.Page1.oPag.oFLARCH_1_8.mCond()
    this.oPgFrm.Page3.oPag.oSAL_Dest_3_21.enabled = this.oPgFrm.Page3.oPag.oSAL_Dest_3_21.mCond()
    this.oPgFrm.Page3.oPag.oRAGSOC_Dest_3_22.enabled = this.oPgFrm.Page3.oPag.oRAGSOC_Dest_3_22.mCond()
    this.oPgFrm.Page3.oPag.oINDIRI_Dest_3_23.enabled = this.oPgFrm.Page3.oPag.oINDIRI_Dest_3_23.mCond()
    this.oPgFrm.Page3.oPag.oLOCALI_Dest_3_24.enabled = this.oPgFrm.Page3.oPag.oLOCALI_Dest_3_24.mCond()
    this.oPgFrm.Page3.oPag.oCAP_Dest_3_25.enabled = this.oPgFrm.Page3.oPag.oCAP_Dest_3_25.mCond()
    this.oPgFrm.Page3.oPag.oPROV_Dest_3_26.enabled = this.oPgFrm.Page3.oPag.oPROV_Dest_3_26.mCond()
    this.oPgFrm.Page3.oPag.oNAZ_Dest_3_27.enabled = this.oPgFrm.Page3.oPag.oNAZ_Dest_3_27.mCond()
    this.oPgFrm.Page3.oPag.oCortAtt_Dest_3_28.enabled = this.oPgFrm.Page3.oPag.oCortAtt_Dest_3_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_10.enabled = this.oPgFrm.Page3.oPag.oBtn_3_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(3).enabled=not(this.w_GRUPSTA#'GSAG_SBU')
    local i_show2
    i_show2=not(this.w_GRUPSTA#'GSAG_SBU')
    this.oPgFrm.Pages(3).enabled=i_show2 and not(this.w_GRUPSTA#'GSAG_SBU')
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Opzioni stampa buste"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oAssocia_1_2.visible=!this.oPgFrm.Page1.oPag.oAssocia_1_2.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_3.visible=!this.oPgFrm.Page1.oPag.oBtn_1_3.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_4.visible=!this.oPgFrm.Page1.oPag.oBtn_1_4.mHide()
    this.oPgFrm.Page1.oPag.oDESCAN_1_5.visible=!this.oPgFrm.Page1.oPag.oDESCAN_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCODCAN_1_6.visible=!this.oPgFrm.Page1.oPag.oCODCAN_1_6.mHide()
    this.oPgFrm.Page1.oPag.oFLAPE_1_7.visible=!this.oPgFrm.Page1.oPag.oFLAPE_1_7.mHide()
    this.oPgFrm.Page1.oPag.oFLARCH_1_8.visible=!this.oPgFrm.Page1.oPag.oFLARCH_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_15.visible=!this.oPgFrm.Page1.oPag.oBtn_1_15.mHide()
    this.oPgFrm.Page2.oPag.oTIPONOME_2_15.visible=!this.oPgFrm.Page2.oPag.oTIPONOME_2_15.mHide()
    this.oPgFrm.Page2.oPag.oTIPONOME_2_16.visible=!this.oPgFrm.Page2.oPag.oTIPONOME_2_16.mHide()
    this.oPgFrm.Page2.oPag.oPERSONE_2_17.visible=!this.oPgFrm.Page2.oPag.oPERSONE_2_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oSELEZI_1_38.visible=!this.oPgFrm.Page1.oPag.oSELEZI_1_38.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AGSBU_OUTPUT.Event(cEvent)
      .oPgFrm.Page1.oPag.AGSBU_NOM.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_MIJIOEJQSQ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.AGSBU_PRA.Event(cEvent)
        if lower(cEvent)==lower("Ingrandisci")
          .Calculate_HALNTOGBAZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Riduci") or lower(cEvent)==lower("Init")
          .Calculate_ZSDRQRGJJG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_PONEEIJXXP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AGSBU_PRA row checked") or lower(cEvent)==lower("AGSBU_PRA row unchecked")
          .Calculate_BNJOWHDEIU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("RicercaNomi") or lower(cEvent)==lower("w_DESNOM Searching")
          .Calculate_VLOICQMGQC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("RicercaPrat") or lower(cEvent)==lower("w_CODCAN Searching") or lower(cEvent)==lower("w_DESCAN Searching")
          .Calculate_SSRRRZDUKL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_SELEZI Changed")
          .Calculate_IXCVRPHEDJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Ricerca") or lower(cEvent)==lower("w_ASSOCIA Changed") or lower(cEvent)==lower("Ricerca2")
          .Calculate_RPPUZTBOJK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Seleziona")
          .Calculate_SCLNZSHZFG()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_sbu
    * --- Nota: prima viene notificato l'evento Blank e poi l'Init
    
    if cevent ='Blank' and Type('This.oparentobject')='O' and upper(This.oparentobject.class)='TGSAR_ANO'
        This.w_DESNOM=This.oparentobject.w_NODESCRI
        * -- Per togliere il focus dal campo w_DESNOM viene posto il focus sullo zoom
        This.W_AGSBU_NOM.grd.SetFocus()
    endif
    
    If cevent='Init'
       This.w_associa='T'
       * -- Per rieseguire lo zoom
       This.Notifyevent('Ricerca2')
    Endif
    
    if cevent ='Ricerca2' and Type('This.oparentobject')='O' and upper(This.oparentobject.class)='TGSAR_ANO'
        * -- Dopo aver rieseguito lo zoom vengono selezionate le relative righe
        This.NotifyEvent('Seleziona')
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AziAttiva
  func Link_3_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AziAttiva) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AziAttiva)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI,AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AziAttiva);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AziAttiva)
            select AZCODAZI,AZRAGAZI,AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AziAttiva = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGSTU = NVL(_Link_.AZRAGAZI,space(100))
      this.w_INDSTU = NVL(_Link_.AZINDAZI,space(100))
      this.w_LOCSTU = NVL(_Link_.AZLOCAZI,space(100))
      this.w_CAPSTU = NVL(_Link_.AZCAPAZI,space(9))
      this.w_PROVSTU = NVL(_Link_.AZPROAZI,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_AziAttiva = space(5)
      endif
      this.w_RAGSTU = space(100)
      this.w_INDSTU = space(100)
      this.w_LOCSTU = space(100)
      this.w_CAPSTU = space(9)
      this.w_PROVSTU = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AziAttiva Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCODGRU
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_NOCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_NOCODGRU))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODGRU)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" GNDESCRI like "+cp_ToStrODBC(trim(this.w_NOCODGRU)+"%");

            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GNDESCRI like "+cp_ToStr(trim(this.w_NOCODGRU)+"%");

            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oNOCODGRU_2_18'),i_cWhere,'',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_NOCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_NOCODGRU)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODGRU = NVL(_Link_.GNCODICE,space(5))
      this.w_DESGRU = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODGRU = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NORIFINT
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NORIFINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoPersona;
                     ,'DPCODICE',trim(this.w_NORIFINT))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NORIFINT)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoPersona);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoPersona);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NORIFINT) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oNORIFINT_2_19'),i_cWhere,'',"Riferimenti interni",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoPersona<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NORIFINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_NORIFINT);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoPersona;
                       ,'DPCODICE',this.w_NORIFINT)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NORIFINT = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOME = NVL(_Link_.DPCOGNOM,space(10))
      this.w_NOME = NVL(_Link_.DPNOME,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_NORIFINT = space(5)
      endif
      this.w_COGNOME = space(10)
      this.w_NOME = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NORIFINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCODORI
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AON',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_NOCODORI)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_NOCODORI))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODORI)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ONDESCRI like "+cp_ToStrODBC(trim(this.w_NOCODORI)+"%");

            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ONDESCRI like "+cp_ToStr(trim(this.w_NOCODORI)+"%");

            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODORI) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oNOCODORI_2_20'),i_cWhere,'GSAR_AON',"Origine",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_NOCODORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_NOCODORI)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODORI = NVL(_Link_.ONCODICE,space(5))
      this.w_DESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODORI = space(5)
      endif
      this.w_DESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodUte
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodUte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CodUte);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CodUte)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CodUte) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','CODE',cp_AbsName(oSource.parent,'oCodUte_2_21'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodUte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CodUte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CodUte)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodUte = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CodUte = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodUte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT1
  func Link_2_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT1))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT1)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT1) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT1_2_45'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT1)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT1 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT1 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT1 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT1 = space(10)
      endif
      this.w_DESATT1 = space(35)
      this.w_TIPCAT1 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT2
  func Link_2_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT2))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT2)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT2) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT2_2_46'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT2)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT2 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT2 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT2 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT2 = space(10)
      endif
      this.w_DESATT2 = space(35)
      this.w_TIPCAT2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT3
  func Link_2_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT3)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT3))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT3)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT3) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT3_2_47'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT3)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT3 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT3 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT3 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT3 = space(10)
      endif
      this.w_DESATT3 = space(35)
      this.w_TIPCAT3 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT1
  func Link_2_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT1)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT1 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT1 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT1 = space(5)
      endif
      this.w_DESCAT1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT2
  func Link_2_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT2)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT2 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT2 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT2 = space(5)
      endif
      this.w_DESCAT2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT3
  func Link_2_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT3)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT3 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT3 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT3 = space(5)
      endif
      this.w_DESCAT3 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SAL_Dest
  func Link_3_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SAL_NOMI_IDX,3]
    i_lTable = "SAL_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SAL_NOMI_IDX,2], .t., this.SAL_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SAL_Dest) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SAL_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SACODICE like "+cp_ToStrODBC(trim(this.w_SAL_Dest)+"%");

          i_ret=cp_SQL(i_nConn,"select SACODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SACODICE',trim(this.w_SAL_Dest))
          select SACODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SAL_Dest)==trim(_Link_.SACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SAL_Dest) and !this.bDontReportError
            deferred_cp_zoom('SAL_NOMI','*','SACODICE',cp_AbsName(oSource.parent,'oSAL_Dest_3_21'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SACODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SACODICE',oSource.xKey(1))
            select SACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SAL_Dest)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SACODICE="+cp_ToStrODBC(this.w_SAL_Dest);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SACODICE',this.w_SAL_Dest)
            select SACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SAL_Dest = NVL(_Link_.SACODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SAL_Dest = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SAL_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.SACODICE,1)
      cp_ShowWarn(i_cKey,this.SAL_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SAL_Dest Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NAZ_Dest
  func Link_3_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NAZ_Dest) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_NAZ_Dest)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_NAZ_Dest))
          select NACODNAZ,NADESNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NAZ_Dest)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NADESNAZ like "+cp_ToStrODBC(trim(this.w_NAZ_Dest)+"%");

            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NADESNAZ like "+cp_ToStr(trim(this.w_NAZ_Dest)+"%");

            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NAZ_Dest) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oNAZ_Dest_3_27'),i_cWhere,'',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NAZ_Dest)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_NAZ_Dest);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_NAZ_Dest)
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NAZ_Dest = NVL(_Link_.NACODNAZ,space(3))
      this.w_NADESNAZ = NVL(_Link_.NADESNAZ,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NAZ_Dest = space(3)
      endif
      this.w_NADESNAZ = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NAZ_Dest Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_1.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_1.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oAssocia_1_2.RadioValue()==this.w_Associa)
      this.oPgFrm.Page1.oPag.oAssocia_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_5.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_5.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAN_1_6.value==this.w_CODCAN)
      this.oPgFrm.Page1.oPag.oCODCAN_1_6.value=this.w_CODCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAPE_1_7.RadioValue()==this.w_FLAPE)
      this.oPgFrm.Page1.oPag.oFLAPE_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLARCH_1_8.RadioValue()==this.w_FLARCH)
      this.oPgFrm.Page1.oPag.oFLARCH_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUPSTA_1_13.RadioValue()==this.w_GRUPSTA)
      this.oPgFrm.Page1.oPag.oGRUPSTA_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oINDIRI_2_7.value==this.w_INDIRI)
      this.oPgFrm.Page2.oPag.oINDIRI_2_7.value=this.w_INDIRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCAP_2_8.value==this.w_CAP)
      this.oPgFrm.Page2.oPag.oCAP_2_8.value=this.w_CAP
    endif
    if not(this.oPgFrm.Page2.oPag.oLOCALI_2_9.value==this.w_LOCALI)
      this.oPgFrm.Page2.oPag.oLOCALI_2_9.value=this.w_LOCALI
    endif
    if not(this.oPgFrm.Page2.oPag.oPROVIN_2_10.value==this.w_PROVIN)
      this.oPgFrm.Page2.oPag.oPROVIN_2_10.value=this.w_PROVIN
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTE_2_11.value==this.w_NOTE)
      this.oPgFrm.Page2.oPag.oNOTE_2_11.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oPDATINIZ_2_13.value==this.w_PDATINIZ)
      this.oPgFrm.Page2.oPag.oPDATINIZ_2_13.value=this.w_PDATINIZ
    endif
    if not(this.oPgFrm.Page2.oPag.oobsodat1_2_14.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page2.oPag.oobsodat1_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPONOME_2_15.RadioValue()==this.w_TIPONOME)
      this.oPgFrm.Page2.oPag.oTIPONOME_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oInsMitt_3_1.RadioValue()==this.w_InsMitt)
      this.oPgFrm.Page3.oPag.oInsMitt_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oRAGSTU_3_2.value==this.w_RAGSTU)
      this.oPgFrm.Page3.oPag.oRAGSTU_3_2.value=this.w_RAGSTU
    endif
    if not(this.oPgFrm.Page3.oPag.oINDSTU_3_3.value==this.w_INDSTU)
      this.oPgFrm.Page3.oPag.oINDSTU_3_3.value=this.w_INDSTU
    endif
    if not(this.oPgFrm.Page3.oPag.oLOCSTU_3_4.value==this.w_LOCSTU)
      this.oPgFrm.Page3.oPag.oLOCSTU_3_4.value=this.w_LOCSTU
    endif
    if not(this.oPgFrm.Page3.oPag.oCAPSTU_3_5.value==this.w_CAPSTU)
      this.oPgFrm.Page3.oPag.oCAPSTU_3_5.value=this.w_CAPSTU
    endif
    if not(this.oPgFrm.Page3.oPag.oPROVSTU_3_6.value==this.w_PROVSTU)
      this.oPgFrm.Page3.oPag.oPROVSTU_3_6.value=this.w_PROVSTU
    endif
    if not(this.oPgFrm.Page3.oPag.oMODSPED_3_7.value==this.w_MODSPED)
      this.oPgFrm.Page3.oPag.oMODSPED_3_7.value=this.w_MODSPED
    endif
    if not(this.oPgFrm.Page3.oPag.oNOTEBUSTA_3_8.value==this.w_NOTEBUSTA)
      this.oPgFrm.Page3.oPag.oNOTEBUSTA_3_8.value=this.w_NOTEBUSTA
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPONOME_2_16.RadioValue()==this.w_TIPONOME)
      this.oPgFrm.Page2.oPag.oTIPONOME_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPERSONE_2_17.RadioValue()==this.w_PERSONE)
      this.oPgFrm.Page2.oPag.oPERSONE_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCODGRU_2_18.value==this.w_NOCODGRU)
      this.oPgFrm.Page2.oPag.oNOCODGRU_2_18.value=this.w_NOCODGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oNORIFINT_2_19.value==this.w_NORIFINT)
      this.oPgFrm.Page2.oPag.oNORIFINT_2_19.value=this.w_NORIFINT
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCODORI_2_20.value==this.w_NOCODORI)
      this.oPgFrm.Page2.oPag.oNOCODORI_2_20.value=this.w_NOCODORI
    endif
    if not(this.oPgFrm.Page2.oPag.oCodUte_2_21.value==this.w_CodUte)
      this.oPgFrm.Page2.oPag.oCodUte_2_21.value=this.w_CodUte
    endif
    if not(this.oPgFrm.Page2.oPag.oCONS_DATI_2_22.RadioValue()==this.w_CONS_DATI)
      this.oPgFrm.Page2.oPag.oCONS_DATI_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU_2_25.value==this.w_DESGRU)
      this.oPgFrm.Page2.oPag.oDESGRU_2_25.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESUTE_2_27.value==this.w_DESUTE)
      this.oPgFrm.Page2.oPag.oDESUTE_2_27.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESORI_2_29.value==this.w_DESORI)
      this.oPgFrm.Page2.oPag.oDESORI_2_29.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIFINT_2_32.value==this.w_DESRIFINT)
      this.oPgFrm.Page2.oPag.oDESRIFINT_2_32.value=this.w_DESRIFINT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT1_2_39.value==this.w_DESCAT1)
      this.oPgFrm.Page2.oPag.oDESCAT1_2_39.value=this.w_DESCAT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT1_2_40.value==this.w_DESATT1)
      this.oPgFrm.Page2.oPag.oDESATT1_2_40.value=this.w_DESATT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT2_2_41.value==this.w_DESCAT2)
      this.oPgFrm.Page2.oPag.oDESCAT2_2_41.value=this.w_DESCAT2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT2_2_42.value==this.w_DESATT2)
      this.oPgFrm.Page2.oPag.oDESATT2_2_42.value=this.w_DESATT2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT3_2_43.value==this.w_DESCAT3)
      this.oPgFrm.Page2.oPag.oDESCAT3_2_43.value=this.w_DESCAT3
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT3_2_44.value==this.w_DESATT3)
      this.oPgFrm.Page2.oPag.oDESATT3_2_44.value=this.w_DESATT3
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT1_2_45.value==this.w_RACODAT1)
      this.oPgFrm.Page2.oPag.oRACODAT1_2_45.value=this.w_RACODAT1
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT2_2_46.value==this.w_RACODAT2)
      this.oPgFrm.Page2.oPag.oRACODAT2_2_46.value=this.w_RACODAT2
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT3_2_47.value==this.w_RACODAT3)
      this.oPgFrm.Page2.oPag.oRACODAT3_2_47.value=this.w_RACODAT3
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_38.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oForzaNom_3_20.RadioValue()==this.w_ForzaNom)
      this.oPgFrm.Page3.oPag.oForzaNom_3_20.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oSAL_Dest_3_21.RadioValue()==this.w_SAL_Dest)
      this.oPgFrm.Page3.oPag.oSAL_Dest_3_21.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oRAGSOC_Dest_3_22.value==this.w_RAGSOC_Dest)
      this.oPgFrm.Page3.oPag.oRAGSOC_Dest_3_22.value=this.w_RAGSOC_Dest
    endif
    if not(this.oPgFrm.Page3.oPag.oINDIRI_Dest_3_23.value==this.w_INDIRI_Dest)
      this.oPgFrm.Page3.oPag.oINDIRI_Dest_3_23.value=this.w_INDIRI_Dest
    endif
    if not(this.oPgFrm.Page3.oPag.oLOCALI_Dest_3_24.value==this.w_LOCALI_Dest)
      this.oPgFrm.Page3.oPag.oLOCALI_Dest_3_24.value=this.w_LOCALI_Dest
    endif
    if not(this.oPgFrm.Page3.oPag.oCAP_Dest_3_25.value==this.w_CAP_Dest)
      this.oPgFrm.Page3.oPag.oCAP_Dest_3_25.value=this.w_CAP_Dest
    endif
    if not(this.oPgFrm.Page3.oPag.oPROV_Dest_3_26.value==this.w_PROV_Dest)
      this.oPgFrm.Page3.oPag.oPROV_Dest_3_26.value=this.w_PROV_Dest
    endif
    if not(this.oPgFrm.Page3.oPag.oNAZ_Dest_3_27.value==this.w_NAZ_Dest)
      this.oPgFrm.Page3.oPag.oNAZ_Dest_3_27.value=this.w_NAZ_Dest
    endif
    if not(this.oPgFrm.Page3.oPag.oCortAtt_Dest_3_28.value==this.w_CortAtt_Dest)
      this.oPgFrm.Page3.oPag.oCortAtt_Dest_3_28.value=this.w_CortAtt_Dest
    endif
    if not(this.oPgFrm.Page3.oPag.oNADESNAZ_3_36.value==this.w_NADESNAZ)
      this.oPgFrm.Page3.oPag.oNADESNAZ_3_36.value=this.w_NADESNAZ
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PDATINIZ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPDATINIZ_2_13.SetFocus()
            i_bnoObbl = !empty(.w_PDATINIZ)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_Associa = this.w_Associa
    this.o_GRUPSTA = this.w_GRUPSTA
    this.o_PDATINIZ = this.w_PDATINIZ
    this.o_NORIFINT = this.w_NORIFINT
    this.o_ForzaNom = this.w_ForzaNom
    return

enddefine

* --- Define pages as container
define class tgsag_sbuPag1 as StdContainer
  Width  = 790
  height = 454
  stdWidth  = 790
  stdheight = 454
  resizeXpos=303
  resizeYpos=295
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESNOM_1_1 as AH_SEARCHFLD with uid="CBKKYKLBHC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale o parte di essa (ricerca contestuale)",;
    HelpContextID = 198791734,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=104, Top=9, InputMask=replicate('X',40)

  add object oAssocia_1_2 as StdRadio with uid="NVECKGBUVY",rtseq=2,rtrep=.f.,left=338, top=9, width=376,height=23;
    , ToolTipText = "Selezionare il tipo di associazione";
    , cFormVar="w_Associa", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oAssocia_1_2.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Tutti"
      this.Buttons(1).HelpContextID = 154959878
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Non associati a pratiche"
      this.Buttons(2).HelpContextID = 154959878
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Non associati a pratiche","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Associati a pratiche"
      this.Buttons(3).HelpContextID = 154959878
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Associati a pratiche","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Selezionare il tipo di associazione")
      StdRadio::init()
    endproc

  func oAssocia_1_2.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oAssocia_1_2.GetRadio()
    this.Parent.oContained.w_Associa = this.RadioValue()
    return .t.
  endfunc

  func oAssocia_1_2.SetRadio()
    this.Parent.oContained.w_Associa=trim(this.Parent.oContained.w_Associa)
    this.value = ;
      iif(this.Parent.oContained.w_Associa=='T',1,;
      iif(this.Parent.oContained.w_Associa=='N',2,;
      iif(this.Parent.oContained.w_Associa=='A',3,;
      0)))
  endfunc

  func oAssocia_1_2.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oBtn_1_3 as StdButton with uid="BQJHHOQZPU",left=724, top=8, width=23,height=23,;
    CpPicture="bmp\refresh.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per annullare i filtri di selezione";
    , HelpContextID = 107620106;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        GSAG_BSB(this.Parent.oContained,"PULISCIFILTRI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_4 as StdButton with uid="LOVXDGGUAB",left=751, top=8, width=23,height=23,;
    CpPicture="tzoom.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca nominativi";
    , HelpContextID = 107620106;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!IsAlt())
     endwith
    endif
  endfunc

  add object oDESCAN_1_5 as AH_SEARCHFLD with uid="YZDXTPCKGO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Nome pratica o parte di esso (ricerca contestuale)",;
    HelpContextID = 200167990,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=103, Top=41, InputMask=replicate('X',100)

  func oDESCAN_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Associa='A')
    endwith
   endif
  endfunc

  func oDESCAN_1_5.mHide()
    with this.Parent.oContained
      return (.w_DimScheda=1)
    endwith
  endfunc

  add object oCODCAN_1_6 as AH_SEARCHFLD with uid="LEFUSAGYNV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODCAN", cQueryName = "CODCAN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice o numero della pratica o parte di esso",;
    HelpContextID = 200109094,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=379, Top=41, InputMask=replicate('X',15)

  func oCODCAN_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Associa='A')
    endwith
   endif
  endfunc

  func oCODCAN_1_6.mHide()
    with this.Parent.oContained
      return (.w_DimScheda=1)
    endwith
  endfunc


  add object oFLAPE_1_7 as StdCombo with uid="FFVUYEGYZW",rtseq=5,rtrep=.f.,left=505,top=41,width=104,height=21;
    , ToolTipText = "Selezione su apertura/chiusura";
    , HelpContextID = 29738666;
    , cFormVar="w_FLAPE",RowSource=""+"Tutte,"+"Aperte,"+"Chiuse", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLAPE_1_7.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oFLAPE_1_7.GetRadio()
    this.Parent.oContained.w_FLAPE = this.RadioValue()
    return .t.
  endfunc

  func oFLAPE_1_7.SetRadio()
    this.Parent.oContained.w_FLAPE=trim(this.Parent.oContained.w_FLAPE)
    this.value = ;
      iif(this.Parent.oContained.w_FLAPE=='T',1,;
      iif(this.Parent.oContained.w_FLAPE=='A',2,;
      iif(this.Parent.oContained.w_FLAPE=='C',3,;
      0)))
  endfunc

  func oFLAPE_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Associa='A')
    endwith
   endif
  endfunc

  func oFLAPE_1_7.mHide()
    with this.Parent.oContained
      return (.w_Associa<>'A')
    endwith
  endfunc


  add object oFLARCH_1_8 as StdCombo with uid="ACDUZJFCZT",rtseq=6,rtrep=.f.,left=613,top=41,width=104,height=21;
    , ToolTipText = "Selezione su archiviazione";
    , HelpContextID = 102512982;
    , cFormVar="w_FLARCH",RowSource=""+"Archiviate,"+"Non archiviate,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLARCH_1_8.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLARCH_1_8.GetRadio()
    this.Parent.oContained.w_FLARCH = this.RadioValue()
    return .t.
  endfunc

  func oFLARCH_1_8.SetRadio()
    this.Parent.oContained.w_FLARCH=trim(this.Parent.oContained.w_FLARCH)
    this.value = ;
      iif(this.Parent.oContained.w_FLARCH=='A',1,;
      iif(this.Parent.oContained.w_FLARCH=='N',2,;
      iif(this.Parent.oContained.w_FLARCH=='T',3,;
      0)))
  endfunc

  func oFLARCH_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Associa='A' AND .w_FLAPE='C')
    endwith
   endif
  endfunc

  func oFLARCH_1_8.mHide()
    with this.Parent.oContained
      return (.w_Associa<>'A' OR .w_FLAPE<>'C')
    endwith
  endfunc


  add object oBtn_1_9 as StdButton with uid="OKDHYSKAYN",left=724, top=40, width=23,height=23,;
    CpPicture="bmp\Check_small.bmp", caption="", nPag=1;
    , ToolTipText = "Seleziona tutte le pratiche";
    , HelpContextID = 107620106;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSAG_BSB(this.Parent.oContained,"SELEZ_PRA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Associa='A')
      endwith
    endif
  endfunc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_DimScheda=1)
     endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="KKCTLVFEOP",left=751, top=40, width=23,height=23,;
    CpPicture="bmp\unCheck_small.bmp", caption="", nPag=1;
    , ToolTipText = "Deseleziona tutte le pratiche";
    , HelpContextID = 107620106;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        GSAG_BSB(this.Parent.oContained,"DESEL_PRA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Associa='A')
      endwith
    endif
  endfunc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_DimScheda=1)
     endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="JBFSHNCMPX",left=13, top=418, width=23,height=23,;
    CpPicture="bmp\Check_small.bmp", caption="", nPag=1;
    , ToolTipText = "Seleziona tutti i nominativi";
    , HelpContextID = 107620106;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        GSAG_BSB(this.Parent.oContained,"SELEZ_NOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="GZBYUQIAUW",left=38, top=418, width=23,height=23,;
    CpPicture="bmp\unCheck_small.bmp", caption="", nPag=1;
    , ToolTipText = "Deseleziona tutti i nominativi";
    , HelpContextID = 107620106;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSAG_BSB(this.Parent.oContained,"DESEL_NOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!IsAlt())
     endwith
    endif
  endfunc


  add object oGRUPSTA_1_13 as StdCombo with uid="VDJISFVFDX",rtseq=7,rtrep=.f.,left=200,top=419,width=138,height=21;
    , ToolTipText = "Selezionare il gruppo stampa";
    , HelpContextID = 52133734;
    , cFormVar="w_GRUPSTA",RowSource=""+"Stampa buste,"+"Nominativi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGRUPSTA_1_13.RadioValue()
    return(iif(this.value =1,'GSAG_SBU',;
    iif(this.value =2,'GSAG_SNO',;
    space(8))))
  endfunc
  func oGRUPSTA_1_13.GetRadio()
    this.Parent.oContained.w_GRUPSTA = this.RadioValue()
    return .t.
  endfunc

  func oGRUPSTA_1_13.SetRadio()
    this.Parent.oContained.w_GRUPSTA=trim(this.Parent.oContained.w_GRUPSTA)
    this.value = ;
      iif(this.Parent.oContained.w_GRUPSTA=='GSAG_SBU',1,;
      iif(this.Parent.oContained.w_GRUPSTA=='GSAG_SNO',2,;
      0))
  endfunc


  add object AGSBU_OUTPUT as cp_outputCombo with uid="HTXEREPUOY",left=436, top=419, width=186,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 70377446


  add object oBtn_1_15 as StdButton with uid="NKKKKEHVSZ",left=626, top=408, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca nominativi";
    , HelpContextID = 69302038;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSAG_BSB(this.Parent.oContained,"RICERCANOMI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="MBXAIGGBZE",left=678, top=408, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 34169382;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSAG_BSB(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OREP))
      endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="TKCMXZXILU",left=730, top=408, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 100302778;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AGSBU_NOM as cp_szoombox with uid="NEVIFHDGHN",left=0, top=194, width=782,height=202,;
    caption='AGSBU_NOM',;
   bGlobalFont=.t.,;
    cTable="OFF_NOMI",cZoomFile="GSAG_SBU",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.f.,;
    cEvent = "NessunEvento",;
    nPag=1;
    , HelpContextID = 30582491


  add object AGSBU_PRA as cp_szoombox with uid="MLNSACXJRL",left=0, top=66, width=787,height=127,;
    caption='AGSBU_PRA',;
   bGlobalFont=.t.,;
    cTable="CAN_TIER",cZoomFile="GSAG_SBU",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.f.,;
    cEvent = "AggiornaPratiche",;
    nPag=1;
    , HelpContextID = 237852776

  add object oSELEZI_1_38 as StdRadio with uid="VNICEXLLTR",rtseq=56,rtrep=.f.,left=3, top=419, width=127,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona tutte le righe";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_38.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 142598950
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 142598950
      this.Buttons(2).Top=15
      this.SetAll("Width",125)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutte le righe")
      StdRadio::init()
    endproc

  func oSELEZI_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_38.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_38.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_1_38.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="AUXASRUDLA",Visible=.t., Left=3, Top=14,;
    Alignment=1, Width=96, Height=18,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="UKHJNTPHQR",Visible=.t., Left=340, Top=422,;
    Alignment=1, Width=93, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="QCZDRFVESV",Visible=.t., Left=94, Top=422,;
    Alignment=1, Width=102, Height=18,;
    Caption="Gruppo stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="BXBENZYVRD",Visible=.t., Left=8, Top=41,;
    Alignment=1, Width=91, Height=18,;
    Caption="Nome pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_DimScheda=1)
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="PXYZUVRXPL",Visible=.t., Left=322, Top=43,;
    Alignment=1, Width=51, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_DimScheda=1)
    endwith
  endfunc

  add object oBox_1_19 as StdBox with uid="LOIZABPOHX",left=101, top=6, width=223,height=28

  add object oBox_1_34 as StdBox with uid="JZWQNEEELC",left=376, top=38, width=124,height=27

  add object oBox_1_35 as StdBox with uid="NMBCBASFAL",left=100, top=38, width=223,height=27
enddefine
define class tgsag_sbuPag2 as StdContainer
  Width  = 790
  height = 454
  stdWidth  = 790
  stdheight = 454
  resizeXpos=404
  resizeYpos=388
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oINDIRI_2_7 as AH_SEARCHFLD with uid="NQEOFMRBEJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_INDIRI", cQueryName = "INDIRI",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di residenza o parte di esso (ricerca contestuale)",;
    HelpContextID = 134441862,;
   bGlobalFont=.t.,;
    Height=21, Width=426, Left=126, Top=12, InputMask=replicate('X',50)

  add object oCAP_2_8 as StdField with uid="TZUIBEXOLE",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CAP", cQueryName = "CAP",;
    bObbl = .f. , nPag = 2, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. di residenza",;
    HelpContextID = 107274714,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=648, Top=12, InputMask=replicate('X',8)

  add object oLOCALI_2_9 as AH_SEARCHFLD with uid="SNARLNNZOG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_LOCALI", cQueryName = "LOCALI",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� o parte di essa (ricerca contestuale)",;
    HelpContextID = 127622326,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=126, Top=41, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oLOCALI_2_9.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_LOCALI",".w_PROVIN"," ")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPROVIN_2_10 as StdField with uid="HOLMLAWTKH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PROVIN", cQueryName = "PROVIN",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di residenza",;
    HelpContextID = 209788918,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=525, Top=41, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  add object oNOTE_2_11 as AH_SEARCHMEMO with uid="AXNLDTQQRX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Annotazioni o parte di esse",;
    HelpContextID = 102732586,;
   bGlobalFont=.t.,;
    Height=21, Width=534, Left=126, Top=70

  add object oPDATINIZ_2_13 as StdField with uid="KXZLKOTRZF",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PDATINIZ", cQueryName = "PDATINIZ",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 58838448,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=126, Top=99

  add object oobsodat1_2_14 as StdCheck with uid="TANZHKUOWP",rtseq=17,rtrep=.f.,left=221, top=99, caption="Solo obsoleti",;
    ToolTipText = "Stampa solo obsoleti",;
    HelpContextID = 21787159,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oobsodat1_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_2_14.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_2_14.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc

  add object oTIPONOME_2_15 as StdCheck with uid="XBBFEWCVIO",rtseq=18,rtrep=.f.,left=421, top=99, caption="Solo clienti",;
    ToolTipText = "Se attivo, visualizza solo i clienti",;
    HelpContextID = 37083269,;
    cFormVar="w_TIPONOME", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTIPONOME_2_15.RadioValue()
    return(iif(this.value =1,'C',;
    space(1)))
  endfunc
  func oTIPONOME_2_15.GetRadio()
    this.Parent.oContained.w_TIPONOME = this.RadioValue()
    return .t.
  endfunc

  func oTIPONOME_2_15.SetRadio()
    this.Parent.oContained.w_TIPONOME=trim(this.Parent.oContained.w_TIPONOME)
    this.value = ;
      iif(this.Parent.oContained.w_TIPONOME=='C',1,;
      0)
  endfunc

  func oTIPONOME_2_15.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oTIPONOME_2_16 as StdCombo with uid="DXCXLHQFGI",rtseq=29,rtrep=.f.,left=421,top=99,width=164,height=22;
    , ToolTipText = "Visualizza la tipologia di nominativo selezionata";
    , HelpContextID = 37083269;
    , cFormVar="w_TIPONOME",RowSource=""+"Solo clienti,"+"Solo fornitori,"+"Solo nominativi,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPONOME_2_16.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'T',;
    iif(this.value =4,'X',;
    space(1))))))
  endfunc
  func oTIPONOME_2_16.GetRadio()
    this.Parent.oContained.w_TIPONOME = this.RadioValue()
    return .t.
  endfunc

  func oTIPONOME_2_16.SetRadio()
    this.Parent.oContained.w_TIPONOME=trim(this.Parent.oContained.w_TIPONOME)
    this.value = ;
      iif(this.Parent.oContained.w_TIPONOME=='C',1,;
      iif(this.Parent.oContained.w_TIPONOME=='F',2,;
      iif(this.Parent.oContained.w_TIPONOME=='T',3,;
      iif(this.Parent.oContained.w_TIPONOME=='X',4,;
      0))))
  endfunc

  func oTIPONOME_2_16.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oPERSONE_2_17 as StdCheck with uid="KFELYSHCCV",rtseq=30,rtrep=.f.,left=634, top=99, caption="Persone",;
    ToolTipText = "Se attivo, nella stampa delle etichette vengono riportate anche le persone di riferimento",;
    HelpContextID = 215892726,;
    cFormVar="w_PERSONE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPERSONE_2_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPERSONE_2_17.GetRadio()
    this.Parent.oContained.w_PERSONE = this.RadioValue()
    return .t.
  endfunc

  func oPERSONE_2_17.SetRadio()
    this.Parent.oContained.w_PERSONE=trim(this.Parent.oContained.w_PERSONE)
    this.value = ;
      iif(this.Parent.oContained.w_PERSONE=='S',1,;
      0)
  endfunc

  func oPERSONE_2_17.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oNOCODGRU_2_18 as StdField with uid="VAYPMELLUY",rtseq=31,rtrep=.f.,;
    cFormVar = "w_NOCODGRU", cQueryName = "NOCODGRU",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo",;
    HelpContextID = 86596907,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=128, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", oKey_1_1="GNCODICE", oKey_1_2="this.w_NOCODGRU"

  func oNOCODGRU_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODGRU_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODGRU_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oNOCODGRU_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi nominativi",'',this.parent.oContained
  endproc

  add object oNORIFINT_2_19 as StdField with uid="TQTOGVNAZN",rtseq=32,rtrep=.f.,;
    cFormVar = "w_NORIFINT", cQueryName = "NORIFINT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento interno",;
    HelpContextID = 146518742,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoPersona", oKey_2_1="DPCODICE", oKey_2_2="this.w_NORIFINT"

  func oNORIFINT_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oNORIFINT_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNORIFINT_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoPersona)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoPersona)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oNORIFINT_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Riferimenti interni",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oNOCODORI_2_20 as StdField with uid="OJETTPZXIQ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_NOCODORI", cQueryName = "NOCODORI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine",;
    HelpContextID = 220814623,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=186, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", cZoomOnZoom="GSAR_AON", oKey_1_1="ONCODICE", oKey_1_2="this.w_NOCODORI"

  func oNOCODORI_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODORI_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODORI_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oNOCODORI_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AON',"Origine",'',this.parent.oContained
  endproc
  proc oNOCODORI_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AON()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ONCODICE=this.parent.oContained.w_NOCODORI
     i_obj.ecpSave()
  endproc

  add object oCodUte_2_21 as StdField with uid="PIUVUYSBHD",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CodUte", cQueryName = "CodUte",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente ultima modifica",;
    HelpContextID = 103910438,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=215, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_CodUte"

  func oCodUte_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodUte_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodUte_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','CODE',cp_AbsName(this.parent,'oCodUte_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oCONS_DATI_2_22 as StdRadio with uid="CSXZBUPOVR",rtseq=35,rtrep=.f.,left=635, top=170, width=116,height=49;
    , ToolTipText = "Selezionare lo stato relativo al consenso del trattamento dati personali";
    , cFormVar="w_CONS_DATI", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oCONS_DATI_2_22.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Non ricevuto"
      this.Buttons(1).HelpContextID = 64885002
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Ricevuto"
      this.Buttons(2).HelpContextID = 64885002
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Tutti"
      this.Buttons(3).HelpContextID = 64885002
      this.Buttons(3).Top=30
      this.SetAll("Width",114)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Selezionare lo stato relativo al consenso del trattamento dati personali")
      StdRadio::init()
    endproc

  func oCONS_DATI_2_22.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oCONS_DATI_2_22.GetRadio()
    this.Parent.oContained.w_CONS_DATI = this.RadioValue()
    return .t.
  endfunc

  func oCONS_DATI_2_22.SetRadio()
    this.Parent.oContained.w_CONS_DATI=trim(this.Parent.oContained.w_CONS_DATI)
    this.value = ;
      iif(this.Parent.oContained.w_CONS_DATI=='N',1,;
      iif(this.Parent.oContained.w_CONS_DATI=='S',2,;
      iif(this.Parent.oContained.w_CONS_DATI=='',3,;
      0)))
  endfunc

  add object oDESGRU_2_25 as StdField with uid="FRHMVMPLOH",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 67260982,;
   bGlobalFont=.t.,;
    Height=21, Width=386, Left=199, Top=128, InputMask=replicate('X',35)

  add object oDESUTE_2_27 as StdField with uid="JXGTZBFHCK",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 70275638,;
   bGlobalFont=.t.,;
    Height=21, Width=386, Left=199, Top=215, InputMask=replicate('X',20)

  add object oDESORI_2_29 as StdField with uid="HIDPUISWDJ",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 134894134,;
   bGlobalFont=.t.,;
    Height=21, Width=386, Left=199, Top=186, InputMask=replicate('X',35)

  add object oDESRIFINT_2_32 as StdField with uid="QIOIXUSSHJ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESRIFINT", cQueryName = "DESRIFINT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 193112124,;
   bGlobalFont=.t.,;
    Height=21, Width=386, Left=199, Top=157, InputMask=replicate('X',35)

  add object oDESCAT1_2_39 as StdField with uid="RQHPFELTYB",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESCAT1", cQueryName = "DESCAT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 32395830,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=238, Top=288, InputMask=replicate('X',35)

  add object oDESATT1_2_40 as StdField with uid="BNZQCLINZM",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESATT1", cQueryName = "DESATT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 52187702,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=421, Top=288, InputMask=replicate('X',35)

  add object oDESCAT2_2_41 as StdField with uid="WJCRSTIJKM",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESCAT2", cQueryName = "DESCAT2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 32395830,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=238, Top=313, InputMask=replicate('X',35)

  add object oDESATT2_2_42 as StdField with uid="ZBFJSTINNA",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESATT2", cQueryName = "DESATT2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 52187702,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=421, Top=313, InputMask=replicate('X',35)

  add object oDESCAT3_2_43 as StdField with uid="NKTPRZLJCA",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESCAT3", cQueryName = "DESCAT3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 32395830,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=238, Top=338, InputMask=replicate('X',35)

  add object oDESATT3_2_44 as StdField with uid="TBHEYGIQXL",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESATT3", cQueryName = "DESATT3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 52187702,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=421, Top=338, InputMask=replicate('X',35)

  add object oRACODAT1_2_45 as StdField with uid="QBQJLRJLTX",rtseq=50,rtrep=.f.,;
    cFormVar = "w_RACODAT1", cQueryName = "RACODAT1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 254365511,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=134, Top=288, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT1"

  func oRACODAT1_2_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT1_2_45.ecpDrop(oSource)
    this.Parent.oContained.link_2_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT1_2_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT1_2_45'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT1_2_45.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT1
     i_obj.ecpSave()
  endproc

  add object oRACODAT2_2_46 as StdField with uid="ZGTNSJFFHG",rtseq=51,rtrep=.f.,;
    cFormVar = "w_RACODAT2", cQueryName = "RACODAT2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 254365512,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=134, Top=313, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT2"

  func oRACODAT2_2_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_46('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT2_2_46.ecpDrop(oSource)
    this.Parent.oContained.link_2_46('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT2_2_46.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT2_2_46'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT2_2_46.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT2
     i_obj.ecpSave()
  endproc

  add object oRACODAT3_2_47 as StdField with uid="YAADXVOHNX",rtseq=52,rtrep=.f.,;
    cFormVar = "w_RACODAT3", cQueryName = "RACODAT3",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 254365513,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=134, Top=338, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT3"

  func oRACODAT3_2_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_47('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT3_2_47.ecpDrop(oSource)
    this.Parent.oContained.link_2_47('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT3_2_47.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT3_2_47'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT3_2_47.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT3
     i_obj.ecpSave()
  endproc


  add object oBtn_2_51 as StdButton with uid="NEGKFXOIMY",left=725, top=12, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=2;
    , ToolTipText = "Ricerca nominativi";
    , HelpContextID = 69302038;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_51.Click()
      with this.Parent.oContained
        GSAG_BSB(this.Parent.oContained,"ATTIVA1_E_RICERCA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_2 as StdString with uid="ELGVYCEQRO",Visible=.t., Left=3, Top=99,;
    Alignment=1, Width=116, Height=18,;
    Caption="Data di controllo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="OYQMKSMLAM",Visible=.t., Left=8, Top=12,;
    Alignment=1, Width=111, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="EFYIHWLQLC",Visible=.t., Left=556, Top=12,;
    Alignment=1, Width=88, Height=18,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="LGJJQYJYZQ",Visible=.t., Left=8, Top=41,;
    Alignment=1, Width=111, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="HFHRJAMQUI",Visible=.t., Left=408, Top=41,;
    Alignment=1, Width=111, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="PMBUJPPQBN",Visible=.t., Left=8, Top=70,;
    Alignment=1, Width=111, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="RUHFZXVWYQ",Visible=.t., Left=8, Top=128,;
    Alignment=1, Width=111, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="EIENBMKRMG",Visible=.t., Left=8, Top=215,;
    Alignment=1, Width=111, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="ZPREBAWLMV",Visible=.t., Left=8, Top=186,;
    Alignment=1, Width=111, Height=18,;
    Caption="Origine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="CVVHFUWPFJ",Visible=.t., Left=11, Top=156,;
    Alignment=1, Width=108, Height=18,;
    Caption="Riferimento interno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="UQAGSEBNEY",Visible=.t., Left=593, Top=139,;
    Alignment=1, Width=159, Height=18,;
    Caption="Consenso trattamento dati"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="XQIYOIBOJG",Visible=.t., Left=127, Top=259,;
    Alignment=0, Width=95, Height=18,;
    Caption="Attributi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_36 as StdBox with uid="BXCGNKBEAI",left=617, top=157, width=145,height=80

  add object oBox_2_38 as StdBox with uid="UFZFHCAVGL",left=126, top=278, width=484,height=91
enddefine
define class tgsag_sbuPag3 as StdContainer
  Width  = 790
  height = 454
  stdWidth  = 790
  stdheight = 454
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oInsMitt_3_1 as StdCheck with uid="ZLHHZMBODV",rtseq=19,rtrep=.f.,left=151, top=19, caption="Inserisci dati mittente",;
    ToolTipText = "Se attivo, inserisce i dati del mittente",;
    HelpContextID = 75135878,;
    cFormVar="w_InsMitt", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oInsMitt_3_1.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oInsMitt_3_1.GetRadio()
    this.Parent.oContained.w_InsMitt = this.RadioValue()
    return .t.
  endfunc

  func oInsMitt_3_1.SetRadio()
    this.Parent.oContained.w_InsMitt=trim(this.Parent.oContained.w_InsMitt)
    this.value = ;
      iif(this.Parent.oContained.w_InsMitt=='S',1,;
      0)
  endfunc

  add object oRAGSTU_3_2 as StdField with uid="MTXBAOMLOW",rtseq=20,rtrep=.f.,;
    cFormVar = "w_RAGSTU", cQueryName = "RAGSTU",;
    bObbl = .f. , nPag = 3, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale mittente",;
    HelpContextID = 70094614,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=151, Top=50, InputMask=replicate('X',100)

  add object oINDSTU_3_3 as StdField with uid="KISJTOXPSZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_INDSTU", cQueryName = "INDSTU",;
    bObbl = .f. , nPag = 3, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo mittente",;
    HelpContextID = 70085510,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=151, Top=75, InputMask=replicate('X',100)

  add object oLOCSTU_3_4 as StdField with uid="WPTLREWKML",rtseq=22,rtrep=.f.,;
    cFormVar = "w_LOCSTU", cQueryName = "LOCSTU",;
    bObbl = .f. , nPag = 3, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Localit� mittente",;
    HelpContextID = 70081718,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=151, Top=100, InputMask=replicate('X',100)

  add object oCAPSTU_3_5 as StdField with uid="ABXQGBRLMI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CAPSTU", cQueryName = "CAPSTU",;
    bObbl = .f. , nPag = 3, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. mittente",;
    HelpContextID = 70131238,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=151, Top=125, InputMask=replicate('X',9)

  add object oPROVSTU_3_6 as StdField with uid="EYHYZDDIQZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PROVSTU", cQueryName = "PROVSTU",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia mittente",;
    HelpContextID = 52502518,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=328, Top=125, InputMask=replicate('X',2)

  add object oMODSPED_3_7 as StdField with uid="YRXRXGERFF",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MODSPED", cQueryName = "MODSPED",;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Modalit� di spedizione (es: posta prioritaria, via aerea ecc.)",;
    HelpContextID = 65891526,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=151, Top=158, InputMask=replicate('X',50)

  add object oNOTEBUSTA_3_8 as StdField with uid="AWXHVJXWTN",rtseq=26,rtrep=.f.,;
    cFormVar = "w_NOTEBUSTA", cQueryName = "NOTEBUSTA",;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 50360634,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=151, Top=184, InputMask=replicate('X',50)


  add object oBtn_3_10 as StdButton with uid="ZXLTVNOLFW",left=684, top=406, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 34169382;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_10.Click()
      with this.Parent.oContained
        GSAG_BSB(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OREP))
      endwith
    endif
  endfunc


  add object oBtn_3_11 as StdButton with uid="EOOFUAIAUS",left=736, top=406, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 100302778;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oForzaNom_3_20 as StdCombo with uid="BLLFEKXOTK",rtseq=58,rtrep=.f.,left=151,top=253,width=228,height=21;
    , ToolTipText = "Impostando Nessuno sono stampati i dati dei nominativi selezionati, con Inserisci i dati del nuovo destinatario i dati di un destinatario non presente in anagrafica, con Inserisci i dati del nominativo i dati, modificabili, del nominativo selezionato";
    , HelpContextID = 30970685;
    , cFormVar="w_ForzaNom",RowSource=""+"Inserisci dati del nuovo destinatario,"+"Inserisci dati del nominativo selezionato,"+"Nessuno", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oForzaNom_3_20.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'T',;
    iif(this.value =3,'N',;
    space(10)))))
  endfunc
  func oForzaNom_3_20.GetRadio()
    this.Parent.oContained.w_ForzaNom = this.RadioValue()
    return .t.
  endfunc

  func oForzaNom_3_20.SetRadio()
    this.Parent.oContained.w_ForzaNom=trim(this.Parent.oContained.w_ForzaNom)
    this.value = ;
      iif(this.Parent.oContained.w_ForzaNom=='S',1,;
      iif(this.Parent.oContained.w_ForzaNom=='T',2,;
      iif(this.Parent.oContained.w_ForzaNom=='N',3,;
      0)))
  endfunc


  add object oSAL_Dest_3_21 as StdTableCombo with uid="TCTLIKZFBE",rtseq=59,rtrep=.f.,left=481,top=253,width=271,height=21;
    , ToolTipText = "Formula di cortesia";
    , HelpContextID = 54124442;
    , cFormVar="w_SAL_Dest",tablefilter="", bObbl = .f. , nPag = 3;
    , cLinkFile="SAL_NOMI";
    , cTable='SAL_NOMI',cKey='SACODICE',cValue='SADESCRI',cOrderBy='SADESCRI',xDefault=space(5);
  , bGlobalFont=.t.


  func oSAL_Dest_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ForzaNom='S' OR .w_ForzaNom='T')
    endwith
   endif
  endfunc

  func oSAL_Dest_3_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oSAL_Dest_3_21.ecpDrop(oSource)
    this.Parent.oContained.link_3_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oRAGSOC_Dest_3_22 as StdField with uid="MFURWVWDQO",rtseq=60,rtrep=.f.,;
    cFormVar = "w_RAGSOC_Dest", cQueryName = "RAGSOC_Dest",;
    bObbl = .f. , nPag = 3, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale destinatario",;
    HelpContextID = 236631894,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=151, Top=278, InputMask=replicate('X',100)

  func oRAGSOC_Dest_3_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ForzaNom='S' OR .w_ForzaNom='T')
    endwith
   endif
  endfunc

  add object oINDIRI_Dest_3_23 as StdField with uid="CQHJNFBYGX",rtseq=61,rtrep=.f.,;
    cFormVar = "w_INDIRI_Dest", cQueryName = "INDIRI_Dest",;
    bObbl = .f. , nPag = 3, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo destinatario",;
    HelpContextID = 133487334,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=151, Top=303, InputMask=replicate('X',100)

  func oINDIRI_Dest_3_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ForzaNom='S' OR .w_ForzaNom='T')
    endwith
   endif
  endfunc

  add object oLOCALI_Dest_3_24 as StdField with uid="BPHTSTFRRK",rtseq=62,rtrep=.f.,;
    cFormVar = "w_LOCALI_Dest", cQueryName = "LOCALI_Dest",;
    bObbl = .f. , nPag = 3, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Localit� destinatario",;
    HelpContextID = 140306870,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=151, Top=328, InputMask=replicate('X',100), bHasZoom = .t. 

  func oLOCALI_Dest_3_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ForzaNom='S' OR .w_ForzaNom='T')
    endwith
   endif
  endfunc

  proc oLOCALI_Dest_3_24.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_CAP_Dest",".w_LOCALI_Dest",".w_PROV_Dest")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCAP_Dest_3_25 as StdField with uid="WGGCRVOCZM",rtseq=63,rtrep=.f.,;
    cFormVar = "w_CAP_Dest", cQueryName = "CAP_Dest",;
    bObbl = .f. , nPag = 3, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. destinatario",;
    HelpContextID = 54140570,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=151, Top=353, InputMask=replicate('X',9), bHasZoom = .t. 

  func oCAP_Dest_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ForzaNom='S' OR .w_ForzaNom='T')
    endwith
   endif
  endfunc

  proc oCAP_Dest_3_25.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_CAP_Dest",".w_LOCALI_Dest",".w_PROV_Dest")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPROV_Dest_3_26 as StdField with uid="JUTLPFBGAM",rtseq=64,rtrep=.f.,;
    cFormVar = "w_PROV_Dest", cQueryName = "PROV_Dest",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia destinatario",;
    HelpContextID = 65087401,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=328, Top=353, InputMask=replicate('X',2)

  func oPROV_Dest_3_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ForzaNom='S' OR .w_ForzaNom='T')
    endwith
   endif
  endfunc

  add object oNAZ_Dest_3_27 as StdField with uid="NDAGAWCAXD",rtseq=65,rtrep=.f.,;
    cFormVar = "w_NAZ_Dest", cQueryName = "NAZ_Dest",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Nazione destinatario",;
    HelpContextID = 54181706,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=454, Top=353, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", oKey_1_1="NACODNAZ", oKey_1_2="this.w_NAZ_Dest"

  func oNAZ_Dest_3_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ForzaNom='S' OR .w_ForzaNom='T')
    endwith
   endif
  endfunc

  func oNAZ_Dest_3_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oNAZ_Dest_3_27.ecpDrop(oSource)
    this.Parent.oContained.link_3_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNAZ_Dest_3_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oNAZ_Dest_3_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Nazioni",'',this.parent.oContained
  endproc

  add object oCortAtt_Dest_3_28 as StdField with uid="EAPYSEIPHI",rtseq=66,rtrep=.f.,;
    cFormVar = "w_CortAtt_Dest", cQueryName = "CortAtt_Dest",;
    bObbl = .f. , nPag = 3, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Cortese attenzione",;
    HelpContextID = 43845061,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=151, Top=378, InputMask=replicate('X',100)

  func oCortAtt_Dest_3_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ForzaNom='S' OR .w_ForzaNom='T')
    endwith
   endif
  endfunc

  add object oNADESNAZ_3_36 as StdField with uid="YFGPWCONOA",rtseq=68,rtrep=.f.,;
    cFormVar = "w_NADESNAZ", cQueryName = "NADESNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 219111216,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=494, Top=353, InputMask=replicate('X',35)

  add object oStr_3_13 as StdString with uid="LMNOQULUWC",Visible=.t., Left=2, Top=50,;
    Alignment=1, Width=145, Height=18,;
    Caption="Ragione sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_14 as StdString with uid="VFRTZHJXJD",Visible=.t., Left=2, Top=75,;
    Alignment=1, Width=145, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="TMHIOUYIVO",Visible=.t., Left=2, Top=100,;
    Alignment=1, Width=145, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="ULYBIHKLKZ",Visible=.t., Left=2, Top=125,;
    Alignment=1, Width=145, Height=18,;
    Caption="C.A.P.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="UZMALSFJOA",Visible=.t., Left=243, Top=125,;
    Alignment=1, Width=82, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="KGBFSDGVBH",Visible=.t., Left=2, Top=158,;
    Alignment=1, Width=145, Height=18,;
    Caption="Modalit� di spedizione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="CFJURMRRMP",Visible=.t., Left=2, Top=184,;
    Alignment=1, Width=145, Height=18,;
    Caption="Annotazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_3_29 as StdString with uid="EXZCTJYNUR",Visible=.t., Left=2, Top=278,;
    Alignment=1, Width=145, Height=18,;
    Caption="Ragione sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_30 as StdString with uid="DYAOCWAUEP",Visible=.t., Left=2, Top=303,;
    Alignment=1, Width=145, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_31 as StdString with uid="HDHNVOSMER",Visible=.t., Left=2, Top=328,;
    Alignment=1, Width=145, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_3_32 as StdString with uid="HPIMWSAHII",Visible=.t., Left=2, Top=353,;
    Alignment=1, Width=145, Height=18,;
    Caption="C.A.P.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_33 as StdString with uid="XRFKHZGZMC",Visible=.t., Left=243, Top=353,;
    Alignment=1, Width=82, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_3_35 as StdString with uid="QWODFDLUTV",Visible=.t., Left=401, Top=353,;
    Alignment=1, Width=48, Height=18,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_37 as StdString with uid="PLWERNJEJF",Visible=.t., Left=2, Top=378,;
    Alignment=1, Width=145, Height=18,;
    Caption="Cortese attenzione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_39 as StdString with uid="GYTCCBBKDX",Visible=.t., Left=161, Top=221,;
    Alignment=0, Width=90, Height=18,;
    Caption="Dati destinatario"  ;
  , bGlobalFont=.t.

  add object oBox_3_38 as StdBox with uid="YJOXATPYDZ",left=154, top=238, width=598,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_sbu','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
