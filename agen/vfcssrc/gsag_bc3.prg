* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bc3                                                        *
*              Controlli alla check form di gsag_kcl                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-22                                                      *
* Last revis.: 2010-04-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bc3",oParentObject)
return(i_retval)

define class tgsag_bc3 as StdBatch
  * --- Local variables
  w_DATAINI = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_OBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli alla check form
    if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_STATUS$"T-D-I" AND this.oParentObject.w_DATFIN<I_DATSYS
      if !Ah_yesno( "Inserita data antecedente alla data odierna! Continuare?")
        this.oParentObject.w_RESCHK = -1
        this.w_OBJ = this.oParentObject.Getctrl("w_DATFIN")
        this.w_OBJ.Setfocus()     
      endif
    endif
    this.w_DATAINI = IIF(NOT EMPTY(this.oParentObject.w_DATINI), DTOC(this.oParentObject.w_DATINI)+" "+this.oParentObject.w_ORAINI+":"+this.oParentObject.w_MININI+":00", "  -  -       :  :  ")
    this.w_DATAFIN = IIF(NOT EMPTY(this.oParentObject.w_DATFIN), DTOC(this.oParentObject.w_DATFIN)+" "+this.oParentObject.w_ORAFIN+":"+this.oParentObject.w_MINFIN+":00", "  -  -       :  :  ")
    if this.oParentObject.w_RESCHK <> -1 AND cp_CharToDatetime(this.w_DATAFIN) < cp_CharToDatetime(this.w_DATAINI)
      Ah_errorMsg("La data/ora iniziale � maggiore della data/ora finale",48,"")
      this.oParentObject.w_RESCHK = -1
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
