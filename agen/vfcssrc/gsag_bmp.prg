* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bmp                                                        *
*              Modifica  prezzo medio                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-07-13                                                      *
* Last revis.: 2014-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pORIG,pPRZMED,pPRZMAX,pPRZMIN,pCODPRE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bmp",oParentObject,m.pORIG,m.pPRZMED,m.pPRZMAX,m.pPRZMIN,m.pCODPRE)
return(i_retval)

define class tgsag_bmp as StdBatch
  * --- Local variables
  pORIG = space(1)
  pPRZMED = 0
  pPRZMAX = 0
  pPRZMIN = 0
  pCODPRE = space(20)
  w_PREZMAX = 0
  w_PREZMED = 0
  w_PREZMIN = 0
  w_PREZZO = 0
  w_PERVAR = 0
  w_PARAME = space(1)
  * --- WorkFile variables
  ART_ICOL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Al doppio click prezzo permette variazione in percentuale
    *     Eseguito da GSVE_BCD  
    *     A: attivit�
    *     I: inserimento provvisorio
    *     V: documenti
    *     P: prestazioni  
    if !Isalt()
      i_retcode = 'stop'
      return
    endif
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARPARAME"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.pCODPRE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARPARAME;
        from (i_cTable) where;
            ARCODART = this.pCODPRE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PARAME = NVL(cp_ToDate(_read_.ARPARAME),cp_NullValue(_read_.ARPARAME))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PREZZO = this.pPRZMED
    if this.w_PARAME $ "4S"
      this.w_PREZMAX = this.pPRZMAX
      this.w_PREZMED = this.pPRZMED
      this.w_PREZMIN = this.pPRZMIN
      do gsal_kpm with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_PERVAR<>0
        do case
          case this.pORIG="A" or this.pORIG="I"
            this.oParentObject.w_DAPREZZO = this.w_PREZMED
            This.oparentobject.mcalc(.t.)
          case this.pORIG="P" 
            this.oParentObject.w_PRPREZZO = this.w_PREZMED
            This.oparentobject.mcalc(.t.)
          case this.pORIG="V"
            i_retcode = 'stop'
            i_retval = this.w_PREZMED
            return
        endcase
      else
        if this.pORIG="V"
          i_retcode = 'stop'
          i_retval = this.w_PREZMED
          return
        endif
      endif
    else
      if this.pORIG="V"
        i_retcode = 'stop'
        i_retval = this.w_PREZMED
        return
      endif
    endif
  endproc


  proc Init(oParentObject,pORIG,pPRZMED,pPRZMAX,pPRZMIN,pCODPRE)
    this.pORIG=pORIG
    this.pPRZMED=pPRZMED
    this.pPRZMAX=pPRZMAX
    this.pPRZMIN=pPRZMIN
    this.pCODPRE=pCODPRE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ART_ICOL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pORIG,pPRZMED,pPRZMAX,pPRZMIN,pCODPRE"
endproc
