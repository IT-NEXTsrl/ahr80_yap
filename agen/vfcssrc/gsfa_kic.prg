* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_kic                                                        *
*              Importazione telefonate centralino BLUES                        *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-01                                                      *
* Last revis.: 2010-04-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsfa_kic",oParentObject))

* --- Class definition
define class tgsfa_kic as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 610
  Height = 430+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-04-19"
  HelpContextID=267027049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Constant Properties
  _IDX = 0
  UTE_NTI_IDX = 0
  TIPEVENT_IDX = 0
  BLUESCON_IDX = 0
  DRVEVENT_IDX = 0
  cPrg = "gsfa_kic"
  cComment = "Importazione telefonate centralino BLUES"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_UTCODICE = 0
  o_UTCODICE = 0
  w_UTDESUTE = space(40)
  w_UTTELEFO = space(18)
  w_UTTELEF2 = space(18)
  w_UTPHDTIM = ctot('')
  w_UTSELTRA = space(1)
  w_UTDIRCHI = space(1)
  w_UTSELSNM = space(1)
  w_UTTELSUP = space(6)
  o_UTTELSUP = space(6)
  w_TELEFO = space(18)
  w_TELEF2 = space(18)
  w_SELUTPE = space(1)
  o_SELUTPE = space(1)
  w_IBCODICE = 0
  w_IBTIPEVI = space(10)
  w_IBTIPEVO = space(10)
  w_PHDTIM = ctot('')
  w_PHDTIMFI = ctot('')
  w_SELTRA = space(1)
  w_SELSNM = space(1)
  w_DIRCHI = space(1)
  w_TIPEVEI = space(10)
  o_TIPEVEI = space(10)
  w_TIPEVEO = space(10)
  o_TIPEVEO = space(10)
  w_ORE = 0
  w_MINUTI = 0
  w_SECONDI = 0
  w_TIPDIR1 = space(10)
  w_TIPDIR2 = space(10)
  w_Msg = space(0)
  w_DESCRII = space(50)
  w_DESCRIO = space(50)
  w_DRIVER1 = space(10)
  w_DRIVER2 = space(10)
  w_TIPODRV1 = space(1)
  w_TIPODRV2 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsfa_kicPag1","gsfa_kic",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Impostazioni")
      .Pages(2).addobject("oPag","tgsfa_kicPag2","gsfa_kic",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Log")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oUTCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='UTE_NTI'
    this.cWorkTables[2]='TIPEVENT'
    this.cWorkTables[3]='BLUESCON'
    this.cWorkTables[4]='DRVEVENT'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSFA_BIC with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_UTCODICE=0
      .w_UTDESUTE=space(40)
      .w_UTTELEFO=space(18)
      .w_UTTELEF2=space(18)
      .w_UTPHDTIM=ctot("")
      .w_UTSELTRA=space(1)
      .w_UTDIRCHI=space(1)
      .w_UTSELSNM=space(1)
      .w_UTTELSUP=space(6)
      .w_TELEFO=space(18)
      .w_TELEF2=space(18)
      .w_SELUTPE=space(1)
      .w_IBCODICE=0
      .w_IBTIPEVI=space(10)
      .w_IBTIPEVO=space(10)
      .w_PHDTIM=ctot("")
      .w_PHDTIMFI=ctot("")
      .w_SELTRA=space(1)
      .w_SELSNM=space(1)
      .w_DIRCHI=space(1)
      .w_TIPEVEI=space(10)
      .w_TIPEVEO=space(10)
      .w_ORE=0
      .w_MINUTI=0
      .w_SECONDI=0
      .w_TIPDIR1=space(10)
      .w_TIPDIR2=space(10)
      .w_Msg=space(0)
      .w_DESCRII=space(50)
      .w_DESCRIO=space(50)
      .w_DRIVER1=space(10)
      .w_DRIVER2=space(10)
      .w_TIPODRV1=space(1)
      .w_TIPODRV2=space(1)
        .w_UTCODICE = i_CODUTE
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_UTCODICE))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,5,.f.)
        .w_UTSELTRA = 'S'
        .w_UTDIRCHI = 'T'
        .w_UTSELSNM = 'S'
        .w_UTTELSUP = RIGHT('00'+alltrim(STR(.w_ORE,2)), 2)+RIGHT('00'+alltrim(STR(.w_MINUTI,2)), 2)+RIGHT('00'+alltrim(STR(.w_SECONDI,2)), 2)
        .w_TELEFO = iif(.w_SELUTPE='U', .w_UTTELEFO, space(18))
        .w_TELEF2 = iif(.w_SELUTPE='U', .w_UTTELEF2, space(18))
        .w_SELUTPE = iif(isalt(),'P','U')
        .w_IBCODICE = 1
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_IBCODICE))
          .link_1_17('Full')
        endif
          .DoRTCalc(14,15,.f.)
        .w_PHDTIM = .w_UTPHDTIM
        .w_PHDTIMFI = DATETIME()
        .w_SELTRA = iif(.w_UTCODICE<>0 and not empty(nvl(.w_UTSELTRA,'')), .w_UTSELTRA, 'S')
        .w_SELSNM = iif(.w_UTCODICE<>0 and not empty(nvl(.w_UTSELSNM,'')), .w_UTSELSNM, 'S')
        .w_DIRCHI = iif(.w_UTCODICE<>0 and not empty(nvl(.w_UTDIRCHI,'')), .w_UTDIRCHI, 'T')
        .w_TIPEVEI = .w_IBTIPEVI
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_TIPEVEI))
          .link_1_25('Full')
        endif
        .w_TIPEVEO = .w_IBTIPEVO
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_TIPEVEO))
          .link_1_26('Full')
        endif
        .w_ORE = val(left(.w_UTTELSUP, 2))
        .w_MINUTI = val(substr(.w_UTTELSUP, 3, 2))
        .w_SECONDI = val(right(.w_UTTELSUP, 2))
        .w_TIPDIR1 = 'E'
        .w_TIPDIR2 = 'U'
        .DoRTCalc(28,31,.f.)
        if not(empty(.w_DRIVER1))
          .link_1_47('Full')
        endif
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_DRIVER2))
          .link_1_48('Full')
        endif
    endwith
    this.DoRTCalc(33,34,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_UTCODICE<>.w_UTCODICE
          .link_1_1('Full')
        endif
        .DoRTCalc(2,8,.t.)
            .w_UTTELSUP = RIGHT('00'+alltrim(STR(.w_ORE,2)), 2)+RIGHT('00'+alltrim(STR(.w_MINUTI,2)), 2)+RIGHT('00'+alltrim(STR(.w_SECONDI,2)), 2)
        if .o_UTCODICE<>.w_UTCODICE.or. .o_SELUTPE<>.w_SELUTPE
            .w_TELEFO = iif(.w_SELUTPE='U', .w_UTTELEFO, space(18))
        endif
        if .o_UTCODICE<>.w_UTCODICE.or. .o_SELUTPE<>.w_SELUTPE
            .w_TELEF2 = iif(.w_SELUTPE='U', .w_UTTELEF2, space(18))
        endif
        .DoRTCalc(12,12,.t.)
          .link_1_17('Full')
        .DoRTCalc(14,15,.t.)
        if .o_UTCODICE<>.w_UTCODICE
            .w_PHDTIM = .w_UTPHDTIM
        endif
        .DoRTCalc(17,17,.t.)
        if .o_UTCODICE<>.w_UTCODICE
            .w_SELTRA = iif(.w_UTCODICE<>0 and not empty(nvl(.w_UTSELTRA,'')), .w_UTSELTRA, 'S')
        endif
        if .o_UTCODICE<>.w_UTCODICE
            .w_SELSNM = iif(.w_UTCODICE<>0 and not empty(nvl(.w_UTSELSNM,'')), .w_UTSELSNM, 'S')
        endif
        if .o_UTCODICE<>.w_UTCODICE
            .w_DIRCHI = iif(.w_UTCODICE<>0 and not empty(nvl(.w_UTDIRCHI,'')), .w_UTDIRCHI, 'T')
        endif
        .DoRTCalc(21,22,.t.)
        if .o_UTTELSUP<>.w_UTTELSUP
            .w_ORE = val(left(.w_UTTELSUP, 2))
        endif
        if .o_UTTELSUP<>.w_UTTELSUP
            .w_MINUTI = val(substr(.w_UTTELSUP, 3, 2))
        endif
        if .o_UTTELSUP<>.w_UTTELSUP
            .w_SECONDI = val(right(.w_UTTELSUP, 2))
        endif
        .DoRTCalc(26,30,.t.)
          .link_1_47('Full')
          .link_1_48('Full')
        if .o_TIPEVEI<>.w_TIPEVEI
          .Calculate_KOHUIRSVTQ()
        endif
        if .o_TIPEVEO<>.w_TIPEVEO
          .Calculate_MZXNQOFHKN()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(33,34,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_KOHUIRSVTQ()
    with this
          * --- Controllo se diver ingresso di tipo telefono
          ChkEVENTO(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_MZXNQOFHKN()
    with this
          * --- Controllo se diver uscita di tipo telefono
          ChkEVENTO(this;
              ,'O';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPEVEI_1_25.visible=!this.oPgFrm.Page1.oPag.oTIPEVEI_1_25.mHide()
    this.oPgFrm.Page1.oPag.oTIPEVEO_1_26.visible=!this.oPgFrm.Page1.oPag.oTIPEVEO_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oDESCRII_1_45.visible=!this.oPgFrm.Page1.oPag.oDESCRII_1_45.mHide()
    this.oPgFrm.Page1.oPag.oDESCRIO_1_46.visible=!this.oPgFrm.Page1.oPag.oDESCRIO_1_46.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=UTCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UTE_NTI_IDX,3]
    i_lTable = "UTE_NTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2], .t., this.UTE_NTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UTE_NTI')
        if i_nConn<>0
          i_cWhere = " UTCODICE="+cp_ToStrODBC(this.w_UTCODICE);

          i_ret=cp_SQL(i_nConn,"select UTCODICE,UTDESUTE,UTTELEF2,UTTELEFO,UTPHDTIM,UTSELTRA,UTSELSNM,UTDIRCHI,UTTELSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UTCODICE',this.w_UTCODICE)
          select UTCODICE,UTDESUTE,UTTELEF2,UTTELEFO,UTPHDTIM,UTSELTRA,UTSELSNM,UTDIRCHI,UTTELSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTCODICE) and !this.bDontReportError
            deferred_cp_zoom('UTE_NTI','*','UTCODICE',cp_AbsName(oSource.parent,'oUTCODICE_1_1'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UTCODICE,UTDESUTE,UTTELEF2,UTTELEFO,UTPHDTIM,UTSELTRA,UTSELSNM,UTDIRCHI,UTTELSUP";
                     +" from "+i_cTable+" "+i_lTable+" where UTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UTCODICE',oSource.xKey(1))
            select UTCODICE,UTDESUTE,UTTELEF2,UTTELEFO,UTPHDTIM,UTSELTRA,UTSELSNM,UTDIRCHI,UTTELSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UTCODICE,UTDESUTE,UTTELEF2,UTTELEFO,UTPHDTIM,UTSELTRA,UTSELSNM,UTDIRCHI,UTTELSUP";
                   +" from "+i_cTable+" "+i_lTable+" where UTCODICE="+cp_ToStrODBC(this.w_UTCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UTCODICE',this.w_UTCODICE)
            select UTCODICE,UTDESUTE,UTTELEF2,UTTELEFO,UTPHDTIM,UTSELTRA,UTSELSNM,UTDIRCHI,UTTELSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTCODICE = NVL(_Link_.UTCODICE,0)
      this.w_UTDESUTE = NVL(_Link_.UTDESUTE,space(40))
      this.w_UTTELEF2 = NVL(_Link_.UTTELEF2,space(18))
      this.w_UTTELEFO = NVL(_Link_.UTTELEFO,space(18))
      this.w_UTPHDTIM = NVL(_Link_.UTPHDTIM,ctot(""))
      this.w_UTSELTRA = NVL(_Link_.UTSELTRA,space(1))
      this.w_UTSELSNM = NVL(_Link_.UTSELSNM,space(1))
      this.w_UTDIRCHI = NVL(_Link_.UTDIRCHI,space(1))
      this.w_UTTELSUP = NVL(_Link_.UTTELSUP,space(6))
    else
      if i_cCtrl<>'Load'
        this.w_UTCODICE = 0
      endif
      this.w_UTDESUTE = space(40)
      this.w_UTTELEF2 = space(18)
      this.w_UTTELEFO = space(18)
      this.w_UTPHDTIM = ctot("")
      this.w_UTSELTRA = space(1)
      this.w_UTSELSNM = space(1)
      this.w_UTDIRCHI = space(1)
      this.w_UTTELSUP = space(6)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])+'\'+cp_ToStr(_Link_.UTCODICE,1)
      cp_ShowWarn(i_cKey,this.UTE_NTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IBCODICE
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BLUESCON_IDX,3]
    i_lTable = "BLUESCON"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BLUESCON_IDX,2], .t., this.BLUESCON_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BLUESCON_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IBCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IBCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IBCODICE,IBTIPEVI,IBTIPEVO";
                   +" from "+i_cTable+" "+i_lTable+" where IBCODICE="+cp_ToStrODBC(this.w_IBCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IBCODICE',this.w_IBCODICE)
            select IBCODICE,IBTIPEVI,IBTIPEVO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IBCODICE = NVL(_Link_.IBCODICE,0)
      this.w_IBTIPEVI = NVL(_Link_.IBTIPEVI,space(10))
      this.w_IBTIPEVO = NVL(_Link_.IBTIPEVO,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_IBCODICE = 0
      endif
      this.w_IBTIPEVI = space(10)
      this.w_IBTIPEVO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BLUESCON_IDX,2])+'\'+cp_ToStr(_Link_.IBCODICE,1)
      cp_ShowWarn(i_cKey,this.BLUESCON_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IBCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPEVEI
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPEVEI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSFA_ATE',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_TIPEVEI)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_TIPEVEI))
          select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPEVEI)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPEVEI) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oTIPEVEI_1_25'),i_cWhere,'GSFA_ATE',"Tipi eventi",'gsfa1aib.TIPEVENT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPEVEI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_TIPEVEI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_TIPEVEI)
            select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPEVEI = NVL(_Link_.TETIPEVE,space(10))
      this.w_DESCRII = NVL(_Link_.TEDESCRI,space(50))
      this.w_TIPDIR1 = NVL(_Link_.TETIPDIR,space(10))
      this.w_DRIVER1 = NVL(_Link_.TEDRIVER,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_TIPEVEI = space(10)
      endif
      this.w_DESCRII = space(50)
      this.w_TIPDIR1 = space(10)
      this.w_DRIVER1 = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPDIR1='E'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TIPEVEI = space(10)
        this.w_DESCRII = space(50)
        this.w_TIPDIR1 = space(10)
        this.w_DRIVER1 = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPEVEI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPEVEO
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPEVEO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSFA_ATE',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_TIPEVEO)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_TIPEVEO))
          select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPEVEO)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPEVEO) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oTIPEVEO_1_26'),i_cWhere,'GSFA_ATE',"Tipi eventi",'gsfa2aib.TIPEVENT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPEVEO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_TIPEVEO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_TIPEVEO)
            select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPEVEO = NVL(_Link_.TETIPEVE,space(10))
      this.w_DESCRIO = NVL(_Link_.TEDESCRI,space(50))
      this.w_TIPDIR2 = NVL(_Link_.TETIPDIR,space(10))
      this.w_DRIVER2 = NVL(_Link_.TEDRIVER,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_TIPEVEO = space(10)
      endif
      this.w_DESCRIO = space(50)
      this.w_TIPDIR2 = space(10)
      this.w_DRIVER2 = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPDIR2='U'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_TIPEVEO = space(10)
        this.w_DESCRIO = space(50)
        this.w_TIPDIR2 = space(10)
        this.w_DRIVER2 = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPEVEO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DRIVER1
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_lTable = "DRVEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2], .t., this.DRVEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRIVER1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRIVER1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DEDRIVER,DE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where DEDRIVER="+cp_ToStrODBC(this.w_DRIVER1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DEDRIVER',this.w_DRIVER1)
            select DEDRIVER,DE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRIVER1 = NVL(_Link_.DEDRIVER,space(10))
      this.w_TIPODRV1 = NVL(_Link_.DE__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DRIVER1 = space(10)
      endif
      this.w_TIPODRV1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])+'\'+cp_ToStr(_Link_.DEDRIVER,1)
      cp_ShowWarn(i_cKey,this.DRVEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRIVER1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DRIVER2
  func Link_1_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_lTable = "DRVEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2], .t., this.DRVEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRIVER2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRIVER2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DEDRIVER,DE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where DEDRIVER="+cp_ToStrODBC(this.w_DRIVER2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DEDRIVER',this.w_DRIVER2)
            select DEDRIVER,DE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRIVER2 = NVL(_Link_.DEDRIVER,space(10))
      this.w_TIPODRV2 = NVL(_Link_.DE__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DRIVER2 = space(10)
      endif
      this.w_TIPODRV2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])+'\'+cp_ToStr(_Link_.DEDRIVER,1)
      cp_ShowWarn(i_cKey,this.DRVEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRIVER2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oUTCODICE_1_1.value==this.w_UTCODICE)
      this.oPgFrm.Page1.oPag.oUTCODICE_1_1.value=this.w_UTCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oUTDESUTE_1_2.value==this.w_UTDESUTE)
      this.oPgFrm.Page1.oPag.oUTDESUTE_1_2.value=this.w_UTDESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oTELEFO_1_11.value==this.w_TELEFO)
      this.oPgFrm.Page1.oPag.oTELEFO_1_11.value=this.w_TELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oTELEF2_1_13.value==this.w_TELEF2)
      this.oPgFrm.Page1.oPag.oTELEF2_1_13.value=this.w_TELEF2
    endif
    if not(this.oPgFrm.Page1.oPag.oSELUTPE_1_14.RadioValue()==this.w_SELUTPE)
      this.oPgFrm.Page1.oPag.oSELUTPE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPHDTIM_1_20.value==this.w_PHDTIM)
      this.oPgFrm.Page1.oPag.oPHDTIM_1_20.value=this.w_PHDTIM
    endif
    if not(this.oPgFrm.Page1.oPag.oPHDTIMFI_1_21.value==this.w_PHDTIMFI)
      this.oPgFrm.Page1.oPag.oPHDTIMFI_1_21.value=this.w_PHDTIMFI
    endif
    if not(this.oPgFrm.Page1.oPag.oSELTRA_1_22.RadioValue()==this.w_SELTRA)
      this.oPgFrm.Page1.oPag.oSELTRA_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELSNM_1_23.RadioValue()==this.w_SELSNM)
      this.oPgFrm.Page1.oPag.oSELSNM_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIRCHI_1_24.RadioValue()==this.w_DIRCHI)
      this.oPgFrm.Page1.oPag.oDIRCHI_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPEVEI_1_25.value==this.w_TIPEVEI)
      this.oPgFrm.Page1.oPag.oTIPEVEI_1_25.value=this.w_TIPEVEI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPEVEO_1_26.value==this.w_TIPEVEO)
      this.oPgFrm.Page1.oPag.oTIPEVEO_1_26.value=this.w_TIPEVEO
    endif
    if not(this.oPgFrm.Page1.oPag.oORE_1_34.value==this.w_ORE)
      this.oPgFrm.Page1.oPag.oORE_1_34.value=this.w_ORE
    endif
    if not(this.oPgFrm.Page1.oPag.oMINUTI_1_35.value==this.w_MINUTI)
      this.oPgFrm.Page1.oPag.oMINUTI_1_35.value=this.w_MINUTI
    endif
    if not(this.oPgFrm.Page1.oPag.oSECONDI_1_36.value==this.w_SECONDI)
      this.oPgFrm.Page1.oPag.oSECONDI_1_36.value=this.w_SECONDI
    endif
    if not(this.oPgFrm.Page2.oPag.oMsg_2_1.value==this.w_Msg)
      this.oPgFrm.Page2.oPag.oMsg_2_1.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRII_1_45.value==this.w_DESCRII)
      this.oPgFrm.Page1.oPag.oDESCRII_1_45.value=this.w_DESCRII
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIO_1_46.value==this.w_DESCRIO)
      this.oPgFrm.Page1.oPag.oDESCRIO_1_46.value=this.w_DESCRIO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CHKPHONE(.w_TELEFO, .w_SELUTPE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTELEFO_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKPHONE(.w_TELEF2, .w_SELUTPE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTELEF2_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_TIPEVEI)) or not(.w_TIPDIR1='E'))  and not(.w_DIRCHI='O')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPEVEI_1_25.SetFocus()
            i_bnoObbl = !empty(.w_TIPEVEI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_TIPEVEO)) or not(.w_TIPDIR2='U'))  and not(.w_DIRCHI='I')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPEVEO_1_26.SetFocus()
            i_bnoObbl = !empty(.w_TIPEVEO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_UTCODICE = this.w_UTCODICE
    this.o_UTTELSUP = this.w_UTTELSUP
    this.o_SELUTPE = this.w_SELUTPE
    this.o_TIPEVEI = this.w_TIPEVEI
    this.o_TIPEVEO = this.w_TIPEVEO
    return

enddefine

* --- Define pages as container
define class tgsfa_kicPag1 as StdContainer
  Width  = 606
  height = 430
  stdWidth  = 606
  stdheight = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oUTCODICE_1_1 as StdField with uid="AKGASWYFDW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_UTCODICE", cQueryName = "UTCODICE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 229181323,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=59, Left=148, Top=8, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="UTE_NTI", oKey_1_1="UTCODICE", oKey_1_2="this.w_UTCODICE"

  func oUTCODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTCODICE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUTCODICE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UTE_NTI','*','UTCODICE',cp_AbsName(this.parent,'oUTCODICE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oUTDESUTE_1_2 as StdField with uid="SKUOCMAGUQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_UTDESUTE", cQueryName = "UTDESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 177149835,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=208, Top=8, InputMask=replicate('X',40)

  add object oTELEFO_1_11 as StdField with uid="VCHVHYOQMD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_TELEFO", cQueryName = "TELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di telefono",;
    HelpContextID = 205551562,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=148, Top=37, InputMask=replicate('X',18), bHasZoom = .t. 

  func oTELEFO_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKPHONE(.w_TELEFO, .w_SELUTPE))
    endwith
    return bRes
  endfunc

  proc oTELEFO_1_11.mZoom
    vx_exec("..\agen\exe\query\gsfa1kic.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oTELEF2_1_13 as StdField with uid="IHIBGJETBF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_TELEF2", cQueryName = "TELEF2",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Altro telefono",;
    HelpContextID = 155219914,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=148, Top=61, InputMask=replicate('X',18), bHasZoom = .t. 

  func oTELEF2_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKPHONE(.w_TELEF2, .w_SELUTPE))
    endwith
    return bRes
  endfunc

  proc oTELEF2_1_13.mZoom
    vx_exec("..\agen\exe\query\gsfa2kic.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSELUTPE_1_14 as StdRadio with uid="ENLNGGVYWP",rtseq=12,rtrep=.f.,left=363, top=37, width=166,height=40;
    , ToolTipText = "Importa dati con riferimento agli utenti/persone";
    , cFormVar="w_SELUTPE", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELUTPE_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Importa da utenti"
      this.Buttons(1).HelpContextID = 95389734
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Importa da persone"
      this.Buttons(2).HelpContextID = 95389734
      this.Buttons(2).Top=19
      this.SetAll("Width",164)
      this.SetAll("Height",21)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Importa dati con riferimento agli utenti/persone")
      StdRadio::init()
    endproc

  func oSELUTPE_1_14.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oSELUTPE_1_14.GetRadio()
    this.Parent.oContained.w_SELUTPE = this.RadioValue()
    return .t.
  endfunc

  func oSELUTPE_1_14.SetRadio()
    this.Parent.oContained.w_SELUTPE=trim(this.Parent.oContained.w_SELUTPE)
    this.value = ;
      iif(this.Parent.oContained.w_SELUTPE=='U',1,;
      iif(this.Parent.oContained.w_SELUTPE=='P',2,;
      0))
  endfunc


  add object oBtn_1_16 as StdButton with uid="TWWCKBNSBI",left=552, top=55, width=48,height=45,;
    CpPicture="bmp\param.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai parametri connessione";
    , HelpContextID = 206703352;
    , tabstop=.f., Caption='\<Parametri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      do GSFA_KIB with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPHDTIM_1_20 as StdField with uid="DOUFIWZOCY",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PHDTIM", cQueryName = "PHDTIM",;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "Fine data importazione",;
    HelpContextID = 235009290,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=148, Top=115

  add object oPHDTIMFI_1_21 as StdField with uid="CPNBEPYWEX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PHDTIMFI", cQueryName = "PHDTIMFI",;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "Fine data importazione",;
    HelpContextID = 33426239,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=148, Top=142

  add object oSELTRA_1_22 as StdRadio with uid="ADDUBJRACG",rtseq=18,rtrep=.f.,left=148, top=171, width=134,height=43;
    , ToolTipText = "Includi trasferite";
    , cFormVar="w_SELTRA", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELTRA_1_22.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Includi trasferite"
      this.Buttons(1).HelpContextID = 158431194
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Escludi trasferite"
      this.Buttons(2).HelpContextID = 158431194
      this.Buttons(2).Top=20
      this.SetAll("Width",132)
      this.SetAll("Height",22)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Includi trasferite")
      StdRadio::init()
    endproc

  func oSELTRA_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSELTRA_1_22.GetRadio()
    this.Parent.oContained.w_SELTRA = this.RadioValue()
    return .t.
  endfunc

  func oSELTRA_1_22.SetRadio()
    this.Parent.oContained.w_SELTRA=trim(this.Parent.oContained.w_SELTRA)
    this.value = ;
      iif(this.Parent.oContained.w_SELTRA=='S',1,;
      iif(this.Parent.oContained.w_SELTRA=='N',2,;
      0))
  endfunc

  add object oSELSNM_1_23 as StdRadio with uid="YEVETINOBE",rtseq=19,rtrep=.f.,left=363, top=171, width=166,height=40;
    , ToolTipText = "Includi senza numero";
    , cFormVar="w_SELSNM", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELSNM_1_23.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Includi senza numero"
      this.Buttons(1).HelpContextID = 229799898
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Escludi senza numero"
      this.Buttons(2).HelpContextID = 229799898
      this.Buttons(2).Top=19
      this.SetAll("Width",164)
      this.SetAll("Height",21)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Includi senza numero")
      StdRadio::init()
    endproc

  func oSELSNM_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSELSNM_1_23.GetRadio()
    this.Parent.oContained.w_SELSNM = this.RadioValue()
    return .t.
  endfunc

  func oSELSNM_1_23.SetRadio()
    this.Parent.oContained.w_SELSNM=trim(this.Parent.oContained.w_SELSNM)
    this.value = ;
      iif(this.Parent.oContained.w_SELSNM=='S',1,;
      iif(this.Parent.oContained.w_SELSNM=='N',2,;
      0))
  endfunc

  add object oDIRCHI_1_24 as StdRadio with uid="PNPSLFEGBV",rtseq=20,rtrep=.f.,left=148, top=217, width=177,height=54;
    , ToolTipText = "Direzione chiamate (ingresso, uscita o tutte)";
    , cFormVar="w_DIRCHI", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDIRCHI_1_24.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo chiamate in ingresso"
      this.Buttons(1).HelpContextID = 35787978
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo chiamate in uscita"
      this.Buttons(2).HelpContextID = 35787978
      this.Buttons(2).Top=17
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 35787978
      this.Buttons(3).Top=34
      this.SetAll("Width",175)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Direzione chiamate (ingresso, uscita o tutte)")
      StdRadio::init()
    endproc

  func oDIRCHI_1_24.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'O',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oDIRCHI_1_24.GetRadio()
    this.Parent.oContained.w_DIRCHI = this.RadioValue()
    return .t.
  endfunc

  func oDIRCHI_1_24.SetRadio()
    this.Parent.oContained.w_DIRCHI=trim(this.Parent.oContained.w_DIRCHI)
    this.value = ;
      iif(this.Parent.oContained.w_DIRCHI=='I',1,;
      iif(this.Parent.oContained.w_DIRCHI=='O',2,;
      iif(this.Parent.oContained.w_DIRCHI=='T',3,;
      0)))
  endfunc

  add object oTIPEVEI_1_25 as StdField with uid="JIVLZZHJZV",rtseq=21,rtrep=.f.,;
    cFormVar = "w_TIPEVEI", cQueryName = "TIPEVEI",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Tipo evento telefonata in ingresso",;
    HelpContextID = 180341814,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=148, Top=275, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", cZoomOnZoom="GSFA_ATE", oKey_1_1="TETIPEVE", oKey_1_2="this.w_TIPEVEI"

  func oTIPEVEI_1_25.mHide()
    with this.Parent.oContained
      return (.w_DIRCHI='O')
    endwith
  endfunc

  func oTIPEVEI_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPEVEI_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPEVEI_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oTIPEVEI_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSFA_ATE',"Tipi eventi",'gsfa1aib.TIPEVENT_VZM',this.parent.oContained
  endproc
  proc oTIPEVEI_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSFA_ATE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TETIPEVE=this.parent.oContained.w_TIPEVEI
     i_obj.ecpSave()
  endproc

  add object oTIPEVEO_1_26 as StdField with uid="VOHSZWDGRX",rtseq=22,rtrep=.f.,;
    cFormVar = "w_TIPEVEO", cQueryName = "TIPEVEO",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Tipo evento telefonata in uscita",;
    HelpContextID = 88093642,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=148, Top=299, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", cZoomOnZoom="GSFA_ATE", oKey_1_1="TETIPEVE", oKey_1_2="this.w_TIPEVEO"

  func oTIPEVEO_1_26.mHide()
    with this.Parent.oContained
      return (.w_DIRCHI='I')
    endwith
  endfunc

  func oTIPEVEO_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPEVEO_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPEVEO_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oTIPEVEO_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSFA_ATE',"Tipi eventi",'gsfa2aib.TIPEVENT_VZM',this.parent.oContained
  endproc
  proc oTIPEVEO_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSFA_ATE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TETIPEVE=this.parent.oContained.w_TIPEVEO
     i_obj.ecpSave()
  endproc

  add object oORE_1_34 as StdField with uid="VTRFKZJMDU",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ORE", cQueryName = "ORE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ore",;
    HelpContextID = 266722074,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=363, Top=351, cSayPict='"99"', cGetPict='"99"'

  add object oMINUTI_1_35 as StdField with uid="XHSVXIKGHQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MINUTI", cQueryName = "MINUTI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minuti",;
    HelpContextID = 22041658,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=418, Top=351, cSayPict='"99"', cGetPict='"99"'

  add object oSECONDI_1_36 as StdField with uid="LHHWFGSOUU",rtseq=25,rtrep=.f.,;
    cFormVar = "w_SECONDI", cQueryName = "SECONDI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero secondi",;
    HelpContextID = 155777062,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=473, Top=351, cSayPict='"99"', cGetPict='"99"'


  add object oBtn_1_38 as StdButton with uid="ERHOXVEEGI",left=499, top=384, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 266998298;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        GSFA_BIC(this.Parent.oContained,"ESEGUI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_39 as StdButton with uid="TKCMXZXILU",left=552, top=384, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 259709626;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRII_1_45 as StdField with uid="QFDELOBYZU",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCRII", cQueryName = "DESCRII",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 243136310,;
   bGlobalFont=.t.,;
    Height=21, Width=342, Left=258, Top=275, InputMask=replicate('X',50)

  func oDESCRII_1_45.mHide()
    with this.Parent.oContained
      return (.w_DIRCHI='O')
    endwith
  endfunc

  add object oDESCRIO_1_46 as StdField with uid="AEOKZUYYRB",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCRIO", cQueryName = "DESCRIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 25299146,;
   bGlobalFont=.t.,;
    Height=21, Width=342, Left=258, Top=299, InputMask=replicate('X',50)

  func oDESCRIO_1_46.mHide()
    with this.Parent.oContained
      return (.w_DIRCHI='I')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="ALKPFOLIJM",Visible=.t., Left=87, Top=8,;
    Alignment=1, Width=56, Height=15,;
    Caption="Utente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="JMIUBMIJGZ",Visible=.t., Left=63, Top=37,;
    Alignment=1, Width=80, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="JJJYHOOGRG",Visible=.t., Left=51, Top=61,;
    Alignment=1, Width=92, Height=18,;
    Caption="Altro telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="JVCEBAQSSJ",Visible=.t., Left=17, Top=92,;
    Alignment=0, Width=462, Height=18,;
    Caption="Parametri importazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_29 as StdString with uid="GZTPCYGXPN",Visible=.t., Left=93, Top=351,;
    Alignment=1, Width=259, Height=18,;
    Caption="Considera telefonate con durata superiore a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="QOJSXUIZTZ",Visible=.t., Left=363, Top=332,;
    Alignment=2, Width=34, Height=18,;
    Caption="Ore"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="VUEIBKOVNP",Visible=.t., Left=418, Top=332,;
    Alignment=2, Width=34, Height=18,;
    Caption="Minuti"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="IUXXTWSWOU",Visible=.t., Left=464, Top=332,;
    Alignment=2, Width=54, Height=18,;
    Caption="Secondi"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="NDLKSIYHDN",Visible=.t., Left=17, Top=144,;
    Alignment=1, Width=126, Height=18,;
    Caption="Data fine importazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="FECNISRNTS",Visible=.t., Left=9, Top=117,;
    Alignment=1, Width=134, Height=18,;
    Caption="Data inizio importazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="WQMJRTZUOS",Visible=.t., Left=36, Top=275,;
    Alignment=1, Width=107, Height=18,;
    Caption="Evento ingresso:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_DIRCHI='O')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="BQRFWEPPIC",Visible=.t., Left=36, Top=299,;
    Alignment=1, Width=107, Height=18,;
    Caption="Evento uscita:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_DIRCHI='I')
    endwith
  endfunc

  add object oBox_1_27 as StdBox with uid="FYLSTKYOGP",left=10, top=108, width=592,height=1

  add object oBox_1_30 as StdBox with uid="AMYNIYGZRA",left=13, top=325, width=587,height=54
enddefine
define class tgsfa_kicPag2 as StdContainer
  Width  = 606
  height = 430
  stdWidth  = 606
  stdheight = 430
  resizeXpos=361
  resizeYpos=345
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMsg_2_1 as StdMemo with uid="ATBBCHAJSX",rtseq=28,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 266574394,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=374, Width=603, Left=1, Top=6, tabstop = .f., readonly = .t.


  add object oBtn_2_2 as StdButton with uid="RWDSSEOVZU",left=556, top=385, width=48,height=45,;
    CpPicture="BMP\save.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per salvare il file con il log elaborazione";
    , HelpContextID = 92354326;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      with this.Parent.oContained
        GSFA_BIC(this.Parent.oContained,"SALVA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_kic','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsfa_kic
proc ChkEVENTO(obj, pParam)
  if pParam='I'
    i_bRes=obj.w_TIPODRV1='T'
  else
    i_bRes=obj.w_TIPODRV2='T'
  endif
  if not(i_bRes)
    do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
    if pParam='I'
      obj.w_TIPEVEI = space(10)
      obj.w_DESCRII = space(50)
      obj.w_TIPDIR1 = space(10)
      obj.w_DRIVER1 = space(10)  
      obj.w_TIPODRV1 = space(1)
    else
      obj.w_TIPEVEO = space(10)
      obj.w_DESCRIO = space(50)
      obj.w_TIPDIR2 = space(10)
      obj.w_DRIVER2 = space(10)  
      obj.w_TIPODRV2 = space(1)
    endif    
  endif
endproc
* --- Fine Area Manuale
