* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bar                                                        *
*              Generazione attivit� ricorrenti                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-12-17                                                      *
* Last revis.: 2015-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pDatini,pDatFin,pMsgInv
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bar",oParentObject,m.pDatini,m.pDatFin,m.pMsgInv)
return(i_retval)

define class tgsag_bar as StdBatch
  * --- Local variables
  pDatini = ctot("")
  pDatFin = ctot("")
  pMsgInv = space(1)
  w_DECTOT = 0
  w_DECUNI = 0
  w_OBJ = .NULL.
  w_ATSERIAL = space(20)
  w_ATCODUID = space(20)
  w_ATCAITER = space(20)
  w_SERIAL = space(20)
  w_ANNO = space(4)
  w_SERIALE = space(20)
  w_ATSERIALE = space(10)
  w_InviaMail = .f.
  w_RIGA = 0
  w_GESTATT = .NULL.
  w_GSAG_MDA = .NULL.
  w_OCC = 0
  w_RESULT = space(100)
  * --- WorkFile variables
  OFF_PART_idx=0
  OFFDATTI_idx=0
  COM_ATTI_idx=0
  OFF_ATTI_idx=0
  ASS_ATEV_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- S si a inviare gli avvisi
    *     N no a inviare avvisi
    *     H non passato 
    if this.oParentObject.w_TipRic <> "N"
      this.oParentObject.w_MD_MONTH = BITOR(this.oParentObject.w_gen, this.oParentObject.w_feb, this.oParentObject.w_marz, this.oParentObject.w_apr, this.oParentObject.w_mag, this.oParentObject.w_giu, this.oParentObject.w_lug, this.oParentObject.w_ago, this.oParentObject.w_set, this.oParentObject.w_ott, this.oParentObject.w_nov, this.oParentObject.w_dic)
      this.oParentObject.w_MD__WEEK = BITOR(this.oParentObject.w_lun, this.oParentObject.w_mar, this.oParentObject.w_mer, this.oParentObject.w_gio, this.oParentObject.w_ven, this.oParentObject.w_sab, this.oParentObject.w_dom)
    endif
    this.w_OBJ = CREATEOBJECT("ActivityWriter",this,"OFF_ATTI")
    * --- This.oParentObject.oParentObject � sempre GSAG_AAT, anche se si proviene da GSAG_BAD
    this.w_OBJ.SetObjectValues(This.Oparentobject.oparentobject)     
    this.w_ATSERIAL = this.w_OBJ.w_ATSERIAL
    this.w_ATCODUID = this.w_OBJ.w_ATCODUID
    this.w_ATCAITER = this.w_OBJ.w_ATCAITER
    this.w_OBJ.w_ATINIRIC = ctod("  /  /  ")
    this.w_OBJ.w_ATFINRIC = ctod("  /  /  ")
    this.w_OBJ.w_ATINICOS = ctod("  /  /  ")
    this.w_OBJ.w_ATFINCOS = ctod("  /  /  ")
    if Vartype(this.pMsgInv)="L"
      this.pMsgInv = "H"
    else
      this.w_InviaMail = IIF(this.pMsgInv="S",.T.,.F.)
    endif
    if this.w_OBJ.w_ATFLNOTI <>"N" and this.pMsgInv="H"
      this.w_InviaMail = Ah_YesNo("Attenzione: inviare gli avvisi per ogni nuova occorrenza dell'attivit�?")
    endif
    * --- Select from OFF_PART
    i_nConn=i_TableProp[this.OFF_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2],.t.,this.OFF_PART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" OFF_PART ";
          +" where PASERIAL="+cp_ToStrODBC(this.w_ATSERIAL)+"";
           ,"_Curs_OFF_PART")
    else
      select * from (i_cTable);
       where PASERIAL=this.w_ATSERIAL;
        into cursor _Curs_OFF_PART
    endif
    if used('_Curs_OFF_PART')
      select _Curs_OFF_PART
      locate for 1=1
      do while not(eof())
      this.w_OBJ.AddPart(0,0,_Curs_OFF_PART.PATIPRIS,_Curs_OFF_PART.PACODRIS,_Curs_OFF_PART.PAGRURIS)     
      this.w_OBJ.w_OFF_PART.SaveCurrentRecord()     
        select _Curs_OFF_PART
        continue
      enddo
      use
    endif
    this.w_GESTATT = This.Oparentobject.oparentobject
    this.w_GSAG_MDA = This.Oparentobject.oparentobject.GSAG_MDA
    * --- Select from OFFDATTI
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2],.t.,this.OFFDATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" OFFDATTI ";
          +" where DASERIAL="+cp_ToStrODBC(this.w_ATSERIAL)+"";
           ,"_Curs_OFFDATTI")
    else
      select * from (i_cTable);
       where DASERIAL=this.w_ATSERIAL;
        into cursor _Curs_OFFDATTI
    endif
    if used('_Curs_OFFDATTI')
      select _Curs_OFFDATTI
      locate for 1=1
      do while not(eof())
      this.w_RIGA = _Curs_OFFDATTI.CPROWNUM
      this.w_RIGA = this.w_GSAG_MDA.search("CPROWNUM= "+cp_tostr( this.w_RIGA ) +" And Not Deleted() ")
      this.w_GSAG_MDA.SetRow(this.w_RIGA)     
      this.w_GSAG_MDA.WorkFromTrs()     
      this.w_OBJ.AddDetail(0,0,_Curs_OFFDATTI.DACODRES,_Curs_OFFDATTI.DACODATT,_Curs_OFFDATTI.DADESATT,_Curs_OFFDATTI.DAUNIMIS,_Curs_OFFDATTI.DAQTAMOV,_Curs_OFFDATTI.DAPREZZO,"D",i_CODUTE,i_datsys,"","D","D")     
      this.w_OBJ.w_OFFDATTI.SetObjectValues(this.w_GSAG_MDA)     
      this.w_OBJ.w_OFFDATTI.DACONTRA = Space(10)
      this.w_OBJ.w_OFFDATTI.DACODMOD = Space(10)
      this.w_OBJ.w_OFFDATTI.DACODIMP = Space(10)
      this.w_OBJ.w_OFFDATTI.DACOCOMP = 0
      this.w_OBJ.w_OFFDATTI.DARINNOV = 0
      this.w_OBJ.w_OFFDATTI.DATCOINI = ctod("  /  /  ")
      this.w_OBJ.w_OFFDATTI.DATCOFIN = ctod("  /  /  ")
      this.w_OBJ.w_OFFDATTI.DAINICOM = ctod("  /  /  ")
      this.w_OBJ.w_OFFDATTI.DAFINCOM = ctod("  /  /  ")
      this.w_OBJ.w_OFFDATTI.DATRIINI = ctod("  /  /  ")
      this.w_OBJ.w_OFFDATTI.DATRIFIN = ctod("  /  /  ")
      this.w_OBJ.w_OFFDATTI.SaveCurrentRecord()     
        select _Curs_OFFDATTI
        continue
      enddo
      use
    endif
    if Not Isalt()
      * --- Select from COM_ATTI
      i_nConn=i_TableProp[this.COM_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COM_ATTI_idx,2],.t.,this.COM_ATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" COM_ATTI ";
            +" where CASERIAL="+cp_ToStrODBC(this.w_ATSERIAL)+"";
             ,"_Curs_COM_ATTI")
      else
        select * from (i_cTable);
         where CASERIAL=this.w_ATSERIAL;
          into cursor _Curs_COM_ATTI
      endif
      if used('_Curs_COM_ATTI')
        select _Curs_COM_ATTI
        locate for 1=1
        do while not(eof())
        this.w_OBJ.AddComp(0,NVL( _Curs_COM_ATTI.CACODIMP , SPACE(10)), NVL(_Curs_COM_ATTI.CACODCOM, 0 ))     
          select _Curs_COM_ATTI
          continue
        enddo
        use
      endif
    endif
    this.w_OCC = this.oParentObject.w_NUMOCC-1
    * --- Seriale dell'attivit� originale
    this.w_OBJ.w_ATRIFSER = this.w_ATSERIAL
    * --- Se l'attivit� di partenza era a sua volta una ricorrente, dobbiamo ricondurci a quella primaria
    if EMPTY(this.w_ATCAITER)
      this.w_OBJ.w_ATCAITER = this.w_ATSERIAL
      * --- Aggiorniamo l'attivit� di origine in modo che diventi la principale per le attivit� collegate generate
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATFLRICO ="+cp_NullLink(cp_ToStrODBC("S"),'OFF_ATTI','ATFLRICO');
        +",ATCAITER ="+cp_NullLink(cp_ToStrODBC(this.w_ATSERIAL),'OFF_ATTI','ATCAITER');
            +i_ccchkf ;
        +" where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
               )
      else
        update (i_cTable) set;
            ATFLRICO = "S";
            ,ATCAITER = this.w_ATSERIAL;
            &i_ccchkf. ;
         where;
            ATSERIAL = this.w_ATSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      this.w_OBJ.w_ATCAITER = this.w_ATCAITER
    endif
    do case
      case this.oParentObject.w_TIPRIC="D" and this.w_OCC>0
        do while this.w_OCC>0
          this.w_OBJ.w_ATDATINI = NEXTTIME2( this.w_obj.w_ATDATINI, this.oParentObject.w_MD__FREQ, this.oParentObject.w_MDNUMFRQ, this.oParentObject.w_MDDATUNI, this.oParentObject.w_MDIMPDAT, this.oParentObject.w_MD_GGFIS, HOUR(this.w_obj.w_ATDATINI) * 60 + MINUTE(this.w_obj.w_ATDATINI), HOUR(this.w_obj.w_ATDATFIN) * 60 + MINUTE(this.w_obj.w_ATDATFIN), this.oParentObject.w_MDMININT, this.oParentObject.w_MD__WEEK, this.oParentObject.w_MD_MONTH, this.oParentObject.w_MDESCLUS, this.oParentObject.w_MDCALEND, .t., .t.)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_OCC = this.w_OCC-1
        enddo
      case this.oParentObject.w_TIPRIC="E" and Not Empty(this.oParentObject.w_DATA_ENTRO)
        this.w_OBJ.w_ATDATINI = NEXTTIME2( this.w_obj.w_ATDATINI, this.oParentObject.w_MD__FREQ, this.oParentObject.w_MDNUMFRQ, this.oParentObject.w_MDDATUNI, this.oParentObject.w_MDIMPDAT, this.oParentObject.w_MD_GGFIS, HOUR(this.w_obj.w_ATDATINI) * 60 + MINUTE(this.w_obj.w_ATDATINI), HOUR(this.w_obj.w_ATDATFIN) * 60 + MINUTE(this.w_obj.w_ATDATFIN), this.oParentObject.w_MDMININT, this.oParentObject.w_MD__WEEK, this.oParentObject.w_MD_MONTH, this.oParentObject.w_MDESCLUS, this.oParentObject.w_MDCALEND, .t., .t.)
        do while this.w_OBJ.w_ATDATINI<=this.oParentObject.w_DATA_ENTRO
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_OBJ.w_ATDATINI = NEXTTIME2( this.w_obj.w_ATDATINI, this.oParentObject.w_MD__FREQ, this.oParentObject.w_MDNUMFRQ, this.oParentObject.w_MDDATUNI, this.oParentObject.w_MDIMPDAT, this.oParentObject.w_MD_GGFIS, HOUR(this.w_obj.w_ATDATINI) * 60 + MINUTE(this.w_obj.w_ATDATINI), HOUR(this.w_obj.w_ATDATFIN) * 60 + MINUTE(this.w_obj.w_ATDATFIN), this.oParentObject.w_MDMININT, this.oParentObject.w_MD__WEEK, this.oParentObject.w_MD_MONTH, this.oParentObject.w_MDESCLUS, this.oParentObject.w_MDCALEND, .t., .t.)
        enddo
      otherwise
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    this.w_GESTATT.w_ATCAITER = this.w_OBJ.w_ATCAITER
    this.w_GESTATT.mHideControls()     
    this.w_GESTATT = .NULL.
    this.w_OBJ = .NULL.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OBJ.w_ATFLRICO = "S"
    this.w_OBJ.w_ATDATDOC = cp_Chartodate("  -  -    ")
    this.w_OBJ.w_ATPROMEM = cp_Chartodate("  -  -    ")
    if this.oParentObject.w_TipRic = "N"
      this.w_OBJ.w_ATDATINI = this.pDATINI
      this.w_OBJ.w_ATDATFIN = this.pDATFIN
      this.w_OBJ.w_ATRIFSER = this.oParentObject.w_RIFSER
    else
      this.w_OBJ.w_ATDATFIN = cp_CharToDatetime( DTOC(TTOD(this.w_OBJ.w_ATDATINI))+" "+this.w_OBJ.w_ORAFIN+":"+this.w_OBJ.w_MINFIN+":00")
    endif
    this.w_OBJ.w_ATSERIAL = Space(20)
    this.w_OBJ.w_ATNUMDOC = Space(15)
    this.w_OBJ.w_ATCODUID = Space(254)
    this.w_OBJ.bSendMSG = .f.
    this.w_RESULT = this.w_OBJ.CreateActivity()
    if NOT ISALT()
      this.w_SERIALE = this.w_OBJ.w_ATSERIAL
      this.w_ATSERIALE = this.w_OBJ.w_ATSEREVE
      this.w_ANNO = this.w_OBJ.w_ATEVANNO
      if not empty(this.w_ATSERIALE)
        * --- Try
        local bErr_04852C60
        bErr_04852C60=bTrsErr
        this.Try_04852C60()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04852C60
        * --- End
      endif
    endif
    * --- Seriale dell'attivit� precedente nella catena
    if this.w_InviaMail
      * --- Il terzo parametro serve per impedire il messaggio "Si desidera inviare gli avvisi?"
      do GSAG_BAP with this.w_OBJ,"C", this.w_OBJ.w_ATSERIAL,.T.
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_TipRic = "N"
      this.oParentObject.w_RIFSER = this.w_OBJ.w_ATSERIAL
    else
      this.w_OBJ.w_ATRIFSER = this.w_OBJ.w_ATSERIAL
    endif
  endproc
  proc Try_04852C60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ASS_ATEV
    i_nConn=i_TableProp[this.ASS_ATEV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATEV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ASS_ATEV_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AE__ANNO"+",AECODATT"+",AECODEVE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ANNO),'ASS_ATEV','AE__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'ASS_ATEV','AECODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATSERIALE),'ASS_ATEV','AECODEVE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AE__ANNO',this.w_ANNO,'AECODATT',this.w_SERIALE,'AECODEVE',this.w_ATSERIALE)
      insert into (i_cTable) (AE__ANNO,AECODATT,AECODEVE &i_ccchkf. );
         values (;
           this.w_ANNO;
           ,this.w_SERIALE;
           ,this.w_ATSERIALE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pDatini,pDatFin,pMsgInv)
    this.pDatini=pDatini
    this.pDatFin=pDatFin
    this.pMsgInv=pMsgInv
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='OFF_PART'
    this.cWorkTables[2]='OFFDATTI'
    this.cWorkTables[3]='COM_ATTI'
    this.cWorkTables[4]='OFF_ATTI'
    this.cWorkTables[5]='ASS_ATEV'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_OFF_PART')
      use in _Curs_OFF_PART
    endif
    if used('_Curs_OFFDATTI')
      use in _Curs_OFFDATTI
    endif
    if used('_Curs_COM_ATTI')
      use in _Curs_COM_ATTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pDatini,pDatFin,pMsgInv"
endproc
