* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bre                                                        *
*              Resoconto giornaliero                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-04                                                      *
* Last revis.: 2014-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipoImport,pDataIni,pDataFin,pCodPer,pStatoAtt,pFlSingPre
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bre",oParentObject,m.pTipoImport,m.pDataIni,m.pDataFin,m.pCodPer,m.pStatoAtt,m.pFlSingPre)
return(i_retval)

define class tgsag_bre as StdBatch
  * --- Local variables
  pTipoImport = space(1)
  pDataIni = ctod("  /  /  ")
  pDataFin = ctod("  /  /  ")
  pCodPer = space(5)
  pStatoAtt = space(1)
  pFlSingPre = space(1)
  w_RGCODPER = space(5)
  w_DATA_GIORNO = ctod("  /  /  ")
  w_RG__DATA = ctod("  /  /  ")
  w_ObjMRG = .NULL.
  w_COGNOME = space(40)
  w_NOME = space(40)
  w_CARAGGST = space(1)
  w_TMPMIN = 0
  w_CodPersona = space(5)
  w_OkImpMas = .f.
  w_NUMRIGA = 0
  w_ATCAUATT = space(20)
  w_ATOGGETT = space(254)
  w_ATSERIAL = space(20)
  w_CAUPRE = space(20)
  w_NomeFileLog = space(10)
  w_HANDLE = 0
  w_InsAttNelGiorno = .f.
  w_InsAttivit� = .f.
  TotSecondi = 0
  Minuti = 0
  Ore = 0
  Data = ctod("  /  /  ")
  w_OreEffet = 0
  w_MinEffet = 0
  w_SerialAt = space(20)
  * --- WorkFile variables
  CAUMATTI_idx=0
  RIL_GIOR_idx=0
  PAR_AGEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                                    Caricamento rilevazione giornaliera del tempo
    * --- pTipoImport = 'S' : impotazione singolo giorno
    *     pTipoImport = 'M' : importazione massiva (intervallo di date)
    * --- Data iniziale
    * --- Data finale
    * --- Codice persona
    * --- Stato attivit�  (K: tutte  -  B: evase o completate)
    * --- Check Inserisci una riga per ogni prestazione
    * --- Lasciata tale variabile per non modificare le query
    this.w_RGCODPER = this.pCodPer
    this.w_DATA_GIORNO = this.pDataIni
    * --- Se import massivo (da Caricamento automatico resoconto giornaliero)
    if this.pTipoImport = "M"
      if EMPTY(this.pDataIni) AND EMPTY(this.pDataFin)
        AH_ErrorMsg("Selezionare la data iniziale e quella finale")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.pDataIni)
        AH_ErrorMsg("Selezionare la data iniziale")
        i_retcode = 'stop'
        return
      endif
      if EMPTY(this.pDataFin)
        AH_ErrorMsg("Selezionare la data finale")
        i_retcode = 'stop'
        return
      endif
      this.w_InsAttivit� = .F.
      this.w_NomeFileLog = ADDBS(Tempadhoc())+"LogResocGiornalieroUt"+ALLTRIM(STR(i_CodUte))+".Txt"
      * --- Creazione log errori
      this.w_HANDLE = FCREATE(this.w_NomeFileLog)
      =fputs(this.w_Handle, Ah_MsgFormat("Caricamento automatico resoconto giornaliero"))
      =fputs(this.w_Handle,"")
    endif
    * --- Se importazione singolo giorno da Resoconto giornaliero
    if this.pTipoImport = "S"
      this.w_ObjMRG = this.oParentObject
    endif
    * --- Se attivo il check Inserisci una riga per ogni prestazione
    if this.pFlSingPre = "S"
      * --- Legge il tipo attivit� usata per l'inserimento definitivo delle prestazioni provvisorie
      * --- Read from PAR_AGEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PACAUPRE"+;
          " from "+i_cTable+" PAR_AGEN where ";
              +"PACODAZI = "+cp_ToStrODBC(i_codazi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PACAUPRE;
          from (i_cTable) where;
              PACODAZI = i_codazi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAUPRE = NVL(cp_ToDate(_read_.PACAUPRE),cp_NullValue(_read_.PACAUPRE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Ciclo sui giorni compresi nell'intervallo (nel caso di Resoconto giornaliero l'intervallo � ridotto ad un solo giorno)
    do while this.w_DATA_GIORNO <= this.pDataFin
      * --- Data del giorno (Lasciata la var. w_RG__DATA per non modificare le query)
      this.w_RG__DATA = this.w_DATA_GIORNO
      * --- Se import massivo (da Caricamento automatico resoconto giornaliero)
      if this.pTipoImport = "M"
        this.w_NUMRIGA = 0
        this.w_OkImpMas = .F.
        this.w_InsAttNelGiorno = .F.
        * --- Legge se esiste almeno una riga (attivit�) relativa alla persona ed al giorno in esame
        * --- Read from RIL_GIOR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.RIL_GIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIL_GIOR_idx,2],.t.,this.RIL_GIOR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "RGCODPER"+;
            " from "+i_cTable+" RIL_GIOR where ";
                +"RGCODPER = "+cp_ToStrODBC(this.pCodPer);
                +" and RG__DATA = "+cp_ToStrODBC(this.w_RG__DATA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            RGCODPER;
            from (i_cTable) where;
                RGCODPER = this.pCodPer;
                and RG__DATA = this.w_RG__DATA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CodPersona = NVL(cp_ToDate(_read_.RGCODPER),cp_NullValue(_read_.RGCODPER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Se non esiste alcuna riga
        if EMPTY(this.w_CodPersona)
          this.w_OkImpMas = .T.
        else
          =fputs(this.w_Handle, Ah_MsgFormat("Data: ")+dtoc(this.w_RG__DATA)+Ah_MsgFormat("  Persona: ")+this.pCodPer+space(1)+alltrim(this.oParentObject.w_DESCRIZ)+Ah_MsgFormat(" (Resoconto giornaliero gi� presente!)"))
          this.w_InsAttivit� = .T.
        endif
      endif
      * --- Ciclo sulle attivit� 
      VQ_EXEC("..\AGEN\EXE\QUERY\GSAG_MRG",this,"TEMP")
      SELECT * FROM TEMP WHERE CP_TODATE(ATDATINI)<=this.w_RG__DATA AND CP_TODATE(ATDATFIN)>=this.w_RG__DATA INTO CURSOR TEMP
      SELECT TEMP 
 GO TOP 
 SCAN
      * --- IMPORT SINGOLO GIORNO (da Resoconto giornaliero)
      if this.pTipoImport = "S"
        * --- Se nel temporaneo non � gi� presente il seriale dell'attivit� in esame
        if this.w_ObjMRG.Search("NVL(t_RGATTSER, SPACE(20))= " + cp_ToStrODBC(NVL(ATSERIAL, SPACE(20))) + " AND NOT DELETED()") = -1
          * --- Se attivo il check Inserisci una riga per ogni prestazione e se l'attivit� proviene dall'inserimento provvisorio
          if this.pFLSINGPRE="S" AND ATCAUATT=this.w_CAUPRE
            this.w_SerialAt = ATSERIAL
            * --- Cicla sulle prestazioni
            * --- Select from ..\AGEN\EXE\QUERY\GSAG2MRG
            do vq_exec with '..\AGEN\EXE\QUERY\GSAG2MRG',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG','',.f.,.t.
            if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG')
              select _Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG
              locate for 1=1
              do while not(eof())
              this.Pag4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
                select _Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG
                continue
              enddo
              use
            endif
          else
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      else
        * --- IMPORT MASSIVO (da Caricamento automatico resoconto giornaliero)
        * --- Se non  esiste alcuna attivit� relativa alla persona ed al giorno in esame
        if this.w_OkImpMas
          * --- Se attivo il check Inserisci una riga per ogni prestazione e se l'attivit� proviene dall'inserimento provvisorio
          if this.pFLSINGPRE="S" AND ATCAUATT=this.w_CAUPRE
            this.w_SerialAt = ATSERIAL
            * --- Cicla sulle prestazioni
            * --- Select from ..\AGEN\EXE\QUERY\GSAG2MRG
            do vq_exec with '..\AGEN\EXE\QUERY\GSAG2MRG',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG','',.f.,.t.
            if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG')
              select _Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG
              locate for 1=1
              do while not(eof())
              this.Pag5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
                select _Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG
                continue
              enddo
              use
            endif
          else
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
      ENDSCAN
      * --- Importazione singolo giorno da Resoconto giornaliero
      if this.pTipoImport = "S"
        this.w_ObjMRG.Refresh()     
        this.w_ObjMRG.bUpdated = .T.
      else
        * --- Importazione massiva
        if this.w_InsAttNelGiorno
          =fputs(this.w_Handle, Ah_MsgFormat("Data: ")+dtoc(this.w_RG__DATA)+Ah_MsgFormat("  Persona: ")+this.pCodPer+space(1)+this.oParentObject.w_DESCRIZ)
        endif
      endif
      * --- Incremento il giorno
      this.w_DATA_GIORNO = this.w_DATA_GIORNO + 1
    enddo
    if this.pTipoImport = "M"
      * --- Chiusura file di log
      =fclose(this.w_Handle)
      if this.w_InsAttivit�
        if cp_fileexist(this.w_NomeFileLog) AND AH_YESNO("Vuoi visualizzare il file di log?")
          viewFile(this.w_NomeFileLog)
        endif
      else
        AH_ErrorMsg("Attenzione: non esistono attivit� da inserire nel resoconto giornaliero!")
      endif
    endif
    if used("TEMP")
      select TEMP
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                         Calcolo durata attivit� relativa al giorno in esame
    * --- Se la data iniziale dell'attivit� coincide con quella finale
    if TTOD(ATDATINI) = TTOD(ATDATFIN)
      this.TotSecondi = cp_ROUND(ATDATFIN - ATDATINI, 0)
      this.Ore = INT(this.TotSecondi / 3600)
      this.Minuti = INT(MOD(this.TotSecondi, 3600) / 60)
    else
      * --- Se l'attivit� comprende pi� giorni
      this.Data = TTOD(ATDATINI)
      do while this.Data <= TTOD(ATDATFIN)
        if this.Data = this.w_RG__DATA
          do case
            case this.Data = TTOD(ATDATINI)
              this.TotSecondi = cp_ROUND(cp_CharToDatetime(DTOC(TTOD(ATDATINI)+1)+" 00:00:00") - ATDATINI, 0)
              this.Ore = INT(this.TotSecondi / 3600)
              this.Minuti = INT(MOD(this.TotSecondi, 3600) / 60)
            case this.Data = TTOD(ATDATFIN)
              this.TotSecondi = cp_ROUND(ATDATFIN - cp_CharToDatetime(DTOC(TTOD(ATDATFIN))+" 00:00:00"), 0)
              this.Ore = INT(this.TotSecondi / 3600)
              this.Minuti = INT(MOD(this.TotSecondi, 3600) / 60)
            otherwise
              * --- Giorno intero
              this.Ore = 24
              this.Minuti = 0
          endcase
          EXIT
        endif
        this.Data = this.Data + 1
      enddo
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                         Calcolo durata effettiva dell'attivit�
    this.w_SerialAt = ATSERIAL
    vq_exec("..\AGEN\EXE\QUERY\GSAG1MRG.VQR",this,"APPO")
    SELECT("APPO")
    this.w_OreEffet = NVL(DAOREEFF,0)
    this.w_MinEffet = NVL(DAMINEFF,0)
    USE
    this.w_OreEffet = this.w_OreEffet+INT(this.w_MinEffet / 60)
    this.w_MinEffet = MOD(this.w_MinEffet, 60) 
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                                           Scrive singola attivit� nel transitorio
    this.w_ObjMRG.AddRow()     
    this.w_ObjMRG.w_RGCAUATT = ATCAUATT
    * --- Read from CAUMATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CARAGGST"+;
        " from "+i_cTable+" CAUMATTI where ";
            +"CACODICE = "+cp_ToStrODBC(ATCAUATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CARAGGST;
        from (i_cTable) where;
            CACODICE = ATCAUATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CARAGGST = NVL(cp_ToDate(_read_.CARAGGST),cp_NullValue(_read_.CARAGGST))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ObjMRG.w_CARAGGST = this.w_CARAGGST
    this.w_ObjMRG.w_RGOGGETT = ATOGGETT
    this.w_ObjMRG.w_RGATTSER = ATSERIAL
    * --- Calcola la durata pianificata e quella effettiva (identiche) andando a suddividere l'attivit� fra 
    *     i vari giorni e selezionando quello relativo alla data del giorno
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_ObjMRG.w_RGOREPRE = this.Ore
    this.w_ObjMRG.w_RGMINPRE = this.Minuti
    if this.pFLSINGPRE="S" AND ATCAUATT=this.w_CAUPRE
      this.w_OreEffet = NVL(DAOREEFF,0)
      this.w_MinEffet = NVL(DAMINEFF,0)
    else
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_ObjMRG.w_RGOREEFF = this.w_OreEffet
    this.w_ObjMRG.w_RGMINEFF = this.w_MinEffet
    this.w_ObjMRG.SaveRow()     
    this.oParentObject.w_TOTOREEFF = this.oParentObject.w_TOTOREEFF + this.w_OreEffet
    this.oParentObject.w_TOTMINEFF = this.oParentObject.w_TOTMINEFF + this.w_MinEffet
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                                           Inserisce singola attivit� in tabella
    this.w_NUMRIGA = this.w_NUMRIGA + 1
    this.w_ATCAUATT = ATCAUATT
    this.w_ATOGGETT = ATOGGETT
    this.w_ATSERIAL = ATSERIAL
    * --- Valorizzate Ore e Minuti
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.pFLSINGPRE="S" AND ATCAUATT=this.w_CAUPRE
      this.w_OreEffet = NVL(DAOREEFF,0)
      this.w_MinEffet = NVL(DAMINEFF,0)
    else
      * --- Valorizzate w_OreEffet  e  w_MinEffet
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Try
    local bErr_03A7A6C8
    bErr_03A7A6C8=bTrsErr
    this.Try_03A7A6C8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03A7A6C8
    * --- End
  endproc
  proc Try_03A7A6C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into RIL_GIOR
    i_nConn=i_TableProp[this.RIL_GIOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIL_GIOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RIL_GIOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RGCODPER"+",RG__DATA"+",CPROWNUM"+",RGCAUATT"+",RGOGGETT"+",RGOREPRE"+",RGMINPRE"+",RGOREEFF"+",RGMINEFF"+",RGATTSER"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.pCodPer),'RIL_GIOR','RGCODPER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RG__DATA),'RIL_GIOR','RG__DATA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'RIL_GIOR','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCAUATT),'RIL_GIOR','RGCAUATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATOGGETT),'RIL_GIOR','RGOGGETT');
      +","+cp_NullLink(cp_ToStrODBC(this.Ore),'RIL_GIOR','RGOREPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.Minuti),'RIL_GIOR','RGMINPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OreEffet),'RIL_GIOR','RGOREEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MinEffet),'RIL_GIOR','RGMINEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATSERIAL),'RIL_GIOR','RGATTSER');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RGCODPER',this.pCodPer,'RG__DATA',this.w_RG__DATA,'CPROWNUM',this.w_NUMRIGA,'RGCAUATT',this.w_ATCAUATT,'RGOGGETT',this.w_ATOGGETT,'RGOREPRE',this.Ore,'RGMINPRE',this.Minuti,'RGOREEFF',this.w_OreEffet,'RGMINEFF',this.w_MinEffet,'RGATTSER',this.w_ATSERIAL)
      insert into (i_cTable) (RGCODPER,RG__DATA,CPROWNUM,RGCAUATT,RGOGGETT,RGOREPRE,RGMINPRE,RGOREEFF,RGMINEFF,RGATTSER &i_ccchkf. );
         values (;
           this.pCodPer;
           ,this.w_RG__DATA;
           ,this.w_NUMRIGA;
           ,this.w_ATCAUATT;
           ,this.w_ATOGGETT;
           ,this.Ore;
           ,this.Minuti;
           ,this.w_OreEffet;
           ,this.w_MinEffet;
           ,this.w_ATSERIAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_InsAttNelGiorno = .T.
    this.w_InsAttivit� = .T.
    return


  proc Init(oParentObject,pTipoImport,pDataIni,pDataFin,pCodPer,pStatoAtt,pFlSingPre)
    this.pTipoImport=pTipoImport
    this.pDataIni=pDataIni
    this.pDataFin=pDataFin
    this.pCodPer=pCodPer
    this.pStatoAtt=pStatoAtt
    this.pFlSingPre=pFlSingPre
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CAUMATTI'
    this.cWorkTables[2]='RIL_GIOR'
    this.cWorkTables[3]='PAR_AGEN'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG2MRG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipoImport,pDataIni,pDataFin,pCodPer,pStatoAtt,pFlSingPre"
endproc
