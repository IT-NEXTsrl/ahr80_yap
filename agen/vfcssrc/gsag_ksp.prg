* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ksp                                                        *
*              Spostamento date                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-20                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_ksp",oParentObject))

* --- Class definition
define class tgsag_ksp as StdForm
  Top    = 81
  Left   = 170

  * --- Standard Properties
  Width  = 543
  Height = 234
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=169203863
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  PAR_AGEN_IDX = 0
  CAUMATTI_IDX = 0
  cPrg = "gsag_ksp"
  cComment = "Spostamento date"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PAR_AGEN = space(5)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_ORAINI = space(2)
  w_MININI = space(2)
  w_CONFERMA = space(1)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_ORAFIN = space(2)
  w_MINFIN = space(2)
  w_ATCAUATT = space(20)
  w_ATOGGETTOR = space(254)
  w_DATINIOR = ctod('  /  /  ')
  w_OREINIOR = space(2)
  w_MININIOR = space(2)
  w_DATFINOR = ctod('  /  /  ')
  w_OREFINOR = space(2)
  w_MINFINOR = space(2)
  w_GIOINIOR = space(3)
  w_GIOINI = space(3)
  w_GIOFINOR = space(3)
  w_GIOFIN = space(3)
  w_PACHKFES = space(1)
  w_STATUS = space(1)
  w_PACHKATT = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kspPag1","gsag_ksp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='PAR_AGEN'
    this.cWorkTables[2]='CAUMATTI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PAR_AGEN=space(5)
      .w_DATINI=ctod("  /  /  ")
      .w_ORAINI=space(2)
      .w_MININI=space(2)
      .w_CONFERMA=space(1)
      .w_DATFIN=ctod("  /  /  ")
      .w_ORAFIN=space(2)
      .w_MINFIN=space(2)
      .w_ATCAUATT=space(20)
      .w_ATOGGETTOR=space(254)
      .w_DATINIOR=ctod("  /  /  ")
      .w_OREINIOR=space(2)
      .w_MININIOR=space(2)
      .w_DATFINOR=ctod("  /  /  ")
      .w_OREFINOR=space(2)
      .w_MINFINOR=space(2)
      .w_GIOINIOR=space(3)
      .w_GIOINI=space(3)
      .w_GIOFINOR=space(3)
      .w_GIOFIN=space(3)
      .w_PACHKFES=space(1)
      .w_STATUS=space(1)
      .w_PACHKATT=0
      .w_DATINI=oParentObject.w_DATINI
      .w_ORAINI=oParentObject.w_ORAINI
      .w_MININI=oParentObject.w_MININI
      .w_CONFERMA=oParentObject.w_CONFERMA
      .w_DATFIN=oParentObject.w_DATFIN
      .w_ORAFIN=oParentObject.w_ORAFIN
      .w_MINFIN=oParentObject.w_MINFIN
      .w_ATCAUATT=oParentObject.w_ATCAUATT
      .w_ATOGGETTOR=oParentObject.w_ATOGGETTOR
      .w_DATINIOR=oParentObject.w_DATINIOR
      .w_OREINIOR=oParentObject.w_OREINIOR
      .w_MININIOR=oParentObject.w_MININIOR
      .w_DATFINOR=oParentObject.w_DATFINOR
      .w_OREFINOR=oParentObject.w_OREFINOR
      .w_MINFINOR=oParentObject.w_MINFINOR
      .w_STATUS=oParentObject.w_STATUS
        .w_PAR_AGEN = i_CodAzi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PAR_AGEN))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,4,.f.)
        .w_CONFERMA = "S"
          .DoRTCalc(6,16,.f.)
        .w_GIOINIOR = iif(!EMPTY(.w_DATINIOR), g_GIORNO[DOW(.w_DATINIOR)] , SPACE(3))
        .w_GIOINI = iif(!EMPTY(.w_DATINI), g_GIORNO[DOW(.w_DATINI)] , SPACE(3))
        .w_GIOFINOR = iif(!EMPTY(.w_DATFINOR), g_GIORNO[DOW(.w_DATFINOR)] , SPACE(3))
        .w_GIOFIN = iif(!EMPTY(.w_DATFIN), g_GIORNO[DOW(.w_DATFIN)] , SPACE(3))
    endwith
    this.DoRTCalc(21,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATINI=.w_DATINI
      .oParentObject.w_ORAINI=.w_ORAINI
      .oParentObject.w_MININI=.w_MININI
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.w_DATFIN=.w_DATFIN
      .oParentObject.w_ORAFIN=.w_ORAFIN
      .oParentObject.w_MINFIN=.w_MINFIN
      .oParentObject.w_ATCAUATT=.w_ATCAUATT
      .oParentObject.w_ATOGGETTOR=.w_ATOGGETTOR
      .oParentObject.w_DATINIOR=.w_DATINIOR
      .oParentObject.w_OREINIOR=.w_OREINIOR
      .oParentObject.w_MININIOR=.w_MININIOR
      .oParentObject.w_DATFINOR=.w_DATFINOR
      .oParentObject.w_OREFINOR=.w_OREFINOR
      .oParentObject.w_MINFINOR=.w_MINFINOR
      .oParentObject.w_STATUS=.w_STATUS
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,4,.t.)
            .w_CONFERMA = "S"
        if .o_DATINI<>.w_DATINI
            .w_DATFIN = .w_DATINI
        endif
        .DoRTCalc(7,16,.t.)
            .w_GIOINIOR = iif(!EMPTY(.w_DATINIOR), g_GIORNO[DOW(.w_DATINIOR)] , SPACE(3))
        if .o_DATINI<>.w_DATINI
            .w_GIOINI = iif(!EMPTY(.w_DATINI), g_GIORNO[DOW(.w_DATINI)] , SPACE(3))
        endif
            .w_GIOFINOR = iif(!EMPTY(.w_DATFINOR), g_GIORNO[DOW(.w_DATFINOR)] , SPACE(3))
        if .o_DATFIN<>.w_DATFIN
            .w_GIOFIN = iif(!EMPTY(.w_DATFIN), g_GIORNO[DOW(.w_DATFIN)] , SPACE(3))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PAR_AGEN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAR_AGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAR_AGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACHKFES,PACHKATT";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PAR_AGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PAR_AGEN)
            select PACODAZI,PACHKFES,PACHKATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAR_AGEN = NVL(_Link_.PACODAZI,space(5))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
      this.w_PACHKATT = NVL(_Link_.PACHKATT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PAR_AGEN = space(5)
      endif
      this.w_PACHKFES = space(1)
      this.w_PACHKATT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAR_AGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_3.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_3.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_5.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_5.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_6.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_6.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_1_12.value==this.w_ORAFIN)
      this.oPgFrm.Page1.oPag.oORAFIN_1_12.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_13.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_13.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oATCAUATT_1_20.value==this.w_ATCAUATT)
      this.oPgFrm.Page1.oPag.oATCAUATT_1_20.value=this.w_ATCAUATT
    endif
    if not(this.oPgFrm.Page1.oPag.oATOGGETTOR_1_21.value==this.w_ATOGGETTOR)
      this.oPgFrm.Page1.oPag.oATOGGETTOR_1_21.value=this.w_ATOGGETTOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINIOR_1_23.value==this.w_DATINIOR)
      this.oPgFrm.Page1.oPag.oDATINIOR_1_23.value=this.w_DATINIOR
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINIOR_1_25.value==this.w_OREINIOR)
      this.oPgFrm.Page1.oPag.oOREINIOR_1_25.value=this.w_OREINIOR
    endif
    if not(this.oPgFrm.Page1.oPag.oMININIOR_1_26.value==this.w_MININIOR)
      this.oPgFrm.Page1.oPag.oMININIOR_1_26.value=this.w_MININIOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFINOR_1_29.value==this.w_DATFINOR)
      this.oPgFrm.Page1.oPag.oDATFINOR_1_29.value=this.w_DATFINOR
    endif
    if not(this.oPgFrm.Page1.oPag.oOREFINOR_1_31.value==this.w_OREFINOR)
      this.oPgFrm.Page1.oPag.oOREFINOR_1_31.value=this.w_OREFINOR
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFINOR_1_32.value==this.w_MINFINOR)
      this.oPgFrm.Page1.oPag.oMINFINOR_1_32.value=this.w_MINFINOR
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOINIOR_1_34.value==this.w_GIOINIOR)
      this.oPgFrm.Page1.oPag.oGIOINIOR_1_34.value=this.w_GIOINIOR
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOINI_1_35.value==this.w_GIOINI)
      this.oPgFrm.Page1.oPag.oGIOINI_1_35.value=this.w_GIOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOFINOR_1_36.value==this.w_GIOFINOR)
      this.oPgFrm.Page1.oPag.oGIOFINOR_1_36.value=this.w_GIOFINOR
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOFIN_1_37.value==this.w_GIOFIN)
      this.oPgFrm.Page1.oPag.oGIOFIN_1_37.value=this.w_GIOFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_ORAINI) < 24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   (empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_11.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_ORAFIN) < 24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_ksp
      if i_bRes=.t. And cp_CharToDatetime(DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00') > cp_CharToDatetime(DTOC(.w_DATFIN)+' '+.w_ORAFIN+':'+.w_MINFIN+':00')
        i_bRes=.f.
        ah_ErrorMsg("La data/ora di fine attivit� non deve essere minore della data/ora di inizio attivit�")
      EndIf
      
      if i_bRes=.t. And .w_DATFIN<i_DATSYS AND .w_STATUS$'T-D-I'
        if !ah_Yesno("Inserita data antecedente alla data odierna! Continuare?")
         i_bRes=.f.
        endif
      EndIf
      
      if i_bRes=.t. And Not Empty(.w_PACHKATT) And .w_DATINI>i_DATSYS+.w_PACHKATT AND .w_STATUS$'T-D-I'
        if !Ah_yesno( "Inserita data superiore alla data odierna di oltre %1 giorni. Continuare?", '', alltrim(str(.w_PACHKATT)))
         i_bRes=.f.
        endif
      EndIf
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    return

enddefine

* --- Define pages as container
define class tgsag_kspPag1 as StdContainer
  Width  = 539
  height = 234
  stdWidth  = 539
  stdheight = 234
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_3 as StdField with uid="MYGDNXHRTP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio attivit�",;
    HelpContextID = 129737162,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=99, Top=127

  func oDATINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_PACHKFES='N' OR Chkfestivi(.w_DATINI, .w_PACHKFES ))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oORAINI_1_5 as StdField with uid="VGJIHEDZAE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora inizio attivit�",;
    HelpContextID = 129810458,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=276, Top=127, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_6 as StdField with uid="NXNGNCXJWL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti inizio attivit�",;
    HelpContextID = 129759546,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=318, Top=127, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_11 as StdField with uid="BMQMJCRZZX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine attivit�",;
    HelpContextID = 51290570,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=99, Top=154

  func oDATFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !((.w_PACHKFES='N' OR Chkfestivi(.w_DATFIN , .w_PACHKFES )))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oORAFIN_1_12 as StdField with uid="TRWSSPBSKC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora fine attivit�",;
    HelpContextID = 51363866,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=276, Top=154, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_13 as StdField with uid="OFENOOOJLC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti fine attivit�",;
    HelpContextID = 51312954,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=318, Top=154, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc


  add object oBtn_1_15 as StdButton with uid="ERHOXVEEGI",left=434, top=185, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 169232614;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="TKCMXZXILU",left=487, top=185, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176521286;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oATCAUATT_1_20 as StdField with uid="LBGXFCJQQZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ATCAUATT", cQueryName = "ATCAUATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 11231578,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=62, Top=38, InputMask=replicate('X',20)

  add object oATOGGETTOR_1_21 as StdField with uid="ZBFVVQRHJA",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ATOGGETTOR", cQueryName = "ATOGGETTOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 64125002,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=223, Top=38, InputMask=replicate('X',254)

  add object oDATINIOR_1_23 as StdField with uid="ZJMFUWULJM",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATINIOR", cQueryName = "DATINIOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 129737080,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=99, Top=61

  add object oOREINIOR_1_25 as StdField with uid="LWBQPTRXKS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_OREINIOR", cQueryName = "OREINIOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 129793992,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=276, Top=61, InputMask=replicate('X',2)

  add object oMININIOR_1_26 as StdField with uid="HOGLWIPZWG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MININIOR", cQueryName = "MININIOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 129759464,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=318, Top=61, InputMask=replicate('X',2)

  add object oDATFINOR_1_29 as StdField with uid="TJIGSWXCVP",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATFINOR", cQueryName = "DATFINOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 51290488,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=99, Top=84

  add object oOREFINOR_1_31 as StdField with uid="FODQEWLPOM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_OREFINOR", cQueryName = "OREFINOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 51347400,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=276, Top=84, InputMask=replicate('X',2)

  add object oMINFINOR_1_32 as StdField with uid="LOVSPEKPZV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MINFINOR", cQueryName = "MINFINOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 51312872,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=318, Top=84, InputMask=replicate('X',2)

  add object oGIOINIOR_1_34 as StdField with uid="GVKNUIECUN",rtseq=17,rtrep=.f.,;
    cFormVar = "w_GIOINIOR", cQueryName = "GIOINIOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 129755464,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=62, Top=61, InputMask=replicate('X',3)

  add object oGIOINI_1_35 as StdField with uid="DPWOUXIATY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_GIOINI", cQueryName = "GIOINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 129755546,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=62, Top=127, InputMask=replicate('X',3)

  add object oGIOFINOR_1_36 as StdField with uid="LKYXKXYVUO",rtseq=19,rtrep=.f.,;
    cFormVar = "w_GIOFINOR", cQueryName = "GIOFINOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 51308872,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=62, Top=84, InputMask=replicate('X',3)

  add object oGIOFIN_1_37 as StdField with uid="EKMCUZZKKX",rtseq=20,rtrep=.f.,;
    cFormVar = "w_GIOFIN", cQueryName = "GIOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 51308954,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=62, Top=154, InputMask=replicate('X',3)

  add object oStr_1_2 as StdString with uid="CDWWSMMEYG",Visible=.t., Left=11, Top=129,;
    Alignment=1, Width=46, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="BYZPFCFPKB",Visible=.t., Left=217, Top=129,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="FXUDOJAZEE",Visible=.t., Left=304, Top=129,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="GKUXCCVURZ",Visible=.t., Left=11, Top=156,;
    Alignment=1, Width=46, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="SAALUVLINU",Visible=.t., Left=217, Top=156,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="FHAFBDYBRG",Visible=.t., Left=304, Top=156,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="DDTKAFHBPY",Visible=.t., Left=11, Top=12,;
    Alignment=0, Width=143, Height=18,;
    Caption="Attivit� selezionata"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="ORKJUUCBVM",Visible=.t., Left=11, Top=38,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="LMGOPCHWVW",Visible=.t., Left=11, Top=63,;
    Alignment=1, Width=46, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="JQFSIFZRPU",Visible=.t., Left=217, Top=63,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="RSSICMDKLX",Visible=.t., Left=304, Top=63,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="HPSEZCCCQK",Visible=.t., Left=11, Top=86,;
    Alignment=1, Width=46, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="QGOBFHWYWU",Visible=.t., Left=217, Top=86,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="YGRTYIYYXB",Visible=.t., Left=304, Top=86,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oBox_1_18 as StdBox with uid="MXBVHLEODS",left=7, top=28, width=528,height=85
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_ksp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
