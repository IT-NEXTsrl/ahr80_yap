* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ba3                                                        *
*              Valorizza date in ric. attivit�                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-09                                                      *
* Last revis.: 2014-02-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam,pCODPRO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_ba3",oParentObject,m.pParam,m.pCODPRO)
return(i_retval)

define class tgsag_ba3 as StdBatch
  * --- Local variables
  pParam = space(10)
  pCODPRO = space(10)
  TipoFiltro = space(1)
  N_GG_IN = 0
  N_GG_AV = 0
  w_PAFLCOSE = space(1)
  w_PADACOSE = 0
  w_PA_ACOSE = 0
  w_PAFLCOSE = space(1)
  w_PADACOSE = 0
  w_PA_ACOSE = 0
  * --- WorkFile variables
  PAR_AGEN_idx=0
  PRO_EXPO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza la data iniziale e quella finale.
    *     Se pParam=AGEN significa che siamo in ricerca/stampa attivit�, e quindi legge PANOFLAT con i relativi valori numerici, 
    *     Se pParam=PRES significa che siamo in ricerca/stampa prestazioni e quindi legge PANOFLPR con i relativi valori numerici.
    *     Se pParam=EXPO  eseguito da gsag_kis exort\import ICS
    * --- Se pParam=AGEN vengono avvalorate anche le date di ricerca per le cose da fare
    this.TipoFiltro = space(1)
    this.N_GG_IN = 0
    this.N_GG_AV = 0
    do case
      case this.pParam=="AGEN" OR this.pParam=="ct_List"
        * --- Read from PAR_AGEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PANOFLAT,PADATATP,PADATATS,PAFLCOSE,PADACOSE,PA_ACOSE"+;
            " from "+i_cTable+" PAR_AGEN where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PANOFLAT,PADATATP,PADATATS,PAFLCOSE,PADACOSE,PA_ACOSE;
            from (i_cTable) where;
                PACODAZI = i_codazi;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.TipoFiltro = NVL(cp_ToDate(_read_.PANOFLAT),cp_NullValue(_read_.PANOFLAT))
          this.N_GG_IN = NVL(cp_ToDate(_read_.PADATATP),cp_NullValue(_read_.PADATATP))
          this.N_GG_AV = NVL(cp_ToDate(_read_.PADATATS),cp_NullValue(_read_.PADATATS))
          this.w_PAFLCOSE = NVL(cp_ToDate(_read_.PAFLCOSE),cp_NullValue(_read_.PAFLCOSE))
          this.w_PADACOSE = NVL(cp_ToDate(_read_.PADACOSE),cp_NullValue(_read_.PADACOSE))
          this.w_PA_ACOSE = NVL(cp_ToDate(_read_.PA_ACOSE),cp_NullValue(_read_.PA_ACOSE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pParam=="EXPO"
        * --- Read from PRO_EXPO
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRO_EXPO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRO_EXPO_idx,2],.t.,this.PRO_EXPO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PRNOFLAT,PRDATATP,PRDATATS"+;
            " from "+i_cTable+" PRO_EXPO where ";
                +"PRSERIAL = "+cp_ToStrODBC(this.pCODPRO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PRNOFLAT,PRDATATP,PRDATATS;
            from (i_cTable) where;
                PRSERIAL = this.pCODPRO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.TipoFiltro = NVL(cp_ToDate(_read_.PRNOFLAT),cp_NullValue(_read_.PRNOFLAT))
          this.N_GG_IN = NVL(cp_ToDate(_read_.PRDATATP),cp_NullValue(_read_.PRDATATP))
          this.N_GG_AV = NVL(cp_ToDate(_read_.PRDATATS),cp_NullValue(_read_.PRDATATS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      otherwise
        * --- Read from PAR_AGEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PANOFLPR,PADATPRP,PADATPRS"+;
            " from "+i_cTable+" PAR_AGEN where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PANOFLPR,PADATPRP,PADATPRS;
            from (i_cTable) where;
                PACODAZI = i_codazi;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.TipoFiltro = NVL(cp_ToDate(_read_.PANOFLPR),cp_NullValue(_read_.PANOFLPR))
          this.N_GG_IN = NVL(cp_ToDate(_read_.PADATPRP),cp_NullValue(_read_.PADATPRP))
          this.N_GG_AV = NVL(cp_ToDate(_read_.PADATPRS),cp_NullValue(_read_.PADATPRS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
    endcase
    if !empty(this.TipoFiltro) AND !this.pParam=="ct_List"
      do case
        case this.TipoFiltro="I"
          * --- Prima data vuota
          this.oParentObject.w_DATINI = ctod("  -  -  ")
          this.oParentObject.w_DATFIN = i_datsys + this.N_GG_AV
        case this.TipoFiltro="F"
          * --- Seconda data vuota
          this.oParentObject.w_DATINI = i_datsys - this.N_GG_IN
          this.oParentObject.w_DATFIN = ctod("  -  -  ")
        case this.TipoFiltro="E"
          * --- Entrambe date vuote
          this.oParentObject.w_DATINI = ctod("  -  -  ")
          this.oParentObject.w_DATFIN = ctod("  -  -  ")
        case this.TipoFiltro="A"
          * --- Settimana corrente
          * --- Nota: la Dow() con parametro 2 ritorna valore 1 relativamente al luned�, 2 per marted�, ecc.
          this.oParentObject.w_DATINI = i_datsys - (DOW(i_datsys, 2) -1)
          this.oParentObject.w_DATFIN = i_datsys + (7 - DOW(i_datsys, 2))
        case this.TipoFiltro="B"
          * --- Settimana corrente e successiva
          this.oParentObject.w_DATINI = i_datsys - (DOW(i_datsys, 2) -1)
          this.oParentObject.w_DATFIN = i_datsys +7 + (7 - DOW(i_datsys+7, 2))
        case this.TipoFiltro="C"
          * --- Settimana corrente e precedente
          this.oParentObject.w_DATINI = i_datsys -7 - (DOW(i_datsys-7, 2) -1)
          this.oParentObject.w_DATFIN = i_datsys + (7 - DOW(i_datsys, 2))
        case this.TipoFiltro="D"
          * --- Settimana corrente, precedente e successiva
          this.oParentObject.w_DATINI = i_datsys-7 - (DOW(i_datsys-7, 2) -1)
          this.oParentObject.w_DATFIN = i_datsys+7 + (7 - DOW(i_datsys+7, 2))
        otherwise
          * --- Intervallo / Data odierna
          this.oParentObject.w_DATINI = i_datsys - this.N_GG_IN
          this.oParentObject.w_DATFIN = i_datsys + this.N_GG_AV
      endcase
    endif
    if (this.pParam=="AGEN" OR this.pParam=="ct_List") AND !empty(this.w_PAFLCOSE) AND VARTYPE(this.oParentObject.w_DATINI_Cose)<>"U"
      do case
        case this.w_PAFLCOSE="I"
          * --- Prima data vuota
          this.oParentObject.w_DATINI_Cose = ctod("  -  -  ")
          this.oParentObject.w_DATFIN_Cose = i_datsys + this.w_PA_ACOSE
        case this.w_PAFLCOSE="F"
          * --- Seconda data vuota
          this.oParentObject.w_DATINI_Cose = i_datsys - this.w_PADACOSE
          this.oParentObject.w_DATFIN_Cose = ctod("  -  -  ")
        case this.w_PAFLCOSE="E"
          * --- Entrambe date vuote
          this.oParentObject.w_DATINI_Cose = ctod("  -  -  ")
          this.oParentObject.w_DATFIN_Cose = ctod("  -  -  ")
        case this.w_PAFLCOSE="A"
          * --- Settimana corrente
          * --- Nota: la Dow() con parametro 2 ritorna valore 1 relativamente al luned�, 2 per marted�, ecc.
          this.oParentObject.w_DATINI_Cose = i_datsys - (DOW(i_datsys, 2) -1)
          this.oParentObject.w_DATFIN_Cose = i_datsys + (7 - DOW(i_datsys, 2))
        case this.w_PAFLCOSE="B"
          * --- Settimana corrente e successiva
          this.oParentObject.w_DATINI_Cose = i_datsys - (DOW(i_datsys, 2) -1)
          this.oParentObject.w_DATFIN_Cose = i_datsys +7 + (7 - DOW(i_datsys+7, 2))
        case this.w_PAFLCOSE="C"
          * --- Settimana corrente e precedente
          this.oParentObject.w_DATINI_Cose = i_datsys -7 - (DOW(i_datsys-7, 2) -1)
          this.oParentObject.w_DATFIN_Cose = i_datsys + (7 - DOW(i_datsys, 2))
        case this.w_PAFLCOSE="D"
          * --- Settimana corrente, precedente e successiva
          this.oParentObject.w_DATINI_Cose = i_datsys-7 - (DOW(i_datsys-7, 2) -1)
          this.oParentObject.w_DATFIN_Cose = i_datsys+7 + (7 - DOW(i_datsys+7, 2))
        otherwise
          * --- Intervallo / Data odierna
          this.oParentObject.w_DATINI_Cose = i_datsys - this.w_PADACOSE
          this.oParentObject.w_DATFIN_Cose = i_datsys + this.w_PA_ACOSE
      endcase
    endif
  endproc


  proc Init(oParentObject,pParam,pCODPRO)
    this.pParam=pParam
    this.pCODPRO=pCODPRO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_AGEN'
    this.cWorkTables[2]='PRO_EXPO'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam,pCODPRO"
endproc
