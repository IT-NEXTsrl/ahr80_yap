* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bpd                                                        *
*              Inserimento prestazioni                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-14                                                      *
* Last revis.: 2014-11-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bpd",oParentObject)
return(i_retval)

define class tgsag_bpd as StdBatch
  * --- Local variables
  w_OBJECT = .NULL.
  w_OBJ = .NULL.
  w_ObjMDA = .NULL.
  w_STATUS = space(1)
  w_ROWORD = 0
  w_CPROWNUM = 0
  w_RESOCON1 = space(0)
  w_ANNULLA = .f.
  w_MESBLOK = space(0)
  w_ROW = 0
  w_SavSerial = space(15)
  * --- WorkFile variables
  PAR_AGEN_idx=0
  DIPENDEN_idx=0
  KEY_ARTI_idx=0
  UNIMIS_idx=0
  ANEVENTI_idx=0
  CAUMATTI_idx=0
  OFFDATTI_idx=0
  OFF_ATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da Gsag_KPD 
    *     Inserisce prestazione nel punto desiderato prima o dopo la prestazione selezionata
    if this.oParentObject.w_INSPRDP="P"
      this.w_ROWORD = this.oParentObject.w_PrestRowOrd 
    else
      this.w_ROWORD = this.oParentObject.w_PrestRowOrd + 1
    endif
    g_SCHEDULER="S"
    g_MSG=""
    * --- Instanzio l'anagrafica
    if this.oParentObject.w_TIPOPRE="A"
      this.w_OBJECT = GSAG_AAT()
      * --- Controllo se ha passato il test di accesso
      * --- Per il codice utente, restituiamo il codice presente nel file DIPENDEN
      * --- Vado in caricamento
      if !(this.w_OBJECT.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.w_OBJECT.ECPFILTER()     
      this.w_OBJECT.w_ATSERIAL = this.oParentObject.w_ATSERIAL
      this.w_OBJECT.ECPSAVE()     
      this.w_OBJECT.ECPEDIT()     
      this.w_ObjMDA = this.w_OBJECT.GSAG_MDA
      g_SCHEDULER="N"
      * --- Grazie a queste 2 variabili l'anagrafica si chiude
      this.w_OBJECT.w_MaskCalend = this.oParentObject
      this.w_OBJECT.w_AggiornaCalend = .T.
      * --- Sappiamo il codice della prestazione
      this.w_ObjMDA.AddRow()     
      this.w_ROW = this.w_ObjMDA.rowindex()
      * --- Responsabile valorizzato con il primo partecipante
      = setvaluelinked ( "D" , this.w_ObjMDA, "w_DACODRES" , this.oParentObject.w_CodPart)
      * --- AHR/ALTE
      = setvaluelinked ( "D" , this.w_ObjMDA, "w_DACODATT" , this.oParentObject.w_CodSer)
      this.w_ObjMDA.Setrow(this.w_ROW)     
      this.w_ObjMDA.w_CPROWORD = this.oParentObject.w_PrestRowOrd + IIF(this.oParentObject.w_INSPRDP="D",10,0)
      this.w_ObjMDA.o_DACODICE = this.w_ObjMDA.w_DACODICE
      this.w_ObjMDA.Set("w_DAQTAMOV",this.oParentObject.w_DAQTAMOV,.f.,.t.)     
      this.w_ObjMDA.NotifyEvent("w_DACODATT Changed")     
      this.w_ObjMDA.w_DAUNIMIS = this.oParentObject.w_DAUNIMIS
      this.w_ObjMDA.w_UNMIS1 = this.oParentObject.w_DAUNIMIS
      if this.oParentObject.w_DAOREEFF>0 OR this.oParentObject.w_DAMINEFF>0
        this.w_ObjMDA.w_DAOREEFF = this.oParentObject.w_DAOREEFF
        this.w_ObjMDA.w_DAMINEFF = this.oParentObject.w_DAMINEFF
        this.w_ObjMDA.NotifyEvent("AggSoloCosto")     
      else
        this.w_ObjMDA.NotifyEvent("Aggcosto")     
      endif
      this.w_ObjMDA.o_DACODATT = this.w_ObjMDA.w_DACODATT
      this.w_ObjMDA.w_DADESATT = this.oParentObject.w_DesSer
      this.w_ObjMDA.w_DADESAGG = this.oParentObject.w_DesAgg
      this.w_ObjMDA.w_DAVOCCOS = this.oParentObject.w_DAVOCCOS
      this.w_ObjMDA.w_DAVOCRIC = this.oParentObject.w_DAVOCRIC
      this.w_ObjMDA.w_DACENCOS = this.oParentObject.w_DACENCOS
      this.w_ObjMDA.w_DACENRIC = this.oParentObject.w_DACENRIC
      this.w_ObjMDA.w_DACODCOM = this.oParentObject.w_DACODCOM
      this.w_ObjMDA.w_DAATTIVI = this.oParentObject.w_DAATTIVI
      this.w_ObjMDA.w_DA_SEGNO = this.oParentObject.w_DA_SEGNO
      this.w_ObjMDA.w_DAINICOM = this.oParentObject.w_DAINICOM
      this.w_ObjMDA.w_DAFINCOM = this.oParentObject.w_DAFINCOM
      if this.oParentObject.w_DAPREZZO <>0
        this.w_ObjMDA.w_DAPREZZO = this.oParentObject.w_DAPREZZO
      endif
      * --- Adesso che � stato assegnato il prezzo ricalcola DATIPRIG e DATIPRI2
      this.oParentObject.w_DATIPRIG = iCASE(this.oParentObject.w_TIPART="FO" and this.w_ObjMDA.w_DAPREZZO=0 and this.oParentObject.w_FLGZER="N", "N", this.oParentObject.w_DTIPRIG="N", "N", this.oParentObject.w_ARTIPRIG)
      this.oParentObject.w_DATIPRI2 = iCASE(this.oParentObject.w_TIPART="FO" and this.w_ObjMDA.w_DAPREZZO=0 and this.oParentObject.w_FLGZER="N", "N", this.oParentObject.w_ARTIPRI2)
      this.w_ObjMDA.w_ETIPRIG = this.oParentObject.w_DATIPRIG
      this.w_ObjMDA.w_RTIPRIG = this.oParentObject.w_DATIPRIG
      this.w_ObjMDA.w_DTIPRIG = this.oParentObject.w_DTIPRIG
      this.w_ObjMDA.w_ATIPRIG = this.oParentObject.w_DATIPRIG
      this.w_ObjMDA.w_DATIPRIG = this.oParentObject.w_DATIPRIG
      this.w_ObjMDA.w_ETIPRI2 = this.oParentObject.w_DATIPRI2
      this.w_ObjMDA.w_RTIPRI2 = this.oParentObject.w_DATIPRI2
      this.w_ObjMDA.w_ATIPRI2 = this.oParentObject.w_DATIPRI2
      this.w_ObjMDA.w_DATIPRI2 = this.oParentObject.w_DATIPRI2
      if this.oParentObject.w_DACOSUNI <>0
        this.w_ObjMDA.w_DACOSUNI = this.oParentObject.w_DACOSUNI
        this.w_ObjMDA.w_LICOSTO = this.oParentObject.w_DACOSUNI
        this.w_ObjMDA.w_COSTORA = this.oParentObject.w_DACOSUNI
      endif
      this.w_ObjMDA.SetControlsValue()     
      this.w_ObjMDA.mcalc(.T.)     
      * --- Impostiamo o_ATCauAtt in modo da impedire la rilettura tarmite mCalc
      this.w_ObjMDA.o_DACODRES = this.oParentObject.w_CodPart
      this.w_CPROWNUM = this.w_ObjMDA.w_CPROWNUM
      this.w_ObjMDA.SaveRow()     
      this.w_ObjMDA.Refresh()     
      this.w_OBJECT.SetControlsValue()     
      g_SCHEDULER="S"
      this.w_OBJECT.EcpSave()     
      g_SCHEDULER="N"
      this.w_MESBLOK = ""
      this.w_RESOCON1 = g_MSG
      if Not empty(g_MSG) and BTRSERR=.T. and Ah_yesno("Si desidera vedere log elaborazione?")
        do GSVE_KLG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Write into OFFDATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OFFDATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="DASERIAL,CPROWNUM"
          do vq_exec with 'GSAG_BPD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.OFFDATTI_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
                  +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CPROWORD = _t2.CPROWORD + 10";
              +i_ccchkf;
              +" from "+i_cTable+" OFFDATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
                  +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFFDATTI, "+i_cQueryTable+" _t2 set ";
              +"OFFDATTI.CPROWORD = _t2.CPROWORD + 10";
              +Iif(Empty(i_ccchkf),"",",OFFDATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="OFFDATTI.DASERIAL = t2.DASERIAL";
                  +" and "+"OFFDATTI.CPROWNUM = t2.CPROWNUM";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFFDATTI set (";
              +"CPROWORD";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.CPROWORD + 10";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
                  +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFFDATTI set ";
              +"CPROWORD = _t2.CPROWORD + 10";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CPROWORD = (select CPROWORD + 10 from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        g_SCHEDULER="S"
        g_MSG=""
        this.w_OBJECT = GSAG_AAT()
        this.w_OBJECT.ECPFILTER()     
        this.w_OBJECT.w_ATSERIAL = this.oParentObject.w_ATSERIAL
        this.w_OBJECT.ECPSAVE()     
        this.w_OBJECT.ECPEDIT()     
        this.w_OBJECT.EcpSave()     
        this.w_OBJECT.Ecpquit()     
      endif
    else
      this.w_OBJECT = GSAG_APR()
      * --- Controllo se ha passato il test di accesso
      if !(this.w_OBJECT.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.w_OBJECT.ECPLOAD()     
      * --- Mi salvo il progressivo
      this.w_SavSerial = this.w_OBJECT.w_PRSERIAL
      * --- Imposto il progressivo della vecchia prestazione
      this.w_OBJECT.w_PRSERIAL = ALLTRIM(this.oParentObject.w_ATSERIAL)
      * --- Leggo la prestazione vecchia, in modo da impostare tutti i valori corretti
      this.w_OBJECT.LoadRec()     
      * --- Reimpostiamo il nuovo seriale
      this.w_OBJECT.w_PRSERIAL = this.w_SavSerial
      this.w_OBJECT.w_PRSERAGG = " "
      this.w_OBJECT.w_PRRIFFAT = " "
      this.w_OBJECT.w_PRRIFPRO = " "
      this.w_OBJECT.w_PRRIFPRA = " "
      this.w_OBJECT.w_PRROWPRA = 0
      this.w_OBJECT.w_PRROWPRO = 0
      this.w_OBJECT.w_PRROWFAT = 0
      this.w_OBJECT.w_PRRIFBOZ = " "
      this.w_OBJECT.w_PRROWBOZ = 0
      this.w_OBJECT.w_PRRIFNOT = " "
      this.w_OBJECT.w_PRROWNOT = 0
      this.w_OBJECT.w_PRRIFFAA = " "
      this.w_OBJECT.w_PRROWFAA = 0
      this.w_OBJECT.w_PRCODATT = this.oParentObject.w_CodSer
      = setvaluelinked ( "M" , this.w_OBJECT, "w_PRCODATT" , this.oParentObject.w_CodSer)
      this.w_OBJECT.w_PRDESPRE = this.oParentObject.w_DesSer
      this.w_OBJECT.w_PRDESAGG = this.oParentObject.w_DesAgg
      this.w_OBJECT.w_PRUNIMIS = this.oParentObject.w_DAUNIMIS
      this.w_OBJECT.w_PRQTAMOV = this.oParentObject.w_DAQTAMOV
      if this.oParentObject.w_DAOREEFF>0 OR this.oParentObject.w_DAMINEFF>0
        this.w_OBJECT.w_PROREEFF = this.oParentObject.w_DAOREEFF
        this.w_OBJECT.w_PRMINEFF = this.oParentObject.w_DAMINEFF
      endif
      if this.oParentObject.w_DAPREZZO <>0
        this.w_OBJECT.w_PRPREZZO = this.oParentObject.w_DAPREZZO
      endif
      if this.oParentObject.w_DACOSUNI <>0
        this.w_OBJECT.w_PRCOSUNI = this.oParentObject.w_DACOSUNI
      endif
      this.w_OBJECT.w_PRVOCCOS = this.oParentObject.w_DAVOCCOS
      this.w_OBJECT.w_PRCENCOS = this.oParentObject.w_DACENCOS
      this.w_OBJECT.w_PRATTIVI = this.oParentObject.w_DAATTIVI
      this.w_OBJECT.w_PR_SEGNO = this.oParentObject.w_DA_SEGNO
      this.w_OBJECT.w_PRINICOM = this.oParentObject.w_DAINICOM
      this.w_OBJECT.w_PRFINCOM = this.oParentObject.w_DAFINCOM
      this.w_OBJECT.w_PRTIPRIG = this.oParentObject.w_DATIPRIG
      this.w_OBJECT.w_PRTIPRI2 = this.oParentObject.w_DATIPRI2
      this.w_OBJECT.w_PRNUMPRE = this.oParentObject.w_PrestRowOrd + IIF(this.oParentObject.w_INSPRDP="D",1,IIF(this.oParentObject.w_PrestRowOrd>0,-1,0))
      * --- Per fare in modo che il progressivo non sia forzato alla conferma dobbiamo gestire anche OP_
      if .F.
        this.w_OBJECT.op_PRNUMPRE = this.w_OBJECT.w_PRNUMPRE
      endif
      this.w_OBJECT.op_PRNUMPRE = 1
      this.w_OBJECT.ECPSAVE()     
      this.w_OBJECT.ECPQUIT()     
      this.w_OBJECT.Ecpquit()     
    endif
    g_SCHEDULER="N"
    this.w_OBJECT = .Null.
    g_MSG=""
    This.oparentobject.oparentobject.Notifyevent("Ricerca")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='PAR_AGEN'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='ANEVENTI'
    this.cWorkTables[6]='CAUMATTI'
    this.cWorkTables[7]='OFFDATTI'
    this.cWorkTables[8]='OFF_ATTI'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
