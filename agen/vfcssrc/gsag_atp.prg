* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_atp                                                        *
*              Tipi prestazioni                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-09-14                                                      *
* Last revis.: 2012-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_atp"))

* --- Class definition
define class tgsag_atp as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 453
  Height = 126+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-12"
  HelpContextID=175495319
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  TIPI_PRE_IDX = 0
  cFile = "TIPI_PRE"
  cKeySelect = "TPCODICE"
  cKeyWhere  = "TPCODICE=this.w_TPCODICE"
  cKeyWhereODBC = '"TPCODICE="+cp_ToStrODBC(this.w_TPCODICE)';

  cKeyWhereODBCqualified = '"TIPI_PRE.TPCODICE="+cp_ToStrODBC(this.w_TPCODICE)';

  cPrg = "gsag_atp"
  cComment = "Tipi prestazioni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TPCODICE = space(5)
  w_TPDESCRI = space(50)
  w_TPFLSTAM = space(1)
  w_TPORDSTA = 0
  w_TPPREDEF = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TIPI_PRE','gsag_atp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_atpPag1","gsag_atp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tipi prestazioni")
      .Pages(1).HelpContextID = 49730356
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTPCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TIPI_PRE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIPI_PRE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIPI_PRE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TPCODICE = NVL(TPCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TIPI_PRE where TPCODICE=KeySet.TPCODICE
    *
    i_nConn = i_TableProp[this.TIPI_PRE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPI_PRE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIPI_PRE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIPI_PRE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TIPI_PRE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TPCODICE',this.w_TPCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TPCODICE = NVL(TPCODICE,space(5))
        .w_TPDESCRI = NVL(TPDESCRI,space(50))
        .w_TPFLSTAM = NVL(TPFLSTAM,space(1))
        .w_TPORDSTA = NVL(TPORDSTA,0)
        .w_TPPREDEF = NVL(TPPREDEF,space(1))
        cp_LoadRecExtFlds(this,'TIPI_PRE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TPCODICE = space(5)
      .w_TPDESCRI = space(50)
      .w_TPFLSTAM = space(1)
      .w_TPORDSTA = 0
      .w_TPPREDEF = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_TPFLSTAM = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIPI_PRE')
    this.DoRTCalc(4,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTPCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTPDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oTPFLSTAM_1_5.enabled = i_bVal
      .Page1.oPag.oTPORDSTA_1_6.enabled = i_bVal
      .Page1.oPag.oTPPREDEF_1_8.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTPCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTPCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TIPI_PRE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIPI_PRE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPCODICE,"TPCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPDESCRI,"TPDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPFLSTAM,"TPFLSTAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPORDSTA,"TPORDSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TPPREDEF,"TPPREDEF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TIPI_PRE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPI_PRE_IDX,2])
    i_lTable = "TIPI_PRE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TIPI_PRE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIPI_PRE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPI_PRE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TIPI_PRE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIPI_PRE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIPI_PRE')
        i_extval=cp_InsertValODBCExtFlds(this,'TIPI_PRE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TPCODICE,TPDESCRI,TPFLSTAM,TPORDSTA,TPPREDEF "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TPCODICE)+;
                  ","+cp_ToStrODBC(this.w_TPDESCRI)+;
                  ","+cp_ToStrODBC(this.w_TPFLSTAM)+;
                  ","+cp_ToStrODBC(this.w_TPORDSTA)+;
                  ","+cp_ToStrODBC(this.w_TPPREDEF)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIPI_PRE')
        i_extval=cp_InsertValVFPExtFlds(this,'TIPI_PRE')
        cp_CheckDeletedKey(i_cTable,0,'TPCODICE',this.w_TPCODICE)
        INSERT INTO (i_cTable);
              (TPCODICE,TPDESCRI,TPFLSTAM,TPORDSTA,TPPREDEF  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TPCODICE;
                  ,this.w_TPDESCRI;
                  ,this.w_TPFLSTAM;
                  ,this.w_TPORDSTA;
                  ,this.w_TPPREDEF;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TIPI_PRE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPI_PRE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TIPI_PRE_IDX,i_nConn)
      *
      * update TIPI_PRE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TIPI_PRE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TPDESCRI="+cp_ToStrODBC(this.w_TPDESCRI)+;
             ",TPFLSTAM="+cp_ToStrODBC(this.w_TPFLSTAM)+;
             ",TPORDSTA="+cp_ToStrODBC(this.w_TPORDSTA)+;
             ",TPPREDEF="+cp_ToStrODBC(this.w_TPPREDEF)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TIPI_PRE')
        i_cWhere = cp_PKFox(i_cTable  ,'TPCODICE',this.w_TPCODICE  )
        UPDATE (i_cTable) SET;
              TPDESCRI=this.w_TPDESCRI;
             ,TPFLSTAM=this.w_TPFLSTAM;
             ,TPORDSTA=this.w_TPORDSTA;
             ,TPPREDEF=this.w_TPPREDEF;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TIPI_PRE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPI_PRE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TIPI_PRE_IDX,i_nConn)
      *
      * delete TIPI_PRE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TPCODICE',this.w_TPCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIPI_PRE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPI_PRE_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTPORDSTA_1_6.visible=!this.oPgFrm.Page1.oPag.oTPORDSTA_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTPCODICE_1_1.value==this.w_TPCODICE)
      this.oPgFrm.Page1.oPag.oTPCODICE_1_1.value=this.w_TPCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTPDESCRI_1_3.value==this.w_TPDESCRI)
      this.oPgFrm.Page1.oPag.oTPDESCRI_1_3.value=this.w_TPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTPFLSTAM_1_5.RadioValue()==this.w_TPFLSTAM)
      this.oPgFrm.Page1.oPag.oTPFLSTAM_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTPORDSTA_1_6.value==this.w_TPORDSTA)
      this.oPgFrm.Page1.oPag.oTPORDSTA_1_6.value=this.w_TPORDSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oTPPREDEF_1_8.RadioValue()==this.w_TPPREDEF)
      this.oPgFrm.Page1.oPag.oTPPREDEF_1_8.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'TIPI_PRE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TPCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTPCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TPCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TPDESCRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTPDESCRI_1_3.SetFocus()
            i_bnoObbl = !empty(.w_TPDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TPORDSTA))  and not(.w_TPFLSTAM='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTPORDSTA_1_6.SetFocus()
            i_bnoObbl = !empty(.w_TPORDSTA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TPPREDEF<>'S' or Tippred('P')== .w_TPCODICE OR EMPTY( Tippred('P') ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTPPREDEF_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione esiste gi� un tipo predefinito")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_atpPag1 as StdContainer
  Width  = 449
  height = 126
  stdWidth  = 449
  stdheight = 126
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTPCODICE_1_1 as StdField with uid="VXRVYWEGOI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TPCODICE", cQueryName = "TPCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 133603717,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=82, Top=25, InputMask=replicate('X',5)

  add object oTPDESCRI_1_3 as StdField with uid="XGQSMZFOAH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TPDESCRI", cQueryName = "TPDESCRI",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 219189633,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=82, Top=53, InputMask=replicate('X',50)

  add object oTPFLSTAM_1_5 as StdCheck with uid="KAWAOKAQTH",rtseq=3,rtrep=.f.,left=82, top=84, caption="Escludi da stampa singola attivit�",;
    HelpContextID = 201945469,;
    cFormVar="w_TPFLSTAM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTPFLSTAM_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTPFLSTAM_1_5.GetRadio()
    this.Parent.oContained.w_TPFLSTAM = this.RadioValue()
    return .t.
  endfunc

  func oTPFLSTAM_1_5.SetRadio()
    this.Parent.oContained.w_TPFLSTAM=trim(this.Parent.oContained.w_TPFLSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_TPFLSTAM=='S',1,;
      0)
  endfunc

  add object oTPORDSTA_1_6 as StdField with uid="YGYGPVGGCP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TPORDSTA", cQueryName = "TPORDSTA",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 34414199,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=410, Top=82, cSayPict='"999"', cGetPict='"999"'

  func oTPORDSTA_1_6.mHide()
    with this.Parent.oContained
      return (.w_TPFLSTAM='S')
    endwith
  endfunc

  add object oTPPREDEF_1_8 as StdCheck with uid="MMLMCMVBIG",rtseq=5,rtrep=.f.,left=364, top=25, caption="Predefinito",;
    ToolTipText = "Tipo predefinito",;
    HelpContextID = 52244092,;
    cFormVar="w_TPPREDEF", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Attenzione esiste gi� un tipo predefinito";
   , bGlobalFont=.t.


  func oTPPREDEF_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTPPREDEF_1_8.GetRadio()
    this.Parent.oContained.w_TPPREDEF = this.RadioValue()
    return .t.
  endfunc

  func oTPPREDEF_1_8.SetRadio()
    this.Parent.oContained.w_TPPREDEF=trim(this.Parent.oContained.w_TPPREDEF)
    this.value = ;
      iif(this.Parent.oContained.w_TPPREDEF=='S',1,;
      0)
  endfunc

  func oTPPREDEF_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TPPREDEF<>'S' or Tippred('P')== .w_TPCODICE OR EMPTY( Tippred('P') ))
    endwith
    return bRes
  endfunc

  add object oStr_1_2 as StdString with uid="HFZSSJIBXP",Visible=.t., Left=37, Top=29,;
    Alignment=1, Width=42, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="RYJFWAYYIE",Visible=.t., Left=11, Top=57,;
    Alignment=1, Width=68, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="JDRGGMAFUT",Visible=.t., Left=307, Top=86,;
    Alignment=1, Width=98, Height=18,;
    Caption="Ordine di stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_TPFLSTAM='S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_atp','TIPI_PRE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TPCODICE=TIPI_PRE.TPCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
