* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mca                                                        *
*              Tipi attivit�                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-13                                                      *
* Last revis.: 2015-10-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_mca"))

* --- Class definition
define class tgsag_mca as StdTrsForm
  Top    = 10
  Left   = 7

  * --- Standard Properties
  Width  = 747
  Height = 546+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-19"
  HelpContextID=171301015
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=135

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CAUMATTI_IDX = 0
  CAU_ATTI_IDX = 0
  TIP_DOCU_IDX = 0
  ART_ICOL_IDX = 0
  OFFTIPAT_IDX = 0
  KEY_ARTI_IDX = 0
  PAR_AGEN_IDX = 0
  LISTINI_IDX = 0
  FRA_MODE_IDX = 0
  CAU_CONT_IDX = 0
  PRA_TIPI_IDX = 0
  PRA_MATE_IDX = 0
  PRA_ENTI_IDX = 0
  TIPI_PRE_IDX = 0
  cFile = "CAUMATTI"
  cFileDetail = "CAU_ATTI"
  cKeySelect = "CACODICE"
  cKeyWhere  = "CACODICE=this.w_CACODICE"
  cKeyDetail  = "CACODICE=this.w_CACODICE"
  cKeyWhereODBC = '"CACODICE="+cp_ToStrODBC(this.w_CACODICE)';

  cKeyDetailWhereODBC = '"CACODICE="+cp_ToStrODBC(this.w_CACODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CAU_ATTI.CACODICE="+cp_ToStrODBC(this.w_CACODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CAU_ATTI.CPROWORD '
  cPrg = "gsag_mca"
  cComment = "Tipi attivit�"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 4
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ISALT = .F.
  w_ISAHE = .F.
  w_CACODICE = space(20)
  o_CACODICE = space(20)
  w_CADESCRI = space(254)
  w_PARAGEN = space(10)
  o_PARAGEN = space(10)
  w_CARAGGST = space(1)
  o_CARAGGST = space(1)
  w_CARAGGST = space(1)
  w_CATIPATT = space(5)
  w_CAORASYS = space(1)
  o_CAORASYS = space(1)
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_CADURORE = 0
  w_CADURMIN = 0
  w_CAGGPREA = 0
  w_CA_TIMER = space(1)
  o_CA_TIMER = space(1)
  w_CAFLPDOC = space(1)
  w_CASERDOC = space(10)
  w_CADTOBSO = ctod('  /  /  ')
  w_CASTAATT = space(1)
  w_CASTAATT = space(1)
  w_CAFLNOTI = space(1)
  w_CAPRIORI = 0
  o_CAPRIORI = 0
  w_CAPRIEML = 0
  w_CAMINPRE = 0
  o_CAMINPRE = 0
  w_CAFLRINV = space(1)
  w_CAFLTRIS = space(1)
  w_CACHKOBB = space(1)
  w_CACHKOBB = space(1)
  w_CACHKNOM = space(1)
  w_CADISPON = 0
  w_CAFLSTAT = space(1)
  w_CAFLATRI = space(1)
  o_CAFLATRI = space(1)
  w_CACAUABB = space(5)
  w_CACHKINI = space(1)
  o_CACHKINI = space(1)
  w_CAPUBWEB = space(1)
  w_COLORE = space(10)
  w_COLOREP = space(10)
  w_CACHKFOR = space(1)
  w_CAFLPREA = space(1)
  w_CAFLATTI = space(1)
  w_CAFLATTN = 0
  w_CAFLPART = space(1)
  w_CAFLPARN = 0
  w_CAFLPRAT = space(1)
  w_CAFLPRAN = 0
  w_CAFLSOGG = space(1)
  w_CAFLSOGN = 0
  w_CAFLGIUD = space(1)
  w_CAFLGIUN = 0
  w_CAFLNOMI = space(1)
  w_CAFLNOMN = 0
  w_CAFLNSAP = space(1)
  o_CAFLNSAP = space(1)
  w_ACQU = space(1)
  w_CAFLANAL = space(1)
  o_CAFLANAL = space(1)
  w_CACAUDOC = space(5)
  w_CACAUACQ = space(5)
  w_CATIPEVE = space(10)
  w_CAFLGLIS = space(1)
  w_DESCAUDI = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_CADATINI = ctot('')
  o_CADATINI = ctot('')
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_CAPROMEM = ctot('')
  w_RESCHK = 0
  w_TIPOENTE = space(1)
  w_DESTIPAT = space(50)
  w_TDFLINTE = space(1)
  w_CA__NOTE = space(0)
  w_CAT_DOC = space(2)
  w_TDCATDOC = space(2)
  w_TDFLVEAC = space(1)
  w_FLGLISV = space(1)
  w_FLPRAT = space(1)
  w_GESRIS = space(1)
  w_PACHKFES = space(1)
  w_DESCAUAC = space(35)
  w_AFLVEAC = space(1)
  w_VFLVEAC = space(1)
  w_FLGLIS = space(1)
  w_FLINTE = space(1)
  w_CACOLORE = 0
  o_CACOLORE = 0
  w_ColAttivita = 0
  w_CACOLPOS = 0
  w_ColAttivita = 0
  w_RIGA = space(1)
  w_CPROWORD = 0
  w_CATIPRIS = space(1)
  w_CAKEYART = space(41)
  o_CAKEYART = space(41)
  w_CACODSER = space(20)
  o_CACODSER = space(20)
  w_CACODART = space(20)
  w_ECACODART = space(20)
  w_RCACODART = space(20)
  w_ARTIPRIG = space(1)
  w_ARTIPRI2 = space(1)
  w_CADESSER = space(40)
  w_CATIPRIG = space(1)
  w_CATIPRI2 = space(1)
  w_ATIPRIG = space(1)
  o_ATIPRIG = space(1)
  w_ATIPRI2 = space(1)
  o_ATIPRI2 = space(1)
  w_ETIPRIG = space(1)
  o_ETIPRIG = space(1)
  w_ETIPRI2 = space(1)
  o_ETIPRI2 = space(1)
  w_RTIPRIG = space(1)
  o_RTIPRIG = space(1)
  w_RTIPRI2 = space(1)
  o_RTIPRI2 = space(1)
  w_CAFLDEFF = space(1)
  w_EDESAGG = space(0)
  w_RDESAGG = space(0)
  w_CAFLRESP = space(1)
  w_ECADTOBSO = ctod('  /  /  ')
  w_CADESAGG = space(0)
  w_KADTOBSO = ctod('  /  /  ')
  w_RCADTOBSO = ctod('  /  /  ')
  w_DESCAABB = space(35)
  w_DESABB = space(35)
  w_CFLVEAC = space(1)
  w_CA__NOTE = space(0)
  w_CAUCON = space(5)
  w_FLCOSE = space(1)
  w_CAMODAPP = space(1)
  w_CATIPPRA = space(10)
  w_CAMATPRA = space(10)
  w_CA__ENTE = space(10)
  w_FLGIUD = space(1)
  w_MATETIPOL = space(1)
  o_MATETIPOL = space(1)
  w_RTIPRES = space(1)
  w_ETIPRES = space(1)
  w_PAFLVISI = space(1)
  o_PAFLVISI = space(1)
  w_CAFLDFIN = space(1)
  w_CAFLDINN = 0
  w_CAFLDINI = space(1)
  w_CAFLDFNN = 0
  w_TDFLINTE1 = space(1)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0

  * --- Children pointers
  gsag_mdp = .NULL.
  GSAG_MUM = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_mca
  w_PaginaNote = 0
  w_BckpForClr = 0
  
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CAUMATTI','gsag_mca')
    stdPageFrame::Init()
    *set procedure to GSAG_MUM additive
    with this
      .Pages(1).addobject("oPag","tgsag_mcaPag1","gsag_mca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tipo attivit�")
      .Pages(1).HelpContextID = 203904058
      .Pages(2).addobject("oPag","tgsag_mcaPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Partecipanti")
      .Pages(2).HelpContextID = 74584950
      .Pages(3).addobject("oPag","tgsag_mcaPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Note")
      .Pages(3).HelpContextID = 178425046
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCACODICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAG_MUM
    * --- Area Manuale = Init Page Frame
    * --- gsag_mca
    if VARTYPE(g_SCHEDULER)='C' AND g_SCHEDULER='S'
        this.parent.left=_screen.width+1
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='OFFTIPAT'
    this.cWorkTables[4]='KEY_ARTI'
    this.cWorkTables[5]='PAR_AGEN'
    this.cWorkTables[6]='LISTINI'
    this.cWorkTables[7]='FRA_MODE'
    this.cWorkTables[8]='CAU_CONT'
    this.cWorkTables[9]='PRA_TIPI'
    this.cWorkTables[10]='PRA_MATE'
    this.cWorkTables[11]='PRA_ENTI'
    this.cWorkTables[12]='TIPI_PRE'
    this.cWorkTables[13]='CAUMATTI'
    this.cWorkTables[14]='CAU_ATTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(14))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAUMATTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAUMATTI_IDX,3]
  return

  function CreateChildren()
    this.gsag_mdp = CREATEOBJECT('stdDynamicChild',this,'gsag_mdp',this.oPgFrm.Page2.oPag.oLinkPC_4_1)
    this.gsag_mdp.createrealchild()
    this.GSAG_MUM = CREATEOBJECT('stdLazyChild',this,'GSAG_MUM')
    return

  procedure DestroyChildren()
    if !ISNULL(this.gsag_mdp)
      this.gsag_mdp.DestroyChildrenChain()
      this.gsag_mdp=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_4_1')
    if !ISNULL(this.GSAG_MUM)
      this.GSAG_MUM.DestroyChildrenChain()
      this.GSAG_MUM=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gsag_mdp.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAG_MUM.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gsag_mdp.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAG_MUM.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gsag_mdp.NewDocument()
    this.GSAG_MUM.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .gsag_mdp.ChangeRow(this.cRowID+'      1',1;
             ,.w_CACODICE,"DFCODICE";
             )
      .WriteTo_gsag_mdp()
      .GSAG_MUM.ChangeRow(this.cRowID+'      1',1;
             ,.w_CACODICE,"UMCODICE";
             )
    endwith
    select (i_cOldSel)
    return

procedure WriteTo_gsag_mdp()
  if at('gsag_mdp',lower(this.gsag_mdp.class))<>0
    if this.gsag_mdp.w_PAFLVISI<>this.w_PAFLVISI
      this.gsag_mdp.w_PAFLVISI = this.w_PAFLVISI
      this.gsag_mdp.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CACODICE = NVL(CACODICE,space(20))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_37_joined
    link_1_37_joined=.f.
    local link_1_59_joined
    link_1_59_joined=.f.
    local link_1_60_joined
    link_1_60_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CAUMATTI where CACODICE=KeySet.CACODICE
    *
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2],this.bLoadRecFilter,this.CAUMATTI_IDX,"gsag_mca")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAUMATTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAUMATTI.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CAU_ATTI.","CAUMATTI.")
      i_cTable = i_cTable+' CAUMATTI '
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_37_joined=this.AddJoinedLink_1_37(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_59_joined=this.AddJoinedLink_1_59(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_60_joined=this.AddJoinedLink_1_60(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_ISALT = isalt()
        .w_ISAHE = isahe()
        .w_PARAGEN = i_CODAZI
        .w_COLORE = space(10)
        .w_COLOREP = space(10)
        .w_DESCAUDI = space(35)
        .w_OBTEST = i_datsys
        .w_DATINI = i_inidat
        .w_RESCHK = 0
        .w_TIPOENTE = ' '
        .w_DESTIPAT = space(50)
        .w_TDFLINTE = space(1)
        .w_CAT_DOC = IIF(.w_ISALT, 'DI', SPACE(2) )
        .w_TDCATDOC = space(2)
        .w_TDFLVEAC = space(1)
        .w_FLGLISV = space(1)
        .w_FLPRAT = space(1)
        .w_GESRIS = space(1)
        .w_PACHKFES = space(1)
        .w_DESCAUAC = space(35)
        .w_AFLVEAC = space(1)
        .w_VFLVEAC = space(1)
        .w_FLGLIS = space(1)
        .w_FLINTE = space(1)
        .w_EDESAGG = space(0)
        .w_RDESAGG = space(0)
        .w_DESCAABB = space(35)
        .w_DESABB = space(35)
        .w_CFLVEAC = space(1)
        .w_CAUCON = space(5)
        .w_FLCOSE = space(1)
        .w_FLGIUD = space(1)
        .w_MATETIPOL = space(1)
        .w_PAFLVISI = space(1)
        .w_TDFLINTE1 = space(1)
        .w_CACODICE = NVL(CACODICE,space(20))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CADESCRI = NVL(CADESCRI,space(254))
          .link_1_6('Load')
        .w_CARAGGST = NVL(CARAGGST,space(1))
        .w_CARAGGST = NVL(CARAGGST,space(1))
        .w_CATIPATT = NVL(CATIPATT,space(5))
          if link_1_9_joined
            this.w_CATIPATT = NVL(TACODTIP109,NVL(this.w_CATIPATT,space(5)))
            this.w_DESTIPAT = NVL(TADESCRI109,space(50))
          else
          .link_1_9('Load')
          endif
        .w_CAORASYS = NVL(CAORASYS,space(1))
        .w_ORAINI = IIF(EMPTY(.w_CADATINI),'08',PADL(ALLTRIM(STR(HOUR(.w_CADATINI),2)),2,'0'))
        .w_MININI = RIGHT('00'+ALLTRIM(STR(MINUTE(.w_CADATINI),2)),2)
        .w_CADURORE = NVL(CADURORE,0)
        .w_CADURMIN = NVL(CADURMIN,0)
        .w_CAGGPREA = NVL(CAGGPREA,0)
        .w_CA_TIMER = NVL(CA_TIMER,space(1))
        .w_CAFLPDOC = NVL(CAFLPDOC,space(1))
        .w_CASERDOC = NVL(CASERDOC,space(10))
        .w_CADTOBSO = NVL(cp_ToDate(CADTOBSO),ctod("  /  /  "))
        .w_CASTAATT = NVL(CASTAATT,space(1))
        .w_CASTAATT = NVL(CASTAATT,space(1))
        .w_CAFLNOTI = NVL(CAFLNOTI,space(1))
        .w_CAPRIORI = NVL(CAPRIORI,0)
        .w_CAPRIEML = NVL(CAPRIEML,0)
        .w_CAMINPRE = NVL(CAMINPRE,0)
        .w_CAFLRINV = NVL(CAFLRINV,space(1))
        .w_CAFLTRIS = NVL(CAFLTRIS,space(1))
        .w_CACHKOBB = NVL(CACHKOBB,space(1))
        .w_CACHKOBB = NVL(CACHKOBB,space(1))
        .w_CACHKNOM = NVL(CACHKNOM,space(1))
        .w_CADISPON = NVL(CADISPON,0)
        .w_CAFLSTAT = NVL(CAFLSTAT,space(1))
        .w_CAFLATRI = NVL(CAFLATRI,space(1))
        .w_CACAUABB = NVL(CACAUABB,space(5))
          if link_1_37_joined
            this.w_CACAUABB = NVL(TDTIPDOC137,NVL(this.w_CACAUABB,space(5)))
            this.w_DESABB = NVL(TDDESDOC137,space(35))
            this.w_CFLVEAC = NVL(TDFLVEAC137,space(1))
            this.w_TDFLINTE1 = NVL(TDFLINTE137,space(1))
          else
          .link_1_37('Load')
          endif
        .w_CACHKINI = NVL(CACHKINI,space(1))
        .w_CAPUBWEB = NVL(CAPUBWEB,space(1))
        .w_CACHKFOR = NVL(CACHKFOR,space(1))
        .w_CAFLPREA = NVL(CAFLPREA,space(1))
        .w_CAFLATTI = NVL(CAFLATTI,space(1))
        .w_CAFLATTN = NVL(CAFLATTN,0)
        .w_CAFLPART = NVL(CAFLPART,space(1))
        .w_CAFLPARN = NVL(CAFLPARN,0)
        .w_CAFLPRAT = NVL(CAFLPRAT,space(1))
        .w_CAFLPRAN = NVL(CAFLPRAN,0)
        .w_CAFLSOGG = NVL(CAFLSOGG,space(1))
        .w_CAFLSOGN = NVL(CAFLSOGN,0)
        .w_CAFLGIUD = NVL(CAFLGIUD,space(1))
        .w_CAFLGIUN = NVL(CAFLGIUN,0)
        .w_CAFLNOMI = NVL(CAFLNOMI,space(1))
        .w_CAFLNOMN = NVL(CAFLNOMN,0)
        .w_CAFLNSAP = NVL(CAFLNSAP,space(1))
        .w_ACQU = iif(g_APPLICATION='ad hoc ENTERPRISE',g_CACQ,g_ACQU)
        .w_CAFLANAL = NVL(CAFLANAL,space(1))
        .w_CACAUDOC = NVL(CACAUDOC,space(5))
          if link_1_59_joined
            this.w_CACAUDOC = NVL(TDTIPDOC159,NVL(this.w_CACAUDOC,space(5)))
            this.w_DESCAUDI = NVL(TDDESDOC159,space(35))
            this.w_TDFLINTE = NVL(TDFLINTE159,space(1))
            this.w_TDCATDOC = NVL(TDCATDOC159,space(2))
            this.w_TDFLVEAC = NVL(TDFLVEAC159,space(1))
            this.w_VFLVEAC = NVL(TDFLVEAC159,space(1))
            this.w_FLPRAT = NVL(TDFLPRAT159,space(1))
            this.w_CAUCON = NVL(TDCAUCON159,space(5))
            this.w_FLGLISV = NVL(TDFLGLIS159,space(1))
          else
          .link_1_59('Load')
          endif
        .w_CACAUACQ = NVL(CACAUACQ,space(5))
          if link_1_60_joined
            this.w_CACAUACQ = NVL(TDTIPDOC160,NVL(this.w_CACAUACQ,space(5)))
            this.w_DESCAUAC = NVL(TDDESDOC160,space(35))
            this.w_AFLVEAC = NVL(TDFLVEAC160,space(1))
            this.w_FLGLIS = NVL(TDFLGLIS160,space(1))
            this.w_FLINTE = NVL(TDFLINTE160,space(1))
          else
          .link_1_60('Load')
          endif
        .w_CATIPEVE = NVL(CATIPEVE,space(10))
        .w_CAFLGLIS = NVL(CAFLGLIS,space(1))
        .w_CADATINI = NVL(CADATINI,ctot(""))
        .w_CAPROMEM = NVL(CAPROMEM,ctot(""))
        .w_CA__NOTE = NVL(CA__NOTE,space(0))
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate(IIF(.w_ISALT, "Tipo per la generazione dei documenti interni da assegnare all'attivit�", "Causale per la generazione dei documenti interni da assegnare all'attivit�"))
        .w_CACOLORE = NVL(CACOLORE,0)
        .w_ColAttivita = IIF(.w_CACOLORE=0 or .w_CAPRIORI<>1 ,0,.w_CACOLORE)
        .oPgFrm.Page1.oPag.oObj_1_124.Calculate(.w_ColAttivita)
        .w_CACOLPOS = NVL(CACOLPOS,0)
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate(.w_CACOLPOS)
        .w_ColAttivita = IIF(.w_CACOLORE=0 or .w_CAPRIORI<>1 ,0,.w_CACOLORE)
        .w_CA__NOTE = NVL(CA__NOTE,space(0))
          .link_1_137('Load')
        .w_CAMODAPP = NVL(CAMODAPP,space(1))
        .w_CATIPPRA = NVL(CATIPPRA,space(10))
          .link_1_139('Load')
        .w_CAMATPRA = NVL(CAMATPRA,space(10))
          .link_1_140('Load')
        .w_CA__ENTE = NVL(CA__ENTE,space(10))
          * evitabile
          *.link_1_141('Load')
        .w_CAFLDFIN = NVL(CAFLDFIN,space(1))
        .w_CAFLDINN = NVL(CAFLDINN,0)
        .w_CAFLDINI = NVL(CAFLDINI,space(1))
        .w_CAFLDFNN = NVL(CAFLDFNN,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        cp_LoadRecExtFlds(this,'CAUMATTI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CAU_ATTI where CACODICE=KeySet.CACODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CAU_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_ATTI_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CAU_ATTI')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CAU_ATTI.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CAU_ATTI"
        link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
        select * from (i_cTable) CAU_ATTI where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_RIGA = space(1)
          .w_ECACODART = space(20)
          .w_RCACODART = space(20)
          .w_ARTIPRIG = space(1)
          .w_ARTIPRI2 = space(1)
          .w_ECADTOBSO = ctod("  /  /  ")
          .w_RCADTOBSO = ctod("  /  /  ")
          .w_RTIPRES = space(1)
          .w_ETIPRES = space(1)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CATIPRIS = NVL(CATIPRIS,space(1))
          .w_CAKEYART = NVL(CAKEYART,space(41))
          if link_2_4_joined
            this.w_CAKEYART = NVL(CACODICE204,NVL(this.w_CAKEYART,space(41)))
            this.w_CADESSER = NVL(CADESART204,space(40))
            this.w_EDESAGG = NVL(CADESSUP204,space(0))
            this.w_ECACODART = NVL(CACODART204,space(20))
            this.w_ECADTOBSO = NVL(cp_ToDate(CADTOBSO204),ctod("  /  /  "))
          else
          .link_2_4('Load')
          endif
          .w_CACODSER = NVL(CACODSER,space(20))
          if link_2_5_joined
            this.w_CACODSER = NVL(CACODICE205,NVL(this.w_CACODSER,space(20)))
            this.w_CADESSER = NVL(CADESART205,space(40))
            this.w_RDESAGG = NVL(CADESSUP205,space(0))
            this.w_CACODART = NVL(CACODART205,space(20))
            this.w_RCACODART = NVL(CACODART205,space(20))
            this.w_RCADTOBSO = NVL(cp_ToDate(CADTOBSO205),ctod("  /  /  "))
          else
          .link_2_5('Load')
          endif
        .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
          .link_2_7('Load')
          .link_2_8('Load')
          .w_CADESSER = NVL(CADESSER,space(40))
          .w_CATIPRIG = NVL(CATIPRIG,space(1))
          .w_CATIPRI2 = NVL(CATIPRI2,space(1))
        .w_ATIPRIG = IIF(Empty(.w_CATIPRIG),.w_ARTIPRIG,.w_CATIPRIG)
        .w_ATIPRI2 = IIF(Empty(.w_CATIPRI2),.w_ARTIPRI2,.w_CATIPRI2)
        .w_ETIPRIG = IIF(Empty(.w_CATIPRIG),'D',.w_CATIPRIG)
        .w_ETIPRI2 = IIF(Empty(.w_CATIPRI2),'D',.w_CATIPRI2)
        .w_RTIPRIG = IIF(Empty(.w_CATIPRIG),'D',.w_CATIPRIG)
        .w_RTIPRI2 = IIF(Empty(.w_CATIPRI2),'D',.w_CATIPRI2)
          .w_CAFLDEFF = NVL(CAFLDEFF,space(1))
          .w_CAFLRESP = NVL(CAFLRESP,space(1))
          .w_CADESAGG = NVL(CADESAGG,space(0))
        .w_KADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_ORAINI = IIF(EMPTY(.w_CADATINI),'08',PADL(ALLTRIM(STR(HOUR(.w_CADATINI),2)),2,'0'))
        .w_MININI = RIGHT('00'+ALLTRIM(STR(MINUTE(.w_CADATINI),2)),2)
        .w_ACQU = iif(g_APPLICATION='ad hoc ENTERPRISE',g_CACQ,g_ACQU)
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate(IIF(.w_ISALT, "Tipo per la generazione dei documenti interni da assegnare all'attivit�", "Causale per la generazione dei documenti interni da assegnare all'attivit�"))
        .w_ColAttivita = IIF(.w_CACOLORE=0 or .w_CAPRIORI<>1 ,0,.w_CACOLORE)
        .oPgFrm.Page1.oPag.oObj_1_124.Calculate(.w_ColAttivita)
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate(.w_CACOLPOS)
        .w_ColAttivita = IIF(.w_CACOLORE=0 or .w_CAPRIORI<>1 ,0,.w_CACOLORE)
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_88.enabled = .oPgFrm.Page1.oPag.oBtn_1_88.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_89.enabled = .oPgFrm.Page1.oPag.oBtn_1_89.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_ISALT=.f.
      .w_ISAHE=.f.
      .w_CACODICE=space(20)
      .w_CADESCRI=space(254)
      .w_PARAGEN=space(10)
      .w_CARAGGST=space(1)
      .w_CARAGGST=space(1)
      .w_CATIPATT=space(5)
      .w_CAORASYS=space(1)
      .w_ORAINI=space(2)
      .w_MININI=space(2)
      .w_CADURORE=0
      .w_CADURMIN=0
      .w_CAGGPREA=0
      .w_CA_TIMER=space(1)
      .w_CAFLPDOC=space(1)
      .w_CASERDOC=space(10)
      .w_CADTOBSO=ctod("  /  /  ")
      .w_CASTAATT=space(1)
      .w_CASTAATT=space(1)
      .w_CAFLNOTI=space(1)
      .w_CAPRIORI=0
      .w_CAPRIEML=0
      .w_CAMINPRE=0
      .w_CAFLRINV=space(1)
      .w_CAFLTRIS=space(1)
      .w_CACHKOBB=space(1)
      .w_CACHKOBB=space(1)
      .w_CACHKNOM=space(1)
      .w_CADISPON=0
      .w_CAFLSTAT=space(1)
      .w_CAFLATRI=space(1)
      .w_CACAUABB=space(5)
      .w_CACHKINI=space(1)
      .w_CAPUBWEB=space(1)
      .w_COLORE=space(10)
      .w_COLOREP=space(10)
      .w_CACHKFOR=space(1)
      .w_CAFLPREA=space(1)
      .w_CAFLATTI=space(1)
      .w_CAFLATTN=0
      .w_CAFLPART=space(1)
      .w_CAFLPARN=0
      .w_CAFLPRAT=space(1)
      .w_CAFLPRAN=0
      .w_CAFLSOGG=space(1)
      .w_CAFLSOGN=0
      .w_CAFLGIUD=space(1)
      .w_CAFLGIUN=0
      .w_CAFLNOMI=space(1)
      .w_CAFLNOMN=0
      .w_CAFLNSAP=space(1)
      .w_ACQU=space(1)
      .w_CAFLANAL=space(1)
      .w_CACAUDOC=space(5)
      .w_CACAUACQ=space(5)
      .w_CATIPEVE=space(10)
      .w_CAFLGLIS=space(1)
      .w_DESCAUDI=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_CADATINI=ctot("")
      .w_DATINI=ctod("  /  /  ")
      .w_CAPROMEM=ctot("")
      .w_RESCHK=0
      .w_TIPOENTE=space(1)
      .w_DESTIPAT=space(50)
      .w_TDFLINTE=space(1)
      .w_CA__NOTE=space(0)
      .w_CAT_DOC=space(2)
      .w_TDCATDOC=space(2)
      .w_TDFLVEAC=space(1)
      .w_FLGLISV=space(1)
      .w_FLPRAT=space(1)
      .w_GESRIS=space(1)
      .w_PACHKFES=space(1)
      .w_DESCAUAC=space(35)
      .w_AFLVEAC=space(1)
      .w_VFLVEAC=space(1)
      .w_FLGLIS=space(1)
      .w_FLINTE=space(1)
      .w_CACOLORE=0
      .w_ColAttivita=0
      .w_CACOLPOS=0
      .w_ColAttivita=0
      .w_RIGA=space(1)
      .w_CPROWORD=10
      .w_CATIPRIS=space(1)
      .w_CAKEYART=space(41)
      .w_CACODSER=space(20)
      .w_CACODART=space(20)
      .w_ECACODART=space(20)
      .w_RCACODART=space(20)
      .w_ARTIPRIG=space(1)
      .w_ARTIPRI2=space(1)
      .w_CADESSER=space(40)
      .w_CATIPRIG=space(1)
      .w_CATIPRI2=space(1)
      .w_ATIPRIG=space(1)
      .w_ATIPRI2=space(1)
      .w_ETIPRIG=space(1)
      .w_ETIPRI2=space(1)
      .w_RTIPRIG=space(1)
      .w_RTIPRI2=space(1)
      .w_CAFLDEFF=space(1)
      .w_EDESAGG=space(0)
      .w_RDESAGG=space(0)
      .w_CAFLRESP=space(1)
      .w_ECADTOBSO=ctod("  /  /  ")
      .w_CADESAGG=space(0)
      .w_KADTOBSO=ctod("  /  /  ")
      .w_RCADTOBSO=ctod("  /  /  ")
      .w_DESCAABB=space(35)
      .w_DESABB=space(35)
      .w_CFLVEAC=space(1)
      .w_CA__NOTE=space(0)
      .w_CAUCON=space(5)
      .w_FLCOSE=space(1)
      .w_CAMODAPP=space(1)
      .w_CATIPPRA=space(10)
      .w_CAMATPRA=space(10)
      .w_CA__ENTE=space(10)
      .w_FLGIUD=space(1)
      .w_MATETIPOL=space(1)
      .w_RTIPRES=space(1)
      .w_ETIPRES=space(1)
      .w_PAFLVISI=space(1)
      .w_CAFLDFIN=space(1)
      .w_CAFLDINN=0
      .w_CAFLDINI=space(1)
      .w_CAFLDFNN=0
      .w_TDFLINTE1=space(1)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .w_ISALT = isalt()
        .w_ISAHE = isahe()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(3,4,.f.)
        .w_PARAGEN = i_CODAZI
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PARAGEN))
         .link_1_6('Full')
        endif
        .w_CARAGGST = IIF(.w_CAFLNSAP='S',IIF(.w_CARAGGST='C','G',.w_CARAGGST),IIF(.w_CARAGGST='c' OR EMPTY(.w_CARAGGST),'G',.w_CARAGGST))
        .w_CARAGGST = IIF(.w_CAFLNSAP='S',IIF(.w_CARAGGST='C','G',.w_CARAGGST),IIF(.w_CARAGGST='c' OR EMPTY(.w_CARAGGST),'G',.w_CARAGGST))
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CATIPATT))
         .link_1_9('Full')
        endif
        .w_CAORASYS = 'N'
        .w_ORAINI = IIF(EMPTY(.w_CADATINI),'08',PADL(ALLTRIM(STR(HOUR(.w_CADATINI),2)),2,'0'))
        .w_MININI = RIGHT('00'+ALLTRIM(STR(MINUTE(.w_CADATINI),2)),2)
        .DoRTCalc(12,12,.f.)
        .w_CADURMIN = 30
        .DoRTCalc(14,15,.f.)
        .w_CAFLPDOC = 'N'
        .DoRTCalc(17,18,.f.)
        .w_CASTAATT = IIF(.w_CAFLNSAP='S',IIF(.w_CASTAATT='P','D',.w_CASTAATT),IIF(.w_CASTAATT='P' OR EMPTY(.w_CASTAATT),'D',.w_CASTAATT))
        .w_CASTAATT = IIF(.w_CAFLNSAP='S',IIF(.w_CASTAATT='P','D',.w_CASTAATT),IIF(.w_CASTAATT='P' OR EMPTY(.w_CASTAATT),'D',.w_CASTAATT))
        .w_CAFLNOTI = IIF(.w_CARAGGST$'SF','S','N')
        .w_CAPRIORI = 1
        .w_CAPRIEML = 3
        .w_CAMINPRE = -1
        .w_CAFLRINV = IIF(.w_CARAGGST='U','S','N')
        .w_CAFLTRIS = IIF(.w_CARAGGST='U','S','N')
        .w_CACHKOBB = IIF(.w_CARAGGST='U','P',IIF(.w_CARAGGST='M',' ','N'))
        .w_CACHKOBB = iif(.w_ISAHE,'N',IIF(.w_CARAGGST='U','P',IIF(.w_CARAGGST$'MA',' ','N')))
        .w_CACHKNOM = IIF(.w_CARAGGST='U','P',IIF(.w_CARAGGST$'MA',' ','N'))
        .w_CADISPON = 2
        .w_CAFLSTAT = 'N'
        .w_CAFLATRI = 'N'
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_CACAUABB))
         .link_1_37('Full')
        endif
        .w_CACHKINI = 'N'
        .w_CAPUBWEB = 'S'
        .DoRTCalc(36,37,.f.)
        .w_CACHKFOR = iif( .w_CARAGGST='U' , 'S' , ' ')
        .w_CAFLPREA = 'N'
        .w_CAFLATTI = 'S'
        .w_CAFLATTN = 1
        .w_CAFLPART = 'S'
        .w_CAFLPARN = 2
        .w_CAFLPRAT = 'N'
        .DoRTCalc(45,45,.f.)
        .w_CAFLSOGG = 'N'
        .DoRTCalc(47,47,.f.)
        .w_CAFLGIUD = 'N'
        .DoRTCalc(49,49,.f.)
        .w_CAFLNOMI = 'N'
        .DoRTCalc(51,51,.f.)
        .w_CAFLNSAP = IIF(.w_CARAGGST$'MA','S','N')
        .w_ACQU = iif(g_APPLICATION='ad hoc ENTERPRISE',g_CACQ,g_ACQU)
        .w_CAFLANAL = 'N'
        .w_CACAUDOC = iif(.w_CAFLNSAP="S" or .w_ISAHE, space(5), iif(.w_ISALT, TipoPref("T") , Docpref("DI", "V") ) )
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_CACAUDOC))
         .link_1_59('Full')
        endif
        .w_CACAUACQ = space(5)
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_CACAUACQ))
         .link_1_60('Full')
        endif
        .DoRTCalc(57,57,.f.)
        .w_CAFLGLIS = 'N'
        .DoRTCalc(59,59,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(61,61,.f.)
        .w_DATINI = i_inidat
        .DoRTCalc(63,63,.f.)
        .w_RESCHK = 0
        .w_TIPOENTE = ' '
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate(IIF(.w_ISALT, "Tipo per la generazione dei documenti interni da assegnare all'attivit�", "Causale per la generazione dei documenti interni da assegnare all'attivit�"))
        .DoRTCalc(66,68,.f.)
        .w_CAT_DOC = IIF(.w_ISALT, 'DI', SPACE(2) )
        .DoRTCalc(70,80,.f.)
        .w_CACOLORE = IIF(.w_CAPRIORI<>1 ,0,.w_CACOLORE)
        .w_ColAttivita = IIF(.w_CACOLORE=0 or .w_CAPRIORI<>1 ,0,.w_CACOLORE)
        .oPgFrm.Page1.oPag.oObj_1_124.Calculate(.w_ColAttivita)
        .w_CACOLPOS = g_PostInColor
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate(.w_CACOLPOS)
        .w_ColAttivita = IIF(.w_CACOLORE=0 or .w_CAPRIORI<>1 ,0,.w_CACOLORE)
        .DoRTCalc(85,86,.f.)
        .w_CATIPRIS = 'P'
        .DoRTCalc(88,88,.f.)
        if not(empty(.w_CAKEYART))
         .link_2_4('Full')
        endif
        .DoRTCalc(89,89,.f.)
        if not(empty(.w_CACODSER))
         .link_2_5('Full')
        endif
        .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
        .DoRTCalc(91,91,.f.)
        if not(empty(.w_ECACODART))
         .link_2_7('Full')
        endif
        .DoRTCalc(92,92,.f.)
        if not(empty(.w_RCACODART))
         .link_2_8('Full')
        endif
        .DoRTCalc(93,97,.f.)
        .w_ATIPRIG = IIF(Empty(.w_CATIPRIG),.w_ARTIPRIG,.w_CATIPRIG)
        .w_ATIPRI2 = IIF(Empty(.w_CATIPRI2),.w_ARTIPRI2,.w_CATIPRI2)
        .w_ETIPRIG = IIF(Empty(.w_CATIPRIG),'D',.w_CATIPRIG)
        .w_ETIPRI2 = IIF(Empty(.w_CATIPRI2),'D',.w_CATIPRI2)
        .w_RTIPRIG = IIF(Empty(.w_CATIPRIG),'D',.w_CATIPRIG)
        .w_RTIPRI2 = IIF(Empty(.w_CATIPRI2),'D',.w_CATIPRI2)
        .DoRTCalc(104,108,.f.)
        .w_CADESAGG = IIF(.w_ISAHE, .w_EDESAGG, .w_RDESAGG)
        .w_KADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
        .DoRTCalc(111,116,.f.)
        if not(empty(.w_CAUCON))
         .link_1_137('Full')
        endif
        .DoRTCalc(117,117,.f.)
        .w_CAMODAPP = 'P'
        .DoRTCalc(119,119,.f.)
        if not(empty(.w_CATIPPRA))
         .link_1_139('Full')
        endif
        .DoRTCalc(120,120,.f.)
        if not(empty(.w_CAMATPRA))
         .link_1_140('Full')
        endif
        .DoRTCalc(121,121,.f.)
        if not(empty(.w_CA__ENTE))
         .link_1_141('Full')
        endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAUMATTI')
    this.DoRTCalc(122,135,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_88.enabled = this.oPgFrm.Page1.oPag.oBtn_1_88.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_89.enabled = this.oPgFrm.Page1.oPag.oBtn_1_89.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsag_mca
    *** serve per nascondere le linee verticali della griglia sotto le note
    local L_CTRL
    L_CTRL=this.GetCtrl("w_CA__NOTE")
    L_CTRL.ZOrder(0)
    L_CTRL=.null.
    release L_CTRL
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCACODICE_1_3.enabled = i_bVal
      .Page1.oPag.oCADESCRI_1_5.enabled = i_bVal
      .Page1.oPag.oCARAGGST_1_7.enabled = i_bVal
      .Page1.oPag.oCARAGGST_1_8.enabled = i_bVal
      .Page1.oPag.oCATIPATT_1_9.enabled = i_bVal
      .Page1.oPag.oCAORASYS_1_10.enabled = i_bVal
      .Page1.oPag.oORAINI_1_12.enabled = i_bVal
      .Page1.oPag.oMININI_1_13.enabled = i_bVal
      .Page1.oPag.oCADURORE_1_14.enabled = i_bVal
      .Page1.oPag.oCADURMIN_1_15.enabled = i_bVal
      .Page1.oPag.oCAGGPREA_1_16.enabled = i_bVal
      .Page1.oPag.oCA_TIMER_1_17.enabled = i_bVal
      .Page1.oPag.oCAFLPDOC_1_18.enabled = i_bVal
      .Page1.oPag.oCASERDOC_1_19.enabled = i_bVal
      .Page1.oPag.oCADTOBSO_1_20.enabled = i_bVal
      .Page1.oPag.oCASTAATT_1_21.enabled = i_bVal
      .Page1.oPag.oCASTAATT_1_22.enabled = i_bVal
      .Page1.oPag.oCAFLNOTI_1_23.enabled = i_bVal
      .Page1.oPag.oCAPRIORI_1_25.enabled = i_bVal
      .Page1.oPag.oCAPRIEML_1_27.enabled = i_bVal
      .Page1.oPag.oCAMINPRE_1_28.enabled = i_bVal
      .Page1.oPag.oCAFLRINV_1_29.enabled = i_bVal
      .Page1.oPag.oCAFLTRIS_1_30.enabled = i_bVal
      .Page1.oPag.oCACHKOBB_1_31.enabled = i_bVal
      .Page1.oPag.oCACHKOBB_1_32.enabled = i_bVal
      .Page1.oPag.oCACHKNOM_1_33.enabled = i_bVal
      .Page1.oPag.oCADISPON_1_34.enabled = i_bVal
      .Page1.oPag.oCAFLSTAT_1_35.enabled = i_bVal
      .Page1.oPag.oCAFLATRI_1_36.enabled = i_bVal
      .Page1.oPag.oCACAUABB_1_37.enabled = i_bVal
      .Page1.oPag.oCACHKINI_1_38.enabled = i_bVal
      .Page1.oPag.oCAPUBWEB_1_39.enabled = i_bVal
      .Page1.oPag.oCACHKFOR_1_42.enabled = i_bVal
      .Page1.oPag.oCAFLNSAP_1_56.enabled = i_bVal
      .Page1.oPag.oCAFLANAL_1_58.enabled = i_bVal
      .Page1.oPag.oCACAUDOC_1_59.enabled = i_bVal
      .Page1.oPag.oCACAUACQ_1_60.enabled = i_bVal
      .Page1.oPag.oCATIPEVE_1_61.enabled = i_bVal
      .Page1.oPag.oCA__NOTE_1_97.enabled = i_bVal
      .Page1.oPag.oCADESAGG_2_25.enabled = i_bVal
      .Page3.oPag.oCA__NOTE_5_1.enabled = i_bVal
      .Page2.oPag.oCAMODAPP_4_2.enabled = i_bVal
      .Page1.oPag.oCATIPPRA_1_139.enabled = i_bVal
      .Page1.oPag.oCAMATPRA_1_140.enabled = i_bVal
      .Page1.oPag.oCA__ENTE_1_141.enabled = i_bVal
      .Page1.oPag.oBtn_1_24.enabled = i_bVal
      .Page1.oPag.oBtn_1_26.enabled = i_bVal
      .Page1.oPag.oBtn_1_88.enabled = .Page1.oPag.oBtn_1_88.mCond()
      .Page1.oPag.oBtn_1_89.enabled = .Page1.oPag.oBtn_1_89.mCond()
      .Page2.oPag.oBtn_4_4.enabled = i_bVal
      .Page1.oPag.oObj_1_124.enabled = i_bVal
      .Page1.oPag.oObj_1_127.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCACODICE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCACODICE_1_3.enabled = .t.
        .Page1.oPag.oCADESCRI_1_5.enabled = .t.
      endif
    endwith
    this.gsag_mdp.SetStatus(i_cOp)
    this.GSAG_MUM.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CAUMATTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gsag_mdp.SetChildrenStatus(i_cOp)
  *  this.GSAG_MUM.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACODICE,"CACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADESCRI,"CADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CARAGGST,"CARAGGST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CARAGGST,"CARAGGST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPATT,"CATIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAORASYS,"CAORASYS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADURORE,"CADURORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADURMIN,"CADURMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAGGPREA,"CAGGPREA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CA_TIMER,"CA_TIMER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLPDOC,"CAFLPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CASERDOC,"CASERDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADTOBSO,"CADTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CASTAATT,"CASTAATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CASTAATT,"CASTAATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLNOTI,"CAFLNOTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAPRIORI,"CAPRIORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAPRIEML,"CAPRIEML",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAMINPRE,"CAMINPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLRINV,"CAFLRINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLTRIS,"CAFLTRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACHKOBB,"CACHKOBB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACHKOBB,"CACHKOBB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACHKNOM,"CACHKNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADISPON,"CADISPON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLSTAT,"CAFLSTAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLATRI,"CAFLATRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACAUABB,"CACAUABB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACHKINI,"CACHKINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAPUBWEB,"CAPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACHKFOR,"CACHKFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLPREA,"CAFLPREA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLATTI,"CAFLATTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLATTN,"CAFLATTN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLPART,"CAFLPART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLPARN,"CAFLPARN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLPRAT,"CAFLPRAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLPRAN,"CAFLPRAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLSOGG,"CAFLSOGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLSOGN,"CAFLSOGN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLGIUD,"CAFLGIUD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLGIUN,"CAFLGIUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLNOMI,"CAFLNOMI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLNOMN,"CAFLNOMN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLNSAP,"CAFLNSAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLANAL,"CAFLANAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACAUDOC,"CACAUDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACAUACQ,"CACAUACQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPEVE,"CATIPEVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLGLIS,"CAFLGLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CADATINI,"CADATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAPROMEM,"CAPROMEM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CA__NOTE,"CA__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACOLORE,"CACOLORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CACOLPOS,"CACOLPOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CA__NOTE,"CA__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAMODAPP,"CAMODAPP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CATIPPRA,"CATIPPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAMATPRA,"CAMATPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CA__ENTE,"CA__ENTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLDFIN,"CAFLDFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLDINN,"CAFLDINN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLDINI,"CAFLDINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAFLDFNN,"CAFLDFNN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    i_lTable = "CAUMATTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CAUMATTI_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAG_STA with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_RIGA C(1);
      ,t_CPROWORD N(4);
      ,t_CAKEYART C(41);
      ,t_CACODSER C(20);
      ,t_CADESSER C(40);
      ,t_ATIPRIG N(3);
      ,t_ATIPRI2 N(3);
      ,t_ETIPRIG N(3);
      ,t_RTIPRIG N(3);
      ,t_CAFLRESP N(3);
      ,t_CADESAGG M(10);
      ,t_RTIPRES N(3);
      ,t_ETIPRES N(3);
      ,CPROWNUM N(10);
      ,t_CATIPRIS C(1);
      ,t_CACODART C(20);
      ,t_ECACODART C(20);
      ,t_RCACODART C(20);
      ,t_ARTIPRIG C(1);
      ,t_ARTIPRI2 C(1);
      ,t_CATIPRIG C(1);
      ,t_CATIPRI2 C(1);
      ,t_ETIPRI2 C(1);
      ,t_RTIPRI2 C(1);
      ,t_CAFLDEFF C(1);
      ,t_ECADTOBSO D(8);
      ,t_KADTOBSO D(8);
      ,t_RCADTOBSO D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mcabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRIGA_2_1.controlsource=this.cTrsName+'.t_RIGA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAKEYART_2_4.controlsource=this.cTrsName+'.t_CAKEYART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCACODSER_2_5.controlsource=this.cTrsName+'.t_CACODSER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCADESSER_2_11.controlsource=this.cTrsName+'.t_CADESSER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRIG_2_14.controlsource=this.cTrsName+'.t_ATIPRIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRI2_2_15.controlsource=this.cTrsName+'.t_ATIPRI2'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRIG_2_16.controlsource=this.cTrsName+'.t_ETIPRIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRIG_2_18.controlsource=this.cTrsName+'.t_RTIPRIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLRESP_2_23.controlsource=this.cTrsName+'.t_CAFLRESP'
    this.oPgFRm.Page1.oPag.oCADESAGG_2_25.controlsource=this.cTrsName+'.t_CADESAGG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRES_2_29.controlsource=this.cTrsName+'.t_RTIPRES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRES_2_30.controlsource=this.cTrsName+'.t_ETIPRES'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(72)
    this.AddVLine(222)
    this.AddVLine(441)
    this.AddVLine(559)
    this.AddVLine(678)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIGA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CAUMATTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAUMATTI')
        i_extval=cp_InsertValODBCExtFlds(this,'CAUMATTI')
        local i_cFld
        i_cFld=" "+;
                  "(CACODICE,CADESCRI,CARAGGST,CATIPATT,CAORASYS"+;
                  ",CADURORE,CADURMIN,CAGGPREA,CA_TIMER,CAFLPDOC"+;
                  ",CASERDOC,CADTOBSO,CASTAATT,CAFLNOTI,CAPRIORI"+;
                  ",CAPRIEML,CAMINPRE,CAFLRINV,CAFLTRIS,CACHKOBB"+;
                  ",CACHKNOM,CADISPON,CAFLSTAT,CAFLATRI,CACAUABB"+;
                  ",CACHKINI,CAPUBWEB,CACHKFOR,CAFLPREA,CAFLATTI"+;
                  ",CAFLATTN,CAFLPART,CAFLPARN,CAFLPRAT,CAFLPRAN"+;
                  ",CAFLSOGG,CAFLSOGN,CAFLGIUD,CAFLGIUN,CAFLNOMI"+;
                  ",CAFLNOMN,CAFLNSAP,CAFLANAL,CACAUDOC,CACAUACQ"+;
                  ",CATIPEVE,CAFLGLIS,CADATINI,CAPROMEM,CA__NOTE"+;
                  ",CACOLORE,CACOLPOS,CAMODAPP,CATIPPRA,CAMATPRA"+;
                  ",CA__ENTE,CAFLDFIN,CAFLDINN,CAFLDINI,CAFLDFNN"+;
                  ",UTCC,UTDC,UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_CACODICE)+;
                    ","+cp_ToStrODBC(this.w_CADESCRI)+;
                    ","+cp_ToStrODBC(this.w_CARAGGST)+;
                    ","+cp_ToStrODBCNull(this.w_CATIPATT)+;
                    ","+cp_ToStrODBC(this.w_CAORASYS)+;
                    ","+cp_ToStrODBC(this.w_CADURORE)+;
                    ","+cp_ToStrODBC(this.w_CADURMIN)+;
                    ","+cp_ToStrODBC(this.w_CAGGPREA)+;
                    ","+cp_ToStrODBC(this.w_CA_TIMER)+;
                    ","+cp_ToStrODBC(this.w_CAFLPDOC)+;
                    ","+cp_ToStrODBC(this.w_CASERDOC)+;
                    ","+cp_ToStrODBC(this.w_CADTOBSO)+;
                    ","+cp_ToStrODBC(this.w_CASTAATT)+;
                    ","+cp_ToStrODBC(this.w_CAFLNOTI)+;
                    ","+cp_ToStrODBC(this.w_CAPRIORI)+;
                    ","+cp_ToStrODBC(this.w_CAPRIEML)+;
                    ","+cp_ToStrODBC(this.w_CAMINPRE)+;
                    ","+cp_ToStrODBC(this.w_CAFLRINV)+;
                    ","+cp_ToStrODBC(this.w_CAFLTRIS)+;
                    ","+cp_ToStrODBC(this.w_CACHKOBB)+;
                    ","+cp_ToStrODBC(this.w_CACHKNOM)+;
                    ","+cp_ToStrODBC(this.w_CADISPON)+;
                    ","+cp_ToStrODBC(this.w_CAFLSTAT)+;
                    ","+cp_ToStrODBC(this.w_CAFLATRI)+;
                    ","+cp_ToStrODBCNull(this.w_CACAUABB)+;
                    ","+cp_ToStrODBC(this.w_CACHKINI)+;
                    ","+cp_ToStrODBC(this.w_CAPUBWEB)+;
                    ","+cp_ToStrODBC(this.w_CACHKFOR)+;
                    ","+cp_ToStrODBC(this.w_CAFLPREA)+;
                    ","+cp_ToStrODBC(this.w_CAFLATTI)+;
                    ","+cp_ToStrODBC(this.w_CAFLATTN)+;
                    ","+cp_ToStrODBC(this.w_CAFLPART)+;
                    ","+cp_ToStrODBC(this.w_CAFLPARN)+;
                    ","+cp_ToStrODBC(this.w_CAFLPRAT)+;
                    ","+cp_ToStrODBC(this.w_CAFLPRAN)+;
                    ","+cp_ToStrODBC(this.w_CAFLSOGG)+;
                    ","+cp_ToStrODBC(this.w_CAFLSOGN)+;
                    ","+cp_ToStrODBC(this.w_CAFLGIUD)+;
                    ","+cp_ToStrODBC(this.w_CAFLGIUN)+;
                    ","+cp_ToStrODBC(this.w_CAFLNOMI)+;
                    ","+cp_ToStrODBC(this.w_CAFLNOMN)+;
                    ","+cp_ToStrODBC(this.w_CAFLNSAP)+;
                    ","+cp_ToStrODBC(this.w_CAFLANAL)+;
                    ","+cp_ToStrODBCNull(this.w_CACAUDOC)+;
                    ","+cp_ToStrODBCNull(this.w_CACAUACQ)+;
                    ","+cp_ToStrODBC(this.w_CATIPEVE)+;
                    ","+cp_ToStrODBC(this.w_CAFLGLIS)+;
                    ","+cp_ToStrODBC(this.w_CADATINI)+;
                    ","+cp_ToStrODBC(this.w_CAPROMEM)+;
                    ","+cp_ToStrODBC(this.w_CA__NOTE)+;
                    ","+cp_ToStrODBC(this.w_CACOLORE)+;
                    ","+cp_ToStrODBC(this.w_CACOLPOS)+;
                    ","+cp_ToStrODBC(this.w_CAMODAPP)+;
                    ","+cp_ToStrODBCNull(this.w_CATIPPRA)+;
                    ","+cp_ToStrODBCNull(this.w_CAMATPRA)+;
                    ","+cp_ToStrODBCNull(this.w_CA__ENTE)+;
                    ","+cp_ToStrODBC(this.w_CAFLDFIN)+;
                    ","+cp_ToStrODBC(this.w_CAFLDINN)+;
                    ","+cp_ToStrODBC(this.w_CAFLDINI)+;
                    ","+cp_ToStrODBC(this.w_CAFLDFNN)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAUMATTI')
        i_extval=cp_InsertValVFPExtFlds(this,'CAUMATTI')
        cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_CACODICE)
        INSERT INTO (i_cTable);
              (CACODICE,CADESCRI,CARAGGST,CATIPATT,CAORASYS,CADURORE,CADURMIN,CAGGPREA,CA_TIMER,CAFLPDOC,CASERDOC,CADTOBSO,CASTAATT,CAFLNOTI,CAPRIORI,CAPRIEML,CAMINPRE,CAFLRINV,CAFLTRIS,CACHKOBB,CACHKNOM,CADISPON,CAFLSTAT,CAFLATRI,CACAUABB,CACHKINI,CAPUBWEB,CACHKFOR,CAFLPREA,CAFLATTI,CAFLATTN,CAFLPART,CAFLPARN,CAFLPRAT,CAFLPRAN,CAFLSOGG,CAFLSOGN,CAFLGIUD,CAFLGIUN,CAFLNOMI,CAFLNOMN,CAFLNSAP,CAFLANAL,CACAUDOC,CACAUACQ,CATIPEVE,CAFLGLIS,CADATINI,CAPROMEM,CA__NOTE,CACOLORE,CACOLPOS,CAMODAPP,CATIPPRA,CAMATPRA,CA__ENTE,CAFLDFIN,CAFLDINN,CAFLDINI,CAFLDFNN,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_CACODICE;
                  ,this.w_CADESCRI;
                  ,this.w_CARAGGST;
                  ,this.w_CATIPATT;
                  ,this.w_CAORASYS;
                  ,this.w_CADURORE;
                  ,this.w_CADURMIN;
                  ,this.w_CAGGPREA;
                  ,this.w_CA_TIMER;
                  ,this.w_CAFLPDOC;
                  ,this.w_CASERDOC;
                  ,this.w_CADTOBSO;
                  ,this.w_CASTAATT;
                  ,this.w_CAFLNOTI;
                  ,this.w_CAPRIORI;
                  ,this.w_CAPRIEML;
                  ,this.w_CAMINPRE;
                  ,this.w_CAFLRINV;
                  ,this.w_CAFLTRIS;
                  ,this.w_CACHKOBB;
                  ,this.w_CACHKNOM;
                  ,this.w_CADISPON;
                  ,this.w_CAFLSTAT;
                  ,this.w_CAFLATRI;
                  ,this.w_CACAUABB;
                  ,this.w_CACHKINI;
                  ,this.w_CAPUBWEB;
                  ,this.w_CACHKFOR;
                  ,this.w_CAFLPREA;
                  ,this.w_CAFLATTI;
                  ,this.w_CAFLATTN;
                  ,this.w_CAFLPART;
                  ,this.w_CAFLPARN;
                  ,this.w_CAFLPRAT;
                  ,this.w_CAFLPRAN;
                  ,this.w_CAFLSOGG;
                  ,this.w_CAFLSOGN;
                  ,this.w_CAFLGIUD;
                  ,this.w_CAFLGIUN;
                  ,this.w_CAFLNOMI;
                  ,this.w_CAFLNOMN;
                  ,this.w_CAFLNSAP;
                  ,this.w_CAFLANAL;
                  ,this.w_CACAUDOC;
                  ,this.w_CACAUACQ;
                  ,this.w_CATIPEVE;
                  ,this.w_CAFLGLIS;
                  ,this.w_CADATINI;
                  ,this.w_CAPROMEM;
                  ,this.w_CA__NOTE;
                  ,this.w_CACOLORE;
                  ,this.w_CACOLPOS;
                  ,this.w_CAMODAPP;
                  ,this.w_CATIPPRA;
                  ,this.w_CAMATPRA;
                  ,this.w_CA__ENTE;
                  ,this.w_CAFLDFIN;
                  ,this.w_CAFLDINN;
                  ,this.w_CAFLDINI;
                  ,this.w_CAFLDFNN;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAU_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_ATTI_IDX,2])
      *
      * insert into CAU_ATTI
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CACODICE,CPROWORD,CATIPRIS,CAKEYART,CACODSER"+;
                  ",CADESSER,CATIPRIG,CATIPRI2,CAFLDEFF,CAFLRESP"+;
                  ",CADESAGG,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CACODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CATIPRIS)+","+cp_ToStrODBCNull(this.w_CAKEYART)+","+cp_ToStrODBCNull(this.w_CACODSER)+;
             ","+cp_ToStrODBC(this.w_CADESSER)+","+cp_ToStrODBC(this.w_CATIPRIG)+","+cp_ToStrODBC(this.w_CATIPRI2)+","+cp_ToStrODBC(this.w_CAFLDEFF)+","+cp_ToStrODBC(this.w_CAFLRESP)+;
             ","+cp_ToStrODBC(this.w_CADESAGG)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CACODICE',this.w_CACODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CACODICE,this.w_CPROWORD,this.w_CATIPRIS,this.w_CAKEYART,this.w_CACODSER"+;
                ",this.w_CADESSER,this.w_CATIPRIG,this.w_CATIPRI2,this.w_CAFLDEFF,this.w_CAFLRESP"+;
                ",this.w_CADESAGG,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CAUMATTI
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CAUMATTI')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CADESCRI="+cp_ToStrODBC(this.w_CADESCRI)+;
             ",CARAGGST="+cp_ToStrODBC(this.w_CARAGGST)+;
             ",CATIPATT="+cp_ToStrODBCNull(this.w_CATIPATT)+;
             ",CAORASYS="+cp_ToStrODBC(this.w_CAORASYS)+;
             ",CADURORE="+cp_ToStrODBC(this.w_CADURORE)+;
             ",CADURMIN="+cp_ToStrODBC(this.w_CADURMIN)+;
             ",CAGGPREA="+cp_ToStrODBC(this.w_CAGGPREA)+;
             ",CA_TIMER="+cp_ToStrODBC(this.w_CA_TIMER)+;
             ",CAFLPDOC="+cp_ToStrODBC(this.w_CAFLPDOC)+;
             ",CASERDOC="+cp_ToStrODBC(this.w_CASERDOC)+;
             ",CADTOBSO="+cp_ToStrODBC(this.w_CADTOBSO)+;
             ",CASTAATT="+cp_ToStrODBC(this.w_CASTAATT)+;
             ",CAFLNOTI="+cp_ToStrODBC(this.w_CAFLNOTI)+;
             ",CAPRIORI="+cp_ToStrODBC(this.w_CAPRIORI)+;
             ",CAPRIEML="+cp_ToStrODBC(this.w_CAPRIEML)+;
             ",CAMINPRE="+cp_ToStrODBC(this.w_CAMINPRE)+;
             ",CAFLRINV="+cp_ToStrODBC(this.w_CAFLRINV)+;
             ",CAFLTRIS="+cp_ToStrODBC(this.w_CAFLTRIS)+;
             ",CACHKOBB="+cp_ToStrODBC(this.w_CACHKOBB)+;
             ",CACHKNOM="+cp_ToStrODBC(this.w_CACHKNOM)+;
             ",CADISPON="+cp_ToStrODBC(this.w_CADISPON)+;
             ",CAFLSTAT="+cp_ToStrODBC(this.w_CAFLSTAT)+;
             ",CAFLATRI="+cp_ToStrODBC(this.w_CAFLATRI)+;
             ",CACAUABB="+cp_ToStrODBCNull(this.w_CACAUABB)+;
             ",CACHKINI="+cp_ToStrODBC(this.w_CACHKINI)+;
             ",CAPUBWEB="+cp_ToStrODBC(this.w_CAPUBWEB)+;
             ",CACHKFOR="+cp_ToStrODBC(this.w_CACHKFOR)+;
             ",CAFLPREA="+cp_ToStrODBC(this.w_CAFLPREA)+;
             ",CAFLATTI="+cp_ToStrODBC(this.w_CAFLATTI)+;
             ",CAFLATTN="+cp_ToStrODBC(this.w_CAFLATTN)+;
             ",CAFLPART="+cp_ToStrODBC(this.w_CAFLPART)+;
             ",CAFLPARN="+cp_ToStrODBC(this.w_CAFLPARN)+;
             ",CAFLPRAT="+cp_ToStrODBC(this.w_CAFLPRAT)+;
             ",CAFLPRAN="+cp_ToStrODBC(this.w_CAFLPRAN)+;
             ",CAFLSOGG="+cp_ToStrODBC(this.w_CAFLSOGG)+;
             ",CAFLSOGN="+cp_ToStrODBC(this.w_CAFLSOGN)+;
             ",CAFLGIUD="+cp_ToStrODBC(this.w_CAFLGIUD)+;
             ",CAFLGIUN="+cp_ToStrODBC(this.w_CAFLGIUN)+;
             ",CAFLNOMI="+cp_ToStrODBC(this.w_CAFLNOMI)+;
             ",CAFLNOMN="+cp_ToStrODBC(this.w_CAFLNOMN)+;
             ",CAFLNSAP="+cp_ToStrODBC(this.w_CAFLNSAP)+;
             ",CAFLANAL="+cp_ToStrODBC(this.w_CAFLANAL)+;
             ",CACAUDOC="+cp_ToStrODBCNull(this.w_CACAUDOC)+;
             ",CACAUACQ="+cp_ToStrODBCNull(this.w_CACAUACQ)+;
             ",CATIPEVE="+cp_ToStrODBC(this.w_CATIPEVE)+;
             ",CAFLGLIS="+cp_ToStrODBC(this.w_CAFLGLIS)+;
             ",CADATINI="+cp_ToStrODBC(this.w_CADATINI)+;
             ",CAPROMEM="+cp_ToStrODBC(this.w_CAPROMEM)+;
             ",CA__NOTE="+cp_ToStrODBC(this.w_CA__NOTE)+;
             ",CACOLORE="+cp_ToStrODBC(this.w_CACOLORE)+;
             ",CACOLPOS="+cp_ToStrODBC(this.w_CACOLPOS)+;
             ",CAMODAPP="+cp_ToStrODBC(this.w_CAMODAPP)+;
             ",CATIPPRA="+cp_ToStrODBCNull(this.w_CATIPPRA)+;
             ",CAMATPRA="+cp_ToStrODBCNull(this.w_CAMATPRA)+;
             ",CA__ENTE="+cp_ToStrODBCNull(this.w_CA__ENTE)+;
             ",CAFLDFIN="+cp_ToStrODBC(this.w_CAFLDFIN)+;
             ",CAFLDINN="+cp_ToStrODBC(this.w_CAFLDINN)+;
             ",CAFLDINI="+cp_ToStrODBC(this.w_CAFLDINI)+;
             ",CAFLDFNN="+cp_ToStrODBC(this.w_CAFLDFNN)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CAUMATTI')
          i_cWhere = cp_PKFox(i_cTable  ,'CACODICE',this.w_CACODICE  )
          UPDATE (i_cTable) SET;
              CADESCRI=this.w_CADESCRI;
             ,CARAGGST=this.w_CARAGGST;
             ,CATIPATT=this.w_CATIPATT;
             ,CAORASYS=this.w_CAORASYS;
             ,CADURORE=this.w_CADURORE;
             ,CADURMIN=this.w_CADURMIN;
             ,CAGGPREA=this.w_CAGGPREA;
             ,CA_TIMER=this.w_CA_TIMER;
             ,CAFLPDOC=this.w_CAFLPDOC;
             ,CASERDOC=this.w_CASERDOC;
             ,CADTOBSO=this.w_CADTOBSO;
             ,CASTAATT=this.w_CASTAATT;
             ,CAFLNOTI=this.w_CAFLNOTI;
             ,CAPRIORI=this.w_CAPRIORI;
             ,CAPRIEML=this.w_CAPRIEML;
             ,CAMINPRE=this.w_CAMINPRE;
             ,CAFLRINV=this.w_CAFLRINV;
             ,CAFLTRIS=this.w_CAFLTRIS;
             ,CACHKOBB=this.w_CACHKOBB;
             ,CACHKNOM=this.w_CACHKNOM;
             ,CADISPON=this.w_CADISPON;
             ,CAFLSTAT=this.w_CAFLSTAT;
             ,CAFLATRI=this.w_CAFLATRI;
             ,CACAUABB=this.w_CACAUABB;
             ,CACHKINI=this.w_CACHKINI;
             ,CAPUBWEB=this.w_CAPUBWEB;
             ,CACHKFOR=this.w_CACHKFOR;
             ,CAFLPREA=this.w_CAFLPREA;
             ,CAFLATTI=this.w_CAFLATTI;
             ,CAFLATTN=this.w_CAFLATTN;
             ,CAFLPART=this.w_CAFLPART;
             ,CAFLPARN=this.w_CAFLPARN;
             ,CAFLPRAT=this.w_CAFLPRAT;
             ,CAFLPRAN=this.w_CAFLPRAN;
             ,CAFLSOGG=this.w_CAFLSOGG;
             ,CAFLSOGN=this.w_CAFLSOGN;
             ,CAFLGIUD=this.w_CAFLGIUD;
             ,CAFLGIUN=this.w_CAFLGIUN;
             ,CAFLNOMI=this.w_CAFLNOMI;
             ,CAFLNOMN=this.w_CAFLNOMN;
             ,CAFLNSAP=this.w_CAFLNSAP;
             ,CAFLANAL=this.w_CAFLANAL;
             ,CACAUDOC=this.w_CACAUDOC;
             ,CACAUACQ=this.w_CACAUACQ;
             ,CATIPEVE=this.w_CATIPEVE;
             ,CAFLGLIS=this.w_CAFLGLIS;
             ,CADATINI=this.w_CADATINI;
             ,CAPROMEM=this.w_CAPROMEM;
             ,CA__NOTE=this.w_CA__NOTE;
             ,CACOLORE=this.w_CACOLORE;
             ,CACOLPOS=this.w_CACOLPOS;
             ,CAMODAPP=this.w_CAMODAPP;
             ,CATIPPRA=this.w_CATIPPRA;
             ,CAMATPRA=this.w_CAMATPRA;
             ,CA__ENTE=this.w_CA__ENTE;
             ,CAFLDFIN=this.w_CAFLDFIN;
             ,CAFLDINN=this.w_CAFLDINN;
             ,CAFLDINI=this.w_CAFLDINI;
             ,CAFLDFNN=this.w_CAFLDFNN;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) and (not (Empty(t_CACODSER)) OR not (Empty(t_CAKEYART)))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CAU_ATTI_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CAU_ATTI_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from CAU_ATTI
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CAU_ATTI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CATIPRIS="+cp_ToStrODBC(this.w_CATIPRIS)+;
                     ",CAKEYART="+cp_ToStrODBCNull(this.w_CAKEYART)+;
                     ",CACODSER="+cp_ToStrODBCNull(this.w_CACODSER)+;
                     ",CADESSER="+cp_ToStrODBC(this.w_CADESSER)+;
                     ",CATIPRIG="+cp_ToStrODBC(this.w_CATIPRIG)+;
                     ",CATIPRI2="+cp_ToStrODBC(this.w_CATIPRI2)+;
                     ",CAFLDEFF="+cp_ToStrODBC(this.w_CAFLDEFF)+;
                     ",CAFLRESP="+cp_ToStrODBC(this.w_CAFLRESP)+;
                     ",CADESAGG="+cp_ToStrODBC(this.w_CADESAGG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CATIPRIS=this.w_CATIPRIS;
                     ,CAKEYART=this.w_CAKEYART;
                     ,CACODSER=this.w_CACODSER;
                     ,CADESSER=this.w_CADESSER;
                     ,CATIPRIG=this.w_CATIPRIG;
                     ,CATIPRI2=this.w_CATIPRI2;
                     ,CAFLDEFF=this.w_CAFLDEFF;
                     ,CAFLRESP=this.w_CAFLRESP;
                     ,CADESAGG=this.w_CADESAGG;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- gsag_mdp : Saving
      this.gsag_mdp.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CACODICE,"DFCODICE";
             )
      this.gsag_mdp.mReplace()
      * --- GSAG_MUM : Saving
      this.GSAG_MUM.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CACODICE,"UMCODICE";
             )
      this.GSAG_MUM.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- gsag_mdp : Deleting
    this.gsag_mdp.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CACODICE,"DFCODICE";
           )
    this.gsag_mdp.mDelete()
    * --- GSAG_MUM : Deleting
    this.GSAG_MUM.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CACODICE,"UMCODICE";
           )
    this.GSAG_MUM.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) and (not (Empty(t_CACODSER)) OR not (Empty(t_CAKEYART)))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CAU_ATTI_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CAU_ATTI_IDX,2])
        *
        * delete CAU_ATTI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
        *
        * delete CAUMATTI
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) and (not (Empty(t_CACODSER)) OR not (Empty(t_CAKEYART)))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,4,.t.)
        if .o_PARAGEN<>.w_PARAGEN
          .link_1_6('Full')
        endif
        if .o_CAFLNSAP<>.w_CAFLNSAP
          .w_CARAGGST = IIF(.w_CAFLNSAP='S',IIF(.w_CARAGGST='C','G',.w_CARAGGST),IIF(.w_CARAGGST='c' OR EMPTY(.w_CARAGGST),'G',.w_CARAGGST))
        endif
        if .o_CAFLNSAP<>.w_CAFLNSAP
          .w_CARAGGST = IIF(.w_CAFLNSAP='S',IIF(.w_CARAGGST='C','G',.w_CARAGGST),IIF(.w_CARAGGST='c' OR EMPTY(.w_CARAGGST),'G',.w_CARAGGST))
        endif
        if .o_CAORASYS<>.w_CAORASYS
          .Calculate_EWHNIIMALV()
        endif
        .DoRTCalc(8,9,.t.)
        if .o_CADATINI<>.w_CADATINI
          .w_ORAINI = IIF(EMPTY(.w_CADATINI),'08',PADL(ALLTRIM(STR(HOUR(.w_CADATINI),2)),2,'0'))
        endif
        if .o_CADATINI<>.w_CADATINI
          .w_MININI = RIGHT('00'+ALLTRIM(STR(MINUTE(.w_CADATINI),2)),2)
        endif
        .DoRTCalc(12,18,.t.)
        if .o_CAFLNSAP<>.w_CAFLNSAP
          .w_CASTAATT = IIF(.w_CAFLNSAP='S',IIF(.w_CASTAATT='P','D',.w_CASTAATT),IIF(.w_CASTAATT='P' OR EMPTY(.w_CASTAATT),'D',.w_CASTAATT))
        endif
        if .o_CAFLNSAP<>.w_CAFLNSAP
          .w_CASTAATT = IIF(.w_CAFLNSAP='S',IIF(.w_CASTAATT='P','D',.w_CASTAATT),IIF(.w_CASTAATT='P' OR EMPTY(.w_CASTAATT),'D',.w_CASTAATT))
        endif
        if .o_CARAGGST<>.w_CARAGGST
          .w_CAFLNOTI = IIF(.w_CARAGGST$'SF','S','N')
        endif
        .DoRTCalc(22,22,.t.)
        if .o_CARAGGST<>.w_CARAGGST
          .w_CAPRIEML = 3
        endif
        .DoRTCalc(24,24,.t.)
        if .o_CARAGGST<>.w_CARAGGST
          .w_CAFLRINV = IIF(.w_CARAGGST='U','S','N')
        endif
        if .o_CARAGGST<>.w_CARAGGST
          .w_CAFLTRIS = IIF(.w_CARAGGST='U','S','N')
        endif
        .DoRTCalc(27,32,.t.)
        if .o_CAFLNSAP<>.w_CAFLNSAP
          .link_1_37('Full')
        endif
        .DoRTCalc(34,37,.t.)
        if .o_CARAGGST<>.W_CARAGGST.or. .o_CACHKINI<>.w_CACHKINI
          .w_CACHKFOR = iif( .w_CARAGGST='U' , 'S' , ' ')
        endif
        .DoRTCalc(39,51,.t.)
        if .o_CARAGGST<>.w_CARAGGST
          .w_CAFLNSAP = IIF(.w_CARAGGST$'MA','S','N')
        endif
          .w_ACQU = iif(g_APPLICATION='ad hoc ENTERPRISE',g_CACQ,g_ACQU)
        .DoRTCalc(54,54,.t.)
        if .o_CAFLNSAP<>.w_CAFLNSAP
          .w_CACAUDOC = iif(.w_CAFLNSAP="S" or .w_ISAHE, space(5), iif(.w_ISALT, TipoPref("T") , Docpref("DI", "V") ) )
          .link_1_59('Full')
        endif
        if .o_CAFLNSAP<>.w_CAFLNSAP.or. .o_CAFLANAL<>.w_CAFLANAL
          .w_CACAUACQ = space(5)
          .link_1_60('Full')
        endif
        .DoRTCalc(57,57,.t.)
        if .o_CAFLNSAP<>.w_CAFLNSAP.or. .o_CARAGGST<>.w_CARAGGST
          .w_CAFLGLIS = 'N'
        endif
        if .o_ORAINI<>.w_ORAINI
          .Calculate_CNGCMHSNFW()
        endif
        if .o_MININI<>.w_MININI
          .Calculate_JUNEYSGOQR()
        endif
        if .o_DATINI<>.w_DATINI.or. .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI.or. .o_CACODICE<>.w_CACODICE
          .Calculate_BPOHEVPIDL()
        endif
        if .o_CARAGGST<>.w_CARAGGST
          .Calculate_CLUGDOOAQN()
        endif
        if .o_CAFLNSAP<>.w_CAFLNSAP
          .Calculate_NWECDMGHCK()
        endif
        if .o_CAMINPRE<>.w_CAMINPRE
          .Calculate_GHBBZLIIWM()
        endif
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate(IIF(.w_ISALT, "Tipo per la generazione dei documenti interni da assegnare all'attivit�", "Causale per la generazione dei documenti interni da assegnare all'attivit�"))
        if .o_CAFLATRI<>.w_CAFLATRI
          .Calculate_JZKXQUXVIN()
        endif
        .DoRTCalc(59,80,.t.)
        if .o_CAPRIORI<>.w_CAPRIORI
          .w_CACOLORE = IIF(.w_CAPRIORI<>1 ,0,.w_CACOLORE)
        endif
        if .o_CAPRIORI<>.w_CAPRIORI.or. .o_CACOLORE<>.w_CACOLORE
          .w_ColAttivita = IIF(.w_CACOLORE=0 or .w_CAPRIORI<>1 ,0,.w_CACOLORE)
        endif
        .oPgFrm.Page1.oPag.oObj_1_124.Calculate(.w_ColAttivita)
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate(.w_CACOLPOS)
        .DoRTCalc(83,83,.t.)
        if .o_CAPRIORI<>.w_CAPRIORI.or. .o_CACOLORE<>.w_CACOLORE
          .w_ColAttivita = IIF(.w_CACOLORE=0 or .w_CAPRIORI<>1 ,0,.w_CACOLORE)
        endif
        .DoRTCalc(85,89,.t.)
        if .o_CACODSER<>.w_CACODSER.or. .o_CAKEYART<>.w_CAKEYART
          .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
        endif
          .link_2_7('Full')
          .link_2_8('Full')
        .DoRTCalc(93,97,.t.)
        if .o_CACODSER<>.w_CACODSER
          .w_ATIPRIG = IIF(Empty(.w_CATIPRIG),.w_ARTIPRIG,.w_CATIPRIG)
        endif
        if .o_CACODSER<>.w_CACODSER
          .w_ATIPRI2 = IIF(Empty(.w_CATIPRI2),.w_ARTIPRI2,.w_CATIPRI2)
        endif
        if .o_CAKEYART<>.w_CAKEYART
          .w_ETIPRIG = IIF(Empty(.w_CATIPRIG),'D',.w_CATIPRIG)
        endif
        if .o_CAKEYART<>.w_CAKEYART
          .w_ETIPRI2 = IIF(Empty(.w_CATIPRI2),'D',.w_CATIPRI2)
        endif
        if .o_CACODSER<>.w_CACODSER
          .w_RTIPRIG = IIF(Empty(.w_CATIPRIG),'D',.w_CATIPRIG)
        endif
        if .o_CACODSER<>.w_CACODSER
          .w_RTIPRI2 = IIF(Empty(.w_CATIPRI2),'D',.w_CATIPRI2)
        endif
        .DoRTCalc(104,108,.t.)
        if .o_CACODSER<>.w_CACODSER.or. .o_CAKEYART<>.w_CAKEYART
          .w_CADESAGG = IIF(.w_ISAHE, .w_EDESAGG, .w_RDESAGG)
        endif
        if .o_CACODSER<>.w_CACODSER.or. .o_CAKEYART<>.w_CAKEYART
          .w_KADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
        endif
        Local l_Dep1,l_Dep2
        l_Dep1= .o_ATIPRIG<>.w_ATIPRIG .or. .o_ETIPRIG<>.w_ETIPRIG .or. .o_RTIPRIG<>.w_RTIPRIG .or. .o_ATIPRI2<>.w_ATIPRI2 .or. .o_ETIPRI2<>.w_ETIPRI2        l_Dep2= .o_RTIPRI2<>.w_RTIPRI2 .or. .o_CAKEYART<>.w_CAKEYART .or. .o_CACODSER<>.w_CACODSER
        if m.l_Dep1 .or. m.l_Dep2
          .Calculate_EUZNZQEHYY()
        endif
        if .o_CA_TIMER<>.w_CA_TIMER
          .Calculate_RNTLRIYVBW()
        endif
        .DoRTCalc(111,115,.t.)
          .link_1_137('Full')
        if  .o_PAFLVISI<>.w_PAFLVISI
          .WriteTo_gsag_mdp()
        endif
        if .o_MATETIPOL<>.w_MATETIPOL
          .Calculate_TGIMXKPUCZ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(117,135,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CATIPRIS with this.w_CATIPRIS
      replace t_CACODART with this.w_CACODART
      replace t_ECACODART with this.w_ECACODART
      replace t_RCACODART with this.w_RCACODART
      replace t_ARTIPRIG with this.w_ARTIPRIG
      replace t_ARTIPRI2 with this.w_ARTIPRI2
      replace t_CATIPRIG with this.w_CATIPRIG
      replace t_CATIPRI2 with this.w_CATIPRI2
      replace t_ETIPRI2 with this.w_ETIPRI2
      replace t_RTIPRI2 with this.w_RTIPRI2
      replace t_CAFLDEFF with this.w_CAFLDEFF
      replace t_ECADTOBSO with this.w_ECADTOBSO
      replace t_KADTOBSO with this.w_KADTOBSO
      replace t_RCADTOBSO with this.w_RCADTOBSO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate(IIF(.w_ISALT, "Tipo per la generazione dei documenti interni da assegnare all'attivit�", "Causale per la generazione dei documenti interni da assegnare all'attivit�"))
        .oPgFrm.Page1.oPag.oObj_1_124.Calculate(.w_ColAttivita)
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate(.w_CACOLPOS)
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_EWHNIIMALV()
    with this
          * --- Ora di sistema: in sequenza dopo CAORASYS e prima di ORAINI e MININI
          .w_CADATINI = cp_CharToDatetime( DTOC(.w_DATINI)+' '+IIF(.w_CAORASYS='S','00:00','08:00')+':00')
    endwith
  endproc
  proc Calculate_CNGCMHSNFW()
    with this
          * --- Resetto w_oraini
          .w_ORAINI = .ZeroFill(.w_ORAINI)
    endwith
  endproc
  proc Calculate_JUNEYSGOQR()
    with this
          * --- Resetto w_minini
          .w_MININI = .ZeroFill(.w_MININI)
    endwith
  endproc
  proc Calculate_BPOHEVPIDL()
    with this
          * --- Setta w_cadatini
          .w_CADATINI = cp_CharToDatetime( DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00')
    endwith
  endproc
  proc Calculate_CLUGDOOAQN()
    with this
          * --- Setta w_CACHKOBB, w_CACHKNOM e w_CADISPON
          .w_CACHKOBB = IIF(.w_CARAGGST='M',' ',.w_CACHKOBB)
          .w_CACHKNOM = IIF(.w_CARAGGST$'MA',' ',.w_CACHKNOM)
          .w_CADISPON = IIF(.w_CARAGGST$'DM', 1, 2)
    endwith
  endproc
  proc Calculate_EFIBCCLLIL()
    with this
          * --- Controlli a check form
          GSAG_BCO(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_NWECDMGHCK()
    with this
          * --- gsag_bca - Sbianca le prestazioni
          gsag_bca(this;
             )
    endwith
  endproc
  proc Calculate_GHBBZLIIWM()
    with this
          * --- Flag promemoria
          .w_CAFLPREA = IIF(.w_CAMINPRE>=0,'S','N')
    endwith
  endproc
  proc Calculate_JZKXQUXVIN()
    with this
          * --- Disabilita pubblica su Web se attivit� riservata
          .w_CAPUBWEB = IIF(.w_CAFLATRI='S' , 'N' , .w_CAPUBWEB)
    endwith
  endproc
  proc Calculate_ROVWUYNCWE()
    with this
          * --- Verifica valore visibilit�
          .w_CAFLATRI = IIF(EMPTY(NVL(.w_CAFLATRI, ' ') ), 'N', .w_CAFLATRI)
    endwith
  endproc
  proc Calculate_EUZNZQEHYY()
    with this
          * --- Ricalcolo tipo documeto
          .w_CATIPRIG = ICASE(.w_ISAHE,.w_ETIPRIG,Isahr(),.w_RTIPRIG,.w_ATIPRIG)
          .w_CATIPRI2 = ICASE(.w_ISAHE,.w_ETIPRI2,Isahr(),.w_RTIPRI2,.w_ATIPRI2)
    endwith
  endproc
  proc Calculate_RNTLRIYVBW()
    with this
          * --- Azzera durata presunta se visibile timer
          .w_CADURORE = 0
          .w_CADURMIN = 0
    endwith
  endproc
  proc Calculate_PGQNPFGCXZ()
    with this
          * --- Colora pagina note
          GSUT_BCN(this;
              ,"COLORA";
              ,.w_CA__NOTE;
             )
    endwith
  endproc
  proc Calculate_GRJFLMWOLU()
    with this
          * --- Numero pagina note
          GSUT_BCN(this;
              ,"PAGINA";
              ,"Note";
             )
    endwith
  endproc
  proc Calculate_TGIMXKPUCZ()
    with this
          * --- Rivalorizzo CA__ENTE
          RivalorizzoCombo(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oORAINI_1_12.enabled = this.oPgFrm.Page1.oPag.oORAINI_1_12.mCond()
    this.oPgFrm.Page1.oPag.oMININI_1_13.enabled = this.oPgFrm.Page1.oPag.oMININI_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCADURORE_1_14.enabled = this.oPgFrm.Page1.oPag.oCADURORE_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCADURMIN_1_15.enabled = this.oPgFrm.Page1.oPag.oCADURMIN_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCASTAATT_1_21.enabled = this.oPgFrm.Page1.oPag.oCASTAATT_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCASTAATT_1_22.enabled = this.oPgFrm.Page1.oPag.oCASTAATT_1_22.mCond()
    this.oPgFrm.Page1.oPag.oCAMINPRE_1_28.enabled = this.oPgFrm.Page1.oPag.oCAMINPRE_1_28.mCond()
    this.oPgFrm.Page1.oPag.oCAFLRINV_1_29.enabled = this.oPgFrm.Page1.oPag.oCAFLRINV_1_29.mCond()
    this.oPgFrm.Page1.oPag.oCAFLTRIS_1_30.enabled = this.oPgFrm.Page1.oPag.oCAFLTRIS_1_30.mCond()
    this.oPgFrm.Page1.oPag.oCACHKOBB_1_32.enabled = this.oPgFrm.Page1.oPag.oCACHKOBB_1_32.mCond()
    this.oPgFrm.Page1.oPag.oCACHKNOM_1_33.enabled = this.oPgFrm.Page1.oPag.oCACHKNOM_1_33.mCond()
    this.oPgFrm.Page1.oPag.oCADISPON_1_34.enabled = this.oPgFrm.Page1.oPag.oCADISPON_1_34.mCond()
    this.oPgFrm.Page1.oPag.oCACHKINI_1_38.enabled = this.oPgFrm.Page1.oPag.oCACHKINI_1_38.mCond()
    this.oPgFrm.Page1.oPag.oCACHKFOR_1_42.enabled = this.oPgFrm.Page1.oPag.oCACHKFOR_1_42.mCond()
    this.oPgFrm.Page1.oPag.oCAFLNSAP_1_56.enabled = this.oPgFrm.Page1.oPag.oCAFLNSAP_1_56.mCond()
    this.oPgFrm.Page1.oPag.oCA__NOTE_1_97.enabled = this.oPgFrm.Page1.oPag.oCA__NOTE_1_97.mCond()
    this.oPgFrm.Page3.oPag.oCA__NOTE_5_1.enabled = this.oPgFrm.Page3.oPag.oCA__NOTE_5_1.mCond()
    this.oPgFrm.Page2.oPag.oBtn_4_4.enabled = this.oPgFrm.Page2.oPag.oBtn_4_4.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_149.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_149.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCADESSER_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCADESSER_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAFLRESP_2_23.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAFLRESP_2_23.mCond()
    this.oPgFrm.Page1.oPag.oCADESAGG_2_25.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCADESAGG_2_25.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    local i_show2
    i_show2=not(this.w_CAFLNSAP='S')
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Note"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oCARAGGST_1_7.visible=!this.oPgFrm.Page1.oPag.oCARAGGST_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCARAGGST_1_8.visible=!this.oPgFrm.Page1.oPag.oCARAGGST_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCAFLPDOC_1_18.visible=!this.oPgFrm.Page1.oPag.oCAFLPDOC_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCASERDOC_1_19.visible=!this.oPgFrm.Page1.oPag.oCASERDOC_1_19.mHide()
    this.oPgFrm.Page1.oPag.oCASTAATT_1_21.visible=!this.oPgFrm.Page1.oPag.oCASTAATT_1_21.mHide()
    this.oPgFrm.Page1.oPag.oCASTAATT_1_22.visible=!this.oPgFrm.Page1.oPag.oCASTAATT_1_22.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oCAFLRINV_1_29.visible=!this.oPgFrm.Page1.oPag.oCAFLRINV_1_29.mHide()
    this.oPgFrm.Page1.oPag.oCAFLTRIS_1_30.visible=!this.oPgFrm.Page1.oPag.oCAFLTRIS_1_30.mHide()
    this.oPgFrm.Page1.oPag.oCACHKOBB_1_31.visible=!this.oPgFrm.Page1.oPag.oCACHKOBB_1_31.mHide()
    this.oPgFrm.Page1.oPag.oCACHKOBB_1_32.visible=!this.oPgFrm.Page1.oPag.oCACHKOBB_1_32.mHide()
    this.oPgFrm.Page1.oPag.oCACHKNOM_1_33.visible=!this.oPgFrm.Page1.oPag.oCACHKNOM_1_33.mHide()
    this.oPgFrm.Page1.oPag.oCADISPON_1_34.visible=!this.oPgFrm.Page1.oPag.oCADISPON_1_34.mHide()
    this.oPgFrm.Page1.oPag.oCAFLATRI_1_36.visible=!this.oPgFrm.Page1.oPag.oCAFLATRI_1_36.mHide()
    this.oPgFrm.Page1.oPag.oCACAUABB_1_37.visible=!this.oPgFrm.Page1.oPag.oCACAUABB_1_37.mHide()
    this.oPgFrm.Page1.oPag.oCAPUBWEB_1_39.visible=!this.oPgFrm.Page1.oPag.oCAPUBWEB_1_39.mHide()
    this.oPgFrm.Page1.oPag.oCOLORE_1_40.visible=!this.oPgFrm.Page1.oPag.oCOLORE_1_40.mHide()
    this.oPgFrm.Page1.oPag.oCACHKFOR_1_42.visible=!this.oPgFrm.Page1.oPag.oCACHKFOR_1_42.mHide()
    this.oPgFrm.Page1.oPag.oCAFLANAL_1_58.visible=!this.oPgFrm.Page1.oPag.oCAFLANAL_1_58.mHide()
    this.oPgFrm.Page1.oPag.oCACAUDOC_1_59.visible=!this.oPgFrm.Page1.oPag.oCACAUDOC_1_59.mHide()
    this.oPgFrm.Page1.oPag.oCACAUACQ_1_60.visible=!this.oPgFrm.Page1.oPag.oCACAUACQ_1_60.mHide()
    this.oPgFrm.Page1.oPag.oCATIPEVE_1_61.visible=!this.oPgFrm.Page1.oPag.oCATIPEVE_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.oPgFrm.Page1.oPag.oDESCAUDI_1_68.visible=!this.oPgFrm.Page1.oPag.oDESCAUDI_1_68.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_86.visible=!this.oPgFrm.Page1.oPag.oStr_1_86.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_88.visible=!this.oPgFrm.Page1.oPag.oBtn_1_88.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_89.visible=!this.oPgFrm.Page1.oPag.oBtn_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oCA__NOTE_1_97.visible=!this.oPgFrm.Page1.oPag.oCA__NOTE_1_97.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_98.visible=!this.oPgFrm.Page1.oPag.oStr_1_98.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_101.visible=!this.oPgFrm.Page1.oPag.oStr_1_101.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_112.visible=!this.oPgFrm.Page1.oPag.oStr_1_112.mHide()
    this.oPgFrm.Page1.oPag.oDESCAUAC_1_113.visible=!this.oPgFrm.Page1.oPag.oDESCAUAC_1_113.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_114.visible=!this.oPgFrm.Page1.oPag.oStr_1_114.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_120.visible=!this.oPgFrm.Page1.oPag.oStr_1_120.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_129.visible=!this.oPgFrm.Page1.oPag.oStr_1_129.mHide()
    this.oPgFrm.Page1.oPag.oDESABB_1_131.visible=!this.oPgFrm.Page1.oPag.oDESABB_1_131.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_134.visible=!this.oPgFrm.Page1.oPag.oStr_1_134.mHide()
    this.oPgFrm.Page3.oPag.oCA__NOTE_5_1.visible=!this.oPgFrm.Page3.oPag.oCA__NOTE_5_1.mHide()
    this.oPgFrm.Page1.oPag.oCATIPPRA_1_139.visible=!this.oPgFrm.Page1.oPag.oCATIPPRA_1_139.mHide()
    this.oPgFrm.Page1.oPag.oCAMATPRA_1_140.visible=!this.oPgFrm.Page1.oPag.oCAMATPRA_1_140.mHide()
    this.oPgFrm.Page1.oPag.oCA__ENTE_1_141.visible=!this.oPgFrm.Page1.oPag.oCA__ENTE_1_141.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_142.visible=!this.oPgFrm.Page1.oPag.oStr_1_142.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_143.visible=!this.oPgFrm.Page1.oPag.oStr_1_143.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_144.visible=!this.oPgFrm.Page1.oPag.oStr_1_144.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_145.visible=!this.oPgFrm.Page1.oPag.oStr_1_145.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_149.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_149.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_157.visible=!this.oPgFrm.Page1.oPag.oStr_1_157.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_158.visible=!this.oPgFrm.Page1.oPag.oStr_1_158.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAKEYART_2_4.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAKEYART_2_4.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODSER_2_5.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODSER_2_5.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCADESSER_2_11.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCADESSER_2_11.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRIG_2_14.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRIG_2_14.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRI2_2_15.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRI2_2_15.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRIG_2_16.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRIG_2_16.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRIG_2_18.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRIG_2_18.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLRESP_2_23.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLRESP_2_23.mHide()
    this.oPgFrm.Page1.oPag.oCADESAGG_2_25.visible=!this.oPgFrm.Page1.oPag.oCADESAGG_2_25.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRES_2_29.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRES_2_29.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRES_2_30.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRES_2_30.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_EFIBCCLLIL()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_102.Event(cEvent)
        if lower(cEvent)==lower("Load") or lower(cEvent)==lower("Edit Started")
          .Calculate_ROVWUYNCWE()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_124.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_127.Event(cEvent)
        if lower(cEvent)==lower("Load") or lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_PGQNPFGCXZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_GRJFLMWOLU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_TGIMXKPUCZ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PARAGEN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PARAGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PARAGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAGESRIS,PACHKFES,PAFLVISI";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PARAGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PARAGEN)
            select PACODAZI,PAGESRIS,PACHKFES,PAFLVISI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PARAGEN = NVL(_Link_.PACODAZI,space(10))
      this.w_GESRIS = NVL(_Link_.PAGESRIS,space(1))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
      this.w_PAFLVISI = NVL(_Link_.PAFLVISI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PARAGEN = space(10)
      endif
      this.w_GESRIS = space(1)
      this.w_PACHKFES = space(1)
      this.w_PAFLVISI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PARAGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATIPATT
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFFTIPAT_IDX,3]
    i_lTable = "OFFTIPAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2], .t., this.OFFTIPAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATIPATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ATA',True,'OFFTIPAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODTIP like "+cp_ToStrODBC(trim(this.w_CATIPATT)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODTIP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODTIP',trim(this.w_CATIPATT))
          select TACODTIP,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODTIP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATIPATT)==trim(_Link_.TACODTIP) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStrODBC(trim(this.w_CATIPATT)+"%");

            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStr(trim(this.w_CATIPATT)+"%");

            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATIPATT) and !this.bDontReportError
            deferred_cp_zoom('OFFTIPAT','*','TACODTIP',cp_AbsName(oSource.parent,'oCATIPATT_1_9'),i_cWhere,'GSAG_ATA',"Tipologie attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',oSource.xKey(1))
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATIPATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(this.w_CATIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',this.w_CATIPATT)
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATIPATT = NVL(_Link_.TACODTIP,space(5))
      this.w_DESTIPAT = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CATIPATT = space(5)
      endif
      this.w_DESTIPAT = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])+'\'+cp_ToStr(_Link_.TACODTIP,1)
      cp_ShowWarn(i_cKey,this.OFFTIPAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATIPATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFFTIPAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.TACODTIP as TACODTIP109"+ ",link_1_9.TADESCRI as TADESCRI109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on CAUMATTI.CATIPATT=link_1_9.TACODTIP"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and CAUMATTI.CATIPATT=link_1_9.TACODTIP(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACAUABB
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACAUABB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BLT',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CACAUABB)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CACAUABB))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACAUABB)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStrODBC(trim(this.w_CACAUABB)+"%");

            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStr(trim(this.w_CACAUABB)+"%");

            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACAUABB) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCACAUABB_1_37'),i_cWhere,'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACAUABB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CACAUABB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CACAUABB)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACAUABB = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESABB = NVL(_Link_.TDDESDOC,space(35))
      this.w_CFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_TDFLINTE1 = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACAUABB = space(5)
      endif
      this.w_DESABB = space(35)
      this.w_CFLVEAC = space(1)
      this.w_TDFLINTE1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TDFLINTE1<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale incongruente")
        endif
        this.w_CACAUABB = space(5)
        this.w_DESABB = space(35)
        this.w_CFLVEAC = space(1)
        this.w_TDFLINTE1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACAUABB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_37(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_37.TDTIPDOC as TDTIPDOC137"+ ",link_1_37.TDDESDOC as TDDESDOC137"+ ",link_1_37.TDFLVEAC as TDFLVEAC137"+ ",link_1_37.TDFLINTE as TDFLINTE137"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_37 on CAUMATTI.CACAUABB=link_1_37.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_37"
          i_cKey=i_cKey+'+" and CAUMATTI.CACAUABB=link_1_37.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACAUDOC
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BLT',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CACAUDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC,TDFLPRAT,TDCAUCON,TDFLGLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CACAUDOC))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC,TDFLPRAT,TDCAUCON,TDFLGLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStrODBC(trim(this.w_CACAUDOC)+"%");

            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC,TDFLPRAT,TDCAUCON,TDFLGLIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStr(trim(this.w_CACAUDOC)+"%");

            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC,TDFLPRAT,TDCAUCON,TDFLGLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCACAUDOC_1_59'),i_cWhere,'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC,TDFLPRAT,TDCAUCON,TDFLGLIS";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC,TDFLPRAT,TDCAUCON,TDFLGLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC,TDFLPRAT,TDCAUCON,TDFLGLIS";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CACAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CACAUDOC)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDCATDOC,TDFLVEAC,TDFLPRAT,TDCAUCON,TDFLGLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAUDI = NVL(_Link_.TDDESDOC,space(35))
      this.w_TDFLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_TDCATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_TDFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_VFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLPRAT = NVL(_Link_.TDFLPRAT,space(1))
      this.w_CAUCON = NVL(_Link_.TDCAUCON,space(5))
      this.w_FLGLISV = NVL(_Link_.TDFLGLIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACAUDOC = space(5)
      endif
      this.w_DESCAUDI = space(35)
      this.w_TDFLINTE = space(1)
      this.w_TDCATDOC = space(2)
      this.w_TDFLVEAC = space(1)
      this.w_VFLVEAC = space(1)
      this.w_FLPRAT = space(1)
      this.w_CAUCON = space(5)
      this.w_FLGLISV = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VFLVEAC='V' AND (!.w_ISALT OR .w_TDCATDOC='DI')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale incongruente")
        endif
        this.w_CACAUDOC = space(5)
        this.w_DESCAUDI = space(35)
        this.w_TDFLINTE = space(1)
        this.w_TDCATDOC = space(2)
        this.w_TDFLVEAC = space(1)
        this.w_VFLVEAC = space(1)
        this.w_FLPRAT = space(1)
        this.w_CAUCON = space(5)
        this.w_FLGLISV = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_bRes and i_cCtrl='Drop'
      with this
        if i_bRes and !(!.w_ISAHE OR .w_FLGLISV<>"S" OR  .w_CARAGGST<>'Z')
          i_bRes=(cp_WarningMsg(thisform.msgFmt("Il flag <Vendita su listino> della causale documento � attivo, ma verr� ignorato")))
          if not(i_bRes)
            this.w_CACAUDOC = space(5)
            this.w_DESCAUDI = space(35)
            this.w_TDFLINTE = space(1)
            this.w_TDCATDOC = space(2)
            this.w_TDFLVEAC = space(1)
            this.w_VFLVEAC = space(1)
            this.w_FLPRAT = space(1)
            this.w_CAUCON = space(5)
            this.w_FLGLISV = space(1)
          endif
        endif
      endwith
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_59(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_59.TDTIPDOC as TDTIPDOC159"+ ",link_1_59.TDDESDOC as TDDESDOC159"+ ",link_1_59.TDFLINTE as TDFLINTE159"+ ",link_1_59.TDCATDOC as TDCATDOC159"+ ",link_1_59.TDFLVEAC as TDFLVEAC159"+ ",link_1_59.TDFLVEAC as TDFLVEAC159"+ ",link_1_59.TDFLPRAT as TDFLPRAT159"+ ",link_1_59.TDCAUCON as TDCAUCON159"+ ",link_1_59.TDFLGLIS as TDFLGLIS159"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_59 on CAUMATTI.CACAUDOC=link_1_59.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_59"
          i_cKey=i_cKey+'+" and CAUMATTI.CACAUDOC=link_1_59.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACAUACQ
  func Link_1_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACAUACQ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BLT',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CACAUACQ)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLGLIS,TDFLINTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CACAUACQ))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLGLIS,TDFLINTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACAUACQ)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStrODBC(trim(this.w_CACAUACQ)+"%");

            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLGLIS,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStr(trim(this.w_CACAUACQ)+"%");

            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLGLIS,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACAUACQ) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCACAUACQ_1_60'),i_cWhere,'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLGLIS,TDFLINTE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLGLIS,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACAUACQ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLGLIS,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CACAUACQ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CACAUACQ)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLGLIS,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACAUACQ = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAUAC = NVL(_Link_.TDDESDOC,space(35))
      this.w_AFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLGLIS = NVL(_Link_.TDFLGLIS,space(1))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACAUACQ = space(5)
      endif
      this.w_DESCAUAC = space(35)
      this.w_AFLVEAC = space(1)
      this.w_FLGLIS = space(1)
      this.w_FLINTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_AFLVEAC='A'  and .w_FLINTE='N' and (!.w_ISAHE OR .w_FLGLIS='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale con intestatario o di tipo incongruente")
        endif
        this.w_CACAUACQ = space(5)
        this.w_DESCAUAC = space(35)
        this.w_AFLVEAC = space(1)
        this.w_FLGLIS = space(1)
        this.w_FLINTE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACAUACQ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_60(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_60.TDTIPDOC as TDTIPDOC160"+ ",link_1_60.TDDESDOC as TDDESDOC160"+ ",link_1_60.TDFLVEAC as TDFLVEAC160"+ ",link_1_60.TDFLGLIS as TDFLGLIS160"+ ",link_1_60.TDFLINTE as TDFLINTE160"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_60 on CAUMATTI.CACAUACQ=link_1_60.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_60"
          i_cKey=i_cKey+'+" and CAUMATTI.CACAUACQ=link_1_60.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CAKEYART
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAKEYART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CAKEYART)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CAKEYART))
          select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAKEYART)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CAKEYART)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CAKEYART)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_CAKEYART)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_CAKEYART)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAKEYART) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCAKEYART_2_4'),i_cWhere,'GSMA_BZA',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAKEYART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CAKEYART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CAKEYART)
            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAKEYART = NVL(_Link_.CACODICE,space(41))
      this.w_CADESSER = NVL(_Link_.CADESART,space(40))
      this.w_EDESAGG = NVL(_Link_.CADESSUP,space(0))
      this.w_ECACODART = NVL(_Link_.CACODART,space(20))
      this.w_ECADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CAKEYART = space(41)
      endif
      this.w_CADESSER = space(40)
      this.w_EDESAGG = space(0)
      this.w_ECACODART = space(20)
      this.w_ECADTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_ECACODART, "FM-FO-DE") AND (EMPTY(.w_ECADTOBSO) OR .w_ECADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_CAKEYART = space(41)
        this.w_CADESSER = space(40)
        this.w_EDESAGG = space(0)
        this.w_ECACODART = space(20)
        this.w_ECADTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAKEYART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CACODICE as CACODICE204"+ ",link_2_4.CADESART as CADESART204"+ ",link_2_4.CADESSUP as CADESSUP204"+ ",link_2_4.CACODART as CACODART204"+ ",link_2_4.CADTOBSO as CADTOBSO204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on CAU_ATTI.CAKEYART=link_2_4.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and CAU_ATTI.CAKEYART=link_2_4.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACODSER
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CACODSER)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CACODSER))
          select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODSER)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CACODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CACODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_CACODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_CACODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACODSER) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCACODSER_2_5'),i_cWhere,'GSMA_BZA',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODSER)
            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODSER = NVL(_Link_.CACODICE,space(20))
      this.w_CADESSER = NVL(_Link_.CADESART,space(40))
      this.w_RDESAGG = NVL(_Link_.CADESSUP,space(0))
      this.w_CACODART = NVL(_Link_.CACODART,space(20))
      this.w_RCACODART = NVL(_Link_.CACODART,space(20))
      this.w_RCADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CACODSER = space(20)
      endif
      this.w_CADESSER = space(40)
      this.w_RDESAGG = space(0)
      this.w_CACODART = space(20)
      this.w_RCACODART = space(20)
      this.w_RCADTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_CACODSER = space(20)
        this.w_CADESSER = space(40)
        this.w_RDESAGG = space(0)
        this.w_CACODART = space(20)
        this.w_RCACODART = space(20)
        this.w_RCADTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.CACODICE as CACODICE205"+ ",link_2_5.CADESART as CADESART205"+ ",link_2_5.CADESSUP as CADESSUP205"+ ",link_2_5.CACODART as CACODART205"+ ",link_2_5.CACODART as CACODART205"+ ",link_2_5.CADTOBSO as CADTOBSO205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on CAU_ATTI.CACODSER=link_2_5.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and CAU_ATTI.CACODSER=link_2_5.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ECACODART
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ECACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ECACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPRES";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ECACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ECACODART)
            select ARCODART,ARTIPRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ECACODART = NVL(_Link_.ARCODART,space(20))
      this.w_ETIPRES = NVL(_Link_.ARTIPRES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ECACODART = space(20)
      endif
      this.w_ETIPRES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ECACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RCACODART
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RCACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RCACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPRIG,ARTIPRI2,ARTIPRES";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_RCACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_RCACODART)
            select ARCODART,ARTIPRIG,ARTIPRI2,ARTIPRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RCACODART = NVL(_Link_.ARCODART,space(20))
      this.w_ARTIPRIG = NVL(_Link_.ARTIPRIG,space(1))
      this.w_ARTIPRI2 = NVL(_Link_.ARTIPRI2,space(1))
      this.w_RTIPRES = NVL(_Link_.ARTIPRES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_RCACODART = space(20)
      endif
      this.w_ARTIPRIG = space(1)
      this.w_ARTIPRI2 = space(1)
      this.w_RTIPRES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RCACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCON
  func Link_1_137(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCFLCOSE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUCON)
            select CCCODICE,CCFLCOSE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_FLCOSE = NVL(_Link_.CCFLCOSE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCON = space(5)
      endif
      this.w_FLCOSE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATIPPRA
  func Link_1_139(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_TIPI_IDX,3]
    i_lTable = "PRA_TIPI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_TIPI_IDX,2], .t., this.PRA_TIPI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_TIPI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATIPPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ATP',True,'PRA_TIPI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TPCODICE like "+cp_ToStrODBC(trim(this.w_CATIPPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select TPCODICE,TPFLGIUD";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TPCODICE',trim(this.w_CATIPPRA))
          select TPCODICE,TPFLGIUD;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATIPPRA)==trim(_Link_.TPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TPFLGIUD like "+cp_ToStrODBC(trim(this.w_CATIPPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPFLGIUD";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TPFLGIUD like "+cp_ToStr(trim(this.w_CATIPPRA)+"%");

            select TPCODICE,TPFLGIUD;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATIPPRA) and !this.bDontReportError
            deferred_cp_zoom('PRA_TIPI','*','TPCODICE',cp_AbsName(oSource.parent,'oCATIPPRA_1_139'),i_cWhere,'GSPR_ATP',"Tipi pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPFLGIUD";
                     +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',oSource.xKey(1))
            select TPCODICE,TPFLGIUD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATIPPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPFLGIUD";
                   +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(this.w_CATIPPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',this.w_CATIPPRA)
            select TPCODICE,TPFLGIUD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATIPPRA = NVL(_Link_.TPCODICE,space(10))
      this.w_FLGIUD = NVL(_Link_.TPFLGIUD,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CATIPPRA = space(10)
      endif
      this.w_FLGIUD = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_TIPI_IDX,2])+'\'+cp_ToStr(_Link_.TPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_TIPI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATIPPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAMATPRA
  func Link_1_140(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_MATE_IDX,3]
    i_lTable = "PRA_MATE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_MATE_IDX,2], .t., this.PRA_MATE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_MATE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAMATPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_AMP',True,'PRA_MATE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MPCODICE like "+cp_ToStrODBC(trim(this.w_CAMATPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select MPCODICE,MP_TIPOL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MPCODICE',trim(this.w_CAMATPRA))
          select MPCODICE,MP_TIPOL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAMATPRA)==trim(_Link_.MPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MP_TIPOL like "+cp_ToStrODBC(trim(this.w_CAMATPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select MPCODICE,MP_TIPOL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MP_TIPOL like "+cp_ToStr(trim(this.w_CAMATPRA)+"%");

            select MPCODICE,MP_TIPOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAMATPRA) and !this.bDontReportError
            deferred_cp_zoom('PRA_MATE','*','MPCODICE',cp_AbsName(oSource.parent,'oCAMATPRA_1_140'),i_cWhere,'GSPR_AMP',"Materie pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MP_TIPOL";
                     +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',oSource.xKey(1))
            select MPCODICE,MP_TIPOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAMATPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MP_TIPOL";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_CAMATPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_CAMATPRA)
            select MPCODICE,MP_TIPOL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAMATPRA = NVL(_Link_.MPCODICE,space(10))
      this.w_MATETIPOL = NVL(_Link_.MP_TIPOL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAMATPRA = space(10)
      endif
      this.w_MATETIPOL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_MATE_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_MATE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAMATPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CA__ENTE
  func Link_1_141(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CA__ENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_AEP',True,'PRA_ENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EPCODICE like "+cp_ToStrODBC(trim(this.w_CA__ENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select EPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EPCODICE',trim(this.w_CA__ENTE))
          select EPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CA__ENTE)==trim(_Link_.EPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_CA__ENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_CA__ENTE)+"%");

            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CA__ENTE) and !this.bDontReportError
            deferred_cp_zoom('PRA_ENTI','*','EPCODICE',cp_AbsName(oSource.parent,'oCA__ENTE_1_141'),i_cWhere,'GSPR_AEP',"Enti pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',oSource.xKey(1))
            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CA__ENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_CA__ENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_CA__ENTE)
            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CA__ENTE = NVL(_Link_.EPCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CA__ENTE = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CA__ENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCACODICE_1_3.value==this.w_CACODICE)
      this.oPgFrm.Page1.oPag.oCACODICE_1_3.value=this.w_CACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESCRI_1_5.value==this.w_CADESCRI)
      this.oPgFrm.Page1.oPag.oCADESCRI_1_5.value=this.w_CADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCARAGGST_1_7.RadioValue()==this.w_CARAGGST)
      this.oPgFrm.Page1.oPag.oCARAGGST_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCARAGGST_1_8.RadioValue()==this.w_CARAGGST)
      this.oPgFrm.Page1.oPag.oCARAGGST_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPATT_1_9.value==this.w_CATIPATT)
      this.oPgFrm.Page1.oPag.oCATIPATT_1_9.value=this.w_CATIPATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCAORASYS_1_10.RadioValue()==this.w_CAORASYS)
      this.oPgFrm.Page1.oPag.oCAORASYS_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_12.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_12.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_13.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_13.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oCADURORE_1_14.value==this.w_CADURORE)
      this.oPgFrm.Page1.oPag.oCADURORE_1_14.value=this.w_CADURORE
    endif
    if not(this.oPgFrm.Page1.oPag.oCADURMIN_1_15.value==this.w_CADURMIN)
      this.oPgFrm.Page1.oPag.oCADURMIN_1_15.value=this.w_CADURMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAGGPREA_1_16.value==this.w_CAGGPREA)
      this.oPgFrm.Page1.oPag.oCAGGPREA_1_16.value=this.w_CAGGPREA
    endif
    if not(this.oPgFrm.Page1.oPag.oCA_TIMER_1_17.RadioValue()==this.w_CA_TIMER)
      this.oPgFrm.Page1.oPag.oCA_TIMER_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLPDOC_1_18.RadioValue()==this.w_CAFLPDOC)
      this.oPgFrm.Page1.oPag.oCAFLPDOC_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCASERDOC_1_19.value==this.w_CASERDOC)
      this.oPgFrm.Page1.oPag.oCASERDOC_1_19.value=this.w_CASERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCADTOBSO_1_20.value==this.w_CADTOBSO)
      this.oPgFrm.Page1.oPag.oCADTOBSO_1_20.value=this.w_CADTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oCASTAATT_1_21.RadioValue()==this.w_CASTAATT)
      this.oPgFrm.Page1.oPag.oCASTAATT_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCASTAATT_1_22.RadioValue()==this.w_CASTAATT)
      this.oPgFrm.Page1.oPag.oCASTAATT_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLNOTI_1_23.RadioValue()==this.w_CAFLNOTI)
      this.oPgFrm.Page1.oPag.oCAFLNOTI_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAPRIORI_1_25.RadioValue()==this.w_CAPRIORI)
      this.oPgFrm.Page1.oPag.oCAPRIORI_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAPRIEML_1_27.RadioValue()==this.w_CAPRIEML)
      this.oPgFrm.Page1.oPag.oCAPRIEML_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMINPRE_1_28.RadioValue()==this.w_CAMINPRE)
      this.oPgFrm.Page1.oPag.oCAMINPRE_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLRINV_1_29.RadioValue()==this.w_CAFLRINV)
      this.oPgFrm.Page1.oPag.oCAFLRINV_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLTRIS_1_30.RadioValue()==this.w_CAFLTRIS)
      this.oPgFrm.Page1.oPag.oCAFLTRIS_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACHKOBB_1_31.RadioValue()==this.w_CACHKOBB)
      this.oPgFrm.Page1.oPag.oCACHKOBB_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACHKOBB_1_32.RadioValue()==this.w_CACHKOBB)
      this.oPgFrm.Page1.oPag.oCACHKOBB_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACHKNOM_1_33.RadioValue()==this.w_CACHKNOM)
      this.oPgFrm.Page1.oPag.oCACHKNOM_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADISPON_1_34.RadioValue()==this.w_CADISPON)
      this.oPgFrm.Page1.oPag.oCADISPON_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLSTAT_1_35.RadioValue()==this.w_CAFLSTAT)
      this.oPgFrm.Page1.oPag.oCAFLSTAT_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLATRI_1_36.RadioValue()==this.w_CAFLATRI)
      this.oPgFrm.Page1.oPag.oCAFLATRI_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACAUABB_1_37.value==this.w_CACAUABB)
      this.oPgFrm.Page1.oPag.oCACAUABB_1_37.value=this.w_CACAUABB
    endif
    if not(this.oPgFrm.Page1.oPag.oCACHKINI_1_38.RadioValue()==this.w_CACHKINI)
      this.oPgFrm.Page1.oPag.oCACHKINI_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAPUBWEB_1_39.RadioValue()==this.w_CAPUBWEB)
      this.oPgFrm.Page1.oPag.oCAPUBWEB_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORE_1_40.value==this.w_COLORE)
      this.oPgFrm.Page1.oPag.oCOLORE_1_40.value=this.w_COLORE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLOREP_1_41.value==this.w_COLOREP)
      this.oPgFrm.Page1.oPag.oCOLOREP_1_41.value=this.w_COLOREP
    endif
    if not(this.oPgFrm.Page1.oPag.oCACHKFOR_1_42.RadioValue()==this.w_CACHKFOR)
      this.oPgFrm.Page1.oPag.oCACHKFOR_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLNSAP_1_56.RadioValue()==this.w_CAFLNSAP)
      this.oPgFrm.Page1.oPag.oCAFLNSAP_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLANAL_1_58.RadioValue()==this.w_CAFLANAL)
      this.oPgFrm.Page1.oPag.oCAFLANAL_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACAUDOC_1_59.value==this.w_CACAUDOC)
      this.oPgFrm.Page1.oPag.oCACAUDOC_1_59.value=this.w_CACAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCACAUACQ_1_60.value==this.w_CACAUACQ)
      this.oPgFrm.Page1.oPag.oCACAUACQ_1_60.value=this.w_CACAUACQ
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPEVE_1_61.RadioValue()==this.w_CATIPEVE)
      this.oPgFrm.Page1.oPag.oCATIPEVE_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAUDI_1_68.value==this.w_DESCAUDI)
      this.oPgFrm.Page1.oPag.oDESCAUDI_1_68.value=this.w_DESCAUDI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIPAT_1_95.value==this.w_DESTIPAT)
      this.oPgFrm.Page1.oPag.oDESTIPAT_1_95.value=this.w_DESTIPAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCA__NOTE_1_97.value==this.w_CA__NOTE)
      this.oPgFrm.Page1.oPag.oCA__NOTE_1_97.value=this.w_CA__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAUAC_1_113.value==this.w_DESCAUAC)
      this.oPgFrm.Page1.oPag.oDESCAUAC_1_113.value=this.w_DESCAUAC
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESAGG_2_25.value==this.w_CADESAGG)
      this.oPgFrm.Page1.oPag.oCADESAGG_2_25.value=this.w_CADESAGG
      replace t_CADESAGG with this.oPgFrm.Page1.oPag.oCADESAGG_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESABB_1_131.value==this.w_DESABB)
      this.oPgFrm.Page1.oPag.oDESABB_1_131.value=this.w_DESABB
    endif
    if not(this.oPgFrm.Page3.oPag.oCA__NOTE_5_1.value==this.w_CA__NOTE)
      this.oPgFrm.Page3.oPag.oCA__NOTE_5_1.value=this.w_CA__NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oCAMODAPP_4_2.RadioValue()==this.w_CAMODAPP)
      this.oPgFrm.Page2.oPag.oCAMODAPP_4_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPPRA_1_139.RadioValue()==this.w_CATIPPRA)
      this.oPgFrm.Page1.oPag.oCATIPPRA_1_139.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMATPRA_1_140.RadioValue()==this.w_CAMATPRA)
      this.oPgFrm.Page1.oPag.oCAMATPRA_1_140.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCA__ENTE_1_141.RadioValue()==this.w_CA__ENTE)
      this.oPgFrm.Page1.oPag.oCA__ENTE_1_141.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIGA_2_1.value==this.w_RIGA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIGA_2_1.value=this.w_RIGA
      replace t_RIGA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIGA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAKEYART_2_4.value==this.w_CAKEYART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAKEYART_2_4.value=this.w_CAKEYART
      replace t_CAKEYART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAKEYART_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODSER_2_5.value==this.w_CACODSER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODSER_2_5.value=this.w_CACODSER
      replace t_CACODSER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODSER_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCADESSER_2_11.value==this.w_CADESSER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCADESSER_2_11.value=this.w_CADESSER
      replace t_CADESSER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCADESSER_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRIG_2_14.RadioValue()==this.w_ATIPRIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRIG_2_14.SetRadio()
      replace t_ATIPRIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRIG_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRI2_2_15.RadioValue()==this.w_ATIPRI2)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRI2_2_15.SetRadio()
      replace t_ATIPRI2 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRI2_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRIG_2_16.RadioValue()==this.w_ETIPRIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRIG_2_16.SetRadio()
      replace t_ETIPRIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRIG_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRIG_2_18.RadioValue()==this.w_RTIPRIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRIG_2_18.SetRadio()
      replace t_RTIPRIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRIG_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLRESP_2_23.RadioValue()==this.w_CAFLRESP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLRESP_2_23.SetRadio()
      replace t_CAFLRESP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLRESP_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRES_2_29.RadioValue()==this.w_RTIPRES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRES_2_29.SetRadio()
      replace t_RTIPRES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRES_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRES_2_30.RadioValue()==this.w_ETIPRES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRES_2_30.SetRadio()
      replace t_ETIPRES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRES_2_30.value
    endif
    cp_SetControlsValueExtFlds(this,'CAUMATTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(!.w_ISAHE or .w_CAFLNSAP='S' or (.w_CAFLNSAP='N' And (Not empty(.w_CACAUDOC) or Not empty(.w_CACAUACQ))))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Attenzione,tipo attivit� soggetta a prestazioni, inserire causale documento"))
          case   (empty(.w_CACODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCACODICE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CACODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_ORAINI) < 24)  and (!.w_CAORASYS='S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oORAINI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)  and (!.w_CAORASYS='S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMININI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_CADURORE>-1)  and (.w_CA_TIMER<>"S")
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCADURORE_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CADURMIN<60 and .w_CADURMIN>-1)  and (.w_CA_TIMER<>"S")
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCADURMIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_TDFLINTE1<>'F')  and not(.w_CAFLNSAP="S" or .w_ISALT)  and not(empty(.w_CACAUABB))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCACAUABB_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale incongruente")
          case   not(.w_VFLVEAC='V' AND (!.w_ISALT OR .w_TDCATDOC='DI'))  and not(.w_CAFLNSAP="S")  and not(empty(.w_CACAUDOC))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCACAUDOC_1_59.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale incongruente")
          case   not(.w_AFLVEAC='A'  and .w_FLINTE='N' and (!.w_ISAHE OR .w_FLGLIS='S'))  and not(.w_CAFLNSAP="S" OR (!.w_ISAHE AND .w_CAFLANAL<>'N') OR .w_ISALT OR .w_ACQU='N')  and not(empty(.w_CACAUACQ))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCACAUACQ_1_60.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale con intestatario o di tipo incongruente")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .gsag_mdp.CheckForm()
      if i_bres
        i_bres=  .gsag_mdp.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSAG_MUM.CheckForm()
      if i_bres
        i_bres=  .GSAG_MUM.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsag_mca
      * --- Controlli Finali
      IF i_bRes=.t.
         .w_RESCHK=0
         Ah_Msg('Controlli finali...',.T.)
         .NotifyEvent('ChkFinali')
         WAIT CLEAR
         IF .w_RESCHK<>0
           i_bRes=.f.
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(ChkTipArt(.w_ECACODART, "FM-FO-DE") AND (EMPTY(.w_ECADTOBSO) OR .w_ECADTOBSO>=i_datsys)) and not(empty(.w_CAKEYART)) and (not(Empty(.w_CPROWORD)) and (not (Empty(.w_CACODSER)) OR not (Empty(.w_CAKEYART))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAKEYART_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        case   not(ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys)) and not(empty(.w_CACODSER)) and (not(Empty(.w_CPROWORD)) and (not (Empty(.w_CACODSER)) OR not (Empty(.w_CAKEYART))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODSER_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        case   not(.w_CAFLANAL='N' OR (.w_RTIPRIG<>'A' AND .w_RTIPRIG<>'E')) and (not(Empty(.w_CPROWORD)) and (not (Empty(.w_CACODSER)) OR not (Empty(.w_CAKEYART))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRIG_2_18
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_CPROWORD)) and (not (Empty(.w_CACODSER)) OR not (Empty(.w_CAKEYART)))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CACODICE = this.w_CACODICE
    this.o_PARAGEN = this.w_PARAGEN
    this.o_CARAGGST = this.w_CARAGGST
    this.o_CAORASYS = this.w_CAORASYS
    this.o_ORAINI = this.w_ORAINI
    this.o_MININI = this.w_MININI
    this.o_CA_TIMER = this.w_CA_TIMER
    this.o_CAPRIORI = this.w_CAPRIORI
    this.o_CAMINPRE = this.w_CAMINPRE
    this.o_CAFLATRI = this.w_CAFLATRI
    this.o_CACHKINI = this.w_CACHKINI
    this.o_CAFLNSAP = this.w_CAFLNSAP
    this.o_CAFLANAL = this.w_CAFLANAL
    this.o_CADATINI = this.w_CADATINI
    this.o_DATINI = this.w_DATINI
    this.o_CACOLORE = this.w_CACOLORE
    this.o_CAKEYART = this.w_CAKEYART
    this.o_CACODSER = this.w_CACODSER
    this.o_ATIPRIG = this.w_ATIPRIG
    this.o_ATIPRI2 = this.w_ATIPRI2
    this.o_ETIPRIG = this.w_ETIPRIG
    this.o_ETIPRI2 = this.w_ETIPRI2
    this.o_RTIPRIG = this.w_RTIPRIG
    this.o_RTIPRI2 = this.w_RTIPRI2
    this.o_MATETIPOL = this.w_MATETIPOL
    this.o_PAFLVISI = this.w_PAFLVISI
    * --- gsag_mdp : Depends On
    this.gsag_mdp.SaveDependsOn()
    * --- GSAG_MUM : Depends On
    this.GSAG_MUM.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) and (not (Empty(t_CACODSER)) OR not (Empty(t_CAKEYART))))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_RIGA=space(1)
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_CATIPRIS=space(1)
      .w_CAKEYART=space(41)
      .w_CACODSER=space(20)
      .w_CACODART=space(20)
      .w_ECACODART=space(20)
      .w_RCACODART=space(20)
      .w_ARTIPRIG=space(1)
      .w_ARTIPRI2=space(1)
      .w_CADESSER=space(40)
      .w_CATIPRIG=space(1)
      .w_CATIPRI2=space(1)
      .w_ATIPRIG=space(1)
      .w_ATIPRI2=space(1)
      .w_ETIPRIG=space(1)
      .w_ETIPRI2=space(1)
      .w_RTIPRIG=space(1)
      .w_RTIPRI2=space(1)
      .w_CAFLDEFF=space(1)
      .w_CAFLRESP=space(1)
      .w_ECADTOBSO=ctod("  /  /  ")
      .w_CADESAGG=space(0)
      .w_KADTOBSO=ctod("  /  /  ")
      .w_RCADTOBSO=ctod("  /  /  ")
      .w_RTIPRES=space(1)
      .w_ETIPRES=space(1)
      .DoRTCalc(1,86,.f.)
        .w_CATIPRIS = 'P'
      .DoRTCalc(88,88,.f.)
      if not(empty(.w_CAKEYART))
        .link_2_4('Full')
      endif
      .DoRTCalc(89,89,.f.)
      if not(empty(.w_CACODSER))
        .link_2_5('Full')
      endif
        .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
      .DoRTCalc(91,91,.f.)
      if not(empty(.w_ECACODART))
        .link_2_7('Full')
      endif
      .DoRTCalc(92,92,.f.)
      if not(empty(.w_RCACODART))
        .link_2_8('Full')
      endif
      .DoRTCalc(93,97,.f.)
        .w_ATIPRIG = IIF(Empty(.w_CATIPRIG),.w_ARTIPRIG,.w_CATIPRIG)
        .w_ATIPRI2 = IIF(Empty(.w_CATIPRI2),.w_ARTIPRI2,.w_CATIPRI2)
        .w_ETIPRIG = IIF(Empty(.w_CATIPRIG),'D',.w_CATIPRIG)
        .w_ETIPRI2 = IIF(Empty(.w_CATIPRI2),'D',.w_CATIPRI2)
        .w_RTIPRIG = IIF(Empty(.w_CATIPRIG),'D',.w_CATIPRIG)
        .w_RTIPRI2 = IIF(Empty(.w_CATIPRI2),'D',.w_CATIPRI2)
      .DoRTCalc(104,108,.f.)
        .w_CADESAGG = IIF(.w_ISAHE, .w_EDESAGG, .w_RDESAGG)
        .w_KADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
    endwith
    this.DoRTCalc(111,135,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_RIGA = t_RIGA
    this.w_CPROWORD = t_CPROWORD
    this.w_CATIPRIS = t_CATIPRIS
    this.w_CAKEYART = t_CAKEYART
    this.w_CACODSER = t_CACODSER
    this.w_CACODART = t_CACODART
    this.w_ECACODART = t_ECACODART
    this.w_RCACODART = t_RCACODART
    this.w_ARTIPRIG = t_ARTIPRIG
    this.w_ARTIPRI2 = t_ARTIPRI2
    this.w_CADESSER = t_CADESSER
    this.w_CATIPRIG = t_CATIPRIG
    this.w_CATIPRI2 = t_CATIPRI2
    this.w_ATIPRIG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRIG_2_14.RadioValue(.t.)
    this.w_ATIPRI2 = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRI2_2_15.RadioValue(.t.)
    this.w_ETIPRIG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRIG_2_16.RadioValue(.t.)
    this.w_ETIPRI2 = t_ETIPRI2
    this.w_RTIPRIG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRIG_2_18.RadioValue(.t.)
    this.w_RTIPRI2 = t_RTIPRI2
    this.w_CAFLDEFF = t_CAFLDEFF
    this.w_CAFLRESP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLRESP_2_23.RadioValue(.t.)
    this.w_ECADTOBSO = t_ECADTOBSO
    this.w_CADESAGG = t_CADESAGG
    this.w_KADTOBSO = t_KADTOBSO
    this.w_RCADTOBSO = t_RCADTOBSO
    this.w_RTIPRES = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRES_2_29.RadioValue(.t.)
    this.w_ETIPRES = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRES_2_30.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_RIGA with this.w_RIGA
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CATIPRIS with this.w_CATIPRIS
    replace t_CAKEYART with this.w_CAKEYART
    replace t_CACODSER with this.w_CACODSER
    replace t_CACODART with this.w_CACODART
    replace t_ECACODART with this.w_ECACODART
    replace t_RCACODART with this.w_RCACODART
    replace t_ARTIPRIG with this.w_ARTIPRIG
    replace t_ARTIPRI2 with this.w_ARTIPRI2
    replace t_CADESSER with this.w_CADESSER
    replace t_CATIPRIG with this.w_CATIPRIG
    replace t_CATIPRI2 with this.w_CATIPRI2
    replace t_ATIPRIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRIG_2_14.ToRadio()
    replace t_ATIPRI2 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oATIPRI2_2_15.ToRadio()
    replace t_ETIPRIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRIG_2_16.ToRadio()
    replace t_ETIPRI2 with this.w_ETIPRI2
    replace t_RTIPRIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRIG_2_18.ToRadio()
    replace t_RTIPRI2 with this.w_RTIPRI2
    replace t_CAFLDEFF with this.w_CAFLDEFF
    replace t_CAFLRESP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAFLRESP_2_23.ToRadio()
    replace t_ECADTOBSO with this.w_ECADTOBSO
    replace t_CADESAGG with this.w_CADESAGG
    replace t_KADTOBSO with this.w_KADTOBSO
    replace t_RCADTOBSO with this.w_RCADTOBSO
    replace t_RTIPRES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRTIPRES_2_29.ToRadio()
    replace t_ETIPRES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oETIPRES_2_30.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mcaPag1 as StdContainer
  Width  = 743
  height = 549
  stdWidth  = 743
  stdheight = 549
  resizeXpos=380
  resizeYpos=407
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODICE_1_3 as StdField with uid="PBDPWIUHUB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CACODICE", cQueryName = "CACODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipo",;
    HelpContextID = 137802133,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=90, Top=11, InputMask=replicate('X',20)


  add object oHeaderDetail as cp_DetailHeader with uid="KFDYFZRRIY",left=31, top=354, width=684,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=12,Field1="CPROWORD",Label1="Riga",Field2="RIGA",Label2="",Field3="CAKEYART",Label3="iif(Isahe(),ah_MsgFormat('Prestazione'),'')",Field4="CACODSER",Label4="iif(Isahe(),'',ah_MsgFormat('Prestazione'))",Field5="CADESSER",Label5="Descrizione",Field6="ETIPRIG",Label6="Documento",Field7="RTIPRIG",Label7="",Field8="ATIPRIG",Label8="",Field9="ATIPRI2",Label9="iif(!Isahe(),'',ah_MsgFormat('Nota spese'))",Field10="RTIPRES",Label10="iif(Isahe(),'',ah_MsgFormat('Tipo prestazione'))",Field11="ETIPRES",Label11="iif(Isahe(),'',ah_MsgFormat('Tipo prestazione'))",Field12="CAFLRESP",Label12="Ripetuta",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218956410

  add object oCADESCRI_1_5 as StdField with uid="CAQMGYQNNV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CADESCRI", cQueryName = "CADESCRI",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto da assegnare all'attivit�",;
    HelpContextID = 223388049,;
   bGlobalFont=.t.,;
    Height=21, Width=430, Left=247, Top=11, InputMask=replicate('X',254)


  add object oCARAGGST_1_7 as StdCombo with uid="RBIRLCXEPN",rtseq=6,rtrep=.f.,left=90,top=38,width=169,height=21;
    , ToolTipText = "Categoria da assegnare all'attivit�";
    , HelpContextID = 169066886;
    , cFormVar="w_CARAGGST",RowSource=""+"Generica,"+"Udienza,"+"Appuntamento,"+"Cosa da fare,"+"Nota,"+"Sessione telefonica,"+"Assenza,"+"Da inserimento prestazioni", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCARAGGST_1_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CARAGGST,&i_cF..t_CARAGGST),this.value)
    return(iif(xVal =1,'G',;
    iif(xVal =2,'U',;
    iif(xVal =3,'S',;
    iif(xVal =4,'D',;
    iif(xVal =5,'M',;
    iif(xVal =6,'T',;
    iif(xVal =7,'A',;
    iif(xVal =8,'Z',;
    space(1))))))))))
  endfunc
  func oCARAGGST_1_7.GetRadio()
    this.Parent.oContained.w_CARAGGST = this.RadioValue()
    return .t.
  endfunc

  func oCARAGGST_1_7.ToRadio()
    this.Parent.oContained.w_CARAGGST=trim(this.Parent.oContained.w_CARAGGST)
    return(;
      iif(this.Parent.oContained.w_CARAGGST=='G',1,;
      iif(this.Parent.oContained.w_CARAGGST=='U',2,;
      iif(this.Parent.oContained.w_CARAGGST=='S',3,;
      iif(this.Parent.oContained.w_CARAGGST=='D',4,;
      iif(this.Parent.oContained.w_CARAGGST=='M',5,;
      iif(this.Parent.oContained.w_CARAGGST=='T',6,;
      iif(this.Parent.oContained.w_CARAGGST=='A',7,;
      iif(this.Parent.oContained.w_CARAGGST=='Z',8,;
      0)))))))))
  endfunc

  func oCARAGGST_1_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCARAGGST_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
    endif
  endfunc

  proc oCARAGGST_1_7.mAfter
    with this.Parent.oContained
      .w_CASTAATT= IIF(.w_CARAGGST='C','P',.w_CASTAATT)
    endwith
  endproc


  add object oCARAGGST_1_8 as StdCombo with uid="OBZKFCOWIS",rtseq=7,rtrep=.f.,left=90,top=38,width=169,height=21;
    , ToolTipText = "Categoria da assegnare all'attivit�";
    , HelpContextID = 169066886;
    , cFormVar="w_CARAGGST",RowSource=""+"Generica,"+"Appuntamento,"+"Cosa da fare,"+"Nota,"+"Sessione telefonica,"+"Assenza,"+"Da inserimento prestazioni", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCARAGGST_1_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CARAGGST,&i_cF..t_CARAGGST),this.value)
    return(iif(xVal =1,'G',;
    iif(xVal =2,'S',;
    iif(xVal =3,'D',;
    iif(xVal =4,'M',;
    iif(xVal =5,'T',;
    iif(xVal =6,'A',;
    iif(xVal =7,'Z',;
    space(1)))))))))
  endfunc
  func oCARAGGST_1_8.GetRadio()
    this.Parent.oContained.w_CARAGGST = this.RadioValue()
    return .t.
  endfunc

  func oCARAGGST_1_8.ToRadio()
    this.Parent.oContained.w_CARAGGST=trim(this.Parent.oContained.w_CARAGGST)
    return(;
      iif(this.Parent.oContained.w_CARAGGST=='G',1,;
      iif(this.Parent.oContained.w_CARAGGST=='S',2,;
      iif(this.Parent.oContained.w_CARAGGST=='D',3,;
      iif(this.Parent.oContained.w_CARAGGST=='M',4,;
      iif(this.Parent.oContained.w_CARAGGST=='T',5,;
      iif(this.Parent.oContained.w_CARAGGST=='A',6,;
      iif(this.Parent.oContained.w_CARAGGST=='Z',7,;
      0))))))))
  endfunc

  func oCARAGGST_1_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCARAGGST_1_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
    endif
  endfunc

  proc oCARAGGST_1_8.mAfter
    with this.Parent.oContained
      .w_CASTAATT= IIF(.w_CARAGGST='C','P',.w_CASTAATT)
    endwith
  endproc

  add object oCATIPATT_1_9 as StdField with uid="GCUHLLPSSG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CATIPATT", cQueryName = "CATIPATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia da assegnare all'attivit�",;
    HelpContextID = 8674938,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=338, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="OFFTIPAT", cZoomOnZoom="GSAG_ATA", oKey_1_1="TACODTIP", oKey_1_2="this.w_CATIPATT"

  func oCATIPATT_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATIPATT_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATIPATT_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFFTIPAT','*','TACODTIP',cp_AbsName(this.parent,'oCATIPATT_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ATA',"Tipologie attivit�",'',this.parent.oContained
  endproc
  proc oCATIPATT_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ATA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TACODTIP=this.parent.oContained.w_CATIPATT
    i_obj.ecpSave()
  endproc

  add object oCAORASYS_1_10 as StdCheck with uid="TLCVWHTFLG",rtseq=9,rtrep=.f.,left=20, top=66, caption="Ora di sistema",;
    ToolTipText = "Se attivo, l'ora di inizio attivit� corrisponder� all'ora di sistema",;
    HelpContextID = 27070073,;
    cFormVar="w_CAORASYS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAORASYS_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAORASYS,&i_cF..t_CAORASYS),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCAORASYS_1_10.GetRadio()
    this.Parent.oContained.w_CAORASYS = this.RadioValue()
    return .t.
  endfunc

  func oCAORASYS_1_10.ToRadio()
    this.Parent.oContained.w_CAORASYS=trim(this.Parent.oContained.w_CAORASYS)
    return(;
      iif(this.Parent.oContained.w_CAORASYS=='S',1,;
      0))
  endfunc

  func oCAORASYS_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oORAINI_1_12 as StdField with uid="NOOAXZZALD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora e minuti di inizio attivit�",;
    HelpContextID = 127713306,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=258, Top=63, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI_1_12.mCond()
    with this.Parent.oContained
      return (!.w_CAORASYS='S')
    endwith
  endfunc

  func oORAINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_13 as StdField with uid="IVERYCDUGC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Ora e minuti di inizio attivit�",;
    HelpContextID = 127662394,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=291, Top=63, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_13.mCond()
    with this.Parent.oContained
      return (!.w_CAORASYS='S')
    endwith
  endfunc

  func oMININI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oCADURORE_1_14 as StdField with uid="JXQZLWADWA",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CADURORE", cQueryName = "CADURORE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Durata presunta dell'attivit� - Per il calcolo della data e dell'ora finale dell'attivit� (in ore)",;
    HelpContextID = 22061461,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=435, Top=63, cSayPict='"999"', cGetPict='"999"'

  func oCADURORE_1_14.mCond()
    with this.Parent.oContained
      return (.w_CA_TIMER<>"S")
    endwith
  endfunc

  func oCADURORE_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CADURORE>-1)
    endwith
    return bRes
  endfunc

  add object oCADURMIN_1_15 as StdField with uid="AMZFRXDTJU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CADURMIN", cQueryName = "CADURMIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Durata presunta dell'attivit� (in minuti)",;
    HelpContextID = 212819572,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=478, Top=63, cSayPict='"99"', cGetPict='"99"'

  func oCADURMIN_1_15.mCond()
    with this.Parent.oContained
      return (.w_CA_TIMER<>"S")
    endwith
  endfunc

  func oCADURMIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CADURMIN<60 and .w_CADURMIN>-1)
    endwith
    return bRes
  endfunc

  add object oCAGGPREA_1_16 as StdField with uid="UVQYJKCNRQ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CAGGPREA", cQueryName = "CAGGPREA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni di preavviso prima della scadenza",;
    HelpContextID = 25267815,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=643, Top=63, cSayPict='"999"', cGetPict='"999"'

  add object oCA_TIMER_1_17 as StdCheck with uid="POQJLYQQEO",rtseq=15,rtrep=.f.,left=20, top=87, caption="Visualizza timer",;
    ToolTipText = "Se attivo visualizza il timer sull'attivit�",;
    HelpContextID = 203427448,;
    cFormVar="w_CA_TIMER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCA_TIMER_1_17.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CA_TIMER,&i_cF..t_CA_TIMER),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCA_TIMER_1_17.GetRadio()
    this.Parent.oContained.w_CA_TIMER = this.RadioValue()
    return .t.
  endfunc

  func oCA_TIMER_1_17.ToRadio()
    this.Parent.oContained.w_CA_TIMER=trim(this.Parent.oContained.w_CA_TIMER)
    return(;
      iif(this.Parent.oContained.w_CA_TIMER=='S',1,;
      0))
  endfunc

  func oCA_TIMER_1_17.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oCAFLPDOC_1_18 as StdCombo with uid="VACRGIQFGJ",rtseq=16,rtrep=.f.,left=291,top=87,width=104,height=22;
    , ToolTipText = "Tipo di numerazione attivit�";
    , HelpContextID = 209289623;
    , cFormVar="w_CAFLPDOC",RowSource=""+"Per anno,"+"Libera,"+"Non gestita", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAFLPDOC_1_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLPDOC,&i_cF..t_CAFLPDOC),this.value)
    return(iif(xVal =1,'D',;
    iif(xVal =2,'L',;
    iif(xVal =3,'N',;
    space(1)))))
  endfunc
  func oCAFLPDOC_1_18.GetRadio()
    this.Parent.oContained.w_CAFLPDOC = this.RadioValue()
    return .t.
  endfunc

  func oCAFLPDOC_1_18.ToRadio()
    this.Parent.oContained.w_CAFLPDOC=trim(this.Parent.oContained.w_CAFLPDOC)
    return(;
      iif(this.Parent.oContained.w_CAFLPDOC=='D',1,;
      iif(this.Parent.oContained.w_CAFLPDOC=='L',2,;
      iif(this.Parent.oContained.w_CAFLPDOC=='N',3,;
      0))))
  endfunc

  func oCAFLPDOC_1_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAFLPDOC_1_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
    endif
  endfunc

  add object oCASERDOC_1_19 as StdField with uid="OTUAKEFDTB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CASERDOC", cQueryName = "CASERDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale serie dell'attivit� proposta in automatico",;
    HelpContextID = 207597975,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=435, Top=88, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oCASERDOC_1_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
    endif
  endfunc

  add object oCADTOBSO_1_20 as StdField with uid="JUNILUZLCJ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CADTOBSO", cQueryName = "CADTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio obsolescenza",;
    HelpContextID = 243376523,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=637, Top=527


  add object oCASTAATT_1_21 as StdCombo with uid="GITHAUDFIW",rtseq=19,rtrep=.f.,left=90,top=121,width=141,height=21;
    , ToolTipText = "Stato da assegnare all'attivit�";
    , HelpContextID = 6336902;
    , cFormVar="w_CASTAATT",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa,"+"Completata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCASTAATT_1_21.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CASTAATT,&i_cF..t_CASTAATT),this.value)
    return(iif(xVal =1,'T',;
    iif(xVal =2,'D',;
    iif(xVal =3,'I',;
    iif(xVal =4,'F',;
    iif(xVal =5,'P',;
    space(1)))))))
  endfunc
  func oCASTAATT_1_21.GetRadio()
    this.Parent.oContained.w_CASTAATT = this.RadioValue()
    return .t.
  endfunc

  func oCASTAATT_1_21.ToRadio()
    this.Parent.oContained.w_CASTAATT=trim(this.Parent.oContained.w_CASTAATT)
    return(;
      iif(this.Parent.oContained.w_CASTAATT=='T',1,;
      iif(this.Parent.oContained.w_CASTAATT=='D',2,;
      iif(this.Parent.oContained.w_CASTAATT=='I',3,;
      iif(this.Parent.oContained.w_CASTAATT=='F',4,;
      iif(this.Parent.oContained.w_CASTAATT=='P',5,;
      0))))))
  endfunc

  func oCASTAATT_1_21.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCASTAATT_1_21.mCond()
    with this.Parent.oContained
      return (.w_CAFLNSAP<>'S')
    endwith
  endfunc

  func oCASTAATT_1_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP='S')
    endwith
    endif
  endfunc


  add object oCASTAATT_1_22 as StdCombo with uid="CZXTTPPODH",rtseq=20,rtrep=.f.,left=90,top=121,width=141,height=21;
    , ToolTipText = "Stato da assegnare all'attivit�";
    , HelpContextID = 6336902;
    , cFormVar="w_CASTAATT",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCASTAATT_1_22.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CASTAATT,&i_cF..t_CASTAATT),this.value)
    return(iif(xVal =1,'T',;
    iif(xVal =2,'D',;
    iif(xVal =3,'I',;
    iif(xVal =4,'F',;
    space(1))))))
  endfunc
  func oCASTAATT_1_22.GetRadio()
    this.Parent.oContained.w_CASTAATT = this.RadioValue()
    return .t.
  endfunc

  func oCASTAATT_1_22.ToRadio()
    this.Parent.oContained.w_CASTAATT=trim(this.Parent.oContained.w_CASTAATT)
    return(;
      iif(this.Parent.oContained.w_CASTAATT=='T',1,;
      iif(this.Parent.oContained.w_CASTAATT=='D',2,;
      iif(this.Parent.oContained.w_CASTAATT=='I',3,;
      iif(this.Parent.oContained.w_CASTAATT=='F',4,;
      0)))))
  endfunc

  func oCASTAATT_1_22.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCASTAATT_1_22.mCond()
    with this.Parent.oContained
      return (.w_CAFLNSAP='S')
    endwith
  endfunc

  func oCASTAATT_1_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP<>'S')
    endwith
    endif
  endfunc


  add object oCAFLNOTI_1_23 as StdCombo with uid="OVQEHDWQLD",rtseq=21,rtrep=.f.,left=329,top=121,width=111,height=21;
    , ToolTipText = "Se attivo, nelle attivit� sar� gestito l'invio dell'avviso ai partecipanti";
    , HelpContextID = 241598063;
    , cFormVar="w_CAFLNOTI",RowSource=""+"Nessun avviso,"+"Avviso,"+"Avviso con VCS,"+"Avviso con ICS,"+"Avviso con VCS e ICS", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAFLNOTI_1_23.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLNOTI,&i_cF..t_CAFLNOTI),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    iif(xVal =3,'C',;
    iif(xVal =4,'I',;
    iif(xVal =5,'E',;
    space(1)))))))
  endfunc
  func oCAFLNOTI_1_23.GetRadio()
    this.Parent.oContained.w_CAFLNOTI = this.RadioValue()
    return .t.
  endfunc

  func oCAFLNOTI_1_23.ToRadio()
    this.Parent.oContained.w_CAFLNOTI=trim(this.Parent.oContained.w_CAFLNOTI)
    return(;
      iif(this.Parent.oContained.w_CAFLNOTI=='N',1,;
      iif(this.Parent.oContained.w_CAFLNOTI=='S',2,;
      iif(this.Parent.oContained.w_CAFLNOTI=='C',3,;
      iif(this.Parent.oContained.w_CAFLNOTI=='I',4,;
      iif(this.Parent.oContained.w_CAFLNOTI=='E',5,;
      0))))))
  endfunc

  func oCAFLNOTI_1_23.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oBtn_1_24 as StdButton with uid="HJEVXCUKCK",left=463, top=122, width=19,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 171502038;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CACOLPOS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oCAPRIORI_1_25 as StdCombo with uid="LLGYXHOWZX",rtseq=22,rtrep=.f.,left=90,top=144,width=141,height=21;
    , ToolTipText = "Priorit� da assegnare all'attivit� (normale, urgente, scadenza termine = da fare in un determinato giorno ad un'ora prestabilita)";
    , HelpContextID = 31646097;
    , cFormVar="w_CAPRIORI",RowSource=""+"Normale,"+"Urgente,"+"Scadenza termine", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAPRIORI_1_25.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAPRIORI,&i_cF..t_CAPRIORI),this.value)
    return(iif(xVal =1,1,;
    iif(xVal =2,3,;
    iif(xVal =3,4,;
    0))))
  endfunc
  func oCAPRIORI_1_25.GetRadio()
    this.Parent.oContained.w_CAPRIORI = this.RadioValue()
    return .t.
  endfunc

  func oCAPRIORI_1_25.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_CAPRIORI==1,1,;
      iif(this.Parent.oContained.w_CAPRIORI==3,2,;
      iif(this.Parent.oContained.w_CAPRIORI==4,3,;
      0))))
  endfunc

  func oCAPRIORI_1_25.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oBtn_1_26 as StdButton with uid="UEPNZSIHTA",left=253, top=145, width=19,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona colore";
    , HelpContextID = 171502038;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSUT_BCI(this.Parent.oContained,"CACOLORE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAPRIORI<>1)
    endwith
   endif
  endfunc


  add object oCAPRIEML_1_27 as StdCombo with uid="KOVCKRILXM",rtseq=23,rtrep=.f.,left=329,top=145,width=153,height=21;
    , ToolTipText = "Priorit� email";
    , HelpContextID = 199418254;
    , cFormVar="w_CAPRIEML",RowSource=""+"Bassa priorit�,"+"Normale,"+"Alta priorit�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAPRIEML_1_27.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAPRIEML,&i_cF..t_CAPRIEML),this.value)
    return(iif(xVal =1,5,;
    iif(xVal =2,3,;
    iif(xVal =3,1,;
    0))))
  endfunc
  func oCAPRIEML_1_27.GetRadio()
    this.Parent.oContained.w_CAPRIEML = this.RadioValue()
    return .t.
  endfunc

  func oCAPRIEML_1_27.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_CAPRIEML==5,1,;
      iif(this.Parent.oContained.w_CAPRIEML==3,2,;
      iif(this.Parent.oContained.w_CAPRIEML==1,3,;
      0))))
  endfunc

  func oCAPRIEML_1_27.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oCAMINPRE_1_28 as StdCombo with uid="XGYWJMHTEX",value=2,rtseq=24,rtrep=.f.,left=90,top=167,width=141,height=21;
    , ToolTipText = "Tempo di promemoria";
    , HelpContextID = 10228117;
    , cFormVar="w_CAMINPRE",RowSource=""+"Nessun promemoria,"+"0 minuti,"+"5 minuti prima,"+"10 minuti prima,"+"15 minuti prima,"+"30 minuti prima,"+"1 ora prima,"+"2 ore prima,"+"3 ore prima,"+"4 ore prima,"+"1 giorno prima,"+"2 giorni prima,"+"3 giorni prima,"+"1 settimana prima,"+"2 settimane prima,"+"3 settimane prima", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAMINPRE_1_28.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAMINPRE,&i_cF..t_CAMINPRE),this.value)
    return(iif(xVal =1,-1,;
    iif(xVal =2,0,;
    iif(xVal =3,5,;
    iif(xVal =4,10,;
    iif(xVal =5,15,;
    iif(xVal =6,30,;
    iif(xVal =7,60,;
    iif(xVal =8,120,;
    iif(xVal =9,180,;
    iif(xVal =10,240,;
    iif(xVal =11,1440,;
    iif(xVal =12,2880,;
    iif(xVal =13,4320,;
    iif(xVal =14,10080,;
    iif(xVal =15,20160,;
    iif(xVal =16,30240,;
    0)))))))))))))))))
  endfunc
  func oCAMINPRE_1_28.GetRadio()
    this.Parent.oContained.w_CAMINPRE = this.RadioValue()
    return .t.
  endfunc

  func oCAMINPRE_1_28.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_CAMINPRE==-1,1,;
      iif(this.Parent.oContained.w_CAMINPRE==0,2,;
      iif(this.Parent.oContained.w_CAMINPRE==5,3,;
      iif(this.Parent.oContained.w_CAMINPRE==10,4,;
      iif(this.Parent.oContained.w_CAMINPRE==15,5,;
      iif(this.Parent.oContained.w_CAMINPRE==30,6,;
      iif(this.Parent.oContained.w_CAMINPRE==60,7,;
      iif(this.Parent.oContained.w_CAMINPRE==120,8,;
      iif(this.Parent.oContained.w_CAMINPRE==180,9,;
      iif(this.Parent.oContained.w_CAMINPRE==240,10,;
      iif(this.Parent.oContained.w_CAMINPRE==1440,11,;
      iif(this.Parent.oContained.w_CAMINPRE==2880,12,;
      iif(this.Parent.oContained.w_CAMINPRE==4320,13,;
      iif(this.Parent.oContained.w_CAMINPRE==10080,14,;
      iif(this.Parent.oContained.w_CAMINPRE==20160,15,;
      iif(this.Parent.oContained.w_CAMINPRE==30240,16,;
      0)))))))))))))))))
  endfunc

  func oCAMINPRE_1_28.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAMINPRE_1_28.mCond()
    with this.Parent.oContained
      return (g_JBSH='S')
    endwith
  endfunc

  add object oCAFLRINV_1_29 as StdCheck with uid="MUXDSTMJPM",rtseq=25,rtrep=.f.,left=329, top=166, caption="Gestione rinvio",;
    ToolTipText = "Se attivo, queste attivit� potranno usufruire della gestione rinvio",;
    HelpContextID = 123306372,;
    cFormVar="w_CAFLRINV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLRINV_1_29.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLRINV,&i_cF..t_CAFLRINV),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCAFLRINV_1_29.GetRadio()
    this.Parent.oContained.w_CAFLRINV = this.RadioValue()
    return .t.
  endfunc

  func oCAFLRINV_1_29.ToRadio()
    this.Parent.oContained.w_CAFLRINV=trim(this.Parent.oContained.w_CAFLRINV)
    return(;
      iif(this.Parent.oContained.w_CAFLRINV=='S',1,;
      0))
  endfunc

  func oCAFLRINV_1_29.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAFLRINV_1_29.mCond()
    with this.Parent.oContained
      return (NOT .w_CARAGGST$'MA')
    endwith
  endfunc

  func oCAFLRINV_1_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CARAGGST$'MA' OR !.w_ISALT)
    endwith
    endif
  endfunc

  add object oCAFLTRIS_1_30 as StdCheck with uid="UEPUSISBVK",rtseq=26,rtrep=.f.,left=329, top=190, caption="Gestione riserve",;
    ToolTipText = "Se attivo, queste attivit� potranno usufruire della gestione riserve",;
    HelpContextID = 29785721,;
    cFormVar="w_CAFLTRIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLTRIS_1_30.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLTRIS,&i_cF..t_CAFLTRIS),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCAFLTRIS_1_30.GetRadio()
    this.Parent.oContained.w_CAFLTRIS = this.RadioValue()
    return .t.
  endfunc

  func oCAFLTRIS_1_30.ToRadio()
    this.Parent.oContained.w_CAFLTRIS=trim(this.Parent.oContained.w_CAFLTRIS)
    return(;
      iif(this.Parent.oContained.w_CAFLTRIS=='S',1,;
      0))
  endfunc

  func oCAFLTRIS_1_30.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAFLTRIS_1_30.mCond()
    with this.Parent.oContained
      return (NOT .w_CARAGGST$'MA')
    endwith
  endfunc

  func oCAFLTRIS_1_30.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISALT OR .w_CARAGGST$'MA')
    endwith
    endif
  endfunc


  add object oCACHKOBB_1_31 as StdCombo with uid="VWOARZVHKB",value=3,rtseq=27,rtrep=.f.,left=567,top=121,width=114,height=21;
    , ToolTipText = "Obbligatoriet� o meno della pratica in relazione ad una determinata attivit� nel momento in cui diventa prestazione";
    , HelpContextID = 30257560;
    , cFormVar="w_CACHKOBB",RowSource=""+"Obbligatoria,"+"Facoltativa,"+"Non gestita", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCACHKOBB_1_31.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CACHKOBB,&i_cF..t_CACHKOBB),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'N',;
    iif(xVal =3,' ',;
    space(1)))))
  endfunc
  func oCACHKOBB_1_31.GetRadio()
    this.Parent.oContained.w_CACHKOBB = this.RadioValue()
    return .t.
  endfunc

  func oCACHKOBB_1_31.ToRadio()
    this.Parent.oContained.w_CACHKOBB=trim(this.Parent.oContained.w_CACHKOBB)
    return(;
      iif(this.Parent.oContained.w_CACHKOBB=='P',1,;
      iif(this.Parent.oContained.w_CACHKOBB=='N',2,;
      iif(this.Parent.oContained.w_CACHKOBB=='',3,;
      0))))
  endfunc

  func oCACHKOBB_1_31.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCACHKOBB_1_31.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_ISALT)
    endwith
    endif
  endfunc


  add object oCACHKOBB_1_32 as StdCombo with uid="BPMCMCQGYF",value=3,rtseq=28,rtrep=.f.,left=566,top=121,width=172,height=21;
    , ToolTipText = "Obbligatoriet� o meno della commessa in relazione ad una determinata attivit�";
    , HelpContextID = 30257560;
    , cFormVar="w_CACHKOBB",RowSource=""+"Obbligatoria,"+"Facoltativa,"+"Non gestita", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCACHKOBB_1_32.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CACHKOBB,&i_cF..t_CACHKOBB),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'N',;
    iif(xVal =3,' ',;
    space(1)))))
  endfunc
  func oCACHKOBB_1_32.GetRadio()
    this.Parent.oContained.w_CACHKOBB = this.RadioValue()
    return .t.
  endfunc

  func oCACHKOBB_1_32.ToRadio()
    this.Parent.oContained.w_CACHKOBB=trim(this.Parent.oContained.w_CACHKOBB)
    return(;
      iif(this.Parent.oContained.w_CACHKOBB=='P',1,;
      iif(this.Parent.oContained.w_CACHKOBB=='N',2,;
      iif(this.Parent.oContained.w_CACHKOBB=='',3,;
      0))))
  endfunc

  func oCACHKOBB_1_32.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCACHKOBB_1_32.mCond()
    with this.Parent.oContained
      return (NOT .w_CARAGGST$'MA')
    endwith
  endfunc

  func oCACHKOBB_1_32.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CARAGGST$'MA' OR  .w_ISALT OR .w_ISAHE)
    endwith
    endif
  endfunc


  add object oCACHKNOM_1_33 as StdCombo with uid="CGMBAWUVBB",value=3,rtseq=29,rtrep=.f.,left=566,top=144,width=172,height=21;
    , ToolTipText = "Gestione nominativo";
    , HelpContextID = 47034765;
    , cFormVar="w_CACHKNOM",RowSource=""+"Obbligatorio,"+"Facoltativo,"+"Non gestito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCACHKNOM_1_33.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CACHKNOM,&i_cF..t_CACHKNOM),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'N',;
    iif(xVal =3,' ',;
    space(1)))))
  endfunc
  func oCACHKNOM_1_33.GetRadio()
    this.Parent.oContained.w_CACHKNOM = this.RadioValue()
    return .t.
  endfunc

  func oCACHKNOM_1_33.ToRadio()
    this.Parent.oContained.w_CACHKNOM=trim(this.Parent.oContained.w_CACHKNOM)
    return(;
      iif(this.Parent.oContained.w_CACHKNOM=='P',1,;
      iif(this.Parent.oContained.w_CACHKNOM=='N',2,;
      iif(this.Parent.oContained.w_CACHKNOM=='',3,;
      0))))
  endfunc

  func oCACHKNOM_1_33.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCACHKNOM_1_33.mCond()
    with this.Parent.oContained
      return (!.w_CARAGGST$'MA')
    endwith
  endfunc

  func oCACHKNOM_1_33.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CARAGGST$'MA')
    endwith
    endif
  endfunc


  add object oCADISPON_1_34 as StdCombo with uid="HGZPVJTLFR",rtseq=30,rtrep=.f.,left=566,top=167,width=172,height=21;
    , ToolTipText = "Disponibilit�";
    , HelpContextID = 5022092;
    , cFormVar="w_CADISPON",RowSource=""+"Occupato,"+"Fuori sede,"+"Per urgenze,"+"Libero", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCADISPON_1_34.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CADISPON,&i_cF..t_CADISPON),this.value)
    return(iif(xVal =1,2,;
    iif(xVal =2,4,;
    iif(xVal =3,3,;
    iif(xVal =4,1,;
    0)))))
  endfunc
  func oCADISPON_1_34.GetRadio()
    this.Parent.oContained.w_CADISPON = this.RadioValue()
    return .t.
  endfunc

  func oCADISPON_1_34.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_CADISPON==2,1,;
      iif(this.Parent.oContained.w_CADISPON==4,2,;
      iif(this.Parent.oContained.w_CADISPON==3,3,;
      iif(this.Parent.oContained.w_CADISPON==1,4,;
      0)))))
  endfunc

  func oCADISPON_1_34.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCADISPON_1_34.mCond()
    with this.Parent.oContained
      return (NOT .w_CARAGGST$'DM')
    endwith
  endfunc

  func oCADISPON_1_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CARAGGST$'DM')
    endwith
    endif
  endfunc

  add object oCAFLSTAT_1_35 as StdCheck with uid="IROFQWCCIM",rtseq=31,rtrep=.f.,left=90, top=190, caption="Stampa immediata",;
    ToolTipText = "Se attivo, dopo la conferma dell'attivit� verr� lanciata la relativa stampa",;
    HelpContextID = 206143878,;
    cFormVar="w_CAFLSTAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLSTAT_1_35.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLSTAT,&i_cF..t_CAFLSTAT),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCAFLSTAT_1_35.GetRadio()
    this.Parent.oContained.w_CAFLSTAT = this.RadioValue()
    return .t.
  endfunc

  func oCAFLSTAT_1_35.ToRadio()
    this.Parent.oContained.w_CAFLSTAT=trim(this.Parent.oContained.w_CAFLSTAT)
    return(;
      iif(this.Parent.oContained.w_CAFLSTAT=='S',1,;
      0))
  endfunc

  func oCAFLSTAT_1_35.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oCAFLATRI_1_36 as StdCombo with uid="HLAGSFRMZX",rtseq=32,rtrep=.f.,left=329,top=217,width=153,height=21;
    , ToolTipText = "Valore predefinito per l'attributo visibilit� dell'attivit�";
    , HelpContextID = 225018257;
    , cFormVar="w_CAFLATRI",RowSource=""+"Riservata,"+"Libera,"+"Nascosta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAFLATRI_1_36.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLATRI,&i_cF..t_CAFLATRI),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    iif(xVal =3,'H',;
    space(1)))))
  endfunc
  func oCAFLATRI_1_36.GetRadio()
    this.Parent.oContained.w_CAFLATRI = this.RadioValue()
    return .t.
  endfunc

  func oCAFLATRI_1_36.ToRadio()
    this.Parent.oContained.w_CAFLATRI=trim(this.Parent.oContained.w_CAFLATRI)
    return(;
      iif(this.Parent.oContained.w_CAFLATRI=='S',1,;
      iif(this.Parent.oContained.w_CAFLATRI=='N',2,;
      iif(this.Parent.oContained.w_CAFLATRI=='H',3,;
      0))))
  endfunc

  func oCAFLATRI_1_36.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAFLATRI_1_36.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GESRIS<>'S')
    endwith
    endif
  endfunc

  add object oCACAUABB_1_37 as StdField with uid="QZPZQIKAJR",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CACAUABB", cQueryName = "CACAUABB",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale incongruente",;
    ToolTipText = "Causale nuovo documento abbinato",;
    HelpContextID = 255111576,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=476, Top=302, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAR_BLT", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CACAUABB"

  func oCACAUABB_1_37.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S" or .w_ISALT)
    endwith
    endif
  endfunc

  proc oCACAUABB_1_37.mBefore
    with this.Parent.oContained
      .w_TDFLVEAC='V'
    endwith
  endproc

  func oCACAUABB_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACAUABB_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACAUABB_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCACAUABB_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oCACAUABB_1_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BLT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_CACAUABB
    i_obj.ecpSave()
  endproc


  add object oCACHKINI_1_38 as StdCombo with uid="AMPMQFJNHB",rtseq=34,rtrep=.f.,left=566,top=190,width=172,height=21;
    , ToolTipText = "Metodo di calcolo della data inizio nel caso di generazione attivit� collegate";
    , HelpContextID = 130920849;
    , cFormVar="w_CACHKINI",RowSource=""+"Con conferma,"+"Automatica,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCACHKINI_1_38.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CACHKINI,&i_cF..t_CACHKINI),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'A',;
    iif(xVal =3,'N',;
    space(1)))))
  endfunc
  func oCACHKINI_1_38.GetRadio()
    this.Parent.oContained.w_CACHKINI = this.RadioValue()
    return .t.
  endfunc

  func oCACHKINI_1_38.ToRadio()
    this.Parent.oContained.w_CACHKINI=trim(this.Parent.oContained.w_CACHKINI)
    return(;
      iif(this.Parent.oContained.w_CACHKINI=='C',1,;
      iif(this.Parent.oContained.w_CACHKINI=='A',2,;
      iif(this.Parent.oContained.w_CACHKINI=='N',3,;
      0))))
  endfunc

  func oCACHKINI_1_38.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCACHKINI_1_38.mCond()
    with this.Parent.oContained
      return (.w_PACHKFES<>'N')
    endwith
  endfunc

  add object oCAPUBWEB_1_39 as StdCheck with uid="CPKLYDVOUE",rtseq=35,rtrep=.f.,left=90, top=213, caption="Pubblica su Web",;
    HelpContextID = 95428200,;
    cFormVar="w_CAPUBWEB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAPUBWEB_1_39.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAPUBWEB,&i_cF..t_CAPUBWEB),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCAPUBWEB_1_39.GetRadio()
    this.Parent.oContained.w_CAPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oCAPUBWEB_1_39.ToRadio()
    this.Parent.oContained.w_CAPUBWEB=trim(this.Parent.oContained.w_CAPUBWEB)
    return(;
      iif(this.Parent.oContained.w_CAPUBWEB=='S',1,;
      0))
  endfunc

  func oCAPUBWEB_1_39.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAPUBWEB_1_39.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
    endif
  endfunc

  add object oCOLORE_1_40 as StdField with uid="ZCEBGASECM",rtseq=36,rtrep=.f.,;
    cFormVar = "w_COLORE", cQueryName = "COLORE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore relativo alla visualizzazione dei dati per le attivit� con priorit� normale",;
    HelpContextID = 190190554,;
   bGlobalFont=.t.,;
    Height=19, Width=19, Left=233, Top=145, InputMask=replicate('X',10)

  func oCOLORE_1_40.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAPRIORI<>1)
    endwith
    endif
  endfunc

  add object oCOLOREP_1_41 as StdField with uid="ZAAQQQGIAD",rtseq=37,rtrep=.f.,;
    cFormVar = "w_COLOREP", cQueryName = "COLOREP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore di sfondo avvisi post-in",;
    HelpContextID = 190190554,;
   bGlobalFont=.t.,;
    Height=19, Width=19, Left=442, Top=122, InputMask=replicate('X',10)

  add object oCACHKFOR_1_42 as StdCheck with uid="SZVHUJOINL",rtseq=38,rtrep=.f.,left=566, top=213, caption="Sospensioni forensi",;
    ToolTipText = "Se attivo nella determinazione della data inizio delle attivit� tiene conto anche della sospensione forense",;
    HelpContextID = 181252488,;
    cFormVar="w_CACHKFOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCACHKFOR_1_42.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CACHKFOR,&i_cF..t_CACHKFOR),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCACHKFOR_1_42.GetRadio()
    this.Parent.oContained.w_CACHKFOR = this.RadioValue()
    return .t.
  endfunc

  func oCACHKFOR_1_42.ToRadio()
    this.Parent.oContained.w_CACHKFOR=trim(this.Parent.oContained.w_CACHKFOR)
    return(;
      iif(this.Parent.oContained.w_CACHKFOR=='S',1,;
      0))
  endfunc

  func oCACHKFOR_1_42.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCACHKFOR_1_42.mCond()
    with this.Parent.oContained
      return (.w_PACHKFES<>'N')
    endwith
  endfunc

  func oCACHKFOR_1_42.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_ISALT Or .w_CACHKINI='N' Or .w_PACHKFES<>'C')
    endwith
    endif
  endfunc

  add object oCAFLNSAP_1_56 as StdCheck with uid="YFCRPJDWJC",rtseq=52,rtrep=.f.,left=90, top=248, caption="Non soggetta a prestazione",;
    ToolTipText = "Se attivo, l�attivit� non potr� essere soggetta a prestazione (es: assenze)",;
    HelpContextID = 228163978,;
    cFormVar="w_CAFLNSAP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLNSAP_1_56.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLNSAP,&i_cF..t_CAFLNSAP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCAFLNSAP_1_56.GetRadio()
    this.Parent.oContained.w_CAFLNSAP = this.RadioValue()
    return .t.
  endfunc

  func oCAFLNSAP_1_56.ToRadio()
    this.Parent.oContained.w_CAFLNSAP=trim(this.Parent.oContained.w_CAFLNSAP)
    return(;
      iif(this.Parent.oContained.w_CAFLNSAP=='S',1,;
      0))
  endfunc

  func oCAFLNSAP_1_56.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAFLNSAP_1_56.mCond()
    with this.Parent.oContained
      return (NOT .w_CARAGGST$'MA')
    endwith
  endfunc

  add object oCAFLANAL_1_58 as StdCheck with uid="JVMLCUIYVF",rtseq=54,rtrep=.f.,left=90, top=267, caption="Movimento di analitica",;
    ToolTipText = "Se attivo, determina creazione movimento di analitica nell'attivit�",;
    HelpContextID = 57246094,;
    cFormVar="w_CAFLANAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAFLANAL_1_58.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLANAL,&i_cF..t_CAFLANAL),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCAFLANAL_1_58.GetRadio()
    this.Parent.oContained.w_CAFLANAL = this.RadioValue()
    return .t.
  endfunc

  func oCAFLANAL_1_58.ToRadio()
    this.Parent.oContained.w_CAFLANAL=trim(this.Parent.oContained.w_CAFLANAL)
    return(;
      iif(this.Parent.oContained.w_CAFLANAL=='S',1,;
      0))
  endfunc

  func oCAFLANAL_1_58.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAFLANAL_1_58.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_COAN<>'S' AND .w_ACQU<>'S') OR .w_CAFLNSAP='S' OR .w_ISAHE)
    endwith
    endif
  endfunc

  add object oCACAUDOC_1_59 as StdField with uid="DYXAACQCRH",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CACAUDOC", cQueryName = "CACAUDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale incongruente",;
    HelpContextID = 204779927,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=476, Top=247, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAR_BLT", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CACAUDOC"

  func oCACAUDOC_1_59.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S")
    endwith
    endif
  endfunc

  proc oCACAUDOC_1_59.mBefore
    with this.Parent.oContained
      .w_TDFLVEAC='V'
    endwith
  endproc

  func oCACAUDOC_1_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_59('Part',this)
      if bRes and !(!.w_ISAHE OR .w_FLGLISV<>"S" OR  .w_CARAGGST<>'Z')
         bRes=(cp_WarningMsg(thisform.msgFmt("Il flag <Vendita su listino> della causale documento � attivo, ma verr� ignorato")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oCACAUDOC_1_59.ecpDrop(oSource)
    this.Parent.oContained.link_1_59('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACAUDOC_1_59.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCACAUDOC_1_59'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oCACAUDOC_1_59.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BLT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_CACAUDOC
    i_obj.ecpSave()
  endproc

  add object oCACAUACQ_1_60 as StdField with uid="BRAMYYXKOI",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CACAUACQ", cQueryName = "CACAUACQ",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale con intestatario o di tipo incongruente",;
    HelpContextID = 255111561,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=476, Top=275, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAR_BLT", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CACAUACQ"

  func oCACAUACQ_1_60.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S" OR (!.w_ISAHE AND .w_CAFLANAL<>'N') OR .w_ISALT OR .w_ACQU='N')
    endwith
    endif
  endfunc

  proc oCACAUACQ_1_60.mBefore
    with this.Parent.oContained
      .w_TDFLVEAC='A'
    endwith
  endproc

  func oCACAUACQ_1_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_60('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACAUACQ_1_60.ecpDrop(oSource)
    this.Parent.oContained.link_1_60('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACAUACQ_1_60.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCACAUACQ_1_60'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oCACAUACQ_1_60.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BLT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_CACAUACQ
    i_obj.ecpSave()
  endproc


  add object oCATIPEVE_1_61 as StdTableCombo with uid="RYVGKRSVNP",rtseq=57,rtrep=.f.,left=90,top=303,width=204,height=21;
    , ToolTipText = "Tipo evento";
    , HelpContextID = 75783787;
    , cFormVar="w_CATIPEVE",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='..\AGEN\EXE\QUERY\GSAG_QCT.VQR',cKey='TETIPEVE',cValue='TEDESCRI',cOrderBy='TEDESCRI',xDefault=space(10);
  , bGlobalFont=.t.


  func oCATIPEVE_1_61.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_AGFA <>'S' )
    endwith
    endif
  endfunc

  add object oDESCAUDI_1_68 as StdField with uid="UNTUGHSXMT",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DESCAUDI", cQueryName = "DESCAUDI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59658879,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=542, Top=248, InputMask=replicate('X',35)

  func oDESCAUDI_1_68.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S")
    endwith
    endif
  endfunc


  add object oBtn_1_88 as StdButton with uid="ORSWOUHUED",left=690, top=11, width=48,height=45,;
    CpPicture="copy.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per duplicare il tipo attivit�";
    , HelpContextID = 197569994;
    , TabStop=.f.,Caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_88.Click()
      do GSAG_KTA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_88.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction#"Query" OR (.cFunction="Query" AND EMPTY(.w_CACODICE)))
    endwith
   endif
  endfunc


  add object oBtn_1_89 as StdButton with uid="JOBWVJFNSM",left=690, top=58, width=48,height=45,;
    CpPicture="copy.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per comporre descrizione aggiuntiva attivit� in agenda";
    , HelpContextID = 225145444;
    , TabStop=.f.,Caption='\<Des.Ag.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_89.Click()
      do GSAG_KCD with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_89.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CACODICE))
    endwith
   endif
  endfunc

  add object oDESTIPAT_1_95 as StdField with uid="ZISSQCUENS",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESTIPAT", cQueryName = "DESTIPAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 14724470,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=419, Top=38, InputMask=replicate('X',50)

  add object oCA__NOTE_1_97 as StdMemo with uid="NDIWPMSRMP",rtseq=68,rtrep=.f.,;
    cFormVar = "w_CA__NOTE", cQueryName = "CA__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 242945643,;
   bGlobalFont=.t.,;
    Height=149, Width=714, Left=21, Top=345

  func oCA__NOTE_1_97.mCond()
    with this.Parent.oContained
      return (.w_CAFLNSAP=='S')
    endwith
  endfunc

  func oCA__NOTE_1_97.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP<>'S')
    endwith
    endif
  endfunc


  add object oObj_1_102 as cp_setobjprop with uid="ATELPAVSLO",left=552, top=611, width=191,height=19,;
    caption='Tooltip di w_CACAUDOC',;
   bGlobalFont=.t.,;
    cObj="w_CACAUDOC",cProp="ToolTipText",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 148260329

  add object oDESCAUAC_1_113 as StdField with uid="ALHWZESWKV",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DESCAUAC", cQueryName = "DESCAUAC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 208776583,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=542, Top=275, InputMask=replicate('X',35)

  func oDESCAUAC_1_113.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S" OR (!.w_ISAHE AND .w_CAFLANAL<>'N') OR .w_ISALT OR .w_ACQU='N')
    endwith
    endif
  endfunc


  add object oObj_1_124 as cp_setobjprop with uid="OMXITLGVUX",left=808, top=474, width=197,height=22,;
    caption='Colore attivit� con priorit� normale',;
   bGlobalFont=.t.,;
    cObj="w_COLORE",cProp="disabledbackcolor",;
    nPag=1;
    , ToolTipText = "Colore attivit�";
    , HelpContextID = 61585944


  add object oObj_1_127 as cp_setobjprop with uid="LFXOPCYZWJ",left=808, top=494, width=197,height=22,;
    caption='Colore sfondo postin',;
   bGlobalFont=.t.,;
    cObj="w_COLOREP",cProp="disabledbackcolor",;
    nPag=1;
    , ToolTipText = "Colore postin";
    , HelpContextID = 245132696

  add object oDESABB_1_131 as StdField with uid="RDFCVRFFXM",rtseq=113,rtrep=.f.,;
    cFormVar = "w_DESABB", cQueryName = "DESABB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 258190794,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=542, Top=302, InputMask=replicate('X',35)

  func oDESABB_1_131.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S" or .w_ISALT)
    endwith
    endif
  endfunc


  add object oCATIPPRA_1_139 as StdTableCombo with uid="QDRZGNYYZD",rtseq=119,rtrep=.f.,left=434,top=275,width=299,height=21;
    , ToolTipText = "Tipo pratica";
    , HelpContextID = 8102297;
    , cFormVar="w_CATIPPRA",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="PRA_TIPI";
    , cTable='PRA_TIPI',cKey='TPCODICE',cValue='TPDESCRI',cOrderBy='TPDESCRI',xDefault=space(10);
  , bGlobalFont=.t.


  func oCATIPPRA_1_139.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
    endif
  endfunc

  func oCATIPPRA_1_139.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_139('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATIPPRA_1_139.ecpDrop(oSource)
    this.Parent.oContained.link_1_139('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oCAMATPRA_1_140 as StdTableCombo with uid="KLCNQTFGRK",rtseq=120,rtrep=.f.,left=434,top=302,width=299,height=21;
    , ToolTipText = "Materia pratica";
    , HelpContextID = 4460953;
    , cFormVar="w_CAMATPRA",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="PRA_MATE";
    , cTable='PRA_MATE',cKey='MPCODICE',cValue='MPDESCRI',cOrderBy='MPDESCRI',xDefault=space(10);
  , bGlobalFont=.t.


  func oCAMATPRA_1_140.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
    endif
  endfunc

  func oCAMATPRA_1_140.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_140('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAMATPRA_1_140.ecpDrop(oSource)
    this.Parent.oContained.link_1_140('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oCA__ENTE_1_141 as StdTableCombo with uid="ATGDRWEVNU",rtseq=121,rtrep=.f.,left=439,top=322,width=299,height=21;
    , ToolTipText = "Ente (ufficio giudiziario se pratica giudiziale)";
    , HelpContextID = 216731243;
    , cFormVar="w_CA__ENTE",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="PRA_ENTI";
    , cTable='QUERY\PRAENTIZOOM.VQR',cKey='EPCODICE',cValue='EPDESCRI',cOrderBy='',xDefault=space(10);
  , bGlobalFont=.t.


  func oCA__ENTE_1_141.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
    endif
  endfunc

  func oCA__ENTE_1_141.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_141('Part',this)
    endwith
    return bRes
  endfunc

  proc oCA__ENTE_1_141.ecpDrop(oSource)
    this.Parent.oContained.link_1_141('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oLinkPC_1_149 as StdButton with uid="ZWXRAREOLQ",left=19, top=248, width=48,height=45,;
    CpPicture="BMP\UTEPOS2.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per associare il tipo attivit� gli utenti in modalit� wizard";
    , HelpContextID = 161125178;
    , TabStop=.f.,Caption='\<Utenti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_149.Click()
      this.Parent.oContained.GSAG_MUM.LinkPCClick()
    endproc

  func oLinkPC_1_149.mCond()
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
  endfunc

  func oLinkPC_1_149.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISALT OR (.cFunction="Query" AND EMPTY(.w_CACODICE)))
    endwith
   endif
  endfunc

  add object oStr_1_63 as StdString with uid="KINFAALLGV",Visible=.t., Left=16, Top=14,;
    Alignment=1, Width=70, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="FDMTJFSAVY",Visible=.t., Left=43, Top=122,;
    Alignment=1, Width=43, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="XDRBXBOHND",Visible=.t., Left=415, Top=250,;
    Alignment=1, Width=54, Height=18,;
    Caption="Tipo doc.:"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (.w_CAFLNSAP="S" OR NOT .w_ISALT)
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="EGKGJSKCRD",Visible=.t., Left=469, Top=66,;
    Alignment=0, Width=6, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="DFVISOOQHK",Visible=.t., Left=326, Top=66,;
    Alignment=1, Width=105, Height=18,;
    Caption="Durata presunta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="OOGGFIUUJH",Visible=.t., Left=286, Top=66,;
    Alignment=0, Width=6, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="VRXJAKUKAZ",Visible=.t., Left=4, Top=145,;
    Alignment=1, Width=82, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="LDCIEYPEWN",Visible=.t., Left=522, Top=66,;
    Alignment=1, Width=117, Height=18,;
    Caption="Giorni di preavviso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="AVIOCWGQQA",Visible=.t., Left=209, Top=63,;
    Alignment=1, Width=45, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="EEONDWVOKX",Visible=.t., Left=16, Top=39,;
    Alignment=1, Width=70, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="ETBSYMPIBD",Visible=.t., Left=20, Top=457,;
    Alignment=0, Width=166, Height=18,;
    Caption="Descrizione aggiuntiva"  ;
  , bGlobalFont=.t.

  func oStr_1_86.mHide()
    with this.Parent.oContained
      return (.w_CAFLNSAP="S")
    endwith
  endfunc

  add object oStr_1_87 as StdString with uid="UGVRLUHOUA",Visible=.t., Left=472, Top=168,;
    Alignment=1, Width=91, Height=18,;
    Caption="Disponibilit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (.w_CARAGGST$'DM')
    endwith
  endfunc

  add object oStr_1_92 as StdString with uid="KWIFCXDIUN",Visible=.t., Left=481, Top=122,;
    Alignment=1, Width=82, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return ( NOT .w_ISALT)
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="OMDXTPKCPO",Visible=.t., Left=472, Top=144,;
    Alignment=1, Width=91, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (.w_CARAGGST$'MA')
    endwith
  endfunc

  add object oStr_1_94 as StdString with uid="KMXOQPCYQZ",Visible=.t., Left=265, Top=39,;
    Alignment=1, Width=71, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="UHDAMSHAZF",Visible=.t., Left=19, Top=326,;
    Alignment=0, Width=93, Height=18,;
    Caption="Note"  ;
  , bGlobalFont=.t.

  func oStr_1_98.mHide()
    with this.Parent.oContained
      return (.w_CAFLNSAP<>'S')
    endwith
  endfunc

  add object oStr_1_100 as StdString with uid="UXLLVRKWHR",Visible=.t., Left=10, Top=168,;
    Alignment=1, Width=76, Height=18,;
    Caption="Promemoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_101 as StdString with uid="NFCHWYYOZD",Visible=.t., Left=486, Top=122,;
    Alignment=1, Width=77, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_101.mHide()
    with this.Parent.oContained
      return (.w_CARAGGST$'MA' OR .w_ISALT or .w_ISAHE)
    endwith
  endfunc

  add object oStr_1_110 as StdString with uid="ASITDWTOLS",Visible=.t., Left=491, Top=190,;
    Alignment=1, Width=72, Height=18,;
    Caption="Calcolo data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_112 as StdString with uid="LAIKBTUMJU",Visible=.t., Left=337, Top=250,;
    Alignment=1, Width=135, Height=18,;
    Caption="Causale doc. ven.:"  ;
  , bGlobalFont=.t.

  func oStr_1_112.mHide()
    with this.Parent.oContained
      return (.w_CAFLNSAP="S" OR .w_ISALT)
    endwith
  endfunc

  add object oStr_1_114 as StdString with uid="HRELXDIBLW",Visible=.t., Left=329, Top=276,;
    Alignment=1, Width=143, Height=18,;
    Caption="Causale doc. acq.:"  ;
  , bGlobalFont=.t.

  func oStr_1_114.mHide()
    with this.Parent.oContained
      return (.w_CAFLNSAP="S" OR (!.w_ISAHE AND .w_CAFLANAL<>'N') OR .w_ISALT OR .w_ACQU='N')
    endwith
  endfunc

  add object oStr_1_119 as StdString with uid="YNKWOPMAET",Visible=.t., Left=233, Top=122,;
    Alignment=1, Width=94, Height=18,;
    Caption="Gestione avvisi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_120 as StdString with uid="NFGSIPLVGL",Visible=.t., Left=233, Top=218,;
    Alignment=1, Width=94, Height=18,;
    Caption="Visibilit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_120.mHide()
    with this.Parent.oContained
      return (.w_GESRIS<>'S')
    endwith
  endfunc

  add object oStr_1_126 as StdString with uid="NIYUGLESYW",Visible=.t., Left=288, Top=145,;
    Alignment=1, Width=39, Height=18,;
    Caption="Email:"  ;
  , bGlobalFont=.t.

  add object oStr_1_129 as StdString with uid="EEALFUIMVT",Visible=.t., Left=307, Top=304,;
    Alignment=1, Width=165, Height=18,;
    Caption="Causale doc. di abbinamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_129.mHide()
    with this.Parent.oContained
      return (.w_CAFLNSAP="S" or .w_ISALT)
    endwith
  endfunc

  add object oStr_1_134 as StdString with uid="WRCOUHTHXY",Visible=.t., Left=20, Top=303,;
    Alignment=1, Width=66, Height=18,;
    Caption="Tipo evento:"  ;
  , bGlobalFont=.t.

  func oStr_1_134.mHide()
    with this.Parent.oContained
      return (g_AGFA <>'S' )
    endwith
  endfunc

  add object oStr_1_142 as StdString with uid="CKWRSFNLWA",Visible=.t., Left=395, Top=276,;
    Alignment=1, Width=34, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_142.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_1_143 as StdString with uid="NZZOJDNASN",Visible=.t., Left=382, Top=299,;
    Alignment=1, Width=47, Height=15,;
    Caption="Materia:"  ;
  , bGlobalFont=.t.

  func oStr_1_143.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_1_144 as StdString with uid="ZPQFKFPUGN",Visible=.t., Left=361, Top=322,;
    Alignment=1, Width=68, Height=15,;
    Caption="Ente:"  ;
  , bGlobalFont=.t.

  func oStr_1_144.mHide()
    with this.Parent.oContained
      return (.w_FLGIUD='G' OR !.w_ISALT)
    endwith
  endfunc

  add object oStr_1_145 as StdString with uid="FDSDASDXIO",Visible=.t., Left=361, Top=322,;
    Alignment=1, Width=68, Height=15,;
    Caption="Uff. Giudiz.:"  ;
  , bGlobalFont=.t.

  func oStr_1_145.mHide()
    with this.Parent.oContained
      return (.w_FLGIUD<>'G' OR !.w_ISALT)
    endwith
  endfunc

  add object oStr_1_150 as StdString with uid="DEKEZAZUCR",Visible=.t., Left=525, Top=529,;
    Alignment=1, Width=109, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_157 as StdString with uid="LPKJQBCJSO",Visible=.t., Left=393, Top=88,;
    Alignment=1, Width=38, Height=15,;
    Caption="Serie:"  ;
  , bGlobalFont=.t.

  func oStr_1_157.mHide()
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
  endfunc

  add object oStr_1_158 as StdString with uid="UKECWRTEPG",Visible=.t., Left=212, Top=88,;
    Alignment=1, Width=76, Height=15,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  func oStr_1_158.mHide()
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
  endfunc

  add object oBox_1_84 as StdBox with uid="ESOXKRNVLD",left=0, top=112, width=728,height=2

  add object oBox_1_85 as StdBox with uid="HKODHCKKFF",left=0, top=242, width=728,height=2

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=21,top=371,;
    width=697+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*4*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=22,top=372,width=696+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*4*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCADESAGG_2_25.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oCAKEYART_2_4
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oCACODSER_2_5
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oCADESAGG_2_25 as StdTrsMemo with uid="OVNKQHLZCJ",rtseq=109,rtrep=.t.,;
    cFormVar="w_CADESAGG",value=space(0),;
    ToolTipText = "Eventuale descrizione aggiuntiva della prestazione",;
    HelpContextID = 11492973,;
    cTotal="", bFixedPos=.t., cQueryName = "CADESAGG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=46, Width=682, Left=31, Top=476

  func oCADESAGG_2_25.mCond()
    with this.Parent.oContained
      return (not empty(.w_CACODSER) or not empty(.w_CAKEYART))
    endwith
  endfunc

  func oCADESAGG_2_25.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S")
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsag_mcaPag2 as StdContainer
    Width  = 743
    height = 549
    stdWidth  = 743
    stdheight = 549
  resizeXpos=579
  resizeYpos=351
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_4_1 as stdDynamicChildContainer with uid="OPKOEGMPCN",left=46, top=77, width=682, height=233, bOnScreen=.t.;



  add object oCAMODAPP_4_2 as StdCombo with uid="LRTLAIDOLK",rtseq=118,rtrep=.f.,left=154,top=36,width=220,height=21;
    , HelpContextID = 3543434;
    , cFormVar="w_CAMODAPP",RowSource=""+"Predefinito,"+"Dettaglio partecipanti,"+"Predefinito + dettaglio partecipanti", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oCAMODAPP_4_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAMODAPP,&i_cF..t_CAMODAPP),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'D',;
    iif(xVal =3,'E',;
    space(1)))))
  endfunc
  func oCAMODAPP_4_2.GetRadio()
    this.Parent.oContained.w_CAMODAPP = this.RadioValue()
    return .t.
  endfunc

  func oCAMODAPP_4_2.ToRadio()
    this.Parent.oContained.w_CAMODAPP=trim(this.Parent.oContained.w_CAMODAPP)
    return(;
      iif(this.Parent.oContained.w_CAMODAPP=='P',1,;
      iif(this.Parent.oContained.w_CAMODAPP=='D',2,;
      iif(this.Parent.oContained.w_CAMODAPP=='E',3,;
      0))))
  endfunc

  func oCAMODAPP_4_2.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oBtn_4_4 as StdButton with uid="ZAPOEQADXT",left=9, top=75, width=22,height=20,;
    CpPicture="bmp\INS_RAP.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per eseguire il caricamento multiplo dei partecipanti";
    , HelpContextID = 171502038;
    , IMGSIZE=16;
  , bGlobalFont=.t.

    proc oBtn_4_4.Click()
      do gsag_kpg with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_4.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Query')
    endwith
  endfunc

  add object oStr_4_3 as StdString with uid="AXQVHMTLHW",Visible=.t., Left=16, Top=35,;
    Alignment=1, Width=134, Height=18,;
    Caption="Modalit� di applicazione:"  ;
  , bGlobalFont=.t.
enddefine

  define class tgsag_mcaPag3 as StdContainer
    Width  = 743
    height = 549
    stdWidth  = 743
    stdheight = 549
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCA__NOTE_5_1 as StdMemo with uid="LFEGJGCSJA",rtseq=115,rtrep=.f.,;
    cFormVar = "w_CA__NOTE", cQueryName = "CA__NOTE",;
    bObbl = .f. , nPag = 5, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 242945643,;
   bGlobalFont=.t.,;
    Height=510, Width=616, Left=83, Top=9

  func oCA__NOTE_5_1.mCond()
    with this.Parent.oContained
      return (.w_CAFLNSAP<>'S')
    endwith
  endfunc

  func oCA__NOTE_5_1.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP=='S')
    endwith
    endif
  endfunc

  add object oStr_5_2 as StdString with uid="SWAXVQVOBO",Visible=.t., Left=5, Top=11,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsag_mcaBodyRow as CPBodyRowCnt
  Width=687
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oRIGA_2_1 as StdTrsField with uid="PRSGKYQISJ",rtseq=85,rtrep=.t.,;
    cFormVar="w_RIGA",value=space(1),;
    HelpContextID = 175871766,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=2, Left=-1, Top=0, InputMask=replicate('X',1)

  add object oCPROWORD_2_2 as StdTrsField with uid="NRKXHVJSOI",rtseq=86,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 17150614,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"], TabStop=.f.

  func oCPROWORD_2_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S")
    endwith
    endif
  endfunc

  add object oCAKEYART_2_4 as StdTrsField with uid="HJKBXDHDDR",rtseq=88,rtrep=.t.,;
    cFormVar="w_CAKEYART",value=space(41),;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 250622342,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=147, Left=40, Top=0, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_CAKEYART"

  func oCAKEYART_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S" OR ! .w_ISAHE)
    endwith
    endif
  endfunc

  func oCAKEYART_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAKEYART_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCAKEYART_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCAKEYART_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCAKEYART_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CAKEYART
    i_obj.ecpSave()
  endproc

  add object oCACODSER_2_5 as StdTrsField with uid="HPMYWMJOHL",rtseq=89,rtrep=.t.,;
    cFormVar="w_CACODSER",value=space(20),;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 29970040,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=147, Left=40, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_CACODSER"

  func oCACODSER_2_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S" or .w_ISAHE)
    endwith
    endif
  endfunc

  func oCACODSER_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODSER_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCACODSER_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCACODSER_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCACODSER_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CACODSER
    i_obj.ecpSave()
  endproc

  add object oCADESSER_2_11 as StdTrsField with uid="GOOUWARFBK",rtseq=95,rtrep=.t.,;
    cFormVar="w_CADESSER",value=space(40),;
    ToolTipText = "Descrizione della prestazione",;
    HelpContextID = 45047416,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=213, Left=192, Top=0, InputMask=replicate('X',40)

  func oCADESSER_2_11.mCond()
    with this.Parent.oContained
      return (not empty(.w_CACODSER) or not empty(.w_CAKEYART))
    endwith
  endfunc

  func oCADESSER_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S")
    endwith
    endif
  endfunc

  add object oATIPRIG_2_14 as StdTrsCombo with uid="AGEBLOUZJC",rtrep=.t.,;
    cFormVar="w_ATIPRIG", RowSource=""+"Non fatturabile,"+"Fatturabile" , ;
    ToolTipText = "Tipologia riga per la generazione del documento.",;
    HelpContextID = 145408262,;
    Height=21, Width=114, Left=412, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oATIPRIG_2_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATIPRIG,&i_cF..t_ATIPRIG),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    space(1))))
  endfunc
  func oATIPRIG_2_14.GetRadio()
    this.Parent.oContained.w_ATIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oATIPRIG_2_14.ToRadio()
    this.Parent.oContained.w_ATIPRIG=trim(this.Parent.oContained.w_ATIPRIG)
    return(;
      iif(this.Parent.oContained.w_ATIPRIG=='N',1,;
      iif(this.Parent.oContained.w_ATIPRIG=='D',2,;
      0)))
  endfunc

  func oATIPRIG_2_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oATIPRIG_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not .w_ISALT)
    endwith
    endif
  endfunc

  add object oATIPRI2_2_15 as StdTrsCombo with uid="CKGVEEFTSS",rtrep=.t.,;
    cFormVar="w_ATIPRI2", RowSource=""+"Senza nota spese,"+"Con nota spese" , ;
    ToolTipText = "Tipologia riga per la generazione della nota spese",;
    HelpContextID = 123027194,;
    Height=18, Width=114, Left=527, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oATIPRI2_2_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATIPRI2,&i_cF..t_ATIPRI2),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    space(1))))
  endfunc
  func oATIPRI2_2_15.GetRadio()
    this.Parent.oContained.w_ATIPRI2 = this.RadioValue()
    return .t.
  endfunc

  func oATIPRI2_2_15.ToRadio()
    this.Parent.oContained.w_ATIPRI2=trim(this.Parent.oContained.w_ATIPRI2)
    return(;
      iif(this.Parent.oContained.w_ATIPRI2=='N',1,;
      iif(this.Parent.oContained.w_ATIPRI2=='D',2,;
      0)))
  endfunc

  func oATIPRI2_2_15.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oATIPRI2_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not .w_ISALT)
    endwith
    endif
  endfunc

  add object oETIPRIG_2_16 as StdTrsCombo with uid="WDOQTSAWKZ",rtrep=.t.,;
    cFormVar="w_ETIPRIG", RowSource=""+"Nessuno,"+"Doc. vendite,"+"Doc. acquisti,"+"Entrambi" , ;
    ToolTipText = "Tipologia riga per la generazione del documento.",;
    HelpContextID = 145408326,;
    Height=21, Width=114, Left=412, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oETIPRIG_2_16.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ETIPRIG,&i_cF..t_ETIPRIG),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    iif(xVal =3,'A',;
    iif(xVal =4,'E',;
    space(1))))))
  endfunc
  func oETIPRIG_2_16.GetRadio()
    this.Parent.oContained.w_ETIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oETIPRIG_2_16.ToRadio()
    this.Parent.oContained.w_ETIPRIG=trim(this.Parent.oContained.w_ETIPRIG)
    return(;
      iif(this.Parent.oContained.w_ETIPRIG=='N',1,;
      iif(this.Parent.oContained.w_ETIPRIG=='D',2,;
      iif(this.Parent.oContained.w_ETIPRIG=='A',3,;
      iif(this.Parent.oContained.w_ETIPRIG=='E',4,;
      0)))))
  endfunc

  func oETIPRIG_2_16.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oETIPRIG_2_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISAHE)
    endwith
    endif
  endfunc

  add object oRTIPRIG_2_18 as StdTrsCombo with uid="XKDCKNPLAZ",rtrep=.t.,;
    cFormVar="w_RTIPRIG", RowSource=""+"Nessuno,"+"Doc. vendite,"+"Doc. acquisti,"+"Entrambi" , ;
    ToolTipText = "Tipologia riga per la generazione del documento.",;
    HelpContextID = 145408534,;
    Height=21, Width=114, Left=412, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oRTIPRIG_2_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RTIPRIG,&i_cF..t_RTIPRIG),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    iif(xVal =3,'A',;
    iif(xVal =4,'E',;
    space(1))))))
  endfunc
  func oRTIPRIG_2_18.GetRadio()
    this.Parent.oContained.w_RTIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oRTIPRIG_2_18.ToRadio()
    this.Parent.oContained.w_RTIPRIG=trim(this.Parent.oContained.w_RTIPRIG)
    return(;
      iif(this.Parent.oContained.w_RTIPRIG=='N',1,;
      iif(this.Parent.oContained.w_RTIPRIG=='D',2,;
      iif(this.Parent.oContained.w_RTIPRIG=='A',3,;
      iif(this.Parent.oContained.w_RTIPRIG=='E',4,;
      0)))))
  endfunc

  func oRTIPRIG_2_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oRTIPRIG_2_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!Isahr())
    endwith
    endif
  endfunc

  func oRTIPRIG_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CAFLANAL='N' OR (.w_RTIPRIG<>'A' AND .w_RTIPRIG<>'E'))
    endwith
    return bRes
  endfunc

  add object oCAFLRESP_2_23 as StdTrsCheck with uid="SKADHNJERU",rtrep=.t.,;
    cFormVar="w_CAFLRESP",  caption="",;
    ToolTipText = "Se attivo: ripete questa prestazione per ogni partecipante all'attivit� (Es: redazione di un contratto a cui lavorano contemporaneamente pi� persone).",;
    HelpContextID = 190415242,;
    Left=650, Top=1, Width=32,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oCAFLRESP_2_23.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..CAFLRESP,&i_cF..t_CAFLRESP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCAFLRESP_2_23.GetRadio()
    this.Parent.oContained.w_CAFLRESP = this.RadioValue()
    return .t.
  endfunc

  func oCAFLRESP_2_23.ToRadio()
    this.Parent.oContained.w_CAFLRESP=trim(this.Parent.oContained.w_CAFLRESP)
    return(;
      iif(this.Parent.oContained.w_CAFLRESP=='S',1,;
      0))
  endfunc

  func oCAFLRESP_2_23.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCAFLRESP_2_23.mCond()
    with this.Parent.oContained
      return (not empty(.w_CACODSER) or not empty(.w_CAKEYART))
    endwith
  endfunc

  func oCAFLRESP_2_23.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAFLNSAP="S")
    endwith
    endif
  endfunc

  add object oRTIPRES_2_29 as stdTrsTableCombo with uid="YDYNWNBYTP",rtrep=.t.,;
    cFormVar="w_RTIPRES", tablefilter="" , enabled=.f.,;
    HelpContextID = 190135786,;
    Height=21, Width=114, Left=527, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
    , cTable='TIPI_PRE',cKey='TPCODICE',cValue='TPDESCRI',cOrderBy='TPDESCRI',xDefault=space(1);
  , bGlobalFont=.t.



  func oRTIPRES_2_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!Isahr())
    endwith
    endif
  endfunc

  add object oETIPRES_2_30 as stdTrsTableCombo with uid="DADEZPLUBB",rtrep=.t.,;
    cFormVar="w_ETIPRES", tablefilter="" , enabled=.f.,;
    HelpContextID = 190135994,;
    Height=21, Width=114, Left=527, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
    , cTable='TIPI_PRE',cKey='TPCODICE',cValue='TPDESCRI',cOrderBy='TPDESCRI',xDefault=space(1);
  , bGlobalFont=.t.



  func oETIPRES_2_30.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISAHE)
    endwith
    endif
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oRIGA_2_1.When()
    return(.t.)
  proc oRIGA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oRIGA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=3
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mca','CAUMATTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CACODICE=CAUMATTI.CACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_mca
Proc RivalorizzoCombo(pParent)
   local obj
   obj = pParent.getCtrl("w_CA__ENTE")
   obj.Clear()
   obj.Init()
   obj.SetRadio()
   * Aggiorniamo la variabile
   pParent.w_CA__ENTE=obj.RadioValue()
   obj = .null.
Endproc
* --- Fine Area Manuale
