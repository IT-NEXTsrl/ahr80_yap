* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kel                                                        *
*              Eliminazione attivit� collegate                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-03-07                                                      *
* Last revis.: 2015-10-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kel",oParentObject))

* --- Class definition
define class tgsag_kel as StdForm
  Top    = 37
  Left   = 46

  * --- Standard Properties
  Width  = 692
  Height = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-19"
  HelpContextID=202758295
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_kel"
  cComment = "Eliminazione attivit� collegate"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATA_INI = ctot('')
  w_SERIAL = space(20)
  w_SELROW = 0
  w_CAITER = space(20)
  w_DESC_ATT = space(254)
  w_DES_PRAT = space(100)
  w_SELEZI = space(1)
  w_FLRICO = space(1)
  w_AGKEL_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kelPag1","gsag_kel",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_14
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGKEL_ZOOM = this.oPgFrm.Pages(1).oPag.AGKEL_ZOOM
    DoDefault()
    proc Destroy()
      this.w_AGKEL_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSAG_BAD(this,"D")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATA_INI=ctot("")
      .w_SERIAL=space(20)
      .w_SELROW=0
      .w_CAITER=space(20)
      .w_DESC_ATT=space(254)
      .w_DES_PRAT=space(100)
      .w_SELEZI=space(1)
      .w_FLRICO=space(1)
      .w_DATA_INI=oParentObject.w_DATA_INI
      .w_SERIAL=oParentObject.w_SERIAL
      .w_CAITER=oParentObject.w_CAITER
      .w_DESC_ATT=oParentObject.w_DESC_ATT
      .w_DES_PRAT=oParentObject.w_DES_PRAT
      .w_FLRICO=oParentObject.w_FLRICO
          .DoRTCalc(1,2,.f.)
        .w_SELROW = 0
      .oPgFrm.Page1.oPag.AGKEL_ZOOM.Calculate()
          .DoRTCalc(4,6,.f.)
        .w_SELEZI = 'S'
    endwith
    this.DoRTCalc(8,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATA_INI=.w_DATA_INI
      .oParentObject.w_SERIAL=.w_SERIAL
      .oParentObject.w_CAITER=.w_CAITER
      .oParentObject.w_DESC_ATT=.w_DESC_ATT
      .oParentObject.w_DES_PRAT=.w_DES_PRAT
      .oParentObject.w_FLRICO=.w_FLRICO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.AGKEL_ZOOM.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AGKEL_ZOOM.Calculate()
    endwith
  return

  proc Calculate_HXPJOKGWGH()
    with this
          * --- Seleziona \deseleziona
          Seldesel(this;
             )
    endwith
  endproc
  proc Calculate_PGWMDAMXMT()
    with this
          * --- Calcolo selrow
          .w_SELROW = iif( upper(this.currentEvent)='W_AGKEL_ZOOM ROW CHECKED' , .w_SELROW+1 ,iif( upper(this.currentEvent)='W_AGKEL_ZOOM ROW UNCHECKED' , .w_SELROW-1 , 0))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.visible=!this.oPgFrm.Page1.oPag.oBtn_1_5.mHide()
    this.oPgFrm.Page1.oPag.oDES_PRAT_1_10.visible=!this.oPgFrm.Page1.oPag.oDES_PRAT_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AGKEL_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("w_SELEZI Changed")
          .Calculate_HXPJOKGWGH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_AGKEL_ZOOM before query") or lower(cEvent)==lower("w_AGKEL_ZOOM row checked") or lower(cEvent)==lower("w_AGKEL_ZOOM row unchecked") or lower(cEvent)==lower("AggiornaSel")
          .Calculate_PGWMDAMXMT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kel
    * --- Al checked/unchecked della riga sullo zoom non � immediatamente visualizzato
    * --- o nascosto il OK completa (la HideControls scatta solo al cambio
    * --- di riga dello zoom, per cui forzo la chiamata).
    If upper(cEvent)='W_AGKEL_ZOOM ROW CHECKED' OR Upper(cEvent)='W_AGKEL_ZOOM ROW UNCHECKED' or cEvent='Init'
        this.mHideControls()
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATA_INI_1_1.value==this.w_DATA_INI)
      this.oPgFrm.Page1.oPag.oDATA_INI_1_1.value=this.w_DATA_INI
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIAL_1_2.value==this.w_SERIAL)
      this.oPgFrm.Page1.oPag.oSERIAL_1_2.value=this.w_SERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCAITER_1_4.value==this.w_CAITER)
      this.oPgFrm.Page1.oPag.oCAITER_1_4.value=this.w_CAITER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC_ATT_1_9.value==this.w_DESC_ATT)
      this.oPgFrm.Page1.oPag.oDESC_ATT_1_9.value=this.w_DESC_ATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDES_PRAT_1_10.value==this.w_DES_PRAT)
      this.oPgFrm.Page1.oPag.oDES_PRAT_1_10.value=this.w_DES_PRAT
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_14.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_kelPag1 as StdContainer
  Width  = 688
  height = 413
  stdWidth  = 688
  stdheight = 413
  resizeXpos=476
  resizeYpos=242
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATA_INI_1_1 as StdField with uid="QCITKHXZZR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATA_INI", cQueryName = "DATA_INI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 189554303,;
   bGlobalFont=.t.,;
    Height=21, Width=108, Left=511, Top=14

  add object oSERIAL_1_2 as StdField with uid="IRGOUFXEOT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SERIAL", cQueryName = "SERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 208945958,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=219, Top=359, InputMask=replicate('X',20)

  add object oCAITER_1_4 as StdField with uid="YNTHADKUSV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CAITER", cQueryName = "CAITER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 222384602,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=221, Top=384, InputMask=replicate('X',20)


  add object oBtn_1_5 as StdButton with uid="ERHOXVEEGI",left=585, top=365, width=48,height=45,;
    CpPicture="BMP\ELIMINA2.BMP", caption="", nPag=1;
    , ToolTipText = "Elimina le attivit� selezionate";
    , HelpContextID = 249964218;
    , caption='Eli\<mina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SELROW=0)
     endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="TKCMXZXILU",left=638, top=365, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 210075718;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AGKEL_ZOOM as cp_szoombox with uid="WODZOSNAJX",left=-2, top=81, width=697,height=273,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_ATTI",cZoomFile="GSAG_KEL",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 156114970

  add object oDESC_ATT_1_9 as StdField with uid="CJTQQCRMVY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESC_ATT", cQueryName = "DESC_ATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 212970870,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=126, Top=14, InputMask=replicate('X',254)

  add object oDES_PRAT_1_10 as StdField with uid="XNUFOXWBGI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DES_PRAT", cQueryName = "DES_PRAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 210087286,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=126, Top=38, InputMask=replicate('X',100)

  func oDES_PRAT_1_10.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oSELEZI_1_14 as StdRadio with uid="VNICEXLLTR",rtseq=7,rtrep=.f.,left=7, top=370, width=123,height=32;
    , tabstop=.f.;
    , ToolTipText = "Selezionate/deselezionate automaticamente solo le attivit� con stato 'Provvisoria' o 'Da svolgere'";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 184541990
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 184541990
      this.Buttons(2).Top=15
      this.SetAll("Width",121)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Selezionate/deselezionate automaticamente solo le attivit� con stato 'Provvisoria' o 'Da svolgere'")
      StdRadio::init()
    endproc

  func oSELEZI_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_14.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_14.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  add object oStr_1_8 as StdString with uid="OZATNEHMGU",Visible=.t., Left=20, Top=71,;
    Alignment=0, Width=167, Height=18,;
    Caption="Attivit� collegate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="PFVPIBIGVQ",Visible=.t., Left=4, Top=14,;
    Alignment=1, Width=116, Height=18,;
    Caption="Attivit� di partenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="COTKAPENGA",Visible=.t., Left=463, Top=14,;
    Alignment=1, Width=39, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="FGAPFHFYQF",Visible=.t., Left=66, Top=38,;
    Alignment=1, Width=54, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="JLPTGYJBLO",Visible=.t., Left=128, Top=62,;
    Alignment=0, Width=58, Height=17,;
    Caption="(Rosso)"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_FLRICO<>'S')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="KPEGKTKNKF",Visible=.t., Left=186, Top=62,;
    Alignment=0, Width=266, Height=17,;
    Caption="Attivit� con stato In corso, Evasa o Completata"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.w_FLRICO<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kel','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_kel
proc Seldesel(obj)
	Local l_flsele
   l_flsele= IIF ( obj.w_SELEZI='S' ,1, 0 )
   obj.w_SELROW= IIF ( obj.w_SELEZI='S' ,1, 0 )
   UPDATE ( obj.w_AGKEL_ZOOM.cCursor ) SET XCHK = l_flsele where FLELIM='N'
   Select ( obj.w_AGKEL_ZOOM.cCursor )
   Go Top
   obj.w_SELROW=IIF ( obj.w_SELEZI='S' ,1, 0 )
   obj.mHideControls()
endproc
* --- Fine Area Manuale
