* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bpk                                                        *
*              Controlli finali prestazioni                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-09                                                      *
* Last revis.: 2014-09-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bpk",oParentObject,m.pParam)
return(i_retval)

define class tgsag_bpk as StdBatch
  * --- Local variables
  pParam = space(1)
  w_OK = .f.
  w_MESSTRS = space(100)
  w_CFUNC = space(10)
  w_SERPAD = space(10)
  w_ATT_SERIAL = space(10)
  w_OK = .f.
  pOper = space(10)
  w_CALCONC = 0
  * --- WorkFile variables
  PRE_STAZ_idx=0
  OFFDATTI_idx=0
  OFF_ATTI_idx=0
  DET_PART_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli finali anagrafica prestazioni GSAG_APR
    do case
      case this.pPARAM="K"
        this.w_OK = .t.
        this.w_CFUNC = This.oparentobject.cFunction
        if this.w_OK and this.w_CFUNC="Query"
          * --- Verifico se sono una parte
          if Not Empty(NVL(this.oParentObject.w_PRSERATT, "")) 
 
            this.w_OK = .f.
            this.w_MESSTRS = "Prestazione collegata ad attivit� impossibile eliminare"
          endif
          * --- Read from DET_PART
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DET_PART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DET_PART_idx,2],.t.,this.DET_PART_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPSERIAL"+;
              " from "+i_cTable+" DET_PART where ";
                  +"DPSERPRE = "+cp_ToStrODBC(this.oParentObject.w_PRSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPSERIAL;
              from (i_cTable) where;
                  DPSERPRE = this.oParentObject.w_PRSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERPAD = NVL(cp_ToDate(_read_.DPSERIAL),cp_NullValue(_read_.DPSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows<>0
            this.w_MESSTRS = "Cancellazione abbandonata"
            this.w_OK = .f.
          else
            * --- Try
            local bErr_00D5B278
            bErr_00D5B278=bTrsErr
            this.Try_00D5B278()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              this.w_MESSTRS = "Errrore cancellazione prestazioni collegate"
              this.w_OK = .f.
            endif
            bTrsErr=bTrsErr or bErr_00D5B278
            * --- End
          endif
        endif
        if this.w_OK and this.oParentObject.w_PRROWATT>0 and this.w_CFUNC="Edit"
          * --- Aggiorno variazioni su  attivit� collegata
          * --- Try
          local bErr_00D57C20
          bErr_00D57C20=bTrsErr
          this.Try_00D57C20()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_MESSTRS = "Errrore Aggiornamento attivit� collegate collegate"
            this.w_OK = .f.
          endif
          bTrsErr=bTrsErr or bErr_00D57C20
          * --- End
        endif
        if !this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=Ah_msgformat(this.w_MESSTRS)
        endif
        if Not Empty(NVL(this.w_SERPAD, "")) and Ah_yesno("Attenzione: prestazione proveniente da parti, impossibile eliminare direttamente. Si desidera aprire la prestazione collegata?") 
          This.oparentobject.Serpad=this.w_SERPAD
        endif
      case this.pPARAM="C"
        * --- Collegamento spese/anticipazioni a prestazione
        if !EMPTY(NVL(this.oParentObject.w_PRSERIAL, "")) AND !EMPTY(NVL(this.oParentObject.w_NEWSERIAL, ""))
          * --- Try
          local bErr_00D5CCA0
          bErr_00D5CCA0=bTrsErr
          this.Try_00D5CCA0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_00D5CCA0
          * --- End
          This.oparentobject.oparentobject.Loadrecwarn()
        endif
      case this.pPARAM $ "D-M"
        * --- Controlli alla delete init
        this.w_OK = .T.
        do case
          case Not Empty(NVL(this.oParentObject.w_RIFFAT, "")) 
            this.oParentObject.w_MESS = ah_Msgformat("Attenzione esiste una fattura associata alla prestazione")
            this.w_OK = .F.
          case Not Empty(NVL(this.oParentObject.w_RIFPRO, ""))
            this.oParentObject.w_MESS = ah_Msgformat("Attenzione esiste una proforma associata alla prestazione")
            this.w_OK = .F.
          case Not Empty(NVL(this.oParentObject.w_RIFNOT, ""))
            this.oParentObject.w_MESS = ah_Msgformat("Attenzione esiste una nota spese associata alla prestazione")
            this.w_OK = .F.
          case Not Empty(NVL(this.oParentObject.w_RIFBOZ, ""))
            this.oParentObject.w_MESS = ah_Msgformat("Attenzione esiste una bozza associata alla prestazione")
            this.w_OK = .F.
          case Not Empty(NVL(this.oParentObject.w_RIFFAA, ""))
            this.oParentObject.w_MESS = ah_Msgformat("Attenzione esiste una fattura d'acconto alla prestazione")
            this.w_OK = .F.
          case Not Empty(NVL(this.oParentObject.w_RIFPRA, "")) 
            this.oParentObject.w_MESS = ah_Msgformat("Attenzione esiste una proforma d'acconto associata alla prestazione")
            this.w_OK = .F.
          case Not Empty(NVL(this.oParentObject.w_PRRIFCON, "")) 
            this.oParentObject.w_MESS = ah_Msgformat("Attenzione prestazione contabilizzata")
            this.w_OK = .F.
          case EMPTY(NVL(this.oParentObject.w_PRSERAGG, "")) AND this.oParentObject.w_PRRIGPRE="S"
            * --- Prestazione principale associata ad una spesa/anticip. contabilizzata
            * --- Select from PRE_STAZ
            i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2],.t.,this.PRE_STAZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select PRRIFCON  from "+i_cTable+" PRE_STAZ ";
                  +" where PRSERAGG="+cp_ToStrODBC(this.oParentObject.w_PRSERIAL)+"";
                   ,"_Curs_PRE_STAZ")
            else
              select PRRIFCON from (i_cTable);
               where PRSERAGG=this.oParentObject.w_PRSERIAL;
                into cursor _Curs_PRE_STAZ
            endif
            if used('_Curs_PRE_STAZ')
              select _Curs_PRE_STAZ
              locate for 1=1
              do while not(eof())
              if NOT EMPTY(NVL(_Curs_PRE_STAZ.PRRIFCON, ""))
                this.oParentObject.w_MESS = ah_Msgformat("Attenzione prestazione associata ad una spesa/anticip. contabilizzata")
                this.w_OK = .F.
              endif
                select _Curs_PRE_STAZ
                continue
              enddo
              use
            endif
          case NOT EMPTY(NVL(this.oParentObject.w_PRSERAGG, ""))
            * --- Spesa/anticip. associata ad un'altra spesa/anticip. contabilizzata
            * --- Select from PRE_STAZ
            i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2],.t.,this.PRE_STAZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select PRRIFCON  from "+i_cTable+" PRE_STAZ ";
                  +" where PRSERAGG="+cp_ToStrODBC(this.oParentObject.w_PRSERAGG)+" AND PRSERIAL<>"+cp_ToStrODBC(this.oParentObject.w_PRSERIAL)+"";
                   ,"_Curs_PRE_STAZ")
            else
              select PRRIFCON from (i_cTable);
               where PRSERAGG=this.oParentObject.w_PRSERAGG AND PRSERIAL<>this.oParentObject.w_PRSERIAL;
                into cursor _Curs_PRE_STAZ
            endif
            if used('_Curs_PRE_STAZ')
              select _Curs_PRE_STAZ
              locate for 1=1
              do while not(eof())
              if NOT EMPTY(NVL(_Curs_PRE_STAZ.PRRIFCON, ""))
                this.oParentObject.w_MESS = ah_Msgformat("Attenzione prestazione associata ad una spesa/anticip. contabilizzata")
                this.w_OK = .F.
              endif
                select _Curs_PRE_STAZ
                continue
              enddo
              use
            endif
        endcase
        if !this.w_OK
          if this.pParam="D"
            this.oParentObject.w_MESS = ah_Msgformat("%1%0Impossibile eliminare",Alltrim(this.oParentObject.w_MESS))
          else
            this.oParentObject.w_MESS = ah_Msgformat("%1%0Impossibile modificare",Alltrim(this.oParentObject.w_MESS))
          endif
        endif
        i_retcode = 'stop'
        i_retval = this.w_OK
        return
      case this.pPARAM="A"
        * --- Aggiornamento campi delle prestaz./spese/anticip. collegate
        * --- In presenza di una prestazione collegata ad una spesa/anticip. o viceversa
        if NOT EMPTY(NVL(this.oParentObject.w_PRSERAGG, "")) OR this.oParentObject.w_PRRIGPRE="S"
          * --- Se non � una spesa /anticip. isolata e se � stato variato uno dei campi
          if this.oParentObject.w_OLD_PR__DATA<>this.oParentObject.w_PR__DATA OR this.oParentObject.w_OLD_PRCODRES<>this.oParentObject.w_PRCODRES OR this.oParentObject.w_OLD_PRTIPRIG<>this.oParentObject.w_PRTIPRIG OR this.oParentObject.w_OLD_PRTIPRI2<>this.oParentObject.w_PRTIPRI2 OR this.oParentObject.w_OLD_PRNUMPRA<>this.oParentObject.w_PRNUMPRA
            * --- Se siamo in presenza di una spesa/anticip. collegata
            if NOT EMPTY(NVL(this.oParentObject.w_PRSERAGG, ""))
              * --- Aggiorna la prestazione principale
              * --- Write into PRE_STAZ
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PR__DATA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PR__DATA),'PRE_STAZ','PR__DATA');
                +",PRCODRES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRCODRES),'PRE_STAZ','PRCODRES');
                +",PRTIPRIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRTIPRIG),'PRE_STAZ','PRTIPRIG');
                +",PRTIPRI2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRTIPRI2),'PRE_STAZ','PRTIPRI2');
                +",PRNUMPRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRNUMPRA),'PRE_STAZ','PRNUMPRA');
                +",PRCODVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRCODVAL),'PRE_STAZ','PRCODVAL');
                +",PRCODLIS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRCODLIS),'PRE_STAZ','PRCODLIS');
                    +i_ccchkf ;
                +" where ";
                    +"PRSERIAL <> "+cp_ToStrODBC(this.oParentObject.w_PRSERIAL);
                    +" and PRSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PRSERAGG);
                       )
              else
                update (i_cTable) set;
                    PR__DATA = this.oParentObject.w_PR__DATA;
                    ,PRCODRES = this.oParentObject.w_PRCODRES;
                    ,PRTIPRIG = this.oParentObject.w_PRTIPRIG;
                    ,PRTIPRI2 = this.oParentObject.w_PRTIPRI2;
                    ,PRNUMPRA = this.oParentObject.w_PRNUMPRA;
                    ,PRCODVAL = this.oParentObject.w_PRCODVAL;
                    ,PRCODLIS = this.oParentObject.w_PRCODLIS;
                    &i_ccchkf. ;
                 where;
                    PRSERIAL <> this.oParentObject.w_PRSERIAL;
                    and PRSERIAL = this.oParentObject.w_PRSERAGG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              if this.oParentObject.w_OLD_PRNUMPRA<>this.oParentObject.w_PRNUMPRA
                * --- Write into PRE_STAZ
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PRNUMPRE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRNUMPRE),'PRE_STAZ','PRNUMPRE');
                      +i_ccchkf ;
                  +" where ";
                      +"PRSERIAL <> "+cp_ToStrODBC(this.oParentObject.w_PRSERIAL);
                      +" and PRSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PRSERAGG);
                         )
                else
                  update (i_cTable) set;
                      PRNUMPRE = this.oParentObject.w_PRNUMPRE;
                      &i_ccchkf. ;
                   where;
                      PRSERIAL <> this.oParentObject.w_PRSERIAL;
                      and PRSERIAL = this.oParentObject.w_PRSERAGG;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              * --- Aggiorna l'altra spesa/anticip.
              * --- Write into PRE_STAZ
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PR__DATA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PR__DATA),'PRE_STAZ','PR__DATA');
                +",PRCODRES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRCODRES),'PRE_STAZ','PRCODRES');
                +",PRTIPRIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRTIPRIG),'PRE_STAZ','PRTIPRIG');
                +",PRTIPRI2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRTIPRI2),'PRE_STAZ','PRTIPRI2');
                +",PRNUMPRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRNUMPRA),'PRE_STAZ','PRNUMPRA');
                +",PRCODVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRCODVAL),'PRE_STAZ','PRCODVAL');
                +",PRCODLIS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRCODLIS),'PRE_STAZ','PRCODLIS');
                    +i_ccchkf ;
                +" where ";
                    +"PRSERIAL <> "+cp_ToStrODBC(this.oParentObject.w_PRSERIAL);
                    +" and PRSERAGG = "+cp_ToStrODBC(this.oParentObject.w_PRSERAGG);
                       )
              else
                update (i_cTable) set;
                    PR__DATA = this.oParentObject.w_PR__DATA;
                    ,PRCODRES = this.oParentObject.w_PRCODRES;
                    ,PRTIPRIG = this.oParentObject.w_PRTIPRIG;
                    ,PRTIPRI2 = this.oParentObject.w_PRTIPRI2;
                    ,PRNUMPRA = this.oParentObject.w_PRNUMPRA;
                    ,PRCODVAL = this.oParentObject.w_PRCODVAL;
                    ,PRCODLIS = this.oParentObject.w_PRCODLIS;
                    &i_ccchkf. ;
                 where;
                    PRSERIAL <> this.oParentObject.w_PRSERIAL;
                    and PRSERAGG = this.oParentObject.w_PRSERAGG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              if this.oParentObject.w_OLD_PRNUMPRA<>this.oParentObject.w_PRNUMPRA
                * --- Write into PRE_STAZ
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PRNUMPRE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRNUMPRE),'PRE_STAZ','PRNUMPRE');
                      +i_ccchkf ;
                  +" where ";
                      +"PRSERIAL <> "+cp_ToStrODBC(this.oParentObject.w_PRSERIAL);
                      +" and PRSERAGG = "+cp_ToStrODBC(this.oParentObject.w_PRSERAGG);
                         )
                else
                  update (i_cTable) set;
                      PRNUMPRE = this.oParentObject.w_PRNUMPRE;
                      &i_ccchkf. ;
                   where;
                      PRSERIAL <> this.oParentObject.w_PRSERIAL;
                      and PRSERAGG = this.oParentObject.w_PRSERAGG;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
            * --- Aggiorna le spese/antic. collegate
            * --- Write into PRE_STAZ
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PR__DATA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PR__DATA),'PRE_STAZ','PR__DATA');
              +",PRCODRES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRCODRES),'PRE_STAZ','PRCODRES');
              +",PRTIPRIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRTIPRIG),'PRE_STAZ','PRTIPRIG');
              +",PRTIPRI2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRTIPRI2),'PRE_STAZ','PRTIPRI2');
              +",PRNUMPRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRNUMPRA),'PRE_STAZ','PRNUMPRA');
              +",PRCODVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRCODVAL),'PRE_STAZ','PRCODVAL');
              +",PRCODLIS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRCODLIS),'PRE_STAZ','PRCODLIS');
                  +i_ccchkf ;
              +" where ";
                  +"PRSERIAL <> "+cp_ToStrODBC(this.oParentObject.w_PRSERIAL);
                  +" and PRSERAGG = "+cp_ToStrODBC(this.oParentObject.w_PRSERIAL);
                     )
            else
              update (i_cTable) set;
                  PR__DATA = this.oParentObject.w_PR__DATA;
                  ,PRCODRES = this.oParentObject.w_PRCODRES;
                  ,PRTIPRIG = this.oParentObject.w_PRTIPRIG;
                  ,PRTIPRI2 = this.oParentObject.w_PRTIPRI2;
                  ,PRNUMPRA = this.oParentObject.w_PRNUMPRA;
                  ,PRCODVAL = this.oParentObject.w_PRCODVAL;
                  ,PRCODLIS = this.oParentObject.w_PRCODLIS;
                  &i_ccchkf. ;
               where;
                  PRSERIAL <> this.oParentObject.w_PRSERIAL;
                  and PRSERAGG = this.oParentObject.w_PRSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            if this.oParentObject.w_OLD_PRNUMPRA<>this.oParentObject.w_PRNUMPRA
              * --- Write into PRE_STAZ
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PRNUMPRE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRNUMPRE),'PRE_STAZ','PRNUMPRE');
                    +i_ccchkf ;
                +" where ";
                    +"PRSERIAL <> "+cp_ToStrODBC(this.oParentObject.w_PRSERIAL);
                    +" and PRSERAGG = "+cp_ToStrODBC(this.oParentObject.w_PRSERIAL);
                       )
              else
                update (i_cTable) set;
                    PRNUMPRE = this.oParentObject.w_PRNUMPRE;
                    &i_ccchkf. ;
                 where;
                    PRSERIAL <> this.oParentObject.w_PRSERIAL;
                    and PRSERAGG = this.oParentObject.w_PRSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
      case this.pPARAM="Z"
        * --- Lanciata sotto transazione all'evento Delete row start da GSAG_MMP (Dettaglio parti)
        this.w_OK = .t.
        * --- Try
        local bErr_00DFD520
        bErr_00DFD520=bTrsErr
        this.Try_00DFD520()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESSTRS = "Errrore cancellazione prestazioni relative alle parti"
          this.w_OK = .f.
        endif
        bTrsErr=bTrsErr or bErr_00DFD520
        * --- End
        if !this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=Ah_msgformat(this.w_MESSTRS)
        endif
      case this.pPARAM="T"
        * --- Se sulla pratica � definita Tariffa concordata e
        *     se siamo in presenza di una tariffa a tempo
        if this.oParentObject.w_CNTARTEM = "C" AND this.oParentObject.w_PRESTA="P"
          * --- Dichiarazione array tariffe a tempo concordate
          DECLARE ARRTARCON (3,1)
          * --- Azzero l'array che verr� riempito dalla funzione Caltarcon()
          ARRTARCON(1)=0 
 ARRTARCON(2)= " " 
 ARRTARCON(3)=0
          this.w_CALCONC = CALTARCON(this.oParentObject.w_PRNUMPRA, this.oParentObject.w_PRCODRES, this.oParentObject.w_PRCODATT, @ARRTARCON)
          this.oParentObject.w_CNTARCON = ARRTARCON(1)
          this.oParentObject.w_PRUNIMIS = IIF(Not Empty(ARRTARCON(2)), ARRTARCON(2), this.oParentObject.w_PRUNIMIS)
          this.oParentObject.w_UNMIS1 = IIF(Not Empty(ARRTARCON(2)), ARRTARCON(2), this.oParentObject.w_UNMIS1)
          this.oParentObject.w_PRQTAMOV = IIF(Not Empty(ARRTARCON(3)), ARRTARCON(3), this.oParentObject.w_PRQTAMOV)
        endif
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMFLTEMP,UMDURORE,UMFLFRAZ,UMMODUM2"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UNMIS1);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMFLTEMP,UMDURORE,UMFLFRAZ,UMMODUM2;
            from (i_cTable) where;
                UMCODICE = this.oParentObject.w_UNMIS1;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_CHKTEMP = NVL(cp_ToDate(_read_.UMFLTEMP),cp_NullValue(_read_.UMFLTEMP))
          this.oParentObject.w_DUR_ORE = NVL(cp_ToDate(_read_.UMDURORE),cp_NullValue(_read_.UMDURORE))
          this.oParentObject.w_FLFRAZ1 = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          this.oParentObject.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
    endcase
  endproc
  proc Try_00D5B278()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_ATT_SERIAL = this.oParentObject.w_PRSERIAL
    * --- Elimino  le prestazioni che sono parti delle spese anticipazioni collegate alla prestazione padre
    * --- Delete from PRE_STAZ
    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".PRSERIAL = "+i_cQueryTable+".PRSERIAL";
    
      do vq_exec with 'GSALSBRR_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Elimino  il recod relativo nelle parti 
    * --- Delete from DET_PART
    i_nConn=i_TableProp[this.DET_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_PART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".DPSERIAL = "+i_cQueryTable+".DPSERIAL";
    
      do vq_exec with 'GSALSBRR_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PRE_STAZ
    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PRSERAGG = "+cp_ToStrODBC(this.oParentObject.w_PRSERIAL);
             )
    else
      delete from (i_cTable) where;
            PRSERAGG = this.oParentObject.w_PRSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_00D57C20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into OFFDATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFFDATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DAQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRQTAMOV),'OFFDATTI','DAQTAMOV');
      +",DAPREZZO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRPREZZO),'OFFDATTI','DAPREZZO');
      +",DAVALRIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRVALRIG),'OFFDATTI','DAVALRIG');
      +",DAPREMIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRPREMIN),'OFFDATTI','DAPREMIN');
      +",DAPREMAX ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRPREMAX),'OFFDATTI','DAPREMAX');
      +",DATIPRIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRTIPRIG),'OFFDATTI','DATIPRIG');
      +",DATIPRI2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRTIPRI2),'OFFDATTI','DATIPRI2');
      +",DAMINEFF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRMINEFF),'OFFDATTI','DAMINEFF');
      +",DAOREEFF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROREEFF),'OFFDATTI','DAOREEFF');
      +",DADESATT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRDESPRE),'OFFDATTI','DADESATT');
      +",DADESAGG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRDESAGG),'OFFDATTI','DADESAGG');
          +i_ccchkf ;
      +" where ";
          +"DASERIAL = "+cp_ToStrODBC(this.oParentObject.w_PRSERATT);
          +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_PRROWATT);
             )
    else
      update (i_cTable) set;
          DAQTAMOV = this.oParentObject.w_PRQTAMOV;
          ,DAPREZZO = this.oParentObject.w_PRPREZZO;
          ,DAVALRIG = this.oParentObject.w_PRVALRIG;
          ,DAPREMIN = this.oParentObject.w_PRPREMIN;
          ,DAPREMAX = this.oParentObject.w_PRPREMAX;
          ,DATIPRIG = this.oParentObject.w_PRTIPRIG;
          ,DATIPRI2 = this.oParentObject.w_PRTIPRI2;
          ,DAMINEFF = this.oParentObject.w_PRMINEFF;
          ,DAOREEFF = this.oParentObject.w_PROREEFF;
          ,DADESATT = this.oParentObject.w_PRDESPRE;
          ,DADESAGG = this.oParentObject.w_PRDESAGG;
          &i_ccchkf. ;
       where;
          DASERIAL = this.oParentObject.w_PRSERATT;
          and CPROWNUM = this.oParentObject.w_PRROWATT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_00D5CCA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorna le prestazioni
    * --- Write into PRE_STAZ
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRSERAGG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NewSerial),'PRE_STAZ','PRSERAGG');
          +i_ccchkf ;
      +" where ";
          +"PRSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PRSERIAL);
             )
    else
      update (i_cTable) set;
          PRSERAGG = this.oParentObject.w_NewSerial;
          &i_ccchkf. ;
       where;
          PRSERIAL = this.oParentObject.w_PRSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into PRE_STAZ
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRRIGPRE ="+cp_NullLink(cp_ToStrODBC("S"),'PRE_STAZ','PRRIGPRE');
          +i_ccchkf ;
      +" where ";
          +"PRSERIAL = "+cp_ToStrODBC(this.oParentObject.w_NewSerial);
             )
    else
      update (i_cTable) set;
          PRRIGPRE = "S";
          &i_ccchkf. ;
       where;
          PRSERIAL = this.oParentObject.w_NewSerial;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_00DFD520()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Elimino le prestazioni relative alle parti
    * --- Delete from PRE_STAZ
    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PRSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DPSERPRE);
             )
    else
      delete from (i_cTable) where;
            PRSERIAL = this.oParentObject.w_DPSERPRE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PRE_STAZ'
    this.cWorkTables[2]='OFFDATTI'
    this.cWorkTables[3]='OFF_ATTI'
    this.cWorkTables[4]='DET_PART'
    this.cWorkTables[5]='UNIMIS'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_PRE_STAZ')
      use in _Curs_PRE_STAZ
    endif
    if used('_Curs_PRE_STAZ')
      use in _Curs_PRE_STAZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
