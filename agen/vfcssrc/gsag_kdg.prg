* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kdg                                                        *
*              Dati aggiuntivi                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-11                                                      *
* Last revis.: 2012-06-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kdg",oParentObject))

* --- Class definition
define class tgsag_kdg as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 550
  Height = 232+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-15"
  HelpContextID=82454377
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  _IDX = 0
  MODCLDAT_IDX = 0
  VOC_COST_IDX = 0
  CENCOST_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gsag_kdg"
  cComment = "Dati aggiuntivi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ELDATINI = ctod('  /  /  ')
  w_ELDATFIN = ctod('  /  /  ')
  o_ELDATFIN = ctod('  /  /  ')
  w_ELRINNOV = 0
  w_ELESCRIN = space(1)
  o_ELESCRIN = space(1)
  w_ELPERCON = space(3)
  w_MDDESCRI = space(50)
  w_ELRINCON = space(1)
  o_ELRINCON = space(1)
  w_ELNUMGIO = 0
  o_ELNUMGIO = 0
  w_ELDATDIS = ctod('  /  /  ')
  w_TIPATT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_ELVOCCEN = space(15)
  w_DESVOC = space(40)
  w_DESCEN = space(40)
  w_DESCOMM = space(100)
  w_ELCODCEN = space(15)
  w_ELCOCOMM = space(15)
  w_ELCODATT = space(15)
  w_DESATT = space(100)
  w_VOCOBSO = ctod('  /  /  ')
  w_CENOBSO = ctod('  /  /  ')
  w_COMOBSO = ctod('  /  /  ')
  w_FLNSAP = space(1)
  w_ELTIPATT = space(20)
  w_ELPERATT = space(5)
  o_ELPERATT = space(5)
  w_ELPERFAT = space(5)
  o_ELPERFAT = space(5)
  w_ELINICOA = ctod('  /  /  ')
  o_ELINICOA = ctod('  /  /  ')
  w_ELFINCOA = ctod('  /  /  ')
  w_ELINICOD = ctod('  /  /  ')
  o_ELINICOD = ctod('  /  /  ')
  w_ELFINCOD = ctod('  /  /  ')
  w_ELCAUDOC = space(5)
  w_TIPVOR = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kdgPag1","gsag_kdg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati rinnovo ")
      .Pages(2).addobject("oPag","tgsag_kdgPag2","gsag_kdg",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati analitica")
      .Pages(3).addobject("oPag","tgsag_kdgPag3","gsag_kdg",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Date competenza")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oELESCRIN_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='MODCLDAT'
    this.cWorkTables[2]='VOC_COST'
    this.cWorkTables[3]='CENCOST'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='ATTIVITA'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ELDATINI=ctod("  /  /  ")
      .w_ELDATFIN=ctod("  /  /  ")
      .w_ELRINNOV=0
      .w_ELESCRIN=space(1)
      .w_ELPERCON=space(3)
      .w_MDDESCRI=space(50)
      .w_ELRINCON=space(1)
      .w_ELNUMGIO=0
      .w_ELDATDIS=ctod("  /  /  ")
      .w_TIPATT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_ELVOCCEN=space(15)
      .w_DESVOC=space(40)
      .w_DESCEN=space(40)
      .w_DESCOMM=space(100)
      .w_ELCODCEN=space(15)
      .w_ELCOCOMM=space(15)
      .w_ELCODATT=space(15)
      .w_DESATT=space(100)
      .w_VOCOBSO=ctod("  /  /  ")
      .w_CENOBSO=ctod("  /  /  ")
      .w_COMOBSO=ctod("  /  /  ")
      .w_FLNSAP=space(1)
      .w_ELTIPATT=space(20)
      .w_ELPERATT=space(5)
      .w_ELPERFAT=space(5)
      .w_ELINICOA=ctod("  /  /  ")
      .w_ELFINCOA=ctod("  /  /  ")
      .w_ELINICOD=ctod("  /  /  ")
      .w_ELFINCOD=ctod("  /  /  ")
      .w_ELCAUDOC=space(5)
      .w_TIPVOR=space(1)
      .w_ELDATINI=oParentObject.w_ELDATINI
      .w_ELDATFIN=oParentObject.w_ELDATFIN
      .w_ELRINNOV=oParentObject.w_ELRINNOV
      .w_ELESCRIN=oParentObject.w_ELESCRIN
      .w_ELPERCON=oParentObject.w_ELPERCON
      .w_ELRINCON=oParentObject.w_ELRINCON
      .w_ELNUMGIO=oParentObject.w_ELNUMGIO
      .w_ELDATDIS=oParentObject.w_ELDATDIS
      .w_ELVOCCEN=oParentObject.w_ELVOCCEN
      .w_ELCODCEN=oParentObject.w_ELCODCEN
      .w_ELCOCOMM=oParentObject.w_ELCOCOMM
      .w_ELCODATT=oParentObject.w_ELCODATT
      .w_FLNSAP=oParentObject.w_FLNSAP
      .w_ELTIPATT=oParentObject.w_ELTIPATT
      .w_ELPERATT=oParentObject.w_ELPERATT
      .w_ELPERFAT=oParentObject.w_ELPERFAT
      .w_ELINICOA=oParentObject.w_ELINICOA
      .w_ELFINCOA=oParentObject.w_ELFINCOA
      .w_ELINICOD=oParentObject.w_ELINICOD
      .w_ELFINCOD=oParentObject.w_ELFINCOD
      .w_ELCAUDOC=oParentObject.w_ELCAUDOC
          .DoRTCalc(1,3,.f.)
        .w_ELESCRIN = .w_ELESCRIN
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_ELPERCON))
          .link_1_9('Full')
        endif
          .DoRTCalc(6,9,.f.)
        .w_TIPATT = 'A'
        .w_OBTEST = i_DATSYS
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_ELVOCCEN))
          .link_2_3('Full')
        endif
        .DoRTCalc(13,16,.f.)
        if not(empty(.w_ELCODCEN))
          .link_2_7('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_ELCOCOMM))
          .link_2_8('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_ELCODATT))
          .link_2_9('Full')
        endif
    endwith
    this.DoRTCalc(19,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oELESCRIN_1_8.enabled = i_bVal
      .Page1.oPag.oELPERCON_1_9.enabled = i_bVal
      .Page1.oPag.oELRINCON_1_11.enabled = i_bVal
      .Page1.oPag.oELNUMGIO_1_12.enabled = i_bVal
      .Page1.oPag.oELDATDIS_1_13.enabled = i_bVal
      .Page2.oPag.oELVOCCEN_2_3.enabled = i_bVal
      .Page2.oPag.oELCODCEN_2_7.enabled = i_bVal
      .Page2.oPag.oELCOCOMM_2_8.enabled = i_bVal
      .Page2.oPag.oELCODATT_2_9.enabled = i_bVal
      .Page3.oPag.oELINICOA_3_13.enabled = i_bVal
      .Page3.oPag.oELFINCOA_3_14.enabled = i_bVal
      .Page3.oPag.oELINICOD_3_15.enabled = i_bVal
      .Page3.oPag.oELFINCOD_3_16.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ELDATINI=.w_ELDATINI
      .oParentObject.w_ELDATFIN=.w_ELDATFIN
      .oParentObject.w_ELRINNOV=.w_ELRINNOV
      .oParentObject.w_ELESCRIN=.w_ELESCRIN
      .oParentObject.w_ELPERCON=.w_ELPERCON
      .oParentObject.w_ELRINCON=.w_ELRINCON
      .oParentObject.w_ELNUMGIO=.w_ELNUMGIO
      .oParentObject.w_ELDATDIS=.w_ELDATDIS
      .oParentObject.w_ELVOCCEN=.w_ELVOCCEN
      .oParentObject.w_ELCODCEN=.w_ELCODCEN
      .oParentObject.w_ELCOCOMM=.w_ELCOCOMM
      .oParentObject.w_ELCODATT=.w_ELCODATT
      .oParentObject.w_FLNSAP=.w_FLNSAP
      .oParentObject.w_ELTIPATT=.w_ELTIPATT
      .oParentObject.w_ELPERATT=.w_ELPERATT
      .oParentObject.w_ELPERFAT=.w_ELPERFAT
      .oParentObject.w_ELINICOA=.w_ELINICOA
      .oParentObject.w_ELFINCOA=.w_ELFINCOA
      .oParentObject.w_ELINICOD=.w_ELINICOD
      .oParentObject.w_ELFINCOD=.w_ELFINCOD
      .oParentObject.w_ELCAUDOC=.w_ELCAUDOC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_ELESCRIN<>.w_ELESCRIN
            .w_ELPERCON = IIF( .w_ELESCRIN <>"N", "   ", .w_ELPERCON )
          .link_1_9('Full')
        endif
        .DoRTCalc(6,6,.t.)
        if .o_ELESCRIN<>.w_ELESCRIN
            .w_ELRINCON = iif(.w_ELESCRIN='S',"N",.w_ELRINCON)
        endif
        if .o_ELRINCON<>.w_ELRINCON
            .w_ELNUMGIO = IIF(.w_ELRINCON = "S" ,.w_ELNUMGIO,0)
        endif
        if .o_ELDATFIN<>.w_ELDATFIN.or. .o_ELNUMGIO<>.w_ELNUMGIO.or. .o_ELRINCON<>.w_ELRINCON
            .w_ELDATDIS = IIF(.w_ELRINCON = "S" ,.w_ELDATFIN - .w_ELNUMGIO,CP_Chartodate('  -  -    '))
        endif
        .DoRTCalc(10,10,.t.)
            .w_OBTEST = i_DATSYS
        .DoRTCalc(12,27,.t.)
        if .o_ELPERATT<>.w_ELPERATT.or. .o_ELINICOA<>.w_ELINICOA
            .w_ELFINCOA = IIF(NOT EMPTY(.w_ELPERATT), NEXTTIME( .w_ELPERATT, .w_ELINICOA-1,.F.,.T. ),.w_ELINICOA)
        endif
        .DoRTCalc(29,29,.t.)
        if .o_ELPERFAT<>.w_ELPERFAT.or. .o_ELINICOD<>.w_ELINICOD
            .w_ELFINCOD = IIF(NOT EMPTY(.w_ELPERFAT), NEXTTIME( .w_ELPERFAT, .w_ELINICOD-1,.F.,.T. ),.w_ELFINCOD)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(31,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_HBWXDEHEWF()
    with this
          * --- Sbianca periodicit� - rinnovo tacito - giorni di preavviso
          .w_ELPERCON = IIF( .w_ELESCRIN ="S", "   ", .w_ELPERCON )
          .link_1_9('Full')
          .w_ELRINCON = IIF( .w_ELESCRIN = "S", "M", .w_ELRINCON )
          .w_ELNUMGIO = IIF( .w_ELESCRIN = "S", 0, .w_ELNUMGIO )
          .w_ELDATDIS = Cp_chartodate('  -  -    ')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oELPERCON_1_9.enabled = this.oPgFrm.Page1.oPag.oELPERCON_1_9.mCond()
    this.oPgFrm.Page1.oPag.oELRINCON_1_11.enabled = this.oPgFrm.Page1.oPag.oELRINCON_1_11.mCond()
    this.oPgFrm.Page1.oPag.oELNUMGIO_1_12.enabled = this.oPgFrm.Page1.oPag.oELNUMGIO_1_12.mCond()
    this.oPgFrm.Page1.oPag.oELDATDIS_1_13.enabled = this.oPgFrm.Page1.oPag.oELDATDIS_1_13.mCond()
    this.oPgFrm.Page3.oPag.oELINICOA_3_13.enabled = this.oPgFrm.Page3.oPag.oELINICOA_3_13.mCond()
    this.oPgFrm.Page3.oPag.oELFINCOA_3_14.enabled = this.oPgFrm.Page3.oPag.oELFINCOA_3_14.mCond()
    this.oPgFrm.Page3.oPag.oELINICOD_3_15.enabled = this.oPgFrm.Page3.oPag.oELINICOD_3_15.mCond()
    this.oPgFrm.Page3.oPag.oELFINCOD_3_16.enabled = this.oPgFrm.Page3.oPag.oELFINCOD_3_16.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oELVOCCEN_2_3.visible=!this.oPgFrm.Page2.oPag.oELVOCCEN_2_3.mHide()
    this.oPgFrm.Page2.oPag.oDESVOC_2_4.visible=!this.oPgFrm.Page2.oPag.oDESVOC_2_4.mHide()
    this.oPgFrm.Page2.oPag.oDESCEN_2_5.visible=!this.oPgFrm.Page2.oPag.oDESCEN_2_5.mHide()
    this.oPgFrm.Page2.oPag.oDESCOMM_2_6.visible=!this.oPgFrm.Page2.oPag.oDESCOMM_2_6.mHide()
    this.oPgFrm.Page2.oPag.oELCODCEN_2_7.visible=!this.oPgFrm.Page2.oPag.oELCODCEN_2_7.mHide()
    this.oPgFrm.Page2.oPag.oELCOCOMM_2_8.visible=!this.oPgFrm.Page2.oPag.oELCOCOMM_2_8.mHide()
    this.oPgFrm.Page2.oPag.oELCODATT_2_9.visible=!this.oPgFrm.Page2.oPag.oELCODATT_2_9.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_10.visible=!this.oPgFrm.Page2.oPag.oStr_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_11.visible=!this.oPgFrm.Page2.oPag.oStr_2_11.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_12.visible=!this.oPgFrm.Page2.oPag.oStr_2_12.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_13.visible=!this.oPgFrm.Page2.oPag.oStr_2_13.mHide()
    this.oPgFrm.Page2.oPag.oDESATT_2_14.visible=!this.oPgFrm.Page2.oPag.oDESATT_2_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_ELESCRIN Changed")
          .Calculate_HBWXDEHEWF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ELPERCON
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELPERCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_ELPERCON)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_ELPERCON))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELPERCON)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStrODBC(trim(this.w_ELPERCON)+"%");

            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStr(trim(this.w_ELPERCON)+"%");

            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELPERCON) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oELPERCON_1_9'),i_cWhere,'GSAR_AMD',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELPERCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_ELPERCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_ELPERCON)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELPERCON = NVL(_Link_.MDCODICE,space(3))
      this.w_MDDESCRI = NVL(_Link_.MDDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ELPERCON = space(3)
      endif
      this.w_MDDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELPERCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELVOCCEN
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELVOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_ELVOCCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPVOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_ELVOCCEN))
          select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPVOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELVOCCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStrODBC(trim(this.w_ELVOCCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPVOC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStr(trim(this.w_ELVOCCEN)+"%");

            select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELVOCCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oELVOCCEN_2_3'),i_cWhere,'GSCA_AVC',"Voci di ricavo",'VOCZOOM6.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPVOC";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELVOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPVOC";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_ELVOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_ELVOCCEN)
            select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELVOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_VOCOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
      this.w_TIPVOR = NVL(_Link_.VCTIPVOC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELVOCCEN = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_VOCOBSO = ctod("  /  /  ")
      this.w_TIPVOR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOR='R' AND (.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
        endif
        this.w_ELVOCCEN = space(15)
        this.w_DESVOC = space(40)
        this.w_VOCOBSO = ctod("  /  /  ")
        this.w_TIPVOR = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELVOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODCEN
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ELCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ELCODCEN))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_ELCODCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_ELCODCEN)+"%");

            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oELCODCEN_2_7'),i_cWhere,'GSCA_ACC',"Centri di ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ELCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ELCODCEN)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODCEN = space(15)
      endif
      this.w_DESCEN = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.), CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
        endif
        this.w_ELCODCEN = space(15)
        this.w_DESCEN = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCOCOMM
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCOCOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ELCOCOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ELCOCOMM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCOCOMM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_ELCOCOMM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_ELCOCOMM)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCOCOMM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oELCOCOMM_2_8'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCOCOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ELCOCOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ELCOCOMM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCOCOMM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMM  = NVL(_Link_.CNDESCAN,.null.)
      this.w_COMOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ELCOCOMM = space(15)
      endif
      this.w_DESCOMM  = .null.
      this.w_COMOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.), CHKDTOBS(.w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_ELCOCOMM = space(15)
        this.w_DESCOMM  = .null.
        this.w_COMOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCOCOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODATT
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ELCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ELCOCOMM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_ELCOCOMM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_ELCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_ELCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ELCOCOMM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_ELCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_ELCOCOMM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oELCODATT_2_9'),i_cWhere,'GSPC_BZZ',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ELCOCOMM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_ELCOCOMM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ELCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ELCOCOMM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_ELCOCOMM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_ELCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODATT = space(15)
      endif
      this.w_DESATT = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oELRINNOV_1_3.value==this.w_ELRINNOV)
      this.oPgFrm.Page1.oPag.oELRINNOV_1_3.value=this.w_ELRINNOV
    endif
    if not(this.oPgFrm.Page1.oPag.oELESCRIN_1_8.RadioValue()==this.w_ELESCRIN)
      this.oPgFrm.Page1.oPag.oELESCRIN_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELPERCON_1_9.value==this.w_ELPERCON)
      this.oPgFrm.Page1.oPag.oELPERCON_1_9.value=this.w_ELPERCON
    endif
    if not(this.oPgFrm.Page1.oPag.oMDDESCRI_1_10.value==this.w_MDDESCRI)
      this.oPgFrm.Page1.oPag.oMDDESCRI_1_10.value=this.w_MDDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oELRINCON_1_11.RadioValue()==this.w_ELRINCON)
      this.oPgFrm.Page1.oPag.oELRINCON_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELNUMGIO_1_12.value==this.w_ELNUMGIO)
      this.oPgFrm.Page1.oPag.oELNUMGIO_1_12.value=this.w_ELNUMGIO
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATDIS_1_13.value==this.w_ELDATDIS)
      this.oPgFrm.Page1.oPag.oELDATDIS_1_13.value=this.w_ELDATDIS
    endif
    if not(this.oPgFrm.Page2.oPag.oELVOCCEN_2_3.value==this.w_ELVOCCEN)
      this.oPgFrm.Page2.oPag.oELVOCCEN_2_3.value=this.w_ELVOCCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVOC_2_4.value==this.w_DESVOC)
      this.oPgFrm.Page2.oPag.oDESVOC_2_4.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCEN_2_5.value==this.w_DESCEN)
      this.oPgFrm.Page2.oPag.oDESCEN_2_5.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOMM_2_6.value==this.w_DESCOMM)
      this.oPgFrm.Page2.oPag.oDESCOMM_2_6.value=this.w_DESCOMM
    endif
    if not(this.oPgFrm.Page2.oPag.oELCODCEN_2_7.value==this.w_ELCODCEN)
      this.oPgFrm.Page2.oPag.oELCODCEN_2_7.value=this.w_ELCODCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oELCOCOMM_2_8.value==this.w_ELCOCOMM)
      this.oPgFrm.Page2.oPag.oELCOCOMM_2_8.value=this.w_ELCOCOMM
    endif
    if not(this.oPgFrm.Page2.oPag.oELCODATT_2_9.value==this.w_ELCODATT)
      this.oPgFrm.Page2.oPag.oELCODATT_2_9.value=this.w_ELCODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT_2_14.value==this.w_DESATT)
      this.oPgFrm.Page2.oPag.oDESATT_2_14.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page3.oPag.oELINICOA_3_13.value==this.w_ELINICOA)
      this.oPgFrm.Page3.oPag.oELINICOA_3_13.value=this.w_ELINICOA
    endif
    if not(this.oPgFrm.Page3.oPag.oELFINCOA_3_14.value==this.w_ELFINCOA)
      this.oPgFrm.Page3.oPag.oELFINCOA_3_14.value=this.w_ELFINCOA
    endif
    if not(this.oPgFrm.Page3.oPag.oELINICOD_3_15.value==this.w_ELINICOD)
      this.oPgFrm.Page3.oPag.oELINICOD_3_15.value=this.w_ELINICOD
    endif
    if not(this.oPgFrm.Page3.oPag.oELFINCOD_3_16.value==this.w_ELFINCOD)
      this.oPgFrm.Page3.oPag.oELFINCOD_3_16.value=this.w_ELFINCOD
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPVOR='R' AND (.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO)))  and not(NOT (g_PERCCR='S' or g_COMM='S'))  and not(empty(.w_ELVOCCEN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELVOCCEN_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.), CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.) ))  and not(g_PERCCR<>'S')  and not(empty(.w_ELCODCEN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELCODCEN_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.), CHKDTOBS(.w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.) ))  and not(NOT(g_PERCAN='S' OR g_COMM='S' ))  and not(empty(.w_ELCOCOMM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELCOCOMM_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
          case   not((.w_ELINICOA<=.w_ELFINCOA) or (empty(.w_ELFINCOA)))  and (.w_FLNSAP<>'S' AND NOT EMPTY(.w_ELTIPATT))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oELINICOA_3_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   ((empty(.w_ELFINCOA)) or not((.w_ELINICOA<=.w_ELFINCOA) or (empty(.w_ELFINCOA))))  and (.w_FLNSAP<>'S' AND NOT EMPTY(.w_ELTIPATT) AND NOT EMPTY(.w_ELINICOA))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oELFINCOA_3_14.SetFocus()
            i_bnoObbl = !empty(.w_ELFINCOA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not((.w_ELINICOD<=.w_ELFINCOD) or (empty(.w_ELFINCOD)))  and (NOT EMPTY(NVL(.w_ELCAUDOC,'')))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oELINICOD_3_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   ((empty(.w_ELFINCOD)) or not((.w_ELINICOD<=.w_ELFINCOD) or (empty(.w_ELFINCOD))))  and (NOT EMPTY(NVL(.w_ELCAUDOC,'')) AND NOT EMPTY(.w_ELINICOD))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oELFINCOD_3_16.SetFocus()
            i_bnoObbl = !empty(.w_ELFINCOD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_kdg
      IF .w_ELESCRIN ="N" and EMPTY(.w_ELPERCON)
       i_bnoChk=.F.
       i_bRes=.F.
       i_cErrorMsg=Ah_MsgFormat("Periodicit� rinnovo contratti non valorizzata")
      ENDIF
      
      * --- Controllo congruita' date competenza
      IF i_bRes
        if ((.w_ELINICOA>.w_ELFINCOA)  or (empty(.w_ELINICOA) AND NOT empty(.w_ELFINCOA)) OR (NOT EMPTY(.w_ELINICOA) AND EMPTY(.w_ELFINCOA)))
          i_bRes = .f.
          i_bnoChk = .f.
      	  i_cErrorMsg =Ah_MsgFormat("Data inizio competenza attivit� maggiore della data finale o vuota")
        Endif
      Endif
      
      IF i_bRes
        if ((.w_ELINICOD>.w_ELFINCOD)  or (empty(.w_ELINICOD) AND NOT empty(.w_ELFINCOD)) OR (NOT EMPTY(.w_ELINICOD) AND EMPTY(.w_ELFINCOD)))
          i_bRes = .f.
          i_bnoChk = .f.
      	  i_cErrorMsg =Ah_MsgFormat("Data inizio competenza documenti maggiore della data finale o vuota")
        Endif
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ELDATFIN = this.w_ELDATFIN
    this.o_ELESCRIN = this.w_ELESCRIN
    this.o_ELRINCON = this.w_ELRINCON
    this.o_ELNUMGIO = this.w_ELNUMGIO
    this.o_ELPERATT = this.w_ELPERATT
    this.o_ELPERFAT = this.w_ELPERFAT
    this.o_ELINICOA = this.w_ELINICOA
    this.o_ELINICOD = this.w_ELINICOD
    return

enddefine

* --- Define pages as container
define class tgsag_kdgPag1 as StdContainer
  Width  = 546
  height = 232
  stdWidth  = 546
  stdheight = 232
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELRINNOV_1_3 as StdField with uid="VDSOZVGUUS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ELRINNOV", cQueryName = "ELRINNOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero rinnovi",;
    HelpContextID = 239356316,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=118, Top=37

  add object oELESCRIN_1_8 as StdCheck with uid="EYQNPSDKVZ",rtseq=4,rtrep=.f.,left=231, top=37, caption="Esclude rinnovo",;
    ToolTipText = "Eslude rinnovi",;
    HelpContextID = 241337964,;
    cFormVar="w_ELESCRIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELESCRIN_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oELESCRIN_1_8.GetRadio()
    this.Parent.oContained.w_ELESCRIN = this.RadioValue()
    return .t.
  endfunc

  func oELESCRIN_1_8.SetRadio()
    this.Parent.oContained.w_ELESCRIN=trim(this.Parent.oContained.w_ELESCRIN)
    this.value = ;
      iif(this.Parent.oContained.w_ELESCRIN=='S',1,;
      0)
  endfunc

  add object oELPERCON_1_9 as StdField with uid="HCXGTZZSZP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ELPERCON", cQueryName = "ELPERCON",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Periodicit� rinnovo contratti",;
    HelpContextID = 58730900,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=118, Top=65, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_ELPERCON"

  func oELPERCON_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELESCRIN ="N")
    endwith
   endif
  endfunc

  func oELPERCON_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oELPERCON_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELPERCON_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oELPERCON_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oELPERCON_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_ELPERCON
     i_obj.ecpSave()
  endproc

  add object oMDDESCRI_1_10 as StdField with uid="AVIBRAGQJH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MDDESCRI", cQueryName = "MDDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 59728399,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=178, Top=65, InputMask=replicate('X',50)

  add object oELRINCON_1_11 as StdCheck with uid="JXHOYXXLZV",rtseq=7,rtrep=.f.,left=118, top=94, caption="Rinnovo tacito",;
    ToolTipText = "Rinnovo tacito",;
    HelpContextID = 54806932,;
    cFormVar="w_ELRINCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELRINCON_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oELRINCON_1_11.GetRadio()
    this.Parent.oContained.w_ELRINCON = this.RadioValue()
    return .t.
  endfunc

  func oELRINCON_1_11.SetRadio()
    this.Parent.oContained.w_ELRINCON=trim(this.Parent.oContained.w_ELRINCON)
    this.value = ;
      iif(this.Parent.oContained.w_ELRINCON=='S',1,;
      0)
  endfunc

  func oELRINCON_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELESCRIN='N')
    endwith
   endif
  endfunc

  add object oELNUMGIO_1_12 as StdField with uid="ZYPLAYYNSN",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ELNUMGIO", cQueryName = "ELNUMGIO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni entro i quali comunicare la disdetta. E' editabile se il rinnovo tacito � attivo e se la data limite disdetta � vuota oppure � uguale alla data di fine validit� dell'elemento contratto",;
    HelpContextID = 146798187,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=265, Top=129

  func oELNUMGIO_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELRINCON = "S" )
    endwith
   endif
  endfunc

  add object oELDATDIS_1_13 as StdField with uid="AELJLNCZHO",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ELDATDIS", cQueryName = "ELDATDIS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data limite disdetta",;
    HelpContextID = 191141479,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=265, Top=161

  func oELDATDIS_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELRINCON = "S" AND .w_ELNUMGIO = 0)
    endwith
   endif
  endfunc

  add object oStr_1_4 as StdString with uid="YCPFJDEKMD",Visible=.t., Left=4, Top=39,;
    Alignment=1, Width=109, Height=18,;
    Caption="Numero rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="JOMWBBMPTU",Visible=.t., Left=1, Top=65,;
    Alignment=1, Width=112, Height=18,;
    Caption="Periodicit� rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="BHOBFOMPCL",Visible=.t., Left=121, Top=161,;
    Alignment=1, Width=138, Height=18,;
    Caption="Data limite disdetta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="TSFVLERUOH",Visible=.t., Left=68, Top=130,;
    Alignment=1, Width=191, Height=18,;
    Caption="Giorni preavviso disdetta:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsag_kdgPag2 as StdContainer
  Width  = 546
  height = 232
  stdWidth  = 546
  stdheight = 232
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELVOCCEN_2_3 as StdField with uid="GWAYEVIZAI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ELVOCCEN", cQueryName = "ELVOCCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di ricavo incongruente o obsoleto",;
    ToolTipText = "Voce di ricavo",;
    HelpContextID = 43682196,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=117, Top=33, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_ELVOCCEN"

  func oELVOCCEN_2_3.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' or g_COMM='S'))
    endwith
  endfunc

  func oELVOCCEN_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oELVOCCEN_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELVOCCEN_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oELVOCCEN_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di ricavo",'VOCZOOM6.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oELVOCCEN_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_ELVOCCEN
     i_obj.ecpSave()
  endproc

  add object oDESVOC_2_4 as StdField with uid="TBESVEIPFD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 56709686,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=241, Top=33, InputMask=replicate('X',40)

  func oDESVOC_2_4.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' or g_COMM='S'))
    endwith
  endfunc

  add object oDESCEN_2_5 as StdField with uid="TZYJTUVYJB",rtseq=14,rtrep=.t.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229528118,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=241, Top=66, InputMask=replicate('X',40)

  func oDESCEN_2_5.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oDESCOMM_2_6 as StdField with uid="XFJRSTNDWN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCOMM", cQueryName = "DESCOMM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 45198794,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=241, Top=95, InputMask=replicate('X',100)

  func oDESCOMM_2_6.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc

  add object oELCODCEN_2_7 as StdField with uid="VIERPZMVIG",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ELCODCEN", cQueryName = "ELCODCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di ricavo incongruente o obsoleto",;
    ToolTipText = "Centro di ricavo",;
    HelpContextID = 44652948,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=118, Top=66, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ELCODCEN"

  func oELCODCEN_2_7.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oELCODCEN_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODCEN_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODCEN_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oELCODCEN_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di ricavo",'',this.parent.oContained
  endproc
  proc oELCODCEN_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ELCODCEN
     i_obj.ecpSave()
  endproc

  add object oELCOCOMM_2_8 as StdField with uid="YTUMVYJHSL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ELCOCOMM", cQueryName = "ELCOCOMM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    ToolTipText = "Commessa",;
    HelpContextID = 23504493,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=117, Top=95, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ELCOCOMM"

  func oELCOCOMM_2_8.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc

  func oELCOCOMM_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
      if .not. empty(.w_ELCODATT)
        bRes2=.link_2_9('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oELCOCOMM_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCOCOMM_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oELCOCOMM_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oELCOCOMM_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_ELCOCOMM
     i_obj.ecpSave()
  endproc

  add object oELCODATT_2_9 as StdField with uid="GPXHAEZIDW",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ELCODATT", cQueryName = "ELCODATT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 11098522,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=117, Top=123, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_ELCOCOMM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_ELCODATT"

  func oELCODATT_2_9.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  func oELCODATT_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODATT_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODATT_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_ELCOCOMM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_ELCOCOMM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oELCODATT_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'',this.parent.oContained
  endproc
  proc oELCODATT_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_ELCOCOMM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_ELCODATT
     i_obj.ecpSave()
  endproc

  add object oDESATT_2_14 as StdField with uid="ZEOQLRVGNK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 77353526,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=241, Top=121, InputMask=replicate('X',100)

  func oDESATT_2_14.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oStr_2_10 as StdString with uid="FRRXLKVZKC",Visible=.t., Left=22, Top=33,;
    Alignment=1, Width=91, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_10.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' or g_COMM='S'))
    endwith
  endfunc

  add object oStr_2_11 as StdString with uid="JEGGTPWUNR",Visible=.t., Left=9, Top=66,;
    Alignment=1, Width=104, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_11.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_2_12 as StdString with uid="IGALBXRWFT",Visible=.t., Left=22, Top=94,;
    Alignment=1, Width=91, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_12.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc

  add object oStr_2_13 as StdString with uid="JDSEVDIYVD",Visible=.t., Left=22, Top=125,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_13.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc
enddefine
define class tgsag_kdgPag3 as StdContainer
  Width  = 546
  height = 232
  stdWidth  = 546
  stdheight = 232
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELINICOA_3_13 as StdField with uid="RHEZHSITTJ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ELINICOA", cQueryName = "ELINICOA",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data inizio competenza attivit�",;
    HelpContextID = 49854855,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=126, Top=44

  func oELINICOA_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP<>'S' AND NOT EMPTY(.w_ELTIPATT))
    endwith
   endif
  endfunc

  func oELINICOA_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ELINICOA<=.w_ELFINCOA) or (empty(.w_ELFINCOA)))
    endwith
    return bRes
  endfunc

  add object oELFINCOA_3_14 as StdField with uid="GYQZQYHAHK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ELFINCOA", cQueryName = "ELFINCOA",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data fine competenza attivit�",;
    HelpContextID = 54757767,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=232, Top=44

  func oELFINCOA_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP<>'S' AND NOT EMPTY(.w_ELTIPATT) AND NOT EMPTY(.w_ELINICOA))
    endwith
   endif
  endfunc

  func oELFINCOA_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ELINICOA<=.w_ELFINCOA) or (empty(.w_ELFINCOA)))
    endwith
    return bRes
  endfunc

  add object oELINICOD_3_15 as StdField with uid="YQMOYASWGT",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ELINICOD", cQueryName = "ELINICOD",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data inizio competenza documenti",;
    HelpContextID = 49854858,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=126, Top=100

  func oELINICOD_3_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(NVL(.w_ELCAUDOC,'')))
    endwith
   endif
  endfunc

  func oELINICOD_3_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ELINICOD<=.w_ELFINCOD) or (empty(.w_ELFINCOD)))
    endwith
    return bRes
  endfunc

  add object oELFINCOD_3_16 as StdField with uid="SRLZYTNAHU",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ELFINCOD", cQueryName = "ELFINCOD",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data fine competenza documenti",;
    HelpContextID = 54757770,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=232, Top=100

  func oELFINCOD_3_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(NVL(.w_ELCAUDOC,'')) AND NOT EMPTY(.w_ELINICOD))
    endwith
   endif
  endfunc

  func oELFINCOD_3_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ELINICOD<=.w_ELFINCOD) or (empty(.w_ELFINCOD)))
    endwith
    return bRes
  endfunc

  add object oStr_3_6 as StdString with uid="AVFXAJDCUT",Visible=.t., Left=14, Top=71,;
    Alignment=0, Width=182, Height=19,;
    Caption="Date competenza documenti"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_7 as StdString with uid="GCPODTESED",Visible=.t., Left=210, Top=100,;
    Alignment=1, Width=18, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_3_8 as StdString with uid="NGIFGGTXVQ",Visible=.t., Left=100, Top=100,;
    Alignment=1, Width=22, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_3_9 as StdString with uid="OZGCXNDBIP",Visible=.t., Left=210, Top=44,;
    Alignment=1, Width=18, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="BVZFJJLPAP",Visible=.t., Left=100, Top=44,;
    Alignment=1, Width=22, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="NWIKHRMAGV",Visible=.t., Left=14, Top=17,;
    Alignment=0, Width=158, Height=19,;
    Caption="Date competenza attivit�"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_5 as StdBox with uid="BFTZJFFWUL",left=3, top=92, width=517,height=2

  add object oBox_3_11 as StdBox with uid="PYXUPMIUOM",left=3, top=38, width=517,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kdg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
