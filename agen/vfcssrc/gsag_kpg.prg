* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kpg                                                        *
*              Inserimento multiplo partecipanti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-24                                                      *
* Last revis.: 2013-08-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kpg",oParentObject))

* --- Class definition
define class tgsag_kpg as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 687
  Height = 467
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-08-01"
  HelpContextID=149563241
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  cPrg = "gsag_kpg"
  cComment = "Inserimento multiplo partecipanti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PATIPRIS = space(1)
  o_PATIPRIS = space(1)
  w_GRUPPO = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_COGNPART = space(40)
  w_NOMEPART = space(40)
  w_DESCRPART = space(40)
  w_DPTIPRIS = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_RESCHK = 0
  w_DPDESCRI = space(40)
  w_TIPRIS = space(1)
  w_zoompart = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kpgPag1","gsag_kpg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATIPRIS_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_zoompart = this.oPgFrm.Pages(1).oPag.zoompart
    DoDefault()
    proc Destroy()
      this.w_zoompart = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DIPENDEN'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PATIPRIS=space(1)
      .w_GRUPPO=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_COGNPART=space(40)
      .w_NOMEPART=space(40)
      .w_DESCRPART=space(40)
      .w_DPTIPRIS=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_RESCHK=0
      .w_DPDESCRI=space(40)
      .w_TIPRIS=space(1)
        .w_PATIPRIS = ''
        .w_GRUPPO = ''
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_GRUPPO))
          .link_1_2('Full')
        endif
        .w_DATOBSO = IIF(Lower(This.oparentobject.class) ='tgsag_mca',i_datsys,this.oparentobject.w_DATFIN)
      .oPgFrm.Page1.oPag.zoompart.Calculate(.w_GRUPPO)
          .DoRTCalc(4,7,.f.)
        .w_OB_TEST = i_INIDAT
        .w_RESCHK = 0
    endwith
    this.DoRTCalc(10,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_PATIPRIS<>.w_PATIPRIS
            .w_GRUPPO = ''
          .link_1_2('Full')
        endif
            .w_DATOBSO = IIF(Lower(This.oparentobject.class) ='tgsag_mca',i_datsys,this.oparentobject.w_DATFIN)
        .oPgFrm.Page1.oPag.zoompart.Calculate(.w_GRUPPO)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.zoompart.Calculate(.w_GRUPPO)
    endwith
  return

  proc Calculate_QTYFFRAAXR()
    with this
          * --- Inserimento multiplo prestazioni
          GSAG_BPG(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGRUPPO_1_2.enabled = this.oPgFrm.Page1.oPag.oGRUPPO_1_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.zoompart.Event(cEvent)
        if lower(cEvent)==lower("InsPrest")
          .Calculate_QTYFFRAAXR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GRUPPO
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPPO)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_GRUPPO))
          select DPCODICE,DPDESCRI,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPPO)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUPPO) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oGRUPPO_1_2'),i_cWhere,'GSAR_BDZ',"Gruppi",'GRUPPI.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPPO)
            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPPO = NVL(_Link_.DPCODICE,space(5))
      this.w_DPDESCRI = NVL(_Link_.DPDESCRI,space(40))
      this.w_TIPRIS = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPPO = space(5)
      endif
      this.w_DPDESCRI = space(40)
      this.w_TIPRIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIS='G' AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore incongruente o obsoleto")
        endif
        this.w_GRUPPO = space(5)
        this.w_DPDESCRI = space(40)
        this.w_TIPRIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPATIPRIS_1_1.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page1.oPag.oPATIPRIS_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUPPO_1_2.value==this.w_GRUPPO)
      this.oPgFrm.Page1.oPag.oGRUPPO_1_2.value=this.w_GRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDESCRI_1_17.value==this.w_DPDESCRI)
      this.oPgFrm.Page1.oPag.oDPDESCRI_1_17.value=this.w_DPDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPRIS='G' AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST))  and (.w_PATIPRIS<>'G' AND .w_PATIPRIS<>'R' AND .w_PATIPRIS<>'P')  and not(empty(.w_GRUPPO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUPPO_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore incongruente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_kpg
      IF i_bRes=.t.
         IF i_bRes
           .w_RESCHK=0 
           .NotifyEvent('InsPrest')
           IF .w_RESCHK<>0
             i_bRes=.f.
           ENDIF
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PATIPRIS = this.w_PATIPRIS
    return

enddefine

* --- Define pages as container
define class tgsag_kpgPag1 as StdContainer
  Width  = 683
  height = 467
  stdWidth  = 683
  stdheight = 467
  resizeXpos=501
  resizeYpos=347
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPATIPRIS_1_1 as StdCombo with uid="FEFTOWFWCM",value=4,rtseq=1,rtrep=.f.,left=91,top=12,width=76,height=21;
    , HelpContextID = 26976439;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPATIPRIS_1_1.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    iif(this.value =3,'R',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oPATIPRIS_1_1.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_1_1.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',3,;
      iif(this.Parent.oContained.w_PATIPRIS=='',4,;
      0))))
  endfunc

  add object oGRUPPO_1_2 as StdField with uid="ZKQXFBFZCD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GRUPPO", cQueryName = "GRUPPO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valore incongruente o obsoleto",;
    HelpContextID = 191594342,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=91, Top=39, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_GRUPPO"

  func oGRUPPO_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PATIPRIS<>'G' AND .w_PATIPRIS<>'R' AND .w_PATIPRIS<>'P')
    endwith
   endif
  endfunc

  func oGRUPPO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPPO_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPPO_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oGRUPPO_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GRUPPI.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oGRUPPO_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_GRUPPO
     i_obj.ecpSave()
  endproc


  add object oBtn_1_4 as StdButton with uid="PHEWMSSLEL",left=614, top=16, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 27358998;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object zoompart as cp_szoombox with uid="OLCEFFBPUU",left=8, top=73, width=661,height=335,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSAG_KPG",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cMenuFile="",bQueryOnLoad=.t.,bReadOnly=.t.,cTable="DIPENDEN",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "w_PATIPRIS Changed,Ricerca",;
    nPag=1;
    , HelpContextID = 28434406


  add object oBtn_1_6 as StdButton with uid="ERHOXVEEGI",left=562, top=416, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 149534490;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="TKCMXZXILU",left=614, top=416, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142245818;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDPDESCRI_1_17 as StdField with uid="HXOWMFSHMI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DPDESCRI", cQueryName = "DPDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 261057919,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=176, Top=39, InputMask=replicate('X',40)

  add object oStr_1_8 as StdString with uid="ZGENJNNRPH",Visible=.t., Left=10, Top=12,;
    Alignment=1, Width=77, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="IEFMRANLXT",Visible=.t., Left=43, Top=39,;
    Alignment=0, Width=44, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kpg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
