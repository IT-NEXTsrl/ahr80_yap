* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kri                                                        *
*              Posponi il promemoria dell'attivit�                             *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-06-16                                                      *
* Last revis.: 2010-10-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kri",oParentObject))

* --- Class definition
define class tgsag_kri as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 652
  Height = 343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-10-25"
  HelpContextID=152426647
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_kri"
  cComment = "Posponi il promemoria dell'attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_ORAINI = space(2)
  w_ORAFIN = space(2)
  w_MININI = space(2)
  w_GIOINI = space(10)
  w_MINFIN = space(2)
  w_GIOFIN = space(10)
  w_CAMINPRE = 0
  o_CAMINPRE = 0
  w_DATAPROMEMORIA = space(10)
  w_DATAPRO = ctod('  /  /  ')
  w_ORAPRO = space(2)
  w_MINPRO = space(2)
  w_GIOPRM = space(10)
  w_AGGIORNA = space(10)
  w_ATPROMEM = ctot('')
  w_ATCAUATT = space(20)
  w_ATOGGETT = space(254)
  w_ATNOTPIA = space(0)
  w_ATCODPRA = space(15)
  w_DESCAN = space(100)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kriPag1","gsag_kri",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCAMINPRE_1_13
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_ORAINI=space(2)
      .w_ORAFIN=space(2)
      .w_MININI=space(2)
      .w_GIOINI=space(10)
      .w_MINFIN=space(2)
      .w_GIOFIN=space(10)
      .w_CAMINPRE=0
      .w_DATAPROMEMORIA=space(10)
      .w_DATAPRO=ctod("  /  /  ")
      .w_ORAPRO=space(2)
      .w_MINPRO=space(2)
      .w_GIOPRM=space(10)
      .w_AGGIORNA=space(10)
      .w_ATPROMEM=ctot("")
      .w_ATCAUATT=space(20)
      .w_ATOGGETT=space(254)
      .w_ATNOTPIA=space(0)
      .w_ATCODPRA=space(15)
      .w_DESCAN=space(100)
      .w_DATINI=oParentObject.w_DATINI
      .w_DATFIN=oParentObject.w_DATFIN
      .w_ORAINI=oParentObject.w_ORAINI
      .w_ORAFIN=oParentObject.w_ORAFIN
      .w_MININI=oParentObject.w_MININI
      .w_MINFIN=oParentObject.w_MINFIN
      .w_DATAPRO=oParentObject.w_DATAPRO
      .w_ORAPRO=oParentObject.w_ORAPRO
      .w_MINPRO=oParentObject.w_MINPRO
      .w_AGGIORNA=oParentObject.w_AGGIORNA
      .w_ATPROMEM=oParentObject.w_ATPROMEM
      .w_ATCAUATT=oParentObject.w_ATCAUATT
      .w_ATOGGETT=oParentObject.w_ATOGGETT
      .w_ATNOTPIA=oParentObject.w_ATNOTPIA
      .w_ATCODPRA=oParentObject.w_ATCODPRA
      .w_DESCAN=oParentObject.w_DESCAN
          .DoRTCalc(1,5,.f.)
        .w_GIOINI = IIF(NOT EMPTY(.w_DATINI),LEFT(g_GIORNO[DOW(.w_DATINI)],3), '   ')
          .DoRTCalc(7,7,.f.)
        .w_GIOFIN = IIF(NOT EMPTY(.w_DATFIN),LEFT(g_GIORNO[DOW(.w_DATFIN)],3), '   ')
        .w_CAMINPRE = 0
        .w_DATAPROMEMORIA = Datetime() + IIF( .w_CAMINPRE >=0 , .w_CAMINPRE , 0 ) * 60
          .DoRTCalc(11,13,.f.)
        .w_GIOPRM = IIF(NOT EMPTY(.w_DATAPRO),LEFT(g_GIORNO[DOW(.w_DATAPRO)],3), '   ')
        .w_AGGIORNA = "S"
    endwith
    this.DoRTCalc(16,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATINI=.w_DATINI
      .oParentObject.w_DATFIN=.w_DATFIN
      .oParentObject.w_ORAINI=.w_ORAINI
      .oParentObject.w_ORAFIN=.w_ORAFIN
      .oParentObject.w_MININI=.w_MININI
      .oParentObject.w_MINFIN=.w_MINFIN
      .oParentObject.w_DATAPRO=.w_DATAPRO
      .oParentObject.w_ORAPRO=.w_ORAPRO
      .oParentObject.w_MINPRO=.w_MINPRO
      .oParentObject.w_AGGIORNA=.w_AGGIORNA
      .oParentObject.w_ATPROMEM=.w_ATPROMEM
      .oParentObject.w_ATCAUATT=.w_ATCAUATT
      .oParentObject.w_ATOGGETT=.w_ATOGGETT
      .oParentObject.w_ATNOTPIA=.w_ATNOTPIA
      .oParentObject.w_ATCODPRA=.w_ATCODPRA
      .oParentObject.w_DESCAN=.w_DESCAN
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_GIOINI = IIF(NOT EMPTY(.w_DATINI),LEFT(g_GIORNO[DOW(.w_DATINI)],3), '   ')
        .DoRTCalc(7,7,.t.)
            .w_GIOFIN = IIF(NOT EMPTY(.w_DATFIN),LEFT(g_GIORNO[DOW(.w_DATFIN)],3), '   ')
        .DoRTCalc(9,9,.t.)
            .w_DATAPROMEMORIA = Datetime() + IIF( .w_CAMINPRE >=0 , .w_CAMINPRE , 0 ) * 60
        if .o_CAMINPRE<>.w_CAMINPRE
            .w_DATAPRO = CP_TODATE( .w_DATAPROMEMORIA )
        endif
        if .o_CAMINPRE<>.w_CAMINPRE
            .w_ORAPRO = RIGHT( "00" + ALLTRIM( STR( HOUR( .w_DATAPROMEMORIA ) ) ) , 2 )
        endif
        if .o_CAMINPRE<>.w_CAMINPRE
            .w_MINPRO = RIGHT( "00" + ALLTRIM( STR( MINUTE( .w_DATAPROMEMORIA ) ) ) , 2 )
        endif
            .w_GIOPRM = IIF(NOT EMPTY(.w_DATAPRO),LEFT(g_GIORNO[DOW(.w_DATAPRO)],3), '   ')
            .w_AGGIORNA = "S"
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_1.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_3.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_3.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_7.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_7.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_1_8.value==this.w_ORAFIN)
      this.oPgFrm.Page1.oPag.oORAFIN_1_8.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_9.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_9.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOINI_1_10.value==this.w_GIOINI)
      this.oPgFrm.Page1.oPag.oGIOINI_1_10.value=this.w_GIOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_11.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_11.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOFIN_1_12.value==this.w_GIOFIN)
      this.oPgFrm.Page1.oPag.oGIOFIN_1_12.value=this.w_GIOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMINPRE_1_13.RadioValue()==this.w_CAMINPRE)
      this.oPgFrm.Page1.oPag.oCAMINPRE_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAPRO_1_15.value==this.w_DATAPRO)
      this.oPgFrm.Page1.oPag.oDATAPRO_1_15.value=this.w_DATAPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oORAPRO_1_18.value==this.w_ORAPRO)
      this.oPgFrm.Page1.oPag.oORAPRO_1_18.value=this.w_ORAPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oMINPRO_1_19.value==this.w_MINPRO)
      this.oPgFrm.Page1.oPag.oMINPRO_1_19.value=this.w_MINPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOPRM_1_20.value==this.w_GIOPRM)
      this.oPgFrm.Page1.oPag.oGIOPRM_1_20.value=this.w_GIOPRM
    endif
    if not(this.oPgFrm.Page1.oPag.oATCAUATT_1_27.value==this.w_ATCAUATT)
      this.oPgFrm.Page1.oPag.oATCAUATT_1_27.value=this.w_ATCAUATT
    endif
    if not(this.oPgFrm.Page1.oPag.oATOGGETT_1_28.value==this.w_ATOGGETT)
      this.oPgFrm.Page1.oPag.oATOGGETT_1_28.value=this.w_ATOGGETT
    endif
    if not(this.oPgFrm.Page1.oPag.oATNOTPIA_1_31.value==this.w_ATNOTPIA)
      this.oPgFrm.Page1.oPag.oATNOTPIA_1_31.value=this.w_ATNOTPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODPRA_1_32.value==this.w_ATCODPRA)
      this.oPgFrm.Page1.oPag.oATCODPRA_1_32.value=this.w_ATCODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_33.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_33.value=this.w_DESCAN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(VAL( .w_ORAPRO ) < 24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAPRO_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL( .w_MINPRO ) < 60)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINPRO_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CAMINPRE = this.w_CAMINPRE
    return

enddefine

* --- Define pages as container
define class tgsag_kriPag1 as StdContainer
  Width  = 648
  height = 343
  stdWidth  = 648
  stdheight = 343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_1 as StdField with uid="FXKXDCGBZM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 121921078,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=132, Top=64

  add object oDATFIN_1_3 as StdField with uid="ZGTJQCTDCT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 200367670,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=132, Top=91

  add object oORAINI_1_7 as StdField with uid="GPIKVFLWWL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 121847782,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=288, Top=64, InputMask=replicate('X',2)

  add object oORAFIN_1_8 as StdField with uid="GYQFKAUYGW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 200294374,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=288, Top=90, InputMask=replicate('X',2)

  add object oMININI_1_9 as StdField with uid="OUBSUDMDGI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 121898694,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=320, Top=64, InputMask=replicate('X',2)

  add object oGIOINI_1_10 as StdField with uid="HCGHSAKHPY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_GIOINI", cQueryName = "GIOINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 121902694,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=91, Top=64, InputMask=replicate('X',10)

  add object oMINFIN_1_11 as StdField with uid="FCCQTLXPQN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 200345286,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=320, Top=90, InputMask=replicate('X',2)

  add object oGIOFIN_1_12 as StdField with uid="YPCPUCAQLO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_GIOFIN", cQueryName = "GIOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 200349286,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=91, Top=90, InputMask=replicate('X',10)


  add object oCAMINPRE_1_13 as StdCombo with uid="JDGKKBWCTW",value=1,rtseq=9,rtrep=.f.,left=92,top=242,width=184,height=21;
    , HelpContextID = 239332971;
    , cFormVar="w_CAMINPRE",RowSource=""+"0 minuti,"+"5 minuti dopo,"+"10 minuti dopo,"+"15 minuti dopo,"+"30 minuti dopo,"+"1 ora dopo,"+"2 ore dopo,"+"3 ore dopo,"+"4 ore dopo,"+"1 giorno dopo,"+"2 giorni dopo,"+"3 giorni dopo,"+"1 settimana dopo,"+"2 settimane dopo,"+"3 settimane dopo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAMINPRE_1_13.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,5,;
    iif(this.value =3,10,;
    iif(this.value =4,15,;
    iif(this.value =5,30,;
    iif(this.value =6,60,;
    iif(this.value =7,120,;
    iif(this.value =8,180,;
    iif(this.value =9,240,;
    iif(this.value =10,1440,;
    iif(this.value =11,2880,;
    iif(this.value =12,4320,;
    iif(this.value =13,10080,;
    iif(this.value =14,20160,;
    iif(this.value =15,30240,;
    0))))))))))))))))
  endfunc
  func oCAMINPRE_1_13.GetRadio()
    this.Parent.oContained.w_CAMINPRE = this.RadioValue()
    return .t.
  endfunc

  func oCAMINPRE_1_13.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CAMINPRE==0,1,;
      iif(this.Parent.oContained.w_CAMINPRE==5,2,;
      iif(this.Parent.oContained.w_CAMINPRE==10,3,;
      iif(this.Parent.oContained.w_CAMINPRE==15,4,;
      iif(this.Parent.oContained.w_CAMINPRE==30,5,;
      iif(this.Parent.oContained.w_CAMINPRE==60,6,;
      iif(this.Parent.oContained.w_CAMINPRE==120,7,;
      iif(this.Parent.oContained.w_CAMINPRE==180,8,;
      iif(this.Parent.oContained.w_CAMINPRE==240,9,;
      iif(this.Parent.oContained.w_CAMINPRE==1440,10,;
      iif(this.Parent.oContained.w_CAMINPRE==2880,11,;
      iif(this.Parent.oContained.w_CAMINPRE==4320,12,;
      iif(this.Parent.oContained.w_CAMINPRE==10080,13,;
      iif(this.Parent.oContained.w_CAMINPRE==20160,14,;
      iif(this.Parent.oContained.w_CAMINPRE==30240,15,;
      0)))))))))))))))
  endfunc

  add object oDATAPRO_1_15 as StdField with uid="YTFFPSDTQK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATAPRO", cQueryName = "DATAPRO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 6053430,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=132, Top=268

  add object oORAPRO_1_18 as StdField with uid="DGQGHTOYRT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ORAPRO", cQueryName = "ORAPRO",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 227164134,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=288, Top=267, InputMask=replicate('X',2)

  func oORAPRO_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL( .w_ORAPRO ) < 24)
    endwith
    return bRes
  endfunc

  add object oMINPRO_1_19 as StdField with uid="NNCMVWJZKO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MINPRO", cQueryName = "MINPRO",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 227215046,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=320, Top=267, InputMask=replicate('X',2)

  func oMINPRO_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL( .w_MINPRO ) < 60)
    endwith
    return bRes
  endfunc

  add object oGIOPRM_1_20 as StdField with uid="KAWSKXYFLR",rtseq=14,rtrep=.f.,;
    cFormVar = "w_GIOPRM", cQueryName = "GIOPRM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 193664614,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=91, Top=268, InputMask=replicate('X',10)


  add object oBtn_1_24 as StdButton with uid="UUNSDLSMLD",left=537, top=290, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 152455398;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_25 as StdButton with uid="TKCMXZXILU",left=590, top=290, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 159744070;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oATCAUATT_1_27 as StdField with uid="EZVMZCVLYY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ATCAUATT", cQueryName = "ATCAUATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 5545638,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=91, Top=10, InputMask=replicate('X',20)

  add object oATOGGETT_1_28 as StdField with uid="TFFCVESQZU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ATOGGETT", cQueryName = "ATOGGETT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 221109926,;
   bGlobalFont=.t.,;
    Height=21, Width=545, Left=91, Top=38, InputMask=replicate('X',254)

  add object oATNOTPIA_1_31 as StdMemo with uid="UWDMJDAHMC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ATNOTPIA", cQueryName = "ATNOTPIA",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 22408889,;
   bGlobalFont=.t.,;
    Height=92, Width=547, Left=91, Top=115, readonly = .t.

  add object oATCODPRA_1_32 as StdField with uid="UTZUBEAYUA",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ATCODPRA", cQueryName = "ATCODPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 229204295,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=92, Top=213, InputMask=replicate('X',15)

  add object oDESCAN_1_33 as StdField with uid="KPCBERMQAY",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 191779382,;
   bGlobalFont=.t.,;
    Height=21, Width=426, Left=213, Top=213, InputMask=replicate('X',100)

  add object oStr_1_2 as StdString with uid="OCFGKEYSSG",Visible=.t., Left=25, Top=65,;
    Alignment=1, Width=62, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="AOPPZOIKFZ",Visible=.t., Left=25, Top=92,;
    Alignment=1, Width=62, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="EZMYBTMUCC",Visible=.t., Left=222, Top=65,;
    Alignment=1, Width=62, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="XGFOJGGKUL",Visible=.t., Left=222, Top=92,;
    Alignment=1, Width=62, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="GWFVXBTTMC",Visible=.t., Left=15, Top=269,;
    Alignment=1, Width=72, Height=18,;
    Caption="Promemoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="HJWCHVYKWW",Visible=.t., Left=222, Top=269,;
    Alignment=1, Width=62, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="CCZXMSVYOQ",Visible=.t., Left=2, Top=243,;
    Alignment=1, Width=85, Height=18,;
    Caption="Posponi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="JBMJEAYLWX",Visible=.t., Left=13, Top=13,;
    Alignment=1, Width=72, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="KVVYXUUAFA",Visible=.t., Left=19, Top=123,;
    Alignment=1, Width=66, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="YTGGNFMNMH",Visible=.t., Left=6, Top=214,;
    Alignment=1, Width=83, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (! IsAlt())
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="MSTIPXIIOM",Visible=.t., Left=6, Top=214,;
    Alignment=1, Width=83, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kri','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
