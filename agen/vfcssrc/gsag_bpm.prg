* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bpm                                                        *
*              Modifica zoom gsar_kpm                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-29                                                      *
* Last revis.: 2012-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bpm",oParentObject,m.pOPER)
return(i_retval)

define class tgsag_bpm as StdBatch
  * --- Local variables
  pOPER = space(3)
  w_LOOP = 0
  w_CursorZoom = space(1)
  w_ITNUMGIO = 0
  w_ITRIFPRO = 0
  w_NewDateTime = ctot("")
  w_NewDate = ctod("  /  /  ")
  w_SAVREC = 0
  * --- WorkFile variables
  CAL_AZIE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pOPER=="ZAQ"
        this.w_LOOP = 1
        do while this.w_LOOP <= this.oParentObject.w_AGKPM_ZOOM.grd.ColumnCount
          do case
            case !isALT() AND INLIST( UPPER( this.oParentObject.w_AGKPM_ZOOM.grd.Columns[ this.w_LOOP ].ControlSource), "CACODART", "DESTIPOL" )
              this.oParentObject.w_AGKPM_ZOOM.grd.Columns[ this.w_LOOP ].Visible = .F.
            case isALT() AND INLIST( UPPER( this.oParentObject.w_AGKPM_ZOOM.grd.Columns[ this.w_LOOP ].ControlSource), "ARGRUMER", "ARCATCON", "ARCODIVA", "ARUNMIS1" )
              this.oParentObject.w_AGKPM_ZOOM.grd.Columns[ this.w_LOOP ].Visible = .F.
          endcase
          if UPPER( this.oParentObject.w_AGKPM_ZOOM.grd.Columns[ this.w_LOOP ].ControlSource)="NEWDT" AND (!IsAlt() OR EMPTY(this.oParentObject.w_SELRAGG))
            this.oParentObject.w_AGKPM_ZOOM.grd.Columns[ this.w_LOOP ].Visible = .F.
          endif
          if this.w_LOOP<=ALEN(this.oParentObject.w_AGKPM_ZOOM.nColOrd, 1)
            this.oParentObject.w_AGKPM_ZOOM.nColOrd[ this.w_LOOP] = this.w_LOOP
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
        if Vartype(This.oparentobject.w_AGKPM_ZOOM.oRefHeaderObject)="O"
          this.oParentObject.w_AGKPM_ZOOM.oRefHeaderObject.HeaderRedraw()     
        endif
      case this.pOPER=="RAGG"
        * --- E' stato selezionato un raggruppamento, calcoliamo le date delle prestazioni
        if IsAlt()
          this.w_CursorZoom = this.oParentObject.w_AGKPM_ZOOM.ccursor
          SELECT (this.w_CursorZoom)
          SCAN
          this.w_ITNUMGIO = ITNUMGIO
          this.w_ITRIFPRO = ITRIFPRO
          this.w_NewDateTime = NEWDT
          this.w_NewDate = this.oParentObject.w_DATAPRE
          if !EMPTY(this.w_ITRIFPRO)
            this.w_SAVREC = RECNO()
            * --- Cerchiamo la data di riferimento
            * ---  and Not (isnull(DATA_ATT) OR empty(DATA_ATT))
            LOCATE FOR CPROWORD=this.w_ITRIFPRO
            if FOUND() AND !EMPTY(NEWDT)
              * --- C'� una data specificata nel cursore
              this.w_NewDateTime = NEWDT
              this.w_NewDate = TTOD(this.w_NewDateTime)
            endif
            SELECT (this.w_CursorZoom)
            GOTO this.w_SAVREC
          endif
          this.w_NewDate = this.w_NewDate+this.w_ITNUMGIO
          REPLACE NEWDT WITH DTOT(this.w_NewDate)
          ENDSCAN
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAL_AZIE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
