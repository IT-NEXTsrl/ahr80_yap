* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_brg                                                        *
*              Rilevazione giornaliera                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-16                                                      *
* Last revis.: 2010-03-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_brg",oParentObject,m.pOper)
return(i_retval)

define class tgsag_brg as StdBatch
  * --- Local variables
  pOper = space(10)
  w_OBJECT = .NULL.
  w_FOUND = .f.
  * --- WorkFile variables
  DIPENDEN_idx=0
  CAUMATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                                    Gestione rilevazione giornaliera del tempo
    do case
      case this.pOper == "N"
        * --- LANCIA RILEVAZIONE GIORNALIERA DEL TEMPO IN CARICAMENTO
        * --- Istanzio la movimentazione
        this.w_OBJECT = GSAG_MRG()
        * --- Controllo se ha passato il test di accesso
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.ECPLOAD()     
      case this.pOper == "V"
        * --- VISUALIZZA RILEVAZIONE GIORNALIERA DEL TEMPO
        * --- Istanzio la movimentazione
        this.w_OBJECT = GSAG_MRG()
        * --- Controllo se ha passato il test di accesso
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Carico il record selezionato
        this.w_OBJECT.ECPFILTER()     
        this.w_OBJECT.w_RGCODPER = this.oParentObject.w_RGCODPER
        this.w_OBJECT.w_RG__DATA = this.oParentObject.w_RG__DATA
        this.w_OBJECT.ECPSAVE()     
        * --- Tenta di posizionarsi sulla riga dell'attivit� selezionata
        this.w_FOUND = .F.
        SELECT (this.w_OBJECT.cTrsName)
        if reccount()<>0
          * --- Si posiziona sulla prima riga
          this.w_OBJECT.FirstRow()     
          do while ! this.w_OBJECT.Eof_Trs() AND !this.w_FOUND
            this.w_OBJECT.SetRow()     
            if this.w_OBJECT.w_CPROWNUM = this.oParentObject.w_RowNum
              * --- E' la riga dell'attivit�
              this.w_OBJECT.MarkPos()     
              this.w_FOUND = .T.
            else
              this.w_OBJECT.NextRow()     
            endif
          enddo
          if this.w_FOUND
            * --- Si posiziona sulla riga su cui ha fatto la MarkPos
            this.w_OBJECT.RePos()     
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAUMATTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
