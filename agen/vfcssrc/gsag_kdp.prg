* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kdp                                                        *
*              Disponibilit� di persone e risorse                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-29                                                      *
* Last revis.: 2014-02-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsag_kdp
* Controllo presenza delle ocx
IF ProvaOcx('cp_ctList')
   Ah_ErrorMsg("Errore nella gestione di ctList.OCX")
   RETURN
ENDIF
IF ProvaOcx('cp_ctSchedule')
   Ah_ErrorMsg("Errore nella gestione di ctSchedule.OCX")
   RETURN
ENDIF
* --- Fine Area Manuale
return(createobject("tgsag_kdp",oParentObject))

* --- Class definition
define class tgsag_kdp as StdForm
  Top    = 9
  Left   = 7

  * --- Standard Properties
  Width  = 813
  Height = 373+35
  bGlobalFont=.f.
  FontName      = "MS Sans Serif"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-26"
  HelpContextID=185981079
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_kdp"
  cComment = "Disponibilit� di persone e risorse"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATA_INI = ctot('')
  w_DATA_FIN = ctot('')
  w_ClnCheck = 0
  w_ClnDesRis = 0
  w_DipCodice = space(20)
  w_KeyDispo = space(10)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_OREFIN = space(2)
  o_OREFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_DipendSel = space(20)
  w_DimScheda = 0
  w_ATTSTU = space(1)
  w_SESSTEL = space(1)
  w_ASSENZE = space(1)
  w_ATTGENER = space(1)
  w_MEMO = space(1)
  w_DAFARE = space(1)
  w_UDIENZE = space(1)
  w_DAINSPRE = space(1)
  w_ATTFUOSTU = space(1)
  w_ClnCheck_Ris = 0
  w_ClnDesRis_Ris = 0
  w_STATO = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_RISERVE = space(1)
  w_ctList_Parte = .NULL.
  w_ctSchedParte = .NULL.
  w_ctList_Risorse = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kdpPag1","gsag_kdp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Agenda")
      .Pages(2).addobject("oPag","tgsag_kdpPag2","gsag_kdp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_15
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ctList_Parte = this.oPgFrm.Pages(1).oPag.ctList_Parte
    this.w_ctSchedParte = this.oPgFrm.Pages(1).oPag.ctSchedParte
    this.w_ctList_Risorse = this.oPgFrm.Pages(1).oPag.ctList_Risorse
    DoDefault()
    proc Destroy()
      this.w_ctList_Parte = .NULL.
      this.w_ctSchedParte = .NULL.
      this.w_ctList_Risorse = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATA_INI=ctot("")
      .w_DATA_FIN=ctot("")
      .w_ClnCheck=0
      .w_ClnDesRis=0
      .w_DipCodice=space(20)
      .w_KeyDispo=space(10)
      .w_DATINI=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_DATFIN=ctod("  /  /  ")
      .w_OREFIN=space(2)
      .w_MINFIN=space(2)
      .w_DipendSel=space(20)
      .w_DimScheda=0
      .w_ATTSTU=space(1)
      .w_SESSTEL=space(1)
      .w_ASSENZE=space(1)
      .w_ATTGENER=space(1)
      .w_MEMO=space(1)
      .w_DAFARE=space(1)
      .w_UDIENZE=space(1)
      .w_DAINSPRE=space(1)
      .w_ATTFUOSTU=space(1)
      .w_ClnCheck_Ris=0
      .w_ClnDesRis_Ris=0
      .w_STATO=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_RISERVE=space(1)
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI) AND !EMPTY(.w_OREINI) AND !EMPTY(.w_MININI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN) AND !EMPTY(.w_OREFIN) AND !EMPTY(.w_MINFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
      .oPgFrm.Page1.oPag.ctList_Parte.Calculate()
          .DoRTCalc(3,5,.f.)
        .w_KeyDispo = SYS(2015)
      .oPgFrm.Page1.oPag.ctSchedParte.Calculate()
          .DoRTCalc(7,7,.f.)
        .w_OREINI = PADL( IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2 ,'0')
        .w_MININI = PADL( IIF(empty(.w_MININI),'00',.w_MININI),2,'0')
        .w_DATFIN = iif(isalt(),Trasdatfin(.w_DATINI,.o_DATINI,.w_DATFIN),.w_DATFIN)
        .w_OREFIN = PADL( IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2,'0')
        .w_MINFIN = PADL( IIF(empty(.w_MINFIN) OR (.w_OREFIN="00" AND .w_MINFIN="00"), '59', .w_MINFIN),2,'0')
          .DoRTCalc(13,14,.f.)
        .w_ATTSTU = 'S'
        .w_SESSTEL = 'T'
        .w_ASSENZE = 'A'
        .w_ATTGENER = 'G'
        .w_MEMO = ' '
        .w_DAFARE = ' '
        .w_UDIENZE = 'U'
        .w_DAINSPRE = ' '
        .w_ATTFUOSTU = ' '
      .oPgFrm.Page1.oPag.ctList_Risorse.Calculate()
          .DoRTCalc(24,25,.f.)
        .w_STATO = 'N'
        .w_OB_TEST = i_INIDAT
        .w_RISERVE = 'E'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_11.enabled = this.oPgFrm.Page2.oPag.oBtn_2_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI) AND !EMPTY(.w_OREINI) AND !EMPTY(.w_MININI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN<>.w_DATFIN.or. .o_OREFIN<>.w_OREFIN.or. .o_MINFIN<>.w_MINFIN
            .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN) AND !EMPTY(.w_OREFIN) AND !EMPTY(.w_MINFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        .oPgFrm.Page1.oPag.ctList_Parte.Calculate()
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI.or. .o_DATFIN<>.w_DATFIN.or. .o_OREFIN<>.w_OREFIN.or. .o_MINFIN<>.w_MINFIN
          .Calculate_QKCKJJLEOF()
        endif
        if .o_DATINI<>.w_DATINI.or. .o_DATFIN<>.w_DATFIN
          .Calculate_SIJMIQAGQA()
        endif
        .oPgFrm.Page1.oPag.ctSchedParte.Calculate()
        .DoRTCalc(3,7,.t.)
        if .o_OREINI<>.w_OREINI
            .w_OREINI = PADL( IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2 ,'0')
        endif
        if .o_MININI<>.w_MININI
            .w_MININI = PADL( IIF(empty(.w_MININI),'00',.w_MININI),2,'0')
        endif
        if .o_DATINI<>.w_DATINI
            .w_DATFIN = iif(isalt(),Trasdatfin(.w_DATINI,.o_DATINI,.w_DATFIN),.w_DATFIN)
        endif
        if .o_OREFIN<>.w_OREFIN.or. .o_DATFIN<>.w_DATFIN
            .w_OREFIN = PADL( IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2,'0')
        endif
        if .o_MINFIN<>.w_MINFIN.or. .o_DATFIN<>.w_DATFIN
            .w_MINFIN = PADL( IIF(empty(.w_MINFIN) OR (.w_OREFIN="00" AND .w_MINFIN="00"), '59', .w_MINFIN),2,'0')
        endif
        if .o_DATINI<>.w_DATINI.or. .o_DATFIN<>.w_DATFIN
          .Calculate_NHGZPPOKMU()
        endif
        .oPgFrm.Page1.oPag.ctList_Risorse.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ctList_Parte.Calculate()
        .oPgFrm.Page1.oPag.ctSchedParte.Calculate()
        .oPgFrm.Page1.oPag.ctList_Risorse.Calculate()
    endwith
  return

  proc Calculate_UDTGOEMFEC()
    with this
          * --- Inizializza date
          .w_DATINI = i_datsys
          .w_DATFIN = i_datsys
          .w_OREINI = PADL( ALLTR(STR(INT(g_OraLavIni))),2 ,'0')
          .w_OREFIN = PADL( ALLTR(STR(INT(g_OraLavFin))),2 ,'0')
          .w_MININI = PADL( ALLTR(STR((g_OraLavIni-INT(g_OraLavIni))*60)),2 ,'0')
          .w_MINFIN = PADL( ALLTR(STR((g_OraLavFin-INT(g_OraLavFin))*60)),2 ,'0')
          GSAG_BDP(this;
              ,'INIT';
             )
          GSAG_BDP(this;
              ,'AGGIORNASCHEDULE';
             )
          .o_DATINI = .w_DATFIN
          .o_DATFIN = .w_DATFIN
          .o_OREINI = .w_OREINI
          .o_OREFIN = .w_OREFIN
          .o_MININI = .w_MININI
          .o_MINFIN = .w_MINFIN
    endwith
  endproc
  proc Calculate_ABLCCHFOLY()
    with this
          * --- Gestione check attivo
          gsag_bdp(this;
              ,'CHECKATTIVO';
             )
    endwith
  endproc
  proc Calculate_CWLWOSBKVB()
    with this
          * --- Gestione check disattivo
          gsag_bdp(this;
              ,'CHECKDISATTIVO';
             )
    endwith
  endproc
  proc Calculate_QKCKJJLEOF()
    with this
          * --- Aggiorna schedule
          gsag_bdp(this;
              ,'AGGIORNASCHEDULE';
             )
    endwith
  endproc
  proc Calculate_SIJMIQAGQA()
    with this
          * --- Imposta ora iniziale
          gsag_bdp(this;
              ,'IMPOSTAORA';
             )
    endwith
  endproc
  proc Calculate_WXWXRTBVWT()
    with this
          * --- Partecipante selezionato
          gsag_bdp(this;
              ,'MODIFICA';
             )
    endwith
  endproc
  proc Calculate_HALNTOGBAZ()
    with this
          * --- Ingrandisce la scheda
          gsag_bdp(this;
              ,"INGRANDISCI";
             )
    endwith
  endproc
  proc Calculate_ZSDRQRGJJG()
    with this
          * --- Riduce la scheda
          gsag_bdp(this;
              ,"RIDUCI";
             )
    endwith
  endproc
  proc Calculate_LYBISUKBKM()
    with this
          * --- Imposta data odierna
          .w_DATINI = i_DatSys
          .w_DATFIN = i_DatSys
          .w_OREINI = '08'
          .w_MININI = '00'
          .w_OREFIN = '23'
          .w_MINFIN = '59'
          .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '01-01-1900 00:00:00'))
          .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00', '31-12-2999 23:59:59'))
    endwith
  endproc
  proc Calculate_PDRFHTSFNI()
    with this
          * --- Aggiunge giorno
          .w_DATINI = .w_DATINI+1
          .w_DATFIN = .w_DATFIN+1
          .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '01-01-1900 00:00:00'))
          .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00', '31-12-2999 23:59:59'))
    endwith
  endproc
  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .Caption = IIF(.w_DATINI=.w_DATFIN,AH_MSGFORMAT("Disponibilit� di persone e risorse di %1 %2", IIF(!empty(.w_DATINI),alltrim(g_giorno[dow(.w_DATINI)]),''),DTOC(.w_DATINI)), AH_MSGFORMAT("Disponibilit� delle persone da %1 %2 a  %3 %4", IIF(!empty(.w_DATINI), alltrim(g_giorno[dow(.w_DATINI)]),''), DTOC(.w_DATINI), IIF(!empty(.w_DATFIN), alltrim(g_giorno[dow(.w_DATFIN)]),''), DTOC(.w_DATFIN)))
          .cComment = .Caption
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOREINI_1_16.enabled = this.oPgFrm.Page1.oPag.oOREINI_1_16.mCond()
    this.oPgFrm.Page1.oPag.oMININI_1_17.enabled = this.oPgFrm.Page1.oPag.oMININI_1_17.mCond()
    this.oPgFrm.Page1.oPag.oOREFIN_1_20.enabled = this.oPgFrm.Page1.oPag.oOREFIN_1_20.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_21.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oOREINI_1_16.visible=!this.oPgFrm.Page1.oPag.oOREINI_1_16.mHide()
    this.oPgFrm.Page1.oPag.oMININI_1_17.visible=!this.oPgFrm.Page1.oPag.oMININI_1_17.mHide()
    this.oPgFrm.Page1.oPag.oOREFIN_1_20.visible=!this.oPgFrm.Page1.oPag.oOREFIN_1_20.mHide()
    this.oPgFrm.Page1.oPag.oMINFIN_1_21.visible=!this.oPgFrm.Page1.oPag.oMINFIN_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_35.visible=!this.oPgFrm.Page1.oPag.oBtn_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_38.visible=!this.oPgFrm.Page1.oPag.oBtn_1_38.mHide()
    this.oPgFrm.Page2.oPag.oUDIENZE_2_7.visible=!this.oPgFrm.Page2.oPag.oUDIENZE_2_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ctList_Parte.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_UDTGOEMFEC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CTLIST_PARTE_CheckAttivo") or lower(cEvent)==lower("CTLIST_RISORSE_CheckAttivo")
          .Calculate_ABLCCHFOLY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CTLIST_PARTE_CheckDisattivo") or lower(cEvent)==lower("CTLIST_RISORSE_CheckDisattivo")
          .Calculate_CWLWOSBKVB()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ctSchedParte.Event(cEvent)
        if lower(cEvent)==lower("Modifica")
          .Calculate_WXWXRTBVWT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Ingrandisci")
          .Calculate_HALNTOGBAZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Riduci")
          .Calculate_ZSDRQRGJJG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ImpostaDataOdierna")
          .Calculate_LYBISUKBKM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AggiungeGiorno")
          .Calculate_PDRFHTSFNI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ctList_Risorse.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_15.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_15.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINI_1_16.value==this.w_OREINI)
      this.oPgFrm.Page1.oPag.oOREINI_1_16.value=this.w_OREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_17.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_17.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_19.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_19.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOREFIN_1_20.value==this.w_OREFIN)
      this.oPgFrm.Page1.oPag.oOREFIN_1_20.value=this.w_OREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_21.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_21.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oATTSTU_2_1.RadioValue()==this.w_ATTSTU)
      this.oPgFrm.Page2.oPag.oATTSTU_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSESSTEL_2_2.RadioValue()==this.w_SESSTEL)
      this.oPgFrm.Page2.oPag.oSESSTEL_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oASSENZE_2_3.RadioValue()==this.w_ASSENZE)
      this.oPgFrm.Page2.oPag.oASSENZE_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATTGENER_2_4.RadioValue()==this.w_ATTGENER)
      this.oPgFrm.Page2.oPag.oATTGENER_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMEMO_2_5.RadioValue()==this.w_MEMO)
      this.oPgFrm.Page2.oPag.oMEMO_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDAFARE_2_6.RadioValue()==this.w_DAFARE)
      this.oPgFrm.Page2.oPag.oDAFARE_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oUDIENZE_2_7.RadioValue()==this.w_UDIENZE)
      this.oPgFrm.Page2.oPag.oUDIENZE_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDAINSPRE_2_8.RadioValue()==this.w_DAINSPRE)
      this.oPgFrm.Page2.oPag.oDAINSPRE_2_8.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(VAL(.w_OREINI) < 24)  and not(empty(.w_DATINI))  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREINI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)  and not(empty(.w_DATINI))  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(VAL(.w_OREFIN) < 24)  and not(empty(.w_DATFIN))  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREFIN_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)  and not(empty(.w_DATFIN))  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATINI = this.w_DATINI
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_DATFIN = this.w_DATFIN
    this.o_OREFIN = this.w_OREFIN
    this.o_MINFIN = this.w_MINFIN
    return

enddefine

* --- Define pages as container
define class tgsag_kdpPag1 as StdContainer
  Width  = 809
  height = 373
  stdWidth  = 809
  stdheight = 373
  resizeXpos=174
  resizeYpos=211
  bGlobalFont=.f.
  FontName      = "MS Sans Serif"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ctList_Parte as cp_ctList with uid="YDSGZQVXNO",left=587, top=43, width=223,height=212,;
    caption='ctList_Parte',;
    FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    FontSize=8,HeaderFillType=0,FontName="MsSansSerif",HeadrText="",ItemSel="",ColumnSel="",serial="w_DipCodice",HeaderBorder=0,VertGridLines=.f.,;
    nPag=1;
    , HelpContextID = 148845690


  add object ctSchedParte as cp_ctSchedule with uid="MVWOJLEPUU",left=0, top=43, width=588,height=330,;
    caption='ctSchedParte',;
    FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Serial="w_DipendSel",FontName="MsSansSerif",FontSize=8,DayNames="",DateFormat="D M Y",HeaderFillType=2,MonthNames="",;
    nPag=1;
    , HelpContextID = 124468102

  add object oDATINI_1_15 as StdField with uid="VAJTUNAFFS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 112959946,;
    FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=66, Left=220, Top=15

  add object oOREINI_1_16 as StdField with uid="WOKDXXPUIP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OREINI", cQueryName = "OREINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di inizio selezione",;
    HelpContextID = 113016858,;
    FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=24, Left=359, Top=15, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREINI_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oOREINI_1_16.mHide()
    with this.Parent.oContained
      return (empty(.w_DATINI))
    endwith
  endfunc

  func oOREINI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_17 as StdField with uid="QMRKNBACQP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio selezione",;
    HelpContextID = 112982330,;
    FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=24, Left=396, Top=15, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oMININI_1_17.mHide()
    with this.Parent.oContained
      return (empty(.w_DATINI))
    endwith
  endfunc

  func oMININI_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_19 as StdField with uid="VRHGJGBHQK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 34513354,;
    FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=66, Left=460, Top=15

  func oDATFIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oOREFIN_1_20 as StdField with uid="VKJCJSQZFK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_OREFIN", cQueryName = "OREFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di fine selezione",;
    HelpContextID = 34570266,;
    FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=24, Left=600, Top=15, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREFIN_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oOREFIN_1_20.mHide()
    with this.Parent.oContained
      return (empty(.w_DATFIN))
    endwith
  endfunc

  func oOREFIN_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_21 as StdField with uid="GIWPXXGMLC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di fine selezione",;
    HelpContextID = 34535738,;
    FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=24, Left=637, Top=15, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oMINFIN_1_21.mHide()
    with this.Parent.oContained
      return (empty(.w_DATFIN))
    endwith
  endfunc

  func oMINFIN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc


  add object oBtn_1_26 as StdButton with uid="ODQLJUAFBO",left=1, top=10, width=23,height=25,;
    CpPicture="..\exe\bmp\legenda_colori.bmp", caption="", nPag=1;
    , ToolTipText = "Legenda colori e immagini";
    , HelpContextID = 161368906;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      do gsag_kle with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_33 as StdButton with uid="PHEWMSSLEL",left=24, top=10, width=23,height=25,;
    CpPicture="tzoom.bmp", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 173967594;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        gsag_bdp(this.Parent.oContained,"AGGIORNASCHEDULE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_34 as StdButton with uid="BQJHHOQZPU",left=47, top=10, width=76,height=25,;
    caption="Oggi", nPag=1;
    , ToolTipText = "Premere per impostare la data odierna";
    , HelpContextID = 193311974;
    , Forecolor=15181930, caption='\<Oggi';
    , FontName = "MS Sans Serif", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      this.parent.oContained.NotifyEvent("ImpostaDataOdierna")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_35 as StdButton with uid="GEEKRQNQOI",left=146, top=10, width=23,height=25,;
    CpPicture="bmp\frecciadx_small.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiungere un giorno";
    , HelpContextID = 185981174;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      with this.Parent.oContained
        gsag_bdp(this.Parent.oContained,"+1")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DatIni=.w_DatFin)
      endwith
    endif
  endfunc

  func oBtn_1_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_DatIni) OR !.w_DatIni=.w_DatFin OR empty(.w_DatFin))
     endwith
    endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="NBMULPSDQD",left=123, top=10, width=23,height=25,;
    CpPicture="bmp\frecciasn_small.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per  sottrarre un giorno";
    , HelpContextID = 185981174;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        gsag_bdp(this.Parent.oContained,"-1")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DatIni=.w_DatFin)
      endwith
    endif
  endfunc

  func oBtn_1_38.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_DatIni) OR !.w_DatIni=.w_DatFin OR empty(.w_DatFin))
     endwith
    endif
  endfunc


  add object ctList_Risorse as cp_ctList with uid="UEPYTIHUYG",left=587, top=255, width=223,height=118,;
    caption='ctList_Risorse',;
    FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    FontSize=8,HeaderFillType=0,FontName="MsSansSerif",HeadrText="",ItemSel="",ColumnSel="",serial="w_DipCodice",HeaderBorder=0,VertGridLines=.f.,;
    nPag=1;
    , HelpContextID = 56458504

  add object oStr_1_14 as StdString with uid="KLEZMLKSOB",Visible=.t., Left=186, Top=18,;
    Alignment=1, Width=30, Height=16,;
    Caption="Da:"  ;
  ,  FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="JAHWAWKLCQ",Visible=.t., Left=438, Top=18,;
    Alignment=1, Width=18, Height=16,;
    Caption="A:"  ;
  ,  FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="EWZSPAOTVW",Visible=.t., Left=298, Top=18,;
    Alignment=1, Width=55, Height=16,;
    Caption="Alle ore:"  ;
  ,  FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (empty(.w_DATINI))
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="CTAHWBKCXZ",Visible=.t., Left=537, Top=18,;
    Alignment=1, Width=57, Height=16,;
    Caption="Alle ore:"  ;
  ,  FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (empty(.w_DATFIN))
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="DAAABZBARA",Visible=.t., Left=385, Top=18,;
    Alignment=1, Width=7, Height=16,;
    Caption=":"  ;
  ,  FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (empty(.w_DATINI))
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="BTYOAEACQT",Visible=.t., Left=626, Top=18,;
    Alignment=1, Width=7, Height=16,;
    Caption=":"  ;
  ,  FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (empty(.w_DATFIN))
    endwith
  endfunc
enddefine
define class tgsag_kdpPag2 as StdContainer
  Width  = 809
  height = 373
  stdWidth  = 809
  stdheight = 373
  resizeXpos=657
  bGlobalFont=.f.
  FontName      = "MS Sans Serif"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATTSTU_2_1 as StdCheck with uid="FUHPBAIQSC",rtseq=15,rtrep=.f.,left=26, top=23, caption="Appuntamenti",;
    ToolTipText = "Se attivo, visualizza gli appuntamenti",;
    HelpContextID = 173117178,;
    cFormVar="w_ATTSTU", bObbl = .f. , nPag = 2;
   , FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oATTSTU_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oATTSTU_2_1.GetRadio()
    this.Parent.oContained.w_ATTSTU = this.RadioValue()
    return .t.
  endfunc

  func oATTSTU_2_1.SetRadio()
    this.Parent.oContained.w_ATTSTU=trim(this.Parent.oContained.w_ATTSTU)
    this.value = ;
      iif(this.Parent.oContained.w_ATTSTU=='S',1,;
      0)
  endfunc

  add object oSESSTEL_2_2 as StdCheck with uid="KYMNVXZYIW",rtseq=16,rtrep=.f.,left=26, top=46, caption="Sessioni telefoniche",;
    ToolTipText = "Se attivo, visualizza le sessioni telefoniche",;
    HelpContextID = 95310630,;
    cFormVar="w_SESSTEL", bObbl = .f. , nPag = 2;
   , FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oSESSTEL_2_2.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oSESSTEL_2_2.GetRadio()
    this.Parent.oContained.w_SESSTEL = this.RadioValue()
    return .t.
  endfunc

  func oSESSTEL_2_2.SetRadio()
    this.Parent.oContained.w_SESSTEL=trim(this.Parent.oContained.w_SESSTEL)
    this.value = ;
      iif(this.Parent.oContained.w_SESSTEL=='T',1,;
      0)
  endfunc

  add object oASSENZE_2_3 as StdCheck with uid="RPXCBGRCCL",rtseq=17,rtrep=.f.,left=200, top=23, caption="Assenze",;
    ToolTipText = "Se attivo, visualizza le assenze",;
    HelpContextID = 171991046,;
    cFormVar="w_ASSENZE", bObbl = .f. , nPag = 2;
   , FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oASSENZE_2_3.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oASSENZE_2_3.GetRadio()
    this.Parent.oContained.w_ASSENZE = this.RadioValue()
    return .t.
  endfunc

  func oASSENZE_2_3.SetRadio()
    this.Parent.oContained.w_ASSENZE=trim(this.Parent.oContained.w_ASSENZE)
    this.value = ;
      iif(this.Parent.oContained.w_ASSENZE=='A',1,;
      0)
  endfunc

  add object oATTGENER_2_4 as StdCheck with uid="KLBNRLNCJC",rtseq=18,rtrep=.f.,left=200, top=46, caption="Attivit� generiche",;
    ToolTipText = "Se attivo, visualizza le attivit� generiche",;
    HelpContextID = 229798232,;
    cFormVar="w_ATTGENER", bObbl = .f. , nPag = 2;
   , FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oATTGENER_2_4.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oATTGENER_2_4.GetRadio()
    this.Parent.oContained.w_ATTGENER = this.RadioValue()
    return .t.
  endfunc

  func oATTGENER_2_4.SetRadio()
    this.Parent.oContained.w_ATTGENER=trim(this.Parent.oContained.w_ATTGENER)
    this.value = ;
      iif(this.Parent.oContained.w_ATTGENER=='G',1,;
      0)
  endfunc

  add object oMEMO_2_5 as StdCheck with uid="ELRSHFYRWO",rtseq=19,rtrep=.f.,left=374, top=23, caption="Note",;
    ToolTipText = "Se attivo, visualizza le note",;
    HelpContextID = 191492806,;
    cFormVar="w_MEMO", bObbl = .f. , nPag = 2;
   , FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oMEMO_2_5.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oMEMO_2_5.GetRadio()
    this.Parent.oContained.w_MEMO = this.RadioValue()
    return .t.
  endfunc

  func oMEMO_2_5.SetRadio()
    this.Parent.oContained.w_MEMO=trim(this.Parent.oContained.w_MEMO)
    this.value = ;
      iif(this.Parent.oContained.w_MEMO=='M',1,;
      0)
  endfunc

  add object oDAFARE_2_6 as StdCheck with uid="JYTLCSHGGN",rtseq=20,rtrep=.f.,left=374, top=46, caption="Cose da fare",;
    ToolTipText = "Se attivo, visualizza le cose da fare",;
    HelpContextID = 176456138,;
    cFormVar="w_DAFARE", bObbl = .f. , nPag = 2;
   , FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDAFARE_2_6.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oDAFARE_2_6.GetRadio()
    this.Parent.oContained.w_DAFARE = this.RadioValue()
    return .t.
  endfunc

  func oDAFARE_2_6.SetRadio()
    this.Parent.oContained.w_DAFARE=trim(this.Parent.oContained.w_DAFARE)
    this.value = ;
      iif(this.Parent.oContained.w_DAFARE=='D',1,;
      0)
  endfunc

  add object oUDIENZE_2_7 as StdCheck with uid="HBQKVBXHMX",rtseq=21,rtrep=.f.,left=528, top=23, caption="Udienze",;
    ToolTipText = "Se attivo, visualizza le udienze",;
    HelpContextID = 171946566,;
    cFormVar="w_UDIENZE", bObbl = .f. , nPag = 2;
   , FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oUDIENZE_2_7.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oUDIENZE_2_7.GetRadio()
    this.Parent.oContained.w_UDIENZE = this.RadioValue()
    return .t.
  endfunc

  func oUDIENZE_2_7.SetRadio()
    this.Parent.oContained.w_UDIENZE=trim(this.Parent.oContained.w_UDIENZE)
    this.value = ;
      iif(this.Parent.oContained.w_UDIENZE=='U',1,;
      0)
  endfunc

  func oUDIENZE_2_7.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oDAINSPRE_2_8 as StdCheck with uid="AMMYJSOHVW",rtseq=22,rtrep=.f.,left=528, top=46, caption="Da inserimento prestazioni",;
    ToolTipText = "Se attivo: visualizza le attivit� generate dall'inserimento delle prestazioni",;
    HelpContextID = 258429317,;
    cFormVar="w_DAINSPRE", bObbl = .f. , nPag = 2;
   , FontName = "MS Sans Serif", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oDAINSPRE_2_8.RadioValue()
    return(iif(this.value =1,'Z',;
    ' '))
  endfunc
  func oDAINSPRE_2_8.GetRadio()
    this.Parent.oContained.w_DAINSPRE = this.RadioValue()
    return .t.
  endfunc

  func oDAINSPRE_2_8.SetRadio()
    this.Parent.oContained.w_DAINSPRE=trim(this.Parent.oContained.w_DAINSPRE)
    this.value = ;
      iif(this.Parent.oContained.w_DAINSPRE=='Z',1,;
      0)
  endfunc


  add object oBtn_2_10 as StdButton with uid="UJEFIAGLLR",left=706, top=27, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=2;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 173967594;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_10.Click()
      with this.Parent.oContained
        gsag_bdp(this.Parent.oContained,"AGGIORNASCHEDULE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_11 as StdButton with uid="MBXAIGGBZE",left=706, top=89, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per effettuare la stampa della visualizzazione corrente";
    , HelpContextID = 209100250;
    , caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      with this.Parent.oContained
        GSAG_BDP(this.Parent.oContained,"STAMPA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kdp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
