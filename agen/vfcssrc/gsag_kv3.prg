* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kv3                                                        *
*              Abbina/Disabbina elementi contratto                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-17                                                      *
* Last revis.: 2015-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kv3",oParentObject))

* --- Class definition
define class tgsag_kv3 as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 789
  Height = 554
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-01"
  HelpContextID=48899945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=40

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  DES_DIVE_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  CON_TRAS_IDX = 0
  MOD_ELEM_IDX = 0
  OFF_NOMI_IDX = 0
  cPrg = "gsag_kv3"
  cComment = "Abbina/Disabbina elementi contratto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLTYPEOP = space(1)
  w_TIPCON = space(1)
  w_CODMOD = space(10)
  w_CODICE = space(15)
  o_CODICE = space(15)
  w_CODCLI = space(15)
  w_CODNOM = space(15)
  o_CODNOM = space(15)
  w_DESCRI = space(60)
  w_CODCONTR = space(10)
  w_IMPIANTO = space(10)
  o_IMPIANTO = space(10)
  w_COMPIMP = space(50)
  o_COMPIMP = space(50)
  w_OLDCOMP = space(10)
  w_COMPIMPKEY = 0
  o_COMPIMPKEY = 0
  w_COMPIMPKEYREAD = 0
  w_IMMODATR = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_ATCODNOM = space(15)
  w_ATCODSED = space(5)
  w_ELCONTRA = space(10)
  w_ELCODMOD = space(10)
  w_CODESCON = space(50)
  w_ELCODIMP = space(10)
  w_ELCODCOM = 0
  w_ELCODCLI = space(15)
  w_MODESCRI = space(50)
  w_ANDESCRI = space(50)
  w_CODESCON_ZOOM = space(50)
  w_IMDESCRI_ZOOM = space(50)
  w_IMDESCON = space(50)
  w_DESMODELLO = space(60)
  w_CODSED = space(5)
  w_ELRINNOV = 0
  w_MOTIPCON = space(1)
  w_IMPIANTO = space(10)
  w_IMCODCON = space(15)
  w_COMPIMP = space(50)
  w_IMDESCRI = space(50)
  w_IMDESCRI = space(50)
  w_IMPIANTOSEL = space(10)
  w_ELEMENTI = space(1)
  w_OGGETTO = space(10)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kv3Pag1","gsag_kv3",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODMOD_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsag_kv3
    If oParentObject='A'
       this.parent.cComment=ah_msgformat("Abbina elementi contratto")
    else
       this.parent.cComment=ah_msgformat("Disabbina elementi contratto")
       Local nCtrl
       For nCtrl=1 to this.page1.Controls(1).ControlCount
       If UPPER(this.page1.Controls(1).Controls(nCtrl).Class)=='STDBOX'
         this.page1.Controls(1).Controls(nCtrl).Visible=.F.
       EndIf
    Next
    Endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DES_DIVE'
    this.cWorkTables[3]='IMP_MAST'
    this.cWorkTables[4]='IMP_DETT'
    this.cWorkTables[5]='CON_TRAS'
    this.cWorkTables[6]='MOD_ELEM'
    this.cWorkTables[7]='OFF_NOMI'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLTYPEOP=space(1)
      .w_TIPCON=space(1)
      .w_CODMOD=space(10)
      .w_CODICE=space(15)
      .w_CODCLI=space(15)
      .w_CODNOM=space(15)
      .w_DESCRI=space(60)
      .w_CODCONTR=space(10)
      .w_IMPIANTO=space(10)
      .w_COMPIMP=space(50)
      .w_OLDCOMP=space(10)
      .w_COMPIMPKEY=0
      .w_COMPIMPKEYREAD=0
      .w_IMMODATR=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_ATCODNOM=space(15)
      .w_ATCODSED=space(5)
      .w_ELCONTRA=space(10)
      .w_ELCODMOD=space(10)
      .w_CODESCON=space(50)
      .w_ELCODIMP=space(10)
      .w_ELCODCOM=0
      .w_ELCODCLI=space(15)
      .w_MODESCRI=space(50)
      .w_ANDESCRI=space(50)
      .w_CODESCON_ZOOM=space(50)
      .w_IMDESCRI_ZOOM=space(50)
      .w_IMDESCON=space(50)
      .w_DESMODELLO=space(60)
      .w_CODSED=space(5)
      .w_ELRINNOV=0
      .w_MOTIPCON=space(1)
      .w_IMPIANTO=space(10)
      .w_IMCODCON=space(15)
      .w_COMPIMP=space(50)
      .w_IMDESCRI=space(50)
      .w_IMDESCRI=space(50)
      .w_IMPIANTOSEL=space(10)
      .w_ELEMENTI=space(1)
      .w_OGGETTO=space(10)
        .w_FLTYPEOP = this.oParentObject
        .w_TIPCON = 'C'
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODMOD))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODICE))
          .link_1_4('Full')
        endif
        .w_CODCLI = .w_CODICE
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODCLI))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,8,.f.)
        if not(empty(.w_CODCONTR))
          .link_1_9('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_IMPIANTO))
          .link_1_10('Full')
        endif
          .DoRTCalc(10,11,.f.)
        .w_COMPIMPKEY = 0
        .w_COMPIMPKEYREAD = .w_COMPIMPKEY
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_COMPIMPKEYREAD))
          .link_1_16('Full')
        endif
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
          .DoRTCalc(14,14,.f.)
        .w_OBTEST = i_DATSYS
        .w_ATCODNOM = .w_CODNOM
          .DoRTCalc(17,17,.f.)
        .w_ELCONTRA = .w_ZOOM.GetVar("ELCONTRA")
        .w_ELCODMOD = .w_ZOOM.GetVar("ELCODMOD")
          .DoRTCalc(20,20,.f.)
        .w_ELCODIMP = .w_ZOOM.GetVar("ELCODIMP")
        .w_ELCODCOM = .w_ZOOM.GetVar("ELCODCOM")
        .w_ELCODCLI = .w_CODNOM
        .w_MODESCRI = .w_ZOOM.GETVAR("MODESCRI")
        .w_ANDESCRI = .w_ZOOM.GETVAR("ANDESCRI")
        .w_CODESCON_ZOOM = .w_ZOOM.GETVAR("CODESCON")
        .w_IMDESCRI_ZOOM = .w_ZOOM.GETVAR("IMDESCRI")
        .w_IMDESCON = .w_ZOOM.GETVAR("IMDESCON")
          .DoRTCalc(29,29,.f.)
        .w_CODSED = SPACE(5)
        .w_ELRINNOV = .w_ZOOM.GetVar("ELRINNOV")
        .DoRTCalc(32,33,.f.)
        if not(empty(.w_IMPIANTO))
          .link_1_54('Full')
        endif
          .DoRTCalc(34,37,.f.)
        .w_IMPIANTOSEL = .w_IMPIANTO
        .w_ELEMENTI = "A"
    endwith
    this.DoRTCalc(40,40,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_50.enabled = this.oPgFrm.Page1.oPag.oBtn_1_50.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_69.enabled = this.oPgFrm.Page1.oPag.oBtn_1_69.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CODICE<>.w_CODICE
            .w_CODCLI = .w_CODICE
          .link_1_5('Full')
        endif
        .DoRTCalc(6,12,.t.)
        if .o_IMPIANTO<>.w_IMPIANTO.or. .o_COMPIMPKEY<>.w_COMPIMPKEY
            .w_COMPIMPKEYREAD = .w_COMPIMPKEY
          .link_1_16('Full')
        endif
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .DoRTCalc(14,15,.t.)
            .w_ATCODNOM = .w_CODNOM
        .DoRTCalc(17,17,.t.)
            .w_ELCONTRA = .w_ZOOM.GetVar("ELCONTRA")
            .w_ELCODMOD = .w_ZOOM.GetVar("ELCODMOD")
        .DoRTCalc(20,20,.t.)
            .w_ELCODIMP = .w_ZOOM.GetVar("ELCODIMP")
            .w_ELCODCOM = .w_ZOOM.GetVar("ELCODCOM")
            .w_ELCODCLI = .w_CODNOM
            .w_MODESCRI = .w_ZOOM.GETVAR("MODESCRI")
            .w_ANDESCRI = .w_ZOOM.GETVAR("ANDESCRI")
            .w_CODESCON_ZOOM = .w_ZOOM.GETVAR("CODESCON")
            .w_IMDESCRI_ZOOM = .w_ZOOM.GETVAR("IMDESCRI")
            .w_IMDESCON = .w_ZOOM.GETVAR("IMDESCON")
        .DoRTCalc(29,29,.t.)
        if .o_CODNOM<>.w_CODNOM
            .w_CODSED = SPACE(5)
        endif
            .w_ELRINNOV = .w_ZOOM.GetVar("ELRINNOV")
        .DoRTCalc(32,37,.t.)
            .w_IMPIANTOSEL = .w_IMPIANTO
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(39,40,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
    endwith
  return

  proc Calculate_BWBOIOUNMQ()
    with this
          * --- Sbianca contratto e impianto
          .w_CODCONTR = SPACE( 15 )
          .w_CODESCON = SPACE( 50 )
    endwith
  endproc
  proc Calculate_OBHMFWHSCD()
    with this
          * --- LINKIMP
          .w_IMPIANTO = .w_IMPIANTO
          .link_1_10('Full')
          .link_1_54('Full')
    endwith
  endproc
  proc Calculate_NQSWRMYRDG()
    with this
          * --- LINKCOMP
          .w_COMPIMPKEYREAD = .w_COMPIMPKEYREAD
          .link_1_16('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCOMPIMP_1_12.enabled = this.oPgFrm.Page1.oPag.oCOMPIMP_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCOMPIMP_1_57.enabled = this.oPgFrm.Page1.oPag.oCOMPIMP_1_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oIMPIANTO_1_10.visible=!this.oPgFrm.Page1.oPag.oIMPIANTO_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCOMPIMP_1_12.visible=!this.oPgFrm.Page1.oPag.oCOMPIMP_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_15.visible=!this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oIMDESCRI_ZOOM_1_38.visible=!this.oPgFrm.Page1.oPag.oIMDESCRI_ZOOM_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oIMDESCON_1_40.visible=!this.oPgFrm.Page1.oPag.oIMDESCON_1_40.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_43.visible=!this.oPgFrm.Page1.oPag.oBtn_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_50.visible=!this.oPgFrm.Page1.oPag.oBtn_1_50.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_51.visible=!this.oPgFrm.Page1.oPag.oBtn_1_51.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_52.visible=!this.oPgFrm.Page1.oPag.oBtn_1_52.mHide()
    this.oPgFrm.Page1.oPag.oIMPIANTO_1_54.visible=!this.oPgFrm.Page1.oPag.oIMPIANTO_1_54.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_55.visible=!this.oPgFrm.Page1.oPag.oBtn_1_55.mHide()
    this.oPgFrm.Page1.oPag.oCOMPIMP_1_57.visible=!this.oPgFrm.Page1.oPag.oCOMPIMP_1_57.mHide()
    this.oPgFrm.Page1.oPag.oIMDESCRI_1_58.visible=!this.oPgFrm.Page1.oPag.oIMDESCRI_1_58.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_59.visible=!this.oPgFrm.Page1.oPag.oBtn_1_59.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_60.visible=!this.oPgFrm.Page1.oPag.oBtn_1_60.mHide()
    this.oPgFrm.Page1.oPag.oIMDESCRI_1_61.visible=!this.oPgFrm.Page1.oPag.oIMDESCRI_1_61.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_63.visible=!this.oPgFrm.Page1.oPag.oBtn_1_63.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_kv3
    iF CEVENT='Done' and Type('this.w_OGGETTO')='O'
       This.w_OGGETTO.Notifyevent('SearchT')
       This.w_OGGETTO.Notifyevent('SearchR')
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_CODICE Changed")
          .Calculate_BWBOIOUNMQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("LINKIMP")
          .Calculate_OBHMFWHSCD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("LINKCOMP")
          .Calculate_NQSWRMYRDG()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kv3
    if lower(cEvent)=='w_zoom selected'
      opengest("A","gsag_ael","ELCODMOD", this.w_ELCODMOD,"ELCONTRA", this.w_ELCONTRA,"ELCODIMP", this.w_ELCODIMP,"ELCODCOM", this.w_ELCODCOM,"ELRINNOV", this.w_ELRINNOV)
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMOD
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MOD_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_CODMOD)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_CODMOD))
          select MOCODICE,MODESCRI,MOTIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMOD)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMOD) and !this.bDontReportError
            deferred_cp_zoom('MOD_ELEM','*','MOCODICE',cp_AbsName(oSource.parent,'oCODMOD_1_3'),i_cWhere,'',"Modelli elementi",'GSAG_KV3.MOD_ELEM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI,MOTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_CODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_CODMOD)
            select MOCODICE,MODESCRI,MOTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMOD = NVL(_Link_.MOCODICE,space(10))
      this.w_DESMODELLO = NVL(_Link_.MODESCRI,space(60))
      this.w_MOTIPCON = NVL(_Link_.MOTIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODMOD = space(10)
      endif
      this.w_DESMODELLO = space(60)
      this.w_MOTIPCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODICE))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODICE_1_4'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODICE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODICE)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(15)
      endif
      this.w_DESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOTIPCLI,NOCODCLI,NOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODCLI="+cp_ToStrODBC(this.w_CODCLI);
                   +" and NOTIPCLI="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOTIPCLI',this.w_TIPCON;
                       ,'NOCODCLI',this.w_CODCLI)
            select NOTIPCLI,NOCODCLI,NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.NOCODCLI,space(15))
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_CODNOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOTIPCLI,1)+'\'+cp_ToStr(_Link_.NOCODCLI,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCONTR
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCONTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CON_TRAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COSERIAL like "+cp_ToStrODBC(trim(this.w_CODCONTR)+"%");

          i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COSERIAL',trim(this.w_CODCONTR))
          select COSERIAL,CODESCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCONTR)==trim(_Link_.COSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCONTR) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAS','*','COSERIAL',cp_AbsName(oSource.parent,'oCODCONTR_1_9'),i_cWhere,'',"Contratti d'assistenza",'GSAG_AEL.CON_TRAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                     +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',oSource.xKey(1))
            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCONTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_CODCONTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_CODCONTR)
            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCONTR = NVL(_Link_.COSERIAL,space(10))
      this.w_CODESCON = NVL(_Link_.CODESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODCONTR = space(10)
      endif
      this.w_CODESCON = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCONTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMPIANTO
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMPIANTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIM',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_IMPIANTO)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_IMPIANTO))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMPIANTO)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IMPIANTO) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oIMPIANTO_1_10'),i_cWhere,'GSAG_MIM',"Impianto",'GSAG_AE3.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMPIANTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMPIANTO = NVL(_Link_.IMCODICE,space(10))
      this.w_IMDESCRI = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IMPIANTO = space(10)
      endif
      this.w_IMDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMPIANTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPIMPKEYREAD
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPIMPKEYREAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPIMPKEYREAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_COMPIMPKEYREAD);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO;
                       ,'CPROWNUM',this.w_COMPIMPKEYREAD)
            select IMCODICE,CPROWNUM,IMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPIMPKEYREAD = NVL(_Link_.CPROWNUM,0)
      this.w_COMPIMP = NVL(_Link_.IMDESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_COMPIMPKEYREAD = 0
      endif
      this.w_COMPIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPIMPKEYREAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMPIANTO
  func Link_1_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMPIANTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIM',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_IMPIANTO)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI,IMCODCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_IMPIANTO))
          select IMCODICE,IMDESCRI,IMCODCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMPIANTO)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IMPIANTO) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oIMPIANTO_1_54'),i_cWhere,'GSAG_MIM',"Impianto",'GSAG_AE3.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI,IMCODCON";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI,IMCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMPIANTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI,IMCODCON";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO)
            select IMCODICE,IMDESCRI,IMCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMPIANTO = NVL(_Link_.IMCODICE,space(10))
      this.w_IMDESCRI = NVL(_Link_.IMDESCRI,space(50))
      this.w_IMCODCON = NVL(_Link_.IMCODCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_IMPIANTO = space(10)
      endif
      this.w_IMDESCRI = space(50)
      this.w_IMCODCON = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMPIANTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODMOD_1_3.value==this.w_CODMOD)
      this.oPgFrm.Page1.oPag.oCODMOD_1_3.value=this.w_CODMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_4.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_4.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_8.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_8.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCONTR_1_9.value==this.w_CODCONTR)
      this.oPgFrm.Page1.oPag.oCODCONTR_1_9.value=this.w_CODCONTR
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPIANTO_1_10.value==this.w_IMPIANTO)
      this.oPgFrm.Page1.oPag.oIMPIANTO_1_10.value=this.w_IMPIANTO
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPIMP_1_12.value==this.w_COMPIMP)
      this.oPgFrm.Page1.oPag.oCOMPIMP_1_12.value=this.w_COMPIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_15.value==this.w_COMPIMPKEY)
      this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_15.value=this.w_COMPIMPKEY
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_1_25.value==this.w_CODESCON)
      this.oPgFrm.Page1.oPag.oCODESCON_1_25.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESCRI_1_33.value==this.w_MODESCRI)
      this.oPgFrm.Page1.oPag.oMODESCRI_1_33.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_34.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_34.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_ZOOM_1_37.value==this.w_CODESCON_ZOOM)
      this.oPgFrm.Page1.oPag.oCODESCON_ZOOM_1_37.value=this.w_CODESCON_ZOOM
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCRI_ZOOM_1_38.value==this.w_IMDESCRI_ZOOM)
      this.oPgFrm.Page1.oPag.oIMDESCRI_ZOOM_1_38.value=this.w_IMDESCRI_ZOOM
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCON_1_40.value==this.w_IMDESCON)
      this.oPgFrm.Page1.oPag.oIMDESCON_1_40.value=this.w_IMDESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMODELLO_1_42.value==this.w_DESMODELLO)
      this.oPgFrm.Page1.oPag.oDESMODELLO_1_42.value=this.w_DESMODELLO
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPIANTO_1_54.value==this.w_IMPIANTO)
      this.oPgFrm.Page1.oPag.oIMPIANTO_1_54.value=this.w_IMPIANTO
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPIMP_1_57.value==this.w_COMPIMP)
      this.oPgFrm.Page1.oPag.oCOMPIMP_1_57.value=this.w_COMPIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCRI_1_58.value==this.w_IMDESCRI)
      this.oPgFrm.Page1.oPag.oIMDESCRI_1_58.value=this.w_IMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCRI_1_61.value==this.w_IMDESCRI)
      this.oPgFrm.Page1.oPag.oIMDESCRI_1_61.value=this.w_IMDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODICE = this.w_CODICE
    this.o_CODNOM = this.w_CODNOM
    this.o_IMPIANTO = this.w_IMPIANTO
    this.o_COMPIMP = this.w_COMPIMP
    this.o_COMPIMPKEY = this.w_COMPIMPKEY
    return

enddefine

* --- Define pages as container
define class tgsag_kv3Pag1 as StdContainer
  Width  = 785
  height = 554
  stdWidth  = 785
  stdheight = 554
  resizeXpos=516
  resizeYpos=364
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODMOD_1_3 as StdField with uid="MERUHLPFQO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODMOD", cQueryName = "CODMOD",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Modello elementi",;
    HelpContextID = 162042842,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=112, Top=7, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_ELEM", oKey_1_1="MOCODICE", oKey_1_2="this.w_CODMOD"

  func oCODMOD_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMOD_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMOD_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_ELEM','*','MOCODICE',cp_AbsName(this.parent,'oCODMOD_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Modelli elementi",'GSAG_KV3.MOD_ELEM_VZM',this.parent.oContained
  endproc

  add object oCODICE_1_4 as StdField with uid="QGNANWIGWY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Intestatario dell'elemento contratto",;
    HelpContextID = 158110682,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=112, Top=33, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODICE"

  func oCODICE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODICE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCODICE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODICE
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_8 as StdField with uid="JUZHYVZVMZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 75607498,;
   bGlobalFont=.t.,;
    Height=21, Width=473, Left=234, Top=33, InputMask=replicate('X',60)

  add object oCODCONTR_1_9 as StdField with uid="EOIIVYIRNT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODCONTR", cQueryName = "CODCONTR",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Contratto",;
    HelpContextID = 5074040,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=112, Top=59, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CON_TRAS", oKey_1_1="COSERIAL", oKey_1_2="this.w_CODCONTR"

  func oCODCONTR_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCONTR_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCONTR_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CON_TRAS','*','COSERIAL',cp_AbsName(this.parent,'oCODCONTR_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Contratti d'assistenza",'GSAG_AEL.CON_TRAS_VZM',this.parent.oContained
  endproc

  add object oIMPIANTO_1_10 as StdField with uid="NSXUUBVBOI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_IMPIANTO", cQueryName = "IMPIANTO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Impianto",;
    HelpContextID = 259271381,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=112, Top=84, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", cZoomOnZoom="GSAG_MIM", oKey_1_1="IMCODICE", oKey_1_2="this.w_IMPIANTO"

  func oIMPIANTO_1_10.mHide()
    with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='A')
    endwith
  endfunc

  func oIMPIANTO_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
      if .not. empty(.w_COMPIMPKEYREAD)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIMPIANTO_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMPIANTO_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oIMPIANTO_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIM',"Impianto",'GSAG_AE3.IMP_MAST_VZM',this.parent.oContained
  endproc
  proc oIMPIANTO_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_IMPIANTO
     i_obj.ecpSave()
  endproc

  add object oCOMPIMP_1_12 as StdField with uid="GOVKVGTSZB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_COMPIMP", cQueryName = "COMPIMP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Componente dell'impianto",;
    HelpContextID = 17105882,;
   bGlobalFont=.t.,;
    Height=21, Width=470, Left=112, Top=109, InputMask=replicate('X',50), bHasZoom = .t. 

  func oCOMPIMP_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_IMPIANTO))
    endwith
   endif
  endfunc

  func oCOMPIMP_1_12.mHide()
    with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='A')
    endwith
  endfunc

  proc oCOMPIMP_1_12.mBefore
    with this.Parent.oContained
      .w_OLDCOMP=.w_COMPIMP
    endwith
  endproc

  proc oCOMPIMP_1_12.mAfter
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M")
      endwith
  endproc

  proc oCOMPIMP_1_12.mZoom
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M",.T.)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCOMPIMPKEY_1_15 as StdField with uid="SBSCAGJTGI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COMPIMPKEY", cQueryName = "COMPIMPKEY",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 17081919,;
   bGlobalFont=.t.,;
    Height=22, Width=124, Left=12, Top=20

  func oCOMPIMPKEY_1_15.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc


  add object oBtn_1_18 as StdButton with uid="PPWFAOCXOY",left=722, top=86, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la ricerca";
    , HelpContextID = 128022294;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      this.parent.oContained.NotifyEvent("Search")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOM as cp_szoombox with uid="VZMOVSYNOQ",left=3, top=134, width=774,height=281,;
    caption='ZOOM',;
   bGlobalFont=.t.,;
    cTable="ELE_CONT",bRetriveAllRows=.t.,cZoomFile="gsag_kv3",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Search",;
    nPag=1;
    , HelpContextID = 43508330

  add object oCODESCON_1_25 as StdField with uid="JIKTIXDDXZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 175149964,;
   bGlobalFont=.t.,;
    Height=21, Width=494, Left=213, Top=59, InputMask=replicate('X',50)

  add object oMODESCRI_1_33 as StdField with uid="YUTQSCCACI",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 93285647,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=297, Top=419, InputMask=replicate('X',50)

  add object oANDESCRI_1_34 as StdField with uid="ACNTZAUZOM",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 93285199,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=297, Top=441, InputMask=replicate('X',50)

  add object oCODESCON_ZOOM_1_37 as StdField with uid="EWFOXIKFWQ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODESCON_ZOOM", cQueryName = "CODESCON_ZOOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 88884124,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=297, Top=463, InputMask=replicate('X',50)

  add object oIMDESCRI_ZOOM_1_38 as StdField with uid="RWUOGMWYUL",rtseq=27,rtrep=.f.,;
    cFormVar = "w_IMDESCRI_ZOOM", cQueryName = "IMDESCRI_ZOOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 179550911,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=297, Top=485, InputMask=replicate('X',50)

  func oIMDESCRI_ZOOM_1_38.mHide()
    with this.Parent.oContained
      return (.w_FLTYPEOP='A')
    endwith
  endfunc

  add object oIMDESCON_1_40 as StdField with uid="FYHRCQDSWS",rtseq=28,rtrep=.f.,;
    cFormVar = "w_IMDESCON", cQueryName = "IMDESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 175150380,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=297, Top=507, InputMask=replicate('X',50)

  func oIMDESCON_1_40.mHide()
    with this.Parent.oContained
      return (.w_FLTYPEOP='A')
    endwith
  endfunc

  add object oDESMODELLO_1_42 as StdField with uid="ZYEUQNAHCA",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESMODELLO", cQueryName = "DESMODELLO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 106473026,;
   bGlobalFont=.t.,;
    Height=21, Width=473, Left=234, Top=7, InputMask=replicate('X',60)


  add object oBtn_1_43 as StdButton with uid="JBFSHNCMPX",left=27, top=428, width=23,height=23,;
    CpPicture="bmp\Check_small.bmp", caption="", nPag=1;
    , ToolTipText = "Seleziona tutti i nominativi";
    , HelpContextID = 48899850;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        GSAG_BDU(this.Parent.oContained,"SELEZ_NOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_43.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_50 as StdButton with uid="BGWPKFENTZ",left=13, top=420, width=48,height=45,;
    CpPicture="bmp\Check.bmp", caption="", nPag=1;
    , ToolTipText = "Seleziona tutti i contratti";
    , HelpContextID = 48899850;
    , Caption='\<Sel. tutti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_50.Click()
      with this.Parent.oContained
        GSAG_BV3(this.Parent.oContained,"SELEZ_ALL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_50.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_51 as StdButton with uid="DBNWMIPODI",left=65, top=420, width=48,height=45,;
    CpPicture="bmp\UnCheck.bmp", caption="", nPag=1;
    , ToolTipText = "Deseleziona tutti i contratti";
    , HelpContextID = 48899850;
    , Caption='\<Desel. tutti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_51.Click()
      with this.Parent.oContained
        GSAG_BV3(this.Parent.oContained,"DESEL_ALL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_51.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_52 as StdButton with uid="LIGYBDAVXB",left=117, top=420, width=48,height=45,;
    CpPicture="bmp\InvCheck.bmp", caption="", nPag=1;
    , ToolTipText = "Inverte la selezione dei contratti";
    , HelpContextID = 48899850;
    , Caption='\<Inv. selez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_52.Click()
      with this.Parent.oContained
        GSAG_BV3(this.Parent.oContained,"INV_SELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_52.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc

  add object oIMPIANTO_1_54 as StdField with uid="WOZHYLQBZU",rtseq=33,rtrep=.f.,;
    cFormVar = "w_IMPIANTO", cQueryName = "IMPIANTO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Impianto",;
    HelpContextID = 259271381,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=96, Top=496, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", cZoomOnZoom="GSAG_MIM", oKey_1_1="IMCODICE", oKey_1_2="this.w_IMPIANTO"

  func oIMPIANTO_1_54.mHide()
    with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='D')
    endwith
  endfunc

  func oIMPIANTO_1_54.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_54('Part',this)
      if .not. empty(.w_COMPIMPKEYREAD)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIMPIANTO_1_54.ecpDrop(oSource)
    this.Parent.oContained.link_1_54('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMPIANTO_1_54.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oIMPIANTO_1_54'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIM',"Impianto",'GSAG_AE3.IMP_MAST_VZM',this.parent.oContained
  endproc
  proc oIMPIANTO_1_54.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_IMPIANTO
     i_obj.ecpSave()
  endproc


  add object oBtn_1_55 as StdButton with uid="MNVZAMDXTB",left=196, top=497, width=22,height=21,;
    CpPicture="bmp\ZOOM.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per ricercare per attributo";
    , HelpContextID = 48698922;
    , IMGSIZE=16;
  , bGlobalFont=.t.

    proc oBtn_1_55.Click()
      do gsag_kir with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_55.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='D')
     endwith
    endif
  endfunc

  add object oCOMPIMP_1_57 as StdField with uid="VYWPQDKDEY",rtseq=35,rtrep=.f.,;
    cFormVar = "w_COMPIMP", cQueryName = "COMPIMP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Componente dell'impianto",;
    HelpContextID = 17105882,;
   bGlobalFont=.t.,;
    Height=21, Width=490, Left=96, Top=521, InputMask=replicate('X',50), bHasZoom = .t. 

  func oCOMPIMP_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_IMPIANTO))
    endwith
   endif
  endfunc

  func oCOMPIMP_1_57.mHide()
    with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='D')
    endwith
  endfunc

  proc oCOMPIMP_1_57.mBefore
    with this.Parent.oContained
      .w_OLDCOMP=.w_COMPIMP
    endwith
  endproc

  proc oCOMPIMP_1_57.mAfter
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M")
      endwith
  endproc

  proc oCOMPIMP_1_57.mZoom
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M",.T.)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oIMDESCRI_1_58 as StdField with uid="IJTEIEMZQA",rtseq=36,rtrep=.f.,;
    cFormVar = "w_IMDESCRI", cQueryName = "IMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 93285071,;
   bGlobalFont=.t.,;
    Height=21, Width=364, Left=222, Top=497, InputMask=replicate('X',50)

  func oIMDESCRI_1_58.mHide()
    with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='D')
    endwith
  endfunc


  add object oBtn_1_59 as StdButton with uid="FZIYGHIBOV",left=671, top=420, width=48,height=45,;
    CpPicture="bmp\abbina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per abbinare gli elementi contratto all'impianto e al componente";
    , HelpContextID = 90529542;
    , caption='\<Abbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_59.Click()
      with this.Parent.oContained
        GSAG_BV3(this.Parent.oContained,"ABBINA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_59.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY( .w_IMPIANTO ))
      endwith
    endif
  endfunc

  func oBtn_1_59.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLTYPEOP='D')
     endwith
    endif
  endfunc


  add object oBtn_1_60 as StdButton with uid="IYJFLJYWKA",left=671, top=420, width=48,height=45,;
    CpPicture="bmp\Disabbina.bmp", caption="", nPag=1;
    , ToolTipText = "premere per disabbinare gli elementi contratto dall'impianto e dal componente";
    , HelpContextID = 174162764;
    , caption='\<Disabbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_60.Click()
      with this.Parent.oContained
        GSAG_BV3(this.Parent.oContained,"DISABBINA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_60.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLTYPEOP='A')
     endwith
    endif
  endfunc

  add object oIMDESCRI_1_61 as StdField with uid="CRTOJJDMBX",rtseq=37,rtrep=.f.,;
    cFormVar = "w_IMDESCRI", cQueryName = "IMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 93285071,;
   bGlobalFont=.t.,;
    Height=21, Width=342, Left=240, Top=84, InputMask=replicate('X',50)

  func oIMDESCRI_1_61.mHide()
    with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='A')
    endwith
  endfunc


  add object oBtn_1_62 as StdButton with uid="KOTWJAFPMC",left=722, top=498, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41582522;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_63 as StdButton with uid="AULZWFKOCH",left=213, top=84, width=22,height=21,;
    CpPicture="bmp\ZOOM.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per ricercare per attributo";
    , HelpContextID = 48698922;
    , IMGSIZE=16;
  , bGlobalFont=.t.

    proc oBtn_1_63.Click()
      do gsag_kir with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_63.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='A')
     endwith
    endif
  endfunc


  add object oBtn_1_69 as StdButton with uid="YQRMZIVNTJ",left=722, top=420, width=48,height=45,;
    CpPicture="BMP\visuali.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire l'elemento contratto selezionato";
    , HelpContextID = 41521914;
    , Caption='A\<pri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_69.Click()
      this.parent.oContained.NotifyEvent("w_zoom selected")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="USFFVOTFSF",Visible=.t., Left=18, Top=35,;
    Alignment=1, Width=93, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="TUXQEFOWXW",Visible=.t., Left=18, Top=85,;
    Alignment=1, Width=93, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='A')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="PYXGIHNCPT",Visible=.t., Left=18, Top=110,;
    Alignment=1, Width=93, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='A')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="PIRVOQOWEM",Visible=.t., Left=18, Top=61,;
    Alignment=1, Width=93, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="JMNZWRVMBC",Visible=.t., Left=176, Top=419,;
    Alignment=1, Width=120, Height=18,;
    Caption="Descrizione modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="ZPSOZWYUGS",Visible=.t., Left=187, Top=441,;
    Alignment=1, Width=109, Height=18,;
    Caption="Descrizione cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="EGSAJTEXEB",Visible=.t., Left=176, Top=463,;
    Alignment=1, Width=120, Height=18,;
    Caption="Descrizione contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="QCJADCOLDU",Visible=.t., Left=171, Top=485,;
    Alignment=1, Width=125, Height=18,;
    Caption="Descrizione impianto:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_FLTYPEOP='A')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="HLGFFRTGDE",Visible=.t., Left=156, Top=507,;
    Alignment=1, Width=140, Height=18,;
    Caption="Descrizione componente:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (.w_FLTYPEOP='A')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="HNOXWDPZKG",Visible=.t., Left=55, Top=8,;
    Alignment=1, Width=56, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="IJQOWDTFRS",Visible=.t., Left=2, Top=497,;
    Alignment=1, Width=93, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='D')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="NRUAIXLJXA",Visible=.t., Left=2, Top=522,;
    Alignment=1, Width=93, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (IsALT() OR .w_FLTYPEOP='D')
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="PFALYVPNGQ",Visible=.t., Left=12, Top=476,;
    Alignment=0, Width=79, Height=19,;
    Caption="Abbina a:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.w_FLTYPEOP='D')
    endwith
  endfunc

  add object oBox_1_49 as StdBox with uid="QRVAIQNUAY",left=7, top=493, width=581,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kv3','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
