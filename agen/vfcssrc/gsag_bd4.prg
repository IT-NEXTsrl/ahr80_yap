* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bd4                                                        *
*              Rinnova elemento contratto                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-16                                                      *
* Last revis.: 2015-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM1,pPARAM2,pPARAM3,pPARAM4,pPARAM5,pPARAM6,pPARAM7,pRICPE1,pRICVA1,pARROT1,pVALORIN,pARROT2,pVALOR2IN,pARROT3,pVALOR3IN,pARROT4,pFLDATFIS
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bd4",oParentObject,m.pPARAM1,m.pPARAM2,m.pPARAM3,m.pPARAM4,m.pPARAM5,m.pPARAM6,m.pPARAM7,m.pRICPE1,m.pRICVA1,m.pARROT1,m.pVALORIN,m.pARROT2,m.pVALOR2IN,m.pARROT3,m.pVALOR3IN,m.pARROT4,m.pFLDATFIS)
return(i_retval)

define class tgsag_bd4 as StdBatch
  * --- Local variables
  pPARAM1 = space(1)
  pPARAM2 = space(10)
  pPARAM3 = space(10)
  pPARAM4 = space(10)
  pPARAM5 = 0
  pPARAM6 = 0
  pPARAM7 = space(3)
  pRICPE1 = 0
  pRICVA1 = 0
  pARROT1 = 0
  pVALORIN = 0
  pARROT2 = 0
  pVALOR2IN = 0
  pARROT3 = 0
  pVALOR3IN = 0
  pARROT4 = 0
  pFLDATFIS = space(1)
  w_ELCONTRA = space(10)
  w_ELCODMOD = space(10)
  w_ELCODIMP = space(10)
  w_ELCODCOM = 0
  w_ELRINNOV = 0
  w_ELPERCON = space(3)
  w_ELCODCLI = space(15)
  w_FLDATFIS = space(1)
  w_ELRINNOV_1 = 0
  w_ELPREZZO = 0
  w_ELNUMGIO = 0
  w_ELDATDIS_1 = ctod("  /  /  ")
  w_ELPERATT = space(3)
  w_ELDATATT_1 = ctod("  /  /  ")
  w_ELGIOATT = 0
  w_ELDATGAT_1 = ctod("  /  /  ")
  w_ELPERFAT = space(3)
  w_ELDATFAT_1 = ctod("  /  /  ")
  w_ELGIODOC = 0
  w_ELDATDOC_1 = ctod("  /  /  ")
  w_ELDATFAT = ctod("  /  /  ")
  w_ELDATATT = ctod("  /  /  ")
  w_ELDATDOC = ctod("  /  /  ")
  w_ELDATGAT = ctod("  /  /  ")
  w_ELDATINI = ctod("  /  /  ")
  w_OKBUTTON = .f.
  w_ELDATFIN = ctod("  /  /  ")
  w_ELDATINI_1 = ctod("  /  /  ")
  w_ELDATFIN_1 = ctod("  /  /  ")
  w_RICPE1 = 0
  w_RICVA1 = 0
  w_ARROT1 = 0
  w_VALORIN = 0
  w_ARROT2 = 0
  w_VALOR2IN = 0
  w_ARROT3 = 0
  w_VALOR3IN = 0
  w_ARROT4 = 0
  w_MOTIPAPL = space(1)
  w_MOFLATTI = space(1)
  w_MOFLFATT = space(1)
  w_ELFINCOA = ctod("  /  /  ")
  w_ELINICOA_1 = ctod("  /  /  ")
  w_ELFINCOA_1 = ctod("  /  /  ")
  w_ELFINCOD = ctod("  /  /  ")
  w_ELINICOD_1 = ctod("  /  /  ")
  w_ELFINCOD_1 = ctod("  /  /  ")
  w_CODATFIN = ctod("  /  /  ")
  w_PREZZO1 = 0
  w_DECTOT = 0
  w_ELDATFAT_OLD = ctod("  /  /  ")
  w_ELINICOD_OLD = ctod("  /  /  ")
  w_ELFINCOD_OLD = ctod("  /  /  ")
  w_ELDATATT_OLD = ctod("  /  /  ")
  w_ELINICOA_OLD = ctod("  /  /  ")
  w_ELFINCOA_OLD = ctod("  /  /  ")
  * --- WorkFile variables
  ELE_CONT_idx=0
  RIPATMP1_idx=0
  CON_TRAS_idx=0
  VALUTE_idx=0
  MOD_ELEM_idx=0
  DOC_GENE_idx=0
  DET_GEN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rinnova elemento contratto
    * --- pPARAM1 = "R" - rinnova
    *     pPARAM1 = "S" - rinnova senza aprire la finestra di dialogo
    * --- pPARAM2  - Codice contratto (ELCONTRA)
    * --- pPARAM3  - Modello (ELCODMOD)
    * --- pPARAM4  - Impianto (ELCODIMP)
    * --- pPARAM5  - Componente (ELCODCOM)
    * --- pPARAM6  - Numero rinnovi (ELRINNOV)
    * --- pPARAM7  - Numero rinnovi (ELPERCON)
    * --- pPARAM8  - Data fissa (MODATFIS)
    * --- Arrotondamenti e relativi importi
    * --- --
    * --- pPARAM2  - Codice contratto (ELCONTRA)
    this.w_ELCONTRA = this.pPARAM2
    * --- pPARAM3  - Modello (ELCODMOD)
    this.w_ELCODMOD = this.pPARAM3
    * --- pPARAM4  - Impianto (ELCODIMP)
    this.w_ELCODIMP = this.pPARAM4
    * --- pPARAM5  - Componente (ELCODCOM)
    this.w_ELCODCOM = this.pPARAM5
    * --- pPARAM6  - Numero rinnovi (ELRINNOV)
    this.w_ELRINNOV = this.pPARAM6
    * --- pPARAM7  - Periodicit� rinnovi (ELPERCON)
    this.w_ELPERCON = this.pPARAM7
    * --- pPARAM8  - Data fissa (MODATFIS)
    this.w_FLDATFIS = this.pFLDATFIS
    * --- Nuovo numero di rinnovi
    this.w_ELRINNOV_1 = 0
    * --- Prezzo
    * --- Dati disdetta
    * --- Periodicit� attivit� e data prossima attivit�
    * --- Periodicit� documento e data prossimo documento
    * --- Data prossimo documento e prossima attivit� dell'elemento da rinnovare
    * --- Select from ELE_CONT
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2],.t.,this.ELE_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ELE_CONT ";
          +" where ELCODMOD = "+cp_ToStrODBC(this.w_ELCODMOD)+" and ELCONTRA = "+cp_ToStrODBC(this.w_ELCONTRA)+" and ELCODIMP = "+cp_ToStrODBC(this.w_ELCODIMP)+" and ELCODCOM = "+cp_ToStrODBC(this.w_ELCODCOM)+"";
          +" order by ELRINNOV DESC";
           ,"_Curs_ELE_CONT")
    else
      select * from (i_cTable);
       where ELCODMOD = this.w_ELCODMOD and ELCONTRA = this.w_ELCONTRA and ELCODIMP = this.w_ELCODIMP and ELCODCOM = this.w_ELCODCOM;
       order by ELRINNOV DESC;
        into cursor _Curs_ELE_CONT
    endif
    if used('_Curs_ELE_CONT')
      select _Curs_ELE_CONT
      locate for 1=1
      do while not(eof())
      * --- La query � filtrata per 
      *     ELE_CONT.ELCODMOD = w_ELCODMOD
      *     ELE_CONT.ELCONTRA = w_ELCONTRA
      *     ed � ordinata per
      *     ELRINNOV DESC
      *     In questo modo il primo record restituisce il numero di rinnovi pi� recente
      this.w_ELRINNOV_1 = _Curs_ELE_CONT.ELRINNOV
      this.w_ELPREZZO = NVL( _Curs_ELE_CONT.ELPREZZO, 0 )
      this.w_ELNUMGIO = NVL(_Curs_ELE_CONT.ELNUMGIO,0)
      this.w_ELDATINI = _Curs_ELE_CONT.ELDATINI
      this.w_ELDATFIN = _Curs_ELE_CONT.ELDATFIN
      this.w_ELPERATT = _Curs_ELE_CONT.ELPERATT
      this.w_ELPERFAT = _Curs_ELE_CONT.ELPERFAT
      this.w_ELGIOATT = _Curs_ELE_CONT.ELGIOATT
      this.w_ELGIODOC = _Curs_ELE_CONT.ELGIODOC
      this.w_ELDATATT = NVL( _Curs_ELE_CONT.ELDATATT, CTOD("  -  -  ") )
      this.w_ELDATFAT = NVL( _Curs_ELE_CONT.ELDATFAT, CTOD("  -  -  ") )
      this.w_ELDATDOC = NVL( _Curs_ELE_CONT.ELDATDOC, CTOD("  -  -  ") )
      this.w_ELDATGAT = NVL( _Curs_ELE_CONT.ELDATGAT, CTOD("  -  -  ") )
      this.w_ELFINCOA = NVL( _Curs_ELE_CONT.ELFINCOA, CTOD("  -  -  ") )
      this.w_ELFINCOD = NVL( _Curs_ELE_CONT.ELFINCOD, CTOD("  -  -  ") )
      exit
        select _Curs_ELE_CONT
        continue
      enddo
      use
    endif
    this.w_ELRINNOV_1 = this.w_ELRINNOV_1 + 1
    * --- Verifica se � stato premuto il bottone di OK sulla maschera GSAG_KD4
    this.w_OKBUTTON = .F.
    * --- Data fine validit� elemento contratto da rinnovare (modificabile sulla maschera GSAG_KD4)
    * --- Date inizio e fine validit� elemento contratto rinnovato (modificabili sulla maschera GSAG_KD4)
    if this.w_FLDATFIS="S"
      this.w_ELDATINI_1 = CP_TODATE( this.w_ELDATINI )
    else
      this.w_ELDATINI_1 = CP_TODATE( this.w_ELDATFIN ) + 1
    endif
    this.w_ELDATFIN_1 = NEXTTIME(this.w_ELPERCON, this.w_ELDATFIN )
    * --- Arrotondamenti e relativi importi (valorizzati dalla maschera o dai parametri)
    * --- Se pPARAM1 = "S" la funzionalit� � stata chiamata da routine
    if this.pPARAM1 = "R"
      * --- L'elemento corrente non � l'elemento con l'ultimo numero rinnovo.
      *     Per rinnovare occorre selezionare l'elemento avente l'ultimo numero rinnovo.
      if this.w_ELRINNOV <> this.w_ELRINNOV_1 - 1
        AH_ERRORMSG( "L'elemento corrente ( numero rinnovo %1 ) non � l'elemento con il numero rinnovo pi� alto.%0Per rinnovare occorre selezionare l'elemento avente numero rinnovo %2.", 16, "", ALLTRIM(STR(this.w_ELRINNOV)), ALLTRIM(STR(this.w_ELRINNOV_1 -1 )) )
        i_retcode = 'stop'
        return
      endif
      do GSAG_KD4 with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_OKBUTTON = .T.
      this.w_RICPE1 = this.pRICPE1
      this.w_RICVA1 = this.pRICVA1
      this.w_ARROT1 = this.pARROT1
      this.w_VALORIN = this.pVALORIN
      this.w_ARROT2 = this.pARROT2
      this.w_VALOR2IN = this.pVALOR2IN
      this.w_ARROT3 = this.pARROT3
      this.w_VALOR3IN = this.pVALOR3IN
      this.w_ARROT4 = this.pARROT4
    endif
    if this.w_OKBUTTON
      * --- Read from MOD_ELEM
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MOD_ELEM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_ELEM_idx,2],.t.,this.MOD_ELEM_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MOTIPAPL,MOFLATTI,MOFLFATT"+;
          " from "+i_cTable+" MOD_ELEM where ";
              +"MOCODICE = "+cp_ToStrODBC(this.w_ELCODMOD);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MOTIPAPL,MOFLATTI,MOFLFATT;
          from (i_cTable) where;
              MOCODICE = this.w_ELCODMOD;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MOTIPAPL = NVL(cp_ToDate(_read_.MOTIPAPL),cp_NullValue(_read_.MOTIPAPL))
        this.w_MOFLATTI = NVL(cp_ToDate(_read_.MOFLATTI),cp_NullValue(_read_.MOFLATTI))
        this.w_MOFLFATT = NVL(cp_ToDate(_read_.MOFLFATT),cp_NullValue(_read_.MOFLFATT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Data limite disdetta
      if NOT EMPTY( NVL( this.w_ELDATFIN_1, CTOD("  -  -  ")) )
        this.w_ELDATDIS_1 = this.w_ELDATFIN_1 - this.w_ELNUMGIO
      endif
      * --- Data prossima attivit� e data fino alla quale l'elemento potr� generare attivit�
      if this.w_MOFLATTI = "S"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ELDATATT_1 = NEXTTIME( this.w_ELPERATT, this.w_ELDATATT_OLD )
        this.w_ELDATGAT_1 = CP_TODATE( this.w_ELDATFIN_1 ) + this.w_ELGIOATT
        if NOT EMPTY( this.w_ELFINCOA_OLD )
          * --- Data fine competenza attivit�
          * --- Date inizio e fine competenza attivit� del nuovo elemento contratto
          this.w_ELINICOA_1 = CP_TODATE( this.w_ELFINCOA_OLD ) + 1
          this.w_ELFINCOA_1 = NEXTTIME(this.w_ELPERATT, this.w_ELFINCOA_OLD )
        endif
      else
        this.w_ELDATATT_1 = CP_CHARTODATE( "  -  -  " )
        this.w_ELDATGAT_1 = CP_CHARTODATE( "  -  -  " )
      endif
      * --- Se la data di prossima attivit� risultante cade fuori dall'intervallo ELDATINI - (MAX(ELDATGAT,ELDATFIN)), allora deve essere sbiancata
      if this.w_ELDATATT_1 > MAX( this.w_ELDATGAT_1, this.w_ELDATFIN_1 )
        this.w_ELDATATT_1 = CP_CHARTODATE( "  -  -  " )
      endif
      * --- Data prossimo documento e data fino alla quale l'elemento potr� generare documenti
      if this.w_MOFLFATT = "S"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_ELDATFAT_1 = NEXTTIME( this.w_ELPERFAT, this.w_ELDATFAT_OLD )
        this.w_ELDATDOC_1 = CP_TODATE( this.w_ELDATFIN_1 ) + this.w_ELGIODOC
        if NOT EMPTY( this.w_ELFINCOD_OLD )
          * --- Data fine competenza documento
          * --- Date inizio e fine competenza documento del nuovo elemento contratto
          this.w_ELINICOD_1 = CP_TODATE( this.w_ELFINCOD_OLD ) + 1
          this.w_ELFINCOD_1 = NEXTTIME(this.w_ELPERFAT, this.w_ELFINCOD_OLD )
        endif
      else
        this.w_ELDATFAT_1 = CP_CHARTODATE( "  -  -  " )
        this.w_ELDATDOC_1 = CP_CHARTODATE( "  -  -  " )
      endif
      * --- Se la data di prossimo documento risultante cade fuori dall'intervallo ELDATINI - (MAX(ELDATDOC,ELDATFIN)), allora deve essere sbiancata
      if this.w_ELDATFAT_1 > MAX( this.w_ELDATDOC_1, this.w_ELDATFIN_1 )
        this.w_ELDATFAT_1 = CP_CHARTODATE( "  -  -  " )
      endif
      * --- Crea una tabella temporanea con una copia del record da duplicare
      * --- Create temporary table RIPATMP1
      i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSAG_BD4',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.RIPATMP1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      if this.w_MOTIPAPL = "C"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Apporta le modifiche al nuovo elemento contratto
      * --- Write into RIPATMP1
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RIPATMP1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP1_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ELRINNOV ="+cp_NullLink(cp_ToStrODBC(this.w_ELRINNOV_1),'RIPATMP1','ELRINNOV');
        +",ELDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_ELDATINI_1),'RIPATMP1','ELDATINI');
        +",ELDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ELDATFIN_1),'RIPATMP1','ELDATFIN');
        +",ELPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_ELPREZZO),'RIPATMP1','ELPREZZO');
        +",ELDATDIS ="+cp_NullLink(cp_ToStrODBC(this.w_ELDATDIS_1),'RIPATMP1','ELDATDIS');
        +",ELDATATT ="+cp_NullLink(cp_ToStrODBC(this.w_ELDATATT_1),'RIPATMP1','ELDATATT');
        +",ELDATFAT ="+cp_NullLink(cp_ToStrODBC(this.w_ELDATFAT_1),'RIPATMP1','ELDATFAT');
        +",ELDATGAT ="+cp_NullLink(cp_ToStrODBC(this.w_ELDATGAT_1),'RIPATMP1','ELDATGAT');
        +",ELDATDOC ="+cp_NullLink(cp_ToStrODBC(this.w_ELDATDOC_1),'RIPATMP1','ELDATDOC');
        +",ELQTACON ="+cp_NullLink(cp_ToStrODBC(0),'RIPATMP1','ELQTACON');
        +",ELINICOA ="+cp_NullLink(cp_ToStrODBC(this.w_ELINICOA_1),'RIPATMP1','ELINICOA');
        +",ELFINCOA ="+cp_NullLink(cp_ToStrODBC(this.w_ELFINCOA_1),'RIPATMP1','ELFINCOA');
        +",ELINICOD ="+cp_NullLink(cp_ToStrODBC(this.w_ELINICOD_1),'RIPATMP1','ELINICOD');
        +",ELFINCOD ="+cp_NullLink(cp_ToStrODBC(this.w_ELFINCOD_1),'RIPATMP1','ELFINCOD');
            +i_ccchkf ;
        +" where ";
            +"1 = "+cp_ToStrODBC(1);
               )
      else
        update (i_cTable) set;
            ELRINNOV = this.w_ELRINNOV_1;
            ,ELDATINI = this.w_ELDATINI_1;
            ,ELDATFIN = this.w_ELDATFIN_1;
            ,ELPREZZO = this.w_ELPREZZO;
            ,ELDATDIS = this.w_ELDATDIS_1;
            ,ELDATATT = this.w_ELDATATT_1;
            ,ELDATFAT = this.w_ELDATFAT_1;
            ,ELDATGAT = this.w_ELDATGAT_1;
            ,ELDATDOC = this.w_ELDATDOC_1;
            ,ELQTACON = 0;
            ,ELINICOA = this.w_ELINICOA_1;
            ,ELFINCOA = this.w_ELFINCOA_1;
            ,ELINICOD = this.w_ELINICOD_1;
            ,ELFINCOD = this.w_ELFINCOD_1;
            &i_ccchkf. ;
         where;
            1 = 1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Try
      local bErr_03080E50
      bErr_03080E50=bTrsErr
      this.Try_03080E50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_03080E50
      * --- End
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
      if this.pPARAM1 = "R"
        AH_ERRORMSG("Operazione completata", 64)
      endif
    endif
  endproc
  proc Try_03080E50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Inserisce il nuovo elemento contratto
    * --- Insert into ELE_CONT
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.RIPATMP1_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.ELE_CONT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorna la data di fine contratto se necessario
    * --- Read from CON_TRAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CON_TRAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAS_idx,2],.t.,this.CON_TRAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CODATFIN"+;
        " from "+i_cTable+" CON_TRAS where ";
            +"COSERIAL = "+cp_ToStrODBC(this.w_ELCONTRA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CODATFIN;
        from (i_cTable) where;
            COSERIAL = this.w_ELCONTRA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODATFIN = NVL(cp_ToDate(_read_.CODATFIN),cp_NullValue(_read_.CODATFIN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NVL( this.w_ELDATFIN_1, CHARTODATE("  -  -  ")) > NVL( this.w_CODATFIN, CHARTODATE("  -  -  "))
      * --- Write into CON_TRAS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CON_TRAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_TRAS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CODATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ELDATFIN_1),'CON_TRAS','CODATFIN');
            +i_ccchkf ;
        +" where ";
            +"COSERIAL = "+cp_ToStrODBC(this.w_ELCONTRA);
               )
      else
        update (i_cTable) set;
            CODATFIN = this.w_ELDATFIN_1;
            &i_ccchkf. ;
         where;
            COSERIAL = this.w_ELCONTRA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola il nuovo prezzo
    this.w_PREZZO1 = this.w_ELPREZZO
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_RICPE1<>0
      * --- Ricalcolo dei prezzi dello scaglione per percentuale
      this.w_PREZZO1 = (this.w_PREZZO1*(100+this.w_RICPE1))/100
    else
      if this.w_RICVA1<>0
        * --- Ricalcolo dei prezzi dello scaglione per valore solo se per articoli. Ai gruppi merceologici non si pu� ricalcolare per valore
        this.w_PREZZO1 = this.w_PREZZO1+this.w_RICVA1
      endif
    endif
    * --- Nel contratto di riferimento e attivato il flag scaglioni
    if this.w_ARROT1<>0
      * --- Controllo se il prezzo finale � minore del primo arrotondamento
      if this.w_PREZZO1<=this.w_VALORIN
        * --- Applico l'arrotondamento selezionato.
        this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1/this.w_ARROT1),0)*this.w_ARROT1
      else
        * --- Controllo se il prezzo finale � minore del secondo arrotondamento
        if this.w_PREZZO1<=this.w_VALOR2IN
          if this.w_ARROT2<>0
            * --- Applico l'arrotondamento selezionato.
            this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1/this.w_ARROT2),0)*this.w_ARROT2
          endif
        else
          * --- Controllo se il prezzo finale � minore del terzo arrotondamento
          if this.w_PREZZO1<=this.w_VALOR3IN
            if this.w_ARROT3<>0
              * --- Applico l'arrotondamento selezionato.
              this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1/this.w_ARROT3),0)*this.w_ARROT3
            endif
          else
            * --- Controllo se il prezzo finale � minore del quarto arrotondamento
            if this.w_ARROT4<>0
              * --- Applico l'ultimo arrotondamento selezionato.
              this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1/this.w_ARROT4),0)*this.w_ARROT4
            else
              * --- Il prezzo � maggiore dei vari valori degli arrotondamenti quindi non applico nessun arrotondamento.
              this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1),this.w_DECTOT)
            endif
          endif
        endif
      endif
    else
      * --- Non ho applicato nessun arrotondamento.Il prezzo finale viene calcolato in base alla valuta  finale
      this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1),this.w_DECTOT)
    endif
    this.w_ELPREZZO = this.w_PREZZO1
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if EMPTY( this.w_ELDATFAT )
      * --- Legge la data da utilizzare come parametro per il calcolo della nuova data prossimo documento dalla tabella DOC_GENE
      * --- Select from DOC_GENE
      i_nConn=i_TableProp[this.DOC_GENE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_GENE_idx,2],.t.,this.DOC_GENE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MAX( DDOLDDAT ) AS DDOLDDAT, MAX( DDINICOM ) AS DDINICOM, MAX( DDFINCOM ) AS DDFINCOM  from "+i_cTable+" DOC_GENE ";
            +" where DDCODMOD = "+cp_ToStrODBC(this.w_ELCODMOD)+" and DDCONTRA = "+cp_ToStrODBC(this.w_ELCONTRA)+" and DDCODIMP = "+cp_ToStrODBC(this.w_ELCODIMP)+" and DDCOMPON = "+cp_ToStrODBC(this.w_ELCODCOM)+" and DDRINNOV = "+cp_ToStrODBC(this.w_ELRINNOV)+"";
             ,"_Curs_DOC_GENE")
      else
        select MAX( DDOLDDAT ) AS DDOLDDAT, MAX( DDINICOM ) AS DDINICOM, MAX( DDFINCOM ) AS DDFINCOM from (i_cTable);
         where DDCODMOD = this.w_ELCODMOD and DDCONTRA = this.w_ELCONTRA and DDCODIMP = this.w_ELCODIMP and DDCOMPON = this.w_ELCODCOM and DDRINNOV = this.w_ELRINNOV;
          into cursor _Curs_DOC_GENE
      endif
      if used('_Curs_DOC_GENE')
        select _Curs_DOC_GENE
        locate for 1=1
        do while not(eof())
        this.w_ELDATFAT_OLD = _Curs_DOC_GENE.DDOLDDAT
        this.w_ELINICOD_OLD = _Curs_DOC_GENE.DDINICOM
        this.w_ELFINCOD_OLD = _Curs_DOC_GENE.DDFINCOM
          select _Curs_DOC_GENE
          continue
        enddo
        use
      endif
    else
      * --- In questo caso occorre calcolare l'ultima data di generazione applicando la periodicit� ELPERFAT finch� il risultato
      *     � inferiore a ELDATDOC
      this.w_ELDATFAT_OLD = this.w_ELDATFAT
      this.w_ELFINCOD_OLD = this.w_ELFINCOD
      do while NEXTTIME( this.w_ELPERFAT, this.w_ELDATFAT_OLD ) <= this.w_ELDATDOC
        this.w_ELDATFAT_OLD = NEXTTIME( this.w_ELPERFAT, this.w_ELDATFAT_OLD )
        this.w_ELINICOD_OLD = CP_TODATE( this.w_ELFINCOD_OLD ) + 1
        this.w_ELFINCOD_OLD = NEXTTIME( this.w_ELPERFAT, this.w_ELFINCOD_OLD )
      enddo
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if EMPTY( this.w_ELDATATT )
      * --- Legge la data da utilizzare come parametro per il calcolo della nuova data prossimo documento dalla tabella DOC_GENE
      * --- Select from DET_GEN
      i_nConn=i_TableProp[this.DET_GEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DET_GEN_idx,2],.t.,this.DET_GEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MAX( MDOLDDAT ) AS MDOLDDAT, MAX( MDINICOM ) AS MDINICOM, MAX( MDFINCOM ) AS MDFINCOM  from "+i_cTable+" DET_GEN ";
            +" where MDCODMOD = "+cp_ToStrODBC(this.w_ELCODMOD)+" and MDCONTRA = "+cp_ToStrODBC(this.w_ELCONTRA)+" and MDCODIMP = "+cp_ToStrODBC(this.w_ELCODIMP)+" and MDCOMPON = "+cp_ToStrODBC(this.w_ELCODCOM)+" and MDRINNOV = "+cp_ToStrODBC(this.w_ELRINNOV)+"";
             ,"_Curs_DET_GEN")
      else
        select MAX( MDOLDDAT ) AS MDOLDDAT, MAX( MDINICOM ) AS MDINICOM, MAX( MDFINCOM ) AS MDFINCOM from (i_cTable);
         where MDCODMOD = this.w_ELCODMOD and MDCONTRA = this.w_ELCONTRA and MDCODIMP = this.w_ELCODIMP and MDCOMPON = this.w_ELCODCOM and MDRINNOV = this.w_ELRINNOV;
          into cursor _Curs_DET_GEN
      endif
      if used('_Curs_DET_GEN')
        select _Curs_DET_GEN
        locate for 1=1
        do while not(eof())
        this.w_ELDATATT_OLD = _Curs_DET_GEN.MDOLDDAT
        this.w_ELINICOA_OLD = _Curs_DET_GEN.MDINICOM
        this.w_ELFINCOA_OLD = _Curs_DET_GEN.MDFINCOM
          select _Curs_DET_GEN
          continue
        enddo
        use
      endif
    else
      * --- In questo caso occorre calcolare l'ultima data di generazione applicando la periodicit� ELPERFAT finch� il risultato
      *     � inferiore a ELDATDOC
      this.w_ELDATATT_OLD = this.w_ELDATATT
      this.w_ELFINCOA_OLD = this.w_ELFINCOA
      do while NEXTTIME( this.w_ELPERATT, this.w_ELDATATT_OLD ) <= this.w_ELDATGAT
        this.w_ELDATATT_OLD = NEXTTIME( this.w_ELPERATT, this.w_ELDATATT_OLD )
        this.w_ELINICOA_OLD = CP_TODATE( this.w_ELFINCOA_OLD ) + 1
        this.w_ELFINCOA_OLD = NEXTTIME( this.w_ELPERATT, this.w_ELFINCOA_OLD )
      enddo
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPARAM1,pPARAM2,pPARAM3,pPARAM4,pPARAM5,pPARAM6,pPARAM7,pRICPE1,pRICVA1,pARROT1,pVALORIN,pARROT2,pVALOR2IN,pARROT3,pVALOR3IN,pARROT4,pFLDATFIS)
    this.pPARAM1=pPARAM1
    this.pPARAM2=pPARAM2
    this.pPARAM3=pPARAM3
    this.pPARAM4=pPARAM4
    this.pPARAM5=pPARAM5
    this.pPARAM6=pPARAM6
    this.pPARAM7=pPARAM7
    this.pRICPE1=pRICPE1
    this.pRICVA1=pRICVA1
    this.pARROT1=pARROT1
    this.pVALORIN=pVALORIN
    this.pARROT2=pARROT2
    this.pVALOR2IN=pVALOR2IN
    this.pARROT3=pARROT3
    this.pVALOR3IN=pVALOR3IN
    this.pARROT4=pARROT4
    this.pFLDATFIS=pFLDATFIS
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='ELE_CONT'
    this.cWorkTables[2]='*RIPATMP1'
    this.cWorkTables[3]='CON_TRAS'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='MOD_ELEM'
    this.cWorkTables[6]='DOC_GENE'
    this.cWorkTables[7]='DET_GEN'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_ELE_CONT')
      use in _Curs_ELE_CONT
    endif
    if used('_Curs_DOC_GENE')
      use in _Curs_DOC_GENE
    endif
    if used('_Curs_DET_GEN')
      use in _Curs_DET_GEN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM1,pPARAM2,pPARAM3,pPARAM4,pPARAM5,pPARAM6,pPARAM7,pRICPE1,pRICVA1,pARROT1,pVALORIN,pARROT2,pVALOR2IN,pARROT3,pVALOR3IN,pARROT4,pFLDATFIS"
endproc
