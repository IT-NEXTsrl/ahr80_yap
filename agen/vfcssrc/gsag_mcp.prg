* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mcp                                                        *
*              Componenti attivit�                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-14                                                      *
* Last revis.: 2012-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsag_mcp")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsag_mcp")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsag_mcp")
  return

* --- Class definition
define class tgsag_mcp as StdPCForm
  Width  = 626
  Height = 107
  Top    = 10
  Left   = 10
  cComment = "Componenti attivit�"
  cPrg = "gsag_mcp"
  HelpContextID=171301015
  add object cnt as tcgsag_mcp
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsag_mcp as PCContext
  w_CASERIAL = space(20)
  w_CPROWORD = 0
  w_CACODIMP = space(10)
  w_OLDCOMP = space(50)
  w_CACODCOM = 0
  w_COCOM = 0
  w_DESCOM = space(50)
  w_DESIMP = space(50)
  w_CODCLI = space(15)
  w_CLIIMP = space(15)
  w_IMDATINI = space(8)
  w_IMDATFIN = space(8)
  w_DATINI = space(8)
  w_DTOBSO = space(8)
  w_SEDE = space(5)
  w_ATCODNOM = space(15)
  w_ATCODSED = space(5)
  w_DesNomin = space(60)
  w_NOMDES = space(40)
  w_DATFIN = space(8)
  w_DTOINI = space(8)
  proc Save(i_oFrom)
    this.w_CASERIAL = i_oFrom.w_CASERIAL
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_CACODIMP = i_oFrom.w_CACODIMP
    this.w_OLDCOMP = i_oFrom.w_OLDCOMP
    this.w_CACODCOM = i_oFrom.w_CACODCOM
    this.w_COCOM = i_oFrom.w_COCOM
    this.w_DESCOM = i_oFrom.w_DESCOM
    this.w_DESIMP = i_oFrom.w_DESIMP
    this.w_CODCLI = i_oFrom.w_CODCLI
    this.w_CLIIMP = i_oFrom.w_CLIIMP
    this.w_IMDATINI = i_oFrom.w_IMDATINI
    this.w_IMDATFIN = i_oFrom.w_IMDATFIN
    this.w_DATINI = i_oFrom.w_DATINI
    this.w_DTOBSO = i_oFrom.w_DTOBSO
    this.w_SEDE = i_oFrom.w_SEDE
    this.w_ATCODNOM = i_oFrom.w_ATCODNOM
    this.w_ATCODSED = i_oFrom.w_ATCODSED
    this.w_DesNomin = i_oFrom.w_DesNomin
    this.w_NOMDES = i_oFrom.w_NOMDES
    this.w_DATFIN = i_oFrom.w_DATFIN
    this.w_DTOINI = i_oFrom.w_DTOINI
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CASERIAL = this.w_CASERIAL
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_CACODIMP = this.w_CACODIMP
    i_oTo.w_OLDCOMP = this.w_OLDCOMP
    i_oTo.w_CACODCOM = this.w_CACODCOM
    i_oTo.w_COCOM = this.w_COCOM
    i_oTo.w_DESCOM = this.w_DESCOM
    i_oTo.w_DESIMP = this.w_DESIMP
    i_oTo.w_CODCLI = this.w_CODCLI
    i_oTo.w_CLIIMP = this.w_CLIIMP
    i_oTo.w_IMDATINI = this.w_IMDATINI
    i_oTo.w_IMDATFIN = this.w_IMDATFIN
    i_oTo.w_DATINI = this.w_DATINI
    i_oTo.w_DTOBSO = this.w_DTOBSO
    i_oTo.w_SEDE = this.w_SEDE
    i_oTo.w_ATCODNOM = this.w_ATCODNOM
    i_oTo.w_ATCODSED = this.w_ATCODSED
    i_oTo.w_DesNomin = this.w_DesNomin
    i_oTo.w_NOMDES = this.w_NOMDES
    i_oTo.w_DATFIN = this.w_DATFIN
    i_oTo.w_DTOINI = this.w_DTOINI
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsag_mcp as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 626
  Height = 107
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-03-19"
  HelpContextID=171301015
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  COM_ATTI_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  cFile = "COM_ATTI"
  cKeySelect = "CASERIAL"
  cKeyWhere  = "CASERIAL=this.w_CASERIAL"
  cKeyDetail  = "CASERIAL=this.w_CASERIAL and CACODCOM=this.w_CACODCOM"
  cKeyWhereODBC = '"CASERIAL="+cp_ToStrODBC(this.w_CASERIAL)';

  cKeyDetailWhereODBC = '"CASERIAL="+cp_ToStrODBC(this.w_CASERIAL)';
      +'+" and CACODCOM="+cp_ToStrODBC(this.w_CACODCOM)';

  cKeyWhereODBCqualified = '"COM_ATTI.CASERIAL="+cp_ToStrODBC(this.w_CASERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'COM_ATTI.CPROWORD '
  cPrg = "gsag_mcp"
  cComment = "Componenti attivit�"
  i_nRowNum = 0
  i_nRowPerPage = 3
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CASERIAL = space(20)
  w_CPROWORD = 0
  w_CACODIMP = space(10)
  o_CACODIMP = space(10)
  w_OLDCOMP = space(50)
  w_CACODCOM = 0
  o_CACODCOM = 0
  w_COCOM = 0
  w_DESCOM = space(50)
  w_DESIMP = space(50)
  w_CODCLI = space(15)
  w_CLIIMP = space(15)
  w_IMDATINI = ctod('  /  /  ')
  w_IMDATFIN = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_SEDE = space(5)
  w_ATCODNOM = space(15)
  w_ATCODSED = space(5)
  w_DesNomin = space(60)
  w_NOMDES = space(40)
  w_DATFIN = ctod('  /  /  ')
  w_DTOINI = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_mcpPag1","gsag_mcp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='IMP_MAST'
    this.cWorkTables[2]='IMP_DETT'
    this.cWorkTables[3]='COM_ATTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.COM_ATTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.COM_ATTI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsag_mcp'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from COM_ATTI where CASERIAL=KeySet.CASERIAL
    *                            and CACODCOM=KeySet.CACODCOM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.COM_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_ATTI_IDX,2],this.bLoadRecFilter,this.COM_ATTI_IDX,"gsag_mcp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('COM_ATTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "COM_ATTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' COM_ATTI '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CASERIAL',this.w_CASERIAL  )
      select * from (i_cTable) COM_ATTI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODCLI = space(15)
        .w_DATINI = ctod("  /  /  ")
        .w_ATCODNOM = this.oparentobject.w_ATCODNOM
        .w_ATCODSED = this.oparentobject.w_ATCODSED
        .w_DesNomin = this.oparentobject.w_DESNOMIN
        .w_NOMDES = this.oparentobject.w_NOMDES
        .w_CASERIAL = NVL(CASERIAL,space(20))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_DATFIN = this.oparentobject.w_DATFIN
        cp_LoadRecExtFlds(this,'COM_ATTI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_OLDCOMP = space(50)
          .w_DESCOM = space(50)
          .w_DESIMP = space(50)
          .w_CLIIMP = space(15)
          .w_IMDATINI = ctod("  /  /  ")
          .w_IMDATFIN = ctod("  /  /  ")
          .w_DTOBSO = ctod("  /  /  ")
          .w_SEDE = space(5)
          .w_DTOINI = ctod("  /  /  ")
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CACODIMP = NVL(CACODIMP,space(10))
          if link_2_2_joined
            this.w_CACODIMP = NVL(IMCODICE202,NVL(this.w_CACODIMP,space(10)))
            this.w_DESIMP = NVL(IMDESCRI202,space(50))
            this.w_CLIIMP = NVL(IMCODCON202,space(15))
            this.w_IMDATINI = NVL(cp_ToDate(IMDATINI202),ctod("  /  /  "))
            this.w_IMDATFIN = NVL(cp_ToDate(IMDATFIN202),ctod("  /  /  "))
            this.w_SEDE = NVL(IM__SEDE202,space(5))
          else
          .link_2_2('Load')
          endif
          .w_CACODCOM = NVL(CACODCOM,0)
        .w_COCOM = .w_CACODCOM
          .link_2_5('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CACODCOM with .w_CACODCOM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_DATFIN = this.oparentobject.w_DATFIN
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_CASERIAL=space(20)
      .w_CPROWORD=10
      .w_CACODIMP=space(10)
      .w_OLDCOMP=space(50)
      .w_CACODCOM=0
      .w_COCOM=0
      .w_DESCOM=space(50)
      .w_DESIMP=space(50)
      .w_CODCLI=space(15)
      .w_CLIIMP=space(15)
      .w_IMDATINI=ctod("  /  /  ")
      .w_IMDATFIN=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_SEDE=space(5)
      .w_ATCODNOM=space(15)
      .w_ATCODSED=space(5)
      .w_DesNomin=space(60)
      .w_NOMDES=space(40)
      .w_DATFIN=ctod("  /  /  ")
      .w_DTOINI=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_CACODIMP))
         .link_2_2('Full')
        endif
        .DoRTCalc(4,4,.f.)
        .w_CACODCOM = 0
        .w_COCOM = .w_CACODCOM
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_COCOM))
         .link_2_5('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(7,15,.f.)
        .w_ATCODNOM = this.oparentobject.w_ATCODNOM
        .w_ATCODSED = this.oparentobject.w_ATCODSED
        .w_DesNomin = this.oparentobject.w_DESNOMIN
        .w_NOMDES = this.oparentobject.w_NOMDES
        .w_DATFIN = this.oparentobject.w_DATFIN
      endif
    endwith
    cp_BlankRecExtFlds(this,'COM_ATTI')
    this.DoRTCalc(21,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'COM_ATTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.COM_ATTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CASERIAL,"CASERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_CACODIMP C(10);
      ,t_CACODCOM N(4);
      ,t_DESCOM C(50);
      ,CACODCOM N(4);
      ,t_OLDCOMP C(50);
      ,t_COCOM N(4);
      ,t_DESIMP C(50);
      ,t_CLIIMP C(15);
      ,t_IMDATINI D(8);
      ,t_IMDATFIN D(8);
      ,t_DTOBSO D(8);
      ,t_SEDE C(5);
      ,t_DTOINI D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mcpbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCACODIMP_2_2.controlsource=this.cTrsName+'.t_CACODIMP'
    this.oPgFRm.Page1.oPag.oCACODCOM_2_4.controlsource=this.cTrsName+'.t_CACODCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOM_2_6.controlsource=this.cTrsName+'.t_DESCOM'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(63)
    this.AddVLine(168)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.COM_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_ATTI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.COM_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COM_ATTI_IDX,2])
      *
      * insert into COM_ATTI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'COM_ATTI')
        i_extval=cp_InsertValODBCExtFlds(this,'COM_ATTI')
        i_cFldBody=" "+;
                  "(CASERIAL,CPROWORD,CACODIMP,CACODCOM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CASERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_CACODIMP)+","+cp_ToStrODBC(this.w_CACODCOM)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'COM_ATTI')
        i_extval=cp_InsertValVFPExtFlds(this,'COM_ATTI')
        cp_CheckDeletedKey(i_cTable,0,'CASERIAL',this.w_CASERIAL,'CACODCOM',this.w_CACODCOM)
        INSERT INTO (i_cTable) (;
                   CASERIAL;
                  ,CPROWORD;
                  ,CACODIMP;
                  ,CACODCOM;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CASERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_CACODIMP;
                  ,this.w_CACODCOM;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.COM_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COM_ATTI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) and not(Empty(t_CACODIMP))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'COM_ATTI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CACODCOM="+cp_ToStrODBC(&i_TN.->CACODCOM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'COM_ATTI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CACODCOM=&i_TN.->CACODCOM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) and not(Empty(t_CACODIMP))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CACODCOM="+cp_ToStrODBC(&i_TN.->CACODCOM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CACODCOM=&i_TN.->CACODCOM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace CACODCOM with this.w_CACODCOM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update COM_ATTI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'COM_ATTI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CACODIMP="+cp_ToStrODBCNull(this.w_CACODIMP)+;
                     ",CACODCOM="+cp_ToStrODBC(this.w_CACODCOM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CACODCOM="+cp_ToStrODBC(CACODCOM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'COM_ATTI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CACODIMP=this.w_CACODIMP;
                     ,CACODCOM=this.w_CACODCOM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CACODCOM=&i_TN.->CACODCOM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.COM_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COM_ATTI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) and not(Empty(t_CACODIMP))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete COM_ATTI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CACODCOM="+cp_ToStrODBC(&i_TN.->CACODCOM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CACODCOM=&i_TN.->CACODCOM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) and not(Empty(t_CACODIMP))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COM_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COM_ATTI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CACODIMP<>.w_CACODIMP
          .w_CACODCOM = 0
        endif
        if .o_CACODIMP<>.w_CACODIMP.or. .o_CACODCOM<>.w_CACODCOM
          .w_COCOM = .w_CACODCOM
          .link_2_5('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(7,19,.t.)
          .w_DATFIN = this.oparentobject.w_DATFIN
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_OLDCOMP with this.w_OLDCOMP
      replace t_COCOM with this.w_COCOM
      replace t_DESIMP with this.w_DESIMP
      replace t_CLIIMP with this.w_CLIIMP
      replace t_IMDATINI with this.w_IMDATINI
      replace t_IMDATFIN with this.w_IMDATFIN
      replace t_DTOBSO with this.w_DTOBSO
      replace t_SEDE with this.w_SEDE
      replace t_DTOINI with this.w_DTOINI
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDESCOM_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDESCOM_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oCACODCOM_2_4.visible=!this.oPgFrm.Page1.oPag.oCACODCOM_2_4.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODIMP
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIM',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_CACODIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI,IMCODCON,IMDATINI,IMDATFIN,IM__SEDE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_CACODIMP))
          select IMCODICE,IMDESCRI,IMCODCON,IMDATINI,IMDATFIN,IM__SEDE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODIMP)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IMDESCRI like "+cp_ToStrODBC(trim(this.w_CACODIMP)+"%");

            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI,IMCODCON,IMDATINI,IMDATFIN,IM__SEDE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IMDESCRI like "+cp_ToStr(trim(this.w_CACODIMP)+"%");

            select IMCODICE,IMDESCRI,IMCODCON,IMDATINI,IMDATFIN,IM__SEDE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CACODIMP) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oCACODIMP_2_2'),i_cWhere,'GSAG_MIM',"Impianto",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI,IMCODCON,IMDATINI,IMDATFIN,IM__SEDE";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI,IMCODCON,IMDATINI,IMDATFIN,IM__SEDE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI,IMCODCON,IMDATINI,IMDATFIN,IM__SEDE";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_CACODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_CACODIMP)
            select IMCODICE,IMDESCRI,IMCODCON,IMDATINI,IMDATFIN,IM__SEDE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODIMP = NVL(_Link_.IMCODICE,space(10))
      this.w_DESIMP = NVL(_Link_.IMDESCRI,space(50))
      this.w_CLIIMP = NVL(_Link_.IMCODCON,space(15))
      this.w_IMDATINI = NVL(cp_ToDate(_Link_.IMDATINI),ctod("  /  /  "))
      this.w_IMDATFIN = NVL(cp_ToDate(_Link_.IMDATFIN),ctod("  /  /  "))
      this.w_SEDE = NVL(_Link_.IM__SEDE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CACODIMP = space(10)
      endif
      this.w_DESIMP = space(50)
      this.w_CLIIMP = space(15)
      this.w_IMDATINI = ctod("  /  /  ")
      this.w_IMDATFIN = ctod("  /  /  ")
      this.w_SEDE = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.IMP_MAST_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.IMCODICE as IMCODICE202"+ ",link_2_2.IMDESCRI as IMDESCRI202"+ ",link_2_2.IMCODCON as IMCODCON202"+ ",link_2_2.IMDATINI as IMDATINI202"+ ",link_2_2.IMDATFIN as IMDATFIN202"+ ",link_2_2.IM__SEDE as IM__SEDE202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on COM_ATTI.CACODIMP=link_2_2.IMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and COM_ATTI.CACODIMP=link_2_2.IMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOM
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON,IMDTOBSO,IMDTOINI";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_COCOM);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_CACODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_CACODIMP;
                       ,'CPROWNUM',this.w_COCOM)
            select IMCODICE,CPROWNUM,IMDESCON,IMDTOBSO,IMDTOINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOM = NVL(_Link_.CPROWNUM,0)
      this.w_DESCOM = NVL(_Link_.IMDESCON,space(50))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.IMDTOBSO),ctod("  /  /  "))
      this.w_DTOINI = NVL(cp_ToDate(_Link_.IMDTOINI),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCOM = 0
      endif
      this.w_DESCOM = space(50)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_DTOINI = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCACODCOM_2_4.value==this.w_CACODCOM)
      this.oPgFrm.Page1.oPag.oCACODCOM_2_4.value=this.w_CACODCOM
      replace t_CACODCOM with this.oPgFrm.Page1.oPag.oCACODCOM_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODIMP_2_2.value==this.w_CACODIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODIMP_2_2.value=this.w_CACODIMP
      replace t_CACODIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODIMP_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOM_2_6.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOM_2_6.value=this.w_DESCOM
      replace t_DESCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCOM_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'COM_ATTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CPROWORD)) and not(Empty(.w_CACODIMP))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CACODIMP = this.w_CACODIMP
    this.o_CACODCOM = this.w_CACODCOM
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) and not(Empty(t_CACODIMP)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_CACODIMP=space(10)
      .w_OLDCOMP=space(50)
      .w_CACODCOM=0
      .w_COCOM=0
      .w_DESCOM=space(50)
      .w_DESIMP=space(50)
      .w_CLIIMP=space(15)
      .w_IMDATINI=ctod("  /  /  ")
      .w_IMDATFIN=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_SEDE=space(5)
      .w_DTOINI=ctod("  /  /  ")
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_CACODIMP))
        .link_2_2('Full')
      endif
      .DoRTCalc(4,4,.f.)
        .w_CACODCOM = 0
        .w_COCOM = .w_CACODCOM
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_COCOM))
        .link_2_5('Full')
      endif
    endwith
    this.DoRTCalc(7,21,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CACODIMP = t_CACODIMP
    this.w_OLDCOMP = t_OLDCOMP
    this.w_CACODCOM = t_CACODCOM
    this.w_COCOM = t_COCOM
    this.w_DESCOM = t_DESCOM
    this.w_DESIMP = t_DESIMP
    this.w_CLIIMP = t_CLIIMP
    this.w_IMDATINI = t_IMDATINI
    this.w_IMDATFIN = t_IMDATFIN
    this.w_DTOBSO = t_DTOBSO
    this.w_SEDE = t_SEDE
    this.w_DTOINI = t_DTOINI
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CACODIMP with this.w_CACODIMP
    replace t_OLDCOMP with this.w_OLDCOMP
    replace t_CACODCOM with this.w_CACODCOM
    replace t_COCOM with this.w_COCOM
    replace t_DESCOM with this.w_DESCOM
    replace t_DESIMP with this.w_DESIMP
    replace t_CLIIMP with this.w_CLIIMP
    replace t_IMDATINI with this.w_IMDATINI
    replace t_IMDATFIN with this.w_IMDATFIN
    replace t_DTOBSO with this.w_DTOBSO
    replace t_SEDE with this.w_SEDE
    replace t_DTOINI with this.w_DTOINI
    if i_srv='A'
      replace CACODCOM with this.w_CACODCOM
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mcpPag1 as StdContainer
  Width  = 622
  height = 107
  stdWidth  = 622
  stdheight = 107
  resizeXpos=275
  resizeYpos=58
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=13, top=8, width=604,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="CPROWORD",Label1="Riga",Field2="CACODIMP",Label2="Impianto",Field3="DESCOM",Label3="Componente impianto",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49479046

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=3,top=27,;
    width=600+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*3*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=4,top=28,width=599+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*3*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='IMP_MAST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCACODCOM_2_4.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='IMP_MAST'
        oDropInto=this.oBodyCol.oRow.oCACODIMP_2_2
    endcase
    return(oDropInto)
  EndFunc


  add object oCACODCOM_2_4 as StdTrsField with uid="FRYADKJASK",rtseq=5,rtrep=.t.,;
    cFormVar="w_CACODCOM",value=0,enabled=.f.,isprimarykey=.t.,;
    HelpContextID = 238465421,;
    cTotal="", bFixedPos=.t., cQueryName = "CACODCOM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=88, Left=637, Top=27

  func oCACODCOM_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.T.)
    endwith
    endif
  endfunc

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsag_mcpBodyRow as CPBodyRowCnt
  Width=590
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="RTALIIEJWF",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 17150614,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oCACODIMP_2_2 as StdTrsField with uid="BZKZTRXBHT",rtseq=3,rtrep=.t.,;
    cFormVar="w_CACODIMP",value=space(10),;
    ToolTipText = "Codice impianto",;
    HelpContextID = 137802122,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=102, Left=50, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", cZoomOnZoom="GSAG_MIM", oKey_1_1="IMCODICE", oKey_1_2="this.w_CACODIMP"

  func oCACODIMP_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
      if .not. empty(.w_COCOM)
        bRes2=.link_2_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCACODIMP_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCACODIMP_2_2.mZoom
    do GSAG_KMI with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oCACODIMP_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_CACODIMP
    i_obj.ecpSave()
  endproc

  add object oDESCOM_2_6 as StdTrsField with uid="QLXHBIQTHC",rtseq=7,rtrep=.t.,;
    cFormVar="w_DESCOM",value=space(50),;
    HelpContextID = 59878858,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=431, Left=154, Top=0, InputMask=replicate('X',50), bHasZoom = .t. 

  func oDESCOM_2_6.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_CACODIMP))
    endwith
  endfunc

  proc oDESCOM_2_6.mBefore
    with this.Parent.oContained
      .w_OLDCOMP=.w_DESCOM
    endwith
  endproc

  proc oDESCOM_2_6.mAfter
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_CACODIMP,.w_DESCOM,.w_OLDCOMP,CHR(87)+"_CACODCOM","M")
      endwith
  endproc

  proc oDESCOM_2_6.mZoom
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_CACODIMP,.w_DESCOM,.w_OLDCOMP,CHR(87)+"_CACODCOM","M",.T.)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=2
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mcp','COM_ATTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CASERIAL=COM_ATTI.CASERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_mcp
FUNCTION TESTDATAIMPIANTO( w_DATINI, w_IMDATINI, w_IMDATFIN)
   IF NOT EMPTY( NVL( w_DATINI, CTOD( "  -  -  " ) ) )
     IF NOT EMPTY( NVL( w_IMDATINI, CTOD( "  -  -  " ) ) )
       IF w_IMDATINI > w_DATINI
         RETURN .F.
       ENDIF
     ENDIF
     IF NOT EMPTY( NVL( w_IMDATFIN, CTOD( "  -  -  " ) ) )
       IF w_IMDATFIN < w_DATINI
         RETURN .F.
       ENDIF
     ENDIF
   ENDIF
RETURN .T.
ENDFUNC
* --- Fine Area Manuale
