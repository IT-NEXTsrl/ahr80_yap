* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bag                                                        *
*              Gestione agenda                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-18                                                      *
* Last revis.: 2015-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bag",oParentObject,m.pAZIONE)
return(i_retval)

define class tgsag_bag as StdBatch
  * --- Local variables
  pAZIONE = space(10)
  w_MaxCnt = 0
  w_Oggetto = space(30)
  w_CURDIR = space(254)
  w_OraAtti = 0
  w_nBaseCal = 0
  w_Appoggio_DatiPratica = space(0)
  w_RisEste = space(0)
  w_Stringa_Date = space(200)
  w_DECTOT = 0
  w_DECUNI = 0
  w_RETVAL = 0
  w_TIPOPE = space(1)
  w_OnKey_PGUP = space(1)
  w_OLSERIAL = space(20)
  w_OLDDATAT = space(0)
  w_OraAtti = 0
  w_DurAtti = 0
  ind = 0
  w_PartSerial = space(20)
  w_TextPart = space(254)
  w_TextRisorse = space(254)
  w_LockTimes = .f.
  w_ColSca = 0
  w_nDay = 0
  w_Text = space(254)
  w_ToDay = ctod("  /  /  ")
  w_ADatIni = ctot("")
  w_ADatFin = ctot("")
  w_Continua = .f.
  w_DtIni = ctot("")
  w_DtFin = ctot("")
  w_BRISE = .f.
  w_Oggetto = space(30)
  w_nDay = 0
  w_Text = space(254)
  w_ToDay = ctod("  /  /  ")
  w_ADatIni = ctot("")
  w_ADatFin = ctot("")
  w_Continua = .f.
  w_DtIni = ctot("")
  w_DtFin = ctot("")
  w_HOUR = space(2)
  w_nDay = 0
  w_nMonth = 0
  w_DtIni = ctot("")
  w_DtFin = ctot("")
  w_Title = space(254)
  w_OBJECT = .NULL.
  w_OBJ = .NULL.
  w_OBJECT = .NULL.
  w_DurAtti = 0
  ind = 0
  w_Side = 0
  w_Sav_Ini = ctot("")
  w_Sav_Fin = ctot("")
  AttiOgge = space(10)
  AttiLocali = space(10)
  AttiCodEnte = space(10)
  AttiEnte = space(10)
  AttiCodUff = space(10)
  AttiUfficio = space(10)
  AttiNote = space(0)
  CodPra = space(10)
  PraOgge = space(10)
  PraNome = space(10)
  PraRuolo = space(10)
  PraRiferi = space(10)
  PraCodUbi = space(10)
  PraUbica = space(10)
  PraNote = space(10)
  w_CODIMP = space(10)
  w_CENCOST = space(15)
  w_NOMINAT = space(15)
  w_TELEFO = space(18)
  w_COMRIC = space(15)
  w_CODNOMIN = space(15)
  w_DESNOMIN = space(60)
  w_ATFLATRI = space(1)
  w_RISMSG = space(200)
  w_BRISE = .f.
  w_IMPCOD = space(10)
  w_DESCOMP = space(50)
  w_ATCAITER = space(20)
  w_ATDATINI = ctot("")
  w_ATDATFIN = ctot("")
  w_OLDDATINI = ctot("")
  w_OLDDATFIN = ctot("")
  w_ATOGGETT = space(254)
  w_ATSERIAL = space(20)
  w_ATCODPRA = space(15)
  w_ATFLRICO = space(1)
  w_ATSTATUS = space(1)
  w_OLDSTATUS = space(1)
  w_ATFLPROM = space(1)
  w_ATPROMEM = ctot("")
  w_ATFLNOTI = space(1)
  w_DataCambiata = .f.
  w_DATINI_CHANGED = space(1)
  w_USCITA = space(1)
  w_DESCAN = space(100)
  w_CodRis = space(5)
  GG_PREAV = 0
  STATO = space(1)
  PRIORITA = 0
  TIPOLOGIA = space(5)
  PROMEM = ctot("")
  DISPONIBILITA = 0
  FL_NON_SOG_PRE = space(1)
  OREPRO = space(2)
  MINPRO = space(2)
  w_NewSerial = space(20)
  w_DataAppoPromem = ctot("")
  w_CAFLPREA = space(1)
  w_CAMINPRE = 0
  w_GruPre = space(5)
  w_Data1 = ctot("")
  w_Data2 = ctot("")
  w_OBJ = .NULL.
  w_RESULT = space(254)
  w_SERATT = space(10)
  w_DatItem = 0
  w_TxtItem = space(100)
  w_StatusItem = 0
  w_BMPItem = 0
  w_DatIniItem = 0
  w_DatFinItem = 0
  w_StatoItem = space(1)
  w_CAUATT = space(20)
  w_RAGGST = space(1)
  w_AGGIORNA = .f.
  w_PerComp = 0
  w_OBJECT = .NULL.
  w_DESCR_ATT = space(254)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  PRA_ENTI_idx=0
  PRA_UFFI_idx=0
  CAN_TIER_idx=0
  PRA_OGGE_idx=0
  OFF_PART_idx=0
  CAUMATTI_idx=0
  CAU_ATTI_idx=0
  ART_ICOL_idx=0
  DIPENDEN_idx=0
  OFFDATTI_idx=0
  PRA_UBIC_idx=0
  PAR_AGEN_idx=0
  KEY_ARTI_idx=0
  COM_ATTI_idx=0
  IMP_MAST_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --
    * --- Calendari
    * --- Variabili necessarie per la query
    this.w_nBaseCal = VAL(SYS( 11 , cp_CharToDate("01-01-1900") ))
    * --- Leggo dicitura da sostituire a riservata ( in progress diverr� una variabile pubblica)
    * --- Read from PAR_AGEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PADESRIS"+;
        " from "+i_cTable+" PAR_AGEN where ";
            +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PADESRIS;
        from (i_cTable) where;
            PACODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_RISMSG = NVL(cp_ToDate(_read_.PADESRIS),cp_NullValue(_read_.PADESRIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pAZIONE=="INIT"
        * --- Inizializziamo i campi ctList
        *     w_BlankEseguita � dichiarata in Declare Variables
        if !this.oParentObject.w_BlankEseguita
          this.Page_18()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.oParentObject.w_BlankEseguita = .T.
      case this.pAZIONE=="CHANGEDATE" OR this.pAZIONE=="OGGI"
        if this.pAZIONE=="OGGI"
          this.oParentObject.w_DataSel = i_DatSys
        endif
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAZIONE=="NEWDATE"
        if VarType(pNewDate)="D" and Not(Empty(pNewDate))
          this.oParentObject.w_DataSel = pNewDate
        endif
      case this.pAZIONE=="STAMPAAG"
        if !( this.oParentObject.w_CALATTIVO=2 OR this.oParentObject.w_CALATTIVO=5 OR this.oParentObject.w_CALATTIVO=6) and Ah_yesno("Si desidera eseguire la stampa grafica?")
          This.oparentobject.Notifyevent("StampaGraf")
        else
          this.Page_9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pAZIONE=="SELEZIONATO" OR this.pAZIONE=="DESELEZIONATO" OR this.pAZIONE=="SELEZIONATO_KRA" 
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="MODIFICA"
        * --- Apre l'attivit� solo se � stato selezionato almeno un elemento
        if !EMPTY(this.oParentObject.w_Serial)
          this.Page_11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pAZIONE=="CARICA"
        this.Page_10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAZIONE=="ADDAPP" OR this.pAZIONE=="ADDDB"
        * --- Con parametro ADDAPP chiamato da GSAG_KAG e con ADDDB da GSAG_BEX
        this.Page_16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="ATTIVA1"
        * --- Abilito il selezionato
        this.oParentObject.w_calAttivo = 1
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="ATTIVA5"
        this.oParentObject.w_calAttivo = 2
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="ATTIVA7"
        this.oParentObject.w_calAttivo = 3
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="ATTIVA14"
        this.oParentObject.w_calAttivo = 4
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="ATTIVA31"
        this.oParentObject.w_calAttivo = 5
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="ATTIVA365"
        this.oParentObject.w_calAttivo = 6
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="+"
        * --- Incrementa
        do case
          case this.oParentObject.w_CalAttivo = 1
            * --- 1 giorno
            this.oParentObject.w_DataSel = this.oParentObject.w_DataSel + 1
          case this.oParentObject.w_CalAttivo = 2 or this.oParentObject.w_CalAttivo = 3
            * --- Settimana lavorativa o completa
            this.oParentObject.w_DataSel = this.oParentObject.w_DataSel + 7
          case this.oParentObject.w_CalAttivo = 4
            * --- Bisettimanale
            this.oParentObject.w_DataSel = this.oParentObject.w_DataSel + 14
          case this.oParentObject.w_CalAttivo = 5
            * --- Mensile
            this.oParentObject.w_DataSel = GOMONTH( this.oParentObject.w_DataSel, +1)
          case this.oParentObject.w_CalAttivo = 6
            * --- Annuale
            this.oParentObject.w_DataSel = GOMONTH( this.oParentObject.w_DataSel, IIF(i_bMobileMode, +4 , +12) )
        endcase
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="-"
        * --- Decrementa
        do case
          case this.oParentObject.w_CalAttivo = 1
            * --- 1 giorno
            this.oParentObject.w_DataSel = this.oParentObject.w_DataSel - 1
          case this.oParentObject.w_CalAttivo = 2 or this.oParentObject.w_CalAttivo = 3
            * --- Settimana lavorativa o completa
            this.oParentObject.w_DataSel = this.oParentObject.w_DataSel - 7
          case this.oParentObject.w_CalAttivo = 4
            * --- Bisettimanale
            this.oParentObject.w_DataSel = this.oParentObject.w_DataSel - 14
          case this.oParentObject.w_CalAttivo = 5
            * --- Mensile
            this.oParentObject.w_DataSel = GOMONTH( this.oParentObject.w_DataSel, -1)
          case this.oParentObject.w_CalAttivo = 6
            * --- Annuale
            this.oParentObject.w_DataSel = GOMONTH( this.oParentObject.w_DataSel, IIF(i_bMobileMode, -4 , -12))
        endcase
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="DICITURA"
        * --- Avvalora la label calcolata
        * --- iif(w_calAttivo=1, 
        * --- ,Ah_MsgFormat("Agenda dal "+dtoc(w_Data_Ini)+" al "+dtoc(w_Data_Fin))))
        do case
          case this.oParentObject.w_CalAttivo = 1
            * --- 1 giorno
            if !EMPTY(this.oParentObject.w_DataSel)
              i_retcode = 'stop'
              i_retval = ALLTRIM(g_GIORNO[DOW(this.oParentObject.w_DataSel)])+" "+ALLTRIM(STR(DAY(this.oParentObject.w_DataSel)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_DataSel)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_DataSel)))
              return
            endif
          case this.oParentObject.w_CalAttivo = 2 or this.oParentObject.w_CalAttivo = 3
            * --- Settimana lavorativa o completa
            if !EMPTY(this.oParentObject.w_Data_Ini)
              if MONTH(this.oParentObject.w_DATA_INI)=MONTH(this.oParentObject.w_DATA_FIN)
                i_retcode = 'stop'
                i_retval = ALLTRIM(STR(DAY(this.oParentObject.w_Data_Ini)))+" - "+ALLTRIM(STR(DAY(this.oParentObject.w_Data_Fin)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Ini)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Ini)))
                return
              else
                if YEAR(this.oParentObject.w_DATA_INI)=YEAR(this.oParentObject.w_DATA_FIN)
                  i_retcode = 'stop'
                  i_retval = ALLTRIM(STR(DAY(this.oParentObject.w_Data_Ini)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Ini)])+" - "+ALLTRIM(STR(DAY(this.oParentObject.w_Data_Fin)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Fin)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Fin)))
                  return
                else
                  i_retcode = 'stop'
                  i_retval = ALLTRIM(STR(DAY(this.oParentObject.w_Data_Ini)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Ini)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Ini)))+" - "+ALLTRIM(STR(DAY(this.oParentObject.w_Data_Fin)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Fin)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Fin)))
                  return
                endif
              endif
            endif
          case this.oParentObject.w_CalAttivo = 4
            * --- Bisettimanale
            if !EMPTY(this.oParentObject.w_Data_Ini)
              if YEAR(this.oParentObject.w_DATA_INI)=YEAR(this.oParentObject.w_DATA_FIN)
                i_retcode = 'stop'
                i_retval = ALLTRIM(STR(DAY(this.oParentObject.w_Data_Ini)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Ini)])+" - "+ALLTRIM(STR(DAY(this.oParentObject.w_Data_Fin)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Fin)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Fin)))
                return
              else
                i_retcode = 'stop'
                i_retval = ALLTRIM(STR(DAY(this.oParentObject.w_Data_Ini)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Ini)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Ini)))+" - "+ALLTRIM(STR(DAY(this.oParentObject.w_Data_Fin)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Fin)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Fin)))
                return
              endif
            endif
          case this.oParentObject.w_CalAttivo = 5
            * --- Mensile
            if !EMPTY(this.oParentObject.w_DataSel)
              i_retcode = 'stop'
              i_retval = ALLTRIM(g_MESE[MONTH(this.oParentObject.w_DataSel)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_DataSel)))
              return
            endif
          case this.oParentObject.w_CalAttivo = 6
            * --- Annuale
            if !EMPTY(this.oParentObject.w_DataSel)
              if i_bMobileMode
                if this.oParentObject.w_ctYear.CAL.StartMonth>9
                  i_retcode = 'stop'
                  i_retval = ALLTRIM(g_MESE[this.oParentObject.w_ctYear.CAL.StartMonth])+" "+ALLTRIM(STR( this.oParentObject.w_ctYear.CAL.StartYear ) )+" - "+ALLTRIM(g_MESE[this.oParentObject.w_ctYear.CAL.StartMonth-12+3])+" "+ALLTRIM(STR( this.oParentObject.w_ctYear.CAL.StartYear + 1 ) )
                  return
                else
                  i_retcode = 'stop'
                  i_retval = ALLTRIM(g_MESE[this.oParentObject.w_ctYear.CAL.StartMonth])+" - "+ALLTRIM(g_MESE[this.oParentObject.w_ctYear.CAL.StartMonth+3])+" "+ALLTRIM(STR( this.oParentObject.w_ctYear.CAL.StartYear ) )
                  return
                endif
              else
                i_retcode = 'stop'
                i_retval = Ah_MsgFormat("Anno ")+ALLTRIM(STR(YEAR(this.oParentObject.w_DataSel)))
                return
              endif
            endif
        endcase
      case this.pAzione=="MODIFICATXT" OR this.pAzione=="MODIFICAORA" OR this.pAzione=="MODIFICADAOUTLOOK"
        * --- Modifica il testo di un appuntamento selezionato
        this.Page_15()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="GIORNOSEL"
        * --- Giorno selezionato in visualizzazione MESE o ANNO
        if this.oParentObject.w_CalAttivo = 3
          this.Page_17()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.Page_20()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pAzione=="MEMO"
        * --- Riempiamo i memo solo in presenza di visualizzazione 1-5-7 giorni
        if this.oParentObject.w_calAttivo=1 OR this.oParentObject.w_calAttivo=2 OR this.oParentObject.w_calAttivo=3
          this.Page_13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_19()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_calAttivo=4 OR this.oParentObject.w_calAttivo=5 OR this.oParentObject.w_calAttivo=6
          this.oParentObject.w_ctList_Memo.SetVisible(.F.)     
          this.oParentObject.w_ctDate.SetVisible(.F.)     
        endif
      case this.pAzione=="RICERCA"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if !oParentObject.oPgFrm.ActivePage = 1
          oParentObject.oPgFrm.ActivePage = 1
        endif
      case this.pAzione=="CHECKATTIVO" OR this.pAzione=="CHECKDISATTIVO"
        this.Page_21()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="VISNOTEMEMO"
        * --- Il check ha effetto solo ctList_giorno � visualizzato
        if this.oParentObject.w_CalAttivo<=4
          this.oParentObject.w_ctList_Memo.VisNote(this.oParentObject.w_VisNote=="S")     
        else
          this.oParentObject.w_ctList_Giorno.VisNote(this.oParentObject.w_VisNote=="S")     
        endif
      case this.pAzione=="CHANGENOUPDATE"
        this.oParentObject.w_ctDropDate.SetDate(this.oParentObject.w_dataSel)     
      case this.pAzione=="CANCELLAAPP"
        * --- Cancella l'eventuale appuntamento selezionato
        if (this.oParentObject.w_CalAttivo=1 OR this.oParentObject.w_CalAttivo=5 OR this.oParentObject.w_CalAttivo=99) AND !EMPTY(this.oParentObject.w_Serial)
          this.Page_22()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_CalAttivo=99
            * --- Proveniamo da calendario di pi� partecipanti: non deve riempire l'agenda ma notifica di aggiornarsi
            oParentObject.NotifyEvent("ChangeDate")
          else
            * --- Calendario attivit�: aggiorna
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      case this.pAzione=="RIGHTCLICK"
        if i_VisualTheme = -1
          L_RETVAL=0
          DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT
          DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Evadi")
          DEFINE BAR 2 OF popCmd PROMPT ah_MsgFormat("Completa")
          DEFINE BAR 3 OF popCmd PROMPT ah_MsgFormat("Rinvio") SKIP FOR !IsAlt()
          DEFINE BAR 4 OF popCmd PROMPT ah_MsgFormat("Riserva") SKIP FOR !IsAlt()
          DEFINE BAR 5 OF popCmd PROMPT ah_MsgFormat("Sposta")
          DEFINE BAR 6 OF popCmd PROMPT ah_MsgFormat("Gen. Collegata")
          DEFINE BAR 7 OF popCmd PROMPT ah_MsgFormat("Completa e gen. collegata")
          DEFINE BAR 8 OF popCmd PROMPT ah_MsgFormat("Evadi e gen collegata")
          DEFINE BAR 9 OF popCmd PROMPT ah_MsgFormat("Elimina")
          DEFINE BAR 10 OF popCmd PROMPT ah_MsgFormat("Apri pratica ") SKIP FOR !IsAlt()
          DEFINE BAR 11 OF popCmd PROMPT ah_MsgFormat("Documenti ") FOR IsAlt()
          ON SELECTION BAR 1 OF popCmd L_RETVAL = 1
          ON SELECTION BAR 2 OF popCmd L_RETVAL = 2
          ON SELECTION BAR 3 OF popCmd L_RETVAL = 3
          ON SELECTION BAR 4 OF popCmd L_RETVAL = 4
          ON SELECTION BAR 5 OF popCmd L_RETVAL = 5
          ON SELECTION BAR 6 OF popCmd L_RETVAL = 6
          ON SELECTION BAR 7 OF popCmd L_RETVAL = 7
          ON SELECTION BAR 8 OF popCmd L_RETVAL = 8
          ON SELECTION BAR 9 OF popCmd L_RETVAL = 9
          ON SELECTION BAR 10 OF popCmd L_RETVAL = 10
          ON SELECTION BAR 11 OF popCmd L_RETVAL = 11
          ACTIVATE POPUP popCmd
          DEACTIVATE POPUP popCmd
          RELEASE POPUPS popCmd
          this.w_RETVAL = L_RETVAL
          Release L_RETVAL
        else
          local oBtnMenu, oMenuItem
          oBtnMenu = CreateObject("cbPopupMenu")
          oBtnMenu.oParentObject = This
          oBtnMenu.oPopupMenu.cOnClickProc="ExecScript(NAMEPROC)"
          oMenuItem = oBtnMenu.AddMenuItem()
          oMenuItem.Caption = Ah_Msgformat("Evadi")
          oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=1"
          oMenuItem.Visible=.T.
          oMenuItem = oBtnMenu.AddMenuItem()
          oMenuItem.Caption = Ah_Msgformat("Completa")
          oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=2"
          oMenuItem.Visible=.T.
          if IsAlt()
            oMenuItem = oBtnMenu.AddMenuItem()
            oMenuItem.Caption = Ah_Msgformat("Rinvio")
            oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=3"
            oMenuItem.Visible=.T.
            oMenuItem = oBtnMenu.AddMenuItem()
            oMenuItem.Caption = Ah_Msgformat("Riserva")
            oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=4"
            oMenuItem.Visible=.T.
          endif
          oMenuItem = oBtnMenu.AddMenuItem()
          oMenuItem.Caption = Ah_Msgformat("Sposta")
          oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=5"
          oMenuItem.Visible=.T.
          oMenuItem = oBtnMenu.AddMenuItem()
          oMenuItem.Caption = Ah_Msgformat("Gen. Collegata")
          oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=6"
          oMenuItem.Visible=.T.
          oMenuItem = oBtnMenu.AddMenuItem()
          oMenuItem.Caption = Ah_Msgformat("Completa e gen. collegata")
          oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=7"
          oMenuItem.Visible=.T.
          oMenuItem = oBtnMenu.AddMenuItem()
          oMenuItem.Caption = Ah_Msgformat("Evadi e gen collegata")
          oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=8"
          oMenuItem.Visible=.T.
          oMenuItem = oBtnMenu.AddMenuItem()
          oMenuItem.Caption = Ah_Msgformat("Elimina")
          oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=9"
          oMenuItem.Visible=.T.
          if IsAlt()
            oMenuItem = oBtnMenu.AddMenuItem()
            oMenuItem.Caption = Ah_Msgformat("Apri pratica")
            oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=10"
            oMenuItem.Visible=.T.
          else
            oMenuItem = oBtnMenu.AddMenuItem()
            oMenuItem.Caption = Ah_Msgformat("Documenti")
            oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=11"
            oMenuItem.Visible=.T.
          endif
          oBtnMenu.InitMenu()
          oBtnMenu.ShowMenu()
          oBtnMenu = .NULL.
        endif
        this.w_TIPOPE = icase(this.w_RETVAL=1,"D",this.w_RETVAL=2,"C",this.w_RETVAL=3,"R",this.w_RETVAL=4,"T",this.w_RETVAL=5,"S",this.w_RETVAL=6,"J",this.w_RETVAL=7,"W",this.w_RETVAL=8,"B",this.w_RETVAL=9,"E",this.w_RETVAL=10,"A","K")
        GSZM_BAT(this,this.w_TIPOPE,this.oParentObject.w_SERIAL)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oparentobject.Notifyevent("Ricalcola")
      case this.pAzione=="GOTFOCUS" OR this.pAzione=="LOSTFOCUS"
        * --- Abilita/disabilita pagina su/gi�
        this.w_OnKey_PGUP = ON("KEY","PGUP")
        * --- pAzione=='GOTFOCUS' OR pAzione=='LOSTFOCUS'
        * --- Deve impostare la gestione delle 
        do case
          case this.pAzione=="GOTFOCUS" AND !EMPTY(this.w_OnKey_PGUP)
            * --- Focus a CtMDay e vale ancora la On Key da disabilitare
            PUSH KEY
            ON KEY LABEL PGUP 
            ON KEY LABEL PGDN
          case this.pAzione=="LOSTFOCUS" AND EMPTY(this.w_OnKey_PGUP)
            * --- CtMDay ha perso il focus e deve riabilitare la On Key
            POP KEY
        endcase
      case this.pAzione=="TMRBALLOON" and g_typebalagen="B"
        this.oParentObject.w_oTmrBalloon.Enabled = .F.
        this.w_OLSERIAL = this.oParentObject.w_Serial
        this.oParentObject.w_Serial = this.oParentObject.w_OREFCALE.AppointKeyID(this.oParentObject.w_ATAPPOVR)
        this.w_OLDDATAT = this.oParentObject.w_DatiAttivita
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Visualizzo il balloon alla posizione del mouse (Primo parametro = 6) con icona informazione (Ultimo parametro = 1)
        this.oParentObject.oBalloon.ctlshow(6 , this.w_Stringa_Date+this.w_Appoggio_DatiPratica+CHR(10)+this.oParentObject.w_DatiAttivita , ah_MsgFormat("Informazioni attivit�") , 1)     
        this.oParentObject.w_DatiAttivita = this.w_OLDDATAT
        this.oParentObject.w_Serial = this.w_OLSERIAL
      case this.pAzione=="EXPAND" OR this.pAzione=="REDUCE"
        if this.oParentObject.w_FLCALEXP
          this.oParentObject.w_ct1Day.Width = this.oParentObject.w_ctDate.Left - this.oParentObject.w_ct1Day.Left
          this.oParentObject.w_ct5Day.Width = this.oParentObject.w_ctDate.Left - this.oParentObject.w_ct1Day.Left
          this.oParentObject.w_ctList_Memo.SetVisible(.T.)     
          this.oParentObject.w_ctDate.SetVisible(.T.)     
          this.Page_13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_19()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if this.oParentObject.w_calAttivo=1
            this.oParentObject.w_ct1Day.Width = this.oParentObject.Width - 8
          else
            this.oParentObject.w_ct5Day.Width = this.oParentObject.Width - 8
          endif
          this.oParentObject.w_ctList_Memo.SetVisible(.F.)     
          this.oParentObject.w_ctDate.SetVisible(.F.)     
        endif
        this.oParentObject.w_FLCALEXP = !this.oParentObject.w_FLCALEXP
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riempimento agenda: prima dobbiamo pulire tutti i calendari
    this.oParentObject.w_Serial = space(20)
    this.Page_8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oParentObject.w_ctDropDate.SetDate(this.oParentObject.w_dataSel)     
    do case
      case this.oParentObject.w_CalAttivo = 1
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_CalAttivo = 2
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_CalAttivo = 3
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_CalAttivo = 4
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_CalAttivo = 5
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_CalAttivo = 6
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    if this.oParentObject.w_CalAttivo=1 OR this.oParentObject.w_CalAttivo=2 OR this.oParentObject.w_CalAttivo=3
      * --- Riempiamo i memo solo in presenza di visualizzazione 1-5-7 giorni
      this.Page_13()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_19()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.oParentObject.w_ctList_Giorno.SetVisible(.T.)     
      this.oParentObject.w_DataApp = this.oParentObject.w_DataSel
      this.Page_20()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if NVL(ALLTRIM(this.oParentObject.w_CauDefault),"")=="" AND this.oParentObject.w_AskCau=1
      ah_ErrorMsg("Attenzione, non � stato specificato un tipo nella tabella parametri attivit�")
      this.oParentObject.w_AskCau = 0
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazioni
    this.oParentObject.w_ct1Day.SetDate(this.oParentObject.w_DataSel)     
    this.oParentObject.w_Data_Ini = DTOT(this.oParentObject.w_DataSel)
    this.oParentObject.w_Data_Fin = DTOT(this.oParentObject.w_DataSel)+3600*24-1
    this.w_MaxCnt = 1
    * --- Select from QUERY\GSAG_BAG
    do vq_exec with 'QUERY\GSAG_BAG',this,'_Curs_QUERY_GSAG_BAG','',.f.,.t.
    if used('_Curs_QUERY_GSAG_BAG')
      select _Curs_QUERY_GSAG_BAG
      locate for 1=1
      do while not(eof())
      * --- A pag 23 viene valorizzata la var. w_DESCR_ATT
      this.Page_23()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_Text = ALLTRIM(this.w_DESCR_ATT)
      this.w_LockTimes = .F.
      * --- Aggiorna data
      this.w_ADatIni = VAL(SYS(11,this.oParentObject.w_DataSel)) - this.w_nBaseCal
      if _Curs_QUERY_GSAG_BAG.ATDATINI < this.oParentObject.w_Data_Ini
        * --- L'appuntamento inizia in precedenza
        this.w_OraAtti = 0
        this.w_LockTimes = .T.
      else
        this.w_OraAtti = HOUR(_Curs_QUERY_GSAG_BAG.ATDATINI)*60+MINUTE(_Curs_QUERY_GSAG_BAG.ATDATINI)
      endif
      * --- 3600*24-1
      this.w_DurAtti = 0
      if !ISNULL(_Curs_QUERY_GSAG_BAG.ATDATFIN) AND !EMPTY(_Curs_QUERY_GSAG_BAG.ATDATFIN)
        * --- calcoliamo la durata in minuti
        this.w_DurAtti = ( _Curs_QUERY_GSAG_BAG.ATDATFIN - _Curs_QUERY_GSAG_BAG.ATDATINI ) / 60 
      endif
      if this.w_DurAtti = 0
        this.w_DurAtti = 1
      endif
      * --- Se superiamo la mezzanotte, facciamo arrivare l'appuntamento a mezzanotte.
      *     In questo modo, gestiamo correttamente anche gli appuntamenti che finiscono il giorno successivo
      if this.w_OraAtti + this.w_DurAtti > (60*24)-1
        this.w_DurAtti = (60*24)-1 - this.w_OraAtti
        this.w_LockTimes = .T.
      endif
      this.ind = this.oParentObject.w_ct1Day.AddKeyAppointment(this.w_OraAtti, this.w_OraAtti+this.w_DurAtti, this.w_ADatIni, this.w_Text, _Curs_QUERY_GSAG_BAG.ATSERIAL, this.w_LockTimes, _Curs_QUERY_GSAG_BAG.DISPOCOL, _Curs_QUERY_GSAG_BAG.RIGACOL)
      if _Curs_QUERY_GSAG_BAG.ATSTATUS=="T"
        * --- Attivit� provvisoria
        this.oParentObject.w_ct1Day.SetAppBackColor(this.ind, g_ColAttConf)     
      endif
      * --- Se attivit� riservata la rendo immodificabile...
      if Not IsRisAtt( _Curs_QUERY_GSAG_BAG.ATSERIAL , _Curs_QUERY_GSAG_BAG.ATFLATRI ) 
        * --- Se attivit� riservata maschero alcuni campi...
        *     (in progress la lettura dai parametri potr� essere sostituita da una var. pubblica letta all'avvio)
        this.oParentObject.w_ct1Day.cal.AppointReadOnly( this.ind ) = .t.
        this.oParentObject.w_ct1Day.cal.AppointLockTimes( this.ind ) = .t.
      endif
        select _Curs_QUERY_GSAG_BAG
        continue
      enddo
      use
    endif
    this.oParentObject.w_ct1Day.SetVisible(.T.)     
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazioni
    this.oParentObject.w_ctCalendar.SetDate(this.oParentObject.w_DataSel)     
    Dimension aText(7) 
 store "" to aText(1),aText(2),aText(3),aText(4),aText(5),aText(6),aText(7)
    * --- Inseriamo nell'array aText il testo da inserire nei 5 giorni
    this.oParentObject.w_Data_Ini = DTOT(this.oParentObject.w_DataSel-DOW(this.oParentObject.w_DataSel,2)+1)
    this.oParentObject.w_Data_Fin = DTOT(this.oParentObject.w_DataSel+7-DOW(this.oParentObject.w_DataSel,2))+3600*24-1
    * --- Select from QUERY\GSAG_BAG
    do vq_exec with 'QUERY\GSAG_BAG',this,'_Curs_QUERY_GSAG_BAG','',.f.,.t.
    if used('_Curs_QUERY_GSAG_BAG')
      select _Curs_QUERY_GSAG_BAG
      locate for 1=1
      do while not(eof())
      * --- A pag 23 viene valorizzata la var. w_DESCR_ATT
      this.Page_23()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if ISNULL(_Curs_QUERY_GSAG_BAG.ATDATINI) OR _Curs_QUERY_GSAG_BAG.ATDATINI < this.oParentObject.w_Data_Ini
        * --- L'appuntamento inizia in precedenza
        this.w_DtIni = this.oParentObject.w_Data_Ini
      else
        this.w_DtIni = _Curs_QUERY_GSAG_BAG.ATDATINI
      endif
      if !ISNULL(_Curs_QUERY_GSAG_BAG.ATDATFIN) AND !EMPTY(_Curs_QUERY_GSAG_BAG.ATDATFIN)
        if _Curs_QUERY_GSAG_BAG.ATDATFIN > this.oParentObject.w_Data_Fin
          * --- L'appuntamento finisce dopo
          this.w_DtFin = this.oParentObject.w_Data_Fin
        else
          this.w_DtFin = _Curs_QUERY_GSAG_BAG.ATDATFIN
        endif
      else
        this.w_DtFin = CTOT("  -  -      :  ")
      endif
      if !empty(this.w_DtFin) AND (TTOD(this.w_DtFin) # TTOD(this.w_DtIni))
        * --- L'appuntamento occupa pi� giorni. 
        *     Dovremo inserirne uno per ciascun giorno
        if HOUR(this.w_DtIni)>0 OR MINUTE(this.w_DtIni)>0 
          * --- E' specificata l'ora di inizio
          this.w_HOUR = PADL(ALLTRIM(STR(HOUR(this.w_DtIni))),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DtIni))),2,"0")
        else
          * --- Inizia a mezzanotte
          this.w_HOUR = "00:00"
        endif
        if TTOD(this.w_DtIni) # TTOD(this.w_DtFin)
          * --- Finisce in un giorno successivo, quindi impostiamo come fine a mezzanotte
          this.w_HOUR = this.w_HOUR+" - "+"23:59"
        else
          * --- Finisce nello stesso giorno
          this.w_HOUR = this.w_HOUR+" - "+PADL(ALLTRIM(STR(HOUR(this.w_DtFin))),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DtFin))),2,"0")
        endif
        * --- ------------------------------
        if Not IsRisAtt( _Curs_QUERY_GSAG_BAG.ATSERIAL , _Curs_QUERY_GSAG_BAG.ATFLATRI ) 
          * --- Se attivit� riservata maschero alcuni campi...
          *     (in progress la lettura dai parametri potr� essere sostituita da una var. pubblica letta all'avvio)
          this.w_BRISE = .T.
        else
          this.w_BRISE = .F.
        endif
        * --- Appuntamento solo in un giorno facente parte dell'intervallo
        this.w_Text = this.w_HOUR + " "+ ALLTRIM(this.w_DESCR_ATT) + CHR(13)+ CHR(10)
        * --- Aggiorna testo del giorno
        aText(DOW(TTOD(this.w_DtIni),2)) = aText(DOW(TTOD(this.w_DtIni),2)) + this.w_Text
        * --- Cicliamo fino a che non raggiungiamo la fine dell'intervallo
        this.w_DtIni = cp_CharToDatetime(DTOC(TTOD(this.w_DtIni)+1))
        do while TTOD(this.w_DtIni) <= TTOD(this.w_DtFin)
          this.w_HOUR = "00:00"
          if TTOD(this.w_DtIni) # TTOD(this.w_DtFin)
            * --- L'appuntamento continua, dobbiamo mettere come ora fine la mezzanotte
            this.w_HOUR = this.w_HOUR+" - "+"23:59"
          else
            this.w_HOUR = this.w_HOUR+" - "+PADL(ALLTRIM(STR(HOUR(this.w_DtFin))),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DtFin))),2,"0")
          endif
          this.w_Text = this.w_HOUR + " " + ALLTRIM(this.w_DESCR_ATT) + CHR(13)+ CHR(10)
          aText(DOW(TTOD(this.w_DtIni),2)) = aText(DOW(TTOD(this.w_DtIni),2)) + this.w_Text
          this.w_DtIni = cp_CharToDatetime(DTOC(TTOD(this.w_DtIni)+1))
        enddo
      else
        * --- Appuntamento solo in un giorno facente parte dell'intervallo
        if HOUR(this.w_DtIni)>0 OR MINUTE(this.w_DtIni)>0 
          * --- E' specificata l'ora
          this.w_HOUR = PADL(ALLTRIM(STR(HOUR(this.w_DtIni))),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DtIni))),2,"0")
        else
          * --- Inizia a mezzanotte
          this.w_HOUR = "00:00"
        endif
        if !empty(this.w_DtFin) AND this.w_DtIni # this.w_DtFin
          * --- C'� l'ora di fine
          this.w_HOUR = this.w_HOUR+" - "+PADL(ALLTRIM(STR(HOUR(this.w_DtFin))),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DtFin))),2,"0")
        endif
        this.w_Text = this.w_HOUR + " "+ ALLTRIM(this.w_DESCR_ATT) + CHR(13)+ CHR(10)
        * --- Aggiorna testo del giorno
        aText(DOW(TTOD(this.w_DtIni),2)) = aText(DOW(TTOD(this.w_DtIni),2)) + this.w_Text
      endif
        select _Curs_QUERY_GSAG_BAG
        continue
      enddo
      use
    endif
    this.w_nDay = 1
    do while this.w_nDay<=7
      * --- Giorno considerato
      this.w_Text = aText(this.w_nDay)
      this.w_ToDay = TTOD(this.oParentObject.w_Data_Ini) + this.w_nDay-1
      * --- Aggiorna control
      if Not(Empty(this.w_Text))
        this.oParentObject.w_ctCalendar.SetText(this.w_ToDay,this.w_Text)     
      endif
      * --- Avanza di un giorno
      this.w_nDay = this.w_nDay+1
    enddo
    this.oParentObject.w_ctCalendar.SetVisible(.T.)     
    this.oParentObject.w_DataApp = this.oParentObject.w_DataSel
    this.Page_17()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ctWeek non pi� gestito
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazioni
    this.oParentObject.w_ctMonth.SetDate(this.oParentObject.w_DataSel)     
    this.oParentObject.w_Data_Ini = cp_CharToDatetime("01-"+ALLTRIM(STR(MONTH(this.oParentObject.w_DataSel)))+"-"+ALLTRIM(STR(YEAR(this.oParentObject.w_DataSel)))+" 00:00:00", "dd-mm-yyyy hh:nn:ss")
    * --- Per trovare la data finale, andiamo al giorno precedente il primo giorno del mese successivo
    this.oParentObject.w_Data_Fin = DTOT(GOMONTH(this.oParentObject.w_Data_Ini,1) - 1)+3600*24-1
    * --- Inseriamo nell'array aText il contatore delle ttivit� del giorno, mentre nel secondo valore inseriamo il contatore delle udienze
    this.w_nDay = 1
    Dimension aText(31,2)
    * --- Inizializziamo l'array
    do while this.w_nDay<=31
      * --- Giorno considerato
      aText(this.w_nDay,1) = 0
      aText(this.w_nDay,2) = 0
      * --- Avanza di giorno
      this.w_nDay = this.w_nDay+1
    enddo
    * --- Select from QUERY\GSAG_BAG
    do vq_exec with 'QUERY\GSAG_BAG',this,'_Curs_QUERY_GSAG_BAG','',.f.,.t.
    if used('_Curs_QUERY_GSAG_BAG')
      select _Curs_QUERY_GSAG_BAG
      locate for 1=1
      do while not(eof())
      if ATDATINI < this.oParentObject.w_Data_Ini
        * --- L'appuntamento inizia in precedenza
        this.w_DtIni = this.oParentObject.w_Data_Ini
      else
        this.w_DtIni = ATDATINI
      endif
      if !ISNULL(ATDATFIN) AND !EMPTY(ATDATFIN)
        if ATDATFIN > this.oParentObject.w_Data_Fin
          * --- L'appuntamento finisce dopo
          this.w_DtFin = this.oParentObject.w_Data_Fin
        else
          this.w_DtFin = ATDATFIN
        endif
      else
        this.w_DtFin = CTOT("  -  -      :  ")
      endif
      if Not IsRisAtt( _Curs_QUERY_GSAG_BAG.ATSERIAL , _Curs_QUERY_GSAG_BAG.ATFLATRI ) 
        * --- Se attivit� riservata non si deve vedere udienza
        this.w_BRISE = .T.
      else
        this.w_BRISE = .F.
      endif
      if !empty(this.w_DtFin) AND (TTOD(this.w_DtFin) # TTOD(this.w_DtIni))
        * --- L'appuntamento occupa pi� giorni. 
        *     Dovremo inserirne uno per ciascun giorno
        * --- ------------------------------
        * --- Aggiorna il contatore del giorno
        aText(DAY(this.w_DtIni),1) = aText(DAY(this.w_DtIni),1) + 1
        if CARAGGST="U" And Not this.w_BRISE
          * --- Udienza
          aText(DAY(this.w_DtIni),2) = aText(DAY(this.w_DtIni),2) + 1
        endif
        * --- Cicliamo fino a che non raggiungiamo la fine dell'intervallo
        this.w_DtIni = cp_CharToDatetime(DTOC(TTOD(this.w_DtIni)+1))
        do while TTOD(this.w_DtIni) <= TTOD(this.w_DtFin)
          aText(DAY(this.w_DtIni),1) = aText(DAY(this.w_DtIni),1) + 1
          if CARAGGST="U" And Not this.w_BRISE
            * --- Udienza
            aText(DAY(this.w_DtIni),2) = aText(DAY(this.w_DtIni),2) + 1
          endif
          this.w_DtIni = cp_CharToDatetime(DTOC(TTOD(this.w_DtIni)+1))
        enddo
      else
        * --- Appuntamento solo in un giorno facente parte dell'intervallo
        aText(DAY(this.w_DtIni),1) = aText(DAY(this.w_DtIni),1) + 1
        if CARAGGST="U" And Not this.w_BRISE
          * --- Udienza
          aText(DAY(this.w_DtIni),2) = aText(DAY(this.w_DtIni),2) + 1
        endif
      endif
        select _Curs_QUERY_GSAG_BAG
        continue
      enddo
      use
    endif
    this.w_nDay = 1
    do while this.w_nDay<=31
      * --- Giorno considerato
      if aText(this.w_nDay,1) > 0
        * --- Componiamo il testo per il giorno
        this.w_Text = ALLTRIM(STR(aText(this.w_nDay,1)))+" "+Ah_MsgFormat("attivit�")
        do case
          case aText(this.w_nDay,2) = 1
            this.w_Text = this.w_Text + CHR(13)+CHR(10)+"(1"+Ah_MsgFormat("udienza")+")"
          case aText(this.w_nDay,2) > 1
            this.w_Text = this.w_Text + CHR(13)+CHR(10)+"("+ALLTRIM(STR(aText(this.w_nDay,2)))+" "+Ah_MsgFormat("udienze")+")"
        endcase
        this.oParentObject.w_ctMonth.DateText(this.w_nDay, this.w_text)     
      endif
      * --- Avanza di giorno
      this.w_nDay = this.w_nDay+1
    enddo
    this.oParentObject.w_ctMonth.SetVisible(.T.)     
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazioni
    this.oParentObject.w_ctYear.SetDate(this.oParentObject.w_DataSel)     
    this.oParentObject.w_Data_Ini = cp_CharToDateTime( "01-01-"+ALLTR(STR(YEAR(this.oParentObject.w_DataSel))), "dd-mm-yyyy hh:nn:ss" )
    this.oParentObject.w_Data_Fin = DTOT(GOMONTH(this.oParentObject.w_Data_Ini, 12) - 1)+3600*24-1
    Dimension aText(12,31)
    * --- Select from QUERY\GSAG2BAG
    do vq_exec with 'QUERY\GSAG2BAG',this,'_Curs_QUERY_GSAG2BAG','',.f.,.t.
    if used('_Curs_QUERY_GSAG2BAG')
      select _Curs_QUERY_GSAG2BAG
      locate for 1=1
      do while not(eof())
      if ATDATINI < this.oParentObject.w_Data_Ini
        * --- L'appuntamento inizia in precedenza
        this.w_DtIni = this.oParentObject.w_Data_Ini
      else
        this.w_DtIni = ATDATINI
      endif
      if !ISNULL(ATDATFIN) AND !EMPTY(ATDATFIN)
        if ATDATFIN > this.oParentObject.w_Data_Fin
          * --- L'appuntamento finisce dopo
          this.w_DtFin = this.oParentObject.w_Data_Fin
        else
          this.w_DtFin = ATDATFIN
        endif
      else
        this.w_DtFin = CTOT("  -  -      :  ")
      endif
      if !empty(this.w_DtFin) AND (TTOD(this.w_DtFin) # TTOD(this.w_DtIni))
        * --- L'appuntamento occupa pi� giorni.
        *     Cicliamo fino a che non raggiungiamo la fine dell'intervallo
        do while TTOD(this.w_DtIni) <= TTOD(this.w_DtFin)
          aText(MONTH(this.w_DtIni), DAY(this.w_DtIni)) = "*"
          this.w_DtIni = cp_CharToDatetime(DTOC(TTOD(this.w_DtIni)+1))
        enddo
      else
        * --- Appuntamento solo in un giorno facente parte dell'intervallo
        aText(MONTH(this.w_DtIni), DAY(this.w_DtIni)) = "*"
      endif
        select _Curs_QUERY_GSAG2BAG
        continue
      enddo
      use
    endif
    this.w_nDay = 1
    this.w_nMonth = 1
    do while this.w_nMonth<=12
      this.w_nDay = 1
      do while this.w_nDay<=31
        * --- Giorno considerato
        if ! EMPTY(aText(this.w_nMonth, this.w_nDay))
          this.oParentObject.w_ctYear.DateFont(this.w_nMonth, this.w_nDay)     
        endif
        * --- Avanza di giorno
        this.w_nDay = this.w_nDay+1
      enddo
      this.w_nMonth = this.w_nMonth + 1
    enddo
    this.oParentObject.w_ctYear.SetVisible(.T.)     
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Disabilita tutti i calendari
    if this.oParentObject.Visible
      if this.oParentObject.w_FLCALEXP
        do gsag_bag with this.oparentObject, "REDUCE"
      endif
      * --- Se KSAG_KAG � visibile, nascondiamo prima tutti gli elementi per poi visualizzarli nuovamente
      this.oParentObject.w_ct1Day.SetVisible(.F.)     
      this.oParentObject.w_ct5Day.SetVisible(.F.)     
      this.oParentObject.w_ctCalendar.SetVisible(.F.)     
      this.oParentObject.w_ctMonth.SetVisible(.F.)     
      this.oParentObject.w_ctYear.SetVisible(.F.)     
      this.oParentObject.w_DatiPratica = space(10)
      this.oParentObject.w_DatiAttivita = space(10)
      this.oParentObject.w_DatiAttiPratica = space(10)
      this.oParentObject.w_ctList_Giorno.SetVisible(.F.)     
      this.oParentObject.w_ctList_Memo.SetVisible(.F.)     
      this.oParentObject.w_ctDate.SetVisible(.F.)     
    else
      * --- DESKMENU-la maschera � aggiornata dopo F10 del calendario-non nascondiamo gli elementi
      this.oParentObject.w_ct1Day.ClearCal()     
      this.oParentObject.w_ct5Day.ClearCal()     
      this.oParentObject.w_ctCalendar.ClearCal()     
      this.oParentObject.w_ctMonth.ClearCal()     
      this.oParentObject.w_ctYear.ClearCal()     
      this.oParentObject.w_ctList_Giorno.ClearList()     
      this.oParentObject.w_ctList_Memo.ClearList()     
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CURDIR = SYS(5)+CURDIR()
    this.w_Title = ALLTRIM(CDOW(i_datsys))+" "+ALLTRIM(STR(DAY(i_datSys)))+" "+ALLTRIM(CMONTH(i_datsys))+" "+ALLTRIM(STR(YEAR(i_datSys)))
    do case
      case this.oParentObject.w_CalAttivo =1 OR this.oParentObject.w_CalAttivo =99
        this.oParentObject.w_ct1Day.PrintTitles(Ah_MsgFormat("Stampa agenda giornaliera"), Ah_MsgFormat("Del")+" "+ALLTRIM(STR(DAY(this.oParentObject.w_DataSel)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_DataSel)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_DataSel))))     
        this.oParentObject.w_ct1Day.PrintSchedule()     
      case this.oParentObject.w_CalAttivo =2
        this.oParentObject.w_ct5Day.PrintTitles(Ah_MsgFormat("Stampa agenda lavorativa"), "")     
        this.oParentObject.w_ct5Day.PrintSchedule()     
      case this.oParentObject.w_CalAttivo = 3
        this.oParentObject.w_ctCalendar.PrintTitles(Ah_MsgFormat("Stampa agenda settimanale"), Ah_MsgFormat("Da")+" "+dtoc(this.oParentObject.w_ctCalendar.DayNumber2DayJulian(this.oParentObject.w_ctCalendar.Cal.DateStart))+" "+Ah_MsgFormat("a ")+dtoc(this.oParentObject.w_ctCalendar.DayNumber2DayJulian(this.oParentObject.w_ctCalendar.Cal.DateEnd)))     
        this.oParentObject.w_ctCalendar.PrintCalendar()     
      case this.oParentObject.w_CalAttivo = 4
        ah_ErrorMsg("Opzione non disponibile per agenda bisettimanale")
      case this.oParentObject.w_CalAttivo = 5
        this.oParentObject.w_ctMonth.PrintTitles(Ah_MsgFormat("Stampa agenda mensile"), Ah_MsgFormat("Del mese")+" "+g_Mese[MONTH(this.oParentObject.w_DataSel)])     
        this.oParentObject.w_ctMonth.PrintCalendar()     
      case this.oParentObject.w_CalAttivo = 6
        this.oParentObject.w_ctYear.PrintTitles(Ah_MsgFormat("Stampa agenda annuale"), Ah_MsgFormat("Dell'anno")+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_DataSel))))     
        this.oParentObject.w_ctYear.PrintCalendar()     
    endcase
    cd (this.w_CURDIR)
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Deve caricare una nuova attivit� e restituire nel parametro il seriale
    if !EMPTY(this.oParentObject.w_DataApp)
      this.oParentObject.w_DatiPratica = space(10)
      this.oParentObject.w_DatiAttivita = space(10)
      * --- Instanzio l'anagrafica
      this.w_OBJECT = GSAG_AAT()
      * --- Controllo se ha passato il test di accesso
      if !(this.w_OBJECT.bSec1)
        i_retcode = 'stop'
        return
      endif
      * --- Per il codice utente, restituiamo il codice presente nel file DIPENDEN
      if EMPTY(this.oParentObject.w_CODPART)
        this.w_CodRis = readdipend(i_CodUte, "C")
      else
        * --- La var. w_CODPART � dichiarata nella DECLARE VARIABILES in modo da non essere modificata dalla BlankRec
        this.w_OBJECT.w_CODPART = this.oParentObject.w_CODPART
      endif
      * --- Vado in caricamento
      this.w_OBJECT.ECPLOAD()     
      this.w_OBJECT.w_DATINI = this.oParentObject.w_DataApp
      this.w_Data1 = CTOT(DTOC(this.oParentObject.w_DataApp)+" "+ALLTRIM(STR(INT(this.oParentObject.w_OraStart/60)))+":"+ALLTRIM(STR(MOD(this.oParentObject.w_OraStart,60))))
      this.w_Data2 = CTOT(DTOC(this.oParentObject.w_DataApp)+" "+ALLTRIM(STR(INT(this.oParentObject.w_OraEnd/60)))+":"+ALLTRIM(STR(MOD(this.oParentObject.w_OraEnd,60))))
      this.w_OBJECT.w_ORAINI = PADL(ALLTRIM(STR(INT(this.oParentObject.w_OraStart/60))),2,"0")
      this.w_OBJECT.w_MININI = PADL(ALLTRIM(STR(MOD(this.oParentObject.w_OraStart,60))),2,"0")
      this.w_OBJECT.w_ATDATINI = CTOT(DTOC(this.oParentObject.w_DataApp)+" "+PADL(ALLTRIM(STR(INT(this.oParentObject.w_OraStart/60))),2,"0")+":"+PADL(ALLTRIM(STR(MOD(this.oParentObject.w_OraStart,60))),2,"0"))
      this.w_OBJECT.w_GIOINI = LEFT(g_GIORNO[DOW(this.oParentObject.w_DataApp)],3)
      * --- GIOINI
      this.w_OBJECT.w_DATFIN = this.oParentObject.w_DataApp
      this.w_OBJECT.w_ORAFIN = PADL(ALLTRIM(STR(INT(this.oParentObject.w_OraStart/60))),2,"0")
      this.w_OBJECT.w_MINFIN = PADL(ALLTRIM(STR(MOD(this.oParentObject.w_OraStart,60))),2,"0")
      this.w_OBJECT.w_GIOFIN = LEFT(g_GIORNO[DOW(this.oParentObject.w_DataApp)],3)
      this.w_OBJECT.w_ATDATFIN = CTOT(DTOC(this.oParentObject.w_DataApp)+" 00:00")
      this.w_OBJECT.w_GIOFIN = LEFT(g_GIORNO[DOW(this.oParentObject.w_DataApp)],3)
      if !EMPTY(this.oParentObject.w_OraEnd)
        this.w_OBJECT.w_ORAFIN = PADL(ALLTRIM(STR(INT(this.oParentObject.w_OraEnd/60))),2,"0")
        this.w_OBJECT.w_MINFIN = PADL(ALLTRIM(STR(MOD(this.oParentObject.w_OraEnd,60))),2,"0")
        this.w_OBJECT.w_ATDATFIN = CTOT(DTOC(this.oParentObject.w_DataApp)+" "+PADL(ALLTRIM(STR(INT(this.oParentObject.w_OraEnd/60))),2,"0")+":"+PADL(ALLTRIM(STR(MOD(this.oParentObject.w_OraEnd,60))),2,"0"))
      else
        this.w_OBJECT.w_ORAFIN = PADL(ALLTRIM(STR(INT(this.oParentObject.w_OraStart/60))),2,"0")
        this.w_OBJECT.w_MINFIN = PADL(ALLTRIM(STR(MOD(this.oParentObject.w_OraStart,60))),2,"0")
        this.w_OBJECT.w_ATDATFIN = CTOT(DTOC(this.oParentObject.w_DataApp)+" "+PADL(ALLTRIM(STR(INT(this.oParentObject.w_OraStart/60))),2,"0")+":"+PADL(ALLTRIM(STR(MOD(this.oParentObject.w_OraStart,60))),2,"0"))
      endif
      this.oParentObject.w_DataApp = cp_CharToDate( "  -  -    " , "dd-mm-yyyy" )
      this.oParentObject.w_OraStart = 0
      this.oParentObject.w_OraEnd = 0
      this.w_OBJECT.w_MaskCalend = this.oParentObject
      * --- Inseriamo il valore di default
      this.w_OBJECT.SetControlsValue()     
      this.w_OBJECT.w_tipomask = "C"
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Deve modificare l'attivit� il cui seriale � passato come parametro
    * --- Instanzio l'anagrafica
    this.w_OBJECT = GSAG_AAT()
    * --- Controllo se ha passato il test di accesso
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- Carico il record selezionato
    this.w_OBJECT.ECPFILTER()     
    this.w_OBJECT.w_ATSERIAL = this.oParentObject.w_Serial
    this.w_OBJECT.ECPSAVE()     
    this.w_OBJECT.w_MaskCalend = this.oParentObject
    this.w_OBJECT.w_tipomask = "C"
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.oParentObject.w_ct5Day.SetMultiDate(this.oParentObject.w_DataSel)     
    * --- Inizializzazioni
    this.oParentObject.w_Data_Ini = DTOT(this.oParentObject.w_DataSel-DOW(this.oParentObject.w_DataSel,2)+1)
    this.oParentObject.w_Data_Fin = DTOT(this.oParentObject.w_DataSel+5-DOW(this.oParentObject.w_DataSel,2))+3600*24-1
    * --- Select from QUERY\GSAG_BAG
    do vq_exec with 'QUERY\GSAG_BAG',this,'_Curs_QUERY_GSAG_BAG','',.f.,.t.
    if used('_Curs_QUERY_GSAG_BAG')
      select _Curs_QUERY_GSAG_BAG
      locate for 1=1
      do while not(eof())
      * --- A pag 23 viene valorizzata la var. w_DESCR_ATT
      this.Page_23()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_Text = ALLTRIM(this.w_DESCR_ATT)
      if !ISNULL(_Curs_QUERY_GSAG_BAG.ATDATFIN) AND !EMPTY(_Curs_QUERY_GSAG_BAG.ATDATFIN) AND (TTOD(_Curs_QUERY_GSAG_BAG.ATDATINI) # TTOD(_Curs_QUERY_GSAG_BAG.ATDATFIN))
        * --- L'appuntamento occupa pi� giorni. 
        *     Dovremo inserirne uno per ciascun giorno e solo il primo sar� modificabile, gli altri non potranno essere spostati
        this.w_ADatIni = VAL(SYS(11,_Curs_QUERY_GSAG_BAG.ATDATINI)) - this.w_nBaseCal
        this.w_LockTimes = .T.
        if _Curs_QUERY_GSAG_BAG.ATDATINI < this.oParentObject.w_Data_Ini
          * --- L'appuntamento inizia in precedenza
          this.w_ADatIni = VAL(SYS(11,this.oParentObject.w_Data_Ini)) - this.w_nBaseCal
          this.w_OraAtti = 0
        else
          this.w_OraAtti = HOUR(_Curs_QUERY_GSAG_BAG.ATDATINI)*60+MINUTE(_Curs_QUERY_GSAG_BAG.ATDATINI)
        endif
        * --- Inseriamo il primo appuntamento
        do while this.w_ADatIni <= (VAL(SYS(11,_Curs_QUERY_GSAG_BAG.ATDATFIN)) - this.w_nBaseCal) AND this.w_ADatIni <= (VAL(SYS(11,this.oParentObject.w_Data_Fin)) - this.w_nBaseCal)
          if this.w_ADatIni < (VAL(SYS(11,_Curs_QUERY_GSAG_BAG.ATDATFIN)) - this.w_nBaseCal)
            * --- Calcoliamo la durata come tempo che intercorre dall'appuntamento a mezzanotte
            this.w_DurAtti = 24*60 - this.w_OraAtti
          else
            * --- L'appuntamento termina nella giornata
            this.w_DurAtti = HOUR(_Curs_QUERY_GSAG_BAG.ATDATFIN)*60+MINUTE(_Curs_QUERY_GSAG_BAG.ATDATFIN)
            * --- Se l'appuntamento finisce alle 00:00 dobbiamo inserire almeno un minuto
            if this.w_DurAtti = 0
              this.w_DurAtti = 1
            endif
          endif
          this.ind = this.oParentObject.w_ct5Day.AddKeyAppointment(this.w_OraAtti, this.w_OraAtti+this.w_DurAtti, this.w_ADatIni, this.w_Text, _Curs_QUERY_GSAG_BAG.ATSERIAL, this.w_LockTimes, _Curs_QUERY_GSAG_BAG.DISPOCOL)
          this.w_ADatIni = this.w_ADatIni + 1
          this.w_OraAtti = 0
        enddo
      else
        * --- Appuntamento solo in un giorno facente parte dell'intervallo
        this.w_LockTimes = .F.
        this.w_ADatIni = VAL(SYS(11,_Curs_QUERY_GSAG_BAG.ATDATINI)) - this.w_nBaseCal
        this.w_OraAtti = HOUR(_Curs_QUERY_GSAG_BAG.ATDATINI)*60+MINUTE(_Curs_QUERY_GSAG_BAG.ATDATINI)
        this.w_DurAtti = 0
        if !ISNULL(_Curs_QUERY_GSAG_BAG.ATDATFIN) AND !EMPTY(_Curs_QUERY_GSAG_BAG.ATDATFIN)
          * --- calcoliamo la durata in minuti
          this.w_DurAtti = INT( ( _Curs_QUERY_GSAG_BAG.ATDATFIN-_Curs_QUERY_GSAG_BAG.ATDATINI ) / 60 )
        endif
        if this.w_DurAtti = 0
          this.w_DurAtti = 1
        endif
        this.ind = this.oParentObject.w_ct5Day.AddKeyAppointment(this.w_OraAtti, this.w_OraAtti+this.w_DurAtti, this.w_ADatIni, this.w_Text, _Curs_QUERY_GSAG_BAG.ATSERIAL, this.w_LockTimes, _Curs_QUERY_GSAG_BAG.DISPOCOL, _Curs_QUERY_GSAG_BAG.RIGACOL)
        if _Curs_QUERY_GSAG_BAG.ATSTATUS=="T"
          * --- Attivit� provvisoria
          this.oParentObject.w_ct5Day.SetAppBackColor(this.ind, g_ColAttConf)     
        endif
      endif
      * --- Se attivit� riservata la rendo immodificabile...
      if Not IsRisAtt( _Curs_QUERY_GSAG_BAG.ATSERIAL , _Curs_QUERY_GSAG_BAG.ATFLATRI ) 
        * --- Se attivit� riservata maschero alcuni campi...
        *     (in progress la lettura dai parametri potr� essere sostituita da una var. pubblica letta all'avvio)
        this.oParentObject.w_ct5Day.cal.AppointReadOnly( this.ind ) = .t.
        this.oParentObject.w_ct5Day.cal.AppointLockTimes( this.ind ) = .t.
      endif
        select _Curs_QUERY_GSAG_BAG
        continue
      enddo
      use
    endif
    this.oParentObject.w_ct5Day.SetVisible(.T.)     
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CtDate
    this.oParentObject.w_ctDate.SetVisible(.T.)     
    this.w_Side = this.oParentObject.w_ctDate.DateSection()
    this.oParentObject.w_ctDate.SetDate(this.oParentObject.w_DataSel)     
    this.oParentObject.w_ctDate.Refresh()     
    * --- Utilizziamo impropriamente w_Data_Ini e w_Data_Fin perch� utilizzati dalla query, quindi ne salviamo il valore
    this.w_Sav_Ini = this.oParentObject.w_Data_Ini
    this.w_Sav_Fin = this.oParentObject.w_Data_Fin
    * --- Visualizzazione con un solo mese
    this.oParentObject.w_Data_Ini = cp_CharToDateTime( "01-"+ALLTRIM(STR(MONTH(this.oParentObject.w_DataSel)))+"-"+ALLTR(STR(YEAR(this.oParentObject.w_DataSel))), "dd-mm-yyyy hh:nn:ss" )
    this.oParentObject.w_Data_Fin = DTOT(GOMONTH(this.oParentObject.w_Data_Ini, 1) - 1)+3600*24-1
    * --- Inseriamo in un array bidimensionale: il secondo dato ci segnala la SEZIONE, che dovremo decrementare di uno (0 sinistra, 2 destra)
    Dimension aText(31, 2)
    * --- Select from QUERY\GSAG2BAG
    do vq_exec with 'QUERY\GSAG2BAG',this,'_Curs_QUERY_GSAG2BAG','',.f.,.t.
    if used('_Curs_QUERY_GSAG2BAG')
      select _Curs_QUERY_GSAG2BAG
      locate for 1=1
      do while not(eof())
      if ATDATINI < this.oParentObject.w_Data_Ini
        * --- L'appuntamento inizia in precedenza
        this.w_DtIni = this.oParentObject.w_Data_Ini
      else
        this.w_DtIni = ATDATINI
      endif
      if !ISNULL(ATDATFIN) AND !EMPTY(ATDATFIN)
        if ATDATFIN > this.oParentObject.w_Data_Fin
          * --- L'appuntamento finisce dopo
          this.w_DtFin = this.oParentObject.w_Data_Fin
        else
          this.w_DtFin = ATDATFIN
        endif
      else
        this.w_DtFin = CTOT("  -  -      :  ")
      endif
      if !empty(this.w_DtFin) AND (TTOD(this.w_DtFin) # TTOD(this.w_DtIni))
        * --- L'appuntamento occupa pi� giorni.
        *     Cicliamo fino a che non raggiungiamo la fine dell'intervallo
        do while TTOD(this.w_DtIni) <= TTOD(this.w_DtFin)
          if MONTH(this.w_DtIni)=MONTH(this.oParentObject.w_DataSel)
            * --- Stessa sezione del giorno selezionato
            aText( DAY(this.w_DtIni), this.w_Side + 1 ) = "*"
          else
            * --- Sezione opposta
            aText( DAY(this.w_DtIni), BITXOR(this.w_Side,1) + 1 ) = "*"
          endif
          this.w_DtIni = cp_CharToDatetime(DTOC(TTOD(this.w_DtIni)+1))
        enddo
      else
        * --- Appuntamento solo in un giorno facente parte dell'intervallo
        if MONTH(this.w_DtIni)=MONTH(this.oParentObject.w_DataSel)
          * --- Stessa sezione del giorno selezionato
          aText( DAY(this.w_DtIni), this.w_Side + 1 ) = "*"
        else
          * --- Sezione opposta
          aText( DAY(this.w_DtIni), BITXOR(this.w_Side,1) + 1 ) = "*"
        endif
        this.w_DtIni = cp_CharToDatetime(DTOC(TTOD(this.w_DtIni)+1))
      endif
        select _Curs_QUERY_GSAG2BAG
        continue
      enddo
      use
    endif
    this.w_nDay = 1
    this.w_nMonth = 1
    do while this.w_nMonth<=2
      this.w_nDay = 1
      do while this.w_nDay<=31
        * --- Giorno considerato
        if ! EMPTY(aText(this.w_nDay, this.w_nMonth))
          * --- Per ottenere la sezione dobbiamo decrementare di uno
          this.oParentObject.w_ctDate.DateFont(this.w_nDay, (this.w_nMonth-1))     
        endif
        * --- Avanza di giorno
        this.w_nDay = this.w_nDay+1
      enddo
      this.w_nMonth = this.w_nMonth + 1
    enddo
    this.oParentObject.w_ctDate.SetSection(this.w_Side)     
    * --- Ripristiniamo i valori corretti di w_Data_Ini e w_Data_Fin
    this.oParentObject.w_Data_Ini = this.w_Sav_Ini
    this.oParentObject.w_Data_Fin = this.w_Sav_Fin
  endproc


  procedure Page_14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- E' stata selezionata o deselezionata un'attivit�.
    *     Nel primo caso, dobbiamo avvalorare i dati pratica e i dati attivit�
    *     altrimenti mettiamo i campi a blank
    if (this.pAZIONE=="SELEZIONATO" OR this.pAZIONE=="SELEZIONATO_KRA" OR this.pAzione=="TMRBALLOON") AND !EMPTY(this.oParentObject.w_Serial)
      * --- Leggiamo i dati dell'attivit�
      * --- Leggiamo data inizio e data fine attivit� utilizzando w_Data1 e w_Data2 dichiarate a pag. 16
      *     Servono per filtrare i giudici confrontando le date di validit� con quelle dell'attivit�
      * --- Read from OFF_ATTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATOGGETT,ATLOCALI,AT__ENTE,ATUFFICI,ATCODPRA,ATNOTPIA,ATCENCOS,ATPERSON,ATTELEFO,ATCOMRIC,ATFLATRI,ATDATINI,ATDATFIN,ATCODNOM"+;
          " from "+i_cTable+" OFF_ATTI where ";
              +"ATSERIAL = "+cp_ToStrODBC(this.oParentObject.w_Serial);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATOGGETT,ATLOCALI,AT__ENTE,ATUFFICI,ATCODPRA,ATNOTPIA,ATCENCOS,ATPERSON,ATTELEFO,ATCOMRIC,ATFLATRI,ATDATINI,ATDATFIN,ATCODNOM;
          from (i_cTable) where;
              ATSERIAL = this.oParentObject.w_Serial;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.AttiOgge = NVL(cp_ToDate(_read_.ATOGGETT),cp_NullValue(_read_.ATOGGETT))
        this.AttiLocali = NVL(cp_ToDate(_read_.ATLOCALI),cp_NullValue(_read_.ATLOCALI))
        this.AttiCodEnte = NVL(cp_ToDate(_read_.AT__ENTE),cp_NullValue(_read_.AT__ENTE))
        this.AttiCodUff = NVL(cp_ToDate(_read_.ATUFFICI),cp_NullValue(_read_.ATUFFICI))
        this.CodPra = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
        this.AttiNote = NVL(cp_ToDate(_read_.ATNOTPIA),cp_NullValue(_read_.ATNOTPIA))
        this.w_CENCOST = NVL(cp_ToDate(_read_.ATCENCOS),cp_NullValue(_read_.ATCENCOS))
        this.w_NOMINAT = NVL(cp_ToDate(_read_.ATPERSON),cp_NullValue(_read_.ATPERSON))
        this.w_TELEFO = NVL(cp_ToDate(_read_.ATTELEFO),cp_NullValue(_read_.ATTELEFO))
        this.w_COMRIC = NVL(cp_ToDate(_read_.ATCOMRIC),cp_NullValue(_read_.ATCOMRIC))
        this.w_ATFLATRI = NVL(cp_ToDate(_read_.ATFLATRI),cp_NullValue(_read_.ATFLATRI))
        this.w_DATA1 = NVL(cp_ToDate(_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
        this.w_DATA2 = NVL(cp_ToDate(_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
        this.w_CODNOMIN = NVL(cp_ToDate(_read_.ATCODNOM),cp_NullValue(_read_.ATCODNOM))
        this.w_ATDATINI = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
        this.w_ATDATFIN = NVL((_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from COM_ATTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COM_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COM_ATTI_idx,2],.t.,this.COM_ATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODIMP"+;
          " from "+i_cTable+" COM_ATTI where ";
              +"CASERIAL = "+cp_ToStrODBC(this.oParentObject.w_Serial);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODIMP;
          from (i_cTable) where;
              CASERIAL = this.oParentObject.w_Serial;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODIMP = NVL(cp_ToDate(_read_.CACODIMP),cp_NullValue(_read_.CACODIMP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.oParentObject.w_TIPANA="R"
        this.CodPra = this.w_COMRIC
      endif
      if Not IsRisAtt( this.oParentObject.w_SERIAL , this.w_ATFLATRI ) 
        * --- Se attivit� riservata maschero alcuni campi...
        *     (in progress la lettura dai parametri potr� essere sostituita da una var. pubblica letta all'avvio)
        this.w_BRISE = .T.
      else
        this.w_BRISE = .F.
      endif
      this.oParentObject.w_DatiAttivita = Ah_MsgFormat("Oggetto: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.AttiOgge))+CHR(13)+CHR(10)
      if !EMPTY(NVL(this.AttiNote, ""))
        this.oParentObject.w_DatiAttivita = this.oParentObject.w_DatiAttivita +Ah_MsgFormat("Note: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.AttiNote))+CHR(13)+CHR(10)
      endif
      if !EMPTY(this.AttiLocali)
        this.oParentObject.w_DatiAttivita = this.oParentObject.w_DatiAttivita + Ah_MsgFormat("Localit�: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.AttiLocali))+CHR(13)+CHR(10)
      endif
      if !EMPTY(this.AttiCodEnte)
        * --- Read from PRA_ENTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2],.t.,this.PRA_ENTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "EPDESCRI"+;
            " from "+i_cTable+" PRA_ENTI where ";
                +"EPCODICE = "+cp_ToStrODBC(this.AttiCodEnte);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            EPDESCRI;
            from (i_cTable) where;
                EPCODICE = this.AttiCodEnte;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.AttiEnte = NVL(cp_ToDate(_read_.EPDESCRI),cp_NullValue(_read_.EPDESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_DatiAttivita = this.oParentObject.w_DatiAttivita + Ah_MsgFormat("Ente: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.AttiEnte))+CHR(13)+CHR(10)
      endif
      if !EMPTY(this.AttiCodUff)
        * --- Read from PRA_UFFI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRA_UFFI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRA_UFFI_idx,2],.t.,this.PRA_UFFI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UFDESCRI"+;
            " from "+i_cTable+" PRA_UFFI where ";
                +"UFCODICE = "+cp_ToStrODBC(this.AttiCodUff);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UFDESCRI;
            from (i_cTable) where;
                UFCODICE = this.AttiCodUff;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.AttiUfficio = NVL(cp_ToDate(_read_.UFDESCRI),cp_NullValue(_read_.UFDESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_DatiAttivita = this.oParentObject.w_DatiAttivita + Ah_MsgFormat("Ufficio: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.AttiUfficio))+CHR(13)+CHR(10)
      endif
      * --- Dobbiamo inserire anche i partecipanti
      this.w_PartSerial = this.oParentObject.w_Serial
      this.w_TextPart = ""
      this.w_TextRisorse = ""
      * --- Select from QUERY\GSAG6BAG.VQR
      do vq_exec with 'QUERY\GSAG6BAG.VQR',this,'_Curs_QUERY_GSAG6BAG_d_VQR','',.f.,.t.
      if used('_Curs_QUERY_GSAG6BAG_d_VQR')
        select _Curs_QUERY_GSAG6BAG_d_VQR
        locate for 1=1
        do while not(eof())
        do case
          case DPTIPRIS <> "R"
            this.w_TextPart = this.w_TextPart+ ALLTRIM(DESCR_DIP)+","+SPACE(1)
          case DPTIPRIS = "R"
            this.w_TextRisorse = this.w_TextRisorse+ ALLTRIM(DESCR_DIP)+","+SPACE(1)
        endcase
          select _Curs_QUERY_GSAG6BAG_d_VQR
          continue
        enddo
        use
      endif
      if !EMPTY(this.w_TextRisorse)
        this.w_TextRisorse = Ah_MsgFormat("Risorse:")+SPACE(1)+SUBSTR(this.w_TextRisorse,1, LEN(this.w_TextRisorse)-2 )+CHR(13)+CHR(10)
        this.oParentObject.w_DatiAttivita = this.w_TextRisorse + this.oParentObject.w_DatiAttivita
      endif
      if !EMPTY(this.w_TextPart)
        this.w_TextPart = Ah_MsgFormat("Partecipanti:")+SPACE(1)+SUBSTR(this.w_TextPart,1, LEN(this.w_TextPart)-2 )+CHR(13)+CHR(10)
        this.oParentObject.w_DatiAttivita = this.w_TextPart + this.oParentObject.w_DatiAttivita
      endif
      this.w_Appoggio_DatiPratica = ""
      if !EMPTY(this.CodPra)
        * --- Leggiamo i dati della pratica
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNDESCAN,CNDESOGG,CNRIFGIU,CNRIFERI,CNCODUBI,CN__NOTE"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.CodPra);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNDESCAN,CNDESOGG,CNRIFGIU,CNRIFERI,CNCODUBI,CN__NOTE;
            from (i_cTable) where;
                CNCODCAN = this.CodPra;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.PraNome = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
          this.PraOgge = NVL(cp_ToDate(_read_.CNDESOGG),cp_NullValue(_read_.CNDESOGG))
          this.PraRuolo = NVL(cp_ToDate(_read_.CNRIFGIU),cp_NullValue(_read_.CNRIFGIU))
          this.PraRiferi = NVL(cp_ToDate(_read_.CNRIFERI),cp_NullValue(_read_.CNRIFERI))
          this.PraCodUbi = NVL(cp_ToDate(_read_.CNCODUBI),cp_NullValue(_read_.CNCODUBI))
          this.PraNote = NVL(cp_ToDate(_read_.CN__NOTE),cp_NullValue(_read_.CN__NOTE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_Appoggio_DatiPratica = IIF(ISALT(),Ah_MsgFormat("Pratica: "),Ah_MsgFormat("Commessa: "))+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.CodPra))+" - "+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.PraNome))+CHR(13)+CHR(10)
        if !EMPTY(this.PraOgge) AND (this.pAZIONE=="SELEZIONATO" OR ( ( this.pAZIONE=="SELEZIONATO_KRA" OR this.pAZIONE=="TMRBALLOON") AND ISALT() ))
          this.w_Appoggio_DatiPratica = this.w_Appoggio_DatiPratica + Ah_MsgFormat("Oggetto: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.PraOgge))+CHR(13)+CHR(10)
        endif
        if !EMPTY(this.PraRuolo) AND (this.pAZIONE=="SELEZIONATO" OR ( ( this.pAZIONE=="SELEZIONATO_KRA" OR this.pAZIONE=="TMRBALLOON") AND ISALT() ))
          this.w_Appoggio_DatiPratica = this.w_Appoggio_DatiPratica + Ah_MsgFormat("Ruolo: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.PraRuolo))+CHR(13)+CHR(10)
        endif
        if !EMPTY(this.PraRiferi) AND (this.pAZIONE=="SELEZIONATO" OR ( ( this.pAZIONE=="SELEZIONATO_KRA" OR this.pAZIONE=="TMRBALLOON") AND ISALT() ))
          this.w_Appoggio_DatiPratica = this.w_Appoggio_DatiPratica + Ah_MsgFormat("Riferimento: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.PraRiferi))+CHR(13)+CHR(10)
        endif
        if !EMPTY(this.PraCodUbi) AND (this.pAZIONE=="SELEZIONATO" OR ( ( this.pAZIONE=="SELEZIONATO_KRA" OR this.pAZIONE=="TMRBALLOON") AND ISALT() ))
          * --- Read from PRA_UBIC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRA_UBIC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRA_UBIC_idx,2],.t.,this.PRA_UBIC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "UPDESCRI"+;
              " from "+i_cTable+" PRA_UBIC where ";
                  +"UPCODICE = "+cp_ToStrODBC(this.PraCodUbi);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              UPDESCRI;
              from (i_cTable) where;
                  UPCODICE = this.PraCodUbi;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.PraUbica = NVL(cp_ToDate(_read_.UPDESCRI),cp_NullValue(_read_.UPDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_Appoggio_DatiPratica = this.w_Appoggio_DatiPratica+Ah_MsgFormat("Ubicazione: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.PraUbica))+CHR(13)+CHR(10)
        endif
        this.w_RisEste = ""
        if IsAlt()
          * --- Select from QUERY\GSAG7BAG.VQR
          do vq_exec with 'QUERY\GSAG7BAG.VQR',this,'_Curs_QUERY_GSAG7BAG_d_VQR','',.f.,.t.
          if used('_Curs_QUERY_GSAG7BAG_d_VQR')
            select _Curs_QUERY_GSAG7BAG_d_VQR
            locate for 1=1
            do while not(eof())
            this.w_RisEste = this.w_RisEste + ALLTRIM(CSDESCAT)+": "+ALLTRIM(NODESCRI)+CHR(13)+CHR(10)
              select _Curs_QUERY_GSAG7BAG_d_VQR
              continue
            enddo
            use
          endif
        endif
        if !EMPTY(this.w_RisEste)
          this.w_Appoggio_DatiPratica = this.w_Appoggio_DatiPratica + this.w_RisEste
        endif
        if !EMPTY(this.PraNote) AND (this.pAZIONE=="SELEZIONATO" OR ( ( this.pAZIONE=="SELEZIONATO_KRA" OR this.pAZIONE=="TMRBALLOON") AND ISALT() ))
          this.w_Appoggio_DatiPratica = this.w_Appoggio_DatiPratica + Ah_MsgFormat("Annotazioni: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.PraNote))+CHR(13)+CHR(10)
        endif
      else
        this.w_Appoggio_DatiPratica = ""
      endif
      if !EMPTY(this.oParentObject.w_CENCOS) AND (this.pAZIONE=="SELEZIONATO" OR ( ( this.pAZIONE=="SELEZIONATO_KRA" OR this.pAZIONE=="TMRBALLOON") AND ISALT() ))
        this.w_Appoggio_DatiPratica = this.w_Appoggio_DatiPratica + Ah_MsgFormat("C/C.R.: ")+ALLTRIM(this.w_CENCOST)+CHR(13)+CHR(10)
      endif
      if NOT EMPTY(this.w_CODNOMIN)
        * --- Legge la descrizione del nominativo
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NODESCRI"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_CODNOMIN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NODESCRI;
            from (i_cTable) where;
                NOCODICE = this.w_CODNOMIN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESNOMIN = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if NOT EMPTY(this.w_NOMINAT) OR !EMPTY(this.w_DESNOMIN)
        this.w_Appoggio_DatiPratica = this.w_Appoggio_DatiPratica + IIF(!EMPTY(this.w_DESNOMIN), Ah_MsgFormat("Nominativo: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.w_DESNOMIN))+" - ","")+ IIF(!EMPTY(this.w_NOMINAT) AND !ALLTRIM(this.w_NOMINAT)==ALLTRIM(NVL(this.w_DESNOMIN,"")), Ah_MsgFormat("Riferimento persona: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.w_NOMINAT))+" - ","")
        this.w_Appoggio_DatiPratica = this.w_Appoggio_DatiPratica + Ah_MsgFormat("Telefono: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.w_TELEFO))+CHR(13)+CHR(10)
      endif
      if !EMPTY(this.w_CODIMP)
        this.w_SERATT = this.oParentObject.w_Serial
        * --- Select from GSAG1BAG
        do vq_exec with 'GSAG1BAG',this,'_Curs_GSAG1BAG','',.f.,.t.
        if used('_Curs_GSAG1BAG')
          select _Curs_GSAG1BAG
          locate for 1=1
          do while not(eof())
          this.w_IMPCOD = _Curs_GSAG1BAG.CACODIMP
          this.w_DESCOMP = _Curs_GSAG1BAG.IMDESCON
          this.w_Appoggio_DatiPratica = this.w_Appoggio_DatiPratica + Ah_MsgFormat("Impianto: ")+ALLTRIM(Nvl(this.w_IMPCOD,""))+IIF(!EMPTY(NVL(this.w_DESCOMP,SPACE(50)))," - "+Ah_MsgFormat("Componente: ")+ALLTRIM(this.w_DESCOMP),"")+CHR(13)+CHR(10)
            select _Curs_GSAG1BAG
            continue
          enddo
          use
        endif
      endif
    else
      this.w_Appoggio_DatiPratica = space(10)
      this.oParentObject.w_DatiAttivita = space(10)
    endif
    if this.pAzione=="SELEZIONATO" OR this.pAzione=="TMRBALLOON"
      * --- Da GSAG_KAG (Agenda)
      if this.pAzione<>"TMRBALLOON"
        this.oParentObject.w_DatiPratica = this.w_Appoggio_DatiPratica
      endif
    else
      * --- Da GSAG_KRA (Elenco Attivit�)
      if NOT ISALT()
        * --- In AHR/AHE i dati sul piede sono:
        *     - Localit�
        *     - Commessa
        *     - Nominativo, Telefono
        *     - Impianto, Componente
        * --- Inserita in testa la Localit� dell'attivit�
        if !EMPTY(this.AttiLocali)
          this.w_Appoggio_DatiPratica = Ah_MsgFormat("Localit�: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.AttiLocali))+CHR(13)+CHR(10)+IIF(not empty(this.w_Appoggio_DatiPratica), this.w_Appoggio_DatiPratica, "")
        endif
      endif
      * --- Inserite in testa le Note dell'attivit�
      if !EMPTY(NVL(this.AttiNote, ""))
        this.w_Appoggio_DatiPratica = IIF(Not ISALT(), Ah_MsgFormat("Note: "), Ah_MsgFormat("Note attivit�: "))+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , this.AttiNote))+CHR(13)+CHR(10)+IIF(not empty(this.w_Appoggio_DatiPratica), this.w_Appoggio_DatiPratica, "")
      endif
      this.oParentObject.w_Pratica_Attivita = this.w_Appoggio_DatiPratica
    endif
    this.w_Stringa_Date = Ah_MsgFormat("Inizio: ")+TTOC(this.w_ATDATINI)+CHR(13)+CHR(10)+Ah_MsgFormat("Fine: ")+TTOC(this.w_ATDATFIN)+CHR(13)+CHR(10)
  endproc


  procedure Page_15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica il testo dell'attivit� il cui seriale � passato come parametro
    *     impostando come oggetto il contenuto della variabile w_TxtSel
    if !EMPTY(this.oParentObject.w_Serial) and !EMPTY(this.oParentObject.w_TxtSel) AND this.pAzione=="MODIFICATXT"
      if AT("[",this.oParentObject.w_TxtSel,1)>0 AND AT("]",this.oParentObject.w_TxtSel,1)>0 AND AT("[",this.oParentObject.w_TxtSel,1)<AT("]",this.oParentObject.w_TxtSel,1)
        this.oParentObject.w_TxtSel = ALLTRIM( SUBSTR(this.oParentObject.w_TxtSel, 1, AT("[",this.oParentObject.w_TxtSel,1)-1))+ALLTRIM( SUBSTR(this.oParentObject.w_TxtSel, AT("]",this.oParentObject.w_TxtSel,1)+1))
      endif
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATOGGETT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TxtSel),'OFF_ATTI','ATOGGETT');
        +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
            +i_ccchkf ;
        +" where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.oParentObject.w_Serial);
               )
      else
        update (i_cTable) set;
            ATOGGETT = this.oParentObject.w_TxtSel;
            ,ATDATOUT = DATETIME();
            &i_ccchkf. ;
         where;
            ATSERIAL = this.oParentObject.w_Serial;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.pAzione=="MODIFICAORA" OR this.pAzione=="MODIFICADAOUTLOOK"
      * --- OraStart e OraEnd rappresentano il numero di minuti dalla mezzanotte. Per avere l'ora e i minuti dobbiamo fare la divisione intera ed il modulo
      this.w_Data1 = CTOT(DTOC(this.oParentObject.w_DataApp)+" "+ALLTRIM(STR(INT(this.oParentObject.w_OraStart/60)))+":"+ALLTRIM(STR(MOD(this.oParentObject.w_OraStart,60))))
      if this.pAzione=="MODIFICAORA"
        * --- Da GSAG_KAG - tutto all'interno dello stesso giorno
        this.w_Data2 = CTOT(DTOC(this.oParentObject.w_DataApp)+" "+ALLTRIM(STR(INT(this.oParentObject.w_OraEnd/60)))+":"+ALLTRIM(STR(MOD(this.oParentObject.w_OraEnd,60))))
      else
        * --- Da GSAG_BEX, anche su pi� giorni
        this.w_Data2 = CTOT(DTOC(this.oParentObject.w_DataAppFin)+" "+ALLTRIM(STR(INT(this.oParentObject.w_OraEnd/60)))+":"+ALLTRIM(STR(MOD(this.oParentObject.w_OraEnd,60))))
      endif
      * --- Leggiamo prima data, ora e gli altri dati dell'attivit�: se fa parte di un iter, dobbiamo eventualmente modificare anche le scadenze correlate.
      *     Dichiariamo le variabili che serviranno nel batch GSAG_BAD
      * --- Flag attivit� ricorrente
      * --- Lettura dati promemoria
      * --- Il flag di notifica serve in GSAG_BAP
      * --- Read from OFF_ATTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATDATINI,ATCAITER,ATOGGETT,ATCODPRA,ATFLNOTI,ATFLPROM,ATPROMEM,ATSTATUS,ATDATFIN,ATFLRICO"+;
          " from "+i_cTable+" OFF_ATTI where ";
              +"ATSERIAL = "+cp_ToStrODBC(this.oParentObject.w_Serial);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATDATINI,ATCAITER,ATOGGETT,ATCODPRA,ATFLNOTI,ATFLPROM,ATPROMEM,ATSTATUS,ATDATFIN,ATFLRICO;
          from (i_cTable) where;
              ATSERIAL = this.oParentObject.w_Serial;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ATDATINI = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
        this.w_ATCAITER = NVL(cp_ToDate(_read_.ATCAITER),cp_NullValue(_read_.ATCAITER))
        this.w_ATOGGETT = NVL(cp_ToDate(_read_.ATOGGETT),cp_NullValue(_read_.ATOGGETT))
        this.w_ATCODPRA = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
        this.w_ATFLNOTI = NVL(cp_ToDate(_read_.ATFLNOTI),cp_NullValue(_read_.ATFLNOTI))
        this.w_ATFLPROM = NVL(cp_ToDate(_read_.ATFLPROM),cp_NullValue(_read_.ATFLPROM))
        this.w_ATPROMEM = NVL((_read_.ATPROMEM),cp_NullValue(_read_.ATPROMEM))
        this.w_ATSTATUS = NVL(cp_ToDate(_read_.ATSTATUS),cp_NullValue(_read_.ATSTATUS))
        this.w_ATDATFIN = NVL((_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
        this.w_ATFLRICO = NVL(cp_ToDate(_read_.ATFLRICO),cp_NullValue(_read_.ATFLRICO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_OLDDATINI = this.w_ATDATINI
      this.w_OLDDATFIN = this.w_ATDATFIN
      this.w_OLDSTATUS = this.w_ATSTATUS
      * --- Gestito anche promemoria
      if this.w_ATFLPROM="S"
        this.w_ATPROMEM = this.w_ATPROMEM + (this.w_DATA1 - this.w_ATDATINI)
      endif
      * --- Aggiorniamo l'attivit�
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_Data2),'OFF_ATTI','ATDATFIN');
        +",ATDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_Data1),'OFF_ATTI','ATDATINI');
        +",ATPROMEM ="+cp_NullLink(cp_ToStrODBC(this.w_ATPROMEM),'OFF_ATTI','ATPROMEM');
        +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
            +i_ccchkf ;
        +" where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.oParentObject.w_Serial);
               )
      else
        update (i_cTable) set;
            ATDATFIN = this.w_Data2;
            ,ATDATINI = this.w_Data1;
            ,ATPROMEM = this.w_ATPROMEM;
            ,ATDATOUT = DATETIME();
            &i_ccchkf. ;
         where;
            ATSERIAL = this.oParentObject.w_Serial;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Se � stata cambiata la data e fa parte di un iter, aggiorna eventualmente le attivit� successive
      if this.pAzione=="MODIFICAORA"
        * --- Deve inviare la mail
        this.w_DataCambiata = .T.
        GSAG_BAP(this,"M", this.oParentObject.w_SERIAL)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if not empty(this.w_ATCAITER) and TTOD(this.w_ATDATINI)<>this.oParentObject.w_DataApp
        this.w_DATINI_CHANGED = "S"
        this.w_USCITA = ""
        * --- Dobbiamo ottenere anche la descrizione della pratica
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNDESCAN"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_ATCODPRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNDESCAN;
            from (i_cTable) where;
                CNCODCAN = this.w_ATCODPRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESCAN = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ATDATFIN = this.w_Data2
        this.w_ATDATINI = this.w_Data1
        this.w_ATSERIAL = this.oParentObject.w_SERIAL
        GSAG_BAD(this,"M")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- "GSAG_BAD('M')"
      endif
    endif
    this.oParentObject.w_Serial = space(20)
  endproc


  procedure Page_16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiunge un appuntamento inserito a mano nel DB
    if EMPTY( this.oParentObject.w_Serial )
      if !empty(this.oParentObject.w_CauDefault)
        * --- E' stato aggiunto un appuntamento nella visualizzazione 1-5 giorni
        if EMPTY(this.oParentObject.w_CODPART)
          this.w_CodRis = readdipend(i_CodUte, "C")
        else
          this.w_CodRis = this.oParentObject.w_CODPART
        endif
        if !EMPTY(this.w_CodRis)
          * --- Gestione promemoria
          * --- Leggiamo i dati del tipo
          * --- Read from CAUMATTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAUMATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CAGGPREA,CASTAATT,CAPRIORI,CATIPATT,CAPROMEM,CADISPON,CAFLNSAP,CAFLPREA,CAMINPRE,CAFLNOTI"+;
              " from "+i_cTable+" CAUMATTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_CauDefault);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CAGGPREA,CASTAATT,CAPRIORI,CATIPATT,CAPROMEM,CADISPON,CAFLNSAP,CAFLPREA,CAMINPRE,CAFLNOTI;
              from (i_cTable) where;
                  CACODICE = this.oParentObject.w_CauDefault;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.GG_PREAV = NVL(cp_ToDate(_read_.CAGGPREA),cp_NullValue(_read_.CAGGPREA))
            this.STATO = NVL(cp_ToDate(_read_.CASTAATT),cp_NullValue(_read_.CASTAATT))
            this.PRIORITA = NVL(cp_ToDate(_read_.CAPRIORI),cp_NullValue(_read_.CAPRIORI))
            this.TIPOLOGIA = NVL(cp_ToDate(_read_.CATIPATT),cp_NullValue(_read_.CATIPATT))
            this.PROMEM = NVL((_read_.CAPROMEM),cp_NullValue(_read_.CAPROMEM))
            this.DISPONIBILITA = NVL(cp_ToDate(_read_.CADISPON),cp_NullValue(_read_.CADISPON))
            this.FL_NON_SOG_PRE = NVL(cp_ToDate(_read_.CAFLNSAP),cp_NullValue(_read_.CAFLNSAP))
            this.w_CAFLPREA = NVL(cp_ToDate(_read_.CAFLPREA),cp_NullValue(_read_.CAFLPREA))
            this.w_CAMINPRE = NVL(cp_ToDate(_read_.CAMINPRE),cp_NullValue(_read_.CAMINPRE))
            this.w_ATFLNOTI = NVL(cp_ToDate(_read_.CAFLNOTI),cp_NullValue(_read_.CAFLNOTI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Per il codice utente, restituiamo il codice presente nel file DIPENDEN
          * --- OraStart e OraEnd rappresentano il numero di minuti dalla mezzanotte. Per avere l'ora e i minuti dobbiamo fare la divisione intera ed il modulo
          this.w_Data1 = CTOT(DTOC(this.oParentObject.w_DataApp)+" "+ALLTRIM(STR(INT(this.oParentObject.w_OraStart/60)))+":"+ALLTRIM(STR(MOD(this.oParentObject.w_OraStart,60))))
          if this.pAZIONE=="ADDAPP"
            * --- Da GSAG_KAG - tutto all'interno dello stesso giorno
            this.w_Data2 = CTOT(DTOC(this.oParentObject.w_DataApp)+" "+ALLTRIM(STR(INT(this.oParentObject.w_OraEnd/60)))+":"+ALLTRIM(STR(MOD(this.oParentObject.w_OraEnd,60))))
          else
            * --- Da GSAG_BEX, anche su pi� giorni
            this.w_Data2 = CTOT(DTOC(this.oParentObject.w_DataAppFin)+" "+ALLTRIM(STR(INT(this.oParentObject.w_OraEnd/60)))+":"+ALLTRIM(STR(MOD(this.oParentObject.w_OraEnd,60))))
          endif
          * --- Ore e minuti promemoria
          this.OREPRO = RIGHT("00"+ALLTRIM(STR(HOUR(this.PROMEM),2)),2)
          this.MINPRO = RIGHT("00"+ALLTRIM(STR(MINUTE(this.PROMEM),2)),2)
          * --- Gestione promemoria
          if this.w_CAFLPREA="S" AND this.w_CAMINPRE>0
            this.w_CAFLPREA = "S"
            this.w_DataAppoPromem = this.w_Data1 - this.w_CAMINPRE*60
          else
            this.w_CAFLPREA = "N"
          endif
          * --- Inseriamo l'attivit� tramite l'oggetto ActivitiyWriter
          this.w_OBJ = CREATEOBJECT("ActivityWriter",this,"OFF_ATTI")
          * --- Disabilito l'invio di messaggi
          this.w_OBJ.bSendMSG = .f.
          * --- Creiamo tutte le propriet� dell'oggetto in modo che l'attivit� abbia i valori corretti
          this.w_OBJ.w_ATOGGETT = this.oParentObject.w_TxtSel
          this.w_OBJ.w_ATDATINI = this.w_Data1
          this.w_OBJ.w_ATDATFIN = this.w_Data2
          this.w_OBJ.w_ATCODATT = this.TIPOLOGIA
          this.w_OBJ.w_ATNUMPRI = this.PRIORITA
          this.w_OBJ.w_ATSTATUS = this.STATO
          this.w_OBJ.w_ATCAUATT = this.oParentObject.w_CauDefault
          this.w_OBJ.w_ATGGPREA = this.GG_PREAV
          this.w_OBJ.w_ATPROMEM = this.w_DataAppoPromem
          this.w_OBJ.w_ATPERCOM = IIF(this.STATO="F", 100, 0)
          this.w_OBJ.w_ATTIPRIS = NULL
          this.w_OBJ.w_ATDATRIN = cp_CharToDatetime("  -  -       :  :  ")
          this.w_OBJ.w_ATDATPRO = cp_CharToDatetime("  -  -       :  :  ")
          this.w_OBJ.w_ATSTAATT = this.DISPONIBILITA
          this.w_OBJ.w_ATFLPROM = this.w_CAFLPREA
          this.w_OBJ.w_ATDATOUT = DATETIME()
          this.w_OBJ.w_ATFLNOTI = this.w_ATFLNOTI
          this.w_OBJ.w_UTCC = i_CodUte
          this.w_OBJ.w_UTDC = SetInfoDate( g_CALUTD )
          if (this.pAZIONE=="ADDDB" OR this.pAZIONE=="ADDAPP") AND !EMPTY(this.oParentObject.w_CODPART) AND !this.oParentObject.w_CODPART==readdipend(i_CodUte, "C")
            * --- Da GSAG_BEX - E' stato inserito un partecipante differente dall'utente attivo
            this.w_OBJ.AddPart(0,0,IIF(this.pAZIONE=="ADDAPP",this.oParentObject.w_PATIPRIS,this.oParentObject.w_TIPPART),this.oParentObject.w_CODPART,IIF(this.pAZIONE=="ADDAPP",this.oParentObject.w_GRUPART,""))     
          endif
          this.w_RESULT = this.w_OBJ.CreateActivity()
          if EMPTY(this.w_RESULT)
            * --- Creazione attivit� riuscita
            this.oParentObject.w_Serial = this.w_OBJ.w_ATSERIAL
          else
            ah_ErrorMsg("%1","","",Alltrim(this.w_RESULT))
          endif
        endif
      else
        ah_ErrorMsg("Attenzione, non � possibile caricare attivit� poich� non � specificato un tipo nella tabella parametri attivit�")
      endif
    endif
  endproc


  procedure Page_17
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.oParentObject.w_DatiAttiPratica = ""
    * --- Utilizziamo w_Data_Ini e w_Data_Fin per GSAG_BAG, quindi ne salviamo il valore in w_SAV_INI e w_SAV_FIN
    this.w_Sav_Ini = this.oParentObject.w_Data_Ini
    this.w_Sav_Fin = this.oParentObject.w_Data_Fin
    this.oParentObject.w_Data_Ini = DTOT(this.oParentObject.w_DataApp)
    this.oParentObject.w_Data_Fin = DTOT(this.oParentObject.w_DataApp)+3600*24-1
    * --- Select from QUERY\GSAG_BAG
    do vq_exec with 'QUERY\GSAG_BAG',this,'_Curs_QUERY_GSAG_BAG','',.f.,.t.
    if used('_Curs_QUERY_GSAG_BAG')
      select _Curs_QUERY_GSAG_BAG
      locate for 1=1
      do while not(eof())
      if ATDATINI < this.oParentObject.w_Data_Ini
        * --- L'appuntamento inizia in precedenza
        this.w_HOUR = "00:00"
      else
        this.w_HOUR = PADL(ALLTRIM(STR(HOUR(ATDATINI))),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(ATDATINI))),2,"0")
      endif
      if !ISNULL(ATDATFIN) AND !EMPTY(ATDATFIN)
        if ATDATFIN > this.oParentObject.w_Data_Fin
          * --- L'appuntamento finisce dopo
          this.w_HOUR = this.w_HOUR+" - "+"23:59"
        else
          this.w_HOUR = this.w_HOUR+" - "+PADL(ALLTRIM(STR(HOUR(ATDATFIN))),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(ATDATFIN))),2,"0")
        endif
      endif
      if Not IsRisAtt( _Curs_QUERY_GSAG_BAG.ATSERIAL , _Curs_QUERY_GSAG_BAG.ATFLATRI ) 
        * --- Se attivit� riservata maschero alcuni campi...
        *     (in progress la lettura dai parametri potr� essere sostituita da una var. pubblica letta all'avvio)
        this.w_BRISE = .T.
      else
        this.w_BRISE = .F.
      endif
      if this.oParentObject.w_calAttivo=3
        this.oParentObject.w_DatiAttiPratica = this.oParentObject.w_DatiAttiPratica +IIF(RECNO()>1,CHR(13)+CHR(10),"") + this.w_HOUR + " "+ ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , _Curs_QUERY_GSAG_BAG.ATOGGETT))
      endif
      if !EMPTY( _Curs_QUERY_GSAG_BAG.ATCODPRA) AND !ISNULL(_Curs_QUERY_GSAG_BAG.ATCODPRA)
        if isalt()
          this.oParentObject.w_DatiAttiPratica = this.oParentObject.w_DatiAttiPratica + this.w_HOUR + " "+ ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , _Curs_QUERY_GSAG_BAG.ATOGGETT)) + " "+ Ah_MsgFormat("Pratica: ")+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , _Curs_QUERY_GSAG_BAG.ATCODPRA))+" - "+ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , _Curs_QUERY_GSAG_BAG.CNDESCAN))+CHR(13)+CHR(10)
        else
          this.oParentObject.w_DatiAttiPratica = this.oParentObject.w_DatiAttiPratica + IIF(this.oParentObject.w_calAttivo=3," -",this.w_HOUR + " "+ ALLTRIM(_Curs_QUERY_GSAG_BAG.ATOGGETT)) + " "+ Ah_MsgFormat("Commessa: ")+ALLTRIM(_Curs_QUERY_GSAG_BAG.ATCODPRA)+" - "+ALLTRIM(_Curs_QUERY_GSAG_BAG.CNDESCAN)+IIF(this.oParentObject.w_calAttivo=3,"",CHR(13)+CHR(10))
        endif
      else
        if Isalt()
          this.oParentObject.w_DatiAttiPratica = this.oParentObject.w_DatiAttiPratica + this.w_HOUR + " "+ ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , _Curs_QUERY_GSAG_BAG.ATOGGETT))+CHR(13)+CHR(10)
        endif
      endif
      if NOT EMPTY( NVL(_Curs_QUERY_GSAG_BAG.ATPERSON,""))
        this.oParentObject.w_DatiAttiPratica = this.oParentObject.w_DatiAttiPratica + IIF(this.oParentObject.w_calAttivo=3," -",this.w_HOUR + " "+ ALLTRIM(IIF( this.w_BRISE , this.w_RISMSG , _Curs_QUERY_GSAG_BAG.ATOGGETT)) ) + " "+ Ah_MsgFormat("Nominativo: ")+ALLTRIM(Nvl(IIF( this.w_BRISE , this.w_RISMSG , _Curs_QUERY_GSAG_BAG.NODESCRI),""))+IIF(this.oParentObject.w_calAttivo=3,"",CHR(13)+CHR(10))
      endif
      this.w_SERATT = ALLTRIM(_Curs_QUERY_GSAG_BAG.ATSERIAL)
      if this.oParentObject.w_calAttivo<>3
        * --- Select from GSAG1BAG
        do vq_exec with 'GSAG1BAG',this,'_Curs_GSAG1BAG','',.f.,.t.
        if used('_Curs_GSAG1BAG')
          select _Curs_GSAG1BAG
          locate for 1=1
          do while not(eof())
          this.w_IMPCOD = _Curs_GSAG1BAG.CACODIMP
          this.w_DESCOMP = _Curs_GSAG1BAG.IMDESCON
          this.oParentObject.w_DatiAttiPratica = this.oParentObject.w_DatiAttiPratica + this.w_HOUR + " "+ ALLTRIM(_Curs_QUERY_GSAG_BAG.ATOGGETT) + " "+ Ah_MsgFormat("Impianto: ")+ALLTRIM(Nvl(this.w_IMPCOD,""))+IIF(!EMPTY(NVL(this.w_DESCOMP,SPACE(50)))," - "+Ah_MsgFormat("Componente: ")+ALLTRIM(this.w_DESCOMP),"")+CHR(13)+CHR(10)
          this.oParentObject.w_DatiPratica = this.oParentObject.w_DatiPratica + Ah_MsgFormat("Impianto: ")+ALLTRIM(Nvl(this.w_IMPCOD,""))+IIF((EMPTY(NVL(this.w_DESCOMP,SPACE(50))))," - "+Ah_MsgFormat("Componente: ")+ALLTRIM(this.w_DESCOMP),"")+CHR(13)+CHR(10)
            select _Curs_GSAG1BAG
            continue
          enddo
          use
        endif
      endif
      if !EMPTY( _Curs_QUERY_GSAG_BAG.ATCENCOS) AND !ISNULL(_Curs_QUERY_GSAG_BAG.ATCENCOS)
        this.oParentObject.w_DatiAttiPratica = this.oParentObject.w_DatiAttiPratica + IIF(this.oParentObject.w_calAttivo=3," -",this.w_HOUR + " "+ ALLTRIM(_Curs_QUERY_GSAG_BAG.ATOGGETT)) + " "+ Ah_MsgFormat("C/C.R.: ")+ALLTRIM(_Curs_QUERY_GSAG_BAG.ATCENCOS)+IIF(this.oParentObject.w_calAttivo=3,"",CHR(13)+CHR(10))
      endif
        select _Curs_QUERY_GSAG_BAG
        continue
      enddo
      use
    endif
    * --- Ripristiniamo i valori corretti di w_Data_Ini e w_Data_Fin
    this.oParentObject.w_Data_Ini = this.w_Sav_Ini
    this.oParentObject.w_Data_Fin = this.w_Sav_Fin
  endproc


  procedure Page_18
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializziamo i campi CtList aggiungendo le colonne con i vari campi
    *     I parametri sono cos� determinati
    *     
    *     1 - titolo colonna, 2 - larghezza,
    *     3 - tipo della colonna (0 - Text, 1 - Integer, 2 - Real, 3 - Date / Time)
    *     4 - tipo del check box, (0 - nessuno, 1 - bidimensionale, 2 - tridimensionale, 3 - User Defined)
    *     5 - Lock larghezza colonna, 6 - Sort abilitato T/F
    *     7 - Allineamento del testo (0 - Left Justified, 1 - Right Justified, 2 - Centered, 3 - Default)
    *     
    * --- CTLIST_MEMO
    this.oParentObject.w_ctList_Memo.SetVisible(.F.)     
    * --- Spazio per il BMP
    this.oParentObject.w_ClnBMP = this.oParentObject.w_ctList_Memo.AddColumn(" ",35,0,0,.F.,.T.,2)
    if g_Age_FlComp="S"
      * --- Oggetto
      this.oParentObject.w_ClnOgg = this.oParentObject.w_ctList_Memo.AddColumn("Da fare - preavviso - nota",155,0,0,.F.,.T.,0)
      * --- % Completamento
      this.oParentObject.w_ClnComp = this.oParentObject.w_ctList_Memo.AddColumn("%",25,0,0,.F.,.T.,0)
    else
      this.oParentObject.w_ClnOgg = this.oParentObject.w_ctList_Memo.AddColumn("Da fare-preavviso-nota",120,0,0,.F.,.T.,0)
      this.oParentObject.w_ClnComp = this.oParentObject.w_ctList_Memo.AddColumn("Scadenza",70,0,0,.F.,.T.,0)
    endif
    * --- Aggiungiamo le icone
    if CP_FILEEXIST("BMP\note_small.ico")
      this.oParentObject.w_ImgNote = this.oParentObject.w_ctList_Memo.AddImage( "BMP\note_small.ico" )
    endif
    if CP_FILEEXIST("BMP\DaFare.ico")
      this.oParentObject.w_ImgDaFare = this.oParentObject.w_ctList_Memo.AddImage( "BMP\DaFare.ico" )
    endif
    if CP_FILEEXIST("BMP\Preavviso.ico")
      this.oParentObject.w_ImgPreav = this.oParentObject.w_ctList_Memo.AddImage( "BMP\Preavviso.ico" )
    endif
    this.oParentObject.w_ctList_Memo.SubTextNodes(.T., this.oParentObject.w_ClnOgg, this.oParentObject.w_ClnOgg)     
    * --- CTLIST_GIORNO
    this.oParentObject.w_ctList_Giorno.SetVisible(.F.)     
    * --- Oggetto
    this.oParentObject.w_ClnOgg_Gio = this.oParentObject.w_ctList_Giorno.AddColumn("Oggetto",180,0,0,.F.,.T.,0)
    * --- Dal - Abbreviazione + giorno
    this.oParentObject.w_ClnGiornoA_Gio = this.oParentObject.w_ctList_Giorno.AddColumn(" ",26,0,0,.F.,.T.,0)
    this.oParentObject.w_ClnDataIni_Gio = this.oParentObject.w_ctList_Giorno.AddColumn("Inizio",65,3,0,.F.,.T.,0)
    * --- Al - Abbreviazione + giorno
    this.oParentObject.w_ClnGiornoDa_Gio = this.oParentObject.w_ctList_Giorno.AddColumn(" ",26,0,0,.F.,.T.,0)
    this.oParentObject.w_ClnDataFin_Gio = this.oParentObject.w_ctList_Giorno.AddColumn("Fine",65,3,0,.F.,.T.,0)
    * --- % Completamento
    *     w_ClnComp_Gio = w_ctList_Giorno.AddColumn("% comp.",60,0,0,.F.,.T.,0)
    * --- Primo parametro: sub text attivo
    *     secondo: colonna di partenza
    *     terzo: colonna di fine visualizzazione sub text
    this.oParentObject.w_ctList_Giorno.SubTextNodes(.T., this.oParentObject.w_ClnOgg_Gio, this.oParentObject.w_ClnOgg_Gio)     
    * --- Inizializza il tipo di default
    * --- Read from PAR_AGEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PACAUATT"+;
        " from "+i_cTable+" PAR_AGEN where ";
            +"PACODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PACAUATT;
        from (i_cTable) where;
            PACODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_CauDefault = NVL(cp_ToDate(_read_.PACAUATT),cp_NullValue(_read_.PACAUATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NVL(ALLTRIM(this.oParentObject.w_CauDefault),"")==""
      this.oParentObject.w_CauDefault = SPACE(20)
      this.oParentObject.w_AskCau = 1
    endif
    this.oParentObject.w_ctDropDate.SetVisible(.F.)     
    * --- Settaggio font calendario
    this.oParentObject.w_ctDate.CAL.Font.Name = g_GioStdFontName
    this.oParentObject.w_ctDate.CAL.Font.Size = g_GioStdFontSize
    this.oParentObject.w_ctDate.CAL.Font.Italic = g_bGioStdFontItalic
    this.oParentObject.w_ctDate.CAL.Font.Bold = g_bGioStdFontBold
    this.oParentObject.w_ctDate.CAL.DayFont1.Name = g_GioImpFontName
    this.oParentObject.w_ctDate.CAL.DayFont1.Size = g_GioImpFontSize
    this.oParentObject.w_ctDate.CAL.DayFont1.Italic = g_bGioImpFontItalic
    this.oParentObject.w_ctDate.CAL.DayFont1.Bold = g_bGioImpFontBold
    this.oParentObject.w_ctYear.CAL.Font.Name = g_GioStdFontName
    this.oParentObject.w_ctYear.CAL.Font.Size = g_GioStdFontSize
    this.oParentObject.w_ctYear.CAL.Font.Italic = g_bGioStdFontItalic
    this.oParentObject.w_ctYear.CAL.Font.Bold = g_bGioStdFontBold
    this.oParentObject.w_ctYear.CAL.DayFont1.Name = g_GioImpFontName
    this.oParentObject.w_ctYear.CAL.DayFont1.Size = g_GioImpFontSize
    this.oParentObject.w_ctYear.CAL.DayFont1.Italic = g_bGioImpFontItalic
    this.oParentObject.w_ctYear.CAL.DayFont1.Bold = g_bGioImpFontBold
    this.oParentObject.w_ct1Day.CAL.heightOffset = g_AgeDayHeightOffset
    this.oParentObject.w_ct5Day.CAL.heightOffset = g_AgeDayHeightOffset
    this.oParentObject.w_ctList_Memo.CAL.subTextFont.Size = this.oParentObject.w_ctList_Memo.CAL.Font.Size
  endproc


  procedure Page_19
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- gestiamo i MEMO in CTLIST
    if this.oParentObject.w_ClnBMP>0 and this.oParentObject.w_ClnOgg>0 and this.oParentObject.w_ClnComp>0
      * --- Se tutti i valori sono maggiori di zero, � segno che tutte le colonne sono state correttamente inserite
      this.oParentObject.w_ctList_Memo.SetVisible(.T.)     
      this.oParentObject.w_ctList_Memo.ClearList()     
      Dimension aText(6) 
 store "" to aText(1),aText(2),aText(3),aText(4), aText(5), aText(6)
      * --- Nella query GSAG4BAG estraiamo i memo ed uniamo il risultato di GSAG5BAG in cui sono estratti i preavvisi
      * --- Select from QUERY\GSAG4BAG
      do vq_exec with 'QUERY\GSAG4BAG',this,'_Curs_QUERY_GSAG4BAG','',.f.,.t.
      if used('_Curs_QUERY_GSAG4BAG')
        select _Curs_QUERY_GSAG4BAG
        locate for 1=1
        do while not(eof())
        * --- Inseriamo ogni elemento nell'array
        * --- Spazio per il BMP
        aText( this.oParentObject.w_ClnBMP ) = " "
        * --- Oggetto
        aText( this.oParentObject.w_ClnOgg ) = ALLTRIM(ATOGGETT)
        if g_Age_FlComp="S"
          * --- Completamento
          aText( this.oParentObject.w_ClnComp ) = ALLTRIM(STR(ATPERCOM))
        else
          if !EMPTY(ATDATORD)
            aText( this.oParentObject.w_ClnComp ) = ALLTRIM(DTOC(TTOD(ATDATORD)))
          else
            aText( this.oParentObject.w_ClnComp ) = ""
          endif
        endif
        * --- Componiamo il testo dell'item
        this.w_TxtItem = aText(1)+CHR(10)+aText(2)+CHR(10)+aText(3)
        this.ind = this.oParentObject.w_ctList_Memo.AddItem(this.w_TxtItem, ATSERIAL)
        if ATSTATUS$"D|I|T"
          * --- && Non ancora completata - DEselezionato
          this.w_StatusItem = 0
        else
          * --- && Completata - selezionato
          this.w_StatusItem = 1
        endif
        * --- Gestiamo il BMP
        do case
          case CARAGGST="M"
            this.oParentObject.w_ctList_Memo.CellPicture(this.Ind, this.oParentObject.w_ClnBMP, this.oParentObject.w_ImgNote)     
          case CARAGGST="D"
            this.oParentObject.w_ctList_Memo.CellPicture(this.Ind, this.oParentObject.w_ClnBMP, this.oParentObject.w_ImgDaFare)     
          otherwise
            this.oParentObject.w_ctList_Memo.CellPicture(this.Ind, this.oParentObject.w_ClnBMP, this.oParentObject.w_ImgPreav)     
        endcase
        * --- Gestiamo il subText
        this.w_TxtItem = "Entro il: "+DTOC(TTOD(ATDATORD))
        if NVL(ALLTRIM(ATCODPRA)," ")>" "
          this.w_TxtItem = this.w_TxtItem + CHR(13)+CHR(10)+IIF(ISALT(),Ah_MsgFormat("Pratica: "),Ah_MsgFormat("Commessa: "))+ALLTRIM(ATCODPRA)+" "+ALLTRIM(CNDESCAN)
        endif
        this.oParentObject.w_ctList_Memo.SubText(this.ind, this.w_TxtItem, this.oParentObject.w_VisNote=="S")     
          select _Curs_QUERY_GSAG4BAG
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Page_20
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- gestiamo le attivit� del giorno in CTLIST_GIORNO
    if this.oParentObject.w_ClnOgg_Gio>0 AND this.oParentObject.w_ClnDataIni_Gio>0 AND this.oParentObject.w_ClnDataFin_Gio>0
      * --- Se tutti i valori sono maggiori di zero, � segno che tutte le colonne sono state correttamente inserite
      Dimension aText(7) 
 store "" to aText(1),aText(2),aText(3),aText(4),aText(5),aText(6),aText(7)
      * --- Utilizziamo w_Data_Ini e w_Data_Fin per GSAG_BAG, quindi ne salviamo il valore in w_SAV_INI e w_SAV_FIN
      this.w_Sav_Ini = this.oParentObject.w_Data_Ini
      this.w_Sav_Fin = this.oParentObject.w_Data_Fin
      this.oParentObject.w_Data_Ini = DTOT(this.oParentObject.w_DataApp)
      this.oParentObject.w_Data_Fin = DTOT(this.oParentObject.w_DataApp)+3600*24-1
      this.oParentObject.w_ctList_Giorno.SetVisible(.T.)     
      this.oParentObject.w_ctList_Giorno.ClearList()     
      * --- Nella query GSAG_BAG estraiamo le attivit� del giorno
      * --- Select from QUERY\GSAG_BAG
      do vq_exec with 'QUERY\GSAG_BAG',this,'_Curs_QUERY_GSAG_BAG','',.f.,.t.
      if used('_Curs_QUERY_GSAG_BAG')
        select _Curs_QUERY_GSAG_BAG
        locate for 1=1
        do while not(eof())
        * --- Oggetto
        if Not IsRisAtt( _Curs_QUERY_GSAG_BAG.ATSERIAL , _Curs_QUERY_GSAG_BAG.ATFLATRI ) 
          * --- Se attivit� riservata maschero alcuni campi...
          *     (in progress la lettura dai parametri potr� essere sostituita da una var. pubblica letta all'avvio)
          this.w_BRISE = .T.
        else
          this.w_BRISE = .F.
        endif
        aText( this.oParentObject.w_ClnOgg_Gio ) = ALLTRIM( IIF( this.w_BRISE , this.w_RISMSG , _Curs_QUERY_GSAG_BAG.ATOGGETT) )
        * --- Data inizio
        this.w_DatIniItem = VAL(SYS(11,ATDATINI)) - this.w_nBaseCal
        * --- aText( w_ClnDataIni_Gio ) = PADL( ALLTRIM(STR(DAY(ATDATINI))), 2, '0')+'/'+PADL( ALLTRIM(STR(MONTH(ATDATINI))), 2, '0')+'/'+ALLTRIM(RIGHT(STR(YEAR(ATDATINI)),2))
        aText( this.oParentObject.w_ClnDataIni_Gio ) = ALLTRIM(STR(this.w_DatIniItem))
        aText( this.oParentObject.w_ClnGiornoDa_Gio ) = LEFT( g_Giorno[DOW(ATDATINI)] , 3 )
        * --- Data fine
        this.w_DatFinItem = VAL(SYS(11,ATDATFIN)) - this.w_nBaseCal
        aText( this.oParentObject.w_ClnDataFin_Gio ) = ALLTRIM(STR(this.w_DatFinItem))
        aText( this.oParentObject.w_ClnGiornoA_Gio ) = LEFT( g_Giorno[DOW(ATDATFIN)] , 3 )
        * --- Completamento - Non pi� utilizzato
        *     aText( w_ClnComp_Gio ) = ALLTRIM(STR(ATPERCOM))
        * --- Componiamo il testo dell'item
        this.w_TxtItem = aText(1)+CHR(10)+aText(2)+CHR(10)+aText(3)+CHR(10)+aText(4)+CHR(10)+aText(5)
        this.ind = this.oParentObject.w_ctList_Giorno.AddItem(this.w_TxtItem, ATSERIAL)
        if isalt()
          if !EMPTY(ALLTRIM(NVL(ATCODPRA," ")))
            this.w_TxtItem = IIF(ISALT(),Ah_MsgFormat("Pratica: "),Ah_MsgFormat("Commessa: "))+ IIF( this.w_BRISE , this.w_RISMSG , ALLTRIM(ATCODPRA)+" "+ALLTRIM(CNDESCAN)) 
          else
            this.w_TxtItem = ""
          endif
        else
          this.w_TxtItem = Ah_MsgFormat("Note: ")+ALLTRIM(NVL(ATNOTPIA,""))
          this.w_TxtItem = Ah_MsgFormat("Nominativo: ")+ALLTRIM(NVL(NODESCRI,""))
        endif
        this.oParentObject.w_ctList_Giorno.SubText(this.ind,this.w_TxtItem, this.oParentObject.w_VisNote=="S")     
          select _Curs_QUERY_GSAG_BAG
          continue
        enddo
        use
      endif
      * --- Ripristiniamo i valori corretti di w_Data_Ini e w_Data_Fin
      this.oParentObject.w_Data_Ini = this.w_Sav_Ini
      this.oParentObject.w_Data_Fin = this.w_Sav_Fin
    endif
  endproc


  procedure Page_21
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Deve modificare comlpetare l'attivit� il cui seriale � passato come parametro
    this.w_PerComp = 0
    if !EMPTY(this.oParentObject.w_Serial)
      * --- Leggiamo i dati dell'attivit�: la data inizio in w_ADatIni e la percentuale di completamento.
      * --- Read from OFF_ATTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATDATINI,ATPERCOM,ATCAUATT"+;
          " from "+i_cTable+" OFF_ATTI where ";
              +"ATSERIAL = "+cp_ToStrODBC(this.oParentObject.w_Serial);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATDATINI,ATPERCOM,ATCAUATT;
          from (i_cTable) where;
              ATSERIAL = this.oParentObject.w_Serial;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ADatIni = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
        this.w_PerComp = NVL(cp_ToDate(_read_.ATPERCOM),cp_NullValue(_read_.ATPERCOM))
        this.w_CAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.pAzione=="CHECKDISATTIVO"
        * --- Dobbiamo riportare lo stato in "Da completare" o "In corso"
        if this.w_PerComp=0
          * --- Non era neppure stata iniziata -> Da svolgere
          this.w_StatoItem = "D"
        else
          * --- In corso
          this.w_StatoItem = "I"
        endif
      else
        * --- pAzione=='CHECKATTIVO'
        this.w_StatoItem = "F"
        if this.oParentObject.w_CalAttivo<=4
          * --- CtList_Memo
          * --- Read from CAUMATTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAUMATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CARAGGST"+;
              " from "+i_cTable+" CAUMATTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_CAUATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CARAGGST;
              from (i_cTable) where;
                  CACODICE = this.w_CAUATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_RAGGST = NVL(cp_ToDate(_read_.CARAGGST),cp_NullValue(_read_.CARAGGST))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if ! this.w_RAGGST $ "M|D"
            * --- E' un preavviso, dobbiamo togliere i giorni di preavvisto dall'attivit�
            this.w_StatoItem = " "
            * --- Write into OFF_ATTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ATGGPREA ="+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','ATGGPREA');
              +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
                  +i_ccchkf ;
              +" where ";
                  +"ATSERIAL = "+cp_ToStrODBC(this.oParentObject.w_Serial);
                     )
            else
              update (i_cTable) set;
                  ATGGPREA = 0;
                  ,ATDATOUT = DATETIME();
                  &i_ccchkf. ;
               where;
                  ATSERIAL = this.oParentObject.w_Serial;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
      * --- Aggiorniamo lo stato
      if !empty(this.w_StatoItem)
        * --- Write into OFF_ATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATSTATUS ="+cp_NullLink(cp_ToStrODBC(this.w_StatoItem),'OFF_ATTI','ATSTATUS');
          +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
              +i_ccchkf ;
          +" where ";
              +"ATSERIAL = "+cp_ToStrODBC(this.oParentObject.w_Serial);
                 )
        else
          update (i_cTable) set;
              ATSTATUS = this.w_StatoItem;
              ,ATDATOUT = DATETIME();
              &i_ccchkf. ;
           where;
              ATSERIAL = this.oParentObject.w_Serial;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc


  procedure Page_22
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Deve modificare cancellare l'attivit�
    * --- Instanzio l'anagrafica
    this.w_OBJECT = GSAG_AAT()
    * --- Controllo se ha passato il test di accesso
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- Cerco il record selezionato
    this.w_OBJECT.ECPFILTER()     
    this.w_OBJECT.w_ATSERIAL = this.oParentObject.w_Serial
    this.w_OBJECT.ECPSAVE()     
    if Not Empty( this.w_OBJECT.w_ATSERIAL )
      * --- Cancello se attivit� disponibile...
      this.w_OBJECT.ECPDELETE()     
    endif
    this.w_OBJECT.ECPQUIT()     
  endproc


  procedure Page_23
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Composizione descrizione attivit�
    * --- In Aetop le opzioni visibili sul tipo attivit� sono:
    *     - Oggetto attivit�
    *     - Partecipanti attivit�
    *     - Pratica
    *     - Soggetti esterni pratica
    *     - Giudice
    *     
    *     In AHR/AHE le opzioni visibili sul tipo attivit� sono:
    *     - Oggetto attivit�
    *     - Partecipanti attivit�
    *     - Commessa
    *     - Nominativo
    this.w_DESCR_ATT = SPACE(254)
    if _Curs_QUERY_GSAG_BAG.CAFLATTI="S" OR _Curs_QUERY_GSAG_BAG.CAFLPART="S" OR _Curs_QUERY_GSAG_BAG.CAFLPRAT="S" OR _Curs_QUERY_GSAG_BAG.CAFLSOGG="S" OR _Curs_QUERY_GSAG_BAG.CAFLGIUD="S" OR _Curs_QUERY_GSAG_BAG.CAFLNOMI="S" OR _Curs_QUERY_GSAG_BAG.CAFLDINI="S" OR _Curs_QUERY_GSAG_BAG.CAFLDFIN="S"
      Create cursor ComposAtt (OPZ C(1), ORD N(1))
      * --- Vengono inserite nel cursore tante righe quante sono le opzioni selezionate e poi ordinate per ordinamento
      if _Curs_QUERY_GSAG_BAG.CAFLATTI="S"
        * --- Oggetto attivit�
        INSERT INTO ComposAtt (OPZ, ORD) VALUES ("O", _Curs_QUERY_GSAG_BAG.CAFLATTN)
      endif
      if _Curs_QUERY_GSAG_BAG.CAFLPART="S"
        * --- Partecipanti attivit�
        INSERT INTO ComposAtt (OPZ, ORD) VALUES ("P", _Curs_QUERY_GSAG_BAG.CAFLPARN)
      endif
      if _Curs_QUERY_GSAG_BAG.CAFLPRAT="S"
        * --- Pratica
        INSERT INTO ComposAtt (OPZ, ORD) VALUES ("R", _Curs_QUERY_GSAG_BAG.CAFLPRAN)
      endif
      if _Curs_QUERY_GSAG_BAG.CAFLSOGG="S"
        * --- Soggetti esterni pratica
        INSERT INTO ComposAtt (OPZ, ORD) VALUES ("S", _Curs_QUERY_GSAG_BAG.CAFLSOGN)
      endif
      if _Curs_QUERY_GSAG_BAG.CAFLGIUD="S"
        * --- Giudice
        INSERT INTO ComposAtt (OPZ, ORD) VALUES ("G", _Curs_QUERY_GSAG_BAG.CAFLGIUN)
      endif
      if _Curs_QUERY_GSAG_BAG.CAFLNOMI="S"
        * --- Nominativo
        INSERT INTO ComposAtt (OPZ, ORD) VALUES ("N", _Curs_QUERY_GSAG_BAG.CAFLNOMN)
      endif
      if _Curs_QUERY_GSAG_BAG.CAFLDINI="S"
        * --- Ora inizio
        INSERT INTO ComposAtt (OPZ, ORD) VALUES ("I", _Curs_QUERY_GSAG_BAG.CAFLDINN)
      endif
      if _Curs_QUERY_GSAG_BAG.CAFLDFIN="S"
        * --- Ora fine
        INSERT INTO ComposAtt (OPZ, ORD) VALUES ("F", _Curs_QUERY_GSAG_BAG.CAFLDFNN)
      endif
      SELECT * FROM ComposAtt INTO CURSOR ComposAtt ORDER BY ORD
      SELECT ComposAtt 
 GO TOP
      SCAN
      do case
        case OPZ = "O"
          * --- Oggetto attivit�
          this.w_DESCR_ATT = this.w_DESCR_ATT + ALLTRIM(_Curs_QUERY_GSAG_BAG.ATOGGETT) + SPACE(1)
        case OPZ = "P"
          * --- Partecipanti attivit�
          this.w_PartSerial = _Curs_QUERY_GSAG_BAG.ATSERIAL
          this.w_TextPart = ""
          * --- Select from QUERY\GSAG6BAG.VQR
          do vq_exec with 'QUERY\GSAG6BAG.VQR',this,'_Curs_QUERY_GSAG6BAG_d_VQR','',.f.,.t.
          if used('_Curs_QUERY_GSAG6BAG_d_VQR')
            select _Curs_QUERY_GSAG6BAG_d_VQR
            locate for 1=1
            do while not(eof())
            do case
              case DPTIPRIS <> "R"
                this.w_TextPart = this.w_TextPart+ ALLTRIM(DESCR_DIP)+","+SPACE(1)
            endcase
              select _Curs_QUERY_GSAG6BAG_d_VQR
              continue
            enddo
            use
          endif
          if !EMPTY(this.w_TextPart)
            this.w_TextPart = SPACE(1)+"["+SUBSTR(this.w_TextPart,1, LEN(this.w_TextPart)-2 )+"]"
            this.w_DESCR_ATT = this.w_DESCR_ATT + this.w_TextPart + SPACE(1)
          endif
        case OPZ = "R"
          * --- Nome pratica
          if NOT EMPTY(_Curs_QUERY_GSAG_BAG.CNDESCAN)
            this.w_DESCR_ATT = this.w_DESCR_ATT + Ah_MsgFormat(IIF(ISALT(), IIF(this.oParentObject.w_StrPratica="S","Pr","Pratica"), "Commessa"))+":"+SPACE(1)+ ALLTRIM(_Curs_QUERY_GSAG_BAG.CNDESCAN) + SPACE(1)
          endif
        case OPZ = "S"
          * --- Soggetti esterni pratica
          if NOT EMPTY(_Curs_QUERY_GSAG_BAG.CNCODCAN)
            this.CodPra = _Curs_QUERY_GSAG_BAG.CNCODCAN
            this.w_RisEste = ""
            * --- Select from QUERY\GSAG8BAG.VQR
            do vq_exec with 'QUERY\GSAG8BAG.VQR',this,'_Curs_QUERY_GSAG8BAG_d_VQR','',.f.,.t.
            if used('_Curs_QUERY_GSAG8BAG_d_VQR')
              select _Curs_QUERY_GSAG8BAG_d_VQR
              locate for 1=1
              do while not(eof())
              this.w_RisEste = this.w_RisEste + ALLTRIM(CSDESCAT)+": "+ALLTRIM(NODESCRI)+SPACE(1)
                select _Curs_QUERY_GSAG8BAG_d_VQR
                continue
              enddo
              use
            endif
            if NOT EMPTY(this.w_RisEste)
              this.w_RisEste = SPACE(1)+"["+SUBSTR(this.w_RisEste,1, LEN(this.w_RisEste)-1 )+"]"
              this.w_DESCR_ATT = this.w_DESCR_ATT + this.w_RisEste + SPACE(1)
            endif
          endif
        case OPZ = "G"
          * --- Giudici
          if NOT EMPTY(_Curs_QUERY_GSAG_BAG.CNCODCAN)
            this.CodPra = _Curs_QUERY_GSAG_BAG.CNCODCAN
            this.w_Data1 = _Curs_QUERY_GSAG_BAG.ATDATINI
            this.w_Data2 = _Curs_QUERY_GSAG_BAG.ATDATFIN
            this.w_RisEste = ""
            * --- Select from QUERY\GSAG9BAG.VQR
            do vq_exec with 'QUERY\GSAG9BAG.VQR',this,'_Curs_QUERY_GSAG9BAG_d_VQR','',.f.,.t.
            if used('_Curs_QUERY_GSAG9BAG_d_VQR')
              select _Curs_QUERY_GSAG9BAG_d_VQR
              locate for 1=1
              do while not(eof())
              this.w_RisEste = this.w_RisEste + ALLTRIM(CSDESCAT)+": "+ALLTRIM(NODESCRI)+SPACE(1)
                select _Curs_QUERY_GSAG9BAG_d_VQR
                continue
              enddo
              use
            endif
            if NOT EMPTY(this.w_RisEste)
              this.w_RisEste = SPACE(1)+"["+SUBSTR(this.w_RisEste,1, LEN(this.w_RisEste)-1 )+"]"
              this.w_DESCR_ATT = this.w_DESCR_ATT + this.w_RisEste + SPACE(1)
            endif
          endif
        case OPZ = "N"
          * --- Descrizione nominativo
          if NOT EMPTY(_Curs_QUERY_GSAG_BAG.NODESCRI)
            this.w_DESCR_ATT = this.w_DESCR_ATT + "Nominativo: " + ALLTRIM(_Curs_QUERY_GSAG_BAG.NODESCRI) + SPACE(1)
          endif
        case OPZ = "I"
          * --- Ora inizio
          this.w_Data1 = _Curs_QUERY_GSAG_BAG.ATDATINI
          this.w_DESCR_ATT = this.w_DESCR_ATT + Ah_MsgFormat("Inizio")+":"+SPACE(1)+PADL(ALLTRIM(STR(HOUR(this.w_Data1))),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_Data1))),2,"0")+ SPACE(1)
        case OPZ = "F"
          * --- Ora fine
          this.w_Data1 = _Curs_QUERY_GSAG_BAG.ATDATFIN
          this.w_DESCR_ATT = this.w_DESCR_ATT + Ah_MsgFormat("Fine")+":"+SPACE(1)+PADL(ALLTRIM(STR(HOUR(this.w_Data1))),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_Data1))),2,"0")+ SPACE(1)
      endcase
      ENDSCAN
      SELECT ComposAtt
      USE
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAZIONE)
    this.pAZIONE=pAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,17)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='PRA_ENTI'
    this.cWorkTables[3]='PRA_UFFI'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='PRA_OGGE'
    this.cWorkTables[6]='OFF_PART'
    this.cWorkTables[7]='CAUMATTI'
    this.cWorkTables[8]='CAU_ATTI'
    this.cWorkTables[9]='ART_ICOL'
    this.cWorkTables[10]='DIPENDEN'
    this.cWorkTables[11]='OFFDATTI'
    this.cWorkTables[12]='PRA_UBIC'
    this.cWorkTables[13]='PAR_AGEN'
    this.cWorkTables[14]='KEY_ARTI'
    this.cWorkTables[15]='COM_ATTI'
    this.cWorkTables[16]='IMP_MAST'
    this.cWorkTables[17]='OFF_NOMI'
    return(this.OpenAllTables(17))

  proc CloseCursors()
    if used('_Curs_QUERY_GSAG_BAG')
      use in _Curs_QUERY_GSAG_BAG
    endif
    if used('_Curs_QUERY_GSAG_BAG')
      use in _Curs_QUERY_GSAG_BAG
    endif
    if used('_Curs_QUERY_GSAG_BAG')
      use in _Curs_QUERY_GSAG_BAG
    endif
    if used('_Curs_QUERY_GSAG2BAG')
      use in _Curs_QUERY_GSAG2BAG
    endif
    if used('_Curs_QUERY_GSAG_BAG')
      use in _Curs_QUERY_GSAG_BAG
    endif
    if used('_Curs_QUERY_GSAG2BAG')
      use in _Curs_QUERY_GSAG2BAG
    endif
    if used('_Curs_QUERY_GSAG6BAG_d_VQR')
      use in _Curs_QUERY_GSAG6BAG_d_VQR
    endif
    if used('_Curs_QUERY_GSAG7BAG_d_VQR')
      use in _Curs_QUERY_GSAG7BAG_d_VQR
    endif
    if used('_Curs_GSAG1BAG')
      use in _Curs_GSAG1BAG
    endif
    if used('_Curs_QUERY_GSAG_BAG')
      use in _Curs_QUERY_GSAG_BAG
    endif
    if used('_Curs_GSAG1BAG')
      use in _Curs_GSAG1BAG
    endif
    if used('_Curs_QUERY_GSAG4BAG')
      use in _Curs_QUERY_GSAG4BAG
    endif
    if used('_Curs_QUERY_GSAG_BAG')
      use in _Curs_QUERY_GSAG_BAG
    endif
    if used('_Curs_QUERY_GSAG6BAG_d_VQR')
      use in _Curs_QUERY_GSAG6BAG_d_VQR
    endif
    if used('_Curs_QUERY_GSAG8BAG_d_VQR')
      use in _Curs_QUERY_GSAG8BAG_d_VQR
    endif
    if used('_Curs_QUERY_GSAG9BAG_d_VQR')
      use in _Curs_QUERY_GSAG9BAG_d_VQR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIONE"
endproc
