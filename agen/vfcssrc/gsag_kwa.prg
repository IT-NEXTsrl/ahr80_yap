* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kwa                                                        *
*              Wizard caricamento attivit�                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-15                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kwa",oParentObject))

* --- Class definition
define class tgsag_kwa as StdForm
  Top    = 7
  Left   = 9

  * --- Standard Properties
  Width  = 622
  Height = 328+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-13"
  HelpContextID=32122729
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=104

  * --- Constant Properties
  _IDX = 0
  CAU_UTEN_IDX = 0
  OFF_NOMI_IDX = 0
  CAUMATTI_IDX = 0
  PAR_AGEN_IDX = 0
  CAN_TIER_IDX = 0
  PRA_ENTI_IDX = 0
  PRA_UFFI_IDX = 0
  NOM_CONT_IDX = 0
  TIP_RISE_IDX = 0
  OFFTIPAT_IDX = 0
  CENCOST_IDX = 0
  LISTINI_IDX = 0
  CONTI_IDX = 0
  TIP_DOCU_IDX = 0
  cPrg = "gsag_kwa"
  cComment = "Wizard caricamento attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PRCODUTE = space(5)
  o_PRCODUTE = space(5)
  w_ATCAUATT = space(20)
  o_ATCAUATT = space(20)
  w_LinkCausali = space(20)
  w_CAORASYS = space(1)
  w_ATDURORE = 0
  w_ATDURMIN = 0
  w_CADTIN = ctot('')
  w_ATOGGETT = space(254)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_OREFIN = space(2)
  w_MINFIN = space(2)
  w_CUR_STEP = 0
  w_TOT_STEP = 0
  w_ATCODPRA = space(15)
  o_ATCODPRA = space(15)
  w_ATCODNOM = space(15)
  o_ATCODNOM = space(15)
  w_ATLOCALI = space(30)
  w_AT__ENTE = space(10)
  w_ATUFFICI = space(10)
  w_DESNOM = space(100)
  w_LetturaParAgen = space(10)
  w_PACHKFES = space(1)
  w_DESPRA = space(100)
  w_COMODO = space(50)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CACHKOBB = space(1)
  w_MATETIPOL = space(1)
  w_DATOBSONOM = ctod('  /  /  ')
  w_OFDATDOC = space(10)
  w_TIPNOM = space(1)
  w_CACHKNOM = space(1)
  w_CARAGGST = space(10)
  w_FLNSAP = space(1)
  w_ATCODVAL = space(3)
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_IVALIS = space(1)
  w_QUALIS = space(1)
  w_VALLIS = space(3)
  w_FLSCOR = space(1)
  w_NUMLIS = space(5)
  w_CODCLI = space(15)
  w_ATSTATUS = space(1)
  o_ATSTATUS = space(1)
  w_ATSTATUS = space(1)
  w_ATPERCOM = 0
  o_ATPERCOM = 0
  w_ATFLNOTI = space(10)
  w_ATGGPREA = 0
  w_ATNUMPRI = 0
  w_ATFLPROM = space(1)
  o_ATFLPROM = space(1)
  w_DATAPRO = ctod('  /  /  ')
  o_DATAPRO = ctod('  /  /  ')
  w_ORAPRO = space(2)
  o_ORAPRO = space(2)
  w_MINPRO = space(2)
  o_MINPRO = space(2)
  w_ATTIPRIS = space(1)
  w_ATPUBWEB = space(10)
  w_ATSTAATT = 0
  w_ATFLATRI = space(1)
  o_ATFLATRI = space(1)
  w_ATCODATT = space(5)
  w_ATPROMEM = ctot('')
  w_CAMINPRE = 0
  w_GESRIS = space(1)
  w_FLTRIS = space(1)
  w_PATIPRIS = space(1)
  w_PATIPRIS = space(1)
  w_CAUDOC = space(5)
  w_ATCENCOS = space(15)
  w_ATCONTAT = space(5)
  w_ATPERSON = space(60)
  w_ATTELEFO = space(18)
  w_ATCELLUL = space(18)
  w_AT_EMAIL = space(18)
  w_AT_EMPEC = space(254)
  w_ATNOTPIA = space(0)
  w_OB_TEST = ctod('  /  /  ')
  w_GIOPRM = space(10)
  w_DESTIPOL = space(50)
  w_DESNOM = space(40)
  w_OFCODNOM = space(15)
  w_CCDESPIA = space(40)
  w_VARLOG = .F.
  w_FLDANA = space(1)
  w_FLANAL = space(1)
  w_GIOINI = space(10)
  w_GIOFIN = space(10)
  w_CATIPPRA = space(10)
  w_CAMATPRA = space(10)
  w_CA__ENTE = space(10)
  w_RESCHK = 0
  w_PAFLVISI = space(1)
  w_PACHKATT = 0
  w_ATFLRICO = space(1)
  w_PACHKCAR = space(1)
  w_ATDATINI = ctot('')
  w_ATDATFIN = ctot('')
  w_CODLIS = space(5)
  w_ATCODLIS = space(5)
  w_DESLIS = space(40)
  w_NCODLIS = space(5)
  w_CNDATFIN = ctod('  /  /  ')
  w_CENOBSO = ctod('  /  /  ')
  w_FLINTE = space(1)
  w_FLCOSE = space(1)
  w_ZOOMPART = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kwaPag1","gsag_kwa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati principali")
      .Pages(2).addobject("oPag","tgsag_kwaPag2","gsag_kwa",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati secondari")
      .Pages(3).addobject("oPag","tgsag_kwaPag3","gsag_kwa",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Partecipanti")
      .Pages(4).addobject("oPag","tgsag_kwaPag4","gsag_kwa",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Note")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oATCAUATT_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsag_kwa
    * Nascondo le tabs in vecchia configurazione interfaccia
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMPART = this.oPgFrm.Pages(3).oPag.ZOOMPART
    DoDefault()
    proc Destroy()
      this.w_ZOOMPART = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='CAU_UTEN'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='CAUMATTI'
    this.cWorkTables[4]='PAR_AGEN'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='PRA_ENTI'
    this.cWorkTables[7]='PRA_UFFI'
    this.cWorkTables[8]='NOM_CONT'
    this.cWorkTables[9]='TIP_RISE'
    this.cWorkTables[10]='OFFTIPAT'
    this.cWorkTables[11]='CENCOST'
    this.cWorkTables[12]='LISTINI'
    this.cWorkTables[13]='CONTI'
    this.cWorkTables[14]='TIP_DOCU'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      GSAG_BWA('INS_ATT')
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PRCODUTE=space(5)
      .w_ATCAUATT=space(20)
      .w_LinkCausali=space(20)
      .w_CAORASYS=space(1)
      .w_ATDURORE=0
      .w_ATDURMIN=0
      .w_CADTIN=ctot("")
      .w_ATOGGETT=space(254)
      .w_DATINI=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_DATFIN=ctod("  /  /  ")
      .w_OREFIN=space(2)
      .w_MINFIN=space(2)
      .w_CUR_STEP=0
      .w_TOT_STEP=0
      .w_ATCODPRA=space(15)
      .w_ATCODNOM=space(15)
      .w_ATLOCALI=space(30)
      .w_AT__ENTE=space(10)
      .w_ATUFFICI=space(10)
      .w_DESNOM=space(100)
      .w_LetturaParAgen=space(10)
      .w_PACHKFES=space(1)
      .w_DESPRA=space(100)
      .w_COMODO=space(50)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_CACHKOBB=space(1)
      .w_MATETIPOL=space(1)
      .w_DATOBSONOM=ctod("  /  /  ")
      .w_OFDATDOC=space(10)
      .w_TIPNOM=space(1)
      .w_CACHKNOM=space(1)
      .w_CARAGGST=space(10)
      .w_FLNSAP=space(1)
      .w_ATCODVAL=space(3)
      .w_INILIS=ctod("  /  /  ")
      .w_FINLIS=ctod("  /  /  ")
      .w_IVALIS=space(1)
      .w_QUALIS=space(1)
      .w_VALLIS=space(3)
      .w_FLSCOR=space(1)
      .w_NUMLIS=space(5)
      .w_CODCLI=space(15)
      .w_ATSTATUS=space(1)
      .w_ATSTATUS=space(1)
      .w_ATPERCOM=0
      .w_ATFLNOTI=space(10)
      .w_ATGGPREA=0
      .w_ATNUMPRI=0
      .w_ATFLPROM=space(1)
      .w_DATAPRO=ctod("  /  /  ")
      .w_ORAPRO=space(2)
      .w_MINPRO=space(2)
      .w_ATTIPRIS=space(1)
      .w_ATPUBWEB=space(10)
      .w_ATSTAATT=0
      .w_ATFLATRI=space(1)
      .w_ATCODATT=space(5)
      .w_ATPROMEM=ctot("")
      .w_CAMINPRE=0
      .w_GESRIS=space(1)
      .w_FLTRIS=space(1)
      .w_PATIPRIS=space(1)
      .w_PATIPRIS=space(1)
      .w_CAUDOC=space(5)
      .w_ATCENCOS=space(15)
      .w_ATCONTAT=space(5)
      .w_ATPERSON=space(60)
      .w_ATTELEFO=space(18)
      .w_ATCELLUL=space(18)
      .w_AT_EMAIL=space(18)
      .w_AT_EMPEC=space(254)
      .w_ATNOTPIA=space(0)
      .w_OB_TEST=ctod("  /  /  ")
      .w_GIOPRM=space(10)
      .w_DESTIPOL=space(50)
      .w_DESNOM=space(40)
      .w_OFCODNOM=space(15)
      .w_CCDESPIA=space(40)
      .w_VARLOG=.f.
      .w_FLDANA=space(1)
      .w_FLANAL=space(1)
      .w_GIOINI=space(10)
      .w_GIOFIN=space(10)
      .w_CATIPPRA=space(10)
      .w_CAMATPRA=space(10)
      .w_CA__ENTE=space(10)
      .w_RESCHK=0
      .w_PAFLVISI=space(1)
      .w_PACHKATT=0
      .w_ATFLRICO=space(1)
      .w_PACHKCAR=space(1)
      .w_ATDATINI=ctot("")
      .w_ATDATFIN=ctot("")
      .w_CODLIS=space(5)
      .w_ATCODLIS=space(5)
      .w_DESLIS=space(40)
      .w_NCODLIS=space(5)
      .w_CNDATFIN=ctod("  /  /  ")
      .w_CENOBSO=ctod("  /  /  ")
      .w_FLINTE=space(1)
      .w_FLCOSE=space(1)
        .w_PRCODUTE = readdipend(i_CodUte, 'C')
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ATCAUATT))
          .link_1_2('Full')
        endif
        .w_LinkCausali = .w_ATCAUATT
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_LinkCausali))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,8,.f.)
        .w_DATINI = i_datSys
        .w_OREINI = PADL(ALLTRIM(STR(HOUR(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        .w_MININI = PADL(ALLTRIM(STR(MINUTE(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        .w_DATFIN = .w_DATINI+INT( (VAL(.w_OREINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)) / 24)
        .w_OREFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_OREINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)),24))),2)
        .w_MINFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_MININI)+.w_ATDURMIN), 60),2)),2)
        .w_CUR_STEP = 1
        .w_TOT_STEP = this.oPGFRM.PageCount
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_ATCODPRA))
          .link_1_19('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_ATCODNOM))
          .link_1_20('Full')
        endif
        .DoRTCalc(19,20,.f.)
        if not(empty(.w_AT__ENTE))
          .link_1_22('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_ATUFFICI))
          .link_1_23('Full')
        endif
          .DoRTCalc(22,22,.f.)
        .w_LetturaParAgen = i_CodAzi
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_LetturaParAgen))
          .link_1_33('Full')
        endif
          .DoRTCalc(24,27,.f.)
        .w_OBTEST = i_DatSys
          .DoRTCalc(29,31,.f.)
        .w_OFDATDOC = i_DatSys
          .DoRTCalc(33,35,.f.)
        .w_FLNSAP = 'N'
        .w_ATCODVAL = g_PERVAL
        .DoRTCalc(38,45,.f.)
        if not(empty(.w_CODCLI))
          .link_2_9('Full')
        endif
        .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        .w_ATPERCOM = ICASE(.w_ATSTATUS $ 'FP' , 100 , .w_ATSTATUS='D' , 0, .w_ATPERCOM)
        .w_ATFLNOTI = 'N'
          .DoRTCalc(50,50,.f.)
        .w_ATNUMPRI = IIF(EMPTY(.w_ATNUMPRI),1,.w_ATNUMPRI)
        .w_ATFLPROM = 'N'
          .DoRTCalc(53,55,.f.)
        .w_ATTIPRIS = IIF(.w_ATSTATUS $ 'FP',.w_ATTIPRIS,space(1))
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_ATTIPRIS))
          .link_2_20('Full')
        endif
        .w_ATPUBWEB = 'S'
        .w_ATSTAATT = 1
        .w_ATFLATRI = IIF(EMPTY(NVL(.w_ATFLATRI, ' ')), 'N', .w_ATFLATRI)
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_ATCODATT))
          .link_2_24('Full')
        endif
          .DoRTCalc(61,64,.f.)
        .w_PATIPRIS = 'P'
      .oPgFrm.Page3.oPag.ZOOMPART.Calculate()
        .DoRTCalc(66,67,.f.)
        if not(empty(.w_CAUDOC))
          .link_4_1('Full')
        endif
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_ATCENCOS))
          .link_4_2('Full')
        endif
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_ATCONTAT))
          .link_4_3('Full')
        endif
          .DoRTCalc(70,75,.f.)
        .w_OB_TEST = i_datsys
          .DoRTCalc(77,82,.f.)
        .w_FLDANA = Docgesana(.w_CAUDOC,'A')
          .DoRTCalc(84,84,.f.)
        .w_GIOINI = IIF(NOT EMPTY(.w_DATINI),LEFT(g_GIORNO[DOW(.w_DATINI)],3), '   ')
        .w_GIOFIN = IIF(NOT EMPTY(.w_DATFIN),LEFT(g_GIORNO[DOW(.w_DATFIN)],3), '   ')
          .DoRTCalc(87,89,.f.)
        .w_RESCHK = 0
          .DoRTCalc(91,92,.f.)
        .w_ATFLRICO = 'N'
          .DoRTCalc(94,94,.f.)
        .w_ATDATINI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '  -  -       :  :  '))
        .w_ATDATFIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00', '  -  -       :  :  '))
          .DoRTCalc(97,97,.f.)
        .w_ATCODLIS = IIF(EMPTY(.w_ATCODLIS) or .w_ATCODLIS=.w_CODLIS, IIF(EMPTY(.w_NUMLIS), IIF(Not Empty(.w_NCODLIS),.w_NCODLIS,.w_CODLIS), .w_NUMLIS) ,.w_ATCODLIS)
        .DoRTCalc(98,98,.f.)
        if not(empty(.w_ATCODLIS))
          .link_2_46('Full')
        endif
    endwith
    this.DoRTCalc(99,104,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page2.oPag.oBtn_2_28.enabled = this.oPgFrm.Page2.oPag.oBtn_2_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_4.enabled = this.oPgFrm.Page3.oPag.oBtn_3_4.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_5.enabled = this.oPgFrm.Page3.oPag.oBtn_3_5.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_9.enabled = this.oPgFrm.Page3.oPag.oBtn_3_9.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_12.enabled = this.oPgFrm.Page4.oPag.oBtn_4_12.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_12.enabled = this.oPgFrm.Page3.oPag.oBtn_3_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsag_kwa
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_PRCODUTE = readdipend(i_CodUte, 'C')
        .DoRTCalc(2,2,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_LinkCausali = .w_ATCAUATT
          .link_1_3('Full')
        endif
        .DoRTCalc(4,9,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_OREINI = PADL(ALLTRIM(STR(HOUR(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        endif
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_MININI = PADL(ALLTRIM(STR(MINUTE(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        endif
        if .o_DATINI<>.w_DATINI.or. .o_ATCAUATT<>.w_ATCAUATT.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATFIN = .w_DATINI+INT( (VAL(.w_OREINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)) / 24)
        endif
        if .o_ATCAUATT<>.w_ATCAUATT.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_OREFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_OREINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)),24))),2)
        endif
        if .o_ATCAUATT<>.w_ATCAUATT.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_MINFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_MININI)+.w_ATDURMIN), 60),2)),2)
        endif
        .DoRTCalc(15,22,.t.)
            .w_LetturaParAgen = i_CodAzi
          .link_1_33('Full')
        .DoRTCalc(24,44,.t.)
          .link_2_9('Full')
        if .o_ATSTATUS<>.w_ATSTATUS.or. .o_ATCAUATT<>.w_ATCAUATT.or. .o_ATPERCOM<>.w_ATPERCOM
            .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        endif
        if .o_ATCAUATT<>.w_ATCAUATT.or. .o_ATPERCOM<>.w_ATPERCOM
            .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        endif
        if .o_ATSTATUS<>.w_ATSTATUS
            .w_ATPERCOM = ICASE(.w_ATSTATUS $ 'FP' , 100 , .w_ATSTATUS='D' , 0, .w_ATPERCOM)
        endif
        .DoRTCalc(49,50,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_ATNUMPRI = IIF(EMPTY(.w_ATNUMPRI),1,.w_ATNUMPRI)
        endif
        .DoRTCalc(52,55,.t.)
        if .o_ATSTATUS<>.w_ATSTATUS
            .w_ATTIPRIS = IIF(.w_ATSTATUS $ 'FP',.w_ATTIPRIS,space(1))
          .link_2_20('Full')
        endif
        .DoRTCalc(57,58,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_ATFLATRI = IIF(EMPTY(NVL(.w_ATFLATRI, ' ')), 'N', .w_ATFLATRI)
        endif
        if .o_ATCAUATT<>.w_ATCAUATT
          .link_2_24('Full')
        endif
        if .o_DATAPRO<>.w_DATAPRO.or. .o_ORAPRO<>.w_ORAPRO.or. .o_MINPRO<>.w_MINPRO.or. .o_ATFLPROM<>.w_ATFLPROM
          .Calculate_MYDPUJWBKE()
        endif
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI.or. .o_ATFLPROM<>.w_ATFLPROM.or. .o_ATCAUATT<>.w_ATCAUATT
          .Calculate_GKYMFCTJZJ()
        endif
        .oPgFrm.Page3.oPag.ZOOMPART.Calculate()
        .DoRTCalc(61,66,.t.)
          .link_4_1('Full')
        if .o_ATFLATRI<>.w_ATFLATRI
          .Calculate_JZKXQUXVIN()
        endif
        if .o_PRCODUTE<>.w_PRCODUTE
          .Calculate_YELHEOSIYD()
        endif
        if .o_ATCODPRA<>.w_ATCODPRA
          .Calculate_BHKKPYIFKZ()
        endif
        .DoRTCalc(68,82,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_FLDANA = Docgesana(.w_CAUDOC,'A')
        endif
        .DoRTCalc(84,84,.t.)
        if .o_DATINI<>.w_DATINI
            .w_GIOINI = IIF(NOT EMPTY(.w_DATINI),LEFT(g_GIORNO[DOW(.w_DATINI)],3), '   ')
        endif
        if .o_DATFIN<>.w_DATFIN
            .w_GIOFIN = IIF(NOT EMPTY(.w_DATFIN),LEFT(g_GIORNO[DOW(.w_DATFIN)],3), '   ')
        endif
        .DoRTCalc(87,94,.t.)
            .w_ATDATINI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '  -  -       :  :  '))
            .w_ATDATFIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00', '  -  -       :  :  '))
        .DoRTCalc(97,97,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT.or. .o_ATCODPRA<>.w_ATCODPRA.or. .o_ATCODNOM<>.w_ATCODNOM
            .w_ATCODLIS = IIF(EMPTY(.w_ATCODLIS) or .w_ATCODLIS=.w_CODLIS, IIF(EMPTY(.w_NUMLIS), IIF(Not Empty(.w_NCODLIS),.w_NCODLIS,.w_CODLIS), .w_NUMLIS) ,.w_ATCODLIS)
          .link_2_46('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(99,104,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page3.oPag.ZOOMPART.Calculate()
    endwith
  return

  proc Calculate_MYDPUJWBKE()
    with this
          * --- Setta w_atpromem
          .w_ATPROMEM = cp_CharToDatetime( IIF(.w_ATFLPROM='S' AND NOT EMPTY(.w_DATAPRO),  DTOC(.w_DATAPRO)+' '+.w_ORAPRO+':'+.w_MINPRO+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_GKYMFCTJZJ()
    with this
          * --- Setta var. promemoria
          .w_ATPROMEM = IIF(.w_ATFLPROM='S',cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '  -  -       :  :  '))-IIF(.w_CAMINPRE>0,.w_CAMINPRE*60,0),DTOT(cp_CharToDate("  -  -    ")))
          .w_DATAPRO = IIF(.w_ATFLPROM='S',TTOD(.w_ATPROMEM),cp_CharToDate("  -  -    "))
          .w_ORAPRO = IIF(.w_ATFLPROM='S',PADL(ALLTRIM(STR(HOUR(.w_ATPROMEM))),2,'0'),'')
          .w_MINPRO = IIF(.w_ATFLPROM='S',PADL(ALLTRIM(STR(MINUTE(.w_ATPROMEM))),2,'0'),'')
          .w_GIOPRM = IIF(.w_ATFLPROM='S',LEFT(g_GIORNO[DOW(.w_ATPROMEM)],3),'')
    endwith
  endproc
  proc Calculate_JZKXQUXVIN()
    with this
          * --- Disabilita pubblica su Web se attivit� riservata
          .w_ATPUBWEB = IIF(.w_ATFLATRI='S' , 'N' , .w_ATPUBWEB)
    endwith
  endproc
  proc Calculate_HFDBZOGJCM()
    with this
          * --- Messaggio per utente non mappato in persone
          .w_VARLOG = iif(EMPTY(.w_PRCODUTE), ah_ErrorMsg("Attenzione: l'utente "+alltrim(g_desute)+" non � identificato fra le persone"), .T.)
    endwith
  endproc
  proc Calculate_YELHEOSIYD()
    with this
          * --- Rilegge i tipi attivit�
          RivalorizzoCombo(this;
             )
    endwith
  endproc
  proc Calculate_BHKKPYIFKZ()
    with this
          * --- E' stata variata la pratica - eventualmente aggiorna i partecipanti
          gsag_bwa(this;
              ,'VARIATA_PRAT';
             )
    endwith
  endproc
  proc Calculate_SJMHBUZTAZ()
    with this
          * --- Focus su ATCAUATT
          gsag_bwa(this;
              ,'INIT_PAGE_FRAME';
             )
    endwith
  endproc
  proc Calculate_JRJHOSKADX()
    with this
          * --- Calcoli e controlli a checkform - GSAG_BCO
          gsag_bco(this;
              ,'W';
             )
    endwith
  endproc
  proc Calculate_YGAAQKHNUA()
    with this
          * --- Controlli a check form
          gsag_bco(this;
              ,'W';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATCODPRA_1_19.enabled = this.oPgFrm.Page1.oPag.oATCODPRA_1_19.mCond()
    this.oPgFrm.Page1.oPag.oATCODNOM_1_20.enabled = this.oPgFrm.Page1.oPag.oATCODNOM_1_20.mCond()
    this.oPgFrm.Page2.oPag.oATSTATUS_2_10.enabled = this.oPgFrm.Page2.oPag.oATSTATUS_2_10.mCond()
    this.oPgFrm.Page2.oPag.oATSTATUS_2_11.enabled = this.oPgFrm.Page2.oPag.oATSTATUS_2_11.mCond()
    this.oPgFrm.Page2.oPag.oDATAPRO_2_17.enabled = this.oPgFrm.Page2.oPag.oDATAPRO_2_17.mCond()
    this.oPgFrm.Page2.oPag.oORAPRO_2_18.enabled = this.oPgFrm.Page2.oPag.oORAPRO_2_18.mCond()
    this.oPgFrm.Page2.oPag.oMINPRO_2_19.enabled = this.oPgFrm.Page2.oPag.oMINPRO_2_19.mCond()
    this.oPgFrm.Page2.oPag.oATTIPRIS_2_20.enabled = this.oPgFrm.Page2.oPag.oATTIPRIS_2_20.mCond()
    this.oPgFrm.Page3.oPag.oPATIPRIS_3_1.enabled = this.oPgFrm.Page3.oPag.oPATIPRIS_3_1.mCond()
    this.oPgFrm.Page3.oPag.oPATIPRIS_3_2.enabled = this.oPgFrm.Page3.oPag.oPATIPRIS_3_2.mCond()
    this.oPgFrm.Page4.oPag.oATCONTAT_4_3.enabled = this.oPgFrm.Page4.oPag.oATCONTAT_4_3.mCond()
    this.oPgFrm.Page4.oPag.oATPERSON_4_4.enabled = this.oPgFrm.Page4.oPag.oATPERSON_4_4.mCond()
    this.oPgFrm.Page4.oPag.oATTELEFO_4_5.enabled = this.oPgFrm.Page4.oPag.oATTELEFO_4_5.mCond()
    this.oPgFrm.Page4.oPag.oATCELLUL_4_6.enabled = this.oPgFrm.Page4.oPag.oATCELLUL_4_6.mCond()
    this.oPgFrm.Page4.oPag.oAT_EMAIL_4_7.enabled = this.oPgFrm.Page4.oPag.oAT_EMAIL_4_7.mCond()
    this.oPgFrm.Page4.oPag.oAT_EMPEC_4_8.enabled = this.oPgFrm.Page4.oPag.oAT_EMPEC_4_8.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_25.enabled = this.oPgFrm.Page2.oPag.oBtn_2_25.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_26.enabled = this.oPgFrm.Page2.oPag.oBtn_2_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_6.enabled = this.oPgFrm.Page3.oPag.oBtn_3_6.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_7.enabled = this.oPgFrm.Page3.oPag.oBtn_3_7.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_10.enabled = this.oPgFrm.Page4.oPag.oBtn_4_10.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_26.enabled = this.oPgFrm.Page4.oPag.oBtn_4_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_78.enabled = this.oPgFrm.Page1.oPag.oBtn_1_78.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oDESPRA_1_36.visible=!this.oPgFrm.Page1.oPag.oDESPRA_1_36.mHide()
    this.oPgFrm.Page2.oPag.oATSTATUS_2_10.visible=!this.oPgFrm.Page2.oPag.oATSTATUS_2_10.mHide()
    this.oPgFrm.Page2.oPag.oATSTATUS_2_11.visible=!this.oPgFrm.Page2.oPag.oATSTATUS_2_11.mHide()
    this.oPgFrm.Page2.oPag.oATTIPRIS_2_20.visible=!this.oPgFrm.Page2.oPag.oATTIPRIS_2_20.mHide()
    this.oPgFrm.Page2.oPag.oATSTAATT_2_22.visible=!this.oPgFrm.Page2.oPag.oATSTAATT_2_22.mHide()
    this.oPgFrm.Page2.oPag.oATFLATRI_2_23.visible=!this.oPgFrm.Page2.oPag.oATFLATRI_2_23.mHide()
    this.oPgFrm.Page3.oPag.oPATIPRIS_3_1.visible=!this.oPgFrm.Page3.oPag.oPATIPRIS_3_1.mHide()
    this.oPgFrm.Page3.oPag.oPATIPRIS_3_2.visible=!this.oPgFrm.Page3.oPag.oPATIPRIS_3_2.mHide()
    this.oPgFrm.Page4.oPag.oATCENCOS_4_2.visible=!this.oPgFrm.Page4.oPag.oATCENCOS_4_2.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_30.visible=!this.oPgFrm.Page2.oPag.oStr_2_30.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_32.visible=!this.oPgFrm.Page2.oPag.oStr_2_32.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_34.visible=!this.oPgFrm.Page2.oPag.oStr_2_34.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_36.visible=!this.oPgFrm.Page2.oPag.oStr_2_36.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_37.visible=!this.oPgFrm.Page2.oPag.oStr_2_37.mHide()
    this.oPgFrm.Page4.oPag.oCCDESPIA_4_22.visible=!this.oPgFrm.Page4.oPag.oCCDESPIA_4_22.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_23.visible=!this.oPgFrm.Page4.oPag.oStr_4_23.mHide()
    this.oPgFrm.Page2.oPag.oATCODLIS_2_46.visible=!this.oPgFrm.Page2.oPag.oATCODLIS_2_46.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page3.oPag.ZOOMPART.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_HFDBZOGJCM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_YELHEOSIYD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_SJMHBUZTAZ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kwa
    If UPPER(cevent) ='BLANK' OR UPPER(cevent)='W_PATIPRIS CHANGED' OR UPPER(cevent)='AGGIORNA'
       Select (This.w_ZOOMPART.ccursor)
       Go top
       * Update (This.w_ZOOMPART.ccursor) set XCHK=1 where  This.w_prcodute=DPCODICE
       Update (This.w_ZOOMPART.ccursor) set XCHK=1 where RIFLATTI='S'
       GO TOP
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ATCAUATT
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_UTEN_IDX,3]
    i_lTable = "CAU_UTEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_UTEN_IDX,2], .t., this.CAU_UTEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_UTEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCAUATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MUM',True,'CAU_UTEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ATCAUATT)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ATCAUATT))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCAUATT)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCAUATT) and !this.bDontReportError
            deferred_cp_zoom('CAU_UTEN','*','UMCODICE',cp_AbsName(oSource.parent,'oATCAUATT_1_2'),i_cWhere,'GSAG_MUM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCAUATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ATCAUATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ATCAUATT)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCAUATT = NVL(_Link_.UMCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_ATCAUATT = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_UTEN_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_UTEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCAUATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LinkCausali
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LinkCausali) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LinkCausali)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CADURORE,CADURMIN,CAORASYS,CADATINI,CACHKOBB,CACHKNOM,CARAGGST,CAFLNOTI,CAFLNSAP,CAFLPREA,CAMINPRE,CAGGPREA,CAFLATRI,CADISPON,CAFLTRIS,CATIPATT,CACAUDOC,CAFLANAL,CATIPPRA,CAMATPRA,CA__ENTE";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_LinkCausali);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_LinkCausali)
            select CACODICE,CADESCRI,CADURORE,CADURMIN,CAORASYS,CADATINI,CACHKOBB,CACHKNOM,CARAGGST,CAFLNOTI,CAFLNSAP,CAFLPREA,CAMINPRE,CAGGPREA,CAFLATRI,CADISPON,CAFLTRIS,CATIPATT,CACAUDOC,CAFLANAL,CATIPPRA,CAMATPRA,CA__ENTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LinkCausali = NVL(_Link_.CACODICE,space(20))
      this.w_ATOGGETT = NVL(_Link_.CADESCRI,space(254))
      this.w_ATDURORE = NVL(_Link_.CADURORE,0)
      this.w_ATDURMIN = NVL(_Link_.CADURMIN,0)
      this.w_CAORASYS = NVL(_Link_.CAORASYS,space(1))
      this.w_CADTIN = NVL(_Link_.CADATINI,ctot(""))
      this.w_CACHKOBB = NVL(_Link_.CACHKOBB,space(1))
      this.w_CACHKNOM = NVL(_Link_.CACHKNOM,space(1))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(10))
      this.w_ATFLNOTI = NVL(_Link_.CAFLNOTI,space(10))
      this.w_FLNSAP = NVL(_Link_.CAFLNSAP,space(1))
      this.w_ATFLPROM = NVL(_Link_.CAFLPREA,space(1))
      this.w_CAMINPRE = NVL(_Link_.CAMINPRE,0)
      this.w_ATGGPREA = NVL(_Link_.CAGGPREA,0)
      this.w_ATFLATRI = NVL(_Link_.CAFLATRI,space(1))
      this.w_ATSTAATT = NVL(_Link_.CADISPON,0)
      this.w_FLTRIS = NVL(_Link_.CAFLTRIS,space(1))
      this.w_ATCODATT = NVL(_Link_.CATIPATT,space(5))
      this.w_CAUDOC = NVL(_Link_.CACAUDOC,space(5))
      this.w_FLANAL = NVL(_Link_.CAFLANAL,space(1))
      this.w_CATIPPRA = NVL(_Link_.CATIPPRA,space(10))
      this.w_CAMATPRA = NVL(_Link_.CAMATPRA,space(10))
      this.w_CA__ENTE = NVL(_Link_.CA__ENTE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_LinkCausali = space(20)
      endif
      this.w_ATOGGETT = space(254)
      this.w_ATDURORE = 0
      this.w_ATDURMIN = 0
      this.w_CAORASYS = space(1)
      this.w_CADTIN = ctot("")
      this.w_CACHKOBB = space(1)
      this.w_CACHKNOM = space(1)
      this.w_CARAGGST = space(10)
      this.w_ATFLNOTI = space(10)
      this.w_FLNSAP = space(1)
      this.w_ATFLPROM = space(1)
      this.w_CAMINPRE = 0
      this.w_ATGGPREA = 0
      this.w_ATFLATRI = space(1)
      this.w_ATSTAATT = 0
      this.w_FLTRIS = space(1)
      this.w_ATCODATT = space(5)
      this.w_CAUDOC = space(5)
      this.w_FLANAL = space(1)
      this.w_CATIPPRA = space(10)
      this.w_CAMATPRA = space(10)
      this.w_CA__ENTE = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LinkCausali Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODPRA
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_bzz',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ATCODPRA))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_ATCODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_ATCODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_ATCODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oATCODPRA_1_19'),i_cWhere,'gsar_bzz',"Pratiche",'GSAG_AAT.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ATCODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ATCODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CNDTOBSO,CN__ENTE,CNLOCALI,CNUFFICI,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(50))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(50))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_AT__ENTE = NVL(_Link_.CN__ENTE,space(10))
      this.w_ATLOCALI = NVL(_Link_.CNLOCALI,space(30))
      this.w_ATUFFICI = NVL(_Link_.CNUFFICI,space(10))
      this.w_CNDATFIN = NVL(cp_ToDate(_Link_.CNDATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODPRA = space(15)
      endif
      this.w_DESPRA = space(100)
      this.w_COMODO = space(50)
      this.w_COMODO = space(50)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_AT__ENTE = space(10)
      this.w_ATLOCALI = space(30)
      this.w_ATUFFICI = space(10)
      this.w_CNDATFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_ATSTATUS='P' AND NOT EMPTY(.w_ATCODPRA)) OR .w_ATSTATUS<>'P') AND (.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATCODPRA = space(15)
        this.w_DESPRA = space(100)
        this.w_COMODO = space(50)
        this.w_COMODO = space(50)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_AT__ENTE = space(10)
        this.w_ATLOCALI = space(30)
        this.w_ATUFFICI = space(10)
        this.w_CNDATFIN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_bRes and i_cCtrl='Drop'
      with this
        if i_bRes and !( ((.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) and (.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))) OR ! Isalt())
          i_bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
          if not(i_bRes)
            this.w_ATCODPRA = space(15)
            this.w_DESPRA = space(100)
            this.w_COMODO = space(50)
            this.w_COMODO = space(50)
            this.w_DATOBSO = ctod("  /  /  ")
            this.w_AT__ENTE = space(10)
            this.w_ATLOCALI = space(30)
            this.w_ATUFFICI = space(10)
            this.w_CNDATFIN = ctod("  /  /  ")
          endif
        endif
      endwith
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODNOM
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_ATCODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NODTOBSO,NOTELEFO,NO_EMAIL,NO_EMPEC,NONUMCEL,NONUMLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_ATCODNOM))
          select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NODTOBSO,NOTELEFO,NO_EMAIL,NO_EMPEC,NONUMCEL,NONUMLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_ATCODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NODTOBSO,NOTELEFO,NO_EMAIL,NO_EMPEC,NONUMCEL,NONUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_ATCODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NODTOBSO,NOTELEFO,NO_EMAIL,NO_EMPEC,NONUMCEL,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_ATCODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NODTOBSO,NOTELEFO,NO_EMAIL,NO_EMPEC,NONUMCEL,NONUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_ATCODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NODTOBSO,NOTELEFO,NO_EMAIL,NO_EMPEC,NONUMCEL,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oATCODNOM_1_20'),i_cWhere,'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NODTOBSO,NOTELEFO,NO_EMAIL,NO_EMPEC,NONUMCEL,NONUMLIS";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NODTOBSO,NOTELEFO,NO_EMAIL,NO_EMPEC,NONUMCEL,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NODTOBSO,NOTELEFO,NO_EMAIL,NO_EMPEC,NONUMCEL,NONUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_ATCODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NODTOBSO,NOTELEFO,NO_EMAIL,NO_EMPEC,NONUMCEL,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(50))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
      this.w_DATOBSONOM = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_OFCODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_ATTELEFO = NVL(_Link_.NOTELEFO,space(18))
      this.w_AT_EMAIL = NVL(_Link_.NO_EMAIL,space(18))
      this.w_AT_EMPEC = NVL(_Link_.NO_EMPEC,space(254))
      this.w_ATCELLUL = NVL(_Link_.NONUMCEL,space(18))
      this.w_NCODLIS = NVL(_Link_.NONUMLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODNOM = space(15)
      endif
      this.w_DESNOM = space(40)
      this.w_COMODO = space(50)
      this.w_TIPNOM = space(1)
      this.w_DATOBSONOM = ctod("  /  /  ")
      this.w_OFCODNOM = space(15)
      this.w_ATTELEFO = space(18)
      this.w_AT_EMAIL = space(18)
      this.w_AT_EMPEC = space(254)
      this.w_ATCELLUL = space(18)
      this.w_NCODLIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATCODNOM = space(15)
        this.w_DESNOM = space(40)
        this.w_COMODO = space(50)
        this.w_TIPNOM = space(1)
        this.w_DATOBSONOM = ctod("  /  /  ")
        this.w_OFCODNOM = space(15)
        this.w_ATTELEFO = space(18)
        this.w_AT_EMAIL = space(18)
        this.w_AT_EMPEC = space(254)
        this.w_ATCELLUL = space(18)
        this.w_NCODLIS = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AT__ENTE
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AT__ENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRA_ENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EPCODICE like "+cp_ToStrODBC(trim(this.w_AT__ENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select EPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EPCODICE',trim(this.w_AT__ENTE))
          select EPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AT__ENTE)==trim(_Link_.EPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_AT__ENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_AT__ENTE)+"%");

            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AT__ENTE) and !this.bDontReportError
            deferred_cp_zoom('PRA_ENTI','*','EPCODICE',cp_AbsName(oSource.parent,'oAT__ENTE_1_22'),i_cWhere,'',"Enti pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',oSource.xKey(1))
            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AT__ENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_AT__ENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_AT__ENTE)
            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AT__ENTE = NVL(_Link_.EPCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_AT__ENTE = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AT__ENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATUFFICI
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_UFFI_IDX,3]
    i_lTable = "PRA_UFFI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2], .t., this.PRA_UFFI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATUFFICI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRA_UFFI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UFCODICE like "+cp_ToStrODBC(trim(this.w_ATUFFICI)+"%");

          i_ret=cp_SQL(i_nConn,"select UFCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UFCODICE',trim(this.w_ATUFFICI))
          select UFCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATUFFICI)==trim(_Link_.UFCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_ATUFFICI)+"%");

            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_ATUFFICI)+"%");

            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATUFFICI) and !this.bDontReportError
            deferred_cp_zoom('PRA_UFFI','*','UFCODICE',cp_AbsName(oSource.parent,'oATUFFICI_1_23'),i_cWhere,'',"Uffici pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',oSource.xKey(1))
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATUFFICI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(this.w_ATUFFICI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',this.w_ATUFFICI)
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATUFFICI = NVL(_Link_.UFCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ATUFFICI = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2])+'\'+cp_ToStr(_Link_.UFCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_UFFI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATUFFICI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LetturaParAgen
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LetturaParAgen) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LetturaParAgen)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACHKFES,PAGESRIS,PAFLVISI,PACHKATT,PACHKCAR,PACODLIS";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LetturaParAgen);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LetturaParAgen)
            select PACODAZI,PACHKFES,PAGESRIS,PAFLVISI,PACHKATT,PACHKCAR,PACODLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LetturaParAgen = NVL(_Link_.PACODAZI,space(10))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
      this.w_GESRIS = NVL(_Link_.PAGESRIS,space(1))
      this.w_PAFLVISI = NVL(_Link_.PAFLVISI,space(1))
      this.w_PACHKATT = NVL(_Link_.PACHKATT,0)
      this.w_PACHKCAR = NVL(_Link_.PACHKCAR,space(1))
      this.w_CODLIS = NVL(_Link_.PACODLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_LetturaParAgen = space(10)
      endif
      this.w_PACHKFES = space(1)
      this.w_GESRIS = space(1)
      this.w_PAFLVISI = space(1)
      this.w_PACHKATT = 0
      this.w_PACHKCAR = space(1)
      this.w_CODLIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LetturaParAgen Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLI
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ATTIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ATTIPCLI;
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_FLSCOR = NVL(_Link_.ANSCORPO,space(1))
      this.w_NUMLIS = NVL(_Link_.ANNUMLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_FLSCOR = space(1)
      this.w_NUMLIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTIPRIS
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_RISE_IDX,3]
    i_lTable = "TIP_RISE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2], .t., this.TIP_RISE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTIPRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_RISE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_ATTIPRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_ATTIPRIS))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTIPRIS)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTIPRIS) and !this.bDontReportError
            deferred_cp_zoom('TIP_RISE','*','TRCODICE',cp_AbsName(oSource.parent,'oATTIPRIS_2_20'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTIPRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_ATTIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_ATTIPRIS)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTIPRIS = NVL(_Link_.TRCODICE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATTIPRIS = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_RISE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTIPRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODATT
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFFTIPAT_IDX,3]
    i_lTable = "OFFTIPAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2], .t., this.OFFTIPAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFFTIPAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODTIP like "+cp_ToStrODBC(trim(this.w_ATCODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODTIP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODTIP',trim(this.w_ATCODATT))
          select TACODTIP,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODTIP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODATT)==trim(_Link_.TACODTIP) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStrODBC(trim(this.w_ATCODATT)+"%");

            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStr(trim(this.w_ATCODATT)+"%");

            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCODATT) and !this.bDontReportError
            deferred_cp_zoom('OFFTIPAT','*','TACODTIP',cp_AbsName(oSource.parent,'oATCODATT_2_24'),i_cWhere,'',"Tipologie attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',oSource.xKey(1))
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(this.w_ATCODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',this.w_ATCODATT)
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODATT = NVL(_Link_.TACODTIP,space(5))
      this.w_DESTIPOL = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODATT = space(5)
      endif
      this.w_DESTIPOL = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])+'\'+cp_ToStr(_Link_.TACODTIP,1)
      cp_ShowWarn(i_cKey,this.OFFTIPAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUDOC
  func Link_4_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUDOC)
            select TDTIPDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUDOC = space(5)
      endif
      this.w_FLINTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCENCOS
  func Link_4_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ATCENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ATCENCOS))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_ATCENCOS)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_ATCENCOS)+"%");

            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oATCENCOS_4_2'),i_cWhere,'',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ATCENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ATCENCOS)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCENCOS = space(15)
      endif
      this.w_CCDESPIA = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT (g_COAN='S' AND (.w_FLDANA='S' OR .w_FLANAL='S')) or  ! Isalt() or .w_FLANAL='N' OR CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
        endif
        this.w_ATCENCOS = space(15)
        this.w_CCDESPIA = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCONTAT
  func Link_4_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCONTAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCCODCON like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

          i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCCODICE,NCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCCODICE',this.w_ATCODNOM;
                     ,'NCCODCON',trim(this.w_ATCONTAT))
          select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCCODICE,NCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCONTAT)==trim(_Link_.NCCODCON) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NCPERSON like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NCPERSON like "+cp_ToStr(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStr(this.w_ATCODNOM);

            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NCTELEFO like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NCTELEFO like "+cp_ToStr(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStr(this.w_ATCODNOM);

            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NCNUMCEL like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NCNUMCEL like "+cp_ToStr(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStr(this.w_ATCODNOM);

            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NC_EMAIL like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NC_EMAIL like "+cp_ToStr(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStr(this.w_ATCODNOM);

            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCONTAT) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(oSource.parent,'oATCONTAT_4_3'),i_cWhere,'',"Persone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODNOM<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                     +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',oSource.xKey(1);
                       ,'NCCODCON',oSource.xKey(2))
            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCONTAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(this.w_ATCONTAT);
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',this.w_ATCODNOM;
                       ,'NCCODCON',this.w_ATCONTAT)
            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCONTAT = NVL(_Link_.NCCODCON,space(5))
      this.w_ATPERSON = NVL(_Link_.NCPERSON,space(60))
      this.w_ATTELEFO = NVL(_Link_.NCTELEFO,space(18))
      this.w_ATCELLUL = NVL(_Link_.NCNUMCEL,space(18))
      this.w_AT_EMAIL = NVL(_Link_.NC_EMAIL,space(18))
      this.w_AT_EMPEC = NVL(_Link_.NC_EMPEC,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_ATCONTAT = space(5)
      endif
      this.w_ATPERSON = space(60)
      this.w_ATTELEFO = space(18)
      this.w_ATCELLUL = space(18)
      this.w_AT_EMAIL = space(18)
      this.w_AT_EMPEC = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCCODICE,1)+'\'+cp_ToStr(_Link_.NCCODCON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCONTAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODLIS
  func Link_2_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ATCODLIS)+"%");
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);

          i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSVALLIS,LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSVALLIS',this.w_ATCODVAL;
                     ,'LSCODLIS',trim(this.w_ATCODLIS))
          select LSVALLIS,LSCODLIS,LSDESLIS,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSVALLIS,LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(oSource.parent,'oATCODLIS_2_46'),i_cWhere,'',"",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODVAL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LSVALLIS,LSCODLIS,LSDESLIS,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, listino non valido")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',oSource.xKey(1);
                       ,'LSCODLIS',oSource.xKey(2))
            select LSVALLIS,LSCODLIS,LSDESLIS,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ATCODLIS);
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',this.w_ATCODVAL;
                       ,'LSCODLIS',this.w_ATCODLIS)
            select LSVALLIS,LSCODLIS,LSDESLIS,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_QUALIS = NVL(_Link_.LSQUANTI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_INILIS = ctod("  /  /  ")
      this.w_FINLIS = ctod("  /  /  ")
      this.w_IVALIS = space(1)
      this.w_QUALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Isahe() or .w_FLNSAP="S" or CHKLISD(.w_ATCODLIS,.w_IVALIS,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_ATCODVAL, .w_DATINI) 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, listino non valido")
        endif
        this.w_ATCODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_VALLIS = space(3)
        this.w_INILIS = ctod("  /  /  ")
        this.w_FINLIS = ctod("  /  /  ")
        this.w_IVALIS = space(1)
        this.w_QUALIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSVALLIS,1)+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATCAUATT_1_2.RadioValue()==this.w_ATCAUATT)
      this.oPgFrm.Page1.oPag.oATCAUATT_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATOGGETT_1_8.value==this.w_ATOGGETT)
      this.oPgFrm.Page1.oPag.oATOGGETT_1_8.value=this.w_ATOGGETT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_9.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_9.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINI_1_10.value==this.w_OREINI)
      this.oPgFrm.Page1.oPag.oOREINI_1_10.value=this.w_OREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_11.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_11.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_12.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_12.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOREFIN_1_13.value==this.w_OREFIN)
      this.oPgFrm.Page1.oPag.oOREFIN_1_13.value=this.w_OREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_14.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_14.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODPRA_1_19.value==this.w_ATCODPRA)
      this.oPgFrm.Page1.oPag.oATCODPRA_1_19.value=this.w_ATCODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODNOM_1_20.value==this.w_ATCODNOM)
      this.oPgFrm.Page1.oPag.oATCODNOM_1_20.value=this.w_ATCODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oATLOCALI_1_21.value==this.w_ATLOCALI)
      this.oPgFrm.Page1.oPag.oATLOCALI_1_21.value=this.w_ATLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oAT__ENTE_1_22.RadioValue()==this.w_AT__ENTE)
      this.oPgFrm.Page1.oPag.oAT__ENTE_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATUFFICI_1_23.RadioValue()==this.w_ATUFFICI)
      this.oPgFrm.Page1.oPag.oATUFFICI_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_25.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_25.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRA_1_36.value==this.w_DESPRA)
      this.oPgFrm.Page1.oPag.oDESPRA_1_36.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCARAGGST_1_46.RadioValue()==this.w_CARAGGST)
      this.oPgFrm.Page1.oPag.oCARAGGST_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATSTATUS_2_10.RadioValue()==this.w_ATSTATUS)
      this.oPgFrm.Page2.oPag.oATSTATUS_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATSTATUS_2_11.RadioValue()==this.w_ATSTATUS)
      this.oPgFrm.Page2.oPag.oATSTATUS_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATPERCOM_2_12.value==this.w_ATPERCOM)
      this.oPgFrm.Page2.oPag.oATPERCOM_2_12.value=this.w_ATPERCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oATFLNOTI_2_13.RadioValue()==this.w_ATFLNOTI)
      this.oPgFrm.Page2.oPag.oATFLNOTI_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATGGPREA_2_14.value==this.w_ATGGPREA)
      this.oPgFrm.Page2.oPag.oATGGPREA_2_14.value=this.w_ATGGPREA
    endif
    if not(this.oPgFrm.Page2.oPag.oATNUMPRI_2_15.RadioValue()==this.w_ATNUMPRI)
      this.oPgFrm.Page2.oPag.oATNUMPRI_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATFLPROM_2_16.RadioValue()==this.w_ATFLPROM)
      this.oPgFrm.Page2.oPag.oATFLPROM_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDATAPRO_2_17.value==this.w_DATAPRO)
      this.oPgFrm.Page2.oPag.oDATAPRO_2_17.value=this.w_DATAPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oORAPRO_2_18.value==this.w_ORAPRO)
      this.oPgFrm.Page2.oPag.oORAPRO_2_18.value=this.w_ORAPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oMINPRO_2_19.value==this.w_MINPRO)
      this.oPgFrm.Page2.oPag.oMINPRO_2_19.value=this.w_MINPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oATTIPRIS_2_20.RadioValue()==this.w_ATTIPRIS)
      this.oPgFrm.Page2.oPag.oATTIPRIS_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATPUBWEB_2_21.RadioValue()==this.w_ATPUBWEB)
      this.oPgFrm.Page2.oPag.oATPUBWEB_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATSTAATT_2_22.RadioValue()==this.w_ATSTAATT)
      this.oPgFrm.Page2.oPag.oATSTAATT_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATFLATRI_2_23.RadioValue()==this.w_ATFLATRI)
      this.oPgFrm.Page2.oPag.oATFLATRI_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATCODATT_2_24.value==this.w_ATCODATT)
      this.oPgFrm.Page2.oPag.oATCODATT_2_24.value=this.w_ATCODATT
    endif
    if not(this.oPgFrm.Page3.oPag.oPATIPRIS_3_1.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page3.oPag.oPATIPRIS_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPATIPRIS_3_2.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page3.oPag.oPATIPRIS_3_2.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oATCENCOS_4_2.value==this.w_ATCENCOS)
      this.oPgFrm.Page4.oPag.oATCENCOS_4_2.value=this.w_ATCENCOS
    endif
    if not(this.oPgFrm.Page4.oPag.oATCONTAT_4_3.value==this.w_ATCONTAT)
      this.oPgFrm.Page4.oPag.oATCONTAT_4_3.value=this.w_ATCONTAT
    endif
    if not(this.oPgFrm.Page4.oPag.oATPERSON_4_4.value==this.w_ATPERSON)
      this.oPgFrm.Page4.oPag.oATPERSON_4_4.value=this.w_ATPERSON
    endif
    if not(this.oPgFrm.Page4.oPag.oATTELEFO_4_5.value==this.w_ATTELEFO)
      this.oPgFrm.Page4.oPag.oATTELEFO_4_5.value=this.w_ATTELEFO
    endif
    if not(this.oPgFrm.Page4.oPag.oATCELLUL_4_6.value==this.w_ATCELLUL)
      this.oPgFrm.Page4.oPag.oATCELLUL_4_6.value=this.w_ATCELLUL
    endif
    if not(this.oPgFrm.Page4.oPag.oAT_EMAIL_4_7.value==this.w_AT_EMAIL)
      this.oPgFrm.Page4.oPag.oAT_EMAIL_4_7.value=this.w_AT_EMAIL
    endif
    if not(this.oPgFrm.Page4.oPag.oAT_EMPEC_4_8.value==this.w_AT_EMPEC)
      this.oPgFrm.Page4.oPag.oAT_EMPEC_4_8.value=this.w_AT_EMPEC
    endif
    if not(this.oPgFrm.Page4.oPag.oATNOTPIA_4_9.value==this.w_ATNOTPIA)
      this.oPgFrm.Page4.oPag.oATNOTPIA_4_9.value=this.w_ATNOTPIA
    endif
    if not(this.oPgFrm.Page2.oPag.oGIOPRM_2_31.value==this.w_GIOPRM)
      this.oPgFrm.Page2.oPag.oGIOPRM_2_31.value=this.w_GIOPRM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESTIPOL_2_38.value==this.w_DESTIPOL)
      this.oPgFrm.Page2.oPag.oDESTIPOL_2_38.value=this.w_DESTIPOL
    endif
    if not(this.oPgFrm.Page4.oPag.oDESNOM_4_15.value==this.w_DESNOM)
      this.oPgFrm.Page4.oPag.oDESNOM_4_15.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page4.oPag.oOFCODNOM_4_21.value==this.w_OFCODNOM)
      this.oPgFrm.Page4.oPag.oOFCODNOM_4_21.value=this.w_OFCODNOM
    endif
    if not(this.oPgFrm.Page4.oPag.oCCDESPIA_4_22.value==this.w_CCDESPIA)
      this.oPgFrm.Page4.oPag.oCCDESPIA_4_22.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOINI_1_66.value==this.w_GIOINI)
      this.oPgFrm.Page1.oPag.oGIOINI_1_66.value=this.w_GIOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOFIN_1_67.value==this.w_GIOFIN)
      this.oPgFrm.Page1.oPag.oGIOFIN_1_67.value=this.w_GIOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oATCODLIS_2_46.value==this.w_ATCODLIS)
      this.oPgFrm.Page2.oPag.oATCODLIS_2_46.value=this.w_ATCODLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLIS_2_47.value==this.w_DESLIS)
      this.oPgFrm.Page2.oPag.oDESLIS_2_47.value=this.w_DESLIS
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ATOGGETT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATOGGETT_1_8.SetFocus()
            i_bnoObbl = !empty(.w_ATOGGETT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   ((empty(.w_OREINI)) or not(VAL(.w_OREINI) < 24))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREINI_1_10.SetFocus()
            i_bnoObbl = !empty(.w_OREINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   ((empty(.w_MININI)) or not(VAL(.w_MININI) < 60))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_11.SetFocus()
            i_bnoObbl = !empty(.w_MININI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   (empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   ((empty(.w_OREFIN)) or not(VAL(.w_OREFIN) < 24))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREFIN_1_13.SetFocus()
            i_bnoObbl = !empty(.w_OREFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   ((empty(.w_MINFIN)) or not(VAL(.w_MINFIN) < 60))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_14.SetFocus()
            i_bnoObbl = !empty(.w_MINFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(((.w_ATSTATUS='P' AND NOT EMPTY(.w_ATCODPRA)) OR .w_ATSTATUS<>'P') AND (.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)))  and (NOT EMPTY(.w_CACHKOBB))  and not(empty(.w_ATCODPRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODPRA_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and (!EMPTY(.w_CACHKNOM))  and not(empty(.w_ATCODNOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODNOM_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ATPERCOM <= 100 AND .w_ATPERCOM >= 0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATPERCOM_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore non inferiore a 0 e non superiore a 100")
          case   (empty(.w_DATAPRO))  and (.w_ATFLPROM='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATAPRO_2_17.SetFocus()
            i_bnoObbl = !empty(.w_DATAPRO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ORAPRO)) or not(VAL(.w_ORAPRO) < 24 AND IIF(!EMPTY(.w_DATAPRO) AND !EMPTY(.w_ORAPRO) AND !EMPTY(.w_MINPRO),cp_CharToDateTime( ALLTRIM(STR(DAY(.w_DATAPRO)))+'-'+ALLTRIM(STR(MONTH(.w_DATAPRO)))+'-'+ALLTR(STR(YEAR(.w_DATAPRO)))+' '+iif(empty(.w_ORAPRO),'00', .w_ORAPRO)+':'+iif(empty(.w_MINPRO),'00', .w_MINPRO), 'dd-mm-yyyy hh:nn:ss')<=cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '  -  -       :  :  ')),.T.)))  and (.w_ATFLPROM='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oORAPRO_2_18.SetFocus()
            i_bnoObbl = !empty(.w_ORAPRO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23 ed inferiore all'ora di inizio attivit�")
          case   ((empty(.w_MINPRO)) or not(VAL(.w_MINPRO) < 60 AND IIF(!EMPTY(.w_DATAPRO) AND !EMPTY(.w_ORAPRO) AND !EMPTY(.w_MINPRO),cp_CharToDateTime( ALLTRIM(STR(DAY(.w_DATAPRO)))+'-'+ALLTRIM(STR(MONTH(.w_DATAPRO)))+'-'+ALLTR(STR(YEAR(.w_DATAPRO)))+' '+.w_ORAPRO+':'+.w_MINPRO, 'dd-mm-yyyy hh:nn:ss')<=cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '  -  -       :  :  ')),.T.)))  and (.w_ATFLPROM='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMINPRO_2_19.SetFocus()
            i_bnoObbl = !empty(.w_MINPRO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59 ed inferiore all'ora di inizio attivit�")
          case   not(NOT (g_COAN='S' AND (.w_FLDANA='S' OR .w_FLANAL='S')) or  ! Isalt() or .w_FLANAL='N' OR CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.))  and not(NOT (g_COAN='S' AND (.w_FLDANA='S' OR .w_FLANAL='S')) or  ! Isalt() or .w_FLANAL='N')  and not(empty(.w_ATCENCOS))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oATCENCOS_4_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
          case   not(Isahe() or .w_FLNSAP="S" or CHKLISD(.w_ATCODLIS,.w_IVALIS,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_ATCODVAL, .w_DATINI) )  and not(Isahe())  and not(empty(.w_ATCODLIS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCODLIS_2_46.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, listino non valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      this.Calculate_JRJHOSKADX()
      this.Calculate_YGAAQKHNUA()
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PRCODUTE = this.w_PRCODUTE
    this.o_ATCAUATT = this.w_ATCAUATT
    this.o_DATINI = this.w_DATINI
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_DATFIN = this.w_DATFIN
    this.o_ATCODPRA = this.w_ATCODPRA
    this.o_ATCODNOM = this.w_ATCODNOM
    this.o_ATSTATUS = this.w_ATSTATUS
    this.o_ATPERCOM = this.w_ATPERCOM
    this.o_ATFLPROM = this.w_ATFLPROM
    this.o_DATAPRO = this.w_DATAPRO
    this.o_ORAPRO = this.w_ORAPRO
    this.o_MINPRO = this.w_MINPRO
    this.o_ATFLATRI = this.w_ATFLATRI
    return

enddefine

* --- Define pages as container
define class tgsag_kwaPag1 as StdContainer
  Width  = 618
  height = 328
  stdWidth  = 618
  stdheight = 328
  resizeXpos=373
  resizeYpos=263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oATCAUATT_1_2 as StdTableCombo with uid="MBGICDMGLD",rtseq=2,rtrep=.f.,left=102,top=16,width=203,height=21;
    , ToolTipText = "Tipo attivit�";
    , HelpContextID = 78340442;
    , cFormVar="w_ATCAUATT",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="CAU_UTEN";
    , cTable='QUERY\CAU_UTEN.vqr',cKey='UMCODICE',cValue='UMCODICE',cOrderBy='',xDefault=space(20);
  , bGlobalFont=.t.


  func oATCAUATT_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCAUATT_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oATOGGETT_1_8 as StdField with uid="AVSKMMIFVK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ATOGGETT", cQueryName = "ATOGGETT",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 131211610,;
   bGlobalFont=.t.,;
    Height=21, Width=503, Left=102, Top=46, InputMask=replicate('X',254)

  add object oDATINI_1_9 as StdField with uid="VAJTUNAFFS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 62628298,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=151, Top=76

  func oDATINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_PACHKFES='N' OR Chkfestivi(.w_DATINI,.w_PACHKFES))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oOREINI_1_10 as StdField with uid="WOKDXXPUIP",rtseq=10,rtrep=.f.,;
    cFormVar = "w_OREINI", cQueryName = "OREINI",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di inizio selezione",;
    HelpContextID = 62685210,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=306, Top=76, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_11 as StdField with uid="QMRKNBACQP",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio selezione",;
    HelpContextID = 62650682,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=343, Top=76, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_12 as StdField with uid="VRHGJGBHQK",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 252617162,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=151, Top=106

  add object oOREFIN_1_13 as StdField with uid="VKJCJSQZFK",rtseq=13,rtrep=.f.,;
    cFormVar = "w_OREFIN", cQueryName = "OREFIN",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di fine selezione",;
    HelpContextID = 252674074,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=306, Top=106, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_14 as StdField with uid="GIWPXXGMLC",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di fine selezione",;
    HelpContextID = 252639546,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=343, Top=106, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc

  add object oATCODPRA_1_19 as StdField with uid="LNZCMMZIKS",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ATCODPRA", cQueryName = "ATCODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice pratica",;
    HelpContextID = 44654919,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=102, Top=136, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="gsar_bzz", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ATCODPRA"

  func oATCODPRA_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CACHKOBB))
    endwith
   endif
  endfunc

  func oATCODPRA_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
      if bRes and !( ((.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) and (.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))) OR ! Isalt())
         bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oATCODPRA_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODPRA_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oATCODPRA_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_bzz',"Pratiche",'GSAG_AAT.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oATCODPRA_1_19.mZoomOnZoom
    local i_obj
    i_obj=gsar_bzz()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_ATCODPRA
     i_obj.ecpSave()
  endproc

  add object oATCODNOM_1_20 as StdField with uid="LDORSNJAGM",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ATCODNOM", cQueryName = "ATCODNOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo o soggetto esterno della pratica",;
    HelpContextID = 257334957,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=102, Top=166, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_ATCODNOM"

  func oATCODNOM_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CACHKNOM))
    endwith
   endif
  endfunc

  func oATCODNOM_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
      if .not. empty(.w_ATCONTAT)
        bRes2=.link_4_3('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oATCODNOM_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODNOM_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oATCODNOM_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oATCODNOM_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_ATCODNOM
     i_obj.ecpSave()
  endproc

  add object oATLOCALI_1_21 as StdField with uid="FJMOJMLCTU",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ATLOCALI", cQueryName = "ATLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� in cui si svolge l'attivit�",;
    HelpContextID = 208015025,;
   bGlobalFont=.t.,;
    Height=21, Width=503, Left=102, Top=196, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oATLOCALI_1_21.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_ATLOCALI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oAT__ENTE_1_22 as StdTableCombo with uid="NPTCPWUNMC",rtseq=20,rtrep=.f.,left=102,top=226,width=203,height=21;
    , ToolTipText = "Codice ente/ufficio giudiziario";
    , HelpContextID = 13312331;
    , cFormVar="w_AT__ENTE",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="PRA_ENTI";
    , cTable='QUERY\PRAENTIZOOM.VQR',cKey='EPCODICE',cValue='EPDESCRI',cOrderBy='',xDefault=space(10);
  , bGlobalFont=.t.


  func oAT__ENTE_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oAT__ENTE_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oATUFFICI_1_23 as StdTableCombo with uid="NTLGSXCCSF",rtseq=21,rtrep=.f.,left=402,top=226,width=203,height=21;
    , ToolTipText = "Codice ufficio/sezione del foro";
    , HelpContextID = 197230927;
    , cFormVar="w_ATUFFICI",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="PRA_UFFI";
    , cTable='PRA_UFFI',cKey='UFCODICE',cValue='UFDESCRI',cOrderBy='',xDefault=space(10);
  , bGlobalFont=.t.


  func oATUFFICI_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oATUFFICI_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oDESNOM_1_25 as StdField with uid="YBKTMCLSXL",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 262581706,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=242, Top=166, InputMask=replicate('X',100)

  add object oDESPRA_1_36 as StdField with uid="DHZPCSQKFH",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 192196042,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=242, Top=136, InputMask=replicate('X',100)

  func oDESPRA_1_36.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oCARAGGST_1_46 as StdCombo with uid="VAAWEXCMBR",rtseq=35,rtrep=.f.,left=402,top=16,width=203,height=21, enabled=.f.;
    , HelpContextID = 164380282;
    , cFormVar="w_CARAGGST",RowSource=""+"Generico,"+"Udienze,"+"Appuntamento,"+"Cose da fare,"+"Note,"+"Sessione telefonica,"+"Assenze,"+"Da inserimento prestazioni", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCARAGGST_1_46.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'U',;
    iif(this.value =3,'S',;
    iif(this.value =4,'D',;
    iif(this.value =5,'M',;
    iif(this.value =6,'T',;
    iif(this.value =7,'A',;
    iif(this.value =8,'Z',;
    space(10))))))))))
  endfunc
  func oCARAGGST_1_46.GetRadio()
    this.Parent.oContained.w_CARAGGST = this.RadioValue()
    return .t.
  endfunc

  func oCARAGGST_1_46.SetRadio()
    this.Parent.oContained.w_CARAGGST=trim(this.Parent.oContained.w_CARAGGST)
    this.value = ;
      iif(this.Parent.oContained.w_CARAGGST=='G',1,;
      iif(this.Parent.oContained.w_CARAGGST=='U',2,;
      iif(this.Parent.oContained.w_CARAGGST=='S',3,;
      iif(this.Parent.oContained.w_CARAGGST=='D',4,;
      iif(this.Parent.oContained.w_CARAGGST=='M',5,;
      iif(this.Parent.oContained.w_CARAGGST=='T',6,;
      iif(this.Parent.oContained.w_CARAGGST=='A',7,;
      iif(this.Parent.oContained.w_CARAGGST=='Z',8,;
      0))))))))
  endfunc


  add object oBtn_1_54 as StdButton with uid="IBRDXZOPMJ",left=457, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=1;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 20290810;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_54.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_54.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_1_55 as StdButton with uid="NLRIOAGGKF",left=510, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Procede con l'inserimento dell'attivit�";
    , HelpContextID = 32102170;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_55.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"INS_ATT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_57 as StdButton with uid="ABKYYMEJLO",left=563, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 22162182;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_57.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oGIOINI_1_66 as StdField with uid="FDLKJPVEHW",rtseq=85,rtrep=.f.,;
    cFormVar = "w_GIOINI", cQueryName = "GIOINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 62646682,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=102, Top=76, InputMask=replicate('X',10)

  add object oGIOFIN_1_67 as StdField with uid="GWEHUCPCHB",rtseq=86,rtrep=.f.,;
    cFormVar = "w_GIOFIN", cQueryName = "GIOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 252635546,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=102, Top=106, InputMask=replicate('X',10)


  add object oBtn_1_78 as StdButton with uid="QSXKJQMRCW",left=404, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=1;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 148212725;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_78.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_78.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.F.)
      endwith
    endif
  endfunc

  add object oStr_1_18 as StdString with uid="GXOWXVAARV",Visible=.t., Left=13, Top=46,;
    Alignment=1, Width=83, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="RCYDFBJYOO",Visible=.t., Left=13, Top=166,;
    Alignment=1, Width=83, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="QVTFQRDCTZ",Visible=.t., Left=13, Top=16,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="KLEZMLKSOB",Visible=.t., Left=13, Top=76,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="JAHWAWKLCQ",Visible=.t., Left=13, Top=106,;
    Alignment=1, Width=83, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="EWZSPAOTVW",Visible=.t., Left=242, Top=76,;
    Alignment=1, Width=59, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="CTAHWBKCXZ",Visible=.t., Left=242, Top=106,;
    Alignment=1, Width=59, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="DAAABZBARA",Visible=.t., Left=338, Top=76,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="BTYOAEACQT",Visible=.t., Left=338, Top=106,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="HZALIZNENK",Visible=.t., Left=13, Top=136,;
    Alignment=1, Width=83, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="BUUPJTSMEG",Visible=.t., Left=312, Top=16,;
    Alignment=1, Width=83, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="EOUDIELXMA",Visible=.t., Left=13, Top=196,;
    Alignment=1, Width=83, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="RXZXEPKUFL",Visible=.t., Left=13, Top=226,;
    Alignment=1, Width=83, Height=18,;
    Caption="Ente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="JHQQNRHBPP",Visible=.t., Left=312, Top=226,;
    Alignment=1, Width=83, Height=18,;
    Caption="Ufficio:"  ;
  , bGlobalFont=.t.

  add object oBox_1_15 as StdBox with uid="URYVCGNFAN",left=393, top=271, width=223,height=2
enddefine
define class tgsag_kwaPag2 as StdContainer
  Width  = 618
  height = 328
  stdWidth  = 618
  stdheight = 328
  resizeXpos=240
  resizeYpos=256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oATSTATUS_2_10 as StdCombo with uid="ZGNLAMKYJB",rtseq=46,rtrep=.f.,left=102,top=16,width=167,height=21;
    , ToolTipText = "Stato da assegnare all'attivit�";
    , HelpContextID = 109011289;
    , cFormVar="w_ATSTATUS",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa,"+"Completata", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATSTATUS_2_10.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oATSTATUS_2_10.GetRadio()
    this.Parent.oContained.w_ATSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oATSTATUS_2_10.SetRadio()
    this.Parent.oContained.w_ATSTATUS=trim(this.Parent.oContained.w_ATSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_ATSTATUS=='T',1,;
      iif(this.Parent.oContained.w_ATSTATUS=='D',2,;
      iif(this.Parent.oContained.w_ATSTATUS=='I',3,;
      iif(this.Parent.oContained.w_ATSTATUS=='F',4,;
      iif(this.Parent.oContained.w_ATSTATUS=='P',5,;
      0)))))
  endfunc

  func oATSTATUS_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP<>'S')
    endwith
   endif
  endfunc

  func oATSTATUS_2_10.mHide()
    with this.Parent.oContained
      return (.w_FLNSAP='S')
    endwith
  endfunc


  add object oATSTATUS_2_11 as StdCombo with uid="HPCXRCGRJY",rtseq=47,rtrep=.f.,left=102,top=16,width=167,height=21;
    , ToolTipText = "Stato da assegnare all'attivit�";
    , HelpContextID = 109011289;
    , cFormVar="w_ATSTATUS",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATSTATUS_2_11.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    space(1))))))
  endfunc
  func oATSTATUS_2_11.GetRadio()
    this.Parent.oContained.w_ATSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oATSTATUS_2_11.SetRadio()
    this.Parent.oContained.w_ATSTATUS=trim(this.Parent.oContained.w_ATSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_ATSTATUS=='T',1,;
      iif(this.Parent.oContained.w_ATSTATUS=='D',2,;
      iif(this.Parent.oContained.w_ATSTATUS=='I',3,;
      iif(this.Parent.oContained.w_ATSTATUS=='F',4,;
      0))))
  endfunc

  func oATSTATUS_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP='S')
    endwith
   endif
  endfunc

  func oATSTATUS_2_11.mHide()
    with this.Parent.oContained
      return (.w_FLNSAP<>'S')
    endwith
  endfunc

  add object oATPERCOM_2_12 as StdField with uid="HURQDLYWOD",rtseq=48,rtrep=.f.,;
    cFormVar = "w_ATPERCOM", cQueryName = "ATPERCOM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore non inferiore a 0 e non superiore a 100",;
    ToolTipText = "% di completamento dell'attivit�",;
    HelpContextID = 159370925,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=395, Top=16, cSayPict='"999"', cGetPict='"999"'

  func oATPERCOM_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ATPERCOM <= 100 AND .w_ATPERCOM >= 0)
    endwith
    return bRes
  endfunc


  add object oATFLNOTI_2_13 as StdCombo with uid="KQNBGIRQMN",rtseq=49,rtrep=.f.,left=102,top=46,width=167,height=21;
    , ToolTipText = "Permette di inviare una e-mail o un post-in ai partecipanti dell'attivit� in creazione o in cancellazione dell'attivit�, in modifica della data oppure in modifica, aggiunta o cancellazione di partecipanti";
    , HelpContextID = 38179151;
    , cFormVar="w_ATFLNOTI",RowSource=""+"Nessun avviso,"+"Avviso,"+"Avviso con VCS,"+"Avviso con ICS,"+"Avviso con VCS e ICS", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATFLNOTI_2_13.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    iif(this.value =4,'I',;
    iif(this.value =5,'E',;
    space(10)))))))
  endfunc
  func oATFLNOTI_2_13.GetRadio()
    this.Parent.oContained.w_ATFLNOTI = this.RadioValue()
    return .t.
  endfunc

  func oATFLNOTI_2_13.SetRadio()
    this.Parent.oContained.w_ATFLNOTI=trim(this.Parent.oContained.w_ATFLNOTI)
    this.value = ;
      iif(this.Parent.oContained.w_ATFLNOTI=='N',1,;
      iif(this.Parent.oContained.w_ATFLNOTI=='S',2,;
      iif(this.Parent.oContained.w_ATFLNOTI=='C',3,;
      iif(this.Parent.oContained.w_ATFLNOTI=='I',4,;
      iif(this.Parent.oContained.w_ATFLNOTI=='E',5,;
      0)))))
  endfunc

  add object oATGGPREA_2_14 as StdField with uid="DGVHHQQIKA",rtseq=50,rtrep=.f.,;
    cFormVar = "w_ATGGPREA", cQueryName = "ATGGPREA",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni di preavviso prima della scadenza",;
    HelpContextID = 90284359,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=395, Top=46, cSayPict='"999"', cGetPict='"999"'


  add object oATNUMPRI_2_15 as StdCombo with uid="QYZAIWBRWN",rtseq=51,rtrep=.f.,left=102,top=76,width=167,height=21;
    , ToolTipText = "Priorit� da assegnare all'attivit� (normale, urgente, scadenza termine=da fare in un determinato giorno ad un'ora prestabilita)";
    , HelpContextID = 54530383;
    , cFormVar="w_ATNUMPRI",RowSource=""+"Normale,"+"Urgente,"+"Scadenza termine", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATNUMPRI_2_15.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,3,;
    iif(this.value =3,4,;
    0))))
  endfunc
  func oATNUMPRI_2_15.GetRadio()
    this.Parent.oContained.w_ATNUMPRI = this.RadioValue()
    return .t.
  endfunc

  func oATNUMPRI_2_15.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ATNUMPRI==1,1,;
      iif(this.Parent.oContained.w_ATNUMPRI==3,2,;
      iif(this.Parent.oContained.w_ATNUMPRI==4,3,;
      0)))
  endfunc

  add object oATFLPROM_2_16 as StdCheck with uid="WPVNSKJCCY",rtseq=52,rtrep=.f.,left=102, top=106, caption="Promemoria",;
    ToolTipText = "Se attivo, abilita la gestione dei promemoria",;
    HelpContextID = 177827501,;
    cFormVar="w_ATFLPROM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATFLPROM_2_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oATFLPROM_2_16.GetRadio()
    this.Parent.oContained.w_ATFLPROM = this.RadioValue()
    return .t.
  endfunc

  func oATFLPROM_2_16.SetRadio()
    this.Parent.oContained.w_ATFLPROM=trim(this.Parent.oContained.w_ATFLPROM)
    this.value = ;
      iif(this.Parent.oContained.w_ATFLPROM=='S',1,;
      0)
  endfunc

  add object oDATAPRO_2_17 as StdField with uid="XQMEDUWBMS",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DATAPRO", cQueryName = "DATAPRO",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data promemoria",;
    HelpContextID = 178495946,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=395, Top=106

  func oDATAPRO_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATFLPROM='S')
    endwith
   endif
  endfunc

  func oDATAPRO_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_DATAPRO<=.w_DATINI AND IIF(!EMPTY(.w_DATAPRO) AND !EMPTY(.w_ORAPRO) AND !EMPTY(.w_MINPRO),cp_CharToDateTime( ALLTRIM(STR(DAY(.w_DATAPRO)))+'-'+ALLTRIM(STR(MONTH(.w_DATAPRO)))+'-'+ALLTR(STR(YEAR(.w_DATAPRO)))+' '+.w_ORAPRO+':'+.w_MINPRO, 'dd-mm-yyyy hh:nn:ss')<=cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '  -  -       :  :  ')),.T.) AND (.w_PACHKFES='N' OR Chkfestivi(.w_DATAPRO,.w_PACHKFES)))
         bRes=(cp_WarningMsg(thisform.msgFmt("Impostare un valore inferiore alla data di inizio attivit�")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oORAPRO_2_18 as StdField with uid="TWVCQEVEXZ",rtseq=54,rtrep=.f.,;
    cFormVar = "w_ORAPRO", cQueryName = "ORAPRO",nZero=2,;
    bObbl = .t. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23 ed inferiore all'ora di inizio attivit�",;
    ToolTipText = "Ora promemoria",;
    HelpContextID = 225820698,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=527, Top=106, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAPRO_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATFLPROM='S')
    endwith
   endif
  endfunc

  func oORAPRO_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAPRO) < 24 AND IIF(!EMPTY(.w_DATAPRO) AND !EMPTY(.w_ORAPRO) AND !EMPTY(.w_MINPRO),cp_CharToDateTime( ALLTRIM(STR(DAY(.w_DATAPRO)))+'-'+ALLTRIM(STR(MONTH(.w_DATAPRO)))+'-'+ALLTR(STR(YEAR(.w_DATAPRO)))+' '+iif(empty(.w_ORAPRO),'00', .w_ORAPRO)+':'+iif(empty(.w_MINPRO),'00', .w_MINPRO), 'dd-mm-yyyy hh:nn:ss')<=cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '  -  -       :  :  ')),.T.))
    endwith
    return bRes
  endfunc

  add object oMINPRO_2_19 as StdField with uid="VEYPJJGXRE",rtseq=55,rtrep=.f.,;
    cFormVar = "w_MINPRO", cQueryName = "MINPRO",nZero=2,;
    bObbl = .t. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59 ed inferiore all'ora di inizio attivit�",;
    ToolTipText = "Minuti promemoria",;
    HelpContextID = 225769786,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=563, Top=106, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINPRO_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATFLPROM='S')
    endwith
   endif
  endfunc

  func oMINPRO_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINPRO) < 60 AND IIF(!EMPTY(.w_DATAPRO) AND !EMPTY(.w_ORAPRO) AND !EMPTY(.w_MINPRO),cp_CharToDateTime( ALLTRIM(STR(DAY(.w_DATAPRO)))+'-'+ALLTRIM(STR(MONTH(.w_DATAPRO)))+'-'+ALLTR(STR(YEAR(.w_DATAPRO)))+' '+.w_ORAPRO+':'+.w_MINPRO, 'dd-mm-yyyy hh:nn:ss')<=cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', '  -  -       :  :  ')),.T.))
    endwith
    return bRes
  endfunc


  add object oATTIPRIS_2_20 as StdTableCombo with uid="CJLYCLUGLE",rtseq=56,rtrep=.f.,left=102,top=136,width=167,height=21;
    , ToolTipText = "Tipo riserva";
    , HelpContextID = 90468697;
    , cFormVar="w_ATTIPRIS",tablefilter="", bObbl = .f. , nPag = 2;
    , cLinkFile="TIP_RISE";
    , cTable='TIP_RISE',cKey='TRCODICE',cValue='TRDESTIP',cOrderBy='TRDESTIP',xDefault=space(1);
  , bGlobalFont=.t.


  func oATTIPRIS_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATSTATUS $ 'FP')
    endwith
   endif
  endfunc

  func oATTIPRIS_2_20.mHide()
    with this.Parent.oContained
      return (NOT(.w_ATSTATUS $ 'FP') OR .w_FLTRIS#'S')
    endwith
  endfunc

  func oATTIPRIS_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTIPRIS_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oATPUBWEB_2_21 as StdCheck with uid="YSOHUDYKNM",rtseq=57,rtrep=.f.,left=395, top=136, caption="Pubblica su Web",;
    ToolTipText = "Se attivo, l'attivit� sar� pubblicata su Web",;
    HelpContextID = 160444744,;
    cFormVar="w_ATPUBWEB", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATPUBWEB_2_21.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oATPUBWEB_2_21.GetRadio()
    this.Parent.oContained.w_ATPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oATPUBWEB_2_21.SetRadio()
    this.Parent.oContained.w_ATPUBWEB=trim(this.Parent.oContained.w_ATPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_ATPUBWEB=='S',1,;
      0)
  endfunc


  add object oATSTAATT_2_22 as StdCombo with uid="UAVVODWLOJ",rtseq=58,rtrep=.f.,left=102,top=166,width=167,height=21;
    , ToolTipText = "Disponibilit�";
    , HelpContextID = 58679642;
    , cFormVar="w_ATSTAATT",RowSource=""+"Occupato,"+"Fuori sede,"+"Per urgenze,"+"Libero", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATSTAATT_2_22.RadioValue()
    return(iif(this.value =1,2,;
    iif(this.value =2,4,;
    iif(this.value =3,3,;
    iif(this.value =4,1,;
    0)))))
  endfunc
  func oATSTAATT_2_22.GetRadio()
    this.Parent.oContained.w_ATSTAATT = this.RadioValue()
    return .t.
  endfunc

  func oATSTAATT_2_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ATSTAATT==2,1,;
      iif(this.Parent.oContained.w_ATSTAATT==4,2,;
      iif(this.Parent.oContained.w_ATSTAATT==3,3,;
      iif(this.Parent.oContained.w_ATSTAATT==1,4,;
      0))))
  endfunc

  func oATSTAATT_2_22.mHide()
    with this.Parent.oContained
      return (.w_CARAGGST $ 'MD')
    endwith
  endfunc


  add object oATFLATRI_2_23 as StdCombo with uid="HJKGMCBXML",rtseq=59,rtrep=.f.,left=395,top=166,width=167,height=21;
    , ToolTipText = "Attributo che determina la visibilit� dell'attivit�";
    , HelpContextID = 108433743;
    , cFormVar="w_ATFLATRI",RowSource=""+"Riservata,"+"Libera,"+"Nascosta", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATFLATRI_2_23.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'H',;
    space(1)))))
  endfunc
  func oATFLATRI_2_23.GetRadio()
    this.Parent.oContained.w_ATFLATRI = this.RadioValue()
    return .t.
  endfunc

  func oATFLATRI_2_23.SetRadio()
    this.Parent.oContained.w_ATFLATRI=trim(this.Parent.oContained.w_ATFLATRI)
    this.value = ;
      iif(this.Parent.oContained.w_ATFLATRI=='S',1,;
      iif(this.Parent.oContained.w_ATFLATRI=='N',2,;
      iif(this.Parent.oContained.w_ATFLATRI=='H',3,;
      0)))
  endfunc

  func oATFLATRI_2_23.mHide()
    with this.Parent.oContained
      return (.w_GESRIS<>'S')
    endwith
  endfunc

  add object oATCODATT_2_24 as StdField with uid="XZOPKEYJRO",rtseq=60,rtrep=.f.,;
    cFormVar = "w_ATCODATT", cQueryName = "ATCODATT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipologia attivit�",;
    HelpContextID = 61432154,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=102, Top=196, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="OFFTIPAT", oKey_1_1="TACODTIP", oKey_1_2="this.w_ATCODATT"

  func oATCODATT_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODATT_2_24.ecpDrop(oSource)
    this.Parent.oContained.link_2_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODATT_2_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFFTIPAT','*','TACODTIP',cp_AbsName(this.parent,'oATCODATT_2_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipologie attivit�",'',this.parent.oContained
  endproc


  add object oBtn_2_25 as StdButton with uid="WOPIEMBYYG",left=404, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=2;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 148212725;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_25.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_2_26 as StdButton with uid="SKNGBPXLWG",left=457, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=2;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 20290810;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_26.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_2_27 as StdButton with uid="OGFSPOSQAD",left=510, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Procede con l'inserimento dell'attivit�";
    , HelpContextID = 32102170;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_27.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"INS_ATT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_28 as StdButton with uid="MYAGWUJBZG",left=563, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 22162182;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_28.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oGIOPRM_2_31 as StdField with uid="JPMOSKQXFU",rtseq=77,rtrep=.f.,;
    cFormVar = "w_GIOPRM", cQueryName = "GIOPRM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 259320218,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=352, Top=106, InputMask=replicate('X',10)

  add object oDESTIPOL_2_38 as StdField with uid="JMNGCUYYLA",rtseq=78,rtrep=.f.,;
    cFormVar = "w_DESTIPOL", cQueryName = "DESTIPOL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 218148222,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=178, Top=196, InputMask=replicate('X',50)

  add object oATCODLIS_2_46 as StdField with uid="TQHTXHMUFX",rtseq=98,rtrep=.f.,;
    cFormVar = "w_ATCODLIS", cQueryName = "ATCODLIS",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, listino non valido",;
    ToolTipText = "Codice listino",;
    HelpContextID = 245981529,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=102, Top=231, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSVALLIS", oKey_1_2="this.w_ATCODVAL", oKey_2_1="LSCODLIS", oKey_2_2="this.w_ATCODLIS"

  func oATCODLIS_2_46.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oATCODLIS_2_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_46('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODLIS_2_46.ecpDrop(oSource)
    this.Parent.oContained.link_2_46('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODLIS_2_46.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LISTINI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStrODBC(this.Parent.oContained.w_ATCODVAL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStr(this.Parent.oContained.w_ATCODVAL)
    endif
    do cp_zoom with 'LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(this.parent,'oATCODLIS_2_46'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc

  add object oDESLIS_2_47 as StdField with uid="OFWPOIEGTI",rtseq=99,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 168340938,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=178, Top=231, InputMask=replicate('X',40)

  add object oStr_2_30 as StdString with uid="QWTHOZTTVE",Visible=.t., Left=558, Top=106,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_2_30.mHide()
    with this.Parent.oContained
      return (g_JBSH<>'S')
    endwith
  endfunc

  add object oStr_2_32 as StdString with uid="TLJFLYESCF",Visible=.t., Left=477, Top=106,;
    Alignment=1, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  func oStr_2_32.mHide()
    with this.Parent.oContained
      return (g_JBSH<>'S')
    endwith
  endfunc

  add object oStr_2_33 as StdString with uid="GGDQWERLCV",Visible=.t., Left=264, Top=46,;
    Alignment=1, Width=125, Height=18,;
    Caption="Giorni di preavviso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="EHWQNJRIKW",Visible=.t., Left=306, Top=166,;
    Alignment=1, Width=83, Height=18,;
    Caption="Visibilit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_34.mHide()
    with this.Parent.oContained
      return (.w_GESRIS<>'S')
    endwith
  endfunc

  add object oStr_2_35 as StdString with uid="QAOAEQTAVO",Visible=.t., Left=13, Top=76,;
    Alignment=1, Width=83, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="URBDWJJSKY",Visible=.t., Left=13, Top=166,;
    Alignment=1, Width=83, Height=18,;
    Caption="Disponibilit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_36.mHide()
    with this.Parent.oContained
      return (.w_CARAGGST $ 'MD')
    endwith
  endfunc

  add object oStr_2_37 as StdString with uid="CLUNKBOWSL",Visible=.t., Left=13, Top=136,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipo riserva:"  ;
  , bGlobalFont=.t.

  func oStr_2_37.mHide()
    with this.Parent.oContained
      return (NOT(.w_ATSTATUS $ 'FP') OR .w_FLTRIS#'S')
    endwith
  endfunc

  add object oStr_2_39 as StdString with uid="NCKMIPTTCY",Visible=.t., Left=41, Top=196,;
    Alignment=1, Width=55, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="ZQQTUBVYLR",Visible=.t., Left=13, Top=16,;
    Alignment=1, Width=83, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="DSHTBKUWVP",Visible=.t., Left=264, Top=16,;
    Alignment=1, Width=125, Height=18,;
    Caption="% completamento"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="MGNWSRGZOE",Visible=.t., Left=13, Top=46,;
    Alignment=1, Width=83, Height=18,;
    Caption="Avviso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="MUAKJHLCQV",Visible=.t., Left=13, Top=231,;
    Alignment=1, Width=83, Height=18,;
    Caption="Listino ricavi:"  ;
  , bGlobalFont=.t.

  add object oBox_2_29 as StdBox with uid="NCBZLIVHQW",left=393, top=271, width=223,height=2
enddefine
define class tgsag_kwaPag3 as StdContainer
  Width  = 618
  height = 328
  stdWidth  = 618
  stdheight = 328
  resizeXpos=347
  resizeYpos=137
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPATIPRIS_3_1 as StdCombo with uid="GTJVRKATPA",value=4,rtseq=65,rtrep=.f.,left=96,top=21,width=183,height=21;
    , HelpContextID = 90464073;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi,"+"Risorse,"+"Tutti,"+"Soggetti della pratica", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPATIPRIS_3_1.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    iif(this.value =3,'R',;
    iif(this.value =4,'',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oPATIPRIS_3_1.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_3_1.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',3,;
      iif(this.Parent.oContained.w_PATIPRIS=='',4,;
      iif(this.Parent.oContained.w_PATIPRIS=='X',5,;
      0)))))
  endfunc

  func oPATIPRIS_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_ATCODPRA))
    endwith
   endif
  endfunc

  func oPATIPRIS_3_1.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA))
    endwith
  endfunc


  add object oPATIPRIS_3_2 as StdCombo with uid="XJDLDNLILX",value=4,rtseq=66,rtrep=.f.,left=96,top=21,width=183,height=21;
    , HelpContextID = 90464073;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPATIPRIS_3_2.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    iif(this.value =3,'R',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oPATIPRIS_3_2.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_3_2.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',3,;
      iif(this.Parent.oContained.w_PATIPRIS=='',4,;
      0))))
  endfunc

  proc oPATIPRIS_3_2.mDefault
    with this.Parent.oContained
      if empty(.w_PATIPRIS)
        .w_PATIPRIS = 'P'
      endif
    endwith
  endproc

  func oPATIPRIS_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA))
    endwith
   endif
  endfunc

  func oPATIPRIS_3_2.mHide()
    with this.Parent.oContained
      return (!EMPTY(.w_ATCODPRA))
    endwith
  endfunc


  add object ZOOMPART as cp_szoombox with uid="WODZOSNAJX",left=15, top=40, width=592,height=224,;
    caption='ZOOMPART',;
   bGlobalFont=.t.,;
    cTable="DIPENDEN",cZoomFile="GSAG_KWA",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,;
    cEvent = "Blank,w_PATIPRIS Changed,Aggiorna",;
    nPag=3;
    , HelpContextID = 194503190


  add object oBtn_3_4 as StdButton with uid="OKDHYSKAYN",left=17, top=277, width=48,height=45,;
    CpPicture="bmp\Check.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per selezionare tutti i partecipanti";
    , HelpContextID = 32122634;
    , Caption='\<Sel.tutti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_4.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"SELEZIONA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_5 as StdButton with uid="KKCTLVFEOP",left=70, top=277, width=48,height=45,;
    CpPicture="bmp\unCheck.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per deselezionare tutti i partecipanti";
    , HelpContextID = 32122634;
    , Caption='\<Desel.tutti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_5.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"DESELEZIONA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_6 as StdButton with uid="URFGQKOJCG",left=404, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=3;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 148212725;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_6.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_3_7 as StdButton with uid="RBPHXKMMFM",left=457, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=3;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 20290810;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_7.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP<.w_TOT_STEP)
      endwith
    endif
  endfunc


  add object oBtn_3_8 as StdButton with uid="HRKBQGYZTV",left=510, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\OK.BMP", caption="", nPag=3;
    , ToolTipText = "Procede con l'inserimento dell'attivit�";
    , HelpContextID = 32102170;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_8.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"INS_ATT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_9 as StdButton with uid="WTRMHGKOLI",left=563, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 22162182;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_12 as StdButton with uid="DKIYDWTZKC",left=123, top=277, width=48,height=45,;
    CpPicture="bmp\InvCheck.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per invertire la selezione dei partecipanti";
    , HelpContextID = 32122634;
    , Caption='\<Inverti sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_12.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"INVSELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_3_11 as StdString with uid="IIPQFPVHAG",Visible=.t., Left=16, Top=22,;
    Alignment=1, Width=77, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oBox_3_10 as StdBox with uid="JGQGIUYYLZ",left=393, top=271, width=223,height=2
enddefine
define class tgsag_kwaPag4 as StdContainer
  Width  = 618
  height = 328
  stdWidth  = 618
  stdheight = 328
  resizeXpos=380
  resizeYpos=219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATCENCOS_4_2 as StdField with uid="HRSJCXRLPG",rtseq=68,rtrep=.f.,;
    cFormVar = "w_ATCENCOS", cQueryName = "ATCENCOS",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di ricavo incongruente o obsoleto",;
    ToolTipText = "Centro di costo/ricavo",;
    HelpContextID = 163618471,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=94, Top=8, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ATCENCOS"

  func oATCENCOS_4_2.mHide()
    with this.Parent.oContained
      return (NOT (g_COAN='S' AND (.w_FLDANA='S' OR .w_FLANAL='S')) or  ! Isalt() or .w_FLANAL='N')
    endwith
  endfunc

  func oATCENCOS_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCENCOS_4_2.ecpDrop(oSource)
    this.Parent.oContained.link_4_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCENCOS_4_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oATCENCOS_4_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Centri di costo/ricavo",'',this.parent.oContained
  endproc

  add object oATCONTAT_4_3 as StdField with uid="NWBAOLLKIR",rtseq=69,rtrep=.f.,;
    cFormVar = "w_ATCONTAT", cQueryName = "ATCONTAT",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona del nominativo",;
    HelpContextID = 146185894,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=94, Top=59, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="NOM_CONT", oKey_1_1="NCCODICE", oKey_1_2="this.w_ATCODNOM", oKey_2_1="NCCODCON", oKey_2_2="this.w_ATCONTAT"

  func oATCONTAT_4_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_ATCODNOM))
    endwith
   endif
  endfunc

  func oATCONTAT_4_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCONTAT_4_3.ecpDrop(oSource)
    this.Parent.oContained.link_4_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCONTAT_4_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.NOM_CONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_ATCODNOM)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStr(this.Parent.oContained.w_ATCODNOM)
    endif
    do cp_zoom with 'NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(this.parent,'oATCONTAT_4_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Persone",'',this.parent.oContained
  endproc

  add object oATPERSON_4_4 as StdField with uid="AZUHYVDJVL",rtseq=70,rtrep=.f.,;
    cFormVar = "w_ATPERSON", cQueryName = "ATPERSON",;
    bObbl = .f. , nPag = 4, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento persona",;
    HelpContextID = 159370924,;
   bGlobalFont=.t.,;
    Height=21, Width=440, Left=160, Top=59, InputMask=replicate('X',60)

  func oATPERSON_4_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CACHKNOM))
    endwith
   endif
  endfunc

  add object oATTELEFO_4_5 as StdField with uid="FTEFTQUHDF",rtseq=71,rtrep=.f.,;
    cFormVar = "w_ATTELEFO", cQueryName = "ATTELEFO",;
    bObbl = .f. , nPag = 4, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Telefono",;
    HelpContextID = 136343893,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=94, Top=85, InputMask=replicate('X',18)

  func oATTELEFO_4_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CACHKNOM))
    endwith
   endif
  endfunc

  add object oATCELLUL_4_6 as StdField with uid="HXJMFWUEJF",rtseq=72,rtrep=.f.,;
    cFormVar = "w_ATCELLUL", cQueryName = "ATCELLUL",;
    bObbl = .f. , nPag = 4, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Cellulare",;
    HelpContextID = 253714770,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=461, Top=85, InputMask=replicate('X',18)

  func oATCELLUL_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CACHKNOM))
    endwith
   endif
  endfunc

  add object oAT_EMAIL_4_7 as StdField with uid="SCRJEGFXAL",rtseq=73,rtrep=.f.,;
    cFormVar = "w_AT_EMAIL", cQueryName = "AT_EMAIL",;
    bObbl = .f. , nPag = 4, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo email",;
    HelpContextID = 70328658,;
   bGlobalFont=.t.,;
    Height=21, Width=506, Left=94, Top=111, InputMask=replicate('X',18)

  func oAT_EMAIL_4_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CACHKNOM))
    endwith
   endif
  endfunc

  add object oAT_EMPEC_4_8 as StdField with uid="WPSZMGFOZG",rtseq=74,rtrep=.f.,;
    cFormVar = "w_AT_EMPEC", cQueryName = "AT_EMPEC",;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo email PEC",;
    HelpContextID = 53551433,;
   bGlobalFont=.t.,;
    Height=21, Width=506, Left=94, Top=137, InputMask=replicate('X',254)

  func oAT_EMPEC_4_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CACHKNOM))
    endwith
   endif
  endfunc

  add object oATNOTPIA_4_9 as StdMemo with uid="YGIOIMCWIM",rtseq=75,rtrep=.f.,;
    cFormVar = "w_ATNOTPIA", cQueryName = "ATNOTPIA",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note dell'attivit�",;
    HelpContextID = 61477191,;
   bGlobalFont=.t.,;
    Height=92, Width=595, Left=12, Top=176


  add object oBtn_4_10 as StdButton with uid="TVNXMOBKAG",left=404, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\LEFT.BMP", caption="", nPag=4;
    , ToolTipText = "Torna al passo precedente";
    , HelpContextID = 148212725;
    , Caption='\<Indietro';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_10.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"PREV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CUR_STEP>1)
      endwith
    endif
  endfunc


  add object oBtn_4_11 as StdButton with uid="MNNFHIFVAZ",left=510, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\OK.BMP", caption="", nPag=4;
    , ToolTipText = "Procede con l'inserimento dell'attivit�";
    , HelpContextID = 32102170;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_11.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"INS_ATT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_12 as StdButton with uid="CRBBHMEGBS",left=563, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 22162182;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESNOM_4_15 as StdField with uid="GDGMBLJNAG",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 262581706,;
   bGlobalFont=.t.,;
    Height=21, Width=366, Left=234, Top=33, InputMask=replicate('X',40)

  add object oOFCODNOM_4_21 as StdField with uid="YOZQJOEYPS",rtseq=80,rtrep=.f.,;
    cFormVar = "w_OFCODNOM", cQueryName = "OFCODNOM",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 257338317,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=94, Top=33, InputMask=replicate('X',15)

  add object oCCDESPIA_4_22 as StdField with uid="PVWTDFCXOH",rtseq=81,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 59727975,;
   bGlobalFont=.t.,;
    Height=21, Width=366, Left=234, Top=8, InputMask=replicate('X',40)

  func oCCDESPIA_4_22.mHide()
    with this.Parent.oContained
      return (NOT (g_COAN='S' AND (.w_FLDANA='S' OR .w_FLANAL='S')) or  ! Isalt() or .w_FLANAL='N')
    endwith
  endfunc


  add object oBtn_4_26 as StdButton with uid="MKUIKONAWW",left=457, top=277, width=48,height=45,;
    CpPicture="..\EXE\BMP\RIGHT.BMP", caption="", nPag=4;
    , ToolTipText = "Avanza al passo successivo";
    , HelpContextID = 20290810;
    , Caption='A\<vanti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_26.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"NEXT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.F.)
      endwith
    endif
  endfunc

  add object oStr_4_14 as StdString with uid="RQPJUMNDAV",Visible=.t., Left=12, Top=157,;
    Alignment=0, Width=29, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="NWCQVTGGLG",Visible=.t., Left=10, Top=33,;
    Alignment=1, Width=79, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_4_17 as StdString with uid="MNRIPUBYZN",Visible=.t., Left=10, Top=59,;
    Alignment=1, Width=79, Height=18,;
    Caption="Rif.persona:"  ;
  , bGlobalFont=.t.

  add object oStr_4_18 as StdString with uid="NPMPAUIHYR",Visible=.t., Left=10, Top=85,;
    Alignment=1, Width=79, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_4_19 as StdString with uid="JFLQWQKTEA",Visible=.t., Left=366, Top=85,;
    Alignment=1, Width=94, Height=18,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_4_20 as StdString with uid="BAYDLNMCQY",Visible=.t., Left=10, Top=111,;
    Alignment=1, Width=79, Height=18,;
    Caption="Email:"  ;
  , bGlobalFont=.t.

  add object oStr_4_23 as StdString with uid="FVDUAIOFQM",Visible=.t., Left=10, Top=9,;
    Alignment=1, Width=79, Height=18,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  func oStr_4_23.mHide()
    with this.Parent.oContained
      return (NOT (g_COAN='S' AND (.w_FLDANA='S' OR .w_FLANAL='S')) or  ! Isalt() or .w_FLANAL='N')
    endwith
  endfunc

  add object oStr_4_27 as StdString with uid="GYMFTYDXFF",Visible=.t., Left=10, Top=137,;
    Alignment=1, Width=79, Height=18,;
    Caption="Email PEC:"  ;
  , bGlobalFont=.t.

  add object oBox_4_13 as StdBox with uid="QCQOIYVAXH",left=393, top=271, width=223,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kwa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_kwa
Proc RivalorizzoCombo(pParent)
   local obj
   obj = pParent.getCtrl("w_ATCAUATT")
   obj.Clear()
   obj.Init()
   obj.SetRadio()
   * Aggiorniamo la variabile
   pParent.w_ATCAUATT=obj.RadioValue()
   obj = .null.
Endproc
* --- Fine Area Manuale
