* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bdx                                                        *
*              Import eventi da excel                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-02                                                      *
* Last revis.: 2017-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPath,pCodPer,pCodGru,pErr_Log,pEventType
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bdx",oParentObject,m.pPath,m.pCodPer,m.pCodGru,m.pErr_Log,m.pEventType)
return(i_retval)

define class tgsfa_bdx as StdBatch
  * --- Local variables
  pPath = space(254)
  pCodPer = space(5)
  pCodGru = space(5)
  pErr_Log = .NULL.
  pEventType = space(10)
  w_FLERRORS = .f.
  w_NSER_ATT = 0
  w_NUM_ITEM = 0
  w_CURSNAME = space(10)
  w_EXCELCON = 0
  w_EXCELERR = 0
  w_CUR_FILE = space(254)
  w_FLDINDEX = 0
  w_EVCURNAM = space(10)
  w_IDFLDTBL = 0
  w_FLD_TYPE = space(1)
  w_LSTFIELDS = space(254)
  w_NVLFIELD = space(30)
  w_TETIPDIR = space(1)
  * --- WorkFile variables
  TIPEVENT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Read from TIPEVENT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIPEVENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIPEVENT_idx,2],.t.,this.TIPEVENT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TETIPDIR"+;
        " from "+i_cTable+" TIPEVENT where ";
            +"TETIPEVE = "+cp_ToStrODBC(this.pEventType);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TETIPDIR;
        from (i_cTable) where;
            TETIPEVE = this.pEventType;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TETIPDIR = NVL(cp_ToDate(_read_.TETIPDIR),cp_NullValue(_read_.TETIPDIR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_FLERRORS = .F.
    this.w_NSER_ATT = 0
    LOCAL L_OLDERR, L_Err, L_MACRO, L_LSTFIELDS
    L_OLDERR=ON("ERROR")
    L_Err=.F.
    ON ERROR L_Err=.T.
    private L_OldPath
    L_OldPath = Sys(5)+Sys(2003)
    CD (this.pPath)
    if L_Err
      this.pErr_Log.AddMsgLog("Percorso inesistente o non raggiungibile")     
      this.w_FLERRORS = .T.
    else
      * --- La ADIR del foxpro con skeleton xls restituisce anche i file xlsx
      this.w_NUM_ITEM = ADIR(L_ArrFile, "*.xls")
    endif
    CD (L_OldPath)
    release L_OldPath
    if this.w_NUM_ITEM>0
      if !this.w_FLERRORS
        this.w_EVCURNAM = SYS(2015)
        vq_exec("..\agen\exe\QUERY\GSFA_BIM.VQR",this,this.w_EVCURNAM)
        * --- Metto in un array i campi della tabella per determinare successivamente
        *     il tipo nella creazione dinamica della frase di insert
        AFIELDS(L_TableFld)
      endif
    endif
    if !this.w_FLERRORS
      * --- Analisi file da importare
      do while this.w_NUM_ITEM>0
        this.w_CUR_FILE = ADDBS(ALLTRIM(this.pPath))+ALLTRIM(L_ArrFile(this.w_NUM_ITEM,1))
        if TYPE ("g_EXCELSTRINGCONN")="C" AND not empty (g_EXCELSTRINGCONN)
          this.w_EXCELCON = SQLSTRINGCONNECT( STRTRAN ( STRTRAN(g_EXCELSTRINGCONN,"<NOMEFILE>", this.w_CUR_FILE ), "<NOMEDIR>", ADDBS(JUSTPATH(this.w_CUR_FILE)) ) )
        else
          if JUSTEXT(this.w_CUR_FILE)=="XLSX"
            * --- Creo la connessione al foglio Excel con il driver di office 2007
            this.w_EXCELCON = SQLSTRINGCONNECT("Driver=Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb);DBQ="+this.w_CUR_FILE+";DefaultDir="+ADDBS(JUSTPATH(this.w_CUR_FILE))+";MaxBufferSize=2048;PageTimeout=5;")
          else
            * --- Creo la connessione al foglio Excel
            this.w_EXCELCON = SQLSTRINGCONNECT("Driver=Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb);DBQ="+this.w_CUR_FILE+";DefaultDir="+ADDBS(JUSTPATH(this.w_CUR_FILE))+";DriverId=790;FIL=excel 8.0;MaxBufferSize=2048;PageTimeout=5;")
          endif
        endif
        if this.w_EXCELCON>0
          * --- Eseguo la query...
          *     Il riferimento alla pagina del foglio Excel avviene SEMPRE e SOLO per NOME...
          *     A causa di problemi con le gestione degli handle dei processi aperti, la 
          *     lettura dei dati dal foglio Excel NECESSITA che la pagina sorgente sia nominata
          *     come "Foglio1"
          *     Cambiando il nome alla pagina di riferimento, la connessione restituir� un errore.
          this.w_CURSNAME = SYS(2015)
          this.w_EXCELERR = SQLEXEC(this.w_EXCELCON, "select * from [Foglio1$]" , this.w_CURSNAME )
          if this.w_EXCELERR<0
            this.pErr_Log.AddMsgLog("Impossibile stabilire connessione al foglio Excel %1%0%2", this.w_CUR_FILE, MESSAGE())     
          endif
          * --- Chiusura della connessione al foglio Excel
          SQLCANCEL(this.w_EXCELCON)
          SQLDISCONNECT(this.w_EXCELCON)
          if USED(this.w_CURSNAME) AND RECCOUNT(this.w_CURSNAME)>0
            SELECT (this.w_CURSNAME)
            GO TOP
            * --- Recupero la lista dei campi inclusi nel foglio excel
            AFIELDS(L_ExcelFlds)
            * --- Preparo l'array dei campi per la creazione delle variabili per l'inserimento
            *     dei dati nel cursore per il successivo inserimento degli eventi
            * --- Scan dei dati del foglio excel per inserimento nel cursore della successiva insert degli eventi
            SCAN
            this.w_FLDINDEX = ALEN(L_ExcelFlds,1)
            L_MACRO = "INSERT INTO '"+this.w_EVCURNAM+"' ("
            this.w_LSTFIELDS = ") VALUES ("
            do while this.w_FLDINDEX>0
              * --- Determino il tipo di campo per impostare correttamente il tipo nell'NVL
              this.w_IDFLDTBL = ASCAN( L_TableFld , ALLTRIM(L_ExcelFlds(this.w_FLDINDEX, 1)), 1, ALEN(L_TableFld,1), 1, 15)
              this.w_FLD_TYPE = L_TableFld(this.w_IDFLDTBL, 2)
              L_MACRO = L_MACRO + ALLTRIM(L_ExcelFlds(this.w_FLDINDEX, 1))+IIF(this.w_FLDINDEX>1, ", ", "")
              this.w_NVLFIELD = ALLTRIM(this.w_CURSNAME)+"."+ALLTRIM(L_ExcelFlds(this.w_FLDINDEX, 1))
              * --- Gestione campi particolari (Es. Seriale e tipo evento)
              do case
                case L_ExcelFlds(this.w_FLDINDEX, 1)="EVSERIAL"
                  local L_VerSerial
                  L_VerSerial = this.w_NVLFIELD
                  this.w_NSER_ATT = this.w_NSER_ATT + 1
                  this.w_NVLFIELD = "'"+IIF(EMPTY(NVL(&L_VerSerial , " ")), "E"+RIGHT(REPL("0",10)+ALLTRIM(STR(this.w_NSER_ATT)),9), this.w_NVLFIELD)+"'"
                  release L_VerSerial
                case L_ExcelFlds(this.w_FLDINDEX, 1)="EVTIPEVE"
                  local L_TipEve
                  L_TipEve = this.w_NVLFIELD
                  this.w_NVLFIELD = "'"+IIF( EMPTY(NVL(&L_TipEve, " ")), this.pEventType, this.w_NVLFIELD)+"'"
                  release L_TipEve
                case L_ExcelFlds(this.w_FLDINDEX, 1)="EVDIREVE"
                  local L_DirEve
                  L_DirEve = this.w_NVLFIELD
                  this.w_NVLFIELD = IIF(EMPTY(NVL(&L_DirEve , " ")), "'"+this.w_TETIPDIR+"'" , this.w_NVLFIELD)
                  release L_DirEve
                case L_ExcelFlds(this.w_FLDINDEX, 1)="EVPERSON"
                  local L_CodPer
                  L_CodPer = this.w_NVLFIELD
                  this.w_NVLFIELD = "'"+IIF(EMPTY(NVL(&L_CodPer , " ")), ALLTRIM(this.pCodPer) , this.w_NVLFIELD)+"'"
                  release L_CodPer
                case L_ExcelFlds(this.w_FLDINDEX, 1)="EVGRUPPO"
                  local L_CodGru
                  L_CodGru = this.w_NVLFIELD
                  this.w_NVLFIELD = "'"+IIF(EMPTY(NVL(&L_CodGru , " ")), ALLTRIM(this.pCodGru) , this.w_NVLFIELD)+"'"
                  release L_CodGru
                otherwise
                  * --- Verifico se � necessaria la conversione del dato
                  do case
                    case L_ExcelFlds(this.w_FLDINDEX, 2)="B" AND this.w_FLD_TYPE="C"
                      this.w_NVLFIELD = "ALLTRIM(STR("+ALLTRIM(this.w_CURSNAME)+"."+ALLTRIM(L_ExcelFlds(this.w_FLDINDEX, 1))+"))"
                    case L_ExcelFlds(this.w_FLDINDEX, 2)="C" AND this.w_FLD_TYPE="I"
                      this.w_NVLFIELD = "VAL("+ALLTRIM(this.w_CURSNAME)+"."+ALLTRIM(L_ExcelFlds(this.w_FLDINDEX, 1))+")"
                    case L_ExcelFlds(this.w_FLDINDEX, 2)="C" AND this.w_FLD_TYPE="T"
                      this.w_NVLFIELD = "CTOT("+ALLTRIM(this.w_CURSNAME)+"."+ALLTRIM(L_ExcelFlds(this.w_FLDINDEX, 1))+")"
                  endcase
                  do case
                    case this.w_FLD_TYPE="C" OR this.w_FLD_TYPE="G" OR this.w_FLD_TYPE="M" OR this.w_FLD_TYPE="V"
                      this.w_NVLFIELD = "NVL("+this.w_NVLFIELD+",'')"
                    case this.w_FLD_TYPE="D"
                      this.w_NVLFIELD = "NVL("+this.w_NVLFIELD+",cpCharToDate('  -  -    '))"
                    case this.w_FLD_TYPE="T"
                      this.w_NVLFIELD = "NVL("+this.w_NVLFIELD+",CTOT('  -  -    '))"
                    case this.w_FLD_TYPE="L"
                      this.w_NVLFIELD = "NVL("+this.w_NVLFIELD+", .F. )"
                    case this.w_FLD_TYPE="N" OR this.w_FLD_TYPE="I"
                      this.w_NVLFIELD = "NVL("+this.w_NVLFIELD+", 0 )"
                  endcase
              endcase
              this.w_LSTFIELDS = this.w_LSTFIELDS + this.w_NVLFIELD + IIF(this.w_FLDINDEX>1, ", ", "")
              this.w_FLDINDEX = this.w_FLDINDEX - 1
            enddo
            * --- Unisco le due stringhe per formare la frase d'insert corretta con i soli campi presenti nel foglio excel
            L_MACRO = L_MACRO + this.w_LSTFIELDS+")"
            &L_MACRO
            SELECT (this.w_CURSNAME)
            ENDSCAN
          else
            this.pErr_Log.AddMsgLog("Nessun dato da importare")     
          endif
          USE IN SELECT(this.w_CURSNAME)
        else
          this.pErr_Log.AddMsgLog("Impossibile stabilire connessione al foglio Excel %1%0%2", this.w_CUR_FILE, MESSAGE())     
        endif
        release L_ExcelFlds
        this.w_NUM_ITEM = this.w_NUM_ITEM - 1
      enddo
      * --- Analizzo gli eventi da inserire, i seriali vuoti vanno riempiti
      SELECT (this.w_EVCURNAM)
      GO TOP
      SCAN FOR EMPTY(NVL(EVSERIAL, " "))
      this.w_NSER_ATT = this.w_NSER_ATT + 1
      REPLACE EVSERIAL WITH "E"+RIGHT(REPL("0",10)+ALLTRIM(STR(this.w_NSER_ATT)),9)
      ENDSCAN
      * --- Elimino dal cursore tutte le righe con direzione non congruente 
      *     al tipo evento passato come parametro
      DELETE FROM (this.w_EVCURNAM) WHERE NVL(EVDIREVE, " ")<>this.w_TETIPDIR OR NVL(EVTIPEVE, " ")<>this.pEventType
      * --- Chiamata a routine standard d'inserimento eventi
      this.w_FLERRORS = this.w_FLERRORS OR !GSFA_BEV(this, this.w_EVCURNAM , "", this.pErr_Log, "X")
    endif
    release L_ArrFile
    ON ERROR &L_OLDERR
    i_retcode = 'stop'
    i_retval = IIF(this.w_FLERRORS, "E", "")
    return
  endproc


  proc Init(oParentObject,pPath,pCodPer,pCodGru,pErr_Log,pEventType)
    this.pPath=pPath
    this.pCodPer=pCodPer
    this.pCodGru=pCodGru
    this.pErr_Log=pErr_Log
    this.pEventType=pEventType
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TIPEVENT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPath,pCodPer,pCodGru,pErr_Log,pEventType"
endproc
