* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bba                                                        *
*              Inserisce dati in ALT_DETT da generatore                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-10-01                                                      *
* Last revis.: 2008-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PADRE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bba",oParentObject,m.w_PADRE)
return(i_retval)

define class tgsag_bba as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_RETVAL = space(100)
  w_CPROWNUM = 0
  w_bUnderTran = .f.
  w_CHILDREF = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_bUnderTran = vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    if this.w_bUnderTran
      * --- begin transaction
      cp_BeginTrs()
    endif
    if this.w_PADRE.w_ALT_DETT.reccount()<>0
      this.w_PADRE.w_ALT_DETT.GoTop()     
      do while !this.w_PADRE.w_ALT_DETT.Eof()
        this.w_PADRE.w_ALT_DETT.ReadCurrentRecord()     
        this.w_PADRE.w_ALT_DETT.DESERIAL = this.w_PADRE.w_MVSERIAL
        this.w_PADRE.w_ALT_DETT.cpccchk = cp_NewCCChk()
        this.w_PADRE.w_ALT_DETT.SaveCurrentRecord()     
        DIMENSION ArrInsert( 1 ,2)
        * --- Popolo l'array con i dati del dettaglio pagamenti
        this.w_PADRE.w_ALT_DETT.CurrentRecordToArray(@ArrInsert)     
        * --- Inserimento record del dettaglio pagamenti
        * --- Try
        local bErr_035ABEC0
        bErr_035ABEC0=bTrsErr
        this.Try_035ABEC0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_RETVAL = ah_MsgFormat("Errore inserimento dettaglio pagamenti.%0%1", MESSAGE())
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_RETVAL
        endif
        bTrsErr=bTrsErr or bErr_035ABEC0
        * --- End
        Release ArrInsert
        this.w_PADRE.w_ALT_DETT.Next()     
      enddo
    endif
    if this.w_bUnderTran
      if EMPTY(this.w_RETVAL)
        * --- commit
        cp_EndTrs(.t.)
      else
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
    endif
  endproc
  proc Try_035ABEC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_RETVAL = InsertTable("ALT_DETT", @ArrInsert )
    if !EMPTY(this.w_RETVAL)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Errore inserimento dettaglio pagamenti.%0%1", this.w_RETVAL)
    else
      this.w_CHILDREF = "w_ALT_DETT"
    endif
    return


  proc Init(oParentObject,w_PADRE)
    this.w_PADRE=w_PADRE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PADRE"
endproc
