* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bex                                                        *
*              Sincronizzazione con Outlook                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-22                                                      *
* Last revis.: 2017-05-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione,pSerialVCS,pCodPraVCS,pDatIniVCS,pDatFinVCS,pOggettVCS,pCauAttVCS,pPersonaVCS,pNoteVCS,pFormatoNote
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bex",oParentObject,m.pAzione,m.pSerialVCS,m.pCodPraVCS,m.pDatIniVCS,m.pDatFinVCS,m.pOggettVCS,m.pCauAttVCS,m.pPersonaVCS,m.pNoteVCS,m.pFormatoNote)
return(i_retval)

define class tgsag_bex as StdBatch
  * --- Local variables
  pAzione = space(10)
  pSerialVCS = space(0)
  pCodPraVCS = space(0)
  pDatIniVCS = ctod("  /  /  ")
  pDatFinVCS = space(0)
  pOggettVCS = space(10)
  pCauAttVCS = space(10)
  pPersonaVCS = space(10)
  pNoteVCS = space(10)
  pFormatoNote = space(1)
  w_DECTOT = 0
  w_DECUNI = 0
  loOutlook = .NULL.
  loNamespace = .NULL.
  loDefaultFol = .NULL.
  oAppuntamOutlook = .NULL.
  oAppDaAnalizzare = .NULL.
  oAttivitaGiaExp = .NULL.
  oAppuntamento = .NULL.
  w_CntDaImp = 0
  w_CntAggAE = 0
  w_CntAggOU = 0
  w_CntDelOU = 0
  w_CntCreOU = 0
  w_CntAtti = 0
  w_DaContr = 0
  w_OuCateg = space(10)
  w_Ok = .f.
  w_ATSERIAL = space(20)
  w_DatApp = ctot("")
  w_DatMod = ctot("")
  w_ATSTATUS = space(1)
  w_Serial = space(20)
  w_DataApp = ctod("  /  /  ")
  w_DataAppFin = ctod("  /  /  ")
  w_OraStart = 0
  w_OraEnd = 0
  w_TxtSel = space(10)
  w_CauDefault = space(20)
  w_ATDATINI = ctot("")
  w_ATDATFIN = ctot("")
  w_ATOGGETT = space(100)
  w_ATLOCALI = space(100)
  w_AT__ENTE = space(10)
  w_ATCODPRA = space(100)
  w_SerialToExp = space(25)
  w_PartSerial = space(20)
  w_TextPart = space(254)
  w_TextRis = space(254)
  w_TextGru = space(254)
  w_DesPraAttivita = space(10)
  w_TextApp = space(254)
  w_OggApp = space(254)
  w_ATFLPROM = space(1)
  w_ATPROMEM = ctot("")
  w_ATNOTPIA = space(0)
  w_ATCAUATT = space(20)
  w_ATPERSON = space(60)
  w_CARAGGST = space(1)
  w_Categoria = space(10)
  w_ATSTAATT = 0
  w_CNRIFGIU = space(10)
  w_CSTipCat = space(1)
  w_Giudici = space(100)
  w_DataIni = ctod("  /  /  ")
  w_DataFin = ctod("  /  /  ")
  w_OraIni = space(5)
  w_OraFin = space(5)
  w_RitornoCarrello = space(2)
  w_CODIMP = space(10)
  w_Impianti = space(10)
  w_NoteApp = space(0)
  w_OutlProperty = .NULL.
  w_OutlCodice = space(10)
  w_PAFLVISI = space(1)
  w_TokenNote = space(5)
  w_Handle = 0
  w_NomeFileLog = space(10)
  w_OggettoAppuntamento = space(10)
  w_ATFLATRI = space(1)
  w_ATNUMPRI = 0
  w_NEWSTAATT = 0
  w_NEWNUMPRI = 0
  w_ATCOMRIC = space(15)
  w_ATATTRIC = space(15)
  w_ATATTCOS = space(15)
  w_ATCENCOS = space(15)
  w_ATCENRIC = space(15)
  w_DataAppToDel = ctod("  /  /  ")
  w_CODPART = space(5)
  w_TIPPART = space(1)
  w_Locali_Atti = space(30)
  w_EPDESCRI = space(60)
  w_DesATC = space(40)
  w_Descec = space(40)
  w_DesCoR = space(40)
  w_DesATR = space(40)
  w_Descer = space(40)
  w_DESCON = space(40)
  w_IMDESCRI = space(50)
  w_NoteCodiceImp = space(10)
  w_NoteDescrImp = space(10)
  w_NoteCodiceCmp = 0
  w_NoteDescrCmp = space(10)
  * --- WorkFile variables
  TMPDPOFF_idx=0
  OFF_ATTI_idx=0
  PAR_AGEN_idx=0
  CAN_TIER_idx=0
  COM_ATTI_idx=0
  ATTIVITA_idx=0
  CENCOST_idx=0
  IMP_DETT_idx=0
  IMP_MAST_idx=0
  PRA_ENTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametri aggiunti per utilizzare la composizione delle note: chiamaiamo pag 11 da GSAG_BAP per la creazione del VCS
    *     In questo caso pAzione � COMPONINOTE ed il batch restituisce la stringa ottenuta
    * --- Dichiariamo qui la stringa da utilizzare per determinare le note
    this.w_TokenNote = "Note:"
    * --- Dichiarazione variabili VisualFox
    PRIVATE L_OldErr, L_Err
    L_OldErr = ON("ERROR")
    L_Err = .F.
     
 #define olFolderCalendar 9 
 #define olAppointmentItem 1 
 #define olNumber 3
    PRIVATE MSOUTL_CODICE, olText
    MSOUTL_CODICE = "Zucchetti"
    olText = 1
    * --- Gestione log
    this.w_Handle = 0
    this.w_DatApp = DATETIME()
    this.w_NomeFileLog = ADDBS(Tempadhoc())+"LogAttivita"+CHKSTRCH(TTOC(this.w_DatApp))+ALLTRIM(STR(i_CodUte))+".Txt"
    this.w_OggettoAppuntamento = "<OGGETT>"
    * --- Read from PAR_AGEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PAFLVISI,PAOGGOUT"+;
        " from "+i_cTable+" PAR_AGEN where ";
            +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PAFLVISI,PAOGGOUT;
        from (i_cTable) where;
            PACODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PAFLVISI = NVL(cp_ToDate(_read_.PAFLVISI),cp_NullValue(_read_.PAFLVISI))
      this.w_OggettoAppuntamento = NVL(cp_ToDate(_read_.PAOGGOUT),cp_NullValue(_read_.PAOGGOUT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pAzione=="SINCRO"
        * --- Disabilita il Decoartor altrimento si generano errori quando l utente muove il mouse sul form
        DecoratorState ("D")
        * --- Creiamo loOutlook, loNamespace, loDefaultFol, oAppuntamOutlook
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- 1) Estraiamo i codici delle scadenze da esportare in una tabella temporanea
        *     2) importiamo da Outlook: se troviamo un codice presente nella tabella lo cancelliamo
        *     3) esportiamo i codici rimasti nella tabella temporanea
        * --- ATSTATUS
        *     'D' - "Da svolgere"
        *     'I' - "In corso"
        *     'T' - "Provvisoria"
        *     'F' - "Evasa"
        *     'P' - "Completata"
        * --- Nella query filtriamo le attivit� che soddisfano il filtro di data
        * --- Leggiamo anche w_OggettoAppuntamento
        if EMPTY(this.w_OggettoAppuntamento)
          this.w_OggettoAppuntamento = "<OGGETT>"
        endif
        if this.w_PAFLVISI="L"
          * --- Create temporary table TMPDPOFF
          i_nIdx=cp_AddTableDef('TMPDPOFF') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_HOJJNAPDEZ[1]
          indexes_HOJJNAPDEZ[1]='ATSERIAL'
          vq_exec('QUERY\GSAG_BEX.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_HOJJNAPDEZ,.f.)
          this.TMPDPOFF_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Create temporary table TMPDPOFF
          i_nIdx=cp_AddTableDef('TMPDPOFF') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_TLKBGTUJRP[1]
          indexes_TLKBGTUJRP[1]='ATSERIAL'
          vq_exec('QUERY\GSAG1BEX.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_TLKBGTUJRP,.f.)
          this.TMPDPOFF_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
        =fputs(this.w_Handle,Ah_MsgFormat("Fase di import"))
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        =fputs(this.w_Handle,Ah_MsgFormat(""))
        =fputs(this.w_Handle,Ah_MsgFormat("Fase di export"))
        =fputs(this.w_Handle,Ah_MsgFormat(""))
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Al termine del esportazion riattiva il decorator dei form 
        DecoratorState ("A")
      case this.pAzione=="INITVAR"
        this.oParentObject.w_Causale = ""
        * --- Read from PAR_AGEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PACAUATT"+;
            " from "+i_cTable+" PAR_AGEN where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PACAUATT;
            from (i_cTable) where;
                PACODAZI = i_codazi;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_Causale = NVL(cp_ToDate(_read_.PACAUATT),cp_NullValue(_read_.PACAUATT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if .F. AND NVL(ALLTRIM(this.oParentObject.w_Causale),"")==""
          ah_ErrorMsg("Attenzione, non � stato specificato un tipo nella tabella parametri attivit�")
        endif
      case this.pAzione=="DATAFINE"
        * --- Gestione data finale
        this.oParentObject.w_OREFIN = "23"
        this.oParentObject.w_MINFIN = "59"
        do case
          case this.oParentObject.w_GestDataFin="1"
            * --- Un mese
            this.oParentObject.w_DATFIN = GOMONTH(this.oParentObject.w_DATINI,1)
          case this.oParentObject.w_GestDataFin="2"
            * --- Due mesi
            this.oParentObject.w_DATFIN = GOMONTH(this.oParentObject.w_DATINI,2)
          case this.oParentObject.w_GestDataFin="3"
            * --- Tre mesi
            this.oParentObject.w_DATFIN = GOMONTH(this.oParentObject.w_DATINI,3)
          case this.oParentObject.w_GestDataFin="6"
            * --- Sei mesi
            this.oParentObject.w_DATFIN = GOMONTH(this.oParentObject.w_DATINI,6)
          case this.oParentObject.w_GestDataFin="A"
            * --- Un anno
            this.oParentObject.w_DATFIN = GOMONTH(this.oParentObject.w_DATINI,12)
          case this.oParentObject.w_GestDataFin="O"
            * --- Oggi
            this.oParentObject.w_DATINI = i_datsys
            this.oParentObject.w_MININI = "00"
            this.oParentObject.w_OREINI = "00"
            this.oParentObject.w_DATFIN = this.oParentObject.w_DATINI
          case this.oParentObject.w_GestDataFin="V"
            * --- Vuoto
            this.oParentObject.w_DATFIN = CTOD("  -  -  ")
        endcase
      case this.pAzione=="CANCELLA"
        =fputs(this.w_Handle,Ah_MsgFormat("Cancellazione appuntamenti"))
        * --- Disabilita il Decoartor altrimento si generano errori quando l utente muove il mouse sul form
        DecoratorState ("D")
        * --- Creiamo loOutlook, loNamespace, loDefaultFol, oAppuntamOutlook
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Al termine del esportazion riattiva il decorator dei form 
        DecoratorState ("A")
      case this.pAzione=="COMPONINOTE"
        this.w_ATSERIAL = this.pSerialVCS
        this.w_ATCODPRA = this.pCodPraVCS
        this.w_ATDATINI = this.pDatIniVCS
        this.w_ATDATFIN = this.pDatFinVCS
        this.w_ATOGGETT = this.pOggettVCS
        this.w_ATCAUATT = this.pCauAttVCS
        this.w_ATPERSON = this.pPersonaVCS
        this.w_ATNOTPIA = this.pNoteVCS
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        i_retval = this.w_TextApp
        return
    endcase
    if cp_fileexist(this.w_NomeFileLog)
      viewFile(this.w_NomeFileLog)
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa gli appuntamenti di Outlook
    * --- Per fare una ricerca in outlook sulla data ho bisogno del formato datetime senza i secondi
    PRIVATE SetSec
    SetSec = SET("SECONDS")
    L_Err = .F.
    ON ERROR L_Err=.T.
    SET SECONDS OFF
    * --- Inseriamo in oAppDaAnalizzare gli item del calendario da importare
    this.oAppDaAnalizzare = this.oAppuntamOutlook.Restrict("[Start] > '"+TTOC(this.oParentObject.w_DATA_INI)+"' AND [Start] <= '"+TTOC(this.oParentObject.w_DATA_FIN)+"'")
    if L_Err OR ISNULL(this.oAppDaAnalizzare)
      * --- Nessuna appuntamento da analizzare
      this.w_CntDaImp = 0
    else
      * --- Ci sono appuntamenti da importare
      this.w_CntDaImp = this.oAppDaAnalizzare.Count
    endif
    SET SECONDS &SetSec
    ON ERROR &L_OldErr
    =fputs(this.w_Handle,Ah_MsgFormat("Appuntamenti di Outlook da analizzare:")+SPACE(1)+ALLTRIM(STR(this.w_CntDaImp)))
    =fputs(this.w_Handle,"")
    this.w_CntAggAE = 0
    this.w_CntAggOU = 0
    this.w_CntDelOU = 0
    if this.w_CntDaImp>0
      * --- Ci sono appuntamenti da controllare
      L_Err = .F.
      ON ERROR L_Err=.T.
      this.oAppDaAnalizzare.Sort("Start")     
      * --- Tentiamo di ordinare per codice scadenza, se il campo manca viene generato un errore che gestiamo riordinando per data
      this.oAppDaAnalizzare.Sort(MSOUTL_CODICE)     
      * --- Gestiamo nella variabile w_DaContr il numero di appuntamenti che ancora ci restano da controllare
      ON ERROR &L_OldErr
      L_Err = .F.
      this.w_DaContr = this.w_CntDaImp
      do while this.w_DaContr > 0 AND !L_Err
        * --- Procediamo decrescendo in modo che non sia necessario un ulteriore sort 
        *     e affinch� Outlook non vada in confusione decrementando la Count
        this.oAppuntamento = this.oAppDaAnalizzare.Item( this.w_DaContr )
        this.w_DaContr = this.w_DaContr - 1
        * --- Appoggiamo la user property in una var. di appoggio in modo che si possa controllare il valore in debug
        this.w_OutlProperty = this.oAppuntamento.UserProperties(MSOUTL_CODICE)
        this.w_OutlCodice = ""
        if !ISNULL(this.w_OutlProperty)
          this.w_OutlCodice = NVL(this.oAppuntamento.UserProperties(MSOUTL_CODICE).Value,"")
        endif
        if !EMPTY( this.w_OutlCodice )
          if AT("#", this.w_OutlCodice, 1)=0 OR ALLTRIM(SUBSTR(this.w_OutlCodice, 1, AT("#", this.w_OutlCodice, 1)-1))==ALLTRIM(i_CodAzi)
            * --- L'appuntamento � stato esportato in precedenza proprio dallo studio attivo
            if AT("#", this.w_OutlCodice, 1)=0
              this.w_ATSERIAL = this.oAppuntamento.UserProperties(MSOUTL_CODICE).Value
              * --- Impostiamo il codice correttamente composto
              this.oAppuntamento.UserProperties(MSOUTL_CODICE).Value = ALLTRIM(i_CodAzi)+"#"+this.w_ATSERIAL
              this.oAppuntamento.Save()     
            else
              this.w_ATSERIAL = SUBSTR(this.oAppuntamento.UserProperties(MSOUTL_CODICE).Value, AT("#", this.oAppuntamento.UserProperties(MSOUTL_CODICE).Value, 1)+1)
            endif
            this.w_ATSTATUS = " "
            L_Err = .F.
            ON ERROR L_Err=.T.
            ON ERROR &L_OldErr
            this.w_DatApp = this.oAppuntamento.LastModificationTime
            * --- Controlliamo con le attivit� ed aggiorniamo il meno recente
            this.w_DatMod = CTOD("  -  -       :  :  ")
            * --- Leggiamo data creazione e data modifica e controlliamo l'esistenza dell'attivit�
            * --- Inolte avvalora le seguenti variabili necessarie a pagina 8:
            *     w_ATDATINI, w_ATDATFIN, w_ATLOCALI, w_ATFLPROM, w_ATPROMEM, w_ATSTAATT, w_ATNUMPRI
            * --- Read from OFF_ATTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ATSTATUS,ATDATOUT,ATDATINI,ATDATFIN,ATLOCALI,ATFLPROM,ATPROMEM,ATSTAATT,ATOGGETT,ATCODPRA,ATPERSON,ATNOTPIA,ATCOMRIC,ATATTRIC,ATATTCOS,ATCENCOS,ATCENRIC,ATFLATRI,ATNUMPRI"+;
                " from "+i_cTable+" OFF_ATTI where ";
                    +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ATSTATUS,ATDATOUT,ATDATINI,ATDATFIN,ATLOCALI,ATFLPROM,ATPROMEM,ATSTAATT,ATOGGETT,ATCODPRA,ATPERSON,ATNOTPIA,ATCOMRIC,ATATTRIC,ATATTCOS,ATCENCOS,ATCENRIC,ATFLATRI,ATNUMPRI;
                from (i_cTable) where;
                    ATSERIAL = this.w_ATSERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ATSTATUS = NVL(cp_ToDate(_read_.ATSTATUS),cp_NullValue(_read_.ATSTATUS))
              this.w_DatMod = NVL((_read_.ATDATOUT),cp_NullValue(_read_.ATDATOUT))
              this.w_ATDATINI = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
              this.w_ATDATFIN = NVL((_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
              this.w_ATLOCALI = NVL(cp_ToDate(_read_.ATLOCALI),cp_NullValue(_read_.ATLOCALI))
              this.w_ATFLPROM = NVL(cp_ToDate(_read_.ATFLPROM),cp_NullValue(_read_.ATFLPROM))
              this.w_ATPROMEM = NVL((_read_.ATPROMEM),cp_NullValue(_read_.ATPROMEM))
              this.w_ATSTAATT = NVL(cp_ToDate(_read_.ATSTAATT),cp_NullValue(_read_.ATSTAATT))
              this.w_ATOGGETT = NVL(cp_ToDate(_read_.ATOGGETT),cp_NullValue(_read_.ATOGGETT))
              this.w_ATCODPRA = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
              this.w_ATPERSON = NVL(cp_ToDate(_read_.ATPERSON),cp_NullValue(_read_.ATPERSON))
              this.w_ATNOTPIA = NVL(cp_ToDate(_read_.ATNOTPIA),cp_NullValue(_read_.ATNOTPIA))
              this.w_ATCOMRIC = NVL(cp_ToDate(_read_.ATCOMRIC),cp_NullValue(_read_.ATCOMRIC))
              this.w_ATATTRIC = NVL(cp_ToDate(_read_.ATATTRIC),cp_NullValue(_read_.ATATTRIC))
              this.w_ATCOMRIC = NVL(cp_ToDate(_read_.ATCOMRIC),cp_NullValue(_read_.ATCOMRIC))
              this.w_ATATTCOS = NVL(cp_ToDate(_read_.ATATTCOS),cp_NullValue(_read_.ATATTCOS))
              this.w_ATCENCOS = NVL(cp_ToDate(_read_.ATCENCOS),cp_NullValue(_read_.ATCENCOS))
              this.w_ATCENRIC = NVL(cp_ToDate(_read_.ATCENRIC),cp_NullValue(_read_.ATCENRIC))
              this.w_ATFLATRI = NVL(cp_ToDate(_read_.ATFLATRI),cp_NullValue(_read_.ATFLATRI))
              this.w_ATNUMPRI = NVL(cp_ToDate(_read_.ATNUMPRI),cp_NullValue(_read_.ATNUMPRI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Not IsRisAtt( this.w_ATSERIAL , this.w_ATFLATRI ) 
              * --- E' un'attivit� riservata sul database non devo fare nulla...
            else
              do case
                case EMPTY(this.w_DatMod) AND EMPTY(this.w_ATSTATUS)
                  * --- La lettura non � andata a buon fine, l'appuntamento non � pi� presente come attivit� oppure l'attivit� � stata evasa
                  =fputs(this.w_Handle,Ah_MsgFormat("Cancellazione appuntamento")+SPACE(1)+ALLTRIM(NVL(this.oAppuntamento.Subject,""))+SPACE(1)+Ah_MsgFormat("del")+SPACE(1)+TTOC(this.oAppuntamento.Start))
                  = this.oAppuntamento.Delete()
                  this.w_CntDelOU = this.w_CntDelOU + 1
                case EMPTY(this.w_DatMod) OR this.w_DatMod < this.w_DatApp
                  * --- E' pi� recente Outlook, dobbiamo aggiornare OFF_ATTI
                  =fputs(this.w_Handle,Ah_MsgFormat("Aggiornamento attivit�")+SPACE(1)+ALLTRIM(this.w_ATOGGETT)+SPACE(1)+Ah_MsgFormat("del")+SPACE(1)+TTOC(this.w_ATDATINI))
                  this.Page_6()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.w_CntAggAE = this.w_CntAggAE + 1
                case this.w_DatMod > this.w_DatApp
                  * --- E' pi� recente OFF_ATTI, dobbiamo aggiornare Outlook
                  =fputs(this.w_Handle,Ah_MsgFormat("Aggiornamento appuntamento")+SPACE(1)+ALLTRIM(this.w_ATOGGETT)+SPACE(1)+Ah_MsgFormat("del")+SPACE(1)+TTOC(this.w_ATDATINI))
                  this.Page_8()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.w_CntAggOU = this.w_CntAggOU + 1
                otherwise
                  * --- Le due date sono uguali, non facciamo nulla
              endcase
            endif
            * --- Dobbiamo eventualmente cancellare dalla tabella temporanea il codice della scadenza, in modo da non analizzarla per l'export
            * --- Delete from TMPDPOFF
            i_nConn=i_TableProp[this.TMPDPOFF_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPDPOFF_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
                     )
            else
              delete from (i_cTable) where;
                    ATSERIAL = this.w_ATSERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          else
            * --- Appuntamento non di pertinenza
          endif
        else
          * --- Appuntamento non ancora importato
          if this.oParentObject.w_ImportaApp="S"
            * --- Importa nel gestionale l'appuntamento di Outlook
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      enddo
      if this.w_CntAggAE > 0
        =fputs(this.w_Handle,Ah_MsgFormat("Attivit� aggiornate:")+SPACE(1)+ALLTRIM(STR(this.w_CntAggAE)))
      endif
      if this.w_CntAggOU > 0
        =fputs(this.w_Handle,Ah_MsgFormat("Appuntamenti di Outlook aggiornati:")+SPACE(1)+ALLTRIM(STR(this.w_CntAggOU)))
      endif
      if this.w_CntDelOU > 0
        =fputs(this.w_Handle,Ah_MsgFormat("Appuntamenti di Outlook cancellati:")+SPACE(1)+ALLTRIM(STR(this.w_CntDelOU)))
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esporta le attivit� in Oultlook.
    * --- Scandiamo la tabella temporanea ed inseriamo un appuntamento per ciascuna attivit�.
    *     Contiamo se ci sono appuntamenti con codice avvalorato, ossia attivit� gi� esportate, nel qual caso dobbiamo controllare che non sia presente un appuntamento con lo stesso codice.
    *     I codici appena esportati non sono presenti nel temporaneo, ma influiscono ugualmente sul conto.
    * --- Inizialmente ordiniamo per data inizio
    L_Err = .F.
    ON ERROR L_Err=.T.
    this.oAppuntamOutlook.Sort("Start")     
    * --- Gestiamo nella variabile w_DaContr il numero di appuntamenti che ancora ci restano da controllare
    L_Err = .F.
    this.oAttivitaGiaExp = this.oAppuntamOutlook.Restrict("["+MSOUTL_CODICE+"] > 0")
    if !ISNULL(this.oAttivitaGiaExp)
      * --- Nessuna attivit� gi� esportata per l'intervallo selezionato
      this.w_DaContr = this.oAttivitaGiaExp.Count
    else
      * --- Contiamo le attivit� gi� esportate
      this.w_DaContr = 0
    endif
    * --- Adesso tentiamo di ordinare per codice
    L_Err = .F.
    this.oAppuntamOutlook.Sort(MSOUTL_CODICE)     
    ON ERROR &L_OldErr
    this.w_CntAggAE = 0
    this.w_CntAggOU = 0
    this.w_CntDelOU = 0
    this.w_CntCreOU = 0
    this.w_CntAtti = 0
    * --- Select from TMPDPOFF
    i_nConn=i_TableProp[this.TMPDPOFF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDPOFF_idx,2],.t.,this.TMPDPOFF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPDPOFF ";
           ,"_Curs_TMPDPOFF")
    else
      select * from (i_cTable);
        into cursor _Curs_TMPDPOFF
    endif
    if used('_Curs_TMPDPOFF')
      select _Curs_TMPDPOFF
      locate for 1=1
      do while not(eof())
      this.w_CntAtti = this.w_CntAtti + 1
      * --- f !w_ErrOutl
      *            * Procedo solo se non ho errori
      this.w_ATSERIAL = ATSERIAL
      this.w_CARAGGST = NVL(CARAGGST,"")
      this.w_DatMod = NVL(ATDATOUT,CTOD("  -  -  "))
      this.w_ATSTATUS = NVL(ATSTATUS,"")
      this.w_ATDATINI = ATDATINI
      this.w_ATDATFIN = ATDATFIN
      this.w_ATLOCALI = ALLTRIM(NVL(ATLOCALI,""))
      this.w_ATCODPRA = NVL(ATCODPRA, SPACE(15))
      this.w_ATFLPROM = NVL(ATFLPROM,"")
      this.w_ATPROMEM = NVL(ATPROMEM,"")
      this.w_ATNOTPIA = ""
      this.w_SerialToExp = ALLTRIM(i_CodAzi)+"#"+this.w_ATSERIAL
      this.w_ATCAUATT = ATCAUATT
      this.w_ATSTAATT = NVL(ATSTAATT,"")
      this.w_ATPERSON = NVL(ATPERSON,"")
      this.w_ATOGGETT = ALLTRIM(NVL(ATOGGETT,"."))
      this.w_ATCOMRIC = NVL(ATCOMRIC, SPACE(15))
      this.w_ATATTRIC = NVL(ATATTRIC, SPACE(15))
      this.w_ATATTCOS = NVL(ATATTCOS, SPACE(15))
      this.w_ATCENCOS = NVL(ATCENCOS, SPACE(15))
      this.w_ATCENRIC = NVL(ATCENRIC, SPACE(15))
      * --- Abbiamo problemi con le note nella DISTINCT, quindi leggiamo adesso le note dell'attivit�
      *     ed anche AT__ENTE
      * --- Read from OFF_ATTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATNOTPIA,AT__ENTE,ATNUMPRI"+;
          " from "+i_cTable+" OFF_ATTI where ";
              +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATNOTPIA,AT__ENTE,ATNUMPRI;
          from (i_cTable) where;
              ATSERIAL = this.w_ATSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ATNOTPIA = NVL(cp_ToDate(_read_.ATNOTPIA),cp_NullValue(_read_.ATNOTPIA))
        this.w_AT__ENTE = NVL(cp_ToDate(_read_.AT__ENTE),cp_NullValue(_read_.AT__ENTE))
        this.w_ATNUMPRI = NVL(cp_ToDate(_read_.ATNUMPRI),cp_NullValue(_read_.ATNUMPRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_Categoria = ""
      do case
        case this.w_CARAGGST="G"
          * --- Generica
          this.w_Categoria = Ah_MsgFormat("Generica")
        case this.w_CARAGGST="U"
          * --- Udienza
          this.w_Categoria = Ah_MsgFormat("Udienza")
        case this.w_CARAGGST="S"
          * --- Appuntamento
          this.w_Categoria = Ah_MsgFormat("Appuntamento")
        case this.w_CARAGGST="D"
          * --- Cosa da fare
          this.w_Categoria = Ah_MsgFormat("Cosa da fare")
        case this.w_CARAGGST="M"
          * --- Nota
          this.w_Categoria = Ah_MsgFormat("Nota")
        case this.w_CARAGGST="T"
          * --- Sessione telefonica
          this.w_Categoria = Ah_MsgFormat("Sessione telefonica")
        case this.w_CARAGGST="A"
          * --- Assenza
          this.w_Categoria = Ah_MsgFormat("Assenza")
        case this.w_CARAGGST="Z"
          * --- Da inserimento prestazioni
          this.w_Categoria = Ah_MsgFormat("Da inserimento prestazioni")
      endcase
      L_Err = .F.
      ON ERROR L_Err=.T.
      if this.w_DaContr>0
        * --- Erano gi� presenti appuntamenti, devo controllare che l'attivit� non sia gi� stata esportata in precedenza
        this.oAppuntamento = this.oAppuntamOutlook.Find("["+MSOUTL_CODICE+'] = "'+ALLTRIM(this.w_SerialToExp)+'"')
        if L_Err OR ISNULL(this.oAppuntamento)
          * --- L'attivit� non era ancora stata esportata
          =fputs(this.w_Handle,Ah_MsgFormat("Creazione appuntamento")+SPACE(1)+ALLTRIM(this.w_ATOGGETT)+SPACE(1)+Ah_MsgFormat("del")+SPACE(1)+TTOC(this.w_ATDATINI))
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_CntCreOU = this.w_CntCreOU + 1
        else
          * --- L'attivit� era gi� presente e non � stata analizzata nella fase di import
          this.w_DatApp = this.oAppuntamento.LastModificationTime
          * --- In teoria w_DatMod dovrebbe sempre essere avvalorata
          do case
            case !EMPTY(this.w_DatMod) AND this.w_DatMod < this.w_DatApp
              * --- E' pi� recente Outlook, dobbiamo aggiornare OFF_ATTI
              =fputs(this.w_Handle,Ah_MsgFormat("Aggiornamento attivit�")+SPACE(1)+ALLTRIM(this.w_ATOGGETT)+SPACE(1)+Ah_MsgFormat("del")+SPACE(1)+TTOC(this.w_ATDATINI))
              this.Page_6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_CntAggAE = this.w_CntAggAE + 1
            case !EMPTY(this.w_DatMod) AND this.w_DatMod > this.w_DatApp
              * --- E' pi� recente OFF_ATTI, dobbiamo aggiornare Outlook
              =fputs(this.w_Handle,Ah_MsgFormat("Aggiornamento appuntamento")+SPACE(1)+ALLTRIM(this.w_ATOGGETT)+SPACE(1)+Ah_MsgFormat("del")+SPACE(1)+TTOC(this.w_ATDATINI))
              this.Page_8()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_CntAggOU = this.w_CntAggOU + 1
            otherwise
              * --- Le due date sono uguali, non facciamo nulla
          endcase
        endif
      else
        * --- Esportiamo senza controllare l'esistenza di un appuntamento con il codice dell'attivit�
        =fputs(this.w_Handle,Ah_MsgFormat("Creazione appuntamento")+SPACE(1)+ALLTRIM(this.w_ATOGGETT)+SPACE(1)+Ah_MsgFormat("del")+SPACE(1)+TTOC(this.w_ATDATINI))
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_CntCreOU = this.w_CntCreOU + 1
      endif
      ON ERROR &L_OldErr
        select _Curs_TMPDPOFF
        continue
      enddo
      use
    endif
    =fputs(this.w_Handle,Ah_MsgFormat("Attivit� analizzate:")+SPACE(1)+ALLTRIM(STR(this.w_CntAtti)))
    if this.w_CntAggAE > 0
      =fputs(this.w_Handle,Ah_MsgFormat("Attivit� aggiornate:")+SPACE(1)+ALLTRIM(STR(this.w_CntAggAE)))
    endif
    if this.w_CntCreOU > 0
      =fputs(this.w_Handle,Ah_MsgFormat("Appuntamenti creati in Outlook:")+SPACE(1)+ALLTRIM(STR(this.w_CntCreOU)))
    endif
    if this.w_CntAggOU > 0
      =fputs(this.w_Handle,Ah_MsgFormat("Appuntamenti di Outlook aggiornati:")+SPACE(1)+ALLTRIM(STR(this.w_CntAggOU)))
    endif
    if this.w_CntDelOU > 0
      =fputs(this.w_Handle,Ah_MsgFormat("Appuntamenti di Outlook cancellati:")+SPACE(1)+ALLTRIM(STR(this.w_CntDelOU)))
    endif
    ah_Msg("Export completato")
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancella le attivit� evase
    L_Err = .F.
    ON ERROR L_Err=.T.
    this.oAppuntamOutlook.Sort("Start")     
    * --- Tentiamo di ordinare per codice scadenza, se il campo manca viene generato un errore che gestiamo riordinando per data
    this.oAppuntamOutlook.Sort(MSOUTL_CODICE)     
    ON ERROR &L_OldErr
    if this.w_PAFLVISI="L"
      * --- Create temporary table TMPDPOFF
      i_nIdx=cp_AddTableDef('TMPDPOFF') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_FLPOWQBSRY[1]
      indexes_FLPOWQBSRY[1]='ATSERIAL'
      vq_exec('QUERY\GSAG_BEX.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_FLPOWQBSRY,.f.)
      this.TMPDPOFF_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table TMPDPOFF
      i_nIdx=cp_AddTableDef('TMPDPOFF') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_CLPAHEXRBJ[1]
      indexes_CLPAHEXRBJ[1]='ATSERIAL'
      vq_exec('QUERY\GSAG1BEX.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_CLPAHEXRBJ,.f.)
      this.TMPDPOFF_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    this.w_CntDelOU = 0
    * --- Select from TMPDPOFF
    i_nConn=i_TableProp[this.TMPDPOFF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPDPOFF_idx,2],.t.,this.TMPDPOFF_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPDPOFF ";
           ,"_Curs_TMPDPOFF")
    else
      select * from (i_cTable);
        into cursor _Curs_TMPDPOFF
    endif
    if used('_Curs_TMPDPOFF')
      select _Curs_TMPDPOFF
      locate for 1=1
      do while not(eof())
      this.w_SerialToExp = ALLTRIM(i_CodAzi)+"#"+ATSERIAL
      L_Err = .F.
      this.oAppuntamento = this.oAppuntamOutlook.Find("["+MSOUTL_CODICE+'] = "'+ALLTRIM(this.w_SerialToExp)+'"')
      if L_Err OR ISNULL(this.oAppuntamento)
        * --- L'attivit� non � mai stata esportata oppure non � pi� presente in Outlook
      else
        * --- Procede con la cancellazione
        this.w_DataAppToDel = this.oAppuntamento.Start
        =fputs(this.w_Handle,Ah_MsgFormat("Cancellazione appuntamento")+SPACE(1)+ALLTRIM(NVL(this.oAppuntamento.Subject,""))+SPACE(1)+Ah_MsgFormat("del")+SPACE(1)+TTOC(this.oAppuntamento.Start))
        = this.oAppuntamento.Delete()
        if !L_Err
          * --- Cancellazione riuscita
          this.w_CntDelOU = this.w_CntDelOU + 1
        endif
      endif
        select _Curs_TMPDPOFF
        continue
      enddo
      use
    endif
    * --- Drop temporary table TMPDPOFF
    i_nIdx=cp_GetTableDefIdx('TMPDPOFF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDPOFF')
    endif
    if this.w_CntDelOU > 0
      =fputs(this.w_Handle,Ah_MsgFormat("Appuntamenti di Outlook cancellati:")+SPACE(1)+ALLTRIM(STR(this.w_CntDelOU)))
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea un'attivit� in OFF_ATTI.
    *     Per inserire un appuntamento nel DB chiamiamo il batch GSAG_BAG
    if this.oAppuntamento.Sensitivity=0
      * --- Importiamo solo gli appuntamenti non marcati come privati
      this.w_DataApp = TTOD(this.oAppuntamento.Start)
      this.w_DataAppFin = TTOD(this.oAppuntamento.End)
      * --- OraStart e OraEnd rappresentano il numero di minuti dalla mezzanotte
      this.w_OraStart = HOUR(this.oAppuntamento.Start)*60 + MINUTE(this.oAppuntamento.Start)
      this.w_OraEnd = HOUR(this.oAppuntamento.End)*60 + MINUTE(this.oAppuntamento.End)
      this.w_TxtSel = this.oAppuntamento.Subject
      this.w_Serial = ""
      this.w_CauDefault = this.oParentObject.w_Causale
      if Not Empty(this.w_DataApp) and Not EMpty(this.w_DataAppFin)
        this.w_CODPART = this.oParentObject.w_CodPartApp
        this.w_TIPPART = this.oParentObject.w_TipPartApp
        GSAG_BAG(this,"ADDDB")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Dobbiamo inserire il seriale nella user property dell'appuntamento
      if !EMPTY(this.w_SERIAL)
        this.oAppuntamento.UserProperties.Add(MSOUTL_CODICE, olText)     
        this.oAppuntamento.UserProperties(MSOUTL_CODICE).Value = ALLTRIM(i_CodAzi)+"#"+this.w_SERIAL
        this.oAppuntamento.Save()     
        =fputs(this.w_Handle,Ah_MsgFormat("Inserita attivit�")+SPACE(1)+ALLTRIM(NVL(this.oAppuntamento.Subject,""))+SPACE(1)+Ah_MsgFormat("del")+SPACE(1)+TTOC(this.oAppuntamento.Start))
        * --- Dichiariamo le variabili in modo che non soddisfino i requisiti di pag. 6
        this.w_ATNUMPRI = 0
        this.w_ATSTAATT = 0
        this.w_ATSERIAL = this.w_Serial
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna OFF_ATTI.
    *     Per farlo chiamiamo il batch GSAG_BAG, che gestisce anche l'eventuale ITER
    this.w_Serial = this.w_ATSERIAL
    * --- Gestire w_TxtSel
    this.w_TxtSel = this.oAppuntamento.Subject
    this.w_DataApp = TTOD(this.oAppuntamento.Start)
    this.w_DataAppFin = TTOD(this.oAppuntamento.End)
    * --- OraStart e OraEnd rappresentano il numero di minuti dalla mezzanotte
    this.w_OraStart = HOUR(this.oAppuntamento.Start)*60 + MINUTE(this.oAppuntamento.Start)
    this.w_OraEnd = HOUR(this.oAppuntamento.End)*60 + MINUTE(this.oAppuntamento.End)
    GSAG_BAG(this,"MODIFICADAOUTLOOK")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Gestiamo anche il promemoria: in GSAG_BAG � shiftato
    * --- Dobbiamo forzare nell'attivit� l'ora di ultima modifica di Outlook in modo che alla prossima sincronizzazione siano uguali
    if this.oAppuntamento.ReminderSet
      * --- Gestione promemoria
      this.w_ATFLPROM = "S"
      * --- La differenza tra date utilizza i secondi, quindi dobbiamo moltiplicare per 60 i minuti
      this.w_ATPROMEM = this.oAppuntamento.Start - (this.oAppuntamento.ReminderMinutesBeforeStart*60)
    else
      * --- Nessun promemoria
      this.w_ATFLPROM = "N"
      this.w_ATPROMEM = DTOT(CTOD("  -  -  "))
    endif
    this.w_ATLOCALI = this.oAppuntamento.Location
    this.w_NoteApp = this.oAppuntamento.Body
    if AT(this.w_TokenNote, this.w_NoteApp)>0
      this.w_NoteApp = ALLTRIM( SUBSTR(this.w_NoteApp, AT(this.w_TokenNote, this.w_NoteApp)+LEN(this.w_TokenNote)) )
    endif
    * --- Nel caso in cui nel corpo dell'appuntamento NON ci fossero note, NON gestiamo il campo nella write.
    *     La gestione tramite IF ci risparmia di leggere i dati dell'attivit�
    * --- BusyStatus: 0 Free, 1 Tentative, 2 Busy, 3 Out of Office
    *     ATSTATT: Occupato - 2, Fuori sede - 4, Per urgenze - 3, Libero - 1
    do case
      case this.oAppuntamento.BusyStatus=0
        * --- Libero
        this.w_NEWSTAATT = 1
      case this.oAppuntamento.BusyStatus=1
        * --- Tentative - Provvisorio
        this.w_NEWSTAATT = 3
      case this.oAppuntamento.BusyStatus=2
        * --- Occupato
        this.w_NEWSTAATT = 2
      case this.oAppuntamento.BusyStatus=3
        * --- Fuori ufficio
        this.w_NEWSTAATT = 4
      otherwise
        * --- Non previsto ma comunque gestito
        this.w_NEWSTAATT = 1
    endcase
    if this.w_ATSTAATT=3 AND this.oAppuntamento.BusyStatus=2
      * --- Attivit� "Per urgenze" e appuntamento "Occupato"
      *     Reimposto lo stato a "Per urgenze"
      this.w_NEWSTAATT = 3
    endif
    * --- 0 Low importance
    *     1 Normal importance
    *     2 High importance
    if this.oAppuntamento.Importance=2
      * --- Urgente
      this.w_NEWNUMPRI = 3
    else
      * --- Normale
      this.w_NEWNUMPRI = 1
    endif
    if this.w_ATNUMPRI=4 AND this.oAppuntamento.Importance=2
      * --- Attivit� "Scadenza termine" e appuntamento "High importance"
      *     Reimposto la priorit� a "Scadenza termine"
      this.w_NEWNUMPRI = 4
    endif
    this.w_DatApp = this.oAppuntamento.LastModificationTime
    if !EMPTY(this.w_NoteApp)
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATOUT ="+cp_NullLink(cp_ToStrODBC(this.w_DatApp),'OFF_ATTI','ATDATOUT');
        +",ATFLPROM ="+cp_NullLink(cp_ToStrODBC(this.w_ATFLPROM),'OFF_ATTI','ATFLPROM');
        +",ATPROMEM ="+cp_NullLink(cp_ToStrODBC(this.w_ATPROMEM),'OFF_ATTI','ATPROMEM');
        +",ATLOCALI ="+cp_NullLink(cp_ToStrODBC(this.w_ATLOCALI),'OFF_ATTI','ATLOCALI');
        +",ATSTAATT ="+cp_NullLink(cp_ToStrODBC(this.w_NEWSTAATT),'OFF_ATTI','ATSTAATT');
        +",ATNUMPRI ="+cp_NullLink(cp_ToStrODBC(this.w_NEWNUMPRI),'OFF_ATTI','ATNUMPRI');
        +",ATNOTPIA ="+cp_NullLink(cp_ToStrODBC(this.w_NoteApp),'OFF_ATTI','ATNOTPIA');
            +i_ccchkf ;
        +" where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
               )
      else
        update (i_cTable) set;
            ATDATOUT = this.w_DatApp;
            ,ATFLPROM = this.w_ATFLPROM;
            ,ATPROMEM = this.w_ATPROMEM;
            ,ATLOCALI = this.w_ATLOCALI;
            ,ATSTAATT = this.w_NEWSTAATT;
            ,ATNUMPRI = this.w_NEWNUMPRI;
            ,ATNOTPIA = this.w_NoteApp;
            &i_ccchkf. ;
         where;
            ATSERIAL = this.w_ATSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATOUT ="+cp_NullLink(cp_ToStrODBC(this.w_DatApp),'OFF_ATTI','ATDATOUT');
        +",ATFLPROM ="+cp_NullLink(cp_ToStrODBC(this.w_ATFLPROM),'OFF_ATTI','ATFLPROM');
        +",ATPROMEM ="+cp_NullLink(cp_ToStrODBC(this.w_ATPROMEM),'OFF_ATTI','ATPROMEM');
        +",ATLOCALI ="+cp_NullLink(cp_ToStrODBC(this.w_ATLOCALI),'OFF_ATTI','ATLOCALI');
        +",ATSTAATT ="+cp_NullLink(cp_ToStrODBC(this.w_NEWSTAATT),'OFF_ATTI','ATSTAATT');
        +",ATNUMPRI ="+cp_NullLink(cp_ToStrODBC(this.w_NEWNUMPRI),'OFF_ATTI','ATNUMPRI');
            +i_ccchkf ;
        +" where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
               )
      else
        update (i_cTable) set;
            ATDATOUT = this.w_DatApp;
            ,ATFLPROM = this.w_ATFLPROM;
            ,ATPROMEM = this.w_ATPROMEM;
            ,ATLOCALI = this.w_ATLOCALI;
            ,ATSTAATT = this.w_NEWSTAATT;
            ,ATNUMPRI = this.w_NEWNUMPRI;
            &i_ccchkf. ;
         where;
            ATSERIAL = this.w_ATSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce in Outlook un appuntamento, gestendo solo il codice.
    *     Per gli altri dati si utilizza la fase di aggiornamento
    this.oAppuntamento = this.oAppuntamOutlook.Add(olAppointmentItem)
    this.oAppuntamento.UserProperties.Add(MSOUTL_CODICE, olText)     
    this.oAppuntamento.UserProperties(MSOUTL_CODICE).Value = this.w_SerialToExp
    this.oAppuntamento.Save()     
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna un appuntamento in Outlook.
    *     Abbiamo in w_ATSERIAL il seriale dell'attivit� ed ha bisogno delle seguenti variabili:
    *     w_ATDATINI, w_ATDATFIN, w_ATLOCALI, w_ATFLPROM, w_ATPROMEM, w_ATSTAATT
    this.Page_11()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.oAppuntamento.Start = this.w_ATDATINI
    this.oAppuntamento.End = this.w_ATDATFIN
    this.oAppuntamento.Subject = this.w_OggApp
    if !EMPTY(this.w_ATLOCALI)
      this.oAppuntamento.Location = this.w_ATLOCALI
    endif
    * --- Nessun promemoria
    this.oAppuntamento.ReminderSet = .F.
    this.oAppuntamento.ReminderMinutesBeforeStart = 0
    if !EMPTY(this.w_ATFLPROM) AND this.w_ATFLPROM=="S" AND this.w_ATDATINI>=this.w_ATPROMEM
      * --- Gestiamo il promemoria in Outlook: l'unit� di misura � i minuti mentre noi lo calcoliamo in secondi
      this.oAppuntamento.ReminderMinutesBeforeStart = (this.w_ATDATINI-this.w_ATPROMEM) / 60
      * --- ReminderSet � forzato a .f. da Outlook nel caso in cui il promemoria sia antecedente la data/ora di sistema
      this.oAppuntamento.ReminderSet = .T.
    endif
    this.oAppuntamento.Categories = ""
    this.oAppuntamento.Categories = this.w_Categoria
    this.oAppuntamento.Body = this.w_TextApp
    * --- BusyStatus: 0 Free, 1 Tentative, 2 Busy, 3 Out of Office
    *     ATSTATT: Occupato - 2, Fuori sede - 4, Per urgenze - 3, Libero - 1
    do case
      case this.w_ATSTAATT=1
        * --- Libero
        this.oAppuntamento.BusyStatus = 0
      case this.w_ATSTAATT=2
        * --- Occupato
        this.oAppuntamento.BusyStatus = 2
      case this.w_ATSTAATT=3
        * --- Per urgenze
        *     Lo stato 1 significa "Provvisorio", quindi anche in questo caso mettiamo "Occupato"
        this.oAppuntamento.BusyStatus = 2
      case this.w_ATSTAATT=4
        * --- Fuori sede
        this.oAppuntamento.BusyStatus = 3
    endcase
    * --- Importance: 0 Low importance, 1 Normal importance, 2 High importance
    *     ATNUMPRI: Normale - 1, Urgente - 3, Scadenza termine - 4
    if this.w_ATNUMPRI=1
      this.oAppuntamento.Importance = 1
    else
      this.oAppuntamento.Importance = 2
    endif
    this.oAppuntamento.Save()     
    * --- Dobbiamo forzare nell'attivit� l'ora di ultima modifica di Outlook in modo che alla prossima sincronizzazione siano uguali
    this.w_DatApp = this.oAppuntamento.LastModificationTime
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATDATOUT ="+cp_NullLink(cp_ToStrODBC(this.w_DatApp),'OFF_ATTI','ATDATOUT');
          +i_ccchkf ;
      +" where ";
          +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
             )
    else
      update (i_cTable) set;
          ATDATOUT = this.w_DatApp;
          &i_ccchkf. ;
       where;
          ATSERIAL = this.w_ATSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione istanza Outlook e apertura log
    * --- * potrei includere  #INCLUDE "MSOutl9.H" ma servono solo poche definizioni
    * --- Gestione errori: dobbiamo gestire L_OldErr come statement perch� la ON ERROR segnala errore con l'espansione di macro
    L_Err = .F.
    ON ERROR L_Err=.T.
    * --- Creo una sessione di Outlook
    this.loOutlook = CREATEOBJECT("Outlook.Application")
    if L_Err OR ISNULL(this.loOutlook)
      * --- Non ha creato l'istanza
      ON ERROR &L_OldErr
      ah_ErrorMsg("Errore nella creazione di un'istanza di Outlook")
      i_retcode = 'stop'
      return
    endif
    * --- * Inizializzo il NameSpace
    this.loNamespace = this.loOutlook.GetNamespace("MAPI")
    * --- * Individuo la cartella di default del calendario
    this.loDefaultFol = this.loNamespace.GetDefaultFolder( olFolderCalendar )
    if L_Err OR ISNULL(this.loDefaultFol)
      * --- Non � stato scelto un profilo
      ON ERROR &L_OldErr
      ah_ErrorMsg("Impossibile aprire Outlook!")
      i_retcode = 'stop'
      return
    endif
    this.oAppuntamOutlook = this.loDefaultFol.Items
    ON ERROR &L_OldErr
    * --- Creazione log errori
    this.w_HANDLE = FCREATE(this.w_NomeFileLog)
    =fputs(this.w_Handle,Ah_MsgFormat("Ora inizio")+SPACE(1)+TTOC(this.w_DatApp))
    =fputs(this.w_Handle,"")
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura log
    * --- Drop temporary table TMPDPOFF
    i_nIdx=cp_GetTableDefIdx('TMPDPOFF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDPOFF')
    endif
    ah_Msg("Operazione completata")
    =fputs(this.w_Handle,"")
    =fputs(this.w_Handle,Ah_MsgFormat("Operazione conclusa")+SPACE(1)+TTOC(datetime()))
    =fclose(this.w_Handle)
    ON ERROR &L_OldErr
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Composizione oggetto e note
    this.w_PartSerial = this.w_ATSERIAL
    this.w_TextPart = ""
    this.w_TextRis = ""
    this.w_TextGru = ""
    this.w_OggApp = ""
    this.w_Giudici = ""
    this.w_Locali_Atti = ""
    this.w_EPDESCRI = ""
    if this.pAzione=="COMPONINOTE"
      if this.pFormatoNote="I"
        * --- ICS
        this.w_RitornoCarrello = "\n"
      else
        * --- VCS
        this.w_RitornoCarrello = "=0D=0A"
      endif
    else
      this.w_Locali_Atti = this.w_ATLOCALI
      this.w_RitornoCarrello = CHR(13)+CHR(10)
      * --- Stiamo sincronizzando, abbiamo AT__ENTE tra i dati disponibili
      if !EMPTY(this.w_AT__ENTE)
        * --- Descrizione dell'autorit�
        * --- Read from PRA_ENTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2],.t.,this.PRA_ENTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "EPDESCRI"+;
            " from "+i_cTable+" PRA_ENTI where ";
                +"EPCODICE = "+cp_ToStrODBC(this.w_AT__ENTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            EPDESCRI;
            from (i_cTable) where;
                EPCODICE = this.w_AT__ENTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_EPDESCRI = NVL(cp_ToDate(_read_.EPDESCRI),cp_NullValue(_read_.EPDESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    this.w_DesPraAttivita = ""
    this.w_CNRIFGIU = ""
    this.w_DataIni = TTOD(this.w_ATDATINI)
    this.w_DataFin = TTOD(this.w_ATDATFIN)
    this.w_OraIni = PADL(ALLTRIM(STR(HOUR(this.w_ATDATINI))),2,"0")+":"++PADL(ALLTRIM(STR(MINUTE(this.w_ATDATINI))),2,"0")
    this.w_OraFin = PADL(ALLTRIM(STR(HOUR(this.w_ATDATFIN))),2,"0")+":"++PADL(ALLTRIM(STR(MINUTE(this.w_ATDATFIN))),2,"0")
    * --- Select from QUERY\GSAG_BAP.VQR
    do vq_exec with 'QUERY\GSAG_BAP.VQR',this,'_Curs_QUERY_GSAG_BAP_d_VQR','',.f.,.t.
    if used('_Curs_QUERY_GSAG_BAP_d_VQR')
      select _Curs_QUERY_GSAG_BAP_d_VQR
      locate for 1=1
      do while not(eof())
      do case
        case DPTIPRIS="P"
          this.w_TextPart = this.w_TextPart+ ALLTRIM( ALLTRIM(NVL(DPCOGNOM,""))+" "+ALLTRIM(NVL(DPNOME,"")) )+" -"+SPACE(1)
        case DPTIPRIS="R"
          this.w_TextRis = this.w_TextRis+ALLTRIM(DPDESCRI)+" -"+SPACE(1)
        case DPTIPRIS="G"
          this.w_TextGru = this.w_TextGru+ALLTRIM(DPDESCRI)+" -"+SPACE(1)
      endcase
        select _Curs_QUERY_GSAG_BAP_d_VQR
        continue
      enddo
      use
    endif
    * --- Inseriamo i partecipanti nelle note
    if !EMPTY(this.w_TextPart)
      * --- Partecipanti
      this.w_TextPart = SUBSTR(this.w_TextPart,1, LEN(this.w_TextPart)-2 )
    endif
    if !EMPTY(this.w_TextRis)
      * --- Risorse
      this.w_TextRis = SUBSTR(this.w_TextRis,1, LEN(this.w_TextRis)-2 )
    endif
    if !EMPTY(this.w_TextGru)
      * --- Gruppi
      this.w_TextGru = SUBSTR(this.w_TextGru,1, LEN(this.w_TextGru)-2 )
    endif
    if !EMPTY(this.w_ATCODPRA)
      * --- Pratica e descrizione
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNDESCAN,CNRIFGIU"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.w_ATCODPRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNDESCAN,CNRIFGIU;
          from (i_cTable) where;
              CNCODCAN = this.w_ATCODPRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DesPraAttivita = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
        this.w_CNRIFGIU = NVL(cp_ToDate(_read_.CNRIFGIU),cp_NullValue(_read_.CNRIFGIU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggiamo i giudici della pratica
      this.w_Giudici = GetGiudici(this.w_ATCODPRA, "", i_DatSys, 250, "")
    else
      this.w_Giudici = ""
    endif
    * --- Oggetto e messaggio di default
    this.w_OggApp = this.w_OggettoAppuntamento
    this.w_TextApp = ""
    if Not Empty(this.w_ATPERSON)
      this.w_TextApp = this.w_TextApp+"Nominativo: <NOMIN> <RITCAR>"
    endif
    if !EMPTY(this.w_TextPart)
      this.w_TextApp = this.w_TextApp+"Partecipanti: <PERSONE> <RITCAR>"
    endif
    if !EMPTY(this.w_TextRis)
      this.w_TextApp = this.w_TextApp+"Risorse: <RISORSE> <RITCAR>"
    endif
    if !EMPTY(this.w_TextGru)
      this.w_TextApp = this.w_TextApp+"Gruppi: <GRUPPI> <RITCAR>"
    endif
    if Isahe()
      this.w_TextApp = this.w_TextApp+Ah_MsgFormat("Commessa costi:")+" <CODPRA>  -  <DESPRA> <RITCAR>"
      if !EMPTY(this.w_ATCODPRA)
        if !EMPTY(this.w_ATATTCOS)
          * --- Attivit� e descrizione
          * --- Read from ATTIVITA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ATDESCRI"+;
              " from "+i_cTable+" ATTIVITA where ";
                  +"ATCODCOM = "+cp_ToStrODBC(this.w_ATCODPRA);
                  +" and ATTIPATT = "+cp_ToStrODBC("A");
                  +" and ATCODATT = "+cp_ToStrODBC(this.w_ATATTCOS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ATDESCRI;
              from (i_cTable) where;
                  ATCODCOM = this.w_ATCODPRA;
                  and ATTIPATT = "A";
                  and ATCODATT = this.w_ATATTCOS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESATC = NVL(cp_ToDate(_read_.ATDESCRI),cp_NullValue(_read_.ATDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_TextApp = this.w_TextApp+Ah_MsgFormat("Attivit� costi:") +" <ATTCOS>  -  <DESATC> <RITCAR>"
        endif
      endif
      if !EMPTY(this.w_ATCENCOS)
        * --- Centro di costo e descrizione
        * --- Read from CENCOST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CENCOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2],.t.,this.CENCOST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCDESPIA"+;
            " from "+i_cTable+" CENCOST where ";
                +"CC_CONTO = "+cp_ToStrODBC(this.w_ATCENCOS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCDESPIA;
            from (i_cTable) where;
                CC_CONTO = this.w_ATCENCOS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESCEC = NVL(cp_ToDate(_read_.CCDESPIA),cp_NullValue(_read_.CCDESPIA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_TextApp = this.w_TextApp+Ah_MsgFormat("Centro di costo:")+" <CENCOS>  -  <DESCEC> <RITCAR>"
      endif
      if !EMPTY(this.w_ATCOMRIC)
        * --- Commessa ricavi e descrizione
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNDESCAN"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_ATCOMRIC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNDESCAN;
            from (i_cTable) where;
                CNCODCAN = this.w_ATCOMRIC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DesCoR = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_TextApp = this.w_TextApp+Ah_MsgFormat("Commessa ricavi:")+" <COMRIC>  -  <DESCOR> <RITCAR>"
        if !EMPTY(this.w_ATATTRIC)
          * --- attivit� e descrizione
          * --- Read from ATTIVITA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ATDESCRI"+;
              " from "+i_cTable+" ATTIVITA where ";
                  +"ATCODCOM = "+cp_ToStrODBC(this.w_ATCOMRIC);
                  +" and ATTIPATT = "+cp_ToStrODBC("A");
                  +" and ATCODATT = "+cp_ToStrODBC(this.w_ATATTRIC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ATDESCRI;
              from (i_cTable) where;
                  ATCODCOM = this.w_ATCOMRIC;
                  and ATTIPATT = "A";
                  and ATCODATT = this.w_ATATTRIC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESATR = NVL(cp_ToDate(_read_.ATDESCRI),cp_NullValue(_read_.ATDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_TextApp = this.w_TextApp+Ah_MsgFormat("Attivit� ricavi:")+" <ATTRIC>  -  <DESATR> <RITCAR>"
        endif
      endif
      if !EMPTY(this.w_ATCENRIC)
        * --- Centro di ricavo e descrizione
        * --- Read from CENCOST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CENCOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2],.t.,this.CENCOST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCDESPIA"+;
            " from "+i_cTable+" CENCOST where ";
                +"CC_CONTO = "+cp_ToStrODBC(this.w_ATCENRIC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCDESPIA;
            from (i_cTable) where;
                CC_CONTO = this.w_ATCENRIC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESCER = NVL(cp_ToDate(_read_.CCDESPIA),cp_NullValue(_read_.CCDESPIA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_TextApp = this.w_TextApp+Ah_MsgFormat("Centro di ricavo:")+" <CENRIC>  -  <DESCER> <RITCAR>"
      endif
    else
      if Not Empty(this.w_ATCODPRA)
        this.w_TextApp = this.w_TextApp+IIF(ISALT(),"Pratica:","Commessa:")+" <CODPRA> <DESPRA> <RITCAR>"
      endif
    endif
    if ISALT()
      if Not Empty(this.w_CNRIFGIU)
        this.w_TextApp = this.w_TextApp+"Ruolo: <RUOLO> <RITCAR>"
      endif
      if Not Empty(this.w_giudici)
        this.w_TextApp = this.w_TextApp+"Giudici: <GIUDICI> <RITCAR>"
      endif
    endif
    this.w_TextApp = this.w_TextApp+"<IMPIANTI>"
    if Not Empty(this.w_ATNOTPIA)
      this.w_TextApp = this.w_TextApp+this.w_TokenNote+" <NOTPIA> <RITCAR>"
    endif
    * --- Gestiamo anche impianti e componenti 
    this.w_Impianti = ""
    * --- Read from COM_ATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.COM_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COM_ATTI_idx,2],.t.,this.COM_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODIMP"+;
        " from "+i_cTable+" COM_ATTI where ";
            +"CASERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODIMP;
        from (i_cTable) where;
            CASERIAL = this.w_ATSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODIMP = NVL(cp_ToDate(_read_.CACODIMP),cp_NullValue(_read_.CACODIMP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if !EMPTY(this.w_CODIMP)
      * --- Select from QUERY\GSAG3BEX.VQR
      do vq_exec with 'QUERY\GSAG3BEX.VQR',this,'_Curs_QUERY_GSAG3BEX_d_VQR','',.f.,.t.
      if used('_Curs_QUERY_GSAG3BEX_d_VQR')
        select _Curs_QUERY_GSAG3BEX_d_VQR
        locate for 1=1
        do while not(eof())
        this.w_NoteCodiceImp = NVL(CACODIMP,"")
        this.w_NoteDescrImp = NVL(IMDESCRI,"")
        this.w_NoteCodiceCmp = NVL(CACODCOM,0)
        this.w_NoteDescrCmp = NVL(DESCON,"")
        * --- " - "+Ah_MsgFormat("Componente:")+SPACE(1)+ALLTRIM(GSAG3BEX->DESCON),"")+CHR(13)+CHR(10)
        this.w_Impianti = this.w_Impianti+Ah_MsgFormat("Impianto:")+SPACE(1)+ALLTRIM(this.w_NoteCodiceImp)+SPACE(1)+ALLTRIM(this.w_NoteDescrImp)+IIF(this.w_NoteCodiceCmp>0," - "+Ah_MsgFormat("Componente:")+SPACE(1)+ALLTRIM(this.w_NoteDescrCmp),"")+" <RITCAR>"
          select _Curs_QUERY_GSAG3BEX_d_VQR
          continue
        enddo
        use
      endif
    endif
    * --- Creazione cursore per preparazione messaggio per invio mail e post-in
    if this.pAzione=="COMPONINOTE"
      * --- In questo caso il ritorno carrello "=0D=0A" � lungo ben 6 caratteri
      CREATE CURSOR APPOMESS ;
      (DATAINI D(8), ORAINI C(5), DATAFIN D(8), ORAFIN C(5), OGGETT C(254), NOTPIA M(10), CODPRA C(15), DESPRA C(100), PERSONE M(10), RISORSE M(10), RITCAR C(6), RUOLO C(30), GIUDICI C(254), CAUATT C(20), IMPIANTI M(10), NOMIN C(60), GRUPPI C(254),COMRIC C(15), ATTCOS C(15), ATTRIC C(15) ,CENCOS C(15),CENRIC C(15),; 
 DESATC C(30),DESATR C(30),DESCEC C(40),DESCER C(40),DESCOR C(100), ATLOCALI C(30), EPDESCRI C(60))
    else
      CREATE CURSOR APPOMESS ;
      (DATAINI D(8), ORAINI C(5), DATAFIN D(8), ORAFIN C(5), OGGETT C(254), NOTPIA M(10), CODPRA C(15), DESPRA C(100), PERSONE M(10), RISORSE M(10), RITCAR C(2), RUOLO C(30), GIUDICI C(254), CAUATT C(20), IMPIANTI M(10), NOMIN C(60), GRUPPI C(254),COMRIC C(15), ATTCOS C(15), ATTRIC C(15) ,CENCOS C(15),CENRIC C(15),; 
 DESATC C(30),DESATR C(30),DESCEC C(40),DESCER C(40),DESCOR C(100), ATLOCALI C(30), EPDESCRI C(60))
    endif
    * --- Inserimento nel cursore
     
 INSERT INTO APPOMESS (DATAINI, ORAINI, DATAFIN, ORAFIN, OGGETT, NOTPIA, CODPRA, DESPRA, PERSONE, RISORSE, RITCAR, RUOLO, GIUDICI, CAUATT, IMPIANTI, NOMIN, GRUPPI,COMRIC,; 
 ATTRIC,ATTCOS,CENRIC,CENCOS,DESATC,DESATR,DESCEC,DESCER,DESCOR, ATLOCALI, EPDESCRI) VALUES;
    (this.w_DATAINI, this.w_ORAINI, this.w_DATAFIN, this.w_ORAFIN, this.w_ATOGGETT, this.w_ATNOTPIA, this.w_ATCODPRA, this.w_DesPraAttivita, this.w_TextPart, this.w_TextRis, this.w_RitornoCarrello, this.w_CNRIFGIU, this.w_Giudici,; 
 this.w_ATCAUATT, this.w_Impianti, this.w_ATPERSON, this.w_TextGru,this.w_ATCOMRIC,this.w_ATATTRIC,this.w_ATATTCOS,this.w_ATCENRIC,this.w_ATCENCOS,this.w_DESATC,this.w_DESATR,this.w_DESCEC,this.w_DESCER,this.w_DESCOR, this.w_Locali_Atti, this.w_EPDESCRI)
    this.w_OggApp = CUR_TO_MSG( "APPOMESS" , "" ,"" , this.w_OggApp )
    this.w_TextApp = CUR_TO_MSG( "APPOMESS" , "" , this.w_TextApp, "" )
    USE IN SELECT("APPOMESS")
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAzione,pSerialVCS,pCodPraVCS,pDatIniVCS,pDatFinVCS,pOggettVCS,pCauAttVCS,pPersonaVCS,pNoteVCS,pFormatoNote)
    this.pAzione=pAzione
    this.pSerialVCS=pSerialVCS
    this.pCodPraVCS=pCodPraVCS
    this.pDatIniVCS=pDatIniVCS
    this.pDatFinVCS=pDatFinVCS
    this.pOggettVCS=pOggettVCS
    this.pCauAttVCS=pCauAttVCS
    this.pPersonaVCS=pPersonaVCS
    this.pNoteVCS=pNoteVCS
    this.pFormatoNote=pFormatoNote
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='*TMPDPOFF'
    this.cWorkTables[2]='OFF_ATTI'
    this.cWorkTables[3]='PAR_AGEN'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='COM_ATTI'
    this.cWorkTables[6]='ATTIVITA'
    this.cWorkTables[7]='CENCOST'
    this.cWorkTables[8]='IMP_DETT'
    this.cWorkTables[9]='IMP_MAST'
    this.cWorkTables[10]='PRA_ENTI'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_TMPDPOFF')
      use in _Curs_TMPDPOFF
    endif
    if used('_Curs_TMPDPOFF')
      use in _Curs_TMPDPOFF
    endif
    if used('_Curs_QUERY_GSAG_BAP_d_VQR')
      use in _Curs_QUERY_GSAG_BAP_d_VQR
    endif
    if used('_Curs_QUERY_GSAG3BEX_d_VQR')
      use in _Curs_QUERY_GSAG3BEX_d_VQR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione,pSerialVCS,pCodPraVCS,pDatIniVCS,pDatFinVCS,pOggettVCS,pCauAttVCS,pPersonaVCS,pNoteVCS,pFormatoNote"
endproc
