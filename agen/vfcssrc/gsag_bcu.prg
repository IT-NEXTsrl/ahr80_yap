* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bcu                                                        *
*              Controlla univocit� prestazioni                                 *
*                                                                              *
*      Author: Zucchetti TAM S.p.a.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_66]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-20                                                      *
* Last revis.: 2011-05-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bcu",oParentObject)
return(i_retval)

define class tgsag_bcu as StdBatch
  * --- Local variables
  w_CODPRA = space(15)
  w_DESPRA = space(100)
  w_CODART = space(20)
  w_DESATT = space(40)
  w_ORDINA = space(1)
  w_CODATT = space(20)
  w_FLUNIV = space(1)
  w_NUMPRE = 0
  w_DATINI = ctod("  /  /  ")
  w_OCCORR = 0
  w_INSERI = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo univocit� prestazioni (lanciato da GSAG_SCU.MSKDEF)
    * --- Codice pratica da controllare
    * --- Codice prestazione da controllare
    * --- Variabile per inserimento righe nel cursore
    this.w_CODPRA = ""
    this.w_DESPRA = ""
    this.w_CODART = ""
    this.w_DESATT = ""
    this.w_ORDINA = ""
    this.w_CODATT = ""
    this.w_FLUNIV = ""
    this.w_NUMPRE = 0
    this.w_DATINI = CTOD("  -  -  ")
    this.w_OCCORR = 0
    this.w_INSERI = "N"
    CREATE CURSOR __tmp__ (CODPRA C(15), DESPRA C(100), CODART C(20), DESATT C(40), ORDINA C(1), CODATT C(20), NUMPRE N(4), DATINI D(8), OCCORR N(4))
    * --- Select from gsag_scu
    do vq_exec with 'gsag_scu',this,'_Curs_gsag_scu','',.f.,.t.
    if used('_Curs_gsag_scu')
      select _Curs_gsag_scu
      locate for 1=1
      do while not(eof())
      this.w_CODPRA = CODPRA
      this.w_DESPRA = DESPRA
      this.w_CODART = CODART
      this.w_DESATT = DESATT
      this.w_ORDINA = ORDINA
      this.w_CODATT = CODATT
      this.w_FLUNIV = FLUNIV
      this.w_NUMPRE = NUMPRE
      this.w_DATINI = TTOD(DATINI)
      this.w_OCCORR = OCCORR
      if this.w_ORDINA="A"
        * --- Se w_ORDINA='A' la riga viene dalla query (raggruppata) GSAG1SCU
        if this.w_OCCORR>this.w_NUMPRE
          * --- Se il numero di occorrenze � maggiore del massimo previsto
          *     inserisco la riga relativa al codice articolo
          *     e, tramite la variabile w_INSERI, verranno inserite tutte le righe relative al codice di ricerca con relativa data di inizio attivit�
          INSERT INTO __tmp__ (CODPRA, DESPRA,CODART,DESATT,ORDINA,CODATT,NUMPRE,DATINI,OCCORR) VALUES(this.w_CODPRA,this.w_DESPRA, this.w_CODART,this.w_DESATT,this.w_ORDINA,this.w_CODATT,this.w_NUMPRE,this.w_DATINI,this.w_OCCORR)
          this.w_INSERI = "S"
        else
          * --- Se il numero di occorrenze non � maggiore del massimo previsto
          *     non inserisco la riga relativa al codice articolo
          *     e, tramite la variabile w_INSERI, non verranno inserite tutte le righe relative al codice di ricerca con relativa data di inizio attivit�
          this.w_INSERI = "N"
        endif
      else
        * --- Se w_ORDINA='B' la riga viene dalla query (dettagliata) GSAG_SCU
        if this.w_INSERI="S"
          INSERT INTO __tmp__ (CODPRA, DESPRA,CODART,DESATT,ORDINA,CODATT,NUMPRE,DATINI,OCCORR) VALUES(this.w_CODPRA,this.w_DESPRA, this.w_CODART,this.w_DESATT,this.w_ORDINA,this.w_CODATT,this.w_NUMPRE,this.w_DATINI,this.w_OCCORR)
        endif
      endif
        select _Curs_gsag_scu
        continue
      enddo
      use
    endif
    if USED("__tmp__") AND RECCOUNT("__tmp__")>0
      * --- L_STADET='S' La stampa � dettagliata con codice di KEY_ARTI e data della prestazione
      L_STADET="S"
      select "__tmp__"
      CP_CHPRN("..\AGEN\EXE\QUERY\GSAG_SCU.FRX", " ", this)
    endif
    USE IN SELECT("__TMP__")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_gsag_scu')
      use in _Curs_gsag_scu
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
