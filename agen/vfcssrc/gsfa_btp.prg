* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_btp                                                        *
*              Driver centralinio da file di testo                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-28                                                      *
* Last revis.: 2010-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCONTACT,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_btp",oParentObject,m.pCONTACT,m.pPARAM)
return(i_retval)

define class tgsfa_btp as StdBatch
  * --- Local variables
  pCONTACT = space(50)
  pPARAM = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Driver centralino file di testo
    * --- Crea un file di testo nel percorso selezionato che contiene il numero di telefono
    *     da chiamare
    Local l_OldSafety 
 l_OldSafety = SET("SAFETY")
    SET SAFETY OFF
    if Empty(this.pPARAM) Or STRTOFILE(Alltrim(this.pCONTACT), Alltrim(this.pPARAM)) = 0
      * --- Errore
      Ah_ErrorMsg("Impossibile creare il file")
    endif
    SET SAFETY &l_OldSafety
  endproc


  proc Init(oParentObject,pCONTACT,pPARAM)
    this.pCONTACT=pCONTACT
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCONTACT,pPARAM"
endproc
