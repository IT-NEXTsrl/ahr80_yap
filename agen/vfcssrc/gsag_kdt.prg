* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kdt                                                        *
*              Dati di testata                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-22                                                      *
* Last revis.: 2014-12-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kdt",oParentObject))

* --- Class definition
define class tgsag_kdt as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 723
  Height = 149+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-03"
  HelpContextID=82454377
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=99

  * --- Constant Properties
  _IDX = 0
  LISTINI_IDX = 0
  TIP_DOCU_IDX = 0
  CENCOST_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gsag_kdt"
  cComment = "Dati di testata"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FLNSAP = space(1)
  o_FLNSAP = space(1)
  w_ATSTATUS = space(1)
  w_DATALIST = ctod('  /  /  ')
  w_MVFLVEAC = space(1)
  w_ATDATDOC = space(10)
  w_DATFIN = space(10)
  w_ATCODVAL = space(3)
  w_FLGLIS = space(1)
  w_FLSCOR = space(10)
  w_VALLIS = space(3)
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_VALLIS2 = space(3)
  w_INILIS2 = ctod('  /  /  ')
  w_FINLIS2 = ctod('  /  /  ')
  w_TIPLIS = space(1)
  w_FINLIS3 = ctod('  /  /  ')
  w_INILIS3 = ctod('  /  /  ')
  w_VALLIS3 = space(3)
  w_FINLIS4 = ctod('  /  /  ')
  w_INILIS4 = ctod('  /  /  ')
  w_VALLIS4 = space(3)
  w_IVALIS = space(1)
  w_FLGCIC = space(1)
  w_FLGCIC2 = space(1)
  w_FLGCIC3 = space(1)
  w_FLGCIC4 = space(1)
  w_TIPLIS4 = space(1)
  o_TIPLIS4 = space(1)
  w_TIPLIS3 = space(1)
  o_TIPLIS3 = space(1)
  w_TIPLIS = space(1)
  w_TIPLIS2 = space(1)
  w_FLSTAT = space(1)
  w_F2STAT = space(1)
  w_F3STAT = space(1)
  w_F4STAT = space(1)
  w_MVTSCLIS = space(5)
  w_MVTCOLIS = space(5)
  w_IVALISP = space(1)
  w_CLASSE = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_TIPATT = space(10)
  w_DATOBSO = ctod('  /  /  ')
  w_FLANAL = space(1)
  w_FLDANA = space(1)
  o_FLDANA = space(1)
  w_CENOBSO = ctod('  /  /  ')
  w_FLGCOA = space(10)
  w_CODCLI = space(15)
  w_ATTIPCLI = space(15)
  w_KEYLISTB = space(10)
  w_MVCODVAL = space(3)
  w_VALACQ = space(3)
  w_INIACQ = ctod('  /  /  ')
  w_TIPACQ = space(1)
  w_FLSACQ = space(1)
  w_IVAACQ = space(1)
  w_FLGACQ = space(1)
  w_FINACQ = ctod('  /  /  ')
  w_FLACQ = space(1)
  w_BUFLANAL = space(1)
  w_FLSCOAC = space(1)
  w_DATINI = ctod('  /  /  ')
  w_CODLIS = space(5)
  w_ATTCOLIS = space(5)
  w_ATCODLIS = space(5)
  w_ATTSCLIS = space(5)
  w_ATTPROLI = space(5)
  w_ATTPROSC = space(5)
  w_ATCENRIC = space(15)
  w_CCDESRIC = space(40)
  w_ATCOMRIC = space(15)
  o_ATCOMRIC = space(15)
  w_ATATTRIC = space(15)
  w_DESLIS = space(40)
  w_DESLIS2 = space(40)
  w_ATCENCOS = space(15)
  w_ATCODPRA = space(15)
  o_ATCODPRA = space(15)
  w_ATATTCOS = space(15)
  w_CCDESPIA = space(40)
  w_FLGCOM = space(1)
  w_DESDOC = space(35)
  w_VFLEVEAC = space(1)
  w_CATDOC = space(2)
  w_ATCAUACQ = space(10)
  w_DESACQ = space(35)
  w_AFLEVEAC = space(1)
  w_CATACQ = space(2)
  w_ATLISACQ = space(5)
  w_DESACQ1 = space(40)
  w_TDFLVEAC = space(1)
  w_CAT_DOC = space(10)
  w_DESLIS1 = space(40)
  w_ATCAUDOC = space(5)
  o_ATCAUDOC = space(5)
  w_CAUCON = space(10)
  w_FLGLISA = space(1)
  w_FLINTE = space(1)
  w_ATINIRIC = ctod('  /  /  ')
  o_ATINIRIC = ctod('  /  /  ')
  w_ATFINRIC = ctod('  /  /  ')
  w_ATINICOS = ctod('  /  /  ')
  o_ATINICOS = ctod('  /  /  ')
  w_ATFINCOS = ctod('  /  /  ')
  w_NUMSCO = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kdtPag1","gsag_kdt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Ricavi")
      .Pages(2).addobject("oPag","tgsag_kdtPag2","gsag_kdt",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Costi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oATTCOLIS_1_69
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='CENCOST'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='ATTIVITA'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLNSAP=space(1)
      .w_ATSTATUS=space(1)
      .w_DATALIST=ctod("  /  /  ")
      .w_MVFLVEAC=space(1)
      .w_ATDATDOC=space(10)
      .w_DATFIN=space(10)
      .w_ATCODVAL=space(3)
      .w_FLGLIS=space(1)
      .w_FLSCOR=space(10)
      .w_VALLIS=space(3)
      .w_INILIS=ctod("  /  /  ")
      .w_FINLIS=ctod("  /  /  ")
      .w_VALLIS2=space(3)
      .w_INILIS2=ctod("  /  /  ")
      .w_FINLIS2=ctod("  /  /  ")
      .w_TIPLIS=space(1)
      .w_FINLIS3=ctod("  /  /  ")
      .w_INILIS3=ctod("  /  /  ")
      .w_VALLIS3=space(3)
      .w_FINLIS4=ctod("  /  /  ")
      .w_INILIS4=ctod("  /  /  ")
      .w_VALLIS4=space(3)
      .w_IVALIS=space(1)
      .w_FLGCIC=space(1)
      .w_FLGCIC2=space(1)
      .w_FLGCIC3=space(1)
      .w_FLGCIC4=space(1)
      .w_TIPLIS4=space(1)
      .w_TIPLIS3=space(1)
      .w_TIPLIS=space(1)
      .w_TIPLIS2=space(1)
      .w_FLSTAT=space(1)
      .w_F2STAT=space(1)
      .w_F3STAT=space(1)
      .w_F4STAT=space(1)
      .w_MVTSCLIS=space(5)
      .w_MVTCOLIS=space(5)
      .w_IVALISP=space(1)
      .w_CLASSE=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPATT=space(10)
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLANAL=space(1)
      .w_FLDANA=space(1)
      .w_CENOBSO=ctod("  /  /  ")
      .w_FLGCOA=space(10)
      .w_CODCLI=space(15)
      .w_ATTIPCLI=space(15)
      .w_KEYLISTB=space(10)
      .w_MVCODVAL=space(3)
      .w_VALACQ=space(3)
      .w_INIACQ=ctod("  /  /  ")
      .w_TIPACQ=space(1)
      .w_FLSACQ=space(1)
      .w_IVAACQ=space(1)
      .w_FLGACQ=space(1)
      .w_FINACQ=ctod("  /  /  ")
      .w_FLACQ=space(1)
      .w_BUFLANAL=space(1)
      .w_FLSCOAC=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_CODLIS=space(5)
      .w_ATTCOLIS=space(5)
      .w_ATCODLIS=space(5)
      .w_ATTSCLIS=space(5)
      .w_ATTPROLI=space(5)
      .w_ATTPROSC=space(5)
      .w_ATCENRIC=space(15)
      .w_CCDESRIC=space(40)
      .w_ATCOMRIC=space(15)
      .w_ATATTRIC=space(15)
      .w_DESLIS=space(40)
      .w_DESLIS2=space(40)
      .w_ATCENCOS=space(15)
      .w_ATCODPRA=space(15)
      .w_ATATTCOS=space(15)
      .w_CCDESPIA=space(40)
      .w_FLGCOM=space(1)
      .w_DESDOC=space(35)
      .w_VFLEVEAC=space(1)
      .w_CATDOC=space(2)
      .w_ATCAUACQ=space(10)
      .w_DESACQ=space(35)
      .w_AFLEVEAC=space(1)
      .w_CATACQ=space(2)
      .w_ATLISACQ=space(5)
      .w_DESACQ1=space(40)
      .w_TDFLVEAC=space(1)
      .w_CAT_DOC=space(10)
      .w_DESLIS1=space(40)
      .w_ATCAUDOC=space(5)
      .w_CAUCON=space(10)
      .w_FLGLISA=space(1)
      .w_FLINTE=space(1)
      .w_ATINIRIC=ctod("  /  /  ")
      .w_ATFINRIC=ctod("  /  /  ")
      .w_ATINICOS=ctod("  /  /  ")
      .w_ATFINCOS=ctod("  /  /  ")
      .w_NUMSCO=0
      .w_FLNSAP=oParentObject.w_FLNSAP
      .w_ATSTATUS=oParentObject.w_ATSTATUS
      .w_ATDATDOC=oParentObject.w_ATDATDOC
      .w_DATFIN=oParentObject.w_DATFIN
      .w_ATCODVAL=oParentObject.w_ATCODVAL
      .w_FLGLIS=oParentObject.w_FLGLIS
      .w_FLSCOR=oParentObject.w_FLSCOR
      .w_FLANAL=oParentObject.w_FLANAL
      .w_FLDANA=oParentObject.w_FLDANA
      .w_CODCLI=oParentObject.w_CODCLI
      .w_ATTIPCLI=oParentObject.w_ATTIPCLI
      .w_KEYLISTB=oParentObject.w_KEYLISTB
      .w_FLACQ=oParentObject.w_FLACQ
      .w_BUFLANAL=oParentObject.w_BUFLANAL
      .w_DATINI=oParentObject.w_DATINI
      .w_CODLIS=oParentObject.w_CODLIS
      .w_ATTCOLIS=oParentObject.w_ATTCOLIS
      .w_ATCODLIS=oParentObject.w_ATCODLIS
      .w_ATTSCLIS=oParentObject.w_ATTSCLIS
      .w_ATTPROLI=oParentObject.w_ATTPROLI
      .w_ATTPROSC=oParentObject.w_ATTPROSC
      .w_ATCENRIC=oParentObject.w_ATCENRIC
      .w_ATCOMRIC=oParentObject.w_ATCOMRIC
      .w_ATATTRIC=oParentObject.w_ATATTRIC
      .w_ATCENCOS=oParentObject.w_ATCENCOS
      .w_ATCODPRA=oParentObject.w_ATCODPRA
      .w_ATATTCOS=oParentObject.w_ATATTCOS
      .w_FLGCOM=oParentObject.w_FLGCOM
      .w_ATCAUACQ=oParentObject.w_ATCAUACQ
      .w_ATLISACQ=oParentObject.w_ATLISACQ
      .w_ATCAUDOC=oParentObject.w_ATCAUDOC
      .w_CAUCON=oParentObject.w_CAUCON
      .w_ATINIRIC=oParentObject.w_ATINIRIC
      .w_ATFINRIC=oParentObject.w_ATFINRIC
      .w_ATINICOS=oParentObject.w_ATINICOS
      .w_ATFINCOS=oParentObject.w_ATFINCOS
      .w_NUMSCO=oParentObject.w_NUMSCO
          .DoRTCalc(1,2,.f.)
        .w_DATALIST = IIF(Not Empty(.w_ATDATDOC),.w_ATDATDOC,.w_DATFIN)
          .DoRTCalc(4,38,.f.)
        .w_CLASSE = This.oparentobject.class
        .w_OBTEST = i_DATSYS
        .w_TIPATT = 'A'
          .DoRTCalc(42,45,.f.)
        .w_FLGCOA = Docgesana(.w_ATCAUACQ,'C')
      .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
          .DoRTCalc(47,49,.f.)
        .w_MVCODVAL = This.oparentobject .w_ATCODVAL
      .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .DoRTCalc(51,63,.f.)
        if not(empty(.w_ATTCOLIS))
          .link_1_69('Full')
        endif
        .DoRTCalc(64,64,.f.)
        if not(empty(.w_ATCODLIS))
          .link_1_70('Full')
        endif
        .DoRTCalc(65,65,.f.)
        if not(empty(.w_ATTSCLIS))
          .link_1_71('Full')
        endif
        .DoRTCalc(66,66,.f.)
        if not(empty(.w_ATTPROLI))
          .link_1_72('Full')
        endif
        .DoRTCalc(67,67,.f.)
        if not(empty(.w_ATTPROSC))
          .link_1_73('Full')
        endif
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_ATCENRIC))
          .link_1_74('Full')
        endif
        .DoRTCalc(69,70,.f.)
        if not(empty(.w_ATCOMRIC))
          .link_1_76('Full')
        endif
        .DoRTCalc(71,71,.f.)
        if not(empty(.w_ATATTRIC))
          .link_1_77('Full')
        endif
        .DoRTCalc(72,74,.f.)
        if not(empty(.w_ATCENCOS))
          .link_2_1('Full')
        endif
        .DoRTCalc(75,75,.f.)
        if not(empty(.w_ATCODPRA))
          .link_2_2('Full')
        endif
        .DoRTCalc(76,76,.f.)
        if not(empty(.w_ATATTCOS))
          .link_2_3('Full')
        endif
        .DoRTCalc(77,82,.f.)
        if not(empty(.w_ATCAUACQ))
          .link_2_9('Full')
        endif
        .DoRTCalc(83,86,.f.)
        if not(empty(.w_ATLISACQ))
          .link_2_14('Full')
        endif
          .DoRTCalc(87,88,.f.)
        .w_CAT_DOC = IIF(isalt(), 'DI', SPACE(2) )
          .DoRTCalc(90,90,.f.)
        .w_ATCAUDOC = .w_ATCAUDOC
        .DoRTCalc(91,91,.f.)
        if not(empty(.w_ATCAUDOC))
          .link_1_95('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
    endwith
    this.DoRTCalc(92,99,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oATTCOLIS_1_69.enabled = i_bVal
      .Page1.oPag.oATCODLIS_1_70.enabled = i_bVal
      .Page1.oPag.oATTSCLIS_1_71.enabled = i_bVal
      .Page1.oPag.oATTPROLI_1_72.enabled = i_bVal
      .Page1.oPag.oATTPROSC_1_73.enabled = i_bVal
      .Page1.oPag.oATCENRIC_1_74.enabled = i_bVal
      .Page1.oPag.oATCOMRIC_1_76.enabled = i_bVal
      .Page1.oPag.oATATTRIC_1_77.enabled = i_bVal
      .Page2.oPag.oATCENCOS_2_1.enabled = i_bVal
      .Page2.oPag.oATCODPRA_2_2.enabled = i_bVal
      .Page2.oPag.oATATTCOS_2_3.enabled = i_bVal
      .Page2.oPag.oATCAUACQ_2_9.enabled = i_bVal
      .Page2.oPag.oATLISACQ_2_14.enabled = i_bVal
      .Page1.oPag.oATCAUDOC_1_95.enabled = i_bVal
      .Page1.oPag.oATINIRIC_1_97.enabled = i_bVal
      .Page1.oPag.oATFINRIC_1_98.enabled = i_bVal
      .Page2.oPag.oATINICOS_2_20.enabled = i_bVal
      .Page2.oPag.oATFINCOS_2_21.enabled = i_bVal
      .Page1.oPag.oObj_1_51.enabled = i_bVal
      .Page1.oPag.oObj_1_52.enabled = i_bVal
      .Page1.oPag.oObj_1_53.enabled = i_bVal
      .Page1.oPag.oObj_1_54.enabled = i_bVal
      .Page1.oPag.oObj_1_63.enabled = i_bVal
      .Page1.oPag.oObj_1_101.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_FLNSAP=.w_FLNSAP
      .oParentObject.w_ATSTATUS=.w_ATSTATUS
      .oParentObject.w_ATDATDOC=.w_ATDATDOC
      .oParentObject.w_DATFIN=.w_DATFIN
      .oParentObject.w_ATCODVAL=.w_ATCODVAL
      .oParentObject.w_FLGLIS=.w_FLGLIS
      .oParentObject.w_FLSCOR=.w_FLSCOR
      .oParentObject.w_FLANAL=.w_FLANAL
      .oParentObject.w_FLDANA=.w_FLDANA
      .oParentObject.w_CODCLI=.w_CODCLI
      .oParentObject.w_ATTIPCLI=.w_ATTIPCLI
      .oParentObject.w_KEYLISTB=.w_KEYLISTB
      .oParentObject.w_FLACQ=.w_FLACQ
      .oParentObject.w_BUFLANAL=.w_BUFLANAL
      .oParentObject.w_DATINI=.w_DATINI
      .oParentObject.w_CODLIS=.w_CODLIS
      .oParentObject.w_ATTCOLIS=.w_ATTCOLIS
      .oParentObject.w_ATCODLIS=.w_ATCODLIS
      .oParentObject.w_ATTSCLIS=.w_ATTSCLIS
      .oParentObject.w_ATTPROLI=.w_ATTPROLI
      .oParentObject.w_ATTPROSC=.w_ATTPROSC
      .oParentObject.w_ATCENRIC=.w_ATCENRIC
      .oParentObject.w_ATCOMRIC=.w_ATCOMRIC
      .oParentObject.w_ATATTRIC=.w_ATATTRIC
      .oParentObject.w_ATCENCOS=.w_ATCENCOS
      .oParentObject.w_ATCODPRA=.w_ATCODPRA
      .oParentObject.w_ATATTCOS=.w_ATATTCOS
      .oParentObject.w_FLGCOM=.w_FLGCOM
      .oParentObject.w_ATCAUACQ=.w_ATCAUACQ
      .oParentObject.w_ATLISACQ=.w_ATLISACQ
      .oParentObject.w_ATCAUDOC=.w_ATCAUDOC
      .oParentObject.w_CAUCON=.w_CAUCON
      .oParentObject.w_ATINIRIC=.w_ATINIRIC
      .oParentObject.w_ATFINRIC=.w_ATFINRIC
      .oParentObject.w_ATINICOS=.w_ATINICOS
      .oParentObject.w_ATFINCOS=.w_ATFINCOS
      .oParentObject.w_NUMSCO=.w_NUMSCO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_DATALIST = IIF(Not Empty(.w_ATDATDOC),.w_ATDATDOC,.w_DATFIN)
        .DoRTCalc(4,38,.t.)
            .w_CLASSE = This.oparentobject.class
            .w_OBTEST = i_DATSYS
            .w_TIPATT = 'A'
        .DoRTCalc(42,45,.t.)
            .w_FLGCOA = Docgesana(.w_ATCAUACQ,'C')
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .DoRTCalc(47,49,.t.)
            .w_MVCODVAL = This.oparentobject .w_ATCODVAL
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .DoRTCalc(51,62,.t.)
        if .o_ATCAUDOC<>.w_ATCAUDOC
          .link_1_69('Full')
        endif
        if .o_ATCAUDOC<>.w_ATCAUDOC
          .link_1_70('Full')
        endif
        .DoRTCalc(65,65,.t.)
        if .o_TIPLIS4<>.w_TIPLIS4
            .w_ATTPROLI = iif(.w_TIPLIS4='E' And Empty(.w_ATTPROLI), .w_ATTPROSC, .w_ATTPROLI)
          .link_1_72('Full')
        endif
        if .o_TIPLIS3<>.w_TIPLIS3
            .w_ATTPROSC = iif(.w_TIPLIS3='E' And Empty(.w_ATTPROSC), .w_ATTPROLI, .w_ATTPROSC)
          .link_1_73('Full')
        endif
        .DoRTCalc(68,70,.t.)
        if .o_ATCOMRIC<>.w_ATCOMRIC
            .w_ATATTRIC = SPACE(15)
          .link_1_77('Full')
        endif
        .DoRTCalc(72,75,.t.)
        if .o_ATCODPRA<>.w_ATCODPRA
            .w_ATATTCOS = SPACE(15)
          .link_2_3('Full')
        endif
        .DoRTCalc(77,81,.t.)
        if .o_FLDANA<>.w_FLDANA.or. .o_FLNSAP<>.w_FLNSAP
            .w_ATCAUACQ = .w_ATCAUACQ
          .link_2_9('Full')
        endif
        .DoRTCalc(83,88,.t.)
            .w_CAT_DOC = IIF(isalt(), 'DI', SPACE(2) )
        .DoRTCalc(90,95,.t.)
        if .o_ATINIRIC<>.w_ATINIRIC
            .w_ATFINRIC = IIF(EMPTY(.w_ATINIRIC),CP_CHARTODATE('  -  -  '), .w_ATFINRIC)
        endif
        .DoRTCalc(97,97,.t.)
        if .o_ATINICOS<>.w_ATINICOS
            .w_ATFINCOS = IIF(EMPTY(.w_ATINICOS),CP_CHARTODATE('  -  -  '), .w_ATFINCOS)
        endif
        .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(99,99,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_101.Calculate()
    endwith
  return

  proc Calculate_NGIZUFSHJC()
    with this
          * --- DaCostoaRicavo
          .w_ATCENRIC = IIF(Empty(.w_ATCENRIC),.w_ATCENCOS,.w_ATCENRIC)
          .link_1_74('Full')
    endwith
  endproc
  proc Calculate_TNMHOXGOZO()
    with this
          * --- DaCommessaCostoaCommessaRicavo
          .w_ATCOMRIC = IIF(Empty(.w_ATCOMRIC),.w_ATCODPRA,.w_ATCOMRIC)
          .link_1_76('Full')
    endwith
  endproc
  proc Calculate_RAGNLRAPPA()
    with this
          * --- DaRicavoaCosto
          .w_ATCENCOS = IIF(Empty(.w_ATCENCOS),.w_ATCENRIC,.w_ATCENCOS)
          .link_2_1('Full')
    endwith
  endproc
  proc Calculate_HCZCSPGIAW()
    with this
          * --- DaCommessaRicavoaCommessaCosto
          .w_ATCODPRA = IIF(Empty(.w_ATCODPRA),.w_ATCOMRIC,.w_ATCODPRA)
          .link_2_2('Full')
    endwith
  endproc
  proc Calculate_ZRRHNREKTH()
    with this
          * --- DaAttivitaCostoaAttivitaRicavo
          .w_ATATTRIC = IIF(Empty(.w_ATATTRIC) AND .w_ATCOMRIC=.w_ATCODPRA AND g_PERCAN='S',.w_ATATTCOS,.w_ATATTRIC)
          .link_1_77('Full')
    endwith
  endproc
  proc Calculate_JRXVAVMJVR()
    with this
          * --- DaAttivitaRicavoaAttivitaCosto
          .w_ATATTCOS = IIF(Empty(.w_ATATTCOS) AND .w_ATCOMRIC=.w_ATCODPRA AND g_PERCAN='S',.w_ATATTRIC,.w_ATATTCOS)
          .link_2_3('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATTCOLIS_1_69.enabled = this.oPgFrm.Page1.oPag.oATTCOLIS_1_69.mCond()
    this.oPgFrm.Page1.oPag.oATTSCLIS_1_71.enabled = this.oPgFrm.Page1.oPag.oATTSCLIS_1_71.mCond()
    this.oPgFrm.Page1.oPag.oATTPROLI_1_72.enabled = this.oPgFrm.Page1.oPag.oATTPROLI_1_72.mCond()
    this.oPgFrm.Page1.oPag.oATTPROSC_1_73.enabled = this.oPgFrm.Page1.oPag.oATTPROSC_1_73.mCond()
    this.oPgFrm.Page1.oPag.oATCENRIC_1_74.enabled = this.oPgFrm.Page1.oPag.oATCENRIC_1_74.mCond()
    this.oPgFrm.Page1.oPag.oATATTRIC_1_77.enabled = this.oPgFrm.Page1.oPag.oATATTRIC_1_77.mCond()
    this.oPgFrm.Page2.oPag.oATCENCOS_2_1.enabled = this.oPgFrm.Page2.oPag.oATCENCOS_2_1.mCond()
    this.oPgFrm.Page2.oPag.oATATTCOS_2_3.enabled = this.oPgFrm.Page2.oPag.oATATTCOS_2_3.mCond()
    this.oPgFrm.Page2.oPag.oATCAUACQ_2_9.enabled = this.oPgFrm.Page2.oPag.oATCAUACQ_2_9.mCond()
    this.oPgFrm.Page1.oPag.oATCAUDOC_1_95.enabled = this.oPgFrm.Page1.oPag.oATCAUDOC_1_95.mCond()
    this.oPgFrm.Page1.oPag.oATFINRIC_1_98.enabled = this.oPgFrm.Page1.oPag.oATFINRIC_1_98.mCond()
    this.oPgFrm.Page2.oPag.oATFINCOS_2_21.enabled = this.oPgFrm.Page2.oPag.oATFINCOS_2_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oATTCOLIS_1_69.visible=!this.oPgFrm.Page1.oPag.oATTCOLIS_1_69.mHide()
    this.oPgFrm.Page1.oPag.oATCODLIS_1_70.visible=!this.oPgFrm.Page1.oPag.oATCODLIS_1_70.mHide()
    this.oPgFrm.Page1.oPag.oATTSCLIS_1_71.visible=!this.oPgFrm.Page1.oPag.oATTSCLIS_1_71.mHide()
    this.oPgFrm.Page1.oPag.oATTPROLI_1_72.visible=!this.oPgFrm.Page1.oPag.oATTPROLI_1_72.mHide()
    this.oPgFrm.Page1.oPag.oATTPROSC_1_73.visible=!this.oPgFrm.Page1.oPag.oATTPROSC_1_73.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS_1_81.visible=!this.oPgFrm.Page1.oPag.oDESLIS_1_81.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS2_1_82.visible=!this.oPgFrm.Page1.oPag.oDESLIS2_1_82.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_83.visible=!this.oPgFrm.Page1.oPag.oStr_1_83.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_85.visible=!this.oPgFrm.Page1.oPag.oStr_1_85.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_86.visible=!this.oPgFrm.Page1.oPag.oStr_1_86.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_6.visible=!this.oPgFrm.Page2.oPag.oStr_2_6.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_7.visible=!this.oPgFrm.Page2.oPag.oStr_2_7.mHide()
    this.oPgFrm.Page2.oPag.oATCAUACQ_2_9.visible=!this.oPgFrm.Page2.oPag.oATCAUACQ_2_9.mHide()
    this.oPgFrm.Page2.oPag.oDESACQ_2_10.visible=!this.oPgFrm.Page2.oPag.oDESACQ_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_13.visible=!this.oPgFrm.Page2.oPag.oStr_2_13.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS1_1_94.visible=!this.oPgFrm.Page1.oPag.oDESLIS1_1_94.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_kdt
    * Valorizzo dati analitica
    If !Isalt()
     If Cevent='w_ATCENRIC Changed'
       This.Notifyevent('DaRicavoaCosto')
     Endif
     If Cevent='w_ATCOMRIC Changed' 
       This.Notifyevent('DaCommessaRicavoaCommessaCosto')
     Endif
     If Cevent='w_ATCENCOS Changed' 
       This.Notifyevent('DaCostoaRicavo')
     Endif
     If Cevent='w_ATCODPRA Changed'
       This.Notifyevent('DaCommessaCostoaCommessaRicavo')
     Endif
     If Cevent='w_ATATTRIC Changed'
       This.Notifyevent('DaAttivitaRicavoaAttivitaCosto')
     endif
     if Cevent='w_ATATTCOS Changed'
       This.Notifyevent('DaAttivitaCostoaAttivitaRicavo')
     endif
    Endif
    If cEvent='w_ATCODPRA Changed'
         this.oparentobject.w_ATCODPRA=This.w_ATCODPRA
          this.oparentobject.w_ATCOMRIC=This.w_ATCOMRIC
         this.oparentobject.NotifyEvent('w_ATCODPRA Changed')
    endif
    If cEvent='w_ATCENCOS Changed'
         this.oparentobject.w_ATCENCOS=This.w_ATCENCOS
         this.oparentobject.w_ATCENRIC=This.w_ATCENRIC
         this.oparentobject.NotifyEvent('w_ATCENCOS Changed')
    endif
    If cEvent='w_ATCENRIC Changed'
         this.oparentobject.w_ATCENRIC=This.w_ATCENRIC
         this.oparentobject.w_ATCENCOS=This.w_ATCENCOS
         this.oparentobject.NotifyEvent('w_ATCENRIC Changed')
    endif
    If cEvent='w_ATCOMRIC Changed'
         this.oparentobject.w_ATCOMRIC=This.w_ATCOMRIC
         this.oparentobject.w_ATCODPRA=This.w_ATCODPRA
         this.oparentobject.NotifyEvent('w_ATCOMRIC Changed')
    endif
    If cEvent='w_ATATTRIC Changed'
         this.oparentobject.w_ATATTRIC=This.w_ATATTRIC
         this.oparentobject.w_ATATTCOS=This.w_ATATTCOS
         this.oparentobject.NotifyEvent('w_ATATTRIC Changed')
    endif
    If cEvent='w_ATATTCOS Changed'
         this.oparentobject.w_ATATTCOS=This.w_ATATTCOS
         this.oparentobject.w_ATATTRIC=This.w_ATATTRIC
         this.oparentobject.NotifyEvent('w_ATATTCOS Changed')
    endif
    If cEvent='w_ATLISACQ Changed'
         this.oparentobject.w_ATLISACQ=This.w_ATLISACQ
         this.oparentobject.NotifyEvent('w_ATLISACQ Changed')
    endif
    If cEvent='w_ATCODLIS Changed'
         this.oparentobject.w_ATCODLIS=This.w_ATCODLIS
         this.oparentobject.NotifyEvent('w_ATCODLIS Changed')
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_101.Event(cEvent)
        if lower(cEvent)==lower("DaCostoaRicavo")
          .Calculate_NGIZUFSHJC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaCommessaCostoaCommessaRicavo")
          .Calculate_TNMHOXGOZO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaRicavoaCosto")
          .Calculate_RAGNLRAPPA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaCommessaRicavoaCommessaCosto")
          .Calculate_HCZCSPGIAW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaAttivitaCostoaAttivitaRicavo")
          .Calculate_ZRRHNREKTH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaAttivitaRicavoaAttivitaCosto")
          .Calculate_JRXVAVMJVR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kdt
    * Valorizzo dati analitica
    If !Isalt()
     If Cevent='w_ATCENRIC Changed'
       This.Notifyevent('DaRicavoaCosto')
     Endif
     If Cevent='w_ATCOMRIC Changed' 
       This.Notifyevent('DaCommessaRicavoaCommessaCosto')
     Endif
     If Cevent='w_ATCENCOS Changed' 
       This.Notifyevent('DaCostoaRicavo')
     Endif
     If Cevent='w_ATCODPRA Changed'
       This.Notifyevent('DaCommessaCostoaCommessaRicavo')
     Endif
     If Cevent='w_ATATTRIC Changed'
       This.Notifyevent('DaAttivitaRicavoaAttivitaCosto')
     endif
     if Cevent='w_ATATTCOS Changed'
       This.Notifyevent('DaAttivitaCostoaAttivitaRicavo')
     endif
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ATTCOLIS
  func Link_1_69(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTCOLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ATTCOLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ATTCOLIS))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTCOLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_ATTCOLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_ATTCOLIS)+"%");

            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATTCOLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oATTCOLIS_1_69'),i_cWhere,'',"ELENCO LISTINI",'listdocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTCOLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ATTCOLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ATTCOLIS)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTCOLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC = NVL(_Link_.LSFLGCIC,space(1))
      this.w_FLSTAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATTCOLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_IVALIS = space(1)
      this.w_VALLIS = space(3)
      this.w_INILIS = ctod("  /  /  ")
      this.w_FINLIS = ctod("  /  /  ")
      this.w_TIPLIS = space(1)
      this.w_FLGCIC = space(1)
      this.w_FLSTAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAhe() OR CHKLISD(.w_ATTCOLIS, .w_IVALIS, .w_VALLIS, .w_INILIS, .w_FINLIS,.w_TIPLIS,'P',.w_FLGCIC, .w_FLSTAT,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'V')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATTCOLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_IVALIS = space(1)
        this.w_VALLIS = space(3)
        this.w_INILIS = ctod("  /  /  ")
        this.w_FINLIS = ctod("  /  /  ")
        this.w_TIPLIS = space(1)
        this.w_FLGCIC = space(1)
        this.w_FLSTAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTCOLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODLIS
  func Link_1_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ATCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ATCODLIS))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_ATCODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_ATCODLIS)+"%");

            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oATCODLIS_1_70'),i_cWhere,'',"ELENCO LISTINI",'listdocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ATCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ATCODLIS)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS1 = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC = NVL(_Link_.LSFLGCIC,space(1))
      this.w_FLSTAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODLIS = space(5)
      endif
      this.w_DESLIS1 = space(40)
      this.w_IVALIS = space(1)
      this.w_VALLIS = space(3)
      this.w_INILIS = ctod("  /  /  ")
      this.w_FINLIS = ctod("  /  /  ")
      this.w_TIPLIS = space(1)
      this.w_FLGCIC = space(1)
      this.w_FLSTAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(Isahe() or !IsAhe() OR CHKLISD(.w_ATTSCLIS, 'L', .w_VALLIS, .w_INILIS, .w_FINLIS,.w_TIPLIS,'S',.w_FLGCIC, .w_F2STAT,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'V')) or (Isahr() and CHKLISD(.w_ATCODLIS,.w_IVALIS,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATCODLIS = space(5)
        this.w_DESLIS1 = space(40)
        this.w_IVALIS = space(1)
        this.w_VALLIS = space(3)
        this.w_INILIS = ctod("  /  /  ")
        this.w_FINLIS = ctod("  /  /  ")
        this.w_TIPLIS = space(1)
        this.w_FLGCIC = space(1)
        this.w_FLSTAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTSCLIS
  func Link_1_71(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTSCLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ATTSCLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ATTSCLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTSCLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_ATTSCLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_ATTSCLIS)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATTSCLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oATTSCLIS_1_71'),i_cWhere,'',"ELENCO LISTINI",'listdocu2.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTSCLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ATTSCLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ATTSCLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTSCLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS2 = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS2 = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS2 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS2 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS2 = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC2 = NVL(_Link_.LSFLGCIC,space(1))
      this.w_F2STAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATTSCLIS = space(5)
      endif
      this.w_DESLIS2 = space(40)
      this.w_VALLIS2 = space(3)
      this.w_INILIS2 = ctod("  /  /  ")
      this.w_FINLIS2 = ctod("  /  /  ")
      this.w_TIPLIS2 = space(1)
      this.w_FLGCIC2 = space(1)
      this.w_F2STAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAhe() OR CHKLISD(.w_ATTSCLIS, 'L', .w_VALLIS2, .w_INILIS2, .w_FINLIS2,.w_TIPLIS2,'S',.w_FLGCIC2, .w_F2STAT,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'V')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATTSCLIS = space(5)
        this.w_DESLIS2 = space(40)
        this.w_VALLIS2 = space(3)
        this.w_INILIS2 = ctod("  /  /  ")
        this.w_FINLIS2 = ctod("  /  /  ")
        this.w_TIPLIS2 = space(1)
        this.w_FLGCIC2 = space(1)
        this.w_F2STAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTSCLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTPROLI
  func Link_1_72(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTPROLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ATTPROLI)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ATTPROLI))
          select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTPROLI)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTPROLI) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oATTPROLI_1_72'),i_cWhere,'',"ELENCO PROMOZIONI",'prodocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTPROLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ATTPROLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ATTPROLI)
            select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTPROLI = NVL(_Link_.LSCODLIS,space(5))
      this.w_IVALISP = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALLIS3 = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS3 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS3 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS3 = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC3 = NVL(_Link_.LSFLGCIC,space(1))
      this.w_F3STAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATTPROLI = space(5)
      endif
      this.w_IVALISP = space(1)
      this.w_VALLIS3 = space(3)
      this.w_INILIS3 = ctod("  /  /  ")
      this.w_FINLIS3 = ctod("  /  /  ")
      this.w_TIPLIS3 = space(1)
      this.w_FLGCIC3 = space(1)
      this.w_F3STAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAhe() OR CHKLISD(.w_ATTPROLI, .w_IVALISP, .w_VALLIS3, .w_INILIS3, .w_FINLIS3,.w_TIPLIS3,'P',.w_FLGCIC3, .w_F3STAT,.w_FLSCOR, .w_ATCODVAL,.w_DATALIST,'V',.w_ATTCOLIS,.w_ATTSCLIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATTPROLI = space(5)
        this.w_IVALISP = space(1)
        this.w_VALLIS3 = space(3)
        this.w_INILIS3 = ctod("  /  /  ")
        this.w_FINLIS3 = ctod("  /  /  ")
        this.w_TIPLIS3 = space(1)
        this.w_FLGCIC3 = space(1)
        this.w_F3STAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTPROLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATTPROSC
  func Link_1_73(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTPROSC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ATTPROSC)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ATTPROSC))
          select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTPROSC)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTPROSC) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oATTPROSC_1_73'),i_cWhere,'',"ELENCO PROMOZIONI",'prodocu2.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTPROSC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ATTPROSC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ATTPROSC)
            select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTPROSC = NVL(_Link_.LSCODLIS,space(5))
      this.w_VALLIS4 = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS4 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS4 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS4 = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC4 = NVL(_Link_.LSFLGCIC,space(1))
      this.w_F4STAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATTPROSC = space(5)
      endif
      this.w_VALLIS4 = space(3)
      this.w_INILIS4 = ctod("  /  /  ")
      this.w_FINLIS4 = ctod("  /  /  ")
      this.w_TIPLIS4 = space(1)
      this.w_FLGCIC4 = space(1)
      this.w_F4STAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAhe() OR CHKLISD(.w_ATTPROSC, 'L', .w_VALLIS4, .w_INILIS4, .w_FINLIS4,.w_TIPLIS4,'S',.w_FLGCIC4, .w_F4STAT,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'V', .w_ATTCOLIS, .w_ATTSCLIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATTPROSC = space(5)
        this.w_VALLIS4 = space(3)
        this.w_INILIS4 = ctod("  /  /  ")
        this.w_FINLIS4 = ctod("  /  /  ")
        this.w_TIPLIS4 = space(1)
        this.w_FLGCIC4 = space(1)
        this.w_F4STAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTPROSC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCENRIC
  func Link_1_74(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCENRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ATCENRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ATCENRIC))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCENRIC)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCENRIC) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oATCENRIC_1_74'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCENRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ATCENRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ATCENRIC)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCENRIC = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESRIC = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCENRIC = space(15)
      endif
      this.w_CCDESRIC = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATCENRIC = space(15)
        this.w_CCDESRIC = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCENRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCOMRIC
  func Link_1_76(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCOMRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ATCOMRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ATCOMRIC))
          select CNCODCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCOMRIC)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCOMRIC) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oATCOMRIC_1_76'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCOMRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ATCOMRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ATCOMRIC)
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCOMRIC = NVL(_Link_.CNCODCAN,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCOMRIC = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_ATCOMRIC = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCOMRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATATTRIC
  func Link_1_77(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATATTRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATATTRIC)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCOMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_ATCOMRIC;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_ATATTRIC))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATATTRIC)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATATTRIC) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATATTRIC_1_77'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCOMRIC<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCOMRIC);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATATTRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATATTRIC);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCOMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_ATCOMRIC;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_ATATTRIC)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATATTRIC = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ATATTRIC = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATATTRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCENCOS
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ATCENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ATCENCOS))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oATCENCOS_2_1'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ATCENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ATCENCOS)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCENCOS = space(15)
      endif
      this.w_CCDESPIA = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATCENCOS = space(15)
        this.w_CCDESPIA = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODPRA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ATCODPRA))
          select CNCODCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oATCODPRA_2_2'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ATCODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ATCODPRA)
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODPRA = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_ATCODPRA = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATATTCOS
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATATTCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATATTCOS)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCODPRA);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_ATCODPRA;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_ATATTCOS))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATATTCOS)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATATTCOS) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATATTCOS_2_3'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODPRA<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCODPRA);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATATTCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATATTCOS);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCODPRA);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_ATCODPRA;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_ATATTCOS)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATATTCOS = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ATATTCOS = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATATTCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCAUACQ
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCAUACQ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BLT',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_ATCAUACQ)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL,TDFLGLIS,TDFLINTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_ATCAUACQ))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL,TDFLGLIS,TDFLINTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCAUACQ)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCAUACQ) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oATCAUACQ_2_9'),i_cWhere,'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL,TDFLGLIS,TDFLINTE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL,TDFLGLIS,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCAUACQ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL,TDFLGLIS,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_ATCAUACQ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_ATCAUACQ)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLANAL,TDFLGLIS,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCAUACQ = NVL(_Link_.TDTIPDOC,space(10))
      this.w_DESACQ = NVL(_Link_.TDDESDOC,space(35))
      this.w_AFLEVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CATACQ = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLDANA = NVL(_Link_.TDFLANAL,space(1))
      this.w_FLGLISA = NVL(_Link_.TDFLGLIS,space(1))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCAUACQ = space(10)
      endif
      this.w_DESACQ = space(35)
      this.w_AFLEVEAC = space(1)
      this.w_CATACQ = space(2)
      this.w_FLDANA = space(1)
      this.w_FLGLISA = space(1)
      this.w_FLINTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_AFLEVEAC='A'  and .w_FLINTE='N' and (!ISAHE() OR .w_FLGLISA='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale con intestatario o di tipo incongruente")
        endif
        this.w_ATCAUACQ = space(10)
        this.w_DESACQ = space(35)
        this.w_AFLEVEAC = space(1)
        this.w_CATACQ = space(2)
        this.w_FLDANA = space(1)
        this.w_FLGLISA = space(1)
        this.w_FLINTE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCAUACQ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATLISACQ
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATLISACQ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ATLISACQ)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ATLISACQ))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATLISACQ)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_ATLISACQ)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_ATLISACQ)+"%");

            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATLISACQ) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oATLISACQ_2_14'),i_cWhere,'',"ELENCO LISTINI",'listdocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATLISACQ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ATLISACQ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ATLISACQ)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATLISACQ = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESACQ1 = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVAACQ = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALACQ = NVL(_Link_.LSVALLIS,space(3))
      this.w_INIACQ = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINACQ = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPACQ = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGACQ = NVL(_Link_.LSFLGCIC,space(1))
      this.w_FLSACQ = NVL(_Link_.LSFLSTAT,space(1))
      this.w_FLSCOAC = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATLISACQ = space(5)
      endif
      this.w_DESACQ1 = space(40)
      this.w_IVAACQ = space(1)
      this.w_VALACQ = space(3)
      this.w_INIACQ = ctod("  /  /  ")
      this.w_FINACQ = ctod("  /  /  ")
      this.w_TIPACQ = space(1)
      this.w_FLGACQ = space(1)
      this.w_FLSACQ = space(1)
      this.w_FLSCOAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(Isahe() and CHKLISD(.w_ATLISACQ, 'L', .w_VALACQ, .w_INIACQ, .w_FINACQ,.w_TIPACQ,'S',.w_FLGACQ, .w_FLSACQ,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'A','',.w_FLSCOAC)) or (Isahr() and .w_FLSCOAC <>'S' AND .w_IVAACQ<>'L' AND CHKLISD(.w_ATLISACQ,.w_IVAACQ,.w_VALACQ,.w_INIACQ,.w_FINLIS,'N', .w_ATCODVAL, .w_DATINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, listino inesistente o di tipo Iva inclusa o di tipo sconti")
        endif
        this.w_ATLISACQ = space(5)
        this.w_DESACQ1 = space(40)
        this.w_IVAACQ = space(1)
        this.w_VALACQ = space(3)
        this.w_INIACQ = ctod("  /  /  ")
        this.w_FINACQ = ctod("  /  /  ")
        this.w_TIPACQ = space(1)
        this.w_FLGACQ = space(1)
        this.w_FLSACQ = space(1)
        this.w_FLSCOAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATLISACQ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCAUDOC
  func Link_1_95(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BLT',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_ATCAUDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLGLIS,TDCAUCON,TDNUMSCO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_ATCAUDOC))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLGLIS,TDCAUCON,TDNUMSCO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oATCAUDOC_1_95'),i_cWhere,'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLGLIS,TDCAUCON,TDNUMSCO";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLGLIS,TDCAUCON,TDNUMSCO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLGLIS,TDCAUCON,TDNUMSCO";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_ATCAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_ATCAUDOC)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLGLIS,TDCAUCON,TDNUMSCO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_VFLEVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLGLIS = NVL(_Link_.TDFLGLIS,space(1))
      this.w_CAUCON = NVL(_Link_.TDCAUCON,space(10))
      this.w_NUMSCO = NVL(_Link_.TDNUMSCO,0)
    else
      if i_cCtrl<>'Load'
        this.w_ATCAUDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_VFLEVEAC = space(1)
      this.w_CATDOC = space(2)
      this.w_FLGLIS = space(1)
      this.w_CAUCON = space(10)
      this.w_NUMSCO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VFLEVEAC='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale incongruente")
        endif
        this.w_ATCAUDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_VFLEVEAC = space(1)
        this.w_CATDOC = space(2)
        this.w_FLGLIS = space(1)
        this.w_CAUCON = space(10)
        this.w_NUMSCO = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATTCOLIS_1_69.value==this.w_ATTCOLIS)
      this.oPgFrm.Page1.oPag.oATTCOLIS_1_69.value=this.w_ATTCOLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODLIS_1_70.value==this.w_ATCODLIS)
      this.oPgFrm.Page1.oPag.oATCODLIS_1_70.value=this.w_ATCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oATTSCLIS_1_71.value==this.w_ATTSCLIS)
      this.oPgFrm.Page1.oPag.oATTSCLIS_1_71.value=this.w_ATTSCLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oATTPROLI_1_72.value==this.w_ATTPROLI)
      this.oPgFrm.Page1.oPag.oATTPROLI_1_72.value=this.w_ATTPROLI
    endif
    if not(this.oPgFrm.Page1.oPag.oATTPROSC_1_73.value==this.w_ATTPROSC)
      this.oPgFrm.Page1.oPag.oATTPROSC_1_73.value=this.w_ATTPROSC
    endif
    if not(this.oPgFrm.Page1.oPag.oATCENRIC_1_74.value==this.w_ATCENRIC)
      this.oPgFrm.Page1.oPag.oATCENRIC_1_74.value=this.w_ATCENRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESRIC_1_75.value==this.w_CCDESRIC)
      this.oPgFrm.Page1.oPag.oCCDESRIC_1_75.value=this.w_CCDESRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oATCOMRIC_1_76.value==this.w_ATCOMRIC)
      this.oPgFrm.Page1.oPag.oATCOMRIC_1_76.value=this.w_ATCOMRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oATATTRIC_1_77.value==this.w_ATATTRIC)
      this.oPgFrm.Page1.oPag.oATATTRIC_1_77.value=this.w_ATATTRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_81.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_81.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS2_1_82.value==this.w_DESLIS2)
      this.oPgFrm.Page1.oPag.oDESLIS2_1_82.value=this.w_DESLIS2
    endif
    if not(this.oPgFrm.Page2.oPag.oATCENCOS_2_1.value==this.w_ATCENCOS)
      this.oPgFrm.Page2.oPag.oATCENCOS_2_1.value=this.w_ATCENCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oATCODPRA_2_2.value==this.w_ATCODPRA)
      this.oPgFrm.Page2.oPag.oATCODPRA_2_2.value=this.w_ATCODPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oATATTCOS_2_3.value==this.w_ATATTCOS)
      this.oPgFrm.Page2.oPag.oATATTCOS_2_3.value=this.w_ATATTCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDESPIA_2_4.value==this.w_CCDESPIA)
      this.oPgFrm.Page2.oPag.oCCDESPIA_2_4.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_88.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_88.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oATCAUACQ_2_9.value==this.w_ATCAUACQ)
      this.oPgFrm.Page2.oPag.oATCAUACQ_2_9.value=this.w_ATCAUACQ
    endif
    if not(this.oPgFrm.Page2.oPag.oDESACQ_2_10.value==this.w_DESACQ)
      this.oPgFrm.Page2.oPag.oDESACQ_2_10.value=this.w_DESACQ
    endif
    if not(this.oPgFrm.Page2.oPag.oATLISACQ_2_14.value==this.w_ATLISACQ)
      this.oPgFrm.Page2.oPag.oATLISACQ_2_14.value=this.w_ATLISACQ
    endif
    if not(this.oPgFrm.Page2.oPag.oDESACQ1_2_15.value==this.w_DESACQ1)
      this.oPgFrm.Page2.oPag.oDESACQ1_2_15.value=this.w_DESACQ1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS1_1_94.value==this.w_DESLIS1)
      this.oPgFrm.Page1.oPag.oDESLIS1_1_94.value=this.w_DESLIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oATCAUDOC_1_95.value==this.w_ATCAUDOC)
      this.oPgFrm.Page1.oPag.oATCAUDOC_1_95.value=this.w_ATCAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oATINIRIC_1_97.value==this.w_ATINIRIC)
      this.oPgFrm.Page1.oPag.oATINIRIC_1_97.value=this.w_ATINIRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oATFINRIC_1_98.value==this.w_ATFINRIC)
      this.oPgFrm.Page1.oPag.oATFINRIC_1_98.value=this.w_ATFINRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oATINICOS_2_20.value==this.w_ATINICOS)
      this.oPgFrm.Page2.oPag.oATINICOS_2_20.value=this.w_ATINICOS
    endif
    if not(this.oPgFrm.Page2.oPag.oATFINCOS_2_21.value==this.w_ATFINCOS)
      this.oPgFrm.Page2.oPag.oATFINCOS_2_21.value=this.w_ATFINCOS
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(!IsAhe() OR CHKLISD(.w_ATTCOLIS, .w_IVALIS, .w_VALLIS, .w_INILIS, .w_FINLIS,.w_TIPLIS,'P',.w_FLGCIC, .w_FLSTAT,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'V'))  and not(isAhr())  and (.w_FLGLIS='S'  And Empty(.w_ATTPROLI+.w_ATTPROSC))  and not(empty(.w_ATTCOLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTCOLIS_1_69.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((Isahe() or !IsAhe() OR CHKLISD(.w_ATTSCLIS, 'L', .w_VALLIS, .w_INILIS, .w_FINLIS,.w_TIPLIS,'S',.w_FLGCIC, .w_F2STAT,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'V')) or (Isahr() and CHKLISD(.w_ATCODLIS,.w_IVALIS,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST) ))  and not(isAhe())  and not(empty(.w_ATCODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODLIS_1_70.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!IsAhe() OR CHKLISD(.w_ATTSCLIS, 'L', .w_VALLIS2, .w_INILIS2, .w_FINLIS2,.w_TIPLIS2,'S',.w_FLGCIC2, .w_F2STAT,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'V'))  and not(isAhr())  and (.w_FLGLIS='S'  And Empty(.w_ATTPROLI+.w_ATTPROSC))  and not(empty(.w_ATTSCLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTSCLIS_1_71.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!IsAhe() OR CHKLISD(.w_ATTPROLI, .w_IVALISP, .w_VALLIS3, .w_INILIS3, .w_FINLIS3,.w_TIPLIS3,'P',.w_FLGCIC3, .w_F3STAT,.w_FLSCOR, .w_ATCODVAL,.w_DATALIST,'V',.w_ATTCOLIS,.w_ATTSCLIS))  and not(isAhr())  and (.w_FLGLIS='S' And !Empty(.w_ATTCOLIS+.w_ATTSCLIS))  and not(empty(.w_ATTPROLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTPROLI_1_72.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!IsAhe() OR CHKLISD(.w_ATTPROSC, 'L', .w_VALLIS4, .w_INILIS4, .w_FINLIS4,.w_TIPLIS4,'S',.w_FLGCIC4, .w_F4STAT,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'V', .w_ATTCOLIS, .w_ATTSCLIS))  and not(isAhr())  and (.w_FLGLIS='S'  And !Empty(.w_ATTCOLIS+.w_ATTSCLIS))  and not(empty(.w_ATTPROSC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATTPROSC_1_73.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO))  and (!((((g_PERCCM<>'S' and Isahe()) or (g_PERCCR<>'S' and Isahr())) and g_PERCAN<>'S') or (IsAhe() and .w_BUFLANAL<>'S' and not empty(g_CODBUN) )))  and not(empty(.w_ATCENRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCENRIC_1_74.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(empty(.w_ATCOMRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCOMRIC_1_76.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
          case   not(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO))  and (!((IsAhr() and g_ACQU<>'S') Or (IsAhe() AND (g_CACQ<>'S' or (.w_BUFLANAL<>'S' and not empty(g_CODBUN) ) )) or (((g_PERCCM<>'S' and Isahe()) or (g_PERCCR<>'S' and Isahr())) and g_PERCAN<>'S')))  and not(empty(.w_ATCENCOS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCENCOS_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(empty(.w_ATCODPRA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCODPRA_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
          case   not(.w_AFLEVEAC='A'  and .w_FLINTE='N' and (!ISAHE() OR .w_FLGLISA='S'))  and not(ISALT() or .w_FLANAL='S')  and (.w_ATSTATUS<>'P' AND .w_FLNSAP<>'S')  and not(empty(.w_ATCAUACQ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCAUACQ_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale con intestatario o di tipo incongruente")
          case   not((Isahe() and CHKLISD(.w_ATLISACQ, 'L', .w_VALACQ, .w_INIACQ, .w_FINACQ,.w_TIPACQ,'S',.w_FLGACQ, .w_FLSACQ,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'A','',.w_FLSCOAC)) or (Isahr() and .w_FLSCOAC <>'S' AND .w_IVAACQ<>'L' AND CHKLISD(.w_ATLISACQ,.w_IVAACQ,.w_VALACQ,.w_INIACQ,.w_FINLIS,'N', .w_ATCODVAL, .w_DATINI)))  and not(empty(.w_ATLISACQ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATLISACQ_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, listino inesistente o di tipo Iva inclusa o di tipo sconti")
          case   not(.w_VFLEVEAC='V')  and (.w_ATSTATUS<>'P' AND .w_FLNSAP<>'S')  and not(empty(.w_ATCAUDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCAUDOC_1_95.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale incongruente")
          case   not((.w_ATINIRIC<=.w_ATFINRIC) or (empty(.w_ATFINRIC)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATINIRIC_1_97.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   ((empty(.w_ATFINRIC)) or not((.w_ATINIRIC<=.w_ATFINRIC) or (empty(.w_ATFINRIC))))  and (NOT EMPTY(.w_ATINIRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATFINRIC_1_98.SetFocus()
            i_bnoObbl = !empty(.w_ATFINRIC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not((.w_ATINICOS<=.w_ATFINCOS) or (empty(.w_ATFINCOS)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATINICOS_2_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   ((empty(.w_ATFINCOS)) or not((.w_ATINICOS<=.w_ATFINCOS) or (empty(.w_ATFINCOS))))  and (NOT EMPTY(.w_ATINICOS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATFINCOS_2_21.SetFocus()
            i_bnoObbl = !empty(.w_ATFINCOS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_kdt
      This.oparentobject.o_ATCAUATT=This.oparentobject.w_ATCAUATT
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLNSAP = this.w_FLNSAP
    this.o_TIPLIS4 = this.w_TIPLIS4
    this.o_TIPLIS3 = this.w_TIPLIS3
    this.o_FLDANA = this.w_FLDANA
    this.o_ATCOMRIC = this.w_ATCOMRIC
    this.o_ATCODPRA = this.w_ATCODPRA
    this.o_ATCAUDOC = this.w_ATCAUDOC
    this.o_ATINIRIC = this.w_ATINIRIC
    this.o_ATINICOS = this.w_ATINICOS
    return

enddefine

* --- Define pages as container
define class tgsag_kdtPag1 as StdContainer
  Width  = 719
  height = 149
  stdWidth  = 719
  stdheight = 149
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_51 as cp_runprogram with uid="LMFFPFDEQR",left=-1, top=167, width=233,height=20,;
    caption='GSAG_BM2(A)',;
   bGlobalFont=.t.,;
    prg="GSAG_BM2('A')",;
    cEvent = "w_ATTCOLIS Changed",;
    nPag=1;
    , HelpContextID = 212593640


  add object oObj_1_52 as cp_runprogram with uid="DLVGFCPPNE",left=-1, top=190, width=233,height=20,;
    caption='GSAG_BM2(B)',;
   bGlobalFont=.t.,;
    prg="GSAG_BM2('B')",;
    cEvent = "w_ATTPROLI Changed",;
    nPag=1;
    , HelpContextID = 212593384


  add object oObj_1_53 as cp_runprogram with uid="ZMGFTAKJUN",left=-1, top=212, width=233,height=20,;
    caption='GSAG_BM2(C)',;
   bGlobalFont=.t.,;
    prg="GSAG_BM2('C')",;
    cEvent = "w_ATTSCLIS Changed",;
    nPag=1;
    , HelpContextID = 212593128


  add object oObj_1_54 as cp_runprogram with uid="LDFFNIOXVM",left=-1, top=235, width=233,height=20,;
    caption='GSAG_BM2(D)',;
   bGlobalFont=.t.,;
    prg="GSAG_BM2('D')",;
    cEvent = "w_ATTPROSC Changed",;
    nPag=1;
    , HelpContextID = 212592872


  add object oObj_1_63 as cp_runprogram with uid="ZMDNDBKFST",left=-2, top=257, width=234,height=20,;
    caption='GSAG_BM2(G)',;
   bGlobalFont=.t.,;
    prg="GSAG_BM2('G')",;
    cEvent = "w_ATLISACQ Changed",;
    nPag=1;
    , HelpContextID = 212592104

  add object oATTCOLIS_1_69 as StdField with uid="ICMRWOPFLQ",rtseq=63,rtrep=.f.,;
    cFormVar = "w_ATTCOLIS", cQueryName = "ATTCOLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino utilizzato per la determinazione del prezzo",;
    HelpContextID = 61968039,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=113, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ATTCOLIS"

  func oATTCOLIS_1_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGLIS='S'  And Empty(.w_ATTPROLI+.w_ATTPROSC))
    endwith
   endif
  endfunc

  func oATTCOLIS_1_69.mHide()
    with this.Parent.oContained
      return (isAhr())
    endwith
  endfunc

  proc oATTCOLIS_1_69.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oATTCOLIS_1_69.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_69('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTCOLIS_1_69.ecpDrop(oSource)
    this.Parent.oContained.link_1_69('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTCOLIS_1_69.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oATTCOLIS_1_69'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO LISTINI",'listdocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oATCODLIS_1_70 as StdField with uid="UAHTKCUAHJ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_ATCODLIS", cQueryName = "ATCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino utilizzato per la determinazione del prezzo",;
    HelpContextID = 72785575,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=113, Top=37, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ATCODLIS"

  func oATCODLIS_1_70.mHide()
    with this.Parent.oContained
      return (isAhe())
    endwith
  endfunc

  proc oATCODLIS_1_70.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oATCODLIS_1_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_70('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODLIS_1_70.ecpDrop(oSource)
    this.Parent.oContained.link_1_70('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODLIS_1_70.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oATCODLIS_1_70'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO LISTINI",'listdocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oATTSCLIS_1_71 as StdField with uid="ZBIWFUOLGF",rtseq=65,rtrep=.f.,;
    cFormVar = "w_ATTSCLIS", cQueryName = "ATTSCLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino utilizzato per la determinazione degli sconti/maggiorazioni",;
    HelpContextID = 73502375,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=113, Top=62, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ATTSCLIS"

  func oATTSCLIS_1_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGLIS='S'  And Empty(.w_ATTPROLI+.w_ATTPROSC))
    endwith
   endif
  endfunc

  func oATTSCLIS_1_71.mHide()
    with this.Parent.oContained
      return (isAhr())
    endwith
  endfunc

  proc oATTSCLIS_1_71.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oATTSCLIS_1_71.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_71('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTSCLIS_1_71.ecpDrop(oSource)
    this.Parent.oContained.link_1_71('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTSCLIS_1_71.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oATTSCLIS_1_71'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO LISTINI",'listdocu2.LISTINI_VZM',this.parent.oContained
  endproc

  add object oATTPROLI_1_72 as StdField with uid="VRIDBZETVO",rtseq=66,rtrep=.f.,;
    cFormVar = "w_ATTPROLI", cQueryName = "ATTPROLI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice promozione utilizzato per la determinazione del prezzo",;
    HelpContextID = 7638705,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=556, Top=37, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ATTPROLI"

  func oATTPROLI_1_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGLIS='S' And !Empty(.w_ATTCOLIS+.w_ATTSCLIS))
    endwith
   endif
  endfunc

  func oATTPROLI_1_72.mHide()
    with this.Parent.oContained
      return (isAhr())
    endwith
  endfunc

  proc oATTPROLI_1_72.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oATTPROLI_1_72.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_72('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTPROLI_1_72.ecpDrop(oSource)
    this.Parent.oContained.link_1_72('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTPROLI_1_72.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oATTPROLI_1_72'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO PROMOZIONI",'prodocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oATTPROSC_1_73 as StdField with uid="DXITMYNBNF",rtseq=67,rtrep=.f.,;
    cFormVar = "w_ATTPROSC", cQueryName = "ATTPROSC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice promozione utilizzato per la determinazione degli sconti/maggiorazioni",;
    HelpContextID = 260796745,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=556, Top=62, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ATTPROSC"

  func oATTPROSC_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGLIS='S'  And !Empty(.w_ATTCOLIS+.w_ATTSCLIS))
    endwith
   endif
  endfunc

  func oATTPROSC_1_73.mHide()
    with this.Parent.oContained
      return (isAhr())
    endwith
  endfunc

  proc oATTPROSC_1_73.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oATTPROSC_1_73.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_73('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTPROSC_1_73.ecpDrop(oSource)
    this.Parent.oContained.link_1_73('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTPROSC_1_73.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oATTPROSC_1_73'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO PROMOZIONI",'prodocu2.LISTINI_VZM',this.parent.oContained
  endproc

  add object oATCENRIC_1_74 as StdField with uid="GWCBQUFVPT",rtseq=68,rtrep=.f.,;
    cFormVar = "w_ATCENRIC", cQueryName = "ATCENRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di ricavo",;
    HelpContextID = 230727351,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=113, Top=98, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ATCENRIC"

  func oATCENRIC_1_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!((((g_PERCCM<>'S' and Isahe()) or (g_PERCCR<>'S' and Isahr())) and g_PERCAN<>'S') or (IsAhe() and .w_BUFLANAL<>'S' and not empty(g_CODBUN) )))
    endwith
   endif
  endfunc

  func oATCENRIC_1_74.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_74('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCENRIC_1_74.ecpDrop(oSource)
    this.Parent.oContained.link_1_74('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCENRIC_1_74.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oATCENRIC_1_74'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oATCENRIC_1_74.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ATCENRIC
     i_obj.ecpSave()
  endproc

  add object oCCDESRIC_1_75 as StdField with uid="OQBJBQEOLA",rtseq=69,rtrep=.t.,;
    cFormVar = "w_CCDESRIC", cQueryName = "CCDESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 225484695,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=243, Top=98, InputMask=replicate('X',40)

  add object oATCOMRIC_1_76 as StdField with uid="CCYDSSPMTJ",rtseq=70,rtrep=.f.,;
    cFormVar = "w_ATCOMRIC", cQueryName = "ATCOMRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 231120567,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=113, Top=123, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ATCOMRIC"

  func oATCOMRIC_1_76.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_76('Part',this)
      if .not. empty(.w_ATATTRIC)
        bRes2=.link_1_77('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oATCOMRIC_1_76.ecpDrop(oSource)
    this.Parent.oContained.link_1_76('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCOMRIC_1_76.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oATCOMRIC_1_76'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oATCOMRIC_1_76.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_ATCOMRIC
     i_obj.ecpSave()
  endproc

  add object oATATTRIC_1_77 as StdField with uid="LTBYOWJTQX",rtseq=71,rtrep=.f.,;
    cFormVar = "w_ATATTRIC", cQueryName = "ATATTRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 223461047,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=414, Top=123, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_ATCOMRIC", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_ATATTRIC"

  func oATATTRIC_1_77.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ATCOMRIC) AND g_COMM='S')
    endwith
   endif
  endfunc

  func oATATTRIC_1_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_77('Part',this)
    endwith
    return bRes
  endfunc

  proc oATATTRIC_1_77.ecpDrop(oSource)
    this.Parent.oContained.link_1_77('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATATTRIC_1_77.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_ATCOMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_ATCOMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATATTRIC_1_77'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oATATTRIC_1_77.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_ATCOMRIC
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_ATATTRIC
     i_obj.ecpSave()
  endproc

  add object oDESLIS_1_81 as StdField with uid="JVXIADCBCJ",rtseq=72,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 49762870,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=186, Top=37, InputMask=replicate('X',40)

  func oDESLIS_1_81.mHide()
    with this.Parent.oContained
      return (isAhr())
    endwith
  endfunc

  add object oDESLIS2_1_82 as StdField with uid="CFNLLJPOMY",rtseq=73,rtrep=.f.,;
    cFormVar = "w_DESLIS2", cQueryName = "DESLIS2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 49762870,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=186, Top=62, InputMask=replicate('X',40)

  func oDESLIS2_1_82.mHide()
    with this.Parent.oContained
      return (isAhr())
    endwith
  endfunc

  add object oDESDOC_1_88 as StdField with uid="WYMFMTICGX",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 212905418,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=186, Top=13, InputMask=replicate('X',35)

  add object oDESLIS1_1_94 as StdField with uid="NQLYNLLGHW",rtseq=90,rtrep=.f.,;
    cFormVar = "w_DESLIS1", cQueryName = "DESLIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 49762870,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=186, Top=37, InputMask=replicate('X',40)

  func oDESLIS1_1_94.mHide()
    with this.Parent.oContained
      return (isAhe())
    endwith
  endfunc

  add object oATCAUDOC_1_95 as StdField with uid="VMJKURAUFI",rtseq=91,rtrep=.f.,;
    cFormVar = "w_ATCAUDOC", cQueryName = "ATCAUDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale incongruente",;
    HelpContextID = 190095031,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=113, Top=13, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAR_BLT", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_ATCAUDOC"

  func oATCAUDOC_1_95.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATSTATUS<>'P' AND .w_FLNSAP<>'S')
    endwith
   endif
  endfunc

  proc oATCAUDOC_1_95.mBefore
    with this.Parent.oContained
      .w_TDFLVEAC='V'
    endwith
  endproc

  func oATCAUDOC_1_95.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_95('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCAUDOC_1_95.ecpDrop(oSource)
    this.Parent.oContained.link_1_95('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCAUDOC_1_95.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oATCAUDOC_1_95'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oATCAUDOC_1_95.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BLT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_ATCAUDOC
     i_obj.ecpSave()
  endproc

  add object oATINIRIC_1_97 as StdField with uid="MDPLTAQSSU",rtseq=95,rtrep=.f.,;
    cFormVar = "w_ATINIRIC", cQueryName = "ATINIRIC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    HelpContextID = 235355831,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=542, Top=13

  func oATINIRIC_1_97.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ATINIRIC<=.w_ATFINRIC) or (empty(.w_ATFINRIC)))
    endwith
    return bRes
  endfunc

  add object oATFINRIC_1_98 as StdField with uid="BLWWYQKRME",rtseq=96,rtrep=.f.,;
    cFormVar = "w_ATFINRIC", cQueryName = "ATFINRIC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    HelpContextID = 230452919,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=639, Top=13

  func oATFINRIC_1_98.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ATINIRIC))
    endwith
   endif
  endfunc

  func oATFINRIC_1_98.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ATINIRIC<=.w_ATFINRIC) or (empty(.w_ATFINRIC)))
    endwith
    return bRes
  endfunc


  add object oObj_1_101 as cp_runprogram with uid="CNLRVIPUCM",left=-1, top=282, width=234,height=20,;
    caption='GSAG_BM2(H)',;
   bGlobalFont=.t.,;
    prg="GSAG_BM2('H')",;
    cEvent = "w_ATINIRIC Changed, w_ATFINRIC Changed, w_ATINICOS Changed, w_ATFINCOS Changed",;
    nPag=1;
    , HelpContextID = 212591848

  add object oStr_1_78 as StdString with uid="NDNECIPGFN",Visible=.t., Left=17, Top=98,;
    Alignment=1, Width=92, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="HTOLFADPTS",Visible=.t., Left=36, Top=124,;
    Alignment=1, Width=73, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="THZJZYTFBJ",Visible=.t., Left=320, Top=124,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP' )
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="UHOSUHUQRB",Visible=.t., Left=22, Top=62,;
    Alignment=1, Width=90, Height=18,;
    Caption="Listino sc/mag:"  ;
  , bGlobalFont=.t.

  func oStr_1_83.mHide()
    with this.Parent.oContained
      return (isAhr())
    endwith
  endfunc

  add object oStr_1_84 as StdString with uid="KOHASJEEDX",Visible=.t., Left=463, Top=37,;
    Alignment=1, Width=90, Height=18,;
    Caption="Promoz.prezzi:"  ;
  , bGlobalFont=.t.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return (isAhr())
    endwith
  endfunc

  add object oStr_1_85 as StdString with uid="MRGVFXRUTH",Visible=.t., Left=456, Top=62,;
    Alignment=1, Width=97, Height=18,;
    Caption="Promoz.sc/mag:"  ;
  , bGlobalFont=.t.

  func oStr_1_85.mHide()
    with this.Parent.oContained
      return (isAhr())
    endwith
  endfunc

  add object oStr_1_86 as StdString with uid="OVCEGGNPWE",Visible=.t., Left=35, Top=37,;
    Alignment=1, Width=77, Height=18,;
    Caption="Listino prezzi:"  ;
  , bGlobalFont=.t.

  func oStr_1_86.mHide()
    with this.Parent.oContained
      return (isAhr())
    endwith
  endfunc

  add object oStr_1_87 as StdString with uid="ORYDYXJMYN",Visible=.t., Left=35, Top=37,;
    Alignment=1, Width=77, Height=18,;
    Caption="Listino ricavi:"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (isAhe())
    endwith
  endfunc

  add object oStr_1_91 as StdString with uid="NAJQCZPFRY",Visible=.t., Left=8, Top=13,;
    Alignment=1, Width=104, Height=18,;
    Caption="Causale doc. ven.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_99 as StdString with uid="IUNVJDJAID",Visible=.t., Left=446, Top=15,;
    Alignment=1, Width=96, Height=18,;
    Caption="Competenza dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_100 as StdString with uid="TABONCYOUY",Visible=.t., Left=622, Top=15,;
    Alignment=1, Width=18, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oBox_1_9 as StdBox with uid="YFOEMMRHOF",left=3, top=92, width=715,height=2
enddefine
define class tgsag_kdtPag2 as StdContainer
  Width  = 719
  height = 149
  stdWidth  = 719
  stdheight = 149
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATCENCOS_2_1 as StdField with uid="ASYVZTQKNE",rtseq=74,rtrep=.f.,;
    cFormVar = "w_ATCENCOS", cQueryName = "ATCENCOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo",;
    HelpContextID = 213950119,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=113, Top=98, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ATCENCOS"

  func oATCENCOS_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!((IsAhr() and g_ACQU<>'S') Or (IsAhe() AND (g_CACQ<>'S' or (.w_BUFLANAL<>'S' and not empty(g_CODBUN) ) )) or (((g_PERCCM<>'S' and Isahe()) or (g_PERCCR<>'S' and Isahr())) and g_PERCAN<>'S')))
    endwith
   endif
  endfunc

  func oATCENCOS_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCENCOS_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCENCOS_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oATCENCOS_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oATCENCOS_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ATCENCOS
     i_obj.ecpSave()
  endproc

  add object oATCODPRA_2_2 as StdField with uid="LQGIVCDTQM",rtseq=75,rtrep=.f.,;
    cFormVar = "w_ATCODPRA", cQueryName = "ATCODPRA",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 262758727,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=113, Top=123, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ATCODPRA"

  func oATCODPRA_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
      if .not. empty(.w_ATATTCOS)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oATCODPRA_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODPRA_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oATCODPRA_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oATCODPRA_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_ATCODPRA
     i_obj.ecpSave()
  endproc

  add object oATATTCOS_2_3 as StdField with uid="MPATSJEAEP",rtseq=76,rtrep=.f.,;
    cFormVar = "w_ATATTCOS", cQueryName = "ATATTCOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 206683815,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=414, Top=123, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_ATCODPRA", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_ATATTCOS"

  func oATATTCOS_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ATCODPRA) AND g_COMM='S')
    endwith
   endif
  endfunc

  func oATATTCOS_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oATATTCOS_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATATTCOS_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_ATCODPRA)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_ATCODPRA)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATATTCOS_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oATATTCOS_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_ATCODPRA
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_ATATTCOS
     i_obj.ecpSave()
  endproc

  add object oCCDESPIA_2_4 as StdField with uid="ZJDVIETETW",rtseq=77,rtrep=.t.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 259039129,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=243, Top=98, InputMask=replicate('X',40)

  add object oATCAUACQ_2_9 as StdField with uid="HNIFJZHIVT",rtseq=82,rtrep=.f.,;
    cFormVar = "w_ATCAUACQ", cQueryName = "ATCAUACQ",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Causale con intestatario o di tipo incongruente",;
    HelpContextID = 28008791,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=113, Top=13, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAR_BLT", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_ATCAUACQ"

  func oATCAUACQ_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATSTATUS<>'P' AND .w_FLNSAP<>'S')
    endwith
   endif
  endfunc

  func oATCAUACQ_2_9.mHide()
    with this.Parent.oContained
      return (ISALT() or .w_FLANAL='S')
    endwith
  endfunc

  proc oATCAUACQ_2_9.mBefore
    with this.Parent.oContained
      .w_TDFLVEAC='A'
    endwith
  endproc

  func oATCAUACQ_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCAUACQ_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCAUACQ_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oATCAUACQ_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BLT',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oATCAUACQ_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BLT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_ATCAUACQ
     i_obj.ecpSave()
  endproc

  add object oDESACQ_2_10 as StdField with uid="NEPILKJVBG",rtseq=83,rtrep=.f.,;
    cFormVar = "w_DESACQ", cQueryName = "DESACQ",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 9196086,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=186, Top=13, InputMask=replicate('X',35)

  func oDESACQ_2_10.mHide()
    with this.Parent.oContained
      return (.w_FLANAL='S')
    endwith
  endfunc

  add object oATLISACQ_2_14 as StdField with uid="OCSVUAWCKJ",rtseq=86,rtrep=.f.,;
    cFormVar = "w_ATLISACQ", cQueryName = "ATLISACQ",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, listino inesistente o di tipo Iva inclusa o di tipo sconti",;
    ToolTipText = "Codice listino utilizzato per la determinazione costo interno",;
    HelpContextID = 26472791,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=113, Top=37, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ATLISACQ"

  proc oATLISACQ_2_14.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='A'
    endwith
  endproc

  func oATLISACQ_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oATLISACQ_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATLISACQ_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oATLISACQ_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO LISTINI",'listdocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oDESACQ1_2_15 as StdField with uid="ZTTHUCWYOT",rtseq=87,rtrep=.f.,;
    cFormVar = "w_DESACQ1", cQueryName = "DESACQ1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 9196086,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=186, Top=37, InputMask=replicate('X',40)

  add object oATINICOS_2_20 as StdField with uid="SPTEKCVSUB",rtseq=97,rtrep=.f.,;
    cFormVar = "w_ATINICOS", cQueryName = "ATINICOS",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    HelpContextID = 218578599,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=542, Top=13

  func oATINICOS_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ATINICOS<=.w_ATFINCOS) or (empty(.w_ATFINCOS)))
    endwith
    return bRes
  endfunc

  add object oATFINCOS_2_21 as StdField with uid="EBXNVGGEOC",rtseq=98,rtrep=.f.,;
    cFormVar = "w_ATFINCOS", cQueryName = "ATFINCOS",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    HelpContextID = 213675687,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=639, Top=13

  func oATFINCOS_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ATINICOS))
    endwith
   endif
  endfunc

  func oATFINCOS_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ATINICOS<=.w_ATFINCOS) or (empty(.w_ATFINCOS)))
    endwith
    return bRes
  endfunc

  add object oStr_2_5 as StdString with uid="PQYBVXIDQQ",Visible=.t., Left=18, Top=98,;
    Alignment=1, Width=91, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="CCZQHWQSDD",Visible=.t., Left=28, Top=124,;
    Alignment=1, Width=81, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_6.mHide()
    with this.Parent.oContained
      return (Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP' )
    endwith
  endfunc

  add object oStr_2_7 as StdString with uid="PFALGHYVEF",Visible=.t., Left=320, Top=124,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_7.mHide()
    with this.Parent.oContained
      return (Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP' )
    endwith
  endfunc

  add object oStr_2_13 as StdString with uid="PDRPBFFZDR",Visible=.t., Left=5, Top=13,;
    Alignment=1, Width=107, Height=18,;
    Caption="Causale doc. acq.:"  ;
  , bGlobalFont=.t.

  func oStr_2_13.mHide()
    with this.Parent.oContained
      return (.w_FLANAL='S')
    endwith
  endfunc

  add object oStr_2_16 as StdString with uid="ZNXYATLHXB",Visible=.t., Left=30, Top=37,;
    Alignment=1, Width=82, Height=18,;
    Caption="Listino costi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="IPGJITJJBL",Visible=.t., Left=446, Top=15,;
    Alignment=1, Width=96, Height=18,;
    Caption="Competenza dal:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="RZGSDCBOKI",Visible=.t., Left=622, Top=15,;
    Alignment=1, Width=18, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oBox_2_17 as StdBox with uid="HCSNZTBFJQ",left=2, top=92, width=715,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kdt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
