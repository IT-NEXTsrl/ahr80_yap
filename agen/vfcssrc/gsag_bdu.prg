* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bdu                                                        *
*              Duplicazione elementi contratto                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-19                                                      *
* Last revis.: 2015-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAMETER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bdu",oParentObject,m.pPARAMETER)
return(i_retval)

define class tgsag_bdu as StdBatch
  * --- Local variables
  pPARAMETER = space(10)
  w_RECNO = 0
  w_RECCOUNT = 0
  w_ELCONTRA = space(10)
  w_ELCODMOD = space(10)
  w_ELCODIMP = space(10)
  w_ELCODCOM = 0
  w_ELRINNOV = 0
  w_ELPERCON = space(3)
  w_ELCODLIS = space(5)
  w_ELCODART = space(20)
  w_RIGACOMP = 0
  w_ELENCOCODICI = space(10)
  w_SERIAL = space(10)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_DATFINDOC = ctod("  /  /  ")
  w_TIPATT = space(1)
  w_CURSERAT = space(20)
  w_ATTEXIST = .f.
  w_DOCEXIST = .f.
  w_oERRORMESS = .NULL.
  * --- WorkFile variables
  ELE_CONT_idx=0
  DET_GEN_idx=0
  DOC_GENE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiamato dalla duplicazione elementi contratto
    do case
      case this.pPARAMETER = "SELEZ_NOM" OR this.pPARAMETER = "DESEL_NOM" OR this.pPARAMETER = "INVER_NOM"
        * --- Seleziona, deseleziona/Inverte selezione di tutti gli elementi contratto
        SELECT ( this.oParentObject.w_ZOOM.cCURSOR )
        this.w_RECCOUNT = RECCOUNT()
        if this.w_RECCOUNT <> 0
          this.w_RECNO = RECNO()
          UPDATE ( this.oParentObject.w_ZOOM.cCURSOR ) SET XCHK = ICASE(this.pPARAMETER = "SELEZ_NOM", 1, this.pPARAMETER = "DESEL_NOM", 0, IIF(XCHK=1, 0, 1))
          SELECT ( this.oParentObject.w_ZOOM.cCURSOR )
          GOTO this.w_RECNO
        endif
      case this.pPARAMETER = "DUPLICA"
        * --- Rinnova gli elementi contratto
        * --- Controlla che sia stata selezionata almeno una riga
        L_RIGHESELEZIONATE = 0
        SELECT (this.oParentObject.w_ZOOM.cCURSOR )
        this.w_RECCOUNT = RECCOUNT()
        if this.w_RECCOUNT <> 0
          this.w_RECNO = RECNO()
          COUNT FOR XCHK = 1 TO L_RIGHESELEZIONATE
          GOTO this.w_RECNO
        endif
        if L_RIGHESELEZIONATE = 0
          AH_ERRORMSG("Occorre selezionare almeno una riga")
          i_retcode = 'stop'
          return
        endif
        * --- Controlla se per ogni contratto selezionato sono selezionate tutti gli elementi
        * --- Elementi contratto totali per ogni contratto
        SELECT COUNT(*) AS QUAMMULTI_TOT, ELCONTRA AS ELCONTRA_TOT FROM ( this.oParentObject.w_ZOOM.cCURSOR ) GROUP BY ELCONTRA INTO CURSOR CHECKED_ELCONTRA_TOT
        * --- Elementi contratto selezionati per ogni contratto
        SELECT COUNT(*) AS QUAMMULTI_CHK, ELCONTRA AS ELCONTRA_CHK FROM ( this.oParentObject.w_ZOOM.cCURSOR ) GROUP BY ELCONTRA WHERE XCHK = 1 INTO CURSOR CHECKED_ELCONTRA_CHK
        * --- Costruisce un cursore per confrontare i dati
        SELECT * FROM CHECKED_ELCONTRA_TOT INNER JOIN CHECKED_ELCONTRA_CHK ; 
 ON ELCONTRA_TOT=ELCONTRA_CHK INTO CURSOR ELCONTRA_2
        this.w_ELENCOCODICI = ""
        SELECT ELCONTRA_2
        GO TOP
        SCAN
        if NVL( QUAMMULTI_CHK, 0 ) <> NVL( QUAMMULTI_TOT, 0 )
          if NOT EMPTY( this.w_ELENCOCODICI )
            this.w_ELENCOCODICI = this.w_ELENCOCODICI + " - " + ALLTRIM( ELCONTRA_CHK )
          else
            this.w_ELENCOCODICI = ALLTRIM( ELCONTRA_CHK )
          endif
        endif
        ENDSCAN
        USE IN SELECT( "CHECKED_ELCONTRA_CHK" )
        USE IN SELECT( "CHECKED_ELCONTRA_TOT" )
        USE IN SELECT( "ELCONTRA_2" )
        if NOT EMPTY( this.w_ELENCOCODICI ) AND NOT AH_YESNO("Uno o pi� elementi relativi a contratti da rinnovare (contratti: %1) non sono stati selezionati.%0Proseguire ugualmente con il rinnovo?", , this.w_ELENCOCODICI )
          i_retcode = 'stop'
          return
        endif
        * --- Rinnova gli elementi contratto
        SELECT ( this.oParentObject.w_ZOOM.cCURSOR )
        GO TOP
        do while NOT EOF()
          if XCHK = 1
            this.w_ELCONTRA = ELCONTRA
            this.w_ELCODMOD = ELCODMOD
            this.w_ELCODIMP = ELCODIMP
            this.w_ELCODCOM = ELCODCOM
            this.w_ELRINNOV = NVL( ELRINNOV, 0 )
            this.w_ELPERCON = ELPERCON
            this.w_ELCODLIS = ELCODLIS
            this.w_ELCODART = ELCODART
            * --- Tipo di ricalcolo
            * --- Percentuale di ricalcolo
            * --- Moltiplicatori
            * --- Ricalcolo in valore
            * --- Arrotondamenti e relativi importi
            * --- Flag data fissa
            * --- Rinnova l'elemento senza apirire la finestra di dialogo
            GSAG_BD4(this,"S",this.w_ELCONTRA, this.w_ELCODMOD, this.w_ELCODIMP, this.w_ELCODCOM, this.w_ELRINNOV, this.w_ELPERCON, this.oParentObject.w_RICPE1, this.oParentObject.w_RICVA1, this.oParentObject.w_ARROT1, this.oParentObject.w_VALORIN, this.oParentObject.w_ARROT2, this.oParentObject.w_VALOR2IN, this.oParentObject.w_ARROT3, this.oParentObject.w_VALOR3IN, this.oParentObject.w_ARROT4,this.oParentObject.w_FLDATFIS)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          SKIP
        enddo
        this.oParentObject.NotifyEvent("Search")
        AH_ERRORMSG( "Operazione completata" )
      case  this.pPARAMETER = "ELIMINA"
        this.w_oERRORMESS=createobject("AH_ErrorLog")
        this.w_TIPATT = ""
        this.w_DATINI = CP_CHARTODATE("  -  -  ")
        this.w_DATFINDOC = CP_CHARTODATE("  -  -  ")
        this.w_DATFIN = CP_CHARTODATE("  -  -  ")
        this.w_CURSERAT = SPACE(20)
        * --- Rinnova gli elementi contratto
        * --- Controlla che sia stata selezionata almeno una riga
        L_RIGHESELEZIONATE = 0
        SELECT (this.oParentObject.w_ZOOM.cCURSOR )
        this.w_RECCOUNT = RECCOUNT()
        if this.w_RECCOUNT <> 0
          this.w_RECNO = RECNO()
          COUNT FOR XCHK = 1 TO L_RIGHESELEZIONATE
          GOTO this.w_RECNO
        endif
        if L_RIGHESELEZIONATE = 0
          AH_ERRORMSG("Occorre selezionare almeno una riga")
          i_retcode = 'stop'
          return
        endif
        if !AH_YESNO("Attenzione: gli elementi selezionati non collegati a documenti/attivit� saranno eliminati. Procedere con l'eliminazione?")
          i_retcode = 'stop'
          return
        endif
        SELECT ( this.oParentObject.w_ZOOM.cCURSOR )
        GO TOP
        SCAN FOR XCHK=1
        this.w_DOCEXIST = .F.
        this.w_ATTEXIST = .F.
        this.w_SERIAL = ELCONTRA
        this.w_ELCODMOD = ELCODMOD
        this.w_ELCODIMP = ELCODIMP
        this.w_ELCODCOM = ELCODCOM
        this.w_ELRINNOV = NVL( ELRINNOV, 0 )
        this.w_RIGACOMP = CPROWORD
        * --- Try
        local bErr_0404A5D8
        bErr_0404A5D8=bTrsErr
        this.Try_0404A5D8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          this.w_oERRORMESS.AddMsgLog("L'elemento relativo a contratto %1, modello %2, impianto %3, rinnovo %4 non pu� essere eliminato perch� collegato ad almeno un'attivit�/documento.", ALLTRIM(this.w_SERIAL),ALLTRIM(this.w_ELCODMOD),iif(empty(ALLTRIM(this.w_ELCODIMP)),"non valorizzato",ALLTRIM(this.w_ELCODIMP) +", componente "+ ALLTRIM(STR(NVL(this.w_RIGACOMP,0)))),ALLTRIM(STR(this.w_ELRINNOV)))     
        endif
        bTrsErr=bTrsErr or bErr_0404A5D8
        * --- End
        ENDSCAN
        this.w_oERRORMESS.PrintLog(this,"Errori /warning riscontrati")     
        this.oParentObject.NotifyEvent("Search")
        AH_ERRORMSG( "Operazione completata" )
    endcase
  endproc
  proc Try_0404A5D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Delete from ELE_CONT
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".ELCONTRA = "+i_cQueryTable+".ELCONTRA";
            +" and "+i_cTable+".ELCODMOD = "+i_cQueryTable+".ELCODMOD";
            +" and "+i_cTable+".ELCODIMP = "+i_cQueryTable+".ELCODIMP";
            +" and "+i_cTable+".ELCODCOM = "+i_cQueryTable+".ELCODCOM";
            +" and "+i_cTable+".ELRINNOV = "+i_cQueryTable+".ELRINNOV";
    
      do vq_exec with 'GSAG_BDU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if i_Rows=0
      this.w_oERRORMESS.AddMsgLog("L'elemento relativo a contratto %1, modello %2, impianto %3, rinnovo %4 non pu� essere eliminato perch� collegato ad almeno un'attivit�/documento.", ALLTRIM(this.w_SERIAL),ALLTRIM(this.w_ELCODMOD),iif(empty(ALLTRIM(this.w_ELCODIMP)),"non valorizzato",ALLTRIM(this.w_ELCODIMP) +", componente "+ ALLTRIM(STR(NVL(this.w_RIGACOMP,0)))),ALLTRIM(STR(this.w_ELRINNOV)))     
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pPARAMETER)
    this.pPARAMETER=pPARAMETER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ELE_CONT'
    this.cWorkTables[2]='DET_GEN'
    this.cWorkTables[3]='DOC_GENE'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAMETER"
endproc
