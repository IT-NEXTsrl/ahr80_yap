* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kaa                                                        *
*              Aggiornamento gruppo attivit� e tipi attivit�                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-05                                                      *
* Last revis.: 2012-04-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kaa",oParentObject))

* --- Class definition
define class tgsag_kaa as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 465
  Height = 212
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-04-12"
  HelpContextID=135649431
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_kaa"
  cComment = "Aggiornamento gruppo attivit� e tipi attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_TIPAGG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kaaPag1","gsag_kaa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAG_BAA with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TIPAGG=space(1)
        .w_DATINI = i_inidat
        .w_DATFIN = i_findat
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate(Ah_msgformat("Attraverso questa funzionalit� � possibile aggiornare il gruppo%0delle persone presenti nelle attivit� gi� esistenti con il gruppo%0predefinito associato alla persona stessa."))
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate(Ah_msgformat("Oppure verr� aggiornato il gruppo nel dettaglio partecipanti %0dei tipi attivit� qualora fosse vuoto."))
        .w_TIPAGG = 'A'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(Ah_msgformat("Attraverso questa funzionalit� � possibile aggiornare il gruppo%0delle persone presenti nelle attivit� gi� esistenti con il gruppo%0predefinito associato alla persona stessa."))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(Ah_msgformat("Oppure verr� aggiornato il gruppo nel dettaglio partecipanti %0dei tipi attivit� qualora fosse vuoto."))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate(Ah_msgformat("Attraverso questa funzionalit� � possibile aggiornare il gruppo%0delle persone presenti nelle attivit� gi� esistenti con il gruppo%0predefinito associato alla persona stessa."))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(Ah_msgformat("Oppure verr� aggiornato il gruppo nel dettaglio partecipanti %0dei tipi attivit� qualora fosse vuoto."))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDATINI_1_1.visible=!this.oPgFrm.Page1.oPag.oDATINI_1_1.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_2.visible=!this.oPgFrm.Page1.oPag.oStr_1_2.mHide()
    this.oPgFrm.Page1.oPag.oDATFIN_1_3.visible=!this.oPgFrm.Page1.oPag.oDATFIN_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_1.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_3.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_3.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPAGG_1_9.RadioValue()==this.w_TIPAGG)
      this.oPgFrm.Page1.oPag.oTIPAGG_1_9.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATINI>= i_IniDat AND .w_DATINI<= i_FinDat AND (EMPTY(.w_DATFIN) OR .w_DATINI<=.w_DATFIN))  and not(.w_TIPAGG='T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale deve essere maggiore del 01/01/1900 e minore della data finale")
          case   not(.w_DATFIN>= i_IniDat AND .w_DATFIN<= i_FinDat  AND (EMPTY(.w_DATINI) OR .w_DATINI<=.w_DATFIN))  and not(.w_TIPAGG='T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale deve essere maggiore del 01/01/1900 e della data iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_kaaPag1 as StdContainer
  Width  = 461
  height = 212
  stdWidth  = 461
  stdheight = 212
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_1 as StdField with uid="TXWZLOUBLY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale deve essere maggiore del 01/01/1900 e minore della data finale",;
    ToolTipText = "Data iniziale aggiornamento attivit�",;
    HelpContextID = 163291594,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=233, Top=152, bHasZoom = .t. 

  func oDATINI_1_1.mHide()
    with this.Parent.oContained
      return (.w_TIPAGG='T')
    endwith
  endfunc

  func oDATINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI>= i_IniDat AND .w_DATINI<= i_FinDat AND (EMPTY(.w_DATFIN) OR .w_DATINI<=.w_DATFIN))
    endwith
    return bRes
  endfunc

  proc oDATINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oDATINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDATFIN_1_3 as StdField with uid="JLBGFFUEDW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale deve essere maggiore del 01/01/1900 e della data iniziale",;
    ToolTipText = "Data finale aggiornamento attivit�",;
    HelpContextID = 84845002,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=233, Top=176, bHasZoom = .t. 

  func oDATFIN_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPAGG='T')
    endwith
  endfunc

  func oDATFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFIN>= i_IniDat AND .w_DATFIN<= i_FinDat  AND (EMPTY(.w_DATINI) OR .w_DATINI<=.w_DATFIN))
    endwith
    return bRes
  endfunc

  proc oDATFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oDATFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oBtn_1_5 as StdButton with uid="UUNSDLSMLD",left=348, top=153, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 135678182;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_6 as StdButton with uid="TKCMXZXILU",left=401, top=153, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 142966854;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_7 as cp_calclbl with uid="SJLXCEWQCB",left=10, top=5, width=442,height=39,;
    caption='La procedura permette di salvare il dizionario dati corrente in un database di servizio',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 237988961


  add object oObj_1_8 as cp_calclbl with uid="ANDAUEJCFA",left=10, top=45, width=442,height=73,;
    caption='La procedura permette di salvare il dizionario dati corrente in un database di servizio',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 237988961


  add object oTIPAGG_1_9 as StdCombo with uid="YNKPIYRFUS",rtseq=3,rtrep=.f.,left=121,top=125,width=166,height=22;
    , HelpContextID = 204724426;
    , cFormVar="w_TIPAGG",RowSource=""+"Attivit�,"+"Tipi attivit�", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPAGG_1_9.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTIPAGG_1_9.GetRadio()
    this.Parent.oContained.w_TIPAGG = this.RadioValue()
    return .t.
  endfunc

  func oTIPAGG_1_9.SetRadio()
    this.Parent.oContained.w_TIPAGG=trim(this.Parent.oContained.w_TIPAGG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPAGG=='A',1,;
      iif(this.Parent.oContained.w_TIPAGG=='T',2,;
      0))
  endfunc

  add object oStr_1_2 as StdString with uid="GCRHKMPKLR",Visible=.t., Left=11, Top=154,;
    Alignment=1, Width=215, Height=18,;
    Caption="Data inizio aggiornamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPAGG='T')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="LANQLYXHUF",Visible=.t., Left=12, Top=178,;
    Alignment=1, Width=215, Height=18,;
    Caption="Data fine aggiornamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPAGG='T')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="ANBNFTLKUP",Visible=.t., Left=-101, Top=127,;
    Alignment=1, Width=215, Height=18,;
    Caption="Aggiornamento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kaa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
