* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kac                                                        *
*              Attivit� collegate                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-03-07                                                      *
* Last revis.: 2009-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kac",oParentObject))

* --- Class definition
define class tgsag_kac as StdForm
  Top    = 37
  Left   = 46

  * --- Standard Properties
  Width  = 642
  Height = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-10-12"
  HelpContextID=132786025
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_kac"
  cComment = "Attivit� collegate"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SERIAL = space(20)
  w_DATA_INI = ctot('')
  w_CAITER = space(20)
  w_DESC_ATT = space(254)
  w_DES_PRAT = space(100)
  w_ATSERIAL = space(20)
  w_AGKAC_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kacPag1","gsag_kac",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGKAC_ZOOM = this.oPgFrm.Pages(1).oPag.AGKAC_ZOOM
    DoDefault()
    proc Destroy()
      this.w_AGKAC_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SERIAL=space(20)
      .w_DATA_INI=ctot("")
      .w_CAITER=space(20)
      .w_DESC_ATT=space(254)
      .w_DES_PRAT=space(100)
      .w_ATSERIAL=space(20)
      .w_SERIAL=oParentObject.w_SERIAL
      .w_DATA_INI=oParentObject.w_DATA_INI
      .w_CAITER=oParentObject.w_CAITER
      .w_DESC_ATT=oParentObject.w_DESC_ATT
      .w_DES_PRAT=oParentObject.w_DES_PRAT
      .oPgFrm.Page1.oPag.AGKAC_ZOOM.Calculate()
          .DoRTCalc(1,5,.f.)
        .w_ATSERIAL = .w_AGKAC_ZOOM.GetVar('ATSERIAL')
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SERIAL=.w_SERIAL
      .oParentObject.w_DATA_INI=.w_DATA_INI
      .oParentObject.w_CAITER=.w_CAITER
      .oParentObject.w_DESC_ATT=.w_DESC_ATT
      .oParentObject.w_DES_PRAT=.w_DES_PRAT
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.AGKAC_ZOOM.Calculate()
        .DoRTCalc(1,5,.t.)
            .w_ATSERIAL = .w_AGKAC_ZOOM.GetVar('ATSERIAL')
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AGKAC_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDES_PRAT_1_6.visible=!this.oPgFrm.Page1.oPag.oDES_PRAT_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AGKAC_ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATA_INI_1_2.value==this.w_DATA_INI)
      this.oPgFrm.Page1.oPag.oDATA_INI_1_2.value=this.w_DATA_INI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC_ATT_1_5.value==this.w_DESC_ATT)
      this.oPgFrm.Page1.oPag.oDESC_ATT_1_5.value=this.w_DESC_ATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDES_PRAT_1_6.value==this.w_DES_PRAT)
      this.oPgFrm.Page1.oPag.oDES_PRAT_1_6.value=this.w_DES_PRAT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_kacPag1 as StdContainer
  Width  = 638
  height = 413
  stdWidth  = 638
  stdheight = 413
  resizeXpos=456
  resizeYpos=203
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATA_INI_1_2 as StdField with uid="QCITKHXZZR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATA_INI", cQueryName = "DATA_INI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 145990017,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=483, Top=17


  add object AGKAC_ZOOM as cp_zoombox with uid="WODZOSNAJX",left=5, top=81, width=625,height=277,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_ATTI",cZoomFile="GSAG_KAC",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 45211622

  add object oDESC_ATT_1_5 as StdField with uid="CJTQQCRMVY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESC_ATT", cQueryName = "DESC_ATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 256791178,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=126, Top=17, InputMask=replicate('X',254)

  add object oDES_PRAT_1_6 as StdField with uid="XNUFOXWBGI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DES_PRAT", cQueryName = "DES_PRAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 8760694,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=126, Top=44, InputMask=replicate('X',100)

  func oDES_PRAT_1_6.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oObj_1_11 as cp_runprogram with uid="UAIOGLNCRD",left=62, top=450, width=248,height=28,;
    caption='Doppio click',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('V')",;
    cEvent = "w_AGKAC_ZOOM selected",;
    nPag=1;
    , HelpContextID = 244247641


  add object oBtn_1_12 as StdButton with uid="ARHYBOCNYM",left=8, top=365, width=48,height=45,;
    CpPicture="bmp\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare l'attivit� selezionata";
    , HelpContextID = 125407994;
    , caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSAG_BAD(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ATSERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="TKCMXZXILU",left=580, top=365, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 125468602;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="PFVPIBIGVQ",Visible=.t., Left=10, Top=17,;
    Alignment=1, Width=110, Height=18,;
    Caption="Attivit� di partenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="COTKAPENGA",Visible=.t., Left=451, Top=17,;
    Alignment=1, Width=30, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="FGAPFHFYQF",Visible=.t., Left=71, Top=44,;
    Alignment=1, Width=49, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kac','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
