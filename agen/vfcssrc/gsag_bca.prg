* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bca                                                        *
*              Carica prestazioni in attivita                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-30                                                      *
* Last revis.: 2007-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bca",oParentObject)
return(i_retval)

define class tgsag_bca as StdBatch
  * --- Local variables
  w_Obj = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tipi attivit� - Sbiancamento prestazioni
    * --- Padre
    this.w_Obj = this.oParentObject
    if this.oParentObject.w_CAFLNSAP="S"
      * --- Se tipo attivit� non soggetta a prestazione
      SELECT (this.w_Obj.cTrsName)
      if reccount()<>0
        * --- Azzera Righe prestazioni
        this.w_Obj.MarkPos()     
        this.w_Obj.FirstRow()     
        do while ! this.w_Obj.Eof_Trs()
          this.w_Obj.DeleteRow()     
          this.w_Obj.NextRow()     
        enddo
        this.w_Obj.RePos()     
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
