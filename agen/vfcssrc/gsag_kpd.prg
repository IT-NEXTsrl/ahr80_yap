* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kpd                                                        *
*              Inserisci prestazione                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-07                                                      *
* Last revis.: 2013-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kpd",oParentObject))

* --- Class definition
define class tgsag_kpd as StdForm
  Top    = 14
  Left   = 8

  * --- Standard Properties
  Width  = 612
  Height = 396
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-11-18"
  HelpContextID=149563241
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=91

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  KEY_ARTI_IDX = 0
  DIPENDEN_IDX = 0
  ART_ICOL_IDX = 0
  CAUMATTI_IDX = 0
  PAR_AGEN_IDX = 0
  UNIMIS_IDX = 0
  TIP_DOCU_IDX = 0
  PAR_ALTE_IDX = 0
  CENCOST_IDX = 0
  ATTIVITA_IDX = 0
  OFF_NOMI_IDX = 0
  IMP_DETT_IDX = 0
  BUSIUNIT_IDX = 0
  cPrg = "gsag_kpd"
  cComment = "Inserisci prestazione"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_LetturaParAgen = space(5)
  w_DAGEST = .F.
  w_CodPart = space(5)
  o_CodPart = space(5)
  w_Leggialte = space(10)
  w_NOEDES = space(30)
  w_SerUtente = space(10)
  w_FLGZER = space(1)
  w_ATCAUATT = space(20)
  o_ATCAUATT = space(20)
  w_FLNSAP = space(10)
  w_DTIPRIG = space(1)
  w_CODSER = space(20)
  o_CODSER = space(20)
  w_CACODART = space(20)
  o_CACODART = space(20)
  w_DesSer = space(40)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_DesAgg = space(0)
  w_DAUNIMIS = space(3)
  w_DAQTAMOV = 0
  w_DAPREZZO = 0
  w_DACOSUNI = 0
  w_DATIPRIG = space(1)
  w_DATIPRI2 = space(1)
  w_INSPRDP = space(1)
  w_FLACQ = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_TIPOENTE = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_TxtSel = space(10)
  w_UniMisTempo = space(10)
  w_ConversioneOre = 0
  w_DAVOCCOS = space(15)
  w_FLGPZCO = space(10)
  o_FLGPZCO = space(10)
  w_DAVOCRIC = space(15)
  w_DACENCOS = space(15)
  w_DACODCOM = space(15)
  w_DAATTIVI = space(15)
  w_CAUDOC = space(5)
  w_DA_SEGNO = space(1)
  w_DAINICOM = ctod('  /  /  ')
  w_DAFINCOM = ctod('  /  /  ')
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_FLDANA = space(1)
  w_VOCECR = space(1)
  w_DASCONT1 = 0
  w_DASCONT2 = 0
  w_DASCONT3 = 0
  w_DASCONT4 = 0
  w_GENEVE = space(1)
  w_CAUACQ = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_CENOBSO = ctod('  /  /  ')
  w_TIPATT = space(10)
  w_CAUATT = space(10)
  w_ARTIPRIG = space(1)
  w_ARTIPRI2 = space(1)
  w_ATSERIAL = space(20)
  w_PrestRowOrd = 0
  w_TIPART = space(2)
  w_FL_FRAZ = space(1)
  w_CHKTEMP = space(1)
  w_TIPSER = space(1)
  w_UNMIS3 = space(3)
  w_FLSERG = space(1)
  w_ROWNUM = 0
  w_DACONTRA = space(10)
  w_DACODMOD = space(10)
  w_DACODIMP = space(10)
  w_CODCLI = space(15)
  w_DACODATT = space(20)
  w_DARINNOV = 0
  w_DAPREZZO = 0
  w_DACONCOD = space(10)
  w_DACOCOMP = 0
  w_ATCODPRA = space(15)
  w_ATDATINI = ctot('')
  w_CNDESCAN = space(80)
  w_DUR_ORE = 0
  w_DACODATT = space(20)
  w_DADESATT = space(40)
  w_DAOREEFF = 0
  w_DAMINEFF = 0
  w_PRESTA = space(1)
  w_FLPRES = space(1)
  w_CHKDATAPRE = space(1)
  w_GG_PRE = 0
  w_GG_SUC = 0
  w_DATAPREC = ctod('  /  /  ')
  w_DATASUCC = ctod('  /  /  ')
  w_DACENRIC = space(15)
  w_TIPOPRE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kpdPag1","gsag_kpd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODSER_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='DIPENDEN'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='CAUMATTI'
    this.cWorkTables[6]='PAR_AGEN'
    this.cWorkTables[7]='UNIMIS'
    this.cWorkTables[8]='TIP_DOCU'
    this.cWorkTables[9]='PAR_ALTE'
    this.cWorkTables[10]='CENCOST'
    this.cWorkTables[11]='ATTIVITA'
    this.cWorkTables[12]='OFF_NOMI'
    this.cWorkTables[13]='IMP_DETT'
    this.cWorkTables[14]='BUSIUNIT'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAG_BPD with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LetturaParAgen=space(5)
      .w_DAGEST=.f.
      .w_CodPart=space(5)
      .w_Leggialte=space(10)
      .w_NOEDES=space(30)
      .w_SerUtente=space(10)
      .w_FLGZER=space(1)
      .w_ATCAUATT=space(20)
      .w_FLNSAP=space(10)
      .w_DTIPRIG=space(1)
      .w_CODSER=space(20)
      .w_CACODART=space(20)
      .w_DesSer=space(40)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_DesAgg=space(0)
      .w_DAUNIMIS=space(3)
      .w_DAQTAMOV=0
      .w_DAPREZZO=0
      .w_DACOSUNI=0
      .w_DATIPRIG=space(1)
      .w_DATIPRI2=space(1)
      .w_INSPRDP=space(1)
      .w_FLACQ=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPOENTE=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_TxtSel=space(10)
      .w_UniMisTempo=space(10)
      .w_ConversioneOre=0
      .w_DAVOCCOS=space(15)
      .w_FLGPZCO=space(10)
      .w_DAVOCRIC=space(15)
      .w_DACENCOS=space(15)
      .w_DACODCOM=space(15)
      .w_DAATTIVI=space(15)
      .w_CAUDOC=space(5)
      .w_DA_SEGNO=space(1)
      .w_DAINICOM=ctod("  /  /  ")
      .w_DAFINCOM=ctod("  /  /  ")
      .w_FLANAL=space(1)
      .w_FLGCOM=space(1)
      .w_FLDANA=space(1)
      .w_VOCECR=space(1)
      .w_DASCONT1=0
      .w_DASCONT2=0
      .w_DASCONT3=0
      .w_DASCONT4=0
      .w_GENEVE=space(1)
      .w_CAUACQ=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_CENOBSO=ctod("  /  /  ")
      .w_TIPATT=space(10)
      .w_CAUATT=space(10)
      .w_ARTIPRIG=space(1)
      .w_ARTIPRI2=space(1)
      .w_ATSERIAL=space(20)
      .w_PrestRowOrd=0
      .w_TIPART=space(2)
      .w_FL_FRAZ=space(1)
      .w_CHKTEMP=space(1)
      .w_TIPSER=space(1)
      .w_UNMIS3=space(3)
      .w_FLSERG=space(1)
      .w_ROWNUM=0
      .w_DACONTRA=space(10)
      .w_DACODMOD=space(10)
      .w_DACODIMP=space(10)
      .w_CODCLI=space(15)
      .w_DACODATT=space(20)
      .w_DARINNOV=0
      .w_DAPREZZO=0
      .w_DACONCOD=space(10)
      .w_DACOCOMP=0
      .w_ATCODPRA=space(15)
      .w_ATDATINI=ctot("")
      .w_CNDESCAN=space(80)
      .w_DUR_ORE=0
      .w_DACODATT=space(20)
      .w_DADESATT=space(40)
      .w_DAOREEFF=0
      .w_DAMINEFF=0
      .w_PRESTA=space(1)
      .w_FLPRES=space(1)
      .w_CHKDATAPRE=space(1)
      .w_GG_PRE=0
      .w_GG_SUC=0
      .w_DATAPREC=ctod("  /  /  ")
      .w_DATASUCC=ctod("  /  /  ")
      .w_DACENRIC=space(15)
      .w_TIPOPRE=space(1)
      .w_CodPart=oParentObject.w_CodPart
      .w_ATCAUATT=oParentObject.w_ATCAUATT
      .w_ATSERIAL=oParentObject.w_ATSERIAL
      .w_PrestRowOrd=oParentObject.w_PrestRowOrd
      .w_ATCODPRA=oParentObject.w_ATCODPRA
      .w_ATDATINI=oParentObject.w_ATDATINI
      .w_CNDESCAN=oParentObject.w_CNDESCAN
      .w_DACODATT=oParentObject.w_DACODATT
      .w_DADESATT=oParentObject.w_DADESATT
      .w_TIPOPRE=oParentObject.w_TIPOPRE
        .w_LetturaParAgen = i_CodAzi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_LetturaParAgen))
          .link_1_1('Full')
        endif
        .w_DAGEST = .f.
        .w_CodPart = IIF(ISALT(), ReadResp(i_codute, "C",.w_ATCODPRA), ReadDipend(i_codute, "C") )
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CodPart))
          .link_1_3('Full')
        endif
        .w_Leggialte = i_CodAzi
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_Leggialte))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,8,.f.)
        if not(empty(.w_ATCAUATT))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,11,.f.)
        if not(empty(.w_CODSER))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CACODART))
          .link_1_12('Full')
        endif
        .DoRTCalc(13,14,.f.)
        if not(empty(.w_UNMIS1))
          .link_1_14('Full')
        endif
          .DoRTCalc(15,16,.f.)
        .w_DAUNIMIS = .w_UNMIS1
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_DAUNIMIS))
          .link_1_17('Full')
        endif
        .w_DAQTAMOV = IIF(.w_TIPART='DE', 0, 1)
        .w_DAPREZZO = 0
        .w_DACOSUNI = 0
        .w_DATIPRIG = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N', Not empty(.w_DATIPRIG),.w_DATIPRIG,.w_ARTIPRIG)
        .w_DATIPRI2 = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', Not empty(.w_DATIPRI2),.w_DATIPRI2,.w_ARTIPRI2)
        .w_INSPRDP = 'D'
        .w_FLACQ = IIF(Not Empty(.w_CAUACQ),Docgesana(.w_CAUACQ,'A'),.w_FLANAL)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(26,26,.f.)
        .w_OB_TEST = i_INIDAT
        .DoRTCalc(28,37,.f.)
        if not(empty(.w_CAUDOC))
          .link_1_41('Full')
        endif
        .w_DA_SEGNO = 'D'
          .DoRTCalc(39,41,.f.)
        .w_FLGCOM = Docgesana(.w_CAUDOC,'C')
        .w_FLDANA = Docgesana(.w_CAUDOC,'A')
        .w_VOCECR = Docgesana(.w_CAUDOC,'V')
          .DoRTCalc(45,48,.f.)
        .w_GENEVE = 'N'
          .DoRTCalc(50,52,.f.)
        .w_TIPATT = 'A'
          .DoRTCalc(54,87,.f.)
        .w_DATAPREC = Date() - .w_GG_PRE
        .w_DATASUCC = Date() + .w_GG_SUC
    endwith
    this.DoRTCalc(90,91,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CodPart=.w_CodPart
      .oParentObject.w_ATCAUATT=.w_ATCAUATT
      .oParentObject.w_ATSERIAL=.w_ATSERIAL
      .oParentObject.w_PrestRowOrd=.w_PrestRowOrd
      .oParentObject.w_ATCODPRA=.w_ATCODPRA
      .oParentObject.w_ATDATINI=.w_ATDATINI
      .oParentObject.w_CNDESCAN=.w_CNDESCAN
      .oParentObject.w_DACODATT=.w_DACODATT
      .oParentObject.w_DADESATT=.w_DADESATT
      .oParentObject.w_TIPOPRE=.w_TIPOPRE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
          .link_1_4('Full')
        .DoRTCalc(5,7,.t.)
          .link_1_8('Full')
        .DoRTCalc(9,11,.t.)
        if .o_CODSER<>.w_CODSER.or. .o_CACODART<>.w_CACODART
          .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.t.)
        if .o_CODSER<>.w_CODSER
          .link_1_14('Full')
        endif
        .DoRTCalc(15,16,.t.)
        if .o_CODSER<>.w_CODSER
            .w_DAUNIMIS = .w_UNMIS1
          .link_1_17('Full')
        endif
        .DoRTCalc(18,18,.t.)
        if .o_FLGPZCO<>.w_FLGPZCO
            .w_DAPREZZO = 0
        endif
        if .o_FLGPZCO<>.w_FLGPZCO
            .w_DACOSUNI = 0
        endif
        if .o_CODSER<>.w_CODSER
            .w_DATIPRIG = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N', Not empty(.w_DATIPRIG),.w_DATIPRIG,.w_ARTIPRIG)
        endif
        if .o_CODSER<>.w_CODSER
            .w_DATIPRI2 = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', Not empty(.w_DATIPRI2),.w_DATIPRI2,.w_ARTIPRI2)
        endif
        .DoRTCalc(23,23,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_FLACQ = IIF(Not Empty(.w_CAUACQ),Docgesana(.w_CAUACQ,'A'),.w_FLANAL)
        endif
        .DoRTCalc(25,36,.t.)
          .link_1_41('Full')
        .DoRTCalc(38,41,.t.)
            .w_FLGCOM = Docgesana(.w_CAUDOC,'C')
            .w_FLDANA = Docgesana(.w_CAUDOC,'A')
            .w_VOCECR = Docgesana(.w_CAUDOC,'V')
        .DoRTCalc(45,52,.t.)
            .w_TIPATT = 'A'
        .DoRTCalc(54,87,.t.)
        if .o_CodPart<>.w_CodPart
            .w_DATAPREC = Date() - .w_GG_PRE
        endif
        if .o_CodPart<>.w_CodPart
            .w_DATASUCC = Date() + .w_GG_SUC
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(90,91,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_LDKASKAOZX()
    with this
          * --- Ricalcolo durata eff. (ore e minuti)
          .w_CACODART = .w_CACODART
          .link_1_12('Full')
          .w_DAOREEFF = Min(IIF(.w_CHKTEMP='S', INT(.w_DUR_ORE * .w_DAQTAMOV), .w_DAOREEFF),999)
          .w_DAMINEFF = IIF(.w_CHKTEMP='S', INT(cp_Round((.w_DUR_ORE * .w_DAQTAMOV - INT(.w_DUR_ORE * .w_DAQTAMOV)) * 60,0)), .w_DAMINEFF)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDesSer_1_13.enabled = this.oPgFrm.Page1.oPag.oDesSer_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDAUNIMIS_1_17.enabled = this.oPgFrm.Page1.oPag.oDAUNIMIS_1_17.mCond()
    this.oPgFrm.Page1.oPag.oDAQTAMOV_1_18.enabled = this.oPgFrm.Page1.oPag.oDAQTAMOV_1_18.mCond()
    this.oPgFrm.Page1.oPag.oDAPREZZO_1_19.enabled = this.oPgFrm.Page1.oPag.oDAPREZZO_1_19.mCond()
    this.oPgFrm.Page1.oPag.oDACOSUNI_1_20.enabled = this.oPgFrm.Page1.oPag.oDACOSUNI_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODSER_1_11.visible=!this.oPgFrm.Page1.oPag.oCODSER_1_11.mHide()
    this.oPgFrm.Page1.oPag.oDesSer_1_13.visible=!this.oPgFrm.Page1.oPag.oDesSer_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDesAgg_1_16.visible=!this.oPgFrm.Page1.oPag.oDesAgg_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_32.visible=!this.oPgFrm.Page1.oPag.oBtn_1_32.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_37.visible=!this.oPgFrm.Page1.oPag.oBtn_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Calcola") or lower(cEvent)==lower("w_DAQTAMOV Changed") or lower(cEvent)==lower("w_DAUNIMIS Changed") or lower(cEvent)==lower("w_CODSER Changed")
          .Calculate_LDKASKAOZX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kpd
    If Cevent='w_CODSER Changed'
       This.Notifyevent('Calcola')
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LetturaParAgen
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LetturaParAgen) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LetturaParAgen)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLPRES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LetturaParAgen);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LetturaParAgen)
            select PACODAZI,PAFLPRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LetturaParAgen = NVL(_Link_.PACODAZI,space(5))
      this.w_FLPRES = NVL(_Link_.PAFLPRES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LetturaParAgen = space(5)
      endif
      this.w_FLPRES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LetturaParAgen Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodPart
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodPart) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodPart)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPSERPRE,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CodPart);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CodPart)
            select DPCODICE,DPSERPRE,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodPart = NVL(_Link_.DPCODICE,space(5))
      this.w_SerUtente = NVL(_Link_.DPSERPRE,space(10))
      this.w_DTIPRIG = NVL(_Link_.DPTIPRIG,space(1))
      this.w_CHKDATAPRE = NVL(_Link_.DPCTRPRE,space(1))
      this.w_GG_PRE = NVL(_Link_.DPGG_PRE,0)
      this.w_GG_SUC = NVL(_Link_.DPGG_SUC,0)
    else
      if i_cCtrl<>'Load'
        this.w_CodPart = space(5)
      endif
      this.w_SerUtente = space(10)
      this.w_DTIPRIG = space(1)
      this.w_CHKDATAPRE = space(1)
      this.w_GG_PRE = 0
      this.w_GG_SUC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodPart Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=Leggialte
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_Leggialte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_Leggialte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PANOEDES,PAFLGZER";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_Leggialte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_Leggialte)
            select PACODAZI,PANOEDES,PAFLGZER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_Leggialte = NVL(_Link_.PACODAZI,space(10))
      this.w_NOEDES = NVL(_Link_.PANOEDES,space(30))
      this.w_FLGZER = NVL(_Link_.PAFLGZER,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_Leggialte = space(10)
      endif
      this.w_NOEDES = space(30)
      this.w_FLGZER = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_Leggialte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCAUATT
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCAUATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCAUATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACAUACQ,CAFLANAL,CACAUDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ATCAUATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ATCAUATT)
            select CACODICE,CACAUACQ,CAFLANAL,CACAUDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCAUATT = NVL(_Link_.CACODICE,space(20))
      this.w_CAUACQ = NVL(_Link_.CACAUACQ,space(5))
      this.w_FLANAL = NVL(_Link_.CAFLANAL,space(1))
      this.w_CAUDOC = NVL(_Link_.CACAUDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ATCAUATT = space(20)
      endif
      this.w_CAUACQ = space(5)
      this.w_FLANAL = space(1)
      this.w_CAUDOC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCAUATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSER
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODSER))
          select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSER)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODSER) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODSER_1_11'),i_cWhere,'GSMA_BZA',""+iif(not isalt (), "Codici articoli", "Prestazioni") +"",''+iif(NOT IsAlt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODSER)
            select CACODICE,CADESART,CADESSUP,CACODART,CA__TIPO,CAUNIMIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSER = NVL(_Link_.CACODICE,space(20))
      this.w_DesSer = NVL(_Link_.CADESART,space(40))
      this.w_DesAgg = NVL(_Link_.CADESSUP,space(0))
      this.w_CACODART = NVL(_Link_.CACODART,space(20))
      this.w_TIPSER = NVL(_Link_.CA__TIPO,space(1))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODSER = space(20)
      endif
      this.w_DesSer = space(40)
      this.w_DesAgg = space(0)
      this.w_CACODART = space(20)
      this.w_TIPSER = space(1)
      this.w_UNMIS3 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_CACODART, 'FM-FO-DE')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente")
        endif
        this.w_CODSER = space(20)
        this.w_DesSer = space(40)
        this.w_DesAgg = space(0)
        this.w_CACODART = space(20)
        this.w_TIPSER = space(1)
        this.w_UNMIS3 = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODART
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2,ARTIPRIG,ARTIPRI2,ARTIPART,ARFLSERG,ARPRESTA";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CACODART)
            select ARCODART,ARUNMIS1,ARUNMIS2,ARTIPRIG,ARTIPRI2,ARTIPART,ARFLSERG,ARPRESTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_ARTIPRIG = NVL(_Link_.ARTIPRIG,space(1))
      this.w_ARTIPRI2 = NVL(_Link_.ARTIPRI2,space(1))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_PRESTA = NVL(_Link_.ARPRESTA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_ARTIPRIG = space(1)
      this.w_ARTIPRI2 = space(1)
      this.w_TIPART = space(2)
      this.w_FLSERG = space(1)
      this.w_PRESTA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLTEMP,UMDURORE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLTEMP,UMDURORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_UniMisTempo = NVL(_Link_.UMFLTEMP,space(10))
      this.w_ConversioneOre = NVL(_Link_.UMDURORE,0)
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_UniMisTempo = space(10)
      this.w_ConversioneOre = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAUNIMIS
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_DAUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLTEMP,UMFLFRAZ,UMDURORE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_DAUNIMIS))
          select UMCODICE,UMFLTEMP,UMFLFRAZ,UMDURORE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oDAUNIMIS_1_17'),i_cWhere,'',"",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLTEMP,UMFLFRAZ,UMDURORE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLTEMP,UMFLFRAZ,UMDURORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLTEMP,UMFLFRAZ,UMDURORE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_DAUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_DAUNIMIS)
            select UMCODICE,UMFLTEMP,UMFLFRAZ,UMDURORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_CHKTEMP = NVL(_Link_.UMFLTEMP,space(1))
      this.w_FL_FRAZ = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_DUR_ORE = NVL(_Link_.UMDURORE,0)
    else
      if i_cCtrl<>'Load'
        this.w_DAUNIMIS = space(3)
      endif
      this.w_CHKTEMP = space(1)
      this.w_FL_FRAZ = space(1)
      this.w_DUR_ORE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSERG='S' OR .w_DAUNIMIS=.w_UNMIS1 OR .w_DAUNIMIS=.w_UNMIS2 OR .w_DAUNIMIS=.w_UNMIS3
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire l'unit� di misura principale o secondaria della prestazione")
        endif
        this.w_DAUNIMIS = space(3)
        this.w_CHKTEMP = space(1)
        this.w_FL_FRAZ = space(1)
        this.w_DUR_ORE = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUDOC
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUDOC)
            select TDTIPDOC,TDFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_FLDANA = NVL(_Link_.TDFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUDOC = space(5)
      endif
      this.w_FLDANA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCodPart_1_3.value==this.w_CodPart)
      this.oPgFrm.Page1.oPag.oCodPart_1_3.value=this.w_CodPart
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSER_1_11.value==this.w_CODSER)
      this.oPgFrm.Page1.oPag.oCODSER_1_11.value=this.w_CODSER
    endif
    if not(this.oPgFrm.Page1.oPag.oDesSer_1_13.value==this.w_DesSer)
      this.oPgFrm.Page1.oPag.oDesSer_1_13.value=this.w_DesSer
    endif
    if not(this.oPgFrm.Page1.oPag.oDesAgg_1_16.value==this.w_DesAgg)
      this.oPgFrm.Page1.oPag.oDesAgg_1_16.value=this.w_DesAgg
    endif
    if not(this.oPgFrm.Page1.oPag.oDAUNIMIS_1_17.value==this.w_DAUNIMIS)
      this.oPgFrm.Page1.oPag.oDAUNIMIS_1_17.value=this.w_DAUNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDAQTAMOV_1_18.value==this.w_DAQTAMOV)
      this.oPgFrm.Page1.oPag.oDAQTAMOV_1_18.value=this.w_DAQTAMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPREZZO_1_19.value==this.w_DAPREZZO)
      this.oPgFrm.Page1.oPag.oDAPREZZO_1_19.value=this.w_DAPREZZO
    endif
    if not(this.oPgFrm.Page1.oPag.oDACOSUNI_1_20.value==this.w_DACOSUNI)
      this.oPgFrm.Page1.oPag.oDACOSUNI_1_20.value=this.w_DACOSUNI
    endif
    if not(this.oPgFrm.Page1.oPag.oINSPRDP_1_23.RadioValue()==this.w_INSPRDP)
      this.oPgFrm.Page1.oPag.oINSPRDP_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGPZCO_1_35.RadioValue()==this.w_FLGPZCO)
      this.oPgFrm.Page1.oPag.oFLGPZCO_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODPRA_1_84.value==this.w_ATCODPRA)
      this.oPgFrm.Page1.oPag.oATCODPRA_1_84.value=this.w_ATCODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oATDATINI_1_86.value==this.w_ATDATINI)
      this.oPgFrm.Page1.oPag.oATDATINI_1_86.value=this.w_ATDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCNDESCAN_1_88.value==this.w_CNDESCAN)
      this.oPgFrm.Page1.oPag.oCNDESCAN_1_88.value=this.w_CNDESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODATT_1_91.value==this.w_DACODATT)
      this.oPgFrm.Page1.oPag.oDACODATT_1_91.value=this.w_DACODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDADESATT_1_92.value==this.w_DADESATT)
      this.oPgFrm.Page1.oPag.oDADESATT_1_92.value=this.w_DADESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDAOREEFF_1_95.value==this.w_DAOREEFF)
      this.oPgFrm.Page1.oPag.oDAOREEFF_1_95.value=this.w_DAOREEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oDAMINEFF_1_96.value==this.w_DAMINEFF)
      this.oPgFrm.Page1.oPag.oDAMINEFF_1_96.value=this.w_DAMINEFF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(ChkTipArt(.w_CACODART, 'FM-FO-DE'))  and not(!Isalt())  and not(empty(.w_CODSER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODSER_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente")
          case   ((empty(.w_DAUNIMIS)) or not(.w_FLSERG='S' OR .w_DAUNIMIS=.w_UNMIS1 OR .w_DAUNIMIS=.w_UNMIS2 OR .w_DAUNIMIS=.w_UNMIS3))  and (.w_TIPART = 'FM' AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAUNIMIS_1_17.SetFocus()
            i_bnoObbl = !empty(.w_DAUNIMIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire l'unit� di misura principale o secondaria della prestazione")
          case   not((.w_DAQTAMOV>0 AND (.w_FL_FRAZ<>'S' OR .w_DAQTAMOV=INT(.w_DAQTAMOV))) OR NOT .w_TIPSER $ 'RM')  and (.w_TIPART = 'FM')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAQTAMOV_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_kpd
      IF (This.w_FLGZER='N' and This.w_DAPREZZO=0 AND This.w_TIPART='FO' and This.w_PRESTA $ 'A-S')
         i_bnoChk=.F.
         i_bRes=.F.
         i_cErrorMsg=Ah_MsgFormat("Tariffa mancante")
      ENDIF
      
      IF i_bRes
          * -- Controlla se l'utente non � amministratore e se � attivo il relativo check 
          * -- nei Parametri attivit� e nelle Persone (responsabile)
          IF NOT Cp_IsAdministrator() AND .w_FLPRES = 'S' AND .w_CHKDATAPRE = 'S'
            IF .w_ATDATINI < .w_DATAPREC OR .w_ATDATINI > .w_DATASUCC
              i_bnoChk=.F.
              i_bRes=.f.
              i_cErrorMsg=cp_Translate("Data non consentita dai controlli relativi al responsabile")
            ENDIF
          ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CodPart = this.w_CodPart
    this.o_ATCAUATT = this.w_ATCAUATT
    this.o_CODSER = this.w_CODSER
    this.o_CACODART = this.w_CACODART
    this.o_FLGPZCO = this.w_FLGPZCO
    return

enddefine

* --- Define pages as container
define class tgsag_kpdPag1 as StdContainer
  Width  = 608
  height = 396
  stdWidth  = 608
  stdheight = 396
  resizeXpos=323
  resizeYpos=179
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCodPart_1_3 as StdField with uid="BXFLGODQWO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CodPart", cQueryName = "CodPart",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 259820582,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=469, Top=18, InputMask=replicate('X',5), cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_ADP", oKey_1_1="DPCODICE", oKey_1_2="this.w_CodPart"

  func oCodPart_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCODSER_1_11 as StdField with uid="NQAZQYRMPT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODSER", cQueryName = "CODSER",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente",;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 230517798,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=108, Top=73, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_CODSER"

  func oCODSER_1_11.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  func oCODSER_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSER_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSER_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODSER_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',""+iif(not isalt (), "Codici articoli", "Prestazioni") +"",''+iif(NOT IsAlt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCODSER_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODSER
     i_obj.ecpSave()
  endproc

  add object oDesSer_1_13 as StdField with uid="JWVQJSFOLX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DesSer", cQueryName = "DesSer",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della prestazione",;
    HelpContextID = 264270390,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=278, Top=73, InputMask=replicate('X',40)

  func oDesSer_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODSER) AND (.w_NOEDES='N' OR !Isalt() or cp_IsAdministrator()))
    endwith
   endif
  endfunc

  func oDesSer_1_13.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oDesAgg_1_16 as StdMemo with uid="XENUFWOWED",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DesAgg", cQueryName = "DesAgg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva della prestazione",;
    HelpContextID = 80638518,;
   bGlobalFont=.t.,;
    Height=66, Width=491, Left=108, Top=102

  func oDesAgg_1_16.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oDAUNIMIS_1_17 as StdField with uid="RWLHCOWLKL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DAUNIMIS", cQueryName = "DAUNIMIS",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire l'unit� di misura principale o secondaria della prestazione",;
    HelpContextID = 117870967,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=107, Top=176, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_DAUNIMIS"

  func oDAUNIMIS_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPART = 'FM' AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S'))
    endwith
   endif
  endfunc

  func oDAUNIMIS_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAUNIMIS_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAUNIMIS_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oDAUNIMIS_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc

  add object oDAQTAMOV_1_18 as StdField with uid="EGFXFSMBLL",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DAQTAMOV", cQueryName = "DAQTAMOV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
    ToolTipText = "Quantit�",;
    HelpContextID = 125882740,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=334, Top=176, cSayPict="'@Z '+v_PQ(11)", cGetPict="'@Z '+v_GQ(11)", bHasZoom = .t. 

  func oDAQTAMOV_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPART = 'FM')
    endwith
   endif
  endfunc

  func oDAQTAMOV_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DAQTAMOV>0 AND (.w_FL_FRAZ<>'S' OR .w_DAQTAMOV=INT(.w_DAQTAMOV))) OR NOT .w_TIPSER $ 'RM')
    endwith
    return bRes
  endfunc

  proc oDAQTAMOV_1_18.mZoom
      with this.Parent.oContained
        gsag_bom(this.Parent.oContained,"K")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDAPREZZO_1_19 as StdField with uid="WFREKWWQNG",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DAPREZZO", cQueryName = "DAPREZZO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Inserire l'eventuale tariffa da applicare in alternativa a quella calcolata in base a quanto indicato nel tariffario",;
    HelpContextID = 172155259,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=107, Top=237, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oDAPREZZO_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGPZCO='S')
    endwith
   endif
  endfunc

  add object oDACOSUNI_1_20 as StdField with uid="JZDKUQQGEO",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DACOSUNI", cQueryName = "DACOSUNI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Inserire l'eventuale costo interno  da applicare in alternativa a quello calcolato in base a quanto indicato sulle persone",;
    HelpContextID = 241611137,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=334, Top=237, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oDACOSUNI_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGPZCO='S')
    endwith
   endif
  endfunc


  add object oINSPRDP_1_23 as StdCombo with uid="TUDHIHBZZP",rtseq=23,rtrep=.f.,left=107,top=275,width=146,height=21;
    , ToolTipText = "Tipo inserimanto";
    , HelpContextID = 9132934;
    , cFormVar="w_INSPRDP",RowSource=""+"Prima,"+"Dopo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oINSPRDP_1_23.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oINSPRDP_1_23.GetRadio()
    this.Parent.oContained.w_INSPRDP = this.RadioValue()
    return .t.
  endfunc

  func oINSPRDP_1_23.SetRadio()
    this.Parent.oContained.w_INSPRDP=trim(this.Parent.oContained.w_INSPRDP)
    this.value = ;
      iif(this.Parent.oContained.w_INSPRDP=='P',1,;
      iif(this.Parent.oContained.w_INSPRDP=='D',2,;
      0))
  endfunc


  add object oBtn_1_32 as StdButton with uid="ESZNQMWTGW",left=503, top=342, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per completare la prestazione";
    , HelpContextID = 149534490;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CodSer))
      endwith
    endif
  endfunc

  func oBtn_1_32.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_CodSer))
     endwith
    endif
  endfunc


  add object oBtn_1_33 as StdButton with uid="TKCMXZXILU",left=554, top=342, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142245818;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLGPZCO_1_35 as StdCheck with uid="IDMILXRNZJ",rtseq=32,rtrep=.f.,left=107, top=204, caption="Forza applicazione di tariffa e costo",;
    ToolTipText = "Se attivo permette di inserire la tariffa e il costo interno senza considerare cio� che sarebbe calcolato dalla procedura",;
    HelpContextID = 267740842,;
    cFormVar="w_FLGPZCO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGPZCO_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLGPZCO_1_35.GetRadio()
    this.Parent.oContained.w_FLGPZCO = this.RadioValue()
    return .t.
  endfunc

  func oFLGPZCO_1_35.SetRadio()
    this.Parent.oContained.w_FLGPZCO=trim(this.Parent.oContained.w_FLGPZCO)
    this.value = ;
      iif(this.Parent.oContained.w_FLGPZCO=='S',1,;
      0)
  endfunc


  add object oBtn_1_37 as StdButton with uid="KHCAYSAWPW",left=551, top=226, width=48,height=45,;
    CpPicture="bmp\carica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per caricare dati analitica";
    , HelpContextID = 233283207;
    , TabStop=.f.,Caption='A\<nalitica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      do GSAG_KDA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COAN <> 'S'  or  !Isalt())
     endwith
    endif
  endfunc

  add object oATCODPRA_1_84 as StdField with uid="VPASRHJUCD",rtseq=75,rtrep=.f.,;
    cFormVar = "w_ATCODPRA", cQueryName = "ATCODPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 195649863,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=108, Top=45, InputMask=replicate('X',15)

  add object oATDATINI_1_86 as StdField with uid="GHHVDTMWJK",rtseq=76,rtrep=.f.,;
    cFormVar = "w_ATDATINI", cQueryName = "ATDATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 174362289,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=108, Top=18

  add object oCNDESCAN_1_88 as StdField with uid="IXLJALOYMG",rtseq=77,rtrep=.f.,;
    cFormVar = "w_CNDESCAN", cQueryName = "CNDESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 261057396,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=235, Top=45, InputMask=replicate('X',80)

  add object oDACODATT_1_91 as StdField with uid="LVXFSAZSKC",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DACODATT", cQueryName = "DACODATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 212422282,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=107, Top=304, InputMask=replicate('X',20)

  add object oDADESATT_1_92 as StdField with uid="AGYNLKBRYR",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DADESATT", cQueryName = "DADESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 227499658,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=278, Top=304, InputMask=replicate('X',40)

  add object oDAOREEFF_1_95 as StdField with uid="CYOAPJKGLT",rtseq=81,rtrep=.f.,;
    cFormVar = "w_DAOREEFF", cQueryName = "DAOREEFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 12390012,;
   bGlobalFont=.t.,;
    Height=21, Width=25, Left=528, Top=176

  add object oDAMINEFF_1_96 as StdField with uid="NVHMCLVDDH",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DAMINEFF", cQueryName = "DAMINEFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 21229180,;
   bGlobalFont=.t.,;
    Height=21, Width=33, Left=566, Top=176

  add object oStr_1_26 as StdString with uid="AUEJGKPLZP",Visible=.t., Left=36, Top=76,;
    Alignment=1, Width=68, Height=18,;
    Caption="Prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="FKQQIIZGHB",Visible=.t., Left=36, Top=105,;
    Alignment=1, Width=68, Height=18,;
    Caption="Descr. Agg.:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="TYNZHOYDLW",Visible=.t., Left=52, Top=274,;
    Alignment=1, Width=50, Height=18,;
    Caption="Inserisci:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="CRAUNDRHNM",Visible=.t., Left=282, Top=176,;
    Alignment=1, Width=49, Height=18,;
    Caption="Quantit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="WVLZZUZDOQ",Visible=.t., Left=17, Top=176,;
    Alignment=1, Width=87, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="TSDJIWSTAD",Visible=.t., Left=368, Top=21,;
    Alignment=1, Width=98, Height=18,;
    Caption="Responsabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="OMPRYYRXDY",Visible=.t., Left=26, Top=48,;
    Alignment=1, Width=81, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="IHAQAXETAQ",Visible=.t., Left=56, Top=21,;
    Alignment=1, Width=51, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="QOSORLVNKB",Visible=.t., Left=36, Top=305,;
    Alignment=1, Width=68, Height=18,;
    Caption="Prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_94 as StdString with uid="DLMJCAJVPM",Visible=.t., Left=254, Top=237,;
    Alignment=0, Width=77, Height=18,;
    Caption="Costo interno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="KRFRWINQZV",Visible=.t., Left=430, Top=176,;
    Alignment=1, Width=95, Height=18,;
    Caption="Durata effettiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="PVSFOKOSXX",Visible=.t., Left=559, Top=176,;
    Alignment=0, Width=5, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_99 as StdString with uid="FQBGMGLCBN",Visible=.t., Left=65, Top=237,;
    Alignment=0, Width=39, Height=18,;
    Caption="Tariffa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kpd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
