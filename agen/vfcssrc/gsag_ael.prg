* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ael                                                        *
*              Elementi contratto                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-08-05                                                      *
* Last revis.: 2015-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_ael"))

* --- Class definition
define class tgsag_ael as StdForm
  Top    = -3
  Left   = 20

  * --- Standard Properties
  Width  = 592
  Height = 556+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-23"
  HelpContextID=192272535
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=195

  * --- Constant Properties
  ELE_CONT_IDX = 0
  CON_TRAS_IDX = 0
  MOD_ELEM_IDX = 0
  IMP_MAST_IDX = 0
  UNIMIS_IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  MODCLDAT_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  CAT_ELEM_IDX = 0
  DIPENDEN_IDX = 0
  CAUMATTI_IDX = 0
  TIP_DOCU_IDX = 0
  LISTINI_IDX = 0
  PAG_AMEN_IDX = 0
  IMP_DETT_IDX = 0
  CON_TRAM_IDX = 0
  VOC_COST_IDX = 0
  CENCOST_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  cFile = "ELE_CONT"
  cKeySelect = "ELCODMOD,ELCODIMP,ELCODCOM,ELCONTRA,ELRINNOV"
  cKeyWhere  = "ELCODMOD=this.w_ELCODMOD and ELCODIMP=this.w_ELCODIMP and ELCODCOM=this.w_ELCODCOM and ELCONTRA=this.w_ELCONTRA and ELRINNOV=this.w_ELRINNOV"
  cKeyWhereODBC = '"ELCODMOD="+cp_ToStrODBC(this.w_ELCODMOD)';
      +'+" and ELCODIMP="+cp_ToStrODBC(this.w_ELCODIMP)';
      +'+" and ELCODCOM="+cp_ToStrODBC(this.w_ELCODCOM)';
      +'+" and ELCONTRA="+cp_ToStrODBC(this.w_ELCONTRA)';
      +'+" and ELRINNOV="+cp_ToStrODBC(this.w_ELRINNOV)';

  cKeyWhereODBCqualified = '"ELE_CONT.ELCODMOD="+cp_ToStrODBC(this.w_ELCODMOD)';
      +'+" and ELE_CONT.ELCODIMP="+cp_ToStrODBC(this.w_ELCODIMP)';
      +'+" and ELE_CONT.ELCODCOM="+cp_ToStrODBC(this.w_ELCODCOM)';
      +'+" and ELE_CONT.ELCONTRA="+cp_ToStrODBC(this.w_ELCONTRA)';
      +'+" and ELE_CONT.ELRINNOV="+cp_ToStrODBC(this.w_ELRINNOV)';

  cPrg = "gsag_ael"
  cComment = "Elementi contratto"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RESCHK = 0
  w_ELCODMOD = space(10)
  o_ELCODMOD = space(10)
  w_TIPCON = space(1)
  w_KEYLISTB = space(10)
  w_ELCODCLI = space(15)
  o_ELCODCLI = space(15)
  w_ELCODIMP = space(10)
  w_CODICE = space(15)
  w_ELCODCOM = 0
  w_ELCONTRA = space(10)
  o_ELCONTRA = space(10)
  w_CODCLI = space(15)
  w_TIPATT = space(20)
  o_TIPATT = space(20)
  w_GRUPAR = space(5)
  o_GRUPAR = space(5)
  w_TIPDOC = space(3)
  o_TIPDOC = space(3)
  w_PERFAT = space(3)
  w_PERATT = space(3)
  w_TIPMOD = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_ELDATINI = ctod('  /  /  ')
  o_ELDATINI = ctod('  /  /  ')
  w_FLATTI = space(1)
  o_FLATTI = space(1)
  w_COPRIDAT = space(1)
  w_COPRIDOC = space(1)
  w_PRIMADATA = space(1)
  o_PRIMADATA = space(1)
  w_PRIMADATADOC = space(1)
  o_PRIMADATADOC = space(1)
  w_PRIDAT = space(1)
  w_PRIDOC = space(1)
  w_ELDATFIN = ctod('  /  /  ')
  o_ELDATFIN = ctod('  /  /  ')
  w_ELCATCON = space(5)
  w_ELCODLIS = space(5)
  o_ELCODLIS = space(5)
  w_ELTIPATT = space(20)
  w_FLATT = space(1)
  o_FLATT = space(1)
  w_ELGRUPAR = space(5)
  w_ELPERATT = space(3)
  o_ELPERATT = space(3)
  w_ELDATATT = ctod('  /  /  ')
  o_ELDATATT = ctod('  /  /  ')
  w_ELGIOATT = 0
  o_ELGIOATT = 0
  w_ELCAUDOC = space(5)
  o_ELCAUDOC = space(5)
  w_ELCODPAG = space(5)
  w_ELPERFAT = space(3)
  o_ELPERFAT = space(3)
  w_ELDATFAT = ctod('  /  /  ')
  o_ELDATFAT = ctod('  /  /  ')
  w_ELGIODOC = 0
  o_ELGIODOC = 0
  w_ELESCRIN = space(1)
  o_ELESCRIN = space(1)
  w_DATAFISSA = space(1)
  w_DATACONT = space(1)
  w_ELPERCON = space(3)
  w_FLDATFIS = space(1)
  w_ELRINCON = space(1)
  o_ELRINCON = space(1)
  w_ELNUMGIO = 0
  o_ELNUMGIO = 0
  w_ELDATDIS = ctod('  /  /  ')
  w_ELGESRAT = space(1)
  w_CODPAG = space(5)
  w_PATIPRIS = space(1)
  w_FLNSAP = space(1)
  w_CODART = space(20)
  w_CACODART = space(41)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_DESELE = space(60)
  w_LIPREZZO = 0
  w_ELDATDOC = ctod('  /  /  ')
  w_ELDATGAT = ctod('  /  /  ')
  w_DESCLI = space(50)
  w_DESPER = space(50)
  w_DESCAT = space(60)
  w_DESCRIAT = space(50)
  w_FLSERG = space(1)
  w_DESCON = space(50)
  w_DESATT = space(254)
  w_DESDOC = space(35)
  w_CARAGGST = space(10)
  w_DESLIS = space(40)
  w_PREZUM = space(1)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_CATCLI = space(5)
  w_CATART = space(5)
  w_SCOLIS = space(1)
  w_DECUNI = 0
  w_MOLTI3 = 0
  w_OPERA3 = space(1)
  w_FLUSEP = space(1)
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(2)
  w_VALLIS = space(3)
  w_DESCLI1 = space(60)
  w_CATDOC = space(2)
  w_FLVEAC = space(1)
  w_TIPRIS = space(1)
  w_CICLO = space(1)
  w_DESPAG = space(30)
  w_FLSCOR = space(1)
  w_LORNET = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_QUALIS = space(1)
  w_NUMLIS = space(5)
  w_IVACLI = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPSER = space(1)
  w_ELPROLIS = space(5)
  w_ELPROSCO = space(5)
  w_FLGLIS = space(1)
  w_FLINTE = space(1)
  w_NULLA = space(1)
  w_MVCODVAL = space(5)
  w_DATALIST = ctod('  /  /  ')
  w_ELSCOLIS = space(5)
  w_ELTCOLIS = space(5)
  w_FLRIPS = space(5)
  w_GESTGUID = space(10)
  w_ARUNMIS1 = space(3)
  w_ARUNMIS2 = space(3)
  w_MVFLVEAC = space(1)
  w_MCAUDOC = space(5)
  w_CCODLIS = space(5)
  w_MCODLIS = space(5)
  w_IMDESCRI = space(50)
  w_CPROWORD = 0
  w_IMDESCON = space(50)
  w_CODIMP = space(10)
  w_CODCOM = 0
  w_CPROWORD_IMP_DETT = 0
  w_ATTIPATT = space(1)
  w_ELCODART = space(20)
  o_ELCODART = space(20)
  w_ELCODART = space(20)
  w_ARCODART = space(20)
  w_ELUNIMIS = space(3)
  o_ELUNIMIS = space(3)
  w_ELQTAMOV = 0
  o_ELQTAMOV = 0
  w_ELCONCOD = space(15)
  w_ELPREZZO = 0
  o_ELPREZZO = 0
  w_ELSCONT1 = 0
  o_ELSCONT1 = 0
  w_ELSCONT2 = 0
  o_ELSCONT2 = 0
  w_ELSCONT3 = 0
  o_ELSCONT3 = 0
  w_ELSCONT4 = 0
  o_ELSCONT4 = 0
  w_ELSCONT1 = 0
  w_ELSCONT2 = 0
  w_ELSCONT3 = 0
  w_ELSCONT4 = 0
  w_DESUNI = space(35)
  w_DESCTR = space(50)
  w_DESART = space(40)
  w_DPDESCRI = space(60)
  w_ELCODVAL = space(3)
  o_ELCODVAL = space(3)
  w_DESVAL = space(35)
  w_VOCRIC = space(10)
  w_ELVOCCEN = space(15)
  w_ELCODCEN = space(15)
  w_ELCOCOMM = space(15)
  o_ELCOCOMM = space(15)
  w_ELCODATT = space(15)
  w_DESVOC = space(40)
  w_DESCEN = space(40)
  w_DESCOMM = space(100)
  w_DESATTI = space(100)
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_CODCEN = space(15)
  w_COCOMM = space(15)
  w_VOCOBSO = ctod('  /  /  ')
  w_CENOBSO = ctod('  /  /  ')
  w_COMOBSO = ctod('  /  /  ')
  w_MCOCOMM = space(15)
  w_CLI = space(15)
  w_ELRINNOV = 0
  w_MDDESCRI = space(50)
  w_CATART = space(5)
  w_MCODATT = space(15)
  w_MOQTAMOV = 0
  w_UNMIS1 = space(3)
  w_ELQTACON = 0
  w_QTA__RES = 0
  w_PREZZTOT = 0
  w_MD__FREQ = space(1)
  w_ATTEXIST = .F.
  w_DOCEXIST = .F.
  w_CT = space(1)
  w_CM = space(3)
  w_CC = space(15)
  w_CI = ctod('  /  /  ')
  w_CF = ctod('  /  /  ')
  w_QUACON = space(1)
  w_CV = space(3)
  w_IVACON = space(1)
  w_CATCOM = space(3)
  w_TIPART = space(2)
  o_TIPART = space(2)
  w_MOTIPCON = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_ELINICOA = ctod('  /  /  ')
  o_ELINICOA = ctod('  /  /  ')
  w_ELFINCOA = ctod('  /  /  ')
  w_ELINICOD = ctod('  /  /  ')
  o_ELINICOD = ctod('  /  /  ')
  w_ELFINCOD = ctod('  /  /  ')
  w_UTDC = ctot('')
  w_UTCV = 0
  w_UTDV = ctot('')
  w_UTCC = 0
  * --- Area Manuale = Declare Variables
  * --- gsag_ael
  w_OGGETTO=NULL
  
  PROCEDURE ecpSave()
      DoDefault()
     IF this.w_RESCHK=0
       IF VARTYPE(this.w_OGGETTO)='O'
          this.cFunction="Query"
          this.ecpQuit()
       ENDIF
     ENDIF
  ENDPROC
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ELE_CONT','gsag_ael')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_aelPag1","gsag_ael",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Elementi contratto")
      .Pages(1).HelpContextID = 166684003
      .Pages(2).addobject("oPag","tgsag_aelPag2","gsag_ael",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati di riga")
      .Pages(2).HelpContextID = 38690422
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oELCODMOD_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[22]
    this.cWorkTables[1]='CON_TRAS'
    this.cWorkTables[2]='MOD_ELEM'
    this.cWorkTables[3]='IMP_MAST'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='MODCLDAT'
    this.cWorkTables[8]='KEY_ARTI'
    this.cWorkTables[9]='ART_ICOL'
    this.cWorkTables[10]='CAT_ELEM'
    this.cWorkTables[11]='DIPENDEN'
    this.cWorkTables[12]='CAUMATTI'
    this.cWorkTables[13]='TIP_DOCU'
    this.cWorkTables[14]='LISTINI'
    this.cWorkTables[15]='PAG_AMEN'
    this.cWorkTables[16]='IMP_DETT'
    this.cWorkTables[17]='CON_TRAM'
    this.cWorkTables[18]='VOC_COST'
    this.cWorkTables[19]='CENCOST'
    this.cWorkTables[20]='CAN_TIER'
    this.cWorkTables[21]='ATTIVITA'
    this.cWorkTables[22]='ELE_CONT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(22))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ELE_CONT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ELE_CONT_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ELCODMOD = NVL(ELCODMOD,space(10))
      .w_ELCODIMP = NVL(ELCODIMP,space(10))
      .w_ELCODCOM = NVL(ELCODCOM,0)
      .w_ELCONTRA = NVL(ELCONTRA,space(10))
      .w_ELRINNOV = NVL(ELRINNOV,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_30_joined
    link_1_30_joined=.f.
    local link_1_31_joined
    link_1_31_joined=.f.
    local link_1_32_joined
    link_1_32_joined=.f.
    local link_1_34_joined
    link_1_34_joined=.f.
    local link_1_35_joined
    link_1_35_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    local link_1_39_joined
    link_1_39_joined=.f.
    local link_1_40_joined
    link_1_40_joined=.f.
    local link_1_46_joined
    link_1_46_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_11_joined
    link_2_11_joined=.f.
    local link_1_153_joined
    link_1_153_joined=.f.
    local link_2_36_joined
    link_2_36_joined=.f.
    local link_2_37_joined
    link_2_37_joined=.f.
    local link_2_38_joined
    link_2_38_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ELE_CONT where ELCODMOD=KeySet.ELCODMOD
    *                            and ELCODIMP=KeySet.ELCODIMP
    *                            and ELCODCOM=KeySet.ELCODCOM
    *                            and ELCONTRA=KeySet.ELCONTRA
    *                            and ELRINNOV=KeySet.ELRINNOV
    *
    i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ELE_CONT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ELE_CONT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ELE_CONT '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_30_joined=this.AddJoinedLink_1_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_31_joined=this.AddJoinedLink_1_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_32_joined=this.AddJoinedLink_1_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_34_joined=this.AddJoinedLink_1_34(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_35_joined=this.AddJoinedLink_1_35(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_39_joined=this.AddJoinedLink_1_39(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_40_joined=this.AddJoinedLink_1_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_46_joined=this.AddJoinedLink_1_46(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_153_joined=this.AddJoinedLink_1_153(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_36_joined=this.AddJoinedLink_2_36(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_37_joined=this.AddJoinedLink_2_37(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_38_joined=this.AddJoinedLink_2_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ELCODMOD',this.w_ELCODMOD  ,'ELCODIMP',this.w_ELCODIMP  ,'ELCODCOM',this.w_ELCODCOM  ,'ELCONTRA',this.w_ELCONTRA  ,'ELRINNOV',this.w_ELRINNOV  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RESCHK = 0
        .w_KEYLISTB = Sys(2015)
        .w_CODCLI = space(15)
        .w_TIPATT = space(20)
        .w_GRUPAR = space(5)
        .w_TIPDOC = space(3)
        .w_PERFAT = space(3)
        .w_PERATT = space(3)
        .w_TIPMOD = space(1)
        .w_DATINI = ctod("  /  /  ")
        .w_DATFIN = ctod("  /  /  ")
        .w_FLATTI = space(1)
        .w_COPRIDAT = space(1)
        .w_COPRIDOC = space(1)
        .w_PRIDAT = space(1)
        .w_PRIDOC = space(1)
        .w_FLATT = space(1)
        .w_DATAFISSA = space(1)
        .w_DATACONT = space(1)
        .w_CODPAG = space(5)
        .w_PATIPRIS = 'G'
        .w_FLNSAP = space(1)
        .w_CODART = space(20)
        .w_UNMIS2 = space(3)
        .w_UNMIS3 = space(3)
        .w_DESELE = space(60)
        .w_LIPREZZO = 0
        .w_DESCLI = space(50)
        .w_DESPER = space(50)
        .w_DESCAT = space(60)
        .w_DESCRIAT = space(50)
        .w_FLSERG = space(1)
        .w_DESCON = space(50)
        .w_DESATT = space(254)
        .w_DESDOC = space(35)
        .w_CARAGGST = space(10)
        .w_DESLIS = space(40)
        .w_PREZUM = space(1)
        .w_OPERAT = space(1)
        .w_MOLTIP = 0
        .w_CATCLI = space(5)
        .w_CATART = space(5)
        .w_SCOLIS = space(1)
        .w_DECUNI = 0
        .w_MOLTI3 = 0
        .w_OPERA3 = space(1)
        .w_FLUSEP = space(1)
        .w_FLFRAZ1 = space(1)
        .w_MODUM2 = space(2)
        .w_VALLIS = space(3)
        .w_DESCLI1 = space(60)
        .w_CATDOC = space(2)
        .w_FLVEAC = space(1)
        .w_TIPRIS = space(1)
        .w_CICLO = 'E'
        .w_DESPAG = space(30)
        .w_FLSCOR = space(1)
        .w_LORNET = space(1)
        .w_OBTEST = i_DATSYS
        .w_INILIS = ctod("  /  /  ")
        .w_FINLIS = ctod("  /  /  ")
        .w_QUALIS = space(1)
        .w_NUMLIS = space(5)
        .w_IVACLI = space(1)
        .w_DATOBSO = ctod("  /  /  ")
        .w_TIPSER = space(1)
        .w_FLINTE = space(1)
        .w_FLRIPS = space(5)
        .w_GESTGUID = space(10)
        .w_MCAUDOC = space(5)
        .w_CCODLIS = space(5)
        .w_MCODLIS = space(5)
        .w_IMDESCRI = space(50)
        .w_CPROWORD = 0
        .w_IMDESCON = space(50)
        .w_CPROWORD_IMP_DETT = 0
        .w_ATTIPATT = 'A'
        .w_DESUNI = space(35)
        .w_DESCTR = space(50)
        .w_DESART = space(40)
        .w_DPDESCRI = space(60)
        .w_DESVAL = space(35)
        .w_DESVOC = space(40)
        .w_DESCEN = space(40)
        .w_DESCOMM = space(100)
        .w_DESATTI = space(100)
        .w_FLANAL = space(1)
        .w_FLGCOM = space(1)
        .w_CODCEN = space(15)
        .w_COCOMM = space(15)
        .w_VOCOBSO = ctod("  /  /  ")
        .w_CENOBSO = ctod("  /  /  ")
        .w_COMOBSO = ctod("  /  /  ")
        .w_MCOCOMM = space(15)
        .w_MDDESCRI = space(50)
        .w_CATART = space(5)
        .w_MCODATT = space(15)
        .w_MOQTAMOV = 0
        .w_UNMIS1 = space(3)
        .w_MD__FREQ = space(1)
        .w_ATTEXIST = .f.
        .w_DOCEXIST = .f.
        .w_CT = space(1)
        .w_CM = space(3)
        .w_CC = space(15)
        .w_CI = ctod("  /  /  ")
        .w_CF = ctod("  /  /  ")
        .w_QUACON = space(1)
        .w_CV = space(3)
        .w_IVACON = space(1)
        .w_CATCOM = space(3)
        .w_TIPART = space(2)
        .w_MOTIPCON = space(1)
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_ELCODMOD = NVL(ELCODMOD,space(10))
          if link_1_2_joined
            this.w_ELCODMOD = NVL(MOCODICE102,NVL(this.w_ELCODMOD,space(10)))
            this.w_DESELE = NVL(MODESCRI102,space(60))
            this.w_ELCATCON = NVL(MOCATELE102,space(5))
            this.w_PERFAT = NVL(MOPERFAT102,space(3))
            this.w_ELGESRAT = NVL(MOGESRAT102,space(1))
            this.w_TIPMOD = NVL(MOTIPAPL102,space(1))
            this.w_CODART = NVL(MOCODSER102,space(20))
            this.w_ELCODVAL = NVL(MOCODVAL102,space(3))
            this.w_PERATT = NVL(MOPERATT102,space(3))
            this.w_ELTIPATT = NVL(MOTIPATT102,space(20))
            this.w_ELGRUPAR = NVL(MOGRUPAR102,space(5))
            this.w_MCAUDOC = NVL(MOCAUDOC102,space(5))
            this.w_FLATT = NVL(MOFLFATT102,space(1))
            this.w_FLATTI = NVL(MOFLATTI102,space(1))
            this.w_MCODLIS = NVL(MOCODLIS102,space(5))
            this.w_ELGIODOC = NVL(MOGIODOC102,0)
            this.w_ELGIOATT = NVL(MOGIOATT102,0)
            this.w_ELVOCCEN = NVL(MOVOCCEN102,space(15))
            this.w_ELCODCEN = NVL(MOCODCEN102,space(15))
            this.w_MCOCOMM = NVL(MOCOCOMM102,space(15))
            this.w_MCODATT = NVL(MOCODATT102,space(15))
            this.w_MOTIPCON = NVL(MOTIPCON102,space(1))
            this.w_MOQTAMOV = NVL(MOQTAMOV102,0)
            this.w_PRIDAT = NVL(MOPRIDAT102,space(1))
            this.w_PRIDOC = NVL(MOPRIDOC102,space(1))
            this.w_DATAFISSA = NVL(MODATFIS102,space(1))
          else
          .link_1_2('Load')
          endif
        .w_TIPCON = 'C'
        .w_ELCODCLI = .w_CODCLI
          .link_1_5('Load')
        .w_ELCODIMP = NVL(ELCODIMP,space(10))
        .w_CODICE = .w_ELCODCLI
        .w_ELCODCOM = NVL(ELCODCOM,0)
        .w_ELCONTRA = NVL(ELCONTRA,space(10))
          if link_1_9_joined
            this.w_ELCONTRA = NVL(COSERIAL109,NVL(this.w_ELCONTRA,space(10)))
            this.w_CODCLI = NVL(COCODCON109,space(15))
            this.w_DESCON = NVL(CODESCON109,space(50))
            this.w_ELDATINI = NVL(cp_ToDate(CODATINI109),ctod("  /  /  "))
            this.w_ELDATFIN = NVL(cp_ToDate(CODATFIN109),ctod("  /  /  "))
            this.w_DATINI = NVL(cp_ToDate(CODATINI109),ctod("  /  /  "))
            this.w_DATFIN = NVL(cp_ToDate(CODATFIN109),ctod("  /  /  "))
            this.w_TIPDOC = NVL(COTIPDOC109,space(3))
            this.w_GRUPAR = NVL(COGRUPAR109,space(5))
            this.w_TIPATT = NVL(COTIPATT109,space(20))
            this.w_CODPAG = NVL(COCODPAG109,space(5))
            this.w_CCODLIS = NVL(COCODLIS109,space(5))
            this.w_CODCEN = NVL(COCODCEN109,space(15))
            this.w_COCOMM = NVL(COCOCOMM109,space(15))
            this.w_COPRIDAT = NVL(COPRIDAT109,space(1))
            this.w_COPRIDOC = NVL(COPRIDOC109,space(1))
            this.w_DATACONT = NVL(CODATFIS109,space(1))
          else
          .link_1_9('Load')
          endif
          .link_1_10('Load')
        .w_ELDATINI = NVL(cp_ToDate(ELDATINI),ctod("  /  /  "))
        .w_PRIMADATA = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'', IIF(.w_FLATTI<>'S','',IIF(.w_COPRIDAT='M',.w_PRIDAT,.w_COPRIDAT)))
        .w_PRIMADATADOC = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'',IIF(.w_FLATT<>'S','',IIF(.w_COPRIDOC='M',.w_PRIDOC,.w_COPRIDOC)))
        .w_ELDATFIN = NVL(cp_ToDate(ELDATFIN),ctod("  /  /  "))
        .w_ELCATCON = NVL(ELCATCON,space(5))
          if link_1_30_joined
            this.w_ELCATCON = NVL(CECODELE130,NVL(this.w_ELCATCON,space(5)))
            this.w_DESCAT = NVL(CEDESELE130,space(60))
          else
          .link_1_30('Load')
          endif
        .w_ELCODLIS = NVL(ELCODLIS,space(5))
          if link_1_31_joined
            this.w_ELCODLIS = NVL(LSCODLIS131,NVL(this.w_ELCODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS131,space(40))
            this.w_SCOLIS = NVL(LSFLSCON131,space(1))
            this.w_VALLIS = NVL(LSVALLIS131,space(3))
            this.w_LORNET = NVL(LSIVALIS131,space(1))
            this.w_INILIS = NVL(cp_ToDate(LSDTINVA131),ctod("  /  /  "))
            this.w_FINLIS = NVL(cp_ToDate(LSDTOBSO131),ctod("  /  /  "))
            this.w_QUALIS = NVL(LSQUANTI131,space(1))
          else
          .link_1_31('Load')
          endif
        .w_ELTIPATT = NVL(ELTIPATT,space(20))
          if link_1_32_joined
            this.w_ELTIPATT = NVL(CACODICE132,NVL(this.w_ELTIPATT,space(20)))
            this.w_DESATT = NVL(CADESCRI132,space(254))
            this.w_CARAGGST = NVL(CARAGGST132,space(10))
            this.w_FLNSAP = NVL(CAFLNSAP132,space(1))
          else
          .link_1_32('Load')
          endif
        .w_ELGRUPAR = NVL(ELGRUPAR,space(5))
          if link_1_34_joined
            this.w_ELGRUPAR = NVL(DPCODICE134,NVL(this.w_ELGRUPAR,space(5)))
            this.w_TIPRIS = NVL(DPTIPRIS134,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(DPDTOBSO134),ctod("  /  /  "))
            this.w_DPDESCRI = NVL(DPDESCRI134,space(60))
          else
          .link_1_34('Load')
          endif
        .w_ELPERATT = NVL(ELPERATT,space(3))
          if link_1_35_joined
            this.w_ELPERATT = NVL(MDCODICE135,NVL(this.w_ELPERATT,space(3)))
            this.w_DESCRIAT = NVL(MDDESCRI135,space(50))
          else
          .link_1_35('Load')
          endif
        .w_ELDATATT = NVL(cp_ToDate(ELDATATT),ctod("  /  /  "))
        .w_ELGIOATT = NVL(ELGIOATT,0)
        .w_ELCAUDOC = NVL(ELCAUDOC,space(5))
          if link_1_38_joined
            this.w_ELCAUDOC = NVL(TDTIPDOC138,NVL(this.w_ELCAUDOC,space(5)))
            this.w_DESDOC = NVL(TDDESDOC138,space(35))
            this.w_CATDOC = NVL(TDCATDOC138,space(2))
            this.w_FLVEAC = NVL(TDFLVEAC138,space(1))
            this.w_FLINTE = NVL(TDFLINTE138,space(1))
            this.w_FLRIPS = NVL(TDFLRIPS138,space(5))
            this.w_FLANAL = NVL(TDFLANAL138,space(1))
            this.w_FLGCOM = NVL(TDFLCOMM138,space(1))
          else
          .link_1_38('Load')
          endif
        .w_ELCODPAG = NVL(ELCODPAG,space(5))
          if link_1_39_joined
            this.w_ELCODPAG = NVL(PACODICE139,NVL(this.w_ELCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI139,space(30))
          else
          .link_1_39('Load')
          endif
        .w_ELPERFAT = NVL(ELPERFAT,space(3))
          if link_1_40_joined
            this.w_ELPERFAT = NVL(MDCODICE140,NVL(this.w_ELPERFAT,space(3)))
            this.w_DESPER = NVL(MDDESCRI140,space(50))
            this.w_MD__FREQ = NVL(MD__FREQ140,space(1))
          else
          .link_1_40('Load')
          endif
        .w_ELDATFAT = NVL(cp_ToDate(ELDATFAT),ctod("  /  /  "))
        .w_ELGIODOC = NVL(ELGIODOC,0)
        .w_ELESCRIN = NVL(ELESCRIN,space(1))
        .w_ELPERCON = NVL(ELPERCON,space(3))
          if link_1_46_joined
            this.w_ELPERCON = NVL(MDCODICE146,NVL(this.w_ELPERCON,space(3)))
            this.w_MDDESCRI = NVL(MDDESCRI146,space(50))
          else
          .link_1_46('Load')
          endif
        .w_FLDATFIS = IIF(.w_DATACONT='M',.w_DATAFISSA,.w_DATACONT)
        .w_ELRINCON = NVL(ELRINCON,space(1))
        .w_ELNUMGIO = NVL(ELNUMGIO,0)
        .w_ELDATDIS = NVL(cp_ToDate(ELDATDIS),ctod("  /  /  "))
        .w_ELGESRAT = NVL(ELGESRAT,space(1))
        .w_CACODART = .w_ELCODART
          .link_1_62('Load')
        .w_ELDATDOC = NVL(cp_ToDate(ELDATDOC),ctod("  /  /  "))
        .w_ELDATGAT = NVL(cp_ToDate(ELDATGAT),ctod("  /  /  "))
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
          .link_1_118('Load')
        .w_OB_TEST = IIF(EMPTY(.w_ELDATATT), i_INIDAT, .w_ELDATATT)
        .w_ELPROLIS = NVL(ELPROLIS,space(5))
        .w_ELPROSCO = NVL(ELPROSCO,space(5))
        .w_FLGLIS = 'N'
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate()
        .w_NULLA = 'X'
        .w_MVCODVAL = .w_ELCODVAL
        .w_DATALIST = IIF(Not empty(.w_ELDATFAT),.w_ELDATFAT,IIF(Empty(.w_ELDATATT),.w_ELDATINI,.w_ELDATATT))
        .w_ELSCOLIS = NVL(ELSCOLIS,space(5))
        .w_ELTCOLIS = NVL(ELTCOLIS,space(5))
        .w_ARUNMIS1 = .w_UNMIS1
        .w_ARUNMIS2 = .w_UNMIS2
        .w_MVFLVEAC = 'V'
        .oPgFrm.Page1.oPag.oObj_1_146.Calculate()
        .w_CODIMP = .w_ELCODIMP
          .link_1_147('Load')
        .w_CODCOM = .w_ELCODCOM
          .link_1_148('Load')
        .w_ELCODART = NVL(ELCODART,space(20))
          if link_2_4_joined
            this.w_ELCODART = NVL(ARCODART204,NVL(this.w_ELCODART,space(20)))
            this.w_DESART = NVL(ARDESART204,space(40))
            this.w_TIPART = NVL(ARTIPART204,space(2))
            this.w_ARUNMIS1 = NVL(ARUNMIS1204,space(3))
            this.w_ARUNMIS2 = NVL(ARUNMIS2204,space(3))
            this.w_UNMIS1 = NVL(ARUNMIS1204,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2204,space(3))
            this.w_FLSERG = NVL(ARFLSERG204,space(1))
            this.w_PREZUM = NVL(ARPREZUM204,space(1))
            this.w_OPERAT = NVL(AROPERAT204,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP204,0)
            this.w_CATART = NVL(ARCATSCM204,space(5))
            this.w_FLUSEP = NVL(ARFLUSEP204,space(1))
            this.w_ELUNIMIS = NVL(ARUNMIS1204,space(3))
          else
          .link_2_4('Load')
          endif
        .w_ELCODART = NVL(ELCODART,space(20))
          if link_2_5_joined
            this.w_ELCODART = NVL(ARCODART205,NVL(this.w_ELCODART,space(20)))
            this.w_DESART = NVL(ARDESART205,space(40))
            this.w_TIPART = NVL(ARTIPART205,space(2))
            this.w_UNMIS1 = NVL(ARUNMIS1205,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2205,space(3))
            this.w_FLSERG = NVL(ARFLSERG205,space(1))
            this.w_PREZUM = NVL(ARPREZUM205,space(1))
            this.w_OPERAT = NVL(AROPERAT205,space(1))
            this.w_MOLTIP = NVL(ARMOLTIP205,0)
            this.w_CATART = NVL(ARCATSCM205,space(5))
            this.w_FLUSEP = NVL(ARFLUSEP205,space(1))
            this.w_ELUNIMIS = NVL(ARUNMIS1205,space(3))
          else
          .link_2_5('Load')
          endif
        .w_ARCODART = .w_ELCODART
        .w_ELUNIMIS = NVL(ELUNIMIS,space(3))
          if link_2_11_joined
            this.w_ELUNIMIS = NVL(UMCODICE211,NVL(this.w_ELUNIMIS,space(3)))
            this.w_DESUNI = NVL(UMDESCRI211,space(35))
          else
          .link_2_11('Load')
          endif
        .w_ELQTAMOV = NVL(ELQTAMOV,0)
        .w_ELCONCOD = NVL(ELCONCOD,space(15))
          .link_2_13('Load')
        .w_ELPREZZO = NVL(ELPREZZO,0)
        .w_ELSCONT1 = NVL(ELSCONT1,0)
        .w_ELSCONT2 = NVL(ELSCONT2,0)
        .w_ELSCONT3 = NVL(ELSCONT3,0)
        .w_ELSCONT4 = NVL(ELSCONT4,0)
        .w_ELSCONT1 = NVL(ELSCONT1,0)
        .w_ELSCONT2 = NVL(ELSCONT2,0)
        .w_ELSCONT3 = NVL(ELSCONT3,0)
        .w_ELSCONT4 = NVL(ELSCONT4,0)
        .oPgFrm.Page1.oPag.oObj_1_150.Calculate()
        .w_ELCODVAL = NVL(ELCODVAL,space(3))
          if link_1_153_joined
            this.w_ELCODVAL = NVL(VACODVAL253,NVL(this.w_ELCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL253,space(35))
            this.w_DECUNI = NVL(VADECUNI253,0)
          else
          .link_1_153('Load')
          endif
        .w_VOCRIC = cavocori( .w_elcodart, "R" )
        .w_ELVOCCEN = NVL(ELVOCCEN,space(15))
          if link_2_36_joined
            this.w_ELVOCCEN = NVL(VCCODICE236,NVL(this.w_ELVOCCEN,space(15)))
            this.w_DESVOC = NVL(VCDESCRI236,space(40))
            this.w_VOCOBSO = NVL(cp_ToDate(VCDTOBSO236),ctod("  /  /  "))
          else
          .link_2_36('Load')
          endif
        .w_ELCODCEN = NVL(ELCODCEN,space(15))
          if link_2_37_joined
            this.w_ELCODCEN = NVL(CC_CONTO237,NVL(this.w_ELCODCEN,space(15)))
            this.w_DESCEN = NVL(CCDESPIA237,space(40))
            this.w_CENOBSO = NVL(cp_ToDate(CCDTOBSO237),ctod("  /  /  "))
          else
          .link_2_37('Load')
          endif
        .w_ELCOCOMM = NVL(ELCOCOMM,space(15))
          if link_2_38_joined
            this.w_ELCOCOMM = NVL(CNCODCAN238,NVL(this.w_ELCOCOMM,space(15)))
            this.w_DESCOMM = NVL(CNDESCAN238,space(100))
            this.w_COMOBSO = NVL(cp_ToDate(CNDTOBSO238),ctod("  /  /  "))
          else
          .link_2_38('Load')
          endif
        .w_ELCODATT = NVL(ELCODATT,space(15))
          .link_2_39('Load')
        .w_CLI = .w_ELCODCLI
        .w_ELRINNOV = NVL(ELRINNOV,0)
        .oPgFrm.Page1.oPag.oObj_1_170.Calculate()
          .link_1_173('Load')
        .w_ELQTACON = NVL(ELQTACON,0)
        .w_QTA__RES = .w_ELQTAMOV - .w_ELQTACON
        .w_PREZZTOT = cp_Round(.w_ELQTAMOV * (cp_Round(.w_ELPREZZO *(100+.w_ELSCONT1)*(100+.w_ELSCONT2)*(100+.w_ELSCONT3)*(100+.w_ELSCONT4)/100000000  ,5) ),g_PERPVL)
        .oPgFrm.Page1.oPag.oObj_1_179.Calculate()
        .w_ELINICOA = NVL(cp_ToDate(ELINICOA),ctod("  /  /  "))
        .w_ELFINCOA = NVL(cp_ToDate(ELFINCOA),ctod("  /  /  "))
        .w_ELINICOD = NVL(cp_ToDate(ELINICOD),ctod("  /  /  "))
        .w_ELFINCOD = NVL(cp_ToDate(ELFINCOD),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_183.Calculate()
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCC = NVL(UTCC,0)
        cp_LoadRecExtFlds(this,'ELE_CONT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_7.enabled = this.oPgFrm.Page2.oPag.oBtn_2_7.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_8.enabled = this.oPgFrm.Page2.oPag.oBtn_2_8.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_9.enabled = this.oPgFrm.Page2.oPag.oBtn_2_9.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_177.enabled = this.oPgFrm.Page1.oPag.oBtn_1_177.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_178.enabled = this.oPgFrm.Page1.oPag.oBtn_1_178.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RESCHK = 0
      .w_ELCODMOD = space(10)
      .w_TIPCON = space(1)
      .w_KEYLISTB = space(10)
      .w_ELCODCLI = space(15)
      .w_ELCODIMP = space(10)
      .w_CODICE = space(15)
      .w_ELCODCOM = 0
      .w_ELCONTRA = space(10)
      .w_CODCLI = space(15)
      .w_TIPATT = space(20)
      .w_GRUPAR = space(5)
      .w_TIPDOC = space(3)
      .w_PERFAT = space(3)
      .w_PERATT = space(3)
      .w_TIPMOD = space(1)
      .w_DATINI = ctod("  /  /  ")
      .w_DATFIN = ctod("  /  /  ")
      .w_ELDATINI = ctod("  /  /  ")
      .w_FLATTI = space(1)
      .w_COPRIDAT = space(1)
      .w_COPRIDOC = space(1)
      .w_PRIMADATA = space(1)
      .w_PRIMADATADOC = space(1)
      .w_PRIDAT = space(1)
      .w_PRIDOC = space(1)
      .w_ELDATFIN = ctod("  /  /  ")
      .w_ELCATCON = space(5)
      .w_ELCODLIS = space(5)
      .w_ELTIPATT = space(20)
      .w_FLATT = space(1)
      .w_ELGRUPAR = space(5)
      .w_ELPERATT = space(3)
      .w_ELDATATT = ctod("  /  /  ")
      .w_ELGIOATT = 0
      .w_ELCAUDOC = space(5)
      .w_ELCODPAG = space(5)
      .w_ELPERFAT = space(3)
      .w_ELDATFAT = ctod("  /  /  ")
      .w_ELGIODOC = 0
      .w_ELESCRIN = space(1)
      .w_DATAFISSA = space(1)
      .w_DATACONT = space(1)
      .w_ELPERCON = space(3)
      .w_FLDATFIS = space(1)
      .w_ELRINCON = space(1)
      .w_ELNUMGIO = 0
      .w_ELDATDIS = ctod("  /  /  ")
      .w_ELGESRAT = space(1)
      .w_CODPAG = space(5)
      .w_PATIPRIS = space(1)
      .w_FLNSAP = space(1)
      .w_CODART = space(20)
      .w_CACODART = space(41)
      .w_UNMIS2 = space(3)
      .w_UNMIS3 = space(3)
      .w_DESELE = space(60)
      .w_LIPREZZO = 0
      .w_ELDATDOC = ctod("  /  /  ")
      .w_ELDATGAT = ctod("  /  /  ")
      .w_DESCLI = space(50)
      .w_DESPER = space(50)
      .w_DESCAT = space(60)
      .w_DESCRIAT = space(50)
      .w_FLSERG = space(1)
      .w_DESCON = space(50)
      .w_DESATT = space(254)
      .w_DESDOC = space(35)
      .w_CARAGGST = space(10)
      .w_DESLIS = space(40)
      .w_PREZUM = space(1)
      .w_OPERAT = space(1)
      .w_MOLTIP = 0
      .w_CATCLI = space(5)
      .w_CATART = space(5)
      .w_SCOLIS = space(1)
      .w_DECUNI = 0
      .w_MOLTI3 = 0
      .w_OPERA3 = space(1)
      .w_FLUSEP = space(1)
      .w_FLFRAZ1 = space(1)
      .w_MODUM2 = space(2)
      .w_VALLIS = space(3)
      .w_DESCLI1 = space(60)
      .w_CATDOC = space(2)
      .w_FLVEAC = space(1)
      .w_TIPRIS = space(1)
      .w_CICLO = space(1)
      .w_DESPAG = space(30)
      .w_FLSCOR = space(1)
      .w_LORNET = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_INILIS = ctod("  /  /  ")
      .w_FINLIS = ctod("  /  /  ")
      .w_QUALIS = space(1)
      .w_NUMLIS = space(5)
      .w_IVACLI = space(1)
      .w_OB_TEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_TIPSER = space(1)
      .w_ELPROLIS = space(5)
      .w_ELPROSCO = space(5)
      .w_FLGLIS = space(1)
      .w_FLINTE = space(1)
      .w_NULLA = space(1)
      .w_MVCODVAL = space(5)
      .w_DATALIST = ctod("  /  /  ")
      .w_ELSCOLIS = space(5)
      .w_ELTCOLIS = space(5)
      .w_FLRIPS = space(5)
      .w_GESTGUID = space(10)
      .w_ARUNMIS1 = space(3)
      .w_ARUNMIS2 = space(3)
      .w_MVFLVEAC = space(1)
      .w_MCAUDOC = space(5)
      .w_CCODLIS = space(5)
      .w_MCODLIS = space(5)
      .w_IMDESCRI = space(50)
      .w_CPROWORD = 0
      .w_IMDESCON = space(50)
      .w_CODIMP = space(10)
      .w_CODCOM = 0
      .w_CPROWORD_IMP_DETT = 0
      .w_ATTIPATT = space(1)
      .w_ELCODART = space(20)
      .w_ELCODART = space(20)
      .w_ARCODART = space(20)
      .w_ELUNIMIS = space(3)
      .w_ELQTAMOV = 0
      .w_ELCONCOD = space(15)
      .w_ELPREZZO = 0
      .w_ELSCONT1 = 0
      .w_ELSCONT2 = 0
      .w_ELSCONT3 = 0
      .w_ELSCONT4 = 0
      .w_ELSCONT1 = 0
      .w_ELSCONT2 = 0
      .w_ELSCONT3 = 0
      .w_ELSCONT4 = 0
      .w_DESUNI = space(35)
      .w_DESCTR = space(50)
      .w_DESART = space(40)
      .w_DPDESCRI = space(60)
      .w_ELCODVAL = space(3)
      .w_DESVAL = space(35)
      .w_VOCRIC = space(10)
      .w_ELVOCCEN = space(15)
      .w_ELCODCEN = space(15)
      .w_ELCOCOMM = space(15)
      .w_ELCODATT = space(15)
      .w_DESVOC = space(40)
      .w_DESCEN = space(40)
      .w_DESCOMM = space(100)
      .w_DESATTI = space(100)
      .w_FLANAL = space(1)
      .w_FLGCOM = space(1)
      .w_CODCEN = space(15)
      .w_COCOMM = space(15)
      .w_VOCOBSO = ctod("  /  /  ")
      .w_CENOBSO = ctod("  /  /  ")
      .w_COMOBSO = ctod("  /  /  ")
      .w_MCOCOMM = space(15)
      .w_CLI = space(15)
      .w_ELRINNOV = 0
      .w_MDDESCRI = space(50)
      .w_CATART = space(5)
      .w_MCODATT = space(15)
      .w_MOQTAMOV = 0
      .w_UNMIS1 = space(3)
      .w_ELQTACON = 0
      .w_QTA__RES = 0
      .w_PREZZTOT = 0
      .w_MD__FREQ = space(1)
      .w_ATTEXIST = .f.
      .w_DOCEXIST = .f.
      .w_CT = space(1)
      .w_CM = space(3)
      .w_CC = space(15)
      .w_CI = ctod("  /  /  ")
      .w_CF = ctod("  /  /  ")
      .w_QUACON = space(1)
      .w_CV = space(3)
      .w_IVACON = space(1)
      .w_CATCOM = space(3)
      .w_TIPART = space(2)
      .w_MOTIPCON = space(1)
      .w_DTBSO_CAUMATTI = ctod("  /  /  ")
      .w_ELINICOA = ctod("  /  /  ")
      .w_ELFINCOA = ctod("  /  /  ")
      .w_ELINICOD = ctod("  /  /  ")
      .w_ELFINCOD = ctod("  /  /  ")
      .w_UTDC = ctot("")
      .w_UTCV = 0
      .w_UTDV = ctot("")
      .w_UTCC = 0
      if .cFunction<>"Filter"
        .w_RESCHK = 0
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_ELCODMOD))
          .link_1_2('Full')
          endif
        .w_TIPCON = 'C'
        .w_KEYLISTB = Sys(2015)
        .w_ELCODCLI = .w_CODCLI
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_ELCODCLI))
          .link_1_5('Full')
          endif
          .DoRTCalc(6,6,.f.)
        .w_CODICE = .w_ELCODCLI
        .DoRTCalc(8,9,.f.)
          if not(empty(.w_ELCONTRA))
          .link_1_9('Full')
          endif
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_CODCLI))
          .link_1_10('Full')
          endif
          .DoRTCalc(11,22,.f.)
        .w_PRIMADATA = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'', IIF(.w_FLATTI<>'S','',IIF(.w_COPRIDAT='M',.w_PRIDAT,.w_COPRIDAT)))
        .w_PRIMADATADOC = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'',IIF(.w_FLATT<>'S','',IIF(.w_COPRIDOC='M',.w_PRIDOC,.w_COPRIDOC)))
        .DoRTCalc(25,28,.f.)
          if not(empty(.w_ELCATCON))
          .link_1_30('Full')
          endif
        .w_ELCODLIS = ICASE(!EMPTY(NVL(.w_CCODLIS,'')),.w_CCODLIS,!EMPTY(NVL(.w_MCODLIS,'')),.w_MCODLIS,.w_ELCODLIS)
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_ELCODLIS))
          .link_1_31('Full')
          endif
        .w_ELTIPATT = IIF(.w_FLATTI<>'S', SPACE(20), IIF(NOT EMPTY(.w_TIPATT), .w_TIPATT, .w_ELTIPATT))
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_ELTIPATT))
          .link_1_32('Full')
          endif
          .DoRTCalc(31,31,.f.)
        .w_ELGRUPAR = IIF(.w_FLATTI<>'S', SPACE(5), IIF(NOT EMPTY(.w_GRUPAR), .w_GRUPAR, .w_ELGRUPAR))
        .DoRTCalc(32,32,.f.)
          if not(empty(.w_ELGRUPAR))
          .link_1_34('Full')
          endif
        .w_ELPERATT = IIF(.w_FLATTI<>'S' OR EMPTY(.w_ELTIPATT), SPACE(3), .w_PERATT)
        .DoRTCalc(33,33,.f.)
          if not(empty(.w_ELPERATT))
          .link_1_35('Full')
          endif
        .w_ELDATATT = IIF(EMPTY(.w_ELTIPATT),CP_CHARTODATE('  -  -  '),IIF(.w_PRIMADATA='I',.w_ELDATINI,IIF(.w_PRIMADATA='P' AND NOT EMPTY(.w_ELPERATT),NEXTTIME( .w_ELPERATT, .w_ELDATINI-1,.F.,.T. ),IIF(.w_PRIMADATA='F',.w_ELDATFIN,.w_ELDATATT))))
          .DoRTCalc(35,35,.f.)
        .w_ELCAUDOC = IIF(.w_FLATT<>'S',SPACE(5), IIF(NOT EMPTY(.w_TIPDOC), .w_TIPDOC, .w_MCAUDOC))
        .DoRTCalc(36,36,.f.)
          if not(empty(.w_ELCAUDOC))
          .link_1_38('Full')
          endif
        .w_ELCODPAG = iif(.w_FLATT='S',IIF(NOT EMPTY(.w_CODPAG), .w_CODPAG, .w_ELCODPAG),Space(5))
        .DoRTCalc(37,37,.f.)
          if not(empty(.w_ELCODPAG))
          .link_1_39('Full')
          endif
        .w_ELPERFAT = IIF(.w_FLATT<>'S' or empty(.w_ELCAUDOC), SPACE(3), .w_PERFAT)
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_ELPERFAT))
          .link_1_40('Full')
          endif
        .w_ELDATFAT = IIF(EMPTY(.w_ELCAUDOC),CP_CHARTODATE('  -  -  '),IIF(.w_PRIMADATADOC='I',.w_ELDATINI,IIF(.w_PRIMADATADOC='P' AND NOT EMPTY(.w_ELPERFAT),NEXTTIME( .w_ELPERFAT, .w_ELDATINI-1,.F.,.T. ),IIF(.w_PRIMADATADOC='F',.w_ELDATFIN,.w_ELDATFAT))))
          .DoRTCalc(40,40,.f.)
        .w_ELESCRIN = "N"
          .DoRTCalc(42,43,.f.)
        .w_ELPERCON = IIF( .w_ELESCRIN <>"N", "   ", .w_ELPERCON )
        .DoRTCalc(44,44,.f.)
          if not(empty(.w_ELPERCON))
          .link_1_46('Full')
          endif
        .w_FLDATFIS = IIF(.w_DATACONT='M',.w_DATAFISSA,.w_DATACONT)
        .w_ELRINCON = iif(.w_ELESCRIN='S',"N",.w_ELRINCON)
        .w_ELNUMGIO = IIF(.w_ELRINCON = "S" ,.w_ELNUMGIO,0)
        .w_ELDATDIS = IIF(.w_ELRINCON = "S" ,.w_ELDATFIN - .w_ELNUMGIO,CP_Chartodate('  -  -    '))
          .DoRTCalc(49,50,.f.)
        .w_PATIPRIS = 'G'
          .DoRTCalc(52,53,.f.)
        .w_CACODART = .w_ELCODART
        .DoRTCalc(54,54,.f.)
          if not(empty(.w_CACODART))
          .link_1_62('Full')
          endif
          .DoRTCalc(55,58,.f.)
        .w_ELDATDOC = IIF(.w_FLATT='S',.w_ELDATFIN+.w_ELGIODOC, CP_CHARTODATE('  -  -  '))
        .w_ELDATGAT = IIF(.w_FLATTI='S',.w_ELDATFIN+.w_ELGIOATT, CP_CHARTODATE('  -  -  '))
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
          .DoRTCalc(61,87,.f.)
        .w_CICLO = 'E'
          .DoRTCalc(89,91,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(93,96,.f.)
          if not(empty(.w_NUMLIS))
          .link_1_118('Full')
          endif
          .DoRTCalc(97,97,.f.)
        .w_OB_TEST = IIF(EMPTY(.w_ELDATATT), i_INIDAT, .w_ELDATATT)
          .DoRTCalc(99,102,.f.)
        .w_FLGLIS = 'N'
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate()
          .DoRTCalc(104,104,.f.)
        .w_NULLA = 'X'
        .w_MVCODVAL = .w_ELCODVAL
        .w_DATALIST = IIF(Not empty(.w_ELDATFAT),.w_ELDATFAT,IIF(Empty(.w_ELDATATT),.w_ELDATINI,.w_ELDATATT))
          .DoRTCalc(108,111,.f.)
        .w_ARUNMIS1 = .w_UNMIS1
        .w_ARUNMIS2 = .w_UNMIS2
        .w_MVFLVEAC = 'V'
        .oPgFrm.Page1.oPag.oObj_1_146.Calculate()
          .DoRTCalc(115,120,.f.)
        .w_CODIMP = .w_ELCODIMP
        .DoRTCalc(121,121,.f.)
          if not(empty(.w_CODIMP))
          .link_1_147('Full')
          endif
        .w_CODCOM = .w_ELCODCOM
        .DoRTCalc(122,122,.f.)
          if not(empty(.w_CODCOM))
          .link_1_148('Full')
          endif
          .DoRTCalc(123,123,.f.)
        .w_ATTIPATT = 'A'
        .w_ELCODART = .w_CODART
        .DoRTCalc(125,125,.f.)
          if not(empty(.w_ELCODART))
          .link_2_4('Full')
          endif
        .w_ELCODART = .w_CODART
        .DoRTCalc(126,126,.f.)
          if not(empty(.w_ELCODART))
          .link_2_5('Full')
          endif
        .w_ARCODART = .w_ELCODART
        .w_ELUNIMIS = .w_UNMIS1
        .DoRTCalc(128,128,.f.)
          if not(empty(.w_ELUNIMIS))
          .link_2_11('Full')
          endif
        .w_ELQTAMOV = IIF( .w_MOTIPCON<>'P', IIF(EMPTY(NVL(.w_ELCODART,'')) OR .w_TIPART='FO',1, .w_ELQTAMOV), .w_MOQTAMOV )
        .DoRTCalc(130,130,.f.)
          if not(empty(.w_ELCONCOD))
          .link_2_13('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_150.Calculate()
        .DoRTCalc(131,144,.f.)
          if not(empty(.w_ELCODVAL))
          .link_1_153('Full')
          endif
          .DoRTCalc(145,145,.f.)
        .w_VOCRIC = cavocori( .w_elcodart, "R" )
        .w_ELVOCCEN = IIF(NOT EMPTY(.w_VOCRIC), .w_VOCRIC, .w_ELVOCCEN)
        .DoRTCalc(147,147,.f.)
          if not(empty(.w_ELVOCCEN))
          .link_2_36('Full')
          endif
        .w_ELCODCEN = iif(NOT EMPTY(.w_CODCEN), .w_CODCEN, .w_ELCODCEN)
        .DoRTCalc(148,148,.f.)
          if not(empty(.w_ELCODCEN))
          .link_2_37('Full')
          endif
        .w_ELCOCOMM = iif(NOT EMPTY(.w_COCOMM), .w_COCOMM, .w_MCOCOMM)
        .DoRTCalc(149,149,.f.)
          if not(empty(.w_ELCOCOMM))
          .link_2_38('Full')
          endif
        .w_ELCODATT = IIF(.w_COCOMM<>.w_MCOCOMM and not empty(.w_COCOMM) and .w_ELCOCOMM=.w_COCOMM, SPACE(15), iif(not empty(.w_MCOCOMM) and .w_ELCOCOMM= .w_MCOCOMM and not empty(.w_MCODATT), .w_MCODATT, .w_ELCODATT))
        .DoRTCalc(150,150,.f.)
          if not(empty(.w_ELCODATT))
          .link_2_39('Full')
          endif
          .DoRTCalc(151,162,.f.)
        .w_CLI = .w_ELCODCLI
        .oPgFrm.Page1.oPag.oObj_1_170.Calculate()
        .DoRTCalc(164,169,.f.)
          if not(empty(.w_UNMIS1))
          .link_1_173('Full')
          endif
          .DoRTCalc(170,170,.f.)
        .w_QTA__RES = .w_ELQTAMOV - .w_ELQTACON
        .w_PREZZTOT = cp_Round(.w_ELQTAMOV * (cp_Round(.w_ELPREZZO *(100+.w_ELSCONT1)*(100+.w_ELSCONT2)*(100+.w_ELSCONT3)*(100+.w_ELSCONT4)/100000000  ,5) ),g_PERPVL)
        .oPgFrm.Page1.oPag.oObj_1_179.Calculate()
          .DoRTCalc(173,186,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
          .DoRTCalc(188,188,.f.)
        .w_ELFINCOA = IIF(NOT EMPTY(.w_ELPERATT), NEXTTIME( .w_ELPERATT, .w_ELINICOA-1,.F.,.T. ),.w_ELINICOA)
          .DoRTCalc(190,190,.f.)
        .w_ELFINCOD = IIF(NOT EMPTY(.w_ELPERFAT), NEXTTIME( .w_ELPERFAT, .w_ELINICOD-1,.F.,.T. ),.w_ELFINCOD)
        .oPgFrm.Page1.oPag.oObj_1_183.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ELE_CONT')
    this.DoRTCalc(192,195,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_7.enabled = this.oPgFrm.Page2.oPag.oBtn_2_7.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_8.enabled = this.oPgFrm.Page2.oPag.oBtn_2_8.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_9.enabled = this.oPgFrm.Page2.oPag.oBtn_2_9.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_177.enabled = this.oPgFrm.Page1.oPag.oBtn_1_177.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_178.enabled = this.oPgFrm.Page1.oPag.oBtn_1_178.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oELCODIMP_1_6.enabled = !i_bVal
      .Page1.oPag.oELCODCOM_1_8.enabled = !i_bVal
      .Page1.oPag.oELRINNOV_1_162.enabled = !i_bVal
      .Page1.oPag.oELCODMOD_1_2.enabled = i_bVal
      .Page1.oPag.oELCODCLI_1_5.enabled = i_bVal
      .Page1.oPag.oELCONTRA_1_9.enabled = i_bVal
      .Page1.oPag.oELDATINI_1_21.enabled = i_bVal
      .Page1.oPag.oELDATFIN_1_29.enabled = i_bVal
      .Page1.oPag.oELCATCON_1_30.enabled = i_bVal
      .Page1.oPag.oELCODLIS_1_31.enabled = i_bVal
      .Page1.oPag.oELTIPATT_1_32.enabled = i_bVal
      .Page1.oPag.oELGRUPAR_1_34.enabled = i_bVal
      .Page1.oPag.oELPERATT_1_35.enabled = i_bVal
      .Page1.oPag.oELDATATT_1_36.enabled = i_bVal
      .Page1.oPag.oELGIOATT_1_37.enabled = i_bVal
      .Page1.oPag.oELCAUDOC_1_38.enabled = i_bVal
      .Page1.oPag.oELCODPAG_1_39.enabled = i_bVal
      .Page1.oPag.oELPERFAT_1_40.enabled = i_bVal
      .Page1.oPag.oELDATFAT_1_41.enabled = i_bVal
      .Page1.oPag.oELGIODOC_1_42.enabled = i_bVal
      .Page1.oPag.oELESCRIN_1_43.enabled = i_bVal
      .Page1.oPag.oELPERCON_1_46.enabled = i_bVal
      .Page1.oPag.oELRINCON_1_48.enabled = i_bVal
      .Page1.oPag.oELNUMGIO_1_49.enabled = i_bVal
      .Page1.oPag.oELDATDIS_1_50.enabled = i_bVal
      .Page2.oPag.oELCODART_2_4.enabled = i_bVal
      .Page2.oPag.oELCODART_2_5.enabled = i_bVal
      .Page2.oPag.oELUNIMIS_2_11.enabled = i_bVal
      .Page2.oPag.oELQTAMOV_2_12.enabled = i_bVal
      .Page2.oPag.oELCONCOD_2_13.enabled = i_bVal
      .Page2.oPag.oELPREZZO_2_14.enabled = i_bVal
      .Page2.oPag.oELSCONT1_2_15.enabled = i_bVal
      .Page2.oPag.oELSCONT2_2_16.enabled = i_bVal
      .Page2.oPag.oELSCONT3_2_17.enabled = i_bVal
      .Page2.oPag.oELSCONT4_2_18.enabled = i_bVal
      .Page2.oPag.oELVOCCEN_2_36.enabled = i_bVal
      .Page2.oPag.oELCODCEN_2_37.enabled = i_bVal
      .Page2.oPag.oELCOCOMM_2_38.enabled = i_bVal
      .Page2.oPag.oELCODATT_2_39.enabled = i_bVal
      .Page2.oPag.oPREZZTOT_2_64.enabled = i_bVal
      .Page2.oPag.oELINICOA_2_81.enabled = i_bVal
      .Page2.oPag.oELFINCOA_2_82.enabled = i_bVal
      .Page2.oPag.oELINICOD_2_83.enabled = i_bVal
      .Page2.oPag.oELFINCOD_2_84.enabled = i_bVal
      .Page1.oPag.oBtn_1_51.enabled = .Page1.oPag.oBtn_1_51.mCond()
      .Page2.oPag.oBtn_2_7.enabled = .Page2.oPag.oBtn_2_7.mCond()
      .Page2.oPag.oBtn_2_8.enabled = .Page2.oPag.oBtn_2_8.mCond()
      .Page2.oPag.oBtn_2_9.enabled = .Page2.oPag.oBtn_2_9.mCond()
      .Page2.oPag.oBtn_2_10.enabled = .Page2.oPag.oBtn_2_10.mCond()
      .Page1.oPag.oBtn_1_177.enabled = .Page1.oPag.oBtn_1_177.mCond()
      .Page1.oPag.oBtn_1_178.enabled = .Page1.oPag.oBtn_1_178.mCond()
      .Page2.oPag.oObj_2_1.enabled = i_bVal
      .Page2.oPag.oObj_2_2.enabled = i_bVal
      .Page1.oPag.oObj_1_127.enabled = i_bVal
      .Page1.oPag.oObj_1_146.enabled = i_bVal
      .Page1.oPag.oObj_1_150.enabled = i_bVal
      .Page1.oPag.oObj_1_170.enabled = i_bVal
      .Page1.oPag.oObj_1_179.enabled = i_bVal
      .Page1.oPag.oObj_1_183.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oELCODMOD_1_2.enabled = .f.
        .Page1.oPag.oELCONTRA_1_9.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oELCODMOD_1_2.enabled = .t.
        .Page1.oPag.oELCONTRA_1_9.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ELE_CONT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsag_ael
    AbilitaBtnRinnovo(This)
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODMOD,"ELCODMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODIMP,"ELCODIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODCOM,"ELCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCONTRA,"ELCONTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELDATINI,"ELDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELDATFIN,"ELDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCATCON,"ELCATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODLIS,"ELCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELTIPATT,"ELTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELGRUPAR,"ELGRUPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELPERATT,"ELPERATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELDATATT,"ELDATATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELGIOATT,"ELGIOATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCAUDOC,"ELCAUDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODPAG,"ELCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELPERFAT,"ELPERFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELDATFAT,"ELDATFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELGIODOC,"ELGIODOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELESCRIN,"ELESCRIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELPERCON,"ELPERCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELRINCON,"ELRINCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELNUMGIO,"ELNUMGIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELDATDIS,"ELDATDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELGESRAT,"ELGESRAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELDATDOC,"ELDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELDATGAT,"ELDATGAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELPROLIS,"ELPROLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELPROSCO,"ELPROSCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELSCOLIS,"ELSCOLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELTCOLIS,"ELTCOLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODART,"ELCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODART,"ELCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELUNIMIS,"ELUNIMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELQTAMOV,"ELQTAMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCONCOD,"ELCONCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELPREZZO,"ELPREZZO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELSCONT1,"ELSCONT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELSCONT2,"ELSCONT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELSCONT3,"ELSCONT3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELSCONT4,"ELSCONT4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELSCONT1,"ELSCONT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELSCONT2,"ELSCONT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELSCONT3,"ELSCONT3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELSCONT4,"ELSCONT4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODVAL,"ELCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELVOCCEN,"ELVOCCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODCEN,"ELCODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCOCOMM,"ELCOCOMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODATT,"ELCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELRINNOV,"ELRINNOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELQTACON,"ELQTACON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELINICOA,"ELINICOA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELFINCOA,"ELFINCOA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELINICOD,"ELINICOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELFINCOD,"ELFINCOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
    i_lTable = "ELE_CONT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ELE_CONT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ELE_CONT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ELE_CONT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ELE_CONT')
        i_extval=cp_InsertValODBCExtFlds(this,'ELE_CONT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ELCODMOD,ELCODIMP,ELCODCOM,ELCONTRA,ELDATINI"+;
                  ",ELDATFIN,ELCATCON,ELCODLIS,ELTIPATT,ELGRUPAR"+;
                  ",ELPERATT,ELDATATT,ELGIOATT,ELCAUDOC,ELCODPAG"+;
                  ",ELPERFAT,ELDATFAT,ELGIODOC,ELESCRIN,ELPERCON"+;
                  ",ELRINCON,ELNUMGIO,ELDATDIS,ELGESRAT,ELDATDOC"+;
                  ",ELDATGAT,ELPROLIS,ELPROSCO,ELSCOLIS,ELTCOLIS"+;
                  ",ELCODART,ELUNIMIS,ELQTAMOV,ELCONCOD,ELPREZZO"+;
                  ",ELSCONT1,ELSCONT2,ELSCONT3,ELSCONT4,ELCODVAL"+;
                  ",ELVOCCEN,ELCODCEN,ELCOCOMM,ELCODATT,ELRINNOV"+;
                  ",ELQTACON,ELINICOA,ELFINCOA,ELINICOD,ELFINCOD"+;
                  ",UTDC,UTCV,UTDV,UTCC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_ELCODMOD)+;
                  ","+cp_ToStrODBC(this.w_ELCODIMP)+;
                  ","+cp_ToStrODBC(this.w_ELCODCOM)+;
                  ","+cp_ToStrODBCNull(this.w_ELCONTRA)+;
                  ","+cp_ToStrODBC(this.w_ELDATINI)+;
                  ","+cp_ToStrODBC(this.w_ELDATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_ELCATCON)+;
                  ","+cp_ToStrODBCNull(this.w_ELCODLIS)+;
                  ","+cp_ToStrODBCNull(this.w_ELTIPATT)+;
                  ","+cp_ToStrODBCNull(this.w_ELGRUPAR)+;
                  ","+cp_ToStrODBCNull(this.w_ELPERATT)+;
                  ","+cp_ToStrODBC(this.w_ELDATATT)+;
                  ","+cp_ToStrODBC(this.w_ELGIOATT)+;
                  ","+cp_ToStrODBCNull(this.w_ELCAUDOC)+;
                  ","+cp_ToStrODBCNull(this.w_ELCODPAG)+;
                  ","+cp_ToStrODBCNull(this.w_ELPERFAT)+;
                  ","+cp_ToStrODBC(this.w_ELDATFAT)+;
                  ","+cp_ToStrODBC(this.w_ELGIODOC)+;
                  ","+cp_ToStrODBC(this.w_ELESCRIN)+;
                  ","+cp_ToStrODBCNull(this.w_ELPERCON)+;
                  ","+cp_ToStrODBC(this.w_ELRINCON)+;
                  ","+cp_ToStrODBC(this.w_ELNUMGIO)+;
                  ","+cp_ToStrODBC(this.w_ELDATDIS)+;
                  ","+cp_ToStrODBC(this.w_ELGESRAT)+;
                  ","+cp_ToStrODBC(this.w_ELDATDOC)+;
                  ","+cp_ToStrODBC(this.w_ELDATGAT)+;
                  ","+cp_ToStrODBC(this.w_ELPROLIS)+;
                  ","+cp_ToStrODBC(this.w_ELPROSCO)+;
                  ","+cp_ToStrODBC(this.w_ELSCOLIS)+;
                  ","+cp_ToStrODBC(this.w_ELTCOLIS)+;
                  ","+cp_ToStrODBCNull(this.w_ELCODART)+;
                  ","+cp_ToStrODBCNull(this.w_ELUNIMIS)+;
                  ","+cp_ToStrODBC(this.w_ELQTAMOV)+;
                  ","+cp_ToStrODBCNull(this.w_ELCONCOD)+;
                  ","+cp_ToStrODBC(this.w_ELPREZZO)+;
                  ","+cp_ToStrODBC(this.w_ELSCONT1)+;
                  ","+cp_ToStrODBC(this.w_ELSCONT2)+;
                  ","+cp_ToStrODBC(this.w_ELSCONT3)+;
                  ","+cp_ToStrODBC(this.w_ELSCONT4)+;
                  ","+cp_ToStrODBCNull(this.w_ELCODVAL)+;
                  ","+cp_ToStrODBCNull(this.w_ELVOCCEN)+;
                  ","+cp_ToStrODBCNull(this.w_ELCODCEN)+;
                  ","+cp_ToStrODBCNull(this.w_ELCOCOMM)+;
                  ","+cp_ToStrODBCNull(this.w_ELCODATT)+;
                  ","+cp_ToStrODBC(this.w_ELRINNOV)+;
                  ","+cp_ToStrODBC(this.w_ELQTACON)+;
                  ","+cp_ToStrODBC(this.w_ELINICOA)+;
                  ","+cp_ToStrODBC(this.w_ELFINCOA)+;
                  ","+cp_ToStrODBC(this.w_ELINICOD)+;
                  ","+cp_ToStrODBC(this.w_ELFINCOD)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ELE_CONT')
        i_extval=cp_InsertValVFPExtFlds(this,'ELE_CONT')
        cp_CheckDeletedKey(i_cTable,0,'ELCODMOD',this.w_ELCODMOD,'ELCODIMP',this.w_ELCODIMP,'ELCODCOM',this.w_ELCODCOM,'ELCONTRA',this.w_ELCONTRA,'ELRINNOV',this.w_ELRINNOV)
        INSERT INTO (i_cTable);
              (ELCODMOD,ELCODIMP,ELCODCOM,ELCONTRA,ELDATINI,ELDATFIN,ELCATCON,ELCODLIS,ELTIPATT,ELGRUPAR,ELPERATT,ELDATATT,ELGIOATT,ELCAUDOC,ELCODPAG,ELPERFAT,ELDATFAT,ELGIODOC,ELESCRIN,ELPERCON,ELRINCON,ELNUMGIO,ELDATDIS,ELGESRAT,ELDATDOC,ELDATGAT,ELPROLIS,ELPROSCO,ELSCOLIS,ELTCOLIS,ELCODART,ELUNIMIS,ELQTAMOV,ELCONCOD,ELPREZZO,ELSCONT1,ELSCONT2,ELSCONT3,ELSCONT4,ELCODVAL,ELVOCCEN,ELCODCEN,ELCOCOMM,ELCODATT,ELRINNOV,ELQTACON,ELINICOA,ELFINCOA,ELINICOD,ELFINCOD,UTDC,UTCV,UTDV,UTCC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ELCODMOD;
                  ,this.w_ELCODIMP;
                  ,this.w_ELCODCOM;
                  ,this.w_ELCONTRA;
                  ,this.w_ELDATINI;
                  ,this.w_ELDATFIN;
                  ,this.w_ELCATCON;
                  ,this.w_ELCODLIS;
                  ,this.w_ELTIPATT;
                  ,this.w_ELGRUPAR;
                  ,this.w_ELPERATT;
                  ,this.w_ELDATATT;
                  ,this.w_ELGIOATT;
                  ,this.w_ELCAUDOC;
                  ,this.w_ELCODPAG;
                  ,this.w_ELPERFAT;
                  ,this.w_ELDATFAT;
                  ,this.w_ELGIODOC;
                  ,this.w_ELESCRIN;
                  ,this.w_ELPERCON;
                  ,this.w_ELRINCON;
                  ,this.w_ELNUMGIO;
                  ,this.w_ELDATDIS;
                  ,this.w_ELGESRAT;
                  ,this.w_ELDATDOC;
                  ,this.w_ELDATGAT;
                  ,this.w_ELPROLIS;
                  ,this.w_ELPROSCO;
                  ,this.w_ELSCOLIS;
                  ,this.w_ELTCOLIS;
                  ,this.w_ELCODART;
                  ,this.w_ELUNIMIS;
                  ,this.w_ELQTAMOV;
                  ,this.w_ELCONCOD;
                  ,this.w_ELPREZZO;
                  ,this.w_ELSCONT1;
                  ,this.w_ELSCONT2;
                  ,this.w_ELSCONT3;
                  ,this.w_ELSCONT4;
                  ,this.w_ELCODVAL;
                  ,this.w_ELVOCCEN;
                  ,this.w_ELCODCEN;
                  ,this.w_ELCOCOMM;
                  ,this.w_ELCODATT;
                  ,this.w_ELRINNOV;
                  ,this.w_ELQTACON;
                  ,this.w_ELINICOA;
                  ,this.w_ELFINCOA;
                  ,this.w_ELINICOD;
                  ,this.w_ELFINCOD;
                  ,this.w_UTDC;
                  ,this.w_UTCV;
                  ,this.w_UTDV;
                  ,this.w_UTCC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ELE_CONT_IDX,i_nConn)
      *
      * update ELE_CONT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ELE_CONT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ELDATINI="+cp_ToStrODBC(this.w_ELDATINI)+;
             ",ELDATFIN="+cp_ToStrODBC(this.w_ELDATFIN)+;
             ",ELCATCON="+cp_ToStrODBCNull(this.w_ELCATCON)+;
             ",ELCODLIS="+cp_ToStrODBCNull(this.w_ELCODLIS)+;
             ",ELTIPATT="+cp_ToStrODBCNull(this.w_ELTIPATT)+;
             ",ELGRUPAR="+cp_ToStrODBCNull(this.w_ELGRUPAR)+;
             ",ELPERATT="+cp_ToStrODBCNull(this.w_ELPERATT)+;
             ",ELDATATT="+cp_ToStrODBC(this.w_ELDATATT)+;
             ",ELGIOATT="+cp_ToStrODBC(this.w_ELGIOATT)+;
             ",ELCAUDOC="+cp_ToStrODBCNull(this.w_ELCAUDOC)+;
             ",ELCODPAG="+cp_ToStrODBCNull(this.w_ELCODPAG)+;
             ",ELPERFAT="+cp_ToStrODBCNull(this.w_ELPERFAT)+;
             ",ELDATFAT="+cp_ToStrODBC(this.w_ELDATFAT)+;
             ",ELGIODOC="+cp_ToStrODBC(this.w_ELGIODOC)+;
             ",ELESCRIN="+cp_ToStrODBC(this.w_ELESCRIN)+;
             ",ELPERCON="+cp_ToStrODBCNull(this.w_ELPERCON)+;
             ",ELRINCON="+cp_ToStrODBC(this.w_ELRINCON)+;
             ",ELNUMGIO="+cp_ToStrODBC(this.w_ELNUMGIO)+;
             ",ELDATDIS="+cp_ToStrODBC(this.w_ELDATDIS)+;
             ",ELGESRAT="+cp_ToStrODBC(this.w_ELGESRAT)+;
             ",ELDATDOC="+cp_ToStrODBC(this.w_ELDATDOC)+;
             ",ELDATGAT="+cp_ToStrODBC(this.w_ELDATGAT)+;
             ",ELPROLIS="+cp_ToStrODBC(this.w_ELPROLIS)+;
             ",ELPROSCO="+cp_ToStrODBC(this.w_ELPROSCO)+;
             ",ELSCOLIS="+cp_ToStrODBC(this.w_ELSCOLIS)+;
             ",ELTCOLIS="+cp_ToStrODBC(this.w_ELTCOLIS)+;
             ",ELCODART="+cp_ToStrODBCNull(this.w_ELCODART)+;
             ",ELUNIMIS="+cp_ToStrODBCNull(this.w_ELUNIMIS)+;
             ",ELQTAMOV="+cp_ToStrODBC(this.w_ELQTAMOV)+;
             ",ELCONCOD="+cp_ToStrODBCNull(this.w_ELCONCOD)+;
             ",ELPREZZO="+cp_ToStrODBC(this.w_ELPREZZO)+;
             ",ELSCONT1="+cp_ToStrODBC(this.w_ELSCONT1)+;
             ",ELSCONT2="+cp_ToStrODBC(this.w_ELSCONT2)+;
             ",ELSCONT3="+cp_ToStrODBC(this.w_ELSCONT3)+;
             ",ELSCONT4="+cp_ToStrODBC(this.w_ELSCONT4)+;
             ",ELCODVAL="+cp_ToStrODBCNull(this.w_ELCODVAL)+;
             ",ELVOCCEN="+cp_ToStrODBCNull(this.w_ELVOCCEN)+;
             ",ELCODCEN="+cp_ToStrODBCNull(this.w_ELCODCEN)+;
             ",ELCOCOMM="+cp_ToStrODBCNull(this.w_ELCOCOMM)+;
             ",ELCODATT="+cp_ToStrODBCNull(this.w_ELCODATT)+;
             ",ELQTACON="+cp_ToStrODBC(this.w_ELQTACON)+;
             ",ELINICOA="+cp_ToStrODBC(this.w_ELINICOA)+;
             ",ELFINCOA="+cp_ToStrODBC(this.w_ELFINCOA)+;
             ",ELINICOD="+cp_ToStrODBC(this.w_ELINICOD)+;
             ",ELFINCOD="+cp_ToStrODBC(this.w_ELFINCOD)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ELE_CONT')
        i_cWhere = cp_PKFox(i_cTable  ,'ELCODMOD',this.w_ELCODMOD  ,'ELCODIMP',this.w_ELCODIMP  ,'ELCODCOM',this.w_ELCODCOM  ,'ELCONTRA',this.w_ELCONTRA  ,'ELRINNOV',this.w_ELRINNOV  )
        UPDATE (i_cTable) SET;
              ELDATINI=this.w_ELDATINI;
             ,ELDATFIN=this.w_ELDATFIN;
             ,ELCATCON=this.w_ELCATCON;
             ,ELCODLIS=this.w_ELCODLIS;
             ,ELTIPATT=this.w_ELTIPATT;
             ,ELGRUPAR=this.w_ELGRUPAR;
             ,ELPERATT=this.w_ELPERATT;
             ,ELDATATT=this.w_ELDATATT;
             ,ELGIOATT=this.w_ELGIOATT;
             ,ELCAUDOC=this.w_ELCAUDOC;
             ,ELCODPAG=this.w_ELCODPAG;
             ,ELPERFAT=this.w_ELPERFAT;
             ,ELDATFAT=this.w_ELDATFAT;
             ,ELGIODOC=this.w_ELGIODOC;
             ,ELESCRIN=this.w_ELESCRIN;
             ,ELPERCON=this.w_ELPERCON;
             ,ELRINCON=this.w_ELRINCON;
             ,ELNUMGIO=this.w_ELNUMGIO;
             ,ELDATDIS=this.w_ELDATDIS;
             ,ELGESRAT=this.w_ELGESRAT;
             ,ELDATDOC=this.w_ELDATDOC;
             ,ELDATGAT=this.w_ELDATGAT;
             ,ELPROLIS=this.w_ELPROLIS;
             ,ELPROSCO=this.w_ELPROSCO;
             ,ELSCOLIS=this.w_ELSCOLIS;
             ,ELTCOLIS=this.w_ELTCOLIS;
             ,ELCODART=this.w_ELCODART;
             ,ELUNIMIS=this.w_ELUNIMIS;
             ,ELQTAMOV=this.w_ELQTAMOV;
             ,ELCONCOD=this.w_ELCONCOD;
             ,ELPREZZO=this.w_ELPREZZO;
             ,ELSCONT1=this.w_ELSCONT1;
             ,ELSCONT2=this.w_ELSCONT2;
             ,ELSCONT3=this.w_ELSCONT3;
             ,ELSCONT4=this.w_ELSCONT4;
             ,ELCODVAL=this.w_ELCODVAL;
             ,ELVOCCEN=this.w_ELVOCCEN;
             ,ELCODCEN=this.w_ELCODCEN;
             ,ELCOCOMM=this.w_ELCOCOMM;
             ,ELCODATT=this.w_ELCODATT;
             ,ELQTACON=this.w_ELQTACON;
             ,ELINICOA=this.w_ELINICOA;
             ,ELFINCOA=this.w_ELFINCOA;
             ,ELINICOD=this.w_ELINICOD;
             ,ELFINCOD=this.w_ELFINCOD;
             ,UTDC=this.w_UTDC;
             ,UTCV=this.w_UTCV;
             ,UTDV=this.w_UTDV;
             ,UTCC=this.w_UTCC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ELE_CONT_IDX,i_nConn)
      *
      * delete ELE_CONT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ELCODMOD',this.w_ELCODMOD  ,'ELCODIMP',this.w_ELCODIMP  ,'ELCODCOM',this.w_ELCODCOM  ,'ELCONTRA',this.w_ELCONTRA  ,'ELRINNOV',this.w_ELRINNOV  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_TIPCON = 'C'
        .DoRTCalc(4,4,.t.)
        if .o_ELCONTRA<>.w_ELCONTRA
            .w_ELCODCLI = .w_CODCLI
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.t.)
            .w_CODICE = .w_ELCODCLI
        .DoRTCalc(8,9,.t.)
          .link_1_10('Full')
        .DoRTCalc(11,22,.t.)
            .w_PRIMADATA = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'', IIF(.w_FLATTI<>'S','',IIF(.w_COPRIDAT='M',.w_PRIDAT,.w_COPRIDAT)))
            .w_PRIMADATADOC = IIF(EMPTY(NVL(.w_ELCODMOD,'')) OR EMPTY(NVL(.w_ELCONTRA,'')),'',IIF(.w_FLATT<>'S','',IIF(.w_COPRIDOC='M',.w_PRIDOC,.w_COPRIDOC)))
        .DoRTCalc(25,27,.t.)
        if .o_ELCODMOD<>.w_ELCODMOD
          .link_1_30('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCONTRA<>.w_ELCONTRA
            .w_ELCODLIS = ICASE(!EMPTY(NVL(.w_CCODLIS,'')),.w_CCODLIS,!EMPTY(NVL(.w_MCODLIS,'')),.w_MCODLIS,.w_ELCODLIS)
          .link_1_31('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_TIPATT<>.w_TIPATT
            .w_ELTIPATT = IIF(.w_FLATTI<>'S', SPACE(20), IIF(NOT EMPTY(.w_TIPATT), .w_TIPATT, .w_ELTIPATT))
          .link_1_32('Full')
        endif
        .DoRTCalc(31,31,.t.)
        if .o_GRUPAR<>.w_GRUPAR.or. .o_ELCODMOD<>.w_ELCODMOD
            .w_ELGRUPAR = IIF(.w_FLATTI<>'S', SPACE(5), IIF(NOT EMPTY(.w_GRUPAR), .w_GRUPAR, .w_ELGRUPAR))
          .link_1_34('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_FLATTI<>.w_FLATTI
            .w_ELPERATT = IIF(.w_FLATTI<>'S' OR EMPTY(.w_ELTIPATT), SPACE(3), .w_PERATT)
          .link_1_35('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELPERATT<>.w_ELPERATT.or. .o_ELDATINI<>.w_ELDATINI.or. .o_ELDATFIN<>.w_ELDATFIN.or. .o_PRIMADATA<>.w_PRIMADATA.or. .o_FLATTI<>.w_FLATTI
            .w_ELDATATT = IIF(EMPTY(.w_ELTIPATT),CP_CHARTODATE('  -  -  '),IIF(.w_PRIMADATA='I',.w_ELDATINI,IIF(.w_PRIMADATA='P' AND NOT EMPTY(.w_ELPERATT),NEXTTIME( .w_ELPERATT, .w_ELDATINI-1,.F.,.T. ),IIF(.w_PRIMADATA='F',.w_ELDATFIN,.w_ELDATATT))))
        endif
        .DoRTCalc(35,35,.t.)
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_TIPDOC<>.w_TIPDOC
            .w_ELCAUDOC = IIF(.w_FLATT<>'S',SPACE(5), IIF(NOT EMPTY(.w_TIPDOC), .w_TIPDOC, .w_MCAUDOC))
          .link_1_38('Full')
        endif
        if .o_ELCONTRA<>.w_ELCONTRA.or. .o_ELCODMOD<>.w_ELCODMOD.or. .o_FLATT<>.w_FLATT
            .w_ELCODPAG = iif(.w_FLATT='S',IIF(NOT EMPTY(.w_CODPAG), .w_CODPAG, .w_ELCODPAG),Space(5))
          .link_1_39('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD
            .w_ELPERFAT = IIF(.w_FLATT<>'S' or empty(.w_ELCAUDOC), SPACE(3), .w_PERFAT)
          .link_1_40('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELDATFIN<>.w_ELDATFIN.or. .o_ELDATINI<>.w_ELDATINI.or. .o_ELPERFAT<>.w_ELPERFAT.or. .o_FLATT<>.w_FLATT.or. .o_PRIMADATADOC<>.w_PRIMADATADOC
            .w_ELDATFAT = IIF(EMPTY(.w_ELCAUDOC),CP_CHARTODATE('  -  -  '),IIF(.w_PRIMADATADOC='I',.w_ELDATINI,IIF(.w_PRIMADATADOC='P' AND NOT EMPTY(.w_ELPERFAT),NEXTTIME( .w_ELPERFAT, .w_ELDATINI-1,.F.,.T. ),IIF(.w_PRIMADATADOC='F',.w_ELDATFIN,.w_ELDATFAT))))
        endif
        .DoRTCalc(40,43,.t.)
        if .o_ELESCRIN<>.w_ELESCRIN
            .w_ELPERCON = IIF( .w_ELESCRIN <>"N", "   ", .w_ELPERCON )
          .link_1_46('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCONTRA<>.w_ELCONTRA
            .w_FLDATFIS = IIF(.w_DATACONT='M',.w_DATAFISSA,.w_DATACONT)
        endif
        if .o_ELESCRIN<>.w_ELESCRIN
            .w_ELRINCON = iif(.w_ELESCRIN='S',"N",.w_ELRINCON)
        endif
        if .o_ELRINCON<>.w_ELRINCON
            .w_ELNUMGIO = IIF(.w_ELRINCON = "S" ,.w_ELNUMGIO,0)
        endif
        if .o_ELNUMGIO<>.w_ELNUMGIO.or. .o_ELDATFIN<>.w_ELDATFIN.or. .o_ELRINCON<>.w_ELRINCON
            .w_ELDATDIS = IIF(.w_ELRINCON = "S" ,.w_ELDATFIN - .w_ELNUMGIO,CP_Chartodate('  -  -    '))
        endif
        .DoRTCalc(49,53,.t.)
            .w_CACODART = .w_ELCODART
          .link_1_62('Full')
        .DoRTCalc(55,58,.t.)
        if .o_ELGIODOC<>.w_ELGIODOC.or. .o_ELDATFIN<>.w_ELDATFIN.or. .o_ELCODMOD<>.w_ELCODMOD.or. .o_FLATT<>.w_FLATT
            .w_ELDATDOC = IIF(.w_FLATT='S',.w_ELDATFIN+.w_ELGIODOC, CP_CHARTODATE('  -  -  '))
        endif
        if .o_ELGIOATT<>.w_ELGIOATT.or. .o_ELDATFIN<>.w_ELDATFIN.or. .o_ELCODMOD<>.w_ELCODMOD.or. .o_FLATTI<>.w_FLATTI
            .w_ELDATGAT = IIF(.w_FLATTI='S',.w_ELDATFIN+.w_ELGIOATT, CP_CHARTODATE('  -  -  '))
        endif
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .DoRTCalc(61,95,.t.)
          .link_1_118('Full')
        .DoRTCalc(97,97,.t.)
        if .o_ELDATATT<>.w_ELDATATT
            .w_OB_TEST = IIF(EMPTY(.w_ELDATATT), i_INIDAT, .w_ELDATATT)
        endif
        .DoRTCalc(99,102,.t.)
            .w_FLGLIS = 'N'
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate()
        .DoRTCalc(104,104,.t.)
        if .o_ELCAUDOC<>.w_ELCAUDOC.or. .o_ELCODVAL<>.w_ELCODVAL.or. .o_ELCODCLI<>.w_ELCODCLI.or. .o_ELDATINI<>.w_ELDATINI.or. .o_ELDATFAT<>.w_ELDATFAT
            .w_NULLA = 'X'
        endif
        if .o_ELCODVAL<>.w_ELCODVAL
            .w_MVCODVAL = .w_ELCODVAL
        endif
        if .o_ELDATFAT<>.w_ELDATFAT.or. .o_ELDATATT<>.w_ELDATATT.or. .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCODCLI<>.w_ELCODCLI.or. .o_ELCONTRA<>.w_ELCONTRA
            .w_DATALIST = IIF(Not empty(.w_ELDATFAT),.w_ELDATFAT,IIF(Empty(.w_ELDATATT),.w_ELDATINI,.w_ELDATATT))
        endif
        .DoRTCalc(108,111,.t.)
        if .o_ELCODART<>.w_ELCODART
            .w_ARUNMIS1 = .w_UNMIS1
        endif
        if .o_ELCODART<>.w_ELCODART
            .w_ARUNMIS2 = .w_UNMIS2
        endif
            .w_MVFLVEAC = 'V'
        .oPgFrm.Page1.oPag.oObj_1_146.Calculate()
        .DoRTCalc(115,120,.t.)
            .w_CODIMP = .w_ELCODIMP
          .link_1_147('Full')
            .w_CODCOM = .w_ELCODCOM
          .link_1_148('Full')
        .DoRTCalc(123,124,.t.)
        if .o_ELCODMOD<>.w_ELCODMOD
            .w_ELCODART = .w_CODART
          .link_2_4('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD
            .w_ELCODART = .w_CODART
          .link_2_5('Full')
        endif
            .w_ARCODART = .w_ELCODART
        if .o_ELCODART<>.w_ELCODART.or. .o_ELCODMOD<>.w_ELCODMOD
            .w_ELUNIMIS = .w_UNMIS1
          .link_2_11('Full')
        endif
        if .o_ELCODART<>.w_ELCODART.or. .o_TIPART<>.w_TIPART.or. .o_ELCODMOD<>.w_ELCODMOD
            .w_ELQTAMOV = IIF( .w_MOTIPCON<>'P', IIF(EMPTY(NVL(.w_ELCODART,'')) OR .w_TIPART='FO',1, .w_ELQTAMOV), .w_MOQTAMOV )
        endif
        Local l_Dep1,l_Dep2
        l_Dep1= .o_ELCODART<>.w_ELCODART .or. .o_ELQTAMOV<>.w_ELQTAMOV .or. .o_ELUNIMIS<>.w_ELUNIMIS .or. .o_ELCODLIS<>.w_ELCODLIS .or. .o_ELCONTRA<>.w_ELCONTRA        l_Dep2= .o_ELCODMOD<>.w_ELCODMOD .or. .o_ELCODCLI<>.w_ELCODCLI .or. .o_ELDATATT<>.w_ELDATATT .or. .o_ELDATFAT<>.w_ELDATFAT
        if m.l_Dep1 .or. m.l_Dep2
          .link_2_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_150.Calculate()
        .DoRTCalc(131,143,.t.)
        if .o_ELCODMOD<>.w_ELCODMOD
          .link_1_153('Full')
        endif
        .DoRTCalc(145,145,.t.)
            .w_VOCRIC = cavocori( .w_elcodart, "R" )
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCODART<>.w_ELCODART
            .w_ELVOCCEN = IIF(NOT EMPTY(.w_VOCRIC), .w_VOCRIC, .w_ELVOCCEN)
          .link_2_36('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCONTRA<>.w_ELCONTRA
            .w_ELCODCEN = iif(NOT EMPTY(.w_CODCEN), .w_CODCEN, .w_ELCODCEN)
          .link_2_37('Full')
        endif
        if .o_ELCODMOD<>.w_ELCODMOD.or. .o_ELCONTRA<>.w_ELCONTRA
            .w_ELCOCOMM = iif(NOT EMPTY(.w_COCOMM), .w_COCOMM, .w_MCOCOMM)
          .link_2_38('Full')
        endif
        if .o_ELCOCOMM<>.w_ELCOCOMM.or. .o_ELCONTRA<>.w_ELCONTRA.or. .o_ELCODMOD<>.w_ELCODMOD
            .w_ELCODATT = IIF(.w_COCOMM<>.w_MCOCOMM and not empty(.w_COCOMM) and .w_ELCOCOMM=.w_COCOMM, SPACE(15), iif(not empty(.w_MCOCOMM) and .w_ELCOCOMM= .w_MCOCOMM and not empty(.w_MCODATT), .w_MCODATT, .w_ELCODATT))
          .link_2_39('Full')
        endif
        .DoRTCalc(151,162,.t.)
            .w_CLI = .w_ELCODCLI
        .oPgFrm.Page1.oPag.oObj_1_170.Calculate()
        .DoRTCalc(164,168,.t.)
          .link_1_173('Full')
        .DoRTCalc(170,170,.t.)
            .w_QTA__RES = .w_ELQTAMOV - .w_ELQTACON
        if .o_ELQTAMOV<>.w_ELQTAMOV.or. .o_ELPREZZO<>.w_ELPREZZO.or. .o_ELSCONT1<>.w_ELSCONT1.or. .o_ELSCONT2<>.w_ELSCONT2.or. .o_ELSCONT3<>.w_ELSCONT3.or. .o_ELSCONT4<>.w_ELSCONT4
            .w_PREZZTOT = cp_Round(.w_ELQTAMOV * (cp_Round(.w_ELPREZZO *(100+.w_ELSCONT1)*(100+.w_ELSCONT2)*(100+.w_ELSCONT3)*(100+.w_ELSCONT4)/100000000  ,5) ),g_PERPVL)
        endif
        .oPgFrm.Page1.oPag.oObj_1_179.Calculate()
        .DoRTCalc(173,188,.t.)
        if .o_ELPERATT<>.w_ELPERATT.or. .o_ELINICOA<>.w_ELINICOA
            .w_ELFINCOA = IIF(NOT EMPTY(.w_ELPERATT), NEXTTIME( .w_ELPERATT, .w_ELINICOA-1,.F.,.T. ),.w_ELINICOA)
        endif
        .DoRTCalc(190,190,.t.)
        if .o_ELINICOD<>.w_ELINICOD.or. .o_ELPERFAT<>.w_ELPERFAT
            .w_ELFINCOD = IIF(NOT EMPTY(.w_ELPERFAT), NEXTTIME( .w_ELPERFAT, .w_ELINICOD-1,.F.,.T. ),.w_ELFINCOD)
        endif
        .oPgFrm.Page1.oPag.oObj_1_183.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(192,195,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_146.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_150.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_170.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_179.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_183.Calculate()
    endwith
  return

  proc Calculate_OEWAEUUDHT()
    with this
          * --- Resetto il contratto
          .w_ELCONTRA = ''
          .w_ELDATFAT = cp_chartodate('  -  -  ')
          .w_ELDATATT = cp_chartodate('  -  -  ')
          .w_DATINI = cp_chartodate('  -  -  ')
          .w_DATFIN = cp_chartodate('  -  -  ')
          .w_DESCON = ' '
    endwith
  endproc
  proc Calculate_MOBFKDPKZG()
    with this
          * --- Assegno cliente
          .w_ELCODCLI = .w_CODCLI
          .w_DESCLI = .w_DESCLI1
    endwith
  endproc
  proc Calculate_KHHVUGHDUO()
    with this
          * --- Abilita bottone di rinnovo
          AbilitaBtnRinnovo(this;
             )
    endwith
  endproc
  proc Calculate_HBWXDEHEWF()
    with this
          * --- Sbianca periodicit� - rinnovo tacito - giorni di preavviso
          .w_ELPERCON = IIF( .w_ELESCRIN ="S", "   ", .w_ELPERCON )
          .link_1_46('Full')
          .w_ELRINCON = IIF( .w_ELESCRIN = "S", "N", .w_ELRINCON )
          .w_ELNUMGIO = IIF( .w_ELESCRIN = "S", 0, .w_ELNUMGIO )
          .w_ELDATDIS = IIF( .w_ELESCRIN = "S", CTOD( "  -  -  " ) , .w_ELDATDIS )
    endwith
  endproc
  proc Calculate_DXVHMFFXNO()
    with this
          * --- Azzeramento sconti maggiorazioni al cambio prezzo totale
          .w_ELSCONT1 = 0
          .w_ELSCONT2 = 0
          .w_ELSCONT3 = 0
          .w_ELSCONT4 = 0
          .w_ELPREZZO = IIF(.w_ELQTAMOV<>0, .w_PREZZTOT / .w_ELQTAMOV, 0 )
    endwith
  endproc
  proc Calculate_SKICZIXJXT()
    with this
          * --- Ricalcolo II sconto
          .w_ELSCONT2 = 0
          .w_ELSCONT3 = 0
          .w_ELSCONT4 = 0
    endwith
  endproc
  proc Calculate_ULAREFMTKG()
    with this
          * --- Ricalcolo III sconto
          .w_ELSCONT3 = 0
          .w_ELSCONT4 = 0
    endwith
  endproc
  proc Calculate_ZRHIZLLPMJ()
    with this
          * --- Ricalcolo iv sconto
          .w_ELSCONT4 = 0
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oELCODCLI_1_5.enabled = this.oPgFrm.Page1.oPag.oELCODCLI_1_5.mCond()
    this.oPgFrm.Page1.oPag.oELTIPATT_1_32.enabled = this.oPgFrm.Page1.oPag.oELTIPATT_1_32.mCond()
    this.oPgFrm.Page1.oPag.oELGRUPAR_1_34.enabled = this.oPgFrm.Page1.oPag.oELGRUPAR_1_34.mCond()
    this.oPgFrm.Page1.oPag.oELPERATT_1_35.enabled = this.oPgFrm.Page1.oPag.oELPERATT_1_35.mCond()
    this.oPgFrm.Page1.oPag.oELDATATT_1_36.enabled = this.oPgFrm.Page1.oPag.oELDATATT_1_36.mCond()
    this.oPgFrm.Page1.oPag.oELGIOATT_1_37.enabled = this.oPgFrm.Page1.oPag.oELGIOATT_1_37.mCond()
    this.oPgFrm.Page1.oPag.oELCAUDOC_1_38.enabled = this.oPgFrm.Page1.oPag.oELCAUDOC_1_38.mCond()
    this.oPgFrm.Page1.oPag.oELCODPAG_1_39.enabled = this.oPgFrm.Page1.oPag.oELCODPAG_1_39.mCond()
    this.oPgFrm.Page1.oPag.oELPERFAT_1_40.enabled = this.oPgFrm.Page1.oPag.oELPERFAT_1_40.mCond()
    this.oPgFrm.Page1.oPag.oELDATFAT_1_41.enabled = this.oPgFrm.Page1.oPag.oELDATFAT_1_41.mCond()
    this.oPgFrm.Page1.oPag.oELGIODOC_1_42.enabled = this.oPgFrm.Page1.oPag.oELGIODOC_1_42.mCond()
    this.oPgFrm.Page1.oPag.oELPERCON_1_46.enabled = this.oPgFrm.Page1.oPag.oELPERCON_1_46.mCond()
    this.oPgFrm.Page1.oPag.oELRINCON_1_48.enabled = this.oPgFrm.Page1.oPag.oELRINCON_1_48.mCond()
    this.oPgFrm.Page1.oPag.oELNUMGIO_1_49.enabled = this.oPgFrm.Page1.oPag.oELNUMGIO_1_49.mCond()
    this.oPgFrm.Page1.oPag.oELDATDIS_1_50.enabled = this.oPgFrm.Page1.oPag.oELDATDIS_1_50.mCond()
    this.oPgFrm.Page2.oPag.oELCODART_2_4.enabled = this.oPgFrm.Page2.oPag.oELCODART_2_4.mCond()
    this.oPgFrm.Page2.oPag.oELCODART_2_5.enabled = this.oPgFrm.Page2.oPag.oELCODART_2_5.mCond()
    this.oPgFrm.Page2.oPag.oELUNIMIS_2_11.enabled = this.oPgFrm.Page2.oPag.oELUNIMIS_2_11.mCond()
    this.oPgFrm.Page2.oPag.oELQTAMOV_2_12.enabled = this.oPgFrm.Page2.oPag.oELQTAMOV_2_12.mCond()
    this.oPgFrm.Page2.oPag.oELCONCOD_2_13.enabled = this.oPgFrm.Page2.oPag.oELCONCOD_2_13.mCond()
    this.oPgFrm.Page2.oPag.oELPREZZO_2_14.enabled = this.oPgFrm.Page2.oPag.oELPREZZO_2_14.mCond()
    this.oPgFrm.Page2.oPag.oELSCONT1_2_15.enabled = this.oPgFrm.Page2.oPag.oELSCONT1_2_15.mCond()
    this.oPgFrm.Page2.oPag.oELSCONT2_2_16.enabled = this.oPgFrm.Page2.oPag.oELSCONT2_2_16.mCond()
    this.oPgFrm.Page2.oPag.oELSCONT3_2_17.enabled = this.oPgFrm.Page2.oPag.oELSCONT3_2_17.mCond()
    this.oPgFrm.Page2.oPag.oELSCONT4_2_18.enabled = this.oPgFrm.Page2.oPag.oELSCONT4_2_18.mCond()
    this.oPgFrm.Page2.oPag.oPREZZTOT_2_64.enabled = this.oPgFrm.Page2.oPag.oPREZZTOT_2_64.mCond()
    this.oPgFrm.Page2.oPag.oELINICOA_2_81.enabled = this.oPgFrm.Page2.oPag.oELINICOA_2_81.mCond()
    this.oPgFrm.Page2.oPag.oELFINCOA_2_82.enabled = this.oPgFrm.Page2.oPag.oELFINCOA_2_82.mCond()
    this.oPgFrm.Page2.oPag.oELINICOD_2_83.enabled = this.oPgFrm.Page2.oPag.oELINICOD_2_83.mCond()
    this.oPgFrm.Page2.oPag.oELFINCOD_2_84.enabled = this.oPgFrm.Page2.oPag.oELFINCOD_2_84.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_9.enabled = this.oPgFrm.Page2.oPag.oBtn_2_9.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_177.enabled = this.oPgFrm.Page1.oPag.oBtn_1_177.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_178.enabled = this.oPgFrm.Page1.oPag.oBtn_1_178.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oELCODLIS_1_31.visible=!this.oPgFrm.Page1.oPag.oELCODLIS_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_89.visible=!this.oPgFrm.Page1.oPag.oStr_1_89.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS_1_90.visible=!this.oPgFrm.Page1.oPag.oDESLIS_1_90.mHide()
    this.oPgFrm.Page2.oPag.oELCODART_2_4.visible=!this.oPgFrm.Page2.oPag.oELCODART_2_4.mHide()
    this.oPgFrm.Page2.oPag.oELCODART_2_5.visible=!this.oPgFrm.Page2.oPag.oELCODART_2_5.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_7.visible=!this.oPgFrm.Page2.oPag.oBtn_2_7.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_8.visible=!this.oPgFrm.Page2.oPag.oBtn_2_8.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_9.visible=!this.oPgFrm.Page2.oPag.oBtn_2_9.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_10.visible=!this.oPgFrm.Page2.oPag.oBtn_2_10.mHide()
    this.oPgFrm.Page2.oPag.oELCONCOD_2_13.visible=!this.oPgFrm.Page2.oPag.oELCONCOD_2_13.mHide()
    this.oPgFrm.Page2.oPag.oELSCONT1_2_15.visible=!this.oPgFrm.Page2.oPag.oELSCONT1_2_15.mHide()
    this.oPgFrm.Page2.oPag.oELSCONT2_2_16.visible=!this.oPgFrm.Page2.oPag.oELSCONT2_2_16.mHide()
    this.oPgFrm.Page2.oPag.oELSCONT3_2_17.visible=!this.oPgFrm.Page2.oPag.oELSCONT3_2_17.mHide()
    this.oPgFrm.Page2.oPag.oELSCONT4_2_18.visible=!this.oPgFrm.Page2.oPag.oELSCONT4_2_18.mHide()
    this.oPgFrm.Page2.oPag.oELSCONT1_2_19.visible=!this.oPgFrm.Page2.oPag.oELSCONT1_2_19.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_21.visible=!this.oPgFrm.Page2.oPag.oStr_2_21.mHide()
    this.oPgFrm.Page2.oPag.oELSCONT2_2_23.visible=!this.oPgFrm.Page2.oPag.oELSCONT2_2_23.mHide()
    this.oPgFrm.Page2.oPag.oELSCONT3_2_24.visible=!this.oPgFrm.Page2.oPag.oELSCONT3_2_24.mHide()
    this.oPgFrm.Page2.oPag.oELSCONT4_2_25.visible=!this.oPgFrm.Page2.oPag.oELSCONT4_2_25.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_27.visible=!this.oPgFrm.Page2.oPag.oStr_2_27.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_28.visible=!this.oPgFrm.Page2.oPag.oStr_2_28.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_29.visible=!this.oPgFrm.Page2.oPag.oStr_2_29.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_30.visible=!this.oPgFrm.Page2.oPag.oStr_2_30.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_31.visible=!this.oPgFrm.Page2.oPag.oStr_2_31.mHide()
    this.oPgFrm.Page2.oPag.oDESCTR_2_32.visible=!this.oPgFrm.Page2.oPag.oDESCTR_2_32.mHide()
    this.oPgFrm.Page2.oPag.oELVOCCEN_2_36.visible=!this.oPgFrm.Page2.oPag.oELVOCCEN_2_36.mHide()
    this.oPgFrm.Page2.oPag.oELCODCEN_2_37.visible=!this.oPgFrm.Page2.oPag.oELCODCEN_2_37.mHide()
    this.oPgFrm.Page2.oPag.oELCOCOMM_2_38.visible=!this.oPgFrm.Page2.oPag.oELCOCOMM_2_38.mHide()
    this.oPgFrm.Page2.oPag.oELCODATT_2_39.visible=!this.oPgFrm.Page2.oPag.oELCODATT_2_39.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_42.visible=!this.oPgFrm.Page2.oPag.oStr_2_42.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_43.visible=!this.oPgFrm.Page2.oPag.oStr_2_43.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_44.visible=!this.oPgFrm.Page2.oPag.oStr_2_44.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_45.visible=!this.oPgFrm.Page2.oPag.oStr_2_45.mHide()
    this.oPgFrm.Page2.oPag.oDESVOC_2_46.visible=!this.oPgFrm.Page2.oPag.oDESVOC_2_46.mHide()
    this.oPgFrm.Page2.oPag.oDESCEN_2_47.visible=!this.oPgFrm.Page2.oPag.oDESCEN_2_47.mHide()
    this.oPgFrm.Page2.oPag.oDESCOMM_2_48.visible=!this.oPgFrm.Page2.oPag.oDESCOMM_2_48.mHide()
    this.oPgFrm.Page2.oPag.oDESATTI_2_49.visible=!this.oPgFrm.Page2.oPag.oDESATTI_2_49.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_59.visible=!this.oPgFrm.Page2.oPag.oStr_2_59.mHide()
    this.oPgFrm.Page2.oPag.oELQTACON_2_60.visible=!this.oPgFrm.Page2.oPag.oELQTACON_2_60.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_61.visible=!this.oPgFrm.Page2.oPag.oStr_2_61.mHide()
    this.oPgFrm.Page2.oPag.oQTA__RES_2_62.visible=!this.oPgFrm.Page2.oPag.oQTA__RES_2_62.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_63.visible=!this.oPgFrm.Page2.oPag.oStr_2_63.mHide()
    this.oPgFrm.Page2.oPag.oPREZZTOT_2_64.visible=!this.oPgFrm.Page2.oPag.oPREZZTOT_2_64.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_65.visible=!this.oPgFrm.Page2.oPag.oStr_2_65.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_177.visible=!this.oPgFrm.Page1.oPag.oBtn_1_177.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_178.visible=!this.oPgFrm.Page1.oPag.oBtn_1_178.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_ael
    If CEVENT='Aggio'
       This.w_ELTCOLIS=Space(5)
       This.w_ELSCOLIS=Space(5)
       This.w_ELPROLIS=Space(5)
       This.w_ELPROSCO=Space(5)
    Endif
    iF (CEVENT='Record Inserted' or CEVENT='Record Updated' or CEVENT='Record Deleted') and Type('this.w_OGGETTO')='O'
       This.w_OGGETTO.Notifyevent('SearchT')
       This.w_OGGETTO.Notifyevent('SearchR')
    Endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_ELCODCLI Changed")
          .Calculate_OEWAEUUDHT()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_1.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_2.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_MOBFKDPKZG()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_127.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_146.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_150.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_KHHVUGHDUO()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_170.Event(cEvent)
        if lower(cEvent)==lower("w_ELESCRIN Changed")
          .Calculate_HBWXDEHEWF()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_179.Event(cEvent)
        if lower(cEvent)==lower("Azzera")
          .Calculate_DXVHMFFXNO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ELSCONT1 Changed")
          .Calculate_SKICZIXJXT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ELSCONT2 Changed")
          .Calculate_ULAREFMTKG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ELSCONT3 Changed")
          .Calculate_ZRHIZLLPMJ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_183.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ELCODMOD
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_AME',True,'MOD_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_ELCODMOD)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOCODVAL,MOPERATT,MOTIPATT,MOGRUPAR,MOCAUDOC,MOFLFATT,MOFLATTI,MOCODLIS,MOGIODOC,MOGIOATT,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOQTAMOV,MOPRIDAT,MOPRIDOC,MODATFIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_ELCODMOD))
          select MOCODICE,MODESCRI,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOCODVAL,MOPERATT,MOTIPATT,MOGRUPAR,MOCAUDOC,MOFLFATT,MOFLATTI,MOCODLIS,MOGIODOC,MOGIOATT,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOQTAMOV,MOPRIDAT,MOPRIDOC,MODATFIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODMOD)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MODESCRI like "+cp_ToStrODBC(trim(this.w_ELCODMOD)+"%");

            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOCODVAL,MOPERATT,MOTIPATT,MOGRUPAR,MOCAUDOC,MOFLFATT,MOFLATTI,MOCODLIS,MOGIODOC,MOGIOATT,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOQTAMOV,MOPRIDAT,MOPRIDOC,MODATFIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MODESCRI like "+cp_ToStr(trim(this.w_ELCODMOD)+"%");

            select MOCODICE,MODESCRI,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOCODVAL,MOPERATT,MOTIPATT,MOGRUPAR,MOCAUDOC,MOFLFATT,MOFLATTI,MOCODLIS,MOGIODOC,MOGIOATT,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOQTAMOV,MOPRIDAT,MOPRIDOC,MODATFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODMOD) and !this.bDontReportError
            deferred_cp_zoom('MOD_ELEM','*','MOCODICE',cp_AbsName(oSource.parent,'oELCODMOD_1_2'),i_cWhere,'GSAG_AME',"Modelli",'GSAG_KV3.MOD_ELEM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOCODVAL,MOPERATT,MOTIPATT,MOGRUPAR,MOCAUDOC,MOFLFATT,MOFLATTI,MOCODLIS,MOGIODOC,MOGIOATT,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOQTAMOV,MOPRIDAT,MOPRIDOC,MODATFIS";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOCODVAL,MOPERATT,MOTIPATT,MOGRUPAR,MOCAUDOC,MOFLFATT,MOFLATTI,MOCODLIS,MOGIODOC,MOGIOATT,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOQTAMOV,MOPRIDAT,MOPRIDOC,MODATFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOCODVAL,MOPERATT,MOTIPATT,MOGRUPAR,MOCAUDOC,MOFLFATT,MOFLATTI,MOCODLIS,MOGIODOC,MOGIOATT,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOQTAMOV,MOPRIDAT,MOPRIDOC,MODATFIS";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_ELCODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_ELCODMOD)
            select MOCODICE,MODESCRI,MOCATELE,MOPERFAT,MOGESRAT,MOTIPAPL,MOCODSER,MOCODVAL,MOPERATT,MOTIPATT,MOGRUPAR,MOCAUDOC,MOFLFATT,MOFLATTI,MOCODLIS,MOGIODOC,MOGIOATT,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MOTIPCON,MOQTAMOV,MOPRIDAT,MOPRIDOC,MODATFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODMOD = NVL(_Link_.MOCODICE,space(10))
      this.w_DESELE = NVL(_Link_.MODESCRI,space(60))
      this.w_ELCATCON = NVL(_Link_.MOCATELE,space(5))
      this.w_PERFAT = NVL(_Link_.MOPERFAT,space(3))
      this.w_ELGESRAT = NVL(_Link_.MOGESRAT,space(1))
      this.w_TIPMOD = NVL(_Link_.MOTIPAPL,space(1))
      this.w_CODART = NVL(_Link_.MOCODSER,space(20))
      this.w_ELCODVAL = NVL(_Link_.MOCODVAL,space(3))
      this.w_PERATT = NVL(_Link_.MOPERATT,space(3))
      this.w_ELTIPATT = NVL(_Link_.MOTIPATT,space(20))
      this.w_ELGRUPAR = NVL(_Link_.MOGRUPAR,space(5))
      this.w_MCAUDOC = NVL(_Link_.MOCAUDOC,space(5))
      this.w_FLATT = NVL(_Link_.MOFLFATT,space(1))
      this.w_FLATTI = NVL(_Link_.MOFLATTI,space(1))
      this.w_MCODLIS = NVL(_Link_.MOCODLIS,space(5))
      this.w_ELGIODOC = NVL(_Link_.MOGIODOC,0)
      this.w_ELGIOATT = NVL(_Link_.MOGIOATT,0)
      this.w_ELVOCCEN = NVL(_Link_.MOVOCCEN,space(15))
      this.w_ELCODCEN = NVL(_Link_.MOCODCEN,space(15))
      this.w_MCOCOMM = NVL(_Link_.MOCOCOMM,space(15))
      this.w_MCODATT = NVL(_Link_.MOCODATT,space(15))
      this.w_MOTIPCON = NVL(_Link_.MOTIPCON,space(1))
      this.w_MOQTAMOV = NVL(_Link_.MOQTAMOV,0)
      this.w_PRIDAT = NVL(_Link_.MOPRIDAT,space(1))
      this.w_PRIDOC = NVL(_Link_.MOPRIDOC,space(1))
      this.w_DATAFISSA = NVL(_Link_.MODATFIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODMOD = space(10)
      endif
      this.w_DESELE = space(60)
      this.w_ELCATCON = space(5)
      this.w_PERFAT = space(3)
      this.w_ELGESRAT = space(1)
      this.w_TIPMOD = space(1)
      this.w_CODART = space(20)
      this.w_ELCODVAL = space(3)
      this.w_PERATT = space(3)
      this.w_ELTIPATT = space(20)
      this.w_ELGRUPAR = space(5)
      this.w_MCAUDOC = space(5)
      this.w_FLATT = space(1)
      this.w_FLATTI = space(1)
      this.w_MCODLIS = space(5)
      this.w_ELGIODOC = 0
      this.w_ELGIOATT = 0
      this.w_ELVOCCEN = space(15)
      this.w_ELCODCEN = space(15)
      this.w_MCOCOMM = space(15)
      this.w_MCODATT = space(15)
      this.w_MOTIPCON = space(1)
      this.w_MOQTAMOV = 0
      this.w_PRIDAT = space(1)
      this.w_PRIDOC = space(1)
      this.w_DATAFISSA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 26 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MOD_ELEM_IDX,3] and i_nFlds+26<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.MOCODICE as MOCODICE102"+ ",link_1_2.MODESCRI as MODESCRI102"+ ",link_1_2.MOCATELE as MOCATELE102"+ ",link_1_2.MOPERFAT as MOPERFAT102"+ ",link_1_2.MOGESRAT as MOGESRAT102"+ ",link_1_2.MOTIPAPL as MOTIPAPL102"+ ",link_1_2.MOCODSER as MOCODSER102"+ ",link_1_2.MOCODVAL as MOCODVAL102"+ ",link_1_2.MOPERATT as MOPERATT102"+ ",link_1_2.MOTIPATT as MOTIPATT102"+ ",link_1_2.MOGRUPAR as MOGRUPAR102"+ ",link_1_2.MOCAUDOC as MOCAUDOC102"+ ",link_1_2.MOFLFATT as MOFLFATT102"+ ",link_1_2.MOFLATTI as MOFLATTI102"+ ",link_1_2.MOCODLIS as MOCODLIS102"+ ",link_1_2.MOGIODOC as MOGIODOC102"+ ",link_1_2.MOGIOATT as MOGIOATT102"+ ",link_1_2.MOVOCCEN as MOVOCCEN102"+ ",link_1_2.MOCODCEN as MOCODCEN102"+ ",link_1_2.MOCOCOMM as MOCOCOMM102"+ ",link_1_2.MOCODATT as MOCODATT102"+ ",link_1_2.MOTIPCON as MOTIPCON102"+ ",link_1_2.MOQTAMOV as MOQTAMOV102"+ ",link_1_2.MOPRIDAT as MOPRIDAT102"+ ",link_1_2.MOPRIDOC as MOPRIDOC102"+ ",link_1_2.MODATFIS as MODATFIS102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on ELE_CONT.ELCODMOD=link_1_2.MOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+26
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODMOD=link_1_2.MOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+26
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODCLI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ELCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCATSCM,ANSCORPO,ANNUMLIS,ANCATCOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_ELCODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCATSCM,ANSCORPO,ANNUMLIS,ANCATCOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_ELCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCATSCM,ANSCORPO,ANNUMLIS,ANCATCOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_ELCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCATSCM,ANSCORPO,ANNUMLIS,ANCATCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oELCODCLI_1_5'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCATSCM,ANSCORPO,ANNUMLIS,ANCATCOM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCATSCM,ANSCORPO,ANNUMLIS,ANCATCOM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCATSCM,ANSCORPO,ANNUMLIS,ANCATCOM";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCATSCM,ANSCORPO,ANNUMLIS,ANCATCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCATSCM,ANSCORPO,ANNUMLIS,ANCATCOM";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ELCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_ELCODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCATSCM,ANSCORPO,ANNUMLIS,ANCATCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(50))
      this.w_CATCLI = NVL(_Link_.ANCATSCM,space(5))
      this.w_FLSCOR = NVL(_Link_.ANSCORPO,space(1))
      this.w_NUMLIS = NVL(_Link_.ANNUMLIS,space(5))
      this.w_CATCOM = NVL(_Link_.ANCATCOM,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODCLI = space(15)
      endif
      this.w_DESCLI = space(50)
      this.w_CATCLI = space(5)
      this.w_FLSCOR = space(1)
      this.w_NUMLIS = space(5)
      this.w_CATCOM = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCONTRA
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACA',True,'CON_TRAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COSERIAL like "+cp_ToStrODBC(trim(this.w_ELCONTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select COSERIAL,COCODCON,CODESCON,CODATINI,CODATFIN,COTIPDOC,COGRUPAR,COTIPATT,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC,CODATFIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COSERIAL',trim(this.w_ELCONTRA))
          select COSERIAL,COCODCON,CODESCON,CODATINI,CODATFIN,COTIPDOC,COGRUPAR,COTIPATT,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC,CODATFIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCONTRA)==trim(_Link_.COSERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" COCODCON like "+cp_ToStrODBC(trim(this.w_ELCONTRA)+"%");

            i_ret=cp_SQL(i_nConn,"select COSERIAL,COCODCON,CODESCON,CODATINI,CODATFIN,COTIPDOC,COGRUPAR,COTIPATT,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC,CODATFIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" COCODCON like "+cp_ToStr(trim(this.w_ELCONTRA)+"%");

            select COSERIAL,COCODCON,CODESCON,CODATINI,CODATFIN,COTIPDOC,COGRUPAR,COTIPATT,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC,CODATFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCONTRA) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAS','*','COSERIAL',cp_AbsName(oSource.parent,'oELCONTRA_1_9'),i_cWhere,'GSAG_ACA',"Contratti",'GSAG_AEL.CON_TRAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,COCODCON,CODESCON,CODATINI,CODATFIN,COTIPDOC,COGRUPAR,COTIPATT,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC,CODATFIS";
                     +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',oSource.xKey(1))
            select COSERIAL,COCODCON,CODESCON,CODATINI,CODATFIN,COTIPDOC,COGRUPAR,COTIPATT,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC,CODATFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,COCODCON,CODESCON,CODATINI,CODATFIN,COTIPDOC,COGRUPAR,COTIPATT,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC,CODATFIS";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_ELCONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_ELCONTRA)
            select COSERIAL,COCODCON,CODESCON,CODATINI,CODATFIN,COTIPDOC,COGRUPAR,COTIPATT,COCODPAG,COCODLIS,COCODCEN,COCOCOMM,COPRIDAT,COPRIDOC,CODATFIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCONTRA = NVL(_Link_.COSERIAL,space(10))
      this.w_CODCLI = NVL(_Link_.COCODCON,space(15))
      this.w_DESCON = NVL(_Link_.CODESCON,space(50))
      this.w_ELDATINI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_ELDATFIN = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_DATINI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_TIPDOC = NVL(_Link_.COTIPDOC,space(3))
      this.w_GRUPAR = NVL(_Link_.COGRUPAR,space(5))
      this.w_TIPATT = NVL(_Link_.COTIPATT,space(20))
      this.w_CODPAG = NVL(_Link_.COCODPAG,space(5))
      this.w_CCODLIS = NVL(_Link_.COCODLIS,space(5))
      this.w_CODCEN = NVL(_Link_.COCODCEN,space(15))
      this.w_COCOMM = NVL(_Link_.COCOCOMM,space(15))
      this.w_COPRIDAT = NVL(_Link_.COPRIDAT,space(1))
      this.w_COPRIDOC = NVL(_Link_.COPRIDOC,space(1))
      this.w_DATACONT = NVL(_Link_.CODATFIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCONTRA = space(10)
      endif
      this.w_CODCLI = space(15)
      this.w_DESCON = space(50)
      this.w_ELDATINI = ctod("  /  /  ")
      this.w_ELDATFIN = ctod("  /  /  ")
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
      this.w_TIPDOC = space(3)
      this.w_GRUPAR = space(5)
      this.w_TIPATT = space(20)
      this.w_CODPAG = space(5)
      this.w_CCODLIS = space(5)
      this.w_CODCEN = space(15)
      this.w_COCOMM = space(15)
      this.w_COPRIDAT = space(1)
      this.w_COPRIDOC = space(1)
      this.w_DATACONT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 17 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CON_TRAS_IDX,3] and i_nFlds+17<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.COSERIAL as COSERIAL109"+ ",link_1_9.COCODCON as COCODCON109"+ ",link_1_9.CODESCON as CODESCON109"+ ",link_1_9.CODATINI as CODATINI109"+ ",link_1_9.CODATFIN as CODATFIN109"+ ",link_1_9.CODATINI as CODATINI109"+ ",link_1_9.CODATFIN as CODATFIN109"+ ",link_1_9.COTIPDOC as COTIPDOC109"+ ",link_1_9.COGRUPAR as COGRUPAR109"+ ",link_1_9.COTIPATT as COTIPATT109"+ ",link_1_9.COCODPAG as COCODPAG109"+ ",link_1_9.COCODLIS as COCODLIS109"+ ",link_1_9.COCODCEN as COCODCEN109"+ ",link_1_9.COCOCOMM as COCOCOMM109"+ ",link_1_9.COPRIDAT as COPRIDAT109"+ ",link_1_9.COPRIDOC as COPRIDOC109"+ ",link_1_9.CODATFIS as CODATFIS109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on ELE_CONT.ELCONTRA=link_1_9.COSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+17
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCONTRA=link_1_9.COSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+17
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODCLI
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI1 = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_DESCLI1 = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCATCON
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ELEM_IDX,3]
    i_lTable = "CAT_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2], .t., this.CAT_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACE',True,'CAT_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODELE like "+cp_ToStrODBC(trim(this.w_ELCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODELE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODELE',trim(this.w_ELCATCON))
          select CECODELE,CEDESELE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODELE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCATCON)==trim(_Link_.CECODELE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CEDESELE like "+cp_ToStrODBC(trim(this.w_ELCATCON)+"%");

            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CEDESELE like "+cp_ToStr(trim(this.w_ELCATCON)+"%");

            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCATCON) and !this.bDontReportError
            deferred_cp_zoom('CAT_ELEM','*','CECODELE',cp_AbsName(oSource.parent,'oELCATCON_1_30'),i_cWhere,'GSAG_ACE',"Categorie contratti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                     +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',oSource.xKey(1))
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                   +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(this.w_ELCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',this.w_ELCATCON)
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCATCON = NVL(_Link_.CECODELE,space(5))
      this.w_DESCAT = NVL(_Link_.CEDESELE,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ELCATCON = space(5)
      endif
      this.w_DESCAT = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.CECODELE,1)
      cp_ShowWarn(i_cKey,this.CAT_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_ELEM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_30.CECODELE as CECODELE130"+ ",link_1_30.CEDESELE as CEDESELE130"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_30 on ELE_CONT.ELCATCON=link_1_30.CECODELE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_30"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCATCON=link_1_30.CECODELE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODLIS
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ELCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ELCODLIS))
          select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oELCODLIS_1_31'),i_cWhere,'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ELCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ELCODLIS)
            select LSCODLIS,LSDESLIS,LSFLSCON,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSQUANTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_SCOLIS = NVL(_Link_.LSFLSCON,space(1))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_LORNET = NVL(_Link_.LSIVALIS,space(1))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_QUALIS = NVL(_Link_.LSQUANTI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_SCOLIS = space(1)
      this.w_VALLIS = space(3)
      this.w_LORNET = space(1)
      this.w_INILIS = ctod("  /  /  ")
      this.w_FINLIS = ctod("  /  /  ")
      this.w_QUALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( EMPTY(.w_ELCODMOD) OR EMPTY(.w_ELCONTRA), .T., CHKLISD(.w_ELCODLIS,.w_LORNET,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_ELCODVAL, CTOD('  -  -  ')) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELCODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_SCOLIS = space(1)
        this.w_VALLIS = space(3)
        this.w_LORNET = space(1)
        this.w_INILIS = ctod("  /  /  ")
        this.w_FINLIS = ctod("  /  /  ")
        this.w_QUALIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_31.LSCODLIS as LSCODLIS131"+ ",link_1_31.LSDESLIS as LSDESLIS131"+ ",link_1_31.LSFLSCON as LSFLSCON131"+ ",link_1_31.LSVALLIS as LSVALLIS131"+ ",link_1_31.LSIVALIS as LSIVALIS131"+ ",link_1_31.LSDTINVA as LSDTINVA131"+ ",link_1_31.LSDTOBSO as LSDTOBSO131"+ ",link_1_31.LSQUANTI as LSQUANTI131"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_31 on ELE_CONT.ELCODLIS=link_1_31.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_31"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODLIS=link_1_31.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELTIPATT
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELTIPATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ELTIPATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CAFLNSAP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ELTIPATT))
          select CACODICE,CADESCRI,CARAGGST,CAFLNSAP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELTIPATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELTIPATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oELTIPATT_1_32'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'GSAG_MCA.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CAFLNSAP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CARAGGST,CAFLNSAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELTIPATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CAFLNSAP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ELTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ELTIPATT)
            select CACODICE,CADESCRI,CARAGGST,CAFLNSAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELTIPATT = NVL(_Link_.CACODICE,space(20))
      this.w_DESATT = NVL(_Link_.CADESCRI,space(254))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(10))
      this.w_FLNSAP = NVL(_Link_.CAFLNSAP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELTIPATT = space(20)
      endif
      this.w_DESATT = space(254)
      this.w_CARAGGST = space(10)
      this.w_FLNSAP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CARAGGST<>'Z'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELTIPATT = space(20)
        this.w_DESATT = space(254)
        this.w_CARAGGST = space(10)
        this.w_FLNSAP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELTIPATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_32.CACODICE as CACODICE132"+ ",link_1_32.CADESCRI as CADESCRI132"+ ",link_1_32.CARAGGST as CARAGGST132"+ ",link_1_32.CAFLNSAP as CAFLNSAP132"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_32 on ELE_CONT.ELTIPATT=link_1_32.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_32"
          i_cKey=i_cKey+'+" and ELE_CONT.ELTIPATT=link_1_32.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELGRUPAR
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELGRUPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_ELGRUPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_ELGRUPAR))
          select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELGRUPAR)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELGRUPAR) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oELGRUPAR_1_34'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELGRUPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_ELGRUPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_ELGRUPAR)
            select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELGRUPAR = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
      this.w_DPDESCRI = NVL(_Link_.DPDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ELGRUPAR = space(5)
      endif
      this.w_TIPRIS = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DPDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIS='G' AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore incongruente o obsoleto")
        endif
        this.w_ELGRUPAR = space(5)
        this.w_TIPRIS = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DPDESCRI = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELGRUPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_34(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_34.DPCODICE as DPCODICE134"+ ",link_1_34.DPTIPRIS as DPTIPRIS134"+ ",link_1_34.DPDTOBSO as DPDTOBSO134"+ ",link_1_34.DPDESCRI as DPDESCRI134"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_34 on ELE_CONT.ELGRUPAR=link_1_34.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_34"
          i_cKey=i_cKey+'+" and ELE_CONT.ELGRUPAR=link_1_34.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELPERATT
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELPERATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_ELPERATT)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_ELPERATT))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELPERATT)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELPERATT) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oELPERATT_1_35'),i_cWhere,'GSAR_AMD',"Metodi di calcolo periodicit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELPERATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_ELPERATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_ELPERATT)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELPERATT = NVL(_Link_.MDCODICE,space(3))
      this.w_DESCRIAT = NVL(_Link_.MDDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ELPERATT = space(3)
      endif
      this.w_DESCRIAT = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELPERATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_35(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODCLDAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_35.MDCODICE as MDCODICE135"+ ",link_1_35.MDDESCRI as MDDESCRI135"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_35 on ELE_CONT.ELPERATT=link_1_35.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_35"
          i_cKey=i_cKey+'+" and ELE_CONT.ELPERATT=link_1_35.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCAUDOC
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_ELCAUDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS,TDFLANAL,TDFLCOMM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_ELCAUDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS,TDFLANAL,TDFLCOMM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oELCAUDOC_1_38'),i_cWhere,'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS,TDFLANAL,TDFLCOMM";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS,TDFLANAL,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS,TDFLANAL,TDFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_ELCAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_ELCAUDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLINTE,TDFLRIPS,TDFLANAL,TDFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLRIPS = NVL(_Link_.TDFLRIPS,space(5))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
      this.w_FLGCOM = NVL(_Link_.TDFLCOMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCAUDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
      this.w_FLINTE = space(1)
      this.w_FLRIPS = space(5)
      this.w_FLANAL = space(1)
      this.w_FLGCOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDOC<>'OR' AND .w_FLVEAC='V' and (! Isahe() or .w_FLRIPS='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo")
        endif
        this.w_ELCAUDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
        this.w_FLINTE = space(1)
        this.w_FLRIPS = space(5)
        this.w_FLANAL = space(1)
        this.w_FLGCOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.TDTIPDOC as TDTIPDOC138"+ ",link_1_38.TDDESDOC as TDDESDOC138"+ ",link_1_38.TDCATDOC as TDCATDOC138"+ ",link_1_38.TDFLVEAC as TDFLVEAC138"+ ",link_1_38.TDFLINTE as TDFLINTE138"+ ",link_1_38.TDFLRIPS as TDFLRIPS138"+ ",link_1_38.TDFLANAL as TDFLANAL138"+ ",link_1_38.TDFLCOMM as TDFLCOMM138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on ELE_CONT.ELCAUDOC=link_1_38.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCAUDOC=link_1_38.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODPAG
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_ELCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_ELCODPAG))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oELCODPAG_1_39'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_ELCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_ELCODPAG)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(_Link_.PADESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_39(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_39.PACODICE as PACODICE139"+ ",link_1_39.PADESCRI as PADESCRI139"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_39 on ELE_CONT.ELCODPAG=link_1_39.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_39"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODPAG=link_1_39.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELPERFAT
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELPERFAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_ELPERFAT)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI,MD__FREQ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_ELPERFAT))
          select MDCODICE,MDDESCRI,MD__FREQ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELPERFAT)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELPERFAT) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oELPERFAT_1_40'),i_cWhere,'GSAR_AMD',"Metodi di calcolo periodicit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI,MD__FREQ";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI,MD__FREQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELPERFAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI,MD__FREQ";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_ELPERFAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_ELPERFAT)
            select MDCODICE,MDDESCRI,MD__FREQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELPERFAT = NVL(_Link_.MDCODICE,space(3))
      this.w_DESPER = NVL(_Link_.MDDESCRI,space(50))
      this.w_MD__FREQ = NVL(_Link_.MD__FREQ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELPERFAT = space(3)
      endif
      this.w_DESPER = space(50)
      this.w_MD__FREQ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELPERFAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODCLDAT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_40.MDCODICE as MDCODICE140"+ ",link_1_40.MDDESCRI as MDDESCRI140"+ ",link_1_40.MD__FREQ as MD__FREQ140"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_40 on ELE_CONT.ELPERFAT=link_1_40.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_40"
          i_cKey=i_cKey+'+" and ELE_CONT.ELPERFAT=link_1_40.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELPERCON
  func Link_1_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELPERCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_ELPERCON)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_ELPERCON))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELPERCON)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStrODBC(trim(this.w_ELPERCON)+"%");

            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStr(trim(this.w_ELPERCON)+"%");

            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELPERCON) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oELPERCON_1_46'),i_cWhere,'GSAR_AMD',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELPERCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_ELPERCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_ELPERCON)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELPERCON = NVL(_Link_.MDCODICE,space(3))
      this.w_MDDESCRI = NVL(_Link_.MDDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ELPERCON = space(3)
      endif
      this.w_MDDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELPERCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_46(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODCLDAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_46.MDCODICE as MDCODICE146"+ ",link_1_46.MDDESCRI as MDDESCRI146"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_46 on ELE_CONT.ELPERCON=link_1_46.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_46"
          i_cKey=i_cKey+'+" and ELE_CONT.ELPERCON=link_1_46.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACODART
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CAOPERAT,CAMOLTIP,CAUNIMIS,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODART)
            select CACODICE,CAOPERAT,CAMOLTIP,CAUNIMIS,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODART = NVL(_Link_.CACODICE,space(41))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_TIPSER = NVL(_Link_.CA__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACODART = space(41)
      endif
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_UNMIS3 = space(3)
      this.w_TIPSER = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMLIS
  func Link_1_118(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_NUMLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_NUMLIS)
            select LSCODLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_IVACLI = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NUMLIS = space(5)
      endif
      this.w_IVACLI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIMP
  func Link_1_147(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_CODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_CODIMP)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIMP = NVL(_Link_.IMCODICE,space(10))
      this.w_IMDESCRI = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODIMP = space(10)
      endif
      this.w_IMDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_148(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON,CPROWORD";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_CODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_CODIMP;
                       ,'CPROWNUM',this.w_CODCOM)
            select IMCODICE,CPROWNUM,IMDESCON,CPROWORD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CPROWNUM,0)
      this.w_IMDESCON = NVL(_Link_.IMDESCON,space(50))
      this.w_CPROWORD_IMP_DETT = NVL(_Link_.CPROWORD,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = 0
      endif
      this.w_IMDESCON = space(50)
      this.w_CPROWORD_IMP_DETT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODART
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ELCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ELCODART))
          select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oELCODART_2_4'),i_cWhere,'GSMA_AAS',"Servizi",'GSMA2AAS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ELCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ELCODART)
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_ARUNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_ARUNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_PREZUM = NVL(_Link_.ARPREZUM,space(1))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_CATART = NVL(_Link_.ARCATSCM,space(5))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_ELUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPART = space(2)
      this.w_ARUNMIS1 = space(3)
      this.w_ARUNMIS2 = space(3)
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_PREZUM = space(1)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_CATART = space(5)
      this.w_FLUSEP = space(1)
      this.w_ELUNIMIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MOTIPCON='C' AND(.w_TIPART='FO' OR .w_TIPART='FM')) OR (.w_MOTIPCON='P' AND .w_TIPART='FM')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Servizio n� a valore n� a quantit� e valore ")
        endif
        this.w_ELCODART = space(20)
        this.w_DESART = space(40)
        this.w_TIPART = space(2)
        this.w_ARUNMIS1 = space(3)
        this.w_ARUNMIS2 = space(3)
        this.w_UNMIS1 = space(3)
        this.w_UNMIS2 = space(3)
        this.w_FLSERG = space(1)
        this.w_PREZUM = space(1)
        this.w_OPERAT = space(1)
        this.w_MOLTIP = 0
        this.w_CATART = space(5)
        this.w_FLUSEP = space(1)
        this.w_ELUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 14 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+14<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.ARCODART as ARCODART204"+ ",link_2_4.ARDESART as ARDESART204"+ ",link_2_4.ARTIPART as ARTIPART204"+ ",link_2_4.ARUNMIS1 as ARUNMIS1204"+ ",link_2_4.ARUNMIS2 as ARUNMIS2204"+ ",link_2_4.ARUNMIS1 as ARUNMIS1204"+ ",link_2_4.ARUNMIS2 as ARUNMIS2204"+ ",link_2_4.ARFLSERG as ARFLSERG204"+ ",link_2_4.ARPREZUM as ARPREZUM204"+ ",link_2_4.AROPERAT as AROPERAT204"+ ",link_2_4.ARMOLTIP as ARMOLTIP204"+ ",link_2_4.ARCATSCM as ARCATSCM204"+ ",link_2_4.ARFLUSEP as ARFLUSEP204"+ ",link_2_4.ARUNMIS1 as ARUNMIS1204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on ELE_CONT.ELCODART=link_2_4.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODART=link_2_4.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODART
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ELCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ELCODART))
          select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oELCODART_2_5'),i_cWhere,'GSMA_AAS',"Servizi",'GSMA3AAS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ELCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ELCODART)
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARPREZUM,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_PREZUM = NVL(_Link_.ARPREZUM,space(1))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_CATART = NVL(_Link_.ARCATSCM,space(5))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_ELUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPART = space(2)
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_PREZUM = space(1)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
      this.w_CATART = space(5)
      this.w_FLUSEP = space(1)
      this.w_ELUNIMIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MOTIPCON='C' AND(.w_TIPART='FO' OR .w_TIPART='FM')) OR (.w_MOTIPCON='P' AND .w_TIPART='FM')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Servizio non a quantit� e valore")
        endif
        this.w_ELCODART = space(20)
        this.w_DESART = space(40)
        this.w_TIPART = space(2)
        this.w_UNMIS1 = space(3)
        this.w_UNMIS2 = space(3)
        this.w_FLSERG = space(1)
        this.w_PREZUM = space(1)
        this.w_OPERAT = space(1)
        this.w_MOLTIP = 0
        this.w_CATART = space(5)
        this.w_FLUSEP = space(1)
        this.w_ELUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 12 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+12<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.ARCODART as ARCODART205"+ ",link_2_5.ARDESART as ARDESART205"+ ",link_2_5.ARTIPART as ARTIPART205"+ ",link_2_5.ARUNMIS1 as ARUNMIS1205"+ ",link_2_5.ARUNMIS2 as ARUNMIS2205"+ ",link_2_5.ARFLSERG as ARFLSERG205"+ ",link_2_5.ARPREZUM as ARPREZUM205"+ ",link_2_5.AROPERAT as AROPERAT205"+ ",link_2_5.ARMOLTIP as ARMOLTIP205"+ ",link_2_5.ARCATSCM as ARCATSCM205"+ ",link_2_5.ARFLUSEP as ARFLUSEP205"+ ",link_2_5.ARUNMIS1 as ARUNMIS1205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on ELE_CONT.ELCODART=link_2_5.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+12
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODART=link_2_5.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+12
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELUNIMIS
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ELUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ELUNIMIS))
          select UMCODICE,UMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oELUNIMIS_2_11'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ELUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ELUNIMIS)
            select UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_DESUNI = NVL(_Link_.UMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ELUNIMIS = space(3)
      endif
      this.w_DESUNI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_ELUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_ELUNIMIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELUNIMIS = space(3)
        this.w_DESUNI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.UMCODICE as UMCODICE211"+ ",link_2_11.UMDESCRI as UMDESCRI211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on ELE_CONT.ELUNIMIS=link_2_11.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and ELE_CONT.ELUNIMIS=link_2_11.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCONCOD
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCONCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BCZ',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_ELCONCOD)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COTIPCLF,CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COTIPCLF',this.w_TIPCON;
                     ,'CONUMERO',trim(this.w_ELCONCOD))
          select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COTIPCLF,CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCONCOD)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStrODBC(trim(this.w_ELCONCOD)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStr(trim(this.w_ELCONCOD)+"%");
                   +" and COTIPCLF="+cp_ToStr(this.w_TIPCON);

            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCONCOD) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(oSource.parent,'oELCONCOD_2_13'),i_cWhere,'GSAR_BCZ',"Elenco contratti",'GSAG_AEL.CON_TRAM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',oSource.xKey(1);
                       ,'CONUMERO',oSource.xKey(2))
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCONCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_ELCONCOD);
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',this.w_TIPCON;
                       ,'CONUMERO',this.w_ELCONCOD)
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCONCOD = NVL(_Link_.CONUMERO,space(15))
      this.w_DESCTR = NVL(_Link_.CODESCON,space(50))
      this.w_CT = NVL(_Link_.COTIPCLF,space(1))
      this.w_CC = NVL(_Link_.COCODCLF,space(15))
      this.w_CM = NVL(_Link_.COCATCOM,space(3))
      this.w_CI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_CF = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_CV = NVL(_Link_.COCODVAL,space(3))
      this.w_QUACON = NVL(_Link_.COQUANTI,space(1))
      this.w_IVACON = NVL(_Link_.COIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCONCOD = space(15)
      endif
      this.w_DESCTR = space(50)
      this.w_CT = space(1)
      this.w_CC = space(15)
      this.w_CM = space(3)
      this.w_CI = ctod("  /  /  ")
      this.w_CF = ctod("  /  /  ")
      this.w_CV = space(3)
      this.w_QUACON = space(1)
      this.w_IVACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKCONTR(.w_ELCONCOD,.w_TIPCON,.w_ELCODCLI,.w_CATCOM,.w_FLSCOR,.w_ELCODVAL, .w_DATALIST,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELCONCOD = space(15)
        this.w_DESCTR = space(50)
        this.w_CT = space(1)
        this.w_CC = space(15)
        this.w_CM = space(3)
        this.w_CI = ctod("  /  /  ")
        this.w_CF = ctod("  /  /  ")
        this.w_CV = space(3)
        this.w_QUACON = space(1)
        this.w_IVACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.COTIPCLF,1)+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCONCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODVAL
  func Link_1_153(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_ELCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_ELCODVAL)
            select VACODVAL,VADESVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_ELCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_153(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_153.VACODVAL as VACODVAL253"+ ",link_1_153.VADESVAL as VADESVAL253"+ ",link_1_153.VADECUNI as VADECUNI253"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_153 on ELE_CONT.ELCODVAL=link_1_153.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_153"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODVAL=link_1_153.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELVOCCEN
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELVOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_ELVOCCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_ELVOCCEN))
          select VCCODICE,VCDESCRI,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELVOCCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStrODBC(trim(this.w_ELVOCCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStr(trim(this.w_ELVOCCEN)+"%");

            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELVOCCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oELVOCCEN_2_36'),i_cWhere,'GSCA_AVC',"Voci di ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELVOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_ELVOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_ELVOCCEN)
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELVOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_VOCOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ELVOCCEN = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_VOCOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.), CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ELVOCCEN = space(15)
        this.w_DESVOC = space(40)
        this.w_VOCOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELVOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_36(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_36.VCCODICE as VCCODICE236"+ ",link_2_36.VCDESCRI as VCDESCRI236"+ ",link_2_36.VCDTOBSO as VCDTOBSO236"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_36 on ELE_CONT.ELVOCCEN=link_2_36.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_36"
          i_cKey=i_cKey+'+" and ELE_CONT.ELVOCCEN=link_2_36.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODCEN
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ELCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ELCODCEN))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_ELCODCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_ELCODCEN)+"%");

            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oELCODCEN_2_37'),i_cWhere,'GSCA_ACC',"Centri di ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ELCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ELCODCEN)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODCEN = space(15)
      endif
      this.w_DESCEN = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.), CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
        endif
        this.w_ELCODCEN = space(15)
        this.w_DESCEN = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_37(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_37.CC_CONTO as CC_CONTO237"+ ",link_2_37.CCDESPIA as CCDESPIA237"+ ",link_2_37.CCDTOBSO as CCDTOBSO237"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_37 on ELE_CONT.ELCODCEN=link_2_37.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_37"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCODCEN=link_2_37.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCOCOMM
  func Link_2_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCOCOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ELCOCOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ELCOCOMM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCOCOMM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_ELCOCOMM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_ELCOCOMM)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCOCOMM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oELCOCOMM_2_38'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCOCOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ELCOCOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ELCOCOMM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCOCOMM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMM = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ELCOCOMM = space(15)
      endif
      this.w_DESCOMM = space(100)
      this.w_COMOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.), CHKDTOBS(.w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_ELCOCOMM = space(15)
        this.w_DESCOMM = space(100)
        this.w_COMOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCOCOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_38.CNCODCAN as CNCODCAN238"+ ",link_2_38.CNDESCAN as CNDESCAN238"+ ",link_2_38.CNDTOBSO as CNDTOBSO238"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_38 on ELE_CONT.ELCOCOMM=link_2_38.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_38"
          i_cKey=i_cKey+'+" and ELE_CONT.ELCOCOMM=link_2_38.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODATT
  func Link_2_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ELCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ELCOCOMM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_ELCOCOMM;
                     ,'ATTIPATT',this.w_ATTIPATT;
                     ,'ATCODATT',trim(this.w_ELCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_ELCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ELCOCOMM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_ELCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_ELCOCOMM);
                   +" and ATTIPATT="+cp_ToStr(this.w_ATTIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oELCODATT_2_39'),i_cWhere,'GSPC_BZZ',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ELCOCOMM<>oSource.xKey(1);
           .or. this.w_ATTIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_ELCOCOMM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ELCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ELCOCOMM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_ELCOCOMM;
                       ,'ATTIPATT',this.w_ATTIPATT;
                       ,'ATCODATT',this.w_ELCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATTI = NVL(_Link_.ATDESCRI,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODATT = space(15)
      endif
      this.w_DESATTI = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_1_173(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ1 = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_FLFRAZ1 = space(1)
      this.w_MODUM2 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oELCODMOD_1_2.value==this.w_ELCODMOD)
      this.oPgFrm.Page1.oPag.oELCODMOD_1_2.value=this.w_ELCODMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODCLI_1_5.value==this.w_ELCODCLI)
      this.oPgFrm.Page1.oPag.oELCODCLI_1_5.value=this.w_ELCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODIMP_1_6.value==this.w_ELCODIMP)
      this.oPgFrm.Page1.oPag.oELCODIMP_1_6.value=this.w_ELCODIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODCOM_1_8.value==this.w_ELCODCOM)
      this.oPgFrm.Page1.oPag.oELCODCOM_1_8.value=this.w_ELCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oELCONTRA_1_9.value==this.w_ELCONTRA)
      this.oPgFrm.Page1.oPag.oELCONTRA_1_9.value=this.w_ELCONTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATINI_1_21.value==this.w_ELDATINI)
      this.oPgFrm.Page1.oPag.oELDATINI_1_21.value=this.w_ELDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATFIN_1_29.value==this.w_ELDATFIN)
      this.oPgFrm.Page1.oPag.oELDATFIN_1_29.value=this.w_ELDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oELCATCON_1_30.value==this.w_ELCATCON)
      this.oPgFrm.Page1.oPag.oELCATCON_1_30.value=this.w_ELCATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODLIS_1_31.value==this.w_ELCODLIS)
      this.oPgFrm.Page1.oPag.oELCODLIS_1_31.value=this.w_ELCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oELTIPATT_1_32.value==this.w_ELTIPATT)
      this.oPgFrm.Page1.oPag.oELTIPATT_1_32.value=this.w_ELTIPATT
    endif
    if not(this.oPgFrm.Page1.oPag.oELGRUPAR_1_34.value==this.w_ELGRUPAR)
      this.oPgFrm.Page1.oPag.oELGRUPAR_1_34.value=this.w_ELGRUPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oELPERATT_1_35.value==this.w_ELPERATT)
      this.oPgFrm.Page1.oPag.oELPERATT_1_35.value=this.w_ELPERATT
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATATT_1_36.value==this.w_ELDATATT)
      this.oPgFrm.Page1.oPag.oELDATATT_1_36.value=this.w_ELDATATT
    endif
    if not(this.oPgFrm.Page1.oPag.oELGIOATT_1_37.value==this.w_ELGIOATT)
      this.oPgFrm.Page1.oPag.oELGIOATT_1_37.value=this.w_ELGIOATT
    endif
    if not(this.oPgFrm.Page1.oPag.oELCAUDOC_1_38.value==this.w_ELCAUDOC)
      this.oPgFrm.Page1.oPag.oELCAUDOC_1_38.value=this.w_ELCAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODPAG_1_39.value==this.w_ELCODPAG)
      this.oPgFrm.Page1.oPag.oELCODPAG_1_39.value=this.w_ELCODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oELPERFAT_1_40.value==this.w_ELPERFAT)
      this.oPgFrm.Page1.oPag.oELPERFAT_1_40.value=this.w_ELPERFAT
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATFAT_1_41.value==this.w_ELDATFAT)
      this.oPgFrm.Page1.oPag.oELDATFAT_1_41.value=this.w_ELDATFAT
    endif
    if not(this.oPgFrm.Page1.oPag.oELGIODOC_1_42.value==this.w_ELGIODOC)
      this.oPgFrm.Page1.oPag.oELGIODOC_1_42.value=this.w_ELGIODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oELESCRIN_1_43.RadioValue()==this.w_ELESCRIN)
      this.oPgFrm.Page1.oPag.oELESCRIN_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELPERCON_1_46.value==this.w_ELPERCON)
      this.oPgFrm.Page1.oPag.oELPERCON_1_46.value=this.w_ELPERCON
    endif
    if not(this.oPgFrm.Page1.oPag.oELRINCON_1_48.RadioValue()==this.w_ELRINCON)
      this.oPgFrm.Page1.oPag.oELRINCON_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELNUMGIO_1_49.value==this.w_ELNUMGIO)
      this.oPgFrm.Page1.oPag.oELNUMGIO_1_49.value=this.w_ELNUMGIO
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATDIS_1_50.value==this.w_ELDATDIS)
      this.oPgFrm.Page1.oPag.oELDATDIS_1_50.value=this.w_ELDATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESELE_1_65.value==this.w_DESELE)
      this.oPgFrm.Page1.oPag.oDESELE_1_65.value=this.w_DESELE
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATDOC_1_67.value==this.w_ELDATDOC)
      this.oPgFrm.Page1.oPag.oELDATDOC_1_67.value=this.w_ELDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATGAT_1_69.value==this.w_ELDATGAT)
      this.oPgFrm.Page1.oPag.oELDATGAT_1_69.value=this.w_ELDATGAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_70.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_70.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPER_1_71.value==this.w_DESPER)
      this.oPgFrm.Page1.oPag.oDESPER_1_71.value=this.w_DESPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_72.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_72.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIAT_1_76.value==this.w_DESCRIAT)
      this.oPgFrm.Page1.oPag.oDESCRIAT_1_76.value=this.w_DESCRIAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_78.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_78.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_79.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_79.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_81.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_81.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_90.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_90.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_111.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_111.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCRI_1_142.value==this.w_IMDESCRI)
      this.oPgFrm.Page1.oPag.oIMDESCRI_1_142.value=this.w_IMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCON_1_145.value==this.w_IMDESCON)
      this.oPgFrm.Page1.oPag.oIMDESCON_1_145.value=this.w_IMDESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCPROWORD_IMP_DETT_1_149.value==this.w_CPROWORD_IMP_DETT)
      this.oPgFrm.Page1.oPag.oCPROWORD_IMP_DETT_1_149.value=this.w_CPROWORD_IMP_DETT
    endif
    if not(this.oPgFrm.Page2.oPag.oELCODART_2_4.value==this.w_ELCODART)
      this.oPgFrm.Page2.oPag.oELCODART_2_4.value=this.w_ELCODART
    endif
    if not(this.oPgFrm.Page2.oPag.oELCODART_2_5.value==this.w_ELCODART)
      this.oPgFrm.Page2.oPag.oELCODART_2_5.value=this.w_ELCODART
    endif
    if not(this.oPgFrm.Page2.oPag.oELUNIMIS_2_11.value==this.w_ELUNIMIS)
      this.oPgFrm.Page2.oPag.oELUNIMIS_2_11.value=this.w_ELUNIMIS
    endif
    if not(this.oPgFrm.Page2.oPag.oELQTAMOV_2_12.value==this.w_ELQTAMOV)
      this.oPgFrm.Page2.oPag.oELQTAMOV_2_12.value=this.w_ELQTAMOV
    endif
    if not(this.oPgFrm.Page2.oPag.oELCONCOD_2_13.value==this.w_ELCONCOD)
      this.oPgFrm.Page2.oPag.oELCONCOD_2_13.value=this.w_ELCONCOD
    endif
    if not(this.oPgFrm.Page2.oPag.oELPREZZO_2_14.value==this.w_ELPREZZO)
      this.oPgFrm.Page2.oPag.oELPREZZO_2_14.value=this.w_ELPREZZO
    endif
    if not(this.oPgFrm.Page2.oPag.oELSCONT1_2_15.value==this.w_ELSCONT1)
      this.oPgFrm.Page2.oPag.oELSCONT1_2_15.value=this.w_ELSCONT1
    endif
    if not(this.oPgFrm.Page2.oPag.oELSCONT2_2_16.value==this.w_ELSCONT2)
      this.oPgFrm.Page2.oPag.oELSCONT2_2_16.value=this.w_ELSCONT2
    endif
    if not(this.oPgFrm.Page2.oPag.oELSCONT3_2_17.value==this.w_ELSCONT3)
      this.oPgFrm.Page2.oPag.oELSCONT3_2_17.value=this.w_ELSCONT3
    endif
    if not(this.oPgFrm.Page2.oPag.oELSCONT4_2_18.value==this.w_ELSCONT4)
      this.oPgFrm.Page2.oPag.oELSCONT4_2_18.value=this.w_ELSCONT4
    endif
    if not(this.oPgFrm.Page2.oPag.oELSCONT1_2_19.value==this.w_ELSCONT1)
      this.oPgFrm.Page2.oPag.oELSCONT1_2_19.value=this.w_ELSCONT1
    endif
    if not(this.oPgFrm.Page2.oPag.oELSCONT2_2_23.value==this.w_ELSCONT2)
      this.oPgFrm.Page2.oPag.oELSCONT2_2_23.value=this.w_ELSCONT2
    endif
    if not(this.oPgFrm.Page2.oPag.oELSCONT3_2_24.value==this.w_ELSCONT3)
      this.oPgFrm.Page2.oPag.oELSCONT3_2_24.value=this.w_ELSCONT3
    endif
    if not(this.oPgFrm.Page2.oPag.oELSCONT4_2_25.value==this.w_ELSCONT4)
      this.oPgFrm.Page2.oPag.oELSCONT4_2_25.value=this.w_ELSCONT4
    endif
    if not(this.oPgFrm.Page2.oPag.oDESUNI_2_26.value==this.w_DESUNI)
      this.oPgFrm.Page2.oPag.oDESUNI_2_26.value=this.w_DESUNI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCTR_2_32.value==this.w_DESCTR)
      this.oPgFrm.Page2.oPag.oDESCTR_2_32.value=this.w_DESCTR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESART_2_34.value==this.w_DESART)
      this.oPgFrm.Page2.oPag.oDESART_2_34.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDESCRI_1_152.value==this.w_DPDESCRI)
      this.oPgFrm.Page1.oPag.oDPDESCRI_1_152.value=this.w_DPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODVAL_1_153.value==this.w_ELCODVAL)
      this.oPgFrm.Page1.oPag.oELCODVAL_1_153.value=this.w_ELCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_155.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_155.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oELVOCCEN_2_36.value==this.w_ELVOCCEN)
      this.oPgFrm.Page2.oPag.oELVOCCEN_2_36.value=this.w_ELVOCCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oELCODCEN_2_37.value==this.w_ELCODCEN)
      this.oPgFrm.Page2.oPag.oELCODCEN_2_37.value=this.w_ELCODCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oELCOCOMM_2_38.value==this.w_ELCOCOMM)
      this.oPgFrm.Page2.oPag.oELCOCOMM_2_38.value=this.w_ELCOCOMM
    endif
    if not(this.oPgFrm.Page2.oPag.oELCODATT_2_39.value==this.w_ELCODATT)
      this.oPgFrm.Page2.oPag.oELCODATT_2_39.value=this.w_ELCODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVOC_2_46.value==this.w_DESVOC)
      this.oPgFrm.Page2.oPag.oDESVOC_2_46.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCEN_2_47.value==this.w_DESCEN)
      this.oPgFrm.Page2.oPag.oDESCEN_2_47.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOMM_2_48.value==this.w_DESCOMM)
      this.oPgFrm.Page2.oPag.oDESCOMM_2_48.value=this.w_DESCOMM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATTI_2_49.value==this.w_DESATTI)
      this.oPgFrm.Page2.oPag.oDESATTI_2_49.value=this.w_DESATTI
    endif
    if not(this.oPgFrm.Page1.oPag.oELRINNOV_1_162.value==this.w_ELRINNOV)
      this.oPgFrm.Page1.oPag.oELRINNOV_1_162.value=this.w_ELRINNOV
    endif
    if not(this.oPgFrm.Page1.oPag.oMDDESCRI_1_164.value==this.w_MDDESCRI)
      this.oPgFrm.Page1.oPag.oMDDESCRI_1_164.value=this.w_MDDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oELQTACON_2_60.value==this.w_ELQTACON)
      this.oPgFrm.Page2.oPag.oELQTACON_2_60.value=this.w_ELQTACON
    endif
    if not(this.oPgFrm.Page2.oPag.oQTA__RES_2_62.value==this.w_QTA__RES)
      this.oPgFrm.Page2.oPag.oQTA__RES_2_62.value=this.w_QTA__RES
    endif
    if not(this.oPgFrm.Page2.oPag.oPREZZTOT_2_64.value==this.w_PREZZTOT)
      this.oPgFrm.Page2.oPag.oPREZZTOT_2_64.value=this.w_PREZZTOT
    endif
    if not(this.oPgFrm.Page2.oPag.oELINICOA_2_81.value==this.w_ELINICOA)
      this.oPgFrm.Page2.oPag.oELINICOA_2_81.value=this.w_ELINICOA
    endif
    if not(this.oPgFrm.Page2.oPag.oELFINCOA_2_82.value==this.w_ELFINCOA)
      this.oPgFrm.Page2.oPag.oELFINCOA_2_82.value=this.w_ELFINCOA
    endif
    if not(this.oPgFrm.Page2.oPag.oELINICOD_2_83.value==this.w_ELINICOD)
      this.oPgFrm.Page2.oPag.oELINICOD_2_83.value=this.w_ELINICOD
    endif
    if not(this.oPgFrm.Page2.oPag.oELFINCOD_2_84.value==this.w_ELFINCOD)
      this.oPgFrm.Page2.oPag.oELFINCOD_2_84.value=this.w_ELFINCOD
    endif
    cp_SetControlsValueExtFlds(this,'ELE_CONT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not((NOT EMPTY(.w_ELCODLIS)) OR (EMPTY(.w_ELCODLIS) AND .w_TIPMOD='C'))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione:tipo di applicazione differita: inserire un listino valido!")
          case   (empty(.w_ELCODMOD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELCODMOD_1_2.SetFocus()
            i_bnoObbl = !empty(.w_ELCODMOD)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ELCONTRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELCONTRA_1_9.SetFocus()
            i_bnoObbl = !empty(.w_ELCONTRA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ELDATINI)) or not(.w_ELDATINI>=.w_DATINI AND .w_ELDATINI<=.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELDATINI_1_21.SetFocus()
            i_bnoObbl = !empty(.w_ELDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Date non comprese nell'intervallo specificato in anagrafica contratti!")
          case   ((empty(.w_ELDATFIN)) or not((.w_ELDATFIN>=.w_DATINI AND .w_ELDATFIN<=.w_DATFIN) AND (.w_ELDATINI<=.w_ELDATFIN) or (empty(.w_ELDATINI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELDATFIN_1_29.SetFocus()
            i_bnoObbl = !empty(.w_ELDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Date non comprese nell'intervallo specificato in anagrafica contratti o data iniziale maggiore della data finale")
          case   not(IIF( EMPTY(.w_ELCODMOD) OR EMPTY(.w_ELCONTRA), .T., CHKLISD(.w_ELCODLIS,.w_LORNET,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_ELCODVAL, CTOD('  -  -  ')) ))  and not(Isahe())  and not(empty(.w_ELCODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELCODLIS_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ELTIPATT)) or not(.w_CARAGGST<>'Z'))  and (.w_FLATTI='S' AND .w_MOTIPCON<>'P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELTIPATT_1_32.SetFocus()
            i_bnoObbl = !empty(.w_ELTIPATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ELGRUPAR)) or not(.w_TIPRIS='G' AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)))  and (.w_FLATTI='S' AND .w_MOTIPCON<>'P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELGRUPAR_1_34.SetFocus()
            i_bnoObbl = !empty(.w_ELGRUPAR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore incongruente o obsoleto")
          case   (empty(.w_ELPERATT))  and (.w_FLATTI='S' AND .w_MOTIPCON<>'P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELPERATT_1_35.SetFocus()
            i_bnoObbl = !empty(.w_ELPERATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ELCAUDOC)) or not(.w_CATDOC<>'OR' AND .w_FLVEAC='V' and (! Isahe() or .w_FLRIPS='S')))  and (.w_FLATT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELCAUDOC_1_38.SetFocus()
            i_bnoObbl = !empty(.w_ELCAUDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo")
          case   (empty(.w_ELPERFAT))  and (.w_FLATT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELPERFAT_1_40.SetFocus()
            i_bnoObbl = !empty(.w_ELPERFAT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Periodicit� inesistente")
          case   (empty(.w_ELPERCON))  and (.w_ELESCRIN ="N")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELPERCON_1_46.SetFocus()
            i_bnoObbl = !empty(.w_ELPERCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ELCODART)) or not((.w_MOTIPCON='C' AND(.w_TIPART='FO' OR .w_TIPART='FM')) OR (.w_MOTIPCON='P' AND .w_TIPART='FM')))  and not(.w_MOTIPCON='P')  and ((.w_FLATT='S' OR (.w_FLATTI='S'  AND .w_FLNSAP='N') ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELCODART_2_4.SetFocus()
            i_bnoObbl = !empty(.w_ELCODART)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Servizio n� a valore n� a quantit� e valore ")
          case   ((empty(.w_ELCODART)) or not((.w_MOTIPCON='C' AND(.w_TIPART='FO' OR .w_TIPART='FM')) OR (.w_MOTIPCON='P' AND .w_TIPART='FM')))  and not(.w_MOTIPCON<>'P')  and ((.w_FLATT='S' OR (.w_FLATTI='S'  AND .w_FLNSAP='N') ) AND (.w_MOTIPCON='P' AND .w_ELQTACON=0))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELCODART_2_5.SetFocus()
            i_bnoObbl = !empty(.w_ELCODART)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Servizio non a quantit� e valore")
          case   ((empty(.w_ELUNIMIS)) or not(CHKUNIMI(IIF(.w_FLSERG='S', '***', .w_ELUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3) AND NOT EMPTY(.w_ELUNIMIS)))  and ((.w_FLATT='S' OR (.w_FLATTI='S'  AND .w_FLNSAP='N') ) AND .w_MOTIPCON<>'P')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELUNIMIS_2_11.SetFocus()
            i_bnoObbl = !empty(.w_ELUNIMIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ELQTAMOV>0)  and (.w_TIPART<>'FO')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELQTAMOV_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire quantit� maggiore di zero")
          case   not(CHKCONTR(.w_ELCONCOD,.w_TIPCON,.w_ELCODCLI,.w_CATCOM,.w_FLSCOR,.w_ELCODVAL, .w_DATALIST,.w_CT,.w_CC,.w_CM,.w_CV,.w_CI,.w_CF,.w_IVACON))  and not(Isahe())  and (.w_TIPMOD<>'F'  AND g_GESCON='S')  and not(empty(.w_ELCONCOD))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELCONCOD_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.), CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.) ))  and not(NOT (g_PERCCR='S' or g_COMM='S'))  and not(empty(.w_ELVOCCEN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELVOCCEN_2_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.), CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.) ))  and not(g_PERCCR<>'S')  and not(empty(.w_ELCODCEN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELCODCEN_2_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.), CHKDTOBS(.w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.) ))  and not(NOT(g_PERCAN='S' OR g_COMM='S' ))  and not(empty(.w_ELCOCOMM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELCOCOMM_2_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
          case   not((.w_ELINICOA<=.w_ELFINCOA) or (empty(.w_ELFINCOA)))  and (.w_FLNSAP<>'S' AND NOT EMPTY(.w_ELTIPATT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELINICOA_2_81.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   ((empty(.w_ELFINCOA)) or not((.w_ELINICOA<=.w_ELFINCOA) or (empty(.w_ELFINCOA))))  and (.w_FLNSAP<>'S' AND NOT EMPTY(.w_ELTIPATT) AND NOT EMPTY(.w_ELINICOA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELFINCOA_2_82.SetFocus()
            i_bnoObbl = !empty(.w_ELFINCOA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not((.w_ELINICOD<=.w_ELFINCOD) or (empty(.w_ELFINCOD)))  and (NOT EMPTY(NVL(.w_ELCAUDOC,'')))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELINICOD_2_83.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   ((empty(.w_ELFINCOD)) or not((.w_ELINICOD<=.w_ELFINCOD) or (empty(.w_ELFINCOD))))  and (NOT EMPTY(NVL(.w_ELCAUDOC,'')) AND NOT EMPTY(.w_ELINICOD))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELFINCOD_2_84.SetFocus()
            i_bnoObbl = !empty(.w_ELFINCOD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_ael
      IF i_bRes
         IF (this.w_FLSCOR='S' AND this.w_LORNET='N') 
           Ah_Errormsg('Attenzione.Intestatario con scorporo piede fattura, inserire listino di tipo "Iva inclusa (lordo)"',48,'')
           i_bRes=.f.
           this.w_RESCHK=1
         ENDIF
      ENDIF
      * --- Controllo validit� data prossimo documento
      IF i_bRes
         IF .w_FLATT='S' 
           if EMPTY(.w_ELDATFAT)
           i_bRes=AH_YESNO("Attenzione: affinch� l'elemento sia considerato dal piano di generazione documenti%0occorre valorizzare la data prossimo documento!%0Confermare comunque?")
           else
              if .w_ELDATFAT>(.w_ELDATFIN + .w_ELGIODOC)
               i_bRes = .f.
               i_bnoChk = .f.
               this.w_RESCHK=1
      	       i_cErrorMsg =Ah_MsgFormat("Data prossimo documento non valida")
              Endif
              if .w_ELDATFAT<.w_ELDATINI AND i_bRes = .t.
               i_bRes=AH_YESNO("Attenzione: la data prossimo documento � inferiore alla data inizio validit� dell'elemento contratto!%0Verranno generati documenti in base alla periodicit� %1 fino alla data %2!%0Procedo ugualmente?",,ALLTRIM(.w_DESPER),DTOC(.w_ELDATDOC))
              endif 
           Endif
         Endif 
      endif
      
      * --- Controllo validit� data prossima attivit�
      IF i_bRes
         IF .w_FLATTI='S'
           if EMPTY(.w_ELDATATT)
           i_bRes=AH_YESNO("Attenzione: affinch� l'elemento sia considerato dal piano di generazione attivit�%0occorre valorizzare la data prossima attivit�!%0Confermare comunque?")
           else
              if .w_ELDATATT>(.w_ELDATFIN + .w_ELGIOATT)
               i_bRes = .f.
               i_bnoChk = .f.
               this.w_RESCHK=1
      	       i_cErrorMsg =Ah_MsgFormat("Data prossima attivit� non valida")
              Endif
              if .w_ELDATATT<.w_ELDATINI AND i_bRes = .t.
               i_bRes=AH_YESNO("Attenzione: la data prossima attivit� � inferiore alla data inizio validit� dell'elemento contratto!%0Verranno generate attivit� in base alla periodicit� %1 fino alla data %2!%0Procedo ugualmente?",,ALLTRIM(.w_DESCRIAT),DTOC(.w_ELDATGAT))
              endif 
           Endif
         Endif
      endif
      
      * --- Controllo validit� data limite disdetta
      IF i_bRes
        if .w_ELRINCON='S' and !(.w_ELDATDIS>.w_ELDATINI AND .w_ELDATDIS<=.w_ELDATFIN)
          i_bRes = .f.
          i_bnoChk = .f.
          this.w_RESCHK=1
      	  i_cErrorMsg =Ah_MsgFormat("Data limite disdetta non compresa nell'intervallo di validit�")
        Endif
      Endif
      
      * --- Controllo congruita' Centri di Costo
      if i_bRes AND ((g_PERCCR='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S')) 
         do case
            case EMPTY(.w_ELVOCCEN) AND g_PERCCR='S' AND .w_FLANAL='S'
                 i_bRes = .f.
                 i_bnoChk = .f.
                 this.w_RESCHK=1
      	         i_cErrorMsg = Ah_MsgFormat("Voce di ricavo non definita")
            case EMPTY(.w_ELCOCOMM) AND g_COMM='S' AND .w_FLGCOM='S'
                 i_bRes = .f.
                 i_bnoChk = .f.
                 this.w_RESCHK=1
      	         i_cErrorMsg = Ah_MsgFormat("Commessa non definita")
            case EMPTY(.w_ELCODCEN) AND g_PERCCR='S' AND .w_FLANAL='S'
      	         i_bRes = .f.
      	         i_bnoChk = .f.		
                 this.w_RESCHK=1
      	         i_cErrorMsg = Ah_MsgFormat("Centro di ricavo non definito")
            endcase
      endif
      * --- Controllo congruita' Commessa
      if i_bRes AND g_COMM='S' AND .w_FLGCOM='S' 
         if EMPTY(.w_ELCODATT) AND NOT EMPTY(.w_ELCOCOMM)
            i_bRes = .f.
            i_bnoChk = .f.
            this.w_RESCHK=1
            i_cErrorMsg = Ah_MsgFormat("Attivit� di commessa non definita")
         endif
      endif
      
      * --- Controllo congruita' date competenza
      IF i_bRes
        if ((.w_ELINICOA>.w_ELFINCOA)  or (empty(.w_ELINICOA) AND NOT empty(.w_ELFINCOA)) OR (NOT EMPTY(.w_ELINICOA) AND EMPTY(.w_ELFINCOA)))
          i_bRes = .f.
          i_bnoChk = .f.
          this.w_RESCHK=1
      	  i_cErrorMsg =Ah_MsgFormat("Data inizio competenza attivit� maggiore della data finale o vuota")
        Endif
      Endif
      
      IF i_bRes
        if ((.w_ELINICOD>.w_ELFINCOD)  or (empty(.w_ELINICOD) AND NOT empty(.w_ELFINCOD)) OR (NOT EMPTY(.w_ELINICOD) AND EMPTY(.w_ELFINCOD)))
          i_bRes = .f.
          i_bnoChk = .f.
          this.w_RESCHK=1
      	  i_cErrorMsg =Ah_MsgFormat("Data inizio competenza documenti maggiore della data finale o vuota")
        Endif
      Endif
      
      if i_bRes
         .w_RESCHK=0
           .NotifyEvent('CalcoliFinali')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ELCODMOD = this.w_ELCODMOD
    this.o_ELCODCLI = this.w_ELCODCLI
    this.o_ELCONTRA = this.w_ELCONTRA
    this.o_TIPATT = this.w_TIPATT
    this.o_GRUPAR = this.w_GRUPAR
    this.o_TIPDOC = this.w_TIPDOC
    this.o_ELDATINI = this.w_ELDATINI
    this.o_FLATTI = this.w_FLATTI
    this.o_PRIMADATA = this.w_PRIMADATA
    this.o_PRIMADATADOC = this.w_PRIMADATADOC
    this.o_ELDATFIN = this.w_ELDATFIN
    this.o_ELCODLIS = this.w_ELCODLIS
    this.o_FLATT = this.w_FLATT
    this.o_ELPERATT = this.w_ELPERATT
    this.o_ELDATATT = this.w_ELDATATT
    this.o_ELGIOATT = this.w_ELGIOATT
    this.o_ELCAUDOC = this.w_ELCAUDOC
    this.o_ELPERFAT = this.w_ELPERFAT
    this.o_ELDATFAT = this.w_ELDATFAT
    this.o_ELGIODOC = this.w_ELGIODOC
    this.o_ELESCRIN = this.w_ELESCRIN
    this.o_ELRINCON = this.w_ELRINCON
    this.o_ELNUMGIO = this.w_ELNUMGIO
    this.o_ELCODART = this.w_ELCODART
    this.o_ELUNIMIS = this.w_ELUNIMIS
    this.o_ELQTAMOV = this.w_ELQTAMOV
    this.o_ELPREZZO = this.w_ELPREZZO
    this.o_ELSCONT1 = this.w_ELSCONT1
    this.o_ELSCONT2 = this.w_ELSCONT2
    this.o_ELSCONT3 = this.w_ELSCONT3
    this.o_ELSCONT4 = this.w_ELSCONT4
    this.o_ELCODVAL = this.w_ELCODVAL
    this.o_ELCOCOMM = this.w_ELCOCOMM
    this.o_TIPART = this.w_TIPART
    this.o_ELINICOA = this.w_ELINICOA
    this.o_ELINICOD = this.w_ELINICOD
    return

  func CanDelete()
    local i_res
    i_res=this.w_MOTIPCON<>'P' OR this.w_ELQTACON=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Elemento contratto abbinato ad almeno un'attivit�. Impossibile eliminare!"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsag_aelPag1 as StdContainer
  Width  = 588
  height = 557
  stdWidth  = 588
  stdheight = 557
  resizeXpos=533
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELCODMOD_1_2 as StdField with uid="JIBYUMKBKV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ELCODMOD", cQueryName = "ELCODMOD",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Modello elementi",;
    HelpContextID = 218716554,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=127, Top=6, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_ELEM", cZoomOnZoom="GSAG_AME", oKey_1_1="MOCODICE", oKey_1_2="this.w_ELCODMOD"

  func oELCODMOD_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODMOD_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODMOD_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_ELEM','*','MOCODICE',cp_AbsName(this.parent,'oELCODMOD_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_AME',"Modelli",'GSAG_KV3.MOD_ELEM_VZM',this.parent.oContained
  endproc
  proc oELCODMOD_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAG_AME()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_ELCODMOD
     i_obj.ecpSave()
  endproc

  add object oELCODCLI_1_5 as StdField with uid="GSZVTFERDD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ELCODCLI", cQueryName = "ELCODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 50944399,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=127, Top=31, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_ELCODCLI"

  func oELCODCLI_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oELCODCLI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODCLI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODCLI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oELCODCLI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oELCODCLI_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_ELCODCLI
     i_obj.ecpSave()
  endproc

  add object oELCODIMP_1_6 as StdField with uid="FMSCPOYKPC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ELCODIMP", cQueryName = "ELCODMOD,ELCODIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice dell'impianto",;
    HelpContextID = 151607702,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=127, Top=81, InputMask=replicate('X',10)

  add object oELCODCOM_1_8 as StdField with uid="FITQBEKIEU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ELCODCOM", cQueryName = "ELCODMOD,ELCODIMP,ELCODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50944403,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=127, Top=106

  add object oELCONTRA_1_9 as StdField with uid="NQBWZIKNJD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ELCONTRA", cQueryName = "ELCODMOD,ELCODIMP,ELCODCOM,ELCONTRA",nZero=10,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice contratto",;
    HelpContextID = 190228089,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=127, Top=56, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CON_TRAS", cZoomOnZoom="GSAG_ACA", oKey_1_1="COSERIAL", oKey_1_2="this.w_ELCONTRA"

  func oELCONTRA_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCONTRA_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCONTRA_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CON_TRAS','*','COSERIAL',cp_AbsName(this.parent,'oELCONTRA_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACA',"Contratti",'GSAG_AEL.CON_TRAS_VZM',this.parent.oContained
  endproc
  proc oELCONTRA_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_COSERIAL=this.parent.oContained.w_ELCONTRA
     i_obj.ecpSave()
  endproc

  add object oELDATINI_1_21 as StdField with uid="AJKXPHHGVX",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ELDATINI", cQueryName = "ELDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Date non comprese nell'intervallo specificato in anagrafica contratti!",;
    ToolTipText = "Data inizio applicazione contratto",;
    HelpContextID = 167471503,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=127, Top=132

  func oELDATINI_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ELDATINI>=.w_DATINI AND .w_ELDATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oELDATFIN_1_29 as StdField with uid="JCSMJCZMBG",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ELDATFIN", cQueryName = "ELDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Date non comprese nell'intervallo specificato in anagrafica contratti o data iniziale maggiore della data finale",;
    ToolTipText = "Data fine applicazione contratto",;
    HelpContextID = 117139860,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=279, Top=132

  func oELDATFIN_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ELDATFIN>=.w_DATINI AND .w_ELDATFIN<=.w_DATFIN) AND (.w_ELDATINI<=.w_ELDATFIN) or (empty(.w_ELDATINI)))
    endwith
    return bRes
  endfunc

  add object oELCATCON_1_30 as StdField with uid="KQEOVMLXFF",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ELCATCON", cQueryName = "ELCATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contratto",;
    HelpContextID = 66804116,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=127, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_ELEM", cZoomOnZoom="GSAG_ACE", oKey_1_1="CECODELE", oKey_1_2="this.w_ELCATCON"

  func oELCATCON_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCATCON_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCATCON_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ELEM','*','CECODELE',cp_AbsName(this.parent,'oELCATCON_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACE',"Categorie contratti",'',this.parent.oContained
  endproc
  proc oELCATCON_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODELE=this.parent.oContained.w_ELCATCON
     i_obj.ecpSave()
  endproc

  add object oELCODLIS_1_31 as StdField with uid="XGWRVDFFGC",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ELCODLIS", cQueryName = "ELCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino",;
    HelpContextID = 201939353,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=127, Top=206, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ELCODLIS"

  func oELCODLIS_1_31.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oELCODLIS_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODLIS_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODLIS_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oELCODLIS_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oELCODLIS_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_ELCODLIS
     i_obj.ecpSave()
  endproc

  add object oELTIPATT_1_32 as StdField with uid="SFIFNHEATF",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ELTIPATT", cQueryName = "ELTIPATT",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 238786150,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=127, Top=236, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_ELTIPATT"

  func oELTIPATT_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLATTI='S' AND .w_MOTIPCON<>'P')
    endwith
   endif
  endfunc

  func oELTIPATT_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oELTIPATT_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELTIPATT_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oELTIPATT_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'GSAG_MCA.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oELTIPATT_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ELTIPATT
     i_obj.ecpSave()
  endproc

  add object oELGRUPAR_1_34 as StdField with uid="XSAVVZNWKG",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ELGRUPAR", cQueryName = "ELGRUPAR",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valore incongruente o obsoleto",;
    ToolTipText = "Gruppo partecipante attivit�",;
    HelpContextID = 249783912,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=261, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_ELGRUPAR"

  func oELGRUPAR_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLATTI='S' AND .w_MOTIPCON<>'P')
    endwith
   endif
  endfunc

  func oELGRUPAR_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oELGRUPAR_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELGRUPAR_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oELGRUPAR_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oELGRUPAR_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_ELGRUPAR
     i_obj.ecpSave()
  endproc

  add object oELPERATT_1_35 as StdField with uid="ZDRBTDNVNL",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ELPERATT", cQueryName = "ELPERATT",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Periodicit� attivit�",;
    HelpContextID = 236967526,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=286, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_ELPERATT"

  func oELPERATT_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLATTI='S' AND .w_MOTIPCON<>'P')
    endwith
   endif
  endfunc

  func oELPERATT_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oELPERATT_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELPERATT_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oELPERATT_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo periodicit�",'',this.parent.oContained
  endproc
  proc oELPERATT_1_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_ELPERATT
     i_obj.ecpSave()
  endproc

  add object oELDATATT_1_36 as StdField with uid="MKQNCZTGZU",rtseq=34,rtrep=.f.,;
    cFormVar = "w_ELDATATT", cQueryName = "ELDATATT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prossima attivit�",;
    HelpContextID = 235181670,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=127, Top=310

  func oELDATATT_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLATTI='S' AND .w_MOTIPCON<>'P')
    endwith
   endif
  endfunc

  add object oELGIOATT_1_37 as StdField with uid="CIFPROMYSS",rtseq=35,rtrep=.f.,;
    cFormVar = "w_ELGIOATT", cQueryName = "ELGIOATT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni di tolleranza per gen. att.",;
    HelpContextID = 239887974,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=369, Top=310

  func oELGIOATT_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLATTI='S' AND .w_MOTIPCON<>'P')
    endwith
   endif
  endfunc

  add object oELCAUDOC_1_38 as StdField with uid="UQVSEYLYCZ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_ELCAUDOC", cQueryName = "ELCAUDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo",;
    HelpContextID = 84629897,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=128, Top=347, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_ELCAUDOC"

  func oELCAUDOC_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLATT='S')
    endwith
   endif
  endfunc

  func oELCAUDOC_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCAUDOC_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCAUDOC_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oELCAUDOC_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oELCAUDOC_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_ELCAUDOC
     i_obj.ecpSave()
  endproc

  add object oELCODPAG_1_39 as StdField with uid="ASKOFPGSXQ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_ELCODPAG", cQueryName = "ELCODPAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 267822707,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=127, Top=373, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_ELCODPAG"

  func oELCODPAG_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLATT='S')
    endwith
   endif
  endfunc

  func oELCODPAG_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODPAG_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODPAG_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oELCODPAG_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oELCODPAG_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_ELCODPAG
     i_obj.ecpSave()
  endproc

  add object oELPERFAT_1_40 as StdField with uid="OIFIBVFPTX",rtseq=38,rtrep=.f.,;
    cFormVar = "w_ELPERFAT", cQueryName = "ELPERFAT",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Periodicit� inesistente",;
    ToolTipText = "Periodicit� documento",;
    HelpContextID = 153081446,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=399, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_ELPERFAT"

  func oELPERFAT_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLATT='S')
    endwith
   endif
  endfunc

  func oELPERFAT_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oELPERFAT_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELPERFAT_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oELPERFAT_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo periodicit�",'',this.parent.oContained
  endproc
  proc oELPERFAT_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_ELPERFAT
     i_obj.ecpSave()
  endproc

  add object oELDATFAT_1_41 as StdField with uid="JJMKJZCEVA",rtseq=39,rtrep=.f.,;
    cFormVar = "w_ELDATFAT", cQueryName = "ELDATFAT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prossimo documento",;
    HelpContextID = 151295590,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=127, Top=425

  func oELDATFAT_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLATT='S')
    endwith
   endif
  endfunc

  add object oELGIODOC_1_42 as StdField with uid="SADNDRPEIC",rtseq=40,rtrep=.f.,;
    cFormVar = "w_ELGIODOC", cQueryName = "ELGIODOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "gg di tolleranza per gen. doc.",;
    HelpContextID = 78879113,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=369, Top=425

  func oELGIODOC_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLATT='S')
    endwith
   endif
  endfunc

  add object oELESCRIN_1_43 as StdCheck with uid="SFRDVXIRRC",rtseq=41,rtrep=.f.,left=191, top=463, caption="Esclude rinnovo",;
    ToolTipText = "Eslude rinnovi",;
    HelpContextID = 33388948,;
    cFormVar="w_ELESCRIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELESCRIN_1_43.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oELESCRIN_1_43.GetRadio()
    this.Parent.oContained.w_ELESCRIN = this.RadioValue()
    return .t.
  endfunc

  func oELESCRIN_1_43.SetRadio()
    this.Parent.oContained.w_ELESCRIN=trim(this.Parent.oContained.w_ELESCRIN)
    this.value = ;
      iif(this.Parent.oContained.w_ELESCRIN=='S',1,;
      0)
  endfunc

  add object oELPERCON_1_46 as StdField with uid="PCPYKTKNAF",rtseq=44,rtrep=.f.,;
    cFormVar = "w_ELPERCON", cQueryName = "ELPERCON",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Periodicit� rinnovo contratti",;
    HelpContextID = 65022356,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=486, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_ELPERCON"

  func oELPERCON_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELESCRIN ="N")
    endwith
   endif
  endfunc

  func oELPERCON_1_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_46('Part',this)
    endwith
    return bRes
  endfunc

  proc oELPERCON_1_46.ecpDrop(oSource)
    this.Parent.oContained.link_1_46('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELPERCON_1_46.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oELPERCON_1_46'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oELPERCON_1_46.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_ELPERCON
     i_obj.ecpSave()
  endproc

  add object oELRINCON_1_48 as StdCheck with uid="ASMOIOOXOX",rtseq=46,rtrep=.f.,left=127, top=513, caption="Rinnovo tacito",;
    ToolTipText = "Rinnovo tacito",;
    HelpContextID = 61098388,;
    cFormVar="w_ELRINCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELRINCON_1_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oELRINCON_1_48.GetRadio()
    this.Parent.oContained.w_ELRINCON = this.RadioValue()
    return .t.
  endfunc

  func oELRINCON_1_48.SetRadio()
    this.Parent.oContained.w_ELRINCON=trim(this.Parent.oContained.w_ELRINCON)
    this.value = ;
      iif(this.Parent.oContained.w_ELRINCON=='S',1,;
      0)
  endfunc

  func oELRINCON_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELESCRIN ="N")
    endwith
   endif
  endfunc

  add object oELNUMGIO_1_49 as StdField with uid="GGPAOVIDJS",rtseq=47,rtrep=.f.,;
    cFormVar = "w_ELNUMGIO", cQueryName = "ELNUMGIO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni entro i quali comunicare la disdetta. E' editabile se il rinnovo tacito � attivo e se la data limite disdetta � vuota oppure � uguale alla data di fine validit� dell'elemento contratto",;
    HelpContextID = 127928725,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=490, Top=511

  func oELNUMGIO_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELRINCON = "S" )
    endwith
   endif
  endfunc

  add object oELDATDIS_1_50 as StdField with uid="NNWYPIFLQX",rtseq=48,rtrep=.f.,;
    cFormVar = "w_ELDATDIS", cQueryName = "ELDATDIS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data limite disdetta",;
    HelpContextID = 83585433,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=448, Top=535

  func oELDATDIS_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELRINCON = "S" AND .w_ELNUMGIO = 0)
    endwith
   endif
  endfunc


  add object oBtn_1_51 as StdButton with uid="SSFVFOCHOD",left=536, top=511, width=48,height=45,;
    CpPicture="BMP\CONTRATT.ICO", caption="", nPag=1;
    , ToolTipText = "Premere per rinnovare l'elemento contratto";
    , HelpContextID = 119855338;
    , caption='\<Rinnova';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_51.Click()
      with this.Parent.oContained
        .Notifyevent('Rinnova')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_51.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY( .w_ELPERCON ) AND .cFUNCTION = "Query")
      endwith
    endif
  endfunc

  add object oDESELE_1_65 as StdField with uid="QPMAEWAEFR",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESELE", cQueryName = "DESELE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 92295734,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=228, Top=6, InputMask=replicate('X',60)

  add object oELDATDOC_1_67 as StdField with uid="XGKBJRDNON",rtseq=59,rtrep=.f.,;
    cFormVar = "w_ELDATDOC", cQueryName = "ELDATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fino alla quale l'elemento potr� generare documenti",;
    HelpContextID = 83585417,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=454, Top=425

  add object oELDATGAT_1_69 as StdField with uid="KEFGAQKQFS",rtseq=60,rtrep=.f.,;
    cFormVar = "w_ELDATGAT", cQueryName = "ELDATGAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fino alla quale l'elemento potr� generare attivit�",;
    HelpContextID = 134518374,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=454, Top=310

  add object oDESCLI_1_70 as StdField with uid="VKIXWTYMHT",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 159273526,;
   bGlobalFont=.t.,;
    Height=21, Width=331, Left=250, Top=31, InputMask=replicate('X',50)

  add object oDESPER_1_71 as StdField with uid="FLNNGAKPXT",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESPER", cQueryName = "DESPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 233090506,;
   bGlobalFont=.t.,;
    Height=21, Width=342, Left=188, Top=399, InputMask=replicate('X',50)

  add object oDESCAT_1_72 as StdField with uid="UUJMDVZLQG",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 204582346,;
   bGlobalFont=.t.,;
    Height=21, Width=403, Left=181, Top=157, InputMask=replicate('X',60)

  add object oDESCRIAT_1_76 as StdField with uid="URXPABGLKR",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESCRIAT", cQueryName = "DESCRIAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 102870390,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=177, Top=286, InputMask=replicate('X',50)

  add object oDESCON_1_78 as StdField with uid="YVQICAUMLR",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 246305334,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=228, Top=56, InputMask=replicate('X',50)

  add object oDESATT_1_79 as StdField with uid="LVCSAEKOST",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 184790474,;
   bGlobalFont=.t.,;
    Height=21, Width=301, Left=283, Top=236, InputMask=replicate('X',254)

  add object oDESDOC_1_81 as StdField with uid="LIHTDOVJGR",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 61821494,;
   bGlobalFont=.t.,;
    Height=21, Width=387, Left=197, Top=347, InputMask=replicate('X',35)

  add object oDESLIS_1_90 as StdField with uid="SWKCQHAJKZ",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 212381130,;
   bGlobalFont=.t.,;
    Height=21, Width=389, Left=195, Top=206, InputMask=replicate('X',40)

  func oDESLIS_1_90.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oDESPAG_1_111 as StdField with uid="CEKXLHALVI",rtseq=89,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 115036726,;
   bGlobalFont=.t.,;
    Height=21, Width=387, Left=197, Top=373, InputMask=replicate('X',30)


  add object oObj_1_127 as cp_runprogram with uid="IPEKSAKJSD",left=-19, top=677, width=330,height=21,;
    caption='GSAG_BDL',;
   bGlobalFont=.t.,;
    prg="GSAG_BDL('D',w_KEYLISTB,Null,Space(5))",;
    cEvent = "Record Inserted,Record Updated,Edit Aborted",;
    nPag=1;
    , HelpContextID = 206487374

  add object oIMDESCRI_1_142 as StdField with uid="LPAMSNEPPT",rtseq=118,rtrep=.f.,;
    cFormVar = "w_IMDESCRI", cQueryName = "IMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione dell'impianto",;
    HelpContextID = 202413361,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=228, Top=81, InputMask=replicate('X',50)

  add object oIMDESCON_1_145 as StdField with uid="ONBYHNYWXU",rtseq=120,rtrep=.f.,;
    cFormVar = "w_IMDESCON", cQueryName = "IMDESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del componente dell'impianto",;
    HelpContextID = 66022100,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=225, Top=106, InputMask=replicate('X',50)


  add object oObj_1_146 as cp_runprogram with uid="JFPEHYJVFL",left=-19, top=700, width=330,height=21,;
    caption='GSAG_BCL',;
   bGlobalFont=.t.,;
    prg="GSAG_BCL('R')",;
    cEvent = "w_ELCONTRA Changed,w_ELCODMOD Changed",;
    nPag=1;
    , HelpContextID = 206487374

  add object oCPROWORD_IMP_DETT_1_149 as StdField with uid="QDTITHJNTN",rtseq=123,rtrep=.f.,;
    cFormVar = "w_CPROWORD_IMP_DETT", cQueryName = "CPROWORD_IMP_DETT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di riga del componente dell'impianto",;
    HelpContextID = 176124398,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=127, Top=106


  add object oObj_1_150 as cp_runprogram with uid="ZGCDDYMTWZ",left=-19, top=654, width=231,height=19,;
    caption='GSAG_BD4(R)',;
   bGlobalFont=.t.,;
    prg="GSAG_BD4('R',w_ELCONTRA, w_ELCODMOD, w_ELCODIMP, w_ELCODCOM, w_ELRINNOV, w_ELPERCON,0,0,0,0,0,0,0,0,0,w_FLDATFIS)",;
    cEvent = "Rinnova",;
    nPag=1;
    , HelpContextID = 206297830

  add object oDPDESCRI_1_152 as StdField with uid="BQXYASYUCZ",rtseq=143,rtrep=.f.,;
    cFormVar = "w_DPDESCRI", cQueryName = "DPDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 202412673,;
   bGlobalFont=.t.,;
    Height=21, Width=390, Left=194, Top=261, InputMask=replicate('X',60)

  add object oELCODVAL_1_153 as StdField with uid="JKQEPAGTAM",rtseq=144,rtrep=.f.,;
    cFormVar = "w_ELCODVAL", cQueryName = "ELCODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 167159406,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=127, Top=182, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_ELCODVAL"

  func oELCODVAL_1_153.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESVAL_1_155 as StdField with uid="IMAPCMNKTX",rtseq=145,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 199316022,;
   bGlobalFont=.t.,;
    Height=21, Width=403, Left=181, Top=182, InputMask=replicate('X',35)

  add object oELRINNOV_1_162 as StdField with uid="QGGQVUVWSH",rtseq=164,rtrep=.f.,;
    cFormVar = "w_ELRINNOV", cQueryName = "ELCODMOD,ELCODIMP,ELCODCOM,ELCONTRA,ELRINNOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero rinnovi",;
    HelpContextID = 245647772,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=127, Top=462

  add object oMDDESCRI_1_164 as StdField with uid="SYOXWKZIIL",rtseq=165,rtrep=.f.,;
    cFormVar = "w_MDDESCRI", cQueryName = "MDDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 202415601,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=193, Top=486, InputMask=replicate('X',50)


  add object oObj_1_170 as cp_runprogram with uid="ELDBQWGEIG",left=-19, top=722, width=223,height=19,;
    caption='GSAG_BCL(Z)',;
   bGlobalFont=.t.,;
    prg="GSAG_BCL('Z')",;
    cEvent = "w_ELCONCOD Changed",;
    nPag=1;
    , HelpContextID = 206295758


  add object oBtn_1_177 as StdButton with uid="KSUWNHATXF",left=536, top=286, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla visualizzazione delle attivit� generate";
    , HelpContextID = 235069552;
    , Caption='\<Vis. Att.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_177.Click()
      with this.Parent.oContained
        GSAG2BZM(this.Parent.oContained,"AO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_177.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' and !empty(.w_ELCONTRA))
      endwith
    endif
  endfunc

  func oBtn_1_177.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_ATTEXIST)
     endwith
    endif
  endfunc


  add object oBtn_1_178 as StdButton with uid="CGHHCAFYSP",left=536, top=399, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla visualizzazione dei documenti generati";
    , HelpContextID = 235069552;
    , Caption='\<Vis. Doc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_178.Click()
      with this.Parent.oContained
        GSAG2BZM(this.Parent.oContained,"DO") 
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_178.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' and !empty(.w_ELCONTRA))
      endwith
    endif
  endfunc

  func oBtn_1_178.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_DOCEXIST)
     endwith
    endif
  endfunc


  add object oObj_1_179 as cp_runprogram with uid="WMCMDQULXA",left=-19, top=756, width=220,height=23,;
    caption='GSAG2BZM(XR)',;
   bGlobalFont=.t.,;
    prg="GSAG2BZM('XR')",;
    cEvent = "Load",;
    nPag=1;
    , ToolTipText = "Aggiorna gli elementi contratto";
    , HelpContextID = 17808179


  add object oObj_1_183 as cp_runprogram with uid="ZXQEZMLEQN",left=-19, top=742, width=223,height=19,;
    caption='GSAG_BCL(D)',;
   bGlobalFont=.t.,;
    prg="GSAG_BCL('D')",;
    cEvent = "CalcoliFinali",;
    nPag=1;
    , HelpContextID = 206301390

  add object oStr_1_14 as StdString with uid="OFOGPQSPZZ",Visible=.t., Left=59, Top=56,;
    Alignment=1, Width=66, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="HNOXWDPZKG",Visible=.t., Left=69, Top=6,;
    Alignment=1, Width=56, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="ZPVBPHFFNQ",Visible=.t., Left=91, Top=132,;
    Alignment=1, Width=34, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="SEXIBPXPLX",Visible=.t., Left=255, Top=133,;
    Alignment=1, Width=23, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="TREAKEDOYE",Visible=.t., Left=63, Top=157,;
    Alignment=1, Width=62, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="RECURRHNPY",Visible=.t., Left=25, Top=398,;
    Alignment=1, Width=100, Height=18,;
    Caption="Periodicit� doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="OKFJRRAQCY",Visible=.t., Left=27, Top=425,;
    Alignment=1, Width=98, Height=18,;
    Caption="Prossimo doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="BDMDZSQESG",Visible=.t., Left=83, Top=32,;
    Alignment=1, Width=42, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="GFWLRRIXIP",Visible=.t., Left=24, Top=310,;
    Alignment=1, Width=101, Height=18,;
    Caption="Prossima attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="JUMMMMPTTP",Visible=.t., Left=81, Top=261,;
    Alignment=1, Width=44, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="GRYGCNVWHA",Visible=.t., Left=27, Top=286,;
    Alignment=1, Width=98, Height=18,;
    Caption="Periodicit� att.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="XXTYMZANRA",Visible=.t., Left=45, Top=236,;
    Alignment=1, Width=80, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="MSBVDPRUIW",Visible=.t., Left=10, Top=347,;
    Alignment=1, Width=115, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="EZUZGFJVBZ",Visible=.t., Left=238, Top=425,;
    Alignment=1, Width=129, Height=18,;
    Caption="Giorni toll. gen. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="ZVBJJYPOSX",Visible=.t., Left=257, Top=310,;
    Alignment=1, Width=110, Height=18,;
    Caption="Giorni toll. gen. att.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="NIPJNHNPVP",Visible=.t., Left=411, Top=425,;
    Alignment=1, Width=42, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="STSMTSPMRI",Visible=.t., Left=411, Top=310,;
    Alignment=1, Width=42, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="KNTMQONIRV",Visible=.t., Left=68, Top=206,;
    Alignment=1, Width=57, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_89.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_1_110 as StdString with uid="YBRCUUTOWA",Visible=.t., Left=58, Top=373,;
    Alignment=1, Width=67, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_141 as StdString with uid="BDLWCJFOUX",Visible=.t., Left=14, Top=82,;
    Alignment=1, Width=111, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_143 as StdString with uid="ZPNBJHZOTQ",Visible=.t., Left=15, Top=107,;
    Alignment=1, Width=110, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_154 as StdString with uid="LIQXMMGLKX",Visible=.t., Left=31, Top=182,;
    Alignment=1, Width=94, Height=18,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_160 as StdString with uid="HTISCBXRRV",Visible=.t., Left=5, Top=328,;
    Alignment=0, Width=115, Height=14,;
    Caption="Documento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_161 as StdString with uid="YZVVOCOKFH",Visible=.t., Left=5, Top=218,;
    Alignment=0, Width=64, Height=14,;
    Caption="Attivit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_163 as StdString with uid="YCPFJDEKMD",Visible=.t., Left=16, Top=464,;
    Alignment=1, Width=109, Height=18,;
    Caption="Numero rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_165 as StdString with uid="JOMWBBMPTU",Visible=.t., Left=13, Top=488,;
    Alignment=1, Width=112, Height=18,;
    Caption="Periodicit� rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_166 as StdString with uid="BHOBFOMPCL",Visible=.t., Left=307, Top=535,;
    Alignment=1, Width=138, Height=18,;
    Caption="Data limite disdetta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_167 as StdString with uid="TSFVLERUOH",Visible=.t., Left=298, Top=511,;
    Alignment=1, Width=188, Height=18,;
    Caption="Giorni preavviso disdetta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_169 as StdString with uid="UFRWATMVMF",Visible=.t., Left=5, Top=443,;
    Alignment=0, Width=115, Height=18,;
    Caption="Rinnovi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_158 as StdBox with uid="LIPBORAQCJ",left=6, top=232, width=577,height=2

  add object oBox_1_159 as StdBox with uid="TNGEULSYRY",left=6, top=343, width=577,height=2

  add object oBox_1_168 as StdBox with uid="IWBAEXBSMX",left=6, top=458, width=577,height=2
enddefine
define class tgsag_aelPag2 as StdContainer
  Width  = 588
  height = 557
  stdWidth  = 588
  stdheight = 557
  resizeXpos=412
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_2_1 as cp_runprogram with uid="LJNVXIAYNT",left=1, top=674, width=330,height=21,;
    caption='GSAG_BCL(T)',;
   bGlobalFont=.t.,;
    prg="GSAG_BCL('T')",;
    cEvent = "w_ELCODCLI Changed,w_ELDATFAT Changed,w_ELCODVAL Changed,w_ELDATINI Changed,w_ELCONTRA Changed,Edit Started,w_ELDATATT Changed,LisRig,w_ELCODMOD Changed",;
    nPag=2;
    , HelpContextID = 206297294


  add object oObj_2_2 as cp_runprogram with uid="QCDFOTEQFC",left=2, top=618, width=754,height=19,;
    caption='GSAG_BCL(C)',;
   bGlobalFont=.t.,;
    prg="GSAG_BCL('C')",;
    cEvent = "w_ELCODART Changed,w_ELQTAMOV Changed, w_ELUNIMIS Changed, w_ELCODLIS Changed,w_ELCONTRA Changed,LisRig, w_ELCODMOD Changed, w_ELCODCLI Changed, w_ELDATATT Changed, w_ELDATFAT Changed,Aggio",;
    nPag=2;
    , HelpContextID = 206301646

  add object oELCODART_2_4 as StdField with uid="ORXGOFZIXD",rtseq=125,rtrep=.f.,;
    cFormVar = "w_ELCODART", cQueryName = "ELCODART",;
    bObbl = .t. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Servizio n� a valore n� a quantit� e valore ",;
    ToolTipText = "Codice servizio",;
    HelpContextID = 251045478,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=149, Top=25, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_ELCODART"

  func oELCODART_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLATT='S' OR (.w_FLATTI='S'  AND .w_FLNSAP='N') ))
    endwith
   endif
  endfunc

  func oELCODART_2_4.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON='P')
    endwith
  endfunc

  func oELCODART_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODART_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODART_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oELCODART_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSMA2AAS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oELCODART_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ELCODART
     i_obj.ecpSave()
  endproc

  add object oELCODART_2_5 as StdField with uid="DVRYHJOSXW",rtseq=126,rtrep=.f.,;
    cFormVar = "w_ELCODART", cQueryName = "ELCODART",;
    bObbl = .t. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Servizio non a quantit� e valore",;
    ToolTipText = "Codice servizio",;
    HelpContextID = 251045478,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=149, Top=25, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_ELCODART"

  func oELCODART_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLATT='S' OR (.w_FLATTI='S'  AND .w_FLNSAP='N') ) AND (.w_MOTIPCON='P' AND .w_ELQTACON=0))
    endwith
   endif
  endfunc

  func oELCODART_2_5.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  func oELCODART_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODART_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODART_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oELCODART_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSMA3AAS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oELCODART_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ELCODART
     i_obj.ecpSave()
  endproc


  add object oBtn_2_7 as StdButton with uid="TWKEMSZZJT",left=149, top=54, width=48,height=45,;
    CpPicture="BMP\APPLICA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per rieseguire applicazione dei listini";
    , HelpContextID = 187321241;
    , caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_7.Click()
      with this.Parent.oContained
        .Notifyevent('Aggio')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_2_8 as StdButton with uid="FLVSTLMVZH",left=200, top=54, width=48,height=45,;
    CpPicture="bmp\carica.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai dati di riga";
    , HelpContextID = 8484534;
    , TabStop=.f.,Caption='\<Listini';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_8.Click()
      do GSAG_KLC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!isahe())
     endwith
    endif
  endfunc


  add object oBtn_2_9 as StdButton with uid="DVFYFXDEXI",left=251, top=54, width=48,height=45,;
    CpPicture="BMP\INVENTAR.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al dettaglio listini articolo di vendita/entrambi";
    , HelpContextID = 134969114;
    , tabstop=.f., caption='Lis.ve\<nd.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_9.Click()
      with this.Parent.oContained
        GSVE_BLI(this.Parent.oContained,"LISV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (isahe() and (NOT EMPTY(.w_ARCODART) and !(g_CVEN <> 'S' And g_CACQ = 'S'  ) ))
      endwith
    endif
  endfunc

  func oBtn_2_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!isahe())
     endwith
    endif
  endfunc


  add object oBtn_2_10 as StdButton with uid="DGTXJYSTJS",left=301, top=54, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=2;
    , ToolTipText = "Apre la stampa verifica listini applicabili e applicati";
    , HelpContextID = 79273033;
    , caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_10.Click()
      do GSVE_SVL with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( !Empty(.w_ELTCOLIS+.w_ELSCOLIS))
      endwith
    endif
  endfunc

  func oBtn_2_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (! Isahe())
     endwith
    endif
  endfunc

  add object oELUNIMIS_2_11 as StdField with uid="FSRPABVTMZ",rtseq=128,rtrep=.f.,;
    cFormVar = "w_ELUNIMIS", cQueryName = "ELUNIMIS",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 223967641,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=149, Top=106, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ELUNIMIS"

  func oELUNIMIS_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLATT='S' OR (.w_FLATTI='S'  AND .w_FLNSAP='N') ) AND .w_MOTIPCON<>'P')
    endwith
   endif
  endfunc

  func oELUNIMIS_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oELUNIMIS_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELUNIMIS_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oELUNIMIS_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oELUNIMIS_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ELUNIMIS
     i_obj.ecpSave()
  endproc

  add object oELQTAMOV_2_12 as StdField with uid="MWYRKODYXV",rtseq=129,rtrep=.f.,;
    cFormVar = "w_ELQTAMOV", cQueryName = "ELQTAMOV",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire quantit� maggiore di zero",;
    ToolTipText = "Quantit�",;
    HelpContextID = 215955868,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=149, Top=132, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  func oELQTAMOV_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPART<>'FO')
    endwith
   endif
  endfunc

  func oELQTAMOV_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ELQTAMOV>0)
    endwith
    return bRes
  endfunc

  add object oELCONCOD_2_13 as StdField with uid="WUBZSPRFMS",rtseq=130,rtrep=.f.,;
    cFormVar = "w_ELCONCOD", cQueryName = "ELCONCOD",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 61430154,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=149, Top=210, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CON_TRAM", cZoomOnZoom="GSAR_BCZ", oKey_1_1="COTIPCLF", oKey_1_2="this.w_TIPCON", oKey_2_1="CONUMERO", oKey_2_2="this.w_ELCONCOD"

  func oELCONCOD_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPMOD<>'F'  AND g_GESCON='S')
    endwith
   endif
  endfunc

  func oELCONCOD_2_13.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oELCONCOD_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCONCOD_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCONCOD_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_TRAM_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(this.parent,'oELCONCOD_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BCZ',"Elenco contratti",'GSAG_AEL.CON_TRAM_VZM',this.parent.oContained
  endproc
  proc oELCONCOD_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BCZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.COTIPCLF=w_TIPCON
     i_obj.w_CONUMERO=this.parent.oContained.w_ELCONCOD
     i_obj.ecpSave()
  endproc

  add object oELPREZZO_2_14 as StdField with uid="IMDCHADCCP",rtseq=131,rtrep=.f.,;
    cFormVar = "w_ELPREZZO", cQueryName = "ELPREZZO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Prezzo",;
    HelpContextID = 169683349,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=149, Top=236, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oELPREZZO_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (( .w_TIPMOD<>'F' and !Isahe() ) OR (isAhe() and .w_MOTIPCON='P' ))
    endwith
   endif
  endfunc

  add object oELSCONT1_2_15 as StdField with uid="RIIXFPFQOH",rtseq=132,rtrep=.f.,;
    cFormVar = "w_ELSCONT1", cQueryName = "ELSCONT1",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^Sconto",;
    HelpContextID = 22128265,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=149, Top=262, cSayPict='"999.99"', cGetPict='"999.99"'

  func oELSCONT1_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPMOD<>'F')
    endwith
   endif
  endfunc

  func oELSCONT1_2_15.mHide()
    with this.Parent.oContained
      return (!Isahr())
    endwith
  endfunc

  add object oELSCONT2_2_16 as StdField with uid="CHAQCTSEBK",rtseq=133,rtrep=.f.,;
    cFormVar = "w_ELSCONT2", cQueryName = "ELSCONT2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^Sconto",;
    HelpContextID = 22128264,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=220, Top=262, cSayPict='"999.99"', cGetPict='"999.99"'

  func oELSCONT2_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPMOD<>'F' and .w_ELSCONT1<>0 )
    endwith
   endif
  endfunc

  func oELSCONT2_2_16.mHide()
    with this.Parent.oContained
      return (! Isahr())
    endwith
  endfunc

  add object oELSCONT3_2_17 as StdField with uid="XGEJQUBHQH",rtseq=134,rtrep=.f.,;
    cFormVar = "w_ELSCONT3", cQueryName = "ELSCONT3",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "3^Sconto",;
    HelpContextID = 22128263,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=289, Top=262, cSayPict='"999.99"', cGetPict='"999.99"'

  func oELSCONT3_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPMOD<>'F'  AND .w_ELSCONT2<>0 )
    endwith
   endif
  endfunc

  func oELSCONT3_2_17.mHide()
    with this.Parent.oContained
      return (! Isahr())
    endwith
  endfunc

  add object oELSCONT4_2_18 as StdField with uid="NRCORAOWMW",rtseq=135,rtrep=.f.,;
    cFormVar = "w_ELSCONT4", cQueryName = "ELSCONT4",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "4^Sconto",;
    HelpContextID = 22128262,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=357, Top=262, cSayPict='"999.99"', cGetPict='"999.99"'

  func oELSCONT4_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPMOD<>'F' AND .w_ELSCONT3<>0 )
    endwith
   endif
  endfunc

  func oELSCONT4_2_18.mHide()
    with this.Parent.oContained
      return (! Isahr())
    endwith
  endfunc

  add object oELSCONT1_2_19 as StdField with uid="XXMPTXVADK",rtseq=136,rtrep=.f.,;
    cFormVar = "w_ELSCONT1", cQueryName = "ELSCONT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^Sconto",;
    HelpContextID = 22128265,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=150, Top=262, cSayPict='"999.99"', cGetPict='"999.99"'

  func oELSCONT1_2_19.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oELSCONT2_2_23 as StdField with uid="JWYDOBVQUO",rtseq=137,rtrep=.f.,;
    cFormVar = "w_ELSCONT2", cQueryName = "ELSCONT2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^Sconto",;
    HelpContextID = 22128264,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=220, Top=262, cSayPict='"999.99"', cGetPict='"999.99"'

  func oELSCONT2_2_23.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oELSCONT3_2_24 as StdField with uid="NNFRICYISQ",rtseq=138,rtrep=.f.,;
    cFormVar = "w_ELSCONT3", cQueryName = "ELSCONT3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "3^Sconto",;
    HelpContextID = 22128263,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=289, Top=262, cSayPict='"999.99"', cGetPict='"999.99"'

  func oELSCONT3_2_24.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oELSCONT4_2_25 as StdField with uid="XPRSGEGUZE",rtseq=139,rtrep=.f.,;
    cFormVar = "w_ELSCONT4", cQueryName = "ELSCONT4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "4^Sconto",;
    HelpContextID = 22128262,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=357, Top=262, cSayPict='"999.99"', cGetPict='"999.99"'

  func oELSCONT4_2_25.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oDESUNI_2_26 as StdField with uid="OTWXDJFNTF",rtseq=140,rtrep=.f.,;
    cFormVar = "w_DESUNI", cQueryName = "DESUNI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 162550326,;
   bGlobalFont=.t.,;
    Height=21, Width=373, Left=201, Top=106, InputMask=replicate('X',35)

  add object oDESCTR_2_32 as StdField with uid="YQPVGVIHXJ",rtseq=141,rtrep=.f.,;
    cFormVar = "w_DESCTR", cQueryName = "DESCTR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 218213834,;
   bGlobalFont=.t.,;
    Height=21, Width=305, Left=269, Top=210, InputMask=replicate('X',50)

  func oDESCTR_2_32.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oDESART_2_34 as StdField with uid="UZZJSHYTDU",rtseq=142,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 186887626,;
   bGlobalFont=.t.,;
    Height=21, Width=271, Left=303, Top=25, InputMask=replicate('X',40)

  add object oELVOCCEN_2_36 as StdField with uid="CUDPTRRIDA",rtseq=147,rtrep=.f.,;
    cFormVar = "w_ELVOCCEN", cQueryName = "ELVOCCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice della voce di ricavo",;
    HelpContextID = 218461804,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=151, Top=439, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_ELVOCCEN"

  func oELVOCCEN_2_36.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' or g_COMM='S'))
    endwith
  endfunc

  func oELVOCCEN_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oELVOCCEN_2_36.ecpDrop(oSource)
    this.Parent.oContained.link_2_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELVOCCEN_2_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oELVOCCEN_2_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di ricavo",'',this.parent.oContained
  endproc
  proc oELVOCCEN_2_36.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_ELVOCCEN
     i_obj.ecpSave()
  endproc

  add object oELCODCEN_2_37 as StdField with uid="DJRSGFQLVX",rtseq=148,rtrep=.f.,;
    cFormVar = "w_ELCODCEN", cQueryName = "ELCODCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di ricavo incongruente o obsoleto",;
    ToolTipText = "Codice del centro di ricavo",;
    HelpContextID = 217491052,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=151, Top=468, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ELCODCEN"

  func oELCODCEN_2_37.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oELCODCEN_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODCEN_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODCEN_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oELCODCEN_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di ricavo",'',this.parent.oContained
  endproc
  proc oELCODCEN_2_37.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ELCODCEN
     i_obj.ecpSave()
  endproc

  add object oELCOCOMM_2_38 as StdField with uid="TSPBQYGVAC",rtseq=149,rtrep=.f.,;
    cFormVar = "w_ELCOCOMM", cQueryName = "ELCOCOMM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    ToolTipText = "Codice della commessa associata",;
    HelpContextID = 251222419,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=151, Top=497, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ELCOCOMM"

  func oELCOCOMM_2_38.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc

  func oELCOCOMM_2_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_38('Part',this)
      if .not. empty(.w_ELCODATT)
        bRes2=.link_2_39('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oELCOCOMM_2_38.ecpDrop(oSource)
    this.Parent.oContained.link_2_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCOCOMM_2_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oELCOCOMM_2_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oELCOCOMM_2_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_ELCOCOMM
     i_obj.ecpSave()
  endproc

  add object oELCODATT_2_39 as StdField with uid="BQJIAIYISF",rtseq=150,rtrep=.f.,;
    cFormVar = "w_ELCODATT", cQueryName = "ELCODATT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 251045478,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=151, Top=526, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_ELCOCOMM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_ATTIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_ELCODATT"

  func oELCODATT_2_39.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  func oELCODATT_2_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODATT_2_39.ecpDrop(oSource)
    this.Parent.oContained.link_2_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODATT_2_39.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_ELCOCOMM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_ATTIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_ELCOCOMM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_ATTIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oELCODATT_2_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'',this.parent.oContained
  endproc
  proc oELCODATT_2_39.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_ELCOCOMM
    i_obj.ATTIPATT=w_ATTIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_ELCODATT
     i_obj.ecpSave()
  endproc

  add object oDESVOC_2_46 as StdField with uid="TBESVEIPFD",rtseq=151,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 63001142,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=272, Top=439, InputMask=replicate('X',40)

  func oDESVOC_2_46.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' or g_COMM='S'))
    endwith
  endfunc

  add object oDESCEN_2_47 as StdField with uid="TZYJTUVYJB",rtseq=152,rtrep=.t.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 235819574,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=272, Top=468, InputMask=replicate('X',40)

  func oDESCEN_2_47.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oDESCOMM_2_48 as StdField with uid="XFJRSTNDWN",rtseq=153,rtrep=.f.,;
    cFormVar = "w_DESCOMM", cQueryName = "DESCOMM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 229528118,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=272, Top=497, InputMask=replicate('X',100)

  func oDESCOMM_2_48.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc

  add object oDESATTI_2_49 as StdField with uid="ZEOQLRVGNK",rtseq=154,rtrep=.f.,;
    cFormVar = "w_DESATTI", cQueryName = "DESATTI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 83644982,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=272, Top=526, InputMask=replicate('X',100)

  func oDESATTI_2_49.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oELQTACON_2_60 as StdField with uid="YWQFQQMGRQ",rtseq=170,rtrep=.f.,;
    cFormVar = "w_ELQTACON", cQueryName = "ELQTACON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 48183700,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=149, Top=158, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  func oELQTACON_2_60.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oQTA__RES_2_62 as StdField with uid="OYFEXYAPJW",rtseq=171,rtrep=.f.,;
    cFormVar = "w_QTA__RES", cQueryName = "QTA__RES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 204914087,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=149, Top=183, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  func oQTA__RES_2_62.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oPREZZTOT_2_64 as StdField with uid="KVXBWPMXSK",rtseq=172,rtrep=.f.,;
    cFormVar = "w_PREZZTOT", cQueryName = "PREZZTOT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 91521098,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=435, Top=236, cSayPict="v_PU(38+VVL)", cGetPict="v_GU(38+VVL)"

  func oPREZZTOT_2_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPCON='P' AND .w_TIPMOD<>'F' and !isahe())
    endwith
   endif
  endfunc

  func oPREZZTOT_2_64.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  proc oPREZZTOT_2_64.mAfter
    with this.Parent.oContained
      .Notifyevent('Azzera')
    endwith
  endproc

  add object oELINICOA_2_81 as StdField with uid="SHTDIFLYYQ",rtseq=188,rtrep=.f.,;
    cFormVar = "w_ELINICOA", cQueryName = "ELINICOA",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data inizio competenza attivit�",;
    HelpContextID = 56146311,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=149, Top=326

  func oELINICOA_2_81.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP<>'S' AND NOT EMPTY(.w_ELTIPATT))
    endwith
   endif
  endfunc

  func oELINICOA_2_81.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ELINICOA<=.w_ELFINCOA) or (empty(.w_ELFINCOA)))
    endwith
    return bRes
  endfunc

  add object oELFINCOA_2_82 as StdField with uid="YKKUPCIXTZ",rtseq=189,rtrep=.f.,;
    cFormVar = "w_ELFINCOA", cQueryName = "ELFINCOA",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data fine competenza attivit�",;
    HelpContextID = 61049223,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=255, Top=326

  func oELFINCOA_2_82.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP<>'S' AND NOT EMPTY(.w_ELTIPATT) AND NOT EMPTY(.w_ELINICOA))
    endwith
   endif
  endfunc

  func oELFINCOA_2_82.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ELINICOA<=.w_ELFINCOA) or (empty(.w_ELFINCOA)))
    endwith
    return bRes
  endfunc

  add object oELINICOD_2_83 as StdField with uid="AGPQKNEHRX",rtseq=190,rtrep=.f.,;
    cFormVar = "w_ELINICOD", cQueryName = "ELINICOD",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data inizio competenza documenti",;
    HelpContextID = 56146314,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=151, Top=380

  func oELINICOD_2_83.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(NVL(.w_ELCAUDOC,'')))
    endwith
   endif
  endfunc

  func oELINICOD_2_83.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ELINICOD<=.w_ELFINCOD) or (empty(.w_ELFINCOD)))
    endwith
    return bRes
  endfunc

  add object oELFINCOD_2_84 as StdField with uid="IJQKTMCFYC",rtseq=191,rtrep=.f.,;
    cFormVar = "w_ELFINCOD", cQueryName = "ELFINCOD",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data fine competenza documenti",;
    HelpContextID = 61049226,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=257, Top=380

  func oELFINCOD_2_84.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(NVL(.w_ELCAUDOC,'')) AND NOT EMPTY(.w_ELINICOD))
    endwith
   endif
  endfunc

  func oELFINCOD_2_84.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ELINICOD<=.w_ELFINCOD) or (empty(.w_ELFINCOD)))
    endwith
    return bRes
  endfunc

  add object oStr_2_20 as StdString with uid="MHVTSQPHPB",Visible=.t., Left=38, Top=106,;
    Alignment=1, Width=107, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="NMJKHJPFTF",Visible=.t., Left=38, Top=134,;
    Alignment=1, Width=107, Height=18,;
    Caption="Quantit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_21.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON='P')
    endwith
  endfunc

  add object oStr_2_22 as StdString with uid="KXVKVQGBOR",Visible=.t., Left=40, Top=238,;
    Alignment=1, Width=105, Height=18,;
    Caption="Prezzo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="BTXCUYMVGK",Visible=.t., Left=209, Top=266,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_2_27.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_2_28 as StdString with uid="ZHJAWJZPTZ",Visible=.t., Left=277, Top=266,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_2_28.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_2_29 as StdString with uid="NKPLWNOYFT",Visible=.t., Left=347, Top=266,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_2_29.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_2_30 as StdString with uid="SAFTLWSVCH",Visible=.t., Left=55, Top=264,;
    Alignment=1, Width=90, Height=18,;
    Caption="Sconti/magg.:"  ;
  , bGlobalFont=.t.

  func oStr_2_30.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_2_31 as StdString with uid="BIPHUJKQYI",Visible=.t., Left=18, Top=212,;
    Alignment=1, Width=127, Height=18,;
    Caption="Contratto di vendita:"  ;
  , bGlobalFont=.t.

  func oStr_2_31.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_2_33 as StdString with uid="XUBPFOKMUS",Visible=.t., Left=91, Top=25,;
    Alignment=1, Width=54, Height=18,;
    Caption="Servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="YPYTQEMOYQ",Visible=.t., Left=42, Top=406,;
    Alignment=0, Width=100, Height=19,;
    Caption="Analitica"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_42 as StdString with uid="DKVSLUUGIB",Visible=.t., Left=20, Top=439,;
    Alignment=1, Width=127, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_42.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' or g_COMM='S'))
    endwith
  endfunc

  add object oStr_2_43 as StdString with uid="UHHHGWTMRT",Visible=.t., Left=20, Top=468,;
    Alignment=1, Width=127, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_43.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_2_44 as StdString with uid="DFUFULWPYZ",Visible=.t., Left=20, Top=497,;
    Alignment=1, Width=127, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_44.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc

  add object oStr_2_45 as StdString with uid="TPZUSQZLJW",Visible=.t., Left=20, Top=526,;
    Alignment=1, Width=127, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_45.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oStr_2_59 as StdString with uid="ZVPDNIVAFZ",Visible=.t., Left=38, Top=134,;
    Alignment=1, Width=107, Height=18,;
    Caption="Quantit� totale:"  ;
  , bGlobalFont=.t.

  func oStr_2_59.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oStr_2_61 as StdString with uid="ZNGEITTXVV",Visible=.t., Left=8, Top=159,;
    Alignment=1, Width=137, Height=18,;
    Caption="Quantit� consumata:"  ;
  , bGlobalFont=.t.

  func oStr_2_61.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oStr_2_63 as StdString with uid="PRKGDDZXWO",Visible=.t., Left=8, Top=184,;
    Alignment=1, Width=137, Height=18,;
    Caption="Quantit� residua:"  ;
  , bGlobalFont=.t.

  func oStr_2_63.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oStr_2_65 as StdString with uid="NPRJDERTUV",Visible=.t., Left=325, Top=238,;
    Alignment=1, Width=105, Height=18,;
    Caption="Prezzo totale:"  ;
  , bGlobalFont=.t.

  func oStr_2_65.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oStr_2_79 as StdString with uid="NWIKHRMAGV",Visible=.t., Left=42, Top=298,;
    Alignment=0, Width=158, Height=19,;
    Caption="Date competenza attivit�"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_85 as StdString with uid="BVZFJJLPAP",Visible=.t., Left=123, Top=326,;
    Alignment=1, Width=22, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_2_86 as StdString with uid="OZGCXNDBIP",Visible=.t., Left=236, Top=326,;
    Alignment=1, Width=18, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_2_87 as StdString with uid="NGIFGGTXVQ",Visible=.t., Left=125, Top=383,;
    Alignment=1, Width=22, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_2_88 as StdString with uid="GCPODTESED",Visible=.t., Left=238, Top=383,;
    Alignment=1, Width=18, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_2_89 as StdString with uid="AVFXAJDCUT",Visible=.t., Left=42, Top=352,;
    Alignment=0, Width=182, Height=19,;
    Caption="Date competenza documenti"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_41 as StdBox with uid="COHYAOXIQL",left=31, top=424, width=517,height=1

  add object oBox_2_80 as StdBox with uid="PYXUPMIUOM",left=31, top=319, width=517,height=2

  add object oBox_2_90 as StdBox with uid="BFTZJFFWUL",left=31, top=373, width=517,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_ael','ELE_CONT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ELCODMOD=ELE_CONT.ELCODMOD";
  +" and "+i_cAliasName2+".ELCODIMP=ELE_CONT.ELCODIMP";
  +" and "+i_cAliasName2+".ELCODCOM=ELE_CONT.ELCODCOM";
  +" and "+i_cAliasName2+".ELCONTRA=ELE_CONT.ELCONTRA";
  +" and "+i_cAliasName2+".ELRINNOV=ELE_CONT.ELRINNOV";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_ael
Proc AbilitaBtnRinnovo(pParent)
  local oBtn
  oBtn = pParent.GetCtrl("\<Rinnova")
  if !ISNULL(m.oBtn)
   with pParent
    oBtn.Enabled = NOT EMPTY( .w_ELPERCON ) AND .cFUNCTION = "Query"
   EndWith
  endif
  oBtn = .null.
EndProc
* --- Fine Area Manuale
