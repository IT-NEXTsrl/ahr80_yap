* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bie                                                        *
*              Import eventi                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-01                                                      *
* Last revis.: 2012-05-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bie",oParentObject,m.pOPER)
return(i_retval)

define class tgsfa_bie as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_OLDREC = 0
  w_PADRE = .NULL.
  w_FLERRORS = .f.
  w_ERR_MESS = .NULL.
  w_TETIPEVE = space(10)
  w_TEDESCRI = space(50)
  w_TEDRIVER = space(10)
  w_TECLADOC = space(10)
  w_TETIPDIR = space(1)
  w_DEDESCRI = space(50)
  w_DE__TIPO = space(1)
  w_DEROUTIN = space(20)
  w_CDDESCLA = space(50)
  w_AP__PATH = space(254)
  w_APCODGRU = space(5)
  w_APPERSON = space(5)
  w_IMPORTRES = space(1)
  w_OK = .f.
  * --- WorkFile variables
  TIPEVENT_idx=0
  DRVEVENT_idx=0
  PROMCLAS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = this.oParentObject
    do case
      case this.pOPER=="SELECT_S" OR this.pOPER=="SELECT_D" OR this.pOPER=="SELECT_I"
        if USED(this.oParentObject.w_EVENSELE.cCursor)
          SELECT (this.oParentObject.w_EVENSELE.cCursor)
          this.w_OLDREC = RECNO()
          GO TOP
          UPDATE (this.oParentObject.w_EVENSELE.cCursor) SET XCHK=ICASE(this.pOPER=="SELECT_S", 1, this.pOPER=="SELECT_D", 0, IIF(XCHK=1, 0 ,1))
          GO IIF(this.w_OLDREC<=RECCOUNT(), this.w_OLDREC, 1)
        endif
      case this.pOPER=="IMPORTEVEN"
        this.w_FLERRORS = .F.
        this.oParentObject.w_MSG = ""
        this.w_PADRE.oPGFRM.ActivePage = 2
        addmsgnl("Inizio importazione eventi %1", this.w_PADRE, TTOC(DATETIME()) )
        if USED(this.oParentObject.w_EVENSELE.cCursor)
          SELECT (this.oParentObject.w_EVENSELE.cCursor)
          this.w_OLDREC = RECNO()
          GO TOP
          COUNT FOR XCHK=1 TO this.oParentObject.w_IENUMSEL
          GO IIF(this.w_OLDREC<=RECCOUNT(), this.w_OLDREC, 1)
          this.w_OK = .T.
        endif
        if this.oParentObject.w_IENUMSEL>0 AND this.w_OK
          local L_MACRO, L_OldErr, L_NewErr
          SELECT (this.oParentObject.w_EVENSELE.cCursor)
          GO TOP
          SCAN FOR XCHK=1
          this.w_TETIPEVE = ALLTRIM(NVL(TETIPEVE, " "))
          * --- Lettura informazioni tipo evento
          * --- Read from TIPEVENT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIPEVENT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIPEVENT_idx,2],.t.,this.TIPEVENT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TEDESCRI,TEDRIVER,TECLADOC,TETIPDIR"+;
              " from "+i_cTable+" TIPEVENT where ";
                  +"TETIPEVE = "+cp_ToStrODBC(this.w_TETIPEVE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TEDESCRI,TEDRIVER,TECLADOC,TETIPDIR;
              from (i_cTable) where;
                  TETIPEVE = this.w_TETIPEVE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TEDESCRI = NVL(cp_ToDate(_read_.TEDESCRI),cp_NullValue(_read_.TEDESCRI))
            this.w_TEDRIVER = NVL(cp_ToDate(_read_.TEDRIVER),cp_NullValue(_read_.TEDRIVER))
            this.w_TECLADOC = NVL(cp_ToDate(_read_.TECLADOC),cp_NullValue(_read_.TECLADOC))
            this.w_TETIPDIR = NVL(cp_ToDate(_read_.TETIPDIR),cp_NullValue(_read_.TETIPDIR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Lettura informazioni driver
          * --- Read from DRVEVENT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DRVEVENT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DRVEVENT_idx,2],.t.,this.DRVEVENT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DEDESCRI,DE__TIPO,DEROUTIN"+;
              " from "+i_cTable+" DRVEVENT where ";
                  +"DEDRIVER = "+cp_ToStrODBC(this.w_TEDRIVER);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DEDESCRI,DE__TIPO,DEROUTIN;
              from (i_cTable) where;
                  DEDRIVER = this.w_TEDRIVER;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DEDESCRI = NVL(cp_ToDate(_read_.DEDESCRI),cp_NullValue(_read_.DEDESCRI))
            this.w_DE__TIPO = NVL(cp_ToDate(_read_.DE__TIPO),cp_NullValue(_read_.DE__TIPO))
            this.w_DEROUTIN = NVL(cp_ToDate(_read_.DEROUTIN),cp_NullValue(_read_.DEROUTIN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if !EMPTY(this.w_DEROUTIN)
            * --- Lettura informazioni classe documentale
            * --- Read from PROMCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CDDESCLA"+;
                " from "+i_cTable+" PROMCLAS where ";
                    +"CDCODCLA = "+cp_ToStrODBC(this.w_TECLADOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CDDESCLA;
                from (i_cTable) where;
                    CDCODCLA = this.w_TECLADOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CDDESCLA = NVL(cp_ToDate(_read_.CDDESCLA),cp_NullValue(_read_.CDDESCLA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            addmsgnl("%1Avvio analisi tipologia evento %2 (%3) con driver %4 (%5)", this.w_PADRE, CHR(9), ALLTRIM(this.w_TETIPEVE), ALLTRIM(this.w_TEDESCRI), ALLTRIM(this.w_TEDRIVER), ALLTRIM(this.w_DEDESCRI) )
            addmsgnl("%1Tipo evento %2, classe documentale %3 (%4). Routine utilizzata %5", this.w_PADRE, CHR(9), ah_MsgFormat(ICASE(this.w_DE__TIPO="M", "Mail",this.w_DE__TIPO="T", "Telefonata", this.w_DE__TIPO="F", "Fax", this.w_DE__TIPO="D", "Documento", "Altro" )), ALLTRIM(this.w_TECLADOC), ALLTRIM(this.w_CDDESCLA), ALLTRIM(this.w_DEROUTIN))
            * --- Recupero percorsi da analizzare associati all'evento e persona/Gruppo
            * --- Select from ..\agen\exe\query\gsfa_bie
            do vq_exec with '..\agen\exe\query\gsfa_bie',this,'_Curs__d__d__agen_exe_query_gsfa_bie','',.f.,.t.
            if used('_Curs__d__d__agen_exe_query_gsfa_bie')
              select _Curs__d__d__agen_exe_query_gsfa_bie
              locate for 1=1
              do while not(eof())
              this.w_AP__PATH = ALLTRIM(NVL(AP__PATH, " "))
              this.w_APCODGRU = ALLTRIM(NVL(APCODGRU, " "))
              this.w_APPERSON = ALLTRIM(NVL(APPERSON, " "))
              this.w_ERR_MESS = CREATEOBJECT("ah_ErrorLog")
              this.w_IMPORTRES = ""
              L_MACRO = ALLTRIM(this.w_DEROUTIN)+"( this,'"+this.w_AP__PATH+"', '"+this.w_APPERSON+"', '"+this.w_APCODGRU+"', this.w_ERR_MESS, '"+ALLTRIM(this.w_TETIPEVE)+"')"
              addmsgnl("%1%1Avvio import eventi %2", this.w_PADRE, CHR(9), L_MACRO)
              L_OldErr = ON("ERROR")
              ON ERROR L_NewErr=.T.
              L_NewErr = .F.
              this.w_IMPORTRES = &L_MACRO
              do case
                case !EMPTY(this.w_IMPORTRES) OR this.w_ERR_MESS.isFullLog()
                  addmsgnl("%1%1Riscontrati errori durante l'importazione:", this.w_PADRE, CHR(9))
                  addmsgnl("%1%1%1%2", this.w_PADRE, CHR(9), this.w_ERR_MESS.GetLog())
                  this.w_FLERRORS = .T.
                case L_NewErr
                  addmsgnl("%1%1Riscontrati errori durante l'importazione:", this.w_PADRE, CHR(9))
                  addmsgnl("%1%1%1%2", this.w_PADRE, CHR(9), MESSAGE())
                  this.w_FLERRORS = .T.
              endcase
              ON ERROR &L_OldErr
              this.w_ERR_MESS = .NULL.
                select _Curs__d__d__agen_exe_query_gsfa_bie
                continue
              enddo
              use
            endif
          endif
          SELECT (this.oParentObject.w_EVENSELE.cCursor)
          ENDSCAN
          release L_MACRO, L_OldErr, L_NewErr
        else
          addmsgnl("Nessuna tipologia evento selezionata per l'importazione", this.w_PADRE)
          this.w_FLERRORS = .T.
        endif
        addmsgnl("Fine importazione eventi %1", this.w_PADRE, TTOC(DATETIME()) )
        if this.w_FLERRORS
          this.w_ERR_MESS = CREATEOBJECT("ah_ErrorLog")
          this.w_ERR_MESS.AddMsgLogNoTranslate(this.oParentObject.w_MSG)     
          this.w_ERR_MESS.PrintLog()     
          this.w_ERR_MESS = .NULL.
        endif
      case this.pOPER=="SELECTAUTO"
        if !EMPTY(this.oParentObject.w_IETIPEVE) AND USED(this.oParentObject.w_EVENSELE.cCursor)
          SELECT (this.oParentObject.w_EVENSELE.cCursor)
          this.w_OLDREC = RECNO()
          GO TOP
          UPDATE (this.oParentObject.w_EVENSELE.cCursor) SET XCHK=IIF(TETIPEVE=this.oParentObject.w_IETIPEVE, 1, 0)
          GO IIF(this.w_OLDREC<=RECCOUNT(), this.w_OLDREC, 1)
        endif
        this.pOPER = "SELECTEVEN"
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='TIPEVENT'
    this.cWorkTables[2]='DRVEVENT'
    this.cWorkTables[3]='PROMCLAS'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs__d__d__agen_exe_query_gsfa_bie')
      use in _Curs__d__d__agen_exe_query_gsfa_bie
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
