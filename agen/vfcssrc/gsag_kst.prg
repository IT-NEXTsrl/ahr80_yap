* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kst                                                        *
*              Prestazioni collegate                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-01                                                      *
* Last revis.: 2012-10-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kst",oParentObject))

* --- Class definition
define class tgsag_kst as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 826
  Height = 463
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-26"
  HelpContextID=99231593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_kst"
  cComment = "Prestazioni collegate"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PRSERIAL = space(10)
  w_PRSERAGG = space(10)
  w_CollegaPre = space(1)
  w_PR__DATA = ctod('  /  /  ')
  w_PRNUMPRA = space(15)
  w_NewSerial = space(10)
  w_AGST_ZOOM = .NULL.
  w_AGST1ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kstPag1","gsag_kst",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGST_ZOOM = this.oPgFrm.Pages(1).oPag.AGST_ZOOM
    this.w_AGST1ZOOM = this.oPgFrm.Pages(1).oPag.AGST1ZOOM
    DoDefault()
    proc Destroy()
      this.w_AGST_ZOOM = .NULL.
      this.w_AGST1ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gsag_bpk(this,"C")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PRSERIAL=space(10)
      .w_PRSERAGG=space(10)
      .w_CollegaPre=space(1)
      .w_PR__DATA=ctod("  /  /  ")
      .w_PRNUMPRA=space(15)
      .w_NewSerial=space(10)
      .w_PRSERIAL=oParentObject.w_PRSERIAL
      .w_PRSERAGG=oParentObject.w_PRSERAGG
      .w_CollegaPre=oParentObject.w_CollegaPre
      .w_PR__DATA=oParentObject.w_PR__DATA
      .w_PRNUMPRA=oParentObject.w_PRNUMPRA
      .oPgFrm.Page1.oPag.AGST_ZOOM.Calculate()
      .oPgFrm.Page1.oPag.AGST1ZOOM.Calculate()
          .DoRTCalc(1,5,.f.)
        .w_NewSerial = .w_AGST1ZOOM.GetVar('PRSERIAL')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PRSERIAL=.w_PRSERIAL
      .oParentObject.w_PRSERAGG=.w_PRSERAGG
      .oParentObject.w_CollegaPre=.w_CollegaPre
      .oParentObject.w_PR__DATA=.w_PR__DATA
      .oParentObject.w_PRNUMPRA=.w_PRNUMPRA
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.AGST_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.AGST1ZOOM.Calculate()
        .DoRTCalc(1,5,.t.)
            .w_NewSerial = .w_AGST1ZOOM.GetVar('PRSERIAL')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AGST_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.AGST1ZOOM.Calculate()
    endwith
  return

  proc Calculate_TYCCCLJUCC()
    with this
          * --- VisibilitÓ zoom
          .w_AGST_ZOOM.Visible = IIF(.w_CollegaPre='S',.F.,.T.)
          .w_AGST1ZOOM.Visible = IIF(.w_CollegaPre='S',.T.,.F.)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AGST_ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.AGST1ZOOM.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_TYCCCLJUCC()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kst
    if Upper(CEVENT)=UPPER('w_AGST1ZOOM SELECTED') and This.oparentobject.w_CollegaPre='S'
       This.Ecpsave()
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_kstPag1 as StdContainer
  Width  = 822
  height = 463
  stdWidth  = 822
  stdheight = 463
  resizeXpos=716
  resizeYpos=336
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object AGST_ZOOM as cp_zoombox with uid="VWNKEVNLLO",left=3, top=12, width=813,height=391,;
    caption='AGST_ZOOM',;
   bGlobalFont=.t.,;
    cMenuFile="",bRetriveAllRows=.f.,bNoZoomGridShape=.f.,cTable="PRE_STAZ",bQueryOnDblClick=.f.,cZoomOnZoom="",cZoomFile="GSAG_KST",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.t.,bReadOnly=.t.,;
    nPag=1;
    , HelpContextID = 174020901


  add object oBtn_1_4 as StdButton with uid="TKCMXZXILU",left=768, top=413, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91914170;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AGST1ZOOM as cp_zoombox with uid="ACVHHQXVMD",left=3, top=12, width=813,height=391,;
    caption='AGST1ZOOM',;
   bGlobalFont=.t.,;
    cMenuFile="",cTable="PRE_STAZ",cZoomFile="GSAG1KST",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.f.,bQueryOnLoad=.t.,bReadOnly=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,;
    nPag=1;
    , HelpContextID = 125786405
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kst','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
