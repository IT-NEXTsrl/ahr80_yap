* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bru                                                        *
*              Rubrica                                                         *
*                                                                              *
*      Author: Zucchetti TAM S.p.a.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-20                                                      *
* Last revis.: 2014-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam,pParam2,pParam3
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bru",oParentObject,m.pParam,m.pParam2,m.pParam3)
return(i_retval)

define class tgsag_bru as StdBatch
  * --- Local variables
  pParam = space(5)
  pParam2 = space(5)
  pParam3 = space(5)
  ind = 0
  Descrizione = space(30)
  w_OBJECT = .NULL.
  w_NOCODCLI = space(15)
  w_RETVAL = 0
  w_OBJ = .NULL.
  w_NOTIPNOM = space(1)
  w_PADRE = .NULL.
  * --- WorkFile variables
  RIS_INTE_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pParam: operazione, pParam2: prima lettera da ricercare, pParam3: successiva stringa da ricercare
    * --- Variabili per visualizzazione
    * --- Dichiarazione var. locali
    this.w_RETVAL = 0
    do case
      case this.pParam="RICERCA" OR this.pParam="RAGSOC"
        if this.pParam="RICERCA" OR (this.pParam="RAGSOC" AND !EMPTY(this.oParentObject.w_RAGSOC))
          * --- Ricalcoliamo dopo l'uscita da RAGSOC solo se � stato inserito un valore, altrimenti significa che siamo solo passati per il campo
          * --- ClearAll
          this.oParentObject.w_ctRubrica.ClearAll()     
          * --- Aggiunge tutte le varie colonne
          this.oParentObject.w_ClnRagSoc = 0
          this.oParentObject.w_ClnSocieta = 0
          this.oParentObject.w_ClnIndiri = 0
          this.oParentObject.w_ClnTelefono = 0
          this.oParentObject.w_ClnFax = 0
          this.oParentObject.w_ClnCellulare = 0
          this.oParentObject.w_ClnEmail = 0
          this.oParentObject.w_ClnEmPEC = 0
          this.oParentObject.w_ClnSito = 0
          this.oParentObject.w_ClnLocali = 0
          this.oParentObject.w_ClnTelefo2 = 0
          * --- Prima riga: descrizione se ordiniamo per codice, codice se ordiniamo per descrizione
          * --- w_OrderBy='1'
          this.oParentObject.w_ClnSocieta = this.oParentObject.w_ctRubrica.AddTitle(AH_MSGFORMAT("Soc.:"))
          if this.oParentObject.w_FLIndirizzo="S"
            this.oParentObject.w_ClnIndiri = this.oParentObject.w_ctRubrica.AddTitle(AH_MSGFORMAT("Indir:"))
          endif
          if this.oParentObject.w_FLLocali="S"
            this.oParentObject.w_ClnLocali = this.oParentObject.w_ctRubrica.AddTitle(AH_MSGFORMAT("Loc:"))
          endif
          if this.oParentObject.w_FLTelefono="S"
            this.oParentObject.w_ClnTelefono = this.oParentObject.w_ctRubrica.AddTitle(AH_MSGFORMAT("Tel:"))
          endif
          if this.oParentObject.w_FLCellulare="S"
            this.oParentObject.w_ClnCellulare = this.oParentObject.w_ctRubrica.AddTitle(AH_MSGFORMAT("Cell:"))
          endif
          if this.oParentObject.w_FLFax="S"
            this.oParentObject.w_ClnFax = this.oParentObject.w_ctRubrica.AddTitle(AH_MSGFORMAT("Fax:"))
          endif
          if this.oParentObject.w_FLEmail="S"
            this.oParentObject.w_ClnEmail = this.oParentObject.w_ctRubrica.AddTitle(AH_MSGFORMAT("Mail:"))
          endif
          if this.oParentObject.w_FLEmPEC="S"
            this.oParentObject.w_ClnEmPEC = this.oParentObject.w_ctRubrica.AddTitle(AH_MSGFORMAT("PEC:"))
          endif
          if this.oParentObject.w_FLSito="S"
            this.oParentObject.w_ClnSito = this.oParentObject.w_ctRubrica.AddTitle(AH_MSGFORMAT("Sito:"))
          endif
          if this.oParentObject.w_FLTelefono="S"
            this.oParentObject.w_ClnTelefo2 = this.oParentObject.w_ctRubrica.AddTitle(AH_MSGFORMAT("Tel2:"))
          endif
          * --- Aggiungiamo i nominativi
          * --- Select from QUERY\GSAG_KRU.VQR
          do vq_exec with 'QUERY\GSAG_KRU.VQR',this,'_Curs_QUERY_GSAG_KRU_d_VQR','',.f.,.t.
          if used('_Curs_QUERY_GSAG_KRU_d_VQR')
            select _Curs_QUERY_GSAG_KRU_d_VQR
            locate for 1=1
            do while not(eof())
            this.ind = 0
            if !EMPTY(NVL(RagSoc,""))
              this.Descrizione = ALLTRIM(RagSoc)
            else
              this.Descrizione = ""
            endif
            * --- Ordina per descrizione, quindi deve aggiungere la riga di codice
            this.ind = this.oParentObject.w_ctRubrica.AddHeader(this.Descrizione, NOCODICE)
            this.oParentObject.w_ctRubrica.ItemText(this.ind, this.oParentObject.w_ClnSocieta, ALLTRIM(NVL(NOSOCIETA,"")))     
            this.Descrizione = ALLTRIM( ALLTRIM(NVL(NO___CAP,""))+" "+ALLTRIM(NVL(NOLOCALI,""))+IIF(!EMPTY(NVL(NOPROVIN,"")),", "+NOPROVIN,""))
            this.oParentObject.w_ctRubrica.ItemText(this.ind, this.oParentObject.w_ClnIndiri, ALLTRIM(NVL(NOINDIRI,"")))     
            this.oParentObject.w_ctRubrica.ItemText(this.ind, this.oParentObject.w_ClnLocali, this.Descrizione)     
            this.oParentObject.w_ctRubrica.ItemText(this.ind, this.oParentObject.w_ClnTelefono, ALLTRIM(NVL(NOTELEFO,"")))     
            this.oParentObject.w_ctRubrica.ItemText(this.ind, this.oParentObject.w_ClnFax, ALLTRIM(NVL(NOTELFAX,"")))     
            this.oParentObject.w_ctRubrica.ItemText(this.ind, this.oParentObject.w_ClnCellulare, ALLTRIM(NVL(NONUMCEL,"")))     
            this.oParentObject.w_ctRubrica.ItemText(this.ind, this.oParentObject.w_ClnEmail, ALLTRIM(NVL(NO_EMAIL,"")))     
            this.oParentObject.w_ctRubrica.ItemText(this.ind, this.oParentObject.w_ClnEmPEC, ALLTRIM(NVL(NO_EMPEC,"")))     
            this.oParentObject.w_ctRubrica.ItemText(this.ind, this.oParentObject.w_ClnSito, ALLTRIM(NVL(NOINDWEB,"")))     
            this.oParentObject.w_ctRubrica.ItemText(this.ind, this.oParentObject.w_ClnTelefo2, ALLTRIM(NVL(NCTELEF2,"")))     
              select _Curs_QUERY_GSAG_KRU_d_VQR
              continue
            enddo
            use
          endif
          this.oParentObject.w_PartiDa = ""
          if !oParentObject.oPgFrm.ActivePage=1
            oParentObject.oPgFrm.ActivePage = 1
          endif
        endif
      case this.pParam="PARTIDA"
        this.oParentObject.w_ctRubrica.FindVal(this.oParentObject.w_PartiDa)     
      case this.pParam="INGRANDISCI"
        this.oParentObject.w_DimScheda = 0
        this.oParentObject.w_ctRubrica.Top = this.oParentObject.w_ctRubrica.Top-70
        this.oParentObject.w_ctRubrica.Height = this.oParentObject.w_ctRubrica.Height+70
      case this.pParam="RIDUCI"
        this.oParentObject.w_DimScheda = 1
        this.oParentObject.w_ctRubrica.Top = this.oParentObject.w_ctRubrica.Top+70
        this.oParentObject.w_ctRubrica.Height = this.oParentObject.w_ctRubrica.Height-70
      case this.pParam="MODIFICA"
        * --- Modifica di un cilente o un nominativo
        do case
          case !EMPTY(this.oParentObject.w_Codice)
            * --- Doppio click su un nominativo
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case !EMPTY(this.oParentObject.w_CodCliente)
            * --- Tasto destro su un nominativo
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
      case this.pParam="CERCA"
        this.oParentObject.w_ctRubrica.FindVal(this.pParam2, this.pParam3)     
      case this.pParam="PulisciFiltri"
        * --- Pulisce gli eventuali filtri presenti in maschera
        if !EMPTY(this.oParentObject.w_PartiDa)
          this.oParentObject.w_PartiDa = ""
        endif
        if !EMPTY(this.oParentObject.w_RAGSOC)
          this.oParentObject.w_RAGSOC = ""
        endif
        this.oParentObject.w_FLDenom = "N"
        this.oParentObject.w_FLRiferi = "N"
        this.oParentObject.w_FLEml = "N"
        this.oParentObject.w_FLCoEml = "N"
        this.oParentObject.w_FLTele = "N"
        this.oParentObject.w_FLCotele = "N"
        oParentObject.NotifyEvent("Ricerca")
      case this.pParam="BOTTONE"
        if !EMPTY(this.oParentObject.w_PartiDa)
          if VAL(LEFT(this.oParentObject.w_PartiDa,1))=0
            this.w_OBJ = this.oParentObject.GetCtrl(UPPER(LEFT(this.oParentObject.w_PartiDa,1)))
          else
            this.w_OBJ = this.oParentObject.GetCtrl("0")
          endif
          this.w_OBJ.SetFocus()     
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Instanzio l'anagrafica nominativi
    this.w_OBJECT = GSAR_ANO()
    * --- Controllo se ha passato il test di accesso
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- Carico il record selezionato
    this.w_OBJECT.ECPFILTER()     
    if this.w_RetVal>0
      * --- Dal men� contestuale si � scelto di visualizzare/modificare il nominativo
      this.w_OBJECT.w_NOCODICE = this.oParentObject.w_CodCliente
    else
      this.w_OBJECT.w_NOCODICE = this.oParentObject.w_Codice
    endif
    this.w_OBJECT.ECPSAVE()     
    if this.w_RetVal=2
      * --- Dal men� contestuale si � scelto di modificare il nominativo
      this.w_OBJECT.ECPEDIT()     
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- E' stato premuto il tasto destro su un elemento della rubrica
    * --- Prima di tutto dobbiamo controllare se il record selezionato � relativo solo ad un nominativo oppure � un cliente
    * --- Read from OFF_NOMI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "NOCODCLI,NOTIPNOM"+;
        " from "+i_cTable+" OFF_NOMI where ";
            +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_CodCliente);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        NOCODCLI,NOTIPNOM;
        from (i_cTable) where;
            NOCODICE = this.oParentObject.w_CodCliente;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NOCODCLI = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
      this.w_NOTIPNOM = NVL(cp_ToDate(_read_.NOTIPNOM),cp_NullValue(_read_.NOTIPNOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PADRE = this.oParentObject
    if i_VisualTheme = -1
      L_RETVAL=0
      DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT
      DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Apri nominativo")
      DEFINE BAR 2 OF popCmd PROMPT ah_MsgFormat("Modifica nominativo")
      ON SELECTION BAR 1 OF popCmd L_RETVAL = 1
      ON SELECTION BAR 2 OF popCmd L_RETVAL = 2
      if !EMPTY(this.w_NOCODCLI)
        if this.w_NOTIPNOM="C"
          DEFINE BAR 3 OF popCmd PROMPT ah_MsgFormat("Apri cliente") SKIP FOR EMPTY(this.w_NOCODCLI)
          DEFINE BAR 4 OF popCmd PROMPT ah_MsgFormat("Modifica cliente") SKIP FOR EMPTY(this.w_NOCODCLI)
        else
          DEFINE BAR 3 OF popCmd PROMPT ah_MsgFormat("Apri fornitore") SKIP FOR EMPTY(this.w_NOCODCLI)
          DEFINE BAR 4 OF popCmd PROMPT ah_MsgFormat("Modifica fornitore") SKIP FOR EMPTY(this.w_NOCODCLI)
        endif
        ON SELECTION BAR 3 OF popCmd L_RETVAL = 3
        ON SELECTION BAR 4 OF popCmd L_RETVAL = 4
      endif
      ACTIVATE POPUP popCmd
      DEACTIVATE POPUP popCmd
      RELEASE POPUPS popCmd
      this.w_RETVAL = L_RETVAL
      Release L_RETVAL
    else
      local oBtnMenu, oMenuItem
      oBtnMenu = CreateObject("cbPopupMenu")
      oBtnMenu.oParentObject = This
      oBtnMenu.oPopupMenu.cOnClickProc="ExecScript(NAMEPROC)"
      oMenuItem = oBtnMenu.AddMenuItem()
      oMenuItem.Caption = Ah_Msgformat("Apri nominativo")
      oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=1"
      oMenuItem.Visible=.T.
      oMenuItem = oBtnMenu.AddMenuItem()
      oMenuItem.Caption = Ah_Msgformat("Modifica nominativo")
      oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=2"
      oMenuItem.Visible=.T.
      if !EMPTY(this.w_NOCODCLI)
        oMenuItem = oBtnMenu.AddMenuItem()
        if this.w_NOTIPNOM="C"
          oMenuItem.Caption = Ah_Msgformat("Apri cliente")
        else
          oMenuItem.Caption = Ah_Msgformat("Apri fornitore")
        endif
        oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=3"
        oMenuItem.Visible=.T.
        oMenuItem = oBtnMenu.AddMenuItem()
        if this.w_NOTIPNOM="C"
          oMenuItem.Caption = Ah_Msgformat("Modifica cliente")
        else
          oMenuItem.Caption = Ah_Msgformat("Modifica fornitore")
        endif
        oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=4"
        oMenuItem.Visible=.T.
      endif
      oBtnMenu.InitMenu()
      oBtnMenu.ShowMenu()
      oBtnMenu = .NULL.
    endif
    do case
      case this.w_RETVAL = 1 OR this.w_RETVAL = 2
        * --- Visualizza/modifica il nominativo
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_RETVAL = 3 OR this.w_RETVAL = 4
        * --- Visualizza/modifica il cliente
        GSAR_BO2(this, IIF(this.w_RETVAL=3,"C","M"), this.w_NOCODCLI, IIF(this.w_NOTIPNOM="C","C","F"))
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParam,pParam2,pParam3)
    this.pParam=pParam
    this.pParam2=pParam2
    this.pParam3=pParam3
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='RIS_INTE'
    this.cWorkTables[2]='OFF_NOMI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_QUERY_GSAG_KRU_d_VQR')
      use in _Curs_QUERY_GSAG_KRU_d_VQR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam,pParam2,pParam3"
endproc
