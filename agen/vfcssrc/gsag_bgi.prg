* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bgi                                                        *
*              Verifiche gestioni impianti                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-12-21                                                      *
* Last revis.: 2010-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,w_CODIMP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bgi",oParentObject,m.pOPER,m.w_CODIMP)
return(i_retval)

define class tgsag_bgi as StdBatch
  * --- Local variables
  pOPER = space(5)
  w_CODIMP = space(10)
  w_PADRE = .NULL.
  w_RESULT = .f.
  w_CONTRA = space(10)
  w_CONTCLI = space(15)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CON_TRAS_idx=0
  COM_ATTI_idx=0
  IMP_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_RESULT = .T.
    this.w_PADRE = this.oParentObject
    do case
      case this.pOPER=="CKCON"
        if this.oParentObject.w_TESTMCLI
          CREATE CURSOR CURS_CONTRA (ELCONTRA C(10))
          this.w_PADRE.GSAG_MEC.LinkPCClick(.T.)     
          this.w_PADRE.GSAG_MEC.Cnt.FirstRow()     
          do while !this.w_PADRE.GSAG_MEC.Cnt.Eof_Trs()
            * --- Scorro le righe piene, cancellate (se non in salvataggio), o aggiunte e modificate
            *     se non cancellate
            if this.w_PADRE.GSAG_MEC.Cnt.FullRow()
              this.w_PADRE.GSAG_MEC.Cnt.SetRow()     
              this.w_CONTRA = this.w_PADRE.GSAG_MEC.cnt.w_ELCONTRA
              Select CURS_CONTRA 
 LOCATE FOR ELCONTRA=this.w_CONTRA
              if NOT FOUND( )
                * --- Read from CON_TRAS
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CON_TRAS_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAS_idx,2],.t.,this.CON_TRAS_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "COCODCON"+;
                    " from "+i_cTable+" CON_TRAS where ";
                        +"COSERIAL = "+cp_ToStrODBC(this.w_CONTRA);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    COCODCON;
                    from (i_cTable) where;
                        COSERIAL = this.w_CONTRA;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CONTCLI = NVL(cp_ToDate(_read_.COCODCON),cp_NullValue(_read_.COCODCON))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.oParentObject.w_IMCODCON<>this.w_CONTCLI
                  this.w_RESULT = .F.
                  EXIT
                else
                  INSERT INTO CURS_CONTRA (ELCONTRA) VALUES (this.w_CONTRA)
                endif
              endif
            endif
            this.w_PADRE.GSAG_MEC.Cnt.NextRow()     
          enddo
          this.w_PADRE.GSAG_MEC.LinkPCClick()     
          if this.w_RESULT
            this.w_PADRE.Markpos()     
            this.w_PADRE.FirstRow()     
            do while !this.w_PADRE.Eof_Trs() AND this.w_RESULT
              if this.w_PADRE.FullRow()
                this.w_PADRE.SetRow()     
                this.w_PADRE.ChildrenChangeRow()     
                if this.w_PADRE.FullRow()
                  this.w_PADRE.GSAG_MDE.LinkPCClick(.T.)     
                  this.w_PADRE.GSAG_MDE.Cnt.FirstRow()     
                  do while !this.w_PADRE.GSAG_MDE.Cnt.Eof_Trs()
                    * --- Scorro le righe piene, cancellate (se non in salvataggio), o aggiunte e modificate
                    *     se non cancellate
                    if this.w_PADRE.GSAG_MDE.Cnt.FullRow()
                      this.w_PADRE.GSAG_MDE.Cnt.SetRow()     
                      this.w_CONTRA = this.w_PADRE.GSAG_MDE.cnt.w_ELCONTRA
                      Select CURS_CONTRA 
 LOCATE FOR ELCONTRA=this.w_CONTRA
                      if NOT FOUND( )
                        * --- Read from CON_TRAS
                        i_nOldArea=select()
                        if used('_read_')
                          select _read_
                          use
                        endif
                        i_nConn=i_TableProp[this.CON_TRAS_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAS_idx,2],.t.,this.CON_TRAS_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select "+;
                            "COCODCON"+;
                            " from "+i_cTable+" CON_TRAS where ";
                                +"COSERIAL = "+cp_ToStrODBC(this.w_CONTRA);
                                 ,"_read_")
                          i_Rows=iif(used('_read_'),reccount(),0)
                        else
                          select;
                            COCODCON;
                            from (i_cTable) where;
                                COSERIAL = this.w_CONTRA;
                             into cursor _read_
                          i_Rows=_tally
                        endif
                        if used('_read_')
                          locate for 1=1
                          this.w_CONTCLI = NVL(cp_ToDate(_read_.COCODCON),cp_NullValue(_read_.COCODCON))
                          use
                        else
                          * --- Error: sql sentence error.
                          i_Error = MSG_READ_ERROR
                          return
                        endif
                        select (i_nOldArea)
                        if this.oParentObject.w_IMCODCON<>this.w_CONTCLI
                          this.w_RESULT = .F.
                          EXIT
                        else
                          INSERT INTO CURS_CONTRA (ELCONTRA) VALUES (this.w_CONTRA)
                        endif
                      endif
                    endif
                    this.w_PADRE.GSAG_MDE.Cnt.NextRow()     
                  enddo
                  this.w_PADRE.GSAG_MDE.LinkPCClick()     
                endif
              endif
              this.w_PADRE.NextRow()     
            enddo
            this.w_PADRE.RePos()     
            this.w_PADRE.ChildrenChangeRow()     
          endif
          if this.w_RESULT
            * --- Read from COM_ATTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.COM_ATTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COM_ATTI_idx,2],.t.,this.COM_ATTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" COM_ATTI where ";
                    +"CACODIMP = "+cp_ToStrODBC(this.oParentObject.w_IMCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    CACODIMP = this.oParentObject.w_IMCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_Rows>0
              this.w_RESULT = .F.
            endif
          endif
          Use in Select ("CURS_CONTRA")
          if !this.w_RESULT
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=AH_Msgformat("Impossibile modificare l'instestatario! %0Uno o pi� elementi contratto e/o una o pi� attivit� fanno riferimento all'impianto che si sta tentando di modificare")
          endif
        endif
        this.bUpdateparentObject=.F.
      case this.pOPER=="CKIMP"
        * --- Read from IMP_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.IMP_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.IMP_MAST_idx,2],.t.,this.IMP_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" IMP_MAST where ";
                +"IMCODICE = "+cp_ToStrODBC(this.w_CODIMP);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                IMCODICE = this.w_CODIMP;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          this.w_RESULT = .F.
        endif
    endcase
    i_retcode = 'stop'
    i_retval = this.w_RESULT
    return
  endproc


  proc Init(oParentObject,pOPER,w_CODIMP)
    this.pOPER=pOPER
    this.w_CODIMP=w_CODIMP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CON_TRAS'
    this.cWorkTables[3]='COM_ATTI'
    this.cWorkTables[4]='IMP_MAST'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,w_CODIMP"
endproc
