* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_krc                                                        *
*              Rinvio                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-20                                                      *
* Last revis.: 2010-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_krc",oParentObject))

* --- Class definition
define class tgsag_krc as StdForm
  Top    = 12
  Left   = 164

  * --- Standard Properties
  Width  = 343
  Height = 113
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-12-01"
  HelpContextID=116008809
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  PAR_AGEN_IDX = 0
  cPrg = "gsag_krc"
  cComment = "Rinvio"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PAR_AGEN = space(5)
  w_DATARINV = ctod('  /  /  ')
  o_DATARINV = ctod('  /  /  ')
  w_ORERINV = space(2)
  w_MINRINV = space(2)
  w_CONFERMA = space(1)
  w_SCRIVINOTE = space(1)
  w_RESCHK = 0
  w_GIORINV = space(3)
  w_PACHKFES = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_krcPag1","gsag_krc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATARINV_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PAR_AGEN'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PAR_AGEN=space(5)
      .w_DATARINV=ctod("  /  /  ")
      .w_ORERINV=space(2)
      .w_MINRINV=space(2)
      .w_CONFERMA=space(1)
      .w_SCRIVINOTE=space(1)
      .w_RESCHK=0
      .w_GIORINV=space(3)
      .w_PACHKFES=space(1)
      .w_DATARINV=oParentObject.w_DATARINV
      .w_ORERINV=oParentObject.w_ORERINV
      .w_MINRINV=oParentObject.w_MINRINV
      .w_CONFERMA=oParentObject.w_CONFERMA
      .w_SCRIVINOTE=oParentObject.w_SCRIVINOTE
        .w_PAR_AGEN = i_CodAzi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PAR_AGEN))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,6,.f.)
        .w_RESCHK = 0
        .w_GIORINV = iif(!EMPTY(.w_DATARINV), g_GIORNO[DOW(.w_DATARINV)] , SPACE(3))
    endwith
    this.DoRTCalc(9,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATARINV=.w_DATARINV
      .oParentObject.w_ORERINV=.w_ORERINV
      .oParentObject.w_MINRINV=.w_MINRINV
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.w_SCRIVINOTE=.w_SCRIVINOTE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,7,.t.)
        if .o_DATARINV<>.w_DATARINV
            .w_GIORINV = iif(!EMPTY(.w_DATARINV), g_GIORNO[DOW(.w_DATARINV)] , SPACE(3))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_GDOOYMSXLP()
    with this
          * --- Controlli a checkform
          GSAG_BCK(this;
              ,'C';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_GDOOYMSXLP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PAR_AGEN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAR_AGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAR_AGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACHKFES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PAR_AGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PAR_AGEN)
            select PACODAZI,PACHKFES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAR_AGEN = NVL(_Link_.PACODAZI,space(5))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PAR_AGEN = space(5)
      endif
      this.w_PACHKFES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAR_AGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATARINV_1_3.value==this.w_DATARINV)
      this.oPgFrm.Page1.oPag.oDATARINV_1_3.value=this.w_DATARINV
    endif
    if not(this.oPgFrm.Page1.oPag.oORERINV_1_5.value==this.w_ORERINV)
      this.oPgFrm.Page1.oPag.oORERINV_1_5.value=this.w_ORERINV
    endif
    if not(this.oPgFrm.Page1.oPag.oMINRINV_1_6.value==this.w_MINRINV)
      this.oPgFrm.Page1.oPag.oMINRINV_1_6.value=this.w_MINRINV
    endif
    if not(this.oPgFrm.Page1.oPag.oSCRIVINOTE_1_11.RadioValue()==this.w_SCRIVINOTE)
      this.oPgFrm.Page1.oPag.oSCRIVINOTE_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGIORINV_1_14.value==this.w_GIORINV)
      this.oPgFrm.Page1.oPag.oGIORINV_1_14.value=this.w_GIORINV
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DATARINV)) or not(.w_PACHKFES='N' OR Chkfestivi(.w_DATARINV, .w_PACHKFES)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATARINV_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATARINV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_ORERINV) < 24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORERINV_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINRINV) < 60)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINRINV_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_krc
      IF i_bRes=.t.
          .w_RESCHK=0
          .NotifyEvent('ChkFinali')
          WAIT CLEAR
          IF .w_RESCHK<>0
            i_bRes=.f.
          ENDIF
          IF i_bRes=.t.
            .w_CONFERMA='S'
          ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATARINV = this.w_DATARINV
    return

enddefine

* --- Define pages as container
define class tgsag_krcPag1 as StdContainer
  Width  = 339
  height = 113
  stdWidth  = 339
  stdheight = 113
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATARINV_1_3 as StdField with uid="MYGDNXHRTP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATARINV", cQueryName = "DATARINV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data rinvio",;
    HelpContextID = 142844276,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=110, Top=18

  func oDATARINV_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PACHKFES='N' OR Chkfestivi(.w_DATARINV, .w_PACHKFES))
    endwith
    return bRes
  endfunc

  add object oORERINV_1_5 as StdField with uid="VGJIHEDZAE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ORERINV", cQueryName = "ORERINV",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora rinvio",;
    HelpContextID = 201097190,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=263, Top=18, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORERINV_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORERINV) < 24)
    endwith
    return bRes
  endfunc

  add object oMINRINV_1_6 as StdField with uid="NXNGNCXJWL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MINRINV", cQueryName = "MINRINV",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti rinvio",;
    HelpContextID = 201131718,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=305, Top=18, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINRINV_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINRINV) < 60)
    endwith
    return bRes
  endfunc


  add object oBtn_1_9 as StdButton with uid="ERHOXVEEGI",left=231, top=63, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 115980058;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="TKCMXZXILU",left=284, top=63, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 108691386;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSCRIVINOTE_1_11 as StdCheck with uid="MZWMGVLOMC",rtseq=6,rtrep=.f.,left=19, top=56, caption="Mantieni le note",;
    ToolTipText = "Se attivo: nelle attivit� rinviate verranno mantenute le note",;
    HelpContextID = 138114123,;
    cFormVar="w_SCRIVINOTE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSCRIVINOTE_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSCRIVINOTE_1_11.GetRadio()
    this.Parent.oContained.w_SCRIVINOTE = this.RadioValue()
    return .t.
  endfunc

  func oSCRIVINOTE_1_11.SetRadio()
    this.Parent.oContained.w_SCRIVINOTE=trim(this.Parent.oContained.w_SCRIVINOTE)
    this.value = ;
      iif(this.Parent.oContained.w_SCRIVINOTE=='S',1,;
      0)
  endfunc

  add object oGIORINV_1_14 as StdField with uid="DPWOUXIATY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_GIORINV", cQueryName = "GIORINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 201135718,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=71, Top=18, InputMask=replicate('X',3)

  add object oStr_1_2 as StdString with uid="CDWWSMMEYG",Visible=.t., Left=12, Top=19,;
    Alignment=1, Width=56, Height=18,;
    Caption="Rinvio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="BYZPFCFPKB",Visible=.t., Left=198, Top=20,;
    Alignment=1, Width=60, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="FXUDOJAZEE",Visible=.t., Left=291, Top=20,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_krc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
