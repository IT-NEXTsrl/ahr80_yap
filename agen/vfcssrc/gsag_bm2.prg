* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bm2                                                        *
*              Aggiorna listini di riga                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-02-06                                                      *
* Last revis.: 2012-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bm2",oParentObject,m.pEXEC)
return(i_retval)

define class tgsag_bm2 as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_MVTCOLIS = space(5)
  w_FLINTE = space(1)
  w_AGG = 0
  w_GESTIONE = .NULL.
  w_IVALIST = space(1)
  w_LCODCLI = space(15)
  w_LTIPCLI = space(1)
  w_COND = .f.
  w_CAUATT = space(20)
  w_CODNOM = space(15)
  w_RICALCOLA = .f.
  w_VALLIST = space(3)
  w_IVALISTP = space(1)
  w_VALLIST2 = space(3)
  w_VALLIST3 = space(3)
  w_VALLIST4 = space(3)
  w_MESS = space(100)
  w_PADRE = .NULL.
  * --- WorkFile variables
  LISTINI_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Isahe() OR (Isahr() and this.pEXEC="H")
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDFLVEAC,TDFLGLIS,TDFLINTE"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_ATCAUDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDFLVEAC,TDFLGLIS,TDFLINTE;
          from (i_cTable) where;
              TDTIPDOC = this.oParentObject.w_ATCAUDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
        this.oParentObject.w_FLGLIS = NVL(cp_ToDate(_read_.TDFLGLIS),cp_NullValue(_read_.TDFLGLIS))
        this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do case
        case this.pEXEC="A"
          * --- Controllo se il codice inserito � corretto
          this.w_GESTIONE = This.oparentobject.oparentobject
          if Empty(this.oParentObject.w_ATTCOLIS)
            this.w_AGG = 1
            this.oParentObject.w_ATTPROLI = SPACE(5)
            if Empty(this.oParentObject.w_ATTSCLIS)
              this.oParentObject.w_ATTPROSC = SPACE(5)
            endif
            this.w_MESS = "Elimino i listini prezzo presenti sulle righe?"
          else
            * --- Read from LISTINI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.LISTINI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" LISTINI where ";
                    +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.w_ATTCOLIS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    LSCODLIS = this.oParentObject.w_ATTCOLIS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_Rows > 0
              this.oParentObject.w_ATTPROLI = SPACE(5)
              this.oParentObject.w_ATTPROSC = SPACE(5)
              DECLARE ARRCALC (4,1)
              w_RESULT=CALCLIST("MODI",@ARRCALC,this.oParentObject.w_MVFLVEAC," ",this.oParentObject.w_ATCODVAL,this.oParentObject.w_ATTIPCLI,this.oParentObject.w_CODCLI,this.oParentObject.w_DATALIST,this.oParentObject.w_ATTCOLIS,this.oParentObject.w_ATTSCLIS,this.oParentObject.w_KEYLISTB,-20,this.oParentObject.w_FLSCOR,this.w_IVALIST,this.oParentObject.w_ATCAUDOC)
              * --- Aggiorno solo eventuali nuove Promozioni
              this.oParentObject.w_ATTPROLI = ARRCALC(3)
              this.oParentObject.w_ATTPROSC = ARRCALC(4)
              this.w_AGG = 1
              this.w_MESS = "Aggiorno i listini presenti sulle righe?"
            endif
          endif
        case this.pEXEC="B"
          * --- Controllo se il codice inserito � corretto
          this.w_GESTIONE = This.oparentobject.oparentobject
          if Empty(this.oParentObject.w_ATTSCLIS)
            this.w_AGG = 1
            this.oParentObject.w_ATTPROSC = SPACE(5)
            if Empty(this.oParentObject.w_ATTCOLIS)
              this.oParentObject.w_ATTPROLI = SPACE(5)
            endif
            this.w_MESS = "Elimino i listini sconto presenti sulle righe?"
          else
            if !EMPTY(this.oParentObject.w_ATTPROLI)
              if !EMPTY(this.oParentObject.w_ATTSCLIS)
                if this.oParentObject.w_TIPLIS3="E"
                  this.oParentObject.w_ATTPROSC = this.oParentObject.w_ATTPROLI
                endif
                this.w_AGG = 1
                this.w_MESS = "Aggiorno i listini presenti sulle righe?"
              endif
            else
              if this.oParentObject.w_TIPLIS4="E" AND !EMPTY(this.oParentObject.w_ATTPROSC)
                this.oParentObject.w_ATTPROSC = SPACE(5)
              endif
            endif
          endif
        case this.pEXEC="C"
          this.w_GESTIONE = This.oparentobject.oparentobject
          if (this.oParentObject.w_TIPLIS3="E" Or this.oParentObject.w_TIPLIS4="E") And !(this.oParentObject.w_ATTPROLI == this.oParentObject.w_ATTPROSC) And !Empty(this.oParentObject.w_ATTPROSC) And !Empty(this.oParentObject.w_ATTPROLI)
            this.w_MESS = "Attenzione: quando i listini sono di tipologia prezzi e sconti devono coincidere"
            AH_ERRORMSG(this.w_MESS,48,"")
            this.oParentObject.w_ATTPROLI = this.oParentObject.o_ATTPROLI
            this.oParentObject.w_TIPLIS3 = this.oParentObject.o_TIPLIS3
          else
            if (this.oParentObject.w_TIPLIS3="E" Or this.oParentObject.w_TIPLIS4="E")
              this.oParentObject.w_ATTPROSC = this.oParentObject.w_ATTPROLI
            endif
            this.w_AGG = 1
            this.w_MESS = "Aggiorno le promozioni presenti sulle righe?"
          endif
        case this.pEXEC="D"
          this.w_GESTIONE = This.oparentobject.oparentobject
          if (this.oParentObject.w_TIPLIS3="E" Or this.oParentObject.w_TIPLIS4="E") And !(this.oParentObject.w_ATTPROLI == this.oParentObject.w_ATTPROSC) And !Empty(this.oParentObject.w_ATTPROSC) And !Empty(this.oParentObject.w_ATTPROLI)
            this.w_MESS = "Attenzione: quando i listini sono di tipologia prezzi e sconti devono coincidere"
            AH_ERRORMSG(this.w_MESS,48,"")
            this.oParentObject.w_ATTPROSC = this.oParentObject.o_ATTPROSC
            this.oParentObject.w_TIPLIS4 = this.oParentObject.o_TIPLIS4
          else
            if (this.oParentObject.w_TIPLIS3="E" Or this.oParentObject.w_TIPLIS4="E")
              this.oParentObject.w_ATTPROLI = this.oParentObject.w_ATTPROSC
            endif
            this.w_AGG = 1
            this.w_MESS = "Aggiorno le promozioni presenti sulle righe?"
          endif
        case this.pEXEC="E" OR this.pEXEC="F"
          this.w_GESTIONE = This.oparentobject
          * --- Aggiorno temporaneo listini
          if this.pEXEC="E"
            if upper(this.w_GESTIONE.cPrg)="GSAG_KPR" OR upper(this.w_GESTIONE.cPrg)="GSAG_KPP"
              this.oParentObject.w_DATALIST = this.oParentObject.w_DATLIS
              this.w_COND = .f.
              this.w_LTIPCLI = this.oParentObject.w_NOTIPCLI
              this.w_LCODCLI = this.oParentObject.w_NOCODCLI
            else
              this.w_LCODCLI = this.oParentObject.w_CODCLI
              this.w_LTIPCLI = this.oParentObject.w_ATTIPCLI
              this.w_CAUATT = IIF(NOT EMPTY(this.oParentObject.o_ATCAUATT),this.oParentObject.o_ATCAUATT,this.oParentObject.w_ATCAUATT)
              this.w_CODNOM = IIF(NOT EMPTY(this.oParentObject.o_ATCODNOM),this.oParentObject.o_ATCODNOM,this.oParentObject.w_ATCODNOM)
              this.w_COND = this.oParentObject.o_ATCODVAL <> this.oParentObject.w_ATCODVAL Or (this.w_CAUATT <> this.oParentObject.w_ATCAUATT And (Empty(this.oParentObject.w_CODCLI) Or (!Empty(this.oParentObject.o_ATCAUATT) And this.oParentObject.w_FLGLIS ="S"))) Or ((this.oParentObject.o_ATDATDOC <> this.oParentObject.w_ATDATDOC) And (this.w_FLINTE = "N" Or !Empty(this.oParentObject.w_CODCLI)))
              this.w_COND = this.w_COND or (this.oParentObject.o_ATCODNOM <> this.oParentObject.w_ATCODNOM) OR this.oParentObject.w_FLSCOR="S"
              this.oParentObject.w_DATALIST = IIF(Not Empty(this.oParentObject.w_ATDATDOC),this.oParentObject.w_ATDATDOC,this.oParentObject.w_DATFIN)
            endif
          endif
          * --- Edit aborted
          gsag_bdl(this,"D",this.oParentObject.w_KEYLISTB,.Null.,Space(5))
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.pEXEC="E"
            if this.w_COND
              * --- ricalclolo la data di riferimento per i listini
              * --- E' cambiato il codice della valuta e la causale documento pulizia tabella
              * --- Verifico se devo rleggere i parametri
              if Not Empty(this.oParentObject.w_ATTCOLIS + this.oParentObject.w_ATTSCLIS + this.oParentObject.w_ATTPROLI + this.oParentObject.w_ATTPROSC) or this.oParentObject.w_FLGLIS<>"S"
                this.w_RICALCOLA = .T.
                this.w_AGG = 1
                if Not Empty(this.oParentObject.O_ATCODNOM)
                  this.w_MESS = "Aggiorno i prezzi/promozioni presenti sulle righe?"
                endif
              endif
              this.oParentObject.w_ATTCOLIS = ""
              this.oParentObject.w_ATTSCLIS = ""
              this.oParentObject.w_ATTPROLI = ""
              this.oParentObject.w_ATTPROSC = ""
              * --- Creo la nuova tabella 
              DECLARE ARRCALC (4,1)
              w_RESULT=CALCLIST("CREA",@ARRCALC,this.oParentObject.w_MVFLVEAC,this.oParentObject.w_FLGLIS,this.oParentObject.w_ATCODVAL,this.w_LTIPCLI,this.w_LCODCLI,this.oParentObject.w_DATALIST," "," ",this.oParentObject.w_KEYLISTB,-20,this.oParentObject.w_FLSCOR," ",this.oParentObject.w_ATCAUDOC)
              this.oParentObject.w_ATTCOLIS = ARRCALC(1)
              this.oParentObject.w_ATTSCLIS = ARRCALC(2)
              this.oParentObject.w_ATTPROLI = ARRCALC(3)
              this.oParentObject.w_ATTPROSC = ARRCALC(4)
              if this.w_RICALCOLA
                if !Empty(this.oParentObject.w_ATTCOLIS)
                  * --- Read from LISTINI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.LISTINI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS,LSTIPLIS,LSFLGCIC,LSFLSTAT"+;
                      " from "+i_cTable+" LISTINI where ";
                          +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.w_ATTCOLIS);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      LSVALLIS,LSDTINVA,LSDTOBSO,LSIVALIS,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                      from (i_cTable) where;
                          LSCODLIS = this.oParentObject.w_ATTCOLIS;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_VALLIST = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
                    this.oParentObject.w_INILIS = NVL(cp_ToDate(_read_.LSDTINVA),cp_NullValue(_read_.LSDTINVA))
                    this.oParentObject.w_FINLIS = NVL(cp_ToDate(_read_.LSDTOBSO),cp_NullValue(_read_.LSDTOBSO))
                    this.w_IVALIST = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
                    this.oParentObject.w_TIPLIS = NVL(cp_ToDate(_read_.LSTIPLIS),cp_NullValue(_read_.LSTIPLIS))
                    this.oParentObject.w_FLGCIC = NVL(cp_ToDate(_read_.LSFLGCIC),cp_NullValue(_read_.LSFLGCIC))
                    this.oParentObject.w_FLSTAT = NVL(cp_ToDate(_read_.LSFLSTAT),cp_NullValue(_read_.LSFLSTAT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                if !Empty(this.oParentObject.w_ATTSCLIS)
                  * --- Read from LISTINI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.LISTINI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT"+;
                      " from "+i_cTable+" LISTINI where ";
                          +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.w_ATTSCLIS);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                      from (i_cTable) where;
                          LSCODLIS = this.oParentObject.w_ATTSCLIS;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_VALLIST2 = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
                    this.oParentObject.w_INILIS2 = NVL(cp_ToDate(_read_.LSDTINVA),cp_NullValue(_read_.LSDTINVA))
                    this.oParentObject.w_FINLIS2 = NVL(cp_ToDate(_read_.LSDTOBSO),cp_NullValue(_read_.LSDTOBSO))
                    this.oParentObject.w_TIPLIS2 = NVL(cp_ToDate(_read_.LSTIPLIS),cp_NullValue(_read_.LSTIPLIS))
                    this.oParentObject.w_FLGCIC2 = NVL(cp_ToDate(_read_.LSFLGCIC),cp_NullValue(_read_.LSFLGCIC))
                    this.oParentObject.w_F2STAT = NVL(cp_ToDate(_read_.LSFLSTAT),cp_NullValue(_read_.LSFLSTAT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                if !Empty(this.oParentObject.w_ATTPROLI)
                  * --- Read from LISTINI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.LISTINI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSIVALIS"+;
                      " from "+i_cTable+" LISTINI where ";
                          +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.w_ATTPROLI);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSIVALIS;
                      from (i_cTable) where;
                          LSCODLIS = this.oParentObject.w_ATTPROLI;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_VALLIST3 = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
                    this.oParentObject.w_INILIS3 = NVL(cp_ToDate(_read_.LSDTINVA),cp_NullValue(_read_.LSDTINVA))
                    this.oParentObject.w_FINLIS3 = NVL(cp_ToDate(_read_.LSDTOBSO),cp_NullValue(_read_.LSDTOBSO))
                    this.oParentObject.w_TIPLIS3 = NVL(cp_ToDate(_read_.LSTIPLIS),cp_NullValue(_read_.LSTIPLIS))
                    this.oParentObject.w_FLGCIC3 = NVL(cp_ToDate(_read_.LSFLGCIC),cp_NullValue(_read_.LSFLGCIC))
                    this.oParentObject.w_F3STAT = NVL(cp_ToDate(_read_.LSFLSTAT),cp_NullValue(_read_.LSFLSTAT))
                    this.w_IVALISTP = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                if !Empty(this.oParentObject.w_ATTPROSC)
                  * --- Read from LISTINI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.LISTINI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT"+;
                      " from "+i_cTable+" LISTINI where ";
                          +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.w_ATTPROSC);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                      from (i_cTable) where;
                          LSCODLIS = this.oParentObject.w_ATTPROSC;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_VALLIST4 = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
                    this.oParentObject.w_INILIS4 = NVL(cp_ToDate(_read_.LSDTINVA),cp_NullValue(_read_.LSDTINVA))
                    this.oParentObject.w_FINLIS4 = NVL(cp_ToDate(_read_.LSDTOBSO),cp_NullValue(_read_.LSDTOBSO))
                    this.oParentObject.w_TIPLIS4 = NVL(cp_ToDate(_read_.LSTIPLIS),cp_NullValue(_read_.LSTIPLIS))
                    this.oParentObject.w_FLGCIC4 = NVL(cp_ToDate(_read_.LSFLGCIC),cp_NullValue(_read_.LSFLGCIC))
                    this.oParentObject.w_F4STAT = NVL(cp_ToDate(_read_.LSFLSTAT),cp_NullValue(_read_.LSFLSTAT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
              endif
            else
              * --- Entro in modifica, popolo la tabella temporaneo listini
              DECLARE ARRCALC (4,1)
              w_RESULT=CALCLIST("CREA",@ARRCALC,this.oParentObject.w_MVFLVEAC,"N",this.oParentObject.w_ATCODVAL,this.w_LTIPCLI,this.w_LCODCLI,this.oParentObject.w_DATALIST," "," ",this.oParentObject.w_KEYLISTB,-20,this.oParentObject.w_FLSCOR," ",this.oParentObject.w_ATCAUDOC)
            endif
          endif
          * --- Evito la chiamata alla mCalc comunque fatta dopo la Valid dei vari campi
          this.bUpdateParentObject=.F.
        case this.pEXEC="G"
          * --- Modifica listino costi di testata
          this.w_GESTIONE = This.oparentobject.oparentobject
          this.w_MESS = "Aggiorno i costi interni presenti sulle righe?"
          this.w_AGG = 1
        case this.pEXEC="H"
          this.w_GESTIONE = This.oparentobject.oparentobject
          this.w_MESS = "Aggiorno le date di competenza presenti sulle righe?"
          this.w_AGG = 1
      endcase
      if this.w_AGG>0
        this.w_PADRE = this.w_GESTIONE.GSAG_MDA
        if (Not Empty(this.w_MESS) AND this.w_PADRE.NUMROW()>0 AND AH_YESNO(this.w_MESS)) OR this.w_PADRE.NUMROW()=0
          * --- Anticipo aggiornamento campi gestione
          do case
            case this.pEXEC="A"
              this.w_GESTIONE.w_ATTCOLIS = this.oParentObject.w_ATTCOLIS
            case this.pEXEC="B"
              this.w_GESTIONE.w_ATTPROLI = this.oParentObject.w_ATTPROLI
            case this.pEXEC="C"
              this.w_GESTIONE.w_ATTSCLIS = this.oParentObject.w_ATTSCLIS
            case this.pEXEC="D"
              this.w_GESTIONE.w_ATTPROSC = this.oParentObject.w_ATTPROSC
            case this.pEXEC="H"
              this.w_GESTIONE.w_ATINIRIC = this.oParentObject.w_ATINIRIC
              this.w_GESTIONE.w_ATFINRIC = this.oParentObject.w_ATFINRIC
              this.w_GESTIONE.w_ATINICOS = this.oParentObject.w_ATINICOS
              this.w_GESTIONE.w_ATFINCOS = this.oParentObject.w_ATFINCOS
          endcase
          this.w_PADRE.MarkPos()     
          this.w_PADRE.FirstRow()     
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            this.w_PADRE.SaveDependsOn()     
            do case
              case this.pEXEC="G"
                this.w_PADRE.NotifyEvent("LisAcq")
              case this.pEXEC="H"
                * --- SE PIENA w_ATINIRIC ASSEGNO DATE INIZIO DEI RICAVI
                if NOT EMPTY(this.oParentObject.w_ATINIRIC)
                  this.w_PADRE.w_DAINICOM = this.oParentObject.w_ATINIRIC
                  this.w_PADRE.w_DATCOINI = this.oParentObject.w_ATINIRIC
                endif
                if NOT EMPTY(this.oParentObject.w_ATFINRIC)
                  this.w_PADRE.w_DAFINCOM = this.oParentObject.w_ATFINRIC
                  this.w_PADRE.w_DATCOFIN = this.oParentObject.w_ATFINRIC
                endif
                if NOT EMPTY(this.oParentObject.w_ATINICOS)
                  this.w_PADRE.w_DATRIINI = this.oParentObject.w_ATINICOS
                endif
                if NOT EMPTY(this.oParentObject.w_ATFINCOS)
                  this.w_PADRE.w_DATRIFIN = this.oParentObject.w_ATFINCOS
                endif
              otherwise
                this.w_PADRE.NotifyEvent("Agglis")
            endcase
            * --- Salvo la riga sui temporanei..
            this.w_PADRE.SaveRow()     
            this.w_PADRE.NextRow()     
          enddo
          this.w_PADRE.RePos()     
        endif
      endif
    else
      if this.pEXEC="E" OR this.pEXEC="F"
        this.w_GESTIONE = This.oparentobject
        this.w_GESTIONE.bcalculating = .t.
      endif
    endif
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='TIP_DOCU'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
