* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bd5                                                        *
*              Azzera il dettaglio attributi                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-31                                                      *
* Last revis.: 2011-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bd5",oParentObject)
return(i_retval)

define class tgsag_bd5 as StdBatch
  * --- Local variables
  w_GSAR_MRA = .NULL.
  w_MACODATT = space(10)
  * --- WorkFile variables
  MODDATTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Azzera il dettaglio attributi
    this.w_GSAR_MRA = this.oParentObject.GSAR_MRA
    this.w_GSAR_MRA.FirstRow()     
    do while this.w_GSAR_MRA.FullRow()
      this.w_GSAR_MRA.DeleteRow()     
      this.w_GSAR_MRA.FirstRow()     
    enddo
    this.w_GSAR_MRA.Set("w_CPROWORD",10, .F., .T.)     
    * --- Inizializza il gruppo predefinito
    * --- Read from MODDATTR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MODDATTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODDATTR_idx,2],.t.,this.MODDATTR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MACODATT"+;
        " from "+i_cTable+" MODDATTR where ";
            +"MACODICE = "+cp_ToStrODBC(this.oParentObject.w_IMMODATR);
            +" and MAGRUDEF = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MACODATT;
        from (i_cTable) where;
            MACODICE = this.oParentObject.w_IMMODATR;
            and MAGRUDEF = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MACODATT = NVL(cp_ToDate(_read_.MACODATT),cp_NullValue(_read_.MACODATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_GSAR_MRA.Set("w_ASCODATT", this.w_MACODATT, .F., .T.)     
    this.w_GSAR_MRA.Refresh()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MODDATTR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
