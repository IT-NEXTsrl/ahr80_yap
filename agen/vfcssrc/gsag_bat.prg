* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bat                                                        *
*              Esegue il link sul componente                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-10                                                      *
* Last revis.: 2010-11-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pATCODIMP,pVALATTR,pOLDCOMP,pCodCom,pCTRLPOSITION,pZoom
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bat",oParentObject,m.pATCODIMP,m.pVALATTR,m.pOLDCOMP,m.pCodCom,m.pCTRLPOSITION,m.pZoom)
return(i_retval)

define class tgsag_bat as StdBatch
  * --- Local variables
  pATCODIMP = space(10)
  pVALATTR = space(50)
  pOLDCOMP = space(10)
  pCodCom = space(20)
  pCTRLPOSITION = space(1)
  pZoom = .f.
  w_INZOOM = space(1)
  pParent = .NULL.
  w_CACODIMP = space(10)
  w_CPROWORD = 0
  w_IMDESCON = space(50)
  w_CPROWNUM = 0
  w_BAGG = .f.
  w_LATCOMPIM = space(50)
  w_NUMERO = 0
  w_IMMODATR = space(20)
  * --- WorkFile variables
  IMP_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato alla before input del campo CACOMPIM sulla gestione Attivit�. (GSAG_MCP)
    *     pATCODIMP codice impianto
    *     pVALATTR valore attributo
    *     pOldComp valore campo descrittivo previa modifica utente
    *     pCodCom nome control con chiave codice componente
    *     pCTRLPOSITION = per eseguire la valorizzazone (M=testata, D = dettaglio)
    *     pZoom = se passato e a .t. lancia lo zoom di selezione dei componenti senza ricerche...
    * --- Non eseguo la mcalc
    this.bUpdateParentObject=.f.
    this.pParent = this.oparentobject
    * --- Faccio lo zoom (F9)
    if this.pZoom
      this.w_INZOOM = "S"
      this.w_LATCOMPIM = ""
      this.w_CPROWNUM = 0
      this.w_CACODIMP = this.pATCODIMP
      * --- Variabile CPROWORD dichiarata solo per poter gestire il ritorno di valore
      *     dallo zoom (esso verifica e la variabile esiste sul batch o su suo padre, e nel caso
      *     della movimentazione impianti legati all'attivit� cproword esiste...
      vx_exec("..\AGEN\EXE\QUERY\GSAG_ZCI.VZM",this)
      * --- Se non seleziono elementi lascio quanto trovato...
      if this.w_CPROWNUM<>0
        = setvaluelinked ( this.pCTRLPOSITION , this.pParent , this.pCodCom , this.w_CPROWNUM)
      endif
    else
      * --- Non eseguo lo zoom ma scrivo a mano.
      if NOT EMPTY(this.pATCODIMP) and NOT EMPTY(this.pVALATTR) and NOT this.pOLDCOMP==this.pVALATTR
        * --- Se la descrizione inserita dall'utente coincide con la descrizione di un
        *     componente presente sull'impianto allora non eseguo la ricerca per
        *     attributi. se due componenti con la medesima descrizione la scelta la affido
        *     al database...(caso limite)
        this.w_BAGG = .T.
        this.w_INZOOM = "N"
        this.w_CPROWNUM = 0
        * --- Read from IMP_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.IMP_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.IMP_DETT_idx,2],.t.,this.IMP_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CPROWNUM"+;
            " from "+i_cTable+" IMP_DETT where ";
                +"IMCODICE = "+cp_ToStrODBC(this.pATCODIMP);
                +" and IMDESCON = "+cp_ToStrODBC(this.pVALATTR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CPROWNUM;
            from (i_cTable) where;
                IMCODICE = this.pATCODIMP;
                and IMDESCON = this.pVALATTR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_CPROWNUM=0
          this.w_LATCOMPIM = "%"+Alltrim(this.pVALATTR)+"%"
          this.w_NUMERO = 0
          * --- Se la descrizione componente coincide con quello che ho scritto, non faccio nulla
          * --- Select from ..\AGEN\EXE\QUERY\GSAG_BAT.VQR
          do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BAT.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BAT_d_VQR','',.f.,.t.
          if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BAT_d_VQR')
            select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BAT_d_VQR
            locate for 1=1
            do while not(eof())
            this.w_NUMERO = this.w_NUMERO+1
            this.w_CPROWNUM = CPROWNUM
            if this.w_NUMERO>1
              exit
            endif
              select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BAT_d_VQR
              continue
            enddo
            use
          endif
          * --- Se ne trovo pi� di una apro lo zoom per scegliere
          if this.w_NUMERO>1
            this.w_CPROWNUM = 0
            vx_exec("..\AGEN\EXE\QUERY\GSAG_BAT.VZM",this)
            * --- Aggiorno il dato solo se l'utente seleziona una riga dallo zoom...
            this.w_BAGG = this.w_CPROWNUM<>0
          endif
          * --- Se non ho trovato nulla riapro lo zoom iniziale (F9)
          if this.w_NUMERO=0
            this.w_CPROWNUM = 0
            this.w_LATCOMPIM = "%"+Alltrim(this.pVALATTR)+"%"
            this.w_CACODIMP = this.pATCODIMP
            vx_exec("..\AGEN\EXE\QUERY\GSAG_ZCI.VZM",this)
            * --- Ricerca per componente
            this.w_BAGG = this.w_CPROWNUM<>0
          endif
        endif
        if this.w_BAGG
          * --- Comunque eseguo la valorizzazione del campo a seguito di una ricerca, se 0 svuoto il valore...
          if this.w_CPROWNUM=0
            * --- Se valore non trovato forzo il ricalcolo delle dipedenza, altrimenti il dato
            *     rimanendo a 0 non mi ricalcolerebbe le descrizioni...
            *     (-1 valore impossibile per un CPROWNUM)
            = setvaluelinked ( this.pCTRLPOSITION , this.pParent , this.pCodCom , -1)
          endif
          = setvaluelinked ( this.pCTRLPOSITION , this.pParent , this.pCodCom , this.w_CPROWNUM)
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pATCODIMP,pVALATTR,pOLDCOMP,pCodCom,pCTRLPOSITION,pZoom)
    this.pATCODIMP=pATCODIMP
    this.pVALATTR=pVALATTR
    this.pOLDCOMP=pOLDCOMP
    this.pCodCom=pCodCom
    this.pCTRLPOSITION=pCTRLPOSITION
    this.pZoom=pZoom
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='IMP_DETT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BAT_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BAT_d_VQR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pATCODIMP,pVALATTR,pOLDCOMP,pCodCom,pCTRLPOSITION,pZoom"
endproc
