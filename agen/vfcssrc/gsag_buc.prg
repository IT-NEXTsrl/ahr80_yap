* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_buc                                                        *
*              Utility avvalora costo in prestazioni                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-07-31                                                      *
* Last revis.: 2012-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_buc",oParentObject)
return(i_retval)

define class tgsag_buc as StdBatch
  * --- Local variables
  w_TipoPre = space(1)
  w_SERIAL = space(20)
  w_CPROWNUM = 0
  w_DACODRES = space(5)
  w_DAOREEFF = 0
  w_DAMINEFF = 0
  w_DACOSUNI = 0
  w_DACOSINT = 0
  w_DPCOSORA = 0
  * --- WorkFile variables
  OFFDATTI_idx=0
  PRE_STAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tipi attivit� - Sbiancamento prestazioni
    * --- DACOSUNI
    * --- Controlli preliminari
    if this.oParentObject.w_FLDurata="S" AND this.oParentObject.w_Dur_Ore=0 AND this.oParentObject.w_Dur_Min=0
      * --- E' necessario specificare la durata da impostare
      ah_ErrorMsg("Specificare la durata da impostare nelle prestazioni")
      i_retcode = 'stop'
      return
    endif
    * --- Select from ..\AGEN\EXE\QUERY\GSAG_BUC.VQR
    do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BUC.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BUC_d_VQR','',.f.,.t.
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BUC_d_VQR')
      select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BUC_d_VQR
      locate for 1=1
      do while not(eof())
      * --- Nel cursore sono presenti le prestazioni senza costo e/o senza durata a seconda del valore dei flag di maschera
      this.w_TipoPre = TIPOPRE
      this.w_SERIAL = SERIAL
      this.w_CPROWNUM = CPROWNUM
      this.w_DACODRES = CODRES
      this.w_DAOREEFF = OREEFF
      this.w_DAMINEFF = MINEFF
      this.w_DACOSUNI = COSUNI
      this.w_DPCOSORA = DPCOSORA
      if this.oParentObject.w_FLDurata="S" AND this.w_DAOREEFF=0 AND this.w_DAMINEFF=0
        this.w_DAOREEFF = this.oParentObject.w_Dur_Ore
        this.w_DAMINEFF = this.oParentObject.w_Dur_Min
      endif
      if this.w_DACOSUNI=0
        * --- Imposta il costo orario
        if this.w_DACODRES=this.oParentObject.w_CODPERS
          this.w_DACOSUNI = this.oParentObject.w_COST_ORA
        else
          this.w_DACOSUNI = this.w_DPCOSORA
        endif
      endif
      this.w_DACOSINT = (this.w_DAOREEFF+(this.w_DAMINEFF/60))*this.w_DACOSUNI
      if this.w_TipoPre="A"
        * --- Prestazione da attivit�
        * --- Write into OFFDATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OFFDATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFFDATTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DACOSINT ="+cp_NullLink(cp_ToStrODBC(this.w_DACOSINT),'OFFDATTI','DACOSINT');
          +",DACOSUNI ="+cp_NullLink(cp_ToStrODBC(this.w_DACOSUNI),'OFFDATTI','DACOSUNI');
          +",DAOREEFF ="+cp_NullLink(cp_ToStrODBC(this.w_DAOREEFF),'OFFDATTI','DAOREEFF');
          +",DAMINEFF ="+cp_NullLink(cp_ToStrODBC(this.w_DAMINEFF),'OFFDATTI','DAMINEFF');
              +i_ccchkf ;
          +" where ";
              +"DASERIAL = "+cp_ToStrODBC(this.w_SERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              DACOSINT = this.w_DACOSINT;
              ,DACOSUNI = this.w_DACOSUNI;
              ,DAOREEFF = this.w_DAOREEFF;
              ,DAMINEFF = this.w_DAMINEFF;
              &i_ccchkf. ;
           where;
              DASERIAL = this.w_SERIAL;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- PRE_STAZ
        * --- Write into PRE_STAZ
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PRCOSINT ="+cp_NullLink(cp_ToStrODBC(this.w_DACOSINT),'PRE_STAZ','PRCOSINT');
          +",PRCOSUNI ="+cp_NullLink(cp_ToStrODBC(this.w_DACOSUNI),'PRE_STAZ','PRCOSUNI');
          +",PROREEFF ="+cp_NullLink(cp_ToStrODBC(this.w_DAOREEFF),'PRE_STAZ','PROREEFF');
          +",PRMINEFF ="+cp_NullLink(cp_ToStrODBC(this.w_DAMINEFF),'PRE_STAZ','PRMINEFF');
              +i_ccchkf ;
          +" where ";
              +"PRSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          update (i_cTable) set;
              PRCOSINT = this.w_DACOSINT;
              ,PRCOSUNI = this.w_DACOSUNI;
              ,PROREEFF = this.w_DAOREEFF;
              ,PRMINEFF = this.w_DAMINEFF;
              &i_ccchkf. ;
           where;
              PRSERIAL = this.w_SERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BUC_d_VQR
        continue
      enddo
      use
    endif
    ah_ErrorMsg("Operazione completata")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OFFDATTI'
    this.cWorkTables[2]='PRE_STAZ'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BUC_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BUC_d_VQR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
