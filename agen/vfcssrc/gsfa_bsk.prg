* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bsk                                                        *
*              Apre attivita per chiamata telefonica                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-06-17                                                      *
* Last revis.: 2012-06-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCONTACT,pCOMMTYP,pNOMINATIVO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bsk",oParentObject,m.pCONTACT,m.pCOMMTYP,m.pNOMINATIVO)
return(i_retval)

define class tgsfa_bsk as StdBatch
  * --- Local variables
  pCONTACT = space(50)
  pCOMMTYP = space(2)
  w_EV_PHONE = space(18)
  w_EV_EMAIL = space(254)
  w_EVNOMINA = space(15)
  w_FIRSTSOG = space(0)
  w_NOTESOG = space(0)
  w_EVRIFPER = space(254)
  w_CODICE = space(15)
  w_EVCODPAR = space(5)
  w_Pers_Descri = space(5)
  w_Pers_Telefono = space(5)
  w_Pers_Cellulare = space(5)
  w_Pers_E_Mail = space(5)
  pNOMINATIVO = space(15)
  w_STRFILE = space(0)
  w_CONTACT = space(50)
  w_NOMINATIVO = space(15)
  w_UTATTOUT = space(20)
  w_UTATT_IN = space(20)
  w_GSAG_AAT = .NULL.
  * --- WorkFile variables
  UTE_NTI_idx=0
  NOM_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apro attivit�
    * --- Apertura Attivit�
    *     CM
    *     CT
    *     CI
    *     Telefonata in ingresso
    *     IN
    * --- --
    * --- --
    this.w_EV_PHONE = this.pCONTACT
    this.w_EV_EMAIL = ""
    this.w_EVNOMINA = this.pNOMINATIVO
    this.w_FIRSTSOG = ""
    this.w_NOTESOG = ""
    * --- --
    if this.pCOMMTYP="IN"
      * --- Telefonata in ingresso
      if cp_FileExist(this.pCONTACT)
        this.w_STRFILE = FILETOSTR(this.pCONTACT)
        ALINES(l_arrPar, this.w_STRFILE)
        if TYPE("l_ArrPar[1]")="C"
          this.w_CONTACT = l_ArrPar[1]
        endif
        if TYPE("l_ArrPar[2]")="C"
          this.w_NOMINATIVO = l_ArrPar[2]
        endif
        GSFA_BSK(this,this.w_CONTACT, "CI", this.w_NOMINATIVO)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      if (Not Empty(this.w_EV_PHONE) or Not Empty(this.w_EV_EMAIL) or Not Empty(this.w_EVNOMINA))
        * --- Select from ..\AGEN\EXE\QUERY\GSFA_BSK
        do vq_exec with '..\AGEN\EXE\QUERY\GSFA_BSK',this,'_Curs__d__d__AGEN_EXE_QUERY_GSFA_BSK','',.f.,.t.
        if used('_Curs__d__d__AGEN_EXE_QUERY_GSFA_BSK')
          select _Curs__d__d__AGEN_EXE_QUERY_GSFA_BSK
          locate for 1=1
          do while not(eof())
          * --- Carico solo il primo soggetto, se ne trovo pi� di uno valorizzo w_EVFLGCHK
          this.pNOMINATIVO = IIF(Empty(this.w_EVNOMINA), NVL(CODICE, ""), this.w_EVNOMINA)
          if NOCODQUE="1"
            this.w_EVCODPAR = ""
            this.w_EVRIFPER = ""
          else
            this.w_EVCODPAR = NVL(NCCODCON, "")
            this.w_EVRIFPER = NVL(PERSONA,"")
          endif
          this.w_CODICE = NVL(NODESCRI, "")
          this.w_FIRSTSOG = this.w_CODICE + ALLTRIM( NVL(NODESCRI, "") )+ IIF(EMPTY(NVL(PERSONA, " ")), "", " - " + ALLTRIM( NVL(PERSONA,"") ) )
          * --- Concatener� alle note l'elenco dei soggetti papabili trovati
          this.w_NOTESOG = this.w_NOTESOG + ALLTRIM(NVL(CODICE, "") )+ " " + ALLTRIM( NVL(NODESCRI, "") )+ IIF(EMPTY(NVL(PERSONA, " ")), "", " - " + ALLTRIM( NVL(PERSONA,"") ) )+ CHR(13)+CHR(10)
          * --- Read from NOM_CONT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.NOM_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2],.t.,this.NOM_CONT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL"+;
              " from "+i_cTable+" NOM_CONT where ";
                  +"NCCODICE = "+cp_ToStrODBC(this.pNOMINATIVO);
                  +" and NCCODCON = "+cp_ToStrODBC(this.w_EVCODPAR);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL;
              from (i_cTable) where;
                  NCCODICE = this.pNOMINATIVO;
                  and NCCODCON = this.w_EVCODPAR;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_Pers_Descri = NVL(cp_ToDate(_read_.NCPERSON),cp_NullValue(_read_.NCPERSON))
            this.w_Pers_Telefono = NVL(cp_ToDate(_read_.NCTELEFO),cp_NullValue(_read_.NCTELEFO))
            this.w_Pers_Cellulare = NVL(cp_ToDate(_read_.NCNUMCEL),cp_NullValue(_read_.NCNUMCEL))
            this.w_Pers_E_Mail = NVL(cp_ToDate(_read_.NC_EMAIL),cp_NullValue(_read_.NC_EMAIL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
            select _Curs__d__d__AGEN_EXE_QUERY_GSFA_BSK
            continue
          enddo
          use
        endif
      else
        this.w_FIRSTSOG = Ah_msgformat("Sconosciuto")
        this.w_NOTESOG = this.w_NOTESOG +Ah_msgformat("Sconosciuto")+ CHR(13)+CHR(10)
      endif
      * --- Apertura Attivit�
      * --- Read from UTE_NTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.UTE_NTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2],.t.,this.UTE_NTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "UTATTOUT,UTATT_IN"+;
          " from "+i_cTable+" UTE_NTI where ";
              +"UTCODICE = "+cp_ToStrODBC(i_CODUTE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          UTATTOUT,UTATT_IN;
          from (i_cTable) where;
              UTCODICE = i_CODUTE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_UTATTOUT = NVL(cp_ToDate(_read_.UTATTOUT),cp_NullValue(_read_.UTATTOUT))
        this.w_UTATT_IN = NVL(cp_ToDate(_read_.UTATT_IN),cp_NullValue(_read_.UTATT_IN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if (!Empty(this.w_UTATTOUT) And INLIST(this.pCOMMTYP,"CM","CT", "SO")) Or (!Empty(this.w_UTATT_IN) And this.pCOMMTYP=="CI")
        this.w_GSAG_AAT = GSAG_AAT()
        if Type("this.w_GSAG_AAT.bSec1")="U" Or !(this.w_GSAG_AAT.bSec1)
          this.w_GSAG_AAT = .Null.
          i_retcode = 'stop'
          return
        endif
        this.w_GSAG_AAT.ecpLoad()     
        if this.pCOMMTYP=="CI"
          SetValueLinked("M", this.w_GSAG_AAT, "w_ATCAUATT", this.w_UTATT_IN)
        else
          SetValueLinked("M", this.w_GSAG_AAT, "w_ATCAUATT", this.w_UTATTOUT)
        endif
        if !EMPTY(this.w_GSAG_AAT.w_CACHKNOM)
          SetValueLinked("M", this.w_GSAG_AAT, "w_ATCODNOM", this.pNOMINATIVO)
          this.w_GSAG_AAT.o_ATCODNOM = this.pNOMINATIVO
          if NOT EMPTY(this.w_EVCODPAR)
            this.w_GSAG_AAT.w_ATCONTAT = this.w_EVCODPAR
            this.w_GSAG_AAT.w_Pers_Descri = this.w_Pers_Descri
            this.w_GSAG_AAT.w_Pers_Telefono = this.w_Pers_Telefono
            this.w_GSAG_AAT.w_Pers_Cellulare = this.w_Pers_Cellulare
            this.w_GSAG_AAT.w_Pers_E_Mail = this.w_Pers_E_Mail
          endif
          this.w_GSAG_AAT.w_ATPERSON = this.w_EVRIFPER
          if this.pCOMMTYP=="CM"
            * --- Cellulare
            SetValueLinked("M", this.w_GSAG_AAT, "w_ATCELLUL", this.pCONTACT)
          else
            * --- Telefono
            SetValueLinked("M", this.w_GSAG_AAT, "w_ATTELEFO", this.pCONTACT)
          endif
        endif
      endif
      this.w_GSAG_AAT = .Null.
    endif
  endproc


  proc Init(oParentObject,pCONTACT,pCOMMTYP,pNOMINATIVO)
    this.pCONTACT=pCONTACT
    this.pCOMMTYP=pCOMMTYP
    this.pNOMINATIVO=pNOMINATIVO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='UTE_NTI'
    this.cWorkTables[2]='NOM_CONT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSFA_BSK')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSFA_BSK
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCONTACT,pCOMMTYP,pNOMINATIVO"
endproc
