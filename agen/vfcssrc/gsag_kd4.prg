* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kd4                                                        *
*              Rinnova elemento contratto                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-16                                                      *
* Last revis.: 2015-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kd4",oParentObject))

* --- Class definition
define class tgsag_kd4 as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 797
  Height = 410
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-12"
  HelpContextID=82454377
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Constant Properties
  _IDX = 0
  MOD_ELEM_IDX = 0
  CONTI_IDX = 0
  CON_TRAS_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  cPrg = "gsag_kd4"
  cComment = "Rinnova elemento contratto"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ANTIPCON = space(1)
  w_ELCODMOD = space(10)
  w_MODESCRI = space(60)
  w_ELCONTRA = space(10)
  w_ELCODCLI = space(15)
  w_ANDESCRI = space(50)
  w_CODESCON = space(50)
  w_ELCODIMP = space(10)
  w_IMDESCRI = space(50)
  w_ELCODCOM = 0
  w_IMDESCON = space(50)
  w_CPROWORD = 0
  w_ELRINNOV_1 = 0
  w_OKBUTTON = space(10)
  w_ELDATFIN = ctod('  /  /  ')
  o_ELDATFIN = ctod('  /  /  ')
  w_ELDATINI_1 = ctod('  /  /  ')
  w_ELRINNOV = 0
  w_ELDATFIN_1 = ctod('  /  /  ')
  w_ELPREZZO = 0
  w_RICPE1 = 0
  w_RICVA1 = 0
  w_ARROT1 = 0
  w_VALORIN = 0
  w_ARROT2 = 0
  w_VALOR2IN = 0
  w_ARROT3 = 0
  w_VALOR3IN = 0
  w_ARROT4 = 0
  w_VALOFIN = 0
  w_FLDATFIS = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kd4Pag1","gsag_kd4",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oELDATFIN_1_1_30
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='MOD_ELEM'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CON_TRAS'
    this.cWorkTables[4]='IMP_MAST'
    this.cWorkTables[5]='IMP_DETT'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ANTIPCON=space(1)
      .w_ELCODMOD=space(10)
      .w_MODESCRI=space(60)
      .w_ELCONTRA=space(10)
      .w_ELCODCLI=space(15)
      .w_ANDESCRI=space(50)
      .w_CODESCON=space(50)
      .w_ELCODIMP=space(10)
      .w_IMDESCRI=space(50)
      .w_ELCODCOM=0
      .w_IMDESCON=space(50)
      .w_CPROWORD=0
      .w_ELRINNOV_1=0
      .w_OKBUTTON=space(10)
      .w_ELDATFIN=ctod("  /  /  ")
      .w_ELDATINI_1=ctod("  /  /  ")
      .w_ELRINNOV=0
      .w_ELDATFIN_1=ctod("  /  /  ")
      .w_ELPREZZO=0
      .w_RICPE1=0
      .w_RICVA1=0
      .w_ARROT1=0
      .w_VALORIN=0
      .w_ARROT2=0
      .w_VALOR2IN=0
      .w_ARROT3=0
      .w_VALOR3IN=0
      .w_ARROT4=0
      .w_VALOFIN=0
      .w_FLDATFIS=space(1)
      .w_ELCODMOD=oParentObject.w_ELCODMOD
      .w_ELCONTRA=oParentObject.w_ELCONTRA
      .w_ELCODIMP=oParentObject.w_ELCODIMP
      .w_ELCODCOM=oParentObject.w_ELCODCOM
      .w_ELRINNOV_1=oParentObject.w_ELRINNOV_1
      .w_OKBUTTON=oParentObject.w_OKBUTTON
      .w_ELDATFIN=oParentObject.w_ELDATFIN
      .w_ELDATINI_1=oParentObject.w_ELDATINI_1
      .w_ELRINNOV=oParentObject.w_ELRINNOV
      .w_ELDATFIN_1=oParentObject.w_ELDATFIN_1
      .w_ELPREZZO=oParentObject.w_ELPREZZO
      .w_RICPE1=oParentObject.w_RICPE1
      .w_RICVA1=oParentObject.w_RICVA1
      .w_ARROT1=oParentObject.w_ARROT1
      .w_VALORIN=oParentObject.w_VALORIN
      .w_ARROT2=oParentObject.w_ARROT2
      .w_VALOR2IN=oParentObject.w_VALOR2IN
      .w_ARROT3=oParentObject.w_ARROT3
      .w_VALOR3IN=oParentObject.w_VALOR3IN
      .w_ARROT4=oParentObject.w_ARROT4
        .w_ANTIPCON = "C"
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ELCODMOD))
          .link_1_8('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_ELCONTRA))
          .link_1_10('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_ELCODCLI))
          .link_1_11('Full')
        endif
        .DoRTCalc(6,8,.f.)
        if not(empty(.w_ELCODIMP))
          .link_1_14('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_ELCODCOM))
          .link_1_16('Full')
        endif
          .DoRTCalc(11,13,.f.)
        .w_OKBUTTON = .T.
          .DoRTCalc(15,28,.f.)
        .w_VALOFIN = MAX(.w_VALORIN, .w_VALOR2IN, .w_VALOR3IN)
    endwith
    this.DoRTCalc(30,30,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ELCODMOD=.w_ELCODMOD
      .oParentObject.w_ELCONTRA=.w_ELCONTRA
      .oParentObject.w_ELCODIMP=.w_ELCODIMP
      .oParentObject.w_ELCODCOM=.w_ELCODCOM
      .oParentObject.w_ELRINNOV_1=.w_ELRINNOV_1
      .oParentObject.w_OKBUTTON=.w_OKBUTTON
      .oParentObject.w_ELDATFIN=.w_ELDATFIN
      .oParentObject.w_ELDATINI_1=.w_ELDATINI_1
      .oParentObject.w_ELRINNOV=.w_ELRINNOV
      .oParentObject.w_ELDATFIN_1=.w_ELDATFIN_1
      .oParentObject.w_ELPREZZO=.w_ELPREZZO
      .oParentObject.w_RICPE1=.w_RICPE1
      .oParentObject.w_RICVA1=.w_RICVA1
      .oParentObject.w_ARROT1=.w_ARROT1
      .oParentObject.w_VALORIN=.w_VALORIN
      .oParentObject.w_ARROT2=.w_ARROT2
      .oParentObject.w_VALOR2IN=.w_VALOR2IN
      .oParentObject.w_ARROT3=.w_ARROT3
      .oParentObject.w_VALOR3IN=.w_VALOR3IN
      .oParentObject.w_ARROT4=.w_ARROT4
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_8('Full')
        .DoRTCalc(3,3,.t.)
          .link_1_10('Full')
          .link_1_11('Full')
        .DoRTCalc(6,7,.t.)
          .link_1_14('Full')
        .DoRTCalc(9,9,.t.)
          .link_1_16('Full')
        .DoRTCalc(11,17,.t.)
        if .o_ELDATFIN<>.w_ELDATFIN
            .w_ELDATFIN_1 = DATE( YEAR( .w_ELDATFIN ) + 1, 12, 31 )
        endif
        .DoRTCalc(19,28,.t.)
            .w_VALOFIN = MAX(.w_VALORIN, .w_VALOR2IN, .w_VALOR3IN)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(30,30,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRICPE1_1_36.enabled = this.oPgFrm.Page1.oPag.oRICPE1_1_36.mCond()
    this.oPgFrm.Page1.oPag.oRICVA1_1_37.enabled = this.oPgFrm.Page1.oPag.oRICVA1_1_37.mCond()
    this.oPgFrm.Page1.oPag.oARROT1_1_38.enabled = this.oPgFrm.Page1.oPag.oARROT1_1_38.mCond()
    this.oPgFrm.Page1.oPag.oVALORIN_1_39.enabled = this.oPgFrm.Page1.oPag.oVALORIN_1_39.mCond()
    this.oPgFrm.Page1.oPag.oARROT2_1_40.enabled = this.oPgFrm.Page1.oPag.oARROT2_1_40.mCond()
    this.oPgFrm.Page1.oPag.oVALOR2IN_1_41.enabled = this.oPgFrm.Page1.oPag.oVALOR2IN_1_41.mCond()
    this.oPgFrm.Page1.oPag.oARROT3_1_42.enabled = this.oPgFrm.Page1.oPag.oARROT3_1_42.mCond()
    this.oPgFrm.Page1.oPag.oVALOR3IN_1_43.enabled = this.oPgFrm.Page1.oPag.oVALOR3IN_1_43.mCond()
    this.oPgFrm.Page1.oPag.oARROT4_1_44.enabled = this.oPgFrm.Page1.oPag.oARROT4_1_44.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oRICPE1_1_36.visible=!this.oPgFrm.Page1.oPag.oRICPE1_1_36.mHide()
    this.oPgFrm.Page1.oPag.oRICVA1_1_37.visible=!this.oPgFrm.Page1.oPag.oRICVA1_1_37.mHide()
    this.oPgFrm.Page1.oPag.oARROT1_1_38.visible=!this.oPgFrm.Page1.oPag.oARROT1_1_38.mHide()
    this.oPgFrm.Page1.oPag.oVALORIN_1_39.visible=!this.oPgFrm.Page1.oPag.oVALORIN_1_39.mHide()
    this.oPgFrm.Page1.oPag.oARROT2_1_40.visible=!this.oPgFrm.Page1.oPag.oARROT2_1_40.mHide()
    this.oPgFrm.Page1.oPag.oVALOR2IN_1_41.visible=!this.oPgFrm.Page1.oPag.oVALOR2IN_1_41.mHide()
    this.oPgFrm.Page1.oPag.oARROT3_1_42.visible=!this.oPgFrm.Page1.oPag.oARROT3_1_42.mHide()
    this.oPgFrm.Page1.oPag.oVALOR3IN_1_43.visible=!this.oPgFrm.Page1.oPag.oVALOR3IN_1_43.mHide()
    this.oPgFrm.Page1.oPag.oARROT4_1_44.visible=!this.oPgFrm.Page1.oPag.oARROT4_1_44.mHide()
    this.oPgFrm.Page1.oPag.oVALOFIN_1_45.visible=!this.oPgFrm.Page1.oPag.oVALOFIN_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ELCODMOD
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_ELCODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_ELCODMOD)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODMOD = NVL(_Link_.MOCODICE,space(10))
      this.w_MODESCRI = NVL(_Link_.MODESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODMOD = space(10)
      endif
      this.w_MODESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCONTRA
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON,COCODCON";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_ELCONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_ELCONTRA)
            select COSERIAL,CODESCON,COCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCONTRA = NVL(_Link_.COSERIAL,space(10))
      this.w_CODESCON = NVL(_Link_.CODESCON,space(50))
      this.w_ELCODCLI = NVL(_Link_.COCODCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ELCONTRA = space(10)
      endif
      this.w_CODESCON = space(50)
      this.w_ELCODCLI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODCLI
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ELCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ANTIPCON;
                       ,'ANCODICE',this.w_ELCODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODCLI = space(15)
      endif
      this.w_ANDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODIMP
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_ELCODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_ELCODIMP)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODIMP = NVL(_Link_.IMCODICE,space(10))
      this.w_IMDESCRI = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODIMP = space(10)
      endif
      this.w_IMDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODCOM
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,CPROWORD,IMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_ELCODCOM);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_ELCODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_ELCODIMP;
                       ,'CPROWNUM',this.w_ELCODCOM)
            select IMCODICE,CPROWNUM,CPROWORD,IMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODCOM = NVL(_Link_.CPROWNUM,0)
      this.w_CPROWORD = NVL(_Link_.CPROWORD,0)
      this.w_IMDESCON = NVL(_Link_.IMDESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODCOM = 0
      endif
      this.w_CPROWORD = 0
      this.w_IMDESCON = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oELCODMOD_1_8.value==this.w_ELCODMOD)
      this.oPgFrm.Page1.oPag.oELCODMOD_1_8.value=this.w_ELCODMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESCRI_1_9.value==this.w_MODESCRI)
      this.oPgFrm.Page1.oPag.oMODESCRI_1_9.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oELCONTRA_1_10.value==this.w_ELCONTRA)
      this.oPgFrm.Page1.oPag.oELCONTRA_1_10.value=this.w_ELCONTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODCLI_1_11.value==this.w_ELCODCLI)
      this.oPgFrm.Page1.oPag.oELCODCLI_1_11.value=this.w_ELCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_12.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_12.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_1_13.value==this.w_CODESCON)
      this.oPgFrm.Page1.oPag.oCODESCON_1_13.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODIMP_1_14.value==this.w_ELCODIMP)
      this.oPgFrm.Page1.oPag.oELCODIMP_1_14.value=this.w_ELCODIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCRI_1_15.value==this.w_IMDESCRI)
      this.oPgFrm.Page1.oPag.oIMDESCRI_1_15.value=this.w_IMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCON_1_17.value==this.w_IMDESCON)
      this.oPgFrm.Page1.oPag.oIMDESCON_1_17.value=this.w_IMDESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCPROWORD_1_18.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oCPROWORD_1_18.value=this.w_CPROWORD
    endif
    if not(this.oPgFrm.Page1.oPag.oELRINNOV_1_1_19.value==this.w_ELRINNOV_1)
      this.oPgFrm.Page1.oPag.oELRINNOV_1_1_19.value=this.w_ELRINNOV_1
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATFIN_1_23.value==this.w_ELDATFIN)
      this.oPgFrm.Page1.oPag.oELDATFIN_1_23.value=this.w_ELDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATINI_1_1_24.value==this.w_ELDATINI_1)
      this.oPgFrm.Page1.oPag.oELDATINI_1_1_24.value=this.w_ELDATINI_1
    endif
    if not(this.oPgFrm.Page1.oPag.oELRINNOV_1_25.value==this.w_ELRINNOV)
      this.oPgFrm.Page1.oPag.oELRINNOV_1_25.value=this.w_ELRINNOV
    endif
    if not(this.oPgFrm.Page1.oPag.oELDATFIN_1_1_30.value==this.w_ELDATFIN_1)
      this.oPgFrm.Page1.oPag.oELDATFIN_1_1_30.value=this.w_ELDATFIN_1
    endif
    if not(this.oPgFrm.Page1.oPag.oRICPE1_1_36.value==this.w_RICPE1)
      this.oPgFrm.Page1.oPag.oRICPE1_1_36.value=this.w_RICPE1
    endif
    if not(this.oPgFrm.Page1.oPag.oRICVA1_1_37.value==this.w_RICVA1)
      this.oPgFrm.Page1.oPag.oRICVA1_1_37.value=this.w_RICVA1
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT1_1_38.value==this.w_ARROT1)
      this.oPgFrm.Page1.oPag.oARROT1_1_38.value=this.w_ARROT1
    endif
    if not(this.oPgFrm.Page1.oPag.oVALORIN_1_39.value==this.w_VALORIN)
      this.oPgFrm.Page1.oPag.oVALORIN_1_39.value=this.w_VALORIN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT2_1_40.value==this.w_ARROT2)
      this.oPgFrm.Page1.oPag.oARROT2_1_40.value=this.w_ARROT2
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOR2IN_1_41.value==this.w_VALOR2IN)
      this.oPgFrm.Page1.oPag.oVALOR2IN_1_41.value=this.w_VALOR2IN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT3_1_42.value==this.w_ARROT3)
      this.oPgFrm.Page1.oPag.oARROT3_1_42.value=this.w_ARROT3
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOR3IN_1_43.value==this.w_VALOR3IN)
      this.oPgFrm.Page1.oPag.oVALOR3IN_1_43.value=this.w_VALOR3IN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT4_1_44.value==this.w_ARROT4)
      this.oPgFrm.Page1.oPag.oARROT4_1_44.value=this.w_ARROT4
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOFIN_1_45.value==this.w_VALOFIN)
      this.oPgFrm.Page1.oPag.oVALOFIN_1_45.value=this.w_VALOFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_VALOR2IN>.w_VALORIN OR .w_VALOR2IN=0)  and not(g_APPLICATION <>"ADHOC REVOLUTION")  and (.w_VALORIN<>0 AND .w_ELPREZZO <> 0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVALOR2IN_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_VALOR3IN>.w_VALORIN AND .w_VALOR3IN>.w_VALOR2IN)  OR .w_VALOR3IN=0)  and not(g_APPLICATION <>"ADHOC REVOLUTION")  and (.w_VALORIN<>0 AND .w_VALOR2IN<>0 AND .w_ELPREZZO <> 0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVALOR3IN_1_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_kd4
      if i_bRes and this.w_ELDATFIN_1 < this.w_ELDATINI_1
          i_bnoChk = .f.
      		i_bRes = .f.
      		i_cErrorMsg = Ah_MsgFormat("La data di fine validit� non pu� essere anteriore a quella di inizio validit�")
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ELDATFIN = this.w_ELDATFIN
    return

enddefine

* --- Define pages as container
define class tgsag_kd4Pag1 as StdContainer
  Width  = 793
  height = 410
  stdWidth  = 793
  stdheight = 410
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELCODMOD_1_8 as StdField with uid="MERUHLPFQO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ELCODMOD", cQueryName = "ELCODMOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Modello elementi",;
    HelpContextID = 56010358,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=140, Top=10, InputMask=replicate('X',10), cLinkFile="MOD_ELEM", oKey_1_1="MOCODICE", oKey_1_2="this.w_ELCODMOD"

  func oELCODMOD_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMODESCRI_1_9 as StdField with uid="ZYEUQNAHCA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 59731215,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=262, Top=10, InputMask=replicate('X',60)

  add object oELCONTRA_1_10 as StdField with uid="NLCCMFGETN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ELCONTRA", cQueryName = "ELCONTRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 71915911,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=140, Top=59, InputMask=replicate('X',10), cLinkFile="CON_TRAS", oKey_1_1="COSERIAL", oKey_1_2="this.w_ELCONTRA"

  func oELCONTRA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oELCODCLI_1_11 as StdField with uid="YBSAIWSLXR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ELCODCLI", cQueryName = "ELCODCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 223782513,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=140, Top=35, InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ANTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_ELCODCLI"

  func oELCODCLI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oANDESCRI_1_12 as StdField with uid="XPBCGXWLCY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 59730767,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=262, Top=35, InputMask=replicate('X',50)

  add object oCODESCON_1_13 as StdField with uid="EPBNBTZORX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208704396,;
   bGlobalFont=.t.,;
    Height=21, Width=396, Left=262, Top=59, InputMask=replicate('X',50)

  add object oELCODIMP_1_14 as StdField with uid="AIJTDINYQW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ELCODIMP", cQueryName = "ELCODIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 123119210,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=140, Top=83, InputMask=replicate('X',10), cLinkFile="IMP_MAST", oKey_1_1="IMCODICE", oKey_1_2="this.w_ELCODIMP"

  func oELCODIMP_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_ELCODCOM)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oIMDESCRI_1_15 as StdField with uid="ZOFZIHDTJJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_IMDESCRI", cQueryName = "IMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 59730639,;
   bGlobalFont=.t.,;
    Height=21, Width=396, Left=262, Top=83, InputMask=replicate('X',50)

  add object oIMDESCON_1_17 as StdField with uid="GXSVVWGGFA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_IMDESCON", cQueryName = "IMDESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208704812,;
   bGlobalFont=.t.,;
    Height=21, Width=396, Left=262, Top=108, InputMask=replicate('X',50)

  add object oCPROWORD_1_18 as StdField with uid="VKYQTIXKQG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CPROWORD", cQueryName = "CPROWORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 265964906,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=140, Top=108

  add object oELRINNOV_1_1_19 as StdField with uid="JLVZKHZOGH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ELRINNOV_1", cQueryName = "ELRINNOV_1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Nuovo numero di rinnovi",;
    HelpContextID = 29065076,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=370, Top=218

  add object oELDATFIN_1_23 as StdField with uid="HRTTZDVOIN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ELDATFIN", cQueryName = "ELDATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 157587052,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=140, Top=157

  add object oELDATINI_1_1_24 as StdField with uid="BZAHFKOTIW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ELDATINI_1", cQueryName = "ELDATINI_1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 107241345,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=141, Top=216

  add object oELRINNOV_1_25 as StdField with uid="SJWIHHMMLC",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ELRINNOV", cQueryName = "ELRINNOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Nuovo numero di rinnovi",;
    HelpContextID = 29079140,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=370, Top=158

  add object oELDATFIN_1_1_30 as StdField with uid="NBJVYOVJOT",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ELDATFIN_1", cQueryName = "ELDATFIN_1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 157572988,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=141, Top=242


  add object oBtn_1_33 as StdButton with uid="UUNSDLSMLD",left=686, top=358, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 82425626;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_34 as StdButton with uid="TKCMXZXILU",left=739, top=358, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 75136954;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRICPE1_1_36 as StdField with uid="JCWLSRROBT",rtseq=20,rtrep=.f.,;
    cFormVar = "w_RICPE1", cQueryName = "RICPE1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di ricalcolo del contratto",;
    HelpContextID = 256223466,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=138, Top=307, cSayPict='"999.99"', cGetPict='"999.99"'

  func oRICPE1_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RICVA1=0 AND .w_ELPREZZO <> 0)
    endwith
   endif
  endfunc

  func oRICPE1_1_36.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oRICVA1_1_37 as StdField with uid="RAFGTLVTMZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_RICVA1", cQueryName = "RICVA1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ricalcolo in valore del contratto",;
    HelpContextID = 260024554,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=138, Top=332, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oRICVA1_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RICPE1=0 AND .w_ELPREZZO <> 0)
    endwith
   endif
  endfunc

  func oRICVA1_1_37.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oARROT1_1_38 as StdField with uid="RVCKWZFISF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ARROT1", cQueryName = "ARROT1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento al valore monetario desiderato (decine, centinaia, ecc., 0=nessun arrotondamento)",;
    HelpContextID = 240496890,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=276, Top=307, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oARROT1_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELPREZZO <> 0)
    endwith
   endif
  endfunc

  func oARROT1_1_38.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oVALORIN_1_39 as StdField with uid="MHNGXGXYBX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_VALORIN", cQueryName = "VALORIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore fino al quale si vuole arrotondare",;
    HelpContextID = 108404906,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=408, Top=307, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oVALORIN_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELPREZZO <> 0)
    endwith
   endif
  endfunc

  func oVALORIN_1_39.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oARROT2_1_40 as StdField with uid="RHGJBSFYAZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ARROT2", cQueryName = "ARROT2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento al valore monetario desiderato (decine, centinaia, ecc., 0=nessun arrotondamento)",;
    HelpContextID = 223719674,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=276, Top=332, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oARROT2_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0 AND .w_ELPREZZO <> 0)
    endwith
   endif
  endfunc

  func oARROT2_1_40.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oVALOR2IN_1_41 as StdField with uid="NMSIQUJYRO",rtseq=25,rtrep=.f.,;
    cFormVar = "w_VALOR2IN", cQueryName = "VALOR2IN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore fino al quale si vuole arrotondare",;
    HelpContextID = 42590116,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=408, Top=332, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oVALOR2IN_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0 AND .w_ELPREZZO <> 0)
    endwith
   endif
  endfunc

  func oVALOR2IN_1_41.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oVALOR2IN_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VALOR2IN>.w_VALORIN OR .w_VALOR2IN=0)
    endwith
    return bRes
  endfunc

  add object oARROT3_1_42 as StdField with uid="HXTPQSAAPP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ARROT3", cQueryName = "ARROT3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento al valore monetario desiderato (decine, centinaia, ecc., 0=nessun arrotondamento)",;
    HelpContextID = 206942458,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=276, Top=357, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oARROT3_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0 AND .w_VALOR2IN<>0 AND .w_ELPREZZO <> 0)
    endwith
   endif
  endfunc

  func oARROT3_1_42.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oVALOR3IN_1_43 as StdField with uid="GVJHJMJPXN",rtseq=27,rtrep=.f.,;
    cFormVar = "w_VALOR3IN", cQueryName = "VALOR3IN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore fino al quale si vuole arrotondare",;
    HelpContextID = 59367332,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=408, Top=357, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oVALOR3IN_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0 AND .w_VALOR2IN<>0 AND .w_ELPREZZO <> 0)
    endwith
   endif
  endfunc

  func oVALOR3IN_1_43.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  func oVALOR3IN_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_VALOR3IN>.w_VALORIN AND .w_VALOR3IN>.w_VALOR2IN)  OR .w_VALOR3IN=0)
    endwith
    return bRes
  endfunc

  add object oARROT4_1_44 as StdField with uid="RPOAXMKGRV",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ARROT4", cQueryName = "ARROT4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento per importi diversi dai precedenti (0 = nessun arrotondamento)",;
    HelpContextID = 190165242,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=276, Top=382, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oARROT4_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELPREZZO <> 0)
    endwith
   endif
  endfunc

  func oARROT4_1_44.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oVALOFIN_1_45 as StdField with uid="ZZMRLWXAIF",rtseq=29,rtrep=.f.,;
    cFormVar = "w_VALOFIN", cQueryName = "VALOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 120987818,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=544, Top=382, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oVALOFIN_1_45.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="HNOXWDPZKG",Visible=.t., Left=77, Top=11,;
    Alignment=1, Width=56, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="BDMDZSQESG",Visible=.t., Left=91, Top=35,;
    Alignment=1, Width=42, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="OFOGPQSPZZ",Visible=.t., Left=80, Top=59,;
    Alignment=1, Width=53, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="BDLWCJFOUX",Visible=.t., Left=22, Top=85,;
    Alignment=1, Width=111, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="ZPNBJHZOTQ",Visible=.t., Left=23, Top=110,;
    Alignment=1, Width=110, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="KDADTFOUQF",Visible=.t., Left=238, Top=220,;
    Alignment=1, Width=125, Height=18,;
    Caption="Numero rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="UDJGUUYTSX",Visible=.t., Left=40, Top=160,;
    Alignment=0, Width=93, Height=18,;
    Caption="Data fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="CVNOEMDDVQ",Visible=.t., Left=8, Top=137,;
    Alignment=0, Width=174, Height=18,;
    Caption="Elemento contratto da rinnovare"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="EXUOEWTPOF",Visible=.t., Left=240, Top=160,;
    Alignment=1, Width=125, Height=18,;
    Caption="Numero rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="YBCPXKOSRE",Visible=.t., Left=35, Top=214,;
    Alignment=1, Width=101, Height=18,;
    Caption="Data inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="FUPEIXOGCP",Visible=.t., Left=9, Top=194,;
    Alignment=0, Width=140, Height=18,;
    Caption="Nuovo elemento contratto"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="IBOTLAGOQC",Visible=.t., Left=18, Top=245,;
    Alignment=1, Width=116, Height=18,;
    Caption="Data fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="GOPKQRRHJT",Visible=.t., Left=11, Top=288,;
    Alignment=0, Width=252, Height=15,;
    Caption="Parametri di calcolo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="NYHQXBQHJL",Visible=.t., Left=8, Top=307,;
    Alignment=1, Width=128, Height=15,;
    Caption="Percentuale ricalcolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="SVPRRTBRIR",Visible=.t., Left=8, Top=332,;
    Alignment=1, Width=128, Height=15,;
    Caption="Ricalcolo in valore:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="JEFECLEULP",Visible=.t., Left=278, Top=288,;
    Alignment=0, Width=104, Height=15,;
    Caption="Arrotondamenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="MHEWZFMOQE",Visible=.t., Left=412, Top=289,;
    Alignment=0, Width=146, Height=15,;
    Caption="Per importi fino a"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="HFEZTPBADH",Visible=.t., Left=406, Top=382,;
    Alignment=1, Width=137, Height=15,;
    Caption="Per importi superiori a:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oBox_1_31 as StdBox with uid="MKNFJZNTRM",left=5, top=133, width=653,height=55

  add object oBox_1_32 as StdBox with uid="NXLIVMDQIJ",left=5, top=192, width=653,height=78
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kd4','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
