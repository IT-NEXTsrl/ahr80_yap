* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bsc                                                        *
*              Carica salva dati esterni agenda                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-09                                                      *
* Last revis.: 2010-04-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPATH,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bsc",oParentObject,m.pPATH,m.pTIPO)
return(i_retval)

define class tgsag_bsc as StdBatch
  * --- Local variables
  pPATH = space(254)
  pTIPO = space(1)
  w_CURSORE = 0
  w_DEDRIVER = space(10)
  w_DEDESCRI = space(50)
  w_DE__TIPO = space(10)
  w_DEROUTIN = space(20)
  w_TETIPEVE = space(10)
  w_TEDESCRI = space(50)
  w_TE__PATH = space(254)
  w_TESRCPAT = space(20)
  w_TEDRIVER = space(10)
  w_TECLADOC = space(15)
  w_TEPERSON = space(5)
  w_TEGRUPPO = space(5)
  w_TEPADEVE = space(10)
  w_TEFLGANT = space(1)
  w_TEFILBMP = space(100)
  w_TETIPDIR = space(1)
  w_TE__HIDE = space(1)
  * --- WorkFile variables
  DRVEVENT_idx=0
  TIPEVENT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pTIPO="DE"
        * --- Caricamento degli stati dichiarati in SOStitutiva
        CREATE CURSOR TEMPO (DEDRIVER C(10),DEDESCRI C(50),DE__TIPO C(1),DEROUTIN C(20))
        APPEND FROM (this.pPATH) TYPE DELIMITED
        this.w_CURSORE = SELECT()
        * --- Inserisco nell'archivio
        SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN
        this.w_DEDRIVER = TEMPO.DEDRIVER
        this.w_DEDESCRI = TEMPO.DEDESCRI
        this.w_DE__TIPO = TEMPO.DE__TIPO
        this.w_DEROUTIN = TEMPO.DEROUTIN
        * --- Try
        local bErr_036D6FA0
        bErr_036D6FA0=bTrsErr
        this.Try_036D6FA0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into DRVEVENT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DRVEVENT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DRVEVENT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DRVEVENT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DEDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_DEDESCRI),'DRVEVENT','DEDESCRI');
            +",DE__TIPO ="+cp_NullLink(cp_ToStrODBC(this.w_DE__TIPO),'DRVEVENT','DE__TIPO');
            +",DEROUTIN ="+cp_NullLink(cp_ToStrODBC(this.w_DEROUTIN),'DRVEVENT','DEROUTIN');
                +i_ccchkf ;
            +" where ";
                +"DEDRIVER = "+cp_ToStrODBC(this.w_DEDRIVER);
                   )
          else
            update (i_cTable) set;
                DEDESCRI = this.w_DEDESCRI;
                ,DE__TIPO = this.w_DE__TIPO;
                ,DEROUTIN = this.w_DEROUTIN;
                &i_ccchkf. ;
             where;
                DEDRIVER = this.w_DEDRIVER;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if i_Rows=0
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_036D6FA0
        * --- End
        ENDSCAN
      case this.pTIPO="TE"
        * --- Caricamento degli stati dichiarati in SOStitutiva
         
 CREATE CURSOR TEMPO (TETIPEVE C(10),TEDESCRI C(50),TE__PATH C(254),TESRCPAT C(20),TEDRIVER C(10),TECLADOC C(15),TEPERSON C(5),TEGRUPPO C(5),TEPADEVE C(10),; 
 TEFLGANT C(1),TEFILBMP C(100),TETIPDIR C(1),TE__HIDE C(1))
        APPEND FROM (this.pPATH) TYPE DELIMITED
        this.w_CURSORE = SELECT()
        * --- Inserisco nell'archivio
        SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN 
        this.w_TETIPEVE = TEMPO.TETIPEVE
        this.w_TEDESCRI = TEMPO.TEDESCRI
        this.w_TE__PATH = TEMPO.TE__PATH
        this.w_TESRCPAT = TEMPO.TESRCPAT
        this.w_TEDRIVER = TEMPO.TEDRIVER
        this.w_TECLADOC = TEMPO.TECLADOC
        this.w_TEPERSON = TEMPO.TEPERSON
        this.w_TEGRUPPO = TEMPO.TEGRUPPO
        this.w_TEPADEVE = TEMPO.TEPADEVE
        this.w_TEFLGANT = TEMPO.TEFLGANT
        this.w_TEFILBMP = TEMPO.TEFILBMP
        this.w_TETIPDIR = TEMPO.TETIPDIR
        this.w_TE__HIDE = TEMPO.TE__HIDE
        * --- Try
        local bErr_03A44EE8
        bErr_03A44EE8=bTrsErr
        this.Try_03A44EE8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into TIPEVENT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TIPEVENT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIPEVENT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TIPEVENT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TEDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_TEDESCRI),'TIPEVENT','TEDESCRI');
            +",TE__PATH ="+cp_NullLink(cp_ToStrODBC(this.w_TE__PATH),'TIPEVENT','TE__PATH');
            +",TESRCPAT ="+cp_NullLink(cp_ToStrODBC(this.w_TESRCPAT),'TIPEVENT','TESRCPAT');
            +",TEDRIVER ="+cp_NullLink(cp_ToStrODBC(this.w_TEDRIVER),'TIPEVENT','TEDRIVER');
            +",TECLADOC ="+cp_NullLink(cp_ToStrODBC(this.w_TECLADOC),'TIPEVENT','TECLADOC');
            +",TEPERSON ="+cp_NullLink(cp_ToStrODBC(this.w_TEPERSON),'TIPEVENT','TEPERSON');
            +",TEGRUPPO ="+cp_NullLink(cp_ToStrODBC(this.w_TEGRUPPO),'TIPEVENT','TEGRUPPO');
            +",TEPADEVE ="+cp_NullLink(cp_ToStrODBC(this.w_TEPADEVE),'TIPEVENT','TEPADEVE');
            +",TEFLGANT ="+cp_NullLink(cp_ToStrODBC(this.w_TEFLGANT),'TIPEVENT','TEFLGANT');
            +",TEFILBMP ="+cp_NullLink(cp_ToStrODBC(this.w_TEFILBMP),'TIPEVENT','TEFILBMP');
            +",TETIPDIR ="+cp_NullLink(cp_ToStrODBC(this.w_TETIPDIR),'TIPEVENT','TETIPDIR');
            +",TE__HIDE ="+cp_NullLink(cp_ToStrODBC(this.w_TE__HIDE),'TIPEVENT','TE__HIDE');
                +i_ccchkf ;
            +" where ";
                +"TETIPEVE = "+cp_ToStrODBC(this.w_TETIPEVE);
                   )
          else
            update (i_cTable) set;
                TEDESCRI = this.w_TEDESCRI;
                ,TE__PATH = this.w_TE__PATH;
                ,TESRCPAT = this.w_TESRCPAT;
                ,TEDRIVER = this.w_TEDRIVER;
                ,TECLADOC = this.w_TECLADOC;
                ,TEPERSON = this.w_TEPERSON;
                ,TEGRUPPO = this.w_TEGRUPPO;
                ,TEPADEVE = this.w_TEPADEVE;
                ,TEFLGANT = this.w_TEFLGANT;
                ,TEFILBMP = this.w_TEFILBMP;
                ,TETIPDIR = this.w_TETIPDIR;
                ,TE__HIDE = this.w_TE__HIDE;
                &i_ccchkf. ;
             where;
                TETIPEVE = this.w_TETIPEVE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if i_Rows=0
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_03A44EE8
        * --- End
        ENDSCAN
    endcase
  endproc
  proc Try_036D6FA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DRVEVENT
    i_nConn=i_TableProp[this.DRVEVENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DRVEVENT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DRVEVENT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DEDRIVER"+",DEDESCRI"+",DE__TIPO"+",DEROUTIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_DEDRIVER),'DRVEVENT','DEDRIVER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DEDESCRI),'DRVEVENT','DEDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DE__TIPO),'DRVEVENT','DE__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DEROUTIN),'DRVEVENT','DEROUTIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DEDRIVER',this.w_DEDRIVER,'DEDESCRI',this.w_DEDESCRI,'DE__TIPO',this.w_DE__TIPO,'DEROUTIN',this.w_DEROUTIN)
      insert into (i_cTable) (DEDRIVER,DEDESCRI,DE__TIPO,DEROUTIN &i_ccchkf. );
         values (;
           this.w_DEDRIVER;
           ,this.w_DEDESCRI;
           ,this.w_DE__TIPO;
           ,this.w_DEROUTIN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03A44EE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TIPEVENT
    i_nConn=i_TableProp[this.TIPEVENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIPEVENT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIPEVENT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TETIPEVE"+",TEDESCRI"+",TE__PATH"+",TESRCPAT"+",TEDRIVER"+",TECLADOC"+",TEPERSON"+",TEGRUPPO"+",TEPADEVE"+",TEFLGANT"+",TEFILBMP"+",TETIPDIR"+",TE__HIDE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TETIPEVE),'TIPEVENT','TETIPEVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TEDESCRI),'TIPEVENT','TEDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TE__PATH),'TIPEVENT','TE__PATH');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TESRCPAT),'TIPEVENT','TESRCPAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TEDRIVER),'TIPEVENT','TEDRIVER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TECLADOC),'TIPEVENT','TECLADOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TEPERSON),'TIPEVENT','TEPERSON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TEGRUPPO),'TIPEVENT','TEGRUPPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TEPADEVE),'TIPEVENT','TEPADEVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TEFLGANT),'TIPEVENT','TEFLGANT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TEFILBMP),'TIPEVENT','TEFILBMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TETIPDIR),'TIPEVENT','TETIPDIR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TE__HIDE),'TIPEVENT','TE__HIDE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TETIPEVE',this.w_TETIPEVE,'TEDESCRI',this.w_TEDESCRI,'TE__PATH',this.w_TE__PATH,'TESRCPAT',this.w_TESRCPAT,'TEDRIVER',this.w_TEDRIVER,'TECLADOC',this.w_TECLADOC,'TEPERSON',this.w_TEPERSON,'TEGRUPPO',this.w_TEGRUPPO,'TEPADEVE',this.w_TEPADEVE,'TEFLGANT',this.w_TEFLGANT,'TEFILBMP',this.w_TEFILBMP,'TETIPDIR',this.w_TETIPDIR)
      insert into (i_cTable) (TETIPEVE,TEDESCRI,TE__PATH,TESRCPAT,TEDRIVER,TECLADOC,TEPERSON,TEGRUPPO,TEPADEVE,TEFLGANT,TEFILBMP,TETIPDIR,TE__HIDE &i_ccchkf. );
         values (;
           this.w_TETIPEVE;
           ,this.w_TEDESCRI;
           ,this.w_TE__PATH;
           ,this.w_TESRCPAT;
           ,this.w_TEDRIVER;
           ,this.w_TECLADOC;
           ,this.w_TEPERSON;
           ,this.w_TEGRUPPO;
           ,this.w_TEPADEVE;
           ,this.w_TEFLGANT;
           ,this.w_TEFILBMP;
           ,this.w_TETIPDIR;
           ,this.w_TE__HIDE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pPATH,pTIPO)
    this.pPATH=pPATH
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DRVEVENT'
    this.cWorkTables[2]='TIPEVENT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPATH,pTIPO"
endproc
