* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_kce                                                        *
*              Cruscotto eventi                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-16                                                      *
* Last revis.: 2013-05-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsfa_kce",oParentObject))

* --- Class definition
define class tgsfa_kce as StdForm
  Top    = 8
  Left   = 10

  * --- Standard Properties
  Width  = 795
  Height = 567
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-15"
  HelpContextID=99254889
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=60

  * --- Constant Properties
  _IDX = 0
  TIPEVENT_IDX = 0
  OFF_NOMI_IDX = 0
  CAN_TIER_IDX = 0
  DIPENDEN_IDX = 0
  DRVEVENT_IDX = 0
  cPrg = "gsfa_kce"
  cComment = "Cruscotto eventi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOPE = space(1)
  o_TIPOPE = space(1)
  w_ROOT = space(10)
  w_TIPOTREE = space(1)
  o_TIPOTREE = space(1)
  w_EVPERSON = space(5)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_OREFIN = space(2)
  o_OREFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_EVGRUPPO = space(5)
  w_PERSON = space(5)
  w_GRUPPO = space(5)
  w_NOMINAT = space(15)
  w_TETIPEVE = space(10)
  w_TIPEVE = space(1)
  o_TIPEVE = space(1)
  w_TIPEVE1 = space(10)
  w_TIPDIR = space(1)
  o_TIPDIR = space(1)
  w_ZPERSON = space(10)
  w_ZGRUPPO = space(10)
  w_TIPDIR1 = space(10)
  w_ZNOMINAT = space(15)
  w_ZIDRICH = space(15)
  w_EVSERIAL = space(10)
  o_EVSERIAL = space(10)
  w_TEDRIVER = space(10)
  w_DE__TIPO = space(1)
  w_EV__ANNO = space(4)
  o_EV__ANNO = space(4)
  w_CURSORNA = space(10)
  w_DESROOT = space(50)
  w_STATO = space(1)
  w_NOMINA = space(15)
  w_RIFPER = space(254)
  w_CODPRA = space(15)
  w_EDATINI = ctot('')
  w_EDATFIN = ctot('')
  w_MINEFF = space(2)
  w_OB_TEST = ctod('  /  /  ')
  w_PATIPRIS = space(10)
  w_ORAEFF = ctot('')
  w_TIPPER = space(1)
  w_TIPGRU = space(1)
  w_FLANT = space(10)
  w_EVNOMINA = space(10)
  w_DATA_INI = ctot('')
  w_DATA_FIN = ctot('')
  w_DataIniTimer = ctot('')
  w_DurataOre = 0
  w_DurataMin = 0
  w_Attivo = space(1)
  w_RESET = .F.
  w_LOADMAIL = .F.
  w_HTML_URL = space(254)
  o_HTML_URL = space(254)
  w_STARIC = space(1)
  w_NOMINATIVO = space(15)
  o_NOMINATIVO = space(15)
  w_TIPRIC = space(1)
  o_TIPRIC = space(1)
  w_TIPRIC1 = space(10)
  w_EVSERDOC = space(10)
  w_LVLKEY = space(10)
  w_EVTIPEVE = space(10)
  w_FLIDPA = space(1)

  * --- Children pointers
  GSFA_ACE = .NULL.
  w_TREEVIEW = .NULL.
  w_EVENT = .NULL.
  w_Clock = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSFA_ACE additive
    with this
      .Pages(1).addobject("oPag","tgsfa_kcePag1","gsfa_kce",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oROOT_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSFA_ACE
    * --- Area Manuale = Init Page Frame
    * --- gsfa_kce
    * --- Imposta il Titolo della Finestra della Gestione in Base al tipo
    WITH THIS.PARENT
       DO CASE
             CASE Oparentobject = 'C'
                   .cComment = CP_TRANSLATE('Cruscotto eventi')
             CASE Oparentobject = 'I'
                   .cComment = CP_TRANSLATE('Cruscotto eventi per ID richiesta')
       ENDCASE
    ENDWIT
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TREEVIEW = this.oPgFrm.Pages(1).oPag.TREEVIEW
    this.w_EVENT = this.oPgFrm.Pages(1).oPag.EVENT
    this.w_Clock = this.oPgFrm.Pages(1).oPag.Clock
    DoDefault()
    proc Destroy()
      this.w_TREEVIEW = .NULL.
      this.w_EVENT = .NULL.
      this.w_Clock = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='TIPEVENT'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='DIPENDEN'
    this.cWorkTables[5]='DRVEVENT'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSFA_ACE = CREATEOBJECT('stdDynamicChild',this,'GSFA_ACE',this.oPgFrm.Page1.oPag.oLinkPC_1_50)
    this.GSFA_ACE.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSFA_ACE)
      this.GSFA_ACE.DestroyChildrenChain()
      this.GSFA_ACE=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_50')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSFA_ACE.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSFA_ACE.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSFA_ACE.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSFA_ACE.SetKey(;
            .w_EV__ANNO,"EV__ANNO";
            ,.w_EVSERIAL,"EVSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSFA_ACE.ChangeRow(this.cRowID+'      1',1;
             ,.w_EV__ANNO,"EV__ANNO";
             ,.w_EVSERIAL,"EVSERIAL";
             )
      .WriteTo_GSFA_ACE()
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSFA_ACE)
        i_f=.GSFA_ACE.BuildFilter()
        if !(i_f==.GSFA_ACE.cQueryFilter)
          i_fnidx=.GSFA_ACE.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSFA_ACE.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSFA_ACE.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSFA_ACE.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSFA_ACE.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSFA_ACE()
  if at('gsfa_ace',lower(this.GSFA_ACE.class))<>0
    if this.GSFA_ACE.w_HTML_URL<>this.w_HTML_URL
      this.GSFA_ACE.w_HTML_URL = this.w_HTML_URL
      this.GSFA_ACE.mCalc(.t.)
    endif
  endif
  return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSFA_ACE(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSFA_ACE.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSFA_ACE(i_ask)
    if this.GSFA_ACE.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
      cp_BeginTrs()
      this.GSFA_ACE.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOPE=space(1)
      .w_ROOT=space(10)
      .w_TIPOTREE=space(1)
      .w_EVPERSON=space(5)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_OREFIN=space(2)
      .w_MINFIN=space(2)
      .w_EVGRUPPO=space(5)
      .w_PERSON=space(5)
      .w_GRUPPO=space(5)
      .w_NOMINAT=space(15)
      .w_TETIPEVE=space(10)
      .w_TIPEVE=space(1)
      .w_TIPEVE1=space(10)
      .w_TIPDIR=space(1)
      .w_ZPERSON=space(10)
      .w_ZGRUPPO=space(10)
      .w_TIPDIR1=space(10)
      .w_ZNOMINAT=space(15)
      .w_ZIDRICH=space(15)
      .w_EVSERIAL=space(10)
      .w_TEDRIVER=space(10)
      .w_DE__TIPO=space(1)
      .w_EV__ANNO=space(4)
      .w_CURSORNA=space(10)
      .w_DESROOT=space(50)
      .w_STATO=space(1)
      .w_NOMINA=space(15)
      .w_RIFPER=space(254)
      .w_CODPRA=space(15)
      .w_EDATINI=ctot("")
      .w_EDATFIN=ctot("")
      .w_MINEFF=space(2)
      .w_OB_TEST=ctod("  /  /  ")
      .w_PATIPRIS=space(10)
      .w_ORAEFF=ctot("")
      .w_TIPPER=space(1)
      .w_TIPGRU=space(1)
      .w_FLANT=space(10)
      .w_EVNOMINA=space(10)
      .w_DATA_INI=ctot("")
      .w_DATA_FIN=ctot("")
      .w_DataIniTimer=ctot("")
      .w_DurataOre=0
      .w_DurataMin=0
      .w_Attivo=space(1)
      .w_RESET=.f.
      .w_LOADMAIL=.f.
      .w_HTML_URL=space(254)
      .w_STARIC=space(1)
      .w_NOMINATIVO=space(15)
      .w_TIPRIC=space(1)
      .w_TIPRIC1=space(10)
      .w_EVSERDOC=space(10)
      .w_LVLKEY=space(10)
      .w_EVTIPEVE=space(10)
      .w_FLIDPA=space(1)
        .w_TIPOPE = this.oParentObject
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ROOT))
          .link_1_2('Full')
        endif
        .w_TIPOTREE = iif(.w_TIPOPE='C','T','N')
      .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_EVPERSON))
          .link_1_7('Full')
        endif
          .DoRTCalc(5,6,.f.)
        .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (empty(.w_DATFIN) AND .w_OREFIN="23" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_EVGRUPPO))
          .link_1_14('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_PERSON))
          .link_1_15('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_GRUPPO))
          .link_1_16('Full')
        endif
        .w_NOMINAT = .w_NOMINATIVO
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_NOMINAT))
          .link_1_17('Full')
        endif
        .w_TETIPEVE = iif(.w_TIPOPE='C',.w_TREEVIEW.GETVAR('TETIPEVE'),' ')
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_TETIPEVE))
          .link_1_18('Full')
        endif
        .w_TIPEVE = iif(.w_TIPOTREE='T','D','T')
        .w_TIPEVE1 = IIF(.w_TIPEVE='T','',.w_TIPEVE)
        .w_TIPDIR = 'T'
        .w_ZPERSON = iif(.w_TIPOPE='C',' ',.w_TREEVIEW.GETVAR('PERSONA'))
        .w_ZGRUPPO = iif(.w_TIPOPE='C',' ',.w_TREEVIEW.GETVAR('GRUPPO'))
        .w_TIPDIR1 = IIF(.w_TIPDIR='T','',.w_TIPDIR)
        .w_ZNOMINAT = iif(.w_TIPOPE='C',' ',.w_TREEVIEW.GETVAR('NOMINAT'))
        .w_ZIDRICH = iif(.w_TIPOPE='C',' ',.w_TREEVIEW.GETVAR('IDRICH'))
      .oPgFrm.Page1.oPag.EVENT.Calculate(.w_TETIPEVE)
        .w_EVSERIAL = .w_event.GETVAR('EVSERIAL')
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_TEDRIVER))
          .link_1_30('Full')
        endif
          .DoRTCalc(26,26,.f.)
        .w_EV__ANNO = .w_event.GETVAR('EV__ANNO')
          .DoRTCalc(28,29,.f.)
        .w_STATO = .w_event.GETVAR('EV_STATO')
        .w_NOMINA = .w_event.GETVAR('EVNOMINA')
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_NOMINA))
          .link_1_45('Full')
        endif
        .w_RIFPER = .w_event.GETVAR('EVRIFPER')
        .w_CODPRA = .w_event.GETVAR('EVCODPRA')
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_CODPRA))
          .link_1_47('Full')
        endif
        .w_EDATINI = .w_event.GETVAR('EVDATINI')
        .w_EDATFIN = .w_event.GETVAR('EVDATFIN')
      .GSFA_ACE.NewDocument()
      .GSFA_ACE.ChangeRow('1',1,.w_EV__ANNO,"EV__ANNO",.w_EVSERIAL,"EVSERIAL")
      if not(.GSFA_ACE.bLoaded)
        .GSFA_ACE.SetKey(.w_EV__ANNO,"EV__ANNO",.w_EVSERIAL,"EVSERIAL")
      endif
        .w_MINEFF = .w_event.GETVAR('EVMINEFF')
        .w_OB_TEST = i_datsys
        .w_PATIPRIS = 'G'
        .w_ORAEFF = .w_event.GETVAR('EVORAEFF')
        .w_TIPPER = 'P'
        .w_TIPGRU = 'G'
        .w_FLANT = .w_TREEVIEW.GETVAR('TEFLGANT')
        .w_EVNOMINA = .w_event.GETVAR('EVNOMINA')
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        .w_DataIniTimer = DateTime()
      .oPgFrm.Page1.oPag.Clock.Calculate()
          .DoRTCalc(47,48,.f.)
        .w_Attivo = 'N'
          .DoRTCalc(50,51,.f.)
        .w_HTML_URL = .w_HTML_URL
        .w_STARIC = .w_event.GETVAR('EVSTARIC')
        .w_NOMINATIVO = Space(15)
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_NOMINATIVO))
          .link_1_94('Full')
        endif
        .w_TIPRIC = IIF(.w_TIPOTREE= 'T','T','D')
        .w_TIPRIC1 = IIF(.w_TIPRIC='T','',.w_TIPRIC)
        .w_EVSERDOC = .w_event.GETVAR('EVSERDOC')
        .w_LVLKEY = iif(.w_TIPOPE='C',' ',.w_TREEVIEW.GETVAR('lvlkey'))
        .w_EVTIPEVE = iif(not empty(nvl(.w_TETIPEVE,space(10))), .w_TETIPEVE, .w_EVENT.GETVAR('EVTIPEVE') )
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_EVTIPEVE))
          .link_1_102('Full')
        endif
        .w_FLIDPA = iif(.w_TIPOPE='C',' ','S')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_92.enabled = this.oPgFrm.Page1.oPag.oBtn_1_92.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsfa_kce
    *--- Se TREEVIEW perde il focus l'elemento selezionato rimane evidenziato
    THIS.OPGFRM.PAGE1.OPAG.TREEVIEW.OTREE.OBJECT.HideSelection = .F.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSFA_ACE.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSFA_ACE.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPOPE = this.oParentObject
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
        .DoRTCalc(2,6,.t.)
        if .o_OREINI<>.w_OREINI
            .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        endif
        if .o_MININI<>.w_MININI
            .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        endif
        if .o_OREFIN<>.w_OREFIN.or. .o_DATFIN<>.w_DATFIN
            .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        endif
        if .o_MINFIN<>.w_MINFIN.or. .o_DATFIN<>.w_DATFIN
            .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (empty(.w_DATFIN) AND .w_OREFIN="23" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        endif
        .DoRTCalc(11,13,.t.)
        if .o_NOMINATIVO<>.w_NOMINATIVO
            .w_NOMINAT = .w_NOMINATIVO
          .link_1_17('Full')
        endif
            .w_TETIPEVE = iif(.w_TIPOPE='C',.w_TREEVIEW.GETVAR('TETIPEVE'),' ')
          .link_1_18('Full')
        .DoRTCalc(16,16,.t.)
        if .o_TIPEVE<>.w_TIPEVE
            .w_TIPEVE1 = IIF(.w_TIPEVE='T','',.w_TIPEVE)
        endif
        .DoRTCalc(18,18,.t.)
            .w_ZPERSON = iif(.w_TIPOPE='C',' ',.w_TREEVIEW.GETVAR('PERSONA'))
            .w_ZGRUPPO = iif(.w_TIPOPE='C',' ',.w_TREEVIEW.GETVAR('GRUPPO'))
        if .o_TIPDIR<>.w_TIPDIR
            .w_TIPDIR1 = IIF(.w_TIPDIR='T','',.w_TIPDIR)
        endif
            .w_ZNOMINAT = iif(.w_TIPOPE='C',' ',.w_TREEVIEW.GETVAR('NOMINAT'))
            .w_ZIDRICH = iif(.w_TIPOPE='C',' ',.w_TREEVIEW.GETVAR('IDRICH'))
        .oPgFrm.Page1.oPag.EVENT.Calculate(.w_TETIPEVE)
            .w_EVSERIAL = .w_event.GETVAR('EVSERIAL')
          .link_1_30('Full')
        if .o_EVSERIAL<>.w_EVSERIAL
          .Calculate_KHZWXNFOPG()
        endif
        .DoRTCalc(26,26,.t.)
            .w_EV__ANNO = .w_event.GETVAR('EV__ANNO')
        .DoRTCalc(28,29,.t.)
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_STATO = .w_event.GETVAR('EV_STATO')
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_NOMINA = .w_event.GETVAR('EVNOMINA')
          .link_1_45('Full')
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_RIFPER = .w_event.GETVAR('EVRIFPER')
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_CODPRA = .w_event.GETVAR('EVCODPRA')
          .link_1_47('Full')
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_EDATINI = .w_event.GETVAR('EVDATINI')
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_EDATFIN = .w_event.GETVAR('EVDATFIN')
        endif
        if  .o_HTML_URL<>.w_HTML_URL
          .WriteTo_GSFA_ACE()
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_MINEFF = .w_event.GETVAR('EVMINEFF')
        endif
            .w_OB_TEST = i_datsys
            .w_PATIPRIS = 'G'
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_ORAEFF = .w_event.GETVAR('EVORAEFF')
        endif
        .DoRTCalc(40,41,.t.)
            .w_FLANT = .w_TREEVIEW.GETVAR('TEFLGANT')
            .w_EVNOMINA = .w_event.GETVAR('EVNOMINA')
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN<>.w_DATFIN.or. .o_OREFIN<>.w_OREFIN.or. .o_MINFIN<>.w_MINFIN
            .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        .oPgFrm.Page1.oPag.Clock.Calculate()
        .DoRTCalc(46,52,.t.)
            .w_STARIC = .w_event.GETVAR('EVSTARIC')
        if .o_TIPOTREE<>.w_TIPOTREE.or. .o_TIPOPE<>.w_TIPOPE
            .w_NOMINATIVO = Space(15)
          .link_1_94('Full')
        endif
        .DoRTCalc(55,55,.t.)
        if .o_TIPRIC<>.w_TIPRIC
            .w_TIPRIC1 = IIF(.w_TIPRIC='T','',.w_TIPRIC)
        endif
            .w_EVSERDOC = .w_event.GETVAR('EVSERDOC')
        if .o_TIPOTREE<>.w_TIPOTREE
          .Calculate_CJQHKFNVAY()
        endif
            .w_LVLKEY = iif(.w_TIPOPE='C',' ',.w_TREEVIEW.GETVAR('lvlkey'))
            .w_EVTIPEVE = iif(not empty(nvl(.w_TETIPEVE,space(10))), .w_TETIPEVE, .w_EVENT.GETVAR('EVTIPEVE') )
          .link_1_102('Full')
            .w_FLIDPA = iif(.w_TIPOPE='C',' ','S')
        * --- Area Manuale = Calculate
        * --- gsfa_kce
        This.Gsfa_ace.bupdated=.f.
        * --- Fine Area Manuale
        if .w_EV__ANNO<>.o_EV__ANNO or .w_EVSERIAL<>.o_EVSERIAL
          .Save_GSFA_ACE(.t.)
          .GSFA_ACE.NewDocument()
          .GSFA_ACE.ChangeRow('1',1,.w_EV__ANNO,"EV__ANNO",.w_EVSERIAL,"EVSERIAL")
          if not(.GSFA_ACE.bLoaded)
            .GSFA_ACE.SetKey(.w_EV__ANNO,"EV__ANNO",.w_EVSERIAL,"EVSERIAL")
          endif
        endif
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.TREEVIEW.Calculate()
        .oPgFrm.Page1.oPag.EVENT.Calculate(.w_TETIPEVE)
        .oPgFrm.Page1.oPag.Clock.Calculate()
    endwith
  return

  proc Calculate_KHZWXNFOPG()
    with this
          * --- Carica anteprima mail
          .w_EV__ANNO = .w_event.GETVAR('EV__ANNO')
          gsfa_bce(this;
              ,"GETBM";
             )
    endwith
  endproc
  proc Calculate_PCCPPBWXPE()
    with this
          * --- Apre anagrafica eventi
          .a = opengest('A','GSFA_AEV','EVSERIAL',.w_EVSERIAL,'EV__ANNO',.w_EV__ANNO)
    endwith
  endproc
  proc Calculate_IUBWKGLBYK()
    with this
          * --- Gsfa_bvs(reload) - reload
          gsfa_bvs(this;
              ,'Reload';
             )
    endwith
  endproc
  proc Calculate_QZYVDFKUED()
    with this
          * --- Stop al timer
          .w_Attivo = 'N'
          .w_DurataMin = IIF(.w_Clock.GetMinute()=0 And .w_Clock.GetHour() =0, 1, IIF(.w_Clock.GetSec()>=0 And .w_Clock.GetSec()<30, .w_Clock.GetMinute(), Minute(.w_Clock.GetTime()+(60-Sec(.w_Clock.GetTime())))))
          .w_DurataOre = IIF(.w_Clock.GetSec()>=0 And .w_Clock.GetSec()<30, .w_Clock.GetHour(), Hour(.w_Clock.GetTime()+(60-Sec(.w_Clock.GetTime()))))
    endwith
  endproc
  proc Calculate_YJDICSGHNH()
    with this
          * --- Azzera il timer
          .w_DataIniTimer = DateTime()
          .w_DurataOre = 0
          .w_DurataMin = 0
          .w_RESET = .w_CLOCK.ResetTime()
    endwith
  endproc
  proc Calculate_VSVLPEJGRG()
    with this
          * --- Inizializza il timer
          .w_Attivo = 'S'
          .w_DataIniTimer = EVL(.w_DataIniTimer, DateTime())
    endwith
  endproc
  proc Calculate_CAKWIAAGCU()
    with this
          * --- AggiornaTimer
          .w_RESET = .w_Clock.SetMinute(.w_DurataMin)
          .w_RESET = .w_Clock.SetHour(.w_DurataOre)
    endwith
  endproc
  proc Calculate_CJQHKFNVAY()
    with this
          * --- Eventi ricerca
          gsfa_bvs(this;
              ,'Filteve';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOREINI_1_10.enabled = this.oPgFrm.Page1.oPag.oOREINI_1_10.mCond()
    this.oPgFrm.Page1.oPag.oMININI_1_11.enabled = this.oPgFrm.Page1.oPag.oMININI_1_11.mCond()
    this.oPgFrm.Page1.oPag.oOREFIN_1_12.enabled = this.oPgFrm.Page1.oPag.oOREFIN_1_12.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_13.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDurataOre_1_75.enabled = this.oPgFrm.Page1.oPag.oDurataOre_1_75.mCond()
    this.oPgFrm.Page1.oPag.oDurataMin_1_76.enabled = this.oPgFrm.Page1.oPag.oDurataMin_1_76.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_58.enabled = this.oPgFrm.Page1.oPag.oBtn_1_58.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_71.enabled = this.oPgFrm.Page1.oPag.oBtn_1_71.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_72.enabled = this.oPgFrm.Page1.oPag.oBtn_1_72.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_73.enabled = this.oPgFrm.Page1.oPag.oBtn_1_73.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oROOT_1_2.visible=!this.oPgFrm.Page1.oPag.oROOT_1_2.mHide()
    this.oPgFrm.Page1.oPag.oTIPOTREE_1_3.visible=!this.oPgFrm.Page1.oPag.oTIPOTREE_1_3.mHide()
    this.oPgFrm.Page1.oPag.oEVPERSON_1_7.visible=!this.oPgFrm.Page1.oPag.oEVPERSON_1_7.mHide()
    this.oPgFrm.Page1.oPag.oEVGRUPPO_1_14.visible=!this.oPgFrm.Page1.oPag.oEVGRUPPO_1_14.mHide()
    this.oPgFrm.Page1.oPag.oTETIPEVE_1_18.visible=!this.oPgFrm.Page1.oPag.oTETIPEVE_1_18.mHide()
    this.oPgFrm.Page1.oPag.oTIPEVE_1_19.visible=!this.oPgFrm.Page1.oPag.oTIPEVE_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDESROOT_1_39.visible=!this.oPgFrm.Page1.oPag.oDESROOT_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_58.visible=!this.oPgFrm.Page1.oPag.oBtn_1_58.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_59.visible=!this.oPgFrm.Page1.oPag.oBtn_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_71.visible=!this.oPgFrm.Page1.oPag.oBtn_1_71.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_72.visible=!this.oPgFrm.Page1.oPag.oBtn_1_72.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_73.visible=!this.oPgFrm.Page1.oPag.oBtn_1_73.mHide()
    this.oPgFrm.Page1.oPag.oNOMINATIVO_1_94.visible=!this.oPgFrm.Page1.oPag.oNOMINATIVO_1_94.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_95.visible=!this.oPgFrm.Page1.oPag.oStr_1_95.mHide()
    this.oPgFrm.Page1.oPag.oTIPRIC_1_96.visible=!this.oPgFrm.Page1.oPag.oTIPRIC_1_96.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_97.visible=!this.oPgFrm.Page1.oPag.oStr_1_97.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_103.visible=!this.oPgFrm.Page1.oPag.oBtn_1_103.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.TREEVIEW.Event(cEvent)
      .oPgFrm.Page1.oPag.EVENT.Event(cEvent)
        if lower(cEvent)==lower("w_EVENT selected")
          .Calculate_PCCPPBWXPE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Reload") or lower(cEvent)==lower("w_EVGRUPPO Changed") or lower(cEvent)==lower("w_EVPERSON Changed") or lower(cEvent)==lower("w_NOMINATIVO Changed") or lower(cEvent)==lower("w_ROOT Changed") or lower(cEvent)==lower("w_TIPOTREE Changed") or lower(cEvent)==lower("Blank")
          .Calculate_IUBWKGLBYK()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.Clock.Event(cEvent)
        if lower(cEvent)==lower("StopTimer")
          .Calculate_QZYVDFKUED()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AzzeraTimer")
          .Calculate_YJDICSGHNH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("InitTimer")
          .Calculate_VSVLPEJGRG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_DurataMin Changed") or lower(cEvent)==lower("w_DurataOre Changed") or lower(cEvent)==lower("AzzeraTimer")
          .Calculate_CAKWIAAGCU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Filteve") or lower(cEvent)==lower("w_treeview collapsed")
          .Calculate_CJQHKFNVAY()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsfa_kce
    if lower(cevent)='w_treeview nodeselected'
       if this.w_TREEVIEW.GETVAR('TEFLGANT')='S'
          This.gsfa_ace.oPgFrm.ActivePage = 2
       else
         This.gsfa_ace.oPgFrm.ActivePage = 1
       endif
       This.Notifyevent('Filteve')
    endif
    if cevent='StopTimer'
       Do Gsag_ktm with This
    endif
    if cevent='w_TIPEVE Changed' OR cevent='w_DATINI Changed' OR Cevent='w_TIPDIR Changed' OR Cevent='w_PERSON Changed' OR Cevent='w_GRUPPO Changed' OR Cevent='w_TIPRIC Changed'
       this.w_TIPDIR1=IIF(this.w_TIPDIR='T','',this.w_TIPDIR)
          this.w_TIPric1=IIF(this.w_TIPRIC='T','',this.w_TIPRIC)
       This.Notifyevent('Eventi')
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ROOT
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsfa_ate',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_ROOT)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_ROOT))
          select TETIPEVE,TEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ROOT)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStrODBC(trim(this.w_ROOT)+"%");

            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStr(trim(this.w_ROOT)+"%");

            select TETIPEVE,TEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ROOT) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oROOT_1_2'),i_cWhere,'gsfa_ate',"Tipi eventi",'gsfarkte.TIPEVENT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_ROOT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_ROOT)
            select TETIPEVE,TEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROOT = NVL(_Link_.TETIPEVE,space(10))
      this.w_DESROOT = NVL(_Link_.TEDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ROOT = space(10)
      endif
      this.w_DESROOT = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EVPERSON
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVPERSON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_EVPERSON)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TIPPER;
                     ,'DPCODICE',trim(this.w_EVPERSON))
          select DPTIPRIS,DPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVPERSON)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EVPERSON) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oEVPERSON_1_7'),i_cWhere,'GSAR_BDZ',"Persone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPPER<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVPERSON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_EVPERSON);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TIPPER;
                       ,'DPCODICE',this.w_EVPERSON)
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVPERSON = NVL(_Link_.DPCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EVPERSON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVPERSON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EVGRUPPO
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVGRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_EVGRUPPO)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPGRU);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TIPGRU;
                     ,'DPCODICE',trim(this.w_EVGRUPPO))
          select DPTIPRIS,DPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVGRUPPO)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EVGRUPPO) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oEVGRUPPO_1_14'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPGRU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPGRU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVGRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_EVGRUPPO);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TIPGRU;
                       ,'DPCODICE',this.w_EVGRUPPO)
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVGRUPPO = NVL(_Link_.DPCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EVGRUPPO = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_EVPERSON , .w_EVGRUPPO) OR Empty(.w_EVPERSON))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
        endif
        this.w_EVGRUPPO = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVGRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERSON
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERSON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_PERSON)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TIPPER;
                     ,'DPCODICE',trim(this.w_PERSON))
          select DPTIPRIS,DPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERSON)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PERSON) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oPERSON_1_15'),i_cWhere,'GSAR_BDZ',"Persone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPPER<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERSON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_PERSON);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TIPPER;
                       ,'DPCODICE',this.w_PERSON)
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERSON = NVL(_Link_.DPCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PERSON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERSON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPPO
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPPO)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_GRUPPO))
          select DPCODICE,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPPO)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUPPO) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oGRUPPO_1_16'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPPO)
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPPO = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPGRU = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPPO = space(5)
      endif
      this.w_TIPGRU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_PERSON , .w_GRUPPO) OR Empty(.w_PERSON))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
        endif
        this.w_GRUPPO = space(5)
        this.w_TIPGRU = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOMINAT
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOMINAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_NOMINAT)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_NOMINAT))
          select NOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOMINAT)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOMINAT) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oNOMINAT_1_17'),i_cWhere,'',"Elenco nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOMINAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_NOMINAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_NOMINAT)
            select NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOMINAT = NVL(_Link_.NOCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_NOMINAT = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOMINAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TETIPEVE
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TETIPEVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TETIPEVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_TETIPEVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_TETIPEVE)
            select TETIPEVE,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TETIPEVE = NVL(_Link_.TETIPEVE,space(10))
      this.w_TEDRIVER = NVL(_Link_.TEDRIVER,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_TETIPEVE = space(10)
      endif
      this.w_TEDRIVER = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TETIPEVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TEDRIVER
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_lTable = "DRVEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2], .t., this.DRVEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TEDRIVER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TEDRIVER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DEDRIVER,DE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where DEDRIVER="+cp_ToStrODBC(this.w_TEDRIVER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DEDRIVER',this.w_TEDRIVER)
            select DEDRIVER,DE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TEDRIVER = NVL(_Link_.DEDRIVER,space(10))
      this.w_DE__TIPO = NVL(_Link_.DE__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TEDRIVER = space(10)
      endif
      this.w_DE__TIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])+'\'+cp_ToStr(_Link_.DEDRIVER,1)
      cp_ShowWarn(i_cKey,this.DRVEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TEDRIVER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOMINA
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOMINA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOMINA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_NOMINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_NOMINA)
            select NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOMINA = NVL(_Link_.NOCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_NOMINA = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOMINA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOMINATIVO
  func Link_1_94(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOMINATIVO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_NOMINATIVO)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_NOMINATIVO))
          select NOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOMINATIVO)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOMINATIVO) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oNOMINATIVO_1_94'),i_cWhere,'',"Elenco nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOMINATIVO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_NOMINATIVO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_NOMINATIVO)
            select NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOMINATIVO = NVL(_Link_.NOCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_NOMINATIVO = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOMINATIVO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EVTIPEVE
  func Link_1_102(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVTIPEVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVTIPEVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_EVTIPEVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_EVTIPEVE)
            select TETIPEVE,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVTIPEVE = NVL(_Link_.TETIPEVE,space(10))
      this.w_TEDRIVER = NVL(_Link_.TEDRIVER,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_EVTIPEVE = space(10)
      endif
      this.w_TEDRIVER = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVTIPEVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oROOT_1_2.value==this.w_ROOT)
      this.oPgFrm.Page1.oPag.oROOT_1_2.value=this.w_ROOT
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOTREE_1_3.RadioValue()==this.w_TIPOTREE)
      this.oPgFrm.Page1.oPag.oTIPOTREE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEVPERSON_1_7.value==this.w_EVPERSON)
      this.oPgFrm.Page1.oPag.oEVPERSON_1_7.value=this.w_EVPERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_8.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_8.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_9.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_9.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINI_1_10.value==this.w_OREINI)
      this.oPgFrm.Page1.oPag.oOREINI_1_10.value=this.w_OREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_11.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_11.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oOREFIN_1_12.value==this.w_OREFIN)
      this.oPgFrm.Page1.oPag.oOREFIN_1_12.value=this.w_OREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_13.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_13.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oEVGRUPPO_1_14.value==this.w_EVGRUPPO)
      this.oPgFrm.Page1.oPag.oEVGRUPPO_1_14.value=this.w_EVGRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oPERSON_1_15.value==this.w_PERSON)
      this.oPgFrm.Page1.oPag.oPERSON_1_15.value=this.w_PERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUPPO_1_16.value==this.w_GRUPPO)
      this.oPgFrm.Page1.oPag.oGRUPPO_1_16.value=this.w_GRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMINAT_1_17.value==this.w_NOMINAT)
      this.oPgFrm.Page1.oPag.oNOMINAT_1_17.value=this.w_NOMINAT
    endif
    if not(this.oPgFrm.Page1.oPag.oTETIPEVE_1_18.value==this.w_TETIPEVE)
      this.oPgFrm.Page1.oPag.oTETIPEVE_1_18.value=this.w_TETIPEVE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPEVE_1_19.RadioValue()==this.w_TIPEVE)
      this.oPgFrm.Page1.oPag.oTIPEVE_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDIR_1_21.RadioValue()==this.w_TIPDIR)
      this.oPgFrm.Page1.oPag.oTIPDIR_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESROOT_1_39.value==this.w_DESROOT)
      this.oPgFrm.Page1.oPag.oDESROOT_1_39.value=this.w_DESROOT
    endif
    if not(this.oPgFrm.Page1.oPag.oDataIniTimer_1_74.value==this.w_DataIniTimer)
      this.oPgFrm.Page1.oPag.oDataIniTimer_1_74.value=this.w_DataIniTimer
    endif
    if not(this.oPgFrm.Page1.oPag.oDurataOre_1_75.value==this.w_DurataOre)
      this.oPgFrm.Page1.oPag.oDurataOre_1_75.value=this.w_DurataOre
    endif
    if not(this.oPgFrm.Page1.oPag.oDurataMin_1_76.value==this.w_DurataMin)
      this.oPgFrm.Page1.oPag.oDurataMin_1_76.value=this.w_DurataMin
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMINATIVO_1_94.value==this.w_NOMINATIVO)
      this.oPgFrm.Page1.oPag.oNOMINATIVO_1_94.value=this.w_NOMINATIVO
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPRIC_1_96.RadioValue()==this.w_TIPRIC)
      this.oPgFrm.Page1.oPag.oTIPRIC_1_96.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(VAL(.w_OREINI) < 24)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREINI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(VAL(.w_OREFIN) < 24)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_EVPERSON , .w_EVGRUPPO) OR Empty(.w_EVPERSON)))  and not(.w_TIPOTREE $ 'N-G')  and not(empty(.w_EVGRUPPO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVGRUPPO_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
          case   not(.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_PERSON , .w_GRUPPO) OR Empty(.w_PERSON)))  and not(empty(.w_GRUPPO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUPPO_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
          case   not(.w_DurataMin<60)  and (.w_Attivo='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDurataMin_1_76.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSFA_ACE.CheckForm()
      if i_bres
        i_bres=  .GSFA_ACE.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOPE = this.w_TIPOPE
    this.o_TIPOTREE = this.w_TIPOTREE
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_OREFIN = this.w_OREFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_TIPEVE = this.w_TIPEVE
    this.o_TIPDIR = this.w_TIPDIR
    this.o_EVSERIAL = this.w_EVSERIAL
    this.o_EV__ANNO = this.w_EV__ANNO
    this.o_HTML_URL = this.w_HTML_URL
    this.o_NOMINATIVO = this.w_NOMINATIVO
    this.o_TIPRIC = this.w_TIPRIC
    * --- GSFA_ACE : Depends On
    this.GSFA_ACE.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsfa_kcePag1 as StdContainer
  Width  = 791
  height = 567
  stdWidth  = 791
  stdheight = 567
  resizeXpos=636
  resizeYpos=268
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oROOT_1_2 as StdField with uid="GEMRLXLOPV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ROOT", cQueryName = "ROOT",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Tipo evento",;
    HelpContextID = 93404650,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=60, Top=9, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", cZoomOnZoom="gsfa_ate", oKey_1_1="TETIPEVE", oKey_1_2="this.w_ROOT"

  func oROOT_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPOPE='I')
    endwith
  endfunc

  func oROOT_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oROOT_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROOT_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oROOT_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'gsfa_ate',"Tipi eventi",'gsfarkte.TIPEVENT_VZM',this.parent.oContained
  endproc
  proc oROOT_1_2.mZoomOnZoom
    local i_obj
    i_obj=gsfa_ate()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TETIPEVE=this.parent.oContained.w_ROOT
     i_obj.ecpSave()
  endproc


  add object oTIPOTREE_1_3 as StdCombo with uid="BEEFORHCGJ",rtseq=3,rtrep=.f.,left=60,top=10,width=293,height=21;
    , HelpContextID = 27905147;
    , cFormVar="w_TIPOTREE",RowSource=""+"Nominativo/id/gruppo/persona,"+"Nominativo/gruppo/persona/id,"+"Gruppo/persona/nominativo/id", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOTREE_1_3.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'G',;
    iif(this.value =3,'P',;
    space(1)))))
  endfunc
  func oTIPOTREE_1_3.GetRadio()
    this.Parent.oContained.w_TIPOTREE = this.RadioValue()
    return .t.
  endfunc

  func oTIPOTREE_1_3.SetRadio()
    this.Parent.oContained.w_TIPOTREE=trim(this.Parent.oContained.w_TIPOTREE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOTREE=='N',1,;
      iif(this.Parent.oContained.w_TIPOTREE=='G',2,;
      iif(this.Parent.oContained.w_TIPOTREE=='P',3,;
      0)))
  endfunc

  func oTIPOTREE_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPOPE='C')
    endwith
  endfunc


  add object oBtn_1_4 as StdButton with uid="PPWFAOCXOY",left=274, top=63, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per riempire la tree view";
    , HelpContextID = 77667350;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .NotifyEvent("Reload")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="UUNSDLSMLD",left=687, top=517, width=48,height=45,;
    CpPicture="BMP\REFRESH.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare gli eventi selezionati";
    , HelpContextID = 58022247;
    , caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSFA_BVS(this.Parent.oContained,"Aggiorna")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object TREEVIEW as cp_Treeview with uid="FVJYSBPBBD",left=4, top=62, width=262,height=219,;
    caption='Object',;
   bGlobalFont=.t.,;
    cCursor="cur_str",cShowFields="",cNodeShowField="Alltrim(TEDESCRI)",cLeafShowField="TEDESCRI",cNodeBmp="ROOT.BMP,CONTO.BMP,LEAF.BMP,NEW1.BMP",cLeafBmp="",nIndent=20,cLvlSep="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 78742758

  add object oEVPERSON_1_7 as StdField with uid="OOFQBPZIYA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_EVPERSON", cQueryName = "EVPERSON",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Persona",;
    HelpContextID = 41932948,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=60, Top=34, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TIPPER", oKey_2_1="DPCODICE", oKey_2_2="this.w_EVPERSON"

  func oEVPERSON_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPOTREE $ 'N-G')
    endwith
  endfunc

  func oEVPERSON_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVPERSON_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVPERSON_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TIPPER)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TIPPER)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oEVPERSON_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone",'',this.parent.oContained
  endproc
  proc oEVPERSON_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TIPPER
     i_obj.w_DPCODICE=this.parent.oContained.w_EVPERSON
     i_obj.ecpSave()
  endproc

  add object oDATINI_1_8 as StdField with uid="VJALXHXOBD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale",;
    HelpContextID = 138674998,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=405, Top=9

  add object oDATFIN_1_9 as StdField with uid="IHLXVHHGUF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale",;
    HelpContextID = 217121590,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=405, Top=34

  add object oOREINI_1_10 as StdField with uid="WOKDXXPUIP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_OREINI", cQueryName = "OREINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di inizio selezione",;
    HelpContextID = 138618086,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=535, Top=9, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREINI_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oOREINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_11 as StdField with uid="QMRKNBACQP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio selezione",;
    HelpContextID = 138652614,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=569, Top=9, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oMININI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oOREFIN_1_12 as StdField with uid="VKJCJSQZFK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_OREFIN", cQueryName = "OREFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di fine selezione",;
    HelpContextID = 217064678,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=535, Top=34, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREFIN_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oOREFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_13 as StdField with uid="GIWPXXGMLC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di fine selezione",;
    HelpContextID = 217099206,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=569, Top=34, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oMINFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc

  add object oEVGRUPPO_1_14 as StdField with uid="ZLFTNVEAYQ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_EVGRUPPO", cQueryName = "EVGRUPPO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, persona non appartenente al gruppo selezionato",;
    ToolTipText = "Gruppo",;
    HelpContextID = 263997589,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=294, Top=34, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TIPGRU", oKey_2_1="DPCODICE", oKey_2_2="this.w_EVGRUPPO"

  func oEVGRUPPO_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPOTREE $ 'N-G')
    endwith
  endfunc

  func oEVGRUPPO_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVGRUPPO_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVGRUPPO_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TIPGRU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TIPGRU)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oEVGRUPPO_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oEVGRUPPO_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TIPGRU
     i_obj.w_DPCODICE=this.parent.oContained.w_EVGRUPPO
     i_obj.ecpSave()
  endproc

  add object oPERSON_1_15 as StdField with uid="ZTRRYFXJMX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PERSON", cQueryName = "PERSON",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Persona",;
    HelpContextID = 224258038,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=405, Top=59, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TIPPER", oKey_2_1="DPCODICE", oKey_2_2="this.w_PERSON"

  func oPERSON_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERSON_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERSON_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TIPPER)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TIPPER)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oPERSON_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone",'',this.parent.oContained
  endproc
  proc oPERSON_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TIPPER
     i_obj.w_DPCODICE=this.parent.oContained.w_PERSON
     i_obj.ecpSave()
  endproc

  add object oGRUPPO_1_16 as StdField with uid="CUEKIRZXDR",rtseq=13,rtrep=.f.,;
    cFormVar = "w_GRUPPO", cQueryName = "GRUPPO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, persona non appartenente al gruppo selezionato",;
    ToolTipText = "Gruppo",;
    HelpContextID = 241902694,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=535, Top=59, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_GRUPPO"

  func oGRUPPO_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPPO_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPPO_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oGRUPPO_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oGRUPPO_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_GRUPPO
     i_obj.ecpSave()
  endproc

  add object oNOMINAT_1_17 as StdField with uid="JVYIVXEEGT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_NOMINAT", cQueryName = "NOMINAT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nominativo",;
    HelpContextID = 4432342,;
   bGlobalFont=.t.,;
    Height=21, Width=144, Left=640, Top=59, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", oKey_1_1="NOCODICE", oKey_1_2="this.w_NOMINAT"

  func oNOMINAT_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOMINAT_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOMINAT_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oNOMINAT_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco nominativi",'',this.parent.oContained
  endproc

  add object oTETIPEVE_1_18 as StdField with uid="MGURRLFJJI",rtseq=15,rtrep=.f.,;
    cFormVar = "w_TETIPEVE", cQueryName = "TETIPEVE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 73664635,;
   bGlobalFont=.t.,;
    Height=21, Width=92, Left=263, Top=259, InputMask=replicate('X',10), cLinkFile="TIPEVENT", cZoomOnZoom="GSFA_ATE", oKey_1_1="TETIPEVE", oKey_1_2="this.w_TETIPEVE"

  func oTETIPEVE_1_18.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  func oTETIPEVE_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oTIPEVE_1_19 as StdCombo with uid="YJPGLRXDBN",rtseq=16,rtrep=.f.,left=640,top=9,width=95,height=21;
    , HelpContextID = 79678518;
    , cFormVar="w_TIPEVE",RowSource=""+"Processati,"+"Da processare,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPEVE_1_19.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPEVE_1_19.GetRadio()
    this.Parent.oContained.w_TIPEVE = this.RadioValue()
    return .t.
  endfunc

  func oTIPEVE_1_19.SetRadio()
    this.Parent.oContained.w_TIPEVE=trim(this.Parent.oContained.w_TIPEVE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPEVE=='P',1,;
      iif(this.Parent.oContained.w_TIPEVE=='D',2,;
      iif(this.Parent.oContained.w_TIPEVE=='T',3,;
      0)))
  endfunc

  func oTIPEVE_1_19.mHide()
    with this.Parent.oContained
      return (.w_TIPOTREE<> 'T')
    endwith
  endfunc


  add object oTIPDIR_1_21 as StdCombo with uid="UOLGNCUVIK",rtseq=18,rtrep=.f.,left=640,top=34,width=93,height=21;
    , HelpContextID = 15649846;
    , cFormVar="w_TIPDIR",RowSource=""+"Entrata,"+"Uscita,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDIR_1_21.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'U',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPDIR_1_21.GetRadio()
    this.Parent.oContained.w_TIPDIR = this.RadioValue()
    return .t.
  endfunc

  func oTIPDIR_1_21.SetRadio()
    this.Parent.oContained.w_TIPDIR=trim(this.Parent.oContained.w_TIPDIR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDIR=='E',1,;
      iif(this.Parent.oContained.w_TIPDIR=='U',2,;
      iif(this.Parent.oContained.w_TIPDIR=='T',3,;
      0)))
  endfunc


  add object oBtn_1_27 as StdButton with uid="TKCMXZXILU",left=737, top=517, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 91937466;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object EVENT as cp_szoombox with uid="NQHSFSVQZT",left=354, top=86, width=431,height=190,;
    caption='EVENT',;
   bGlobalFont=.t.,;
    cTable="ANEVENTI",cZoomFile="GSFA_KCE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="gsfa_aev",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Eventi",;
    nPag=1;
    , HelpContextID = 5756858


  add object oBtn_1_36 as StdButton with uid="ZVAMGQYVJH",left=274, top=206, width=48,height=45,;
    CpPicture="BMP\IMPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per chiudere la struttura";
    , HelpContextID = 176623738;
    , Caption='\<Implodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_37 as StdButton with uid="XWAVWHNZRN",left=274, top=158, width=48,height=45,;
    CpPicture="BMP\ESPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per espandere la struttura";
    , HelpContextID = 176622266;
    , Caption='\<Esplodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESROOT_1_39 as StdField with uid="NDDFQPFDYF",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESROOT", cQueryName = "DESROOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 240973622,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=144, Top=9, InputMask=replicate('X',50)

  func oDESROOT_1_39.mHide()
    with this.Parent.oContained
      return (.w_TIPOPE='I')
    endwith
  endfunc


  add object oBtn_1_41 as StdButton with uid="DIYASKUDBE",left=274, top=110, width=48,height=45,;
    CpPicture="bmp\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Tipo evento";
    , HelpContextID = 91493322;
    , caption='\<Tipo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      with this.Parent.oContained
        =opengest('A','GSFA_ATE','TETIPEVE',.w_EVTIPEVE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oLinkPC_1_50 as stdDynamicChildContainer with uid="FJGYVQCEVC",left=8, top=286, width=776, height=211, bOnScreen=.t.;



  add object oBtn_1_58 as StdButton with uid="NCTLRMDOAF",left=485, top=518, width=48,height=45,;
    CpPicture="BMP\DOC1.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato all'evento";
    , HelpContextID = 110764342;
    , Caption='\<Doc.ev';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_58.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_EVSERDOC,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_58.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_EVSERDOC))
      endwith
    endif
  endfunc

  func oBtn_1_58.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_EVSERDOC) OR ! ISAHR() )
     endwith
    endif
  endfunc


  add object oBtn_1_59 as StdButton with uid="DKOPTSDDPM",left=587, top=518, width=48,height=45,;
    CpPicture="BMP\pdfarch.bmp", caption="", nPag=1;
    , ToolTipText = "Collega indici";
    , HelpContextID = 263575735;
    , caption='\<Arc. Doc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_59.Click()
      with this.Parent.oContained
        do GSFA_BCP with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_59.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.GSFA_ACE.oPgFrm.ActivePage=5)
      endwith
    endif
  endfunc

  func oBtn_1_59.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!Isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_60 as StdButton with uid="XFTPEMBDOZ",left=587, top=518, width=48,height=45,;
    CpPicture="BMP\visuali.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire visualizzazioni";
    , HelpContextID = 91876858;
    , caption='\<Apri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_60.Click()
      with this.Parent.oContained
        GSFA_BVS(this.Parent.oContained,"Apri")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_62 as StdButton with uid="IFNDLMJVTH",left=637, top=518, width=48,height=45,;
    CpPicture="BMP\carica.bmp", caption="", nPag=1;
    , ToolTipText = "Nuovo";
    , HelpContextID = 243079210;
    , caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      with this.Parent.oContained
        GSFA_BVS(this.Parent.oContained,"Rightclick")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_71 as StdButton with uid="QHYJCJDXQP",left=10, top=518, width=48,height=45,;
    CpPicture="bmp\play.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per attivare il timer";
    , HelpContextID = 238155994;
    , caption='\<Start';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_71.Click()
      this.parent.oContained.NotifyEvent("InitTimer")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_71.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Attivo='N')
      endwith
    endif
  endfunc

  func oBtn_1_71.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_Attivo<>'N')
     endwith
    endif
  endfunc


  add object oBtn_1_72 as StdButton with uid="YZQCVUXQYK",left=10, top=518, width=48,height=45,;
    CpPicture="bmp\stop.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per arrestare il timer (anche parzialmente)";
    , HelpContextID = 91429082;
    , Caption='\<Stop';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_72.Click()
      this.parent.oContained.NotifyEvent("StopTimer")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_72.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Attivo<>'N')
      endwith
    endif
  endfunc

  func oBtn_1_72.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_Attivo='N')
     endwith
    endif
  endfunc


  add object oBtn_1_73 as StdButton with uid="IRQDZWWWVS",left=64, top=518, width=48,height=45,;
    CpPicture="bmp\reset.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per azzerare il timer";
    , HelpContextID = 44211206;
    , caption='\<Azzera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_73.Click()
      this.parent.oContained.NotifyEvent("AzzeraTimer")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_73.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Attivo='N')
      endwith
    endif
  endfunc

  func oBtn_1_73.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_Attivo='N')
     endwith
    endif
  endfunc

  add object oDataIniTimer_1_74 as StdField with uid="ZFOXPZZFPT",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DataIniTimer", cQueryName = "DataIniTimer",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 41490662,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=121, Top=538

  add object oDurataOre_1_75 as StdField with uid="PHQRCKNSLA",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DurataOre", cQueryName = "DurataOre",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 46013944,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=258, Top=538

  func oDurataOre_1_75.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attivo='N')
    endwith
   endif
  endfunc

  add object oDurataMin_1_76 as StdField with uid="LKLOMSPJFE",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DurataMin", cQueryName = "DurataMin",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 222421377,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=302, Top=538

  func oDurataMin_1_76.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attivo='N')
    endwith
   endif
  endfunc

  func oDurataMin_1_76.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DurataMin<60)
    endwith
    return bRes
  endfunc


  add object Clock as cp_clock with uid="FZCXQEGJLK",left=340, top=521, width=121,height=42,;
    caption='Object',;
   bGlobalFont=.t.,;
    bShowSecond=.t.,FontName="Zucchetti Digital",Visible=.t.,FontSize=32,FontColor=48896,;
    cEvent = "StopTimer,InitTimer",;
    nPag=1;
    , HelpContextID = 78742758


  add object oBtn_1_92 as StdButton with uid="SVBJVATCCD",left=738, top=7, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare elenco eventi";
    , HelpContextID = 77667350;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_92.Click()
      this.parent.oContained.NotifyEvent("Eventi")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNOMINATIVO_1_94 as StdField with uid="DYDYJPGSDG",rtseq=54,rtrep=.f.,;
    cFormVar = "w_NOMINATIVO", cQueryName = "NOMINATIVO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nominativo",;
    HelpContextID = 4454015,;
   bGlobalFont=.t.,;
    Height=21, Width=144, Left=209, Top=34, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", oKey_1_1="NOCODICE", oKey_1_2="this.w_NOMINATIVO"

  func oNOMINATIVO_1_94.mHide()
    with this.Parent.oContained
      return (.w_TIPOTREE = 'P' or .w_TIPOPE='C')
    endwith
  endfunc

  func oNOMINATIVO_1_94.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_94('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOMINATIVO_1_94.ecpDrop(oSource)
    this.Parent.oContained.link_1_94('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOMINATIVO_1_94.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oNOMINATIVO_1_94'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco nominativi",'',this.parent.oContained
  endproc


  add object oTIPRIC_1_96 as StdCombo with uid="VQXDZGGOTS",rtseq=55,rtrep=.f.,left=640,top=9,width=93,height=21;
    , HelpContextID = 33344566;
    , cFormVar="w_TIPRIC",RowSource=""+"Da chiudere,"+"Aperta,"+"In corso,"+"In attesa,"+"Chiusa,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPRIC_1_96.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'A',;
    iif(this.value =3,'I',;
    iif(this.value =4,'S',;
    iif(this.value =5,'C',;
    iif(this.value =6,'T',;
    space(1))))))))
  endfunc
  func oTIPRIC_1_96.GetRadio()
    this.Parent.oContained.w_TIPRIC = this.RadioValue()
    return .t.
  endfunc

  func oTIPRIC_1_96.SetRadio()
    this.Parent.oContained.w_TIPRIC=trim(this.Parent.oContained.w_TIPRIC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPRIC=='D',1,;
      iif(this.Parent.oContained.w_TIPRIC=='A',2,;
      iif(this.Parent.oContained.w_TIPRIC=='I',3,;
      iif(this.Parent.oContained.w_TIPRIC=='S',4,;
      iif(this.Parent.oContained.w_TIPRIC=='C',5,;
      iif(this.Parent.oContained.w_TIPRIC=='T',6,;
      0))))))
  endfunc

  func oTIPRIC_1_96.mHide()
    with this.Parent.oContained
      return (.w_TIPOTREE= 'T')
    endwith
  endfunc


  add object oBtn_1_103 as StdButton with uid="OPNCSMOYPN",left=485, top=518, width=48,height=45,;
    CpPicture="BMP\DOC1.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato all'evento";
    , HelpContextID = 110764342;
    , Caption='\<Doc.ev';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_103.Click()
      with this.Parent.oContained
        GSMA_BZM(this.Parent.oContained,.w_EVSERDOC,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_103.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_EVSERDOC))
      endwith
    endif
  endfunc

  func oBtn_1_103.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_EVSERDOC) OR ! ISAHE() )
     endwith
    endif
  endfunc


  add object oBtn_1_104 as StdButton with uid="SRMTNNWWKH",left=536, top=518, width=48,height=45,;
    CpPicture="BMP\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Elimina gli eventi selezionati";
    , HelpContextID = 253328966;
    , caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_104.Click()
      with this.Parent.oContained
        GSFA_BVS(this.Parent.oContained,"Deleve")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_40 as StdString with uid="GTMVVWFFHQ",Visible=.t., Left=30, Top=12,;
    Alignment=1, Width=27, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="KBJNDVCHYH",Visible=.t., Left=599, Top=12,;
    Alignment=1, Width=37, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.w_TIPOTREE<> 'T')
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="AHZQYXDKXK",Visible=.t., Left=598, Top=37,;
    Alignment=1, Width=38, Height=18,;
    Caption="Dir.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="UXDSDXOTLI",Visible=.t., Left=7, Top=37,;
    Alignment=1, Width=50, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (.w_TIPOTREE $ 'N-G')
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="FOAHLIOEEF",Visible=.t., Left=240, Top=37,;
    Alignment=1, Width=50, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_TIPOTREE $ 'N-G')
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="DBGPZMROUJ",Visible=.t., Left=356, Top=12,;
    Alignment=1, Width=46, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="CBRLJUQJGD",Visible=.t., Left=356, Top=37,;
    Alignment=1, Width=46, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="EWZSPAOTVW",Visible=.t., Left=482, Top=12,;
    Alignment=1, Width=49, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="CTAHWBKCXZ",Visible=.t., Left=482, Top=37,;
    Alignment=1, Width=49, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="JMZTRKLLZU",Visible=.t., Left=121, Top=518,;
    Alignment=0, Width=28, Height=18,;
    Caption="Inizio"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="KMGLYDPVXE",Visible=.t., Left=272, Top=518,;
    Alignment=0, Width=20, Height=18,;
    Caption="Ore"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="RXZWLCDQGP",Visible=.t., Left=307, Top=518,;
    Alignment=0, Width=22, Height=18,;
    Caption="Min."  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="KYJNGQLJSJ",Visible=.t., Left=352, Top=62,;
    Alignment=1, Width=50, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="CHKPISWTGJ",Visible=.t., Left=481, Top=62,;
    Alignment=1, Width=50, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="XNMLRHKECQ",Visible=.t., Left=590, Top=61,;
    Alignment=1, Width=46, Height=18,;
    Caption="Nom.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_95 as StdString with uid="QGZLVTSSBH",Visible=.t., Left=136, Top=35,;
    Alignment=1, Width=68, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_95.mHide()
    with this.Parent.oContained
      return (.w_TIPOTREE = 'P' or .w_TIPOPE='C')
    endwith
  endfunc

  add object oStr_1_97 as StdString with uid="OCQASDWGRJ",Visible=.t., Left=596, Top=12,;
    Alignment=1, Width=40, Height=18,;
    Caption="Rich.:"  ;
  , bGlobalFont=.t.

  func oStr_1_97.mHide()
    with this.Parent.oContained
      return (.w_TIPOTREE= 'T')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_kce','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsfa_kce
    proc Destroy()
      this.w_TREEVIEW = .NULL.
      this.w_EVENT = .NULL.
      this.w_Clock = .NULL.
      Do GSFA_BVS WITH .null. , "Close"
      DoDefault()
  
* --- Fine Area Manuale
