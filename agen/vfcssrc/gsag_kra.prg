* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kra                                                        *
*              Elenco attivit�                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-09                                                      *
* Last revis.: 2014-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kra",oParentObject))

* --- Class definition
define class tgsag_kra as StdForm
  Top    = 2
  Left   = 9

  * --- Standard Properties
  Width  = 819
  Height = 548+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-19"
  HelpContextID=116008809
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=160

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  PRA_ENTI_IDX = 0
  PRA_UFFI_IDX = 0
  CPUSERS_IDX = 0
  OFF_NOMI_IDX = 0
  CAT_SOGG_IDX = 0
  OFFTIPAT_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  CENCOST_IDX = 0
  DES_DIVE_IDX = 0
  GEST_ATTI_IDX = 0
  TIP_RISE_IDX = 0
  PAR_AGEN_IDX = 0
  CAUMATTI_IDX = 0
  CON_TRAS_IDX = 0
  MOD_ELEM_IDX = 0
  CONTI_IDX = 0
  RUO_SOGI_IDX = 0
  cPrg = "gsag_kra"
  cComment = "Elenco attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TipoRisorsa = space(1)
  w_CACODICE = space(20)
  w_CADESCRI = space(254)
  w_LAYOUT = .F.
  w_READAZI = space(5)
  o_READAZI = space(5)
  w_FLTPART = space(5)
  o_FLTPART = space(5)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_OREFIN = space(2)
  o_OREFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_PATIPRIS = space(1)
  o_PATIPRIS = space(1)
  w_FLESGR = space(1)
  w_GRUPART = space(5)
  w_CODPART = space(5)
  o_CODPART = space(5)
  w_CODPRA = space(15)
  w_PRIORITA = 0
  w_STATO = space(1)
  o_STATO = space(1)
  w_RISERVE = space(1)
  w_DATA_INI = ctot('')
  w_DATA_FIN = ctot('')
  w_COD_UTE = space(5)
  w_COGNPART = space(40)
  w_NOMEPART = space(40)
  w_DENOM_PART = space(48)
  w_CODNOM = space(15)
  o_CODNOM = space(15)
  w_CODCAT = space(10)
  w_TIPNOM = space(1)
  w_CODCLI = space(15)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_CodRis = space(5)
  o_CodRis = space(5)
  w_RuolRis = space(5)
  w_ATTSTU = space(1)
  w_ATTGENER = space(1)
  w_UDIENZE = space(1)
  w_ATTFUOSTU = space(1)
  w_SESSTEL = space(1)
  w_ASSENZE = space(1)
  w_DAINSPRE = space(1)
  w_MEMO = space(1)
  w_DAFARE = space(1)
  w_DISPONIB = 0
  w_FiltPerson = space(60)
  w_CODTIPOL = space(5)
  w_NOTE = space(0)
  w_GIUDICE = space(15)
  w_ATLOCALI = space(30)
  w_ENTE = space(10)
  w_CODCOM = space(15)
  o_CODCOM = space(15)
  w_TIPANA = space(1)
  w_CENCOS = space(15)
  w_DesEnte = space(60)
  w_CODSED = space(5)
  o_CODSED = space(5)
  w_ATCODSED = space(10)
  w_UFFICIO = space(10)
  w_DesUffi = space(60)
  w_COMPIMPKEYREAD = 0
  w_COMPIMPKEY = 0
  o_COMPIMPKEY = 0
  w_IMPIANTO = space(10)
  o_IMPIANTO = space(10)
  w_COMPIMP = space(50)
  o_COMPIMP = space(50)
  w_CodUte = 0
  w_ChkProm = space(1)
  o_ChkProm = space(1)
  w_DATA_PROM = ctod('  /  /  ')
  o_DATA_PROM = ctod('  /  /  ')
  w_ORA_PROM = space(2)
  o_ORA_PROM = space(2)
  w_MIN_PROM = space(2)
  o_MIN_PROM = space(2)
  w_TIPORIS = space(1)
  w_DESUTE = space(20)
  w_ATSERIAL = space(20)
  o_ATSERIAL = space(20)
  w_ATSTATUS = space(20)
  w_ATSERIAL3 = space(20)
  w_VARLOG = .F.
  w_DESPRA = space(75)
  w_OBTEST = ctod('  /  /  ')
  w_COMODO = space(30)
  w_DESNOM = space(40)
  w_DESCAT = space(30)
  w_TIPCAT = space(1)
  w_OFDATDOC = ctod('  /  /  ')
  w_EDITCODPRA = .F.
  w_EDITCODNOM = .F.
  w_DESTIPOL = space(50)
  w_DESCRPART = space(40)
  w_DPTIPRIS = space(1)
  w_DESPRA = space(75)
  w_OLDCOMP = space(50)
  w_DESPIA = space(40)
  w_DESNOMGIU = space(60)
  w_DESCRI = space(40)
  w_ATCODNOM = space(15)
  w_CACODIMP = space(10)
  w_FILTPROM = ctot('')
  o_FILTPROM = ctot('')
  w_DESCOS = space(60)
  w_SERPIAN = space(20)
  w_FiltroWeb = space(1)
  o_FiltroWeb = space(1)
  w_FiltPubWeb = space(1)
  w_TGIUDICE = space(15)
  w_DATOBSONOM = ctod('  /  /  ')
  w_CAPNOM = space(9)
  w_INDNOM = space(35)
  w_COMODOGIU = space(60)
  w_OB_TEST = ctod('  /  /  ')
  w_TIPOGRUP = space(1)
  w_FLVISI = space(1)
  w_DatiAttivita = space(10)
  w_Pratica_Attivita = space(0)
  w_SERIAL = space(20)
  w_CONTRA = space(10)
  w_ELCODMOD = space(10)
  w_ELCODIMP = space(10)
  o_ELCODIMP = space(10)
  w_ELCODCOM = 0
  o_ELCODCOM = 0
  w_CODNOM = space(15)
  w_ELRINNOV = 0
  w_ELTIPCON = space(1)
  o_ELTIPCON = space(1)
  w_COTIPCON = space(1)
  w_COCODCON = space(15)
  w_ANDESCRI = space(60)
  w_CODESCON = space(50)
  w_MODESCRI = space(60)
  w_MOTIPCON = space(1)
  w_IMDESCRI = space(50)
  w_COMPIMPR = space(50)
  o_COMPIMPR = space(50)
  w_COMPIMPKEYREADR = 0
  w_OLDCOMPR = space(10)
  w_FLTIPCON = space(1)
  w_DESNOM = space(40)
  w_CURPART = space(10)
  w_PATIPRIS = space(1)
  w_CODPART = space(5)
  w_GRUPART = space(5)
  w_FLESGR = space(1)
  w_Annotazioni = space(1)
  w_Descagg = space(1)
  w_DETTIMP = space(1)
  w_DatiPratica = space(1)
  o_DatiPratica = space(1)
  w_ELCONTR = space(1)
  w_AccodaPreavvisi = space(1)
  w_AccodaNote = space(1)
  w_AccodaDaFare = space(1)
  w_VISUDI = space(1)
  w_DENOM_PART = space(48)
  w_NOMPRG = space(10)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_DATINI_Cose = ctod('  /  /  ')
  o_DATINI_Cose = ctod('  /  /  ')
  w_DATFIN_Cose = ctod('  /  /  ')
  o_DATFIN_Cose = ctod('  /  /  ')
  w_OREINI_Cose = space(2)
  o_OREINI_Cose = space(2)
  w_MININI_Cose = space(2)
  o_MININI_Cose = space(2)
  w_OREFIN_Cose = space(2)
  o_OREFIN_Cose = space(2)
  w_MINFIN_Cose = space(2)
  o_MINFIN_Cose = space(2)
  w_DATA_INI_Cose = ctot('')
  w_DATA_FIN_Cose = ctot('')
  w_IMDESCRI = space(50)
  w_DESRIS = space(80)
  w_COGN_CODRIS = space(40)
  w_NOME_CODRIS = space(40)
  w_DATINIZ = ctod('  /  /  ')
  o_DATINIZ = ctod('  /  /  ')
  w_obsodat1 = space(1)
  o_obsodat1 = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_STADETTDOC = space(1)
  w_AGKRA_ZOOM3 = .NULL.
  w_AGKRA_ZOOM4 = .NULL.
  w_AGKRA_ZOOM = .NULL.
  w_OUT1 = .NULL.
  w_OUT = .NULL.
  w_PART_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_kra
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kraPag1","gsag_kra",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsag_kraPag2","gsag_kra",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettagli")
      .Pages(3).addobject("oPag","tgsag_kraPag3","gsag_kra",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Filtri contratti")
      .Pages(4).addobject("oPag","tgsag_kraPag4","gsag_kra",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Partecipanti/stampa")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsag_kra
    if isalt()
      local oAGKRA_ZOOM
      oAGKRA_ZOOM=this.Parent.GetCtrl("AGKRA_ZOOM")
      oAGKRA_ZOOM.cZoomfile="GSAGAKRA"
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGKRA_ZOOM3 = this.oPgFrm.Pages(1).oPag.AGKRA_ZOOM3
    this.w_AGKRA_ZOOM4 = this.oPgFrm.Pages(1).oPag.AGKRA_ZOOM4
    this.w_AGKRA_ZOOM = this.oPgFrm.Pages(1).oPag.AGKRA_ZOOM
    this.w_OUT1 = this.oPgFrm.Pages(4).oPag.OUT1
    this.w_OUT = this.oPgFrm.Pages(4).oPag.OUT
    this.w_PART_ZOOM = this.oPgFrm.Pages(4).oPag.PART_ZOOM
    DoDefault()
    proc Destroy()
      this.w_AGKRA_ZOOM3 = .NULL.
      this.w_AGKRA_ZOOM4 = .NULL.
      this.w_AGKRA_ZOOM = .NULL.
      this.w_OUT1 = .NULL.
      this.w_OUT = .NULL.
      this.w_PART_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[20]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='PRA_ENTI'
    this.cWorkTables[4]='PRA_UFFI'
    this.cWorkTables[5]='CPUSERS'
    this.cWorkTables[6]='OFF_NOMI'
    this.cWorkTables[7]='CAT_SOGG'
    this.cWorkTables[8]='OFFTIPAT'
    this.cWorkTables[9]='IMP_MAST'
    this.cWorkTables[10]='IMP_DETT'
    this.cWorkTables[11]='CENCOST'
    this.cWorkTables[12]='DES_DIVE'
    this.cWorkTables[13]='GEST_ATTI'
    this.cWorkTables[14]='TIP_RISE'
    this.cWorkTables[15]='PAR_AGEN'
    this.cWorkTables[16]='CAUMATTI'
    this.cWorkTables[17]='CON_TRAS'
    this.cWorkTables[18]='MOD_ELEM'
    this.cWorkTables[19]='CONTI'
    this.cWorkTables[20]='RUO_SOGI'
    return(this.OpenAllTables(20))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TipoRisorsa=space(1)
      .w_CACODICE=space(20)
      .w_CADESCRI=space(254)
      .w_LAYOUT=.f.
      .w_READAZI=space(5)
      .w_FLTPART=space(5)
      .w_DATINI=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_DATFIN=ctod("  /  /  ")
      .w_OREFIN=space(2)
      .w_MINFIN=space(2)
      .w_PATIPRIS=space(1)
      .w_FLESGR=space(1)
      .w_GRUPART=space(5)
      .w_CODPART=space(5)
      .w_CODPRA=space(15)
      .w_PRIORITA=0
      .w_STATO=space(1)
      .w_RISERVE=space(1)
      .w_DATA_INI=ctot("")
      .w_DATA_FIN=ctot("")
      .w_COD_UTE=space(5)
      .w_COGNPART=space(40)
      .w_NOMEPART=space(40)
      .w_DENOM_PART=space(48)
      .w_CODNOM=space(15)
      .w_CODCAT=space(10)
      .w_TIPNOM=space(1)
      .w_CODCLI=space(15)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_CodRis=space(5)
      .w_RuolRis=space(5)
      .w_ATTSTU=space(1)
      .w_ATTGENER=space(1)
      .w_UDIENZE=space(1)
      .w_ATTFUOSTU=space(1)
      .w_SESSTEL=space(1)
      .w_ASSENZE=space(1)
      .w_DAINSPRE=space(1)
      .w_MEMO=space(1)
      .w_DAFARE=space(1)
      .w_DISPONIB=0
      .w_FiltPerson=space(60)
      .w_CODTIPOL=space(5)
      .w_NOTE=space(0)
      .w_GIUDICE=space(15)
      .w_ATLOCALI=space(30)
      .w_ENTE=space(10)
      .w_CODCOM=space(15)
      .w_TIPANA=space(1)
      .w_CENCOS=space(15)
      .w_DesEnte=space(60)
      .w_CODSED=space(5)
      .w_ATCODSED=space(10)
      .w_UFFICIO=space(10)
      .w_DesUffi=space(60)
      .w_COMPIMPKEYREAD=0
      .w_COMPIMPKEY=0
      .w_IMPIANTO=space(10)
      .w_COMPIMP=space(50)
      .w_CodUte=0
      .w_ChkProm=space(1)
      .w_DATA_PROM=ctod("  /  /  ")
      .w_ORA_PROM=space(2)
      .w_MIN_PROM=space(2)
      .w_TIPORIS=space(1)
      .w_DESUTE=space(20)
      .w_ATSERIAL=space(20)
      .w_ATSTATUS=space(20)
      .w_ATSERIAL3=space(20)
      .w_VARLOG=.f.
      .w_DESPRA=space(75)
      .w_OBTEST=ctod("  /  /  ")
      .w_COMODO=space(30)
      .w_DESNOM=space(40)
      .w_DESCAT=space(30)
      .w_TIPCAT=space(1)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_EDITCODPRA=.f.
      .w_EDITCODNOM=.f.
      .w_DESTIPOL=space(50)
      .w_DESCRPART=space(40)
      .w_DPTIPRIS=space(1)
      .w_DESPRA=space(75)
      .w_OLDCOMP=space(50)
      .w_DESPIA=space(40)
      .w_DESNOMGIU=space(60)
      .w_DESCRI=space(40)
      .w_ATCODNOM=space(15)
      .w_CACODIMP=space(10)
      .w_FILTPROM=ctot("")
      .w_DESCOS=space(60)
      .w_SERPIAN=space(20)
      .w_FiltroWeb=space(1)
      .w_FiltPubWeb=space(1)
      .w_TGIUDICE=space(15)
      .w_DATOBSONOM=ctod("  /  /  ")
      .w_CAPNOM=space(9)
      .w_INDNOM=space(35)
      .w_COMODOGIU=space(60)
      .w_OB_TEST=ctod("  /  /  ")
      .w_TIPOGRUP=space(1)
      .w_FLVISI=space(1)
      .w_DatiAttivita=space(10)
      .w_Pratica_Attivita=space(0)
      .w_SERIAL=space(20)
      .w_CONTRA=space(10)
      .w_ELCODMOD=space(10)
      .w_ELCODIMP=space(10)
      .w_ELCODCOM=0
      .w_CODNOM=space(15)
      .w_ELRINNOV=0
      .w_ELTIPCON=space(1)
      .w_COTIPCON=space(1)
      .w_COCODCON=space(15)
      .w_ANDESCRI=space(60)
      .w_CODESCON=space(50)
      .w_MODESCRI=space(60)
      .w_MOTIPCON=space(1)
      .w_IMDESCRI=space(50)
      .w_COMPIMPR=space(50)
      .w_COMPIMPKEYREADR=0
      .w_OLDCOMPR=space(10)
      .w_FLTIPCON=space(1)
      .w_DESNOM=space(40)
      .w_CURPART=space(10)
      .w_PATIPRIS=space(1)
      .w_CODPART=space(5)
      .w_GRUPART=space(5)
      .w_FLESGR=space(1)
      .w_Annotazioni=space(1)
      .w_Descagg=space(1)
      .w_DETTIMP=space(1)
      .w_DatiPratica=space(1)
      .w_ELCONTR=space(1)
      .w_AccodaPreavvisi=space(1)
      .w_AccodaNote=space(1)
      .w_AccodaDaFare=space(1)
      .w_VISUDI=space(1)
      .w_DENOM_PART=space(48)
      .w_NOMPRG=space(10)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      .w_DATINI_Cose=ctod("  /  /  ")
      .w_DATFIN_Cose=ctod("  /  /  ")
      .w_OREINI_Cose=space(2)
      .w_MININI_Cose=space(2)
      .w_OREFIN_Cose=space(2)
      .w_MINFIN_Cose=space(2)
      .w_DATA_INI_Cose=ctot("")
      .w_DATA_FIN_Cose=ctot("")
      .w_IMDESCRI=space(50)
      .w_DESRIS=space(80)
      .w_COGN_CODRIS=space(40)
      .w_NOME_CODRIS=space(40)
      .w_DATINIZ=ctod("  /  /  ")
      .w_obsodat1=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_STADETTDOC=space(1)
        .w_TipoRisorsa = 'P'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CACODICE))
          .link_2_3('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_LAYOUT = Not Empty(  IIF(VARTYPE(this.oParentObject)='C',this.oParentObject,'') )
        .w_READAZI = i_CODAZI
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_READAZI))
          .link_1_3('Full')
        endif
        .w_FLTPART = readdipend( i_CodUte, 'C')
          .DoRTCalc(7,7,.f.)
        .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
          .DoRTCalc(10,10,.f.)
        .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (empty(.w_DATFIN) AND .w_OREFIN="23" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .w_PATIPRIS = ''
        .w_FLESGR = 'N'
        .w_GRUPART = Space(5)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_GRUPART))
          .link_1_25('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODPART))
          .link_1_26('Full')
        endif
        .w_CODPRA = .w_CODCOM
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODPRA))
          .link_1_28('Full')
        endif
        .w_PRIORITA = 0
        .w_STATO = IIF(NOT ISALT(), 'N', InitStato())
        .w_RISERVE = 'L'
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
      .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
          .DoRTCalc(24,25,.f.)
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CODNOM))
          .link_2_6('Full')
        endif
        .w_CODCAT = ''
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_CODCAT))
          .link_2_7('Full')
        endif
        .DoRTCalc(29,33,.f.)
        if not(empty(.w_CodRis))
          .link_2_12('Full')
        endif
        .w_RuolRis = ''
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_RuolRis))
          .link_2_13('Full')
        endif
          .DoRTCalc(35,37,.f.)
        .w_ATTFUOSTU = ' '
          .DoRTCalc(39,43,.f.)
        .w_DISPONIB = 0
        .DoRTCalc(45,46,.f.)
        if not(empty(.w_CODTIPOL))
          .link_2_25('Full')
        endif
        .DoRTCalc(47,48,.f.)
        if not(empty(.w_GIUDICE))
          .link_2_27('Full')
        endif
        .DoRTCalc(49,50,.f.)
        if not(empty(.w_ENTE))
          .link_2_29('Full')
        endif
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_CODCOM))
          .link_2_30('Full')
        endif
        .w_TIPANA = 'C'
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_CENCOS))
          .link_2_32('Full')
        endif
        .DoRTCalc(54,55,.f.)
        if not(empty(.w_CODSED))
          .link_2_35('Full')
        endif
        .w_ATCODSED = .w_CODSED
        .DoRTCalc(57,57,.f.)
        if not(empty(.w_UFFICIO))
          .link_2_37('Full')
        endif
          .DoRTCalc(58,58,.f.)
        .w_COMPIMPKEYREAD = .w_COMPIMPKEY
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_COMPIMPKEYREAD))
          .link_2_39('Full')
        endif
        .w_COMPIMPKEY = 0
        .w_IMPIANTO = IIF(EMPTY(.w_CODSED), space(10), .w_IMPIANTO)
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_IMPIANTO))
          .link_2_41('Full')
        endif
        .DoRTCalc(62,63,.f.)
        if not(empty(.w_CodUte))
          .link_2_43('Full')
        endif
        .w_ChkProm = ' '
          .DoRTCalc(65,65,.f.)
        .w_ORA_PROM = PADL(ALLTRIM(STR(VAL(.w_ORA_PROM))),2,'0')
        .w_MIN_PROM = PADL(ALLTRIM(STR(VAL(.w_MIN_PROM))),2,'0')
      .oPgFrm.Page1.oPag.AGKRA_ZOOM3.Calculate()
        .w_TIPORIS = SPACE(1)
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_TIPORIS))
          .link_2_49('Full')
        endif
          .DoRTCalc(69,69,.f.)
        .w_ATSERIAL = .w_AGKRA_ZOOM.GetVar('ATSERIAL')
        .w_ATSTATUS = .w_AGKRA_ZOOM.GetVar('ATSTATUS')
      .oPgFrm.Page1.oPag.AGKRA_ZOOM4.Calculate(.w_ATSERIAL)
        .w_ATSERIAL3 = .w_AGKRA_ZOOM3.GetVar('ATSERIAL')
      .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
          .DoRTCalc(73,74,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(76,79,.f.)
        .w_OFDATDOC = i_datsys
        .w_EDITCODPRA = .T.
        .w_EDITCODNOM = .T.
          .DoRTCalc(83,90,.f.)
        .w_ATCODNOM = .w_CODNOM
        .w_CACODIMP = .w_IMPIANTO
        .w_FILTPROM = IIF(.w_ChkProm='S' AND !EMPTY(.w_DATA_PROM),cp_CharToDatetime(ALLTR(STR(DAY(.w_DATA_PROM)))+'-'+ALLTR(STR(MONTH(.w_DATA_PROM)))+'-'+ALLTR(STR(YEAR(.w_DATA_PROM)))+' '+.w_ORA_PROM+':'+.w_MIN_PROM+':00', 'dd-mm-yyyy hh:nn:ss'),DTOT(CTOD('  -  -  ')))
        .DoRTCalc(94,95,.f.)
        if not(empty(.w_SERPIAN))
          .link_2_85('Full')
        endif
      .oPgFrm.Page1.oPag.AGKRA_ZOOM.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .w_FiltroWeb = 'T'
        .w_FiltPubWeb = IIF(.w_FiltroWeb='T','',.w_FiltroWeb)
          .DoRTCalc(98,102,.f.)
        .w_OB_TEST = i_INIDAT
          .DoRTCalc(104,107,.f.)
        .w_SERIAL = .w_AGKRA_ZOOM.GetVar('ATSERIAL')
        .DoRTCalc(109,109,.f.)
        if not(empty(.w_CONTRA))
          .link_3_1('Full')
        endif
        .w_ELCODMOD = IIF(.w_ELTIPCON='T', .w_ELCODMOD, SPACE(10) )
        .DoRTCalc(110,110,.f.)
        if not(empty(.w_ELCODMOD))
          .link_3_2('Full')
        endif
        .w_ELCODIMP = IIF(.w_ELTIPCON='P', SPACE(10), .w_ELCODIMP )
        .DoRTCalc(111,111,.f.)
        if not(empty(.w_ELCODIMP))
          .link_3_3('Full')
        endif
        .DoRTCalc(112,113,.f.)
        if not(empty(.w_CODNOM))
          .link_3_5('Full')
        endif
          .DoRTCalc(114,114,.f.)
        .w_ELTIPCON = 'T'
        .w_COTIPCON = 'C'
        .w_COCODCON = IIF(.w_COTIPCON='C', .w_CODCLI, .w_COCODCON)
        .DoRTCalc(117,117,.f.)
        if not(empty(.w_COCODCON))
          .link_3_11('Full')
        endif
          .DoRTCalc(118,122,.f.)
        .w_COMPIMPR = IIF(.w_ELTIPCON='P', Space(50), .w_COMPIMPR)
        .w_COMPIMPKEYREADR = .w_ELCODCOM
        .DoRTCalc(124,124,.f.)
        if not(empty(.w_COMPIMPKEYREADR))
          .link_3_24('Full')
        endif
          .DoRTCalc(125,125,.f.)
        .w_FLTIPCON = .w_ELTIPCON
          .DoRTCalc(127,127,.f.)
        .w_CURPART = .w_PART_ZOOM.cCursor
        .w_PATIPRIS = ''
        .DoRTCalc(130,130,.f.)
        if not(empty(.w_CODPART))
          .link_4_7('Full')
        endif
        .w_GRUPART = Space(5)
        .DoRTCalc(131,131,.f.)
        if not(empty(.w_GRUPART))
          .link_4_8('Full')
        endif
        .w_FLESGR = 'N'
        .w_Annotazioni = 'S'
        .w_Descagg = 'N'
          .DoRTCalc(135,135,.f.)
        .w_DatiPratica = 'S'
          .DoRTCalc(137,137,.f.)
        .w_AccodaPreavvisi = 'N'
        .w_AccodaNote = 'N'
        .w_AccodaDaFare = 'N'
        .w_VISUDI = 'N'
      .oPgFrm.Page4.oPag.OUT1.Calculate()
      .oPgFrm.Page4.oPag.OUT.Calculate()
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
      .oPgFrm.Page4.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
        .w_NOMPRG = iif(!isalt(),'GSAG1SAT','GSAG_SAT')
        .w_DTBSO_CAUMATTI = i_DatSys
          .DoRTCalc(145,146,.f.)
        .w_OREINI_Cose = RIGHT('00'+IIF(EMPTY(.w_OREINI_Cose),'00',.w_OREINI_Cose),2)
        .w_MININI_Cose = RIGHT('00'+IIF(empty(.w_MININI_Cose),'00',.w_MININI_Cose),2)
        .w_OREFIN_Cose = RIGHT('00'+IIF(empty(.w_OREFIN_Cose) OR .w_OREFIN_Cose="00", '23', .w_OREFIN_Cose),2)
        .w_MINFIN_Cose = RIGHT('00'+IIF(empty(.w_MINFIN_Cose) OR (empty(.w_DATFIN_Cose) AND .w_OREFIN_Cose="23" AND .w_MINFIN_Cose="00"), '59', .w_MINFIN_Cose),2)
        .w_DATA_INI_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI_Cose),  ALLTR(STR(DAY(.w_DATINI_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATINI_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATINI_Cose)))+' '+.w_OREINI_Cose+':'+.w_MININI_Cose+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN_Cose),  ALLTR(STR(DAY(.w_DATFIN_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATFIN_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATFIN_Cose)))+' '+.w_OREFIN_Cose+':'+.w_MINFIN_Cose+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
          .DoRTCalc(153,153,.f.)
        .w_DESRIS = ALLTRIM(.w_COGN_CODRIS)+' '+ALLTRIM(.w_NOME_CODRIS)
          .DoRTCalc(155,156,.f.)
        .w_DATINIZ = i_datsys
        .w_obsodat1 = 'N'
          .DoRTCalc(159,159,.f.)
        .w_STADETTDOC = IIF(.w_STATO='P','S','N')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_5.enabled = this.oPgFrm.Page2.oPag.oBtn_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_66.enabled = this.oPgFrm.Page1.oPag.oBtn_1_66.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_69.enabled = this.oPgFrm.Page1.oPag.oBtn_1_69.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_71.enabled = this.oPgFrm.Page1.oPag.oBtn_1_71.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_73.enabled = this.oPgFrm.Page1.oPag.oBtn_1_73.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_29.enabled = this.oPgFrm.Page3.oPag.oBtn_3_29.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_10.enabled = this.oPgFrm.Page4.oPag.oBtn_4_10.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_22.enabled = this.oPgFrm.Page4.oPag.oBtn_4_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsag_kra
    this.w_OUT.visible=iif(IsAlt(),.T.,.F.)
    this.w_OUT1.visible=iif(IsAlt(),.F.,.T.)
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_READAZI<>.w_READAZI
            .w_READAZI = i_CODAZI
          .link_1_3('Full')
        endif
        if .o_FLTPART<>.w_FLTPART
            .w_FLTPART = readdipend( i_CodUte, 'C')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_OREINI<>.w_OREINI
            .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        endif
        if .o_OREINI<>.w_OREINI
          .Calculate_MVFLEXPGGM()
        endif
        if .o_MININI<>.w_MININI
            .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        endif
        if .o_MININI<>.w_MININI
          .Calculate_SUGRFFLBCH()
        endif
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(10,10,.t.)
        if .o_OREFIN<>.w_OREFIN.or. .o_DATFIN<>.w_DATFIN
            .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        endif
        if .o_OREFIN<>.w_OREFIN
          .Calculate_FPJEAUJXWE()
        endif
        if .o_MINFIN<>.w_MINFIN.or. .o_DATFIN<>.w_DATFIN
            .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (empty(.w_DATFIN) AND .w_OREFIN="23" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        endif
        if .o_MINFIN<>.w_MINFIN
          .Calculate_KTQUMLVLEF()
        endif
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .DoRTCalc(13,13,.t.)
        if .o_CODPART<>.w_CODPART
            .w_FLESGR = 'N'
        endif
        if .o_CODPART<>.w_CODPART
            .w_GRUPART = Space(5)
          .link_1_25('Full')
        endif
        if .o_PATIPRIS<>.w_PATIPRIS
          .link_1_26('Full')
        endif
        if .o_CODCOM<>.w_CODCOM
            .w_CODPRA = .w_CODCOM
          .link_1_28('Full')
        endif
        .DoRTCalc(18,20,.t.)
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN<>.w_DATFIN.or. .o_OREFIN<>.w_OREFIN.or. .o_MINFIN<>.w_MINFIN
            .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        if .o_CODPART<>.w_CODPART
            .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
        endif
        .DoRTCalc(24,25,.t.)
        if .o_CODPART<>.w_CODPART
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        .DoRTCalc(27,27,.t.)
        if .o_CODNOM<>.w_CODNOM
            .w_CODCAT = ''
          .link_2_7('Full')
        endif
        .DoRTCalc(29,33,.t.)
        if .o_CodRis<>.w_CodRis
            .w_RuolRis = ''
          .link_2_13('Full')
        endif
        .DoRTCalc(35,55,.t.)
            .w_ATCODSED = .w_CODSED
        .DoRTCalc(57,58,.t.)
        if .o_IMPIANTO<>.w_IMPIANTO.or. .o_COMPIMPKEY<>.w_COMPIMPKEY
            .w_COMPIMPKEYREAD = .w_COMPIMPKEY
          .link_2_39('Full')
        endif
        if .o_COMPIMP<>.w_COMPIMP.or. .o_IMPIANTO<>.w_IMPIANTO
            .w_COMPIMPKEY = 0
        endif
        if .o_CODSED<>.w_CODSED
            .w_IMPIANTO = IIF(EMPTY(.w_CODSED), space(10), .w_IMPIANTO)
          .link_2_41('Full')
        endif
        .DoRTCalc(62,65,.t.)
        if .o_ORA_PROM<>.w_ORA_PROM
            .w_ORA_PROM = PADL(ALLTRIM(STR(VAL(.w_ORA_PROM))),2,'0')
        endif
        if .o_MIN_PROM<>.w_MIN_PROM
            .w_MIN_PROM = PADL(ALLTRIM(STR(VAL(.w_MIN_PROM))),2,'0')
        endif
        .oPgFrm.Page1.oPag.AGKRA_ZOOM3.Calculate()
        .DoRTCalc(68,69,.t.)
            .w_ATSERIAL = .w_AGKRA_ZOOM.GetVar('ATSERIAL')
            .w_ATSTATUS = .w_AGKRA_ZOOM.GetVar('ATSTATUS')
        .oPgFrm.Page1.oPag.AGKRA_ZOOM4.Calculate(.w_ATSERIAL)
            .w_ATSERIAL3 = .w_AGKRA_ZOOM3.GetVar('ATSERIAL')
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        if .o_CodPart<>.w_CodPart
          .Calculate_NHGZPPOKMU()
        endif
        .DoRTCalc(73,90,.t.)
            .w_ATCODNOM = .w_CODNOM
            .w_CACODIMP = .w_IMPIANTO
        if .o_ChkProm<>.w_ChkProm
          .Calculate_WWJSDCQGIA()
        endif
        if .o_DATA_PROM<>.w_DATA_PROM.or. .o_MIN_PROM<>.w_MIN_PROM.or. .o_ORA_PROM<>.w_ORA_PROM.or. .o_FILTPROM<>.w_FILTPROM.or. .o_ChkProm<>.w_ChkProm
            .w_FILTPROM = IIF(.w_ChkProm='S' AND !EMPTY(.w_DATA_PROM),cp_CharToDatetime(ALLTR(STR(DAY(.w_DATA_PROM)))+'-'+ALLTR(STR(MONTH(.w_DATA_PROM)))+'-'+ALLTR(STR(YEAR(.w_DATA_PROM)))+' '+.w_ORA_PROM+':'+.w_MIN_PROM+':00', 'dd-mm-yyyy hh:nn:ss'),DTOT(CTOD('  -  -  ')))
        endif
        .oPgFrm.Page1.oPag.AGKRA_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .DoRTCalc(94,96,.t.)
        if .o_FiltroWeb<>.w_FiltroWeb
            .w_FiltPubWeb = IIF(.w_FiltroWeb='T','',.w_FiltroWeb)
        endif
        .DoRTCalc(98,107,.t.)
            .w_SERIAL = .w_AGKRA_ZOOM.GetVar('ATSERIAL')
        if .o_ATSERIAL<>.w_ATSERIAL
          .Calculate_FZORDMLBAP()
        endif
        if .o_IMPIANTO<>.w_IMPIANTO
          .Calculate_ECMUJGUXHY()
        endif
        .DoRTCalc(109,109,.t.)
        if .o_ELTIPCON<>.w_ELTIPCON
            .w_ELCODMOD = IIF(.w_ELTIPCON='T', .w_ELCODMOD, SPACE(10) )
          .link_3_2('Full')
        endif
        if .o_ELTIPCON<>.w_ELTIPCON
            .w_ELCODIMP = IIF(.w_ELTIPCON='P', SPACE(10), .w_ELCODIMP )
          .link_3_3('Full')
        endif
        .DoRTCalc(112,115,.t.)
            .w_COTIPCON = 'C'
        if .o_CODNOM<>.w_CODNOM
            .w_COCODCON = IIF(.w_COTIPCON='C', .w_CODCLI, .w_COCODCON)
          .link_3_11('Full')
        endif
        .DoRTCalc(118,122,.t.)
        if .o_ELCODIMP<>.w_ELCODIMP.or. .o_ELCODCOM<>.w_ELCODCOM.or. .o_ELTIPCON<>.w_ELTIPCON
            .w_COMPIMPR = IIF(.w_ELTIPCON='P', Space(50), .w_COMPIMPR)
        endif
        if .o_ELCODIMP<>.w_ELCODIMP.or. .o_ELCODCOM<>.w_ELCODCOM
            .w_COMPIMPKEYREADR = .w_ELCODCOM
          .link_3_24('Full')
        endif
        .DoRTCalc(125,125,.t.)
            .w_FLTIPCON = .w_ELTIPCON
        if .o_ELCODIMP<>.w_ELCODIMP
          .Calculate_KYUJBSQRYV()
        endif
        if .o_COMPIMPR<>.w_COMPIMPR
          .Calculate_KEQKODGKRQ()
        endif
        if .o_DatiPratica<>.w_DatiPratica
          .Calculate_DIDIGCQSMK()
        endif
        if .o_DATINIZ<>.w_DATINIZ.or. .o_obsodat1<>.w_obsodat1
          .Calculate_JQCKGKZTVV()
        endif
        if .o_PATIPRIS<>.w_PATIPRIS.or. .o_DATINIZ<>.w_DATINIZ.or. .o_obsodat1<>.w_obsodat1
          .Calculate_BLTFQIRAHO()
        endif
        if .o_CODPART<>.w_CODPART
          .Calculate_FWWDAFBQBE()
        endif
        .DoRTCalc(127,129,.t.)
        if .o_PATIPRIS<>.w_PATIPRIS
          .link_4_7('Full')
        endif
        if .o_CODPART<>.w_CODPART
            .w_GRUPART = Space(5)
          .link_4_8('Full')
        endif
        if .o_CODPART<>.w_CODPART
            .w_FLESGR = 'N'
        endif
        .oPgFrm.Page4.oPag.OUT1.Calculate()
        .oPgFrm.Page4.oPag.OUT.Calculate()
        .DoRTCalc(133,141,.t.)
        if .o_CODPART<>.w_CODPART
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        .oPgFrm.Page4.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
        .DoRTCalc(143,146,.t.)
        if .o_OREINI_Cose<>.w_OREINI_Cose
            .w_OREINI_Cose = RIGHT('00'+IIF(EMPTY(.w_OREINI_Cose),'00',.w_OREINI_Cose),2)
        endif
        if .o_MININI_Cose<>.w_MININI_Cose
            .w_MININI_Cose = RIGHT('00'+IIF(empty(.w_MININI_Cose),'00',.w_MININI_Cose),2)
        endif
        if .o_OREFIN_Cose<>.w_OREFIN_Cose.or. .o_DATFIN_Cose<>.w_DATFIN_Cose
            .w_OREFIN_Cose = RIGHT('00'+IIF(empty(.w_OREFIN_Cose) OR .w_OREFIN_Cose="00", '23', .w_OREFIN_Cose),2)
        endif
        if .o_MINFIN_Cose<>.w_MINFIN_Cose.or. .o_DATFIN_Cose<>.w_DATFIN_Cose
            .w_MINFIN_Cose = RIGHT('00'+IIF(empty(.w_MINFIN_Cose) OR (empty(.w_DATFIN_Cose) AND .w_OREFIN_Cose="23" AND .w_MINFIN_Cose="00"), '59', .w_MINFIN_Cose),2)
        endif
        if .o_DATINI_Cose<>.w_DATINI_Cose
          .Calculate_HWTLQYNXHT()
        endif
        if .o_DATFIN_Cose<>.w_DATFIN_Cose
          .Calculate_JBMQEMONXV()
        endif
        if .o_DATINI_Cose<>.w_DATINI_Cose.or. .o_OREINI_Cose<>.w_OREINI_Cose.or. .o_MININI_Cose<>.w_MININI_Cose
            .w_DATA_INI_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI_Cose),  ALLTR(STR(DAY(.w_DATINI_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATINI_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATINI_Cose)))+' '+.w_OREINI_Cose+':'+.w_MININI_Cose+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN_Cose<>.w_DATFIN_Cose.or. .o_OREFIN_Cose<>.w_OREFIN_Cose.or. .o_MINFIN_Cose<>.w_MINFIN_Cose
            .w_DATA_FIN_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN_Cose),  ALLTR(STR(DAY(.w_DATFIN_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATFIN_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATFIN_Cose)))+' '+.w_OREFIN_Cose+':'+.w_MINFIN_Cose+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        .DoRTCalc(153,153,.t.)
        if .o_CodRis<>.w_CodRis
            .w_DESRIS = ALLTRIM(.w_COGN_CODRIS)+' '+ALLTRIM(.w_NOME_CODRIS)
        endif
        .DoRTCalc(155,159,.t.)
        if .o_STATO<>.w_STATO
            .w_STADETTDOC = IIF(.w_STATO='P','S','N')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.AGKRA_ZOOM3.Calculate()
        .oPgFrm.Page1.oPag.AGKRA_ZOOM4.Calculate(.w_ATSERIAL)
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page1.oPag.AGKRA_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page4.oPag.OUT1.Calculate()
        .oPgFrm.Page4.oPag.OUT.Calculate()
        .oPgFrm.Page4.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
    endwith
  return

  proc Calculate_NRTSRHMIVM()
    with this
          * --- Inizializza date
          gsag_ba3(this;
              ,'AGEN';
             )
    endwith
  endproc
  proc Calculate_MVFLEXPGGM()
    with this
          * --- Resetto w_oreini
          .w_OREINI = .ZeroFill(.w_OREINI)
    endwith
  endproc
  proc Calculate_SUGRFFLBCH()
    with this
          * --- Resetto w_minini
          .w_MININI = .ZeroFill(.w_MININI)
    endwith
  endproc
  proc Calculate_FPJEAUJXWE()
    with this
          * --- Resetto w_orefin
          .w_OREFIN = .ZeroFill(.w_OREFIN)
    endwith
  endproc
  proc Calculate_KTQUMLVLEF()
    with this
          * --- Resetto w_minfin
          .w_MINFIN = .ZeroFill(.w_MINFIN)
    endwith
  endproc
  proc Calculate_LYBISUKBKM()
    with this
          * --- Imposta data odierna
          .w_DATINI = i_DatSys
          .w_DATFIN = i_DatSys
          .w_OREINI = '00'
          .w_MININI = '00'
          .w_OREFIN = '23'
          .w_MINFIN = '59'
          .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'))
          .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'))
    endwith
  endproc
  proc Calculate_PFKYJKKLTX()
    with this
          * --- DecremGiorni
          .w_DATINI = IIF(NOT EMPTY(.w_DATINI), .w_DATINI - 1, .w_DATINI)
          .w_DATFIN = IIF(NOT EMPTY(.w_DATFIN), .w_DATFIN - 1, .w_DATFIN)
          .w_DATA_INI = IIF(NOT EMPTY(.w_DATINI), cp_CharToDatetime(DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00'), .w_DATA_INI)
          .w_DATA_FIN = IIF(NOT EMPTY(.w_DATFIN), cp_CharToDatetime(DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00'), .w_DATA_FIN)
    endwith
  endproc
  proc Calculate_EPTWHVVJZD()
    with this
          * --- IncremGiorni
          .w_DATINI = IIF(NOT EMPTY(.w_DATINI), .w_DATINI + 1, .w_DATINI)
          .w_DATFIN = IIF(NOT EMPTY(.w_DATFIN), .w_DATFIN + 1, .w_DATFIN)
          .w_DATA_INI = IIF(NOT EMPTY(.w_DATINI), cp_CharToDatetime(DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00'), .w_DATA_INI)
          .w_DATA_FIN = IIF(NOT EMPTY(.w_DATFIN), cp_CharToDatetime(DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00'), .w_DATA_FIN)
    endwith
  endproc
  proc Calculate_DOBRBAQOOH()
    with this
          * --- Attiva la prima pagina
          .oPgFrm.ActivePage = 1
    endwith
  endproc
  proc Calculate_UFLZVBNZXV()
    with this
          * --- Attiva font in attivit�
          GSAG_BIF(this;
             )
    endwith
  endproc
  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .cComment = IIF(EMPTY(.w_CodPart), IIF(.w_LAYOUT, AH_MSGFORMAT("Elenco sintetico attivit�"),AH_MSGFORMAT("Elenco attivit�")),IIF(.w_LAYOUT, AH_MSGFORMAT("Elenco sintetico attivit� di %1",alltrim(.w_DENOM_PART)),AH_MSGFORMAT("Elenco attivit� di %1",alltrim(.w_DENOM_PART))))
          .Caption = .cComment
    endwith
  endproc
  proc Calculate_WNNFNWVMSW()
    with this
          * --- Resetto la sede
          .w_CODSED = ' '
          .w_DESCRI = ' '
    endwith
  endproc
  proc Calculate_YBYZXGTXOU()
    with this
          * --- Resetto l'impianto
          .w_IMPIANTO = ' '
          .w_COMPIMP = ' '
    endwith
  endproc
  proc Calculate_WWJSDCQGIA()
    with this
          * --- Imposta data/ora filtro promemoria
          .w_FILTPROM = IIF(.w_ChkProm='S',DATETIME(),DTOT(CTOD('  -  -  ')))
          .w_DATA_PROM = IIF(.w_ChkProm='S',TTOD(.w_FILTPROM),CTOD('  -  -  '))
          .w_ORA_PROM = IIF(.w_ChkProm='S',PADL(ALLTRIM(STR(HOUR(.w_FILTPROM))),2,'0'),'00')
          .w_MIN_PROM = IIF(.w_ChkProm='S',PADL(ALLTRIM(STR(MINUTE(.w_FILTPROM))),2,'0'),'00')
    endwith
  endproc
  proc Calculate_NFPSBYKGYI()
    with this
          * --- Check Selezione attivita
          GSAG_BR1(this;
             )
    endwith
  endproc
  proc Calculate_AMPWWRGFBF()
    with this
          * --- Sposta zoom a video
          .w_LAYOUT = Not Empty(  IIF(VARTYPE(this.oParentObject)='C',this.oParentObject,'') )
          .w_AGKRA_ZOOM4.visible = Not .w_LAYOUT
          .w_AGKRA_ZOOM3.Top = iif( .w_LAYOUT , .w_AGKRA_ZOOM4.Top , .w_AGKRA_ZOOM3.Top)
          .w_AGKRA_ZOOM3.Left = iif( .w_LAYOUT , .w_AGKRA_ZOOM4.Left , .w_AGKRA_ZOOM3.Left)
          .w_AGKRA_ZOOM.Width = iif( .w_LAYOUT , .w_AGKRA_ZOOM.Width+.w_AGKRA_ZOOM3.Width , .w_AGKRA_ZOOM.Width)
          .w_AGKRA_ZOOM3.Width = iif( .w_LAYOUT , .w_AGKRA_ZOOM.Width , .w_AGKRA_ZOOM3.Width )
          .w_AGKRA_ZOOM3.cHeight = iif( .w_LAYOUT , .w_AGKRA_ZOOM4.Height , .w_AGKRA_ZOOM3.cHeight )
          .w_AGKRA_ZOOM3.Height = iif( .w_LAYOUT , .w_AGKRA_ZOOM4.Height , .w_AGKRA_ZOOM3.Height )
          .w_AGKRA_ZOOM.DisableSelMenu = LOOKTAB( 'PAR_AGEN' , 'PAGESRIS' , 'PACODAZI' , i_codazi )='S'
          .w_AGKRA_ZOOM3.DisableSelMenu = LOOKTAB( 'PAR_AGEN' , 'PAGESRIS' , 'PACODAZI' , i_codazi )='S'
    endwith
  endproc
  proc Calculate_FZORDMLBAP()
    with this
          * --- Attivit� selezionata
          gsag_bag(this;
              ,'SELEZIONATO_KRA';
             )
    endwith
  endproc
  proc Calculate_ECMUJGUXHY()
    with this
          * --- Azzera chiave impianto
          .w_COMPIMPKEY = 0
    endwith
  endproc
  proc Calculate_KYUJBSQRYV()
    with this
          * --- Elimina informazioni da codice componente
          .w_ELCODCOM = 0
          .w_COMPIMPR = ""
    endwith
  endproc
  proc Calculate_KEQKODGKRQ()
    with this
          * --- Elimina informazioni da codice componente
          .w_ELCODCOM = IIF(EMPTY(.w_COMPIMPR), 0, .w_ELCODCOM)
    endwith
  endproc
  proc Calculate_DIDIGCQSMK()
    with this
          * --- Azzera w_VISUDI
          .w_VISUDI = IIF(.w_DatiPratica<>'S', 'N', .w_VISUDI)
    endwith
  endproc
  proc Calculate_JQCKGKZTVV()
    with this
          * --- Azzera w_CODPART
          .w_CODPART = space(5)
          .link_1_26('Full')
          .link_4_7('Full')
          .w_DENOM_PART = space(48)
    endwith
  endproc
  proc Calculate_BLTFQIRAHO()
    with this
          * --- Attiva zoom selezionando le righe
          GSAG_BPP(this;
              ,'ATTIVAZOOM';
              ,.w_PART_ZOOM;
              ,.w_CODPART;
              ,this;
             )
    endwith
  endproc
  proc Calculate_FWWDAFBQBE()
    with this
          * --- Seleziona righe
          GSAG_BPP(this;
              ,'SELRIGA_ELE';
              ,.w_PART_ZOOM;
              ,.w_CODPART;
              ,this;
             )
    endwith
  endproc
  proc Calculate_HWTLQYNXHT()
    with this
          * --- Azzera ore- minuti inizio
          .w_OREINI_Cose = IIF(EMPTY(.w_DATINI_Cose),'00',.w_OREINI_Cose)
          .w_MININI_Cose = IIF(EMPTY(.w_DATINI_Cose),'00',.w_MININI_Cose)
    endwith
  endproc
  proc Calculate_JBMQEMONXV()
    with this
          * --- Azzera ore- minuti fine
          .w_OREFIN_Cose = IIF(EMPTY(.w_DATFIN_Cose),'00',.w_OREFIN_Cose)
          .w_MINFIN_Cose = IIF(EMPTY(.w_DATFIN_Cose),'00',.w_MINFIN_Cose)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOREINI_1_7.enabled = this.oPgFrm.Page1.oPag.oOREINI_1_7.mCond()
    this.oPgFrm.Page1.oPag.oMININI_1_9.enabled = this.oPgFrm.Page1.oPag.oMININI_1_9.mCond()
    this.oPgFrm.Page1.oPag.oOREFIN_1_17.enabled = this.oPgFrm.Page1.oPag.oOREFIN_1_17.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_19.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_19.mCond()
    this.oPgFrm.Page1.oPag.oGRUPART_1_25.enabled = this.oPgFrm.Page1.oPag.oGRUPART_1_25.mCond()
    this.oPgFrm.Page1.oPag.oCODPRA_1_28.enabled = this.oPgFrm.Page1.oPag.oCODPRA_1_28.mCond()
    this.oPgFrm.Page2.oPag.oCODNOM_2_6.enabled = this.oPgFrm.Page2.oPag.oCODNOM_2_6.mCond()
    this.oPgFrm.Page2.oPag.oCODCAT_2_7.enabled = this.oPgFrm.Page2.oPag.oCODCAT_2_7.mCond()
    this.oPgFrm.Page2.oPag.oRuolRis_2_13.enabled = this.oPgFrm.Page2.oPag.oRuolRis_2_13.mCond()
    this.oPgFrm.Page2.oPag.oGIUDICE_2_27.enabled = this.oPgFrm.Page2.oPag.oGIUDICE_2_27.mCond()
    this.oPgFrm.Page2.oPag.oCODSED_2_35.enabled = this.oPgFrm.Page2.oPag.oCODSED_2_35.mCond()
    this.oPgFrm.Page2.oPag.oCOMPIMP_2_42.enabled = this.oPgFrm.Page2.oPag.oCOMPIMP_2_42.mCond()
    this.oPgFrm.Page2.oPag.oDATA_PROM_2_45.enabled = this.oPgFrm.Page2.oPag.oDATA_PROM_2_45.mCond()
    this.oPgFrm.Page2.oPag.oORA_PROM_2_46.enabled = this.oPgFrm.Page2.oPag.oORA_PROM_2_46.mCond()
    this.oPgFrm.Page2.oPag.oMIN_PROM_2_47.enabled = this.oPgFrm.Page2.oPag.oMIN_PROM_2_47.mCond()
    this.oPgFrm.Page3.oPag.oCONTRA_3_1.enabled = this.oPgFrm.Page3.oPag.oCONTRA_3_1.mCond()
    this.oPgFrm.Page3.oPag.oELCODMOD_3_2.enabled = this.oPgFrm.Page3.oPag.oELCODMOD_3_2.mCond()
    this.oPgFrm.Page3.oPag.oELCODIMP_3_3.enabled = this.oPgFrm.Page3.oPag.oELCODIMP_3_3.mCond()
    this.oPgFrm.Page3.oPag.oCODNOM_3_5.enabled = this.oPgFrm.Page3.oPag.oCODNOM_3_5.mCond()
    this.oPgFrm.Page3.oPag.oELRINNOV_3_6.enabled = this.oPgFrm.Page3.oPag.oELRINNOV_3_6.mCond()
    this.oPgFrm.Page3.oPag.oCOMPIMPR_3_23.enabled = this.oPgFrm.Page3.oPag.oCOMPIMPR_3_23.mCond()
    this.oPgFrm.Page4.oPag.oGRUPART_4_8.enabled = this.oPgFrm.Page4.oPag.oGRUPART_4_8.mCond()
    this.oPgFrm.Page4.oPag.oAccodaNote_4_17.enabled = this.oPgFrm.Page4.oPag.oAccodaNote_4_17.mCond()
    this.oPgFrm.Page4.oPag.oAccodaDaFare_4_18.enabled = this.oPgFrm.Page4.oPag.oAccodaDaFare_4_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_22.enabled = this.oPgFrm.Page4.oPag.oBtn_4_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show2
    i_show2=not(IsALT())
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Filtri contratti"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oFLESGR_1_24.visible=!this.oPgFrm.Page1.oPag.oFLESGR_1_24.mHide()
    this.oPgFrm.Page1.oPag.oGRUPART_1_25.visible=!this.oPgFrm.Page1.oPag.oGRUPART_1_25.mHide()
    this.oPgFrm.Page1.oPag.oCODPRA_1_28.visible=!this.oPgFrm.Page1.oPag.oCODPRA_1_28.mHide()
    this.oPgFrm.Page1.oPag.oSTATO_1_30.visible=!this.oPgFrm.Page1.oPag.oSTATO_1_30.mHide()
    this.oPgFrm.Page1.oPag.oRISERVE_1_31.visible=!this.oPgFrm.Page1.oPag.oRISERVE_1_31.mHide()
    this.oPgFrm.Page2.oPag.oCODCAT_2_7.visible=!this.oPgFrm.Page2.oPag.oCODCAT_2_7.mHide()
    this.oPgFrm.Page2.oPag.oNUMDOC_2_10.visible=!this.oPgFrm.Page2.oPag.oNUMDOC_2_10.mHide()
    this.oPgFrm.Page2.oPag.oALFDOC_2_11.visible=!this.oPgFrm.Page2.oPag.oALFDOC_2_11.mHide()
    this.oPgFrm.Page2.oPag.oCodRis_2_12.visible=!this.oPgFrm.Page2.oPag.oCodRis_2_12.mHide()
    this.oPgFrm.Page2.oPag.oRuolRis_2_13.visible=!this.oPgFrm.Page2.oPag.oRuolRis_2_13.mHide()
    this.oPgFrm.Page2.oPag.oUDIENZE_2_16.visible=!this.oPgFrm.Page2.oPag.oUDIENZE_2_16.mHide()
    this.oPgFrm.Page2.oPag.oGIUDICE_2_27.visible=!this.oPgFrm.Page2.oPag.oGIUDICE_2_27.mHide()
    this.oPgFrm.Page2.oPag.oENTE_2_29.visible=!this.oPgFrm.Page2.oPag.oENTE_2_29.mHide()
    this.oPgFrm.Page2.oPag.oCODCOM_2_30.visible=!this.oPgFrm.Page2.oPag.oCODCOM_2_30.mHide()
    this.oPgFrm.Page2.oPag.oTIPANA_2_31.visible=!this.oPgFrm.Page2.oPag.oTIPANA_2_31.mHide()
    this.oPgFrm.Page2.oPag.oDesEnte_2_34.visible=!this.oPgFrm.Page2.oPag.oDesEnte_2_34.mHide()
    this.oPgFrm.Page2.oPag.oCODSED_2_35.visible=!this.oPgFrm.Page2.oPag.oCODSED_2_35.mHide()
    this.oPgFrm.Page2.oPag.oUFFICIO_2_37.visible=!this.oPgFrm.Page2.oPag.oUFFICIO_2_37.mHide()
    this.oPgFrm.Page2.oPag.oDesUffi_2_38.visible=!this.oPgFrm.Page2.oPag.oDesUffi_2_38.mHide()
    this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_40.visible=!this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_40.mHide()
    this.oPgFrm.Page2.oPag.oIMPIANTO_2_41.visible=!this.oPgFrm.Page2.oPag.oIMPIANTO_2_41.mHide()
    this.oPgFrm.Page2.oPag.oCOMPIMP_2_42.visible=!this.oPgFrm.Page2.oPag.oCOMPIMP_2_42.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    this.oPgFrm.Page2.oPag.oTIPORIS_2_49.visible=!this.oPgFrm.Page2.oPag.oTIPORIS_2_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_52.visible=!this.oPgFrm.Page2.oPag.oStr_2_52.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_53.visible=!this.oPgFrm.Page2.oPag.oStr_2_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oDESPRA_1_60.visible=!this.oPgFrm.Page1.oPag.oDESPRA_1_60.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_62.visible=!this.oPgFrm.Page1.oPag.oBtn_1_62.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_63.visible=!this.oPgFrm.Page1.oPag.oBtn_1_63.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_64.visible=!this.oPgFrm.Page1.oPag.oBtn_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_64.visible=!this.oPgFrm.Page2.oPag.oStr_2_64.mHide()
    this.oPgFrm.Page2.oPag.oDESPRA_2_65.visible=!this.oPgFrm.Page2.oPag.oDESPRA_2_65.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_67.visible=!this.oPgFrm.Page2.oPag.oStr_2_67.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_68.visible=!this.oPgFrm.Page2.oPag.oStr_2_68.mHide()
    this.oPgFrm.Page2.oPag.oDESPIA_2_69.visible=!this.oPgFrm.Page2.oPag.oDESPIA_2_69.mHide()
    this.oPgFrm.Page2.oPag.oDESNOMGIU_2_70.visible=!this.oPgFrm.Page2.oPag.oDESNOMGIU_2_70.mHide()
    this.oPgFrm.Page2.oPag.oDESCRI_2_72.visible=!this.oPgFrm.Page2.oPag.oDESCRI_2_72.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_73.visible=!this.oPgFrm.Page2.oPag.oStr_2_73.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_81.visible=!this.oPgFrm.Page2.oPag.oStr_2_81.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_83.visible=!this.oPgFrm.Page2.oPag.oStr_2_83.mHide()
    this.oPgFrm.Page2.oPag.oSERPIAN_2_85.visible=!this.oPgFrm.Page2.oPag.oSERPIAN_2_85.mHide()
    this.oPgFrm.Page2.oPag.oFiltroWeb_2_86.visible=!this.oPgFrm.Page2.oPag.oFiltroWeb_2_86.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_88.visible=!this.oPgFrm.Page2.oPag.oStr_2_88.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_94.visible=!this.oPgFrm.Page2.oPag.oStr_2_94.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_95.visible=!this.oPgFrm.Page2.oPag.oStr_2_95.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_82.visible=!this.oPgFrm.Page1.oPag.oStr_1_82.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_98.visible=!this.oPgFrm.Page2.oPag.oStr_2_98.mHide()
    this.oPgFrm.Page3.oPag.oELCODCOM_3_4.visible=!this.oPgFrm.Page3.oPag.oELCODCOM_3_4.mHide()
    this.oPgFrm.Page3.oPag.oCOCODCON_3_11.visible=!this.oPgFrm.Page3.oPag.oCOCODCON_3_11.mHide()
    this.oPgFrm.Page3.oPag.oANDESCRI_3_12.visible=!this.oPgFrm.Page3.oPag.oANDESCRI_3_12.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_13.visible=!this.oPgFrm.Page3.oPag.oStr_3_13.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_14.visible=!this.oPgFrm.Page3.oPag.oStr_3_14.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_20.visible=!this.oPgFrm.Page3.oPag.oStr_3_20.mHide()
    this.oPgFrm.Page3.oPag.oCOMPIMPR_3_23.visible=!this.oPgFrm.Page3.oPag.oCOMPIMPR_3_23.mHide()
    this.oPgFrm.Page4.oPag.oGRUPART_4_8.visible=!this.oPgFrm.Page4.oPag.oGRUPART_4_8.mHide()
    this.oPgFrm.Page4.oPag.oFLESGR_4_9.visible=!this.oPgFrm.Page4.oPag.oFLESGR_4_9.mHide()
    this.oPgFrm.Page4.oPag.oDETTIMP_4_13.visible=!this.oPgFrm.Page4.oPag.oDETTIMP_4_13.mHide()
    this.oPgFrm.Page4.oPag.oDatiPratica_4_14.visible=!this.oPgFrm.Page4.oPag.oDatiPratica_4_14.mHide()
    this.oPgFrm.Page4.oPag.oELCONTR_4_15.visible=!this.oPgFrm.Page4.oPag.oELCONTR_4_15.mHide()
    this.oPgFrm.Page4.oPag.oAccodaNote_4_17.visible=!this.oPgFrm.Page4.oPag.oAccodaNote_4_17.mHide()
    this.oPgFrm.Page4.oPag.oAccodaDaFare_4_18.visible=!this.oPgFrm.Page4.oPag.oAccodaDaFare_4_18.mHide()
    this.oPgFrm.Page4.oPag.oVISUDI_4_19.visible=!this.oPgFrm.Page4.oPag.oVISUDI_4_19.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_28.visible=!this.oPgFrm.Page4.oPag.oStr_4_28.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_113.visible=!this.oPgFrm.Page2.oPag.oBtn_2_113.mHide()
    this.oPgFrm.Page2.oPag.oDESRIS_2_115.visible=!this.oPgFrm.Page2.oPag.oDESRIS_2_115.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_116.visible=!this.oPgFrm.Page2.oPag.oStr_2_116.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_117.visible=!this.oPgFrm.Page2.oPag.oStr_2_117.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_120.visible=!this.oPgFrm.Page2.oPag.oStr_2_120.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_121.visible=!this.oPgFrm.Page2.oPag.oStr_2_121.mHide()
    this.oPgFrm.Page4.oPag.oSTADETTDOC_4_36.visible=!this.oPgFrm.Page4.oPag.oSTADETTDOC_4_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_kra
    * Necessario per DB2 per evitare di eseguire query con variabili non inizializzate
    IF cevent='AGKRA_ZOOM3 before query'
       This.w_DATA_INI=iif(Empty(This.w_DATA_INI),cp_CharToDatetime(  DTOC(i_inidat)+' 00:00:00', 'dd-mm-yyyy hh:nn:ss'),This.w_DATA_INI)
       This.w_DATA_FIN=iif(Empty(This.w_DATA_FIN),cp_CharToDatetime(  DTOC(i_findat)+' 23:59:59', 'dd-mm-yyyy hh:nn:ss'),This.w_DATA_FIN)
    Endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_NRTSRHMIVM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
        if lower(cEvent)==lower("ImpostaDataOdierna")
          .Calculate_LYBISUKBKM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DecremGiorni")
          .Calculate_PFKYJKKLTX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("IncremGiorni")
          .Calculate_EPTWHVVJZD()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
        if lower(cEvent)==lower("Ricerca")
          .Calculate_DOBRBAQOOH()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.AGKRA_ZOOM3.Event(cEvent)
        if lower(cEvent)==lower("CodpartChange3")
          .Calculate_UFLZVBNZXV()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.AGKRA_ZOOM4.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CODNOM Changed")
          .Calculate_WNNFNWVMSW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CODSED Changed")
          .Calculate_YBYZXGTXOU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.AGKRA_ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
        if lower(cEvent)==lower("w_AGKRA_ZOOM row checked") or lower(cEvent)==lower("w_AGKRA_ZOOM3 row checked")
          .Calculate_NFPSBYKGYI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("FormLoad")
          .Calculate_AMPWWRGFBF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DecremGiorni") or lower(cEvent)==lower("ImpostaDataOdierna") or lower(cEvent)==lower("IncremGiorni") or lower(cEvent)==lower("Ricerca")
          .Calculate_FZORDMLBAP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_PART_ZOOM after query")
          .Calculate_BLTFQIRAHO()
          bRefresh=.t.
        endif
      .oPgFrm.Page4.oPag.OUT1.Event(cEvent)
      .oPgFrm.Page4.oPag.OUT.Event(cEvent)
      .oPgFrm.Page4.oPag.PART_ZOOM.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kra
    * --- Al checked/unchecked della riga sullo zoom non � immediatamente visualizzato
    * --- o nascosto il bottone completa (la HideControls scatta solo al cambio
    * --- di riga dello zoom, per cui forzo la chiamata).
    If upper(cEvent)='W_AGKRA_ZOOM ROW CHECKED' OR Upper(cEvent)='W_AGKRA_ZOOM ROW UNCHECKED' OR upper(cEvent)='W_AGKRA_ZOOM3 ROW CHECKED' OR Upper(cEvent)='W_AGKRA_ZOOM3 ROW UNCHECKED'
        this.mHideControls()
    endif
    IF (cEvent)='Ricerca'
       DO gsag_bag WITH THIS,'SELEZIONATO_KRA'
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODICE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CACODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CACODICE))
          select CACODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODICE) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oCACODICE_2_3'),i_cWhere,'',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODICE)
            select CACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODICE = NVL(_Link_.CACODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CACODICE = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLVISI,PAFLAPP1,PAFLATG1,PAFLUDI1,PAFLSES1,PAFLASS1,PAFLINP1,PAFLNOT1,PAFLDAF1";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PAFLVISI,PAFLAPP1,PAFLATG1,PAFLUDI1,PAFLSES1,PAFLASS1,PAFLINP1,PAFLNOT1,PAFLDAF1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_FLVISI = NVL(_Link_.PAFLVISI,space(1))
      this.w_ATTSTU = NVL(_Link_.PAFLAPP1,space(1))
      this.w_ATTGENER = NVL(_Link_.PAFLATG1,space(1))
      this.w_UDIENZE = NVL(_Link_.PAFLUDI1,space(1))
      this.w_SESSTEL = NVL(_Link_.PAFLSES1,space(1))
      this.w_ASSENZE = NVL(_Link_.PAFLASS1,space(1))
      this.w_DAINSPRE = NVL(_Link_.PAFLINP1,space(1))
      this.w_MEMO = NVL(_Link_.PAFLNOT1,space(1))
      this.w_DAFARE = NVL(_Link_.PAFLDAF1,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_FLVISI = space(1)
      this.w_ATTSTU = space(1)
      this.w_ATTGENER = space(1)
      this.w_UDIENZE = space(1)
      this.w_SESSTEL = space(1)
      this.w_ASSENZE = space(1)
      this.w_DAINSPRE = space(1)
      this.w_MEMO = space(1)
      this.w_DAFARE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPART
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_GRUPART))
          select DPCODICE,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oGRUPART_1_25'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPART)
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPART = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPOGRUP = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPART = space(5)
      endif
      this.w_TIPOGRUP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
        endif
        this.w_GRUPART = space(5)
        this.w_TIPOGRUP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPART
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_CODPART))
          select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oCODPART_1_26'),i_cWhere,'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODPART)
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPART = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPART = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
      this.w_DPTIPRIS = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS)  AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1='N') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODPART = space(5)
        this.w_COGNPART = space(40)
        this.w_NOMEPART = space(40)
        this.w_DESCRPART = space(40)
        this.w_DPTIPRIS = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_bzz',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRA))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRA_1_28'),i_cWhere,'gsar_bzz',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(75))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESPRA = space(75)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_2_6'),i_cWhere,'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(30))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
      this.w_CODCLI = NVL(_Link_.NOCODCLI,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_DESNOM = space(40)
      this.w_COMODO = space(30)
      this.w_TIPNOM = space(1)
      this.w_CODCLI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM = space(15)
        this.w_DESNOM = space(40)
        this.w_COMODO = space(30)
        this.w_TIPNOM = space(1)
        this.w_CODCLI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SOGG_IDX,3]
    i_lTable = "CAT_SOGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2], .t., this.CAT_SOGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACS',True,'CAT_SOGG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCAT like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODCAT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODCAT',trim(this.w_CODCAT))
          select CSCODCAT,CSDESCAT,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODCAT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.CSCODCAT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStr(trim(this.w_CODCAT)+"%");

            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CAT_SOGG','*','CSCODCAT',cp_AbsName(oSource.parent,'oCODCAT_2_7'),i_cWhere,'GSPR_ACS',"Categorie soggetti esterni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',oSource.xKey(1))
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',this.w_CODCAT)
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.CSCODCAT,space(10))
      this.w_DESCAT = NVL(_Link_.CSDESCAT,space(30))
      this.w_TIPCAT = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(10)
      endif
      this.w_DESCAT = space(30)
      this.w_TIPCAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCAT,1)
      cp_ShowWarn(i_cKey,this.CAT_SOGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodRis
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodRis) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_CodRis))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CodRis)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CodRis) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCodRis_2_12'),i_cWhere,'',"Risorse interne",'GSARPADP2.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodRis)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CodRis);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_CodRis)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodRis = NVL(_Link_.DPCODICE,space(5))
      this.w_COGN_CODRIS = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME_CODRIS = NVL(_Link_.DPNOME,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CodRis = space(5)
      endif
      this.w_COGN_CODRIS = space(40)
      this.w_NOME_CODRIS = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1='N') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CodRis = space(5)
        this.w_COGN_CODRIS = space(40)
        this.w_NOME_CODRIS = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodRis Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RuolRis
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RUO_SOGI_IDX,3]
    i_lTable = "RUO_SOGI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2], .t., this.RUO_SOGI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RuolRis) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ARI',True,'RUO_SOGI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RSCODRUO like "+cp_ToStrODBC(trim(this.w_RuolRis)+"%");

          i_ret=cp_SQL(i_nConn,"select RSCODRUO,RSDESRUO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RSCODRUO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RSCODRUO',trim(this.w_RuolRis))
          select RSCODRUO,RSDESRUO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RSCODRUO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RuolRis)==trim(_Link_.RSCODRUO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RSDESRUO like "+cp_ToStrODBC(trim(this.w_RuolRis)+"%");

            i_ret=cp_SQL(i_nConn,"select RSCODRUO,RSDESRUO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RSDESRUO like "+cp_ToStr(trim(this.w_RuolRis)+"%");

            select RSCODRUO,RSDESRUO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RuolRis) and !this.bDontReportError
            deferred_cp_zoom('RUO_SOGI','*','RSCODRUO',cp_AbsName(oSource.parent,'oRuolRis_2_13'),i_cWhere,'GSPR_ARI',"Ruoli soggetti interni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RSCODRUO,RSDESRUO";
                     +" from "+i_cTable+" "+i_lTable+" where RSCODRUO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RSCODRUO',oSource.xKey(1))
            select RSCODRUO,RSDESRUO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RuolRis)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RSCODRUO,RSDESRUO";
                   +" from "+i_cTable+" "+i_lTable+" where RSCODRUO="+cp_ToStrODBC(this.w_RuolRis);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RSCODRUO',this.w_RuolRis)
            select RSCODRUO,RSDESRUO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RuolRis = NVL(_Link_.RSCODRUO,space(5))
      this.w_COMODO = NVL(_Link_.RSDESRUO,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_RuolRis = space(5)
      endif
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2])+'\'+cp_ToStr(_Link_.RSCODRUO,1)
      cp_ShowWarn(i_cKey,this.RUO_SOGI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RuolRis Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODTIPOL
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFFTIPAT_IDX,3]
    i_lTable = "OFFTIPAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2], .t., this.OFFTIPAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODTIPOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFFTIPAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODTIP like "+cp_ToStrODBC(trim(this.w_CODTIPOL)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODTIP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODTIP',trim(this.w_CODTIPOL))
          select TACODTIP,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODTIP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODTIPOL)==trim(_Link_.TACODTIP) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStrODBC(trim(this.w_CODTIPOL)+"%");

            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStr(trim(this.w_CODTIPOL)+"%");

            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODTIPOL) and !this.bDontReportError
            deferred_cp_zoom('OFFTIPAT','*','TACODTIP',cp_AbsName(oSource.parent,'oCODTIPOL_2_25'),i_cWhere,'',"Tipologie attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',oSource.xKey(1))
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODTIPOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(this.w_CODTIPOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',this.w_CODTIPOL)
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODTIPOL = NVL(_Link_.TACODTIP,space(5))
      this.w_DESTIPOL = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODTIPOL = space(5)
      endif
      this.w_DESTIPOL = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])+'\'+cp_ToStr(_Link_.TACODTIP,1)
      cp_ShowWarn(i_cKey,this.OFFTIPAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODTIPOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GIUDICE
  func Link_2_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GIUDICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_GIUDICE))
          select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GIUDICE)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_GIUDICE)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_GIUDICE)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NOINDIRI like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NOINDIRI like "+cp_ToStr(trim(this.w_GIUDICE)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GIUDICE) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oGIUDICE_2_27'),i_cWhere,'GSAR_ANO',"Giudici",'GSPR_ZGI.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GIUDICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_GIUDICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_GIUDICE)
            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GIUDICE = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOMGIU = NVL(_Link_.NODESCRI,space(60))
      this.w_COMODOGIU = NVL(_Link_.NODESCR2,space(60))
      this.w_INDNOM = NVL(_Link_.NOINDIRI,space(35))
      this.w_CAPNOM = NVL(_Link_.NO___CAP,space(9))
      this.w_DATOBSONOM = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GIUDICE = space(15)
      endif
      this.w_DESNOMGIU = space(60)
      this.w_COMODOGIU = space(60)
      this.w_INDNOM = space(35)
      this.w_CAPNOM = space(9)
      this.w_DATOBSONOM = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAlt() OR (.w_DATOBSONOM>i_datsys OR EMPTY(.w_DATOBSONOM) ) And CHKGIUDI( .w_GIUDICE )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Nominativo inesistente o obsoleto non associato come giudice ad alcuna pratica")
        endif
        this.w_GIUDICE = space(15)
        this.w_DESNOMGIU = space(60)
        this.w_COMODOGIU = space(60)
        this.w_INDNOM = space(35)
        this.w_CAPNOM = space(9)
        this.w_DATOBSONOM = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GIUDICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ENTE
  func Link_2_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRA_ENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EPCODICE like "+cp_ToStrODBC(trim(this.w_ENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EPCODICE',trim(this.w_ENTE))
          select EPCODICE,EPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ENTE)==trim(_Link_.EPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" EPDESCRI like "+cp_ToStrODBC(trim(this.w_ENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" EPDESCRI like "+cp_ToStr(trim(this.w_ENTE)+"%");

            select EPCODICE,EPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ENTE) and !this.bDontReportError
            deferred_cp_zoom('PRA_ENTI','*','EPCODICE',cp_AbsName(oSource.parent,'oENTE_2_29'),i_cWhere,'',"Enti pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',oSource.xKey(1))
            select EPCODICE,EPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_ENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_ENTE)
            select EPCODICE,EPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ENTE = NVL(_Link_.EPCODICE,space(10))
      this.w_DesEnte = NVL(_Link_.EPDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ENTE = space(10)
      endif
      this.w_DesEnte = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_2_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_bzz',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODCOM)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_2_30'),i_cWhere,'gsar_bzz',"Commessa",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(75))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESPRA = space(75)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENCOS
  func Link_2_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CENCOS))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCENCOS_2_32'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENCOS)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCOS = NVL(_Link_.CCDESPIA,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CENCOS = space(15)
      endif
      this.w_DESCOS = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSED
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MDD',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_CODSED)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPNOM);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_TIPNOM;
                     ,'DDCODICE',this.w_CODCLI;
                     ,'DDCODDES',trim(this.w_CODSED))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSED)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSED) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCODSED_2_35'),i_cWhere,'GSAR_MDD',"Sedi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPNOM<>oSource.xKey(1);
           .or. this.w_CODCLI<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPNOM);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_CODSED);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPNOM);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_TIPNOM;
                       ,'DDCODICE',this.w_CODCLI;
                       ,'DDCODDES',this.w_CODSED)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSED = NVL(_Link_.DDCODDES,space(5))
      this.w_DESCRI = NVL(_Link_.DDNOMDES,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODSED = space(5)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UFFICIO
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_UFFI_IDX,3]
    i_lTable = "PRA_UFFI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2], .t., this.PRA_UFFI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UFFICIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRA_UFFI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UFCODICE like "+cp_ToStrODBC(trim(this.w_UFFICIO)+"%");

          i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UFCODICE',trim(this.w_UFFICIO))
          select UFCODICE,UFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UFFICIO)==trim(_Link_.UFCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" UFDESCRI like "+cp_ToStrODBC(trim(this.w_UFFICIO)+"%");

            i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" UFDESCRI like "+cp_ToStr(trim(this.w_UFFICIO)+"%");

            select UFCODICE,UFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_UFFICIO) and !this.bDontReportError
            deferred_cp_zoom('PRA_UFFI','*','UFCODICE',cp_AbsName(oSource.parent,'oUFFICIO_2_37'),i_cWhere,'',"Uffici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',oSource.xKey(1))
            select UFCODICE,UFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UFFICIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(this.w_UFFICIO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',this.w_UFFICIO)
            select UFCODICE,UFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UFFICIO = NVL(_Link_.UFCODICE,space(10))
      this.w_DESUFFI = NVL(_Link_.UFDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_UFFICIO = space(10)
      endif
      this.w_DESUFFI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2])+'\'+cp_ToStr(_Link_.UFCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_UFFI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UFFICIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPIMPKEYREAD
  func Link_2_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPIMPKEYREAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPIMPKEYREAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_COMPIMPKEYREAD);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO;
                       ,'CPROWNUM',this.w_COMPIMPKEYREAD)
            select IMCODICE,CPROWNUM,IMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPIMPKEYREAD = NVL(_Link_.CPROWNUM,0)
      this.w_COMPIMP = NVL(_Link_.IMDESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_COMPIMPKEYREAD = 0
      endif
      this.w_COMPIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPIMPKEYREAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMPIANTO
  func Link_2_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMPIANTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIM',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_IMPIANTO)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_IMPIANTO))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMPIANTO)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IMPIANTO) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oIMPIANTO_2_41'),i_cWhere,'GSAG_MIM',"Impianto",'GSAG_AAT.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMPIANTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMPIANTO = NVL(_Link_.IMCODICE,space(10))
      this.w_IMDESCRI = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IMPIANTO = space(10)
      endif
      this.w_IMDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMPIANTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodUte
  func Link_2_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodUte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CodUte);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CodUte)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CodUte) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCodUte_2_43'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodUte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CodUte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CodUte)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodUte = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CodUte = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodUte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPORIS
  func Link_2_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_RISE_IDX,3]
    i_lTable = "TIP_RISE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2], .t., this.TIP_RISE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPORIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ATR',True,'TIP_RISE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_TIPORIS)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_TIPORIS))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPORIS)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPORIS) and !this.bDontReportError
            deferred_cp_zoom('TIP_RISE','*','TRCODICE',cp_AbsName(oSource.parent,'oTIPORIS_2_49'),i_cWhere,'GSAG_ATR',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPORIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_TIPORIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_TIPORIS)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPORIS = NVL(_Link_.TRCODICE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPORIS = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_RISE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPORIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SERPIAN
  func Link_2_85(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GEST_ATTI_IDX,3]
    i_lTable = "GEST_ATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2], .t., this.GEST_ATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERPIAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_AGA',True,'GEST_ATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATSERIAL like "+cp_ToStrODBC(trim(this.w_SERPIAN)+"%");

          i_ret=cp_SQL(i_nConn,"select ATSERIAL,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATSERIAL',trim(this.w_SERPIAN))
          select ATSERIAL,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SERPIAN)==trim(_Link_.ATSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SERPIAN) and !this.bDontReportError
            deferred_cp_zoom('GEST_ATTI','*','ATSERIAL',cp_AbsName(oSource.parent,'oSERPIAN_2_85'),i_cWhere,'GSAG_AGA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATSERIAL,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATSERIAL',oSource.xKey(1))
            select ATSERIAL,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERPIAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATSERIAL,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATSERIAL="+cp_ToStrODBC(this.w_SERPIAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATSERIAL',this.w_SERPIAN)
            select ATSERIAL,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERPIAN = NVL(_Link_.ATSERIAL,space(20))
      this.w_DESPIA = NVL(_Link_.ATDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SERPIAN = space(20)
      endif
      this.w_DESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2])+'\'+cp_ToStr(_Link_.ATSERIAL,1)
      cp_ShowWarn(i_cKey,this.GEST_ATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERPIAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTRA
  func Link_3_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACA',True,'CON_TRAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COSERIAL like "+cp_ToStrODBC(trim(this.w_CONTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON,COTIPCON,COCODCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COSERIAL',trim(this.w_CONTRA))
          select COSERIAL,CODESCON,COTIPCON,COCODCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONTRA)==trim(_Link_.COSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONTRA) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAS','*','COSERIAL',cp_AbsName(oSource.parent,'oCONTRA_3_1'),i_cWhere,'GSAG_ACA',"Contratti",'GSAGCKRA.CON_TRAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON,COTIPCON,COCODCON";
                     +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',oSource.xKey(1))
            select COSERIAL,CODESCON,COTIPCON,COCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON,COTIPCON,COCODCON";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_CONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_CONTRA)
            select COSERIAL,CODESCON,COTIPCON,COCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTRA = NVL(_Link_.COSERIAL,space(10))
      this.w_CODESCON = NVL(_Link_.CODESCON,space(50))
      this.w_COTIPCON = NVL(_Link_.COTIPCON,space(1))
      this.w_COCODCON = NVL(_Link_.COCODCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CONTRA = space(10)
      endif
      this.w_CODESCON = space(50)
      this.w_COTIPCON = space(1)
      this.w_COCODCON = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODMOD
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_AME',True,'MOD_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_ELCODMOD)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_ELCODMOD))
          select MOCODICE,MODESCRI,MOTIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODMOD)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODMOD) and !this.bDontReportError
            deferred_cp_zoom('MOD_ELEM','*','MOCODICE',cp_AbsName(oSource.parent,'oELCODMOD_3_2'),i_cWhere,'GSAG_AME',"Modelli",'GSAG_KZM.MOD_ELEM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI,MOTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_ELCODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_ELCODMOD)
            select MOCODICE,MODESCRI,MOTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODMOD = NVL(_Link_.MOCODICE,space(10))
      this.w_MODESCRI = NVL(_Link_.MODESCRI,space(60))
      this.w_MOTIPCON = NVL(_Link_.MOTIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODMOD = space(10)
      endif
      this.w_MODESCRI = space(60)
      this.w_MOTIPCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ELTIPCON='T' OR .w_MOTIPCON=.w_ELTIPCON
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice modello inesistente, incongruente o non di tipo a pacchetto")
        endif
        this.w_ELCODMOD = space(10)
        this.w_MODESCRI = space(60)
        this.w_MOTIPCON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODIMP
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_ELCODIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_ELCODIMP))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODIMP)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODIMP) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oELCODIMP_3_3'),i_cWhere,'',"Impianti",'GSAGIKRA.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_ELCODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_ELCODIMP)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODIMP = NVL(_Link_.IMCODICE,space(10))
      this.w_IMDESCRI = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODIMP = space(10)
      endif
      this.w_IMDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_3_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_3_5'),i_cWhere,'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(30))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
      this.w_CODCLI = NVL(_Link_.NOCODCLI,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_DESNOM = space(40)
      this.w_COMODO = space(30)
      this.w_TIPNOM = space(1)
      this.w_CODCLI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM = space(15)
        this.w_DESNOM = space(40)
        this.w_COMODO = space(30)
        this.w_TIPNOM = space(1)
        this.w_CODCLI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODCON
  func Link_3_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_COCODCON = space(15)
      endif
      this.w_ANDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPIMPKEYREADR
  func Link_3_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPIMPKEYREADR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPIMPKEYREADR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_COMPIMPKEYREADR);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_ELCODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_ELCODIMP;
                       ,'CPROWNUM',this.w_COMPIMPKEYREADR)
            select IMCODICE,CPROWNUM,IMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPIMPKEYREADR = NVL(_Link_.CPROWNUM,0)
      this.w_COMPIMPR = NVL(_Link_.IMDESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_COMPIMPKEYREADR = 0
      endif
      this.w_COMPIMPR = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPIMPKEYREADR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPART
  func Link_4_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_CODPART))
          select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oCODPART_4_7'),i_cWhere,'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODPART)
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPART = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPART = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
      this.w_DPTIPRIS = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS)  AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1='N') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODPART = space(5)
        this.w_COGNPART = space(40)
        this.w_NOMEPART = space(40)
        this.w_DESCRPART = space(40)
        this.w_DPTIPRIS = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPART
  func Link_4_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_GRUPART))
          select DPCODICE,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oGRUPART_4_8'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPART)
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPART = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPOGRUP = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPART = space(5)
      endif
      this.w_TIPOGRUP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
        endif
        this.w_GRUPART = space(5)
        this.w_TIPOGRUP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page2.oPag.oCACODICE_2_3.value==this.w_CACODICE)
      this.oPgFrm.Page2.oPag.oCACODICE_2_3.value=this.w_CACODICE
    endif
    if not(this.oPgFrm.Page2.oPag.oCADESCRI_2_4.value==this.w_CADESCRI)
      this.oPgFrm.Page2.oPag.oCADESCRI_2_4.value=this.w_CADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_6.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_6.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINI_1_7.value==this.w_OREINI)
      this.oPgFrm.Page1.oPag.oOREINI_1_7.value=this.w_OREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_9.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_9.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_13.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_13.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOREFIN_1_17.value==this.w_OREFIN)
      this.oPgFrm.Page1.oPag.oOREFIN_1_17.value=this.w_OREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_19.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_19.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPATIPRIS_1_23.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page1.oPag.oPATIPRIS_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLESGR_1_24.RadioValue()==this.w_FLESGR)
      this.oPgFrm.Page1.oPag.oFLESGR_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUPART_1_25.value==this.w_GRUPART)
      this.oPgFrm.Page1.oPag.oGRUPART_1_25.value=this.w_GRUPART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPART_1_26.value==this.w_CODPART)
      this.oPgFrm.Page1.oPag.oCODPART_1_26.value=this.w_CODPART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRA_1_28.value==this.w_CODPRA)
      this.oPgFrm.Page1.oPag.oCODPRA_1_28.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPRIORITA_1_29.RadioValue()==this.w_PRIORITA)
      this.oPgFrm.Page1.oPag.oPRIORITA_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_30.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRISERVE_1_31.RadioValue()==this.w_RISERVE)
      this.oPgFrm.Page1.oPag.oRISERVE_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_PART_1_49.value==this.w_DENOM_PART)
      this.oPgFrm.Page1.oPag.oDENOM_PART_1_49.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page2.oPag.oCODNOM_2_6.value==this.w_CODNOM)
      this.oPgFrm.Page2.oPag.oCODNOM_2_6.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCAT_2_7.RadioValue()==this.w_CODCAT)
      this.oPgFrm.Page2.oPag.oCODCAT_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDOC_2_10.value==this.w_NUMDOC)
      this.oPgFrm.Page2.oPag.oNUMDOC_2_10.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oALFDOC_2_11.value==this.w_ALFDOC)
      this.oPgFrm.Page2.oPag.oALFDOC_2_11.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCodRis_2_12.value==this.w_CodRis)
      this.oPgFrm.Page2.oPag.oCodRis_2_12.value=this.w_CodRis
    endif
    if not(this.oPgFrm.Page2.oPag.oRuolRis_2_13.RadioValue()==this.w_RuolRis)
      this.oPgFrm.Page2.oPag.oRuolRis_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATTSTU_2_14.RadioValue()==this.w_ATTSTU)
      this.oPgFrm.Page2.oPag.oATTSTU_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATTGENER_2_15.RadioValue()==this.w_ATTGENER)
      this.oPgFrm.Page2.oPag.oATTGENER_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oUDIENZE_2_16.RadioValue()==this.w_UDIENZE)
      this.oPgFrm.Page2.oPag.oUDIENZE_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSESSTEL_2_18.RadioValue()==this.w_SESSTEL)
      this.oPgFrm.Page2.oPag.oSESSTEL_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oASSENZE_2_19.RadioValue()==this.w_ASSENZE)
      this.oPgFrm.Page2.oPag.oASSENZE_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDAINSPRE_2_20.RadioValue()==this.w_DAINSPRE)
      this.oPgFrm.Page2.oPag.oDAINSPRE_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMEMO_2_21.RadioValue()==this.w_MEMO)
      this.oPgFrm.Page2.oPag.oMEMO_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDAFARE_2_22.RadioValue()==this.w_DAFARE)
      this.oPgFrm.Page2.oPag.oDAFARE_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDISPONIB_2_23.RadioValue()==this.w_DISPONIB)
      this.oPgFrm.Page2.oPag.oDISPONIB_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFiltPerson_2_24.value==this.w_FiltPerson)
      this.oPgFrm.Page2.oPag.oFiltPerson_2_24.value=this.w_FiltPerson
    endif
    if not(this.oPgFrm.Page2.oPag.oCODTIPOL_2_25.value==this.w_CODTIPOL)
      this.oPgFrm.Page2.oPag.oCODTIPOL_2_25.value=this.w_CODTIPOL
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTE_2_26.value==this.w_NOTE)
      this.oPgFrm.Page2.oPag.oNOTE_2_26.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oGIUDICE_2_27.value==this.w_GIUDICE)
      this.oPgFrm.Page2.oPag.oGIUDICE_2_27.value=this.w_GIUDICE
    endif
    if not(this.oPgFrm.Page2.oPag.oATLOCALI_2_28.value==this.w_ATLOCALI)
      this.oPgFrm.Page2.oPag.oATLOCALI_2_28.value=this.w_ATLOCALI
    endif
    if not(this.oPgFrm.Page2.oPag.oENTE_2_29.value==this.w_ENTE)
      this.oPgFrm.Page2.oPag.oENTE_2_29.value=this.w_ENTE
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCOM_2_30.value==this.w_CODCOM)
      this.oPgFrm.Page2.oPag.oCODCOM_2_30.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPANA_2_31.RadioValue()==this.w_TIPANA)
      this.oPgFrm.Page2.oPag.oTIPANA_2_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCENCOS_2_32.value==this.w_CENCOS)
      this.oPgFrm.Page2.oPag.oCENCOS_2_32.value=this.w_CENCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oDesEnte_2_34.value==this.w_DesEnte)
      this.oPgFrm.Page2.oPag.oDesEnte_2_34.value=this.w_DesEnte
    endif
    if not(this.oPgFrm.Page2.oPag.oCODSED_2_35.value==this.w_CODSED)
      this.oPgFrm.Page2.oPag.oCODSED_2_35.value=this.w_CODSED
    endif
    if not(this.oPgFrm.Page2.oPag.oUFFICIO_2_37.value==this.w_UFFICIO)
      this.oPgFrm.Page2.oPag.oUFFICIO_2_37.value=this.w_UFFICIO
    endif
    if not(this.oPgFrm.Page2.oPag.oDesUffi_2_38.value==this.w_DesUffi)
      this.oPgFrm.Page2.oPag.oDesUffi_2_38.value=this.w_DesUffi
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_40.value==this.w_COMPIMPKEY)
      this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_40.value=this.w_COMPIMPKEY
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPIANTO_2_41.value==this.w_IMPIANTO)
      this.oPgFrm.Page2.oPag.oIMPIANTO_2_41.value=this.w_IMPIANTO
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMPIMP_2_42.value==this.w_COMPIMP)
      this.oPgFrm.Page2.oPag.oCOMPIMP_2_42.value=this.w_COMPIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oCodUte_2_43.value==this.w_CodUte)
      this.oPgFrm.Page2.oPag.oCodUte_2_43.value=this.w_CodUte
    endif
    if not(this.oPgFrm.Page2.oPag.oChkProm_2_44.RadioValue()==this.w_ChkProm)
      this.oPgFrm.Page2.oPag.oChkProm_2_44.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDATA_PROM_2_45.value==this.w_DATA_PROM)
      this.oPgFrm.Page2.oPag.oDATA_PROM_2_45.value=this.w_DATA_PROM
    endif
    if not(this.oPgFrm.Page2.oPag.oORA_PROM_2_46.value==this.w_ORA_PROM)
      this.oPgFrm.Page2.oPag.oORA_PROM_2_46.value=this.w_ORA_PROM
    endif
    if not(this.oPgFrm.Page2.oPag.oMIN_PROM_2_47.value==this.w_MIN_PROM)
      this.oPgFrm.Page2.oPag.oMIN_PROM_2_47.value=this.w_MIN_PROM
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPORIS_2_49.RadioValue()==this.w_TIPORIS)
      this.oPgFrm.Page2.oPag.oTIPORIS_2_49.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESUTE_2_50.value==this.w_DESUTE)
      this.oPgFrm.Page2.oPag.oDESUTE_2_50.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRA_1_60.value==this.w_DESPRA)
      this.oPgFrm.Page1.oPag.oDESPRA_1_60.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOM_2_55.value==this.w_DESNOM)
      this.oPgFrm.Page2.oPag.oDESNOM_2_55.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESTIPOL_2_62.value==this.w_DESTIPOL)
      this.oPgFrm.Page2.oPag.oDESTIPOL_2_62.value=this.w_DESTIPOL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRA_2_65.value==this.w_DESPRA)
      this.oPgFrm.Page2.oPag.oDESPRA_2_65.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPIA_2_69.value==this.w_DESPIA)
      this.oPgFrm.Page2.oPag.oDESPIA_2_69.value=this.w_DESPIA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOMGIU_2_70.value==this.w_DESNOMGIU)
      this.oPgFrm.Page2.oPag.oDESNOMGIU_2_70.value=this.w_DESNOMGIU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCRI_2_72.value==this.w_DESCRI)
      this.oPgFrm.Page2.oPag.oDESCRI_2_72.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOS_2_84.value==this.w_DESCOS)
      this.oPgFrm.Page2.oPag.oDESCOS_2_84.value=this.w_DESCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oSERPIAN_2_85.value==this.w_SERPIAN)
      this.oPgFrm.Page2.oPag.oSERPIAN_2_85.value=this.w_SERPIAN
    endif
    if not(this.oPgFrm.Page2.oPag.oFiltroWeb_2_86.RadioValue()==this.w_FiltroWeb)
      this.oPgFrm.Page2.oPag.oFiltroWeb_2_86.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPratica_Attivita_1_86.value==this.w_Pratica_Attivita)
      this.oPgFrm.Page1.oPag.oPratica_Attivita_1_86.value=this.w_Pratica_Attivita
    endif
    if not(this.oPgFrm.Page3.oPag.oCONTRA_3_1.value==this.w_CONTRA)
      this.oPgFrm.Page3.oPag.oCONTRA_3_1.value=this.w_CONTRA
    endif
    if not(this.oPgFrm.Page3.oPag.oELCODMOD_3_2.value==this.w_ELCODMOD)
      this.oPgFrm.Page3.oPag.oELCODMOD_3_2.value=this.w_ELCODMOD
    endif
    if not(this.oPgFrm.Page3.oPag.oELCODIMP_3_3.value==this.w_ELCODIMP)
      this.oPgFrm.Page3.oPag.oELCODIMP_3_3.value=this.w_ELCODIMP
    endif
    if not(this.oPgFrm.Page3.oPag.oELCODCOM_3_4.value==this.w_ELCODCOM)
      this.oPgFrm.Page3.oPag.oELCODCOM_3_4.value=this.w_ELCODCOM
    endif
    if not(this.oPgFrm.Page3.oPag.oCODNOM_3_5.value==this.w_CODNOM)
      this.oPgFrm.Page3.oPag.oCODNOM_3_5.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page3.oPag.oELRINNOV_3_6.value==this.w_ELRINNOV)
      this.oPgFrm.Page3.oPag.oELRINNOV_3_6.value=this.w_ELRINNOV
    endif
    if not(this.oPgFrm.Page3.oPag.oELTIPCON_3_7.RadioValue()==this.w_ELTIPCON)
      this.oPgFrm.Page3.oPag.oELTIPCON_3_7.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCOCODCON_3_11.value==this.w_COCODCON)
      this.oPgFrm.Page3.oPag.oCOCODCON_3_11.value=this.w_COCODCON
    endif
    if not(this.oPgFrm.Page3.oPag.oANDESCRI_3_12.value==this.w_ANDESCRI)
      this.oPgFrm.Page3.oPag.oANDESCRI_3_12.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oCODESCON_3_15.value==this.w_CODESCON)
      this.oPgFrm.Page3.oPag.oCODESCON_3_15.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page3.oPag.oMODESCRI_3_16.value==this.w_MODESCRI)
      this.oPgFrm.Page3.oPag.oMODESCRI_3_16.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oIMDESCRI_3_22.value==this.w_IMDESCRI)
      this.oPgFrm.Page3.oPag.oIMDESCRI_3_22.value=this.w_IMDESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oCOMPIMPR_3_23.value==this.w_COMPIMPR)
      this.oPgFrm.Page3.oPag.oCOMPIMPR_3_23.value=this.w_COMPIMPR
    endif
    if not(this.oPgFrm.Page3.oPag.oDESNOM_3_31.value==this.w_DESNOM)
      this.oPgFrm.Page3.oPag.oDESNOM_3_31.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page4.oPag.oPATIPRIS_4_6.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page4.oPag.oPATIPRIS_4_6.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oCODPART_4_7.value==this.w_CODPART)
      this.oPgFrm.Page4.oPag.oCODPART_4_7.value=this.w_CODPART
    endif
    if not(this.oPgFrm.Page4.oPag.oGRUPART_4_8.value==this.w_GRUPART)
      this.oPgFrm.Page4.oPag.oGRUPART_4_8.value=this.w_GRUPART
    endif
    if not(this.oPgFrm.Page4.oPag.oFLESGR_4_9.RadioValue()==this.w_FLESGR)
      this.oPgFrm.Page4.oPag.oFLESGR_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAnnotazioni_4_11.RadioValue()==this.w_Annotazioni)
      this.oPgFrm.Page4.oPag.oAnnotazioni_4_11.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDescagg_4_12.RadioValue()==this.w_Descagg)
      this.oPgFrm.Page4.oPag.oDescagg_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDETTIMP_4_13.RadioValue()==this.w_DETTIMP)
      this.oPgFrm.Page4.oPag.oDETTIMP_4_13.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDatiPratica_4_14.RadioValue()==this.w_DatiPratica)
      this.oPgFrm.Page4.oPag.oDatiPratica_4_14.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oELCONTR_4_15.RadioValue()==this.w_ELCONTR)
      this.oPgFrm.Page4.oPag.oELCONTR_4_15.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAccodaPreavvisi_4_16.RadioValue()==this.w_AccodaPreavvisi)
      this.oPgFrm.Page4.oPag.oAccodaPreavvisi_4_16.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAccodaNote_4_17.RadioValue()==this.w_AccodaNote)
      this.oPgFrm.Page4.oPag.oAccodaNote_4_17.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAccodaDaFare_4_18.RadioValue()==this.w_AccodaDaFare)
      this.oPgFrm.Page4.oPag.oAccodaDaFare_4_18.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oVISUDI_4_19.RadioValue()==this.w_VISUDI)
      this.oPgFrm.Page4.oPag.oVISUDI_4_19.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDENOM_PART_4_27.value==this.w_DENOM_PART)
      this.oPgFrm.Page4.oPag.oDENOM_PART_4_27.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINI_Cose_2_102.value==this.w_DATINI_Cose)
      this.oPgFrm.Page2.oPag.oDATINI_Cose_2_102.value=this.w_DATINI_Cose
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFIN_Cose_2_104.value==this.w_DATFIN_Cose)
      this.oPgFrm.Page2.oPag.oDATFIN_Cose_2_104.value=this.w_DATFIN_Cose
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIS_2_115.value==this.w_DESRIS)
      this.oPgFrm.Page2.oPag.oDESRIS_2_115.value=this.w_DESRIS
    endif
    if not(this.oPgFrm.Page4.oPag.oDATINIZ_4_33.value==this.w_DATINIZ)
      this.oPgFrm.Page4.oPag.oDATINIZ_4_33.value=this.w_DATINIZ
    endif
    if not(this.oPgFrm.Page4.oPag.oobsodat1_4_34.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page4.oPag.oobsodat1_4_34.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oSTADETTDOC_4_36.RadioValue()==this.w_STADETTDOC)
      this.oPgFrm.Page4.oPag.oSTADETTDOC_4_36.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(VAL(.w_OREINI) < 24)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(VAL(.w_OREFIN) < 24)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREFIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART))  and not(.w_DPTIPRIS <> 'P' )  and (.w_DPTIPRIS='P' AND Not Empty(.w_CODPART) )  and not(empty(.w_GRUPART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUPART_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
          case   not((EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS)  AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1='N') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S')))  and not(empty(.w_CODPART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPART_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and (.w_EDITCODNOM And Empty( .w_GIUDICE ))  and not(empty(.w_CODNOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODNOM_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1='N') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S'))  and not(NOT ISALT())  and not(empty(.w_CodRis))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCodRis_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!IsAlt() OR (.w_DATOBSONOM>i_datsys OR EMPTY(.w_DATOBSONOM) ) And CHKGIUDI( .w_GIUDICE ))  and not(not isalt())  and (Empty( .w_CODNOM ))  and not(empty(.w_GIUDICE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGIUDICE_2_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Nominativo inesistente o obsoleto non associato come giudice ad alcuna pratica")
          case   not(VAL(.w_ORA_PROM) < 24)  and (.w_ChkProm='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oORA_PROM_2_46.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MIN_PROM) < 60)  and (.w_ChkProm='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMIN_PROM_2_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_ELTIPCON='T' OR .w_MOTIPCON=.w_ELTIPCON)  and (.w_ELTIPCON<>'T' AND (!EMPTY(.w_CODCLI) OR EMPTY(.w_CODNOM)))  and not(empty(.w_ELCODMOD))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oELCODMOD_3_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice modello inesistente, incongruente o non di tipo a pacchetto")
          case   not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and (.w_EDITCODNOM And Empty( .w_GIUDICE ))  and not(empty(.w_CODNOM))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCODNOM_3_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS)  AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1='N') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S')))  and not(empty(.w_CODPART))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCODPART_4_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART))  and not(.w_DPTIPRIS <> 'P' )  and (.w_DPTIPRIS='P' AND Not Empty(.w_CODPART) )  and not(empty(.w_GRUPART))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oGRUPART_4_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
          case   not(.w_DATINI_Cose<=.w_DATFIN_Cose OR EMPTY(.w_DATFIN_Cose))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATFIN_Cose_2_104.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   (empty(.w_DATINIZ))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDATINIZ_4_33.SetFocus()
            i_bnoObbl = !empty(.w_DATINIZ)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_READAZI = this.w_READAZI
    this.o_FLTPART = this.w_FLTPART
    this.o_DATINI = this.w_DATINI
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_DATFIN = this.w_DATFIN
    this.o_OREFIN = this.w_OREFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_PATIPRIS = this.w_PATIPRIS
    this.o_CODPART = this.w_CODPART
    this.o_STATO = this.w_STATO
    this.o_CODNOM = this.w_CODNOM
    this.o_CodRis = this.w_CodRis
    this.o_CODCOM = this.w_CODCOM
    this.o_CODSED = this.w_CODSED
    this.o_COMPIMPKEY = this.w_COMPIMPKEY
    this.o_IMPIANTO = this.w_IMPIANTO
    this.o_COMPIMP = this.w_COMPIMP
    this.o_ChkProm = this.w_ChkProm
    this.o_DATA_PROM = this.w_DATA_PROM
    this.o_ORA_PROM = this.w_ORA_PROM
    this.o_MIN_PROM = this.w_MIN_PROM
    this.o_ATSERIAL = this.w_ATSERIAL
    this.o_FILTPROM = this.w_FILTPROM
    this.o_FiltroWeb = this.w_FiltroWeb
    this.o_ELCODIMP = this.w_ELCODIMP
    this.o_ELCODCOM = this.w_ELCODCOM
    this.o_ELTIPCON = this.w_ELTIPCON
    this.o_COMPIMPR = this.w_COMPIMPR
    this.o_DatiPratica = this.w_DatiPratica
    this.o_DATINI_Cose = this.w_DATINI_Cose
    this.o_DATFIN_Cose = this.w_DATFIN_Cose
    this.o_OREINI_Cose = this.w_OREINI_Cose
    this.o_MININI_Cose = this.w_MININI_Cose
    this.o_OREFIN_Cose = this.w_OREFIN_Cose
    this.o_MINFIN_Cose = this.w_MINFIN_Cose
    this.o_DATINIZ = this.w_DATINIZ
    this.o_obsodat1 = this.w_obsodat1
    return

enddefine

* --- Define pages as container
define class tgsag_kraPag1 as StdContainer
  Width  = 815
  height = 549
  stdWidth  = 815
  stdheight = 549
  resizeXpos=315
  resizeYpos=231
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_6 as StdField with uid="VAJTUNAFFS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 146514378,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=57, Top=14

  add object oOREINI_1_7 as StdField with uid="WOKDXXPUIP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OREINI", cQueryName = "OREINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di inizio selezione",;
    HelpContextID = 146571290,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=197, Top=14, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREINI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oOREINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_9 as StdField with uid="QMRKNBACQP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio selezione",;
    HelpContextID = 146536762,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=231, Top=14, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oMININI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc


  add object oObj_1_11 as cp_runprogram with uid="VSYPGKICOH",left=2, top=578, width=211,height=26,;
    caption='Azzera ore e min iniziali',;
   bGlobalFont=.t.,;
    prg="gsag_ba2('1')",;
    cEvent = "w_DATINI Changed",;
    nPag=1;
    , HelpContextID = 35304959

  add object oDATFIN_1_13 as StdField with uid="VRHGJGBHQK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 68067786,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=57, Top=39

  func oDATFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc


  add object oBtn_1_14 as StdButton with uid="OKDHYSKAYN",left=57, top=61, width=23,height=23,;
    CpPicture="bmp\frecciasn_small.bmp", caption="", nPag=1;
    , ToolTipText = "Decrementa di un giorno la data iniziale e quella finale";
    , HelpContextID = 116008714;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      this.parent.oContained.NotifyEvent("DecremGiorni")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DATINI) OR NOT EMPTY(.w_DATFIN))
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="RMCMOHTSVV",left=81, top=61, width=23,height=23,;
    CpPicture="bitmap.bmp", caption="", nPag=1;
    , ToolTipText = "Legenda";
    , HelpContextID = 73511606;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      do gsag_kle with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="KKCTLVFEOP",left=106, top=61, width=23,height=23,;
    CpPicture="bmp\frecciadx_small.bmp", caption="", nPag=1;
    , ToolTipText = "Incrementa di un giorno la data iniziale e quella finale";
    , HelpContextID = 116008714;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      this.parent.oContained.NotifyEvent("IncremGiorni")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DATINI) OR NOT EMPTY(.w_DATFIN))
      endwith
    endif
  endfunc

  add object oOREFIN_1_17 as StdField with uid="VKJCJSQZFK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_OREFIN", cQueryName = "OREFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di fine selezione",;
    HelpContextID = 68124698,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=197, Top=39, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREFIN_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oOREFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_19 as StdField with uid="GIWPXXGMLC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di fine selezione",;
    HelpContextID = 68090170,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=231, Top=39, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oMINFIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc


  add object oObj_1_21 as cp_runprogram with uid="GTGWZWHBZD",left=2, top=607, width=211,height=22,;
    caption='Azzera ore e min finali',;
   bGlobalFont=.t.,;
    prg="gsag_ba2('2')",;
    cEvent = "w_DATFIN Changed",;
    nPag=1;
    , HelpContextID = 208708605


  add object oPATIPRIS_1_23 as StdCombo with uid="FEFTOWFWCM",value=4,rtseq=13,rtrep=.f.,left=392,top=14,width=76,height=21;
    , HelpContextID = 6577993;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPATIPRIS_1_23.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    iif(this.value =3,'R',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oPATIPRIS_1_23.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_1_23.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',3,;
      iif(this.Parent.oContained.w_PATIPRIS=='',4,;
      0))))
  endfunc

  add object oFLESGR_1_24 as StdCheck with uid="AYKWNFTPEF",rtseq=14,rtrep=.f.,left=633, top=41, caption="Esplodi gruppo",;
    ToolTipText = "Se attivo visualizza anche appuntamenti delle persone del gruppo",;
    HelpContextID = 2262698,;
    cFormVar="w_FLESGR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLESGR_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLESGR_1_24.GetRadio()
    this.Parent.oContained.w_FLESGR = this.RadioValue()
    return .t.
  endfunc

  func oFLESGR_1_24.SetRadio()
    this.Parent.oContained.w_FLESGR=trim(this.Parent.oContained.w_FLESGR)
    this.value = ;
      iif(this.Parent.oContained.w_FLESGR=='S',1,;
      0)
  endfunc

  func oFLESGR_1_24.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'G' or Empty(.w_CODPART))
    endwith
  endfunc

  add object oGRUPART_1_25 as StdField with uid="AYUXRJLLLH",rtseq=15,rtrep=.f.,;
    cFormVar = "w_GRUPART", cQueryName = "GRUPART",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo inesistente o non associato al partecipante",;
    HelpContextID = 259751782,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=689, Top=40, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_GRUPART"

  func oGRUPART_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPTIPRIS='P' AND Not Empty(.w_CODPART) )
    endwith
   endif
  endfunc

  func oGRUPART_1_25.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  func oGRUPART_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPART_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPART_1_25.mZoom
      with this.Parent.oContained
        GSAR1BGP(this.Parent.oContained,  "", .w_CODPART, "", "GRUPART" )
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCODPART_1_26 as StdField with uid="QJYWRUDDRU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODPART", cQueryName = "CODPART",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice partecipante",;
    HelpContextID = 259681318,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=477, Top=14, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_CODPART"

  func oCODPART_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPART_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPART_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oCODPART_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oCODPRA_1_28 as StdField with uid="LNZCMMZIKS",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice pratica",;
    HelpContextID = 7705562,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=178, Top=66, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="gsar_bzz", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITCODPRA AND IsAlt())
    endwith
   endif
  endfunc

  func oCODPRA_1_28.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  func oCODPRA_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRA_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRA_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRA_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_bzz',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODPRA_1_28.mZoomOnZoom
    local i_obj
    i_obj=gsar_bzz()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRA
     i_obj.ecpSave()
  endproc


  add object oPRIORITA_1_29 as StdCombo with uid="TPBXXBSCEW",value=4,rtseq=18,rtrep=.f.,left=633,top=66,width=118,height=21;
    , ToolTipText = "Priorit�";
    , HelpContextID = 126468151;
    , cFormVar="w_PRIORITA",RowSource=""+"Normale,"+"Urgente,"+"Scadenza termine,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRIORITA_1_29.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,3,;
    iif(this.value =3,4,;
    iif(this.value =4,0,;
    0)))))
  endfunc
  func oPRIORITA_1_29.GetRadio()
    this.Parent.oContained.w_PRIORITA = this.RadioValue()
    return .t.
  endfunc

  func oPRIORITA_1_29.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PRIORITA==1,1,;
      iif(this.Parent.oContained.w_PRIORITA==3,2,;
      iif(this.Parent.oContained.w_PRIORITA==4,3,;
      iif(this.Parent.oContained.w_PRIORITA==0,4,;
      0))))
  endfunc


  add object oSTATO_1_30 as StdCombo with uid="HPCXRCGRJY",rtseq=19,rtrep=.f.,left=392,top=40,width=124,height=21;
    , ToolTipText = "Stato";
    , HelpContextID = 27377114;
    , cFormVar="w_STATO",RowSource=""+"Non evasa,"+"Da svolgere,"+"In corso,"+"Provvisoria,"+"Evasa,"+"Completata,"+"Evasa o completata,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_30.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'T',;
    iif(this.value =5,'F',;
    iif(this.value =6,'P',;
    iif(this.value =7,'B',;
    iif(this.value =8,'K',;
    space(1))))))))))
  endfunc
  func oSTATO_1_30.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_30.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='N',1,;
      iif(this.Parent.oContained.w_STATO=='D',2,;
      iif(this.Parent.oContained.w_STATO=='I',3,;
      iif(this.Parent.oContained.w_STATO=='T',4,;
      iif(this.Parent.oContained.w_STATO=='F',5,;
      iif(this.Parent.oContained.w_STATO=='P',6,;
      iif(this.Parent.oContained.w_STATO=='B',7,;
      iif(this.Parent.oContained.w_STATO=='K',8,;
      0))))))))
  endfunc

  func oSTATO_1_30.mHide()
    with this.Parent.oContained
      return (.w_RISERVE='S')
    endwith
  endfunc


  add object oRISERVE_1_31 as StdCombo with uid="MIFARZNGXZ",rtseq=20,rtrep=.f.,left=521,top=40,width=106,height=21;
    , ToolTipText = "Considera o meno tutte le attivit� in riserva a prescindere dai filtri sullo stato e sulle date";
    , HelpContextID = 75519766;
    , cFormVar="w_RISERVE",RowSource=""+"Escludi riserve,"+"Aggiungi riserve,"+"No filtro riserve,"+"Solo riserve", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRISERVE_1_31.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'A',;
    iif(this.value =3,'L',;
    iif(this.value =4,'S',;
    space(1))))))
  endfunc
  func oRISERVE_1_31.GetRadio()
    this.Parent.oContained.w_RISERVE = this.RadioValue()
    return .t.
  endfunc

  func oRISERVE_1_31.SetRadio()
    this.Parent.oContained.w_RISERVE=trim(this.Parent.oContained.w_RISERVE)
    this.value = ;
      iif(this.Parent.oContained.w_RISERVE=='E',1,;
      iif(this.Parent.oContained.w_RISERVE=='A',2,;
      iif(this.Parent.oContained.w_RISERVE=='L',3,;
      iif(this.Parent.oContained.w_RISERVE=='S',4,;
      0))))
  endfunc

  func oRISERVE_1_31.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oBtn_1_41 as StdButton with uid="PHEWMSSLEL",left=757, top=39, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 60913430;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_42 as cp_runprogram with uid="UAIOGLNCRD",left=216, top=578, width=248,height=26,;
    caption='Doppio click',;
   bGlobalFont=.t.,;
    prg="GSAG_BRA('V', 'A')",;
    cEvent = "w_AGKRA_ZOOM selected",;
    nPag=1;
    , HelpContextID = 7410599


  add object oObj_1_43 as cp_runprogram with uid="EKYLVYFMOI",left=216, top=607, width=248,height=22,;
    caption='Doppio click',;
   bGlobalFont=.t.,;
    prg="GSAG_BRA('V', 'M')",;
    cEvent = "w_AGKRA_ZOOM3 selected",;
    nPag=1;
    , HelpContextID = 7410599

  add object oDENOM_PART_1_49 as StdField with uid="SKXCKTCAOH",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(48), bMultilanguage =  .f.,;
    HelpContextID = 46507113,;
   bGlobalFont=.t.,;
    Height=21, Width=253, Left=552, Top=14, InputMask=replicate('X',48)


  add object AGKRA_ZOOM3 as cp_szoombox with uid="WOBHMJMULS",left=478, top=89, width=329,height=412,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_ATTI",cZoomFile="GSAG_KRM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSAG_AAT",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,DisableSelMenu=.f.,bNoZoomGridShape=.f.,;
    cEvent = "Ricerca,ImpostaDataOdierna,CodpartChange1,DecremGiorni,IncremGiorni",;
    nPag=1;
    , HelpContextID = 61988838


  add object AGKRA_ZOOM4 as cp_zoombox with uid="CEDMUSEBWJ",left=-2, top=381, width=489,height=120,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_PART",cZoomFile="GSAG_KPP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,;
    nPag=1;
    , HelpContextID = 61988838


  add object oObj_1_58 as cp_runprogram with uid="JEUBWNEVWF",left=474, top=649, width=198,height=24,;
    caption='Esegue zoom',;
   bGlobalFont=.t.,;
    prg="GSAG_BA5",;
    cEvent = "w_AGKRA_ZOOM3 after query",;
    nPag=1;
    , HelpContextID = 170203472

  add object oDESPRA_1_60 as StdField with uid="DHZPCSQKFH",rtseq=74,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(75), bMultilanguage =  .f.,;
    HelpContextID = 7646666,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=305, Top=66, InputMask=replicate('X',75)

  func oDESPRA_1_60.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oBtn_1_62 as StdButton with uid="IROIOQAOBH",left=474, top=503, width=48,height=45,;
    CpPicture="bmp\refresh.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per annullare i filtri di selezione";
    , HelpContextID = 206711558;
    , caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      with this.Parent.oContained
        .Blankrec()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_62.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!ISALT())
     endwith
    endif
  endfunc


  add object oBtn_1_63 as StdButton with uid="WMTZQHGEZY",left=522, top=503, width=48,height=45,;
    CpPicture="disptemp.ico", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare attivit� in scadenza termine";
    , HelpContextID = 31909908;
    , caption='Sc. \<Term';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_63.Click()
      with this.Parent.oContained
        gszm_bat(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_63.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_64 as StdButton with uid="PNFAKLMYVG",left=570, top=503, width=48,height=45,;
    CpPicture="esplodi.ico", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare attivit� scadute";
    , HelpContextID = 80761126;
    , caption='\<Scadute';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_64.Click()
      with this.Parent.oContained
        gszm_bat(this.Parent.oContained,"O")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_64.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_66 as StdButton with uid="MZNLQBMNEX",left=618, top=503, width=48,height=45,;
    CpPicture="bmp\Genera.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'elaborazione su tutte le attivit� selezionate";
    , HelpContextID = 40786246;
    , caption='\<Elabora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_66.Click()
      with this.Parent.oContained
        do GSAG_BGM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_67 as StdButton with uid="IHUCOXTUOW",left=666, top=503, width=48,height=45,;
    CpPicture="bmp\schedulatore.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per caricare attivit� collegate";
    , HelpContextID = 114889450;
    , caption='\<Collegate';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_67.Click()
      do GSAG_KIT with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_69 as StdButton with uid="UUNSDLSMLD",left=714, top=503, width=48,height=45,;
    CpPicture="bmp\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Nuova attivit�";
    , HelpContextID = 259833130;
    , caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_69.Click()
      with this.Parent.oContained
        GSAG_BRA(this.Parent.oContained,"N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_71 as StdButton with uid="TKCMXZXILU",left=762, top=503, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 108691386;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_71.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_73 as StdButton with uid="BQJHHOQZPU",left=262, top=14, width=48,height=45,;
    CpPicture="bmp\date-time.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per impostare la data odierna";
    , HelpContextID = 108677914;
    , caption='\<Oggi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_73.Click()
      this.parent.oContained.NotifyEvent("ImpostaDataOdierna")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AGKRA_ZOOM as cp_szoombox with uid="MPOPQLEMXY",left=-2, top=89, width=489,height=281,;
    caption='AGKRA_ZOOM',;
   bGlobalFont=.t.,;
    cTable="OFF_ATTI",cZoomFile="GSAG_KRA",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSAG_AAT",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,DisableSelMenu=.f.,bNoZoomGridShape=.f.,;
    cEvent = "Ricerca,ImpostaDataOdierna,CodpartChange2,DecremGiorni,IncremGiorni",;
    nPag=1;
    , HelpContextID = 58907067


  add object oObj_1_78 as cp_runprogram with uid="LGMRHRTWUD",left=473, top=625, width=198,height=24,;
    caption='Attiva font in Attivit�',;
   bGlobalFont=.t.,;
    prg="GSAG_BIF",;
    cEvent = "Init,Ricerca,ImpostaDataOdierna,DecremGiorni,IncremGiorni",;
    nPag=1;
    , HelpContextID = 240430627

  add object oPratica_Attivita_1_86 as StdMemo with uid="YKJRFMCTDJ",rtseq=107,rtrep=.f.,;
    cFormVar = "w_Pratica_Attivita", cQueryName = "Pratica_Attivita",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 202303290,;
    FontName = "MS Sans Serif", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=45, Width=469, Left=2, Top=503, ReadOnly=.T.

  add object oStr_1_5 as StdString with uid="KLEZMLKSOB",Visible=.t., Left=4, Top=15,;
    Alignment=1, Width=50, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="JAHWAWKLCQ",Visible=.t., Left=4, Top=40,;
    Alignment=1, Width=50, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="JGUELPQNWG",Visible=.t., Left=337, Top=43,;
    Alignment=1, Width=52, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_RISERVE='S')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="UQSAZJMDFM",Visible=.t., Left=588, Top=67,;
    Alignment=1, Width=42, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="EWZSPAOTVW",Visible=.t., Left=134, Top=15,;
    Alignment=1, Width=59, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="CTAHWBKCXZ",Visible=.t., Left=134, Top=39,;
    Alignment=1, Width=59, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="DAAABZBARA",Visible=.t., Left=226, Top=15,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="BTYOAEACQT",Visible=.t., Left=226, Top=40,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="ZGENJNNRPH",Visible=.t., Left=312, Top=15,;
    Alignment=1, Width=77, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="IDFPFEVCUX",Visible=.t., Left=20, Top=373,;
    Alignment=0, Width=114, Height=14,;
    Caption="Partecipanti attivit�"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.w_LAYOUT)
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="HZALIZNENK",Visible=.t., Left=131, Top=67,;
    Alignment=1, Width=45, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="DEYNVFGTOR",Visible=.t., Left=143, Top=753,;
    Alignment=0, Width=956, Height=18,;
    Caption="Attenzione! ImpostaDataOdierna, DecremGiorni e IncremGiorni devono avere sequenza minore degli zoom per fare s� che i valori che questi ricevono per ricercare siano corretti"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_82 as StdString with uid="VRCOAPPYPP",Visible=.t., Left=639, Top=43,;
    Alignment=1, Width=48, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_1_82.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  add object oBox_1_74 as StdBox with uid="LOIZABPOHX",left=475, top=12, width=68,height=27
enddefine
define class tgsag_kraPag2 as StdContainer
  Width  = 815
  height = 549
  stdWidth  = 815
  stdheight = 549
  resizeXpos=468
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODICE_2_3 as StdField with uid="MBGICDMGLD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CACODICE", cQueryName = "CACODICE",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 111758955,;
   bGlobalFont=.t.,;
    Height=21, Width=171, Left=102, Top=11, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CACODICE"

  func oCACODICE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODICE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODICE_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oCACODICE_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc

  add object oCADESCRI_2_4 as AH_SEARCHFLD with uid="AVSKMMIFVK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CADESCRI", cQueryName = "CADESCRI",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione oggetto o parte di essa",;
    HelpContextID = 242262417,;
   bGlobalFont=.t.,;
    Height=21, Width=375, Left=376, Top=12, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oCADESCRI_2_4.mZoom
    vx_exec("query\GSAG1KRA.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oBtn_2_5 as StdButton with uid="TMRCZGTRFE",left=760, top=39, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=2;
    , ToolTipText = "Ricerca";
    , HelpContextID = 60913430;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_5.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODNOM_2_6 as StdField with uid="LDORSNJAGM",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo o soggetto esterno della pratica",;
    HelpContextID = 78091226,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=102, Top=38, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITCODNOM And Empty( .w_GIUDICE ))
    endwith
   endif
  endfunc

  func oCODNOM_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc


  add object oCODCAT_2_7 as StdTableCombo with uid="AXUHUESMBU",rtseq=28,rtrep=.f.,left=617,top=38,width=134,height=21;
    , ToolTipText = "Ruolo del nominativo (soggetti esterni)";
    , HelpContextID = 244487130;
    , cFormVar="w_CODCAT",tablefilter="", bObbl = .f. , nPag = 2;
    , sErrorMsg = "Codice categoria soggetti inesistente o di tipo cliente";
    , cLinkFile="CAT_SOGG";
    , cTable='CAT_SOGG',cKey='CSCODCAT',cValue='CSDESCAT',cOrderBy='CSDESCAT',xDefault=space(10);
  , bGlobalFont=.t.


  func oCODCAT_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODNOM))
    endwith
   endif
  endfunc

  func oCODCAT_2_7.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oCODCAT_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oNUMDOC_2_10 as StdField with uid="ALSVTWVYIC",rtseq=31,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'attivit�",;
    HelpContextID = 246480170,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=102, Top=65, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMDOC_2_10.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oALFDOC_2_11 as StdField with uid="TNHYATACTB",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie dell'attivit�",;
    HelpContextID = 246511354,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=228, Top=65, InputMask=replicate('X',10)

  func oALFDOC_2_11.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oCodRis_2_12 as StdField with uid="DOBBSYFCFJ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CodRis", cQueryName = "CodRis",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Soggetto interno",;
    HelpContextID = 50236454,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=102, Top=65, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_CodRis"

  func oCodRis_2_12.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oCodRis_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodRis_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodRis_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCodRis_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse interne",'GSARPADP2.DIPENDEN_VZM',this.parent.oContained
  endproc


  add object oRuolRis_2_13 as StdTableCombo with uid="HKRTBWAVCA",rtseq=34,rtrep=.f.,left=617,top=66,width=134,height=21;
    , ToolTipText = "Ruolo risorsa interna";
    , HelpContextID = 128533270;
    , cFormVar="w_RuolRis",tablefilter="", bObbl = .f. , nPag = 2;
    , cLinkFile="RUO_SOGI";
    , cTable='RUO_SOGI',cKey='RSCODRUO',cValue='RSDESRUO',cOrderBy='RSDESRUO',xDefault=space(5);
  , bGlobalFont=.t.


  func oRuolRis_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CodRis))
    endwith
   endif
  endfunc

  func oRuolRis_2_13.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oRuolRis_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oRuolRis_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oATTSTU_2_14 as StdCheck with uid="FUHPBAIQSC",rtseq=35,rtrep=.f.,left=102, top=91, caption="Appuntamenti",;
    ToolTipText = "Se attivo: visualizza gli appuntamenti",;
    HelpContextID = 206671610,;
    cFormVar="w_ATTSTU", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATTSTU_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oATTSTU_2_14.GetRadio()
    this.Parent.oContained.w_ATTSTU = this.RadioValue()
    return .t.
  endfunc

  func oATTSTU_2_14.SetRadio()
    this.Parent.oContained.w_ATTSTU=trim(this.Parent.oContained.w_ATTSTU)
    this.value = ;
      iif(this.Parent.oContained.w_ATTSTU=='S',1,;
      0)
  endfunc

  add object oATTGENER_2_15 as StdCheck with uid="KLBNRLNCJC",rtseq=36,rtrep=.f.,left=102, top=112, caption="Attivit� generiche",;
    ToolTipText = "Se attivo: visualizza le attivit� generiche",;
    HelpContextID = 196243800,;
    cFormVar="w_ATTGENER", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATTGENER_2_15.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oATTGENER_2_15.GetRadio()
    this.Parent.oContained.w_ATTGENER = this.RadioValue()
    return .t.
  endfunc

  func oATTGENER_2_15.SetRadio()
    this.Parent.oContained.w_ATTGENER=trim(this.Parent.oContained.w_ATTGENER)
    this.value = ;
      iif(this.Parent.oContained.w_ATTGENER=='G',1,;
      0)
  endfunc

  add object oUDIENZE_2_16 as StdCheck with uid="HBQKVBXHMX",rtseq=37,rtrep=.f.,left=102, top=133, caption="Udienze",;
    ToolTipText = "Se attivo: visualizza le udienze",;
    HelpContextID = 138392134,;
    cFormVar="w_UDIENZE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oUDIENZE_2_16.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oUDIENZE_2_16.GetRadio()
    this.Parent.oContained.w_UDIENZE = this.RadioValue()
    return .t.
  endfunc

  func oUDIENZE_2_16.SetRadio()
    this.Parent.oContained.w_UDIENZE=trim(this.Parent.oContained.w_UDIENZE)
    this.value = ;
      iif(this.Parent.oContained.w_UDIENZE=='U',1,;
      0)
  endfunc

  func oUDIENZE_2_16.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oSESSTEL_2_18 as StdCheck with uid="KYMNVXZYIW",rtseq=39,rtrep=.f.,left=280, top=91, caption="Sessioni telefoniche",;
    ToolTipText = "Se attivo: visualizza le sessioni telefoniche",;
    HelpContextID = 206679258,;
    cFormVar="w_SESSTEL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSESSTEL_2_18.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oSESSTEL_2_18.GetRadio()
    this.Parent.oContained.w_SESSTEL = this.RadioValue()
    return .t.
  endfunc

  func oSESSTEL_2_18.SetRadio()
    this.Parent.oContained.w_SESSTEL=trim(this.Parent.oContained.w_SESSTEL)
    this.value = ;
      iif(this.Parent.oContained.w_SESSTEL=='T',1,;
      0)
  endfunc

  add object oASSENZE_2_19 as StdCheck with uid="RPXCBGRCCL",rtseq=40,rtrep=.f.,left=280, top=112, caption="Assenze",;
    ToolTipText = "Se attivo: visualizza le assenze",;
    HelpContextID = 138436614,;
    cFormVar="w_ASSENZE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oASSENZE_2_19.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oASSENZE_2_19.GetRadio()
    this.Parent.oContained.w_ASSENZE = this.RadioValue()
    return .t.
  endfunc

  func oASSENZE_2_19.SetRadio()
    this.Parent.oContained.w_ASSENZE=trim(this.Parent.oContained.w_ASSENZE)
    this.value = ;
      iif(this.Parent.oContained.w_ASSENZE=='A',1,;
      0)
  endfunc

  add object oDAINSPRE_2_20 as StdCheck with uid="AMMYJSOHVW",rtseq=41,rtrep=.f.,left=280, top=133, caption="Da inserimento prestazioni",;
    ToolTipText = "Se attivo: visualizza le attivit� generate dall'inserimento delle prestazioni",;
    HelpContextID = 23548293,;
    cFormVar="w_DAINSPRE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDAINSPRE_2_20.RadioValue()
    return(iif(this.value =1,'Z',;
    ' '))
  endfunc
  func oDAINSPRE_2_20.GetRadio()
    this.Parent.oContained.w_DAINSPRE = this.RadioValue()
    return .t.
  endfunc

  func oDAINSPRE_2_20.SetRadio()
    this.Parent.oContained.w_DAINSPRE=trim(this.Parent.oContained.w_DAINSPRE)
    this.value = ;
      iif(this.Parent.oContained.w_DAINSPRE=='Z',1,;
      0)
  endfunc

  add object oMEMO_2_21 as StdCheck with uid="ELRSHFYRWO",rtseq=42,rtrep=.f.,left=459, top=91, caption="Note",;
    ToolTipText = "Se attivo: visualizza le note",;
    HelpContextID = 110497082,;
    cFormVar="w_MEMO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMEMO_2_21.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oMEMO_2_21.GetRadio()
    this.Parent.oContained.w_MEMO = this.RadioValue()
    return .t.
  endfunc

  func oMEMO_2_21.SetRadio()
    this.Parent.oContained.w_MEMO=trim(this.Parent.oContained.w_MEMO)
    this.value = ;
      iif(this.Parent.oContained.w_MEMO=='M',1,;
      0)
  endfunc

  proc oMEMO_2_21.mAfter
    with this.Parent.oContained
      .w_AccodaNote='N'
    endwith
  endproc

  add object oDAFARE_2_22 as StdCheck with uid="JYTLCSHGGN",rtseq=43,rtrep=.f.,left=459, top=112, caption="Cose da fare",;
    ToolTipText = "Se attivo: visualizza le cose da fare",;
    HelpContextID = 210010570,;
    cFormVar="w_DAFARE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDAFARE_2_22.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oDAFARE_2_22.GetRadio()
    this.Parent.oContained.w_DAFARE = this.RadioValue()
    return .t.
  endfunc

  func oDAFARE_2_22.SetRadio()
    this.Parent.oContained.w_DAFARE=trim(this.Parent.oContained.w_DAFARE)
    this.value = ;
      iif(this.Parent.oContained.w_DAFARE=='D',1,;
      0)
  endfunc

  proc oDAFARE_2_22.mAfter
    with this.Parent.oContained
      .w_AccodaDaFare='N'
    endwith
  endproc


  add object oDISPONIB_2_23 as StdCombo with uid="MRAFDOVVBK",value=5,rtseq=44,rtrep=.f.,left=102,top=159,width=118,height=21;
    , ToolTipText = "Disponibilit�";
    , HelpContextID = 207312504;
    , cFormVar="w_DISPONIB",RowSource=""+"Occupato,"+"Per urgenze,"+"Libero,"+"Fuori sede,"+"Tutte", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDISPONIB_2_23.RadioValue()
    return(iif(this.value =1,2,;
    iif(this.value =2,3,;
    iif(this.value =3,1,;
    iif(this.value =4,4,;
    iif(this.value =5,0,;
    0))))))
  endfunc
  func oDISPONIB_2_23.GetRadio()
    this.Parent.oContained.w_DISPONIB = this.RadioValue()
    return .t.
  endfunc

  func oDISPONIB_2_23.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DISPONIB==2,1,;
      iif(this.Parent.oContained.w_DISPONIB==3,2,;
      iif(this.Parent.oContained.w_DISPONIB==1,3,;
      iif(this.Parent.oContained.w_DISPONIB==4,4,;
      iif(this.Parent.oContained.w_DISPONIB==0,5,;
      0)))))
  endfunc

  add object oFiltPerson_2_24 as AH_SEARCHFLD with uid="MYRJGKWLFV",rtseq=45,rtrep=.f.,;
    cFormVar = "w_FiltPerson", cQueryName = "FiltPerson",;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Persona o parte di essa",;
    HelpContextID = 59866041,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=345, Top=159, InputMask=replicate('X',60)

  add object oCODTIPOL_2_25 as StdField with uid="HQBAYRUABD",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CODTIPOL", cQueryName = "CODTIPOL",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia",;
    HelpContextID = 33657742,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=102, Top=187, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="OFFTIPAT", oKey_1_1="TACODTIP", oKey_1_2="this.w_CODTIPOL"

  func oCODTIPOL_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODTIPOL_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODTIPOL_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFFTIPAT','*','TACODTIP',cp_AbsName(this.parent,'oCODTIPOL_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipologie attivit�",'',this.parent.oContained
  endproc

  add object oNOTE_2_26 as AH_SEARCHMEMO with uid="YFIDICHIXZ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Annotazioni o parte di esse",;
    HelpContextID = 111121194,;
   bGlobalFont=.t.,;
    Height=21, Width=676, Left=102, Top=215

  add object oGIUDICE_2_27 as StdField with uid="FJVJFDFGGP",rtseq=48,rtrep=.f.,;
    cFormVar = "w_GIUDICE", cQueryName = "GIUDICE",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Nominativo inesistente o obsoleto non associato come giudice ad alcuna pratica",;
    ToolTipText = "Se specificato filtra le attivit� associate a pratiche con data fine compresa all'interno dell'intervallo di validiit� del giudice",;
    HelpContextID = 15693414,;
   bGlobalFont=.t.,;
    Height=21, Width=138, Left=102, Top=243, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_GIUDICE"

  func oGIUDICE_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_CODNOM ))
    endwith
   endif
  endfunc

  func oGIUDICE_2_27.mHide()
    with this.Parent.oContained
      return (not isalt())
    endwith
  endfunc

  func oGIUDICE_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oGIUDICE_2_27.ecpDrop(oSource)
    this.Parent.oContained.link_2_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGIUDICE_2_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oGIUDICE_2_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Giudici",'GSPR_ZGI.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oGIUDICE_2_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_GIUDICE
     i_obj.ecpSave()
  endproc

  add object oATLOCALI_2_28 as AH_SEARCHFLD with uid="KNSKLUNXGJ",rtseq=49,rtrep=.f.,;
    cFormVar = "w_ATLOCALI", cQueryName = "ATLOCALI",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� o parte di essa",;
    HelpContextID = 23465649,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=102, Top=271, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oATLOCALI_2_28.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_ATLOCALI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oENTE_2_29 as StdField with uid="NHZLOLJQPN",rtseq=50,rtrep=.f.,;
    cFormVar = "w_ENTE", cQueryName = "ENTE",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente/ufficio giudiziario",;
    HelpContextID = 111121594,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=102, Top=299, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRA_ENTI", oKey_1_1="EPCODICE", oKey_1_2="this.w_ENTE"

  func oENTE_2_29.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  func oENTE_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oENTE_2_29.ecpDrop(oSource)
    this.Parent.oContained.link_2_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oENTE_2_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRA_ENTI','*','EPCODICE',cp_AbsName(this.parent,'oENTE_2_29'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Enti pratica",'',this.parent.oContained
  endproc

  add object oCODCOM_2_30 as StdField with uid="KRSMLUAVRO",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 78812122,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=102, Top=299, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="gsar_bzz", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_2_30.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCODCOM_2_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM_2_30.ecpDrop(oSource)
    this.Parent.oContained.link_2_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_2_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_2_30'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_bzz',"Commessa",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODCOM_2_30.mZoomOnZoom
    local i_obj
    i_obj=gsar_bzz()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODCOM
     i_obj.ecpSave()
  endproc


  add object oTIPANA_2_31 as StdCombo with uid="OOYOOVQSKI",rtseq=52,rtrep=.f.,left=102,top=327,width=146,height=21;
    , ToolTipText = "Natura ";
    , HelpContextID = 12835018;
    , cFormVar="w_TIPANA",RowSource=""+"Costo,"+"Ricavo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPANA_2_31.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPANA_2_31.GetRadio()
    this.Parent.oContained.w_TIPANA = this.RadioValue()
    return .t.
  endfunc

  func oTIPANA_2_31.SetRadio()
    this.Parent.oContained.w_TIPANA=trim(this.Parent.oContained.w_TIPANA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPANA=='C',1,;
      iif(this.Parent.oContained.w_TIPANA=='R',2,;
      0))
  endfunc

  func oTIPANA_2_31.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCENCOS_2_32 as StdField with uid="LTYDLVFPON",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CENCOS", cQueryName = "CENCOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo",;
    HelpContextID = 246545882,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=102, Top=355, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENCOS"

  func oCENCOS_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oCENCOS_2_32.ecpDrop(oSource)
    this.Parent.oContained.link_2_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCENCOS_2_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCENCOS_2_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCENCOS_2_32.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CENCOS
     i_obj.ecpSave()
  endproc

  add object oDesEnte_2_34 as StdField with uid="LTVJZFGKNZ",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DesEnte", cQueryName = "DesEnte",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 71463478,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=212, Top=299, InputMask=replicate('X',60)

  func oDesEnte_2_34.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oCODSED_2_35 as StdField with uid="DGJQCXNDEJ",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CODSED", cQueryName = "CODSED",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Sede dell'intestatario",;
    HelpContextID = 239244250,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=102, Top=383, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", cZoomOnZoom="GSAR_MDD", oKey_1_1="DDTIPCON", oKey_1_2="this.w_TIPNOM", oKey_2_1="DDCODICE", oKey_2_2="this.w_CODCLI", oKey_3_1="DDCODDES", oKey_3_2="this.w_CODSED"

  func oCODSED_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPNOM='C')
    endwith
   endif
  endfunc

  func oCODSED_2_35.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  func oCODSED_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSED_2_35.ecpDrop(oSource)
    this.Parent.oContained.link_2_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSED_2_35.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPNOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPNOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CODCLI)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCODSED_2_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MDD',"Sedi",'',this.parent.oContained
  endproc
  proc oCODSED_2_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MDD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DDTIPCON=w_TIPNOM
    i_obj.DDCODICE=w_CODCLI
     i_obj.w_DDCODDES=this.parent.oContained.w_CODSED
     i_obj.ecpSave()
  endproc

  add object oUFFICIO_2_37 as StdField with uid="NQCZTZGKLZ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_UFFICIO", cQueryName = "UFFICIO",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice ufficio/sezione",;
    HelpContextID = 158104506,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=102, Top=411, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRA_UFFI", oKey_1_1="UFCODICE", oKey_1_2="this.w_UFFICIO"

  func oUFFICIO_2_37.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  func oUFFICIO_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oUFFICIO_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUFFICIO_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRA_UFFI','*','UFCODICE',cp_AbsName(this.parent,'oUFFICIO_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Uffici",'',this.parent.oContained
  endproc

  add object oDesUffi_2_38 as StdField with uid="PFQCMQCJNZ",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DesUffi", cQueryName = "DesUffi",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 170757578,;
   bGlobalFont=.t.,;
    Height=21, Width=574, Left=223, Top=411, InputMask=replicate('X',60)

  func oDesUffi_2_38.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oCOMPIMPKEY_2_40 as StdField with uid="SBSCAGJTGI",rtseq=60,rtrep=.f.,;
    cFormVar = "w_COMPIMPKEY", cQueryName = "COMPIMPKEY",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 84190783,;
   bGlobalFont=.t.,;
    Height=22, Width=94, Left=612, Top=271

  func oCOMPIMPKEY_2_40.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oIMPIANTO_2_41 as StdField with uid="WYZQFLHLBI",rtseq=61,rtrep=.f.,;
    cFormVar = "w_IMPIANTO", cQueryName = "IMPIANTO",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice impianto",;
    HelpContextID = 192162517,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=102, Top=411, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", cZoomOnZoom="GSAG_MIM", oKey_1_1="IMCODICE", oKey_1_2="this.w_IMPIANTO"

  func oIMPIANTO_2_41.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  func oIMPIANTO_2_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_41('Part',this)
      if .not. empty(.w_COMPIMPKEYREAD)
        bRes2=.link_2_39('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIMPIANTO_2_41.ecpDrop(oSource)
    this.Parent.oContained.link_2_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMPIANTO_2_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oIMPIANTO_2_41'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIM',"Impianto",'GSAG_AAT.IMP_MAST_VZM',this.parent.oContained
  endproc
  proc oIMPIANTO_2_41.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_IMPIANTO
     i_obj.ecpSave()
  endproc

  add object oCOMPIMP_2_42 as StdField with uid="GOVKVGTSZB",rtseq=62,rtrep=.f.,;
    cFormVar = "w_COMPIMP", cQueryName = "COMPIMP",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Componente impianto",;
    HelpContextID = 84214746,;
   bGlobalFont=.t.,;
    Height=21, Width=486, Left=311, Top=411, InputMask=replicate('X',50), bHasZoom = .t. 

  func oCOMPIMP_2_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_IMPIANTO))
    endwith
   endif
  endfunc

  func oCOMPIMP_2_42.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  proc oCOMPIMP_2_42.mBefore
    with this.Parent.oContained
      .w_OLDCOMP=.w_COMPIMP
    endwith
  endproc

  proc oCOMPIMP_2_42.mAfter
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M")
      endwith
  endproc

  proc oCOMPIMP_2_42.mZoom
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M",.T.)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCodUte_2_43 as StdField with uid="PIUVUYSBHD",rtseq=63,rtrep=.f.,;
    cFormVar = "w_CodUte", cQueryName = "CodUte",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente ultima modifica",;
    HelpContextID = 172913626,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=102, Top=439, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CodUte"

  func oCodUte_2_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodUte_2_43.ecpDrop(oSource)
    this.Parent.oContained.link_2_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodUte_2_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCodUte_2_43'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oChkProm_2_44 as StdCheck with uid="XNJEQKNWQH",rtseq=64,rtrep=.f.,left=102, top=467, caption="Filtra le attivit� con promemoria inferiore a",;
    ToolTipText = "Se attivo: seleziona le attivit� con promemoria inferiore alla data selezionata",;
    HelpContextID = 7539418,;
    cFormVar="w_ChkProm", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oChkProm_2_44.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oChkProm_2_44.GetRadio()
    this.Parent.oContained.w_ChkProm = this.RadioValue()
    return .t.
  endfunc

  func oChkProm_2_44.SetRadio()
    this.Parent.oContained.w_ChkProm=trim(this.Parent.oContained.w_ChkProm)
    this.value = ;
      iif(this.Parent.oContained.w_ChkProm=='S',1,;
      0)
  endfunc

  add object oDATA_PROM_2_45 as StdField with uid="SWIUVBMUDH",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DATA_PROM", cQueryName = "DATA_PROM",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 11771051,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=387, Top=467

  func oDATA_PROM_2_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ChkProm='S')
    endwith
   endif
  endfunc

  add object oORA_PROM_2_46 as StdField with uid="QRHLNNKGBQ",rtseq=66,rtrep=.f.,;
    cFormVar = "w_ORA_PROM", cQueryName = "ORA_PROM",nZero=2,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora promemoria",;
    HelpContextID = 260489165,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=525, Top=467, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORA_PROM_2_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ChkProm='S')
    endwith
   endif
  endfunc

  func oORA_PROM_2_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORA_PROM) < 24)
    endwith
    return bRes
  endfunc

  add object oMIN_PROM_2_47 as StdField with uid="BUHARTDGYH",rtseq=67,rtrep=.f.,;
    cFormVar = "w_MIN_PROM", cQueryName = "MIN_PROM",nZero=2,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti promemoria",;
    HelpContextID = 260438253,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=559, Top=467, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMIN_PROM_2_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ChkProm='S')
    endwith
   endif
  endfunc

  func oMIN_PROM_2_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MIN_PROM) < 60)
    endwith
    return bRes
  endfunc


  add object oTIPORIS_2_49 as StdTableCombo with uid="XMEKXWDJPL",rtseq=68,rtrep=.f.,left=105,top=496,width=123,height=21;
    , HelpContextID = 126494518;
    , cFormVar="w_TIPORIS",tablefilter="", bObbl = .f. , nPag = 2;
    , cLinkFile="TIP_RISE";
    , cTable='TIP_RISE',cKey='TRCODICE',cValue='TRDESTIP',cOrderBy='TRDESTIP',xDefault=space(1);
  , bGlobalFont=.t.


  func oTIPORIS_2_49.mHide()
    with this.Parent.oContained
      return (.w_RISERVE<> 'S')
    endwith
  endfunc

  func oTIPORIS_2_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPORIS_2_49.ecpDrop(oSource)
    this.Parent.oContained.link_2_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oDESUTE_2_50 as StdField with uid="JXGTZBFHCK",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 206548426,;
   bGlobalFont=.t.,;
    Height=21, Width=633, Left=164, Top=439, InputMask=replicate('X',20)

  add object oDESNOM_2_55 as StdField with uid="YBKTMCLSXL",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78032330,;
   bGlobalFont=.t.,;
    Height=21, Width=331, Left=242, Top=38, InputMask=replicate('X',40)

  add object oDESTIPOL_2_62 as StdField with uid="XDLBTHYTYA",rtseq=83,rtrep=.f.,;
    cFormVar = "w_DESTIPOL", cQueryName = "DESTIPOL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 33598846,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=177, Top=187, InputMask=replicate('X',50)

  add object oDESPRA_2_65 as StdField with uid="FPINBUYLXX",rtseq=86,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(75), bMultilanguage =  .f.,;
    HelpContextID = 7646666,;
   bGlobalFont=.t.,;
    Height=21, Width=552, Left=237, Top=299, InputMask=replicate('X',75)

  func oDESPRA_2_65.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDESPIA_2_69 as StdField with uid="RDERHTGXNR",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DESPIA", cQueryName = "DESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 17083850,;
   bGlobalFont=.t.,;
    Height=21, Width=536, Left=272, Top=495, InputMask=replicate('X',40)

  func oDESPIA_2_69.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oDESNOMGIU_2_70 as StdField with uid="OVSVURVTXJ",rtseq=89,rtrep=.f.,;
    cFormVar = "w_DESNOMGIU", cQueryName = "DESNOMGIU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 190404559,;
   bGlobalFont=.t.,;
    Height=21, Width=405, Left=243, Top=243, InputMask=replicate('X',60)

  func oDESNOMGIU_2_70.mHide()
    with this.Parent.oContained
      return (Not isAlt())
    endwith
  endfunc

  add object oDESCRI_2_72 as StdField with uid="FFAMWOLEDT",rtseq=90,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 142716362,;
   bGlobalFont=.t.,;
    Height=21, Width=530, Left=223, Top=383, InputMask=replicate('X',40)

  func oDESCRI_2_72.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oDESCOS_2_84 as StdField with uid="XTEJBUNQYP",rtseq=94,rtrep=.f.,;
    cFormVar = "w_DESCOS", cQueryName = "DESCOS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 246525386,;
   bGlobalFont=.t.,;
    Height=21, Width=530, Left=223, Top=355, InputMask=replicate('X',60)

  add object oSERPIAN_2_85 as StdField with uid="DXJGXWZCBD",rtseq=95,rtrep=.f.,;
    cFormVar = "w_SERPIAN", cQueryName = "SERPIAN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Piano di generazione ativit�",;
    HelpContextID = 17087706,;
   bGlobalFont=.t.,;
    Height=21, Width=169, Left=102, Top=495, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="GEST_ATTI", cZoomOnZoom="GSAG_AGA", oKey_1_1="ATSERIAL", oKey_1_2="this.w_SERPIAN"

  func oSERPIAN_2_85.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oSERPIAN_2_85.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_85('Part',this)
    endwith
    return bRes
  endfunc

  proc oSERPIAN_2_85.ecpDrop(oSource)
    this.Parent.oContained.link_2_85('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSERPIAN_2_85.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GEST_ATTI','*','ATSERIAL',cp_AbsName(this.parent,'oSERPIAN_2_85'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_AGA',"",'',this.parent.oContained
  endproc
  proc oSERPIAN_2_85.mZoomOnZoom
    local i_obj
    i_obj=GSAG_AGA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ATSERIAL=this.parent.oContained.w_SERPIAN
     i_obj.ecpSave()
  endproc

  add object oFiltroWeb_2_86 as StdRadio with uid="NOISYYKDWQ",rtseq=96,rtrep=.f.,left=617, top=93, width=150,height=49;
    , ToolTipText = "Filtro su flag di pubblicazione su Web";
    , cFormVar="w_FiltroWeb", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oFiltroWeb_2_86.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo pubblicabili"
      this.Buttons(1).HelpContextID = 263261403
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo non pubblicabili"
      this.Buttons(2).HelpContextID = 263261403
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 263261403
      this.Buttons(3).Top=30
      this.SetAll("Width",148)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Filtro su flag di pubblicazione su Web")
      StdRadio::init()
    endproc

  func oFiltroWeb_2_86.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFiltroWeb_2_86.GetRadio()
    this.Parent.oContained.w_FiltroWeb = this.RadioValue()
    return .t.
  endfunc

  func oFiltroWeb_2_86.SetRadio()
    this.Parent.oContained.w_FiltroWeb=trim(this.Parent.oContained.w_FiltroWeb)
    this.value = ;
      iif(this.Parent.oContained.w_FiltroWeb=='S',1,;
      iif(this.Parent.oContained.w_FiltroWeb=='N',2,;
      iif(this.Parent.oContained.w_FiltroWeb=='T',3,;
      0)))
  endfunc

  func oFiltroWeb_2_86.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDATINI_Cose_2_102 as StdField with uid="QWTXKWCFBP",rtseq=145,rtrep=.f.,;
    cFormVar = "w_DATINI_Cose", cQueryName = "DATINI_Cose",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione per note e cose da fare",;
    HelpContextID = 146069399,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=102, Top=523

  add object oDATFIN_Cose_2_104 as StdField with uid="DETMKSVCXW",rtseq=146,rtrep=.f.,;
    cFormVar = "w_DATFIN_Cose", cQueryName = "DATFIN_Cose",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione per note e cose da fare",;
    HelpContextID = 67622807,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=223, Top=523

  func oDATFIN_Cose_2_104.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI_Cose<=.w_DATFIN_Cose OR EMPTY(.w_DATFIN_Cose))
    endwith
    return bRes
  endfunc


  add object oBtn_2_113 as StdButton with uid="MNVZAMDXTB",left=201, top=411, width=22,height=21,;
    CpPicture="bmp\ZOOM.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per ricercare per attributo";
    , HelpContextID = 115807786;
    , IMGSIZE=16;
  , bGlobalFont=.t.

    proc oBtn_2_113.Click()
      do gsag_kir with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_113.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsALT())
     endwith
    endif
  endfunc

  add object oDESRIS_2_115 as StdField with uid="WDOPCYWMCC",rtseq=154,rtrep=.f.,;
    cFormVar = "w_DESRIS", cQueryName = "DESRIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 251833802,;
   bGlobalFont=.t.,;
    Height=21, Width=331, Left=242, Top=65, InputMask=replicate('X',80)

  func oDESRIS_2_115.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_2_2 as StdString with uid="GXOWXVAARV",Visible=.t., Left=279, Top=13,;
    Alignment=1, Width=92, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="BSGKMHWZHM",Visible=.t., Left=31, Top=215,;
    Alignment=1, Width=67, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="QSNQCLUHKK",Visible=.t., Left=6, Top=495,;
    Alignment=1, Width=92, Height=18,;
    Caption="Tipo riserva:"  ;
  , bGlobalFont=.t.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (.w_RISERVE<> 'S' or not IsAlt())
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="EIENBMKRMG",Visible=.t., Left=6, Top=439,;
    Alignment=1, Width=92, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="ITQYQODJLV",Visible=.t., Left=6, Top=299,;
    Alignment=1, Width=92, Height=18,;
    Caption="Ente/uff. giud.:"  ;
  , bGlobalFont=.t.

  func oStr_2_52.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oStr_2_53 as StdString with uid="KQSLTINWPU",Visible=.t., Left=6, Top=411,;
    Alignment=1, Width=92, Height=18,;
    Caption="Ufficio/sezione:"  ;
  , bGlobalFont=.t.

  func oStr_2_53.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oStr_2_54 as StdString with uid="RCYDFBJYOO",Visible=.t., Left=6, Top=42,;
    Alignment=1, Width=92, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_59 as StdString with uid="LBTIYCWQWU",Visible=.t., Left=6, Top=159,;
    Alignment=1, Width=92, Height=18,;
    Caption="Disponibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_61 as StdString with uid="YJIZXRPWAZ",Visible=.t., Left=31, Top=187,;
    Alignment=1, Width=67, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_63 as StdString with uid="CTBVQCRJMM",Visible=.t., Left=42, Top=271,;
    Alignment=1, Width=56, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_64 as StdString with uid="BPYZWLNTLX",Visible=.t., Left=6, Top=299,;
    Alignment=1, Width=92, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_64.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_67 as StdString with uid="QWICFPOKNJ",Visible=.t., Left=6, Top=411,;
    Alignment=1, Width=92, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  func oStr_2_67.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  add object oStr_2_68 as StdString with uid="PYXGIHNCPT",Visible=.t., Left=234, Top=411,;
    Alignment=1, Width=75, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  func oStr_2_68.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  add object oStr_2_71 as StdString with uid="ISPYDIAKEG",Visible=.t., Left=6, Top=355,;
    Alignment=1, Width=92, Height=18,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_73 as StdString with uid="CKQRVEYIHP",Visible=.t., Left=40, Top=383,;
    Alignment=1, Width=58, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  func oStr_2_73.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_2_78 as StdString with uid="MMVSQAYOEN",Visible=.t., Left=553, Top=467,;
    Alignment=1, Width=5, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_2_81 as StdString with uid="UASYQLKIAH",Visible=.t., Left=278, Top=605,;
    Alignment=0, Width=868, Height=18,;
    Caption="Attenzione! la seq. della calculation deve essere inferiore a FiltProm in modo che questo ricalcoli dopo l'assegnamento, cancellando i secondi!!!"  ;
  , bGlobalFont=.t.

  func oStr_2_81.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_2_82 as StdString with uid="NUKLUCBHUI",Visible=.t., Left=472, Top=467,;
    Alignment=1, Width=51, Height=18,;
    Caption="Ore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_83 as StdString with uid="LXDDNGIUPI",Visible=.t., Left=63, Top=495,;
    Alignment=1, Width=35, Height=18,;
    Caption="Piano:"  ;
  , bGlobalFont=.t.

  func oStr_2_83.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_2_88 as StdString with uid="YAKKYAVGYE",Visible=.t., Left=53, Top=243,;
    Alignment=1, Width=45, Height=18,;
    Caption="Giudice:"  ;
  , bGlobalFont=.t.

  func oStr_2_88.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oStr_2_94 as StdString with uid="AONOKHKHRZ",Visible=.t., Left=277, Top=581,;
    Alignment=0, Width=415, Height=19,;
    Caption="Attenzione posizione COMPIMPKEY  importante per il resize"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_94.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_2_95 as StdString with uid="KSBGPQZUEI",Visible=.t., Left=48, Top=327,;
    Alignment=1, Width=50, Height=18,;
    Caption="Natura:"  ;
  , bGlobalFont=.t.

  func oStr_2_95.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_2_96 as StdString with uid="QVTFQRDCTZ",Visible=.t., Left=15, Top=13,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_97 as StdString with uid="DNFJJECVYE",Visible=.t., Left=216, Top=159,;
    Alignment=1, Width=125, Height=18,;
    Caption="Riferimento persona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_98 as StdString with uid="GOADFKEWFS",Visible=.t., Left=570, Top=42,;
    Alignment=1, Width=45, Height=18,;
    Caption="Ruolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_98.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_2_101 as StdString with uid="DACIAVJCND",Visible=.t., Left=2, Top=523,;
    Alignment=1, Width=96, Height=18,;
    Caption="Cose da fare da:"  ;
  , bGlobalFont=.t.

  add object oStr_2_103 as StdString with uid="KMXRIRFDUM",Visible=.t., Left=189, Top=523,;
    Alignment=1, Width=29, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_2_116 as StdString with uid="HRAEFMYVHV",Visible=.t., Left=-16, Top=67,;
    Alignment=1, Width=114, Height=18,;
    Caption="Soggetto interno:"  ;
  , bGlobalFont=.t.

  func oStr_2_116.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_2_117 as StdString with uid="TGWAUCUBXZ",Visible=.t., Left=565, Top=67,;
    Alignment=1, Width=50, Height=18,;
    Caption="Ruolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_117.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_2_120 as StdString with uid="LIJLPIXOSP",Visible=.t., Left=57, Top=67,;
    Alignment=1, Width=40, Height=18,;
    Caption="Num.:"  ;
  , bGlobalFont=.t.

  func oStr_2_120.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_2_121 as StdString with uid="JMLLMGFFZD",Visible=.t., Left=219, Top=67,;
    Alignment=1, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_121.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc
enddefine
define class tgsag_kraPag3 as StdContainer
  Width  = 815
  height = 549
  stdWidth  = 815
  stdheight = 549
  resizeXpos=601
  resizeYpos=478
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCONTRA_3_1 as StdField with uid="ONBXTNPJIK",rtseq=109,rtrep=.f.,;
    cFormVar = "w_CONTRA", cQueryName = "CONTRA",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 7402458,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=188, Top=91, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CON_TRAS", cZoomOnZoom="GSAG_ACA", oKey_1_1="COSERIAL", oKey_1_2="this.w_CONTRA"

  func oCONTRA_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELTIPCON<>'T' AND (!EMPTY(.w_CODCLI) OR EMPTY(.w_CODNOM)))
    endwith
   endif
  endfunc

  func oCONTRA_3_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONTRA_3_1.ecpDrop(oSource)
    this.Parent.oContained.link_3_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONTRA_3_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CON_TRAS','*','COSERIAL',cp_AbsName(this.parent,'oCONTRA_3_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACA',"Contratti",'GSAGCKRA.CON_TRAS_VZM',this.parent.oContained
  endproc
  proc oCONTRA_3_1.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_COSERIAL=this.parent.oContained.w_CONTRA
     i_obj.ecpSave()
  endproc

  add object oELCODMOD_3_2 as StdField with uid="FIJRSKLOCD",rtseq=110,rtrep=.f.,;
    cFormVar = "w_ELCODMOD", cQueryName = "ELCODMOD",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice modello inesistente, incongruente o non di tipo a pacchetto",;
    HelpContextID = 89564790,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=188, Top=145, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_ELEM", cZoomOnZoom="GSAG_AME", oKey_1_1="MOCODICE", oKey_1_2="this.w_ELCODMOD"

  func oELCODMOD_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELTIPCON<>'T' AND (!EMPTY(.w_CODCLI) OR EMPTY(.w_CODNOM)))
    endwith
   endif
  endfunc

  func oELCODMOD_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODMOD_3_2.ecpDrop(oSource)
    this.Parent.oContained.link_3_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODMOD_3_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_ELEM','*','MOCODICE',cp_AbsName(this.parent,'oELCODMOD_3_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_AME',"Modelli",'GSAG_KZM.MOD_ELEM_VZM',this.parent.oContained
  endproc
  proc oELCODMOD_3_2.mZoomOnZoom
    local i_obj
    i_obj=GSAG_AME()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_ELCODMOD
     i_obj.ecpSave()
  endproc

  add object oELCODIMP_3_3 as StdField with uid="YNEVJERWLQ",rtseq=111,rtrep=.f.,;
    cFormVar = "w_ELCODIMP", cQueryName = "ELCODIMP",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 156673642,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=188, Top=172, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", oKey_1_1="IMCODICE", oKey_1_2="this.w_ELCODIMP"

  func oELCODIMP_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELTIPCON<>'T' AND (.w_ELTIPCON<>'P' AND (!EMPTY(.w_CODCLI) OR EMPTY(.w_CODNOM))))
    endwith
   endif
  endfunc

  func oELCODIMP_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_3('Part',this)
      if .not. empty(.w_COMPIMPKEYREADR)
        bRes2=.link_3_24('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oELCODIMP_3_3.ecpDrop(oSource)
    this.Parent.oContained.link_3_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODIMP_3_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oELCODIMP_3_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Impianti",'GSAGIKRA.IMP_MAST_VZM',this.parent.oContained
  endproc

  add object oELCODCOM_3_4 as StdField with uid="CJQGBQXQKQ",rtseq=112,rtrep=.f.,;
    cFormVar = "w_ELCODCOM", cQueryName = "ELCODCOM",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 257336941,;
   bGlobalFont=.t.,;
    Height=21, Width=80, Left=727, Top=198

  func oELCODCOM_3_4.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oCODNOM_3_5 as StdField with uid="YIMMNUNKIE",rtseq=113,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo o soggetto esterno della pratica",;
    HelpContextID = 78091226,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=188, Top=36, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITCODNOM And Empty( .w_GIUDICE ))
    endwith
   endif
  endfunc

  func oCODNOM_3_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_3_5.ecpDrop(oSource)
    this.Parent.oContained.link_3_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_3_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_3_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_3_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc

  add object oELRINNOV_3_6 as StdField with uid="RIUNECVQTJ",rtseq=114,rtrep=.f.,;
    cFormVar = "w_ELRINNOV", cQueryName = "ELRINNOV",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 62633572,;
   bGlobalFont=.t.,;
    Height=21, Width=80, Left=188, Top=226, cSayPict='"999999"', cGetPict='"999999"'

  func oELRINNOV_3_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELTIPCON<>'T' AND (!EMPTY(.w_CODCLI) OR EMPTY(.w_CODNOM)))
    endwith
   endif
  endfunc


  add object oELTIPCON_3_7 as StdCombo with uid="UQGYIGUVIC",rtseq=115,rtrep=.f.,left=188,top=65,width=203,height=21;
    , HelpContextID = 245077612;
    , cFormVar="w_ELTIPCON",RowSource=""+"Nessuno,"+"Canone,"+"Pacchetto", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oELTIPCON_3_7.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    iif(this.value =3,'P',;
    space(1)))))
  endfunc
  func oELTIPCON_3_7.GetRadio()
    this.Parent.oContained.w_ELTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oELTIPCON_3_7.SetRadio()
    this.Parent.oContained.w_ELTIPCON=trim(this.Parent.oContained.w_ELTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_ELTIPCON=='T',1,;
      iif(this.Parent.oContained.w_ELTIPCON=='C',2,;
      iif(this.Parent.oContained.w_ELTIPCON=='P',3,;
      0)))
  endfunc

  add object oCOCODCON_3_11 as StdField with uid="CDCGRXWGQH",rtseq=117,rtrep=.f.,;
    cFormVar = "w_COCODCON", cQueryName = "COCODCON",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 257336204,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=188, Top=118, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCODCON"

  func oCOCODCON_3_11.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_COTIPCON))
    endwith
  endfunc

  func oCOCODCON_3_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oANDESCRI_3_12 as StdField with uid="MBSIXOJOZV",rtseq=118,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 242259121,;
   bGlobalFont=.t.,;
    Height=21, Width=400, Left=323, Top=118, InputMask=replicate('X',60)

  func oANDESCRI_3_12.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_COTIPCON))
    endwith
  endfunc

  add object oCODESCON_3_15 as StdField with uid="YOKWJQRYKO",rtseq=119,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 242258828,;
   bGlobalFont=.t.,;
    Height=21, Width=400, Left=323, Top=91, InputMask=replicate('X',50)

  add object oMODESCRI_3_16 as StdField with uid="VQWWSDZUPA",rtseq=120,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 242258673,;
   bGlobalFont=.t.,;
    Height=21, Width=400, Left=323, Top=145, InputMask=replicate('X',60)

  add object oIMDESCRI_3_22 as StdField with uid="RHJOAXDHAR",rtseq=122,rtrep=.f.,;
    cFormVar = "w_IMDESCRI", cQueryName = "IMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 242259249,;
   bGlobalFont=.t.,;
    Height=21, Width=400, Left=323, Top=172, InputMask=replicate('X',50)

  add object oCOMPIMPR_3_23 as StdField with uid="AKXQSRDOOH",rtseq=123,rtrep=.f.,;
    cFormVar = "w_COMPIMPR", cQueryName = "COMPIMPR",;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Componente dell'impianto",;
    HelpContextID = 84214664,;
   bGlobalFont=.t.,;
    Height=21, Width=535, Left=188, Top=199, InputMask=replicate('X',50), bHasZoom = .t. 

  func oCOMPIMPR_3_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ELCODIMP) AND .w_ELTIPCON<>'P')
    endwith
   endif
  endfunc

  func oCOMPIMPR_3_23.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  proc oCOMPIMPR_3_23.mBefore
    with this.Parent.oContained
      .w_OLDCOMPR=.w_COMPIMPR
    endwith
  endproc

  proc oCOMPIMPR_3_23.mAfter
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_ELCODIMP,.w_COMPIMPR,.w_OLDCOMPR,CHR(87)+"_ELCODCOM","M")
      endwith
  endproc

  proc oCOMPIMPR_3_23.mZoom
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_ELCODIMP,.w_COMPIMPR,.w_OLDCOMPR,CHR(87)+"_ELCODCOM","M",.T.)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oBtn_3_29 as StdButton with uid="WOGBFPGBLG",left=757, top=39, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=3;
    , ToolTipText = "Ricerca";
    , HelpContextID = 60913430;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_29.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESNOM_3_31 as StdField with uid="MBZVGQLOWC",rtseq=127,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78032330,;
   bGlobalFont=.t.,;
    Height=21, Width=400, Left=325, Top=36, InputMask=replicate('X',40)

  add object oStr_3_8 as StdString with uid="NMOJWOSVVS",Visible=.t., Left=22, Top=66,;
    Alignment=1, Width=160, Height=18,;
    Caption="Tipo contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_9 as StdString with uid="EKXMHDUAZQ",Visible=.t., Left=22, Top=92,;
    Alignment=1, Width=160, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="WTYLCNVLYT",Visible=.t., Left=43, Top=121,;
    Alignment=1, Width=141, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_3_13.mHide()
    with this.Parent.oContained
      return (.w_COTIPCON<>'C')
    endwith
  endfunc

  add object oStr_3_14 as StdString with uid="ILGAZJXWVF",Visible=.t., Left=43, Top=121,;
    Alignment=1, Width=141, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_3_14.mHide()
    with this.Parent.oContained
      return (.w_COTIPCON<>'F')
    endwith
  endfunc

  add object oStr_3_18 as StdString with uid="EBAFTVIIFN",Visible=.t., Left=44, Top=147,;
    Alignment=1, Width=141, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="LDHRPRAXUT",Visible=.t., Left=44, Top=173,;
    Alignment=1, Width=141, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_20 as StdString with uid="AURUTIHBUT",Visible=.t., Left=44, Top=201,;
    Alignment=1, Width=141, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  func oStr_3_20.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  add object oStr_3_21 as StdString with uid="TIJEGZIRBD",Visible=.t., Left=44, Top=227,;
    Alignment=1, Width=141, Height=18,;
    Caption="Numero rinnovo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_30 as StdString with uid="XIAUXZREFT",Visible=.t., Left=90, Top=37,;
    Alignment=1, Width=92, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsag_kraPag4 as StdContainer
  Width  = 815
  height = 549
  stdWidth  = 815
  stdheight = 549
  resizeYpos=266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPATIPRIS_4_6 as StdCombo with uid="GTJVRKATPA",value=4,rtseq=129,rtrep=.f.,left=85,top=9,width=76,height=21;
    , HelpContextID = 6577993;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPATIPRIS_4_6.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    iif(this.value =3,'R',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oPATIPRIS_4_6.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_4_6.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',3,;
      iif(this.Parent.oContained.w_PATIPRIS=='',4,;
      0))))
  endfunc

  add object oCODPART_4_7 as StdField with uid="JOJKPNUICX",rtseq=130,rtrep=.f.,;
    cFormVar = "w_CODPART", cQueryName = "CODPART",nZero=5,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice partecipante",;
    HelpContextID = 259681318,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=170, Top=9, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_CODPART"

  func oCODPART_4_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPART_4_7.ecpDrop(oSource)
    this.Parent.oContained.link_4_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPART_4_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oCODPART_4_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oGRUPART_4_8 as StdField with uid="ZEDITMWRPB",rtseq=131,rtrep=.f.,;
    cFormVar = "w_GRUPART", cQueryName = "GRUPART",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo inesistente o non associato al partecipante",;
    HelpContextID = 259751782,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=245, Top=32, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_GRUPART"

  func oGRUPART_4_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DPTIPRIS='P' AND Not Empty(.w_CODPART) )
    endwith
   endif
  endfunc

  func oGRUPART_4_8.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  func oGRUPART_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPART_4_8.ecpDrop(oSource)
    this.Parent.oContained.link_4_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPART_4_8.mZoom
      with this.Parent.oContained
        GSAR1BGP(this.Parent.oContained,  "", .w_CODPART, "", "GRUPART" )
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFLESGR_4_9 as StdCheck with uid="XQUAKWMXQO",rtseq=132,rtrep=.f.,left=245, top=33, caption="Esplodi gruppo",;
    ToolTipText = "Se attivo visualizza anche appuntamenti delle persone del gruppo",;
    HelpContextID = 2262698,;
    cFormVar="w_FLESGR", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLESGR_4_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLESGR_4_9.GetRadio()
    this.Parent.oContained.w_FLESGR = this.RadioValue()
    return .t.
  endfunc

  func oFLESGR_4_9.SetRadio()
    this.Parent.oContained.w_FLESGR=trim(this.Parent.oContained.w_FLESGR)
    this.value = ;
      iif(this.Parent.oContained.w_FLESGR=='S',1,;
      0)
  endfunc

  func oFLESGR_4_9.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'G' or Empty(.w_CODPART))
    endwith
  endfunc


  add object oBtn_4_10 as StdButton with uid="VLOWMBFCXT",left=753, top=43, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=4;
    , ToolTipText = "Ricerca";
    , HelpContextID = 60913430;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_10.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oAnnotazioni_4_11 as StdCheck with uid="WGRONEBKZM",rtseq=133,rtrep=.f.,left=21, top=473, caption="Note attivit�",;
    ToolTipText = "Se attivo, stampa le note attivit�",;
    HelpContextID = 237817761,;
    cFormVar="w_Annotazioni", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAnnotazioni_4_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAnnotazioni_4_11.GetRadio()
    this.Parent.oContained.w_Annotazioni = this.RadioValue()
    return .t.
  endfunc

  func oAnnotazioni_4_11.SetRadio()
    this.Parent.oContained.w_Annotazioni=trim(this.Parent.oContained.w_Annotazioni)
    this.value = ;
      iif(this.Parent.oContained.w_Annotazioni=='S',1,;
      0)
  endfunc

  add object oDescagg_4_12 as StdCheck with uid="FSYHMFZRNQ",rtseq=134,rtrep=.f.,left=21, top=494, caption="Descrizione aggiuntiva",;
    ToolTipText = "Se attivo, stampa la descrizione aggiuntiva",;
    HelpContextID = 110129718,;
    cFormVar="w_Descagg", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oDescagg_4_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDescagg_4_12.GetRadio()
    this.Parent.oContained.w_Descagg = this.RadioValue()
    return .t.
  endfunc

  func oDescagg_4_12.SetRadio()
    this.Parent.oContained.w_Descagg=trim(this.Parent.oContained.w_Descagg)
    this.value = ;
      iif(this.Parent.oContained.w_Descagg=='S',1,;
      0)
  endfunc

  add object oDETTIMP_4_13 as StdCheck with uid="UINFRLTFXU",rtseq=135,rtrep=.f.,left=212, top=473, caption="Dettaglio attributi",;
    ToolTipText = "Se attivo, stampa il dettaglio attributi degli impianti",;
    HelpContextID = 83926474,;
    cFormVar="w_DETTIMP", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oDETTIMP_4_13.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDETTIMP_4_13.GetRadio()
    this.Parent.oContained.w_DETTIMP = this.RadioValue()
    return .t.
  endfunc

  func oDETTIMP_4_13.SetRadio()
    this.Parent.oContained.w_DETTIMP=trim(this.Parent.oContained.w_DETTIMP)
    this.value = ;
      iif(this.Parent.oContained.w_DETTIMP=='S',1,;
      0)
  endfunc

  func oDETTIMP_4_13.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDatiPratica_4_14 as StdCheck with uid="ULJQESJSTY",rtseq=136,rtrep=.f.,left=212, top=473, caption="Dati pratica",;
    ToolTipText = "Se attivo, stampa i dati della pratica",;
    HelpContextID = 9238586,;
    cFormVar="w_DatiPratica", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oDatiPratica_4_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDatiPratica_4_14.GetRadio()
    this.Parent.oContained.w_DatiPratica = this.RadioValue()
    return .t.
  endfunc

  func oDatiPratica_4_14.SetRadio()
    this.Parent.oContained.w_DatiPratica=trim(this.Parent.oContained.w_DatiPratica)
    this.value = ;
      iif(this.Parent.oContained.w_DatiPratica=='S',1,;
      0)
  endfunc

  func oDatiPratica_4_14.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oELCONTR_4_15 as StdCheck with uid="RBDYNNUKWL",rtseq=137,rtrep=.f.,left=212, top=494, caption="Elemento contratto",;
    ToolTipText = "Se attivo, stampa l'elemento contratto del dettaglio prestazioni",;
    HelpContextID = 38361414,;
    cFormVar="w_ELCONTR", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oELCONTR_4_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oELCONTR_4_15.GetRadio()
    this.Parent.oContained.w_ELCONTR = this.RadioValue()
    return .t.
  endfunc

  func oELCONTR_4_15.SetRadio()
    this.Parent.oContained.w_ELCONTR=trim(this.Parent.oContained.w_ELCONTR)
    this.value = ;
      iif(this.Parent.oContained.w_ELCONTR=='S',1,;
      0)
  endfunc

  func oELCONTR_4_15.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oAccodaPreavvisi_4_16 as StdCheck with uid="ZFZLPXQWWQ",rtseq=138,rtrep=.f.,left=364, top=473, caption="Accoda preavvisi",;
    ToolTipText = "Se attivo, accoda i preavvisi",;
    HelpContextID = 86427704,;
    cFormVar="w_AccodaPreavvisi", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAccodaPreavvisi_4_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAccodaPreavvisi_4_16.GetRadio()
    this.Parent.oContained.w_AccodaPreavvisi = this.RadioValue()
    return .t.
  endfunc

  func oAccodaPreavvisi_4_16.SetRadio()
    this.Parent.oContained.w_AccodaPreavvisi=trim(this.Parent.oContained.w_AccodaPreavvisi)
    this.value = ;
      iif(this.Parent.oContained.w_AccodaPreavvisi=='S',1,;
      0)
  endfunc

  add object oAccodaNote_4_17 as StdCheck with uid="WKLVDZRVGV",rtseq=139,rtrep=.f.,left=364, top=494, caption="Accoda le note",;
    ToolTipText = "Se attivo, accoda le note",;
    HelpContextID = 255075147,;
    cFormVar="w_AccodaNote", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAccodaNote_4_17.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAccodaNote_4_17.GetRadio()
    this.Parent.oContained.w_AccodaNote = this.RadioValue()
    return .t.
  endfunc

  func oAccodaNote_4_17.SetRadio()
    this.Parent.oContained.w_AccodaNote=trim(this.Parent.oContained.w_AccodaNote)
    this.value = ;
      iif(this.Parent.oContained.w_AccodaNote=='S',1,;
      0)
  endfunc

  func oAccodaNote_4_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_MEMO='M')
    endwith
   endif
  endfunc

  func oAccodaNote_4_17.mHide()
    with this.Parent.oContained
      return (.w_MEMO='M')
    endwith
  endfunc

  add object oAccodaDaFare_4_18 as StdCheck with uid="ZOGZDBZCXV",rtseq=140,rtrep=.f.,left=551, top=473, caption="Accoda le cose da fare",;
    ToolTipText = "Se attivo, accoda le cose da fare",;
    HelpContextID = 20444615,;
    cFormVar="w_AccodaDaFare", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oAccodaDaFare_4_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAccodaDaFare_4_18.GetRadio()
    this.Parent.oContained.w_AccodaDaFare = this.RadioValue()
    return .t.
  endfunc

  func oAccodaDaFare_4_18.SetRadio()
    this.Parent.oContained.w_AccodaDaFare=trim(this.Parent.oContained.w_AccodaDaFare)
    this.value = ;
      iif(this.Parent.oContained.w_AccodaDaFare=='S',1,;
      0)
  endfunc

  func oAccodaDaFare_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_DAFARE='D')
    endwith
   endif
  endfunc

  func oAccodaDaFare_4_18.mHide()
    with this.Parent.oContained
      return (.w_DAFARE='D')
    endwith
  endfunc

  add object oVISUDI_4_19 as StdCheck with uid="QNKFDXRENN",rtseq=141,rtrep=.f.,left=551, top=494, caption="Dati udienza",;
    ToolTipText = "Se attivo, per ogni attivit� non di tipo Udienza/Da inser.prestaz./Assenza/Nota vengono riportate in stampa le date della prossima udienza da evadere e quella dell'ultima udienza evasa.",;
    HelpContextID = 156215466,;
    cFormVar="w_VISUDI", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oVISUDI_4_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVISUDI_4_19.GetRadio()
    this.Parent.oContained.w_VISUDI = this.RadioValue()
    return .t.
  endfunc

  func oVISUDI_4_19.SetRadio()
    this.Parent.oContained.w_VISUDI=trim(this.Parent.oContained.w_VISUDI)
    this.value = ;
      iif(this.Parent.oContained.w_VISUDI=='S',1,;
      0)
  endfunc

  func oVISUDI_4_19.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR .w_DatiPratica<>'S')
    endwith
  endfunc


  add object OUT1 as cp_outputCombo with uid="EKMSDZCNWW",left=188, top=522, width=276,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=4;
    , HelpContextID = 61988838


  add object OUT as cp_outputCombo with uid="ZLERXIVPWT",left=187, top=522, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=4;
    , HelpContextID = 61988838


  add object oBtn_4_22 as StdButton with uid="MBXAIGGBZE",left=753, top=502, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 242654682;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_22.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc

  add object oDENOM_PART_4_27 as StdField with uid="VCFNHXFDWG",rtseq=142,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(48), bMultilanguage =  .f.,;
    HelpContextID = 46507113,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=245, Top=9, InputMask=replicate('X',48)


  add object PART_ZOOM as cp_szoombox with uid="WODZOSNAJX",left=7, top=55, width=731,height=402,;
    caption='PART_ZOOM',;
   bGlobalFont=.t.,;
    cTable="DIPENDEN",cZoomFile="GSAG2MPP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Blank,w_DATINIZ Changed,w_obsodat1 Changed",;
    nPag=4;
    , HelpContextID = 111197163

  add object oDATINIZ_4_33 as StdField with uid="KXZLKOTRZF",rtseq=157,rtrep=.f.,;
    cFormVar = "w_DATINIZ", cQueryName = "DATINIZ",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 121921078,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=529, Top=33

  add object oobsodat1_4_34 as StdCheck with uid="TANZHKUOWP",rtseq=158,rtrep=.f.,left=641, top=33, caption="Solo obsoleti",;
    ToolTipText = "Se attivo, visualizza solo i partecipanti obsoleti",;
    HelpContextID = 13398551,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oobsodat1_4_34.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_4_34.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_4_34.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc

  add object oSTADETTDOC_4_36 as StdCheck with uid="XGFLBDWCLX",rtseq=160,rtrep=.f.,left=551, top=494, caption="Dettaglio documenti associati",;
    ToolTipText = "Dettaglio documenti associati",;
    HelpContextID = 28215898,;
    cFormVar="w_STADETTDOC", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oSTADETTDOC_4_36.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTADETTDOC_4_36.GetRadio()
    this.Parent.oContained.w_STADETTDOC = this.RadioValue()
    return .t.
  endfunc

  func oSTADETTDOC_4_36.SetRadio()
    this.Parent.oContained.w_STADETTDOC=trim(this.Parent.oContained.w_STADETTDOC)
    this.value = ;
      iif(this.Parent.oContained.w_STADETTDOC=='S',1,;
      0)
  endfunc

  func oSTADETTDOC_4_36.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_23 as StdString with uid="HVQDALZAWB",Visible=.t., Left=48, Top=525,;
    Alignment=1, Width=136, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_4_26 as StdString with uid="IIPQFPVHAG",Visible=.t., Left=5, Top=10,;
    Alignment=1, Width=77, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_4_28 as StdString with uid="QEJPOXEYOK",Visible=.t., Left=195, Top=35,;
    Alignment=1, Width=48, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_4_28.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  add object oStr_4_32 as StdString with uid="XVUBYBVEQE",Visible=.t., Left=432, Top=35,;
    Alignment=1, Width=92, Height=18,;
    Caption="Data di controllo:"  ;
  , bGlobalFont=.t.

  add object oBox_4_24 as StdBox with uid="RFNQYNETFL",left=17, top=468, width=732,height=50

  add object oBox_4_25 as StdBox with uid="EQZVEGJGOF",left=359, top=468, width=1,height=50

  add object oBox_4_30 as StdBox with uid="VXMCKPSRKH",left=168, top=7, width=68,height=27
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kra','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
