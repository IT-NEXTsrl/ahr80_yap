* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_aib                                                        *
*              Parametri importazione BLUES                                    *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-02                                                      *
* Last revis.: 2010-04-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsfa_aib")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsfa_aib")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsfa_aib")
  return

* --- Class definition
define class tgsfa_aib as StdPCForm
  Width  = 695
  Height = 132
  Top    = 10
  Left   = 10
  cComment = "Parametri importazione BLUES"
  cPrg = "gsfa_aib"
  HelpContextID=9077353
  add object cnt as tcgsfa_aib
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsfa_aib as PCContext
  w_IBCODICE = 0
  w_IBNAMFIL = space(254)
  w_IBCODUID = space(100)
  w_IB___PWD = space(100)
  w_TIPDIR1 = space(10)
  w_TIPDIR2 = space(10)
  w_DESCRII = space(50)
  w_DESCRIO = space(50)
  w_IBTIPEVI = space(10)
  w_IBTIPEVO = space(10)
  w_DRIVER1 = space(10)
  w_DRIVER2 = space(10)
  w_TIPODRV1 = space(1)
  w_TIPODRV2 = space(1)
  proc Save(oFrom)
    this.w_IBCODICE = oFrom.w_IBCODICE
    this.w_IBNAMFIL = oFrom.w_IBNAMFIL
    this.w_IBCODUID = oFrom.w_IBCODUID
    this.w_IB___PWD = oFrom.w_IB___PWD
    this.w_TIPDIR1 = oFrom.w_TIPDIR1
    this.w_TIPDIR2 = oFrom.w_TIPDIR2
    this.w_DESCRII = oFrom.w_DESCRII
    this.w_DESCRIO = oFrom.w_DESCRIO
    this.w_IBTIPEVI = oFrom.w_IBTIPEVI
    this.w_IBTIPEVO = oFrom.w_IBTIPEVO
    this.w_DRIVER1 = oFrom.w_DRIVER1
    this.w_DRIVER2 = oFrom.w_DRIVER2
    this.w_TIPODRV1 = oFrom.w_TIPODRV1
    this.w_TIPODRV2 = oFrom.w_TIPODRV2
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_IBCODICE = this.w_IBCODICE
    oTo.w_IBNAMFIL = this.w_IBNAMFIL
    oTo.w_IBCODUID = this.w_IBCODUID
    oTo.w_IB___PWD = this.w_IB___PWD
    oTo.w_TIPDIR1 = this.w_TIPDIR1
    oTo.w_TIPDIR2 = this.w_TIPDIR2
    oTo.w_DESCRII = this.w_DESCRII
    oTo.w_DESCRIO = this.w_DESCRIO
    oTo.w_IBTIPEVI = this.w_IBTIPEVI
    oTo.w_IBTIPEVO = this.w_IBTIPEVO
    oTo.w_DRIVER1 = this.w_DRIVER1
    oTo.w_DRIVER2 = this.w_DRIVER2
    oTo.w_TIPODRV1 = this.w_TIPODRV1
    oTo.w_TIPODRV2 = this.w_TIPODRV2
    PCContext::Load(oTo)
enddefine

define class tcgsfa_aib as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 695
  Height = 132
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-04-19"
  HelpContextID=9077353
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  BLUESCON_IDX = 0
  TIPEVENT_IDX = 0
  DRVEVENT_IDX = 0
  cFile = "BLUESCON"
  cKeySelect = "IBCODICE"
  cKeyWhere  = "IBCODICE=this.w_IBCODICE"
  cKeyWhereODBC = '"IBCODICE="+cp_ToStrODBC(this.w_IBCODICE)';

  cKeyWhereODBCqualified = '"BLUESCON.IBCODICE="+cp_ToStrODBC(this.w_IBCODICE)';

  cPrg = "gsfa_aib"
  cComment = "Parametri importazione BLUES"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_IBCODICE = 0
  w_IBNAMFIL = space(254)
  w_IBCODUID = space(100)
  w_IB___PWD = space(100)
  w_TIPDIR1 = space(10)
  w_TIPDIR2 = space(10)
  w_DESCRII = space(50)
  w_DESCRIO = space(50)
  w_IBTIPEVI = space(10)
  o_IBTIPEVI = space(10)
  w_IBTIPEVO = space(10)
  o_IBTIPEVO = space(10)
  w_DRIVER1 = space(10)
  w_DRIVER2 = space(10)
  w_TIPODRV1 = space(1)
  w_TIPODRV2 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsfa_aibPag1","gsfa_aib",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 219065354
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIBNAMFIL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TIPEVENT'
    this.cWorkTables[2]='DRVEVENT'
    this.cWorkTables[3]='BLUESCON'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.BLUESCON_IDX,5],7]
    this.nPostItConn=i_TableProp[this.BLUESCON_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsfa_aib'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from BLUESCON where IBCODICE=KeySet.IBCODICE
    *
    i_nConn = i_TableProp[this.BLUESCON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BLUESCON_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('BLUESCON')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "BLUESCON.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' BLUESCON '
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IBCODICE',this.w_IBCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPDIR1 = 'E'
        .w_TIPDIR2 = 'U'
        .w_DESCRII = space(50)
        .w_DESCRIO = space(50)
        .w_TIPODRV1 = space(1)
        .w_TIPODRV2 = space(1)
        .w_IBCODICE = NVL(IBCODICE,0)
        .w_IBNAMFIL = NVL(IBNAMFIL,space(254))
        .w_IBCODUID = NVL(IBCODUID,space(100))
        .w_IB___PWD = NVL(IB___PWD,space(100))
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate('*')
        .w_IBTIPEVI = NVL(IBTIPEVI,space(10))
          if link_1_15_joined
            this.w_IBTIPEVI = NVL(TETIPEVE115,NVL(this.w_IBTIPEVI,space(10)))
            this.w_DESCRII = NVL(TEDESCRI115,space(50))
            this.w_TIPDIR1 = NVL(TETIPDIR115,space(10))
            this.w_DRIVER1 = NVL(TEDRIVER115,space(10))
          else
          .link_1_15('Load')
          endif
        .w_IBTIPEVO = NVL(IBTIPEVO,space(10))
          if link_1_16_joined
            this.w_IBTIPEVO = NVL(TETIPEVE116,NVL(this.w_IBTIPEVO,space(10)))
            this.w_DESCRIO = NVL(TEDESCRI116,space(50))
            this.w_TIPDIR2 = NVL(TETIPDIR116,space(10))
            this.w_DRIVER2 = NVL(TEDRIVER116,space(10))
          else
          .link_1_16('Load')
          endif
        .w_DRIVER1 = .w_DRIVER1
          .link_1_17('Load')
        .w_DRIVER2 = .w_DRIVER2
          .link_1_18('Load')
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        cp_LoadRecExtFlds(this,'BLUESCON')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_IBCODICE = 0
      .w_IBNAMFIL = space(254)
      .w_IBCODUID = space(100)
      .w_IB___PWD = space(100)
      .w_TIPDIR1 = space(10)
      .w_TIPDIR2 = space(10)
      .w_DESCRII = space(50)
      .w_DESCRIO = space(50)
      .w_IBTIPEVI = space(10)
      .w_IBTIPEVO = space(10)
      .w_DRIVER1 = space(10)
      .w_DRIVER2 = space(10)
      .w_TIPODRV1 = space(1)
      .w_TIPODRV2 = space(1)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate('*')
          .DoRTCalc(1,4,.f.)
        .w_TIPDIR1 = 'E'
        .w_TIPDIR2 = 'U'
        .DoRTCalc(7,9,.f.)
          if not(empty(.w_IBTIPEVI))
          .link_1_15('Full')
          endif
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_IBTIPEVO))
          .link_1_16('Full')
          endif
        .w_DRIVER1 = .w_DRIVER1
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_DRIVER1))
          .link_1_17('Full')
          endif
        .w_DRIVER2 = .w_DRIVER2
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_DRIVER2))
          .link_1_18('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'BLUESCON')
    this.DoRTCalc(13,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oIBNAMFIL_1_2.enabled = i_bVal
      .Page1.oPag.oIBCODUID_1_4.enabled = i_bVal
      .Page1.oPag.oIB___PWD_1_6.enabled = i_bVal
      .Page1.oPag.oIBTIPEVI_1_15.enabled = i_bVal
      .Page1.oPag.oIBTIPEVO_1_16.enabled = i_bVal
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      .Page1.oPag.oObj_1_21.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'BLUESCON',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.BLUESCON_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IBCODICE,"IBCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IBNAMFIL,"IBNAMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IBCODUID,"IBCODUID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IB___PWD,"IB___PWD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IBTIPEVI,"IBTIPEVI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IBTIPEVO,"IBTIPEVO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.BLUESCON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BLUESCON_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.BLUESCON_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into BLUESCON
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'BLUESCON')
        i_extval=cp_InsertValODBCExtFlds(this,'BLUESCON')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(IBCODICE,IBNAMFIL,IBCODUID,IB___PWD,IBTIPEVI"+;
                  ",IBTIPEVO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_IBCODICE)+;
                  ","+cp_ToStrODBC(this.w_IBNAMFIL)+;
                  ","+cp_ToStrODBC(this.w_IBCODUID)+;
                  ","+cp_ToStrODBC(this.w_IB___PWD)+;
                  ","+cp_ToStrODBCNull(this.w_IBTIPEVI)+;
                  ","+cp_ToStrODBCNull(this.w_IBTIPEVO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'BLUESCON')
        i_extval=cp_InsertValVFPExtFlds(this,'BLUESCON')
        cp_CheckDeletedKey(i_cTable,0,'IBCODICE',this.w_IBCODICE)
        INSERT INTO (i_cTable);
              (IBCODICE,IBNAMFIL,IBCODUID,IB___PWD,IBTIPEVI,IBTIPEVO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_IBCODICE;
                  ,this.w_IBNAMFIL;
                  ,this.w_IBCODUID;
                  ,this.w_IB___PWD;
                  ,this.w_IBTIPEVI;
                  ,this.w_IBTIPEVO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.BLUESCON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BLUESCON_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.BLUESCON_IDX,i_nConn)
      *
      * update BLUESCON
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'BLUESCON')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " IBNAMFIL="+cp_ToStrODBC(this.w_IBNAMFIL)+;
             ",IBCODUID="+cp_ToStrODBC(this.w_IBCODUID)+;
             ",IB___PWD="+cp_ToStrODBC(this.w_IB___PWD)+;
             ",IBTIPEVI="+cp_ToStrODBCNull(this.w_IBTIPEVI)+;
             ",IBTIPEVO="+cp_ToStrODBCNull(this.w_IBTIPEVO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'BLUESCON')
        i_cWhere = cp_PKFox(i_cTable  ,'IBCODICE',this.w_IBCODICE  )
        UPDATE (i_cTable) SET;
              IBNAMFIL=this.w_IBNAMFIL;
             ,IBCODUID=this.w_IBCODUID;
             ,IB___PWD=this.w_IB___PWD;
             ,IBTIPEVI=this.w_IBTIPEVI;
             ,IBTIPEVO=this.w_IBTIPEVO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.BLUESCON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BLUESCON_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.BLUESCON_IDX,i_nConn)
      *
      * delete BLUESCON
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'IBCODICE',this.w_IBCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.BLUESCON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BLUESCON_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate('*')
        .DoRTCalc(1,10,.t.)
            .w_DRIVER1 = .w_DRIVER1
          .link_1_17('Full')
            .w_DRIVER2 = .w_DRIVER2
          .link_1_18('Full')
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        if .o_IBTIPEVI<>.w_IBTIPEVI
          .Calculate_KOHUIRSVTQ()
        endif
        if .o_IBTIPEVO<>.w_IBTIPEVO
          .Calculate_MZXNQOFHKN()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(13,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
    endwith
  return

  proc Calculate_KOHUIRSVTQ()
    with this
          * --- Controllo se diver ingresso di tipo telefono
          ChkEVENTO(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_MZXNQOFHKN()
    with this
          * --- Controllo se diver uscita di tipo telefono
          ChkEVENTO(this;
              ,'O';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IBTIPEVI
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IBTIPEVI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSFA_ATE',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_IBTIPEVI)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_IBTIPEVI))
          select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IBTIPEVI)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStrODBC(trim(this.w_IBTIPEVI)+"%");

            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStr(trim(this.w_IBTIPEVI)+"%");

            select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IBTIPEVI) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oIBTIPEVI_1_15'),i_cWhere,'GSFA_ATE',"Tipi eventi",'gsfa1aib.TIPEVENT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IBTIPEVI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_IBTIPEVI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_IBTIPEVI)
            select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IBTIPEVI = NVL(_Link_.TETIPEVE,space(10))
      this.w_DESCRII = NVL(_Link_.TEDESCRI,space(50))
      this.w_TIPDIR1 = NVL(_Link_.TETIPDIR,space(10))
      this.w_DRIVER1 = NVL(_Link_.TEDRIVER,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_IBTIPEVI = space(10)
      endif
      this.w_DESCRII = space(50)
      this.w_TIPDIR1 = space(10)
      this.w_DRIVER1 = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPDIR1='E'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_IBTIPEVI = space(10)
        this.w_DESCRII = space(50)
        this.w_TIPDIR1 = space(10)
        this.w_DRIVER1 = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IBTIPEVI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPEVENT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.TETIPEVE as TETIPEVE115"+ ",link_1_15.TEDESCRI as TEDESCRI115"+ ",link_1_15.TETIPDIR as TETIPDIR115"+ ",link_1_15.TEDRIVER as TEDRIVER115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on BLUESCON.IBTIPEVI=link_1_15.TETIPEVE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and BLUESCON.IBTIPEVI=link_1_15.TETIPEVE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IBTIPEVO
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IBTIPEVO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSFA_ATE',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_IBTIPEVO)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_IBTIPEVO))
          select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IBTIPEVO)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStrODBC(trim(this.w_IBTIPEVO)+"%");

            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStr(trim(this.w_IBTIPEVO)+"%");

            select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IBTIPEVO) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oIBTIPEVO_1_16'),i_cWhere,'GSFA_ATE',"Tipi eventi",'gsfa2aib.TIPEVENT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IBTIPEVO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_IBTIPEVO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_IBTIPEVO)
            select TETIPEVE,TEDESCRI,TETIPDIR,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IBTIPEVO = NVL(_Link_.TETIPEVE,space(10))
      this.w_DESCRIO = NVL(_Link_.TEDESCRI,space(50))
      this.w_TIPDIR2 = NVL(_Link_.TETIPDIR,space(10))
      this.w_DRIVER2 = NVL(_Link_.TEDRIVER,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_IBTIPEVO = space(10)
      endif
      this.w_DESCRIO = space(50)
      this.w_TIPDIR2 = space(10)
      this.w_DRIVER2 = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPDIR2='U'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_IBTIPEVO = space(10)
        this.w_DESCRIO = space(50)
        this.w_TIPDIR2 = space(10)
        this.w_DRIVER2 = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IBTIPEVO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPEVENT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.TETIPEVE as TETIPEVE116"+ ",link_1_16.TEDESCRI as TEDESCRI116"+ ",link_1_16.TETIPDIR as TETIPDIR116"+ ",link_1_16.TEDRIVER as TEDRIVER116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on BLUESCON.IBTIPEVO=link_1_16.TETIPEVE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and BLUESCON.IBTIPEVO=link_1_16.TETIPEVE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRIVER1
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_lTable = "DRVEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2], .t., this.DRVEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRIVER1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRIVER1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DEDRIVER,DE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where DEDRIVER="+cp_ToStrODBC(this.w_DRIVER1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DEDRIVER',this.w_DRIVER1)
            select DEDRIVER,DE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRIVER1 = NVL(_Link_.DEDRIVER,space(10))
      this.w_TIPODRV1 = NVL(_Link_.DE__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DRIVER1 = space(10)
      endif
      this.w_TIPODRV1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])+'\'+cp_ToStr(_Link_.DEDRIVER,1)
      cp_ShowWarn(i_cKey,this.DRVEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRIVER1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DRIVER2
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_lTable = "DRVEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2], .t., this.DRVEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRIVER2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRIVER2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DEDRIVER,DE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where DEDRIVER="+cp_ToStrODBC(this.w_DRIVER2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DEDRIVER',this.w_DRIVER2)
            select DEDRIVER,DE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRIVER2 = NVL(_Link_.DEDRIVER,space(10))
      this.w_TIPODRV2 = NVL(_Link_.DE__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DRIVER2 = space(10)
      endif
      this.w_TIPODRV2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])+'\'+cp_ToStr(_Link_.DEDRIVER,1)
      cp_ShowWarn(i_cKey,this.DRVEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRIVER2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIBNAMFIL_1_2.value==this.w_IBNAMFIL)
      this.oPgFrm.Page1.oPag.oIBNAMFIL_1_2.value=this.w_IBNAMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oIBCODUID_1_4.value==this.w_IBCODUID)
      this.oPgFrm.Page1.oPag.oIBCODUID_1_4.value=this.w_IBCODUID
    endif
    if not(this.oPgFrm.Page1.oPag.oIB___PWD_1_6.value==this.w_IB___PWD)
      this.oPgFrm.Page1.oPag.oIB___PWD_1_6.value=this.w_IB___PWD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRII_1_13.value==this.w_DESCRII)
      this.oPgFrm.Page1.oPag.oDESCRII_1_13.value=this.w_DESCRII
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIO_1_14.value==this.w_DESCRIO)
      this.oPgFrm.Page1.oPag.oDESCRIO_1_14.value=this.w_DESCRIO
    endif
    if not(this.oPgFrm.Page1.oPag.oIBTIPEVI_1_15.value==this.w_IBTIPEVI)
      this.oPgFrm.Page1.oPag.oIBTIPEVI_1_15.value=this.w_IBTIPEVI
    endif
    if not(this.oPgFrm.Page1.oPag.oIBTIPEVO_1_16.value==this.w_IBTIPEVO)
      this.oPgFrm.Page1.oPag.oIBTIPEVO_1_16.value=this.w_IBTIPEVO
    endif
    cp_SetControlsValueExtFlds(this,'BLUESCON')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPDIR1='E')  and not(empty(.w_IBTIPEVI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIBTIPEVI_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPDIR2='U')  and not(empty(.w_IBTIPEVO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIBTIPEVO_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IBTIPEVI = this.w_IBTIPEVI
    this.o_IBTIPEVO = this.w_IBTIPEVO
    return

enddefine

* --- Define pages as container
define class tgsfa_aibPag1 as StdContainer
  Width  = 691
  height = 132
  stdWidth  = 691
  stdheight = 132
  resizeXpos=411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIBNAMFIL_1_2 as StdField with uid="GOBHPAAPUI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_IBNAMFIL", cQueryName = "IBNAMFIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome file mdb di Blues",;
    HelpContextID = 176923858,;
   bGlobalFont=.t.,;
    Height=21, Width=506, Left=153, Top=4, InputMask=replicate('X',254)

  add object oIBCODUID_1_4 as StdField with uid="YFJFDQSVXP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_IBCODUID", cQueryName = "IBCODUID",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Codice utente per la connessione",;
    HelpContextID = 151581898,;
   bGlobalFont=.t.,;
    Height=21, Width=531, Left=153, Top=29, InputMask=replicate('X',100)

  add object oIB___PWD_1_6 as StdField with uid="WLDDVGSHTK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_IB___PWD", cQueryName = "IB___PWD",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Password per la connessione",;
    HelpContextID = 97170634,;
   bGlobalFont=.t.,;
    Height=21, Width=531, Left=153, Top=54, InputMask=replicate('X',100)


  add object oObj_1_8 as cp_setobjprop with uid="AUWPJKIPCT",left=64, top=181, width=204,height=19,;
    caption='Pwd',;
   bGlobalFont=.t.,;
    cObj="w_IB___PWD",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 8635914

  add object oDESCRII_1_13 as StdField with uid="QFDELOBYZU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCRII", cQueryName = "DESCRII",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 232650550,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=265, Top=79, InputMask=replicate('X',50)

  add object oDESCRIO_1_14 as StdField with uid="AEOKZUYYRB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCRIO", cQueryName = "DESCRIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 35784906,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=265, Top=104, InputMask=replicate('X',50)

  add object oIBTIPEVI_1_15 as StdField with uid="JIVLZZHJZV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_IBTIPEVI", cQueryName = "IBTIPEVI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Tipo evento telefonata in ingresso",;
    HelpContextID = 163841231,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=153, Top=79, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", cZoomOnZoom="GSFA_ATE", oKey_1_1="TETIPEVE", oKey_1_2="this.w_IBTIPEVI"

  func oIBTIPEVI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oIBTIPEVI_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIBTIPEVI_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oIBTIPEVI_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSFA_ATE',"Tipi eventi",'gsfa1aib.TIPEVENT_VZM',this.parent.oContained
  endproc
  proc oIBTIPEVI_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSFA_ATE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TETIPEVE=this.parent.oContained.w_IBTIPEVI
     i_obj.ecpSave()
  endproc

  add object oIBTIPEVO_1_16 as StdField with uid="VOHSZWDGRX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_IBTIPEVO", cQueryName = "IBTIPEVO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Tipo evento telefonata in uscita",;
    HelpContextID = 163841237,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=153, Top=104, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", cZoomOnZoom="GSFA_ATE", oKey_1_1="TETIPEVE", oKey_1_2="this.w_IBTIPEVO"

  func oIBTIPEVO_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oIBTIPEVO_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIBTIPEVO_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oIBTIPEVO_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSFA_ATE',"Tipi eventi",'gsfa2aib.TIPEVENT_VZM',this.parent.oContained
  endproc
  proc oIBTIPEVO_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSFA_ATE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TETIPEVE=this.parent.oContained.w_IBTIPEVO
     i_obj.ecpSave()
  endproc


  add object oObj_1_21 as cp_askfile with uid="OBMZWDHYCM",left=664, top=6, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_IBNAMFIL",cExt="mdb",;
    nPag=1;
    , ToolTipText = "Premere per selezionare un file";
    , HelpContextID = 8876330

  add object oStr_1_3 as StdString with uid="BYDXNZYTYJ",Visible=.t., Left=17, Top=6,;
    Alignment=1, Width=132, Height=18,;
    Caption="Nome file mdb di Blues:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="SHLXHZSTCK",Visible=.t., Left=17, Top=31,;
    Alignment=1, Width=132, Height=18,;
    Caption="Codice utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="UNQCECHTHB",Visible=.t., Left=17, Top=56,;
    Alignment=1, Width=132, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="WQMJRTZUOS",Visible=.t., Left=15, Top=81,;
    Alignment=1, Width=134, Height=18,;
    Caption="Tipo evento ingresso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="BQRFWEPPIC",Visible=.t., Left=17, Top=106,;
    Alignment=1, Width=132, Height=18,;
    Caption="Tipo evento uscita:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_aib','BLUESCON','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IBCODICE=BLUESCON.IBCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsfa_aib
proc ChkEVENTO(obj, pParam)
  if pParam='I'
    i_bRes=obj.w_TIPODRV1='T'
  else
    i_bRes=obj.w_TIPODRV2='T'
  endif
  if not(i_bRes)
    do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
    if pParam='I'
      obj.w_IBTIPEVI = space(10)
      obj.w_DESCRII = space(50)
      obj.w_TIPDIR1 = space(10)
      obj.w_DRIVER1 = space(10)  
      obj.w_TIPODRV1 = space(1)
    else
      obj.w_IBTIPEVO = space(10)
      obj.w_DESCRIO = space(50)
      obj.w_TIPDIR2 = space(10)
      obj.w_DRIVER2 = space(10)  
      obj.w_TIPODRV2 = space(1)
    endif    
  endif
endproc
* --- Fine Area Manuale
