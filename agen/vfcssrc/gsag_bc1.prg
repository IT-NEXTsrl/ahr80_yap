* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bc1                                                        *
*              Controlli alla check form dei contratti di assistenza           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-17                                                      *
* Last revis.: 2016-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bc1",oParentObject,m.pParam)
return(i_retval)

define class tgsag_bc1 as StdBatch
  * --- Local variables
  pParam = space(1)
  w_RESOCON1 = space(0)
  w_ANNULLA = .f.
  w_MESBLOK = space(0)
  w_MODELLO = space(10)
  w_IMPIANTO = space(10)
  w_COMPONENTE = 0
  w_RINNOVO = 0
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_ResMsg = .NULL.
  w_ResMsg1 = .NULL.
  w_oPART = .NULL.
  w_OK = .f.
  w_MESS = space(0)
  w_OLD_COTIPDOC = space(5)
  w_OLD_COTIPATT = space(20)
  w_OLD_COGRUPAR = space(5)
  w_OLD_COCODPAG = space(5)
  w_OLD_COESCRIN = space(1)
  w_OLD_COPERCON = space(3)
  w_OLD_CORINCON = space(1)
  w_OLD_CONUMGIO = 0
  w_OLD_COCODCEN = space(15)
  w_OLD_COCOCOMM = space(15)
  w_OLD_COCODLIS = space(5)
  w_OLD_COCODCON = space(5)
  * --- WorkFile variables
  CON_TRAS_idx=0
  ELE_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli alla check form dei contratti di assistenza
    * --- Read from CON_TRAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CON_TRAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAS_idx,2],.t.,this.CON_TRAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COESCRIN,COPERCON,CORINCON,CONUMGIO,COCODCEN,COCOCOMM,COCODLIS,COCODCON"+;
        " from "+i_cTable+" CON_TRAS where ";
            +"COSERIAL = "+cp_ToStrODBC(this.oParentObject.w_COSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COESCRIN,COPERCON,CORINCON,CONUMGIO,COCODCEN,COCOCOMM,COCODLIS,COCODCON;
        from (i_cTable) where;
            COSERIAL = this.oParentObject.w_COSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OLD_COTIPDOC = NVL(cp_ToDate(_read_.COTIPDOC),cp_NullValue(_read_.COTIPDOC))
      this.w_OLD_COTIPATT = NVL(cp_ToDate(_read_.COTIPATT),cp_NullValue(_read_.COTIPATT))
      this.w_OLD_COGRUPAR = NVL(cp_ToDate(_read_.COGRUPAR),cp_NullValue(_read_.COGRUPAR))
      this.w_OLD_COCODPAG = NVL(cp_ToDate(_read_.COCODPAG),cp_NullValue(_read_.COCODPAG))
      this.w_OLD_COESCRIN = NVL(cp_ToDate(_read_.COESCRIN),cp_NullValue(_read_.COESCRIN))
      this.w_OLD_COPERCON = NVL(cp_ToDate(_read_.COPERCON),cp_NullValue(_read_.COPERCON))
      this.w_OLD_CORINCON = NVL(cp_ToDate(_read_.CORINCON),cp_NullValue(_read_.CORINCON))
      this.w_OLD_CONUMGIO = NVL(cp_ToDate(_read_.CONUMGIO),cp_NullValue(_read_.CONUMGIO))
      this.w_OLD_COCODCEN = NVL(cp_ToDate(_read_.COCODCEN),cp_NullValue(_read_.COCODCEN))
      this.w_OLD_COCOCOMM = NVL(cp_ToDate(_read_.COCOCOMM),cp_NullValue(_read_.COCOCOMM))
      this.w_OLD_COCODLIS = NVL(cp_ToDate(_read_.COCODLIS),cp_NullValue(_read_.COCODLIS))
      this.w_OLD_COCODCON = NVL(cp_ToDate(_read_.COCODCON),cp_NullValue(_read_.COCODCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Select from gsag_aca
    do vq_exec with 'gsag_aca',this,'_Curs_gsag_aca','',.f.,.t.
    if used('_Curs_gsag_aca')
      select _Curs_gsag_aca
      locate for 1=1
      do while not(eof())
      this.w_MODELLO = _Curs_gsag_aca.elcodmod
      this.w_IMPIANTO = _Curs_gsag_aca.elcodimp
      this.w_COMPONENTE = _Curs_gsag_aca.elcodcom
      this.w_RINNOVO = _Curs_gsag_aca.elrinnov
      this.w_DATINI = _Curs_gsag_aca.eldatini
      this.w_DATFIN = _Curs_gsag_aca.eldatfin
      do case
        case EMPTY(NVL(this.w_IMPIANTO,""))
          this.w_MESS = ah_MsgFormat("Attenzione! Impossibile confermare le modifiche. L'elemento con modello: %1 Numero rinnovo: %2 Data inizio validit�: %3", Alltrim(this.w_MODELLO),this.w_RINNOVO,DTOC(this.w_DATINI))
        case !EMPTY(NVL(this.w_IMPIANTO,""))
          if EMPTY(NVL(this.w_COMPONENTE,0))
            this.w_MESS = ah_MsgFormat("Attenzione! Impossibile confermare le modifiche. L'elemento con modello: %1 Impianto: %2 Numero rinnovo: %3 Data inizio validit�: %4", Alltrim(this.w_MODELLO),alltrim(this.w_IMPIANTO),this.w_RINNOVO,DTOC(this.w_DATINI))
          else
            this.w_MESS = ah_MsgFormat("Attenzione! Impossibile confermare le modifiche. L'elemento con modello: %1 Impianto: %2 Componente: %3 Numero rinnovo: %4 Data inizio validit�: %5", Alltrim(this.w_MODELLO),alltrim(this.w_IMPIANTO),this.w_COMPONENTE,this.w_RINNOVO,DTOC(this.w_DATINI))
          endif
        otherwise
          this.w_MESS = ah_MsgFormat("Attenzione! Impossibile confermare le modifiche. L'elemento con modello: %1 Impianto: %2 Componente: %3 Numero rinnovo: %4 Data inizio validit�: %5", Alltrim(this.w_MODELLO),alltrim(this.w_IMPIANTO),this.w_COMPONENTE,this.w_RINNOVO,DTOC(this.w_DATINI))
      endcase
      this.w_ResMsg.AddMsgPartNL(ah_msgformat("%1  Data fine validit�: %2 ha date validit� non comprese all'interno del periodo di validit� del contratto!",Alltrim(this.w_MESS),DTOC(this.w_DATFIN))+CHR(13)+CHR(10))     
        select _Curs_gsag_aca
        continue
      enddo
      use
    endif
    this.w_MESBLOK = this.w_ResMsg.ComposeMessage()
    * --- Controlla, e ne d� avviso, se risultano variati i seguenti dati:
    *     Causale,
    *     Tipo attivit�,
    *     Gruppo,
    *     Pagamento,
    *     Escludi rinnovo,
    *     Periodicit�,
    *     Rinnovo contratti,
    *     Rinnovo tacito,
    *     Numero preavviso disdetta
    * --- Read from CON_TRAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CON_TRAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAS_idx,2],.t.,this.CON_TRAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COESCRIN,COPERCON,CORINCON,CONUMGIO,COCODCEN,COCOCOMM,COCODLIS"+;
        " from "+i_cTable+" CON_TRAS where ";
            +"COSERIAL = "+cp_ToStrODBC(this.oParentObject.w_COSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COTIPDOC,COTIPATT,COGRUPAR,COCODPAG,COESCRIN,COPERCON,CORINCON,CONUMGIO,COCODCEN,COCOCOMM,COCODLIS;
        from (i_cTable) where;
            COSERIAL = this.oParentObject.w_COSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OLD_COTIPDOC = NVL(cp_ToDate(_read_.COTIPDOC),cp_NullValue(_read_.COTIPDOC))
      this.w_OLD_COTIPATT = NVL(cp_ToDate(_read_.COTIPATT),cp_NullValue(_read_.COTIPATT))
      this.w_OLD_COGRUPAR = NVL(cp_ToDate(_read_.COGRUPAR),cp_NullValue(_read_.COGRUPAR))
      this.w_OLD_COCODPAG = NVL(cp_ToDate(_read_.COCODPAG),cp_NullValue(_read_.COCODPAG))
      this.w_OLD_COESCRIN = NVL(cp_ToDate(_read_.COESCRIN),cp_NullValue(_read_.COESCRIN))
      this.w_OLD_COPERCON = NVL(cp_ToDate(_read_.COPERCON),cp_NullValue(_read_.COPERCON))
      this.w_OLD_CORINCON = NVL(cp_ToDate(_read_.CORINCON),cp_NullValue(_read_.CORINCON))
      this.w_OLD_CONUMGIO = NVL(cp_ToDate(_read_.CONUMGIO),cp_NullValue(_read_.CONUMGIO))
      this.w_OLD_COCODCEN = NVL(cp_ToDate(_read_.COCODCEN),cp_NullValue(_read_.COCODCEN))
      this.w_OLD_COCOCOMM = NVL(cp_ToDate(_read_.COCOCOMM),cp_NullValue(_read_.COCOCOMM))
      this.w_OLD_COCODLIS = NVL(cp_ToDate(_read_.COCODLIS),cp_NullValue(_read_.COCODLIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.oParentObject.w_AGGIORNAELEMENTI = .F.
    if this.oParentObject.w_COTIPDOC <> this.w_OLD_COTIPDOC
      * --- Controlla se � stata modificata la causale
      this.w_oPART = this.w_ResMsg1.addmsgpartNL("La causale � stata modificata da: %1 a: %2")
      this.w_oPART.addParam(IIF( EMPTY( Alltrim(this.w_OLD_COTIPDOC) ), "Nessun valore", Alltrim(this.w_OLD_COTIPDOC) ))     
      this.w_oPART.addParam(IIF( EMPTY( alltrim(this.oParentObject.w_COTIPDOC) ), "Nessun valore", Alltrim(this.oParentObject.w_COTIPDOC) ))     
      this.oParentObject.w_AGGIORNAELEMENTI = .T.
    endif
    if this.oParentObject.w_COTIPATT <> this.w_OLD_COTIPATT
      * --- Controlla se � stato modificato il tipo attivit�
      this.w_oPART = this.w_ResMsg1.addmsgpartNL("Il tipo attivit� � stato modificato da: %1 a: %2")
      this.w_oPART.addParam(IIF( EMPTY( Alltrim(this.w_OLD_COTIPATT) ), "Nessun valore", Alltrim(this.w_OLD_COTIPATT) ))     
      this.w_oPART.addParam(IIF( EMPTY( alltrim(this.oParentObject.w_COTIPATT) ), "Nessun valore", Alltrim(this.oParentObject.w_COTIPATT) ))     
      this.oParentObject.w_AGGIORNAELEMENTI = .T.
    endif
    if this.oParentObject.w_COGRUPAR <> this.w_OLD_COGRUPAR
      * --- Controlla se � stato modificato il gruppo
      this.w_oPART = this.w_ResMsg1.addmsgpartNL("Il gruppo � stato modificato da: %1 a: %2")
      this.w_oPART.addParam(IIF( EMPTY( Alltrim(this.w_OLD_COGRUPAR) ), "Nessun valore", Alltrim(this.w_OLD_COGRUPAR) ))     
      this.w_oPART.addParam(IIF( EMPTY( alltrim(this.oParentObject.w_COGRUPAR) ), "Nessun valore", Alltrim(this.oParentObject.w_COGRUPAR) ))     
      this.oParentObject.w_AGGIORNAELEMENTI = .T.
    endif
    if this.oParentObject.w_COCODPAG <> this.w_OLD_COCODPAG
      * --- Controlla se � stato modificato il codice del pagamento
      this.w_oPART = this.w_ResMsg1.addmsgpartNL("Il codice del pagamento � stato modificato da: %1 a: %2")
      this.w_oPART.addParam(IIF( EMPTY( Alltrim(this.w_OLD_COCODPAG) ), "Nessun valore", Alltrim(this.w_OLD_COCODPAG) ))     
      this.w_oPART.addParam(IIF( EMPTY( alltrim(this.oParentObject.w_COCODPAG) ), "Nessun valore", Alltrim(this.oParentObject.w_COCODPAG) ))     
      this.oParentObject.w_AGGIORNAELEMENTI = .T.
    endif
    if LEFT( NVL( this.oParentObject.w_COESCRIN, " " ) + " ", 1) <> LEFT( NVL( this.w_OLD_COESCRIN, " " ) + " ", 1 )
      * --- Controlla se � stato modificata la combo di esclusione rinnovo
      this.w_oPART = this.w_ResMsg1.addmsgpartNL("L'esclusione del rinnovo � stata modificata da: %1 a: %2")
      this.w_oPART.addParam(ICASE(this.w_OLD_COESCRIN="M","Da modello",this.w_OLD_COESCRIN="S","S�",this.w_OLD_COESCRIN="N","No","Nessun valore"))     
      this.w_oPART.addParam(ICASE(this.oParentObject.w_COESCRIN="M","Da modello",this.oParentObject.w_COESCRIN="S","S�",this.oParentObject.w_COESCRIN="N","No","Nessun valore"))     
      this.oParentObject.w_AGGIORNAELEMENTI = .T.
    endif
    if this.oParentObject.w_COPERCON <> this.w_OLD_COPERCON
      * --- Controlla se � stato modificata la periodicit� del rinnovo contratti
      this.w_oPART = this.w_ResMsg1.addmsgpartNL("La periodicit� del rinnovo contratti � stata modificata da: %1 a: %2")
      this.w_oPART.addParam(IIF( EMPTY( ALLTRIM(this.w_OLD_COPERCON) ), "Nessun valore", Alltrim(this.w_OLD_COPERCON) ))     
      this.w_oPART.addParam(IIF( EMPTY( ALLTRIM(this.oParentObject.w_COPERCON) ), "Nessun valore", Alltrim(this.oParentObject.w_COPERCON) ))     
      this.oParentObject.w_AGGIORNAELEMENTI = .T.
    endif
    if LEFT( NVL( this.oParentObject.w_CORINCON , " " ) + " ",1 ) <> LEFT( NVL( this.w_OLD_CORINCON, " " ) + " ",1 )
      * --- Controlla se � stato modificata la combo di rinnovo tacito
      this.w_oPART = this.w_ResMsg1.addmsgpartNL("Il rinnovo tacito � stato modificato da: %1 a: %2")
      this.w_oPART.addParam(ICASE(this.w_OLD_CORINCON="M","Da modello",this.w_OLD_CORINCON="S","S�",this.w_OLD_CORINCON="N","No","Nessun valore"))     
      this.w_oPART.addParam(ICASE(this.oParentObject.w_CORINCON="M","Da modello",this.oParentObject.w_CORINCON="S","S�",this.oParentObject.w_CORINCON="N","No","Nessun valore"))     
      this.oParentObject.w_AGGIORNAELEMENTI = .T.
    endif
    if this.oParentObject.w_CONUMGIO <> this.w_OLD_CONUMGIO
      * --- Controlla se � stato modificato il numero giorni di preavviso disdetta
      this.w_oPART = this.w_ResMsg1.addmsgpartNL("Il numero giorni di preavviso disdetta � stato modificato da: %1 a: %2")
      this.w_oPART.addParam(IIF( EMPTY( ALLTRIM(STR(this.w_OLD_CONUMGIO)) ), "Nessun valore", Alltrim(STR(this.w_OLD_CONUMGIO)) ))     
      this.w_oPART.addParam(IIF( EMPTY( ALLTRIM(STR(this.oParentObject.w_CONUMGIO)) ), "Nessun valore", Alltrim(STR(this.oParentObject.w_CONUMGIO)) ))     
      this.oParentObject.w_AGGIORNAELEMENTI = .T.
    endif
    if this.oParentObject.w_COCODCEN <> this.w_OLD_COCODCEN
      * --- Controlla se � stato modificato il centro di costo
      this.w_oPART = this.w_ResMsg1.addmsgpartNL("Il centro di ricavo � stato modificato da: %1 a: %2")
      this.w_oPART.addParam(IIF( EMPTY( ALLTRIM(this.w_OLD_COCODCEN) ), "Nessun valore", Alltrim(this.w_OLD_COCODCEN) ))     
      this.w_oPART.addParam(IIF( EMPTY( ALLTRIM(this.oParentObject.w_COCODCEN) ), "Nessun valore", Alltrim(this.oParentObject.w_COCODCEN) ))     
      this.oParentObject.w_AGGIORNAELEMENTI = .T.
    endif
    if this.oParentObject.w_COCOCOMM <> this.w_OLD_COCOCOMM
      * --- Controlla se � stata modificata la commessa
      this.w_oPART = this.w_ResMsg1.addmsgpartNL("La commessa � stata modificata da: %1 a: %2")
      this.w_oPART.addParam(IIF( EMPTY( ALLTRIM(this.w_OLD_COCOCOMM) ), "Nessun valore", Alltrim(this.w_OLD_COCOCOMM) ))     
      this.w_oPART.addParam(IIF( EMPTY( ALLTRIM(this.oParentObject.w_COCOCOMM) ), "Nessun valore", Alltrim(this.oParentObject.w_COCOCOMM) ))     
      this.oParentObject.w_AGGIORNAELEMENTI = .T.
    endif
    if this.oParentObject.w_COCODLIS <> this.w_OLD_COCODLIS
      * --- Controlla se � stato modificato il listino
      this.w_oPART = this.w_ResMsg1.addmsgpartNL("Il listino � stato modificato da: %1 a: %2")
      this.w_oPART.addParam(IIF( EMPTY( Alltrim(this.w_OLD_COCODLIS) ), "Nessun valore", Alltrim(this.w_OLD_COCODLIS) ))     
      this.w_oPART.addParam(IIF( EMPTY( alltrim(this.oParentObject.w_COCODLIS) ), "Nessun valore", Alltrim(this.oParentObject.w_COCODLIS) ))     
      this.oParentObject.w_AGGIORNAELEMENTI = .T.
    endif
    this.w_RESOCON1 = this.w_ResMsg1.ComposeMessage()
    if NOT EMPTY(this.w_MESBLOK) OR NOT EMPTY(this.w_RESOCON1)
      do GSUT_KLG with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_RESCHK = IIF(Not this.w_ANNULLA, .F., .T.)
    endif
    if this.oParentObject.w_COCODCON <> this.w_OLD_COCODCON
      * --- Controlla se � stato modificato il cliente
      vq_exec("..\AGEN\EXE\QUERY\GSAG_BC1.VQR",this,"__tmp__")
      if RECCOUNT("__tmp__")>0
        this.w_OK = ah_YesNo("Attenzione! Esistono uno o pi� impianti legati al contratto che si sta modificando! Procedo ugualmente?")
        this.oParentObject.w_OKSTAMPA = .T.
        if !this.w_OK
          this.oParentObject.w_RESCHK = .F.
        endif
      endif
    endif
    USE IN SELECT("__tmp__")
    this.w_ResMsg = .NULL.
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    this.w_ResMsg=createobject("Ah_Message")
    this.w_ResMsg1=createobject("Ah_Message")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CON_TRAS'
    this.cWorkTables[2]='ELE_CONT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_gsag_aca')
      use in _Curs_gsag_aca
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
