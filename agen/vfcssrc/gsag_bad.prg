* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bad                                                        *
*              Aggiornamento data in attivita da iter                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-03-07                                                      *
* Last revis.: 2016-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Param
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bad",oParentObject,m.Param)
return(i_retval)

define class tgsag_bad as StdBatch
  * --- Local variables
  Param = space(1)
  w_ATSTATUS = space(1)
  w_ATTIPRIS = space(1)
  w_ObjMDA = .NULL.
  w_OBJECT = .NULL.
  w_MESS = space(100)
  w_DATA_INI = ctot("")
  w_CAITER = space(20)
  w_DESC_ATT = space(254)
  w_DES_PRAT = space(100)
  w_ATSERIALE = space(20)
  w_Mask = .NULL.
  w_CursorZoom = space(1)
  w_DATA_AGG = ctot("")
  w_TDSERIAL = space(20)
  w_OKATT = .f.
  w_MESSERR = space(100)
  w_CODPRA_ATT = space(15)
  w_DECTOT = 0
  w_DECUNI = 0
  w_DataCambiata = .f.
  w_DATA_INI = ctot("")
  w_CAITER = space(20)
  w_DESC_ATT = space(254)
  w_DES_PRAT = space(100)
  w_ResMsg = .NULL.
  w_RESOCON1 = space(0)
  w_ANNULLA = .f.
  w_MEMO = space(0)
  w_TIPRIC = space(1)
  w_NUMOCC = 0
  w_SERIAL = space(20)
  w_RIFSER = space(20)
  w_InviaMail = space(1)
  w_Mask = .NULL.
  w_CursorZoom = space(1)
  w_Durata = 0
  w_DatIniAttivita = ctot("")
  w_DatFinAttivita = ctot("")
  w_ATFLNOTI = space(1)
  w_STATUS = space(1)
  w_ATCAUATT = space(5)
  w_CODPRA = space(15)
  w_ATRIFMOV = space(10)
  w_OKSPOSTA = .f.
  w_SERDOC = space(10)
  w_RETVAL = space(254)
  w_CODSES = space(15)
  w_CAUDOC = space(5)
  w_FLRESTO = .f.
  w_FLINTE = space(1)
  w_CAUNOT = space(5)
  CURSORE = space(10)
  w_RETVAL = space(100)
  w_ResMsg = .NULL.
  w_OKGEN = .f.
  w_RESOCON1 = space(0)
  w_ANNULLA = .f.
  w_MESBLOK = space(0)
  w_FLRICO = space(1)
  w_OKAGG = .f.
  w_LSERIAL = space(20)
  w_OBJ = .NULL.
  w_CHKFOR = space(1)
  w_CHKINI = space(1)
  w_NOTEERRORE = space(254)
  w_DATACAL = ctot("")
  w_OGGETTO = space(80)
  w_ATDATINI = ctot("")
  w_Mask = .NULL.
  w_CAITER = space(20)
  w_PACHKFES = space(1)
  w_CursorZoom = space(1)
  w_TIPO = space(1)
  w_RIFSER = space(20)
  w_NUMGIO = 0
  w_STATO = space(1)
  w_DAT_RIF = ctot("")
  w_AGGDAT = .f.
  w_ODATINI = ctot("")
  DATAFIN = ctod("  /  /  ")
  OREFIN = space(2)
  MINFIN = space(2)
  w_ATDATFIN = ctot("")
  w_GIORNO = space(15)
  w_ATT_SERIAL = space(15)
  w_PAFLESAT = space(1)
  w_DESPRA = space(100)
  w_DATASYS = ctod("  /  /  ")
  * --- WorkFile variables
  OFF_ATTI_idx=0
  OFF_PART_idx=0
  OFFDATTI_idx=0
  COM_ATTI_idx=0
  MOVICOST_idx=0
  CDC_MANU_idx=0
  RIS_ESTR_idx=0
  CAUMATTI_idx=0
  TIP_DOCU_idx=0
  ATT_DCOL_idx=0
  PAR_PRAT_idx=0
  TMPVEND1_idx=0
  ASS_ATEV_idx=0
  ANEVENTI_idx=0
  PAR_AGEN_idx=0
  CAN_TIER_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- PROPONE MASCHERA CON ATTIVITA' (DA ITER) AVENTI DATA VARIATA
    *                                                                               (batch lanciato da GSAG_AAT, GSAG_KAD e GSAG_KEL)
    * --- Param = A  :  Aggiornamento Data iniziale nelle attivit� selezionate nello zoom
    *     Param = B :  Aggiornamento commessa nelle prestazioni
    *     Param = C  :  Controlla che sullo zoom non sia vuota la data ricalcolata
    *     Param = D :  Cancella le attivit� selezionate
    *     Param = E  :  Da Gestione attivit� (all'F5 - cancellazione)
    *     Param = F :  Aggiornamento centro di ricavo acquisti nelle prestazioni
    *     Param = G  : Aggiornamento centro di costo nelle prestazioni
    *     Param = H  :   Aggiornamento attivit� acquisti di riga AHE
    *     Param = I  : Aggiornamento attivit� vendite di riga AHE
    *     Param = K  :  Da Gestione attivit� (bottone attivit� collegate)
    *     Param = M  :  Da Gestione attivit� (all'F10 in variazione)
    *     Param = N :  Azzera dettaglio componenti al variare del nominativo
    *     Param = R :  Aggiornamento commessa acquisti nelle prestazioni
    *     Param = S :   Seleziona le righe delle attivit� 
    *     Param = T :  Elimina in Dettaglio attivit� le prestazioni con importo a zero
    *     Param = U :  Aggiorna data spostamento
    *     Param = V :  Visualizza attivit� selezionata
    *     Param = Z :  Aggiorna campo ATCAITER in attivit� collegata di origine
    *     Param = L : Lancia stampa singola attivit� (all'F10 in Gestione attivit�)
    *     Param = P : Lancia stampa singola attivit� (all'F2 in Gestione attivit�)
    * --- Dichiarazione e inizializzazione w_DataCambiata, necessario per GSAG_BAP
    this.w_DataCambiata = .T.
    do case
      case this.Param = "M"
        * --- DOPO AVER SALVATO (F10) UN' ATTIVITA' IN VARIAZIONE
        * --- Se l'attivit� � stata generata da un iter, non � ricorrente e se � stata modificata la relativa data iniziale
        if not empty(this.oParentObject.w_ATCAITER) and this.oParentObject.w_DATINI_CHANGED="S" AND this.oParentObject.w_ATFLRICO<>"S"
          this.w_SERIAL = this.oParentObject.w_ATSERIAL
          this.w_CAITER = this.oParentObject.w_ATCAITER
          this.w_DATA_INI = This.oparentobject.w_ATDATINI
          this.w_DESC_ATT = this.oParentObject.w_ATOGGETT
          this.w_DES_PRAT = this.oParentObject.w_DESCAN
          this.oParentObject.w_USCITA = "S"
          do GSAG_KAD with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_ATDATINI = This.oparentobject.w_ATDATINI
        this.w_ATDATFIN = This.oparentobject.w_ATDATFIN
        this.w_ATSTATUS = This.oparentobject.w_ATSTATUS
        if not empty(this.oParentObject.w_ATCAITER) AND this.oParentObject.w_ATFLRICO="S" and this.w_ATSTATUS=this.oParentObject.w_OLDSTATUS and this.w_ATDATINI=this.oParentObject.w_OLDDATINI and this.w_ATDATFIN=this.oParentObject.w_OLDDATFIN and this.w_ATSTATUS $ "T-D"
          * --- Se ho modificato lo stato o le date non devo rigenerare le attivit� ricorrenti collegate
          * --- E' stata cambiata un'attivit� ricorrente: si verifica se esistono attivit� con data successiva e con stato inferiore a "In corso".
          *     Nel caso in cui si siano attivit� di questo tipo, queste devono essere cancellate e ricreate con le stesse caratteristiche della prima.
          *     Si crea - nascosta - l'attivit� principale e si generano le successive utilizzando il cursore "ATT_RICORR"
          vq_exec("..\AGEN\EXE\QUERY\GSAG2BAD.VQR",this.oParentObject,"ATT_RICORR")
          if RECCOUNT() > 0 AND Ah_YesNo("Attenzione! Eliminare e ricreare tutte le attivit� ricorrenti successive per applicare le modifiche effettuate?%0%0Verranno eliminate e ricreate solo le attivit� con stato 'Provvisoria' o 'Da Svolgere' mantenendo data/ora delle singole attivit�")
            * --- Verranno eliminate e ricreate solo le attivit� con stato "Provvisoria" o "Da Svolgere" mantenendo Data/Ora delle singole attivit�
            * --- Deve modificare l'attivit� il cui seriale � passato come parametro
            this.w_TIPRIC = "N"
            this.w_RESOCON1 = ""
            this.w_RIFSER = this.oParentObject.w_ATSERIAL
            if this.w_ATFLNOTI <>"N"
              this.w_InviaMail = IIF(Ah_YesNo("Attenzione: inviare gli avvisi per ogni nuova occorrenza dell'attivit�?"),"S","N")
            endif
             
 SELECT("ATT_RICORR") 
 Go top 
 Scan
            this.w_SERIAL = ATSERIAL
            this.w_RETVAL = GSAG_BDA(this, this.w_SERIAL)
            if Empty(this.w_RETVAL)
              GSAG_BAR(this,ATDATINI,ATDATFIN,this.w_InviaMail)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if Not Empty(this.w_SERIAL) 
                * --- Aggiorno riferimento su eventuali attivita collegate
                * --- Write into OFF_ATTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
                if i_nConn<>0
                  local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
                  declare i_aIndex[1]
                  i_cQueryTable=cp_getTempTableName(i_nConn)
                  i_aIndex(1)="ATSERIAL"
                  do vq_exec with 'gsag3bad',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)	
                  i_cDB=cp_GetDatabaseType(i_nConn)
                  do case
                  case i_cDB="SQLServer"
                    i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ATRIFSER ="+cp_NullLink(cp_ToStrODBC(this.w_RIFSER),'OFF_ATTI','ATRIFSER');
                      +i_ccchkf;
                      +" from "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
                  case i_cDB="MySQL"
                    i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI, "+i_cQueryTable+" _t2 set ";
                  +"OFF_ATTI.ATRIFSER ="+cp_NullLink(cp_ToStrODBC(this.w_RIFSER),'OFF_ATTI','ATRIFSER');
                      +Iif(Empty(i_ccchkf),"",",OFF_ATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                      +" where "+i_cWhere)
                  case i_cDB="Oracle"
                    i_cWhere="OFF_ATTI.ATSERIAL = t2.ATSERIAL";
                    
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set (";
                      +"ATRIFSER";
                      +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                      +cp_NullLink(cp_ToStrODBC(this.w_RIFSER),'OFF_ATTI','ATRIFSER')+"";
                      +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                      +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
                  case i_cDB="PostgreSQL"
                    i_cWhere="OFF_ATTI.ATSERIAL = _t2.ATSERIAL";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFF_ATTI set ";
                  +"ATRIFSER ="+cp_NullLink(cp_ToStrODBC(this.w_RIFSER),'OFF_ATTI','ATRIFSER');
                      +i_ccchkf;
                      +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
                  otherwise
                    i_cWhere=i_cTable+".ATSERIAL = "+i_cQueryTable+".ATSERIAL";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ATRIFSER ="+cp_NullLink(cp_ToStrODBC(this.w_RIFSER),'OFF_ATTI','ATRIFSER');
                      +i_ccchkf;
                      +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
                  endcase
                  cp_DropTempTable(i_nConn,i_cQueryTable)
                else
                  error "not yet implemented!"
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              SELECT("ATT_RICORR")
              this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Modificata attivit� %1 del %2 ", ALLTRIM(ATOGGETT),ttoc(ATDATINI)))     
            else
              this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Attivit� %1 del %2  non eliminata:%0%3", ALLTRIM(ATOGGETT),ttoc(ATDATINI),Alltrim(this.w_RETVAL)))     
              * --- Il prossimo atrifser in questo caso dovr�  essere il seriale dell'attivit� non eliminata
              this.w_RIFSER = ATSERIAL
            endif
            this.w_RESOCON1 = this.w_RESOCON1 + this.w_ResMsg.ComposeMessage()
             
 Endscan
          else
            * --- L'attivit� deve chiedere se mandare o meno la mail
            this.oParentObject.w_MailAttivitaRicorrente = "S"
          endif
          USE IN SELECT("ATT_RICORR")
          if Not Empty(this.w_RESOCON1)
            do GSUT_KLG with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_ResMsg = .null.
        endif
      case this.Param = "K"
        * --- BOTTONE 'ATTIVITA' COLLEGATE'  IN GESTIONE ATTIVITA'
        this.w_SERIAL = this.oParentObject.w_ATSERIAL
        this.w_CAITER = this.oParentObject.w_ATCAITER
        this.w_DATA_INI = This.oparentobject.w_ATDATINI
        this.w_DESC_ATT = this.oParentObject.w_ATOGGETT
        this.w_DES_PRAT = this.oParentObject.w_DESCAN
        do GSAG_KAC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Param = "A"
        * --- AGGIORNAMENTO DATA INIZIALE
        this.w_Mask = this.oParentObject
        * --- Zoom delle Attivit�
        this.w_CursorZoom = this.w_Mask.w_AGKAD_ZOOM.ccursor
        SELECT (this.w_CursorZoom)
        * --- Cicla sul cursore dello zoom relativo alle Attivit� (da iter) con data iniziale variata ed esamina i records selezionati
        scan for XCHK=1
        this.w_ATSERIALE = ATSERIAL
        this.w_DATA_AGG = DATAAGG 
        * --- Prima di tutto leggiamo data iniziale e data finale dell'attivit�
        this.w_OKSPOSTA = .T.
        * --- Read from OFF_ATTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATDATINI,ATDATFIN,ATFLNOTI,ATSTATUS,ATCAUATT,ATCODPRA,ATRIFMOV"+;
            " from "+i_cTable+" OFF_ATTI where ";
                +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIALE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATDATINI,ATDATFIN,ATFLNOTI,ATSTATUS,ATCAUATT,ATCODPRA,ATRIFMOV;
            from (i_cTable) where;
                ATSERIAL = this.w_ATSERIALE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DatIniAttivita = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
          this.w_DatFinAttivita = NVL((_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
          this.w_ATFLNOTI = NVL(cp_ToDate(_read_.ATFLNOTI),cp_NullValue(_read_.ATFLNOTI))
          this.w_STATUS = NVL(cp_ToDate(_read_.ATSTATUS),cp_NullValue(_read_.ATSTATUS))
          this.w_ATCAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
          this.w_CODPRA = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
          this.w_ATRIFMOV = NVL(cp_ToDate(_read_.ATRIFMOV),cp_NullValue(_read_.ATRIFMOV))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !this.w_DatIniAttivita=this.w_DATA_AGG
          * --- Calcoliamo la data durata e la sommiamo alla data iniziale per calcolare la data finale
          this.w_Durata = this.w_DatFinAttivita - this.w_DatIniAttivita
          this.w_DatFinAttivita = this.w_DATA_AGG + this.w_Durata
          * --- Aggiorna le date dell'attivit�
          if this.w_STATUS= "P" 
            * --- Generazione documenti interni dell'attivit� collegata
            * --- Try
            local bErr_02A68AE8
            bErr_02A68AE8=bTrsErr
            this.Try_02A68AE8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              Ah_ErrorMsg("%1",,"",this.w_RETVAL)
              this.w_OKSPOSTA = .F.
            endif
            bTrsErr=bTrsErr or bErr_02A68AE8
            * --- End
            if this.w_OKSPOSTA
              * --- Aggiorno movimento di analitica associato
              if ! Isahe() and Not Empty(this.w_ATRIFMOV) 
                * --- Try
                local bErr_04025E48
                bErr_04025E48=bTrsErr
                this.Try_04025E48()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  Ah_ErrorMsg("Attenzione errore nell'aggiornamento movimento di analitica associato impossibile spostare l'attivit�")
                endif
                bTrsErr=bTrsErr or bErr_04025E48
                * --- End
              endif
              * --- Genero documento collegato
              * --- Write into OFF_ATTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_DATA_AGG),'OFF_ATTI','ATDATINI');
                +",ATDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_DatFinAttivita),'OFF_ATTI','ATDATFIN');
                +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
                    +i_ccchkf ;
                +" where ";
                    +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIALE);
                       )
              else
                update (i_cTable) set;
                    ATDATINI = this.w_DATA_AGG;
                    ,ATDATFIN = this.w_DatFinAttivita;
                    ,ATDATOUT = DATETIME();
                    &i_ccchkf. ;
                 where;
                    ATSERIAL = this.w_ATSERIALE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Read from RIS_ESTR
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "SRCODSES"+;
                  " from "+i_cTable+" RIS_ESTR where ";
                      +"SRCODPRA = "+cp_ToStrODBC(this.w_CODPRA);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  SRCODSES;
                  from (i_cTable) where;
                      SRCODPRA = this.w_CODPRA;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODSES = NVL(cp_ToDate(_read_.SRCODSES),cp_NullValue(_read_.SRCODSES))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if Isalt() and Not Empty(this.w_CODSES)
                * --- Fatturazione multipla prevista nella pratica
                this.w_FLRESTO = .t.
                * --- Read from CAUMATTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CAUMATTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CACAUDOC"+;
                    " from "+i_cTable+" CAUMATTI where ";
                        +"CACODICE = "+cp_ToStrODBC(this.w_ATCAUATT);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CACAUDOC;
                    from (i_cTable) where;
                        CACODICE = this.w_ATCAUATT;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CAUDOC = NVL(cp_ToDate(_read_.CACAUDOC),cp_NullValue(_read_.CACAUDOC))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Read from TIP_DOCU
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "TDFLINTE"+;
                    " from "+i_cTable+" TIP_DOCU where ";
                        +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    TDFLINTE;
                    from (i_cTable) where;
                        TDTIPDOC = this.w_CAUDOC;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_FLINTE <> "N" and Not Empty(this.w_CODPRA) 
                  * --- Select from RIS_ESTR
                  i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select SRCODPRA,SRCODSES,SRPARAME  from "+i_cTable+" RIS_ESTR ";
                        +" where SRCODPRA="+cp_ToStrODBC(this.w_CODPRA)+"";
                        +" order by SRPARAME Desc";
                         ,"_Curs_RIS_ESTR")
                  else
                    select SRCODPRA,SRCODSES,SRPARAME from (i_cTable);
                     where SRCODPRA=this.w_CODPRA;
                     order by SRPARAME Desc;
                      into cursor _Curs_RIS_ESTR
                  endif
                  if used('_Curs_RIS_ESTR')
                    select _Curs_RIS_ESTR
                    locate for 1=1
                    do while not(eof())
                    this.w_RETVAL = GSAG_BGE(.null.,this.w_ATSERIALE,space(10),_Curs_RIS_ESTR.SRCODSES,_Curs_RIS_ESTR.SRPARAME,this.w_FLRESTO,"R", .F.)
                    this.w_FLRESTO = .f.
                    if Not Empty(this.w_RETVAL)
                      exit
                    endif
                      select _Curs_RIS_ESTR
                      continue
                    enddo
                    use
                  endif
                endif
                if Empty(this.w_RETVAL)
                  this.w_RETVAL = GSAG_BGE(this, this.w_ATSERIALE,SPACE(10),Space(15),0,.f.,"N", .F.)
                endif
              else
                this.w_RETVAL = GSAG_BGE(this, this.w_ATSERIALE,SPACE(10),Space(15),0,.f.,"D", .F.)
                if Isalt()
                  * --- Read from PAR_PRAT
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PAR_PRAT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PAR_PRAT_idx,2],.t.,this.PAR_PRAT_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "PACAUNOT"+;
                      " from "+i_cTable+" PAR_PRAT where ";
                          +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      PACAUNOT;
                      from (i_cTable) where;
                          PACODAZI = i_codazi;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_CAUNOT = NVL(cp_ToDate(_read_.PACAUNOT),cp_NullValue(_read_.PACAUNOT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if Not Empty(this.w_CAUNOT) and Empty(this.w_RETVAL)
                    * --- Genero documento pre nota spese
                    this.w_RETVAL = GSAG_BGE(this, this.w_ATSERIALE,SPACE(10),Space(15),0,.f.,"N", .F.)
                  endif
                endif
              endif
            endif
          else
            * --- Write into OFF_ATTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"ATDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_DATA_AGG),'OFF_ATTI','ATDATINI');
              +",ATDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_DatFinAttivita),'OFF_ATTI','ATDATFIN');
              +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
                  +i_ccchkf ;
              +" where ";
                  +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIALE);
                     )
            else
              update (i_cTable) set;
                  ATDATINI = this.w_DATA_AGG;
                  ,ATDATFIN = this.w_DatFinAttivita;
                  ,ATDATOUT = DATETIME();
                  &i_ccchkf. ;
               where;
                  ATSERIAL = this.w_ATSERIALE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Deve inviare la mail: ai fini di GSAG_BAP servono solo il check "Data cambiata" ed il seriale.
          GSAG_BAP(this,"M", this.w_ATSERIALE)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        SELECT (this.w_CursorZoom)
        endscan
      case this.Param = "S"
        * --- SELEZIONA RIGHE ATTIVITA'
        this.CURSORE = this.oParentObject.w_AGKAD_ZOOM.cCursor
        UPDATE (this.CURSORE) SET XCHK = 1
        select(this.CURSORE)
        this.oParentObject.w_SELROW = reccount()
      case this.Param = "C"
        * --- CONTROLLO DATA RICALCOLATA SULLO ZOOM
        this.w_OKGEN = .T.
        this.w_Mask = this.oParentObject
        * --- Zoom delle Attivit�
        this.w_CursorZoom = this.w_Mask.w_AGKAD_ZOOM.ccursor
        SELECT (this.w_CursorZoom)
        * --- Cicla sul cursore dello zoom relativo alle Attivit� (da iter) e controlla la data ricalcolata di quelle selezionate
        this.w_MESBLOK = ""
        this.w_RESOCON1 = ""
        scan for XCHK=1
        this.w_RETVAL = ""
        this.w_Durata = ATDATFIN - ATDATINI
        this.w_DatFinAttivita = DATAAGG + this.w_Durata
        if empty(DATAAGG)
          this.w_RETVAL = Ah_msgFormat("data ricalcolata non valorizzata!")
          this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Attenzione, impossibile spostare attivit� collegata  %1 del %3 ,%0%2", Alltrim(ATOGGETT),this.w_RETVAL,TTOC(ATDATINI)))     
          if this.w_OKGEN
            this.w_OKGEN = .F.
          endif
        else
          if 1=0
            * --- Per il momento non previsto per uniformita con la gestione Nuove attivit� collegate
            this.oParentObject.w_RESCHK = ChkDatCal(ATSERIAL,This,DATAAGG,this.w_DatFinAttivita,.t.)
            if Not Empty(this.w_RETVAL)
              * --- Registro problema in spostamento
              this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Attenzione, impossibile spostare attivit� collegata  %1 del %3 ,%0%2", Alltrim(ATOGGETT),this.w_RETVAL,TTOC(ATDATINI)))     
              if this.w_OKGEN
                this.w_OKGEN = .F.
              endif
            endif
          endif
        endif
        this.w_MESBLOK = this.w_MESBLOK+this.w_ResMsg.ComposeMessage()
        this.w_RESOCON1 = this.w_RESOCON1+this.w_ResMsg.ComposeMessage()
        endscan
        if !this.w_OKGEN
          do GSUT_KLG with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.oParentObject.w_RESCHK = -1
        endif
      case this.Param = "E"
        * --- DOPO AVER ELIMINATO UN' ATTIVITA' 
        * --- Try
        local bErr_0403D018
        bErr_0403D018=bTrsErr
        this.Try_0403D018()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0403D018
        * --- End
        * --- Se l'attivit� � stata generata da un iter
        this.w_FLRICO = this.oParentObject.w_ATFLRICO
        if not empty(this.oParentObject.w_ATCAITER)
          if Ah_yesno("Vuoi vedere le attivit� collegate?")
            this.w_SERIAL = this.oParentObject.w_ATSERIAL
            this.w_CAITER = this.oParentObject.w_ATCAITER
            this.w_DATA_INI = This.oparentobject.w_ATDATINI
            this.w_DESC_ATT = this.oParentObject.w_ATOGGETT
            this.w_DES_PRAT = this.oParentObject.w_DESCAN
            do GSAG_KEL with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        * --- Se l'attivit� � associata ad una pratica
        if ISALT() AND NOT EMPTY(this.oParentObject.w_ATCODPRA)
          this.w_CODPRA_ATT = this.oParentObject.w_ATCODPRA
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.Param = "D"
        * --- ELIMINAZIONE DELLE ATTIVITA' E RELATIVI DATI ASSOCIATI
        this.w_RESOCON1 = ""
        this.w_Mask = this.oParentObject
        * --- Zoom delle Attivit�
        this.w_CursorZoom = this.w_Mask.w_AGKEL_ZOOM.ccursor
        SELECT (this.w_CursorZoom)
        * --- Cicla sul cursore dello zoom relativo alle Attivit� (da iter) ed elimina quelle selezionate
         
 scan for XCHK=1
        this.w_SERIAL = ""
        this.w_ATSERIALE = ATSERIAL
        this.w_RETVAL = GSAG_BDA(this, this.w_ATSERIALE)
        if Not empty(this.w_RETVAL)
          this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Attivit� %1 del %2  non eliminata:%0%3", ALLTRIM(ATOGGETT),ttoc(ATDATINI),Alltrim(this.w_RETVAL)))     
        else
          this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Eliminata attivit� %1 del %2 ", ALLTRIM(ATOGGETT),ttoc(ATDATINI)))     
        endif
        this.w_RESOCON1 = this.w_RESOCON1 + this.w_ResMsg.ComposeMessage()
        SELECT (this.w_CursorZoom)
        endscan
        if Not Empty(this.w_RESOCON1)
          do GSUT_KLG with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_ResMsg = .null.
      case this.Param = "V"
        * --- VISUALIZZA ATTIVITA' SELEZIONATA IN GSAG_KAC (ATTIVITA' COLLEGATE)
        * --- Istanzio l'anagrafica
        this.w_OBJECT = GSAG_AAT()
        * --- Controllo se ha passato il test di accesso
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Carico il record selezionato
        this.w_OBJECT.ECPFILTER()     
        this.w_OBJECT.w_ATSERIAL = this.oParentObject.w_ATSERIAL
        this.w_OBJECT.ECPSAVE()     
      case this.Param = "T"
        * --- ELIMINA PRESTAZIONI CON IMPORTO A ZERO IN DETTAGLIO ATTIVITA'
        this.w_ObjMDA = this.oParentObject.GSAG_MDA
        this.w_ATSTATUS = This.oparentobject.w_ATSTATUS
        * --- Se l'attivit� � completata
        if this.w_ATSTATUS="P"
          if this.w_ObjMDA.NumRow() = 0
            ah_ErrorMsg("Lo stato non pu� essere 'Completata' se non � presente almeno una prestazione")
            this.oParentObject.w_RESCHK = -1
          endif
        endif
      case this.Param = "N" 
        this.w_ObjMDA = this.oParentObject.GSAG_MCP
        if this.w_ObjMDA.NumRow() <> 0
          * --- Elimina Righe compinenti
          this.w_ObjMDA.FirstRow()     
          do while ! this.w_ObjMDA.Eof_Trs()
            * --- Se riga piena elimino
            if this.w_ObjMDA.Fullrow()
              if this.Param = "N"
                this.w_ObjMDA.DeleteRow()     
              else
                this.w_ObjMDA.set("w_DACODCEN",this.oParentObject.w_ATCENCOS)     
              endif
            endif
            this.w_ObjMDA.NextRow()     
          enddo
          this.w_ObjMDA.FirstRow()     
          this.w_ObjMDA.SetRow()     
          this.w_ObjMDA.Refresh()     
        endif
      case this.Param = "G" or this.Param = "F" or this.Param = "I" or this.Param = "H" or ( (this.Param = "B"or this.Param = "R") and Not Isalt() and this.oParentObject.w_FLNSAP <>"S")
        * --- aggiorno centro di costo nelle prestazioni
        this.w_ObjMDA = this.oParentObject.GSAG_MDA
        do case
          case this.Param = "B" or this.Param = "R"
            * --- Messaggio solo se attivita soggetta a prestazioni
            this.w_MESS = "Si desidera aggiornare codice commessa nel dettaglio prestazioni?"
          case this.Param = "G" or this.Param = "F"
            this.w_MESS = "Si desidera aggiornare centro di costo/ricavo nel dettaglio prestazioni?"
          case this.Param = "H" or this.Param = "I"
            this.w_MESS = "Si desidera aggiornare attivit� di commessa nel dettaglio prestazioni?"
        endcase
        if this.w_ObjMDA.Numrow() >0 and Ah_yesno(this.w_MESS)
          this.w_ObjMDA.MarkPos()     
          * --- Elimina Righe compinenti
          this.w_ObjMDA.FirstRow()     
          do while ! this.w_ObjMDA.Eof_Trs()
            * --- Se riga piena elimino
            if this.w_ObjMDA.Fullrow()
              do case
                case this.Param = "B"
                  * --- Messaggio solo se attivita soggetta a prestazioni
                  this.w_ObjMDA.set("w_DACODCOM",this.oParentObject.w_ATCODPRA)     
                  if this.oParentObject.w_ATCODPRA=this.oParentObject.w_ATCOMRIC
                    this.w_ObjMDA.set("w_DACOMRIC",this.oParentObject.w_ATCOMRIC)     
                  endif
                case this.Param = "G"
                  this.w_ObjMDA.set("w_DACENCOS",this.oParentObject.w_ATCENCOS)     
                  if this.oParentObject.w_ATCENRIC=this.oParentObject.w_ATCENCOS
                    this.w_ObjMDA.set("w_DACENRIC",this.oParentObject.w_ATCENRIC)     
                  endif
                case this.Param = "F"
                  this.w_ObjMDA.set("w_DACENRIC",this.oParentObject.w_ATCENRIC)     
                  if this.oParentObject.w_ATCENRIC=this.oParentObject.w_ATCENCOS
                    this.w_ObjMDA.set("w_DACENCOS",this.oParentObject.w_ATCENCOS)     
                  endif
                case this.Param = "R"
                  this.w_ObjMDA.set("w_DACOMRIC",this.oParentObject.w_ATCOMRIC)     
                  if this.oParentObject.w_ATCODPRA=this.oParentObject.w_ATCOMRIC
                    this.w_ObjMDA.set("w_DACODCOM",this.oParentObject.w_ATCODPRA)     
                  endif
                case this.Param = "H"
                  this.w_ObjMDA.set("w_DAATTRIC",this.oParentObject.w_ATATTRIC)     
                  if this.oParentObject.w_ATATTRIC=this.oParentObject.w_ATATTCOS
                    this.w_ObjMDA.set("w_DAATTIVI",this.oParentObject.w_ATATTCOS)     
                  endif
                case this.Param = "I"
                  this.w_ObjMDA.set("w_DAATTIVI",this.oParentObject.w_ATATTCOS)     
                  if this.oParentObject.w_ATATTRIC=this.oParentObject.w_ATATTCOS
                    this.w_ObjMDA.set("w_DAATTRIC",this.oParentObject.w_ATATTRIC)     
                  endif
              endcase
            endif
            this.w_ObjMDA.NextRow()     
          enddo
          this.w_ObjMDA.FirstRow()     
          this.w_ObjMDA.SetRow()     
          this.w_ObjMDA.Refresh()     
          this.w_ObjMDA.RePos()     
        endif
      case this.Param = "Z"
        * --- Aggiorna campo ATCAITER in attivit� collegata di origine
        if this.oParentObject.w_AGG_ATT_ORIGINE == "S"
          * --- Write into OFF_ATTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATCAITER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATCAITER),'OFF_ATTI','ATCAITER');
                +i_ccchkf ;
            +" where ";
                +"ATSERIAL = "+cp_ToStrODBC(this.oParentObject.w_ATCAITER);
                   )
          else
            update (i_cTable) set;
                ATCAITER = this.oParentObject.w_ATCAITER;
                &i_ccchkf. ;
             where;
                ATSERIAL = this.oParentObject.w_ATCAITER;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        if g_AGFA="S" 
          if Used((this.oParentObject.w_CUR_EVE)) 
            * --- eseguo associazione tra evento e attivit�
            if Reccount((this.oParentObject.w_CUR_EVE)) >0
              gsfa_bae(this,this.oParentObject.w_ATSERIAL,this.oParentObject.w_CUR_EVE,this.oParentObject.w_NOMSG)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            Use in (this.oParentObject.w_CUR_EVE)
            this.oParentObject.w_CUR_EVE = " "
          endif
          if Not Empty(this.oParentObject.w_IDRICH)
            * --- Aggiorno stato
            * --- Write into ANEVENTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ANEVENTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ANEVENTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"EVSTARIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_STARIC),'ANEVENTI','EVSTARIC');
                  +i_ccchkf ;
              +" where ";
                  +"EVIDRICH = "+cp_ToStrODBC(this.oParentObject.w_IDRICH);
                     )
            else
              update (i_cTable) set;
                  EVSTARIC = this.oParentObject.w_STARIC;
                  &i_ccchkf. ;
               where;
                  EVIDRICH = this.oParentObject.w_IDRICH;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      case this.Param = "L" OR this.Param = "P"
        if (this.oParentObject.w_CAFLSTAT="S" AND this.Param="L") OR this.Param="P"
          this.w_SERIAL = this.oParentObject.w_ATSERIAL
          L_LOGO = GetBmp(i_codazi)
          do GSAG_SSA with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.Param = "U"
        this.w_SERIAL = This.oparentobject.w_SERIAL
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Param = "Q"
        * --- DOPO AVER SALVATO (F10) UN' ATTIVITA' IN VARIAZIONE
        this.w_ATSTATUS = This.oparentobject.w_ATSTATUS
        this.w_ATTIPRIS = This.oparentobject.w_ATTIPRIS
        * --- Se l'attivit� � associata ad una pratica ed � Evasa/Completata e se � variato lo stato/tipo riserva/pratica dell'attivit�
        if ISALT() AND NOT EMPTY(this.oParentObject.w_ATCODPRA) AND this.w_ATSTATUS$"FP" AND (this.w_ATSTATUS<>this.oParentObject.w_OLD_ATSTATUS OR this.w_ATTIPRIS<>this.oParentObject.w_OLD_ATTIPRIS OR this.oParentObject.w_ATCODPRA<>this.oParentObject.w_OLD_ATCODPRA)
          this.w_CODPRA_ATT = this.oParentObject.w_ATCODPRA
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Se � variata la pratica associata all'attivit�
        if ISALT() AND NOT EMPTY(this.oParentObject.w_OLD_ATCODPRA) AND this.oParentObject.w_ATCODPRA<>this.oParentObject.w_OLD_ATCODPRA
          this.w_CODPRA_ATT = this.oParentObject.w_OLD_ATCODPRA
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
  endproc
  proc Try_02A68AE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from ATT_DCOL
    i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2],.t.,this.ATT_DCOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ADSERIAL,ADTIPDOC,ADSERDOC  from "+i_cTable+" ATT_DCOL ";
          +" where ADSERIAL="+cp_ToStrODBC(this.w_ATSERIALE)+" and (ADTIPDOC='A' or ADTIPDOC='V')";
           ,"_Curs_ATT_DCOL")
    else
      select ADSERIAL,ADTIPDOC,ADSERDOC from (i_cTable);
       where ADSERIAL=this.w_ATSERIALE and (ADTIPDOC="A" or ADTIPDOC="V");
        into cursor _Curs_ATT_DCOL
    endif
    if used('_Curs_ATT_DCOL')
      select _Curs_ATT_DCOL
      locate for 1=1
      do while not(eof())
      this.w_SERDOC = Nvl(_Curs_ATT_DCOL.ADSERDOC," ")
      this.w_RETVAL = gsar_bdk(.null.,_Curs_ATT_DCOL.ADSERDOC)
      if Not Empty(this.w_RETVAL)
        * --- Raise
        i_Error=this.w_RETVAL
        return
      else
        * --- Delete from ATT_DCOL
        i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"ADSERIAL = "+cp_ToStrODBC(this.w_ATSERIALE);
                +" and ADSERDOC = "+cp_ToStrODBC(this.w_SERDOC);
                 )
        else
          delete from (i_cTable) where;
                ADSERIAL = this.w_ATSERIALE;
                and ADSERDOC = this.w_SERDOC;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
        select _Curs_ATT_DCOL
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04025E48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CDC_MANU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CDC_MANU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CDC_MANU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CDC_MANU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CMDATREG ="+cp_NullLink(cp_ToStrODBC(this.w_DatFinAttivita),'CDC_MANU','CMDATREG');
      +",CMDATDOC ="+cp_NullLink(cp_ToStrODBC(this.w_DatFinAttivita),'CDC_MANU','CMDATDOC');
          +i_ccchkf ;
      +" where ";
          +"CMCODICE = "+cp_ToStrODBC(this.w_ATRIFMOV);
             )
    else
      update (i_cTable) set;
          CMDATREG = this.w_DatFinAttivita;
          ,CMDATDOC = this.w_DatFinAttivita;
          &i_ccchkf. ;
       where;
          CMCODICE = this.w_ATRIFMOV;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0403D018()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from ASS_ATEV
    i_nConn=i_TableProp[this.ASS_ATEV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATEV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"AECODATT = "+cp_ToStrODBC(this.oParentObject.w_ATSERIAL);
             )
    else
      delete from (i_cTable) where;
            AECODATT = this.oParentObject.w_ATSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OBJ = CREATEOBJECT("ActivityWriter",this,"OFF_ATTI")
    this.w_Mask = this.oParentObject
    this.w_SERIAL = this.w_Mask.w_SERIAL
    this.w_CAITER = this.w_Mask.w_CAITER
    this.w_DATA_INI = this.w_Mask.w_DATA_INI
    this.w_PACHKFES = this.w_Mask.w_PACHKFES
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSAG_KAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Zoom delle Attivit�
    this.w_CursorZoom = this.w_Mask.w_AGKAD_ZOOM.ccursor
    * --- Select from TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2],.t.,this.TMPVEND1_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPVEND1 ";
           ,"_Curs_TMPVEND1")
    else
      select * from (i_cTable);
        into cursor _Curs_TMPVEND1
    endif
    if used('_Curs_TMPVEND1')
      select _Curs_TMPVEND1
      locate for 1=1
      do while not(eof())
      this.w_AGGDAT = .F.
      this.w_LSERIAL = _Curs_TMPVEND1.ATSERIAL
      this.w_ATDATINI = _Curs_TMPVEND1.DATAAGG
      this.w_OGGETTO = _Curs_TMPVEND1.ATOGGETT
      this.w_TIPO = _Curs_TMPVEND1.TIPO
      this.w_NUMGIO = _Curs_TMPVEND1.ATNUMGIO
      this.w_STATO = _Curs_TMPVEND1.ATSTATUS
      this.w_RIFSER = _Curs_TMPVEND1.ATRIFSER
      if this.w_TIPO="I" AND ! this.w_STATO $ "I-F-P" and this.w_LSERIAL>this.w_SERIAL and Not Empty(this.w_RIFSER)
        * --- Collegata indiretta verifico lo spostamento da fare
        *     ovvero se la collegata ha subito uno spostamento devo traslare in funzione dell'Iter
        * --- Read from TMPVEND1
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TMPVEND1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2],.t.,this.TMPVEND1_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DATAAGG"+;
            " from "+i_cTable+" TMPVEND1 where ";
                +"ATSERIAL = "+cp_ToStrODBC(this.w_RIFSER);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DATAAGG;
            from (i_cTable) where;
                ATSERIAL = this.w_RIFSER;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DAT_RIF = NVL((_read_.DATAAGG),cp_NullValue(_read_.DATAAGG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ODATINI = this.w_ATDATINI
        this.w_ATDATINI = DTOT((TTOD(this.w_DAT_RIF)+this.w_NUMGIO))+HOUR(this.w_DAT_RIF)*3600+MINU(this.w_DAT_RIF)*60+SEC(this.w_DAT_RIF)
        this.w_DATACAL = this.w_ATDATINI
        if this.w_ATDATINI<>this.w_ODATINI
          this.w_AGGDAT = .T.
        endif
      endif
      if this.oParentObject.w_cachkini="T"
        * --- Se da tipo attivit� lo leggo dal tipo sia il metodo di calcolo
        *     che l'esclusione o meno delle sospensioni forensi
        this.w_CHKFOR = Nvl(_Curs_TMPVEND1.CACHKFOR," ")
        this.w_CHKINI = Nvl(_Curs_TMPVEND1.CACHKINI,"N")
      else
        * --- Eseguo il calcolo come impostato  in maschere. Le sospensioni forensi
        *     sempre escluse solo per alter ego (ed in base a quanto specificato in maschera)
        this.w_CHKFOR = iif( isalt() , this.oParentObject.w_CACHKFOR ,"N")
        this.w_CHKINI = this.oParentObject.w_CACHKINI
      endif
      * --- Valorizzo oggetto
      this.w_OBJ.bSospForensi = this.w_CHKFOR="S"
      this.w_OBJ.w_ATDATINI = this.w_ATDATINI
      * --- Gestione promemoria demandata all'oggetto ActivityWriter
      this.w_DataCambiata = .T.
      * --- Select from OFF_PART
      i_nConn=i_TableProp[this.OFF_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2],.t.,this.OFF_PART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" OFF_PART ";
            +" where PASERIAL="+cp_ToStrODBC(this.w_LSERIAL)+"";
             ,"_Curs_OFF_PART")
      else
        select * from (i_cTable);
         where PASERIAL=this.w_LSERIAL;
          into cursor _Curs_OFF_PART
      endif
      if used('_Curs_OFF_PART')
        select _Curs_OFF_PART
        locate for 1=1
        do while not(eof())
        this.w_OBJ.AddPart(0,0,_Curs_OFF_PART.PATIPRIS,_Curs_OFF_PART.PACODRIS,_Curs_OFF_PART.PAGRURIS)     
          select _Curs_OFF_PART
          continue
        enddo
        use
      endif
      if this.w_CHKINI<>"N"
        * --- Calcolo automatico della data inizio...
        this.w_OBJ.GetFirstDay()     
        this.w_DATACAL = this.w_OBJ.dStartDateTime
        * --- Se data differente e con conferma... o data vuota
        *     mostro la richiesta di nuova data
        if this.w_DATACAL<>this.w_ATDATINI And this.w_CHKINI="C" Or Empty( this.w_DATACAL )
          * --- data differente tra quanto determinato dall'iter e quando disponibile
          *     dai calendari...
          * --- Visualizzazo il motivo dell'incongruenza (descrizione del giorno), in linea teorica, avendo valutato 
          *     pi� calendari � possibile che i motivi siano pi� di uno
          *     per cui leggo la descrizione del primo calendario che per il giono
          *     calcolato si hanno problemi...
          this.w_NOTEERRORE = this.w_OBJ.cStartDateTimeError
          do GSAG_SGF with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Se il calcolo o l'utente mantiene la data precedetemente calcolata non faccio niente
        *     altrimenti occorre ricalcolare la fine attivit�...
        if this.w_DATACAL<>this.w_ATDATINI And Not Empty( this.w_DATACAL)
          * --- La data ricalcolata in funzione del criterio scelto � diversa devo eseguire aggiornamento
          this.w_AGGDAT = .T.
        endif
        this.w_OBJ.Blank()     
      endif
      if this.w_AGGDAT
        this.w_GIORNO = Left(G_GIORNO[DOW(this.w_DATACAL)],3)
        * --- Write into TMPVEND1
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPVEND1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DATAAGG ="+cp_NullLink(cp_ToStrODBC(this.w_DATACAL),'TMPVEND1','DATAAGG');
          +",GIOFIN ="+cp_NullLink(cp_ToStrODBC(this.w_GIORNO),'TMPVEND1','GIOFIN');
              +i_ccchkf ;
          +" where ";
              +"ATSERIAL = "+cp_ToStrODBC(this.w_LSERIAL);
                 )
        else
          update (i_cTable) set;
              DATAAGG = this.w_DATACAL;
              ,GIOFIN = this.w_GIORNO;
              &i_ccchkf. ;
           where;
              ATSERIAL = this.w_LSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_TMPVEND1
        continue
      enddo
      use
    endif
    this.w_Mask.w_AGKAD_ZOOM.cCpQueryName = "QUERY\GSAG0KAD"
    this.w_Mask.Notifyevent("Aggiorna")     
    this.w_OBJ = .null.
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo attivit� non evase
    this.w_DATASYS = i_datsys
    * --- Lettura parametro Controllo inesistenza attivit�
    * --- Read from PAR_AGEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PAFLESAT,PAATTRIS"+;
        " from "+i_cTable+" PAR_AGEN where ";
            +"PACODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PAFLESAT,PAATTRIS;
        from (i_cTable) where;
            PACODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PAFLESAT = NVL(cp_ToDate(_read_.PAFLESAT),cp_NullValue(_read_.PAFLESAT))
      this.oParentObject.w_PAATTRIS = NVL(cp_ToDate(_read_.PAATTRIS),cp_NullValue(_read_.PAATTRIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_PAFLESAT="S"
      * --- Attivit� non evase con data uguale o successiva a quella di ingresso
      * --- Select from OFF_ATTI
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ATSERIAL  from "+i_cTable+" OFF_ATTI ";
            +" where ATCODPRA="+cp_ToStrODBC(this.w_CODPRA_ATT)+" AND ATDATINI>= "+cp_ToStrODBC(this.w_DATASYS)+" AND (ATSTATUS='T' OR ATSTATUS='D' OR ATSTATUS='I' )";
             ,"_Curs_OFF_ATTI")
      else
        select ATSERIAL from (i_cTable);
         where ATCODPRA=this.w_CODPRA_ATT AND ATDATINI>= this.w_DATASYS AND (ATSTATUS="T" OR ATSTATUS="D" OR ATSTATUS="I" );
          into cursor _Curs_OFF_ATTI
      endif
      if used('_Curs_OFF_ATTI')
        select _Curs_OFF_ATTI
        locate for 1=1
        do while not(eof())
        this.w_ATT_SERIAL = _Curs_OFF_ATTI.ATSERIAL
          select _Curs_OFF_ATTI
          continue
        enddo
        use
      endif
      * --- In assenza di attivit� non evase
      if EMPTY(this.w_ATT_SERIAL) 
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNDESCAN"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_CODPRA_ATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNDESCAN;
            from (i_cTable) where;
                CNCODCAN = this.w_CODPRA_ATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESPRA = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if ah_Yesno("Attenzione: nessuna attivit� da evadere, in corso o provvisoria presente nella pratica %1 a partire dal %2.%0Si desidera caricare un'altra attivit�?" , "", alltrim(this.w_CODPRA_ATT)+" ("+alltrim(this.w_DESPRA)+")", dtoc(this.w_DATASYS))
          This.oparentobject.w_Genera=.t.
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,Param)
    this.Param=Param
    this.w_ResMsg=createobject("Ah_Message")
    this.w_ResMsg=createobject("Ah_Message")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,16)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='OFF_PART'
    this.cWorkTables[3]='OFFDATTI'
    this.cWorkTables[4]='COM_ATTI'
    this.cWorkTables[5]='MOVICOST'
    this.cWorkTables[6]='CDC_MANU'
    this.cWorkTables[7]='RIS_ESTR'
    this.cWorkTables[8]='CAUMATTI'
    this.cWorkTables[9]='TIP_DOCU'
    this.cWorkTables[10]='ATT_DCOL'
    this.cWorkTables[11]='PAR_PRAT'
    this.cWorkTables[12]='*TMPVEND1'
    this.cWorkTables[13]='ASS_ATEV'
    this.cWorkTables[14]='ANEVENTI'
    this.cWorkTables[15]='PAR_AGEN'
    this.cWorkTables[16]='CAN_TIER'
    return(this.OpenAllTables(16))

  proc CloseCursors()
    if used('_Curs_ATT_DCOL')
      use in _Curs_ATT_DCOL
    endif
    if used('_Curs_RIS_ESTR')
      use in _Curs_RIS_ESTR
    endif
    if used('_Curs_TMPVEND1')
      use in _Curs_TMPVEND1
    endif
    if used('_Curs_OFF_PART')
      use in _Curs_OFF_PART
    endif
    if used('_Curs_OFF_ATTI')
      use in _Curs_OFF_ATTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Param"
endproc
