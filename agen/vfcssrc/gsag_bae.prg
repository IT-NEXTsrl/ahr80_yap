* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bae                                                        *
*              Aggiornamento elementi contratto                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-19                                                      *
* Last revis.: 2013-03-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAMETER,pPARAMETER2,pMOD_CALCOLO,pFORZA_CALCOLO_DATE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bae",oParentObject,m.pPARAMETER,m.pPARAMETER2,m.pMOD_CALCOLO,m.pFORZA_CALCOLO_DATE)
return(i_retval)

define class tgsag_bae as StdBatch
  * --- Local variables
  pPARAMETER = space(10)
  pPARAMETER2 = space(10)
  pMOD_CALCOLO = space(1)
  pFORZA_CALCOLO_DATE = space(1)
  w_RECNO = 0
  w_RECCOUNT = 0
  w_MODALITA = space(1)
  w_MESS = space(10)
  w_CONFERMA = .f.
  w_ELCONTRA = space(10)
  w_ELCODMOD = space(10)
  w_ELCODIMP = space(10)
  w_ELCODCOM = 0
  w_ELRINNOV = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiamato dall'aggiornamento elementi contratto
    * --- pPARAMETER2
    *     '0' o vuoto: aggiorna da modello/contratto
    *     '1' aggiorna date di competenza attivit�
    *     '2' aggiorna date di competenza del documento
    *     '3' aggiorna date di competenza dell'attivit� e del documento
    if NOT EMPTY( this.pPARAMETER2 )
      this.w_MODALITA = this.pPARAMETER2
    else
      this.w_MODALITA = "0"
    endif
    do case
      case this.w_MODALITA="0"
        this.w_MESS = "Procedere con l'aggiornamento degli elementi contratto selezionati in base alle impostazioni attuali di modelli elemento e contratti di riferimento?"
      case this.w_MODALITA="1"
        this.w_MESS = "Procedere con l'aggiornamento delle date competenza attivit� sugli elementi contratto selezionati?"
      case this.w_MODALITA="2"
        this.w_MESS = "Procedere con l'aggiornamento delle date competenza documento sugli elementi contratto selezionati?"
      case this.w_MODALITA="3"
        this.w_MESS = "Procedere con l'aggiornamento delle date competenza attivit� e documento sugli elementi contratto selezionati?"
    endcase
    do case
      case this.pPARAMETER = "SELEZ_NOM" OR this.pPARAMETER = "DESEL_NOM" OR this.pPARAMETER = "INVER_NOM"
        * --- Seleziona, deseleziona/Inverte selezione di tutti i nominativi
        SELECT ( this.oParentObject.w_ZOOM.cCURSOR )
        this.w_RECCOUNT = RECCOUNT()
        if this.w_RECCOUNT <> 0
          this.w_RECNO = MIN( RECNO(), RECCOUNT() )
          UPDATE ( this.oParentObject.w_ZOOM.cCURSOR ) SET XCHK = ICASE(this.pPARAMETER = "SELEZ_NOM", 1, this.pPARAMETER = "DESEL_NOM", 0, IIF(XCHK=1, 0, 1))
          SELECT ( this.oParentObject.w_ZOOM.cCURSOR )
          if this.w_RECNO > 0
            GOTO this.w_RECNO
          endif
        endif
      case this.pPARAMETER = "AGGIORNA"
        * --- Controlla che sia stata selezionata almeno una riga
        L_RIGHESELEZIONATE = 0
        SELECT (this.oParentObject.w_ZOOM.cCURSOR )
        this.w_RECCOUNT = RECCOUNT()
        if this.w_RECCOUNT <> 0
          this.w_RECNO = RECNO()
          COUNT FOR XCHK = 1 TO L_RIGHESELEZIONATE
          GOTO this.w_RECNO
        endif
        if L_RIGHESELEZIONATE = 0
          AH_ERRORMSG("Occorre selezionare almeno una riga")
          i_retcode = 'stop'
          return
        endif
        * --- Se il flag Forza ricalcolo date � attivo, si chiede conferma
        this.w_CONFERMA = .F.
        if this.pFORZA_CALCOLO_DATE = "S"
          this.w_CONFERMA = AH_YESNO( "Attenzione! Verranno aggiornate tutte le date competenza degli elementi selezionati anche se gi� valorizzate!%0Procedo ugualmente?" )
          if NOT this.w_CONFERMA
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Aggiorna gli elementi contratto selezionati
        if NOT this.w_CONFERMA
          this.w_CONFERMA = AH_YESNO(this.w_MESS)
        endif
        if this.w_CONFERMA
          SELECT ( this.oParentObject.w_ZOOM.cCURSOR )
          this.w_RECNO = RECNO()
          GO TOP
          do while NOT EOF()
            if XCHK = 1
              this.w_ELCONTRA = ELCONTRA
              this.w_ELCODMOD = ELCODMOD
              this.w_ELCODIMP = ELCODIMP
              this.w_ELCODCOM = ELCODCOM
              this.w_ELRINNOV = NVL( ELRINNOV, 0 )
              * --- Aggiorna l'elemento
              * --- w_MODALITA
              *     '0' aggiorna da modello/contratto
              *     '1' aggiorna date di competenza attivit�
              *     '2' aggiorna date di competenza del documento
              *     '3' aggiorna date di competenza dell'attivit� e del documento
              do case
                case this.w_MODALITA="0"
                  GSAG_BA6(this,this.w_ELCONTRA, this.w_ELCODMOD, this.w_ELCODIMP, this.w_ELCODCOM, this.w_ELRINNOV)
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                case this.w_MODALITA="1"
                  GSAG_BA7(this,this.w_ELCONTRA, this.w_ELCODMOD, this.w_ELCODIMP, this.w_ELCODCOM, this.w_ELRINNOV, "A", this.pMOD_CALCOLO, this.pFORZA_CALCOLO_DATE)
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                case this.w_MODALITA="2"
                  GSAG_BA7(this,this.w_ELCONTRA, this.w_ELCODMOD, this.w_ELCODIMP, this.w_ELCODCOM, this.w_ELRINNOV, "D", this.pMOD_CALCOLO, this.pFORZA_CALCOLO_DATE)
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                case this.w_MODALITA="3"
                  GSAG_BA7(this,this.w_ELCONTRA, this.w_ELCODMOD, this.w_ELCODIMP, this.w_ELCODCOM, this.w_ELRINNOV, "A", this.pMOD_CALCOLO, this.pFORZA_CALCOLO_DATE)
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  GSAG_BA7(this,this.w_ELCONTRA, this.w_ELCODMOD, this.w_ELCODIMP, this.w_ELCODCOM, this.w_ELRINNOV, "D", this.pMOD_CALCOLO, this.pFORZA_CALCOLO_DATE)
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
              endcase
            endif
            SELECT ( this.oParentObject.w_ZOOM.cCURSOR )
            SKIP
          enddo
          this.oParentObject.NotifyEvent("Search")
          AH_ERRORMSG( "Operazione completata" )
        endif
    endcase
  endproc


  proc Init(oParentObject,pPARAMETER,pPARAMETER2,pMOD_CALCOLO,pFORZA_CALCOLO_DATE)
    this.pPARAMETER=pPARAMETER
    this.pPARAMETER2=pPARAMETER2
    this.pMOD_CALCOLO=pMOD_CALCOLO
    this.pFORZA_CALCOLO_DATE=pFORZA_CALCOLO_DATE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAMETER,pPARAMETER2,pMOD_CALCOLO,pFORZA_CALCOLO_DATE"
endproc
