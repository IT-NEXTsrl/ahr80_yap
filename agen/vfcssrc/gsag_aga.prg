* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_aga                                                        *
*              Piano di generazione attivit�                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-09                                                      *
* Last revis.: 2016-10-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_aga"))

* --- Class definition
define class tgsag_aga as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 555
  Height = 319+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-10-14"
  HelpContextID=225826967
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=39

  * --- Constant Properties
  GEST_ATTI_IDX = 0
  CAT_ELEM_IDX = 0
  CONTI_IDX = 0
  IMP_MAST_IDX = 0
  DES_DIVE_IDX = 0
  TIP_DOCU_IDX = 0
  CAUMATTI_IDX = 0
  DIPENDEN_IDX = 0
  PAR_AGEN_IDX = 0
  cFile = "GEST_ATTI"
  cKeySelect = "ATSERIAL"
  cKeyWhere  = "ATSERIAL=this.w_ATSERIAL"
  cKeyWhereODBC = '"ATSERIAL="+cp_ToStrODBC(this.w_ATSERIAL)';

  cKeyWhereODBCqualified = '"GEST_ATTI.ATSERIAL="+cp_ToStrODBC(this.w_ATSERIAL)';

  cPrg = "gsag_aga"
  cComment = "Piano di generazione attivit�"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ATSERIAL = space(20)
  w_READAZI = space(10)
  w_FLAT = space(1)
  w_CODPER = space(3)
  w_TIPCON = space(1)
  w_ATDATINI = ctod('  /  /  ')
  o_ATDATINI = ctod('  /  /  ')
  w_ATDATFIN = ctod('  /  /  ')
  w_ATDESCRI = space(60)
  w_ATCODCLI = space(15)
  w_CODICE = space(15)
  w_ATCODSED = space(5)
  w_SEDE = space(5)
  w_ATCODIMP = space(10)
  w_ATCAUDOC = space(5)
  w_ATTIPATT = space(20)
  w_ATGRUPAR = space(5)
  w_DESELEM = space(60)
  w_ATCATCON = space(5)
  w_DESCLI = space(60)
  w_ATFLRAGG = space(1)
  w_DESIMP = space(50)
  w_NOMDES = space(50)
  w_DESDOC = space(35)
  w_PATIPRIS = space(1)
  w_CARAGGST = space(10)
  w_DESATT = space(254)
  w_CATDOC = space(2)
  w_FLVEAC = space(1)
  w_TIPRIS = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_OB_TEST = ctod('  /  /  ')
  w_DESCRI = space(60)
  w_HASEVCOP = space(15)
  w_HASEVENT = .F.
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_UTDC = ctot('')
  w_UTCV = 0
  w_UTDV = ctot('')
  w_UTCC = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_ATSERIAL = this.W_ATSERIAL

  * --- Children pointers
  GSAG_MDG = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_aga
    proc ecpDelete()
      local bRes,nRec,bErr
  		*** Zucchetti aulla - gestione oggetto Hedaer nei detail		
          * --- Per ripristinare il cursore dai filtri prima di effettuare eventuali controlli
          if vartype(this.oPgFrm.Page1.oPag.oHeaderDetail)='O'
              this.oPgFrm.Page1.oPag.oHeaderDetail.BlankFilter()
              Select (This.cTrsName)
              SET FILTER TO
          endif
      if type('this.oPgFrm.Pages(this.oPgFrm.activepage).oWarning')<>'U'
        this.oPgFrm.Pages(this.oPgFrm.ActivePage).oWarning.ecpDelete()
        this.ReleaseWarn()
        this.LoadWarn()
        return
      endif
      if !this.bSec4
        cp_errormsg(MSG_ACCESS_DENIED,16,MSG_SECURITY_ADMIN)
        return
      endif
      if !this.CanDelete()
        return
      endif
      IF !this.deletewarn()
        RETURN
      ENDIF
      IF this.bDeleting
        RETURN
      ENDIF
      this.bDeleting=.t.
      if this.bLoaded and this.cFunction='Query'
        * --- se il dato e' vecchio deve essere riletto per avere i timestamp aggiornati
        if this.nLoadTime=0
          this.LoadRecWarn()  && rilettura se si cancella direttamente dopo un editing senza eseguire una query
        endif
        * --- Richiesta conferma Cancellazione Record
        bRes=.t.
        if bRes and !this.CheckChildrenForDelete()
          bRes=cp_YesNo(MSG_DATA_EXIST_IN_CHILD_ENTITY_C_PROCEED_QP)
        endif
        if bRes
                  *--- Activity logger traccio la modifica di un record
                  If i_nACTIVATEPROFILER>0
                      cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UF5", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
                  Endif
                  *--- Activity logger traccio la modifica di un record
          thisform.lockscreen=.t.
          select (this.cKeySet)
          nRec=recno()
          cp_BeginTrs()
                  **** Zucchetti Aulla inizio - controllo flussi
                  this.cFlowStamp = SYS(2015)
  			    this.bFlowNoRules = .f.
                  **** Zucchetti Aulla fine - controllo flussi
          this.lockscreen=.t.
       * Zucchetti aulla inizio - Gestione attributi
  	  if TYPE("this.bGestAtt")='O' And vartype(I_CURFORM.w_GESTGUID)='C'
  	  * Elimino l'attributo associato (di fatto la funzione � una DELETE (utilizziamo un comando Painter per gestire bTrsErr in automatico e la connessione...)
  	  ELIMINATT( I_CURFORM.w_GESTGUID)
  	  endif
  	 * Zucchetti aulla fine - Gestione attributi
          this.mDelete()
          bErr=bTrsErr
          this.lockscreen=.f.
          cp_EndTrs()
          IF not(bErr)
            this.NotifyEvent("Record Deleted")
          ENDIF
          this.QueryKeySet(this.cLastWhere,this.cLastOrderBy)
          if reccount()>=nRec
            goto nRec
          endif
          this.LoadRecWarn()
                  **** Zucchetti Aulla inizio - Sincronizzazione
                  *** Delete
                  If g_APPLICATION = "ad hoc ENTERPRISE" And Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And Vartype(g_SINC)="C" And g_SINC='S'
                      Local i_cParam
                      i_cParam='DM'
                      If !Empty(i_cParam) And Type("g_SINC") = "C"
                          If g_SINC = "S"
                              l_sPrg="GSSI_BSD"
                              Do (l_sPrg) With This,i_cParam
                          Endif
                      Endif
                  Endif
  
                  **** Zucchetti Aulla fine - Sincronizzazione
          thisform.lockscreen=.f.
          this.Refresh()
        endif
      ENDIF
      this.bDeleting=.f.
    return
  
    Func ah_HasCPEvents(i_cOp)	
  	If (this.cFunction='Query' And (Upper(i_cop)='ECPDELETE'))
        if (Upper(i_cop)='ECPEDIT' And (seconds()>this.nLoadTime+60 or this.bupdated)) Or(Upper(i_cop)='ECPDELETE' And this.nLoadTime=0)
          this.LoadRecWarn()  && rilettura se sono passati piu' di 60 secondi dall' ultima lettura o il record � stato variato in Interroga
        ENDIF
  		This.w_HASEVCOP=i_cop	
  		this.NotifyEvent('HasEvent')
  		return(this.w_HASEVENT)
  	Else
  		return(.t.)	
  	endif
    EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GEST_ATTI','gsag_aga')
    stdPageFrame::Init()
    *set procedure to GSAG_MDG additive
    with this
      .Pages(1).addobject("oPag","tgsag_agaPag1","gsag_aga",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Piano")
      .Pages(1).HelpContextID = 187017482
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAG_MDG
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CAT_ELEM'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='IMP_MAST'
    this.cWorkTables[4]='DES_DIVE'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='CAUMATTI'
    this.cWorkTables[7]='DIPENDEN'
    this.cWorkTables[8]='PAR_AGEN'
    this.cWorkTables[9]='GEST_ATTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GEST_ATTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GEST_ATTI_IDX,3]
  return

  function CreateChildren()
    this.GSAG_MDG = CREATEOBJECT('stdLazyChild',this,'GSAG_MDG')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAG_MDG)
      this.GSAG_MDG.DestroyChildrenChain()
      this.GSAG_MDG=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAG_MDG.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAG_MDG.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAG_MDG.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAG_MDG.SetKey(;
            .w_ATSERIAL,"MDSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAG_MDG.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATSERIAL,"MDSERIAL";
             )
    endwith
    return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ATSERIAL = NVL(ATSERIAL,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_24_joined
    link_1_24_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GEST_ATTI where ATSERIAL=KeySet.ATSERIAL
    *
    i_nConn = i_TableProp[this.GEST_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GEST_ATTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GEST_ATTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GEST_ATTI '
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_24_joined=this.AddJoinedLink_1_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_FLAT = space(1)
        .w_CODPER = space(3)
        .w_TIPCON = 'C'
        .w_DESELEM = space(60)
        .w_DESCLI = space(60)
        .w_DESIMP = space(50)
        .w_NOMDES = space(50)
        .w_DESDOC = space(35)
        .w_PATIPRIS = 'G'
        .w_CARAGGST = space(10)
        .w_DESATT = space(254)
        .w_CATDOC = space(2)
        .w_FLVEAC = space(1)
        .w_TIPRIS = space(1)
        .w_OBTEST = ctod("  /  /  ")
        .w_OB_TEST = ctod("  /  /  ")
        .w_DESCRI = space(60)
        .w_HASEVCOP = space(15)
        .w_HASEVENT = .f.
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_ATSERIAL = NVL(ATSERIAL,space(20))
        .op_ATSERIAL = .w_ATSERIAL
        .w_READAZI = I_CODAZI
          .link_1_2('Load')
        .w_ATDATINI = NVL(cp_ToDate(ATDATINI),ctod("  /  /  "))
        .w_ATDATFIN = NVL(cp_ToDate(ATDATFIN),ctod("  /  /  "))
        .w_ATDESCRI = NVL(ATDESCRI,space(60))
        .w_ATCODCLI = NVL(ATCODCLI,space(15))
          .link_1_12('Load')
        .w_CODICE = .w_ATCODCLI
        .w_ATCODSED = NVL(ATCODSED,space(5))
          .link_1_16('Load')
        .w_SEDE = .w_ATCODSED
        .w_ATCODIMP = NVL(ATCODIMP,space(10))
          if link_1_19_joined
            this.w_ATCODIMP = NVL(IMCODICE119,NVL(this.w_ATCODIMP,space(10)))
            this.w_DESIMP = NVL(IMDESCRI119,space(50))
          else
          .link_1_19('Load')
          endif
        .w_ATCAUDOC = NVL(ATCAUDOC,space(5))
          if link_1_20_joined
            this.w_ATCAUDOC = NVL(TDTIPDOC120,NVL(this.w_ATCAUDOC,space(5)))
            this.w_DESDOC = NVL(TDDESDOC120,space(35))
            this.w_CATDOC = NVL(TDCATDOC120,space(2))
            this.w_FLVEAC = NVL(TDFLVEAC120,space(1))
          else
          .link_1_20('Load')
          endif
        .w_ATTIPATT = NVL(ATTIPATT,space(20))
          if link_1_21_joined
            this.w_ATTIPATT = NVL(CACODICE121,NVL(this.w_ATTIPATT,space(20)))
            this.w_DESATT = NVL(CADESCRI121,space(254))
            this.w_CARAGGST = NVL(CARAGGST121,space(10))
          else
          .link_1_21('Load')
          endif
        .w_ATGRUPAR = NVL(ATGRUPAR,space(5))
          if link_1_22_joined
            this.w_ATGRUPAR = NVL(DPCODICE122,NVL(this.w_ATGRUPAR,space(5)))
            this.w_TIPRIS = NVL(DPTIPRIS122,space(1))
            this.w_DESCRI = NVL(DPDESCRI122,space(60))
          else
          .link_1_22('Load')
          endif
        .w_ATCATCON = NVL(ATCATCON,space(5))
          if link_1_24_joined
            this.w_ATCATCON = NVL(CECODELE124,NVL(this.w_ATCATCON,space(5)))
            this.w_DESELEM = NVL(CEDESELE124,space(60))
          else
          .link_1_24('Load')
          endif
        .w_ATFLRAGG = NVL(ATFLRAGG,space(1))
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCC = NVL(UTCC,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'GEST_ATTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ATSERIAL = space(20)
      .w_READAZI = space(10)
      .w_FLAT = space(1)
      .w_CODPER = space(3)
      .w_TIPCON = space(1)
      .w_ATDATINI = ctod("  /  /  ")
      .w_ATDATFIN = ctod("  /  /  ")
      .w_ATDESCRI = space(60)
      .w_ATCODCLI = space(15)
      .w_CODICE = space(15)
      .w_ATCODSED = space(5)
      .w_SEDE = space(5)
      .w_ATCODIMP = space(10)
      .w_ATCAUDOC = space(5)
      .w_ATTIPATT = space(20)
      .w_ATGRUPAR = space(5)
      .w_DESELEM = space(60)
      .w_ATCATCON = space(5)
      .w_DESCLI = space(60)
      .w_ATFLRAGG = space(1)
      .w_DESIMP = space(50)
      .w_NOMDES = space(50)
      .w_DESDOC = space(35)
      .w_PATIPRIS = space(1)
      .w_CARAGGST = space(10)
      .w_DESATT = space(254)
      .w_CATDOC = space(2)
      .w_FLVEAC = space(1)
      .w_TIPRIS = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_OB_TEST = ctod("  /  /  ")
      .w_DESCRI = space(60)
      .w_HASEVCOP = space(15)
      .w_HASEVENT = .f.
      .w_DTBSO_CAUMATTI = ctod("  /  /  ")
      .w_UTDC = ctot("")
      .w_UTCV = 0
      .w_UTDV = ctot("")
      .w_UTCC = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_READAZI = I_CODAZI
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_READAZI))
          .link_1_2('Full')
          endif
          .DoRTCalc(3,4,.f.)
        .w_TIPCON = 'C'
        .w_ATDATINI = IIF(.w_FLAT='E', CTOD('01-01-'+ALLTRIM(STR(YEAR(I_DATSYS)))), IIF(.w_FLAT='F', CTOD('01-' +ALLTRIM(STR(MONTH(I_DATSYS)))+'-' +ALLTRIM(STR(YEAR(I_DATSYS)))),IIF(.w_FLAT='I', CTOD( DTOC(I_datsys-DOW(i_datsys,2)+1)),i_DATSYS)))
        .w_ATDATFIN = NEXTTIME(.w_CODPER, .w_ATDATINI)
        .DoRTCalc(8,9,.f.)
          if not(empty(.w_ATCODCLI))
          .link_1_12('Full')
          endif
        .w_CODICE = .w_ATCODCLI
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_ATCODSED))
          .link_1_16('Full')
          endif
        .w_SEDE = .w_ATCODSED
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_ATCODIMP))
          .link_1_19('Full')
          endif
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_ATCAUDOC))
          .link_1_20('Full')
          endif
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_ATTIPATT))
          .link_1_21('Full')
          endif
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_ATGRUPAR))
          .link_1_22('Full')
          endif
        .DoRTCalc(17,18,.f.)
          if not(empty(.w_ATCATCON))
          .link_1_24('Full')
          endif
          .DoRTCalc(19,19,.f.)
        .w_ATFLRAGG = 'N'
          .DoRTCalc(21,23,.f.)
        .w_PATIPRIS = 'G'
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
          .DoRTCalc(25,34,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
      endif
    endwith
    cp_BlankRecExtFlds(this,'GEST_ATTI')
    this.DoRTCalc(36,39,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GEST_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SERAT","i_CODAZI,w_ATSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_ATSERIAL = .w_ATSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oATDATINI_1_6.enabled = i_bVal
      .Page1.oPag.oATDATFIN_1_7.enabled = i_bVal
      .Page1.oPag.oATDESCRI_1_8.enabled = i_bVal
      .Page1.oPag.oATCODCLI_1_12.enabled = i_bVal
      .Page1.oPag.oATCODSED_1_16.enabled = i_bVal
      .Page1.oPag.oATCODIMP_1_19.enabled = i_bVal
      .Page1.oPag.oATCAUDOC_1_20.enabled = i_bVal
      .Page1.oPag.oATTIPATT_1_21.enabled = i_bVal
      .Page1.oPag.oATGRUPAR_1_22.enabled = i_bVal
      .Page1.oPag.oATCATCON_1_24.enabled = i_bVal
      .Page1.oPag.oATFLRAGG_1_26.enabled = i_bVal
      .Page1.oPag.oBtn_1_37.enabled = i_bVal
      .Page1.oPag.oBtn_1_38.enabled = .Page1.oPag.oBtn_1_38.mCond()
      .Page1.oPag.oBtn_1_46.enabled = .Page1.oPag.oBtn_1_46.mCond()
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_50.enabled = i_bVal
    endwith
    this.GSAG_MDG.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'GEST_ATTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAG_MDG.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GEST_ATTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSERIAL,"ATSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATINI,"ATDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATFIN,"ATDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDESCRI,"ATDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODCLI,"ATCODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODSED,"ATCODSED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODIMP,"ATCODIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCAUDOC,"ATCAUDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPATT,"ATTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATGRUPAR,"ATGRUPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCATCON,"ATCATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATFLRAGG,"ATFLRAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GEST_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2])
    i_lTable = "GEST_ATTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GEST_ATTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GEST_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GEST_ATTI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SERAT","i_CODAZI,w_ATSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GEST_ATTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GEST_ATTI')
        i_extval=cp_InsertValODBCExtFlds(this,'GEST_ATTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ATSERIAL,ATDATINI,ATDATFIN,ATDESCRI,ATCODCLI"+;
                  ",ATCODSED,ATCODIMP,ATCAUDOC,ATTIPATT,ATGRUPAR"+;
                  ",ATCATCON,ATFLRAGG,UTDC,UTCV,UTDV"+;
                  ",UTCC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ATSERIAL)+;
                  ","+cp_ToStrODBC(this.w_ATDATINI)+;
                  ","+cp_ToStrODBC(this.w_ATDATFIN)+;
                  ","+cp_ToStrODBC(this.w_ATDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODCLI)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODSED)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODIMP)+;
                  ","+cp_ToStrODBCNull(this.w_ATCAUDOC)+;
                  ","+cp_ToStrODBCNull(this.w_ATTIPATT)+;
                  ","+cp_ToStrODBCNull(this.w_ATGRUPAR)+;
                  ","+cp_ToStrODBCNull(this.w_ATCATCON)+;
                  ","+cp_ToStrODBC(this.w_ATFLRAGG)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GEST_ATTI')
        i_extval=cp_InsertValVFPExtFlds(this,'GEST_ATTI')
        cp_CheckDeletedKey(i_cTable,0,'ATSERIAL',this.w_ATSERIAL)
        INSERT INTO (i_cTable);
              (ATSERIAL,ATDATINI,ATDATFIN,ATDESCRI,ATCODCLI,ATCODSED,ATCODIMP,ATCAUDOC,ATTIPATT,ATGRUPAR,ATCATCON,ATFLRAGG,UTDC,UTCV,UTDV,UTCC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ATSERIAL;
                  ,this.w_ATDATINI;
                  ,this.w_ATDATFIN;
                  ,this.w_ATDESCRI;
                  ,this.w_ATCODCLI;
                  ,this.w_ATCODSED;
                  ,this.w_ATCODIMP;
                  ,this.w_ATCAUDOC;
                  ,this.w_ATTIPATT;
                  ,this.w_ATGRUPAR;
                  ,this.w_ATCATCON;
                  ,this.w_ATFLRAGG;
                  ,this.w_UTDC;
                  ,this.w_UTCV;
                  ,this.w_UTDV;
                  ,this.w_UTCC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GEST_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GEST_ATTI_IDX,i_nConn)
      *
      * update GEST_ATTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GEST_ATTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ATDATINI="+cp_ToStrODBC(this.w_ATDATINI)+;
             ",ATDATFIN="+cp_ToStrODBC(this.w_ATDATFIN)+;
             ",ATDESCRI="+cp_ToStrODBC(this.w_ATDESCRI)+;
             ",ATCODCLI="+cp_ToStrODBCNull(this.w_ATCODCLI)+;
             ",ATCODSED="+cp_ToStrODBCNull(this.w_ATCODSED)+;
             ",ATCODIMP="+cp_ToStrODBCNull(this.w_ATCODIMP)+;
             ",ATCAUDOC="+cp_ToStrODBCNull(this.w_ATCAUDOC)+;
             ",ATTIPATT="+cp_ToStrODBCNull(this.w_ATTIPATT)+;
             ",ATGRUPAR="+cp_ToStrODBCNull(this.w_ATGRUPAR)+;
             ",ATCATCON="+cp_ToStrODBCNull(this.w_ATCATCON)+;
             ",ATFLRAGG="+cp_ToStrODBC(this.w_ATFLRAGG)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GEST_ATTI')
        i_cWhere = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
        UPDATE (i_cTable) SET;
              ATDATINI=this.w_ATDATINI;
             ,ATDATFIN=this.w_ATDATFIN;
             ,ATDESCRI=this.w_ATDESCRI;
             ,ATCODCLI=this.w_ATCODCLI;
             ,ATCODSED=this.w_ATCODSED;
             ,ATCODIMP=this.w_ATCODIMP;
             ,ATCAUDOC=this.w_ATCAUDOC;
             ,ATTIPATT=this.w_ATTIPATT;
             ,ATGRUPAR=this.w_ATGRUPAR;
             ,ATCATCON=this.w_ATCATCON;
             ,ATFLRAGG=this.w_ATFLRAGG;
             ,UTDC=this.w_UTDC;
             ,UTCV=this.w_UTCV;
             ,UTDV=this.w_UTDV;
             ,UTCC=this.w_UTCC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAG_MDG : Saving
      this.GSAG_MDG.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATSERIAL,"MDSERIAL";
             )
      this.GSAG_MDG.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAG_MDG : Deleting
    this.GSAG_MDG.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATSERIAL,"MDSERIAL";
           )
    this.GSAG_MDG.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GEST_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GEST_ATTI_IDX,i_nConn)
      *
      * delete GEST_ATTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GEST_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEST_ATTI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_READAZI = I_CODAZI
          .link_1_2('Full')
        .DoRTCalc(3,6,.t.)
        if .o_ATDATINI<>.w_ATDATINI
            .w_ATDATFIN = NEXTTIME(.w_CODPER, .w_ATDATINI)
        endif
        .DoRTCalc(8,9,.t.)
            .w_CODICE = .w_ATCODCLI
        .DoRTCalc(11,11,.t.)
            .w_SEDE = .w_ATCODSED
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SERAT","i_CODAZI,w_ATSERIAL")
          .op_ATSERIAL = .w_ATSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(13,39,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
    endwith
  return

  proc Calculate_KVPJLUUNUL()
    with this
          * --- Cancella date
          .w_ATDATFIN = cp_chartodate('  -  -  ')
    endwith
  endproc
  proc Calculate_HGUMKCTBOL()
    with this
          * --- Forza aggiornamento
          .bupdated = .T.
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_46.enabled = this.oPgFrm.Page1.oPag.oBtn_1_46.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oLinkPC_1_36.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_ATDATINI Changed")
          .Calculate_KVPJLUUNUL()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
        if lower(cEvent)==lower("New record")
          .Calculate_HGUMKCTBOL()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PCNOFLAT,PACODPER";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PCNOFLAT,PACODPER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(10))
      this.w_FLAT = NVL(_Link_.PCNOFLAT,space(1))
      this.w_CODPER = NVL(_Link_.PACODPER,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(10)
      endif
      this.w_FLAT = space(1)
      this.w_CODPER = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODCLI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ATCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_ATCODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_ATCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_ATCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oATCODCLI_1_12'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ATCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_ATCODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODCLI = space(15)
      endif
      this.w_DESCLI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODSED
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_ATCODSED)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_ATCODCLI);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_TIPCON;
                     ,'DDCODICE',this.w_ATCODCLI;
                     ,'DDCODDES',trim(this.w_ATCODSED))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODSED)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODSED) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oATCODSED_1_16'),i_cWhere,'',"Sedi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);
           .or. this.w_ATCODCLI<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_ATCODCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_ATCODSED);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_ATCODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_TIPCON;
                       ,'DDCODICE',this.w_ATCODCLI;
                       ,'DDCODDES',this.w_ATCODSED)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODSED = NVL(_Link_.DDCODDES,space(5))
      this.w_NOMDES = NVL(_Link_.DDNOMDES,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODSED = space(5)
      endif
      this.w_NOMDES = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODIMP
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIM',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_ATCODIMP)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_ATCODIMP))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODIMP)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODIMP) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oATCODIMP_1_19'),i_cWhere,'GSAG_MIM',"Impianti",'GSAG_KTR.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_ATCODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_ATCODIMP)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODIMP = NVL(_Link_.IMCODICE,space(10))
      this.w_DESIMP = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODIMP = space(10)
      endif
      this.w_DESIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.IMP_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.IMCODICE as IMCODICE119"+ ",link_1_19.IMDESCRI as IMDESCRI119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on GEST_ATTI.ATCODIMP=link_1_19.IMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and GEST_ATTI.ATCODIMP=link_1_19.IMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCAUDOC
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_ATCAUDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_ATCAUDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oATCAUDOC_1_20'),i_cWhere,'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_ATCAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_ATCAUDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCAUDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDOC<>'OR' AND .w_FLVEAC='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti")
        endif
        this.w_ATCAUDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.TDTIPDOC as TDTIPDOC120"+ ",link_1_20.TDDESDOC as TDDESDOC120"+ ",link_1_20.TDCATDOC as TDCATDOC120"+ ",link_1_20.TDFLVEAC as TDFLVEAC120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on GEST_ATTI.ATCAUDOC=link_1_20.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and GEST_ATTI.ATCAUDOC=link_1_20.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATTIPATT
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTIPATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ATTIPATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ATTIPATT))
          select CACODICE,CADESCRI,CARAGGST;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTIPATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_ATTIPATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_ATTIPATT)+"%");

            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATTIPATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oATTIPATT_1_21'),i_cWhere,'GSAG_MCA',"Tipi attivita",'GSAG1AAT.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTIPATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ATTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ATTIPATT)
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTIPATT = NVL(_Link_.CACODICE,space(20))
      this.w_DESATT = NVL(_Link_.CADESCRI,space(254))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ATTIPATT = space(20)
      endif
      this.w_DESATT = space(254)
      this.w_CARAGGST = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTIPATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.CACODICE as CACODICE121"+ ",link_1_21.CADESCRI as CADESCRI121"+ ",link_1_21.CARAGGST as CARAGGST121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on GEST_ATTI.ATTIPATT=link_1_21.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and GEST_ATTI.ATTIPATT=link_1_21.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATGRUPAR
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATGRUPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_ATGRUPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_ATGRUPAR))
          select DPCODICE,DPTIPRIS,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATGRUPAR)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATGRUPAR) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oATGRUPAR_1_22'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATGRUPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_ATGRUPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_ATGRUPAR)
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATGRUPAR = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DESCRI = NVL(_Link_.DPDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ATGRUPAR = space(5)
      endif
      this.w_TIPRIS = space(1)
      this.w_DESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIS='G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATGRUPAR = space(5)
        this.w_TIPRIS = space(1)
        this.w_DESCRI = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATGRUPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.DPCODICE as DPCODICE122"+ ",link_1_22.DPTIPRIS as DPTIPRIS122"+ ",link_1_22.DPDESCRI as DPDESCRI122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on GEST_ATTI.ATGRUPAR=link_1_22.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and GEST_ATTI.ATGRUPAR=link_1_22.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCATCON
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ELEM_IDX,3]
    i_lTable = "CAT_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2], .t., this.CAT_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACE',True,'CAT_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODELE like "+cp_ToStrODBC(trim(this.w_ATCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODELE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODELE',trim(this.w_ATCATCON))
          select CECODELE,CEDESELE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODELE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCATCON)==trim(_Link_.CECODELE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCATCON) and !this.bDontReportError
            deferred_cp_zoom('CAT_ELEM','*','CECODELE',cp_AbsName(oSource.parent,'oATCATCON_1_24'),i_cWhere,'GSAG_ACE',"Categorie elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                     +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',oSource.xKey(1))
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                   +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(this.w_ATCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',this.w_ATCATCON)
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCATCON = NVL(_Link_.CECODELE,space(5))
      this.w_DESELEM = NVL(_Link_.CEDESELE,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ATCATCON = space(5)
      endif
      this.w_DESELEM = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.CECODELE,1)
      cp_ShowWarn(i_cKey,this.CAT_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_ELEM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_24.CECODELE as CECODELE124"+ ",link_1_24.CEDESELE as CEDESELE124"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_24 on GEST_ATTI.ATCATCON=link_1_24.CECODELE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_24"
          i_cKey=i_cKey+'+" and GEST_ATTI.ATCATCON=link_1_24.CECODELE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATDATINI_1_6.value==this.w_ATDATINI)
      this.oPgFrm.Page1.oPag.oATDATINI_1_6.value=this.w_ATDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oATDATFIN_1_7.value==this.w_ATDATFIN)
      this.oPgFrm.Page1.oPag.oATDATFIN_1_7.value=this.w_ATDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oATDESCRI_1_8.value==this.w_ATDESCRI)
      this.oPgFrm.Page1.oPag.oATDESCRI_1_8.value=this.w_ATDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODCLI_1_12.value==this.w_ATCODCLI)
      this.oPgFrm.Page1.oPag.oATCODCLI_1_12.value=this.w_ATCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODSED_1_16.value==this.w_ATCODSED)
      this.oPgFrm.Page1.oPag.oATCODSED_1_16.value=this.w_ATCODSED
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODIMP_1_19.value==this.w_ATCODIMP)
      this.oPgFrm.Page1.oPag.oATCODIMP_1_19.value=this.w_ATCODIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oATCAUDOC_1_20.value==this.w_ATCAUDOC)
      this.oPgFrm.Page1.oPag.oATCAUDOC_1_20.value=this.w_ATCAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oATTIPATT_1_21.value==this.w_ATTIPATT)
      this.oPgFrm.Page1.oPag.oATTIPATT_1_21.value=this.w_ATTIPATT
    endif
    if not(this.oPgFrm.Page1.oPag.oATGRUPAR_1_22.value==this.w_ATGRUPAR)
      this.oPgFrm.Page1.oPag.oATGRUPAR_1_22.value=this.w_ATGRUPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESELEM_1_23.value==this.w_DESELEM)
      this.oPgFrm.Page1.oPag.oDESELEM_1_23.value=this.w_DESELEM
    endif
    if not(this.oPgFrm.Page1.oPag.oATCATCON_1_24.value==this.w_ATCATCON)
      this.oPgFrm.Page1.oPag.oATCATCON_1_24.value=this.w_ATCATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_25.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_25.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oATFLRAGG_1_26.RadioValue()==this.w_ATFLRAGG)
      this.oPgFrm.Page1.oPag.oATFLRAGG_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_1_27.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_1_27.value=this.w_DESIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMDES_1_28.value==this.w_NOMDES)
      this.oPgFrm.Page1.oPag.oNOMDES_1_28.value=this.w_NOMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_29.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_29.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_34.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_34.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_49.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_49.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'GEST_ATTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ATDATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATDATINI_1_6.SetFocus()
            i_bnoObbl = !empty(.w_ATDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ATDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATDATFIN_1_7.SetFocus()
            i_bnoObbl = !empty(.w_ATDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATDOC<>'OR' AND .w_FLVEAC='V')  and not(empty(.w_ATCAUDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCAUDOC_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti")
          case   not(.w_TIPRIS='G')  and not(empty(.w_ATGRUPAR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATGRUPAR_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAG_MDG.CheckForm()
      if i_bres
        i_bres=  .GSAG_MDG.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ATDATINI = this.w_ATDATINI
    * --- GSAG_MDG : Depends On
    this.GSAG_MDG.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsag_agaPag1 as StdContainer
  Width  = 551
  height = 319
  stdWidth  = 551
  stdheight = 319
  resizeXpos=302
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATDATINI_1_6 as StdField with uid="HCTJUYEBZH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ATDATINI", cQueryName = "ATDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 67407537,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=88, Top=16

  add object oATDATFIN_1_7 as StdField with uid="PGDQKRPGHD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ATDATFIN", cQueryName = "ATDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione",;
    HelpContextID = 150696276,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=214, Top=16

  add object oATDESCRI_1_8 as StdField with uid="HSYBOMHEHF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ATDESCRI", cQueryName = "ATDESCRI",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione piano",;
    HelpContextID = 168857265,;
   bGlobalFont=.t.,;
    Height=21, Width=460, Left=88, Top=44, InputMask=replicate('X',60)

  add object oATCODCLI_1_12 as StdField with uid="HZUPUNWBNF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ATCODCLI", cQueryName = "ATCODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Cliente",;
    HelpContextID = 183934641,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=88, Top=72, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_ATCODCLI"

  func oATCODCLI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
      if .not. empty(.w_ATCODSED)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oATCODCLI_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODCLI_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oATCODCLI_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oATCODCLI_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_ATCODCLI
     i_obj.ecpSave()
  endproc

  add object oATCODSED_1_16 as StdField with uid="URMAOGUDOI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ATCODSED", cQueryName = "ATCODSED",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Sede in cui si trova l'impianto",;
    HelpContextID = 84500810,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=88, Top=100, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="DDCODICE", oKey_2_2="this.w_ATCODCLI", oKey_3_1="DDCODDES", oKey_3_2="this.w_ATCODSED"

  func oATCODSED_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODSED_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODSED_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_ATCODCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_ATCODCLI)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oATCODSED_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sedi",'',this.parent.oContained
  endproc

  add object oATCODIMP_1_19 as StdField with uid="THUSWJVHDR",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ATCODIMP", cQueryName = "ATCODIMP",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice impianto",;
    HelpContextID = 83271338,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=88, Top=128, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", cZoomOnZoom="GSAG_MIM", oKey_1_1="IMCODICE", oKey_1_2="this.w_ATCODIMP"

  func oATCODIMP_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODIMP_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODIMP_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oATCODIMP_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIM',"Impianti",'GSAG_KTR.IMP_MAST_VZM',this.parent.oContained
  endproc
  proc oATCODIMP_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_ATCODIMP
     i_obj.ecpSave()
  endproc

  add object oATCAUDOC_1_20 as StdField with uid="FPLZSQJDZF",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ATCAUDOC", cQueryName = "ATCAUDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente o di tipo ordine e/o del ciclo acquisti",;
    ToolTipText = "Causale documento",;
    HelpContextID = 150249143,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=88, Top=156, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_ATCAUDOC"

  func oATCAUDOC_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCAUDOC_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCAUDOC_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oATCAUDOC_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oATCAUDOC_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_ATCAUDOC
     i_obj.ecpSave()
  endproc

  add object oATTIPATT_1_21 as StdField with uid="EBIXXZOTWW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ATTIPATT", cQueryName = "ATTIPATT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 63205722,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=88, Top=184, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_ATTIPATT"

  func oATTIPATT_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTIPATT_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTIPATT_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oATTIPATT_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'GSAG1AAT.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oATTIPATT_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ATTIPATT
     i_obj.ecpSave()
  endproc

  add object oATGRUPAR_1_22 as StdField with uid="KRVQRVIADD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ATGRUPAR", cQueryName = "ATGRUPAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo partecipanti",;
    HelpContextID = 216227496,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=88, Top=212, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_ATGRUPAR"

  func oATGRUPAR_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oATGRUPAR_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATGRUPAR_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oATGRUPAR_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oATGRUPAR_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_ATGRUPAR
     i_obj.ecpSave()
  endproc

  add object oDESELEM_1_23 as StdField with uid="LAMEQNOKPK",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESELEM", cQueryName = "DESELEM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 142585290,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=151, Top=240, InputMask=replicate('X',60)

  add object oATCATCON_1_24 as StdField with uid="ZDAIBFEAUU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ATCATCON", cQueryName = "ATCATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contratto",;
    HelpContextID = 168074924,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=88, Top=240, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_ELEM", cZoomOnZoom="GSAG_ACE", oKey_1_1="CECODELE", oKey_1_2="this.w_ATCATCON"

  func oATCATCON_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCATCON_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCATCON_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ELEM','*','CECODELE',cp_AbsName(this.parent,'oATCATCON_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACE',"Categorie elementi",'',this.parent.oContained
  endproc
  proc oATCATCON_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODELE=this.parent.oContained.w_ATCATCON
     i_obj.ecpSave()
  endproc

  add object oDESCLI_1_25 as StdField with uid="IPYBIVGFJI",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 75607498,;
   bGlobalFont=.t.,;
    Height=21, Width=329, Left=219, Top=72, InputMask=replicate('X',60)

  add object oATFLRAGG_1_26 as StdCheck with uid="EJVKCVFCJL",rtseq=20,rtrep=.f.,left=88, top=269, caption="Raggruppa per prestazione",;
    ToolTipText = "Se attivo raggruppa per prestazione",;
    HelpContextID = 65442125,;
    cFormVar="w_ATFLRAGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATFLRAGG_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATFLRAGG_1_26.GetRadio()
    this.Parent.oContained.w_ATFLRAGG = this.RadioValue()
    return .t.
  endfunc

  func oATFLRAGG_1_26.SetRadio()
    this.Parent.oContained.w_ATFLRAGG=trim(this.Parent.oContained.w_ATFLRAGG)
    this.value = ;
      iif(this.Parent.oContained.w_ATFLRAGG=='S',1,;
      0)
  endfunc

  add object oDESIMP_1_27 as StdField with uid="OWGQXUUFVS",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 225160650,;
   bGlobalFont=.t.,;
    Height=21, Width=351, Left=197, Top=128, InputMask=replicate('X',50)

  add object oNOMDES_1_28 as StdField with uid="TGPRZPGZSU",rtseq=22,rtrep=.f.,;
    cFormVar = "w_NOMDES", cQueryName = "NOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 183567146,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=170, Top=101, InputMask=replicate('X',50)

  add object oDESDOC_1_29 as StdField with uid="YTQOVOWVBP",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 173059530,;
   bGlobalFont=.t.,;
    Height=21, Width=390, Left=158, Top=156, InputMask=replicate('X',35)

  add object oDESATT_1_34 as StdField with uid="TDUZEQYLCF",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 151236042,;
   bGlobalFont=.t.,;
    Height=21, Width=290, Left=258, Top=184, InputMask=replicate('X',254)


  add object oLinkPC_1_36 as StdButton with uid="CDVPQMYGOD",left=315, top=271, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al dettaglio delle attivit� generate";
    , HelpContextID = 83787105;
    , tabstop=.f.,caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_36.Click()
      this.Parent.oContained.GSAG_MDG.LinkPCClick()
    endproc

  func oLinkPC_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.T.)
     endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="QJZVDJKIQR",left=449, top=271, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 225855718;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_38 as StdButton with uid="KZSQCYUNAO",left=500, top=271, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 233144390;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_40 as cp_runprogram with uid="MNWXUTUMZL",left=117, top=334, width=187,height=19,;
    caption='GSAG_BTT(G)',;
   bGlobalFont=.t.,;
    prg="GSAG_BTT('G')",;
    cEvent = "Record Inserted",;
    nPag=1;
    , ToolTipText = "Lancio l'elaborazione...";
    , HelpContextID = 95689274


  add object oBtn_1_46 as StdButton with uid="KSUWNHATXF",left=398, top=271, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla visualizzazione delle attivit� generate";
    , HelpContextID = 268246928;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_46.Click()
      with this.Parent.oContained
        do GSAG1BZM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_46.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' and !empty(.w_ATSERIAL))
      endwith
    endif
  endfunc

  add object oDESCRI_1_49 as StdField with uid="ZCOWDQPSIQ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 69316042,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=151, Top=212, InputMask=replicate('X',60)


  add object oObj_1_50 as cp_runprogram with uid="YXZOEHJFAL",left=145, top=359, width=187,height=19,;
    caption='GSAG_BTT(D)',;
   bGlobalFont=.t.,;
    prg="GSAG_BTT('D')",;
    cEvent = "HasEvent",;
    nPag=1;
    , HelpContextID = 95688506

  add object oStr_1_9 as StdString with uid="LJGSIDFNMH",Visible=.t., Left=51, Top=16,;
    Alignment=1, Width=34, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="FEBMROAAYF",Visible=.t., Left=186, Top=16,;
    Alignment=1, Width=23, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="UGBWLCAQFR",Visible=.t., Left=20, Top=241,;
    Alignment=1, Width=63, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="IIGGHOEITM",Visible=.t., Left=41, Top=72,;
    Alignment=1, Width=42, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="WTCKKVSSQT",Visible=.t., Left=37, Top=101,;
    Alignment=1, Width=46, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="NKZPFUXEFA",Visible=.t., Left=27, Top=129,;
    Alignment=1, Width=56, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="DXIBTHNVUL",Visible=.t., Left=33, Top=157,;
    Alignment=1, Width=50, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="TILBEWBMER",Visible=.t., Left=3, Top=185,;
    Alignment=1, Width=80, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="GLUKIDYUKY",Visible=.t., Left=39, Top=213,;
    Alignment=1, Width=44, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="PBOJUDERRP",Visible=.t., Left=3, Top=44,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_aga','GEST_ATTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ATSERIAL=GEST_ATTI.ATSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
