* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ame                                                        *
*              Modelli elementi                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-08-05                                                      *
* Last revis.: 2015-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_ame"))

* --- Class definition
define class tgsag_ame as StdForm
  Top    = -1
  Left   = 9

  * --- Standard Properties
  Width  = 781
  Height = 577+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-30"
  HelpContextID=210380649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=88

  * --- Constant Properties
  MOD_ELEM_IDX = 0
  CAT_ELEM_IDX = 0
  MODCLDAT_IDX = 0
  ART_ICOL_IDX = 0
  VALUTE_IDX = 0
  LISTINI_IDX = 0
  UNIMIS_IDX = 0
  KEY_ARTI_IDX = 0
  CAUMATTI_IDX = 0
  TIP_DOCU_IDX = 0
  DIPENDEN_IDX = 0
  VOC_COST_IDX = 0
  CENCOST_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  PAR_AGEN_IDX = 0
  cFile = "MOD_ELEM"
  cKeySelect = "MOCODICE"
  cKeyWhere  = "MOCODICE=this.w_MOCODICE"
  cKeyWhereODBC = '"MOCODICE="+cp_ToStrODBC(this.w_MOCODICE)';

  cKeyWhereODBCqualified = '"MOD_ELEM.MOCODICE="+cp_ToStrODBC(this.w_MOCODICE)';

  cPrg = "gsag_ame"
  cComment = "Modelli elementi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_CAUDOC = space(5)
  w_TIPATT = space(20)
  w_GRUPAR = space(5)
  w_CODPER = space(3)
  w_CODDOC = space(3)
  w_MOCODICE = space(10)
  w_MODESCRI = space(60)
  w_MOCATELE = space(5)
  w_MOTIPCON = space(1)
  o_MOTIPCON = space(1)
  w_MOCODLIS = space(5)
  w_MOCODSER = space(20)
  o_MOCODSER = space(20)
  w_MOCODSER = space(20)
  w_MOTIPAPL = space(1)
  w_MOQTAMOV = 0
  w_MOFLATTI = space(1)
  o_MOFLATTI = space(1)
  w_MOPERATT = space(3)
  w_MOGIOATT = 0
  w_MOTIPATT = space(20)
  o_MOTIPATT = space(20)
  w_MOGRUPAR = space(5)
  w_MOCHKDAT = space(1)
  o_MOCHKDAT = space(1)
  w_MODESATT = space(0)
  w_MOFLFATT = space(1)
  o_MOFLFATT = space(1)
  w_MOPERFAT = space(3)
  w_MOGIODOC = 0
  w_MOCAUDOC = space(5)
  w_MOPRIDOC = space(1)
  w_MOCHKDDO = space(1)
  o_MOCHKDDO = space(1)
  w_MODESDOC = space(0)
  w_MOESCRIN = space(1)
  w_MORINCON = space(1)
  o_MORINCON = space(1)
  w_MONUMGIO = 0
  w_MOPERCON = space(3)
  w_MOGESRAT = space(1)
  w_FLNSAP = space(1)
  w_UNMIS3 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS1 = space(3)
  w_FLSERG = space(1)
  w_TIPART = space(2)
  w_MOUNIMIS = space(3)
  w_MOCODVAL = space(3)
  w_DESELE = space(60)
  w_DESVAL = space(35)
  w_DESLIS = space(40)
  w_DESUNI = space(35)
  w_DESATT = space(254)
  w_MOPRIDAT = space(1)
  w_CARAGGST = space(10)
  w_DESDOC = space(35)
  w_PATIPRIS = space(1)
  w_MDDATVAL = ctod('  /  /  ')
  w_MODTOBSO = ctod('  /  /  ')
  w_IVALIS = space(1)
  w_FLSCON = space(1)
  w_VALLIS = space(3)
  w_CATDOC = space(2)
  w_FLVEAC = space(1)
  w_TIPRIS = space(1)
  w_CICLO = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_FLRIPS = space(1)
  w_PCESCRIN = space(1)
  w_PCPERCON = space(3)
  w_PCRINCON = space(1)
  w_PCNUMGIO = 0
  w_ATTIPATT = space(1)
  w_DPDESCRI = space(60)
  w_VOCOBSO = ctod('  /  /  ')
  w_CENOBSO = ctod('  /  /  ')
  w_COMOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_MDDESCRI = space(50)
  w_DESART = space(60)
  w_MD__FREQ = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_MOVOCCEN = space(15)
  w_MOCODCEN = space(15)
  w_MOCOCOMM = space(15)
  o_MOCOCOMM = space(15)
  w_MOCODATT = space(15)
  w_DESVOC = space(40)
  w_DESCEN = space(40)
  w_DESCOMM = space(100)
  w_DESATTI = space(100)
  w_MODATFIS = space(1)
  w_PADATFIS = space(1)
  w_PADESATT = space(0)
  w_PADESDOC = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_ELEM','gsag_ame')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_amePag1","gsag_ame",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Modelli elementi")
      .Pages(1).HelpContextID = 191188895
      .Pages(2).addobject("oPag","tgsag_amePag2","gsag_ame",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Analitica")
      .Pages(2).HelpContextID = 25665159
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMOCODICE_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[16]
    this.cWorkTables[1]='CAT_ELEM'
    this.cWorkTables[2]='MODCLDAT'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='LISTINI'
    this.cWorkTables[6]='UNIMIS'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='CAUMATTI'
    this.cWorkTables[9]='TIP_DOCU'
    this.cWorkTables[10]='DIPENDEN'
    this.cWorkTables[11]='VOC_COST'
    this.cWorkTables[12]='CENCOST'
    this.cWorkTables[13]='CAN_TIER'
    this.cWorkTables[14]='ATTIVITA'
    this.cWorkTables[15]='PAR_AGEN'
    this.cWorkTables[16]='MOD_ELEM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(16))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_ELEM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_ELEM_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MOCODICE = NVL(MOCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    local link_1_27_joined
    link_1_27_joined=.f.
    local link_1_34_joined
    link_1_34_joined=.f.
    local link_1_45_joined
    link_1_45_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_ELEM where MOCODICE=KeySet.MOCODICE
    *
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_ELEM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_ELEM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_ELEM '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_27_joined=this.AddJoinedLink_1_27(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_34_joined=this.AddJoinedLink_1_34(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_45_joined=this.AddJoinedLink_1_45(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = I_CODAZI
        .w_CAUDOC = space(5)
        .w_TIPATT = space(20)
        .w_GRUPAR = space(5)
        .w_CODPER = space(3)
        .w_CODDOC = space(3)
        .w_FLNSAP = space(1)
        .w_UNMIS3 = space(3)
        .w_UNMIS2 = space(3)
        .w_UNMIS1 = space(3)
        .w_FLSERG = space(1)
        .w_TIPART = space(2)
        .w_DESELE = space(60)
        .w_DESVAL = space(35)
        .w_DESLIS = space(40)
        .w_DESUNI = space(35)
        .w_DESATT = space(254)
        .w_CARAGGST = space(10)
        .w_DESDOC = space(35)
        .w_PATIPRIS = 'G'
        .w_IVALIS = space(1)
        .w_FLSCON = space(1)
        .w_VALLIS = space(3)
        .w_CATDOC = space(2)
        .w_FLVEAC = space(1)
        .w_TIPRIS = space(1)
        .w_CICLO = 'E'
        .w_OB_TEST = I_DATSYS
        .w_FLRIPS = space(1)
        .w_PCESCRIN = space(1)
        .w_PCPERCON = space(3)
        .w_PCRINCON = space(1)
        .w_PCNUMGIO = 0
        .w_ATTIPATT = 'A'
        .w_DPDESCRI = space(60)
        .w_VOCOBSO = ctod("  /  /  ")
        .w_CENOBSO = ctod("  /  /  ")
        .w_COMOBSO = ctod("  /  /  ")
        .w_OBTEST = i_DATSYS
        .w_MDDESCRI = space(50)
        .w_DESART = space(60)
        .w_MD__FREQ = space(1)
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_DESVOC = space(40)
        .w_DESCEN = space(40)
        .w_DESCOMM = space(100)
        .w_DESATTI = space(100)
        .w_PADATFIS = space(1)
        .w_PADESATT = space(0)
        .w_PADESDOC = space(0)
          .link_1_1('Load')
        .w_MOCODICE = NVL(MOCODICE,space(10))
        .w_MODESCRI = NVL(MODESCRI,space(60))
        .w_MOCATELE = NVL(MOCATELE,space(5))
          if link_1_10_joined
            this.w_MOCATELE = NVL(CECODELE110,NVL(this.w_MOCATELE,space(5)))
            this.w_DESELE = NVL(CEDESELE110,space(60))
          else
          .link_1_10('Load')
          endif
        .w_MOTIPCON = NVL(MOTIPCON,space(1))
        .w_MOCODLIS = NVL(MOCODLIS,space(5))
          if link_1_12_joined
            this.w_MOCODLIS = NVL(LSCODLIS112,NVL(this.w_MOCODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS112,space(40))
            this.w_IVALIS = NVL(LSIVALIS112,space(1))
            this.w_FLSCON = NVL(LSFLSCON112,space(1))
            this.w_VALLIS = NVL(LSVALLIS112,space(3))
          else
          .link_1_12('Load')
          endif
        .w_MOCODSER = NVL(MOCODSER,space(20))
          if link_1_13_joined
            this.w_MOCODSER = NVL(ARCODART113,NVL(this.w_MOCODSER,space(20)))
            this.w_DESART = NVL(ARDESART113,space(60))
            this.w_TIPART = NVL(ARTIPART113,space(2))
            this.w_UNMIS1 = NVL(ARUNMIS1113,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2113,space(3))
            this.w_FLSERG = NVL(ARFLSERG113,space(1))
            this.w_MOUNIMIS = NVL(ARUNMIS1113,space(3))
          else
          .link_1_13('Load')
          endif
        .w_MOCODSER = NVL(MOCODSER,space(20))
          if link_1_14_joined
            this.w_MOCODSER = NVL(ARCODART114,NVL(this.w_MOCODSER,space(20)))
            this.w_DESART = NVL(ARDESART114,space(60))
            this.w_TIPART = NVL(ARTIPART114,space(2))
            this.w_UNMIS1 = NVL(ARUNMIS1114,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2114,space(3))
            this.w_FLSERG = NVL(ARFLSERG114,space(1))
            this.w_MOUNIMIS = NVL(ARUNMIS1114,space(3))
          else
          .link_1_14('Load')
          endif
        .w_MOTIPAPL = NVL(MOTIPAPL,space(1))
        .w_MOQTAMOV = NVL(MOQTAMOV,0)
        .w_MOFLATTI = NVL(MOFLATTI,space(1))
        .w_MOPERATT = NVL(MOPERATT,space(3))
          * evitabile
          *.link_1_18('Load')
        .w_MOGIOATT = NVL(MOGIOATT,0)
        .w_MOTIPATT = NVL(MOTIPATT,space(20))
          if link_1_20_joined
            this.w_MOTIPATT = NVL(CACODICE120,NVL(this.w_MOTIPATT,space(20)))
            this.w_DESATT = NVL(CADESCRI120,space(254))
            this.w_CARAGGST = NVL(CARAGGST120,space(10))
            this.w_FLNSAP = NVL(CAFLNSAP120,space(1))
          else
          .link_1_20('Load')
          endif
        .w_MOGRUPAR = NVL(MOGRUPAR,space(5))
          if link_1_21_joined
            this.w_MOGRUPAR = NVL(DPCODICE121,NVL(this.w_MOGRUPAR,space(5)))
            this.w_TIPRIS = NVL(DPTIPRIS121,space(1))
            this.w_DPDESCRI = NVL(DPDESCRI121,space(60))
          else
          .link_1_21('Load')
          endif
        .w_MOCHKDAT = NVL(MOCHKDAT,space(1))
        .w_MODESATT = NVL(MODESATT,space(0))
        .w_MOFLFATT = NVL(MOFLFATT,space(1))
        .w_MOPERFAT = NVL(MOPERFAT,space(3))
          if link_1_25_joined
            this.w_MOPERFAT = NVL(MDCODICE125,NVL(this.w_MOPERFAT,space(3)))
            this.w_MD__FREQ = NVL(MD__FREQ125,space(1))
          else
          .link_1_25('Load')
          endif
        .w_MOGIODOC = NVL(MOGIODOC,0)
        .w_MOCAUDOC = NVL(MOCAUDOC,space(5))
          if link_1_27_joined
            this.w_MOCAUDOC = NVL(TDTIPDOC127,NVL(this.w_MOCAUDOC,space(5)))
            this.w_DESDOC = NVL(TDDESDOC127,space(35))
            this.w_CATDOC = NVL(TDCATDOC127,space(2))
            this.w_FLVEAC = NVL(TDFLVEAC127,space(1))
            this.w_FLRIPS = NVL(TDFLRIPS127,space(1))
          else
          .link_1_27('Load')
          endif
        .w_MOPRIDOC = NVL(MOPRIDOC,space(1))
        .w_MOCHKDDO = NVL(MOCHKDDO,space(1))
        .w_MODESDOC = NVL(MODESDOC,space(0))
        .w_MOESCRIN = NVL(MOESCRIN,space(1))
        .w_MORINCON = NVL(MORINCON,space(1))
        .w_MONUMGIO = NVL(MONUMGIO,0)
        .w_MOPERCON = NVL(MOPERCON,space(3))
          if link_1_34_joined
            this.w_MOPERCON = NVL(MDCODICE134,NVL(this.w_MOPERCON,space(3)))
            this.w_MDDESCRI = NVL(MDDESCRI134,space(50))
          else
          .link_1_34('Load')
          endif
        .w_MOGESRAT = NVL(MOGESRAT,space(1))
        .w_MOUNIMIS = NVL(MOUNIMIS,space(3))
        .w_MOCODVAL = NVL(MOCODVAL,space(3))
          if link_1_45_joined
            this.w_MOCODVAL = NVL(VACODVAL145,NVL(this.w_MOCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL145,space(35))
          else
          .link_1_45('Load')
          endif
        .w_MOPRIDAT = NVL(MOPRIDAT,space(1))
        .w_MDDATVAL = NVL(cp_ToDate(MDDATVAL),ctod("  /  /  "))
        .w_MODTOBSO = NVL(cp_ToDate(MODTOBSO),ctod("  /  /  "))
        .w_MOVOCCEN = NVL(MOVOCCEN,space(15))
          if link_2_3_joined
            this.w_MOVOCCEN = NVL(VCCODICE203,NVL(this.w_MOVOCCEN,space(15)))
            this.w_DESVOC = NVL(VCDESCRI203,space(40))
            this.w_VOCOBSO = NVL(cp_ToDate(VCDTOBSO203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
        .w_MOCODCEN = NVL(MOCODCEN,space(15))
          if link_2_4_joined
            this.w_MOCODCEN = NVL(CC_CONTO204,NVL(this.w_MOCODCEN,space(15)))
            this.w_DESCEN = NVL(CCDESPIA204,space(40))
            this.w_CENOBSO = NVL(cp_ToDate(CCDTOBSO204),ctod("  /  /  "))
          else
          .link_2_4('Load')
          endif
        .w_MOCOCOMM = NVL(MOCOCOMM,space(15))
          if link_2_5_joined
            this.w_MOCOCOMM = NVL(CNCODCAN205,NVL(this.w_MOCOCOMM,space(15)))
            this.w_DESCOMM = NVL(CNDESCAN205,space(100))
            this.w_COMOBSO = NVL(cp_ToDate(CNDTOBSO205),ctod("  /  /  "))
          else
          .link_2_5('Load')
          endif
        .w_MOCODATT = NVL(MOCODATT,space(15))
          .link_2_6('Load')
        .w_MODATFIS = NVL(MODATFIS,space(1))
        cp_LoadRecExtFlds(this,'MOD_ELEM')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_CAUDOC = space(5)
      .w_TIPATT = space(20)
      .w_GRUPAR = space(5)
      .w_CODPER = space(3)
      .w_CODDOC = space(3)
      .w_MOCODICE = space(10)
      .w_MODESCRI = space(60)
      .w_MOCATELE = space(5)
      .w_MOTIPCON = space(1)
      .w_MOCODLIS = space(5)
      .w_MOCODSER = space(20)
      .w_MOCODSER = space(20)
      .w_MOTIPAPL = space(1)
      .w_MOQTAMOV = 0
      .w_MOFLATTI = space(1)
      .w_MOPERATT = space(3)
      .w_MOGIOATT = 0
      .w_MOTIPATT = space(20)
      .w_MOGRUPAR = space(5)
      .w_MOCHKDAT = space(1)
      .w_MODESATT = space(0)
      .w_MOFLFATT = space(1)
      .w_MOPERFAT = space(3)
      .w_MOGIODOC = 0
      .w_MOCAUDOC = space(5)
      .w_MOPRIDOC = space(1)
      .w_MOCHKDDO = space(1)
      .w_MODESDOC = space(0)
      .w_MOESCRIN = space(1)
      .w_MORINCON = space(1)
      .w_MONUMGIO = 0
      .w_MOPERCON = space(3)
      .w_MOGESRAT = space(1)
      .w_FLNSAP = space(1)
      .w_UNMIS3 = space(3)
      .w_UNMIS2 = space(3)
      .w_UNMIS1 = space(3)
      .w_FLSERG = space(1)
      .w_TIPART = space(2)
      .w_MOUNIMIS = space(3)
      .w_MOCODVAL = space(3)
      .w_DESELE = space(60)
      .w_DESVAL = space(35)
      .w_DESLIS = space(40)
      .w_DESUNI = space(35)
      .w_DESATT = space(254)
      .w_MOPRIDAT = space(1)
      .w_CARAGGST = space(10)
      .w_DESDOC = space(35)
      .w_PATIPRIS = space(1)
      .w_MDDATVAL = ctod("  /  /  ")
      .w_MODTOBSO = ctod("  /  /  ")
      .w_IVALIS = space(1)
      .w_FLSCON = space(1)
      .w_VALLIS = space(3)
      .w_CATDOC = space(2)
      .w_FLVEAC = space(1)
      .w_TIPRIS = space(1)
      .w_CICLO = space(1)
      .w_OB_TEST = ctod("  /  /  ")
      .w_FLRIPS = space(1)
      .w_PCESCRIN = space(1)
      .w_PCPERCON = space(3)
      .w_PCRINCON = space(1)
      .w_PCNUMGIO = 0
      .w_ATTIPATT = space(1)
      .w_DPDESCRI = space(60)
      .w_VOCOBSO = ctod("  /  /  ")
      .w_CENOBSO = ctod("  /  /  ")
      .w_COMOBSO = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_MDDESCRI = space(50)
      .w_DESART = space(60)
      .w_MD__FREQ = space(1)
      .w_DTBSO_CAUMATTI = ctod("  /  /  ")
      .w_MOVOCCEN = space(15)
      .w_MOCODCEN = space(15)
      .w_MOCOCOMM = space(15)
      .w_MOCODATT = space(15)
      .w_DESVOC = space(40)
      .w_DESCEN = space(40)
      .w_DESCOMM = space(100)
      .w_DESATTI = space(100)
      .w_MODATFIS = space(1)
      .w_PADATFIS = space(1)
      .w_PADESATT = space(0)
      .w_PADESDOC = space(0)
      if .cFunction<>"Filter"
        .w_CODAZI = I_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
        .DoRTCalc(2,9,.f.)
          if not(empty(.w_MOCATELE))
          .link_1_10('Full')
          endif
        .w_MOTIPCON = 'C'
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_MOCODLIS))
          .link_1_12('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_MOCODSER))
          .link_1_13('Full')
          endif
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_MOCODSER))
          .link_1_14('Full')
          endif
        .w_MOTIPAPL = 'C'
        .w_MOQTAMOV = IIF(.w_MOTIPCON<>'P', 1, IIF( EMPTY(.w_MOQTAMOV) , 1, .w_MOQTAMOV) )
        .w_MOFLATTI = IIF(.w_MOTIPCON<>'P', IIF(EMPTY(.w_MOFLATTI), 'S', .w_MOFLATTI ), 'N' )
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_MOPERATT))
          .link_1_18('Full')
          endif
        .w_MOGIOATT = IIF(.w_MOTIPCON<>'P', .w_MOGIOATT, 0 )
        .w_MOTIPATT = iif(.w_MOFLATTI='S' AND .w_MOTIPCON<>'P', .w_TIPATT, Space(20) )
        .DoRTCalc(19,19,.f.)
          if not(empty(.w_MOTIPATT))
          .link_1_20('Full')
          endif
        .w_MOGRUPAR = iif(.w_MOFLATTI='S' AND .w_MOTIPCON<>'P', .w_GRUPAR, Space(5) )
        .DoRTCalc(20,20,.f.)
          if not(empty(.w_MOGRUPAR))
          .link_1_21('Full')
          endif
          .DoRTCalc(21,21,.f.)
        .w_MODESATT = IIF(.w_MOCHKDAT='S',.w_PADESATT,.w_MODESATT)
        .w_MOFLFATT = IIF( EMPTY(.w_MOFLFATT), 'S', .w_MOFLFATT)
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_MOPERFAT))
          .link_1_25('Full')
          endif
          .DoRTCalc(25,25,.f.)
        .w_MOCAUDOC = iif(.w_MOFLFATT='S',.w_CAUDOC,Space(5))
        .DoRTCalc(26,26,.f.)
          if not(empty(.w_MOCAUDOC))
          .link_1_27('Full')
          endif
        .w_MOPRIDOC = 'I'
          .DoRTCalc(28,28,.f.)
        .w_MODESDOC = IIF(.w_MOCHKDDO='S',.w_PADESDOC,.w_MODESDOC)
        .w_MOESCRIN = IIF( NOT EMPTY(.w_PCESCRIN), .w_PCESCRIN, "N" )
        .w_MORINCON = IIF( NOT EMPTY(.w_PCRINCON), .w_PCRINCON, "N" )
        .w_MONUMGIO = IIF(.w_MORINCON='S',.w_PCNUMGIO,0)
        .w_MOPERCON = .w_PCPERCON
        .DoRTCalc(33,33,.f.)
          if not(empty(.w_MOPERCON))
          .link_1_34('Full')
          endif
          .DoRTCalc(34,41,.f.)
        .w_MOCODVAL = g_PERVAL
        .DoRTCalc(42,42,.f.)
          if not(empty(.w_MOCODVAL))
          .link_1_45('Full')
          endif
          .DoRTCalc(43,47,.f.)
        .w_MOPRIDAT = 'I'
          .DoRTCalc(49,50,.f.)
        .w_PATIPRIS = 'G'
          .DoRTCalc(52,59,.f.)
        .w_CICLO = 'E'
        .w_OB_TEST = I_DATSYS
          .DoRTCalc(62,66,.f.)
        .w_ATTIPATT = 'A'
          .DoRTCalc(68,71,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(73,75,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_MOVOCCEN = cavocori( .w_MOCODSER, "R" )
        .DoRTCalc(77,77,.f.)
          if not(empty(.w_MOVOCCEN))
          .link_2_3('Full')
          endif
        .DoRTCalc(78,78,.f.)
          if not(empty(.w_MOCODCEN))
          .link_2_4('Full')
          endif
        .DoRTCalc(79,79,.f.)
          if not(empty(.w_MOCOCOMM))
          .link_2_5('Full')
          endif
        .w_MOCODATT = SPACE(15)
        .DoRTCalc(80,80,.f.)
          if not(empty(.w_MOCODATT))
          .link_2_6('Full')
          endif
          .DoRTCalc(81,84,.f.)
        .w_MODATFIS = IIF( NOT EMPTY(.w_PADATFIS), .w_PADATFIS, "N" )
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_ELEM')
    this.DoRTCalc(86,88,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMOCODICE_1_7.enabled = i_bVal
      .Page1.oPag.oMODESCRI_1_9.enabled = i_bVal
      .Page1.oPag.oMOCATELE_1_10.enabled = i_bVal
      .Page1.oPag.oMOTIPCON_1_11.enabled = i_bVal
      .Page1.oPag.oMOCODLIS_1_12.enabled = i_bVal
      .Page1.oPag.oMOCODSER_1_13.enabled = i_bVal
      .Page1.oPag.oMOCODSER_1_14.enabled = i_bVal
      .Page1.oPag.oMOTIPAPL_1_15.enabled = i_bVal
      .Page1.oPag.oMOQTAMOV_1_16.enabled = i_bVal
      .Page1.oPag.oMOFLATTI_1_17.enabled = i_bVal
      .Page1.oPag.oMOPERATT_1_18.enabled = i_bVal
      .Page1.oPag.oMOGIOATT_1_19.enabled = i_bVal
      .Page1.oPag.oMOTIPATT_1_20.enabled = i_bVal
      .Page1.oPag.oMOGRUPAR_1_21.enabled = i_bVal
      .Page1.oPag.oMOCHKDAT_1_22.enabled = i_bVal
      .Page1.oPag.oMODESATT_1_23.enabled = i_bVal
      .Page1.oPag.oMOFLFATT_1_24.enabled = i_bVal
      .Page1.oPag.oMOPERFAT_1_25.enabled = i_bVal
      .Page1.oPag.oMOGIODOC_1_26.enabled = i_bVal
      .Page1.oPag.oMOCAUDOC_1_27.enabled = i_bVal
      .Page1.oPag.oMOPRIDOC_1_28.enabled = i_bVal
      .Page1.oPag.oMOCHKDDO_1_29.enabled = i_bVal
      .Page1.oPag.oMODESDOC_1_30.enabled = i_bVal
      .Page1.oPag.oMOESCRIN_1_31.enabled = i_bVal
      .Page1.oPag.oMORINCON_1_32.enabled = i_bVal
      .Page1.oPag.oMONUMGIO_1_33.enabled = i_bVal
      .Page1.oPag.oMOPERCON_1_34.enabled = i_bVal
      .Page1.oPag.oMOPRIDAT_1_56.enabled = i_bVal
      .Page1.oPag.oMDDATVAL_1_63.enabled = i_bVal
      .Page1.oPag.oMODTOBSO_1_64.enabled = i_bVal
      .Page2.oPag.oMOVOCCEN_2_3.enabled = i_bVal
      .Page2.oPag.oMOCODCEN_2_4.enabled = i_bVal
      .Page2.oPag.oMOCOCOMM_2_5.enabled = i_bVal
      .Page2.oPag.oMOCODATT_2_6.enabled = i_bVal
      .Page1.oPag.oMODATFIS_1_113.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMOCODICE_1_7.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMOCODICE_1_7.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MOD_ELEM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODICE,"MOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODESCRI,"MODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCATELE,"MOCATELE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOTIPCON,"MOTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODLIS,"MOCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODSER,"MOCODSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODSER,"MOCODSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOTIPAPL,"MOTIPAPL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOQTAMOV,"MOQTAMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOFLATTI,"MOFLATTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPERATT,"MOPERATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOGIOATT,"MOGIOATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOTIPATT,"MOTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOGRUPAR,"MOGRUPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCHKDAT,"MOCHKDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODESATT,"MODESATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOFLFATT,"MOFLFATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPERFAT,"MOPERFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOGIODOC,"MOGIODOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCAUDOC,"MOCAUDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPRIDOC,"MOPRIDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCHKDDO,"MOCHKDDO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODESDOC,"MODESDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOESCRIN,"MOESCRIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MORINCON,"MORINCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MONUMGIO,"MONUMGIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPERCON,"MOPERCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOGESRAT,"MOGESRAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOUNIMIS,"MOUNIMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODVAL,"MOCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOPRIDAT,"MOPRIDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MDDATVAL,"MDDATVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODTOBSO,"MODTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOVOCCEN,"MOVOCCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODCEN,"MOCODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCOCOMM,"MOCOCOMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MOCODATT,"MOCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MODATFIS,"MODATFIS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    i_lTable = "MOD_ELEM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_ELEM_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAG_SME with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_ELEM_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_ELEM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_ELEM')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_ELEM')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MOCODICE,MODESCRI,MOCATELE,MOTIPCON,MOCODLIS"+;
                  ",MOCODSER,MOTIPAPL,MOQTAMOV,MOFLATTI,MOPERATT"+;
                  ",MOGIOATT,MOTIPATT,MOGRUPAR,MOCHKDAT,MODESATT"+;
                  ",MOFLFATT,MOPERFAT,MOGIODOC,MOCAUDOC,MOPRIDOC"+;
                  ",MOCHKDDO,MODESDOC,MOESCRIN,MORINCON,MONUMGIO"+;
                  ",MOPERCON,MOGESRAT,MOUNIMIS,MOCODVAL,MOPRIDAT"+;
                  ",MDDATVAL,MODTOBSO,MOVOCCEN,MOCODCEN,MOCOCOMM"+;
                  ",MOCODATT,MODATFIS "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MOCODICE)+;
                  ","+cp_ToStrODBC(this.w_MODESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_MOCATELE)+;
                  ","+cp_ToStrODBC(this.w_MOTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODLIS)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODSER)+;
                  ","+cp_ToStrODBC(this.w_MOTIPAPL)+;
                  ","+cp_ToStrODBC(this.w_MOQTAMOV)+;
                  ","+cp_ToStrODBC(this.w_MOFLATTI)+;
                  ","+cp_ToStrODBCNull(this.w_MOPERATT)+;
                  ","+cp_ToStrODBC(this.w_MOGIOATT)+;
                  ","+cp_ToStrODBCNull(this.w_MOTIPATT)+;
                  ","+cp_ToStrODBCNull(this.w_MOGRUPAR)+;
                  ","+cp_ToStrODBC(this.w_MOCHKDAT)+;
                  ","+cp_ToStrODBC(this.w_MODESATT)+;
                  ","+cp_ToStrODBC(this.w_MOFLFATT)+;
                  ","+cp_ToStrODBCNull(this.w_MOPERFAT)+;
                  ","+cp_ToStrODBC(this.w_MOGIODOC)+;
                  ","+cp_ToStrODBCNull(this.w_MOCAUDOC)+;
                  ","+cp_ToStrODBC(this.w_MOPRIDOC)+;
                  ","+cp_ToStrODBC(this.w_MOCHKDDO)+;
                  ","+cp_ToStrODBC(this.w_MODESDOC)+;
                  ","+cp_ToStrODBC(this.w_MOESCRIN)+;
                  ","+cp_ToStrODBC(this.w_MORINCON)+;
                  ","+cp_ToStrODBC(this.w_MONUMGIO)+;
                  ","+cp_ToStrODBCNull(this.w_MOPERCON)+;
                  ","+cp_ToStrODBC(this.w_MOGESRAT)+;
                  ","+cp_ToStrODBC(this.w_MOUNIMIS)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODVAL)+;
                  ","+cp_ToStrODBC(this.w_MOPRIDAT)+;
                  ","+cp_ToStrODBC(this.w_MDDATVAL)+;
                  ","+cp_ToStrODBC(this.w_MODTOBSO)+;
                  ","+cp_ToStrODBCNull(this.w_MOVOCCEN)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODCEN)+;
                  ","+cp_ToStrODBCNull(this.w_MOCOCOMM)+;
                  ","+cp_ToStrODBCNull(this.w_MOCODATT)+;
                  ","+cp_ToStrODBC(this.w_MODATFIS)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_ELEM')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_ELEM')
        cp_CheckDeletedKey(i_cTable,0,'MOCODICE',this.w_MOCODICE)
        INSERT INTO (i_cTable);
              (MOCODICE,MODESCRI,MOCATELE,MOTIPCON,MOCODLIS,MOCODSER,MOTIPAPL,MOQTAMOV,MOFLATTI,MOPERATT,MOGIOATT,MOTIPATT,MOGRUPAR,MOCHKDAT,MODESATT,MOFLFATT,MOPERFAT,MOGIODOC,MOCAUDOC,MOPRIDOC,MOCHKDDO,MODESDOC,MOESCRIN,MORINCON,MONUMGIO,MOPERCON,MOGESRAT,MOUNIMIS,MOCODVAL,MOPRIDAT,MDDATVAL,MODTOBSO,MOVOCCEN,MOCODCEN,MOCOCOMM,MOCODATT,MODATFIS  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MOCODICE;
                  ,this.w_MODESCRI;
                  ,this.w_MOCATELE;
                  ,this.w_MOTIPCON;
                  ,this.w_MOCODLIS;
                  ,this.w_MOCODSER;
                  ,this.w_MOTIPAPL;
                  ,this.w_MOQTAMOV;
                  ,this.w_MOFLATTI;
                  ,this.w_MOPERATT;
                  ,this.w_MOGIOATT;
                  ,this.w_MOTIPATT;
                  ,this.w_MOGRUPAR;
                  ,this.w_MOCHKDAT;
                  ,this.w_MODESATT;
                  ,this.w_MOFLFATT;
                  ,this.w_MOPERFAT;
                  ,this.w_MOGIODOC;
                  ,this.w_MOCAUDOC;
                  ,this.w_MOPRIDOC;
                  ,this.w_MOCHKDDO;
                  ,this.w_MODESDOC;
                  ,this.w_MOESCRIN;
                  ,this.w_MORINCON;
                  ,this.w_MONUMGIO;
                  ,this.w_MOPERCON;
                  ,this.w_MOGESRAT;
                  ,this.w_MOUNIMIS;
                  ,this.w_MOCODVAL;
                  ,this.w_MOPRIDAT;
                  ,this.w_MDDATVAL;
                  ,this.w_MODTOBSO;
                  ,this.w_MOVOCCEN;
                  ,this.w_MOCODCEN;
                  ,this.w_MOCOCOMM;
                  ,this.w_MOCODATT;
                  ,this.w_MODATFIS;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_ELEM_IDX,i_nConn)
      *
      * update MOD_ELEM
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_ELEM')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MODESCRI="+cp_ToStrODBC(this.w_MODESCRI)+;
             ",MOCATELE="+cp_ToStrODBCNull(this.w_MOCATELE)+;
             ",MOTIPCON="+cp_ToStrODBC(this.w_MOTIPCON)+;
             ",MOCODLIS="+cp_ToStrODBCNull(this.w_MOCODLIS)+;
             ",MOCODSER="+cp_ToStrODBCNull(this.w_MOCODSER)+;
             ",MOTIPAPL="+cp_ToStrODBC(this.w_MOTIPAPL)+;
             ",MOQTAMOV="+cp_ToStrODBC(this.w_MOQTAMOV)+;
             ",MOFLATTI="+cp_ToStrODBC(this.w_MOFLATTI)+;
             ",MOPERATT="+cp_ToStrODBCNull(this.w_MOPERATT)+;
             ",MOGIOATT="+cp_ToStrODBC(this.w_MOGIOATT)+;
             ",MOTIPATT="+cp_ToStrODBCNull(this.w_MOTIPATT)+;
             ",MOGRUPAR="+cp_ToStrODBCNull(this.w_MOGRUPAR)+;
             ",MOCHKDAT="+cp_ToStrODBC(this.w_MOCHKDAT)+;
             ",MODESATT="+cp_ToStrODBC(this.w_MODESATT)+;
             ",MOFLFATT="+cp_ToStrODBC(this.w_MOFLFATT)+;
             ",MOPERFAT="+cp_ToStrODBCNull(this.w_MOPERFAT)+;
             ",MOGIODOC="+cp_ToStrODBC(this.w_MOGIODOC)+;
             ",MOCAUDOC="+cp_ToStrODBCNull(this.w_MOCAUDOC)+;
             ",MOPRIDOC="+cp_ToStrODBC(this.w_MOPRIDOC)+;
             ",MOCHKDDO="+cp_ToStrODBC(this.w_MOCHKDDO)+;
             ",MODESDOC="+cp_ToStrODBC(this.w_MODESDOC)+;
             ",MOESCRIN="+cp_ToStrODBC(this.w_MOESCRIN)+;
             ",MORINCON="+cp_ToStrODBC(this.w_MORINCON)+;
             ",MONUMGIO="+cp_ToStrODBC(this.w_MONUMGIO)+;
             ",MOPERCON="+cp_ToStrODBCNull(this.w_MOPERCON)+;
             ",MOGESRAT="+cp_ToStrODBC(this.w_MOGESRAT)+;
             ",MOUNIMIS="+cp_ToStrODBC(this.w_MOUNIMIS)+;
             ",MOCODVAL="+cp_ToStrODBCNull(this.w_MOCODVAL)+;
             ",MOPRIDAT="+cp_ToStrODBC(this.w_MOPRIDAT)+;
             ",MDDATVAL="+cp_ToStrODBC(this.w_MDDATVAL)+;
             ",MODTOBSO="+cp_ToStrODBC(this.w_MODTOBSO)+;
             ",MOVOCCEN="+cp_ToStrODBCNull(this.w_MOVOCCEN)+;
             ",MOCODCEN="+cp_ToStrODBCNull(this.w_MOCODCEN)+;
             ",MOCOCOMM="+cp_ToStrODBCNull(this.w_MOCOCOMM)+;
             ",MOCODATT="+cp_ToStrODBCNull(this.w_MOCODATT)+;
             ",MODATFIS="+cp_ToStrODBC(this.w_MODATFIS)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_ELEM')
        i_cWhere = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
        UPDATE (i_cTable) SET;
              MODESCRI=this.w_MODESCRI;
             ,MOCATELE=this.w_MOCATELE;
             ,MOTIPCON=this.w_MOTIPCON;
             ,MOCODLIS=this.w_MOCODLIS;
             ,MOCODSER=this.w_MOCODSER;
             ,MOTIPAPL=this.w_MOTIPAPL;
             ,MOQTAMOV=this.w_MOQTAMOV;
             ,MOFLATTI=this.w_MOFLATTI;
             ,MOPERATT=this.w_MOPERATT;
             ,MOGIOATT=this.w_MOGIOATT;
             ,MOTIPATT=this.w_MOTIPATT;
             ,MOGRUPAR=this.w_MOGRUPAR;
             ,MOCHKDAT=this.w_MOCHKDAT;
             ,MODESATT=this.w_MODESATT;
             ,MOFLFATT=this.w_MOFLFATT;
             ,MOPERFAT=this.w_MOPERFAT;
             ,MOGIODOC=this.w_MOGIODOC;
             ,MOCAUDOC=this.w_MOCAUDOC;
             ,MOPRIDOC=this.w_MOPRIDOC;
             ,MOCHKDDO=this.w_MOCHKDDO;
             ,MODESDOC=this.w_MODESDOC;
             ,MOESCRIN=this.w_MOESCRIN;
             ,MORINCON=this.w_MORINCON;
             ,MONUMGIO=this.w_MONUMGIO;
             ,MOPERCON=this.w_MOPERCON;
             ,MOGESRAT=this.w_MOGESRAT;
             ,MOUNIMIS=this.w_MOUNIMIS;
             ,MOCODVAL=this.w_MOCODVAL;
             ,MOPRIDAT=this.w_MOPRIDAT;
             ,MDDATVAL=this.w_MDDATVAL;
             ,MODTOBSO=this.w_MODTOBSO;
             ,MOVOCCEN=this.w_MOVOCCEN;
             ,MOCODCEN=this.w_MOCODCEN;
             ,MOCOCOMM=this.w_MOCOCOMM;
             ,MOCODATT=this.w_MOCODATT;
             ,MODATFIS=this.w_MODATFIS;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_ELEM_IDX,i_nConn)
      *
      * delete MOD_ELEM
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MOCODICE',this.w_MOCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,11,.t.)
        if .o_MOTIPATT<>.w_MOTIPATT.or. .o_MOTIPCON<>.w_MOTIPCON
          .link_1_13('Full')
        endif
        if .o_MOTIPCON<>.w_MOTIPCON.or. .o_MOFLFATT<>.w_MOFLFATT.or. .o_MOFLATTI<>.w_MOFLATTI
          .link_1_14('Full')
        endif
        .DoRTCalc(14,14,.t.)
        if .o_MOTIPCON<>.w_MOTIPCON
            .w_MOQTAMOV = IIF(.w_MOTIPCON<>'P', 1, IIF( EMPTY(.w_MOQTAMOV) , 1, .w_MOQTAMOV) )
        endif
        if .o_MOTIPCON<>.w_MOTIPCON
            .w_MOFLATTI = IIF(.w_MOTIPCON<>'P', IIF(EMPTY(.w_MOFLATTI), 'S', .w_MOFLATTI ), 'N' )
        endif
        if .o_MOFLATTI<>.w_MOFLATTI.or. .o_MOTIPCON<>.w_MOTIPCON
          .link_1_18('Full')
        endif
        if .o_MOFLATTI<>.w_MOFLATTI.or. .o_MOTIPCON<>.w_MOTIPCON
            .w_MOGIOATT = IIF(.w_MOTIPCON<>'P', .w_MOGIOATT, 0 )
        endif
        if .o_MOFLATTI<>.w_MOFLATTI.or. .o_MOTIPCON<>.w_MOTIPCON
            .w_MOTIPATT = iif(.w_MOFLATTI='S' AND .w_MOTIPCON<>'P', .w_TIPATT, Space(20) )
          .link_1_20('Full')
        endif
        if .o_CODAZI<>.w_CODAZI.or. .o_MOFLATTI<>.w_MOFLATTI.or. .o_MOTIPCON<>.w_MOTIPCON
            .w_MOGRUPAR = iif(.w_MOFLATTI='S' AND .w_MOTIPCON<>'P', .w_GRUPAR, Space(5) )
          .link_1_21('Full')
        endif
        .DoRTCalc(21,21,.t.)
        if .o_MOCHKDAT<>.w_MOCHKDAT
            .w_MODESATT = IIF(.w_MOCHKDAT='S',.w_PADESATT,.w_MODESATT)
        endif
        if .o_MOTIPCON<>.w_MOTIPCON
            .w_MOFLFATT = IIF( EMPTY(.w_MOFLFATT), 'S', .w_MOFLFATT)
        endif
        if .o_MOFLFATT<>.w_MOFLFATT.or. .o_MOTIPCON<>.w_MOTIPCON
          .link_1_25('Full')
        endif
        .DoRTCalc(25,25,.t.)
        if .o_MOFLFATT<>.w_MOFLFATT
            .w_MOCAUDOC = iif(.w_MOFLFATT='S',.w_CAUDOC,Space(5))
          .link_1_27('Full')
        endif
        .DoRTCalc(27,28,.t.)
        if .o_MOCHKDDO<>.w_MOCHKDDO
            .w_MODESDOC = IIF(.w_MOCHKDDO='S',.w_PADESDOC,.w_MODESDOC)
        endif
        .DoRTCalc(30,31,.t.)
        if .o_MORINCON<>.w_MORINCON
            .w_MONUMGIO = IIF(.w_MORINCON='S',.w_PCNUMGIO,0)
        endif
        .DoRTCalc(33,41,.t.)
          .link_1_45('Full')
        .DoRTCalc(43,76,.t.)
        if .o_MOCODSER<>.w_MOCODSER
            .w_MOVOCCEN = cavocori( .w_MOCODSER, "R" )
          .link_2_3('Full')
        endif
        .DoRTCalc(78,79,.t.)
        if .o_MOCOCOMM<>.w_MOCOCOMM
            .w_MOCODATT = SPACE(15)
          .link_2_6('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(81,88,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_SQUKATBIFE()
    with this
          * --- Resetto la periodicit� e i giorni di toll. doc.
          .w_MOPERFAT = ''
          .w_MOGIODOC = 0
    endwith
  endproc
  proc Calculate_AFLIRZKTME()
    with this
          * --- Resetto la periodicit� e i giorni di toll. att.
          .w_MOPERATT = ''
          .w_MOGIOATT = 0
    endwith
  endproc
  proc Calculate_HBWXDEHEWF()
    with this
          * --- Sbianca periodicit� - rinnovo tacito - giorni di preavviso
          .w_MOPERCON = IIF( .w_MOESCRIN = "S", "   ", .w_MOPERCON )
          .link_1_34('Full')
          .w_MORINCON = IIF( .w_MOESCRIN = "S", "N", .w_MORINCON )
          .w_MONUMGIO = IIF( .w_MOESCRIN = "S", 0, .w_MONUMGIO )
          .w_MODATFIS = IIF( .w_MOESCRIN = "S", "N", .w_MODATFIS )
    endwith
  endproc
  proc Calculate_QYHSDBHBJI()
    with this
          * --- Sbianca descrizione
          .w_MODESATT = IIF(.w_MOFLATTI='S',.w_MODESATT,' ')
          .w_MODESDOC = IIF(.w_MOFLFATT='S',.w_MODESDOC,' ')
          .w_MOCHKDAT = IIF(.w_MOFLATTI='S',.w_MOCHKDAT,'N')
          .w_MOCHKDDO = IIF(.w_MOFLFATT='S',.w_MOCHKDDO,'N')
    endwith
  endproc
  proc Calculate_VTKARXIKCS()
    with this
          * --- Sbianca descrizione
          .w_MODESATT = IIF(.w_MOCHKDAT='S',.w_MODESATT,' ')
          .w_MODESDOC = IIF(.w_MOCHKDDO='S',.w_MODESDOC,' ')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMOTIPCON_1_11.enabled = this.oPgFrm.Page1.oPag.oMOTIPCON_1_11.mCond()
    this.oPgFrm.Page1.oPag.oMOCODSER_1_13.enabled = this.oPgFrm.Page1.oPag.oMOCODSER_1_13.mCond()
    this.oPgFrm.Page1.oPag.oMOCODSER_1_14.enabled = this.oPgFrm.Page1.oPag.oMOCODSER_1_14.mCond()
    this.oPgFrm.Page1.oPag.oMOFLATTI_1_17.enabled = this.oPgFrm.Page1.oPag.oMOFLATTI_1_17.mCond()
    this.oPgFrm.Page1.oPag.oMOPERATT_1_18.enabled = this.oPgFrm.Page1.oPag.oMOPERATT_1_18.mCond()
    this.oPgFrm.Page1.oPag.oMOGIOATT_1_19.enabled = this.oPgFrm.Page1.oPag.oMOGIOATT_1_19.mCond()
    this.oPgFrm.Page1.oPag.oMOTIPATT_1_20.enabled = this.oPgFrm.Page1.oPag.oMOTIPATT_1_20.mCond()
    this.oPgFrm.Page1.oPag.oMOGRUPAR_1_21.enabled = this.oPgFrm.Page1.oPag.oMOGRUPAR_1_21.mCond()
    this.oPgFrm.Page1.oPag.oMOCHKDAT_1_22.enabled = this.oPgFrm.Page1.oPag.oMOCHKDAT_1_22.mCond()
    this.oPgFrm.Page1.oPag.oMODESATT_1_23.enabled = this.oPgFrm.Page1.oPag.oMODESATT_1_23.mCond()
    this.oPgFrm.Page1.oPag.oMOPERFAT_1_25.enabled = this.oPgFrm.Page1.oPag.oMOPERFAT_1_25.mCond()
    this.oPgFrm.Page1.oPag.oMOGIODOC_1_26.enabled = this.oPgFrm.Page1.oPag.oMOGIODOC_1_26.mCond()
    this.oPgFrm.Page1.oPag.oMOCAUDOC_1_27.enabled = this.oPgFrm.Page1.oPag.oMOCAUDOC_1_27.mCond()
    this.oPgFrm.Page1.oPag.oMOCHKDDO_1_29.enabled = this.oPgFrm.Page1.oPag.oMOCHKDDO_1_29.mCond()
    this.oPgFrm.Page1.oPag.oMODESDOC_1_30.enabled = this.oPgFrm.Page1.oPag.oMODESDOC_1_30.mCond()
    this.oPgFrm.Page1.oPag.oMORINCON_1_32.enabled = this.oPgFrm.Page1.oPag.oMORINCON_1_32.mCond()
    this.oPgFrm.Page1.oPag.oMONUMGIO_1_33.enabled = this.oPgFrm.Page1.oPag.oMONUMGIO_1_33.mCond()
    this.oPgFrm.Page1.oPag.oMOPERCON_1_34.enabled = this.oPgFrm.Page1.oPag.oMOPERCON_1_34.mCond()
    this.oPgFrm.Page1.oPag.oMODATFIS_1_113.enabled = this.oPgFrm.Page1.oPag.oMODATFIS_1_113.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page1.oPag.oMODESATT_1_23
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page1.oPag.oMODESDOC_1_30
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMOCODLIS_1_12.visible=!this.oPgFrm.Page1.oPag.oMOCODLIS_1_12.mHide()
    this.oPgFrm.Page1.oPag.oMOCODSER_1_13.visible=!this.oPgFrm.Page1.oPag.oMOCODSER_1_13.mHide()
    this.oPgFrm.Page1.oPag.oMOCODSER_1_14.visible=!this.oPgFrm.Page1.oPag.oMOCODSER_1_14.mHide()
    this.oPgFrm.Page1.oPag.oMOTIPAPL_1_15.visible=!this.oPgFrm.Page1.oPag.oMOTIPAPL_1_15.mHide()
    this.oPgFrm.Page1.oPag.oMOQTAMOV_1_16.visible=!this.oPgFrm.Page1.oPag.oMOQTAMOV_1_16.mHide()
    this.oPgFrm.Page1.oPag.oUNMIS1_1_41.visible=!this.oPgFrm.Page1.oPag.oUNMIS1_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS_1_52.visible=!this.oPgFrm.Page1.oPag.oDESLIS_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_103.visible=!this.oPgFrm.Page1.oPag.oStr_1_103.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_105.visible=!this.oPgFrm.Page1.oPag.oStr_1_105.mHide()
    this.oPgFrm.Page2.oPag.oMOVOCCEN_2_3.visible=!this.oPgFrm.Page2.oPag.oMOVOCCEN_2_3.mHide()
    this.oPgFrm.Page2.oPag.oMOCODCEN_2_4.visible=!this.oPgFrm.Page2.oPag.oMOCODCEN_2_4.mHide()
    this.oPgFrm.Page2.oPag.oMOCOCOMM_2_5.visible=!this.oPgFrm.Page2.oPag.oMOCOCOMM_2_5.mHide()
    this.oPgFrm.Page2.oPag.oMOCODATT_2_6.visible=!this.oPgFrm.Page2.oPag.oMOCODATT_2_6.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_7.visible=!this.oPgFrm.Page2.oPag.oStr_2_7.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_8.visible=!this.oPgFrm.Page2.oPag.oStr_2_8.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_9.visible=!this.oPgFrm.Page2.oPag.oStr_2_9.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_10.visible=!this.oPgFrm.Page2.oPag.oStr_2_10.mHide()
    this.oPgFrm.Page2.oPag.oDESVOC_2_11.visible=!this.oPgFrm.Page2.oPag.oDESVOC_2_11.mHide()
    this.oPgFrm.Page2.oPag.oDESCEN_2_12.visible=!this.oPgFrm.Page2.oPag.oDESCEN_2_12.mHide()
    this.oPgFrm.Page2.oPag.oDESCOMM_2_13.visible=!this.oPgFrm.Page2.oPag.oDESCOMM_2_13.mHide()
    this.oPgFrm.Page2.oPag.oDESATTI_2_14.visible=!this.oPgFrm.Page2.oPag.oDESATTI_2_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_MOFLFATT Changed")
          .Calculate_SQUKATBIFE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_MOFLATTI Changed")
          .Calculate_AFLIRZKTME()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_MOESCRIN Changed")
          .Calculate_HBWXDEHEWF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_MOFLATTI Changed") or lower(cEvent)==lower("w_MOFLFATT Changed")
          .Calculate_QYHSDBHBJI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_MOCHKDAT Changed") or lower(cEvent)==lower("w_MOCHKDDO Changed")
          .Calculate_VTKARXIKCS()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PCTIPDOC,PCTIPATT,PCGRUPAR,PCESCRIN,PCPERCON,PCRINCON,PCNUMGIO,PADATFIS,PADESATT,PADESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_CODAZI)
            select PACODAZI,PCTIPDOC,PCTIPATT,PCGRUPAR,PCESCRIN,PCPERCON,PCRINCON,PCNUMGIO,PADATFIS,PADESATT,PADESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_CAUDOC = NVL(_Link_.PCTIPDOC,space(5))
      this.w_TIPATT = NVL(_Link_.PCTIPATT,space(20))
      this.w_GRUPAR = NVL(_Link_.PCGRUPAR,space(5))
      this.w_PCESCRIN = NVL(_Link_.PCESCRIN,space(1))
      this.w_PCPERCON = NVL(_Link_.PCPERCON,space(3))
      this.w_PCRINCON = NVL(_Link_.PCRINCON,space(1))
      this.w_PCNUMGIO = NVL(_Link_.PCNUMGIO,0)
      this.w_PADATFIS = NVL(_Link_.PADATFIS,space(1))
      this.w_PADESATT = NVL(_Link_.PADESATT,space(0))
      this.w_PADESDOC = NVL(_Link_.PADESDOC,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CAUDOC = space(5)
      this.w_TIPATT = space(20)
      this.w_GRUPAR = space(5)
      this.w_PCESCRIN = space(1)
      this.w_PCPERCON = space(3)
      this.w_PCRINCON = space(1)
      this.w_PCNUMGIO = 0
      this.w_PADATFIS = space(1)
      this.w_PADESATT = space(0)
      this.w_PADESDOC = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCATELE
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ELEM_IDX,3]
    i_lTable = "CAT_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2], .t., this.CAT_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCATELE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACE',True,'CAT_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODELE like "+cp_ToStrODBC(trim(this.w_MOCATELE)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODELE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODELE',trim(this.w_MOCATELE))
          select CECODELE,CEDESELE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODELE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCATELE)==trim(_Link_.CECODELE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCATELE) and !this.bDontReportError
            deferred_cp_zoom('CAT_ELEM','*','CECODELE',cp_AbsName(oSource.parent,'oMOCATELE_1_10'),i_cWhere,'GSAG_ACE',"Categorie elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                     +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',oSource.xKey(1))
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCATELE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                   +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(this.w_MOCATELE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',this.w_MOCATELE)
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCATELE = NVL(_Link_.CECODELE,space(5))
      this.w_DESELE = NVL(_Link_.CEDESELE,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_MOCATELE = space(5)
      endif
      this.w_DESELE = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.CECODELE,1)
      cp_ShowWarn(i_cKey,this.CAT_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCATELE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_ELEM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.CECODELE as CECODELE110"+ ",link_1_10.CEDESELE as CEDESELE110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on MOD_ELEM.MOCATELE=link_1_10.CECODELE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOCATELE=link_1_10.CECODELE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODLIS
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_MOCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_MOCODLIS))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oMOCODLIS_1_12'),i_cWhere,'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_MOCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_MOCODLIS)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_FLSCON = NVL(_Link_.LSFLSCON,space(1))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_IVALIS = space(1)
      this.w_FLSCON = space(1)
      this.w_VALLIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VALLIS=.w_MOCODVAL 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta listino incongruente ")
        endif
        this.w_MOCODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_IVALIS = space(1)
        this.w_FLSCON = space(1)
        this.w_VALLIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.LSCODLIS as LSCODLIS112"+ ",link_1_12.LSDESLIS as LSDESLIS112"+ ",link_1_12.LSIVALIS as LSIVALIS112"+ ",link_1_12.LSFLSCON as LSFLSCON112"+ ",link_1_12.LSVALLIS as LSVALLIS112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on MOD_ELEM.MOCODLIS=link_1_12.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOCODLIS=link_1_12.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODSER
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_MOCODSER)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_MOCODSER))
          select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODSER)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODSER) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oMOCODSER_1_13'),i_cWhere,'GSMA_AAS',"Servizi",'GSMA2AAS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MOCODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MOCODSER)
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODSER = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(60))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_MOUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODSER = space(20)
      endif
      this.w_DESART = space(60)
      this.w_TIPART = space(2)
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_MOUNIMIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MOTIPCON='C' AND(.w_TIPART='FO' OR .w_TIPART='FM')) OR (.w_MOTIPCON='P' AND .w_TIPART='FM')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Servizio n� a valore n� a quantit� e valore ")
        endif
        this.w_MOCODSER = space(20)
        this.w_DESART = space(60)
        this.w_TIPART = space(2)
        this.w_UNMIS1 = space(3)
        this.w_UNMIS2 = space(3)
        this.w_FLSERG = space(1)
        this.w_MOUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.ARCODART as ARCODART113"+ ",link_1_13.ARDESART as ARDESART113"+ ",link_1_13.ARTIPART as ARTIPART113"+ ",link_1_13.ARUNMIS1 as ARUNMIS1113"+ ",link_1_13.ARUNMIS2 as ARUNMIS2113"+ ",link_1_13.ARFLSERG as ARFLSERG113"+ ",link_1_13.ARUNMIS1 as ARUNMIS1113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on MOD_ELEM.MOCODSER=link_1_13.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOCODSER=link_1_13.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODSER
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_MOCODSER)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_MOCODSER))
          select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODSER)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODSER) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oMOCODSER_1_14'),i_cWhere,'GSMA_AAS',"Servizi",'GSMA3AAS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MOCODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MOCODSER)
            select ARCODART,ARDESART,ARTIPART,ARUNMIS1,ARUNMIS2,ARFLSERG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODSER = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(60))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_MOUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODSER = space(20)
      endif
      this.w_DESART = space(60)
      this.w_TIPART = space(2)
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_MOUNIMIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MOTIPCON='C' AND(.w_TIPART='FO' OR .w_TIPART='FM')) OR (.w_MOTIPCON='P' AND .w_TIPART='FM')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Servizio non a quantit� e valore")
        endif
        this.w_MOCODSER = space(20)
        this.w_DESART = space(60)
        this.w_TIPART = space(2)
        this.w_UNMIS1 = space(3)
        this.w_UNMIS2 = space(3)
        this.w_FLSERG = space(1)
        this.w_MOUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.ARCODART as ARCODART114"+ ",link_1_14.ARDESART as ARDESART114"+ ",link_1_14.ARTIPART as ARTIPART114"+ ",link_1_14.ARUNMIS1 as ARUNMIS1114"+ ",link_1_14.ARUNMIS2 as ARUNMIS2114"+ ",link_1_14.ARFLSERG as ARFLSERG114"+ ",link_1_14.ARUNMIS1 as ARUNMIS1114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on MOD_ELEM.MOCODSER=link_1_14.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOCODSER=link_1_14.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOPERATT
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOPERATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_MOPERATT)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_MOPERATT))
          select MDCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOPERATT)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOPERATT) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oMOPERATT_1_18'),i_cWhere,'GSAR_AMD',"Metodi di calcolo periodicit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOPERATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_MOPERATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_MOPERATT)
            select MDCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOPERATT = NVL(_Link_.MDCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MOPERATT = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOPERATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOTIPATT
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOTIPATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MOTIPATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CAFLNSAP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MOTIPATT))
          select CACODICE,CADESCRI,CARAGGST,CAFLNSAP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOTIPATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOTIPATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oMOTIPATT_1_20'),i_cWhere,'GSAG_MCA',"Tipi attivita",'GSAG_MCA.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CAFLNSAP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CARAGGST,CAFLNSAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOTIPATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CAFLNSAP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MOTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MOTIPATT)
            select CACODICE,CADESCRI,CARAGGST,CAFLNSAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOTIPATT = NVL(_Link_.CACODICE,space(20))
      this.w_DESATT = NVL(_Link_.CADESCRI,space(254))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(10))
      this.w_FLNSAP = NVL(_Link_.CAFLNSAP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MOTIPATT = space(20)
      endif
      this.w_DESATT = space(254)
      this.w_CARAGGST = space(10)
      this.w_FLNSAP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CARAGGST<>'Z'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MOTIPATT = space(20)
        this.w_DESATT = space(254)
        this.w_CARAGGST = space(10)
        this.w_FLNSAP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOTIPATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.CACODICE as CACODICE120"+ ",link_1_20.CADESCRI as CADESCRI120"+ ",link_1_20.CARAGGST as CARAGGST120"+ ",link_1_20.CAFLNSAP as CAFLNSAP120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on MOD_ELEM.MOTIPATT=link_1_20.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOTIPATT=link_1_20.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOGRUPAR
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOGRUPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_MOGRUPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_MOGRUPAR))
          select DPCODICE,DPTIPRIS,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOGRUPAR)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOGRUPAR) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oMOGRUPAR_1_21'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOGRUPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_MOGRUPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_MOGRUPAR)
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOGRUPAR = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DPDESCRI = NVL(_Link_.DPDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_MOGRUPAR = space(5)
      endif
      this.w_TIPRIS = space(1)
      this.w_DPDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIS='G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MOGRUPAR = space(5)
        this.w_TIPRIS = space(1)
        this.w_DPDESCRI = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOGRUPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.DPCODICE as DPCODICE121"+ ",link_1_21.DPTIPRIS as DPTIPRIS121"+ ",link_1_21.DPDESCRI as DPDESCRI121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on MOD_ELEM.MOGRUPAR=link_1_21.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOGRUPAR=link_1_21.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOPERFAT
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOPERFAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_MOPERFAT)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MD__FREQ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_MOPERFAT))
          select MDCODICE,MD__FREQ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOPERFAT)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOPERFAT) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oMOPERFAT_1_25'),i_cWhere,'GSAR_AMD',"Metodi di calcolo periodicit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MD__FREQ";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MD__FREQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOPERFAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MD__FREQ";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_MOPERFAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_MOPERFAT)
            select MDCODICE,MD__FREQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOPERFAT = NVL(_Link_.MDCODICE,space(3))
      this.w_MD__FREQ = NVL(_Link_.MD__FREQ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MOPERFAT = space(3)
      endif
      this.w_MD__FREQ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOPERFAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODCLDAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.MDCODICE as MDCODICE125"+ ",link_1_25.MD__FREQ as MD__FREQ125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on MOD_ELEM.MOPERFAT=link_1_25.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOPERFAT=link_1_25.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCAUDOC
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MOCAUDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MOCAUDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMOCAUDOC_1_27'),i_cWhere,'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MOCAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MOCAUDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLRIPS = NVL(_Link_.TDFLRIPS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MOCAUDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
      this.w_FLRIPS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDOC<>'OR' AND .w_FLVEAC='V' and (! Isahe() or .w_FLRIPS='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo")
        endif
        this.w_MOCAUDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
        this.w_FLRIPS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_27(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_27.TDTIPDOC as TDTIPDOC127"+ ",link_1_27.TDDESDOC as TDDESDOC127"+ ",link_1_27.TDCATDOC as TDCATDOC127"+ ",link_1_27.TDFLVEAC as TDFLVEAC127"+ ",link_1_27.TDFLRIPS as TDFLRIPS127"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_27 on MOD_ELEM.MOCAUDOC=link_1_27.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_27"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOCAUDOC=link_1_27.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOPERCON
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOPERCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_MOPERCON)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_MOPERCON))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOPERCON)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStrODBC(trim(this.w_MOPERCON)+"%");

            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStr(trim(this.w_MOPERCON)+"%");

            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOPERCON) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oMOPERCON_1_34'),i_cWhere,'GSAR_AMD',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOPERCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_MOPERCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_MOPERCON)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOPERCON = NVL(_Link_.MDCODICE,space(3))
      this.w_MDDESCRI = NVL(_Link_.MDDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_MOPERCON = space(3)
      endif
      this.w_MDDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOPERCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_34(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODCLDAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_34.MDCODICE as MDCODICE134"+ ",link_1_34.MDDESCRI as MDDESCRI134"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_34 on MOD_ELEM.MOPERCON=link_1_34.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_34"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOPERCON=link_1_34.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODVAL
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MOCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MOCODVAL)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_45(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_45.VACODVAL as VACODVAL145"+ ",link_1_45.VADESVAL as VADESVAL145"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_45 on MOD_ELEM.MOCODVAL=link_1_45.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_45"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOCODVAL=link_1_45.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOVOCCEN
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOVOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_MOVOCCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_MOVOCCEN))
          select VCCODICE,VCDESCRI,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOVOCCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStrODBC(trim(this.w_MOVOCCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStr(trim(this.w_MOVOCCEN)+"%");

            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOVOCCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oMOVOCCEN_2_3'),i_cWhere,'GSCA_AVC',"Voci di ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOVOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_MOVOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_MOVOCCEN)
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOVOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_VOCOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MOVOCCEN = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_VOCOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.), CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MOVOCCEN = space(15)
        this.w_DESVOC = space(40)
        this.w_VOCOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOVOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.VCCODICE as VCCODICE203"+ ",link_2_3.VCDESCRI as VCDESCRI203"+ ",link_2_3.VCDTOBSO as VCDTOBSO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on MOD_ELEM.MOVOCCEN=link_2_3.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOVOCCEN=link_2_3.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODCEN
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_MOCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_MOCODCEN))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_MOCODCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_MOCODCEN)+"%");

            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oMOCODCEN_2_4'),i_cWhere,'GSCA_ACC',"Centri di ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_MOCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_MOCODCEN)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODCEN = space(15)
      endif
      this.w_DESCEN = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.), CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
        endif
        this.w_MOCODCEN = space(15)
        this.w_DESCEN = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CC_CONTO as CC_CONTO204"+ ",link_2_4.CCDESPIA as CCDESPIA204"+ ",link_2_4.CCDTOBSO as CCDTOBSO204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on MOD_ELEM.MOCODCEN=link_2_4.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOCODCEN=link_2_4.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCOCOMM
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCOCOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_MOCOCOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_MOCOCOMM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCOCOMM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_MOCOCOMM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_MOCOCOMM)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOCOCOMM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oMOCOCOMM_2_5'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCOCOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MOCOCOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MOCOCOMM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCOCOMM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMM = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MOCOCOMM = space(15)
      endif
      this.w_DESCOMM = space(100)
      this.w_COMOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.), CHKDTOBS(.w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_MOCOCOMM = space(15)
        this.w_DESCOMM = space(100)
        this.w_COMOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCOCOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.CNCODCAN as CNCODCAN205"+ ",link_2_5.CNDESCAN as CNDESCAN205"+ ",link_2_5.CNDTOBSO as CNDTOBSO205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on MOD_ELEM.MOCOCOMM=link_2_5.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and MOD_ELEM.MOCOCOMM=link_2_5.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MOCODATT
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_MOCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_MOCOCOMM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_MOCOCOMM;
                     ,'ATTIPATT',this.w_ATTIPATT;
                     ,'ATCODATT',trim(this.w_MOCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_MOCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_MOCOCOMM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_MOCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_MOCOCOMM);
                   +" and ATTIPATT="+cp_ToStr(this.w_ATTIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oMOCODATT_2_6'),i_cWhere,'GSPC_BZZ',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MOCOCOMM<>oSource.xKey(1);
           .or. this.w_ATTIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_MOCOCOMM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_MOCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_MOCOCOMM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_ATTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_MOCOCOMM;
                       ,'ATTIPATT',this.w_ATTIPATT;
                       ,'ATCODATT',this.w_MOCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATTI = NVL(_Link_.ATDESCRI,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODATT = space(15)
      endif
      this.w_DESATTI = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMOCODICE_1_7.value==this.w_MOCODICE)
      this.oPgFrm.Page1.oPag.oMOCODICE_1_7.value=this.w_MOCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESCRI_1_9.value==this.w_MODESCRI)
      this.oPgFrm.Page1.oPag.oMODESCRI_1_9.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCATELE_1_10.value==this.w_MOCATELE)
      this.oPgFrm.Page1.oPag.oMOCATELE_1_10.value=this.w_MOCATELE
    endif
    if not(this.oPgFrm.Page1.oPag.oMOTIPCON_1_11.RadioValue()==this.w_MOTIPCON)
      this.oPgFrm.Page1.oPag.oMOTIPCON_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODLIS_1_12.value==this.w_MOCODLIS)
      this.oPgFrm.Page1.oPag.oMOCODLIS_1_12.value=this.w_MOCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODSER_1_13.value==this.w_MOCODSER)
      this.oPgFrm.Page1.oPag.oMOCODSER_1_13.value=this.w_MOCODSER
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODSER_1_14.value==this.w_MOCODSER)
      this.oPgFrm.Page1.oPag.oMOCODSER_1_14.value=this.w_MOCODSER
    endif
    if not(this.oPgFrm.Page1.oPag.oMOTIPAPL_1_15.RadioValue()==this.w_MOTIPAPL)
      this.oPgFrm.Page1.oPag.oMOTIPAPL_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOQTAMOV_1_16.value==this.w_MOQTAMOV)
      this.oPgFrm.Page1.oPag.oMOQTAMOV_1_16.value=this.w_MOQTAMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oMOFLATTI_1_17.RadioValue()==this.w_MOFLATTI)
      this.oPgFrm.Page1.oPag.oMOFLATTI_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPERATT_1_18.value==this.w_MOPERATT)
      this.oPgFrm.Page1.oPag.oMOPERATT_1_18.value=this.w_MOPERATT
    endif
    if not(this.oPgFrm.Page1.oPag.oMOGIOATT_1_19.value==this.w_MOGIOATT)
      this.oPgFrm.Page1.oPag.oMOGIOATT_1_19.value=this.w_MOGIOATT
    endif
    if not(this.oPgFrm.Page1.oPag.oMOTIPATT_1_20.value==this.w_MOTIPATT)
      this.oPgFrm.Page1.oPag.oMOTIPATT_1_20.value=this.w_MOTIPATT
    endif
    if not(this.oPgFrm.Page1.oPag.oMOGRUPAR_1_21.value==this.w_MOGRUPAR)
      this.oPgFrm.Page1.oPag.oMOGRUPAR_1_21.value=this.w_MOGRUPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCHKDAT_1_22.RadioValue()==this.w_MOCHKDAT)
      this.oPgFrm.Page1.oPag.oMOCHKDAT_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESATT_1_23.value==this.w_MODESATT)
      this.oPgFrm.Page1.oPag.oMODESATT_1_23.value=this.w_MODESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oMOFLFATT_1_24.RadioValue()==this.w_MOFLFATT)
      this.oPgFrm.Page1.oPag.oMOFLFATT_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPERFAT_1_25.value==this.w_MOPERFAT)
      this.oPgFrm.Page1.oPag.oMOPERFAT_1_25.value=this.w_MOPERFAT
    endif
    if not(this.oPgFrm.Page1.oPag.oMOGIODOC_1_26.value==this.w_MOGIODOC)
      this.oPgFrm.Page1.oPag.oMOGIODOC_1_26.value=this.w_MOGIODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCAUDOC_1_27.value==this.w_MOCAUDOC)
      this.oPgFrm.Page1.oPag.oMOCAUDOC_1_27.value=this.w_MOCAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPRIDOC_1_28.RadioValue()==this.w_MOPRIDOC)
      this.oPgFrm.Page1.oPag.oMOPRIDOC_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCHKDDO_1_29.RadioValue()==this.w_MOCHKDDO)
      this.oPgFrm.Page1.oPag.oMOCHKDDO_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESDOC_1_30.value==this.w_MODESDOC)
      this.oPgFrm.Page1.oPag.oMODESDOC_1_30.value=this.w_MODESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMOESCRIN_1_31.RadioValue()==this.w_MOESCRIN)
      this.oPgFrm.Page1.oPag.oMOESCRIN_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMORINCON_1_32.RadioValue()==this.w_MORINCON)
      this.oPgFrm.Page1.oPag.oMORINCON_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMONUMGIO_1_33.value==this.w_MONUMGIO)
      this.oPgFrm.Page1.oPag.oMONUMGIO_1_33.value=this.w_MONUMGIO
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPERCON_1_34.value==this.w_MOPERCON)
      this.oPgFrm.Page1.oPag.oMOPERCON_1_34.value=this.w_MOPERCON
    endif
    if not(this.oPgFrm.Page1.oPag.oUNMIS1_1_41.value==this.w_UNMIS1)
      this.oPgFrm.Page1.oPag.oUNMIS1_1_41.value=this.w_UNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCODVAL_1_45.value==this.w_MOCODVAL)
      this.oPgFrm.Page1.oPag.oMOCODVAL_1_45.value=this.w_MOCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESELE_1_48.value==this.w_DESELE)
      this.oPgFrm.Page1.oPag.oDESELE_1_48.value=this.w_DESELE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_51.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_51.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_52.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_52.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_55.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_55.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPRIDAT_1_56.RadioValue()==this.w_MOPRIDAT)
      this.oPgFrm.Page1.oPag.oMOPRIDAT_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_59.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_59.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMDDATVAL_1_63.value==this.w_MDDATVAL)
      this.oPgFrm.Page1.oPag.oMDDATVAL_1_63.value=this.w_MDDATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMODTOBSO_1_64.value==this.w_MODTOBSO)
      this.oPgFrm.Page1.oPag.oMODTOBSO_1_64.value=this.w_MODTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDESCRI_1_85.value==this.w_DPDESCRI)
      this.oPgFrm.Page1.oPag.oDPDESCRI_1_85.value=this.w_DPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMDDESCRI_1_95.value==this.w_MDDESCRI)
      this.oPgFrm.Page1.oPag.oMDDESCRI_1_95.value=this.w_MDDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_101.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_101.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page2.oPag.oMOVOCCEN_2_3.value==this.w_MOVOCCEN)
      this.oPgFrm.Page2.oPag.oMOVOCCEN_2_3.value=this.w_MOVOCCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODCEN_2_4.value==this.w_MOCODCEN)
      this.oPgFrm.Page2.oPag.oMOCODCEN_2_4.value=this.w_MOCODCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCOCOMM_2_5.value==this.w_MOCOCOMM)
      this.oPgFrm.Page2.oPag.oMOCOCOMM_2_5.value=this.w_MOCOCOMM
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODATT_2_6.value==this.w_MOCODATT)
      this.oPgFrm.Page2.oPag.oMOCODATT_2_6.value=this.w_MOCODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVOC_2_11.value==this.w_DESVOC)
      this.oPgFrm.Page2.oPag.oDESVOC_2_11.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCEN_2_12.value==this.w_DESCEN)
      this.oPgFrm.Page2.oPag.oDESCEN_2_12.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOMM_2_13.value==this.w_DESCOMM)
      this.oPgFrm.Page2.oPag.oDESCOMM_2_13.value=this.w_DESCOMM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATTI_2_14.value==this.w_DESATTI)
      this.oPgFrm.Page2.oPag.oDESATTI_2_14.value=this.w_DESATTI
    endif
    if not(this.oPgFrm.Page1.oPag.oMODATFIS_1_113.RadioValue()==this.w_MODATFIS)
      this.oPgFrm.Page1.oPag.oMODATFIS_1_113.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'MOD_ELEM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not((NOT EMPTY(.w_MOCODLIS)) OR (EMPTY(.w_MOCODLIS) AND .w_MOTIPAPL='C'))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione:tipo di applicazione differita: inserire un listino valido!")
          case   (empty(.w_MOCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCODICE_1_7.SetFocus()
            i_bnoObbl = !empty(.w_MOCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VALLIS=.w_MOCODVAL )  and not(Isahe())  and not(empty(.w_MOCODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCODLIS_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta listino incongruente ")
          case   ((empty(.w_MOCODSER)) or not((.w_MOTIPCON='C' AND(.w_TIPART='FO' OR .w_TIPART='FM')) OR (.w_MOTIPCON='P' AND .w_TIPART='FM')))  and not(.w_MOTIPCON='P')  and ((.w_MOFLFATT='S' OR (.w_MOFLATTI='S'  AND .w_FLNSAP='N') ) AND .w_MOTIPCON<>'P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCODSER_1_13.SetFocus()
            i_bnoObbl = !empty(.w_MOCODSER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Servizio n� a valore n� a quantit� e valore ")
          case   ((empty(.w_MOCODSER)) or not((.w_MOTIPCON='C' AND(.w_TIPART='FO' OR .w_TIPART='FM')) OR (.w_MOTIPCON='P' AND .w_TIPART='FM')))  and not(.w_MOTIPCON<>'P')  and (((.w_MOFLFATT='S' OR (.w_MOFLATTI='S' AND .w_FLNSAP='N') ) AND .w_MOTIPCON<>'P') or .w_MOTIPCON='P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCODSER_1_14.SetFocus()
            i_bnoObbl = !empty(.w_MOCODSER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Servizio non a quantit� e valore")
          case   (empty(.w_MOPERATT))  and (.w_MOFLATTI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOPERATT_1_18.SetFocus()
            i_bnoObbl = !empty(.w_MOPERATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MOTIPATT)) or not(.w_CARAGGST<>'Z'))  and (.w_MOFLATTI='S' AND .w_MOTIPCON<>'P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOTIPATT_1_20.SetFocus()
            i_bnoObbl = !empty(.w_MOTIPATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MOGRUPAR)) or not(.w_TIPRIS='G'))  and (.w_MOFLATTI='S' AND .w_MOTIPCON<>'P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOGRUPAR_1_21.SetFocus()
            i_bnoObbl = !empty(.w_MOGRUPAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MODESATT) and (.w_MOCHKDAT='S'))  and (.w_MOCHKDAT='S' AND .w_MOFLATTI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMODESATT_1_23.SetFocus()
            i_bnoObbl = !empty(.w_MODESATT) or !(.w_MOCHKDAT='S')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MOPERFAT))  and (.w_MOFLFATT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOPERFAT_1_25.SetFocus()
            i_bnoObbl = !empty(.w_MOPERFAT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Periodicit� inesistente ")
          case   ((empty(.w_MOCAUDOC)) or not(.w_CATDOC<>'OR' AND .w_FLVEAC='V' and (! Isahe() or .w_FLRIPS='S')))  and (.w_MOFLFATT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOCAUDOC_1_27.SetFocus()
            i_bnoObbl = !empty(.w_MOCAUDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo")
          case   (empty(.w_MODESDOC) and (.w_MOCHKDDO='S'))  and (.w_MOCHKDDO='S' and .w_MOFLFATT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMODESDOC_1_30.SetFocus()
            i_bnoObbl = !empty(.w_MODESDOC) or !(.w_MOCHKDDO='S')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MOPERCON))  and (.w_MOESCRIN = "N" )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMOPERCON_1_34.SetFocus()
            i_bnoObbl = !empty(.w_MOPERCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.), CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.) ))  and not(NOT (g_PERCCR='S' or g_COMM='S'))  and not(empty(.w_MOVOCCEN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMOVOCCEN_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.), CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.) ))  and not(g_PERCCR<>'S')  and not(empty(.w_MOCODCEN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMOCODCEN_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.), CHKDTOBS(.w_COMOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.) ))  and not(NOT(g_PERCAN='S' OR g_COMM='S' ))  and not(empty(.w_MOCOCOMM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMOCOCOMM_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_ame
      if .w_MOTIPCON<>'P' AND .w_MOFLFATT<>'S' AND .w_MOFLATTI<>'S'
         i_bnoChk = .f.
      	 i_bRes = .f.
      	 i_cErrorMsg = Ah_MsgFormat("Attenzione selezionare almeno un flag di generazione")	
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_MOTIPCON = this.w_MOTIPCON
    this.o_MOCODSER = this.w_MOCODSER
    this.o_MOFLATTI = this.w_MOFLATTI
    this.o_MOTIPATT = this.w_MOTIPATT
    this.o_MOCHKDAT = this.w_MOCHKDAT
    this.o_MOFLFATT = this.w_MOFLFATT
    this.o_MOCHKDDO = this.w_MOCHKDDO
    this.o_MORINCON = this.w_MORINCON
    this.o_MOCOCOMM = this.w_MOCOCOMM
    return

enddefine

* --- Define pages as container
define class tgsag_amePag1 as StdContainer
  Width  = 777
  height = 578
  stdWidth  = 777
  stdheight = 578
  resizeXpos=398
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMOCODICE_1_7 as StdField with uid="QUIJWJLSEO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MOCODICE", cQueryName = "MOCODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice modello",;
    HelpContextID = 17390859,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=141, Top=9, InputMask=replicate('X',10)

  add object oMODESCRI_1_9 as StdField with uid="KSNMFZCWQQ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 200240399,;
   bGlobalFont=.t.,;
    Height=21, Width=544, Left=227, Top=9, InputMask=replicate('X',60)

  add object oMOCATELE_1_10 as StdField with uid="QREHVNYZVI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MOCATELE", cQueryName = "MOCATELE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria elementi",;
    HelpContextID = 33858293,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=141, Top=35, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_ELEM", cZoomOnZoom="GSAG_ACE", oKey_1_1="CECODELE", oKey_1_2="this.w_MOCATELE"

  func oMOCATELE_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCATELE_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCATELE_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ELEM','*','CECODELE',cp_AbsName(this.parent,'oMOCATELE_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACE',"Categorie elementi",'',this.parent.oContained
  endproc
  proc oMOCATELE_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODELE=this.parent.oContained.w_MOCATELE
     i_obj.ecpSave()
  endproc


  add object oMOTIPCON_1_11 as StdCombo with uid="UOJDLFXMAP",rtseq=10,rtrep=.f.,left=141,top=59,width=130,height=21;
    , HelpContextID = 71013100;
    , cFormVar="w_MOTIPCON",RowSource=""+"Canone,"+"Pacchetto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOTIPCON_1_11.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oMOTIPCON_1_11.GetRadio()
    this.Parent.oContained.w_MOTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oMOTIPCON_1_11.SetRadio()
    this.Parent.oContained.w_MOTIPCON=trim(this.Parent.oContained.w_MOTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_MOTIPCON=='C',1,;
      iif(this.Parent.oContained.w_MOTIPCON=='P',2,;
      0))
  endfunc

  func oMOTIPCON_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (GSAG_BME(this.Parent.oContained, "CHKEL"))
    endwith
   endif
  endfunc

  add object oMOCODLIS_1_12 as StdField with uid="XGWRVDFFGC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MOCODLIS", cQueryName = "MOCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta listino incongruente ",;
    ToolTipText = "Codice listino",;
    HelpContextID = 200712935,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=141, Top=84, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_MOCODLIS"

  func oMOCODLIS_1_12.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oMOCODLIS_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODLIS_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODLIS_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oMOCODLIS_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oMOCODLIS_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_MOCODLIS
     i_obj.ecpSave()
  endproc

  add object oMOCODSER_1_13 as StdField with uid="WCTHKOFZED",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MOCODSER", cQueryName = "MOCODSER",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Servizio n� a valore n� a quantit� e valore ",;
    ToolTipText = "Codice Servizio",;
    HelpContextID = 185163032,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=141, Top=109, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_MOCODSER"

  func oMOCODSER_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_MOFLFATT='S' OR (.w_MOFLATTI='S'  AND .w_FLNSAP='N') ) AND .w_MOTIPCON<>'P')
    endwith
   endif
  endfunc

  func oMOCODSER_1_13.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON='P')
    endwith
  endfunc

  func oMOCODSER_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODSER_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODSER_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oMOCODSER_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSMA2AAS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oMOCODSER_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_MOCODSER
     i_obj.ecpSave()
  endproc

  add object oMOCODSER_1_14 as StdField with uid="LYTEYKMGHY",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MOCODSER", cQueryName = "MOCODSER",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Servizio non a quantit� e valore",;
    ToolTipText = "Codice Servizio",;
    HelpContextID = 185163032,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=141, Top=109, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_MOCODSER"

  func oMOCODSER_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (((.w_MOFLFATT='S' OR (.w_MOFLATTI='S' AND .w_FLNSAP='N') ) AND .w_MOTIPCON<>'P') or .w_MOTIPCON='P')
    endwith
   endif
  endfunc

  func oMOCODSER_1_14.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  func oMOCODSER_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODSER_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODSER_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oMOCODSER_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSMA3AAS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oMOCODSER_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_MOCODSER
     i_obj.ecpSave()
  endproc


  add object oMOTIPAPL_1_15 as StdCombo with uid="VWBZCJVWAJ",rtseq=14,rtrep=.f.,left=637,top=84,width=134,height=22;
    , ToolTipText = "Se immediata valorizzo il prezzo su elemento contratto, se differita verr� calcolato da listino";
    , HelpContextID = 163867922;
    , cFormVar="w_MOTIPAPL",RowSource=""+"Differita,"+"Immediata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOTIPAPL_1_15.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oMOTIPAPL_1_15.GetRadio()
    this.Parent.oContained.w_MOTIPAPL = this.RadioValue()
    return .t.
  endfunc

  func oMOTIPAPL_1_15.SetRadio()
    this.Parent.oContained.w_MOTIPAPL=trim(this.Parent.oContained.w_MOTIPAPL)
    this.value = ;
      iif(this.Parent.oContained.w_MOTIPAPL=='F',1,;
      iif(this.Parent.oContained.w_MOTIPAPL=='C',2,;
      0))
  endfunc

  func oMOTIPAPL_1_15.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oMOQTAMOV_1_16 as StdField with uid="ODLLAZLJAC",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MOQTAMOV", cQueryName = "MOQTAMOV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit�",;
    HelpContextID = 81739036,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=324, Top=134, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  func oMOQTAMOV_1_16.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oMOFLATTI_1_17 as StdCheck with uid="BYJRJLBIIF",rtseq=16,rtrep=.f.,left=142, top=168, caption="Gen. Attivit�",;
    ToolTipText = "Gen. Attivit�",;
    HelpContextID = 198610191,;
    cFormVar="w_MOFLATTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOFLATTI_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMOFLATTI_1_17.GetRadio()
    this.Parent.oContained.w_MOFLATTI = this.RadioValue()
    return .t.
  endfunc

  func oMOFLATTI_1_17.SetRadio()
    this.Parent.oContained.w_MOFLATTI=trim(this.Parent.oContained.w_MOFLATTI)
    this.value = ;
      iif(this.Parent.oContained.w_MOFLATTI=='S',1,;
      0)
  endfunc

  func oMOFLATTI_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
   endif
  endfunc

  add object oMOPERATT_1_18 as StdField with uid="ALMRRMMZDJ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MOPERATT", cQueryName = "MOPERATT",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 165686554,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=359, Top=168, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_MOPERATT"

  func oMOPERATT_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOFLATTI='S')
    endwith
   endif
  endfunc

  func oMOPERATT_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOPERATT_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOPERATT_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oMOPERATT_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo periodicit�",'',this.parent.oContained
  endproc
  proc oMOPERATT_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_MOPERATT
     i_obj.ecpSave()
  endproc

  add object oMOGIOATT_1_19 as StdField with uid="OKVNXYTMSE",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MOGIOATT", cQueryName = "MOGIOATT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "gg di tolleranza per gen. attivit�",;
    HelpContextID = 162766106,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=730, Top=168

  func oMOGIOATT_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOFLATTI='S' AND .w_MOTIPCON<>'P')
    endwith
   endif
  endfunc

  add object oMOTIPATT_1_20 as StdField with uid="OTUOJMHIFC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MOTIPATT", cQueryName = "MOTIPATT",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 163867930,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=141, Top=192, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_MOTIPATT"

  func oMOTIPATT_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOFLATTI='S' AND .w_MOTIPCON<>'P')
    endwith
   endif
  endfunc

  func oMOTIPATT_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOTIPATT_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOTIPATT_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oMOTIPATT_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'GSAG_MCA.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oMOTIPATT_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_MOTIPATT
     i_obj.ecpSave()
  endproc

  add object oMOGRUPAR_1_21 as StdField with uid="TYWBWTEVEB",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MOGRUPAR", cQueryName = "MOGRUPAR",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo partecipante attivit�",;
    HelpContextID = 152870168,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=141, Top=216, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_MOGRUPAR"

  func oMOGRUPAR_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOFLATTI='S' AND .w_MOTIPCON<>'P')
    endwith
   endif
  endfunc

  func oMOGRUPAR_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOGRUPAR_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOGRUPAR_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oMOGRUPAR_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oMOGRUPAR_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_MOGRUPAR
     i_obj.ecpSave()
  endproc

  add object oMOCHKDAT_1_22 as StdCheck with uid="MRXQSFKJWU",rtseq=21,rtrep=.f.,left=142, top=245, caption="Gestione descrizione parametrica",;
    ToolTipText = "Se attivo abilita descrizione parametrica",;
    HelpContextID = 208821530,;
    cFormVar="w_MOCHKDAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOCHKDAT_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMOCHKDAT_1_22.GetRadio()
    this.Parent.oContained.w_MOCHKDAT = this.RadioValue()
    return .t.
  endfunc

  func oMOCHKDAT_1_22.SetRadio()
    this.Parent.oContained.w_MOCHKDAT=trim(this.Parent.oContained.w_MOCHKDAT)
    this.value = ;
      iif(this.Parent.oContained.w_MOCHKDAT=='S',1,;
      0)
  endfunc

  func oMOCHKDAT_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOFLATTI='S')
    endwith
   endif
  endfunc

  add object oMODESATT_1_23 as StdMemo with uid="JJXNLMQLIF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MODESATT", cQueryName = "MODESATT",;
    bObbl = .t. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione servizio attivit� parametrica",;
    HelpContextID = 166685978,;
   bGlobalFont=.t.,;
    Height=51, Width=497, Left=141, Top=265

  func oMODESATT_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOCHKDAT='S' AND .w_MOFLATTI='S')
    endwith
   endif
  endfunc

  func oMODESATT_1_23.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_MOCHKDAT='S'
    endwith
    return i_bres
  endfunc

  add object oMOFLFATT_1_24 as StdCheck with uid="ESGDZEANOW",rtseq=23,rtrep=.f.,left=142, top=330, caption="Gen. Documento",;
    ToolTipText = "Se attivo genera un documento",;
    HelpContextID = 153521434,;
    cFormVar="w_MOFLFATT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOFLFATT_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMOFLFATT_1_24.GetRadio()
    this.Parent.oContained.w_MOFLFATT = this.RadioValue()
    return .t.
  endfunc

  func oMOFLFATT_1_24.SetRadio()
    this.Parent.oContained.w_MOFLFATT=trim(this.Parent.oContained.w_MOFLFATT)
    this.value = ;
      iif(this.Parent.oContained.w_MOFLFATT=='S',1,;
      0)
  endfunc

  add object oMOPERFAT_1_25 as StdField with uid="AJDCHRPGVC",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MOPERFAT", cQueryName = "MOPERFAT",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Periodicit� inesistente ",;
    ToolTipText = "Periodicit� fatturazione",;
    HelpContextID = 249572634,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=359, Top=330, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_MOPERFAT"

  func oMOPERFAT_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOFLFATT='S')
    endwith
   endif
  endfunc

  func oMOPERFAT_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOPERFAT_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOPERFAT_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oMOPERFAT_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo periodicit�",'',this.parent.oContained
  endproc
  proc oMOPERFAT_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_MOPERFAT
     i_obj.ecpSave()
  endproc

  add object oMOGIODOC_1_26 as StdField with uid="QIPJPHWELC",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MOGIODOC", cQueryName = "MOGIODOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni di tolleranza per generazione documenti",;
    HelpContextID = 55337719,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=730, Top=330

  func oMOGIODOC_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOFLFATT='S')
    endwith
   endif
  endfunc

  add object oMOCAUDOC_1_27 as StdField with uid="UQVSEYLYCZ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MOCAUDOC", cQueryName = "MOCAUDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo",;
    ToolTipText = "Causale documento",;
    HelpContextID = 49586935,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=141, Top=354, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MOCAUDOC"

  func oMOCAUDOC_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOFLFATT='S')
    endwith
   endif
  endfunc

  func oMOCAUDOC_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCAUDOC_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCAUDOC_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMOCAUDOC_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oMOCAUDOC_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_MOCAUDOC
     i_obj.ecpSave()
  endproc


  add object oMOPRIDOC_1_28 as StdCombo with uid="SFJHODPIQZ",rtseq=27,rtrep=.f.,left=652,top=354,width=119,height=22;
    , HelpContextID = 61002487;
    , cFormVar="w_MOPRIDOC",RowSource=""+"Inizio periodo,"+"Da periodicit�,"+"Fine periodo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOPRIDOC_1_28.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oMOPRIDOC_1_28.GetRadio()
    this.Parent.oContained.w_MOPRIDOC = this.RadioValue()
    return .t.
  endfunc

  func oMOPRIDOC_1_28.SetRadio()
    this.Parent.oContained.w_MOPRIDOC=trim(this.Parent.oContained.w_MOPRIDOC)
    this.value = ;
      iif(this.Parent.oContained.w_MOPRIDOC=='I',1,;
      iif(this.Parent.oContained.w_MOPRIDOC=='P',2,;
      iif(this.Parent.oContained.w_MOPRIDOC=='F',3,;
      0)))
  endfunc

  add object oMOCHKDDO_1_29 as StdCheck with uid="QEWWUVQTIV",rtseq=28,rtrep=.f.,left=142, top=385, caption="Gestione descrizione parametrica",;
    ToolTipText = "Se attivo abilita descrizione parametrica",;
    HelpContextID = 208821525,;
    cFormVar="w_MOCHKDDO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOCHKDDO_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMOCHKDDO_1_29.GetRadio()
    this.Parent.oContained.w_MOCHKDDO = this.RadioValue()
    return .t.
  endfunc

  func oMOCHKDDO_1_29.SetRadio()
    this.Parent.oContained.w_MOCHKDDO=trim(this.Parent.oContained.w_MOCHKDDO)
    this.value = ;
      iif(this.Parent.oContained.w_MOCHKDDO=='S',1,;
      0)
  endfunc

  func oMOCHKDDO_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOFLFATT='S')
    endwith
   endif
  endfunc

  add object oMODESDOC_1_30 as StdMemo with uid="IESLXLLIJP",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MODESDOC", cQueryName = "MODESDOC",;
    bObbl = .t. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione servizio documento parametrica ",;
    HelpContextID = 51417847,;
   bGlobalFont=.t.,;
    Height=51, Width=497, Left=141, Top=406

  func oMODESDOC_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOCHKDDO='S' and .w_MOFLFATT='S')
    endwith
   endif
  endfunc

  func oMODESDOC_1_30.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_MOCHKDDO='S'
    endwith
    return i_bres
  endfunc

  add object oMOESCRIN_1_31 as StdCheck with uid="SFRDVXIRRC",rtseq=30,rtrep=.f.,left=142, top=471, caption="Esclude rinnovo",;
    ToolTipText = "Se attivo, inibisce la possibilit� di rinnovo degli elementi contratto",;
    HelpContextID = 100827884,;
    cFormVar="w_MOESCRIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOESCRIN_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMOESCRIN_1_31.GetRadio()
    this.Parent.oContained.w_MOESCRIN = this.RadioValue()
    return .t.
  endfunc

  func oMOESCRIN_1_31.SetRadio()
    this.Parent.oContained.w_MOESCRIN=trim(this.Parent.oContained.w_MOESCRIN)
    this.value = ;
      iif(this.Parent.oContained.w_MOESCRIN=='S',1,;
      0)
  endfunc

  add object oMORINCON_1_32 as StdCheck with uid="ASMOIOOXOX",rtseq=31,rtrep=.f.,left=142, top=521, caption="Rinnovo tacito",;
    ToolTipText = "Se attivo, per gli elementi contratto � previsto il rinnovo tacito",;
    HelpContextID = 73118444,;
    cFormVar="w_MORINCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMORINCON_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMORINCON_1_32.GetRadio()
    this.Parent.oContained.w_MORINCON = this.RadioValue()
    return .t.
  endfunc

  func oMORINCON_1_32.SetRadio()
    this.Parent.oContained.w_MORINCON=trim(this.Parent.oContained.w_MORINCON)
    this.value = ;
      iif(this.Parent.oContained.w_MORINCON=='S',1,;
      0)
  endfunc

  func oMORINCON_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOESCRIN <> "S" )
    endwith
   endif
  endfunc

  add object oMONUMGIO_1_33 as StdField with uid="GGPAOVIDJS",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MONUMGIO", cQueryName = "MONUMGIO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni entro i quali comunicare la disdetta",;
    HelpContextID = 6288107,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=404, Top=521

  func oMONUMGIO_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOESCRIN <> "S"  AND .w_MORINCON = "S")
    endwith
   endif
  endfunc

  add object oMOPERCON_1_34 as StdField with uid="PCPYKTKNAF",rtseq=33,rtrep=.f.,;
    cFormVar = "w_MOPERCON", cQueryName = "MOPERCON",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Periodicit� rinnovo contratti",;
    HelpContextID = 69194476,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=141, Top=494, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_MOPERCON"

  func oMOPERCON_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOESCRIN = "N" )
    endwith
   endif
  endfunc

  func oMOPERCON_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOPERCON_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOPERCON_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oMOPERCON_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oMOPERCON_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_MOPERCON
     i_obj.ecpSave()
  endproc

  add object oUNMIS1_1_41 as StdField with uid="UXSODFVVVN",rtseq=38,rtrep=.f.,;
    cFormVar = "w_UNMIS1", cQueryName = "UNMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 101450682,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=141, Top=134, InputMask=replicate('X',3)

  func oUNMIS1_1_41.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oMOCODVAL_1_45 as StdField with uid="AYUBKXOEYU",rtseq=42,rtrep=.f.,;
    cFormVar = "w_MOCODVAL", cQueryName = "MOCODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 235494674,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=406, Top=59, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_MOCODVAL"

  func oMOCODVAL_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESELE_1_48 as StdField with uid="VYHMJFKOFM",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESELE", cQueryName = "DESELE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 226513462,;
   bGlobalFont=.t.,;
    Height=21, Width=544, Left=227, Top=35, InputMask=replicate('X',60)

  add object oDESVAL_1_51 as StdField with uid="RGUUUHHTQK",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 65098294,;
   bGlobalFont=.t.,;
    Height=21, Width=313, Left=458, Top=59, InputMask=replicate('X',35)

  add object oDESLIS_1_52 as StdField with uid="SWKCQHAJKZ",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 190272054,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=206, Top=84, InputMask=replicate('X',40)

  func oDESLIS_1_52.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oDESATT_1_55 as StdField with uid="ECGJLGLYZZ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 217862710,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=298, Top=192, InputMask=replicate('X',254)


  add object oMOPRIDAT_1_56 as StdCombo with uid="DVWJAYRVSV",rtseq=48,rtrep=.f.,left=652,top=193,width=119,height=22;
    , HelpContextID = 207432986;
    , cFormVar="w_MOPRIDAT",RowSource=""+"Inizio periodo,"+"Da periodicit�,"+"Fine periodo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOPRIDAT_1_56.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oMOPRIDAT_1_56.GetRadio()
    this.Parent.oContained.w_MOPRIDAT = this.RadioValue()
    return .t.
  endfunc

  func oMOPRIDAT_1_56.SetRadio()
    this.Parent.oContained.w_MOPRIDAT=trim(this.Parent.oContained.w_MOPRIDAT)
    this.value = ;
      iif(this.Parent.oContained.w_MOPRIDAT=='I',1,;
      iif(this.Parent.oContained.w_MOPRIDAT=='P',2,;
      iif(this.Parent.oContained.w_MOPRIDAT=='F',3,;
      0)))
  endfunc

  add object oDESDOC_1_59 as StdField with uid="LIHTDOVJGR",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 196039222,;
   bGlobalFont=.t.,;
    Height=21, Width=372, Left=210, Top=354, InputMask=replicate('X',35)

  add object oMDDATVAL_1_63 as StdField with uid="DQGXTUJXXC",rtseq=52,rtrep=.f.,;
    cFormVar = "w_MDDATVAL", cQueryName = "MDDATVAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 251355666,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=420, Top=556

  add object oMODTOBSO_1_64 as StdField with uid="VRGDBIVVIM",rtseq=53,rtrep=.f.,;
    cFormVar = "w_MODTOBSO", cQueryName = "MODTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 180251925,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=695, Top=556

  add object oDPDESCRI_1_85 as StdField with uid="KZYFFLHGVB",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DPDESCRI", cQueryName = "DPDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 200240511,;
   bGlobalFont=.t.,;
    Height=21, Width=439, Left=206, Top=216, InputMask=replicate('X',60)

  add object oMDDESCRI_1_95 as StdField with uid="SYOXWKZIIL",rtseq=73,rtrep=.f.,;
    cFormVar = "w_MDDESCRI", cQueryName = "MDDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 200237583,;
   bGlobalFont=.t.,;
    Height=21, Width=439, Left=206, Top=494, InputMask=replicate('X',50)

  add object oDESART_1_101 as StdField with uid="NZUUWIMSEG",rtseq=74,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 215765558,;
   bGlobalFont=.t.,;
    Height=21, Width=281, Left=298, Top=109, InputMask=replicate('X',60)

  add object oMODATFIS_1_113 as StdCheck with uid="QESVWYVAPP",rtseq=85,rtrep=.f.,left=519, top=521, caption="Data iniziale fissa",;
    ToolTipText = "Se attivo, la data iniziale del nuovo elemento contratto sar� uguale a quella dell'elemento contratto rinnovato.",;
    HelpContextID = 17076967,;
    cFormVar="w_MODATFIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMODATFIS_1_113.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMODATFIS_1_113.GetRadio()
    this.Parent.oContained.w_MODATFIS = this.RadioValue()
    return .t.
  endfunc

  func oMODATFIS_1_113.SetRadio()
    this.Parent.oContained.w_MODATFIS=trim(this.Parent.oContained.w_MODATFIS)
    this.value = ;
      iif(this.Parent.oContained.w_MODATFIS=='S',1,;
      0)
  endfunc

  func oMODATFIS_1_113.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOESCRIN <> "S" )
    endwith
   endif
  endfunc

  add object oStr_1_8 as StdString with uid="HSPLVPBSOQ",Visible=.t., Left=13, Top=9,;
    Alignment=1, Width=127, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="VJDGJIHECA",Visible=.t., Left=13, Top=35,;
    Alignment=1, Width=127, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="SVNKWZUMLO",Visible=.t., Left=281, Top=330,;
    Alignment=1, Width=73, Height=18,;
    Caption="Periodicit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="DYXUYBCOYN",Visible=.t., Left=275, Top=59,;
    Alignment=1, Width=127, Height=18,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="KNTMQONIRV",Visible=.t., Left=13, Top=84,;
    Alignment=1, Width=127, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="VWUHOXMRFC",Visible=.t., Left=2, Top=59,;
    Alignment=1, Width=138, Height=18,;
    Caption="Tipo elemento contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="UXEYQIBKFW",Visible=.t., Left=504, Top=85,;
    Alignment=1, Width=127, Height=18,;
    Caption="Tipo di applicazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="EQQEPKEFWS",Visible=.t., Left=282, Top=168,;
    Alignment=1, Width=73, Height=18,;
    Caption="Periodicit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="FHHIMOYASQ",Visible=.t., Left=13, Top=192,;
    Alignment=1, Width=127, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="TUUCZKGXKW",Visible=.t., Left=13, Top=216,;
    Alignment=1, Width=127, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="MSBVDPRUIW",Visible=.t., Left=13, Top=354,;
    Alignment=1, Width=127, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="IUYZDHDEFK",Visible=.t., Left=312, Top=556,;
    Alignment=1, Width=104, Height=18,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="EGBMCZOWYE",Visible=.t., Left=551, Top=556,;
    Alignment=1, Width=140, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="ZBYWJLRUSY",Visible=.t., Left=542, Top=330,;
    Alignment=1, Width=185, Height=18,;
    Caption="Giorni di tolleranza per gen. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="QGIHCPFRKV",Visible=.t., Left=548, Top=168,;
    Alignment=1, Width=181, Height=18,;
    Caption="Giorni di tolleranza per gen. att.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="YZVVOCOKFH",Visible=.t., Left=10, Top=150,;
    Alignment=0, Width=64, Height=19,;
    Caption="Attivit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_94 as StdString with uid="HTISCBXRRV",Visible=.t., Left=10, Top=311,;
    Alignment=0, Width=115, Height=19,;
    Caption="Documento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_96 as StdString with uid="JOMWBBMPTU",Visible=.t., Left=13, Top=496,;
    Alignment=1, Width=127, Height=18,;
    Caption="Periodicit� rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="TSFVLERUOH",Visible=.t., Left=259, Top=522,;
    Alignment=1, Width=143, Height=18,;
    Caption="Giorni preavviso disdetta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_99 as StdString with uid="UFRWATMVMF",Visible=.t., Left=10, Top=451,;
    Alignment=0, Width=115, Height=15,;
    Caption="Rinnovi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_100 as StdString with uid="FSSFZXZNCU",Visible=.t., Left=13, Top=109,;
    Alignment=1, Width=127, Height=18,;
    Caption="Servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_102 as StdString with uid="ALDORVKJFM",Visible=.t., Left=13, Top=135,;
    Alignment=1, Width=127, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oStr_1_103 as StdString with uid="WTZGMDWMAM",Visible=.t., Left=193, Top=135,;
    Alignment=1, Width=127, Height=18,;
    Caption="Quantit� totale:"  ;
  , bGlobalFont=.t.

  func oStr_1_103.mHide()
    with this.Parent.oContained
      return (.w_MOTIPCON<>'P')
    endwith
  endfunc

  add object oStr_1_105 as StdString with uid="NWBYOWEKEN",Visible=.t., Left=12, Top=630,;
    Alignment=0, Width=367, Height=22,;
    Caption="Attenzione! Il campo MOCODSER � sovrapposto"  ;
    , FontName = "Arial", FontSize = 12, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_105.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_107 as StdString with uid="XEAKIECWXJ",Visible=.t., Left=13, Top=406,;
    Alignment=1, Width=127, Height=18,;
    Caption="Descrizione servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_108 as StdString with uid="OKOZCMFPKF",Visible=.t., Left=13, Top=265,;
    Alignment=1, Width=127, Height=18,;
    Caption="Descrizione servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_111 as StdString with uid="ZTFIEWGENC",Visible=.t., Left=586, Top=193,;
    Alignment=1, Width=63, Height=18,;
    Caption="Prima data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_112 as StdString with uid="RNBWTIOYTL",Visible=.t., Left=586, Top=354,;
    Alignment=1, Width=63, Height=18,;
    Caption="Prima data:"  ;
  , bGlobalFont=.t.

  add object oBox_1_91 as StdBox with uid="LIPBORAQCJ",left=11, top=164, width=763,height=2

  add object oBox_1_93 as StdBox with uid="TNGEULSYRY",left=11, top=325, width=763,height=2

  add object oBox_1_98 as StdBox with uid="IWBAEXBSMX",left=11, top=466, width=763,height=2
enddefine
define class tgsag_amePag2 as StdContainer
  Width  = 777
  height = 578
  stdWidth  = 777
  stdheight = 578
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMOVOCCEN_2_3 as StdField with uid="ZMRLDMITNI",rtseq=77,rtrep=.f.,;
    cFormVar = "w_MOVOCCEN", cQueryName = "MOVOCCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Voce di ricavo",;
    HelpContextID = 184192276,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=137, Top=35, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_MOVOCCEN"

  func oMOVOCCEN_2_3.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' or g_COMM='S'))
    endwith
  endfunc

  func oMOVOCCEN_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOVOCCEN_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOVOCCEN_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oMOVOCCEN_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di ricavo",'',this.parent.oContained
  endproc
  proc oMOVOCCEN_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_MOVOCCEN
     i_obj.ecpSave()
  endproc

  add object oMOCODCEN_2_4 as StdField with uid="HDYPVGHTFH",rtseq=78,rtrep=.f.,;
    cFormVar = "w_MOCODCEN", cQueryName = "MOCODCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di ricavo incongruente o obsoleto",;
    ToolTipText = "Codice del centro di ricavo",;
    HelpContextID = 185163028,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=137, Top=61, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_MOCODCEN"

  func oMOCODCEN_2_4.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oMOCODCEN_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODCEN_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODCEN_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oMOCODCEN_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di ricavo",'',this.parent.oContained
  endproc
  proc oMOCODCEN_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_MOCODCEN
     i_obj.ecpSave()
  endproc

  add object oMOCOCOMM_2_5 as StdField with uid="IPNZXVYLYC",rtseq=79,rtrep=.f.,;
    cFormVar = "w_MOCOCOMM", cQueryName = "MOCOCOMM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    ToolTipText = "Codice della commessa associata",;
    HelpContextID = 151429869,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=137, Top=87, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MOCOCOMM"

  func oMOCOCOMM_2_5.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc

  func oMOCOCOMM_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
      if .not. empty(.w_MOCODATT)
        bRes2=.link_2_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMOCOCOMM_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCOCOMM_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oMOCOCOMM_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oMOCOCOMM_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_MOCOCOMM
     i_obj.ecpSave()
  endproc

  add object oMOCODATT_2_6 as StdField with uid="DBUJHQHJQR",rtseq=80,rtrep=.f.,;
    cFormVar = "w_MOCODATT", cQueryName = "MOCODATT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 151608602,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=137, Top=113, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_MOCOCOMM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_ATTIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_MOCODATT"

  func oMOCODATT_2_6.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  func oMOCODATT_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODATT_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODATT_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_MOCOCOMM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_ATTIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_MOCOCOMM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_ATTIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oMOCODATT_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'',this.parent.oContained
  endproc
  proc oMOCODATT_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_MOCOCOMM
    i_obj.ATTIPATT=w_ATTIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_MOCODATT
     i_obj.ecpSave()
  endproc

  add object oDESVOC_2_11 as StdField with uid="TBESVEIPFD",rtseq=81,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 197218870,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=260, Top=35, InputMask=replicate('X',40)

  func oDESVOC_2_11.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' or g_COMM='S'))
    endwith
  endfunc

  add object oDESCEN_2_12 as StdField with uid="TZYJTUVYJB",rtseq=82,rtrep=.t.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 101601846,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=260, Top=61, InputMask=replicate('X',40)

  func oDESCEN_2_12.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oDESCOMM_2_13 as StdField with uid="XFJRSTNDWN",rtseq=83,rtrep=.f.,;
    cFormVar = "w_DESCOMM", cQueryName = "DESCOMM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 173125066,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=260, Top=87, InputMask=replicate('X',100)

  func oDESCOMM_2_13.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc

  add object oDESATTI_2_14 as StdField with uid="ZEOQLRVGNK",rtseq=84,rtrep=.f.,;
    cFormVar = "w_DESATTI", cQueryName = "DESATTI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 50572746,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=260, Top=113, InputMask=replicate('X',100)

  func oDESATTI_2_14.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oStr_2_1 as StdString with uid="YPYTQEMOYQ",Visible=.t., Left=6, Top=13,;
    Alignment=0, Width=100, Height=19,;
    Caption="Analitica"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="DKVSLUUGIB",Visible=.t., Left=9, Top=35,;
    Alignment=1, Width=127, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_7.mHide()
    with this.Parent.oContained
      return (NOT (g_PERCCR='S' or g_COMM='S'))
    endwith
  endfunc

  add object oStr_2_8 as StdString with uid="UHHHGWTMRT",Visible=.t., Left=9, Top=61,;
    Alignment=1, Width=127, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_8.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_2_9 as StdString with uid="DFUFULWPYZ",Visible=.t., Left=9, Top=87,;
    Alignment=1, Width=127, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_9.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc

  add object oStr_2_10 as StdString with uid="TPZUSQZLJW",Visible=.t., Left=9, Top=113,;
    Alignment=1, Width=127, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_10.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oBox_2_2 as StdBox with uid="COHYAOXIQL",left=7, top=30, width=763,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_ame','MOD_ELEM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MOCODICE=MOD_ELEM.MOCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
