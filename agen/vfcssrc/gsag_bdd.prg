* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bdd                                                        *
*              Controllo congruit� con tipologia ente                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-07                                                      *
* Last revis.: 2013-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Param,Param2
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bdd",oParentObject,m.Param,m.Param2)
return(i_retval)

define class tgsag_bdd as StdBatch
  * --- Local variables
  Param = space(1)
  Param2 = space(3)
  w_PADRE = .NULL.
  w_cKey = space(20)
  w_DESCOD = space(40)
  w_DESSUP = space(20)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo di congruit� con la tipologia dell'ente
    * --- Param = A : Inserimento prestazioni (gsag_mpr)
    *                  = B : Dettaglio attivit� (gsag_mda)
    *                  = C : Documenti vendite (gsve_mdv)
    * --- Param2 = PRA : Pratica
    *                     = PRE : Prestazione
    this.w_PADRE = this.oParentObject
    do case
      case this.Param = "A"
        if this.Param2="PRE"
          * --- Attiva il Post-In relativo al Codice Articolo associato al Codice di Ricerca
          this.w_cKey = cp_setAzi(i_TableProp[this.w_PADRE.ART_ICOL_IDX, 2]) + "\" + cp_ToStr(this.oParentObject.w_CACODART, 1)
          cp_ShowWarn(this.w_cKey, this.w_PADRE.ART_ICOL_IDX)
        endif
        * --- Inserimento prestazioni
        if Isalt() and NOT(chktipen(this.oParentObject.w_DACODATT,this.oParentObject.w_PRNUMPRA," ")) 
          do case
            case this.Param2 = "PRA"
              * --- Pratica
              ah_ErrorMsg("Pratica non congruente con la tipologia dell'ente")
              this.oParentObject.w_PRNUMPRA = space(15)
            case this.Param2 = "PRE"
              * --- Prestazione
              ah_ErrorMsg("Prestazione non congruente con la tipologia dell'ente")
              this.oParentObject.w_DACODATT = space(20)
          endcase
        endif
        if NOT EMPTY(this.oParentObject.w_CODLIN) AND this.oParentObject.w_CODLIN<>g_CODLIN
          * --- Legge la Traduzione
          DECLARE ARRRIS (2) 
 a=Tradlin(this.oParentObject.w_DACODATT,this.oParentObject.w_CODLIN,"TRADARTI",@ARRRIS,"B") 
 this.w_DESCOD=ARRRIS(1) 
 this.w_DESSUP=ARRRIS(2)
          if Not Empty(this.w_DESCOD)
            this.oParentObject.w_PRDESPRE = this.w_DESCOD
            this.oParentObject.w_PRDESAGG = this.w_DESSUP
          endif
        endif
      case this.Param = "B"
        * --- Dettaglio attivit�
        if Isalt() and NOT(chktipen(this.oParentObject.w_DACODATT, this.oParentObject.oParentObject.w_ATCODPRA, this.oParentObject.oParentObject.w_AT__ENTE))
          * --- Prestazione
          ah_ErrorMsg("Prestazione non congruente con la tipologia dell'ente")
          this.oParentObject.w_DACODATT = space(20)
        endif
        * --- In presenza di una spesa/anticipazione collegata ad una prestazione
        if ISAlt() AND this.oparentobject.cFunction="Edit" AND this.oparentobject.CurrentEvent="w_DACODATT Changed" AND NOT EMPTY(this.oParentObject.w_DARIFPRE) AND this.oParentObject.w_OLD_ARPRESTA<>this.oParentObject.w_ARPRESTA
          ah_ErrorMsg("Non � possibile variare la tipologia di una spesa o di un'anticipazione collegata ad una prestazione.")
          this.oParentObject.w_DACODATT = space(20)
        endif
        * --- Traduzione in Lingua della Descrizione Articoli 
        if NOT EMPTY(this.oParentObject.w_CODLIN) AND this.oParentObject.w_CODLIN<>g_CODLIN
          * --- Legge la Traduzione
          DECLARE ARRRIS (2) 
 a=Tradlin(this.oParentObject.w_DACODATT,this.oParentObject.w_CODLIN,"TRADARTI",@ARRRIS,"B") 
 this.w_DESCOD=ARRRIS(1) 
 this.w_DESSUP=ARRRIS(2)
          if Not Empty(this.w_DESCOD)
            this.oParentObject.w_DADESATT = this.w_DESCOD
            this.oParentObject.w_DADESAGG = this.w_DESSUP
            this.oParentObject.w_EDESATT = this.w_DESCOD
            this.oParentObject.w_RDESATT = this.w_DESCOD
            this.oParentObject.w_EDESAGG = this.w_DESSUP
            this.oParentObject.w_RDESAGG = this.w_DESSUP
            this.oParentObject.o_DACODRES = this.oParentObject.w_DACODRES
          endif
        endif
      case this.Param = "C"
        * --- Documenti vendite
        if IsAlt()
          if Isalt() and NOT(chktipen(this.oParentObject.w_MVCODICE, this.oParentObject.w_MVCODCOM, " "))
            if this.Param2 = "PRA"
              * --- Pratica
              ah_ErrorMsg("Pratica non congruente con la tipologia dell'ente")
              this.oParentObject.w_MVCODCOM = space(15)
            else
              * --- Prestazione
              ah_ErrorMsg("Prestazione non congruente con la tipologia dell'ente")
              this.oParentObject.w_MVCODICE = space(20)
            endif
          endif
        endif
      case this.Param = "D"
        this.w_PADRE.oParentObject.NotifyEvent("AggPre")     
    endcase
  endproc


  proc Init(oParentObject,Param,Param2)
    this.Param=Param
    this.Param2=Param2
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Param,Param2"
endproc
