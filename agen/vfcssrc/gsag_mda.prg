* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mda                                                        *
*              Dettaglio attivit�                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-14                                                      *
* Last revis.: 2015-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsag_mda")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsag_mda")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsag_mda")
  return

* --- Class definition
define class tgsag_mda as StdPCForm
  Width  = 777
  Height = 362
  Top    = 10
  Left   = 10
  cComment = "Dettaglio attivit�"
  cPrg = "gsag_mda"
  HelpContextID=188078231
  add object cnt as tcgsag_mda
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsag_mda as PCContext
  w_DASERIAL = space(20)
  w_ISALT = space(1)
  w_ISAHE = space(1)
  w_ISAHR = space(1)
  w_FLGLIS = space(1)
  w_DATALIST = space(8)
  w_MVCODVAL = space(3)
  w_CAUATT = space(20)
  w_TipoRisorsa = space(1)
  w_CODCOM = space(15)
  w_OLDCOM = space(15)
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_FLDANA = space(1)
  w_FLACOMAQ = space(1)
  w_OB_TEST = space(8)
  w_KEYLISTB = space(10)
  w_MVFLVEAC = space(3)
  w_LISACQ = space(5)
  w_CODLIS = space(5)
  w_FLACQ = space(1)
  w_CAUACQ = space(5)
  w_CAUDOC = space(5)
  w_DACODNOM = space(15)
  w_PR__DATA = space(10)
  w_OLDCOMRIC = space(15)
  w_OLDATTC = space(15)
  w_OLDATTR = space(15)
  w_CODLIN = space(3)
  w_VOCECR = space(1)
  w_LetturaParAlte = space(5)
  w_NOEDDES = space(1)
  w_TIPO = space(1)
  w_CODCLI = space(15)
  w_BUFLANAL = space(1)
  w_FLCCRAUTO = space(10)
  w_DACODRES = space(5)
  w_CPROWORD = 0
  w_DACODICE = space(41)
  w_RCACODART = space(20)
  w_DACODATT = space(20)
  w_CACODART = space(20)
  w_ARTIPRIG = space(1)
  w_ARTIPRI2 = space(1)
  w_CATIPRIG = space(1)
  w_CATIPRI2 = space(1)
  w_DATIPRIG = space(1)
  w_DATIPRI2 = space(1)
  w_UNMIS1 = space(3)
  w_EMOLTI3 = 0
  w_RMOLTI3 = 0
  w_FLFRAZ1 = space(1)
  w_MOLTI3 = 0
  w_EOPERA3 = space(1)
  w_ROPERA3 = space(1)
  w_MODUM2 = space(1)
  w_OPERA3 = space(1)
  w_OPERAT = space(1)
  w_ECOD1ATT = space(20)
  w_RCOD1ATT = space(20)
  w_PREZUM = space(1)
  w_EDESAGG = space(10)
  w_RDESAGG = space(10)
  w_EDESATT = space(40)
  w_RDESATT = space(40)
  w_COD1ATT = space(20)
  w_DADESATT = space(40)
  w_DESRIS = space(39)
  w_DAUNIMIS = space(3)
  w_TIPART = space(2)
  w_DAQTAMOV = 0
  w_DAPREZZO = 0
  w_DADESAGG = space(10)
  w_DACODOPE = 0
  w_DADATMOD = space(8)
  w_DTIPRIG = space(10)
  w_DAVALRIG = 0
  w_ATIPRIG = space(1)
  w_ETIPRIG = space(10)
  w_RTIPRIG = space(10)
  w_ATIPRI2 = space(1)
  w_COST_ORA = 0
  w_DAOREEFF = 0
  w_DAMINEFF = 0
  w_OLDCEN = space(15)
  w_OLDCENRIC = space(15)
  w_LICOSTO = 0
  w_CENCOS = space(15)
  w_DACOSUNI = 0
  w_DCODCEN = space(15)
  w_DACOSINT = 0
  w_FLRESP = space(1)
  w_UNMIS2 = space(3)
  w_RUNMIS3 = space(3)
  w_ECACODART = space(20)
  w_FLSERG = space(1)
  w_UNMIS3 = space(3)
  w_DESUNI = space(35)
  w_DAFLDEFF = space(1)
  w_QTAUM1 = 0
  w_COGNOME = space(40)
  w_NOME = space(40)
  w_DAPREMIN = 0
  w_DAPREMAX = 0
  w_DAGAZUFF = space(6)
  w_DENOM_RESP = space(39)
  w_NOTIFICA = space(1)
  w_DARIFRIG = 0
  w_CHKTEMP = space(1)
  w_DUR_ORE = 0
  w_ECADTOBSO = space(8)
  w_RCADTOBSO = space(8)
  w_CADTOBSO = space(8)
  w_DACODCOM = space(15)
  w_DAVOCRIC = space(15)
  w_DAVOCCOS = space(15)
  w_DAINICOM = space(8)
  w_DAFINCOM = space(8)
  w_DA_SEGNO = space(1)
  w_DACOMRIC = space(15)
  w_DACENCOS = space(15)
  w_DAATTIVI = space(15)
  w_ETIPSER = space(1)
  w_RTIPSER = space(1)
  w_SERPER = space(41)
  w_TIPSER = space(1)
  w_TIPRIS = space(1)
  w_FLUSEP = space(1)
  w_MOLTIP = 0
  w_ARTIPART = space(2)
  w_EUNMIS3 = space(3)
  w_DACODLIS = space(5)
  w_DAPROLIS = space(5)
  w_DAPROSCO = space(5)
  w_DASCOLIS = space(5)
  w_DATOBSO = space(8)
  w_DACENRIC = space(15)
  w_DAATTRIC = space(15)
  w_DASCONT1 = 0
  w_DASCONT2 = 0
  w_DASCONT3 = 0
  w_DASCONT4 = 0
  w_FL_FRAZ = space(1)
  w_DALISACQ = space(5)
  w_DATCOINI = space(8)
  w_DATCOFIN = space(8)
  w_DATRIINI = space(8)
  w_DATRIFIN = space(8)
  w_CENRIC = space(15)
  w_FLSCOR = space(1)
  w_CEN_CauDoc = space(15)
  w_DARIFPRE = 0
  w_DARIGPRE = space(1)
  w_FLSPAN = space(10)
  w_DARIFPRE = 0
  w_DARIGPRE = space(1)
  w_DACONCOD = space(15)
  w_DACONTRA = space(10)
  w_DACODMOD = space(10)
  w_DACODIMP = space(10)
  w_DACOCOMP = 0
  w_DARINNOV = 0
  w_FLUNIV = space(1)
  w_NUMPRE = 0
  w_ARPRESTA = space(1)
  w_FLGZER = space(1)
  w_COTIPCON = space(1)
  w_COCODCON = space(15)
  w_NUMSCO = 0
  w_ETIPRI2 = space(10)
  w_RTIPRI2 = space(10)
  w_TIPRIGCA = space(1)
  w_TIPRI2CA = space(1)
  w_GENPRE = space(1)
  w_FLCNTCHK = space(1)
  w_CHKDATAPRE = space(1)
  w_GG_PRE = 0
  w_GG_SUC = 0
  w_DATFIN = space(8)
  w_CATCOM = space(5)
  w_OLD_ARPRESTA = space(1)
  w_OKTARCON = space(1)
  proc Save(i_oFrom)
    this.w_DASERIAL = i_oFrom.w_DASERIAL
    this.w_ISALT = i_oFrom.w_ISALT
    this.w_ISAHE = i_oFrom.w_ISAHE
    this.w_ISAHR = i_oFrom.w_ISAHR
    this.w_FLGLIS = i_oFrom.w_FLGLIS
    this.w_DATALIST = i_oFrom.w_DATALIST
    this.w_MVCODVAL = i_oFrom.w_MVCODVAL
    this.w_CAUATT = i_oFrom.w_CAUATT
    this.w_TipoRisorsa = i_oFrom.w_TipoRisorsa
    this.w_CODCOM = i_oFrom.w_CODCOM
    this.w_OLDCOM = i_oFrom.w_OLDCOM
    this.w_FLANAL = i_oFrom.w_FLANAL
    this.w_FLGCOM = i_oFrom.w_FLGCOM
    this.w_FLDANA = i_oFrom.w_FLDANA
    this.w_FLACOMAQ = i_oFrom.w_FLACOMAQ
    this.w_OB_TEST = i_oFrom.w_OB_TEST
    this.w_KEYLISTB = i_oFrom.w_KEYLISTB
    this.w_MVFLVEAC = i_oFrom.w_MVFLVEAC
    this.w_LISACQ = i_oFrom.w_LISACQ
    this.w_CODLIS = i_oFrom.w_CODLIS
    this.w_FLACQ = i_oFrom.w_FLACQ
    this.w_CAUACQ = i_oFrom.w_CAUACQ
    this.w_CAUDOC = i_oFrom.w_CAUDOC
    this.w_DACODNOM = i_oFrom.w_DACODNOM
    this.w_PR__DATA = i_oFrom.w_PR__DATA
    this.w_OLDCOMRIC = i_oFrom.w_OLDCOMRIC
    this.w_OLDATTC = i_oFrom.w_OLDATTC
    this.w_OLDATTR = i_oFrom.w_OLDATTR
    this.w_CODLIN = i_oFrom.w_CODLIN
    this.w_VOCECR = i_oFrom.w_VOCECR
    this.w_LetturaParAlte = i_oFrom.w_LetturaParAlte
    this.w_NOEDDES = i_oFrom.w_NOEDDES
    this.w_TIPO = i_oFrom.w_TIPO
    this.w_CODCLI = i_oFrom.w_CODCLI
    this.w_BUFLANAL = i_oFrom.w_BUFLANAL
    this.w_FLCCRAUTO = i_oFrom.w_FLCCRAUTO
    this.w_DACODRES = i_oFrom.w_DACODRES
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_DACODICE = i_oFrom.w_DACODICE
    this.w_RCACODART = i_oFrom.w_RCACODART
    this.w_DACODATT = i_oFrom.w_DACODATT
    this.w_CACODART = i_oFrom.w_CACODART
    this.w_ARTIPRIG = i_oFrom.w_ARTIPRIG
    this.w_ARTIPRI2 = i_oFrom.w_ARTIPRI2
    this.w_CATIPRIG = i_oFrom.w_CATIPRIG
    this.w_CATIPRI2 = i_oFrom.w_CATIPRI2
    this.w_DATIPRIG = i_oFrom.w_DATIPRIG
    this.w_DATIPRI2 = i_oFrom.w_DATIPRI2
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_EMOLTI3 = i_oFrom.w_EMOLTI3
    this.w_RMOLTI3 = i_oFrom.w_RMOLTI3
    this.w_FLFRAZ1 = i_oFrom.w_FLFRAZ1
    this.w_MOLTI3 = i_oFrom.w_MOLTI3
    this.w_EOPERA3 = i_oFrom.w_EOPERA3
    this.w_ROPERA3 = i_oFrom.w_ROPERA3
    this.w_MODUM2 = i_oFrom.w_MODUM2
    this.w_OPERA3 = i_oFrom.w_OPERA3
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_ECOD1ATT = i_oFrom.w_ECOD1ATT
    this.w_RCOD1ATT = i_oFrom.w_RCOD1ATT
    this.w_PREZUM = i_oFrom.w_PREZUM
    this.w_EDESAGG = i_oFrom.w_EDESAGG
    this.w_RDESAGG = i_oFrom.w_RDESAGG
    this.w_EDESATT = i_oFrom.w_EDESATT
    this.w_RDESATT = i_oFrom.w_RDESATT
    this.w_COD1ATT = i_oFrom.w_COD1ATT
    this.w_DADESATT = i_oFrom.w_DADESATT
    this.w_DESRIS = i_oFrom.w_DESRIS
    this.w_DAUNIMIS = i_oFrom.w_DAUNIMIS
    this.w_TIPART = i_oFrom.w_TIPART
    this.w_DAQTAMOV = i_oFrom.w_DAQTAMOV
    this.w_DAPREZZO = i_oFrom.w_DAPREZZO
    this.w_DADESAGG = i_oFrom.w_DADESAGG
    this.w_DACODOPE = i_oFrom.w_DACODOPE
    this.w_DADATMOD = i_oFrom.w_DADATMOD
    this.w_DTIPRIG = i_oFrom.w_DTIPRIG
    this.w_DAVALRIG = i_oFrom.w_DAVALRIG
    this.w_ATIPRIG = i_oFrom.w_ATIPRIG
    this.w_ETIPRIG = i_oFrom.w_ETIPRIG
    this.w_RTIPRIG = i_oFrom.w_RTIPRIG
    this.w_ATIPRI2 = i_oFrom.w_ATIPRI2
    this.w_COST_ORA = i_oFrom.w_COST_ORA
    this.w_DAOREEFF = i_oFrom.w_DAOREEFF
    this.w_DAMINEFF = i_oFrom.w_DAMINEFF
    this.w_OLDCEN = i_oFrom.w_OLDCEN
    this.w_OLDCENRIC = i_oFrom.w_OLDCENRIC
    this.w_LICOSTO = i_oFrom.w_LICOSTO
    this.w_CENCOS = i_oFrom.w_CENCOS
    this.w_DACOSUNI = i_oFrom.w_DACOSUNI
    this.w_DCODCEN = i_oFrom.w_DCODCEN
    this.w_DACOSINT = i_oFrom.w_DACOSINT
    this.w_FLRESP = i_oFrom.w_FLRESP
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_RUNMIS3 = i_oFrom.w_RUNMIS3
    this.w_ECACODART = i_oFrom.w_ECACODART
    this.w_FLSERG = i_oFrom.w_FLSERG
    this.w_UNMIS3 = i_oFrom.w_UNMIS3
    this.w_DESUNI = i_oFrom.w_DESUNI
    this.w_DAFLDEFF = i_oFrom.w_DAFLDEFF
    this.w_QTAUM1 = i_oFrom.w_QTAUM1
    this.w_COGNOME = i_oFrom.w_COGNOME
    this.w_NOME = i_oFrom.w_NOME
    this.w_DAPREMIN = i_oFrom.w_DAPREMIN
    this.w_DAPREMAX = i_oFrom.w_DAPREMAX
    this.w_DAGAZUFF = i_oFrom.w_DAGAZUFF
    this.w_DENOM_RESP = i_oFrom.w_DENOM_RESP
    this.w_NOTIFICA = i_oFrom.w_NOTIFICA
    this.w_DARIFRIG = i_oFrom.w_DARIFRIG
    this.w_CHKTEMP = i_oFrom.w_CHKTEMP
    this.w_DUR_ORE = i_oFrom.w_DUR_ORE
    this.w_ECADTOBSO = i_oFrom.w_ECADTOBSO
    this.w_RCADTOBSO = i_oFrom.w_RCADTOBSO
    this.w_CADTOBSO = i_oFrom.w_CADTOBSO
    this.w_DACODCOM = i_oFrom.w_DACODCOM
    this.w_DAVOCRIC = i_oFrom.w_DAVOCRIC
    this.w_DAVOCCOS = i_oFrom.w_DAVOCCOS
    this.w_DAINICOM = i_oFrom.w_DAINICOM
    this.w_DAFINCOM = i_oFrom.w_DAFINCOM
    this.w_DA_SEGNO = i_oFrom.w_DA_SEGNO
    this.w_DACOMRIC = i_oFrom.w_DACOMRIC
    this.w_DACENCOS = i_oFrom.w_DACENCOS
    this.w_DAATTIVI = i_oFrom.w_DAATTIVI
    this.w_ETIPSER = i_oFrom.w_ETIPSER
    this.w_RTIPSER = i_oFrom.w_RTIPSER
    this.w_SERPER = i_oFrom.w_SERPER
    this.w_TIPSER = i_oFrom.w_TIPSER
    this.w_TIPRIS = i_oFrom.w_TIPRIS
    this.w_FLUSEP = i_oFrom.w_FLUSEP
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_ARTIPART = i_oFrom.w_ARTIPART
    this.w_EUNMIS3 = i_oFrom.w_EUNMIS3
    this.w_DACODLIS = i_oFrom.w_DACODLIS
    this.w_DAPROLIS = i_oFrom.w_DAPROLIS
    this.w_DAPROSCO = i_oFrom.w_DAPROSCO
    this.w_DASCOLIS = i_oFrom.w_DASCOLIS
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_DACENRIC = i_oFrom.w_DACENRIC
    this.w_DAATTRIC = i_oFrom.w_DAATTRIC
    this.w_DASCONT1 = i_oFrom.w_DASCONT1
    this.w_DASCONT2 = i_oFrom.w_DASCONT2
    this.w_DASCONT3 = i_oFrom.w_DASCONT3
    this.w_DASCONT4 = i_oFrom.w_DASCONT4
    this.w_FL_FRAZ = i_oFrom.w_FL_FRAZ
    this.w_DALISACQ = i_oFrom.w_DALISACQ
    this.w_DATCOINI = i_oFrom.w_DATCOINI
    this.w_DATCOFIN = i_oFrom.w_DATCOFIN
    this.w_DATRIINI = i_oFrom.w_DATRIINI
    this.w_DATRIFIN = i_oFrom.w_DATRIFIN
    this.w_CENRIC = i_oFrom.w_CENRIC
    this.w_FLSCOR = i_oFrom.w_FLSCOR
    this.w_CEN_CauDoc = i_oFrom.w_CEN_CauDoc
    this.w_DARIFPRE = i_oFrom.w_DARIFPRE
    this.w_DARIGPRE = i_oFrom.w_DARIGPRE
    this.w_FLSPAN = i_oFrom.w_FLSPAN
    this.w_DARIFPRE = i_oFrom.w_DARIFPRE
    this.w_DARIGPRE = i_oFrom.w_DARIGPRE
    this.w_DACONCOD = i_oFrom.w_DACONCOD
    this.w_DACONTRA = i_oFrom.w_DACONTRA
    this.w_DACODMOD = i_oFrom.w_DACODMOD
    this.w_DACODIMP = i_oFrom.w_DACODIMP
    this.w_DACOCOMP = i_oFrom.w_DACOCOMP
    this.w_DARINNOV = i_oFrom.w_DARINNOV
    this.w_FLUNIV = i_oFrom.w_FLUNIV
    this.w_NUMPRE = i_oFrom.w_NUMPRE
    this.w_ARPRESTA = i_oFrom.w_ARPRESTA
    this.w_FLGZER = i_oFrom.w_FLGZER
    this.w_COTIPCON = i_oFrom.w_COTIPCON
    this.w_COCODCON = i_oFrom.w_COCODCON
    this.w_NUMSCO = i_oFrom.w_NUMSCO
    this.w_ETIPRI2 = i_oFrom.w_ETIPRI2
    this.w_RTIPRI2 = i_oFrom.w_RTIPRI2
    this.w_TIPRIGCA = i_oFrom.w_TIPRIGCA
    this.w_TIPRI2CA = i_oFrom.w_TIPRI2CA
    this.w_GENPRE = i_oFrom.w_GENPRE
    this.w_FLCNTCHK = i_oFrom.w_FLCNTCHK
    this.w_CHKDATAPRE = i_oFrom.w_CHKDATAPRE
    this.w_GG_PRE = i_oFrom.w_GG_PRE
    this.w_GG_SUC = i_oFrom.w_GG_SUC
    this.w_DATFIN = i_oFrom.w_DATFIN
    this.w_CATCOM = i_oFrom.w_CATCOM
    this.w_OLD_ARPRESTA = i_oFrom.w_OLD_ARPRESTA
    this.w_OKTARCON = i_oFrom.w_OKTARCON
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DASERIAL = this.w_DASERIAL
    i_oTo.w_ISALT = this.w_ISALT
    i_oTo.w_ISAHE = this.w_ISAHE
    i_oTo.w_ISAHR = this.w_ISAHR
    i_oTo.w_FLGLIS = this.w_FLGLIS
    i_oTo.w_DATALIST = this.w_DATALIST
    i_oTo.w_MVCODVAL = this.w_MVCODVAL
    i_oTo.w_CAUATT = this.w_CAUATT
    i_oTo.w_TipoRisorsa = this.w_TipoRisorsa
    i_oTo.w_CODCOM = this.w_CODCOM
    i_oTo.w_OLDCOM = this.w_OLDCOM
    i_oTo.w_FLANAL = this.w_FLANAL
    i_oTo.w_FLGCOM = this.w_FLGCOM
    i_oTo.w_FLDANA = this.w_FLDANA
    i_oTo.w_FLACOMAQ = this.w_FLACOMAQ
    i_oTo.w_OB_TEST = this.w_OB_TEST
    i_oTo.w_KEYLISTB = this.w_KEYLISTB
    i_oTo.w_MVFLVEAC = this.w_MVFLVEAC
    i_oTo.w_LISACQ = this.w_LISACQ
    i_oTo.w_CODLIS = this.w_CODLIS
    i_oTo.w_FLACQ = this.w_FLACQ
    i_oTo.w_CAUACQ = this.w_CAUACQ
    i_oTo.w_CAUDOC = this.w_CAUDOC
    i_oTo.w_DACODNOM = this.w_DACODNOM
    i_oTo.w_PR__DATA = this.w_PR__DATA
    i_oTo.w_OLDCOMRIC = this.w_OLDCOMRIC
    i_oTo.w_OLDATTC = this.w_OLDATTC
    i_oTo.w_OLDATTR = this.w_OLDATTR
    i_oTo.w_CODLIN = this.w_CODLIN
    i_oTo.w_VOCECR = this.w_VOCECR
    i_oTo.w_LetturaParAlte = this.w_LetturaParAlte
    i_oTo.w_NOEDDES = this.w_NOEDDES
    i_oTo.w_TIPO = this.w_TIPO
    i_oTo.w_CODCLI = this.w_CODCLI
    i_oTo.w_BUFLANAL = this.w_BUFLANAL
    i_oTo.w_FLCCRAUTO = this.w_FLCCRAUTO
    i_oTo.w_DACODRES = this.w_DACODRES
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_DACODICE = this.w_DACODICE
    i_oTo.w_RCACODART = this.w_RCACODART
    i_oTo.w_DACODATT = this.w_DACODATT
    i_oTo.w_CACODART = this.w_CACODART
    i_oTo.w_ARTIPRIG = this.w_ARTIPRIG
    i_oTo.w_ARTIPRI2 = this.w_ARTIPRI2
    i_oTo.w_CATIPRIG = this.w_CATIPRIG
    i_oTo.w_CATIPRI2 = this.w_CATIPRI2
    i_oTo.w_DATIPRIG = this.w_DATIPRIG
    i_oTo.w_DATIPRI2 = this.w_DATIPRI2
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_EMOLTI3 = this.w_EMOLTI3
    i_oTo.w_RMOLTI3 = this.w_RMOLTI3
    i_oTo.w_FLFRAZ1 = this.w_FLFRAZ1
    i_oTo.w_MOLTI3 = this.w_MOLTI3
    i_oTo.w_EOPERA3 = this.w_EOPERA3
    i_oTo.w_ROPERA3 = this.w_ROPERA3
    i_oTo.w_MODUM2 = this.w_MODUM2
    i_oTo.w_OPERA3 = this.w_OPERA3
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_ECOD1ATT = this.w_ECOD1ATT
    i_oTo.w_RCOD1ATT = this.w_RCOD1ATT
    i_oTo.w_PREZUM = this.w_PREZUM
    i_oTo.w_EDESAGG = this.w_EDESAGG
    i_oTo.w_RDESAGG = this.w_RDESAGG
    i_oTo.w_EDESATT = this.w_EDESATT
    i_oTo.w_RDESATT = this.w_RDESATT
    i_oTo.w_COD1ATT = this.w_COD1ATT
    i_oTo.w_DADESATT = this.w_DADESATT
    i_oTo.w_DESRIS = this.w_DESRIS
    i_oTo.w_DAUNIMIS = this.w_DAUNIMIS
    i_oTo.w_TIPART = this.w_TIPART
    i_oTo.w_DAQTAMOV = this.w_DAQTAMOV
    i_oTo.w_DAPREZZO = this.w_DAPREZZO
    i_oTo.w_DADESAGG = this.w_DADESAGG
    i_oTo.w_DACODOPE = this.w_DACODOPE
    i_oTo.w_DADATMOD = this.w_DADATMOD
    i_oTo.w_DTIPRIG = this.w_DTIPRIG
    i_oTo.w_DAVALRIG = this.w_DAVALRIG
    i_oTo.w_ATIPRIG = this.w_ATIPRIG
    i_oTo.w_ETIPRIG = this.w_ETIPRIG
    i_oTo.w_RTIPRIG = this.w_RTIPRIG
    i_oTo.w_ATIPRI2 = this.w_ATIPRI2
    i_oTo.w_COST_ORA = this.w_COST_ORA
    i_oTo.w_DAOREEFF = this.w_DAOREEFF
    i_oTo.w_DAMINEFF = this.w_DAMINEFF
    i_oTo.w_OLDCEN = this.w_OLDCEN
    i_oTo.w_OLDCENRIC = this.w_OLDCENRIC
    i_oTo.w_LICOSTO = this.w_LICOSTO
    i_oTo.w_CENCOS = this.w_CENCOS
    i_oTo.w_DACOSUNI = this.w_DACOSUNI
    i_oTo.w_DCODCEN = this.w_DCODCEN
    i_oTo.w_DACOSINT = this.w_DACOSINT
    i_oTo.w_FLRESP = this.w_FLRESP
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_RUNMIS3 = this.w_RUNMIS3
    i_oTo.w_ECACODART = this.w_ECACODART
    i_oTo.w_FLSERG = this.w_FLSERG
    i_oTo.w_UNMIS3 = this.w_UNMIS3
    i_oTo.w_DESUNI = this.w_DESUNI
    i_oTo.w_DAFLDEFF = this.w_DAFLDEFF
    i_oTo.w_QTAUM1 = this.w_QTAUM1
    i_oTo.w_COGNOME = this.w_COGNOME
    i_oTo.w_NOME = this.w_NOME
    i_oTo.w_DAPREMIN = this.w_DAPREMIN
    i_oTo.w_DAPREMAX = this.w_DAPREMAX
    i_oTo.w_DAGAZUFF = this.w_DAGAZUFF
    i_oTo.w_DENOM_RESP = this.w_DENOM_RESP
    i_oTo.w_NOTIFICA = this.w_NOTIFICA
    i_oTo.w_DARIFRIG = this.w_DARIFRIG
    i_oTo.w_CHKTEMP = this.w_CHKTEMP
    i_oTo.w_DUR_ORE = this.w_DUR_ORE
    i_oTo.w_ECADTOBSO = this.w_ECADTOBSO
    i_oTo.w_RCADTOBSO = this.w_RCADTOBSO
    i_oTo.w_CADTOBSO = this.w_CADTOBSO
    i_oTo.w_DACODCOM = this.w_DACODCOM
    i_oTo.w_DAVOCRIC = this.w_DAVOCRIC
    i_oTo.w_DAVOCCOS = this.w_DAVOCCOS
    i_oTo.w_DAINICOM = this.w_DAINICOM
    i_oTo.w_DAFINCOM = this.w_DAFINCOM
    i_oTo.w_DA_SEGNO = this.w_DA_SEGNO
    i_oTo.w_DACOMRIC = this.w_DACOMRIC
    i_oTo.w_DACENCOS = this.w_DACENCOS
    i_oTo.w_DAATTIVI = this.w_DAATTIVI
    i_oTo.w_ETIPSER = this.w_ETIPSER
    i_oTo.w_RTIPSER = this.w_RTIPSER
    i_oTo.w_SERPER = this.w_SERPER
    i_oTo.w_TIPSER = this.w_TIPSER
    i_oTo.w_TIPRIS = this.w_TIPRIS
    i_oTo.w_FLUSEP = this.w_FLUSEP
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_ARTIPART = this.w_ARTIPART
    i_oTo.w_EUNMIS3 = this.w_EUNMIS3
    i_oTo.w_DACODLIS = this.w_DACODLIS
    i_oTo.w_DAPROLIS = this.w_DAPROLIS
    i_oTo.w_DAPROSCO = this.w_DAPROSCO
    i_oTo.w_DASCOLIS = this.w_DASCOLIS
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_DACENRIC = this.w_DACENRIC
    i_oTo.w_DAATTRIC = this.w_DAATTRIC
    i_oTo.w_DASCONT1 = this.w_DASCONT1
    i_oTo.w_DASCONT2 = this.w_DASCONT2
    i_oTo.w_DASCONT3 = this.w_DASCONT3
    i_oTo.w_DASCONT4 = this.w_DASCONT4
    i_oTo.w_FL_FRAZ = this.w_FL_FRAZ
    i_oTo.w_DALISACQ = this.w_DALISACQ
    i_oTo.w_DATCOINI = this.w_DATCOINI
    i_oTo.w_DATCOFIN = this.w_DATCOFIN
    i_oTo.w_DATRIINI = this.w_DATRIINI
    i_oTo.w_DATRIFIN = this.w_DATRIFIN
    i_oTo.w_CENRIC = this.w_CENRIC
    i_oTo.w_FLSCOR = this.w_FLSCOR
    i_oTo.w_CEN_CauDoc = this.w_CEN_CauDoc
    i_oTo.w_DARIFPRE = this.w_DARIFPRE
    i_oTo.w_DARIGPRE = this.w_DARIGPRE
    i_oTo.w_FLSPAN = this.w_FLSPAN
    i_oTo.w_DARIFPRE = this.w_DARIFPRE
    i_oTo.w_DARIGPRE = this.w_DARIGPRE
    i_oTo.w_DACONCOD = this.w_DACONCOD
    i_oTo.w_DACONTRA = this.w_DACONTRA
    i_oTo.w_DACODMOD = this.w_DACODMOD
    i_oTo.w_DACODIMP = this.w_DACODIMP
    i_oTo.w_DACOCOMP = this.w_DACOCOMP
    i_oTo.w_DARINNOV = this.w_DARINNOV
    i_oTo.w_FLUNIV = this.w_FLUNIV
    i_oTo.w_NUMPRE = this.w_NUMPRE
    i_oTo.w_ARPRESTA = this.w_ARPRESTA
    i_oTo.w_FLGZER = this.w_FLGZER
    i_oTo.w_COTIPCON = this.w_COTIPCON
    i_oTo.w_COCODCON = this.w_COCODCON
    i_oTo.w_NUMSCO = this.w_NUMSCO
    i_oTo.w_ETIPRI2 = this.w_ETIPRI2
    i_oTo.w_RTIPRI2 = this.w_RTIPRI2
    i_oTo.w_TIPRIGCA = this.w_TIPRIGCA
    i_oTo.w_TIPRI2CA = this.w_TIPRI2CA
    i_oTo.w_GENPRE = this.w_GENPRE
    i_oTo.w_FLCNTCHK = this.w_FLCNTCHK
    i_oTo.w_CHKDATAPRE = this.w_CHKDATAPRE
    i_oTo.w_GG_PRE = this.w_GG_PRE
    i_oTo.w_GG_SUC = this.w_GG_SUC
    i_oTo.w_DATFIN = this.w_DATFIN
    i_oTo.w_CATCOM = this.w_CATCOM
    i_oTo.w_OLD_ARPRESTA = this.w_OLD_ARPRESTA
    i_oTo.w_OKTARCON = this.w_OKTARCON
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsag_mda as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 777
  Height = 362
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-12"
  HelpContextID=188078231
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=182

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  OFFDATTI_IDX = 0
  ART_ICOL_IDX = 0
  DIPENDEN_IDX = 0
  UNIMIS_IDX = 0
  KEY_ARTI_IDX = 0
  CENCOST_IDX = 0
  CAUMATTI_IDX = 0
  TIP_DOCU_IDX = 0
  PAR_ALTE_IDX = 0
  CON_TRAS_IDX = 0
  CONTI_IDX = 0
  cFile = "OFFDATTI"
  cKeySelect = "DASERIAL"
  cKeyWhere  = "DASERIAL=this.w_DASERIAL"
  cKeyDetail  = "DASERIAL=this.w_DASERIAL"
  cKeyWhereODBC = '"DASERIAL="+cp_ToStrODBC(this.w_DASERIAL)';

  cKeyDetailWhereODBC = '"DASERIAL="+cp_ToStrODBC(this.w_DASERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"OFFDATTI.DASERIAL="+cp_ToStrODBC(this.w_DASERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'OFFDATTI.CPROWORD '
  cPrg = "gsag_mda"
  cComment = "Dettaglio attivit�"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DASERIAL = space(20)
  o_DASERIAL = space(20)
  w_ISALT = .F.
  w_ISAHE = .F.
  w_ISAHR = .F.
  w_FLGLIS = space(1)
  w_DATALIST = ctod('  /  /  ')
  w_MVCODVAL = space(3)
  w_CAUATT = space(20)
  w_TipoRisorsa = space(1)
  w_CODCOM = space(15)
  w_OLDCOM = space(15)
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_FLDANA = space(1)
  w_FLACOMAQ = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_KEYLISTB = space(10)
  w_MVFLVEAC = space(3)
  w_LISACQ = space(5)
  w_CODLIS = space(5)
  w_FLACQ = space(1)
  w_CAUACQ = space(5)
  w_CAUDOC = space(5)
  w_DACODNOM = space(15)
  w_PR__DATA = space(10)
  w_OLDCOMRIC = space(15)
  w_OLDATTC = space(15)
  w_OLDATTR = space(15)
  w_CODLIN = space(3)
  w_VOCECR = space(1)
  w_LetturaParAlte = space(5)
  o_LetturaParAlte = space(5)
  w_NOEDDES = space(1)
  w_TIPO = space(1)
  w_CODCLI = space(15)
  w_BUFLANAL = space(1)
  w_FLCCRAUTO = space(10)
  w_DACODRES = space(5)
  o_DACODRES = space(5)
  w_CPROWORD = 0
  w_DACODICE = space(41)
  o_DACODICE = space(41)
  w_RCACODART = space(20)
  w_DACODATT = space(20)
  o_DACODATT = space(20)
  w_CACODART = space(20)
  w_ARTIPRIG = space(1)
  w_ARTIPRI2 = space(1)
  w_CATIPRIG = space(1)
  w_CATIPRI2 = space(1)
  w_DATIPRIG = space(1)
  w_DATIPRI2 = space(1)
  w_UNMIS1 = space(3)
  w_EMOLTI3 = 0
  w_RMOLTI3 = 0
  w_FLFRAZ1 = space(1)
  w_MOLTI3 = 0
  w_EOPERA3 = space(1)
  w_ROPERA3 = space(1)
  w_MODUM2 = space(1)
  w_OPERA3 = space(1)
  w_OPERAT = space(1)
  w_ECOD1ATT = space(20)
  w_RCOD1ATT = space(20)
  w_PREZUM = space(1)
  w_EDESAGG = space(0)
  w_RDESAGG = space(0)
  w_EDESATT = space(40)
  w_RDESATT = space(40)
  w_COD1ATT = space(20)
  w_DADESATT = space(40)
  w_DESRIS = space(39)
  w_DAUNIMIS = space(3)
  o_DAUNIMIS = space(3)
  w_TIPART = space(2)
  w_DAQTAMOV = 0
  o_DAQTAMOV = 0
  w_DAPREZZO = 0
  o_DAPREZZO = 0
  w_DADESAGG = space(0)
  w_DACODOPE = 0
  w_DADATMOD = ctod('  /  /  ')
  w_DTIPRIG = space(10)
  w_DAVALRIG = 0
  w_ATIPRIG = space(1)
  o_ATIPRIG = space(1)
  w_ETIPRIG = space(10)
  o_ETIPRIG = space(10)
  w_RTIPRIG = space(10)
  o_RTIPRIG = space(10)
  w_ATIPRI2 = space(1)
  o_ATIPRI2 = space(1)
  w_COST_ORA = 0
  w_DAOREEFF = 0
  o_DAOREEFF = 0
  w_DAMINEFF = 0
  o_DAMINEFF = 0
  w_OLDCEN = space(15)
  w_OLDCENRIC = space(15)
  w_LICOSTO = 0
  w_CENCOS = space(15)
  o_CENCOS = space(15)
  w_DACOSUNI = 0
  o_DACOSUNI = 0
  w_DCODCEN = space(15)
  o_DCODCEN = space(15)
  w_DACOSINT = 0
  w_FLRESP = space(1)
  w_UNMIS2 = space(3)
  w_RUNMIS3 = space(3)
  w_ECACODART = space(20)
  w_FLSERG = space(1)
  w_UNMIS3 = space(3)
  w_DESUNI = space(35)
  w_DAFLDEFF = space(1)
  w_QTAUM1 = 0
  w_COGNOME = space(40)
  w_NOME = space(40)
  w_DAPREMIN = 0
  w_DAPREMAX = 0
  w_DAGAZUFF = space(6)
  w_DENOM_RESP = space(39)
  w_NOTIFICA = space(1)
  w_DARIFRIG = 0
  w_CHKTEMP = space(1)
  w_DUR_ORE = 0
  w_ECADTOBSO = ctod('  /  /  ')
  w_RCADTOBSO = ctod('  /  /  ')
  w_CADTOBSO = ctod('  /  /  ')
  w_DACODCOM = space(15)
  w_DAVOCRIC = space(15)
  w_DAVOCCOS = space(15)
  w_DAINICOM = ctod('  /  /  ')
  w_DAFINCOM = ctod('  /  /  ')
  w_DA_SEGNO = space(1)
  w_DACOMRIC = space(15)
  w_DACENCOS = space(15)
  o_DACENCOS = space(15)
  w_DAATTIVI = space(15)
  w_ETIPSER = space(1)
  w_RTIPSER = space(1)
  w_SERPER = space(41)
  o_SERPER = space(41)
  w_TIPSER = space(1)
  w_TIPRIS = space(1)
  w_FLUSEP = space(1)
  w_MOLTIP = 0
  w_ARTIPART = space(2)
  w_EUNMIS3 = space(3)
  w_DACODLIS = space(5)
  w_DAPROLIS = space(5)
  w_DAPROSCO = space(5)
  w_DASCOLIS = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_DACENRIC = space(15)
  o_DACENRIC = space(15)
  w_DAATTRIC = space(15)
  w_DASCONT1 = 0
  w_DASCONT2 = 0
  w_DASCONT3 = 0
  w_DASCONT4 = 0
  w_FL_FRAZ = space(1)
  w_DALISACQ = space(5)
  w_DATCOINI = ctod('  /  /  ')
  w_DATCOFIN = ctod('  /  /  ')
  w_DATRIINI = ctod('  /  /  ')
  w_DATRIFIN = ctod('  /  /  ')
  w_CENRIC = space(15)
  w_FLSCOR = space(1)
  w_CEN_CauDoc = space(15)
  w_DARIFPRE = 0
  w_DARIGPRE = space(1)
  w_FLSPAN = space(10)
  w_DARIFPRE = 0
  w_DARIGPRE = space(1)
  w_DACONCOD = space(15)
  w_DACONTRA = space(10)
  w_DACODMOD = space(10)
  w_DACODIMP = space(10)
  w_DACOCOMP = 0
  w_DARINNOV = 0
  w_FLUNIV = space(1)
  w_NUMPRE = 0
  w_ARPRESTA = space(1)
  w_FLGZER = space(1)
  w_COTIPCON = space(1)
  w_COCODCON = space(15)
  w_NUMSCO = 0
  w_ETIPRI2 = space(10)
  o_ETIPRI2 = space(10)
  w_RTIPRI2 = space(10)
  o_RTIPRI2 = space(10)
  w_TIPRIGCA = space(1)
  w_TIPRI2CA = space(1)
  w_GENPRE = space(1)
  w_FLCNTCHK = .F.
  w_CHKDATAPRE = space(1)
  w_GG_PRE = 0
  w_GG_SUC = 0
  w_DATFIN = ctod('  /  /  ')
  w_CATCOM = space(5)
  w_OLD_ARPRESTA = space(1)
  w_OKTARCON = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_mdaPag1","gsag_mda",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='KEY_ARTI'
    this.cWorkTables[5]='CENCOST'
    this.cWorkTables[6]='CAUMATTI'
    this.cWorkTables[7]='TIP_DOCU'
    this.cWorkTables[8]='PAR_ALTE'
    this.cWorkTables[9]='CON_TRAS'
    this.cWorkTables[10]='CONTI'
    this.cWorkTables[11]='OFFDATTI'
    * --- Area Manuale = Open Work Table
    * --- gsag_mda
      * colora le righe collegate a spese\anticipazioni
       This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor= ;
       "Icase(NVL(t_FLSPAN,'N')='S' and NVL(t_DARIGPRE,'N')='N',RGB(255,128,128) ,NVL(t_DARIFPRE,0)=0 and NVL(t_DARIGPRE,'N')<>'S',RGB(255,255,255),RGB(MOD(214+iif(NVL(t_DARIGPRE,'N')='S',"+;
       "NVL(CPROWNUM,0),NVL(t_DARIFPRE,0))*10,255), 150, MOD(174+iif(NVL(t_DARIGPRE,'N')='S',NVL(CPROWNUM,0),NVL(t_DARIFPRE,0))*40,255)))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(11))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.OFFDATTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.OFFDATTI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsag_mda'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_34_joined
    link_2_34_joined=.f.
    local link_2_134_joined
    link_2_134_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from OFFDATTI where DASERIAL=KeySet.DASERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.OFFDATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFFDATTI_IDX,2],this.bLoadRecFilter,this.OFFDATTI_IDX,"gsag_mda")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('OFFDATTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "OFFDATTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' OFFDATTI '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_34_joined=this.AddJoinedLink_2_34(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_134_joined=this.AddJoinedLink_2_134(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DASERIAL',this.w_DASERIAL  )
      select * from (i_cTable) OFFDATTI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ISALT = isalt()
        .w_ISAHE = isahe()
        .w_ISAHR = isahr()
        .w_CAUATT = this.oparentobject.w_ATCAUATT
        .w_TipoRisorsa = IIF(.w_ISALT,'P','N')
        .w_CODCOM = space(15)
        .w_KEYLISTB = this.oparentobject.w_KEYLISTB
        .w_NOEDDES = space(1)
        .w_TIPO = 'C'
        .w_FLGZER = space(1)
        .w_GENPRE = space(1)
        .w_FLCNTCHK = .T.
        .w_CATCOM = space(5)
        .w_DASERIAL = NVL(DASERIAL,space(20))
        .w_FLGLIS = this.oparentobject.w_FLGLIS
        .w_DATALIST = IIF(Not Empty(this.oparentobject.w_ATDATDOC),this.oparentobject.w_ATDATDOC,this.oparentobject.w_DATFIN)
        .w_MVCODVAL = g_PERVAL
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_OLDCOM = iif(Not Empty(.w_CODCOM),.w_CODCOM,.w_DACODCOM)
        .w_FLANAL = this.oparentobject.w_FLANAL
        .w_FLGCOM = this.oparentobject.w_FLGCOM
        .w_FLDANA = this.oparentobject.w_FLDANA
        .w_FLACOMAQ = this.oparentobject.w_FLACOMAQ
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .w_OB_TEST = this.oparentobject.w_DATINI
        .w_MVFLVEAC = this.oparentobject.w_MVFLVEAC
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .w_LISACQ = this.oparentobject.w_LISACQ
        .w_CODLIS = this.oparentobject.w_CODLIS
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .w_FLACQ = this.oparentobject.w_FLACQ
        .w_CAUACQ = this.oparentobject.w_ATCAUACQ
        .w_CAUDOC = this.oparentobject.w_ATCAUDOC
        .w_DACODNOM = this.oparentobject.w_ATCODNOM
        .w_PR__DATA = this.oparentobject.w_DATFIN
        .w_OLDCOMRIC = this.oparentobject.w_ATCOMRIC
        .w_OLDATTC = this.oparentobject.w_ATATTCOS
        .w_OLDATTR = this.oparentobject.w_ATATTRIC
        .w_CODLIN = this.oParentObject.w_CODLIN
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .w_LetturaParAlte = i_CodAzi
          .link_1_55('Load')
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_CODCLI = this.oparentobject.w_CODCLI
          .link_1_59('Load')
        .w_BUFLANAL = this.oParentObject.w_BUFLANAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_FLCCRAUTO = This.oparentobject .w_FLCCRAUTO
        .w_OLDCEN = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENCOS),.w_CENCOS,.w_DACENCOS)
        .w_OLDCENRIC = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENRIC),.w_CENRIC,.w_DACENRIC)
        .w_CENCOS = this.oparentobject.w_ATCENCOS
        .w_CENRIC = this.oparentobject.w_ATCENRIC
        .w_NUMSCO = this.oparentobject.w_NUMSCO
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        cp_LoadRecExtFlds(this,'OFFDATTI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_RCACODART = space(20)
          .w_ARTIPRIG = space(1)
          .w_ARTIPRI2 = space(1)
          .w_CATIPRIG = space(1)
          .w_CATIPRI2 = space(1)
          .w_UNMIS1 = space(3)
          .w_EMOLTI3 = 0
          .w_RMOLTI3 = 0
          .w_FLFRAZ1 = space(1)
          .w_EOPERA3 = space(1)
          .w_ROPERA3 = space(1)
          .w_MODUM2 = space(1)
          .w_OPERAT = space(1)
          .w_ECOD1ATT = space(20)
          .w_RCOD1ATT = space(20)
          .w_PREZUM = space(1)
          .w_EDESAGG = space(0)
          .w_RDESAGG = space(0)
          .w_EDESATT = space(40)
          .w_RDESATT = space(40)
          .w_DESRIS = space(39)
          .w_TIPART = space(2)
          .w_DTIPRIG = space(10)
          .w_COST_ORA = 0
          .w_LICOSTO = 0
          .w_DCODCEN = space(15)
          .w_FLRESP = space(1)
          .w_UNMIS2 = space(3)
          .w_RUNMIS3 = space(3)
          .w_ECACODART = space(20)
          .w_FLSERG = space(1)
          .w_DESUNI = space(35)
          .w_COGNOME = space(40)
          .w_NOME = space(40)
          .w_NOTIFICA = space(1)
          .w_CHKTEMP = space(1)
          .w_DUR_ORE = 0
          .w_ECADTOBSO = ctod("  /  /  ")
          .w_RCADTOBSO = ctod("  /  /  ")
          .w_ETIPSER = space(1)
          .w_RTIPSER = space(1)
          .w_SERPER = space(41)
          .w_TIPRIS = space(1)
          .w_FLUSEP = space(1)
          .w_MOLTIP = 0
          .w_ARTIPART = space(2)
          .w_EUNMIS3 = space(3)
          .w_DATOBSO = ctod("  /  /  ")
          .w_FL_FRAZ = space(1)
          .w_CEN_CauDoc = space(15)
          .w_FLSPAN = space(10)
          .w_FLUNIV = space(1)
          .w_NUMPRE = 0
          .w_ARPRESTA = space(1)
          .w_COTIPCON = space(1)
          .w_COCODCON = space(15)
          .w_TIPRIGCA = space(1)
          .w_TIPRI2CA = space(1)
          .w_CHKDATAPRE = space(1)
          .w_GG_PRE = 0
          .w_GG_SUC = 0
        .w_DATFIN = this.oparentobject.w_DATFIN
          .w_OKTARCON = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
        .w_VOCECR = this.oparentobject.w_VOCECR
          .w_DACODRES = NVL(DACODRES,space(5))
          if link_2_1_joined
            this.w_DACODRES = NVL(DPCODICE201,NVL(this.w_DACODRES,space(5)))
            this.w_COGNOME = NVL(DPCOGNOM201,space(40))
            this.w_NOME = NVL(DPNOME201,space(40))
            this.w_COST_ORA = NVL(DPCOSORA201,0)
            this.w_DCODCEN = NVL(DPCODCEN201,space(15))
            this.w_TIPRIS = NVL(DPTIPRIS201,space(1))
            this.w_SERPER = NVL(DPSERPRE201,space(41))
            this.w_DESRIS = NVL(DPDESCRI201,space(39))
            this.w_DATOBSO = NVL(cp_ToDate(DPDTOBSO201),ctod("  /  /  "))
            this.w_DTIPRIG = NVL(DPTIPRIG201,space(10))
            this.w_CHKDATAPRE = NVL(DPCTRPRE201,space(1))
            this.w_GG_PRE = NVL(DPGG_PRE201,0)
            this.w_GG_SUC = NVL(DPGG_SUC201,0)
          else
          .link_2_1('Load')
          endif
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DACODICE = NVL(DACODICE,space(41))
          if link_2_3_joined
            this.w_DACODICE = NVL(CACODICE203,NVL(this.w_DACODICE,space(41)))
            this.w_EDESATT = NVL(CADESART203,space(40))
            this.w_EDESAGG = NVL(CADESSUP203,space(0))
            this.w_ECOD1ATT = NVL(CACODART203,space(20))
            this.w_EUNMIS3 = NVL(CAUNIMIS203,space(3))
            this.w_ECACODART = NVL(CACODART203,space(20))
            this.w_ECADTOBSO = NVL(cp_ToDate(CADTOBSO203),ctod("  /  /  "))
            this.w_ETIPSER = NVL(CA__TIPO203,space(1))
            this.w_EOPERA3 = NVL(CAOPERAT203,space(1))
            this.w_EMOLTI3 = NVL(CAMOLTIP203,0)
          else
          .link_2_3('Load')
          endif
          .w_DACODATT = NVL(DACODATT,space(20))
          if link_2_5_joined
            this.w_DACODATT = NVL(CACODICE205,NVL(this.w_DACODATT,space(20)))
            this.w_RDESATT = NVL(CADESART205,space(40))
            this.w_RDESAGG = NVL(CADESSUP205,space(0))
            this.w_RCOD1ATT = NVL(CACODART205,space(20))
            this.w_RUNMIS3 = NVL(CAUNIMIS205,space(3))
            this.w_RCACODART = NVL(CACODART205,space(20))
            this.w_RCADTOBSO = NVL(cp_ToDate(CADTOBSO205),ctod("  /  /  "))
            this.w_RTIPSER = NVL(CA__TIPO205,space(1))
            this.w_ROPERA3 = NVL(CAOPERAT205,space(1))
            this.w_RMOLTI3 = NVL(CAMOLTIP205,0)
          else
          .link_2_5('Load')
          endif
        .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
          .link_2_6('Load')
          .w_DATIPRIG = NVL(DATIPRIG,space(1))
          .w_DATIPRI2 = NVL(DATIPRI2,space(1))
          .link_2_14('Load')
        .w_MOLTI3 = IIF(.w_ISAHE,.w_EMOLTI3,.w_RMOLTI3)
        .w_OPERA3 = IIF(.w_ISAHE,.w_EOPERA3,.w_ROPERA3)
        .w_COD1ATT = IIF(.w_ISAHE,.w_ECOD1ATT,.w_RCOD1ATT)
          .link_2_31('Load')
          .w_DADESATT = NVL(DADESATT,space(40))
          .w_DAUNIMIS = NVL(DAUNIMIS,space(3))
          if link_2_34_joined
            this.w_DAUNIMIS = NVL(UMCODICE234,NVL(this.w_DAUNIMIS,space(3)))
            this.w_DESUNI = NVL(UMDESCRI234,space(35))
            this.w_CHKTEMP = NVL(UMFLTEMP234,space(1))
            this.w_DUR_ORE = NVL(UMDURORE234,0)
            this.w_FL_FRAZ = NVL(UMFLFRAZ234,space(1))
          else
          .link_2_34('Load')
          endif
          .w_DAQTAMOV = NVL(DAQTAMOV,0)
          .w_DAPREZZO = NVL(DAPREZZO,0)
          .w_DADESAGG = NVL(DADESAGG,space(0))
          .w_DACODOPE = NVL(DACODOPE,0)
          .w_DADATMOD = NVL(cp_ToDate(DADATMOD),ctod("  /  /  "))
          .w_DAVALRIG = NVL(DAVALRIG,0)
        .w_ATIPRIG = ICASE(.w_ISAHE,.w_ETIPRIG,.w_Isahr,.w_RTIPRIG,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N', Not empty(.w_DATIPRIG),.w_DATIPRIG,!EMPTY(.w_CATIPRIG),.w_CATIPRIG,.w_ARTIPRIG))
        .w_ETIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(!EMPTY(.w_CATIPRIG),.w_CATIPRIG, Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_ATIPRI2 = ICASE(.w_Isahe,.w_ETIPRI2,.w_Isahr,.w_RTIPRI2,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', Not empty(.w_DATIPRI2),.w_DATIPRI2,!EMPTY(.w_CATIPRI2),.w_CATIPRI2,.w_ARTIPRI2))
          .w_DAOREEFF = NVL(DAOREEFF,0)
          .w_DAMINEFF = NVL(DAMINEFF,0)
          .w_DACOSUNI = NVL(DACOSUNI,0)
          .w_DACOSINT = NVL(DACOSINT,0)
        .w_UNMIS3 = IIF(.w_ISAHE,.w_EUNMIS3,.w_RUNMIS3)
          .w_DAFLDEFF = NVL(DAFLDEFF,space(1))
        .w_QTAUM1 = IIF(.w_DATIPRIG='F', 1, CALQTA(.w_DAQTAMOV,.w_DAUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ', .w_UNMIS3, .w_OPERA3, .w_MOLTI3))
          .w_DAPREMIN = NVL(DAPREMIN,0)
          .w_DAPREMAX = NVL(DAPREMAX,0)
          .w_DAGAZUFF = NVL(DAGAZUFF,space(6))
        .w_DENOM_RESP = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_73.Calculate()
          .w_DARIFRIG = NVL(DARIFRIG,0)
        .w_CADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
          .w_DACODCOM = NVL(DACODCOM,space(15))
          .w_DAVOCRIC = NVL(DAVOCRIC,space(15))
          .w_DAVOCCOS = NVL(DAVOCCOS,space(15))
          .w_DAINICOM = NVL(cp_ToDate(DAINICOM),ctod("  /  /  "))
          .w_DAFINCOM = NVL(cp_ToDate(DAFINCOM),ctod("  /  /  "))
          .w_DA_SEGNO = NVL(DA_SEGNO,space(1))
          .w_DACOMRIC = NVL(DACOMRIC,space(15))
          .w_DACENCOS = NVL(DACENCOS,space(15))
          * evitabile
          *.link_2_89('Load')
          .w_DAATTIVI = NVL(DAATTIVI,space(15))
        .w_TIPSER = IIF(.w_ISAHE,.w_ETIPSER,.w_RTIPSER)
          .w_DACODLIS = NVL(DACODLIS,space(5))
          .w_DAPROLIS = NVL(DAPROLIS,space(5))
          .w_DAPROSCO = NVL(DAPROSCO,space(5))
          .w_DASCOLIS = NVL(DASCOLIS,space(5))
          .w_DACENRIC = NVL(DACENRIC,space(15))
          .w_DAATTRIC = NVL(DAATTRIC,space(15))
          .w_DASCONT1 = NVL(DASCONT1,0)
          .w_DASCONT2 = NVL(DASCONT2,0)
          .w_DASCONT3 = NVL(DASCONT3,0)
          .w_DASCONT4 = NVL(DASCONT4,0)
          .w_DALISACQ = NVL(DALISACQ,space(5))
          .w_DATCOINI = NVL(cp_ToDate(DATCOINI),ctod("  /  /  "))
          .w_DATCOFIN = NVL(cp_ToDate(DATCOFIN),ctod("  /  /  "))
          .w_DATRIINI = NVL(cp_ToDate(DATRIINI),ctod("  /  /  "))
          .w_DATRIFIN = NVL(cp_ToDate(DATRIFIN),ctod("  /  /  "))
        .w_FLSCOR = THIS.OPARENTOBJECT.w_FLSCOR
          .w_DARIFPRE = NVL(DARIFPRE,0)
          .w_DARIGPRE = NVL(DARIGPRE,space(1))
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_128.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_129.Calculate()
          .w_DARIFPRE = NVL(DARIFPRE,0)
          .w_DARIGPRE = NVL(DARIGPRE,space(1))
          .w_DACONCOD = NVL(DACONCOD,space(15))
          .w_DACONTRA = NVL(DACONTRA,space(10))
          if link_2_134_joined
            this.w_DACONTRA = NVL(COSERIAL334,NVL(this.w_DACONTRA,space(10)))
            this.w_COTIPCON = NVL(COTIPCON334,space(1))
            this.w_COCODCON = NVL(COCODCON334,space(15))
          else
          .link_2_134('Load')
          endif
          .w_DACODMOD = NVL(DACODMOD,space(10))
          .w_DACODIMP = NVL(DACODIMP,space(10))
          .w_DACOCOMP = NVL(DACOCOMP,0)
          .w_DARINNOV = NVL(DARINNOV,0)
        .w_ETIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,'D')
        .w_OLD_ARPRESTA = .w_ARPRESTA
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_FLGLIS = this.oparentobject.w_FLGLIS
        .w_DATALIST = IIF(Not Empty(this.oparentobject.w_ATDATDOC),this.oparentobject.w_ATDATDOC,this.oparentobject.w_DATFIN)
        .w_MVCODVAL = g_PERVAL
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_OLDCOM = iif(Not Empty(.w_CODCOM),.w_CODCOM,.w_DACODCOM)
        .w_FLANAL = this.oparentobject.w_FLANAL
        .w_FLGCOM = this.oparentobject.w_FLGCOM
        .w_FLDANA = this.oparentobject.w_FLDANA
        .w_FLACOMAQ = this.oparentobject.w_FLACOMAQ
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .w_OB_TEST = this.oparentobject.w_DATINI
        .w_MVFLVEAC = this.oparentobject.w_MVFLVEAC
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .w_LISACQ = this.oparentobject.w_LISACQ
        .w_CODLIS = this.oparentobject.w_CODLIS
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .w_FLACQ = this.oparentobject.w_FLACQ
        .w_CAUACQ = this.oparentobject.w_ATCAUACQ
        .w_CAUDOC = this.oparentobject.w_ATCAUDOC
        .w_DACODNOM = this.oparentobject.w_ATCODNOM
        .w_PR__DATA = this.oparentobject.w_DATFIN
        .w_OLDCOMRIC = this.oparentobject.w_ATCOMRIC
        .w_OLDATTC = this.oparentobject.w_ATATTCOS
        .w_OLDATTR = this.oparentobject.w_ATATTRIC
        .w_CODLIN = this.oParentObject.w_CODLIN
        .w_VOCECR = this.oparentobject.w_VOCECR
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .w_LetturaParAlte = i_CodAzi
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_CODCLI = this.oparentobject.w_CODCLI
        .w_BUFLANAL = this.oParentObject.w_BUFLANAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_FLCCRAUTO = This.oparentobject .w_FLCCRAUTO
        .w_NUMSCO = this.oparentobject.w_NUMSCO
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_87.enabled = .oPgFrm.Page1.oPag.oBtn_2_87.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_100.enabled = .oPgFrm.Page1.oPag.oBtn_2_100.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsag_mda
    *--- Rivalorizzo variabili IS... per problema alla load rec, non venivano valutate
    This.w_ISALT=isAlt()
    This.w_ISAHR=isAhr()
    This.w_ISAHE=isAhe()
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DASERIAL=space(20)
      .w_ISALT=.f.
      .w_ISAHE=.f.
      .w_ISAHR=.f.
      .w_FLGLIS=space(1)
      .w_DATALIST=ctod("  /  /  ")
      .w_MVCODVAL=space(3)
      .w_CAUATT=space(20)
      .w_TipoRisorsa=space(1)
      .w_CODCOM=space(15)
      .w_OLDCOM=space(15)
      .w_FLANAL=space(1)
      .w_FLGCOM=space(1)
      .w_FLDANA=space(1)
      .w_FLACOMAQ=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_KEYLISTB=space(10)
      .w_MVFLVEAC=space(3)
      .w_LISACQ=space(5)
      .w_CODLIS=space(5)
      .w_FLACQ=space(1)
      .w_CAUACQ=space(5)
      .w_CAUDOC=space(5)
      .w_DACODNOM=space(15)
      .w_PR__DATA=space(10)
      .w_OLDCOMRIC=space(15)
      .w_OLDATTC=space(15)
      .w_OLDATTR=space(15)
      .w_CODLIN=space(3)
      .w_VOCECR=space(1)
      .w_LetturaParAlte=space(5)
      .w_NOEDDES=space(1)
      .w_TIPO=space(1)
      .w_CODCLI=space(15)
      .w_BUFLANAL=space(1)
      .w_FLCCRAUTO=space(10)
      .w_DACODRES=space(5)
      .w_CPROWORD=10
      .w_DACODICE=space(41)
      .w_RCACODART=space(20)
      .w_DACODATT=space(20)
      .w_CACODART=space(20)
      .w_ARTIPRIG=space(1)
      .w_ARTIPRI2=space(1)
      .w_CATIPRIG=space(1)
      .w_CATIPRI2=space(1)
      .w_DATIPRIG=space(1)
      .w_DATIPRI2=space(1)
      .w_UNMIS1=space(3)
      .w_EMOLTI3=0
      .w_RMOLTI3=0
      .w_FLFRAZ1=space(1)
      .w_MOLTI3=0
      .w_EOPERA3=space(1)
      .w_ROPERA3=space(1)
      .w_MODUM2=space(1)
      .w_OPERA3=space(1)
      .w_OPERAT=space(1)
      .w_ECOD1ATT=space(20)
      .w_RCOD1ATT=space(20)
      .w_PREZUM=space(1)
      .w_EDESAGG=space(0)
      .w_RDESAGG=space(0)
      .w_EDESATT=space(40)
      .w_RDESATT=space(40)
      .w_COD1ATT=space(20)
      .w_DADESATT=space(40)
      .w_DESRIS=space(39)
      .w_DAUNIMIS=space(3)
      .w_TIPART=space(2)
      .w_DAQTAMOV=0
      .w_DAPREZZO=0
      .w_DADESAGG=space(0)
      .w_DACODOPE=0
      .w_DADATMOD=ctod("  /  /  ")
      .w_DTIPRIG=space(10)
      .w_DAVALRIG=0
      .w_ATIPRIG=space(1)
      .w_ETIPRIG=space(10)
      .w_RTIPRIG=space(10)
      .w_ATIPRI2=space(1)
      .w_COST_ORA=0
      .w_DAOREEFF=0
      .w_DAMINEFF=0
      .w_OLDCEN=space(15)
      .w_OLDCENRIC=space(15)
      .w_LICOSTO=0
      .w_CENCOS=space(15)
      .w_DACOSUNI=0
      .w_DCODCEN=space(15)
      .w_DACOSINT=0
      .w_FLRESP=space(1)
      .w_UNMIS2=space(3)
      .w_RUNMIS3=space(3)
      .w_ECACODART=space(20)
      .w_FLSERG=space(1)
      .w_UNMIS3=space(3)
      .w_DESUNI=space(35)
      .w_DAFLDEFF=space(1)
      .w_QTAUM1=0
      .w_COGNOME=space(40)
      .w_NOME=space(40)
      .w_DAPREMIN=0
      .w_DAPREMAX=0
      .w_DAGAZUFF=space(6)
      .w_DENOM_RESP=space(39)
      .w_NOTIFICA=space(1)
      .w_DARIFRIG=0
      .w_CHKTEMP=space(1)
      .w_DUR_ORE=0
      .w_ECADTOBSO=ctod("  /  /  ")
      .w_RCADTOBSO=ctod("  /  /  ")
      .w_CADTOBSO=ctod("  /  /  ")
      .w_DACODCOM=space(15)
      .w_DAVOCRIC=space(15)
      .w_DAVOCCOS=space(15)
      .w_DAINICOM=ctod("  /  /  ")
      .w_DAFINCOM=ctod("  /  /  ")
      .w_DA_SEGNO=space(1)
      .w_DACOMRIC=space(15)
      .w_DACENCOS=space(15)
      .w_DAATTIVI=space(15)
      .w_ETIPSER=space(1)
      .w_RTIPSER=space(1)
      .w_SERPER=space(41)
      .w_TIPSER=space(1)
      .w_TIPRIS=space(1)
      .w_FLUSEP=space(1)
      .w_MOLTIP=0
      .w_ARTIPART=space(2)
      .w_EUNMIS3=space(3)
      .w_DACODLIS=space(5)
      .w_DAPROLIS=space(5)
      .w_DAPROSCO=space(5)
      .w_DASCOLIS=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DACENRIC=space(15)
      .w_DAATTRIC=space(15)
      .w_DASCONT1=0
      .w_DASCONT2=0
      .w_DASCONT3=0
      .w_DASCONT4=0
      .w_FL_FRAZ=space(1)
      .w_DALISACQ=space(5)
      .w_DATCOINI=ctod("  /  /  ")
      .w_DATCOFIN=ctod("  /  /  ")
      .w_DATRIINI=ctod("  /  /  ")
      .w_DATRIFIN=ctod("  /  /  ")
      .w_CENRIC=space(15)
      .w_FLSCOR=space(1)
      .w_CEN_CauDoc=space(15)
      .w_DARIFPRE=0
      .w_DARIGPRE=space(1)
      .w_FLSPAN=space(10)
      .w_DARIFPRE=0
      .w_DARIGPRE=space(1)
      .w_DACONCOD=space(15)
      .w_DACONTRA=space(10)
      .w_DACODMOD=space(10)
      .w_DACODIMP=space(10)
      .w_DACOCOMP=0
      .w_DARINNOV=0
      .w_FLUNIV=space(1)
      .w_NUMPRE=0
      .w_ARPRESTA=space(1)
      .w_FLGZER=space(1)
      .w_COTIPCON=space(1)
      .w_COCODCON=space(15)
      .w_NUMSCO=0
      .w_ETIPRI2=space(10)
      .w_RTIPRI2=space(10)
      .w_TIPRIGCA=space(1)
      .w_TIPRI2CA=space(1)
      .w_GENPRE=space(1)
      .w_FLCNTCHK=.f.
      .w_CHKDATAPRE=space(1)
      .w_GG_PRE=0
      .w_GG_SUC=0
      .w_DATFIN=ctod("  /  /  ")
      .w_CATCOM=space(5)
      .w_OLD_ARPRESTA=space(1)
      .w_OKTARCON=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_ISALT = isalt()
        .w_ISAHE = isahe()
        .w_ISAHR = isahr()
        .w_FLGLIS = this.oparentobject.w_FLGLIS
        .w_DATALIST = IIF(Not Empty(this.oparentobject.w_ATDATDOC),this.oparentobject.w_ATDATDOC,this.oparentobject.w_DATFIN)
        .w_MVCODVAL = g_PERVAL
        .w_CAUATT = this.oparentobject.w_ATCAUATT
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_TipoRisorsa = IIF(.w_ISALT,'P','N')
        .DoRTCalc(10,10,.f.)
        .w_OLDCOM = iif(Not Empty(.w_CODCOM),.w_CODCOM,.w_DACODCOM)
        .w_FLANAL = this.oparentobject.w_FLANAL
        .w_FLGCOM = this.oparentobject.w_FLGCOM
        .w_FLDANA = this.oparentobject.w_FLDANA
        .w_FLACOMAQ = this.oparentobject.w_FLACOMAQ
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .w_OB_TEST = this.oparentobject.w_DATINI
        .w_KEYLISTB = this.oparentobject.w_KEYLISTB
        .w_MVFLVEAC = this.oparentobject.w_MVFLVEAC
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .w_LISACQ = this.oparentobject.w_LISACQ
        .w_CODLIS = this.oparentobject.w_CODLIS
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .w_FLACQ = this.oparentobject.w_FLACQ
        .w_CAUACQ = this.oparentobject.w_ATCAUACQ
        .w_CAUDOC = this.oparentobject.w_ATCAUDOC
        .w_DACODNOM = this.oparentobject.w_ATCODNOM
        .w_PR__DATA = this.oparentobject.w_DATFIN
        .w_OLDCOMRIC = this.oparentobject.w_ATCOMRIC
        .w_OLDATTC = this.oparentobject.w_ATATTCOS
        .w_OLDATTR = this.oparentobject.w_ATATTRIC
        .w_CODLIN = this.oParentObject.w_CODLIN
        .w_VOCECR = this.oparentobject.w_VOCECR
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .w_LetturaParAlte = i_CodAzi
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_LetturaParAlte))
         .link_1_55('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .DoRTCalc(32,32,.f.)
        .w_TIPO = 'C'
        .w_CODCLI = this.oparentobject.w_CODCLI
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_CODCLI))
         .link_1_59('Full')
        endif
        .w_BUFLANAL = this.oParentObject.w_BUFLANAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_FLCCRAUTO = This.oparentobject .w_FLCCRAUTO
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_DACODRES))
         .link_2_1('Full')
        endif
        .DoRTCalc(38,38,.f.)
        .w_DACODICE = IIF(.w_ISAHE,IIF(EMPTY(.w_DACODICE), .w_SERPER, .w_DACODICE), SPACE(41))
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_DACODICE))
         .link_2_3('Full')
        endif
        .DoRTCalc(40,40,.f.)
        .w_DACODATT = IIF(!.w_ISAHE, IIF(EMPTY(.w_DACODATT), .w_SERPER, .w_DACODATT), SPACE(20))
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_DACODATT))
         .link_2_5('Full')
        endif
        .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_CACODART))
         .link_2_6('Full')
        endif
        .DoRTCalc(43,49,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_14('Full')
        endif
        .DoRTCalc(50,52,.f.)
        .w_MOLTI3 = IIF(.w_ISAHE,.w_EMOLTI3,.w_RMOLTI3)
        .DoRTCalc(54,56,.f.)
        .w_OPERA3 = IIF(.w_ISAHE,.w_EOPERA3,.w_ROPERA3)
        .DoRTCalc(58,65,.f.)
        .w_COD1ATT = IIF(.w_ISAHE,.w_ECOD1ATT,.w_RCOD1ATT)
        .DoRTCalc(66,66,.f.)
        if not(empty(.w_COD1ATT))
         .link_2_31('Full')
        endif
        .w_DADESATT = IIF(.w_ISAHE, .w_EDESATT, .w_RDESATT)
        .DoRTCalc(68,68,.f.)
        .w_DAUNIMIS = .w_UNMIS1
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_DAUNIMIS))
         .link_2_34('Full')
        endif
        .DoRTCalc(70,70,.f.)
        .w_DAQTAMOV = IIF(.w_TIPART='DE', 0, IIF(.w_DAQTAMOV>0,.w_DAQTAMOV,1))
        .DoRTCalc(72,72,.f.)
        .w_DADESAGG = IIF(.w_ISAHE, .w_EDESAGG, .w_RDESAGG)
        .w_DACODOPE = i_codute
        .w_DADATMOD = i_datsys
        .DoRTCalc(76,76,.f.)
        .w_DAVALRIG = IIF(UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE', CAVALRIG(.w_DAPREZZO,.w_DAQTAMOV, .w_DASCONT1,.w_DASCONT2,.w_DASCONT3,.w_DASCONT4,IIF(.w_NOTIFICA='S' AND .w_ISALT, 0, this.oParentObject.w_DECTOT)), cp_ROUND(.w_DAQTAMOV*.w_DAPREZZO, this.oParentObject.w_DECTOT))
        .w_ATIPRIG = ICASE(.w_ISAHE,.w_ETIPRIG,.w_Isahr,.w_RTIPRIG,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N', Not empty(.w_DATIPRIG),.w_DATIPRIG,!EMPTY(.w_CATIPRIG),.w_CATIPRIG,.w_ARTIPRIG))
        .w_ETIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(!EMPTY(.w_CATIPRIG),.w_CATIPRIG, Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_ATIPRI2 = ICASE(.w_Isahe,.w_ETIPRI2,.w_Isahr,.w_RTIPRI2,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', Not empty(.w_DATIPRI2),.w_DATIPRI2,!EMPTY(.w_CATIPRI2),.w_CATIPRI2,.w_ARTIPRI2))
        .DoRTCalc(82,84,.f.)
        .w_OLDCEN = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENCOS),.w_CENCOS,.w_DACENCOS)
        .w_OLDCENRIC = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENRIC),.w_CENRIC,.w_DACENRIC)
        .DoRTCalc(87,87,.f.)
        .w_CENCOS = this.oparentobject.w_ATCENCOS
        .DoRTCalc(89,90,.f.)
        .w_DACOSINT = IIF(.w_LICOSTO>0 and .w_COST_ORA=0 ,.w_DACOSUNI*.w_DAQTAMOV,(.w_DAOREEFF+(.w_DAMINEFF/60))*.w_DACOSUNI)
        .DoRTCalc(92,96,.f.)
        .w_UNMIS3 = IIF(.w_ISAHE,.w_EUNMIS3,.w_RUNMIS3)
        .DoRTCalc(98,99,.f.)
        .w_QTAUM1 = IIF(.w_DATIPRIG='F', 1, CALQTA(.w_DAQTAMOV,.w_DAUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ', .w_UNMIS3, .w_OPERA3, .w_MOLTI3))
        .DoRTCalc(101,105,.f.)
        .w_DENOM_RESP = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_73.Calculate()
        .DoRTCalc(107,112,.f.)
        .w_CADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
        .DoRTCalc(114,114,.f.)
        .w_DAVOCRIC = SearchVoc(this,'R',.w_COD1ATT,this.oparentobject.w_ATTIPCLI,this.oparentobject.w_CODCLI,this.oparentobject.w_ATCAUDOC,this.oparentobject.w_ATCODNOM)
        .w_DAVOCCOS = SearchVoc(this,'C',.w_COD1ATT,this.oparentobject.w_ATTIPCLI,this.oparentobject.w_CODCLI,this.oparentobject.w_ATCAUDOC,this.oparentobject.w_ATCODNOM)
        .w_DAINICOM = this.oparentobject.w_ATINIRIC
        .w_DAFINCOM = this.oparentobject.w_ATFINRIC
        .w_DA_SEGNO = 'D'
        .w_DACOMRIC = .w_OLDCOMRIC
        .DoRTCalc(121,121,.f.)
        if not(empty(.w_DACENCOS))
         .link_2_89('Full')
        endif
        .w_DAATTIVI = .w_OLDATTC
        .DoRTCalc(123,125,.f.)
        .w_TIPSER = IIF(.w_ISAHE,.w_ETIPSER,.w_RTIPSER)
        .DoRTCalc(127,137,.f.)
        .w_DAATTRIC = .w_OLDATTR
        .DoRTCalc(139,144,.f.)
        .w_DATCOINI = this.oparentobject.w_ATINIRIC
        .w_DATCOFIN = this.oparentobject.w_ATFINRIC
        .w_DATRIINI = this.oparentobject.w_ATINICOS
        .w_DATRIFIN = this.oparentobject.w_ATFINCOS
        .w_CENRIC = this.oparentobject.w_ATCENRIC
        .w_FLSCOR = THIS.OPARENTOBJECT.w_FLSCOR
        .DoRTCalc(151,152,.f.)
        .w_DARIGPRE = 'N'
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_128.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_129.Calculate()
        .DoRTCalc(154,158,.f.)
        if not(empty(.w_DACONTRA))
         .link_2_134('Full')
        endif
        .DoRTCalc(159,168,.f.)
        .w_NUMSCO = this.oparentobject.w_NUMSCO
        .w_ETIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,'D')
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .DoRTCalc(172,174,.f.)
        .w_FLCNTCHK = .T.
        .DoRTCalc(176,178,.f.)
        .w_DATFIN = this.oparentobject.w_DATFIN
        .DoRTCalc(180,180,.f.)
        .w_OLD_ARPRESTA = .w_ARPRESTA
      endif
    endwith
    cp_BlankRecExtFlds(this,'OFFDATTI')
    this.DoRTCalc(182,182,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_87.enabled = this.oPgFrm.Page1.oPag.oBtn_2_87.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_100.enabled = this.oPgFrm.Page1.oPag.oBtn_2_100.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDADESAGG_2_38.enabled = i_bVal
      .Page1.oPag.oATIPRIG_2_43.enabled = i_bVal
      .Page1.oPag.oETIPRIG_2_44.enabled = i_bVal
      .Page1.oPag.oRTIPRIG_2_45.enabled = i_bVal
      .Page1.oPag.oATIPRI2_2_46.enabled = i_bVal
      .Page1.oPag.oDAOREEFF_2_48.enabled = i_bVal
      .Page1.oPag.oDAMINEFF_2_49.enabled = i_bVal
      .Page1.oPag.oDACOSUNI_2_54.enabled = i_bVal
      .Page1.oPag.oDACOSINT_2_56.enabled = i_bVal
      .Page1.oPag.oBtn_2_87.enabled = .Page1.oPag.oBtn_2_87.mCond()
      .Page1.oPag.oBtn_2_100.enabled = .Page1.oPag.oBtn_2_100.mCond()
      .Page1.oPag.oBtn_2_101.enabled = i_bVal
      .Page1.oPag.oBtn_2_102.enabled = i_bVal
      .Page1.oPag.oObj_1_17.enabled = i_bVal
      .Page1.oPag.oObj_1_18.enabled = i_bVal
      .Page1.oPag.oObj_1_22.enabled = i_bVal
      .Page1.oPag.oObj_1_33.enabled = i_bVal
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      .Page1.oPag.oObj_1_42.enabled = i_bVal
      .Page1.oPag.oObj_1_53.enabled = i_bVal
      .Page1.oPag.oObj_1_54.enabled = i_bVal
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_73.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_128.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_129.enabled = i_bVal
      .Page1.oPag.oObj_1_67.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'OFFDATTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.OFFDATTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DASERIAL,"DASERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DACODRES C(5);
      ,t_CPROWORD N(5);
      ,t_DACODICE C(41);
      ,t_DACODATT C(20);
      ,t_DADESATT C(40);
      ,t_DESRIS C(39);
      ,t_DAUNIMIS C(3);
      ,t_DAQTAMOV N(12,3);
      ,t_DAPREZZO N(18,5);
      ,t_DADESAGG M(10);
      ,t_DACODOPE N(4);
      ,t_DADATMOD D(8);
      ,t_DAVALRIG N(18,4);
      ,t_ATIPRIG N(3);
      ,t_ETIPRIG N(3);
      ,t_RTIPRIG N(3);
      ,t_ATIPRI2 N(3);
      ,t_DAOREEFF N(3);
      ,t_DAMINEFF N(2);
      ,t_DACOSUNI N(18,5);
      ,t_DACOSINT N(18,4);
      ,t_DESUNI C(35);
      ,t_DAPREMIN N(18,5);
      ,t_DAPREMAX N(18,5);
      ,t_DAGAZUFF C(6);
      ,t_DENOM_RESP C(39);
      ,CPROWNUM N(10);
      ,t_VOCECR C(1);
      ,t_RCACODART C(20);
      ,t_CACODART C(20);
      ,t_ARTIPRIG C(1);
      ,t_ARTIPRI2 C(1);
      ,t_CATIPRIG C(1);
      ,t_CATIPRI2 C(1);
      ,t_DATIPRIG C(1);
      ,t_DATIPRI2 C(1);
      ,t_UNMIS1 C(3);
      ,t_EMOLTI3 N(10,4);
      ,t_RMOLTI3 N(10,4);
      ,t_FLFRAZ1 C(1);
      ,t_MOLTI3 N(10,4);
      ,t_EOPERA3 C(1);
      ,t_ROPERA3 C(1);
      ,t_MODUM2 C(1);
      ,t_OPERA3 C(1);
      ,t_OPERAT C(1);
      ,t_ECOD1ATT C(20);
      ,t_RCOD1ATT C(20);
      ,t_PREZUM C(1);
      ,t_EDESAGG M(10);
      ,t_RDESAGG M(10);
      ,t_EDESATT C(40);
      ,t_RDESATT C(40);
      ,t_COD1ATT C(20);
      ,t_TIPART C(2);
      ,t_DTIPRIG C(10);
      ,t_COST_ORA N(18,4);
      ,t_LICOSTO N(18,5);
      ,t_DCODCEN C(15);
      ,t_FLRESP C(1);
      ,t_UNMIS2 C(3);
      ,t_RUNMIS3 C(3);
      ,t_ECACODART C(20);
      ,t_FLSERG C(1);
      ,t_UNMIS3 C(3);
      ,t_DAFLDEFF C(1);
      ,t_QTAUM1 N(12,3);
      ,t_COGNOME C(40);
      ,t_NOME C(40);
      ,t_NOTIFICA C(1);
      ,t_DARIFRIG N(4);
      ,t_CHKTEMP C(1);
      ,t_DUR_ORE N(9,5);
      ,t_ECADTOBSO D(8);
      ,t_RCADTOBSO D(8);
      ,t_CADTOBSO D(8);
      ,t_DACODCOM C(15);
      ,t_DAVOCRIC C(15);
      ,t_DAVOCCOS C(15);
      ,t_DAINICOM D(8);
      ,t_DAFINCOM D(8);
      ,t_DA_SEGNO C(1);
      ,t_DACOMRIC C(15);
      ,t_DACENCOS C(15);
      ,t_DAATTIVI C(15);
      ,t_ETIPSER C(1);
      ,t_RTIPSER C(1);
      ,t_SERPER C(41);
      ,t_TIPSER C(1);
      ,t_TIPRIS C(1);
      ,t_FLUSEP C(1);
      ,t_MOLTIP N(10,4);
      ,t_ARTIPART C(2);
      ,t_EUNMIS3 C(3);
      ,t_DACODLIS C(5);
      ,t_DAPROLIS C(5);
      ,t_DAPROSCO C(5);
      ,t_DASCOLIS C(5);
      ,t_DATOBSO D(8);
      ,t_DACENRIC C(15);
      ,t_DAATTRIC C(15);
      ,t_DASCONT1 N(6,2);
      ,t_DASCONT2 N(6,2);
      ,t_DASCONT3 N(6,2);
      ,t_DASCONT4 N(6,2);
      ,t_FL_FRAZ C(1);
      ,t_DALISACQ C(5);
      ,t_DATCOINI D(8);
      ,t_DATCOFIN D(8);
      ,t_DATRIINI D(8);
      ,t_DATRIFIN D(8);
      ,t_FLSCOR C(1);
      ,t_CEN_CauDoc C(15);
      ,t_DARIFPRE N(5);
      ,t_DARIGPRE C(1);
      ,t_FLSPAN C(10);
      ,t_DACONCOD C(15);
      ,t_DACONTRA C(10);
      ,t_DACODMOD C(10);
      ,t_DACODIMP C(10);
      ,t_DACOCOMP N(4);
      ,t_DARINNOV N(6);
      ,t_FLUNIV C(1);
      ,t_NUMPRE N(4);
      ,t_ARPRESTA C(1);
      ,t_COTIPCON C(1);
      ,t_COCODCON C(15);
      ,t_ETIPRI2 C(10);
      ,t_RTIPRI2 C(10);
      ,t_TIPRIGCA C(1);
      ,t_TIPRI2CA C(1);
      ,t_CHKDATAPRE C(1);
      ,t_GG_PRE N(4);
      ,t_GG_SUC N(4);
      ,t_DATFIN D(8);
      ,t_OLD_ARPRESTA C(1);
      ,t_OKTARCON C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mdabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDACODRES_2_1.controlsource=this.cTrsName+'.t_DACODRES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_3.controlsource=this.cTrsName+'.t_DACODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_5.controlsource=this.cTrsName+'.t_DACODATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDADESATT_2_32.controlsource=this.cTrsName+'.t_DADESATT'
    this.oPgFRm.Page1.oPag.oDESRIS_2_33.controlsource=this.cTrsName+'.t_DESRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDAUNIMIS_2_34.controlsource=this.cTrsName+'.t_DAUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDAQTAMOV_2_36.controlsource=this.cTrsName+'.t_DAQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDAPREZZO_2_37.controlsource=this.cTrsName+'.t_DAPREZZO'
    this.oPgFRm.Page1.oPag.oDADESAGG_2_38.controlsource=this.cTrsName+'.t_DADESAGG'
    this.oPgFRm.Page1.oPag.oDACODOPE_2_39.controlsource=this.cTrsName+'.t_DACODOPE'
    this.oPgFRm.Page1.oPag.oDADATMOD_2_40.controlsource=this.cTrsName+'.t_DADATMOD'
    this.oPgFRm.Page1.oPag.oDAVALRIG_2_42.controlsource=this.cTrsName+'.t_DAVALRIG'
    this.oPgFRm.Page1.oPag.oATIPRIG_2_43.controlsource=this.cTrsName+'.t_ATIPRIG'
    this.oPgFRm.Page1.oPag.oETIPRIG_2_44.controlsource=this.cTrsName+'.t_ETIPRIG'
    this.oPgFRm.Page1.oPag.oRTIPRIG_2_45.controlsource=this.cTrsName+'.t_RTIPRIG'
    this.oPgFRm.Page1.oPag.oATIPRI2_2_46.controlsource=this.cTrsName+'.t_ATIPRI2'
    this.oPgFRm.Page1.oPag.oDAOREEFF_2_48.controlsource=this.cTrsName+'.t_DAOREEFF'
    this.oPgFRm.Page1.oPag.oDAMINEFF_2_49.controlsource=this.cTrsName+'.t_DAMINEFF'
    this.oPgFRm.Page1.oPag.oDACOSUNI_2_54.controlsource=this.cTrsName+'.t_DACOSUNI'
    this.oPgFRm.Page1.oPag.oDACOSINT_2_56.controlsource=this.cTrsName+'.t_DACOSINT'
    this.oPgFRm.Page1.oPag.oDESUNI_2_63.controlsource=this.cTrsName+'.t_DESUNI'
    this.oPgFRm.Page1.oPag.oDAPREMIN_2_68.controlsource=this.cTrsName+'.t_DAPREMIN'
    this.oPgFRm.Page1.oPag.oDAPREMAX_2_69.controlsource=this.cTrsName+'.t_DAPREMAX'
    this.oPgFRm.Page1.oPag.oDAGAZUFF_2_70.controlsource=this.cTrsName+'.t_DAGAZUFF'
    this.oPgFRm.Page1.oPag.oDENOM_RESP_2_71.controlsource=this.cTrsName+'.t_DENOM_RESP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(55)
    this.AddVLine(116)
    this.AddVLine(259)
    this.AddVLine(494)
    this.AddVLine(534)
    this.AddVLine(619)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODRES_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.OFFDATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFFDATTI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.OFFDATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFFDATTI_IDX,2])
      *
      * insert into OFFDATTI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'OFFDATTI')
        i_extval=cp_InsertValODBCExtFlds(this,'OFFDATTI')
        i_cFldBody=" "+;
                  "(DASERIAL,DACODRES,CPROWORD,DACODICE,DACODATT"+;
                  ",DATIPRIG,DATIPRI2,DADESATT,DAUNIMIS,DAQTAMOV"+;
                  ",DAPREZZO,DADESAGG,DACODOPE,DADATMOD,DAVALRIG"+;
                  ",DAOREEFF,DAMINEFF,DACOSUNI,DACOSINT,DAFLDEFF"+;
                  ",DAPREMIN,DAPREMAX,DAGAZUFF,DARIFRIG,DACODCOM"+;
                  ",DAVOCRIC,DAVOCCOS,DAINICOM,DAFINCOM,DA_SEGNO"+;
                  ",DACOMRIC,DACENCOS,DAATTIVI,DACODLIS,DAPROLIS"+;
                  ",DAPROSCO,DASCOLIS,DACENRIC,DAATTRIC,DASCONT1"+;
                  ",DASCONT2,DASCONT3,DASCONT4,DALISACQ,DATCOINI"+;
                  ",DATCOFIN,DATRIINI,DATRIFIN,DARIFPRE,DARIGPRE"+;
                  ",DACONCOD,DACONTRA,DACODMOD,DACODIMP,DACOCOMP"+;
                  ",DARINNOV,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DASERIAL)+","+cp_ToStrODBCNull(this.w_DACODRES)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_DACODICE)+","+cp_ToStrODBCNull(this.w_DACODATT)+;
             ","+cp_ToStrODBC(this.w_DATIPRIG)+","+cp_ToStrODBC(this.w_DATIPRI2)+","+cp_ToStrODBC(this.w_DADESATT)+","+cp_ToStrODBCNull(this.w_DAUNIMIS)+","+cp_ToStrODBC(this.w_DAQTAMOV)+;
             ","+cp_ToStrODBC(this.w_DAPREZZO)+","+cp_ToStrODBC(this.w_DADESAGG)+","+cp_ToStrODBC(this.w_DACODOPE)+","+cp_ToStrODBC(this.w_DADATMOD)+","+cp_ToStrODBC(this.w_DAVALRIG)+;
             ","+cp_ToStrODBC(this.w_DAOREEFF)+","+cp_ToStrODBC(this.w_DAMINEFF)+","+cp_ToStrODBC(this.w_DACOSUNI)+","+cp_ToStrODBC(this.w_DACOSINT)+","+cp_ToStrODBC(this.w_DAFLDEFF)+;
             ","+cp_ToStrODBC(this.w_DAPREMIN)+","+cp_ToStrODBC(this.w_DAPREMAX)+","+cp_ToStrODBC(this.w_DAGAZUFF)+","+cp_ToStrODBC(this.w_DARIFRIG)+","+cp_ToStrODBC(this.w_DACODCOM)+;
             ","+cp_ToStrODBC(this.w_DAVOCRIC)+","+cp_ToStrODBC(this.w_DAVOCCOS)+","+cp_ToStrODBC(this.w_DAINICOM)+","+cp_ToStrODBC(this.w_DAFINCOM)+","+cp_ToStrODBC(this.w_DA_SEGNO)+;
             ","+cp_ToStrODBC(this.w_DACOMRIC)+","+cp_ToStrODBCNull(this.w_DACENCOS)+","+cp_ToStrODBC(this.w_DAATTIVI)+","+cp_ToStrODBC(this.w_DACODLIS)+","+cp_ToStrODBC(this.w_DAPROLIS)+;
             ","+cp_ToStrODBC(this.w_DAPROSCO)+","+cp_ToStrODBC(this.w_DASCOLIS)+","+cp_ToStrODBC(this.w_DACENRIC)+","+cp_ToStrODBC(this.w_DAATTRIC)+","+cp_ToStrODBC(this.w_DASCONT1)+;
             ","+cp_ToStrODBC(this.w_DASCONT2)+","+cp_ToStrODBC(this.w_DASCONT3)+","+cp_ToStrODBC(this.w_DASCONT4)+","+cp_ToStrODBC(this.w_DALISACQ)+","+cp_ToStrODBC(this.w_DATCOINI)+;
             ","+cp_ToStrODBC(this.w_DATCOFIN)+","+cp_ToStrODBC(this.w_DATRIINI)+","+cp_ToStrODBC(this.w_DATRIFIN)+","+cp_ToStrODBC(this.w_DARIFPRE)+","+cp_ToStrODBC(this.w_DARIGPRE)+;
             ","+cp_ToStrODBC(this.w_DACONCOD)+","+cp_ToStrODBCNull(this.w_DACONTRA)+","+cp_ToStrODBC(this.w_DACODMOD)+","+cp_ToStrODBC(this.w_DACODIMP)+","+cp_ToStrODBC(this.w_DACOCOMP)+;
             ","+cp_ToStrODBC(this.w_DARINNOV)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'OFFDATTI')
        i_extval=cp_InsertValVFPExtFlds(this,'OFFDATTI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DASERIAL',this.w_DASERIAL)
        INSERT INTO (i_cTable) (;
                   DASERIAL;
                  ,DACODRES;
                  ,CPROWORD;
                  ,DACODICE;
                  ,DACODATT;
                  ,DATIPRIG;
                  ,DATIPRI2;
                  ,DADESATT;
                  ,DAUNIMIS;
                  ,DAQTAMOV;
                  ,DAPREZZO;
                  ,DADESAGG;
                  ,DACODOPE;
                  ,DADATMOD;
                  ,DAVALRIG;
                  ,DAOREEFF;
                  ,DAMINEFF;
                  ,DACOSUNI;
                  ,DACOSINT;
                  ,DAFLDEFF;
                  ,DAPREMIN;
                  ,DAPREMAX;
                  ,DAGAZUFF;
                  ,DARIFRIG;
                  ,DACODCOM;
                  ,DAVOCRIC;
                  ,DAVOCCOS;
                  ,DAINICOM;
                  ,DAFINCOM;
                  ,DA_SEGNO;
                  ,DACOMRIC;
                  ,DACENCOS;
                  ,DAATTIVI;
                  ,DACODLIS;
                  ,DAPROLIS;
                  ,DAPROSCO;
                  ,DASCOLIS;
                  ,DACENRIC;
                  ,DAATTRIC;
                  ,DASCONT1;
                  ,DASCONT2;
                  ,DASCONT3;
                  ,DASCONT4;
                  ,DALISACQ;
                  ,DATCOINI;
                  ,DATCOFIN;
                  ,DATRIINI;
                  ,DATRIFIN;
                  ,DARIFPRE;
                  ,DARIGPRE;
                  ,DACONCOD;
                  ,DACONTRA;
                  ,DACODMOD;
                  ,DACODIMP;
                  ,DACOCOMP;
                  ,DARINNOV;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DASERIAL;
                  ,this.w_DACODRES;
                  ,this.w_CPROWORD;
                  ,this.w_DACODICE;
                  ,this.w_DACODATT;
                  ,this.w_DATIPRIG;
                  ,this.w_DATIPRI2;
                  ,this.w_DADESATT;
                  ,this.w_DAUNIMIS;
                  ,this.w_DAQTAMOV;
                  ,this.w_DAPREZZO;
                  ,this.w_DADESAGG;
                  ,this.w_DACODOPE;
                  ,this.w_DADATMOD;
                  ,this.w_DAVALRIG;
                  ,this.w_DAOREEFF;
                  ,this.w_DAMINEFF;
                  ,this.w_DACOSUNI;
                  ,this.w_DACOSINT;
                  ,this.w_DAFLDEFF;
                  ,this.w_DAPREMIN;
                  ,this.w_DAPREMAX;
                  ,this.w_DAGAZUFF;
                  ,this.w_DARIFRIG;
                  ,this.w_DACODCOM;
                  ,this.w_DAVOCRIC;
                  ,this.w_DAVOCCOS;
                  ,this.w_DAINICOM;
                  ,this.w_DAFINCOM;
                  ,this.w_DA_SEGNO;
                  ,this.w_DACOMRIC;
                  ,this.w_DACENCOS;
                  ,this.w_DAATTIVI;
                  ,this.w_DACODLIS;
                  ,this.w_DAPROLIS;
                  ,this.w_DAPROSCO;
                  ,this.w_DASCOLIS;
                  ,this.w_DACENRIC;
                  ,this.w_DAATTRIC;
                  ,this.w_DASCONT1;
                  ,this.w_DASCONT2;
                  ,this.w_DASCONT3;
                  ,this.w_DASCONT4;
                  ,this.w_DALISACQ;
                  ,this.w_DATCOINI;
                  ,this.w_DATCOFIN;
                  ,this.w_DATRIINI;
                  ,this.w_DATRIFIN;
                  ,this.w_DARIFPRE;
                  ,this.w_DARIGPRE;
                  ,this.w_DACONCOD;
                  ,this.w_DACONTRA;
                  ,this.w_DACODMOD;
                  ,this.w_DACODIMP;
                  ,this.w_DACOCOMP;
                  ,this.w_DARINNOV;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.OFFDATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFFDATTI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) and (not(Empty(t_DACODATT)) or not(Empty(t_DACODICE)))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'OFFDATTI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'OFFDATTI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) and (not(Empty(t_DACODATT)) or not(Empty(t_DACODICE)))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update OFFDATTI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'OFFDATTI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DACODRES="+cp_ToStrODBCNull(this.w_DACODRES)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DACODICE="+cp_ToStrODBCNull(this.w_DACODICE)+;
                     ",DACODATT="+cp_ToStrODBCNull(this.w_DACODATT)+;
                     ",DATIPRIG="+cp_ToStrODBC(this.w_DATIPRIG)+;
                     ",DATIPRI2="+cp_ToStrODBC(this.w_DATIPRI2)+;
                     ",DADESATT="+cp_ToStrODBC(this.w_DADESATT)+;
                     ",DAUNIMIS="+cp_ToStrODBCNull(this.w_DAUNIMIS)+;
                     ",DAQTAMOV="+cp_ToStrODBC(this.w_DAQTAMOV)+;
                     ",DAPREZZO="+cp_ToStrODBC(this.w_DAPREZZO)+;
                     ",DADESAGG="+cp_ToStrODBC(this.w_DADESAGG)+;
                     ",DACODOPE="+cp_ToStrODBC(this.w_DACODOPE)+;
                     ",DADATMOD="+cp_ToStrODBC(this.w_DADATMOD)+;
                     ",DAVALRIG="+cp_ToStrODBC(this.w_DAVALRIG)+;
                     ",DAOREEFF="+cp_ToStrODBC(this.w_DAOREEFF)+;
                     ",DAMINEFF="+cp_ToStrODBC(this.w_DAMINEFF)+;
                     ",DACOSUNI="+cp_ToStrODBC(this.w_DACOSUNI)+;
                     ",DACOSINT="+cp_ToStrODBC(this.w_DACOSINT)+;
                     ",DAFLDEFF="+cp_ToStrODBC(this.w_DAFLDEFF)+;
                     ",DAPREMIN="+cp_ToStrODBC(this.w_DAPREMIN)+;
                     ",DAPREMAX="+cp_ToStrODBC(this.w_DAPREMAX)+;
                     ",DAGAZUFF="+cp_ToStrODBC(this.w_DAGAZUFF)+;
                     ",DARIFRIG="+cp_ToStrODBC(this.w_DARIFRIG)+;
                     ",DACODCOM="+cp_ToStrODBC(this.w_DACODCOM)+;
                     ",DAVOCRIC="+cp_ToStrODBC(this.w_DAVOCRIC)+;
                     ",DAVOCCOS="+cp_ToStrODBC(this.w_DAVOCCOS)+;
                     ",DAINICOM="+cp_ToStrODBC(this.w_DAINICOM)+;
                     ",DAFINCOM="+cp_ToStrODBC(this.w_DAFINCOM)+;
                     ",DA_SEGNO="+cp_ToStrODBC(this.w_DA_SEGNO)+;
                     ",DACOMRIC="+cp_ToStrODBC(this.w_DACOMRIC)+;
                     ",DACENCOS="+cp_ToStrODBCNull(this.w_DACENCOS)+;
                     ",DAATTIVI="+cp_ToStrODBC(this.w_DAATTIVI)+;
                     ",DACODLIS="+cp_ToStrODBC(this.w_DACODLIS)+;
                     ",DAPROLIS="+cp_ToStrODBC(this.w_DAPROLIS)+;
                     ",DAPROSCO="+cp_ToStrODBC(this.w_DAPROSCO)+;
                     ",DASCOLIS="+cp_ToStrODBC(this.w_DASCOLIS)+;
                     ",DACENRIC="+cp_ToStrODBC(this.w_DACENRIC)+;
                     ",DAATTRIC="+cp_ToStrODBC(this.w_DAATTRIC)+;
                     ",DASCONT1="+cp_ToStrODBC(this.w_DASCONT1)+;
                     ",DASCONT2="+cp_ToStrODBC(this.w_DASCONT2)+;
                     ",DASCONT3="+cp_ToStrODBC(this.w_DASCONT3)+;
                     ",DASCONT4="+cp_ToStrODBC(this.w_DASCONT4)+;
                     ",DALISACQ="+cp_ToStrODBC(this.w_DALISACQ)+;
                     ",DATCOINI="+cp_ToStrODBC(this.w_DATCOINI)+;
                     ",DATCOFIN="+cp_ToStrODBC(this.w_DATCOFIN)+;
                     ",DATRIINI="+cp_ToStrODBC(this.w_DATRIINI)+;
                     ",DATRIFIN="+cp_ToStrODBC(this.w_DATRIFIN)+;
                     ",DARIFPRE="+cp_ToStrODBC(this.w_DARIFPRE)+;
                     ",DARIGPRE="+cp_ToStrODBC(this.w_DARIGPRE)+;
                     ",DACONCOD="+cp_ToStrODBC(this.w_DACONCOD)+;
                     ",DACONTRA="+cp_ToStrODBCNull(this.w_DACONTRA)+;
                     ",DACODMOD="+cp_ToStrODBC(this.w_DACODMOD)+;
                     ",DACODIMP="+cp_ToStrODBC(this.w_DACODIMP)+;
                     ",DACOCOMP="+cp_ToStrODBC(this.w_DACOCOMP)+;
                     ",DARINNOV="+cp_ToStrODBC(this.w_DARINNOV)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'OFFDATTI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DACODRES=this.w_DACODRES;
                     ,CPROWORD=this.w_CPROWORD;
                     ,DACODICE=this.w_DACODICE;
                     ,DACODATT=this.w_DACODATT;
                     ,DATIPRIG=this.w_DATIPRIG;
                     ,DATIPRI2=this.w_DATIPRI2;
                     ,DADESATT=this.w_DADESATT;
                     ,DAUNIMIS=this.w_DAUNIMIS;
                     ,DAQTAMOV=this.w_DAQTAMOV;
                     ,DAPREZZO=this.w_DAPREZZO;
                     ,DADESAGG=this.w_DADESAGG;
                     ,DACODOPE=this.w_DACODOPE;
                     ,DADATMOD=this.w_DADATMOD;
                     ,DAVALRIG=this.w_DAVALRIG;
                     ,DAOREEFF=this.w_DAOREEFF;
                     ,DAMINEFF=this.w_DAMINEFF;
                     ,DACOSUNI=this.w_DACOSUNI;
                     ,DACOSINT=this.w_DACOSINT;
                     ,DAFLDEFF=this.w_DAFLDEFF;
                     ,DAPREMIN=this.w_DAPREMIN;
                     ,DAPREMAX=this.w_DAPREMAX;
                     ,DAGAZUFF=this.w_DAGAZUFF;
                     ,DARIFRIG=this.w_DARIFRIG;
                     ,DACODCOM=this.w_DACODCOM;
                     ,DAVOCRIC=this.w_DAVOCRIC;
                     ,DAVOCCOS=this.w_DAVOCCOS;
                     ,DAINICOM=this.w_DAINICOM;
                     ,DAFINCOM=this.w_DAFINCOM;
                     ,DA_SEGNO=this.w_DA_SEGNO;
                     ,DACOMRIC=this.w_DACOMRIC;
                     ,DACENCOS=this.w_DACENCOS;
                     ,DAATTIVI=this.w_DAATTIVI;
                     ,DACODLIS=this.w_DACODLIS;
                     ,DAPROLIS=this.w_DAPROLIS;
                     ,DAPROSCO=this.w_DAPROSCO;
                     ,DASCOLIS=this.w_DASCOLIS;
                     ,DACENRIC=this.w_DACENRIC;
                     ,DAATTRIC=this.w_DAATTRIC;
                     ,DASCONT1=this.w_DASCONT1;
                     ,DASCONT2=this.w_DASCONT2;
                     ,DASCONT3=this.w_DASCONT3;
                     ,DASCONT4=this.w_DASCONT4;
                     ,DALISACQ=this.w_DALISACQ;
                     ,DATCOINI=this.w_DATCOINI;
                     ,DATCOFIN=this.w_DATCOFIN;
                     ,DATRIINI=this.w_DATRIINI;
                     ,DATRIFIN=this.w_DATRIFIN;
                     ,DARIFPRE=this.w_DARIFPRE;
                     ,DARIGPRE=this.w_DARIGPRE;
                     ,DACONCOD=this.w_DACONCOD;
                     ,DACONTRA=this.w_DACONTRA;
                     ,DACODMOD=this.w_DACODMOD;
                     ,DACODIMP=this.w_DACODIMP;
                     ,DACOCOMP=this.w_DACOCOMP;
                     ,DARINNOV=this.w_DARINNOV;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.OFFDATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFFDATTI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) and (not(Empty(t_DACODATT)) or not(Empty(t_DACODICE)))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete OFFDATTI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) and (not(Empty(t_DACODATT)) or not(Empty(t_DACODICE)))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OFFDATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFFDATTI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .w_FLGLIS = this.oparentobject.w_FLGLIS
          .w_DATALIST = IIF(Not Empty(this.oparentobject.w_ATDATDOC),this.oparentobject.w_ATDATDOC,this.oparentobject.w_DATFIN)
          .w_MVCODVAL = g_PERVAL
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(8,10,.t.)
          .w_OLDCOM = iif(Not Empty(.w_CODCOM),.w_CODCOM,.w_DACODCOM)
          .w_FLANAL = this.oparentobject.w_FLANAL
          .w_FLGCOM = this.oparentobject.w_FLGCOM
          .w_FLDANA = this.oparentobject.w_FLDANA
          .w_FLACOMAQ = this.oparentobject.w_FLACOMAQ
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
          .w_OB_TEST = this.oparentobject.w_DATINI
        .DoRTCalc(17,17,.t.)
          .w_MVFLVEAC = this.oparentobject.w_MVFLVEAC
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
          .w_LISACQ = this.oparentobject.w_LISACQ
          .w_CODLIS = this.oparentobject.w_CODLIS
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
          .w_FLACQ = this.oparentobject.w_FLACQ
          .w_CAUACQ = this.oparentobject.w_ATCAUACQ
          .w_CAUDOC = this.oparentobject.w_ATCAUDOC
          .w_DACODNOM = this.oparentobject.w_ATCODNOM
          .w_PR__DATA = this.oparentobject.w_DATFIN
          .w_OLDCOMRIC = this.oparentobject.w_ATCOMRIC
          .w_OLDATTC = this.oparentobject.w_ATATTCOS
          .w_OLDATTR = this.oparentobject.w_ATATTRIC
          .w_CODLIN = this.oParentObject.w_CODLIN
          .w_VOCECR = this.oparentobject.w_VOCECR
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        if .o_LetturaParAlte<>.w_LetturaParAlte
          .w_LetturaParAlte = i_CodAzi
          .link_1_55('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .DoRTCalc(32,33,.t.)
          .w_CODCLI = this.oparentobject.w_CODCLI
          .link_1_59('Full')
          .w_BUFLANAL = this.oParentObject.w_BUFLANAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
          .w_FLCCRAUTO = This.oparentobject .w_FLCCRAUTO
        if .o_DACODRES<>.w_DACODRES
          .link_2_1('Full')
        endif
        .DoRTCalc(38,38,.t.)
        if .o_SERPER<>.w_SERPER
          .w_DACODICE = IIF(.w_ISAHE,IIF(EMPTY(.w_DACODICE), .w_SERPER, .w_DACODICE), SPACE(41))
          .link_2_3('Full')
        endif
        .DoRTCalc(40,40,.t.)
        if .o_SERPER<>.w_SERPER
          .w_DACODATT = IIF(!.w_ISAHE, IIF(EMPTY(.w_DACODATT), .w_SERPER, .w_DACODATT), SPACE(20))
          .link_2_5('Full')
        endif
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
          .link_2_6('Full')
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE
          .Calculate_URSUGUQJTW()
        endif
        .DoRTCalc(43,48,.t.)
          .link_2_14('Full')
        .DoRTCalc(50,52,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_MOLTI3 = IIF(.w_ISAHE,.w_EMOLTI3,.w_RMOLTI3)
        endif
        .DoRTCalc(54,56,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_OPERA3 = IIF(.w_ISAHE,.w_EOPERA3,.w_ROPERA3)
        endif
        .DoRTCalc(58,65,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_COD1ATT = IIF(.w_ISAHE,.w_ECOD1ATT,.w_RCOD1ATT)
          .link_2_31('Full')
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE
          .w_DADESATT = IIF(.w_ISAHE, .w_EDESATT, .w_RDESATT)
        endif
        .DoRTCalc(68,68,.t.)
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE
          .w_DAUNIMIS = .w_UNMIS1
          .link_2_34('Full')
        endif
        .DoRTCalc(70,70,.t.)
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE
          .w_DAQTAMOV = IIF(.w_TIPART='DE', 0, IIF(.w_DAQTAMOV>0,.w_DAQTAMOV,1))
        endif
        .DoRTCalc(72,72,.t.)
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE
          .w_DADESAGG = IIF(.w_ISAHE, .w_EDESAGG, .w_RDESAGG)
        endif
        .DoRTCalc(74,76,.t.)
          .w_DAVALRIG = IIF(UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE', CAVALRIG(.w_DAPREZZO,.w_DAQTAMOV, .w_DASCONT1,.w_DASCONT2,.w_DASCONT3,.w_DASCONT4,IIF(.w_NOTIFICA='S' AND .w_ISALT, 0, this.oParentObject.w_DECTOT)), cp_ROUND(.w_DAQTAMOV*.w_DAPREZZO, this.oParentObject.w_DECTOT))
        if .o_DACODATT<>.w_DACODATT.or. .o_DAPREZZO<>.w_DAPREZZO.or. .o_DACODRES<>.w_DACODRES
          .w_ATIPRIG = ICASE(.w_ISAHE,.w_ETIPRIG,.w_Isahr,.w_RTIPRIG,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N', Not empty(.w_DATIPRIG),.w_DATIPRIG,!EMPTY(.w_CATIPRIG),.w_CATIPRIG,.w_ARTIPRIG))
        endif
        if .o_DACODICE<>.w_DACODICE.or. .o_DAPREZZO<>.w_DAPREZZO
          .w_ETIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DAPREZZO<>.w_DAPREZZO
          .w_RTIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(!EMPTY(.w_CATIPRIG),.w_CATIPRIG, Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DAPREZZO<>.w_DAPREZZO
          .w_ATIPRI2 = ICASE(.w_Isahe,.w_ETIPRI2,.w_Isahr,.w_RTIPRI2,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', Not empty(.w_DATIPRI2),.w_DATIPRI2,!EMPTY(.w_CATIPRI2),.w_CATIPRI2,.w_ARTIPRI2))
        endif
        .DoRTCalc(82,84,.t.)
        if .o_DCODCEN<>.w_DCODCEN.or. .o_CENCOS<>.w_CENCOS.or. .o_DACENCOS<>.w_DACENCOS.or. .o_DACODRES<>.w_DACODRES
          .w_OLDCEN = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENCOS),.w_CENCOS,.w_DACENCOS)
        endif
        if .o_DCODCEN<>.w_DCODCEN.or. .o_CENCOS<>.w_CENCOS.or. .o_DACENRIC<>.w_DACENRIC.or. .o_DACODRES<>.w_DACODRES
          .w_OLDCENRIC = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENRIC),.w_CENRIC,.w_DACENRIC)
        endif
        .DoRTCalc(87,87,.t.)
          .w_CENCOS = this.oparentobject.w_ATCENCOS
        .DoRTCalc(89,90,.t.)
        if .o_DAOREEFF<>.w_DAOREEFF.or. .o_DAMINEFF<>.w_DAMINEFF.or. .o_DACOSUNI<>.w_DACOSUNI
          .w_DACOSINT = IIF(.w_LICOSTO>0 and .w_COST_ORA=0 ,.w_DACOSUNI*.w_DAQTAMOV,(.w_DAOREEFF+(.w_DAMINEFF/60))*.w_DACOSUNI)
        endif
        .DoRTCalc(92,96,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_UNMIS3 = IIF(.w_ISAHE,.w_EUNMIS3,.w_RUNMIS3)
        endif
        .DoRTCalc(98,99,.t.)
        if .o_DAQTAMOV<>.w_DAQTAMOV.or. .o_DAUNIMIS<>.w_DAUNIMIS
          .w_QTAUM1 = IIF(.w_DATIPRIG='F', 1, CALQTA(.w_DAQTAMOV,.w_DAUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ', .w_UNMIS3, .w_OPERA3, .w_MOLTI3))
        endif
        .DoRTCalc(101,105,.t.)
        if .o_DACODRES<>.w_DACODRES
          .w_DENOM_RESP = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_73.Calculate()
        .DoRTCalc(107,112,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_CADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
        endif
        .DoRTCalc(114,114,.t.)
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE
          .w_DAVOCRIC = SearchVoc(this,'R',.w_COD1ATT,this.oparentobject.w_ATTIPCLI,this.oparentobject.w_CODCLI,this.oparentobject.w_ATCAUDOC,this.oparentobject.w_ATCODNOM)
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE
          .w_DAVOCCOS = SearchVoc(this,'C',.w_COD1ATT,this.oparentobject.w_ATTIPCLI,this.oparentobject.w_CODCLI,this.oparentobject.w_ATCAUDOC,this.oparentobject.w_ATCODNOM)
        endif
        .DoRTCalc(117,120,.t.)
        if .o_DACODRES<>.w_DACODRES
          .link_2_89('Full')
        endif
        .DoRTCalc(122,125,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_TIPSER = IIF(.w_ISAHE,.w_ETIPSER,.w_RTIPSER)
        endif
        if .o_ATIPRIG<>.w_ATIPRIG.or. .o_ETIPRIG<>.w_ETIPRIG.or. .o_RTIPRIG<>.w_RTIPRIG.or. .o_ATIPRI2<>.w_ATIPRI2.or. .o_ETIPRI2<>.w_ETIPRI2.or. .o_RTIPRI2<>.w_RTIPRI2
          .Calculate_EUZNZQEHYY()
        endif
        .DoRTCalc(127,148,.t.)
          .w_CENRIC = this.oparentobject.w_ATCENRIC
          .w_FLSCOR = THIS.OPARENTOBJECT.w_FLSCOR
        if .o_DAPREZZO<>.w_DAPREZZO.or. .o_DACODRES<>.w_DACODRES
          .Calculate_PLUJZKETBZ()
        endif
        if .o_DACODRES<>.w_DACODRES.or. .o_DACODICE<>.w_DACODICE
          .Calculate_AZIXBNPXNY()
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_128.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_129.Calculate()
        .DoRTCalc(151,157,.t.)
          .link_2_134('Full')
        .DoRTCalc(159,168,.t.)
          .w_NUMSCO = this.oparentobject.w_NUMSCO
        if .o_DACODICE<>.w_DACODICE.or. .o_DAPREZZO<>.w_DAPREZZO
          .w_ETIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DAPREZZO<>.w_DAPREZZO
          .w_RTIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,'D')
        endif
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .DoRTCalc(172,180,.t.)
        if .o_DASERIAL<>.w_DASERIAL
          .w_OLD_ARPRESTA = .w_ARPRESTA
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODRES<>.w_DACODRES
          .Calculate_NRWKWRPQTU()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(182,182,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_VOCECR with this.w_VOCECR
      replace t_RCACODART with this.w_RCACODART
      replace t_CACODART with this.w_CACODART
      replace t_ARTIPRIG with this.w_ARTIPRIG
      replace t_ARTIPRI2 with this.w_ARTIPRI2
      replace t_CATIPRIG with this.w_CATIPRIG
      replace t_CATIPRI2 with this.w_CATIPRI2
      replace t_DATIPRIG with this.w_DATIPRIG
      replace t_DATIPRI2 with this.w_DATIPRI2
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_EMOLTI3 with this.w_EMOLTI3
      replace t_RMOLTI3 with this.w_RMOLTI3
      replace t_FLFRAZ1 with this.w_FLFRAZ1
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_EOPERA3 with this.w_EOPERA3
      replace t_ROPERA3 with this.w_ROPERA3
      replace t_MODUM2 with this.w_MODUM2
      replace t_OPERA3 with this.w_OPERA3
      replace t_OPERAT with this.w_OPERAT
      replace t_ECOD1ATT with this.w_ECOD1ATT
      replace t_RCOD1ATT with this.w_RCOD1ATT
      replace t_PREZUM with this.w_PREZUM
      replace t_EDESAGG with this.w_EDESAGG
      replace t_RDESAGG with this.w_RDESAGG
      replace t_EDESATT with this.w_EDESATT
      replace t_RDESATT with this.w_RDESATT
      replace t_COD1ATT with this.w_COD1ATT
      replace t_TIPART with this.w_TIPART
      replace t_DTIPRIG with this.w_DTIPRIG
      replace t_COST_ORA with this.w_COST_ORA
      replace t_LICOSTO with this.w_LICOSTO
      replace t_DCODCEN with this.w_DCODCEN
      replace t_FLRESP with this.w_FLRESP
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_RUNMIS3 with this.w_RUNMIS3
      replace t_ECACODART with this.w_ECACODART
      replace t_FLSERG with this.w_FLSERG
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_DAFLDEFF with this.w_DAFLDEFF
      replace t_QTAUM1 with this.w_QTAUM1
      replace t_COGNOME with this.w_COGNOME
      replace t_NOME with this.w_NOME
      replace t_NOTIFICA with this.w_NOTIFICA
      replace t_DARIFRIG with this.w_DARIFRIG
      replace t_CHKTEMP with this.w_CHKTEMP
      replace t_DUR_ORE with this.w_DUR_ORE
      replace t_ECADTOBSO with this.w_ECADTOBSO
      replace t_RCADTOBSO with this.w_RCADTOBSO
      replace t_CADTOBSO with this.w_CADTOBSO
      replace t_DACODCOM with this.w_DACODCOM
      replace t_DAVOCRIC with this.w_DAVOCRIC
      replace t_DAVOCCOS with this.w_DAVOCCOS
      replace t_DAINICOM with this.w_DAINICOM
      replace t_DAFINCOM with this.w_DAFINCOM
      replace t_DA_SEGNO with this.w_DA_SEGNO
      replace t_DACOMRIC with this.w_DACOMRIC
      replace t_DACENCOS with this.w_DACENCOS
      replace t_DAATTIVI with this.w_DAATTIVI
      replace t_ETIPSER with this.w_ETIPSER
      replace t_RTIPSER with this.w_RTIPSER
      replace t_SERPER with this.w_SERPER
      replace t_TIPSER with this.w_TIPSER
      replace t_TIPRIS with this.w_TIPRIS
      replace t_FLUSEP with this.w_FLUSEP
      replace t_MOLTIP with this.w_MOLTIP
      replace t_ARTIPART with this.w_ARTIPART
      replace t_EUNMIS3 with this.w_EUNMIS3
      replace t_DACODLIS with this.w_DACODLIS
      replace t_DAPROLIS with this.w_DAPROLIS
      replace t_DAPROSCO with this.w_DAPROSCO
      replace t_DASCOLIS with this.w_DASCOLIS
      replace t_DATOBSO with this.w_DATOBSO
      replace t_DACENRIC with this.w_DACENRIC
      replace t_DAATTRIC with this.w_DAATTRIC
      replace t_DASCONT1 with this.w_DASCONT1
      replace t_DASCONT2 with this.w_DASCONT2
      replace t_DASCONT3 with this.w_DASCONT3
      replace t_DASCONT4 with this.w_DASCONT4
      replace t_FL_FRAZ with this.w_FL_FRAZ
      replace t_DALISACQ with this.w_DALISACQ
      replace t_DATCOINI with this.w_DATCOINI
      replace t_DATCOFIN with this.w_DATCOFIN
      replace t_DATRIINI with this.w_DATRIINI
      replace t_DATRIFIN with this.w_DATRIFIN
      replace t_FLSCOR with this.w_FLSCOR
      replace t_CEN_CauDoc with this.w_CEN_CauDoc
      replace t_DARIFPRE with this.w_DARIFPRE
      replace t_DARIGPRE with this.w_DARIGPRE
      replace t_FLSPAN with this.w_FLSPAN
      replace t_DARIFPRE with this.w_DARIFPRE
      replace t_DARIGPRE with this.w_DARIGPRE
      replace t_DACONCOD with this.w_DACONCOD
      replace t_DACONTRA with this.w_DACONTRA
      replace t_DACODMOD with this.w_DACODMOD
      replace t_DACODIMP with this.w_DACODIMP
      replace t_DACOCOMP with this.w_DACOCOMP
      replace t_DARINNOV with this.w_DARINNOV
      replace t_FLUNIV with this.w_FLUNIV
      replace t_NUMPRE with this.w_NUMPRE
      replace t_ARPRESTA with this.w_ARPRESTA
      replace t_COTIPCON with this.w_COTIPCON
      replace t_COCODCON with this.w_COCODCON
      replace t_ETIPRI2 with this.w_ETIPRI2
      replace t_RTIPRI2 with this.w_RTIPRI2
      replace t_TIPRIGCA with this.w_TIPRIGCA
      replace t_TIPRI2CA with this.w_TIPRI2CA
      replace t_CHKDATAPRE with this.w_CHKDATAPRE
      replace t_GG_PRE with this.w_GG_PRE
      replace t_GG_SUC with this.w_GG_SUC
      replace t_DATFIN with this.w_DATFIN
      replace t_OLD_ARPRESTA with this.w_OLD_ARPRESTA
      replace t_OKTARCON with this.w_OKTARCON
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_73.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_128.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_129.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_73.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_128.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_129.Calculate()
    endwith
  return
  proc Calculate_URSUGUQJTW()
    with this
          * --- Init tipo documento
          .w_DATIPRIG = ''
          .w_DATIPRI2 = ''
          .o_ATIPRIG = ' '
          .o_ATIPRI2 = ' '
    endwith
  endproc
  proc Calculate_LDKASKAOZX()
    with this
          * --- Ricalcolo durata eff. (ore e minuti)
          .w_COD1ATT = IIF(.w_Isahe,.w_ECOD1ATT,.w_RCOD1ATT)
          .link_2_31('Full')
          .w_DAUNIMIS = iif(empty(.w_DAUNIMIS),.w_UNMIS1,.w_DAUNIMIS)
          .w_DAOREEFF = Min(IIF(.w_CHKTEMP='S', INT(.w_DUR_ORE * .w_DAQTAMOV), .w_DAOREEFF),999)
          .w_DAMINEFF = IIF(.w_CHKTEMP='S', INT(cp_Round((.w_DUR_ORE * .w_DAQTAMOV - INT(.w_DUR_ORE * .w_DAQTAMOV)) * 60,0)), .w_DAMINEFF)
    endwith
  endproc
  proc Calculate_EUZNZQEHYY()
    with this
          * --- Ricalcolo tipo documento al cambiare delle combo collegate
          .w_DATIPRIG = ICASE(.w_ISAHE,.w_ETIPRIG,.w_ISAHR,.w_RTIPRIG,.w_ATIPRIG)
          .w_DATIPRI2 = ICASE(.w_Isahe,.w_ETIPRI2,.w_Isahr,.w_RTIPRI2,.w_ATIPRI2)
          .w_ATIPRIG = .w_DATIPRIG
          .w_ATIPRI2 = .w_DATIPRI2
    endwith
  endproc
  proc Calculate_PLUJZKETBZ()
    with this
          * --- Ricalcolo tipo documento al cambiare del prezzo
          .w_ARTIPRIG = IIF(.w_DAPREZZO <>0 AND .w_TIPRIGCA='D', .w_TIPRIGCA,'N')
          .w_ARTIPRI2 = IIF(.w_DAPREZZO <>0 AND .w_TIPRI2CA='D', .w_TIPRI2CA,'N')
          .w_DATIPRIG = ICASE(.w_Isahe,.w_ETIPRIG,.w_Isahr,.w_RTIPRIG,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 AND .w_FLGZER<>'S', 'N',.w_DTIPRIG='N', 'N',.w_DAPREZZO<>0,IIF(!EMPTY(.w_CATIPRIG),.w_CATIPRIG,.w_ARTIPRIG),Not empty(.w_DATIPRIG),.w_DATIPRIG,!EMPTY(.w_CATIPRIG),.w_CATIPRIG,.w_ARTIPRIG))
          .w_DATIPRI2 = ICASE(.w_Isahe,.w_ETIPRI2,.w_Isahr,.w_RTIPRI2,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 AND .w_FLGZER<>'S', 'N',.w_DAPREZZO<>0,IIF(!EMPTY(.w_CATIPRI2),.w_CATIPRI2,.w_ARTIPRI2),Not empty(.w_DATIPRI2),.w_DATIPRI2,!EMPTY(.w_CATIPRI2),.w_CATIPRI2,.w_ARTIPRI2))
          .w_ATIPRIG = .w_DATIPRIG
          .w_ATIPRI2 = .w_DATIPRI2
    endwith
  endproc
  proc Calculate_AZIXBNPXNY()
    with this
          * --- Centri di costo e ricavo
          .w_CEN_CauDoc = SearchVoc(this,'T',.w_DACODICE,'','','','')
          .w_DACENCOS = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,!EMPTY(.w_CENCOS) ,.w_CENCOS,.w_OLDCEN)
          .w_DACENRIC = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,!EMPTY(.w_CENRIC) ,.w_CENRIC,.w_OLDCENRIC)
    endwith
  endproc
  proc Calculate_YHWEZLTWJB()
    with this
          * --- Aggiorna dati di riga Ahr da abbinamento contratto
          .w_DACODATT = .w_DACODATT
          .link_2_5('Full')
    endwith
  endproc
  proc Calculate_BJENDTWDUP()
    with this
          * --- Aggiorna dati di riga Ahe da abbinamento contratto
          .w_DACODICE = .w_DACODICE
          .link_2_3('Full')
    endwith
  endproc
  proc Calculate_GZPILQVQEQ()
    with this
          * --- Aggiorna tipo riga  righe collegate
          GSAG_BCK(this;
              ,'H';
             )
    endwith
  endproc
  proc Calculate_ONMXVQNJVL()
    with this
          * --- Inizializzo partecipante
          .w_DACODRES = iif(.w_ISALT,InitResp( this ),Space(5))
          .link_2_1('Full')
    endwith
  endproc
  proc Calculate_TOXJEOWPVC()
    with this
          * --- Ricalcolo solo il costo interno
          .w_DACOSUNI = IIF(.w_LICOSTO>0 OR (.w_COST_ORA=0 and .w_CHKTEMP<>'S' and .w_TIPRIS$ 'R-P'),.w_LICOSTO,.w_COST_ORA)
          .w_DACOSINT = IIF(.w_LICOSTO>0 and .w_COST_ORA=0 ,.w_DACOSUNI*.w_DAQTAMOV,(.w_DAOREEFF+(.w_DAMINEFF/60))*.w_DACOSUNI)
    endwith
  endproc
  proc Calculate_HJNRWXIGZW()
    with this
          * --- Linkpre
          .w_DACODICE = .w_DACODICE
          .link_2_3('Full')
          .w_DACODATT = .w_DACODATT
          .link_2_5('Full')
    endwith
  endproc
  proc Calculate_NRWKWRPQTU()
    with this
          * --- Legge tariffa concordata e ricalcola il prezzo unitario
          GSAG_BDE(this;
              ,'LEGGETARCONC';
             )
          .w_OKTARCON = ' '
          .o_DACODRES = .w_DACODRES
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDACODATT_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDACODATT_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDADESATT_2_32.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDADESATT_2_32.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDAUNIMIS_2_34.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDAUNIMIS_2_34.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDAQTAMOV_2_36.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDAQTAMOV_2_36.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDAPREZZO_2_37.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDAPREZZO_2_37.mCond()
    this.oPgFrm.Page1.oPag.oATIPRIG_2_43.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oATIPRIG_2_43.mCond()
    this.oPgFrm.Page1.oPag.oATIPRI2_2_46.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oATIPRI2_2_46.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_3.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_3.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_5.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_5.mHide()
    this.oPgFrm.Page1.oPag.oDESRIS_2_33.visible=!this.oPgFrm.Page1.oPag.oDESRIS_2_33.mHide()
    this.oPgFrm.Page1.oPag.oATIPRIG_2_43.visible=!this.oPgFrm.Page1.oPag.oATIPRIG_2_43.mHide()
    this.oPgFrm.Page1.oPag.oETIPRIG_2_44.visible=!this.oPgFrm.Page1.oPag.oETIPRIG_2_44.mHide()
    this.oPgFrm.Page1.oPag.oRTIPRIG_2_45.visible=!this.oPgFrm.Page1.oPag.oRTIPRIG_2_45.mHide()
    this.oPgFrm.Page1.oPag.oATIPRI2_2_46.visible=!this.oPgFrm.Page1.oPag.oATIPRI2_2_46.mHide()
    this.oPgFrm.Page1.oPag.oDACOSUNI_2_54.visible=!this.oPgFrm.Page1.oPag.oDACOSUNI_2_54.mHide()
    this.oPgFrm.Page1.oPag.oDAPREMIN_2_68.visible=!this.oPgFrm.Page1.oPag.oDAPREMIN_2_68.mHide()
    this.oPgFrm.Page1.oPag.oDAPREMAX_2_69.visible=!this.oPgFrm.Page1.oPag.oDAPREMAX_2_69.mHide()
    this.oPgFrm.Page1.oPag.oDAGAZUFF_2_70.visible=!this.oPgFrm.Page1.oPag.oDAGAZUFF_2_70.mHide()
    this.oPgFrm.Page1.oPag.oDENOM_RESP_2_71.visible=!this.oPgFrm.Page1.oPag.oDENOM_RESP_2_71.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_87.visible=!this.oPgFrm.Page1.oPag.oBtn_2_87.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_100.visible=!this.oPgFrm.Page1.oPag.oBtn_2_100.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_101.visible=!this.oPgFrm.Page1.oPag.oBtn_2_101.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_102.visible=!this.oPgFrm.Page1.oPag.oBtn_2_102.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_mda
    If this.w_ISALT And (cEvent='w_DACODATT Changed')
       this.NotifyEvent('Calcola')
    endif
    If this.w_ISALT And (cEvent='w_DAQTAMOV Changed' or cEvent='w_DAUNIMIS Changed')
       this.NotifyEvent('CalcNot')
    endif
    If Not this.w_ISALT and (IIF(this.w_ISAHR,cEvent='w_DACODATT Changed',cEvent='w_DACODICE Changed') or cEvent='w_DAUNIMIS Changed' or cEvent='w_DAQTAMOV Changed' )
        this.NotifyEvent('Calcola')
    endif
    if this.w_ISALT AND cevent='w_DAPREZZO Changed' and (This.w_TIPART='FO' and This.w_DARIGPRE='S' and This.w_DAPREZZO=0)
       Ah_errormsg("Attenzione, riga collegata! Impossibile azzerare la tariffa")
       This.w_DAPREZZO=This.o_DAPREZZO
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Init Row")
          .Calculate_URSUGUQJTW()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_73.Event(cEvent)
        if lower(cEvent)==lower("Aggcosto")
          .Calculate_LDKASKAOZX()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_128.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_129.Event(cEvent)
        if lower(cEvent)==lower("AggiornaAhr")
          .Calculate_YHWEZLTWJB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AggiornaAhe")
          .Calculate_BJENDTWDUP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ATIPRI2 Changed") or lower(cEvent)==lower("w_ATIPRIG Changed")
          .Calculate_GZPILQVQEQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init Row")
          .Calculate_ONMXVQNJVL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AggSoloCosto") or lower(cEvent)==lower("Aggcosto")
          .Calculate_TOXJEOWPVC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Linkpre")
          .Calculate_HJNRWXIGZW()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
        if lower(cEvent)==lower("Leggeimpcon")
          .Calculate_NRWKWRPQTU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_mda
      If this.w_ISALT
       If cevent='w_DACODATT Changed'
         this.NotifyEvent('CalcPrest')
        endif  
        If cevent='Riapplica' AND Ah_YesNo('Attenzione! Spese ed anticipazioni inserite in precedenza saranno eliminate. Continuare?')
         This.Notifyevent('Elispese')
         This.set('w_DARIGPRE' , 'N', .T. ,.T.)
         This.Notifyevent('CalcPrest')
         This.Refresh()
       Endif
      endif
      If cEvent='w_DACODATT Changed' or cEvent='w_DACODICE Changed' or cEvent='w_DAQTAMOV Changed' OR cEvent='w_DAUNIMIS Changed' or cEvent='w_DACODRES Changed'
       this.NotifyEvent('Aggcosto')
      endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LetturaParAlte
  func Link_1_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LetturaParAlte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LetturaParAlte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PANOEDES,PAFLGZER,PAGENPRE";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LetturaParAlte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LetturaParAlte)
            select PACODAZI,PANOEDES,PAFLGZER,PAGENPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LetturaParAlte = NVL(_Link_.PACODAZI,space(5))
      this.w_NOEDDES = NVL(_Link_.PANOEDES,space(1))
      this.w_FLGZER = NVL(_Link_.PAFLGZER,space(1))
      this.w_GENPRE = NVL(_Link_.PAGENPRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LetturaParAlte = space(5)
      endif
      this.w_NOEDDES = space(1)
      this.w_FLGZER = space(1)
      this.w_GENPRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LetturaParAlte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLI
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANCATCOM";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANCATCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_CATCOM = NVL(_Link_.ANCATCOM,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_CATCOM = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODRES
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODRES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_DACODRES)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPCOSORA,DPCODCEN,DPTIPRIS,DPSERPRE,DPDESCRI,DPDTOBSO,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_DACODRES))
          select DPCODICE,DPCOGNOM,DPNOME,DPCOSORA,DPCODCEN,DPTIPRIS,DPSERPRE,DPDESCRI,DPDTOBSO,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODRES)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_DACODRES)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPCOSORA,DPCODCEN,DPTIPRIS,DPSERPRE,DPDESCRI,DPDTOBSO,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_DACODRES)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPCOSORA,DPCODCEN,DPTIPRIS,DPSERPRE,DPDESCRI,DPDTOBSO,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_DACODRES)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPCOSORA,DPCODCEN,DPTIPRIS,DPSERPRE,DPDESCRI,DPDTOBSO,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_DACODRES)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPCOSORA,DPCODCEN,DPTIPRIS,DPSERPRE,DPDESCRI,DPDTOBSO,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACODRES) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oDACODRES_2_1'),i_cWhere,'GSAR_BDZ',"Persone/Risorse/Gruppi",'GSAG_MDA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPCOSORA,DPCODCEN,DPTIPRIS,DPSERPRE,DPDESCRI,DPDTOBSO,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME,DPCOSORA,DPCODCEN,DPTIPRIS,DPSERPRE,DPDESCRI,DPDTOBSO,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODRES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPCOSORA,DPCODCEN,DPTIPRIS,DPSERPRE,DPDESCRI,DPDTOBSO,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_DACODRES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_DACODRES)
            select DPCODICE,DPCOGNOM,DPNOME,DPCOSORA,DPCODCEN,DPTIPRIS,DPSERPRE,DPDESCRI,DPDTOBSO,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODRES = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOME = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME = NVL(_Link_.DPNOME,space(40))
      this.w_COST_ORA = NVL(_Link_.DPCOSORA,0)
      this.w_DCODCEN = NVL(_Link_.DPCODCEN,space(15))
      this.w_TIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_SERPER = NVL(_Link_.DPSERPRE,space(41))
      this.w_DESRIS = NVL(_Link_.DPDESCRI,space(39))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
      this.w_DTIPRIG = NVL(_Link_.DPTIPRIG,space(10))
      this.w_CHKDATAPRE = NVL(_Link_.DPCTRPRE,space(1))
      this.w_GG_PRE = NVL(_Link_.DPGG_PRE,0)
      this.w_GG_SUC = NVL(_Link_.DPGG_SUC,0)
    else
      if i_cCtrl<>'Load'
        this.w_DACODRES = space(5)
      endif
      this.w_COGNOME = space(40)
      this.w_NOME = space(40)
      this.w_COST_ORA = 0
      this.w_DCODCEN = space(15)
      this.w_TIPRIS = space(1)
      this.w_SERPER = space(41)
      this.w_DESRIS = space(39)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DTIPRIG = space(10)
      this.w_CHKDATAPRE = space(1)
      this.w_GG_PRE = 0
      this.w_GG_SUC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_ISALT AND .w_TIPRIS='P') OR (! .w_ISALT AND .w_TIPRIS $ 'P-R' )) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione tipo risorsa incongruente o obsoleta")
        endif
        this.w_DACODRES = space(5)
        this.w_COGNOME = space(40)
        this.w_NOME = space(40)
        this.w_COST_ORA = 0
        this.w_DCODCEN = space(15)
        this.w_TIPRIS = space(1)
        this.w_SERPER = space(41)
        this.w_DESRIS = space(39)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DTIPRIG = space(10)
        this.w_CHKDATAPRE = space(1)
        this.w_GG_PRE = 0
        this.w_GG_SUC = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODRES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 13 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+13<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.DPCODICE as DPCODICE201"+ ",link_2_1.DPCOGNOM as DPCOGNOM201"+ ",link_2_1.DPNOME as DPNOME201"+ ",link_2_1.DPCOSORA as DPCOSORA201"+ ",link_2_1.DPCODCEN as DPCODCEN201"+ ",link_2_1.DPTIPRIS as DPTIPRIS201"+ ",link_2_1.DPSERPRE as DPSERPRE201"+ ",link_2_1.DPDESCRI as DPDESCRI201"+ ",link_2_1.DPDTOBSO as DPDTOBSO201"+ ",link_2_1.DPTIPRIG as DPTIPRIG201"+ ",link_2_1.DPCTRPRE as DPCTRPRE201"+ ",link_2_1.DPGG_PRE as DPGG_PRE201"+ ",link_2_1.DPGG_SUC as DPGG_SUC201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on OFFDATTI.DACODRES=link_2_1.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+13
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and OFFDATTI.DACODRES=link_2_1.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+13
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DACODICE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DACODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DACODICE))
          select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_DACODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_DACODICE)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_DACODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_DACODICE)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDACODICE_2_3'),i_cWhere,'GSMA_BZA',"Prestazioni",'GSAGZKPM.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DACODICE)
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODICE = NVL(_Link_.CACODICE,space(41))
      this.w_EDESATT = NVL(_Link_.CADESART,space(40))
      this.w_EDESAGG = NVL(_Link_.CADESSUP,space(0))
      this.w_ECOD1ATT = NVL(_Link_.CACODART,space(20))
      this.w_EUNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_ECACODART = NVL(_Link_.CACODART,space(20))
      this.w_ECADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_ETIPSER = NVL(_Link_.CA__TIPO,space(1))
      this.w_EOPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_EMOLTI3 = NVL(_Link_.CAMOLTIP,0)
    else
      if i_cCtrl<>'Load'
        this.w_DACODICE = space(41)
      endif
      this.w_EDESATT = space(40)
      this.w_EDESAGG = space(0)
      this.w_ECOD1ATT = space(20)
      this.w_EUNMIS3 = space(3)
      this.w_ECACODART = space(20)
      this.w_ECADTOBSO = ctod("  /  /  ")
      this.w_ETIPSER = space(1)
      this.w_EOPERA3 = space(1)
      this.w_EMOLTI3 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!.w_ISAHE or ChkTipArt(.w_ECACODART, "FM-FO-DE") AND (EMPTY(.w_ECADTOBSO) OR .w_ECADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_DACODICE = space(41)
        this.w_EDESATT = space(40)
        this.w_EDESAGG = space(0)
        this.w_ECOD1ATT = space(20)
        this.w_EUNMIS3 = space(3)
        this.w_ECACODART = space(20)
        this.w_ECADTOBSO = ctod("  /  /  ")
        this.w_ETIPSER = space(1)
        this.w_EOPERA3 = space(1)
        this.w_EMOLTI3 = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CACODICE as CACODICE203"+ ",link_2_3.CADESART as CADESART203"+ ",link_2_3.CADESSUP as CADESSUP203"+ ",link_2_3.CACODART as CACODART203"+ ",link_2_3.CAUNIMIS as CAUNIMIS203"+ ",link_2_3.CACODART as CACODART203"+ ",link_2_3.CADTOBSO as CADTOBSO203"+ ",link_2_3.CA__TIPO as CA__TIPO203"+ ",link_2_3.CAOPERAT as CAOPERAT203"+ ",link_2_3.CAMOLTIP as CAMOLTIP203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on OFFDATTI.DACODICE=link_2_3.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and OFFDATTI.DACODICE=link_2_3.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DACODATT
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DACODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DACODATT))
          select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_DACODATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_DACODATT)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_DACODATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_DACODATT)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACODATT) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDACODATT_2_5'),i_cWhere,'GSMA_BZA',"Prestazioni",''+iif(NOT isalt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DACODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DACODATT)
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODATT = NVL(_Link_.CACODICE,space(20))
      this.w_RDESATT = NVL(_Link_.CADESART,space(40))
      this.w_RDESAGG = NVL(_Link_.CADESSUP,space(0))
      this.w_RCOD1ATT = NVL(_Link_.CACODART,space(20))
      this.w_RUNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_RCACODART = NVL(_Link_.CACODART,space(20))
      this.w_RCADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_RTIPSER = NVL(_Link_.CA__TIPO,space(1))
      this.w_ROPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_RMOLTI3 = NVL(_Link_.CAMOLTIP,0)
    else
      if i_cCtrl<>'Load'
        this.w_DACODATT = space(20)
      endif
      this.w_RDESATT = space(40)
      this.w_RDESAGG = space(0)
      this.w_RCOD1ATT = space(20)
      this.w_RUNMIS3 = space(3)
      this.w_RCACODART = space(20)
      this.w_RCADTOBSO = ctod("  /  /  ")
      this.w_RTIPSER = space(1)
      this.w_ROPERA3 = space(1)
      this.w_RMOLTI3 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ISAHE or ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_DACODATT = space(20)
        this.w_RDESATT = space(40)
        this.w_RDESAGG = space(0)
        this.w_RCOD1ATT = space(20)
        this.w_RUNMIS3 = space(3)
        this.w_RCACODART = space(20)
        this.w_RCADTOBSO = ctod("  /  /  ")
        this.w_RTIPSER = space(1)
        this.w_ROPERA3 = space(1)
        this.w_RMOLTI3 = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.CACODICE as CACODICE205"+ ",link_2_5.CADESART as CADESART205"+ ",link_2_5.CADESSUP as CADESSUP205"+ ",link_2_5.CACODART as CACODART205"+ ",link_2_5.CAUNIMIS as CAUNIMIS205"+ ",link_2_5.CACODART as CACODART205"+ ",link_2_5.CADTOBSO as CADTOBSO205"+ ",link_2_5.CA__TIPO as CA__TIPO205"+ ",link_2_5.CAOPERAT as CAOPERAT205"+ ",link_2_5.CAMOLTIP as CAMOLTIP205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on OFFDATTI.DACODATT=link_2_5.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and OFFDATTI.DACODATT=link_2_5.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CACODART
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,AROPERAT,ARFLUSEP,ARMOLTIP,ARPREZUM,ARTIPART,ARFLSPAN,ARTIPRIG,ARTIPRI2,ARFLUNIV,ARNUMPRE,ARPRESTA";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CACODART)
            select ARCODART,AROPERAT,ARFLUSEP,ARMOLTIP,ARPREZUM,ARTIPART,ARFLSPAN,ARTIPRIG,ARTIPRI2,ARFLUNIV,ARNUMPRE,ARPRESTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODART = NVL(_Link_.ARCODART,space(20))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_PREZUM = NVL(_Link_.ARPREZUM,space(1))
      this.w_ARTIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_FLSPAN = NVL(_Link_.ARFLSPAN,space(10))
      this.w_ARTIPRIG = NVL(_Link_.ARTIPRIG,space(1))
      this.w_ARTIPRI2 = NVL(_Link_.ARTIPRI2,space(1))
      this.w_TIPRIGCA = NVL(_Link_.ARTIPRIG,space(1))
      this.w_TIPRI2CA = NVL(_Link_.ARTIPRI2,space(1))
      this.w_FLUNIV = NVL(_Link_.ARFLUNIV,space(1))
      this.w_NUMPRE = NVL(_Link_.ARNUMPRE,0)
      this.w_ARPRESTA = NVL(_Link_.ARPRESTA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACODART = space(20)
      endif
      this.w_OPERAT = space(1)
      this.w_FLUSEP = space(1)
      this.w_MOLTIP = 0
      this.w_PREZUM = space(1)
      this.w_ARTIPART = space(2)
      this.w_FLSPAN = space(10)
      this.w_ARTIPRIG = space(1)
      this.w_ARTIPRI2 = space(1)
      this.w_TIPRIGCA = space(1)
      this.w_TIPRI2CA = space(1)
      this.w_FLUNIV = space(1)
      this.w_NUMPRE = 0
      this.w_ARPRESTA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ1 = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_FLFRAZ1 = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COD1ATT
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD1ATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD1ATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARSTACOD,ARFLSPAN,ARFLUNIV,ARNUMPRE,ARPRESTA";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_COD1ATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_COD1ATT)
            select ARCODART,ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARSTACOD,ARFLSPAN,ARFLUNIV,ARNUMPRE,ARPRESTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD1ATT = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_NOTIFICA = NVL(_Link_.ARSTACOD,space(1))
      this.w_FLSPAN = NVL(_Link_.ARFLSPAN,space(10))
      this.w_FLUNIV = NVL(_Link_.ARFLUNIV,space(1))
      this.w_NUMPRE = NVL(_Link_.ARNUMPRE,0)
      this.w_ARPRESTA = NVL(_Link_.ARPRESTA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COD1ATT = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_TIPART = space(2)
      this.w_NOTIFICA = space(1)
      this.w_FLSPAN = space(10)
      this.w_FLUNIV = space(1)
      this.w_NUMPRE = 0
      this.w_ARPRESTA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD1ATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAUNIMIS
  func Link_2_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_DAUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_DAUNIMIS))
          select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oDAUNIMIS_2_34'),i_cWhere,'GSAR_AUM',"Unita di misura",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_DAUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_DAUNIMIS)
            select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_DESUNI = NVL(_Link_.UMDESCRI,space(35))
      this.w_CHKTEMP = NVL(_Link_.UMFLTEMP,space(1))
      this.w_DUR_ORE = NVL(_Link_.UMDURORE,0)
      this.w_FL_FRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DAUNIMIS = space(3)
      endif
      this.w_DESUNI = space(35)
      this.w_CHKTEMP = space(1)
      this.w_DUR_ORE = 0
      this.w_FL_FRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSERG='S' OR .w_DAUNIMIS=.w_UNMIS1 OR .w_DAUNIMIS=.w_UNMIS2 OR .w_DAUNIMIS=.w_UNMIS3
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire l'unit� di misura principale o secondaria della prestazione")
        endif
        this.w_DAUNIMIS = space(3)
        this.w_DESUNI = space(35)
        this.w_CHKTEMP = space(1)
        this.w_DUR_ORE = 0
        this.w_FL_FRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_34(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_34.UMCODICE as UMCODICE234"+ ",link_2_34.UMDESCRI as UMDESCRI234"+ ",link_2_34.UMFLTEMP as UMFLTEMP234"+ ",link_2_34.UMDURORE as UMDURORE234"+ ",link_2_34.UMFLFRAZ as UMFLFRAZ234"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_34 on OFFDATTI.DAUNIMIS=link_2_34.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_34"
          i_cKey=i_cKey+'+" and OFFDATTI.DAUNIMIS=link_2_34.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DACENCOS
  func Link_2_89(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_DACENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_DACENCOS)
            select CC_CONTO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACENCOS = NVL(_Link_.CC_CONTO,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_DACENCOS = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACONTRA
  func Link_2_134(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,COTIPCON,COCODCON";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_DACONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_DACONTRA)
            select COSERIAL,COTIPCON,COCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACONTRA = NVL(_Link_.COSERIAL,space(10))
      this.w_COTIPCON = NVL(_Link_.COTIPCON,space(1))
      this.w_COCODCON = NVL(_Link_.COCODCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_DACONTRA = space(10)
      endif
      this.w_COTIPCON = space(1)
      this.w_COCODCON = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_134(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CON_TRAS_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_134.COSERIAL as COSERIAL334"+ ",link_2_134.COTIPCON as COTIPCON334"+ ",link_2_134.COCODCON as COCODCON334"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_134 on OFFDATTI.DACONTRA=link_2_134.COSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_134"
          i_cKey=i_cKey+'+" and OFFDATTI.DACONTRA=link_2_134.COSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESRIS_2_33.value==this.w_DESRIS)
      this.oPgFrm.Page1.oPag.oDESRIS_2_33.value=this.w_DESRIS
      replace t_DESRIS with this.oPgFrm.Page1.oPag.oDESRIS_2_33.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDADESAGG_2_38.value==this.w_DADESAGG)
      this.oPgFrm.Page1.oPag.oDADESAGG_2_38.value=this.w_DADESAGG
      replace t_DADESAGG with this.oPgFrm.Page1.oPag.oDADESAGG_2_38.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODOPE_2_39.value==this.w_DACODOPE)
      this.oPgFrm.Page1.oPag.oDACODOPE_2_39.value=this.w_DACODOPE
      replace t_DACODOPE with this.oPgFrm.Page1.oPag.oDACODOPE_2_39.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATMOD_2_40.value==this.w_DADATMOD)
      this.oPgFrm.Page1.oPag.oDADATMOD_2_40.value=this.w_DADATMOD
      replace t_DADATMOD with this.oPgFrm.Page1.oPag.oDADATMOD_2_40.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDAVALRIG_2_42.value==this.w_DAVALRIG)
      this.oPgFrm.Page1.oPag.oDAVALRIG_2_42.value=this.w_DAVALRIG
      replace t_DAVALRIG with this.oPgFrm.Page1.oPag.oDAVALRIG_2_42.value
    endif
    if not(this.oPgFrm.Page1.oPag.oATIPRIG_2_43.RadioValue()==this.w_ATIPRIG)
      this.oPgFrm.Page1.oPag.oATIPRIG_2_43.SetRadio()
      replace t_ATIPRIG with this.oPgFrm.Page1.oPag.oATIPRIG_2_43.value
    endif
    if not(this.oPgFrm.Page1.oPag.oETIPRIG_2_44.RadioValue()==this.w_ETIPRIG)
      this.oPgFrm.Page1.oPag.oETIPRIG_2_44.SetRadio()
      replace t_ETIPRIG with this.oPgFrm.Page1.oPag.oETIPRIG_2_44.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRTIPRIG_2_45.RadioValue()==this.w_RTIPRIG)
      this.oPgFrm.Page1.oPag.oRTIPRIG_2_45.SetRadio()
      replace t_RTIPRIG with this.oPgFrm.Page1.oPag.oRTIPRIG_2_45.value
    endif
    if not(this.oPgFrm.Page1.oPag.oATIPRI2_2_46.RadioValue()==this.w_ATIPRI2)
      this.oPgFrm.Page1.oPag.oATIPRI2_2_46.SetRadio()
      replace t_ATIPRI2 with this.oPgFrm.Page1.oPag.oATIPRI2_2_46.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDAOREEFF_2_48.value==this.w_DAOREEFF)
      this.oPgFrm.Page1.oPag.oDAOREEFF_2_48.value=this.w_DAOREEFF
      replace t_DAOREEFF with this.oPgFrm.Page1.oPag.oDAOREEFF_2_48.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDAMINEFF_2_49.value==this.w_DAMINEFF)
      this.oPgFrm.Page1.oPag.oDAMINEFF_2_49.value=this.w_DAMINEFF
      replace t_DAMINEFF with this.oPgFrm.Page1.oPag.oDAMINEFF_2_49.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDACOSUNI_2_54.value==this.w_DACOSUNI)
      this.oPgFrm.Page1.oPag.oDACOSUNI_2_54.value=this.w_DACOSUNI
      replace t_DACOSUNI with this.oPgFrm.Page1.oPag.oDACOSUNI_2_54.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDACOSINT_2_56.value==this.w_DACOSINT)
      this.oPgFrm.Page1.oPag.oDACOSINT_2_56.value=this.w_DACOSINT
      replace t_DACOSINT with this.oPgFrm.Page1.oPag.oDACOSINT_2_56.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUNI_2_63.value==this.w_DESUNI)
      this.oPgFrm.Page1.oPag.oDESUNI_2_63.value=this.w_DESUNI
      replace t_DESUNI with this.oPgFrm.Page1.oPag.oDESUNI_2_63.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPREMIN_2_68.value==this.w_DAPREMIN)
      this.oPgFrm.Page1.oPag.oDAPREMIN_2_68.value=this.w_DAPREMIN
      replace t_DAPREMIN with this.oPgFrm.Page1.oPag.oDAPREMIN_2_68.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPREMAX_2_69.value==this.w_DAPREMAX)
      this.oPgFrm.Page1.oPag.oDAPREMAX_2_69.value=this.w_DAPREMAX
      replace t_DAPREMAX with this.oPgFrm.Page1.oPag.oDAPREMAX_2_69.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDAGAZUFF_2_70.value==this.w_DAGAZUFF)
      this.oPgFrm.Page1.oPag.oDAGAZUFF_2_70.value=this.w_DAGAZUFF
      replace t_DAGAZUFF with this.oPgFrm.Page1.oPag.oDAGAZUFF_2_70.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_RESP_2_71.value==this.w_DENOM_RESP)
      this.oPgFrm.Page1.oPag.oDENOM_RESP_2_71.value=this.w_DENOM_RESP
      replace t_DENOM_RESP with this.oPgFrm.Page1.oPag.oDENOM_RESP_2_71.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODRES_2_1.value==this.w_DACODRES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODRES_2_1.value=this.w_DACODRES
      replace t_DACODRES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODRES_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_3.value==this.w_DACODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_3.value=this.w_DACODICE
      replace t_DACODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_5.value==this.w_DACODATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_5.value=this.w_DACODATT
      replace t_DACODATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDADESATT_2_32.value==this.w_DADESATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDADESATT_2_32.value=this.w_DADESATT
      replace t_DADESATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDADESATT_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAUNIMIS_2_34.value==this.w_DAUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAUNIMIS_2_34.value=this.w_DAUNIMIS
      replace t_DAUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAUNIMIS_2_34.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAQTAMOV_2_36.value==this.w_DAQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAQTAMOV_2_36.value=this.w_DAQTAMOV
      replace t_DAQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAQTAMOV_2_36.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAPREZZO_2_37.value==this.w_DAPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAPREZZO_2_37.value=this.w_DAPREZZO
      replace t_DAPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAPREZZO_2_37.value
    endif
    cp_SetControlsValueExtFlds(this,'OFFDATTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(((.w_ISALT AND .w_TIPRIS='P') OR (! .w_ISALT AND .w_TIPRIS $ 'P-R' )) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)) and not(empty(.w_DACODRES)) and (not(Empty(.w_CPROWORD)) and (not(Empty(.w_DACODATT)) or not(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODRES_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Attenzione tipo risorsa incongruente o obsoleta")
        case   not(!.w_ISAHE or ChkTipArt(.w_ECACODART, "FM-FO-DE") AND (EMPTY(.w_ECADTOBSO) OR .w_ECADTOBSO>=i_datsys)) and not(empty(.w_DACODICE)) and (not(Empty(.w_CPROWORD)) and (not(Empty(.w_DACODATT)) or not(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        case   not(.w_ISAHE or ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys)) and (.w_DARIGPRE<>'S') and not(empty(.w_DACODATT)) and (not(Empty(.w_CPROWORD)) and (not(Empty(.w_DACODATT)) or not(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        case   not(.w_FLSERG='S' OR .w_DAUNIMIS=.w_UNMIS1 OR .w_DAUNIMIS=.w_UNMIS2 OR .w_DAUNIMIS=.w_UNMIS3) and (.w_TIPART = 'FM' AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S')) and not(empty(.w_DAUNIMIS)) and (not(Empty(.w_CPROWORD)) and (not(Empty(.w_DACODATT)) or not(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAUNIMIS_2_34
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire l'unit� di misura principale o secondaria della prestazione")
        case   not((.w_DAQTAMOV>0 AND (.w_FL_FRAZ<>'S' OR .w_DAQTAMOV=INT(.w_DAQTAMOV))) OR NOT .w_TIPSER $ 'RM') and (.w_TIPART = 'FM') and (not(Empty(.w_CPROWORD)) and (not(Empty(.w_DACODATT)) or not(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAQTAMOV_2_36
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
        case   not(.w_DAMINEFF < 60) and (not(Empty(.w_CPROWORD)) and (not(Empty(.w_DACODATT)) or not(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oDAMINEFF_2_49
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 0 e 59")
      endcase
      if not(Empty(.w_CPROWORD)) and (not(Empty(.w_DACODATT)) or not(Empty(.w_DACODICE)))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DASERIAL = this.w_DASERIAL
    this.o_LetturaParAlte = this.w_LetturaParAlte
    this.o_DACODRES = this.w_DACODRES
    this.o_DACODICE = this.w_DACODICE
    this.o_DACODATT = this.w_DACODATT
    this.o_DAUNIMIS = this.w_DAUNIMIS
    this.o_DAQTAMOV = this.w_DAQTAMOV
    this.o_DAPREZZO = this.w_DAPREZZO
    this.o_ATIPRIG = this.w_ATIPRIG
    this.o_ETIPRIG = this.w_ETIPRIG
    this.o_RTIPRIG = this.w_RTIPRIG
    this.o_ATIPRI2 = this.w_ATIPRI2
    this.o_DAOREEFF = this.w_DAOREEFF
    this.o_DAMINEFF = this.w_DAMINEFF
    this.o_CENCOS = this.w_CENCOS
    this.o_DACOSUNI = this.w_DACOSUNI
    this.o_DCODCEN = this.w_DCODCEN
    this.o_DACENCOS = this.w_DACENCOS
    this.o_SERPER = this.w_SERPER
    this.o_DACENRIC = this.w_DACENRIC
    this.o_ETIPRI2 = this.w_ETIPRI2
    this.o_RTIPRI2 = this.w_RTIPRI2
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) and (not(Empty(t_DACODATT)) or not(Empty(t_DACODICE))))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_VOCECR=space(1)
      .w_DACODRES=space(5)
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_DACODICE=space(41)
      .w_RCACODART=space(20)
      .w_DACODATT=space(20)
      .w_CACODART=space(20)
      .w_ARTIPRIG=space(1)
      .w_ARTIPRI2=space(1)
      .w_CATIPRIG=space(1)
      .w_CATIPRI2=space(1)
      .w_DATIPRIG=space(1)
      .w_DATIPRI2=space(1)
      .w_UNMIS1=space(3)
      .w_EMOLTI3=0
      .w_RMOLTI3=0
      .w_FLFRAZ1=space(1)
      .w_MOLTI3=0
      .w_EOPERA3=space(1)
      .w_ROPERA3=space(1)
      .w_MODUM2=space(1)
      .w_OPERA3=space(1)
      .w_OPERAT=space(1)
      .w_ECOD1ATT=space(20)
      .w_RCOD1ATT=space(20)
      .w_PREZUM=space(1)
      .w_EDESAGG=space(0)
      .w_RDESAGG=space(0)
      .w_EDESATT=space(40)
      .w_RDESATT=space(40)
      .w_COD1ATT=space(20)
      .w_DADESATT=space(40)
      .w_DESRIS=space(39)
      .w_DAUNIMIS=space(3)
      .w_TIPART=space(2)
      .w_DAQTAMOV=0
      .w_DAPREZZO=0
      .w_DADESAGG=space(0)
      .w_DACODOPE=0
      .w_DADATMOD=ctod("  /  /  ")
      .w_DTIPRIG=space(10)
      .w_DAVALRIG=0
      .w_ATIPRIG=space(1)
      .w_ETIPRIG=space(10)
      .w_RTIPRIG=space(10)
      .w_ATIPRI2=space(1)
      .w_COST_ORA=0
      .w_DAOREEFF=0
      .w_DAMINEFF=0
      .w_LICOSTO=0
      .w_DACOSUNI=0
      .w_DCODCEN=space(15)
      .w_DACOSINT=0
      .w_FLRESP=space(1)
      .w_UNMIS2=space(3)
      .w_RUNMIS3=space(3)
      .w_ECACODART=space(20)
      .w_FLSERG=space(1)
      .w_UNMIS3=space(3)
      .w_DESUNI=space(35)
      .w_DAFLDEFF=space(1)
      .w_QTAUM1=0
      .w_COGNOME=space(40)
      .w_NOME=space(40)
      .w_DAPREMIN=0
      .w_DAPREMAX=0
      .w_DAGAZUFF=space(6)
      .w_DENOM_RESP=space(39)
      .w_NOTIFICA=space(1)
      .w_DARIFRIG=0
      .w_CHKTEMP=space(1)
      .w_DUR_ORE=0
      .w_ECADTOBSO=ctod("  /  /  ")
      .w_RCADTOBSO=ctod("  /  /  ")
      .w_CADTOBSO=ctod("  /  /  ")
      .w_DACODCOM=space(15)
      .w_DAVOCRIC=space(15)
      .w_DAVOCCOS=space(15)
      .w_DAINICOM=ctod("  /  /  ")
      .w_DAFINCOM=ctod("  /  /  ")
      .w_DA_SEGNO=space(1)
      .w_DACOMRIC=space(15)
      .w_DACENCOS=space(15)
      .w_DAATTIVI=space(15)
      .w_ETIPSER=space(1)
      .w_RTIPSER=space(1)
      .w_SERPER=space(41)
      .w_TIPSER=space(1)
      .w_TIPRIS=space(1)
      .w_FLUSEP=space(1)
      .w_MOLTIP=0
      .w_ARTIPART=space(2)
      .w_EUNMIS3=space(3)
      .w_DACODLIS=space(5)
      .w_DAPROLIS=space(5)
      .w_DAPROSCO=space(5)
      .w_DASCOLIS=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DACENRIC=space(15)
      .w_DAATTRIC=space(15)
      .w_DASCONT1=0
      .w_DASCONT2=0
      .w_DASCONT3=0
      .w_DASCONT4=0
      .w_FL_FRAZ=space(1)
      .w_DALISACQ=space(5)
      .w_DATCOINI=ctod("  /  /  ")
      .w_DATCOFIN=ctod("  /  /  ")
      .w_DATRIINI=ctod("  /  /  ")
      .w_DATRIFIN=ctod("  /  /  ")
      .w_FLSCOR=space(1)
      .w_CEN_CauDoc=space(15)
      .w_DARIFPRE=0
      .w_DARIGPRE=space(1)
      .w_FLSPAN=space(10)
      .w_DARIFPRE=0
      .w_DARIGPRE=space(1)
      .w_DACONCOD=space(15)
      .w_DACONTRA=space(10)
      .w_DACODMOD=space(10)
      .w_DACODIMP=space(10)
      .w_DACOCOMP=0
      .w_DARINNOV=0
      .w_FLUNIV=space(1)
      .w_NUMPRE=0
      .w_ARPRESTA=space(1)
      .w_COTIPCON=space(1)
      .w_COCODCON=space(15)
      .w_ETIPRI2=space(10)
      .w_RTIPRI2=space(10)
      .w_TIPRIGCA=space(1)
      .w_TIPRI2CA=space(1)
      .w_CHKDATAPRE=space(1)
      .w_GG_PRE=0
      .w_GG_SUC=0
      .w_DATFIN=ctod("  /  /  ")
      .w_OLD_ARPRESTA=space(1)
      .w_OKTARCON=space(1)
      .DoRTCalc(1,29,.f.)
        .w_VOCECR = this.oparentobject.w_VOCECR
      .DoRTCalc(31,37,.f.)
      if not(empty(.w_DACODRES))
        .link_2_1('Full')
      endif
      .DoRTCalc(38,38,.f.)
        .w_DACODICE = IIF(.w_ISAHE,IIF(EMPTY(.w_DACODICE), .w_SERPER, .w_DACODICE), SPACE(41))
      .DoRTCalc(39,39,.f.)
      if not(empty(.w_DACODICE))
        .link_2_3('Full')
      endif
      .DoRTCalc(40,40,.f.)
        .w_DACODATT = IIF(!.w_ISAHE, IIF(EMPTY(.w_DACODATT), .w_SERPER, .w_DACODATT), SPACE(20))
      .DoRTCalc(41,41,.f.)
      if not(empty(.w_DACODATT))
        .link_2_5('Full')
      endif
        .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
      .DoRTCalc(42,42,.f.)
      if not(empty(.w_CACODART))
        .link_2_6('Full')
      endif
      .DoRTCalc(43,49,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_14('Full')
      endif
      .DoRTCalc(50,52,.f.)
        .w_MOLTI3 = IIF(.w_ISAHE,.w_EMOLTI3,.w_RMOLTI3)
      .DoRTCalc(54,56,.f.)
        .w_OPERA3 = IIF(.w_ISAHE,.w_EOPERA3,.w_ROPERA3)
      .DoRTCalc(58,65,.f.)
        .w_COD1ATT = IIF(.w_ISAHE,.w_ECOD1ATT,.w_RCOD1ATT)
      .DoRTCalc(66,66,.f.)
      if not(empty(.w_COD1ATT))
        .link_2_31('Full')
      endif
        .w_DADESATT = IIF(.w_ISAHE, .w_EDESATT, .w_RDESATT)
      .DoRTCalc(68,68,.f.)
        .w_DAUNIMIS = .w_UNMIS1
      .DoRTCalc(69,69,.f.)
      if not(empty(.w_DAUNIMIS))
        .link_2_34('Full')
      endif
      .DoRTCalc(70,70,.f.)
        .w_DAQTAMOV = IIF(.w_TIPART='DE', 0, IIF(.w_DAQTAMOV>0,.w_DAQTAMOV,1))
      .DoRTCalc(72,72,.f.)
        .w_DADESAGG = IIF(.w_ISAHE, .w_EDESAGG, .w_RDESAGG)
        .w_DACODOPE = i_codute
        .w_DADATMOD = i_datsys
      .DoRTCalc(76,76,.f.)
        .w_DAVALRIG = IIF(UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE', CAVALRIG(.w_DAPREZZO,.w_DAQTAMOV, .w_DASCONT1,.w_DASCONT2,.w_DASCONT3,.w_DASCONT4,IIF(.w_NOTIFICA='S' AND .w_ISALT, 0, this.oParentObject.w_DECTOT)), cp_ROUND(.w_DAQTAMOV*.w_DAPREZZO, this.oParentObject.w_DECTOT))
        .w_ATIPRIG = ICASE(.w_ISAHE,.w_ETIPRIG,.w_Isahr,.w_RTIPRIG,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N', Not empty(.w_DATIPRIG),.w_DATIPRIG,!EMPTY(.w_CATIPRIG),.w_CATIPRIG,.w_ARTIPRIG))
        .w_ETIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(!EMPTY(.w_CATIPRIG),.w_CATIPRIG, Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_ATIPRI2 = ICASE(.w_Isahe,.w_ETIPRI2,.w_Isahr,.w_RTIPRI2,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', Not empty(.w_DATIPRI2),.w_DATIPRI2,!EMPTY(.w_CATIPRI2),.w_CATIPRI2,.w_ARTIPRI2))
      .DoRTCalc(82,90,.f.)
        .w_DACOSINT = IIF(.w_LICOSTO>0 and .w_COST_ORA=0 ,.w_DACOSUNI*.w_DAQTAMOV,(.w_DAOREEFF+(.w_DAMINEFF/60))*.w_DACOSUNI)
      .DoRTCalc(92,96,.f.)
        .w_UNMIS3 = IIF(.w_ISAHE,.w_EUNMIS3,.w_RUNMIS3)
      .DoRTCalc(98,99,.f.)
        .w_QTAUM1 = IIF(.w_DATIPRIG='F', 1, CALQTA(.w_DAQTAMOV,.w_DAUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ', .w_UNMIS3, .w_OPERA3, .w_MOLTI3))
      .DoRTCalc(101,105,.f.)
        .w_DENOM_RESP = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_73.Calculate()
      .DoRTCalc(107,112,.f.)
        .w_CADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
      .DoRTCalc(114,114,.f.)
        .w_DAVOCRIC = SearchVoc(this,'R',.w_COD1ATT,this.oparentobject.w_ATTIPCLI,this.oparentobject.w_CODCLI,this.oparentobject.w_ATCAUDOC,this.oparentobject.w_ATCODNOM)
        .w_DAVOCCOS = SearchVoc(this,'C',.w_COD1ATT,this.oparentobject.w_ATTIPCLI,this.oparentobject.w_CODCLI,this.oparentobject.w_ATCAUDOC,this.oparentobject.w_ATCODNOM)
        .w_DAINICOM = this.oparentobject.w_ATINIRIC
        .w_DAFINCOM = this.oparentobject.w_ATFINRIC
        .w_DA_SEGNO = 'D'
        .w_DACOMRIC = .w_OLDCOMRIC
      .DoRTCalc(121,121,.f.)
      if not(empty(.w_DACENCOS))
        .link_2_89('Full')
      endif
        .w_DAATTIVI = .w_OLDATTC
      .DoRTCalc(123,125,.f.)
        .w_TIPSER = IIF(.w_ISAHE,.w_ETIPSER,.w_RTIPSER)
      .DoRTCalc(127,137,.f.)
        .w_DAATTRIC = .w_OLDATTR
      .DoRTCalc(139,144,.f.)
        .w_DATCOINI = this.oparentobject.w_ATINIRIC
        .w_DATCOFIN = this.oparentobject.w_ATFINRIC
        .w_DATRIINI = this.oparentobject.w_ATINICOS
        .w_DATRIFIN = this.oparentobject.w_ATFINCOS
      .DoRTCalc(149,149,.f.)
        .w_FLSCOR = THIS.OPARENTOBJECT.w_FLSCOR
      .DoRTCalc(151,152,.f.)
        .w_DARIGPRE = 'N'
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_128.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_129.Calculate()
      .DoRTCalc(154,158,.f.)
      if not(empty(.w_DACONTRA))
        .link_2_134('Full')
      endif
      .DoRTCalc(159,169,.f.)
        .w_ETIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,'D')
      .DoRTCalc(172,178,.f.)
        .w_DATFIN = this.oparentobject.w_DATFIN
      .DoRTCalc(180,180,.f.)
        .w_OLD_ARPRESTA = .w_ARPRESTA
    endwith
    this.DoRTCalc(182,182,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_VOCECR = t_VOCECR
    this.w_DACODRES = t_DACODRES
    this.w_CPROWORD = t_CPROWORD
    this.w_DACODICE = t_DACODICE
    this.w_RCACODART = t_RCACODART
    this.w_DACODATT = t_DACODATT
    this.w_CACODART = t_CACODART
    this.w_ARTIPRIG = t_ARTIPRIG
    this.w_ARTIPRI2 = t_ARTIPRI2
    this.w_CATIPRIG = t_CATIPRIG
    this.w_CATIPRI2 = t_CATIPRI2
    this.w_DATIPRIG = t_DATIPRIG
    this.w_DATIPRI2 = t_DATIPRI2
    this.w_UNMIS1 = t_UNMIS1
    this.w_EMOLTI3 = t_EMOLTI3
    this.w_RMOLTI3 = t_RMOLTI3
    this.w_FLFRAZ1 = t_FLFRAZ1
    this.w_MOLTI3 = t_MOLTI3
    this.w_EOPERA3 = t_EOPERA3
    this.w_ROPERA3 = t_ROPERA3
    this.w_MODUM2 = t_MODUM2
    this.w_OPERA3 = t_OPERA3
    this.w_OPERAT = t_OPERAT
    this.w_ECOD1ATT = t_ECOD1ATT
    this.w_RCOD1ATT = t_RCOD1ATT
    this.w_PREZUM = t_PREZUM
    this.w_EDESAGG = t_EDESAGG
    this.w_RDESAGG = t_RDESAGG
    this.w_EDESATT = t_EDESATT
    this.w_RDESATT = t_RDESATT
    this.w_COD1ATT = t_COD1ATT
    this.w_DADESATT = t_DADESATT
    this.w_DESRIS = t_DESRIS
    this.w_DAUNIMIS = t_DAUNIMIS
    this.w_TIPART = t_TIPART
    this.w_DAQTAMOV = t_DAQTAMOV
    this.w_DAPREZZO = t_DAPREZZO
    this.w_DADESAGG = t_DADESAGG
    this.w_DACODOPE = t_DACODOPE
    this.w_DADATMOD = t_DADATMOD
    this.w_DTIPRIG = t_DTIPRIG
    this.w_DAVALRIG = t_DAVALRIG
    this.w_ATIPRIG = this.oPgFrm.Page1.oPag.oATIPRIG_2_43.RadioValue(.t.)
    this.w_ETIPRIG = this.oPgFrm.Page1.oPag.oETIPRIG_2_44.RadioValue(.t.)
    this.w_RTIPRIG = this.oPgFrm.Page1.oPag.oRTIPRIG_2_45.RadioValue(.t.)
    this.w_ATIPRI2 = this.oPgFrm.Page1.oPag.oATIPRI2_2_46.RadioValue(.t.)
    this.w_COST_ORA = t_COST_ORA
    this.w_DAOREEFF = t_DAOREEFF
    this.w_DAMINEFF = t_DAMINEFF
    this.w_LICOSTO = t_LICOSTO
    this.w_DACOSUNI = t_DACOSUNI
    this.w_DCODCEN = t_DCODCEN
    this.w_DACOSINT = t_DACOSINT
    this.w_FLRESP = t_FLRESP
    this.w_UNMIS2 = t_UNMIS2
    this.w_RUNMIS3 = t_RUNMIS3
    this.w_ECACODART = t_ECACODART
    this.w_FLSERG = t_FLSERG
    this.w_UNMIS3 = t_UNMIS3
    this.w_DESUNI = t_DESUNI
    this.w_DAFLDEFF = t_DAFLDEFF
    this.w_QTAUM1 = t_QTAUM1
    this.w_COGNOME = t_COGNOME
    this.w_NOME = t_NOME
    this.w_DAPREMIN = t_DAPREMIN
    this.w_DAPREMAX = t_DAPREMAX
    this.w_DAGAZUFF = t_DAGAZUFF
    this.w_DENOM_RESP = t_DENOM_RESP
    this.w_NOTIFICA = t_NOTIFICA
    this.w_DARIFRIG = t_DARIFRIG
    this.w_CHKTEMP = t_CHKTEMP
    this.w_DUR_ORE = t_DUR_ORE
    this.w_ECADTOBSO = t_ECADTOBSO
    this.w_RCADTOBSO = t_RCADTOBSO
    this.w_CADTOBSO = t_CADTOBSO
    this.w_DACODCOM = t_DACODCOM
    this.w_DAVOCRIC = t_DAVOCRIC
    this.w_DAVOCCOS = t_DAVOCCOS
    this.w_DAINICOM = t_DAINICOM
    this.w_DAFINCOM = t_DAFINCOM
    this.w_DA_SEGNO = t_DA_SEGNO
    this.w_DACOMRIC = t_DACOMRIC
    this.w_DACENCOS = t_DACENCOS
    this.w_DAATTIVI = t_DAATTIVI
    this.w_ETIPSER = t_ETIPSER
    this.w_RTIPSER = t_RTIPSER
    this.w_SERPER = t_SERPER
    this.w_TIPSER = t_TIPSER
    this.w_TIPRIS = t_TIPRIS
    this.w_FLUSEP = t_FLUSEP
    this.w_MOLTIP = t_MOLTIP
    this.w_ARTIPART = t_ARTIPART
    this.w_EUNMIS3 = t_EUNMIS3
    this.w_DACODLIS = t_DACODLIS
    this.w_DAPROLIS = t_DAPROLIS
    this.w_DAPROSCO = t_DAPROSCO
    this.w_DASCOLIS = t_DASCOLIS
    this.w_DATOBSO = t_DATOBSO
    this.w_DACENRIC = t_DACENRIC
    this.w_DAATTRIC = t_DAATTRIC
    this.w_DASCONT1 = t_DASCONT1
    this.w_DASCONT2 = t_DASCONT2
    this.w_DASCONT3 = t_DASCONT3
    this.w_DASCONT4 = t_DASCONT4
    this.w_FL_FRAZ = t_FL_FRAZ
    this.w_DALISACQ = t_DALISACQ
    this.w_DATCOINI = t_DATCOINI
    this.w_DATCOFIN = t_DATCOFIN
    this.w_DATRIINI = t_DATRIINI
    this.w_DATRIFIN = t_DATRIFIN
    this.w_FLSCOR = t_FLSCOR
    this.w_CEN_CauDoc = t_CEN_CauDoc
    this.w_DARIFPRE = t_DARIFPRE
    this.w_DARIGPRE = t_DARIGPRE
    this.w_FLSPAN = t_FLSPAN
    this.w_DARIFPRE = t_DARIFPRE
    this.w_DARIGPRE = t_DARIGPRE
    this.w_DACONCOD = t_DACONCOD
    this.w_DACONTRA = t_DACONTRA
    this.w_DACODMOD = t_DACODMOD
    this.w_DACODIMP = t_DACODIMP
    this.w_DACOCOMP = t_DACOCOMP
    this.w_DARINNOV = t_DARINNOV
    this.w_FLUNIV = t_FLUNIV
    this.w_NUMPRE = t_NUMPRE
    this.w_ARPRESTA = t_ARPRESTA
    this.w_COTIPCON = t_COTIPCON
    this.w_COCODCON = t_COCODCON
    this.w_ETIPRI2 = t_ETIPRI2
    this.w_RTIPRI2 = t_RTIPRI2
    this.w_TIPRIGCA = t_TIPRIGCA
    this.w_TIPRI2CA = t_TIPRI2CA
    this.w_CHKDATAPRE = t_CHKDATAPRE
    this.w_GG_PRE = t_GG_PRE
    this.w_GG_SUC = t_GG_SUC
    this.w_DATFIN = t_DATFIN
    this.w_OLD_ARPRESTA = t_OLD_ARPRESTA
    this.w_OKTARCON = t_OKTARCON
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_VOCECR with this.w_VOCECR
    replace t_DACODRES with this.w_DACODRES
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DACODICE with this.w_DACODICE
    replace t_RCACODART with this.w_RCACODART
    replace t_DACODATT with this.w_DACODATT
    replace t_CACODART with this.w_CACODART
    replace t_ARTIPRIG with this.w_ARTIPRIG
    replace t_ARTIPRI2 with this.w_ARTIPRI2
    replace t_CATIPRIG with this.w_CATIPRIG
    replace t_CATIPRI2 with this.w_CATIPRI2
    replace t_DATIPRIG with this.w_DATIPRIG
    replace t_DATIPRI2 with this.w_DATIPRI2
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_EMOLTI3 with this.w_EMOLTI3
    replace t_RMOLTI3 with this.w_RMOLTI3
    replace t_FLFRAZ1 with this.w_FLFRAZ1
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_EOPERA3 with this.w_EOPERA3
    replace t_ROPERA3 with this.w_ROPERA3
    replace t_MODUM2 with this.w_MODUM2
    replace t_OPERA3 with this.w_OPERA3
    replace t_OPERAT with this.w_OPERAT
    replace t_ECOD1ATT with this.w_ECOD1ATT
    replace t_RCOD1ATT with this.w_RCOD1ATT
    replace t_PREZUM with this.w_PREZUM
    replace t_EDESAGG with this.w_EDESAGG
    replace t_RDESAGG with this.w_RDESAGG
    replace t_EDESATT with this.w_EDESATT
    replace t_RDESATT with this.w_RDESATT
    replace t_COD1ATT with this.w_COD1ATT
    replace t_DADESATT with this.w_DADESATT
    replace t_DESRIS with this.w_DESRIS
    replace t_DAUNIMIS with this.w_DAUNIMIS
    replace t_TIPART with this.w_TIPART
    replace t_DAQTAMOV with this.w_DAQTAMOV
    replace t_DAPREZZO with this.w_DAPREZZO
    replace t_DADESAGG with this.w_DADESAGG
    replace t_DACODOPE with this.w_DACODOPE
    replace t_DADATMOD with this.w_DADATMOD
    replace t_DTIPRIG with this.w_DTIPRIG
    replace t_DAVALRIG with this.w_DAVALRIG
    replace t_ATIPRIG with this.oPgFrm.Page1.oPag.oATIPRIG_2_43.ToRadio()
    replace t_ETIPRIG with this.oPgFrm.Page1.oPag.oETIPRIG_2_44.ToRadio()
    replace t_RTIPRIG with this.oPgFrm.Page1.oPag.oRTIPRIG_2_45.ToRadio()
    replace t_ATIPRI2 with this.oPgFrm.Page1.oPag.oATIPRI2_2_46.ToRadio()
    replace t_COST_ORA with this.w_COST_ORA
    replace t_DAOREEFF with this.w_DAOREEFF
    replace t_DAMINEFF with this.w_DAMINEFF
    replace t_LICOSTO with this.w_LICOSTO
    replace t_DACOSUNI with this.w_DACOSUNI
    replace t_DCODCEN with this.w_DCODCEN
    replace t_DACOSINT with this.w_DACOSINT
    replace t_FLRESP with this.w_FLRESP
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_RUNMIS3 with this.w_RUNMIS3
    replace t_ECACODART with this.w_ECACODART
    replace t_FLSERG with this.w_FLSERG
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_DESUNI with this.w_DESUNI
    replace t_DAFLDEFF with this.w_DAFLDEFF
    replace t_QTAUM1 with this.w_QTAUM1
    replace t_COGNOME with this.w_COGNOME
    replace t_NOME with this.w_NOME
    replace t_DAPREMIN with this.w_DAPREMIN
    replace t_DAPREMAX with this.w_DAPREMAX
    replace t_DAGAZUFF with this.w_DAGAZUFF
    replace t_DENOM_RESP with this.w_DENOM_RESP
    replace t_NOTIFICA with this.w_NOTIFICA
    replace t_DARIFRIG with this.w_DARIFRIG
    replace t_CHKTEMP with this.w_CHKTEMP
    replace t_DUR_ORE with this.w_DUR_ORE
    replace t_ECADTOBSO with this.w_ECADTOBSO
    replace t_RCADTOBSO with this.w_RCADTOBSO
    replace t_CADTOBSO with this.w_CADTOBSO
    replace t_DACODCOM with this.w_DACODCOM
    replace t_DAVOCRIC with this.w_DAVOCRIC
    replace t_DAVOCCOS with this.w_DAVOCCOS
    replace t_DAINICOM with this.w_DAINICOM
    replace t_DAFINCOM with this.w_DAFINCOM
    replace t_DA_SEGNO with this.w_DA_SEGNO
    replace t_DACOMRIC with this.w_DACOMRIC
    replace t_DACENCOS with this.w_DACENCOS
    replace t_DAATTIVI with this.w_DAATTIVI
    replace t_ETIPSER with this.w_ETIPSER
    replace t_RTIPSER with this.w_RTIPSER
    replace t_SERPER with this.w_SERPER
    replace t_TIPSER with this.w_TIPSER
    replace t_TIPRIS with this.w_TIPRIS
    replace t_FLUSEP with this.w_FLUSEP
    replace t_MOLTIP with this.w_MOLTIP
    replace t_ARTIPART with this.w_ARTIPART
    replace t_EUNMIS3 with this.w_EUNMIS3
    replace t_DACODLIS with this.w_DACODLIS
    replace t_DAPROLIS with this.w_DAPROLIS
    replace t_DAPROSCO with this.w_DAPROSCO
    replace t_DASCOLIS with this.w_DASCOLIS
    replace t_DATOBSO with this.w_DATOBSO
    replace t_DACENRIC with this.w_DACENRIC
    replace t_DAATTRIC with this.w_DAATTRIC
    replace t_DASCONT1 with this.w_DASCONT1
    replace t_DASCONT2 with this.w_DASCONT2
    replace t_DASCONT3 with this.w_DASCONT3
    replace t_DASCONT4 with this.w_DASCONT4
    replace t_FL_FRAZ with this.w_FL_FRAZ
    replace t_DALISACQ with this.w_DALISACQ
    replace t_DATCOINI with this.w_DATCOINI
    replace t_DATCOFIN with this.w_DATCOFIN
    replace t_DATRIINI with this.w_DATRIINI
    replace t_DATRIFIN with this.w_DATRIFIN
    replace t_FLSCOR with this.w_FLSCOR
    replace t_CEN_CauDoc with this.w_CEN_CauDoc
    replace t_DARIFPRE with this.w_DARIFPRE
    replace t_DARIGPRE with this.w_DARIGPRE
    replace t_FLSPAN with this.w_FLSPAN
    replace t_DARIFPRE with this.w_DARIFPRE
    replace t_DARIGPRE with this.w_DARIGPRE
    replace t_DACONCOD with this.w_DACONCOD
    replace t_DACONTRA with this.w_DACONTRA
    replace t_DACODMOD with this.w_DACODMOD
    replace t_DACODIMP with this.w_DACODIMP
    replace t_DACOCOMP with this.w_DACOCOMP
    replace t_DARINNOV with this.w_DARINNOV
    replace t_FLUNIV with this.w_FLUNIV
    replace t_NUMPRE with this.w_NUMPRE
    replace t_ARPRESTA with this.w_ARPRESTA
    replace t_COTIPCON with this.w_COTIPCON
    replace t_COCODCON with this.w_COCODCON
    replace t_ETIPRI2 with this.w_ETIPRI2
    replace t_RTIPRI2 with this.w_RTIPRI2
    replace t_TIPRIGCA with this.w_TIPRIGCA
    replace t_TIPRI2CA with this.w_TIPRI2CA
    replace t_CHKDATAPRE with this.w_CHKDATAPRE
    replace t_GG_PRE with this.w_GG_PRE
    replace t_GG_SUC with this.w_GG_SUC
    replace t_DATFIN with this.w_DATFIN
    replace t_OLD_ARPRESTA with this.w_OLD_ARPRESTA
    replace t_OKTARCON with this.w_OKTARCON
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mdaPag1 as StdContainer
  Width  = 773
  height = 362
  stdWidth  = 773
  stdheight = 362
  resizeXpos=338
  resizeYpos=38
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_17 as cp_runprogram with uid="EEJGFFKSMM",left=7, top=372, width=388,height=19,;
    caption='GSAG_BDE(PSTIN)',;
   bGlobalFont=.t.,;
    prg="GSAG_BDE('PSTIN')",;
    cEvent = "w_DACODATT Changed,w_DACODICE Changed",;
    nPag=1;
    , HelpContextID = 161823957


  add object oObj_1_18 as cp_runprogram with uid="ZDBNDAIRKT",left=7, top=392, width=211,height=19,;
    caption='gsag_bcd(1)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('1')",;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 60171850


  add object oObj_1_22 as cp_runprogram with uid="IHIDYBDKXP",left=7, top=412, width=211,height=19,;
    caption='gsag_bcd(2)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('2')",;
    cEvent = "CalcNot",;
    nPag=1;
    , HelpContextID = 60172106


  add object oObj_1_33 as cp_runprogram with uid="QBUJGZHVIR",left=7, top=432, width=211,height=19,;
    caption='gsag_bcd(5)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('5')",;
    cEvent = "w_DACODRES Changed",;
    nPag=1;
    , ToolTipText = "Aggiorna prezzo al cambio del partecipante solo su prezzi a zero";
    , HelpContextID = 60172874


  add object oObj_1_38 as cp_runprogram with uid="RNJNGBXQKZ",left=7, top=452, width=211,height=19,;
    caption='gsag_bcd(6)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('6')",;
    cEvent = "Agglis, Caltes",;
    nPag=1;
    , ToolTipText = "Aggiorna listini da dati di testata";
    , HelpContextID = 60173130


  add object oObj_1_39 as cp_runprogram with uid="VMEZHRHNWS",left=7, top=472, width=211,height=19,;
    caption='gsag_bcd(7)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('7')",;
    cEvent = "LisRig",;
    nPag=1;
    , ToolTipText = "Applico  listini al cambiare dei dati di riga";
    , HelpContextID = 60173386


  add object oObj_1_42 as cp_runprogram with uid="UVAYFKZPVF",left=7, top=492, width=211,height=19,;
    caption='gsag_bcd(8)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('8')",;
    cEvent = "LisAcq",;
    nPag=1;
    , ToolTipText = "Calcola costo interno da listino";
    , HelpContextID = 60173642


  add object oObj_1_53 as cp_runprogram with uid="MVURLMIJTA",left=7, top=512, width=211,height=19,;
    caption='gsag_bcd(9)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('9')",;
    cEvent = "Ricana",;
    nPag=1;
    , ToolTipText = "ricalcola analitica e prezzi al cambiare del nominativo";
    , HelpContextID = 60173898


  add object oObj_1_54 as cp_runprogram with uid="UDOXHXWUDO",left=7, top=532, width=211,height=19,;
    caption='gsag_bcd(0)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('0')",;
    cEvent = "AggPre",;
    nPag=1;
    , ToolTipText = "Aggiorna listini da dati di testata";
    , HelpContextID = 60171594


  add object oObj_1_57 as cp_runprogram with uid="MPZWJHUYRE",left=7, top=552, width=211,height=19,;
    caption='gsag_bcd(Z)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('Z')",;
    cEvent = "RicNoCont",;
    nPag=1;
    , ToolTipText = "Aggiorna listini da dati di testata";
    , HelpContextID = 60182346


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=12, top=0, width=747,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Riga",Field2="DACODRES",Label2="iif(w_ISALT, ah_MsgFormat('Resp.'), ah_MsgFormat('Part.'))",Field3="DACODICE",Label3="Codice",Field4="DACODATT",Label4="",Field5="DADESATT",Label5="Descrizione",Field6="DAUNIMIS",Label6="U.M.",Field7="DAQTAMOV",Label7="Quantit�",Field8="DAPREZZO",Label8="iif(w_ISALT, ah_MsgFormat('Tariffa'), ah_MsgFormat('Prezzo unitario'))",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235733626


  add object oObj_1_67 as cp_runprogram with uid="JKCIZJOJIN",left=268, top=663, width=388,height=19,;
    caption='GSAG_BDE(DISAB)',;
   bGlobalFont=.t.,;
    prg="GSAG_BDE('DISAB')",;
    cEvent = "w_DACODATT Changed,w_DACODICE Changed",;
    nPag=1;
    , HelpContextID = 103213269

  add object oStr_1_8 as StdString with uid="DPASEYXUXB",Visible=.t., Left=17, Top=167,;
    Alignment=0, Width=286, Height=18,;
    Caption="Descrizione aggiuntiva"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="IPXDPIQXPJ",Visible=.t., Left=5, Top=337,;
    Alignment=1, Width=84, Height=18,;
    Caption="Modificato da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="IFSNVFIKXY",Visible=.t., Left=147, Top=337,;
    Alignment=1, Width=31, Height=18,;
    Caption="Il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="YZMQBGQDPB",Visible=.t., Left=530, Top=119,;
    Alignment=1, Width=94, Height=18,;
    Caption="Importo di riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="HVIBWDTHAX",Visible=.t., Left=546, Top=310,;
    Alignment=1, Width=80, Height=18,;
    Caption="Costo interno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="ZOHGBHZIYU",Visible=.t., Left=158, Top=119,;
    Alignment=1, Width=107, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="KRFRWINQZV",Visible=.t., Left=363, Top=285,;
    Alignment=1, Width=99, Height=18,;
    Caption="Durata effettiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="PVSFOKOSXX",Visible=.t., Left=501, Top=285,;
    Alignment=0, Width=5, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="EVHOHPKEQZ",Visible=.t., Left=216, Top=141,;
    Alignment=1, Width=49, Height=18,;
    Caption="G. Uff.:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="VTCDBBUVLN",Visible=.t., Left=313, Top=337,;
    Alignment=1, Width=77, Height=18,;
    Caption="Tariffa min.:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (NOT .w_ISALT)
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="CSVDINHCKF",Visible=.t., Left=556, Top=337,;
    Alignment=1, Width=69, Height=18,;
    Caption="Tariffa max.:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (NOT .w_ISALT)
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="JFQIJNMQYQ",Visible=.t., Left=13, Top=285,;
    Alignment=1, Width=76, Height=18,;
    Caption="Descr. Part.:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="FWKMPZAUIZ",Visible=.t., Left=557, Top=141,;
    Alignment=1, Width=67, Height=18,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="NRSRHROQID",Visible=.t., Left=400, Top=368,;
    Alignment=0, Width=374, Height=21,;
    Caption="Attenzione lasciare link su DACENCOS per la presenza link analisi"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="XXMOFFHGMF",Visible=.t., Left=539, Top=285,;
    Alignment=1, Width=87, Height=18,;
    Caption="Costo unitario:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="QBCQNOVRED",Visible=.t., Left=557, Top=165,;
    Alignment=1, Width=67, Height=18,;
    Caption="Nota spese:"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (! .w_IsAlt)
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=2,top=19,;
    width=743+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=3,top=20,width=742+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='DIPENDEN|KEY_ARTI|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESRIS_2_33.Refresh()
      this.Parent.oDADESAGG_2_38.Refresh()
      this.Parent.oDACODOPE_2_39.Refresh()
      this.Parent.oDADATMOD_2_40.Refresh()
      this.Parent.oDAVALRIG_2_42.Refresh()
      this.Parent.oATIPRIG_2_43.Refresh()
      this.Parent.oETIPRIG_2_44.Refresh()
      this.Parent.oRTIPRIG_2_45.Refresh()
      this.Parent.oATIPRI2_2_46.Refresh()
      this.Parent.oDAOREEFF_2_48.Refresh()
      this.Parent.oDAMINEFF_2_49.Refresh()
      this.Parent.oDACOSUNI_2_54.Refresh()
      this.Parent.oDACOSINT_2_56.Refresh()
      this.Parent.oDESUNI_2_63.Refresh()
      this.Parent.oDAPREMIN_2_68.Refresh()
      this.Parent.oDAPREMAX_2_69.Refresh()
      this.Parent.oDAGAZUFF_2_70.Refresh()
      this.Parent.oDENOM_RESP_2_71.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='DIPENDEN'
        oDropInto=this.oBodyCol.oRow.oDACODRES_2_1
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oDACODICE_2_3
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oDACODATT_2_5
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oDAUNIMIS_2_34
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESRIS_2_33 as StdTrsField with uid="KTTKKCHPFQ",rtseq=68,rtrep=.t.,;
    cFormVar="w_DESRIS",value=space(39),enabled=.f.,;
    ToolTipText = "Descrizione risorsa",;
    HelpContextID = 216182218,;
    cTotal="", bFixedPos=.t., cQueryName = "DESRIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=271, Left=91, Top=283, InputMask=replicate('X',39)

  func oDESRIS_2_33.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPRIS='P'  or Empty(.w_TIPRIS))
    endwith
    endif
  endfunc

  add object oDADESAGG_2_38 as StdTrsMemo with uid="CJZMKTVOIT",rtseq=73,rtrep=.t.,;
    cFormVar="w_DADESAGG",value=space(0),;
    ToolTipText = "Descrizione aggiuntiva",;
    HelpContextID = 28270205,;
    cTotal="", bFixedPos=.t., cQueryName = "DADESAGG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=86, Width=761, Left=6, Top=191

  add object oDACODOPE_2_39 as StdTrsField with uid="YLTZFHKMWI",rtseq=74,rtrep=.t.,;
    cFormVar="w_DACODOPE",value=0,enabled=.f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 20361605,;
    cTotal="", bFixedPos=.t., cQueryName = "DACODOPE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=92, Top=335, cSayPict=["9999"], cGetPict=["9999"]

  add object oDADATMOD_2_40 as StdTrsField with uid="DTFROTUOVZ",rtseq=75,rtrep=.t.,;
    cFormVar="w_DADATMOD",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data ultima modifica",;
    HelpContextID = 38052230,;
    cTotal="", bFixedPos=.t., cQueryName = "DADATMOD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=181, Top=335

  add object oDAVALRIG_2_42 as StdTrsField with uid="EOKYKANTRQ",rtseq=77,rtrep=.t.,;
    cFormVar="w_DAVALRIG",value=0,enabled=.f.,;
    ToolTipText = "Importo",;
    HelpContextID = 37518973,;
    cTotal="", bFixedPos=.t., cQueryName = "DAVALRIG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=626, Top=117, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oATIPRIG_2_43 as StdTrsCombo with uid="AGEBLOUZJC",rtrep=.t.,;
    cFormVar="w_ATIPRIG", RowSource=""+"Non fatturabile,"+"Fatturabile" , ;
    ToolTipText = "Tipologia riga per la generazione del documento.",;
    HelpContextID = 162185478,;
    Height=25, Width=109, Left=626, Top=140,;
    cTotal="", cQueryName = "ATIPRIG",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oATIPRIG_2_43.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATIPRIG,&i_cF..t_ATIPRIG),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    space(1))))
  endfunc
  func oATIPRIG_2_43.GetRadio()
    this.Parent.oContained.w_ATIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oATIPRIG_2_43.ToRadio()
    this.Parent.oContained.w_ATIPRIG=trim(this.Parent.oContained.w_ATIPRIG)
    return(;
      iif(this.Parent.oContained.w_ATIPRIG=='N',1,;
      iif(this.Parent.oContained.w_ATIPRIG=='D',2,;
      0)))
  endfunc

  func oATIPRIG_2_43.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oATIPRIG_2_43.mCond()
    with this.Parent.oContained
      return (NOT(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N') )
    endwith
  endfunc

  func oATIPRIG_2_43.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not .w_ISALT)
    endwith
    endif
  endfunc

  add object oETIPRIG_2_44 as StdTrsCombo with uid="WDOQTSAWKZ",rtrep=.t.,;
    cFormVar="w_ETIPRIG", RowSource=""+"Nessuno,"+"Doc. vendite,"+"Doc. acquisti,"+"Entrambi" , ;
    ToolTipText = "Tipologia riga per la generazione del documento.",;
    HelpContextID = 162185542,;
    Height=25, Width=109, Left=626, Top=140,;
    cTotal="", cQueryName = "ETIPRIG",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oETIPRIG_2_44.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ETIPRIG,&i_cF..t_ETIPRIG),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    iif(xVal =3,'A',;
    iif(xVal =4,'E',;
    space(10))))))
  endfunc
  func oETIPRIG_2_44.GetRadio()
    this.Parent.oContained.w_ETIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oETIPRIG_2_44.ToRadio()
    this.Parent.oContained.w_ETIPRIG=trim(this.Parent.oContained.w_ETIPRIG)
    return(;
      iif(this.Parent.oContained.w_ETIPRIG=='N',1,;
      iif(this.Parent.oContained.w_ETIPRIG=='D',2,;
      iif(this.Parent.oContained.w_ETIPRIG=='A',3,;
      iif(this.Parent.oContained.w_ETIPRIG=='E',4,;
      0)))))
  endfunc

  func oETIPRIG_2_44.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oETIPRIG_2_44.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISAHE)
    endwith
    endif
  endfunc

  add object oRTIPRIG_2_45 as StdTrsCombo with uid="XKDCKNPLAZ",rtrep=.t.,;
    cFormVar="w_RTIPRIG", RowSource=""+"Nessuno,"+"Doc. vendite,"+"Doc. acquisti,"+"Entrambi" , ;
    ToolTipText = "Tipologia riga per la generazione del documento.",;
    HelpContextID = 162185750,;
    Height=25, Width=109, Left=626, Top=140,;
    cTotal="", cQueryName = "RTIPRIG",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oRTIPRIG_2_45.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RTIPRIG,&i_cF..t_RTIPRIG),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    iif(xVal =3,'A',;
    iif(xVal =4,'E',;
    'N')))))
  endfunc
  func oRTIPRIG_2_45.GetRadio()
    this.Parent.oContained.w_RTIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oRTIPRIG_2_45.ToRadio()
    this.Parent.oContained.w_RTIPRIG=trim(this.Parent.oContained.w_RTIPRIG)
    return(;
      iif(this.Parent.oContained.w_RTIPRIG=='N',1,;
      iif(this.Parent.oContained.w_RTIPRIG=='D',2,;
      iif(this.Parent.oContained.w_RTIPRIG=='A',3,;
      iif(this.Parent.oContained.w_RTIPRIG=='E',4,;
      0)))))
  endfunc

  func oRTIPRIG_2_45.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oRTIPRIG_2_45.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISAHR)
    endwith
    endif
  endfunc

  add object oATIPRI2_2_46 as StdTrsCombo with uid="SAEKIQSKDR",rtrep=.t.,;
    cFormVar="w_ATIPRI2", RowSource=""+"Senza nota spese,"+"Con nota spese" , ;
    ToolTipText = "Tipologia riga per la generazione della nota spese",;
    HelpContextID = 106249978,;
    Height=25, Width=109, Left=626, Top=165,;
    cTotal="", cQueryName = "ATIPRI2",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oATIPRI2_2_46.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATIPRI2,&i_cF..t_ATIPRI2),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    space(1))))
  endfunc
  func oATIPRI2_2_46.GetRadio()
    this.Parent.oContained.w_ATIPRI2 = this.RadioValue()
    return .t.
  endfunc

  func oATIPRI2_2_46.ToRadio()
    this.Parent.oContained.w_ATIPRI2=trim(this.Parent.oContained.w_ATIPRI2)
    return(;
      iif(this.Parent.oContained.w_ATIPRI2=='N',1,;
      iif(this.Parent.oContained.w_ATIPRI2=='D',2,;
      0)))
  endfunc

  func oATIPRI2_2_46.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oATIPRI2_2_46.mCond()
    with this.Parent.oContained
      return (NOT(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N') )
    endwith
  endfunc

  func oATIPRI2_2_46.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not .w_Isalt)
    endwith
    endif
  endfunc

  add object oDAOREEFF_2_48 as StdTrsField with uid="HYMDYDGWOM",rtseq=83,rtrep=.t.,;
    cFormVar="w_DAOREEFF",value=0,;
    ToolTipText = "Durata effettiva dell'attivit� (in ore)",;
    HelpContextID = 81596028,;
    cTotal="", bFixedPos=.t., cQueryName = "DAOREEFF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=465, Top=283, cSayPict=["999"], cGetPict=["999"]

  add object oDAMINEFF_2_49 as StdTrsField with uid="IJUBIHCFXM",rtseq=84,rtrep=.t.,;
    cFormVar="w_DAMINEFF",value=0,;
    ToolTipText = "Durata effettiva dell'attivit� (in minuti)",;
    HelpContextID = 90435196,;
    cTotal="", bFixedPos=.t., cQueryName = "DAMINEFF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 0 e 59",;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=505, Top=283, cSayPict=["99"], cGetPict=["99"]

  func oDAMINEFF_2_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DAMINEFF < 60)
    endwith
    return bRes
  endfunc

  add object oDACOSUNI_2_54 as StdTrsField with uid="WIBBFMBWVD",rtseq=89,rtrep=.t.,;
    cFormVar="w_DACOSUNI",value=0,;
    ToolTipText = "Costo unitario",;
    HelpContextID = 172405121,;
    cTotal="", bFixedPos=.t., cQueryName = "DACOSUNI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=628, Top=283, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)]

  func oDACOSUNI_2_54.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
    endif
  endfunc

  add object oDACOSINT_2_56 as StdTrsField with uid="UZRWLFLVNI",rtseq=91,rtrep=.t.,;
    cFormVar="w_DACOSINT",value=0,;
    ToolTipText = "Costo interno",;
    HelpContextID = 105296246,;
    cTotal="", bFixedPos=.t., cQueryName = "DACOSINT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=628, Top=309, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oDESUNI_2_63 as StdTrsField with uid="CWGETYRRIZ",rtseq=98,rtrep=.t.,;
    cFormVar="w_DESUNI",value=space(35),enabled=.f.,;
    ToolTipText = "Descrizione unit� di misura",;
    HelpContextID = 110079434,;
    cTotal="", bFixedPos=.t., cQueryName = "DESUNI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=249, Left=267, Top=117, InputMask=replicate('X',35)

  add object oDAPREMIN_2_68 as StdTrsField with uid="QYYDNVRMQD",rtseq=103,rtrep=.t.,;
    cFormVar="w_DAPREMIN",value=0,enabled=.f.,;
    ToolTipText = "Prezzo minimo",;
    HelpContextID = 215817860,;
    cTotal="", bFixedPos=.t., cQueryName = "DAPREMIN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=395, Top=336, cSayPict=[v_PV(38+VVU)], cGetPict=[v_GV(38+VVU)]

  func oDAPREMIN_2_68.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_ISALT)
    endwith
    endif
  endfunc

  add object oDAPREMAX_2_69 as StdTrsField with uid="CEFJYTZEHT",rtseq=104,rtrep=.t.,;
    cFormVar="w_DAPREMAX",value=0,enabled=.f.,;
    ToolTipText = "Prezzo massimo",;
    HelpContextID = 52617586,;
    cTotal="", bFixedPos=.t., cQueryName = "DAPREMAX",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=628, Top=334, cSayPict=[v_PV(38+VVU)], cGetPict=[v_GV(38+VVU)]

  func oDAPREMAX_2_69.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_ISALT)
    endwith
    endif
  endfunc

  add object oDAGAZUFF_2_70 as StdTrsField with uid="ESTOSQRIZU",rtseq=105,rtrep=.t.,;
    cFormVar="w_DAGAZUFF",value=space(6),enabled=.f.,;
    ToolTipText = "Codice Gazzetta ufficiale",;
    HelpContextID = 102469244,;
    cTotal="", bFixedPos=.t., cQueryName = "DAGAZUFF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=267, Top=141, InputMask=replicate('X',6)

  func oDAGAZUFF_2_70.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
    endif
  endfunc

  add object oDENOM_RESP_2_71 as StdTrsField with uid="QQBATUKLEW",rtseq=106,rtrep=.t.,;
    cFormVar="w_DENOM_RESP",value=space(39),enabled=.f.,;
    ToolTipText = "Cognome e nome",;
    HelpContextID = 10856533,;
    cTotal="", bFixedPos=.t., cQueryName = "DENOM_RESP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=271, Left=91, Top=283, InputMask=replicate('X',39)

  func oDENOM_RESP_2_71.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
    endif
  endfunc

  add object oBtn_2_87 as StdButton with uid="FSSIIAFLPV",width=48,height=45, tabstop=.f.,;
   left=5, top=115,;
    CpPicture="bmp\carica.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai dati di riga";
    , HelpContextID = 164077191;
    , TabStop=.f.,Caption='\<Analitica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_87.Click()
      do GSAG_KDA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_87.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COAN <> 'S'  or ! .w_ISALT)
    endwith
   endif
  endfunc

  add object oBtn_2_100 as StdButton with uid="FLVSTLMVZH",width=48,height=45, tabstop=.f.,;
   left=5, top=115,;
    CpPicture="bmp\carica.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai dati di riga";
    , HelpContextID = 195460662;
    , Caption='\<Dati';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_100.Click()
      do GSAG_KDL with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_100.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
   endif
  endfunc

  add object oBtn_2_101 as StdButton with uid="ZKTFGYXQCP",width=48,height=45,;
   left=54, top=115,;
    CpPicture="bmp\pagat.ico", caption="", nPag=2;
    , ToolTipText = "Premere per inserire la spesa e l'anticipazione relativa alla prestazione selezionata";
    , HelpContextID = 234906074;
    , TabStop=.f.,Caption='\<Spese';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_101.Click()
      this.parent.oContained.NotifyEvent("Riapplica")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_101.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! .w_ISALT OR .w_FLSPAN<>'S' )
    endwith
   endif
  endfunc

  add object oBtn_2_102 as StdButton with uid="WFHMFPDPLN",width=48,height=45, tabstop=.f.,;
   left=102, top=115,;
    CpPicture="bmp\legami.ico", caption="", nPag=2;
    , ToolTipText = "Premere per collegare la spesa o l'anticipazione ad una prestazione";
    , HelpContextID = 117896154;
    , Caption='\<Collega';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_102.Click()
      with this.Parent.oContained
        GSAL_BSA(this.Parent.oContained,"A","L")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_102.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISALT or empty(.w_DACODATT) or .w_DARIFPRE>0 or .w_DAPREZZO=0)
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsag_mdaBodyRow as CPBodyRowCnt
  Width=733
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDACODRES_2_1 as StdTrsField with uid="HABQWPUVJJ",rtseq=37,rtrep=.t.,;
    cFormVar="w_DACODRES",value=space(5),nZero=5,;
    ToolTipText = "Codice partecipante",;
    HelpContextID = 29970057,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione tipo risorsa incongruente o obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=43, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_DACODRES"

  func oDACODRES_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODRES_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDACODRES_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oDACODRES_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone/Risorse/Gruppi",'GSAG_MDA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oDACODRES_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_DACODRES
    i_obj.ecpSave()
  endproc

  add object oCPROWORD_2_2 as StdTrsField with uid="GGTKWNXJMU",rtseq=38,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0, tabstop=.f.,;
    ToolTipText = "Numero riga",;
    HelpContextID = 373398,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oDACODICE_2_3 as StdTrsField with uid="TTKRZADSUP",rtseq=39,rtrep=.t.,;
    cFormVar="w_DACODICE",value=space(41),;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 121024901,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=140, Left=104, Top=0, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_DACODICE"

  func oDACODICE_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISAHE)
    endwith
    endif
  endfunc

  func oDACODICE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODICE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDACODICE_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDACODICE_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",'GSAGZKPM.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDACODICE_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DACODICE
    i_obj.ecpSave()
  endproc

  add object oDACODATT_2_5 as StdTrsField with uid="RAJDVSSBCE",rtseq=41,rtrep=.t.,;
    cFormVar="w_DACODATT",value=space(20),;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 13192842,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=140, Left=104, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_DACODATT"

  func oDACODATT_2_5.mCond()
    with this.Parent.oContained
      return (.w_DARIGPRE<>'S')
    endwith
  endfunc

  func oDACODATT_2_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISAHE)
    endwith
    endif
  endfunc

  func oDACODATT_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODATT_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDACODATT_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDACODATT_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",''+iif(NOT isalt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDACODATT_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DACODATT
    i_obj.ecpSave()
  endproc

  add object oDADESATT_2_32 as StdTrsField with uid="YCNLBZWDQT",rtseq=67,rtrep=.t.,;
    cFormVar="w_DADESATT",value=space(40),;
    ToolTipText = "Descrizione attivit�",;
    HelpContextID = 28270218,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=232, Left=247, Top=0, InputMask=replicate('X',40)

  func oDADESATT_2_32.mCond()
    with this.Parent.oContained
      return ((.w_NOEDDES='N' OR !.w_ISALT or cp_IsAdministrator()))
    endwith
  endfunc

  add object oDAUNIMIS_2_34 as StdTrsField with uid="OLPRVZURWK",rtseq=69,rtrep=.t.,;
    cFormVar="w_DAUNIMIS",value=space(3), tabstop=.f.,;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 219770505,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire l'unit� di misura principale o secondaria della prestazione",;
   bGlobalFont=.t.,;
    Height=17, Width=38, Left=481, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_DAUNIMIS"

  func oDAUNIMIS_2_34.mCond()
    with this.Parent.oContained
      return (.w_TIPART = 'FM' AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S'))
    endwith
  endfunc

  func oDAUNIMIS_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAUNIMIS_2_34.ecpDrop(oSource)
    this.Parent.oContained.link_2_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDAUNIMIS_2_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oDAUNIMIS_2_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oDAUNIMIS_2_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_DAUNIMIS
    i_obj.ecpSave()
  endproc

  add object oDAQTAMOV_2_36 as StdTrsField with uid="JTYWZUBCJC",rtseq=71,rtrep=.t.,;
    cFormVar="w_DAQTAMOV",value=0,;
    ToolTipText = "Quantit�",;
    HelpContextID = 56676724,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=521, Top=0, cSayPict=['@Z '+v_PQ(11)], cGetPict=['@Z '+v_GQ(11)], bHasZoom = .t. 

  func oDAQTAMOV_2_36.mCond()
    with this.Parent.oContained
      return (.w_TIPART = 'FM')
    endwith
  endfunc

  func oDAQTAMOV_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DAQTAMOV>0 AND (.w_FL_FRAZ<>'S' OR .w_DAQTAMOV=INT(.w_DAQTAMOV))) OR NOT .w_TIPSER $ 'RM')
    endwith
    return bRes
  endfunc

  proc oDAQTAMOV_2_36.mZoom
      with this.Parent.oContained
        gsag_bom(this.Parent.oContained,"D")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDAPREZZO_2_37 as StdTrsField with uid="YGRCARMVHO",rtseq=72,rtrep=.t.,;
    cFormVar="w_DAPREZZO",value=0,;
    ToolTipText = "Prezzo unitario",;
    HelpContextID = 165486213,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=122, Left=606, Top=0, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)], bHasZoom = .t. 

  func oDAPREZZO_2_37.mCond()
    with this.Parent.oContained
      return (.w_TIPART<>'DE')
    endwith
  endfunc

  proc oDAPREZZO_2_37.mZoom
      with this.Parent.oContained
        gsag_bmp(this.Parent.oContained,"I",.w_DAPREZZO,.w_DAPREMAX,.w_DAPREMIN,.w_RCACODART)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oObj_2_73 as cp_runprogram with uid="FZVGEATLHK",width=452,height=19,;
   left=245, top=377,;
    caption='GSAG_BDD(B, PRE)',;
   bGlobalFont=.t.,;
    prg="GSAG_BDD('B', 'PRE')",;
    cEvent = "w_DACODATT Changed,w_DACODICE Changed,Asstrad",;
    nPag=2;
    , HelpContextID = 90946221

  add object oObj_2_128 as cp_runprogram with uid="GTPHAFKRYK",width=219,height=19,;
   left=248, top=514,;
    caption='GSAL_BSA(A,C)',;
   bGlobalFont=.t.,;
    prg="gsal_bsa('A','C')",;
    cEvent = "CalcPrest",;
    nPag=2;
    , ToolTipText = "Creazione righe prestazioni collegate";
    , HelpContextID = 162773977

  add object oObj_2_129 as cp_runprogram with uid="OTLXPLRLTQ",width=219,height=19,;
   left=248, top=533,;
    caption='GSAL_BSA(A,E)',;
   bGlobalFont=.t.,;
    prg="gsal_bsa('A','E')",;
    cEvent = "HasEvent,Elispese",;
    nPag=2;
    , ToolTipText = "Eliminazione righe prestazioni collegate";
    , HelpContextID = 162642905
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oDACODRES_2_1.When()
    return(.t.)
  proc oDACODRES_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDACODRES_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mda','OFFDATTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DASERIAL=OFFDATTI.DASERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
