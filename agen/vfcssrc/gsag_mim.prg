* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mim                                                        *
*              Gestione impianti                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-28                                                      *
* Last revis.: 2015-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_mim"))

* --- Class definition
define class tgsag_mim as StdTrsForm
  Top    = 3
  Left   = 10

  * --- Standard Properties
  Width  = 767
  Height = 550+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-31"
  HelpContextID=3528855
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=53

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  CONTI_IDX = 0
  DES_DIVE_IDX = 0
  ZONE_IDX = 0
  MODMATTR_IDX = 0
  MODDATTR_IDX = 0
  PAR_AGEN_IDX = 0
  ELE_CONT_IDX = 0
  cFile = "IMP_MAST"
  cFileDetail = "IMP_DETT"
  cKeySelect = "IMCODICE"
  cKeyWhere  = "IMCODICE=this.w_IMCODICE"
  cKeyDetail  = "IMCODICE=this.w_IMCODICE"
  cKeyWhereODBC = '"IMCODICE="+cp_ToStrODBC(this.w_IMCODICE)';

  cKeyDetailWhereODBC = '"IMCODICE="+cp_ToStrODBC(this.w_IMCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"IMP_DETT.IMCODICE="+cp_ToStrODBC(this.w_IMCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'IMP_DETT.CPROWORD '
  cPrg = "gsag_mim"
  cComment = "Gestione impianti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  cAutoZoom = 'GSAG_MIM'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LRIGA = 0
  w_KEYLISTB = space(10)
  w_IMCODICE = space(10)
  o_IMCODICE = space(10)
  w_IMDESCRI = space(50)
  w_IM__NOTE = space(0)
  w_IMTIPCON = space(1)
  w_IMCODCON = space(15)
  o_IMCODCON = space(15)
  w_CPROWORD = 0
  w_GESTGUID = space(14)
  w_IMLEGAME = 0
  w_IMDESCON = space(50)
  o_IMDESCON = space(50)
  w_IM__SEDE = space(5)
  o_IM__SEDE = space(5)
  w_TELEFO = space(18)
  w_LIVELLO = 0
  w_IM_DNOTE = space(0)
  w_LINK = .F.
  o_LINK = .F.
  w_IMMODATR = space(20)
  o_IMMODATR = space(20)
  w_ZONA = space(3)
  w_ZONDES = space(35)
  w_IM__ZONA = space(3)
  o_IM__ZONA = space(3)
  w_IMPERSON = space(40)
  w_TELEFONO = space(18)
  w_IMTELEFO = space(18)
  w_IMDISTAN = 0
  w_IMDATINI = ctod('  /  /  ')
  w_IMDATFIN = ctod('  /  /  ')
  w_ANDESCRI = space(60)
  w_DDNOMDES = space(40)
  w_DESZONA = space(35)
  w_IMDTOINI = ctod('  /  /  ')
  w_IMDTOBSO = ctod('  /  /  ')
  w_GEST = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_READAZI = space(5)
  o_READAZI = space(5)
  w_CODMOD = space(10)
  w_CODNUM = space(1)
  w_GRUDEF = space(1)
  o_GRUDEF = space(1)
  w_CODATT = space(10)
  w_MADESCRI = space(40)
  w_GRUPPO = space(15)
  w_RIGA = 0
  w_RET = .F.
  w_CATCLI = space(5)
  w_FLSCOR = space(1)
  w_NUMLIS = space(5)
  w_ADDEDELEMENTS = space(10)
  w_DIPENDENZE = space(10)
  w_OCODCON = space(15)
  w_TIPRIF = space(2)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_IMCODICE = this.W_IMCODICE

  * --- Children pointers
  GSAR_MRA = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_mim
  w_OBJMSK = .null.
  o_CPROWNUM = 0
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'IMP_MAST','gsag_mim')
    stdPageFrame::Init()
    *set procedure to GSAR_MRA additive
    with this
      .Pages(1).addobject("oPag","tgsag_mimPag1","gsag_mim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Impianti")
      .Pages(1).HelpContextID = 189379857
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIMCODICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAR_MRA
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DES_DIVE'
    this.cWorkTables[3]='ZONE'
    this.cWorkTables[4]='MODMATTR'
    this.cWorkTables[5]='MODDATTR'
    this.cWorkTables[6]='PAR_AGEN'
    this.cWorkTables[7]='ELE_CONT'
    this.cWorkTables[8]='IMP_MAST'
    this.cWorkTables[9]='IMP_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.IMP_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.IMP_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MRA = CREATEOBJECT('stdDynamicChild',this,'GSAR_MRA',this.oPgFrm.Page1.oPag.oLinkPC_2_15)
    this.GSAR_MRA.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MRA)
      this.GSAR_MRA.DestroyChildrenChain()
      this.GSAR_MRA=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_15')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MRA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MRA.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MRA.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSAR_MRA.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_GESTGUID,"GUID";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_IMCODICE = NVL(IMCODICE,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from IMP_MAST where IMCODICE=KeySet.IMCODICE
    *
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2],this.bLoadRecFilter,this.IMP_MAST_IDX,"gsag_mim")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('IMP_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "IMP_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"IMP_DETT.","IMP_MAST.")
      i_cTable = i_cTable+' IMP_MAST '
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IMCODICE',this.w_IMCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_LRIGA = 0
        .w_KEYLISTB = Sys(2015)
        .w_TELEFO = space(18)
        .w_ZONA = space(3)
        .w_ZONDES = space(35)
        .w_TELEFONO = space(18)
        .w_ANDESCRI = space(60)
        .w_DDNOMDES = space(40)
        .w_DESZONA = .w_ZONDES
        .w_OBTEST = i_DATSYS
        .w_CODMOD = space(10)
        .w_CODNUM = space(1)
        .w_CATCLI = space(5)
        .w_FLSCOR = space(1)
        .w_NUMLIS = space(5)
        .w_ADDEDELEMENTS = SYS( 2015 )
        .w_TIPRIF = space(2)
        .w_IMCODICE = NVL(IMCODICE,space(10))
        .op_IMCODICE = .w_IMCODICE
        .w_IMDESCRI = NVL(IMDESCRI,space(50))
        .w_IM__NOTE = NVL(IM__NOTE,space(0))
        .w_IMTIPCON = NVL(IMTIPCON,space(1))
        .w_IMCODCON = NVL(IMCODCON,space(15))
          if link_1_8_joined
            this.w_IMCODCON = NVL(ANCODICE108,NVL(this.w_IMCODCON,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI108,space(60))
            this.w_ZONA = NVL(ANCODZON108,space(3))
            this.w_TELEFONO = NVL(ANTELEFO108,space(18))
            this.w_CATCLI = NVL(ANCATSCM108,space(5))
            this.w_FLSCOR = NVL(ANSCORPO108,space(1))
            this.w_NUMLIS = NVL(ANNUMLIS108,space(5))
          else
          .link_1_8('Load')
          endif
        .w_IM__SEDE = NVL(IM__SEDE,space(5))
          .link_1_10('Load')
          .link_1_12('Load')
        .w_IM__ZONA = NVL(IM__ZONA,space(3))
          if link_1_14_joined
            this.w_IM__ZONA = NVL(ZOCODZON114,NVL(this.w_IM__ZONA,space(3)))
            this.w_DESZONA = NVL(ZODESZON114,space(35))
          else
          .link_1_14('Load')
          endif
        .w_IMPERSON = NVL(IMPERSON,space(40))
        .w_IMTELEFO = NVL(IMTELEFO,space(18))
        .w_IMDISTAN = NVL(IMDISTAN,0)
        .w_IMDATINI = NVL(cp_ToDate(IMDATINI),ctod("  /  /  "))
        .w_IMDATFIN = NVL(cp_ToDate(IMDATFIN),ctod("  /  /  "))
        .w_READAZI = i_CODAZI
          .link_1_33('Load')
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .w_OCODCON = .w_IMCODCON
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'IMP_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from IMP_DETT where IMCODICE=KeySet.IMCODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('IMP_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "IMP_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" IMP_DETT"
        link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'IMCODICE',this.w_IMCODICE  )
        select * from (i_cTable) IMP_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_LIVELLO = 0
        .w_LINK = .T.
          .w_CODATT = space(10)
          .w_MADESCRI = space(40)
          .w_GRUPPO = space(15)
          .w_RIGA = 0
          .w_RET = .f.
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_GESTGUID = NVL(GESTGUID,space(14))
          .w_IMLEGAME = NVL(IMLEGAME,0)
          .link_2_3('Load')
          .w_IMDESCON = NVL(IMDESCON,space(50))
          .w_IM_DNOTE = NVL(IM_DNOTE,space(0))
          .w_IMMODATR = NVL(IMMODATR,space(20))
          if link_2_8_joined
            this.w_IMMODATR = NVL(MACODICE208,NVL(this.w_IMMODATR,space(20)))
            this.w_MADESCRI = NVL(MADESCRI208,space(40))
          else
          .link_2_8('Load')
          endif
          .w_IMDTOINI = NVL(cp_ToDate(IMDTOINI),ctod("  /  /  "))
          .w_IMDTOBSO = NVL(cp_ToDate(IMDTOBSO),ctod("  /  /  "))
        .w_GEST = .w_IMMODATR
        .w_GRUDEF = 'S'
          .link_2_12('Load')
        .w_DIPENDENZE = "          "
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_READAZI = i_CODAZI
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .w_DIPENDENZE = "          "
        .w_OCODCON = .w_IMCODCON
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_37.enabled = .oPgFrm.Page1.oPag.oBtn_1_37.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_61.enabled = .oPgFrm.Page1.oPag.oBtn_1_61.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_19.enabled = .oPgFrm.Page1.oPag.oBtn_2_19.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_LRIGA=0
      .w_KEYLISTB=space(10)
      .w_IMCODICE=space(10)
      .w_IMDESCRI=space(50)
      .w_IM__NOTE=space(0)
      .w_IMTIPCON=space(1)
      .w_IMCODCON=space(15)
      .w_CPROWORD=10
      .w_GESTGUID=space(14)
      .w_IMLEGAME=0
      .w_IMDESCON=space(50)
      .w_IM__SEDE=space(5)
      .w_TELEFO=space(18)
      .w_LIVELLO=0
      .w_IM_DNOTE=space(0)
      .w_LINK=.f.
      .w_IMMODATR=space(20)
      .w_ZONA=space(3)
      .w_ZONDES=space(35)
      .w_IM__ZONA=space(3)
      .w_IMPERSON=space(40)
      .w_TELEFONO=space(18)
      .w_IMTELEFO=space(18)
      .w_IMDISTAN=0
      .w_IMDATINI=ctod("  /  /  ")
      .w_IMDATFIN=ctod("  /  /  ")
      .w_ANDESCRI=space(60)
      .w_DDNOMDES=space(40)
      .w_DESZONA=space(35)
      .w_IMDTOINI=ctod("  /  /  ")
      .w_IMDTOBSO=ctod("  /  /  ")
      .w_GEST=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_READAZI=space(5)
      .w_CODMOD=space(10)
      .w_CODNUM=space(1)
      .w_GRUDEF=space(1)
      .w_CODATT=space(10)
      .w_MADESCRI=space(40)
      .w_GRUPPO=space(15)
      .w_RIGA=0
      .w_RET=.f.
      .w_CATCLI=space(5)
      .w_FLSCOR=space(1)
      .w_NUMLIS=space(5)
      .w_ADDEDELEMENTS=space(10)
      .w_DIPENDENZE=space(10)
      .w_OCODCON=space(15)
      .w_TIPRIF=space(2)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .w_LRIGA = 0
        .w_KEYLISTB = Sys(2015)
        .DoRTCalc(3,5,.f.)
        .w_IMTIPCON = 'C'
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_IMCODCON))
         .link_1_8('Full')
        endif
        .DoRTCalc(8,9,.f.)
        .w_IMLEGAME = 0
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_IMLEGAME))
         .link_2_3('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_IM__SEDE))
         .link_1_10('Full')
        endif
        .DoRTCalc(13,15,.f.)
        .w_LINK = .T.
        .w_IMMODATR = IIF( empty(.w_IMMODATR) , .w_CODMOD , .w_IMMODATR )
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_IMMODATR))
         .link_2_8('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_ZONA))
         .link_1_12('Full')
        endif
        .DoRTCalc(19,19,.f.)
        .w_IM__ZONA = .w_ZONA
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_IM__ZONA))
         .link_1_14('Full')
        endif
        .DoRTCalc(21,22,.f.)
        .w_IMTELEFO = IIF(EMPTY(NVL(.w_IM__SEDE,'')) AND EMPTY(NVL(.w_TELEFO,'')), .w_TELEFONO,.w_TELEFO)
        .DoRTCalc(24,28,.f.)
        .w_DESZONA = .w_ZONDES
        .DoRTCalc(30,31,.f.)
        .w_GEST = .w_IMMODATR
        .w_OBTEST = i_DATSYS
        .w_READAZI = i_CODAZI
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_READAZI))
         .link_1_33('Full')
        endif
        .DoRTCalc(35,36,.f.)
        .w_GRUDEF = 'S'
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_GRUDEF))
         .link_2_12('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .DoRTCalc(38,45,.f.)
        .w_ADDEDELEMENTS = SYS( 2015 )
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .w_DIPENDENZE = "          "
        .w_OCODCON = .w_IMCODCON
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'IMP_MAST')
    this.DoRTCalc(49,53,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_19.enabled = this.oPgFrm.Page1.oPag.oBtn_2_19.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsag_mim
    this.w_OBJMSK = this
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oIMCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oIMDESCRI_1_5.enabled = i_bVal
      .Page1.oPag.oIM__NOTE_1_6.enabled = i_bVal
      .Page1.oPag.oIMCODCON_1_8.enabled = i_bVal
      .Page1.oPag.oIM__SEDE_1_10.enabled = i_bVal
      .Page1.oPag.oIM_DNOTE_2_6.enabled = i_bVal
      .Page1.oPag.oIM__ZONA_1_14.enabled = i_bVal
      .Page1.oPag.oIMPERSON_1_16.enabled = i_bVal
      .Page1.oPag.oIMTELEFO_1_19.enabled = i_bVal
      .Page1.oPag.oIMDISTAN_1_20.enabled = i_bVal
      .Page1.oPag.oIMDATINI_1_22.enabled = i_bVal
      .Page1.oPag.oIMDATFIN_1_24.enabled = i_bVal
      .Page1.oPag.oIMDTOINI_2_9.enabled = i_bVal
      .Page1.oPag.oIMDTOBSO_2_10.enabled = i_bVal
      .Page1.oPag.oBtn_1_36.enabled = i_bVal
      .Page1.oPag.oBtn_1_37.enabled = .Page1.oPag.oBtn_1_37.mCond()
      .Page1.oPag.oBtn_1_61.enabled = .Page1.oPag.oBtn_1_61.mCond()
      .Page1.oPag.oBtn_2_19.enabled = .Page1.oPag.oBtn_2_19.mCond()
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_45.enabled = i_bVal
      .Page1.oPag.oObj_1_46.enabled = i_bVal
      .Page1.oPag.oObj_1_49.enabled = i_bVal
      .Page1.oPag.oObj_1_52.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oIMCODICE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oIMCODICE_1_3.enabled = .t.
      endif
    endwith
    this.GSAR_MRA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'IMP_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    with this	
      if .w_CODNUM='S'
        cp_AskTableProg(this,i_nConn,"PRIMP","i_codazi,w_IMCODICE")
      endif
      .op_codazi = .w_codazi
      .op_IMCODICE = .w_IMCODICE
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MRA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMCODICE,"IMCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMDESCRI,"IMDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IM__NOTE,"IM__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMTIPCON,"IMTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMCODCON,"IMCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IM__SEDE,"IM__SEDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IM__ZONA,"IM__ZONA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMPERSON,"IMPERSON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMTELEFO,"IMTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMDISTAN,"IMDISTAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMDATINI,"IMDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IMDATFIN,"IMDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    i_lTable = "IMP_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.IMP_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsag_sim with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_IMDESCON C(50);
      ,t_LIVELLO N(4);
      ,t_IM_DNOTE M(10);
      ,t_IMMODATR C(20);
      ,t_IMDTOINI D(8);
      ,t_IMDTOBSO D(8);
      ,CPROWNUM N(10);
      ,t_GESTGUID C(14);
      ,t_IMLEGAME N(4);
      ,t_LINK L(1);
      ,t_GEST C(20);
      ,t_GRUDEF C(1);
      ,t_CODATT C(10);
      ,t_MADESCRI C(40);
      ,t_GRUPPO C(15);
      ,t_RIGA N(4);
      ,t_RET L(1);
      ,t_DIPENDENZE C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mimbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIMDESCON_2_4.controlsource=this.cTrsName+'.t_IMDESCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLIVELLO_2_5.controlsource=this.cTrsName+'.t_LIVELLO'
    this.oPgFRm.Page1.oPag.oIM_DNOTE_2_6.controlsource=this.cTrsName+'.t_IM_DNOTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIMMODATR_2_8.controlsource=this.cTrsName+'.t_IMMODATR'
    this.oPgFRm.Page1.oPag.oIMDTOINI_2_9.controlsource=this.cTrsName+'.t_IMDTOINI'
    this.oPgFRm.Page1.oPag.oIMDTOBSO_2_10.controlsource=this.cTrsName+'.t_IMDTOBSO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(59)
    this.AddVLine(476)
    this.AddVLine(533)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
      with this
        if .w_CODNUM='S'
          cp_NextTableProg(this,i_nConn,"PRIMP","i_codazi,w_IMCODICE")
        endif
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into IMP_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'IMP_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'IMP_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(IMCODICE,IMDESCRI,IM__NOTE,IMTIPCON,IMCODCON"+;
                  ",IM__SEDE,IM__ZONA,IMPERSON,IMTELEFO,IMDISTAN"+;
                  ",IMDATINI,IMDATFIN,UTCC,UTDC,UTDV"+;
                  ",UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_IMCODICE)+;
                    ","+cp_ToStrODBC(this.w_IMDESCRI)+;
                    ","+cp_ToStrODBC(this.w_IM__NOTE)+;
                    ","+cp_ToStrODBC(this.w_IMTIPCON)+;
                    ","+cp_ToStrODBCNull(this.w_IMCODCON)+;
                    ","+cp_ToStrODBCNull(this.w_IM__SEDE)+;
                    ","+cp_ToStrODBCNull(this.w_IM__ZONA)+;
                    ","+cp_ToStrODBC(this.w_IMPERSON)+;
                    ","+cp_ToStrODBC(this.w_IMTELEFO)+;
                    ","+cp_ToStrODBC(this.w_IMDISTAN)+;
                    ","+cp_ToStrODBC(this.w_IMDATINI)+;
                    ","+cp_ToStrODBC(this.w_IMDATFIN)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'IMP_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'IMP_MAST')
        cp_CheckDeletedKey(i_cTable,0,'IMCODICE',this.w_IMCODICE)
        INSERT INTO (i_cTable);
              (IMCODICE,IMDESCRI,IM__NOTE,IMTIPCON,IMCODCON,IM__SEDE,IM__ZONA,IMPERSON,IMTELEFO,IMDISTAN,IMDATINI,IMDATFIN,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_IMCODICE;
                  ,this.w_IMDESCRI;
                  ,this.w_IM__NOTE;
                  ,this.w_IMTIPCON;
                  ,this.w_IMCODCON;
                  ,this.w_IM__SEDE;
                  ,this.w_IM__ZONA;
                  ,this.w_IMPERSON;
                  ,this.w_IMTELEFO;
                  ,this.w_IMDISTAN;
                  ,this.w_IMDATINI;
                  ,this.w_IMDATFIN;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- gsag_mim
    *-Ricalcolo il gestguid per gestire la multiutenza-*
    this.Set('w_GESTGUID',ALLTRIM(ALLTRIM(this.w_IMCODICE)+RIGHT(REPLICATE('0',4)+ALLTRIM(STR(THIS.w_CPROWNUM,4,0)),4)),.T.,.T.)
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
      *
      * insert into IMP_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(IMCODICE,CPROWORD,GESTGUID,IMLEGAME,IMDESCON"+;
                  ",IM_DNOTE,IMMODATR,IMDTOINI,IMDTOBSO,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_IMCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_GESTGUID)+","+cp_ToStrODBCNull(this.w_IMLEGAME)+","+cp_ToStrODBC(this.w_IMDESCON)+;
             ","+cp_ToStrODBC(this.w_IM_DNOTE)+","+cp_ToStrODBCNull(this.w_IMMODATR)+","+cp_ToStrODBC(this.w_IMDTOINI)+","+cp_ToStrODBC(this.w_IMDTOBSO)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'IMCODICE',this.w_IMCODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_IMCODICE,this.w_CPROWORD,this.w_GESTGUID,this.w_IMLEGAME,this.w_IMDESCON"+;
                ",this.w_IM_DNOTE,this.w_IMMODATR,this.w_IMDTOINI,this.w_IMDTOBSO,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update IMP_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'IMP_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " IMDESCRI="+cp_ToStrODBC(this.w_IMDESCRI)+;
             ",IM__NOTE="+cp_ToStrODBC(this.w_IM__NOTE)+;
             ",IMTIPCON="+cp_ToStrODBC(this.w_IMTIPCON)+;
             ",IMCODCON="+cp_ToStrODBCNull(this.w_IMCODCON)+;
             ",IM__SEDE="+cp_ToStrODBCNull(this.w_IM__SEDE)+;
             ",IM__ZONA="+cp_ToStrODBCNull(this.w_IM__ZONA)+;
             ",IMPERSON="+cp_ToStrODBC(this.w_IMPERSON)+;
             ",IMTELEFO="+cp_ToStrODBC(this.w_IMTELEFO)+;
             ",IMDISTAN="+cp_ToStrODBC(this.w_IMDISTAN)+;
             ",IMDATINI="+cp_ToStrODBC(this.w_IMDATINI)+;
             ",IMDATFIN="+cp_ToStrODBC(this.w_IMDATFIN)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'IMP_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'IMCODICE',this.w_IMCODICE  )
          UPDATE (i_cTable) SET;
              IMDESCRI=this.w_IMDESCRI;
             ,IM__NOTE=this.w_IM__NOTE;
             ,IMTIPCON=this.w_IMTIPCON;
             ,IMCODCON=this.w_IMCODCON;
             ,IM__SEDE=this.w_IM__SEDE;
             ,IM__ZONA=this.w_IM__ZONA;
             ,IMPERSON=this.w_IMPERSON;
             ,IMTELEFO=this.w_IMTELEFO;
             ,IMDISTAN=this.w_IMDISTAN;
             ,IMDATINI=this.w_IMDATINI;
             ,IMDATFIN=this.w_IMDATFIN;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_IMDESCON) AND NOT EMPTY(t_IMMODATR)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSAR_MRA.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_GESTGUID,"GUID";
                     )
              this.GSAR_MRA.mDelete()
              *
              * delete from IMP_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update IMP_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",GESTGUID="+cp_ToStrODBC(this.w_GESTGUID)+;
                     ",IMLEGAME="+cp_ToStrODBCNull(this.w_IMLEGAME)+;
                     ",IMDESCON="+cp_ToStrODBC(this.w_IMDESCON)+;
                     ",IM_DNOTE="+cp_ToStrODBC(this.w_IM_DNOTE)+;
                     ",IMMODATR="+cp_ToStrODBCNull(this.w_IMMODATR)+;
                     ",IMDTOINI="+cp_ToStrODBC(this.w_IMDTOINI)+;
                     ",IMDTOBSO="+cp_ToStrODBC(this.w_IMDTOBSO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,GESTGUID=this.w_GESTGUID;
                     ,IMLEGAME=this.w_IMLEGAME;
                     ,IMDESCON=this.w_IMDESCON;
                     ,IM_DNOTE=this.w_IM_DNOTE;
                     ,IMMODATR=this.w_IMMODATR;
                     ,IMDTOINI=this.w_IMDTOINI;
                     ,IMDTOBSO=this.w_IMDTOBSO;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (t_CPROWORD<>0 AND NOT EMPTY(t_IMDESCON) AND NOT EMPTY(t_IMMODATR))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSAR_MRA.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_GESTGUID,"GUID";
             )
        this.GSAR_MRA.mReplace()
        this.GSAR_MRA.bSaveContext=.f.
      endscan
     this.GSAR_MRA.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gsag_mim
    if not(bTrsErr)
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 AND NOT EMPTY(t_IMDESCON) AND NOT EMPTY(t_IMMODATR)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSAR_MRA : Deleting
        this.GSAR_MRA.bSaveContext=.f.
        this.GSAR_MRA.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_GESTGUID,"GUID";
               )
        this.GSAR_MRA.bSaveContext=.t.
        this.GSAR_MRA.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
        *
        * delete IMP_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
        *
        * delete IMP_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_IMDESCON) AND NOT EMPTY(t_IMMODATR)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_LINK<>.w_LINK
          .w_IMLEGAME = 0
          .link_2_3('Full')
        endif
        .DoRTCalc(11,16,.t.)
        if .o_IMDESCON<>.w_IMDESCON.or. .o_IMMODATR<>.w_IMMODATR
          .w_IMMODATR = IIF( empty(.w_IMMODATR) , .w_CODMOD , .w_IMMODATR )
          .link_2_8('Full')
        endif
          .link_1_12('Full')
        .DoRTCalc(19,19,.t.)
        if .o_IMCODCON<>.w_IMCODCON
          .w_IM__ZONA = .w_ZONA
          .link_1_14('Full')
        endif
        .DoRTCalc(21,22,.t.)
        if .o_IM__SEDE<>.w_IM__SEDE.or. .o_IMCODCON<>.w_IMCODCON
          .w_IMTELEFO = IIF(EMPTY(NVL(.w_IM__SEDE,'')) AND EMPTY(NVL(.w_TELEFO,'')), .w_TELEFONO,.w_TELEFO)
        endif
        .DoRTCalc(24,31,.t.)
          .w_GEST = .w_IMMODATR
        .DoRTCalc(33,33,.t.)
        if .o_READAZI<>.w_READAZI
          .w_READAZI = i_CODAZI
          .link_1_33('Full')
        endif
        .DoRTCalc(35,36,.t.)
        if .o_GRUDEF<>.w_GRUDEF.or. .o_IMMODATR<>.w_IMMODATR
          .w_GRUDEF = 'S'
          .link_2_12('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .DoRTCalc(38,46,.t.)
        if .o_IMCODICE<>.w_IMCODICE.or. .o_CPROWNUM<>.w_CPROWNUM
          .w_DIPENDENZE = "          "
        endif
          .w_OCODCON = .w_IMCODCON
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
          if .w_CODNUM='S'
             cp_AskTableProg(this,i_nConn,"PRIMP","i_codazi,w_IMCODICE")
          endif
          .op_IMCODICE = .w_IMCODICE
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(49,53,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_GESTGUID with this.w_GESTGUID
      replace t_IMLEGAME with this.w_IMLEGAME
      replace t_LINK with this.w_LINK
      replace t_GEST with this.w_GEST
      replace t_GRUDEF with this.w_GRUDEF
      replace t_CODATT with this.w_CODATT
      replace t_MADESCRI with this.w_MADESCRI
      replace t_GRUPPO with this.w_GRUPPO
      replace t_RIGA with this.w_RIGA
      replace t_RET with this.w_RET
      replace t_DIPENDENZE with this.w_DIPENDENZE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_YIHUZGOBZA()
    with this
          * --- Chiusura maschera
          ChiusuraMaschera(this;
             )
    endwith
  endproc
  proc Calculate_REDMJUMDAQ()
    with this
          * --- Calcola autonumber
          .w_IMCODICE = IIF(.w_CODNUM<>'S', SPACE(10), RIGHT(.w_IMCODICE, 10))
          .op_IMCODICE = .w_IMCODICE
    endwith
  endproc
  proc Calculate_EYMUKKHXKY()
    with this
          * --- Sbianco la sede
          .w_IM__SEDE = ''
          .w_IM__ZONA = ''
          .w_DDNOMDES = ''
          .w_DESZONA = ''
    endwith
  endproc
  proc Calculate_WTBSFWEPUN()
    with this
          * --- Delete start
          gsag_bik(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIMCODICE_1_3.enabled = this.oPgFrm.Page1.oPag.oIMCODICE_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_19.enabled =this.oPgFrm.Page1.oPag.oBtn_2_19.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Done")
          .Calculate_YIHUZGOBZA()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
        if lower(cEvent)==lower("New record")
          .Calculate_REDMJUMDAQ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
        if lower(cEvent)==lower("w_IMCODCON Changed")
          .Calculate_EYMUKKHXKY()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
        if lower(cEvent)==lower("Delete start")
          .Calculate_WTBSFWEPUN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_mim
    if cEVENT = "Done"
       USE IN SELECT (this.w_ADDEDELEMENTS )
    endif
    if cevent='w_IMCODCON Changed' and This.cFunction='Edit'
        Ah_errormsg("Attenzione, la variazione dell'intestatario pu� portare ad incongruenze con attivit�/elementi contratto collegati!")
    endif
    if cEVENT = "testata" or cEVENT = "riga"
       local L_OBJ_MASK, l_riga
       L_OBJ_MASK=GSAG_kts(this)
       if cEVENT = "testata"
         L_OBJ_MASK.w_RIGA=0
         L_OBJ_MASK.notifyevent('SearchT')
       else
         L_OBJ_MASK.w_RIGA=this.w_CPROWNUM
         L_OBJ_MASK.notifyevent('SearchR')
       endif
       L_OBJ_MASK.MCALC(.T.)
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IMCODCON
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_IMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_IMTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODZON,ANTELEFO,ANCATSCM,ANSCORPO,ANNUMLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_IMTIPCON;
                     ,'ANCODICE',trim(this.w_IMCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCODZON,ANTELEFO,ANCATSCM,ANSCORPO,ANNUMLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_IMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_IMTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODZON,ANTELEFO,ANCATSCM,ANSCORPO,ANNUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_IMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_IMTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCODZON,ANTELEFO,ANCATSCM,ANSCORPO,ANNUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IMCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oIMCODCON_1_8'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_IMTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODZON,ANTELEFO,ANCATSCM,ANSCORPO,ANNUMLIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODZON,ANTELEFO,ANCATSCM,ANSCORPO,ANNUMLIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODZON,ANTELEFO,ANCATSCM,ANSCORPO,ANNUMLIS";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_IMTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODZON,ANTELEFO,ANCATSCM,ANSCORPO,ANNUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODZON,ANTELEFO,ANCATSCM,ANSCORPO,ANNUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_IMCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_IMTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_IMTIPCON;
                       ,'ANCODICE',this.w_IMCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODZON,ANTELEFO,ANCATSCM,ANSCORPO,ANNUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(60))
      this.w_ZONA = NVL(_Link_.ANCODZON,space(3))
      this.w_TELEFONO = NVL(_Link_.ANTELEFO,space(18))
      this.w_CATCLI = NVL(_Link_.ANCATSCM,space(5))
      this.w_FLSCOR = NVL(_Link_.ANSCORPO,space(1))
      this.w_NUMLIS = NVL(_Link_.ANNUMLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_IMCODCON = space(15)
      endif
      this.w_ANDESCRI = space(60)
      this.w_ZONA = space(3)
      this.w_TELEFONO = space(18)
      this.w_CATCLI = space(5)
      this.w_FLSCOR = space(1)
      this.w_NUMLIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.ANCODICE as ANCODICE108"+ ",link_1_8.ANDESCRI as ANDESCRI108"+ ",link_1_8.ANCODZON as ANCODZON108"+ ",link_1_8.ANTELEFO as ANTELEFO108"+ ",link_1_8.ANCATSCM as ANCATSCM108"+ ",link_1_8.ANSCORPO as ANSCORPO108"+ ",link_1_8.ANNUMLIS as ANNUMLIS108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on IMP_MAST.IMCODCON=link_1_8.ANCODICE"+" and IMP_MAST.IMTIPCON=link_1_8.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and IMP_MAST.IMCODCON=link_1_8.ANCODICE(+)"'+'+" and IMP_MAST.IMTIPCON=link_1_8.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IMLEGAME
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMLEGAME) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMLEGAME)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,CPROWORD";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_IMLEGAME);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_IMCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMCODICE;
                       ,'CPROWNUM',this.w_IMLEGAME)
            select IMCODICE,CPROWNUM,CPROWORD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMLEGAME = NVL(_Link_.CPROWNUM,0)
      this.w_LIVELLO = NVL(_Link_.CPROWORD,0)
    else
      if i_cCtrl<>'Load'
        this.w_IMLEGAME = 0
      endif
      this.w_LIVELLO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMLEGAME Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IM__SEDE
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IM__SEDE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_IM__SEDE)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_IMTIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_IMCODCON);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDPERSON,DDNOMDES,DDTELEFO,DDTIPRIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_IMTIPCON;
                     ,'DDCODICE',this.w_IMCODCON;
                     ,'DDCODDES',trim(this.w_IM__SEDE))
          select DDTIPCON,DDCODICE,DDCODDES,DDPERSON,DDNOMDES,DDTELEFO,DDTIPRIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IM__SEDE)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IM__SEDE) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oIM__SEDE_1_10'),i_cWhere,'',"Sedi",'gsag_des.DES_DIVE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_IMTIPCON<>oSource.xKey(1);
           .or. this.w_IMCODCON<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDPERSON,DDNOMDES,DDTELEFO,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDPERSON,DDNOMDES,DDTELEFO,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDPERSON,DDNOMDES,DDTELEFO,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_IMTIPCON);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_IMCODCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDPERSON,DDNOMDES,DDTELEFO,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IM__SEDE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDPERSON,DDNOMDES,DDTELEFO,DDTIPRIF";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_IM__SEDE);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_IMTIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_IMCODCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_IMTIPCON;
                       ,'DDCODICE',this.w_IMCODCON;
                       ,'DDCODDES',this.w_IM__SEDE)
            select DDTIPCON,DDCODICE,DDCODDES,DDPERSON,DDNOMDES,DDTELEFO,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IM__SEDE = NVL(_Link_.DDCODDES,space(5))
      this.w_IMPERSON = NVL(_Link_.DDPERSON,space(40))
      this.w_DDNOMDES = NVL(_Link_.DDNOMDES,space(40))
      this.w_TELEFO = NVL(_Link_.DDTELEFO,space(18))
      this.w_TIPRIF = NVL(_Link_.DDTIPRIF,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_IM__SEDE = space(5)
      endif
      this.w_IMPERSON = space(40)
      this.w_DDNOMDES = space(40)
      this.w_TELEFO = space(18)
      this.w_TIPRIF = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIF='CO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        endif
        this.w_IM__SEDE = space(5)
        this.w_IMPERSON = space(40)
        this.w_DDNOMDES = space(40)
        this.w_TELEFO = space(18)
        this.w_TIPRIF = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IM__SEDE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMMODATR
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODMATTR_IDX,3]
    i_lTable = "MODMATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODMATTR_IDX,2], .t., this.MODMATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODMATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMMODATR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MMA',True,'MODMATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_IMMODATR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_IMMODATR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMMODATR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStrODBC(trim(this.w_IMMODATR)+"%");

            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStr(trim(this.w_IMMODATR)+"%");

            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IMMODATR) and !this.bDontReportError
            deferred_cp_zoom('MODMATTR','*','MACODICE',cp_AbsName(oSource.parent,'oIMMODATR_2_8'),i_cWhere,'GSAR_MMA',"Modelli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMMODATR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_IMMODATR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_IMMODATR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMMODATR = NVL(_Link_.MACODICE,space(20))
      this.w_MADESCRI = NVL(_Link_.MADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_IMMODATR = space(20)
      endif
      this.w_MADESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODMATTR_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MODMATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMMODATR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODMATTR_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODMATTR_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.MACODICE as MACODICE208"+ ",link_2_8.MADESCRI as MADESCRI208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on IMP_DETT.IMMODATR=link_2_8.MACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and IMP_DETT.IMMODATR=link_2_8.MACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ZONA
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZONA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZONA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_ZONA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_ZONA)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZONA = NVL(_Link_.ZOCODZON,space(3))
      this.w_ZONDES = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ZONA = space(3)
      endif
      this.w_ZONDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZONA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IM__ZONA
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IM__ZONA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_IM__ZONA)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_IM__ZONA))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IM__ZONA)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IM__ZONA) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oIM__ZONA_1_14'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IM__ZONA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_IM__ZONA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_IM__ZONA)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IM__ZONA = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZONA = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_IM__ZONA = space(3)
      endif
      this.w_DESZONA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IM__ZONA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ZONE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.ZOCODZON as ZOCODZON114"+ ",link_1_14.ZODESZON as ZODESZON114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on IMP_MAST.IM__ZONA=link_1_14.ZOCODZON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and IMP_MAST.IM__ZONA=link_1_14.ZOCODZON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACODMOD,PACODNUM";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PACODMOD,PACODNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_CODMOD = NVL(_Link_.PACODMOD,space(10))
      this.w_CODNUM = NVL(_Link_.PACODNUM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_CODMOD = space(10)
      this.w_CODNUM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUDEF
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODDATTR_IDX,3]
    i_lTable = "MODDATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2], .t., this.MODDATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUDEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUDEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MAGRUDEF,MACODATT";
                   +" from "+i_cTable+" "+i_lTable+" where MAGRUDEF="+cp_ToStrODBC(this.w_GRUDEF);
                   +" and MACODICE="+cp_ToStrODBC(this.w_IMMODATR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_IMMODATR;
                       ,'MAGRUDEF',this.w_GRUDEF)
            select MACODICE,MAGRUDEF,MACODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUDEF = NVL(_Link_.MAGRUDEF,space(1))
      this.w_CODATT = NVL(_Link_.MACODATT,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_GRUDEF = space(1)
      endif
      this.w_CODATT = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)+'\'+cp_ToStr(_Link_.MAGRUDEF,1)
      cp_ShowWarn(i_cKey,this.MODDATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUDEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oIMCODICE_1_3.value==this.w_IMCODICE)
      this.oPgFrm.Page1.oPag.oIMCODICE_1_3.value=this.w_IMCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCRI_1_5.value==this.w_IMDESCRI)
      this.oPgFrm.Page1.oPag.oIMDESCRI_1_5.value=this.w_IMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oIM__NOTE_1_6.value==this.w_IM__NOTE)
      this.oPgFrm.Page1.oPag.oIM__NOTE_1_6.value=this.w_IM__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oIMCODCON_1_8.value==this.w_IMCODCON)
      this.oPgFrm.Page1.oPag.oIMCODCON_1_8.value=this.w_IMCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oIM__SEDE_1_10.value==this.w_IM__SEDE)
      this.oPgFrm.Page1.oPag.oIM__SEDE_1_10.value=this.w_IM__SEDE
    endif
    if not(this.oPgFrm.Page1.oPag.oIM_DNOTE_2_6.value==this.w_IM_DNOTE)
      this.oPgFrm.Page1.oPag.oIM_DNOTE_2_6.value=this.w_IM_DNOTE
      replace t_IM_DNOTE with this.oPgFrm.Page1.oPag.oIM_DNOTE_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIM__ZONA_1_14.value==this.w_IM__ZONA)
      this.oPgFrm.Page1.oPag.oIM__ZONA_1_14.value=this.w_IM__ZONA
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPERSON_1_16.value==this.w_IMPERSON)
      this.oPgFrm.Page1.oPag.oIMPERSON_1_16.value=this.w_IMPERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oIMTELEFO_1_19.value==this.w_IMTELEFO)
      this.oPgFrm.Page1.oPag.oIMTELEFO_1_19.value=this.w_IMTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDISTAN_1_20.value==this.w_IMDISTAN)
      this.oPgFrm.Page1.oPag.oIMDISTAN_1_20.value=this.w_IMDISTAN
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDATINI_1_22.value==this.w_IMDATINI)
      this.oPgFrm.Page1.oPag.oIMDATINI_1_22.value=this.w_IMDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDATFIN_1_24.value==this.w_IMDATFIN)
      this.oPgFrm.Page1.oPag.oIMDATFIN_1_24.value=this.w_IMDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_27.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_27.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDDNOMDES_1_28.value==this.w_DDNOMDES)
      this.oPgFrm.Page1.oPag.oDDNOMDES_1_28.value=this.w_DDNOMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESZONA_1_29.value==this.w_DESZONA)
      this.oPgFrm.Page1.oPag.oDESZONA_1_29.value=this.w_DESZONA
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDTOINI_2_9.value==this.w_IMDTOINI)
      this.oPgFrm.Page1.oPag.oIMDTOINI_2_9.value=this.w_IMDTOINI
      replace t_IMDTOINI with this.oPgFrm.Page1.oPag.oIMDTOINI_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDTOBSO_2_10.value==this.w_IMDTOBSO)
      this.oPgFrm.Page1.oPag.oIMDTOBSO_2_10.value=this.w_IMDTOBSO
      replace t_IMDTOBSO with this.oPgFrm.Page1.oPag.oIMDTOBSO_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMDESCON_2_4.value==this.w_IMDESCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMDESCON_2_4.value=this.w_IMDESCON
      replace t_IMDESCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMDESCON_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIVELLO_2_5.value==this.w_LIVELLO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIVELLO_2_5.value=this.w_LIVELLO
      replace t_LIVELLO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIVELLO_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMMODATR_2_8.value==this.w_IMMODATR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMMODATR_2_8.value=this.w_IMMODATR
      replace t_IMMODATR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMMODATR_2_8.value
    endif
    cp_SetControlsValueExtFlds(this,'IMP_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_IMCODICE) or not(IIF(.cFunction='Load',gsag_bgi(,'CKIMP',.w_IMCODICE),.T.)))  and (.w_CODNUM<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oIMCODICE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_IMCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Chiave gi� utilizzata")
          case   (empty(.w_IMCODCON))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oIMCODCON_1_8.SetFocus()
            i_bnoObbl = !empty(.w_IMCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPRIF='CO')  and not(empty(.w_IM__SEDE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oIM__SEDE_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (t_CPROWORD<>0 AND NOT EMPTY(t_IMDESCON) AND NOT EMPTY(t_IMMODATR));
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_IMDESCON) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_IMDESCON) AND NOT EMPTY(.w_IMMODATR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIMDESCON_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(chkriga(.w_OBJMSK, .w_LIVELLO,.F.)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_IMDESCON) AND NOT EMPTY(.w_IMMODATR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLIVELLO_2_5
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      i_bRes = i_bRes .and. .GSAR_MRA.CheckForm()
      if .w_CPROWORD<>0 AND NOT EMPTY(.w_IMDESCON) AND NOT EMPTY(.w_IMMODATR)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IMCODICE = this.w_IMCODICE
    this.o_IMCODCON = this.w_IMCODCON
    this.o_IMDESCON = this.w_IMDESCON
    this.o_IM__SEDE = this.w_IM__SEDE
    this.o_LINK = this.w_LINK
    this.o_IMMODATR = this.w_IMMODATR
    this.o_IM__ZONA = this.w_IM__ZONA
    this.o_READAZI = this.w_READAZI
    this.o_GRUDEF = this.w_GRUDEF
    * --- GSAR_MRA : Depends On
    this.GSAR_MRA.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 AND NOT EMPTY(t_IMDESCON) AND NOT EMPTY(t_IMMODATR))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=GSAG_BKT(this, CPROWNUM)
    if !i_bRes
      cp_ErrorMsg("Attenzione, riga padre impossibile cancellare","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_GESTGUID=space(14)
      .w_IMLEGAME=0
      .w_IMDESCON=space(50)
      .w_LIVELLO=0
      .w_IM_DNOTE=space(0)
      .w_LINK=.f.
      .w_IMMODATR=space(20)
      .w_IMDTOINI=ctod("  /  /  ")
      .w_IMDTOBSO=ctod("  /  /  ")
      .w_GEST=space(20)
      .w_GRUDEF=space(1)
      .w_CODATT=space(10)
      .w_MADESCRI=space(40)
      .w_GRUPPO=space(15)
      .w_RIGA=0
      .w_RET=.f.
      .w_DIPENDENZE=space(10)
      .DoRTCalc(1,9,.f.)
        .w_IMLEGAME = 0
      .DoRTCalc(10,10,.f.)
      if not(empty(.w_IMLEGAME))
        .link_2_3('Full')
      endif
      .DoRTCalc(11,15,.f.)
        .w_LINK = .T.
        .w_IMMODATR = IIF( empty(.w_IMMODATR) , .w_CODMOD , .w_IMMODATR )
      .DoRTCalc(17,17,.f.)
      if not(empty(.w_IMMODATR))
        .link_2_8('Full')
      endif
      .DoRTCalc(18,31,.f.)
        .w_GEST = .w_IMMODATR
      .DoRTCalc(33,36,.f.)
        .w_GRUDEF = 'S'
      .DoRTCalc(37,37,.f.)
      if not(empty(.w_GRUDEF))
        .link_2_12('Full')
      endif
      .DoRTCalc(38,46,.f.)
        .w_DIPENDENZE = "          "
    endwith
    this.DoRTCalc(48,53,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_GESTGUID = t_GESTGUID
    this.w_IMLEGAME = t_IMLEGAME
    this.w_IMDESCON = t_IMDESCON
    this.w_LIVELLO = t_LIVELLO
    this.w_IM_DNOTE = t_IM_DNOTE
    this.w_LINK = t_LINK
    this.w_IMMODATR = t_IMMODATR
    this.w_IMDTOINI = t_IMDTOINI
    this.w_IMDTOBSO = t_IMDTOBSO
    this.w_GEST = t_GEST
    this.w_GRUDEF = t_GRUDEF
    this.w_CODATT = t_CODATT
    this.w_MADESCRI = t_MADESCRI
    this.w_GRUPPO = t_GRUPPO
    this.w_RIGA = t_RIGA
    this.w_RET = t_RET
    this.w_DIPENDENZE = t_DIPENDENZE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_GESTGUID with this.w_GESTGUID
    replace t_IMLEGAME with this.w_IMLEGAME
    replace t_IMDESCON with this.w_IMDESCON
    replace t_LIVELLO with this.w_LIVELLO
    replace t_IM_DNOTE with this.w_IM_DNOTE
    replace t_LINK with this.w_LINK
    replace t_IMMODATR with this.w_IMMODATR
    replace t_IMDTOINI with this.w_IMDTOINI
    replace t_IMDTOBSO with this.w_IMDTOBSO
    replace t_GEST with this.w_GEST
    replace t_GRUDEF with this.w_GRUDEF
    replace t_CODATT with this.w_CODATT
    replace t_MADESCRI with this.w_MADESCRI
    replace t_GRUPPO with this.w_GRUPPO
    replace t_RIGA with this.w_RIGA
    replace t_RET with this.w_RET
    replace t_DIPENDENZE with this.w_DIPENDENZE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mimPag1 as StdContainer
  Width  = 763
  height = 550
  stdWidth  = 763
  stdheight = 550
  resizeXpos=556
  resizeYpos=333
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIMCODICE_1_3 as StdField with uid="ZQDLOWTANG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_IMCODICE", cQueryName = "IMCODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Chiave gi� utilizzata",;
    ToolTipText = "Codice impianto",;
    HelpContextID = 37135669,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=69, Top=11, InputMask=replicate('X',10)

  func oIMCODICE_1_3.mCond()
    with this.Parent.oContained
      if (.w_CODNUM<>'S')
        if .nLastRow>1
          return (.f.)
        endif
      else
        return (.f.)
      endif
    endwith
  endfunc

  proc oIMCODICE_1_3.mAfter
    with this.Parent.oContained
      .w_IMCODICE=CALCZER(.w_IMCODICE, 'IMP_MAST')
    endwith
  endproc

  func oIMCODICE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(.cFunction='Load',gsag_bgi(,'CKIMP',.w_IMCODICE),.T.))
      if .not. empty(.w_IMLEGAME)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oIMDESCRI_1_5 as StdField with uid="SQKBHFIMKT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_IMDESCRI", cQueryName = "IMDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 122721585,;
   bGlobalFont=.t.,;
    Height=21, Width=368, Left=154, Top=11, InputMask=replicate('X',50)

  add object oIM__NOTE_1_6 as StdMemo with uid="HLQSCPMFZR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IM__NOTE", cQueryName = "IM__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note dell'impianto",;
    HelpContextID = 193258805,;
   bGlobalFont=.t.,;
    Height=151, Width=230, Left=527, Top=11, TabStop=.f.

  add object oIMCODCON_1_8 as StdField with uid="UQMRLOJVLD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_IMCODCON", cQueryName = "IMCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Intestatario dell'impianto",;
    HelpContextID = 130636500,;
   bGlobalFont=.t.,;
    Height=21, Width=136, Left=70, Top=37, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_IMTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_IMCODCON"

  func oIMCODCON_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
      if .not. empty(.w_IM__SEDE)
        bRes2=.link_1_10('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIMCODCON_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMCODCON_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_IMTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_IMTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oIMCODCON_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oIMCODCON_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_IMTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_IMCODCON
    i_obj.ecpSave()
  endproc

  add object oIM__SEDE_1_10 as StdField with uid="URMAOGUDOI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_IM__SEDE", cQueryName = "IM__SEDE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice destinazione inesistente o di tipo diverso da consegna",;
    ToolTipText = "Sede in cui si trova l'impianto",;
    HelpContextID = 87352629,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=69, Top=63, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_IMTIPCON", oKey_2_1="DDCODICE", oKey_2_2="this.w_IMCODCON", oKey_3_1="DDCODDES", oKey_3_2="this.w_IM__SEDE"

  func oIM__SEDE_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oIM__SEDE_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIM__SEDE_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_IMTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_IMCODCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_IMTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_IMCODCON)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oIM__SEDE_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sedi",'gsag_des.DES_DIVE_VZM',this.parent.oContained
  endproc

  add object oIM__ZONA_1_14 as StdField with uid="SHPVKBHYDG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_IM__ZONA", cQueryName = "IM__ZONA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Zona",;
    HelpContextID = 87759559,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=69, Top=87, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_IM__ZONA"

  func oIM__ZONA_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oIM__ZONA_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIM__ZONA_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oIM__ZONA_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oIM__ZONA_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_IM__ZONA
    i_obj.ecpSave()
  endproc

  add object oIMPERSON_1_16 as StdField with uid="JUOQKFSVVO",rtseq=21,rtrep=.f.,;
    cFormVar = "w_IMPERSON", cQueryName = "IMPERSON",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Referente",;
    HelpContextID = 123721004,;
   bGlobalFont=.t.,;
    Height=21, Width=249, Left=69, Top=115, InputMask=replicate('X',40)

  add object oIMTELEFO_1_19 as StdField with uid="QQSFLIWVXR",rtseq=23,rtrep=.f.,;
    cFormVar = "w_IMTELEFO", cQueryName = "IMTELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Telefono",;
    HelpContextID = 96441643,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=69, Top=141, InputMask=replicate('X',18)

  add object oIMDISTAN_1_20 as StdField with uid="MVWOPBTIIX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_IMDISTAN", cQueryName = "IMDISTAN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Distanza in km",;
    HelpContextID = 105682220,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=277, Top=141, cSayPict='"9999"', cGetPict='"9999"'

  add object oIMDATINI_1_22 as StdField with uid="AUQCTMOQOO",rtseq=25,rtrep=.f.,;
    cFormVar = "w_IMDATINI", cQueryName = "IMDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit� impianto",;
    HelpContextID = 247163599,;
   bGlobalFont=.t.,;
    Height=21, Width=84, Left=437, Top=115

  add object oIMDATFIN_1_24 as StdField with uid="NDRLSEDXHJ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_IMDATFIN", cQueryName = "IMDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data obsolescenza impianto",;
    HelpContextID = 196831956,;
   bGlobalFont=.t.,;
    Height=21, Width=84, Left=437, Top=141

  add object oANDESCRI_1_27 as StdField with uid="PCWTZWUOOH",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 122721457,;
   bGlobalFont=.t.,;
    Height=21, Width=313, Left=209, Top=37, InputMask=replicate('X',60)

  add object oDDNOMDES_1_28 as StdField with uid="UHYORJOPWQ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DDNOMDES", cQueryName = "DDNOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 111541879,;
   bGlobalFont=.t.,;
    Height=21, Width=331, Left=139, Top=62, InputMask=replicate('X',40)

  add object oDESZONA_1_29 as StdField with uid="WRLECKZKTY",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESZONA", cQueryName = "DESZONA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 209366474,;
   bGlobalFont=.t.,;
    Height=21, Width=331, Left=139, Top=87, InputMask=replicate('X',35)


  add object oBtn_1_36 as StdButton with uid="ORSWOUHUED",left=711, top=187, width=48,height=45,;
    CpPicture="BMP\EVASIONE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'importazione dai gruppi componenti";
    , HelpContextID = 70497658;
    , TabStop=.f.,Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      do GSAG_KIM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_37 as StdButton with uid="HOLJBWYOWZ",left=711, top=235, width=48,height=45,;
    CpPicture="BMP\TREEVIEW.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza treview";
    , HelpContextID = 16670547;
    , Caption='Treevie\<w';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        do GSAG_BTW with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mCond()
    with this.Parent.oContained
      return (.cFunction='Query' and not empty(.w_IMCODCON))
    endwith
  endfunc


  add object oObj_1_38 as cp_runprogram with uid="KYOUDPBRHH",left=-5, top=656, width=344,height=23,;
    caption='GSAR_BCN(I)',;
   bGlobalFont=.t.,;
    prg="GSAR_BCN('I')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 125887436


  add object oObj_1_40 as cp_runprogram with uid="IPEKSAKJSD",left=-5, top=634, width=343,height=21,;
    caption='GSAG_BDL',;
   bGlobalFont=.t.,;
    prg="GSAG_BDL('D',w_KEYLISTB,Null,Space(5))",;
    cEvent = "Record Inserted,Record Updated,Edit Aborted",;
    nPag=1;
    , HelpContextID = 126795598


  add object oObj_1_45 as cp_runprogram with uid="FYNDWWRXGH",left=350, top=601, width=343,height=21,;
    caption='GSAG_BD2',;
   bGlobalFont=.t.,;
    prg="GSAG_BD2('MASTER')",;
    cEvent = "Insert end,Update end",;
    nPag=1;
    , HelpContextID = 126795624


  add object oObj_1_46 as cp_runprogram with uid="EBDNNNTYYO",left=350, top=624, width=343,height=21,;
    caption='GSAG_BD2',;
   bGlobalFont=.t.,;
    prg="GSAG_BD2('DETAIL')",;
    cEvent = "Insert row end,Update row end",;
    nPag=1;
    , HelpContextID = 126795624


  add object oObj_1_49 as cp_runprogram with uid="VBQHNWJPEX",left=350, top=576, width=343,height=21,;
    caption='GSAR_BCN(C)',;
   bGlobalFont=.t.,;
    prg="GSAR_BCN('C')",;
    cEvent = "w_IMCODCON Changed",;
    nPag=1;
    , HelpContextID = 125888972


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=168, width=694,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="N.riga",Field2="IMDESCON",Label2="Descrizione componente",Field3="LIVELLO",Label3="Legame",Field4="IMMODATR",Label4="Modello attributo",Field5="IMDTOBSO",Label5="Fine validit�",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 217251206


  add object oObj_1_52 as cp_runprogram with uid="EZNFNPZVQD",left=350, top=645, width=343,height=21,;
    caption='GSAG_BD5',;
   bGlobalFont=.t.,;
    prg="GSAG_BD5",;
    cEvent = "w_IMMODATR Changed",;
    nPag=1;
    , HelpContextID = 126795621


  add object oBtn_1_61 as StdButton with uid="IQHOMZBPEZ",left=474, top=62, width=48,height=45,;
    CpPicture="BMP\CONTRATT.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per gestire gli elementi contratto dell'impianto";
    , HelpContextID = 120507606;
    , TabStop=.f.,Caption='\<Contratti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_61.Click()
      this.parent.oContained.NotifyEvent("testata")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_61.mCond()
    with this.Parent.oContained
      return (.cFunction='Query')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="JDYCOXLSQG",Visible=.t., Left=6, Top=11,;
    Alignment=1, Width=62, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="QMJFVTFSXR",Visible=.t., Left=18, Top=37,;
    Alignment=1, Width=50, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="TXGUVZAQJH",Visible=.t., Left=37, Top=89,;
    Alignment=1, Width=31, Height=18,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="WHYPAGUQIN",Visible=.t., Left=11, Top=115,;
    Alignment=1, Width=57, Height=18,;
    Caption="Referente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="CXAAZIJDXE",Visible=.t., Left=211, Top=143,;
    Alignment=1, Width=65, Height=18,;
    Caption="Distanza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="VHHHFIOIUI",Visible=.t., Left=328, Top=115,;
    Alignment=1, Width=107, Height=18,;
    Caption="Data inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="XDTSVIWHDO",Visible=.t., Left=322, Top=141,;
    Alignment=1, Width=113, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="KFVPHYTMBK",Visible=.t., Left=30, Top=63,;
    Alignment=1, Width=38, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="XITEDBGWCO",Visible=.t., Left=17, Top=141,;
    Alignment=1, Width=51, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="PKAUYKESII",Visible=.t., Left=497, Top=344,;
    Alignment=1, Width=123, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="DMWFGLLDOK",Visible=.t., Left=297, Top=344,;
    Alignment=1, Width=107, Height=18,;
    Caption="Data inizio validit�:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_mra",lower(this.oContained.GSAR_MRA.class))=0
        this.oContained.GSAR_MRA.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=187,;
    width=691+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=188,width=690+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='MODMATTR|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oIM_DNOTE_2_6.Refresh()
      this.Parent.oIMDTOINI_2_9.Refresh()
      this.Parent.oIMDTOBSO_2_10.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='MODMATTR'
        oDropInto=this.oBodyCol.oRow.oIMMODATR_2_8
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oIM_DNOTE_2_6 as StdTrsMemo with uid="OVEWJCFFIZ",rtseq=15,rtrep=.t.,;
    cFormVar="w_IM_DNOTE",value=space(0),;
    ToolTipText = "Note componente dell'impianto",;
    HelpContextID = 195028277,;
    cTotal="", bFixedPos=.t., cQueryName = "IM_DNOTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=159, Width=230, Left=527, Top=388

  add object oIMDTOINI_2_9 as StdTrsField with uid="BSVKMGLCFU",rtseq=30,rtrep=.t.,;
    cFormVar="w_IMDTOINI",value=ctod("  /  /  "),;
    ToolTipText = "Data inizio validit� componente impianto",;
    HelpContextID = 243165903,;
    cTotal="", bFixedPos=.t., cQueryName = "IMDTOINI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=84, Left=407, Top=344

  add object oIMDTOBSO_2_10 as StdTrsField with uid="PXLLVLPIIE",rtseq=31,rtrep=.t.,;
    cFormVar="w_IMDTOBSO",value=ctod("  /  /  "),;
    ToolTipText = "Data obsolescenza componente impianto",;
    HelpContextID = 142710059,;
    cTotal="", bFixedPos=.t., cQueryName = "IMDTOBSO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=84, Left=622, Top=344

  add object oLinkPC_2_15 as stdDynamicChildContainer with uid="DAFIRLYKVU",bOnScreen=.t.,width=507,height=185,;
   left=6, top=365;


  add object oBtn_2_19 as StdButton with uid="AEDSPQOHCU",width=48,height=45,;
   left=711, top=283,;
    CpPicture="BMP\CONTRATT.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per gestire gli elementi contratto del componente dell'impianto";
    , HelpContextID = 120507606;
    , TabStop=.f.,Caption='\<Contratti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_19.Click()
      this.parent.oContained.NotifyEvent("riga")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_19.mCond()
    with this.Parent.oContained
      return (.cFunction='Query')
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsag_mimBodyRow as CPBodyRowCnt
  Width=681
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="RSLLPOYREG",rtseq=8,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 184922774,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oIMDESCON_2_4 as StdTrsField with uid="RWQRGTOBOK",rtseq=11,rtrep=.t.,;
    cFormVar="w_IMDESCON",value=space(50),;
    ToolTipText = "Descrizione componente",;
    HelpContextID = 145713876,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=414, Left=48, Top=0, InputMask=replicate('X',50), bHasZoom = .t. 

  proc oIMDESCON_2_4.mZoom
    do GSAG_KDS with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oLIVELLO_2_5 as StdTrsField with uid="IKHYUSUUCW",rtseq=14,rtrep=.t.,;
    cFormVar="w_LIVELLO",value=0,;
    HelpContextID = 247429450,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=467, Top=0, bHasZoom = .t. 

  proc oLIVELLO_2_5.mAfter
    with this.Parent.oContained
      .w_RET=chkriga(.w_OBJMSK, .w_LIVELLO,.T.)
    endwith
  endproc

  func oLIVELLO_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chkriga(.w_OBJMSK, .w_LIVELLO,.F.))
    endwith
    return bRes
  endfunc

  proc oLIVELLO_2_5.mZoom
      with this.Parent.oContained
        do GSAG_BIM with this.Parent.oContained
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oIMMODATR_2_8 as StdTrsField with uid="IEMEOPJDMD",rtseq=17,rtrep=.t.,;
    cFormVar="w_IMMODATR",value=space(20),;
    ToolTipText = "Modello attributo",;
    HelpContextID = 171312424,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=523, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="MODMATTR", cZoomOnZoom="GSAR_MMA", oKey_1_1="MACODICE", oKey_1_2="this.w_IMMODATR"

  func oIMMODATR_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
      if .not. empty(.w_GRUDEF)
        bRes2=.link_2_12('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIMMODATR_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oIMMODATR_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODMATTR','*','MACODICE',cp_AbsName(this.parent,'oIMMODATR_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MMA',"Modelli",'',this.parent.oContained
  endproc
  proc oIMMODATR_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_IMMODATR
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mim','IMP_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IMCODICE=IMP_MAST.IMCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_mim
proc ChiusuraMaschera(pParent)
   pParent.w_OBJMSK=.null.
endproc
* --- Fine Area Manuale
