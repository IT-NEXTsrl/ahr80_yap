* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_btt                                                        *
*              Generazione attivit�                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-09                                                      *
* Last revis.: 2016-09-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_btt",oParentObject,m.pPARAM)
return(i_retval)

define class tgsag_btt as StdBatch
  * --- Local variables
  pPARAM = space(1)
  w_OBJ = .NULL.
  w_CHIAVE = space(10)
  w_DBRK = space(10)
  w_RESULT = space(254)
  w_SERIALE = space(20)
  w_DESSUPP = space(0)
  w_DATATT = ctod("  /  /  ")
  w_GRUPPO = space(5)
  w_MODESATT = space(0)
  w_TIPOATT = space(20)
  w_CLIENTE = space(15)
  w_NEWDATE = ctod("  /  /  ")
  w_PERIODO = space(3)
  w_ROWNUM = 0
  w_ELECONT = .null.
  w_OGGETTO = space(254)
  w_INIZIO = ctod("  /  /  ")
  w_NOMINATIVO = space(15)
  w_CONTAELEM = 0
  w_CONTATTIV = 0
  w_ERROR = 0
  w_CODIMP = space(10)
  w_DESCRIZIONE = space(40)
  w_DURORE = 0
  w_DURMIN = 0
  w_TIPAPL = space(1)
  w_CODLIS = space(5)
  w_SEDE = space(5)
  w_TELEFONO = space(18)
  w_TIPART = space(2)
  w_ELINICOA = ctod("  /  /  ")
  w_ELFINCOA = ctod("  /  /  ")
  w_ELDATINI = ctod("  /  /  ")
  w_ELDATFIN = ctod("  /  /  ")
  w_CAUDOC = space(5)
  w_CAUACQ = space(5)
  w_ETIPRIG = space(1)
  w_RTIPRIG = space(1)
  w_DATIPRIG = space(1)
  w_ELGIOATT = 0
  w_ELCODART = space(20)
  w_ELCONTRA = space(10)
  w_IMDESCRI = space(50)
  w_IMDESCON = space(50)
  w_CODESCON = space(50)
  w_MOCHKDAT = space(1)
  w_ARVOCCEN = space(15)
  w_oERRORMESS = .NULL.
  w_FLSCOR = space(1)
  w_CADATINI = ctod("  /  /  ")
  w_INCMIN = 0
  w_INCORE = 0
  w_CODCOM = 0
  w_FLGLIS = space(1)
  w_KEYLISTB = space(10)
  w_CALPRZ = 0
  w_ELCAUDOC = space(5)
  w_VALRIG = 0
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_CONTA = 0
  w_DATATOLL = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_NEWCOMPATT1 = ctod("  /  /  ")
  w_NEWCOMPATT2 = ctod("  /  /  ")
  * --- WorkFile variables
  ELE_CONT_idx=0
  DET_GEN_idx=0
  DET_PIAN_idx=0
  DET_DOCU_idx=0
  GEST_ATTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Attivit� (lanciato da GSAG_AGA)
    do case
      case this.pPARAM="G"
        this.w_DBRK = "##zz##"
        this.w_ROWNUM = 0
        this.w_CONTAELEM = 0
        this.w_CONTATTIV = 0
        this.w_ERROR = 0
        this.w_KEYLISTB = Sys(2015)
        * --- Creo l'oggetto per scrivere le attivit�
        DECLARE ArrDb (4,1)
        this.w_oERRORMESS=createobject("AH_ErrorLog")
        this.w_OBJ = CREATEOBJECT("ActivityWriter",this,"OFF_ATTI")
        * --- Disabilito l'invio di messaggi
        this.w_OBJ.bSendMSG = .f.
        * --- Select from GSAG_BTT
        do vq_exec with 'GSAG_BTT',this,'_Curs_GSAG_BTT','',.f.,.t.
        if used('_Curs_GSAG_BTT')
          select _Curs_GSAG_BTT
          locate for 1=1
          do while not(eof())
          * --- Inserisco i dati in un memory cursor con gli stessi record della query gsag_btt
          *     per avere sempre il dettaglio elementi sempre a disposizione.
          this.w_CODESCON = _Curs_GSAG_BTT.CODESCON
          this.w_IMDESCRI = _Curs_GSAG_BTT.IMDESCRI
          this.w_ELDATINI = CP_TODATE(_Curs_GSAG_BTT.ELDATINI)
          this.w_ELDATFIN = CP_TODATE(_Curs_GSAG_BTT.ELDATFIN)
          this.w_MODESATT = _Curs_GSAG_BTT.MODESATT
          this.w_MOCHKDAT = _Curs_GSAG_BTT.MOCHKDAT
          this.w_TIPAPL = _Curs_GSAG_BTT.MOTIPAPL
          this.w_IMDESCON = _Curs_GSAG_BTT.IMDESCON
          this.w_CODLIS = _Curs_GSAG_BTT.ELCODLIS
          this.w_GRUPPO = _Curs_GSAG_BTT.ELGRUPAR
          this.w_TIPOATT = _Curs_GSAG_BTT.ELTIPATT
          this.w_CAUDOC = _Curs_GSAG_BTT.CACAUDOC
          this.w_CAUACQ = _Curs_GSAG_BTT.CACAUACQ
          this.w_CLIENTE = _Curs_GSAG_BTT.COCODCON
          this.w_PERIODO = _Curs_GSAG_BTT.ELPERATT 
          this.w_DATATT = CP_TODATE(_Curs_GSAG_BTT.ELDATATT)
          this.w_CODIMP = _Curs_GSAG_BTT.ELCODIMP
          this.w_DESCRIZIONE = _Curs_GSAG_BTT.ARDESART
          this.w_DESSUPP = _Curs_GSAG_BTT.ARDESSUP
          this.w_DURORE = _Curs_GSAG_BTT.CADURORE
          this.w_DURMIN = _Curs_GSAG_BTT.CADURMIN
          this.w_CADATINI = _Curs_GSAG_BTT.CADATINI
          this.w_SEDE = _Curs_GSAG_BTT.IM__SEDE
          this.w_TELEFONO = _Curs_GSAG_BTT.IMTELEFO
          this.w_TIPART = _Curs_GSAG_BTT.ARTIPART
          this.w_FLGLIS = _Curs_GSAG_BTT.CAFLGLIS
          this.w_ELCAUDOC = _Curs_GSAG_BTT.ELCAUDOC
          this.w_FLSCOR = _Curs_GSAG_BTT.ANSCORPO
          this.w_ELINICOA = CP_TODATE(_Curs_GSAG_BTT.ELINICOA)
          this.w_ELFINCOA = CP_TODATE(_Curs_GSAG_BTT.ELFINCOA)
          this.w_ELGIOATT = _Curs_GSAG_BTT.ELGIOATT
          this.w_ELCODART = _Curs_GSAG_BTT.ELCODART
          this.w_ELCONTRA = _Curs_GSAG_BTT.ELCONTRA
          this.w_ETIPRIG = iif(Not empty(NVL(this.w_CAUACQ,"")) and Not empty(NVL(this.w_CAUDOC,"")),"E",iif(Not empty(NVL(this.w_CAUACQ,"")),"A","D"))
          this.w_RTIPRIG = iif(Not empty(NVL(this.w_CAUACQ,"")) and Not empty(NVL(this.w_CAUDOC,"")),"E",iif(Not empty(NVL(this.w_CAUACQ,"")),"A","D"))
          this.w_DATIPRIG = IIF(Isahe(),this.w_ETIPRIG,this.w_RTIPRIG)
          this.w_ARVOCCEN = _Curs_GSAG_BTT.ARVOCCEN
          this.w_CHIAVE = DTOC(this.w_DATATT)+NVL(this.w_GRUPPO, SPACE(5))+NVL(this.w_TIPOATT, SPACE(20))+NVL(this.w_CLIENTE, SPACE(20))+NVL(this.w_CODIMP,SPACE(10)) +NVL(this.w_CODLIS, SPACE(5))
          if MOCHKDAT="S" AND this.oParentObject.w_ATFLRAGG="N"
            CREATE CURSOR APPOMESS ;
            (SERDES C(40),SERDE2 C(254), CONCOD C(10), CONDES C(50), IMPCOD C(10), IMPDES C(50), IMPCOM C(50),PERIOD C(3),DATATT D(8),DATVAD D(8), DATVAA D(8), DATTOL N(4,0), DATCOD D(8),DATCOA D(8))
            * --- Inserimento nel cursore di tutti i dati necessari
            INSERT INTO APPOMESS (SERDES,SERDE2, CONCOD, CONDES, IMPCOD, IMPDES, IMPCOM,PERIOD,DATATT,DATVAD, DATVAA, DATTOL, DATCOD,DATCOA) VALUES;
            (nvl(alltrim(this.w_DESCRIZIONE),""),NVL(alltrim(this.w_DESSUPP),""),nvl(alltrim(this.w_ELCONTRA),""),nvl(alltrim(this.w_CODESCON),""),nvl(alltrim(this.w_CODIMP),""),nvl(alltrim(this.w_IMDESCRI),""),nvl(alltrim(this.w_IMDESCON),"") ,alltrim(this.w_PERIODO),NVL ( this.w_DATATT,("  /  /  ")),NVL ( this.w_ELDATINI,("  /  /  ")),NVL ( this.w_ELDATFIN,("  /  /  ")),this.w_ELGIOATT,NVL ( this.w_ELINICOA,("  /  /  ")),NVL ( this.w_ELFINCOA,("  /  /  ")))
            this.w_MODESATT = CUR_TO_MSG( "APPOMESS" , "" , this.w_MODESATT, "" )
            this.w_DESCRIZIONE = strtran(SUBSTR(ltrim(this.w_MODESATT),1,40),CHR(13)+CHR(10),space(2))
            this.w_DESSUPP = IIF(len(alltrim(this.w_MODESATT)) >40,SUBSTR(ltrim(this.w_MODESATT),41,LEN(ALLTRIM(this.w_MODESATT))),this.w_DESSUPP)
          endif
          if this.w_CHIAVE<>this.w_DBRK
            this.w_CONTAELEM = this.w_CONTAELEM+1
            * --- Creo la nuova tabella 
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Sbianco l'oggetto e valuto una nuova attivit�
            this.w_OBJ.Blank()     
            if Isahe() 
              gsag_bdl(this,"D",this.w_KEYLISTB,this.w_oERRORMESS,Space(5))
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_RESULT=CALCLIST("CREA",@ArrDb,"V","N",Nvl(_Curs_GSAG_BTT.ELCODVAL,space(5)),"C",this.w_CLIENTE,this.w_DATATT," "," ",this.w_KEYLISTB,-20,this.w_FLSCOR," ",Space(5))
              if this.w_FLGLIS="S"
                this.w_OBJ.w_ATTCOLIS = _Curs_GSAG_BTT.ELTCOLIS
                this.w_OBJ.w_ATTSCLIS = _Curs_GSAG_BTT.ELSCOLIS
                this.w_OBJ.w_ATTPROLI = _Curs_GSAG_BTT.ELPROLIS
                this.w_OBJ.w_ATTPROSC = _Curs_GSAG_BTT.ELPROSCO
              endif
            endif
            this.w_OBJ.w_ATCAUATT = _Curs_GSAG_BTT.ELTIPATT
            this.w_OBJ.w_ATSTATUS = "T"
            this.w_OBJ.w_ATCODNOM = _Curs_GSAG_BTT.NOCODICE
            this.w_OBJ.w_ATNOTPIA = this.oParentObject.w_ATDESCRI
            this.w_OBJ.w_ATCODSED = this.w_SEDE
            this.w_OBJ.w_ATTELEFO = this.w_TELEFONO
            this.w_OBJ.w_ATCODLIS = this.w_CODLIS
            * --- Assegno la data all'attivit�...
            this.w_OBJ.w_ORAINI = IIF(EMPTY(this.w_CADATINI),"08",PADL(ALLTRIM(STR(HOUR(this.w_CADATINI),2)),2,"0"))
            this.w_OBJ.w_MININI = RIGHT("00"+ALLTRIM(STR(MINUTE(this.w_CADATINI),2)),2)
            this.w_OBJ.w_MINFIN = STR(MOD(VAL(this.w_OBJ.w_MININI) + this.w_DURMIN,60))
            this.w_INCMIN = INT((VAL(this.w_OBJ.w_MININI) + this.w_DURMIN)/60)
            this.w_OBJ.w_ORAFIN = STR(MOD(VAL(this.w_OBJ.w_ORAINI) +this.w_DURORE+this.w_INCMIN,24))
            this.w_INCORE = INT((VAL(this.w_OBJ.w_ORAINI) +this.w_DURORE+this.w_INCMIN)/24)
            this.w_OBJ.w_ATDATINI = cp_chartodatetime("")
            this.w_OBJ.w_ATDATFIN = cp_chartodatetime("")
            this.w_OBJ.w_ATDATDOC = this.w_DATATT
            this.w_OBJ.w_DATINI = this.w_DATATT
            this.w_OBJ.w_DATFIN = this.w_DATATT+this.w_INCORE
            this.w_OBJ.SyncDate()     
            this.w_OBJ.AddPart(0,0,"G",this.w_GRUPPO,"")     
          endif
          this.w_ELECONT.AppendBlank()     
          this.w_ELECONT.ELCODMOD = _Curs_GSAG_BTT.ELCODMOD
          this.w_ELECONT.ELCONTRA = _Curs_GSAG_BTT.ELCONTRA
          this.w_ELECONT.ELCODIMP = _Curs_GSAG_BTT.ELCODIMP
          * --- Codice componente...
          this.w_CODCOM = _Curs_GSAG_BTT.ELCODCOM
          this.w_ELECONT.ELCODCOM = this.w_CODCOM
          this.w_ELECONT.ELRINNOV = _Curs_GSAG_BTT.ELRINNOV
          this.w_ELECONT.ELDATATT = _Curs_GSAG_BTT.ELDATATT
          this.w_ELECONT.ELPERATT = _Curs_GSAG_BTT.ELPERATT
          this.w_ELECONT.ELGIOATT = _Curs_GSAG_BTT.ELGIOATT
          this.w_ELECONT.ELDATGAT = _Curs_GSAG_BTT.ELDATGAT
          this.w_ELECONT.ELCODART = _Curs_GSAG_BTT.ELCODART
          this.w_ELECONT.ELUNIMIS = _Curs_GSAG_BTT.ELUNIMIS
          this.w_ELECONT.ELQTAMOV = _Curs_GSAG_BTT.ELQTAMOV
          this.w_ELECONT.ELDATFIN = _Curs_GSAG_BTT.ELDATFIN
          * --- Analitica
          this.w_ELECONT.ELVOCCEN = _Curs_GSAG_BTT.ELVOCCEN
          this.w_ELECONT.ELCODCEN = _Curs_GSAG_BTT.ELCODCEN
          this.w_ELECONT.ELCOCOMM = _Curs_GSAG_BTT.ELCOCOMM
          this.w_ELECONT.ELCODATT = _Curs_GSAG_BTT.ELCODATT
          this.w_ELECONT.ELINICOA = _Curs_GSAG_BTT.ELINICOA
          this.w_ELECONT.ELFINCOA = _Curs_GSAG_BTT.ELFINCOA
          if !Isahe()
            this.w_ELECONT.ELCONCOD = Nvl(_Curs_GSAG_BTT.ELCONCOD,"")
            if this.w_TIPAPL="C"
              * --- Se applicazione prezzo immediata
              this.w_ELECONT.ELPREZZO = Nvl(_Curs_GSAG_BTT.ELPREZZO,0)
              this.w_ELECONT.ELSCONT1 = Nvl(_Curs_GSAG_BTT.ELSCONT1,0)
              this.w_ELECONT.ELSCONT2 = Nvl(_Curs_GSAG_BTT.ELSCONT2,0)
              this.w_ELECONT.ELSCONT3 = Nvl(_Curs_GSAG_BTT.ELSCONT3,0)
              this.w_ELECONT.ELSCONT4 = Nvl(_Curs_GSAG_BTT.ELSCONT4,0)
              this.w_VALRIG = CAVALRIG(this.w_ELECONT.ELPREZZO,Nvl(_Curs_GSAG_BTT.ELQTAMOV,0), this.w_ELECONT.ELSCONT1,this.w_ELECONT.ELSCONT2,this.w_ELECONT.ELSCONT3,this.w_ELECONT.ELSCONT4, g_PERPVL)
            else
              * --- Se applicazione prezzo differita
              DECLARE L_ArrRet (16,1)
              this.w_ELECONT.ELPREZZO = gsar_bpz(this,Nvl(_Curs_GSAG_BTT.ELCODART,Space(20)),this.w_CLIENTE,"C",this.w_CODLIS,Nvl(_Curs_GSAG_BTT.ELQTAMOV,0),Nvl(_Curs_GSAG_BTT.ELCODVAL,space(5)),this.w_OBJ.w_ATDATINI,Nvl(_Curs_GSAG_BTT.ELUNIMIS,Space(3)), _Curs_GSAG_BTT.ELCONCOD)
              this.w_ELECONT.ELSCONT1 = L_ArrRet[1,1]
              this.w_ELECONT.ELSCONT2 = L_ArrRet[2,1]
              this.w_ELECONT.ELSCONT3 = L_ArrRet[3,1]
              this.w_ELECONT.ELSCONT4 = L_ArrRet[4,1]
              this.w_ELECONT.ELCONCOD = L_ArrRet[9,1]
              this.w_VALRIG = CAVALRIG(this.w_ELECONT.ELPREZZO,Nvl(_Curs_GSAG_BTT.ELQTAMOV,0), this.w_ELECONT.ELSCONT1,this.w_ELECONT.ELSCONT2,this.w_ELECONT.ELSCONT3,this.w_ELECONT.ELSCONT4, g_PERPVL)
              RELEASE L_ArrRet
            endif
          else
            * --- Valorizzo lisitini attivit�
            if Not empty(this.w_ELCAUDOC)
               
 DECLARE ARRET (10,1) 
 ARRET[1]= "" 
 ARRET[2]= "" 
 ARRET[3]= "" 
 ARRET[4]= "" 
 ARRET[5]=0 
 ARRET[6]=0 
 ARRET[7]=0 
 ARRET[8]=0 
 ARRET[9]=0 
 ARRET[10]=0
              * --- Azzero l'Array che verr� riempito dalla Funzione
               
 this.w_CALPRZ=gsar_bpz(This,_Curs_GSAG_BTT.ELTCOLIS,_Curs_GSAG_BTT.ELSCOLIS,_Curs_GSAG_BTT.ELPROLIS,; 
 _Curs_GSAG_BTT.ELPROSCO,Nvl(_Curs_GSAG_BTT.ELCODART,Space(20)),"C",this.w_CLIENTE,Nvl(_Curs_GSAG_BTT.ELQTAMOV,0),; 
 Nvl(_Curs_GSAG_BTT.ELCODVAL,space(5)),this.w_OBJ.w_ATDATINI,Nvl(_Curs_GSAG_BTT.ELUNIMIS,Space(3)),; 
 this.w_ELCAUDOC,"N",this.w_KEYLISTB,@ARRET)
              this.w_ELECONT.ELTCOLIS = ARRET[1]
              this.w_ELECONT.ELSCOLIS = ARRET[3]
              this.w_ELECONT.ELPROLIS = ARRET[2]
              this.w_ELECONT.ELPREZZO = ARRET[5]
              this.w_ELECONT.ELPROSCO = ARRET[4]
              this.w_ELECONT.ELSCONT1 = ARRET[6]
              this.w_ELECONT.ELSCONT2 = ARRET[7]
              this.w_ELECONT.ELSCONT3 = ARRET[8]
              this.w_ELECONT.ELSCONT4 = ARRET[9]
              this.w_VALRIG = ARRET[10]
            endif
          endif
          this.w_ELECONT.SaveCurrentRecord()     
          this.w_DBRK = this.w_CHIAVE
          * --- Valorizzo i campi dell'attivit�...
          * --- Se raggruppo per prestazione, cerco se esiste nel memorycursor una riga
          *     che ho gi� inserito.
          *     Se la trovo, aggiungo nulla ma sommo le quantit�
          * --- Occorre ignorare il check "Raggruppa per prestazione" per i servizi di tipo "a valore". 
          *     Tali servizi non sono raggruppabili perch� le quantit� non sono cumulabili 
          *     (ogni riga avr� sempre come quantit� 1): per ognuno di essi deve essere
          *     creata una riga distinta, mentre per gli altri servizi potr� essere effettuato 
          *     il raggruppamento (quelli a quantit� e valore). 
          if this.oParentObject.w_ATFLRAGG="S" AND this.w_TIPART="FM"
            Select (this.w_OBJ.w_OFFDATTI.ccursor) 
 locate for this.w_ELECONT.ELCODART=iif(Isahe(),DACODICE,DACODATT) and this.w_ELECONT.ELUNIMIS=DAUNIMIS and this.w_ELECONT.ELPREZZO=DAPREZZO ; 
 and nvl(this.w_ELECONT.ELSCONT1,0)=DASCONT1 and nvl(this.w_ELECONT.ELSCONT2,0)=DASCONT2 and nvl(this.w_ELECONT.ELSCONT3,0)=DASCONT3 and nvl(this.w_ELECONT.ELSCONT4,0)=DASCONT4; 
 and nvl(this.w_ELECONT.ELCODCEN, space(15))=DACENRIC and nvl( this.w_ELECONT.ELVOCCEN,space(15))=DAVOCRIC and nvl(this.w_ELECONT.ELCOCOMM,space(15))= DACOMRIC and nvl(this.w_ELECONT.ELCODATT,space(15))=DAATTRIC
            if ! found()
              this.w_OBJ.AddDetail(0,0,"",NVL(this.w_ELECONT.ELCODART,SPACE(20)),this.w_DESCRIZIONE,NVL(this.w_ELECONT.ELUNIMIS,SPACE(5)),NVL(this.w_ELECONT.ELQTAMOV,0),NVL(this.w_ELECONT.ELPREZZO,0),"D",i_CODUTE,i_datsys,"",this.w_DATIPRIG,this.w_DATIPRIG)     
            else
              this.w_OBJ.w_OFFDATTI.ReadCurrentRecord()     
              this.w_OBJ.w_OFFDATTI.DAQTAMOV = this.w_ELECONT.ELQTAMOV+this.w_OBJ.w_OFFDATTI.DAQTAMOV
            endif
          else
            this.w_OBJ.AddDetail(0,0,"",NVL(this.w_ELECONT.ELCODART,SPACE(20)),this.w_DESCRIZIONE,NVL(this.w_ELECONT.ELUNIMIS,SPACE(5)),NVL(this.w_ELECONT.ELQTAMOV,0),NVL(this.w_ELECONT.ELPREZZO,0),"D",i_CODUTE,i_datsys,"",this.w_DATIPRIG,this.w_DATIPRIG)     
          endif
          * --- Analitiica
          this.w_OBJ.w_OFFDATTI.DACENRIC = this.w_ELECONT.ELCODCEN
          this.w_OBJ.w_OFFDATTI.DACOMRIC = this.w_ELECONT.ELCOCOMM
          this.w_OBJ.w_OFFDATTI.DAATTRIC = this.w_ELECONT.ELCODATT
          this.w_OBJ.w_OFFDATTI.DACENCOS = this.w_ELECONT.ELCODCEN
          this.w_OBJ.w_OFFDATTI.DACODCOM = this.w_ELECONT.ELCOCOMM
          this.w_OBJ.w_OFFDATTI.DAATTIVI = this.w_ELECONT.ELCODATT
          this.w_OBJ.w_OFFDATTI.DAVOCCOS = SearchVoc(this,"C",this.w_ELECONT.ELCODART,_Curs_GSAG_BTT.NOTIPCLI,_Curs_GSAG_BTT.NOCODCLI,_Curs_GSAG_BTT.CACAUDOC,_Curs_GSAG_BTT.NOCODICE)
          this.w_OBJ.w_OFFDATTI.DAVOCRIC = iif(NOT EMPTY(NVL(_Curs_GSAG_BTT.ELVOCCEN,SPACE(15))),_Curs_GSAG_BTT.ELVOCCEN,SearchVoc(this,"R",this.w_ELECONT.ELCODART,_Curs_GSAG_BTT.NOTIPCLI,_Curs_GSAG_BTT.NOCODCLI,_Curs_GSAG_BTT.CACAUDOC,_Curs_GSAG_BTT.NOCODICE))
          this.w_OBJ.w_OFFDATTI.DAOREEFF = IIF(Nvl(_Curs_GSAG_BTT.UMFLTEMP,"N")="S", INT(Nvl(_Curs_GSAG_BTT.UMDURORE,0) * this.w_OBJ.w_OFFDATTI.DAQTAMOV), IIF(_Curs_GSAG_BTT.ARTIPART="DE", 0, 1))
          this.w_OBJ.w_OFFDATTI.DAMINEFF = IIF(Nvl(_Curs_GSAG_BTT.UMFLTEMP,"N")="S", INT((Nvl(_Curs_GSAG_BTT.UMDURORE,0) * this.w_OBJ.w_OFFDATTI.DAQTAMOV- INT(Nvl(_Curs_GSAG_BTT.UMDURORE,0) * this.w_OBJ.w_OFFDATTI.DAQTAMOV)) * 60), 0)
          this.w_OBJ.w_OFFDATTI.DASCONT1 = this.w_ELECONT.ELSCONT1
          this.w_OBJ.w_OFFDATTI.DASCONT2 = this.w_ELECONT.ELSCONT2
          this.w_OBJ.w_OFFDATTI.DASCONT3 = this.w_ELECONT.ELSCONT3
          this.w_OBJ.w_OFFDATTI.DASCONT4 = this.w_ELECONT.ELSCONT4
          this.w_OBJ.w_OFFDATTI.DAVALRIG = this.w_VALRIG
          this.w_OBJ.w_OFFDATTI.DADESAGG = this.w_DESSUPP
          this.w_OBJ.w_OFFDATTI.DACONCOD = this.w_ELECONT.ELCONCOD
          this.w_OBJ.w_OFFDATTI.DAINICOM = this.w_ELECONT.ELINICOA
          this.w_OBJ.w_OFFDATTI.DAFINCOM = this.w_ELECONT.ELFINCOA
          this.w_OBJ.w_OFFDATTI.DATCOINI = this.w_ELECONT.ELINICOA
          this.w_OBJ.w_OFFDATTI.DATCOFIN = this.w_ELECONT.ELFINCOA
          this.w_OBJ.w_OFFDATTI.DATRIINI = this.w_ELECONT.ELINICOA
          this.w_OBJ.w_OFFDATTI.DATRIFIN = this.w_ELECONT.ELFINCOA
          this.w_OBJ.w_OFFDATTI.DATIPRIG = this.w_DATIPRIG
          if this.w_FLGLIS="N"
            this.w_OBJ.w_OFFDATTI.DACODLIS = this.w_ELECONT.ELTCOLIS
            this.w_OBJ.w_OFFDATTI.DASCOLIS = this.w_ELECONT.ELSCOLIS
            this.w_OBJ.w_OFFDATTI.DAPROLIS = this.w_ELECONT.ELPROLIS
            this.w_OBJ.w_OFFDATTI.DAPROSCO = this.w_ELECONT.ELPROSCO
          endif
          this.w_OBJ.w_OFFDATTI.SaveCurrentRecord()     
          if Not Empty( this.w_CODIMP )
            * --- Ricerco impianto e componente all'interno del figlio dell'attivit�, se non
            *     presenti aggiungo una riga
             
 Select (this.w_OBJ.w_COM_ATTI.ccursor) 
 Go Top
            locate for CACODIMP=this.w_CODIMP and CACODCOM=this.w_CODCOM
            if ! found()
              this.w_OBJ.AddComp(0,NVL( this.w_CODIMP , SPACE(10)), NVL(this.w_CODCOM, 0 ))     
            endif
            this.w_OBJ.w_OFFDATTI.ReadCurrentRecord()     
          endif
            select _Curs_GSAG_BTT
            continue
          enddo
          use
        endif
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Inserisco i dati delle attivit� generate
        * --- Insert into DET_PIAN
        i_nConn=i_TableProp[this.DET_PIAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DET_PIAN_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSAG2BTT",this.DET_PIAN_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_OBJ = .NULL.
        if this.w_CONTAELEM=0
          ah_ErrorMsg("Per le selezioni impostate, non ci sono dati da elaborare")
        else
          * --- oggetto per mess. incrementali
          this.w_oMESS=createobject("ah_message")
          if this.w_CONTAELEM<>0 
            this.w_oPART = this.w_oMESS.addmsgpartNL("Generate n. %1 attivit�%0su n. %2 da generare")
            this.w_oPART.addParam(ALLTRIM(STR(this.w_CONTATTIV)))     
            this.w_oPART.addParam(ALLTRIM(STR(this.w_CONTAELEM)))     
          endif
          this.w_oMESS.ah_ErrorMsg()     
          if this.w_ERROR>0
            this.w_oERRORMESS.PrintLog(this,"Errori /warning riscontrati")     
          endif
        endif
      otherwise
        this.oParentObject.w_HASEVENT = Ah_yesno("Si desidera cancellare il piano di generazione attivit�?")
        if this.oParentObject.w_HASEVENT
          * --- Select from GSAG4BTT
          do vq_exec with 'GSAG4BTT',this,'_Curs_GSAG4BTT','',.f.,.t.
          if used('_Curs_GSAG4BTT')
            select _Curs_GSAG4BTT
            locate for 1=1
            do while not(eof())
            this.w_CONTA = NVL(CONTA,0)
              select _Curs_GSAG4BTT
              continue
            enddo
            use
          endif
          if this.w_CONTA>0
            Ah_ErrorMsg("Impossibile cancellare: esiste almeno un altro piano di generazione attivit� successivo")
            this.oParentObject.w_HASEVENT = .F.
          endif
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_DBRK<>"##zz##"
      * --- Try
      local bErr_04DDDAC0
      bErr_04DDDAC0=bTrsErr
      this.Try_04DDDAC0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_ERROR = this.w_ERROR+1
        this.w_RESULT = iif(Empty(this.w_RESULT),Message(),this.w_RESULT)
        this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Riscontrato errore su attivit� %1%0%2", space(4)+ALLTRIM(this.w_CHIAVE),ALLTRIM(this.w_RESULT))     
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_04DDDAC0
      * --- End
    endif
  endproc
  proc Try_04DDDAC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_RESULT = this.w_OBJ.CreateActivity()
    if EMPTY(this.w_RESULT)
      this.w_SERIALE = this.w_OBJ.w_ATSERIAL
      this.w_ELECONT.GoTop()     
      do while not this.w_ELECONT.Eof()
        if NOT EMPTY(this.w_ELECONT.ELPERATT)
          this.w_NEWDATE = NextTime(this.w_ELECONT.ELPERATT, this.w_ELECONT.ELDATATT, .F.,.T.)
          this.w_DATFIN = IIF(Type("w_ELECONT.ELDATFIN")="T",TTOD(this.w_ELECONT.ELDATFIN),cp_todate(this.w_ELECONT.ELDATFIN))
          this.w_DATATOLL = IIF(this.w_NEWDATE>this.w_DATFIN+this.w_ELECONT.ELGIOATT, CP_CHARTODATE("  -  -  "), this.w_NEWDATE)
          this.w_NEWCOMPATT1 = IIF(EMPTY(this.w_DATATOLL),CP_CHARTODATE("  -  -  "),CP_TODATE(this.w_ELECONT.ELFINCOA)+1)
          this.w_NEWCOMPATT2 = iif(empty(this.w_DATATOLL),CP_CHARTODATE("  -  -  "),NEXTTIME( this.w_ELECONT.ELPERATT, CP_TODATE(this.w_NEWCOMPATT1)-1,.F.,.T. ))
          * --- Write into ELE_CONT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ELE_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ELDATATT ="+cp_NullLink(cp_ToStrODBC(this.w_DATATOLL),'ELE_CONT','ELDATATT');
            +",ELINICOA ="+cp_NullLink(cp_ToStrODBC(this.w_NEWCOMPATT1),'ELE_CONT','ELINICOA');
            +",ELFINCOA ="+cp_NullLink(cp_ToStrODBC(this.w_NEWCOMPATT2),'ELE_CONT','ELFINCOA');
                +i_ccchkf ;
            +" where ";
                +"ELCONTRA = "+cp_ToStrODBC(this.w_ELECONT.ELCONTRA);
                +" and ELCODMOD = "+cp_ToStrODBC(this.w_ELECONT.ELCODMOD);
                +" and ELCODIMP = "+cp_ToStrODBC(this.w_ELECONT.ELCODIMP);
                +" and ELCODCOM = "+cp_ToStrODBC(this.w_ELECONT.ELCODCOM);
                +" and ELPERATT = "+cp_ToStrODBC(this.w_ELECONT.ELPERATT);
                +" and ELDATATT = "+cp_ToStrODBC(this.w_ELECONT.ELDATATT);
                +" and ELRINNOV = "+cp_ToStrODBC(this.w_ELECONT.ELRINNOV);
                   )
          else
            update (i_cTable) set;
                ELDATATT = this.w_DATATOLL;
                ,ELINICOA = this.w_NEWCOMPATT1;
                ,ELFINCOA = this.w_NEWCOMPATT2;
                &i_ccchkf. ;
             where;
                ELCONTRA = this.w_ELECONT.ELCONTRA;
                and ELCODMOD = this.w_ELECONT.ELCODMOD;
                and ELCODIMP = this.w_ELECONT.ELCODIMP;
                and ELCODCOM = this.w_ELECONT.ELCODCOM;
                and ELPERATT = this.w_ELECONT.ELPERATT;
                and ELDATATT = this.w_ELECONT.ELDATATT;
                and ELRINNOV = this.w_ELECONT.ELRINNOV;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Insert into DET_GEN
        i_nConn=i_TableProp[this.DET_GEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DET_GEN_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_GEN_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MDSERIAL"+",MDSERATT"+",MDCODIMP"+",MDCODMOD"+",MDCOMPON"+",MDCONTRA"+",MDOLDDAT"+",MDDATGAT"+",MDRINNOV"+",MDINICOM"+",MDFINCOM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ATSERIAL),'DET_GEN','MDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'DET_GEN','MDSERATT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELCODIMP),'DET_GEN','MDCODIMP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELCODMOD),'DET_GEN','MDCODMOD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELCODCOM),'DET_GEN','MDCOMPON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELCONTRA),'DET_GEN','MDCONTRA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELDATATT),'DET_GEN','MDOLDDAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELDATGAT),'DET_GEN','MDDATGAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELRINNOV),'DET_GEN','MDRINNOV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELINICOA),'DET_GEN','MDINICOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELFINCOA),'DET_GEN','MDFINCOM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MDSERIAL',this.oParentObject.w_ATSERIAL,'MDSERATT',this.w_SERIALE,'MDCODIMP',this.w_ELECONT.ELCODIMP,'MDCODMOD',this.w_ELECONT.ELCODMOD,'MDCOMPON',this.w_ELECONT.ELCODCOM,'MDCONTRA',this.w_ELECONT.ELCONTRA,'MDOLDDAT',this.w_ELECONT.ELDATATT,'MDDATGAT',this.w_ELECONT.ELDATGAT,'MDRINNOV',this.w_ELECONT.ELRINNOV,'MDINICOM',this.w_ELECONT.ELINICOA,'MDFINCOM',this.w_ELECONT.ELFINCOA)
          insert into (i_cTable) (MDSERIAL,MDSERATT,MDCODIMP,MDCODMOD,MDCOMPON,MDCONTRA,MDOLDDAT,MDDATGAT,MDRINNOV,MDINICOM,MDFINCOM &i_ccchkf. );
             values (;
               this.oParentObject.w_ATSERIAL;
               ,this.w_SERIALE;
               ,this.w_ELECONT.ELCODIMP;
               ,this.w_ELECONT.ELCODMOD;
               ,this.w_ELECONT.ELCODCOM;
               ,this.w_ELECONT.ELCONTRA;
               ,this.w_ELECONT.ELDATATT;
               ,this.w_ELECONT.ELDATGAT;
               ,this.w_ELECONT.ELRINNOV;
               ,this.w_ELECONT.ELINICOA;
               ,this.w_ELECONT.ELFINCOA;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_ELECONT.Delete()     
        this.w_ELECONT.Skip(1)     
      enddo
      this.w_CONTATTIV = this.w_CONTATTIV+1
      * --- commit
      cp_EndTrs(.t.)
    else
      * --- Se non viene generata l'attivit� per un qualunque motivo
      *     cancello il contenuto del memory cursor.
      do while not this.w_ELECONT.Eof()
        this.w_ELECONT.Delete()     
        this.w_ELECONT.Skip(1)     
      enddo
      * --- Raise
      i_Error="Operazione abbandonata"
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    this.w_ELECONT=createobject("cp_MemoryCursor","ELE_CONT",this)
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ELE_CONT'
    this.cWorkTables[2]='DET_GEN'
    this.cWorkTables[3]='DET_PIAN'
    this.cWorkTables[4]='DET_DOCU'
    this.cWorkTables[5]='GEST_ATTI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSAG_BTT')
      use in _Curs_GSAG_BTT
    endif
    if used('_Curs_GSAG4BTT')
      use in _Curs_GSAG4BTT
    endif
    return
  proc RemoveMemoryCursors()
    this.w_ELECONT.Done()
  endproc
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
