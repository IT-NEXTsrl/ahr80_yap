* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bcl                                                        *
*              Calcola prezzo listino                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-24                                                      *
* Last revis.: 2013-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bcl",oParentObject,m.pExec)
return(i_retval)

define class tgsag_bcl as StdBatch
  * --- Local variables
  pExec = space(1)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_QTAUM1 = 0
  w_CLUNIMIS = space(3)
  w_CALPRZ = 0
  w_OK = ctod("  /  /  ")
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_PREZZO = 0
  w_OLDPREZZO = 0
  w_OLDSCONT1 = 0
  w_OLDSCONT2 = 0
  w_OLDSCONT3 = 0
  w_OLDSCONT4 = 0
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_CONCOD = space(15)
  w_ARTICOLO = space(20)
  w_LISTINO = space(5)
  w_DATAVAL = ctod("  /  /  ")
  w_DATFAT = ctod("  /  /  ")
  w_DATATT = ctod("  /  /  ")
  w_QTALIS = 0
  w_RICPRE = .f.
  w_RICSCO = .f.
  w_TIPRIS = space(1)
  w_CALPRZ = 0
  w_COND = .f.
  w_DATALIST = ctod("  /  /  ")
  w_COESCRIN = space(1)
  w_COPERCON = space(3)
  w_CORINCON = space(1)
  w_CONUMGIO = 0
  w_MOESCRIN = space(1)
  w_MOPERCON = space(3)
  w_MORINCON = space(1)
  w_MONUMGIO = 0
  w_TROVA = .f.
  w_TEST = .f.
  w_ATDATINI = ctod("  /  /  ")
  w_ATDATFIN = ctod("  /  /  ")
  * --- WorkFile variables
  LISTINI_idx=0
  LIST_TMP_idx=0
  CONTI_idx=0
  MOD_ELEM_idx=0
  CON_TRAS_idx=0
  MODCLDAT_idx=0
  ART_ICOL_idx=0
  UNIMIS_idx=0
  CON_TRAM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola il prezzo sugli elementi contratti
    * --- C calcola listini
    *     T crea temporaneo listini
    do case
      case this.pExec$"CZ"
        if Isahr()
          * --- istanzio oggetto per mess incrementali
          this.w_oMESS=createobject("ah_message")
          DECLARE ARRCALC (16,1)
          * --- Azzero l'Array che verr� riempito dalla Funzione
          ARRCALC(1)=0
          if not empty(this.oParentObject.w_CODART) and empty(this.oParentObject.w_ELCODART)
            this.w_ARTICOLO = this.oParentObject.w_CODART
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARPREZUM,ARUNMIS1,ARUNMIS2,ARFLSERG,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP,ARTIPART"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_ARTICOLO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARPREZUM,ARUNMIS1,ARUNMIS2,ARFLSERG,AROPERAT,ARMOLTIP,ARCATSCM,ARFLUSEP,ARTIPART;
                from (i_cTable) where;
                    ARCODART = this.w_ARTICOLO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
              this.oParentObject.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
              this.oParentObject.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
              this.oParentObject.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
              this.oParentObject.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
              this.oParentObject.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
              this.oParentObject.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
              this.oParentObject.w_CATART = NVL(cp_ToDate(_read_.ARCATSCM),cp_NullValue(_read_.ARCATSCM))
              this.oParentObject.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
              this.oParentObject.w_ELUNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
              this.oParentObject.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from UNIMIS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.UNIMIS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "UMFLFRAZ,UMMODUM2"+;
                " from "+i_cTable+" UNIMIS where ";
                    +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UNMIS1);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                UMFLFRAZ,UMMODUM2;
                from (i_cTable) where;
                    UMCODICE = this.oParentObject.w_UNMIS1;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_FLFRAZ1 = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
              this.oParentObject.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.oParentObject.w_ELQTAMOV = IIF(this.oParentObject.w_TIPART="FO",1, this.oParentObject.w_ELQTAMOV)
          else
            this.w_ARTICOLO = this.oParentObject.w_ELCODART
          endif
          this.w_OLDPREZZO = this.oParentObject.w_ELPREZZO
          this.w_OLDSCONT1 = this.oParentObject.w_ELSCONT1
          this.w_OLDSCONT2 = this.oParentObject.w_ELSCONT2
          this.w_OLDSCONT3 = this.oParentObject.w_ELSCONT3
          this.w_OLDSCONT4 = this.oParentObject.w_ELSCONT4
          this.w_QTAUM1 = CALQTA(this.oParentObject.w_ELQTAMOV,this.oParentObject.w_ELUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.oParentObject.w_FLFRAZ1, this.oParentObject.w_MODUM2, " ", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
          this.w_QTAUM3 = CALQTA( this.w_QTAUM1, this.oParentObject.w_UNMIS3, Space(3),IIF( this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF( this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
          this.w_QTAUM2 = CALQTA( this.w_QTAUM1, this.oParentObject.w_UNMIS2, this.oParentObject.w_UNMIS2,IIF( this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF( this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
          DIMENSION pArrUm[9]
          pArrUm [1] = this.oParentObject.w_PREZUM 
 pArrUm [2] = this.oParentObject.w_ELUNIMIS 
 pArrUm [3] = this.oParentObject.w_ELQTAMOV 
 pArrUm [4] = this.oParentObject.w_UNMIS1 
 pArrUm [5] =this.w_QTAUM1 
 pArrUm [6] = this.oParentObject.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.oParentObject.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
          this.w_DATFAT = IIF(Not empty(this.oParentObject.w_ELDATFAT),this.oParentObject.w_ELDATFAT, IIF(this.oParentObject.w_FLATT="N" OR EMPTY(this.oParentObject.w_ELPERFAT), CP_CHARTODATE("  -  -  "), NextTime(this.oParentObject.w_ELPERFAT,this.oParentObject.w_ELDATINI-1, .F.,.T.)))
          this.w_DATATT = IIF(Not empty(this.oParentObject.w_ELDATATT),this.oParentObject.w_ELDATATT, IIF(this.oParentObject.w_FLATTI="N" OR EMPTY(this.oParentObject.w_ELPERATT), CP_CHARTODATE("  -  -  "), NextTime(this.oParentObject.w_ELPERATT,this.oParentObject.w_ELDATINI-1, .F.,.T.)))
          this.w_LISTINO = IIF(!EMPTY(this.oParentObject.w_ELCODLIS), this.oParentObject.w_ELCODLIS, IIF(!EMPTY(NVL(this.oParentObject.w_CCODLIS,"")),this.oParentObject.w_CCODLIS,this.oParentObject.w_MCODLIS))
          this.w_DATAVAL = IIF(Not empty(this.w_DATFAT),this.w_DATFAT,this.w_DATATT)
          this.w_CONCOD = iif(this.pExec="Z" and empty(this.oParentObject.w_ELCONCOD), REPL("X",16),this.oParentObject.w_ELCONCOD)
          this.w_CALPRZ = CalPrzli( this.w_CONCOD , this.oParentObject.w_TIPCON, this.w_LISTINO , this.w_ARTICOLO , "XXXXXX" , this.w_QTAUM1 , this.oParentObject.w_ELCODVAL , GETCAM(this.oParentObject.w_ELCODVAL,I_DATSYS) , this.w_DATAVAL, " " , "XXXXXX", this.oParentObject.w_ELCODVAL, this.oParentObject.w_ELCODCLI, " ", "N", this.oParentObject.w_SCOLIS, 0,"M", @ARRCALC, " ", " ", "N", @pArrUm )
          this.w_CLUNIMIS = ARRCALC(16)
          this.w_PREZZO = ARRCALC(5)
          this.w_SCONT1 = ARRCALC(1)
          this.w_SCONT2 = ARRCALC(2)
          this.w_SCONT3 = ARRCALC(3)
          this.w_SCONT4 = ARRCALC(4)
          this.w_OK = ARRCALC(7)=2 OR ARRCALC(8)=1
          if this.oParentObject.w_ELUNIMIS<>this.w_CLUNIMIS AND NOT EMPTY(this.w_CLUNIMIS)
            this.w_QTALIS = IIF(this.w_CLUNIMIS=this.oParentObject.w_UNMIS1,this.w_QTAUM1, IIF(this.w_CLUNIMIS=this.oParentObject.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
            this.w_PREZZO = cp_Round(CALMMPZ(this.w_PREZZO, this.oParentObject.w_ELQTAMOV, this.w_QTALIS, " ",0, this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
          endif
          this.w_RICPRE = (this.oParentObject.w_ELPREZZO<>0 AND this.w_PREZZO<>this.w_OLDPREZZO) 
          this.w_RICSCO = (this.w_SCONT1<>this.w_OLDSCONT1 And this.w_OLDSCONT1<>0) OR (this.w_SCONT2<>this.w_OLDSCONT2 And this.w_OLDSCONT2<>0) OR (this.w_SCONT3<>this.w_OLDSCONT3 And this.w_OLDSCONT3<>0) OR (this.w_SCONT4<>this.w_OLDSCONT4 And this.w_OLDSCONT4<>0)
          if this.w_RICPRE OR this.w_RICSCO
            this.w_oMESS.AddMsgPartNL("Ricalcolo il prezzo e/o gli sconti in base alle impostazioni correnti?")     
            if this.w_RICPRE
              do case
                case Arrcalc(7)=1
                  this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovo prezzo: %1 calcolato da contratto")
                  this.w_oPART.addParam(Alltrim(Str(this.w_PREZZO,18,5)))     
                case Arrcalc(7)=2
                  this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovo prezzo: %1 calcolato da listino")
                  this.w_oPART.addParam(Alltrim(Str(this.w_PREZZO,18,5)))     
              endcase
            endif
            if this.w_RICSCO
              do case
                case Arrcalc(8)=1
                  this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 calcolati da contratto")
                  this.w_oPART.addParam(Alltrim(Str(ARRCALC(1),6,2))+ "   "+Alltrim(Str(ARRCALC(2),6,2)) +"   "+Alltrim(Str(ARRCALC(3),6,2))+"   "+Alltrim(Str(ARRCALC(4),6,2)))     
                case Arrcalc(8)=3
                  this.w_oPART = this.w_oMESS.addmsgpartNL("Nuovi sconti: %1 calcolati da listino")
                  this.w_oPART.addParam(Alltrim(Str(ARRCALC(1),6,2))+ "   "+Alltrim(Str(ARRCALC(2),6,2)) +"   "+Alltrim(Str(ARRCALC(3),6,2))+"   "+Alltrim(Str(ARRCALC(4),6,2)))     
              endcase
            endif
            if this.w_oMESS.ah_YesNo()
              this.oParentObject.w_ELPREZZO = this.w_PREZZO
              this.oParentObject.o_ELPREZZO = this.oParentObject.w_ELPREZZO
              this.oParentObject.w_ELCONCOD = IIF(ARRCALC(9)="XXXXXXXXXXXXXXXX",SPACE(15),ARRCALC(9))
              if EMPTY(this.oParentObject.w_ELCONCOD)
                this.oParentObject.w_DESCTR = SPACE(35)
              else
                * --- Read from CON_TRAM
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CON_TRAM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2],.t.,this.CON_TRAM_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CODESCON,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS"+;
                    " from "+i_cTable+" CON_TRAM where ";
                        +"COTIPCLF = "+cp_ToStrODBC(this.oParentObject.w_TIPCON);
                        +" and CONUMERO = "+cp_ToStrODBC(this.oParentObject.w_ELCONCOD);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CODESCON,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                    from (i_cTable) where;
                        COTIPCLF = this.oParentObject.w_TIPCON;
                        and CONUMERO = this.oParentObject.w_ELCONCOD;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_DESCTR = NVL(cp_ToDate(_read_.CODESCON),cp_NullValue(_read_.CODESCON))
                  this.oParentObject.w_CT = NVL(cp_ToDate(_read_.COTIPCLF),cp_NullValue(_read_.COTIPCLF))
                  this.oParentObject.w_CC = NVL(cp_ToDate(_read_.COCODCLF),cp_NullValue(_read_.COCODCLF))
                  this.oParentObject.w_CM = NVL(cp_ToDate(_read_.COCATCOM),cp_NullValue(_read_.COCATCOM))
                  this.oParentObject.w_CI = NVL(cp_ToDate(_read_.CODATINI),cp_NullValue(_read_.CODATINI))
                  this.oParentObject.w_CF = NVL(cp_ToDate(_read_.CODATFIN),cp_NullValue(_read_.CODATFIN))
                  this.oParentObject.w_CV = NVL(cp_ToDate(_read_.COCODVAL),cp_NullValue(_read_.COCODVAL))
                  this.oParentObject.w_QUACON = NVL(cp_ToDate(_read_.COQUANTI),cp_NullValue(_read_.COQUANTI))
                  this.oParentObject.w_IVACON = NVL(cp_ToDate(_read_.COIVALIS),cp_NullValue(_read_.COIVALIS))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              this.oParentObject.w_ELSCONT1 = ARRCALC(1)
              this.oParentObject.w_ELSCONT2 = ARRCALC(2)
              this.oParentObject.w_ELSCONT3 = ARRCALC(3)
              this.oParentObject.w_ELSCONT4 = ARRCALC(4)
              this.oParentObject.w_PREZZTOT = this.oParentObject.w_ELQTAMOV * (cp_Round(this.oParentObject.w_ELPREZZO *(100+this.oParentObject.w_ELSCONT1)*(100+this.oParentObject.w_ELSCONT2)*(100+this.oParentObject.w_ELSCONT3)*(100+this.oParentObject.w_ELSCONT4)/100000000 ,5) )
            endif
          else
            this.oParentObject.w_ELPREZZO = this.w_PREZZO
            this.oParentObject.o_ELPREZZO = this.oParentObject.w_ELPREZZO
            this.oParentObject.w_ELCONCOD = IIF(ARRCALC(9)="XXXXXXXXXXXXXXXX",SPACE(15),ARRCALC(9))
            if EMPTY(this.oParentObject.w_ELCONCOD) AND this.pExec="Z"
              if this.oParentObject.w_ELPREZZO<>0
                Ah_ErrorMsg("Il contratto selezionato non � valido per le selezioni impostate, sar� applicato il prezzo di listino")
              else
                Ah_ErrorMsg("Il contratto selezionato non � valido per le selezioni impostate")
              endif
            endif
            if EMPTY(this.oParentObject.w_ELCONCOD)
              this.oParentObject.w_DESCTR = SPACE(35)
            else
              * --- Read from CON_TRAM
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CON_TRAM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2],.t.,this.CON_TRAM_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CODESCON,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS"+;
                  " from "+i_cTable+" CON_TRAM where ";
                      +"COTIPCLF = "+cp_ToStrODBC(this.oParentObject.w_TIPCON);
                      +" and CONUMERO = "+cp_ToStrODBC(this.oParentObject.w_ELCONCOD);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CODESCON,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
                  from (i_cTable) where;
                      COTIPCLF = this.oParentObject.w_TIPCON;
                      and CONUMERO = this.oParentObject.w_ELCONCOD;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_DESCTR = NVL(cp_ToDate(_read_.CODESCON),cp_NullValue(_read_.CODESCON))
                this.oParentObject.w_CT = NVL(cp_ToDate(_read_.COTIPCLF),cp_NullValue(_read_.COTIPCLF))
                this.oParentObject.w_CC = NVL(cp_ToDate(_read_.COCODCLF),cp_NullValue(_read_.COCODCLF))
                this.oParentObject.w_CM = NVL(cp_ToDate(_read_.COCATCOM),cp_NullValue(_read_.COCATCOM))
                this.oParentObject.w_CI = NVL(cp_ToDate(_read_.CODATINI),cp_NullValue(_read_.CODATINI))
                this.oParentObject.w_CF = NVL(cp_ToDate(_read_.CODATFIN),cp_NullValue(_read_.CODATFIN))
                this.oParentObject.w_CV = NVL(cp_ToDate(_read_.COCODVAL),cp_NullValue(_read_.COCODVAL))
                this.oParentObject.w_QUACON = NVL(cp_ToDate(_read_.COQUANTI),cp_NullValue(_read_.COQUANTI))
                this.oParentObject.w_IVACON = NVL(cp_ToDate(_read_.COIVALIS),cp_NullValue(_read_.COIVALIS))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            this.oParentObject.w_ELSCONT1 = ARRCALC(1)
            this.oParentObject.w_ELSCONT2 = ARRCALC(2)
            this.oParentObject.w_ELSCONT3 = ARRCALC(3)
            this.oParentObject.w_ELSCONT4 = ARRCALC(4)
            this.oParentObject.w_PREZZTOT = this.oParentObject.w_ELQTAMOV * (cp_Round(this.oParentObject.w_ELPREZZO *(100+this.oParentObject.w_ELSCONT1)*(100+this.oParentObject.w_ELSCONT2)*(100+this.oParentObject.w_ELSCONT3)*(100+this.oParentObject.w_ELSCONT4)/100000000 ,5) )
          endif
        else
           
 DECLARE ARRET (10,1) 
 ARRET[1]= "" 
 ARRET[2]= "" 
 ARRET[3]= "" 
 ARRET[4]= "" 
 ARRET[5]=0 
 ARRET[6]=0 
 ARRET[7]=0 
 ARRET[8]=0 
 ARRET[9]=0 
 ARRET[10]=0
          * --- Azzero l'Array che verr� riempito dalla Funzione
          this.w_CALPRZ = gsar_bpz(This,this.oParentObject.w_ELTCOLIS,this.oParentObject.w_ELPROSCO,this.oParentObject.w_ELSCOLIS,this.oParentObject.w_ELPROLIS,this.oParentObject.w_ELCODART,this.oParentObject.w_TIPCON,this.oParentObject.w_ELCODCLI,this.oParentObject.w_ELQTAMOV,this.oParentObject.w_ELCODVAL,Icase(Not empty(this.oParentObject.w_ELDATFAT),this.oParentObject.w_ELDATFAT,Not empty(this.oParentObject.w_ELDATATT),this.oParentObject.w_ELDATATT,this.oParentObject.w_ELDATINI),this.oParentObject.w_ELUNIMIS,this.oParentObject.w_ELCAUDOC,"N",this.oParentObject.w_KEYLISTB,@ARRET)
          this.oParentObject.w_ELTCOLIS = ARRET[1]
          this.oParentObject.w_ELSCOLIS = ARRET[3]
          this.oParentObject.w_ELPROLIS = ARRET[2]
          this.oParentObject.w_ELPROSCO = ARRET[4]
          this.oParentObject.w_ELPREZZO = ARRET[5]
          this.oParentObject.w_LIPREZZO = ARRET[5]
          if this.oParentObject.w_MOTIPCON<>"P"
            this.oParentObject.w_ELSCONT1 = ARRET[6]
            this.oParentObject.w_ELSCONT2 = ARRET[7]
            this.oParentObject.w_ELSCONT3 = ARRET[8]
            this.oParentObject.w_ELSCONT4 = ARRET[9]
          else
            this.oParentObject.w_ELSCONT1 = 0
            this.oParentObject.w_ELSCONT2 = 0
            this.oParentObject.w_ELSCONT3 = 0
            this.oParentObject.w_ELSCONT4 = 0
          endif
        endif
      case this.pExec="T"
        if isahe()
          * --- Creo temporaneo listini
          * --- Edit aborted
          gsag_bdl(this,"D",this.oParentObject.w_KEYLISTB,Null,Space(5))
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_COND = this.oParentObject.o_ELCODVAL <> this.oParentObject.w_ELCODVAL Or (this.oParentObject.o_ELCAUDOC <> this.oParentObject.w_ELCAUDOC And (this.oParentObject.w_FLINTE = "N" Or (!Empty(this.oParentObject.o_ELCAUDOC) And this.oParentObject.w_FLGLIS ="S"))) Or ((this.oParentObject.o_ELDATINI <> this.oParentObject.w_ELDATINI) And (this.oParentObject.w_FLINTE = "N" Or !Empty(this.oParentObject.w_ELCODCLI)) Or this.oParentObject.o_ELCODMOD<>this.oParentObject.w_ELCODMOD)
          DECLARE ARRCALC (4,1)
          if (this.oParentObject.w_ELCODCLI <> this.oParentObject.o_ELCODCLI) or (this.oParentObject.w_ELCONTRA <> this.oParentObject.o_ELCONTRA)
            this.oParentObject.w_ELDATFAT = IIF(this.oParentObject.w_FLATT="N", CP_CHARTODATE("  -  -  "), IIF(NOT EMPTY(this.oParentObject.w_ELPERFAT), NextTime(this.oParentObject.w_ELPERFAT,this.oParentObject.w_ELDATINI, .F.,.T.),this.oParentObject.w_ELDATFAT))
            this.oParentObject.w_ELDATATT = IIF(this.oParentObject.w_FLATTI="N", CP_CHARTODATE("  -  -  "), IIF(NOT EMPTY(this.oParentObject.w_ELPERATT), NextTime(this.oParentObject.w_ELPERATT,this.oParentObject.w_ELDATINI, .F.,.T.),this.oParentObject.w_ELDATATT))
          endif
          if this.w_COND
            * --- ricalclolo la data di riferimento per i listini
            * --- E' cambiato il codice della valuta e la causale documento pulizia tabella
            this.oParentObject.w_ELPROLIS = ""
            this.oParentObject.w_ELPROSCO = ""
            * --- Creo la nuova tabella 
            w_RESULT=CALCLIST("CREA",@ARRCALC,"V","N",this.oParentObject.w_ELCODVAL,this.oParentObject.w_TIPCON,this.oParentObject.w_ELCODCLI,Icase(Not empty(this.oParentObject.w_ELDATFAT),this.oParentObject.w_ELDATFAT,Not empty(this.oParentObject.w_ELDATATT),this.oParentObject.w_ELDATATT,this.oParentObject.w_ELDATINI)," "," ",this.oParentObject.w_KEYLISTB,-20,this.oParentObject.w_FLSCOR," ",this.oParentObject.w_ELCAUDOC)
            this.oParentObject.w_ELTCOLIS = ARRCALC(1)
            this.oParentObject.w_ELSCOLIS = ARRCALC(3)
            this.oParentObject.w_ELPROLIS = ARRCALC(2)
            this.oParentObject.w_ELPROSCO = ARRCALC(4)
          else
            * --- Entro in modifica, popolo la tabella temporaneo listini
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANSCORPO"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCLI);
                    +" and ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANSCORPO;
                from (i_cTable) where;
                    ANCODICE = this.oParentObject.w_CODCLI;
                    and ANTIPCON = this.oParentObject.w_TIPCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_FLSCOR = NVL(cp_ToDate(_read_.ANSCORPO),cp_NullValue(_read_.ANSCORPO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            w_RESULT=CALCLIST("CREA",@ARRCALC,"V","N",this.oParentObject.w_ELCODVAL,this.oParentObject.w_TIPCON,this.oParentObject.w_CODCLI,Icase(Not empty(this.oParentObject.w_ELDATFAT),this.oParentObject.w_ELDATFAT,Not empty(this.oParentObject.w_ELDATATT),this.oParentObject.w_ELDATATT,this.oParentObject.w_ELDATINI)," "," ",this.oParentObject.w_KEYLISTB,-20,this.oParentObject.w_FLSCOR," ",this.oParentObject.w_ELCAUDOC)
          endif
        endif
      case this.pExec="R"
        * --- Read from CON_TRAM
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CON_TRAM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2],.t.,this.CON_TRAM_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CODESCON,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS"+;
            " from "+i_cTable+" CON_TRAM where ";
                +"COTIPCLF = "+cp_ToStrODBC(this.oParentObject.w_TIPCON);
                +" and CONUMERO = "+cp_ToStrODBC(this.oParentObject.w_ELCONCOD);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CODESCON,COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COQUANTI,COIVALIS;
            from (i_cTable) where;
                COTIPCLF = this.oParentObject.w_TIPCON;
                and CONUMERO = this.oParentObject.w_ELCONCOD;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESCTR = NVL(cp_ToDate(_read_.CODESCON),cp_NullValue(_read_.CODESCON))
          this.oParentObject.w_CT = NVL(cp_ToDate(_read_.COTIPCLF),cp_NullValue(_read_.COTIPCLF))
          this.oParentObject.w_CC = NVL(cp_ToDate(_read_.COCODCLF),cp_NullValue(_read_.COCODCLF))
          this.oParentObject.w_CM = NVL(cp_ToDate(_read_.COCATCOM),cp_NullValue(_read_.COCATCOM))
          this.oParentObject.w_CI = NVL(cp_ToDate(_read_.CODATINI),cp_NullValue(_read_.CODATINI))
          this.oParentObject.w_CF = NVL(cp_ToDate(_read_.CODATFIN),cp_NullValue(_read_.CODATFIN))
          this.oParentObject.w_CV = NVL(cp_ToDate(_read_.COCODVAL),cp_NullValue(_read_.COCODVAL))
          this.oParentObject.w_QUACON = NVL(cp_ToDate(_read_.COQUANTI),cp_NullValue(_read_.COQUANTI))
          this.oParentObject.w_IVACON = NVL(cp_ToDate(_read_.COIVALIS),cp_NullValue(_read_.COIVALIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Il contratto ha la precedenza sui dati del rinnovo se COESCRIN = "N"
        if NOT EMPTY( this.oParentObject.w_ELCODMOD ) AND NOT EMPTY( this.oParentObject.w_ELCONTRA )
          * --- Read from CON_TRAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CON_TRAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAS_idx,2],.t.,this.CON_TRAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "COESCRIN,COPERCON,CORINCON,CONUMGIO"+;
              " from "+i_cTable+" CON_TRAS where ";
                  +"COSERIAL = "+cp_ToStrODBC(this.oParentObject.w_ELCONTRA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              COESCRIN,COPERCON,CORINCON,CONUMGIO;
              from (i_cTable) where;
                  COSERIAL = this.oParentObject.w_ELCONTRA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COESCRIN = NVL(cp_ToDate(_read_.COESCRIN),cp_NullValue(_read_.COESCRIN))
            this.w_COPERCON = NVL(cp_ToDate(_read_.COPERCON),cp_NullValue(_read_.COPERCON))
            this.w_CORINCON = NVL(cp_ToDate(_read_.CORINCON),cp_NullValue(_read_.CORINCON))
            this.w_CONUMGIO = NVL(cp_ToDate(_read_.CONUMGIO),cp_NullValue(_read_.CONUMGIO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Esclude rinnovo
          do case
            case this.w_COESCRIN = "S"
              * --- Esclude rinnovo
              this.oParentObject.w_ELESCRIN = "S"
              this.oParentObject.w_ELPERCON = "   "
            case this.w_COESCRIN = "N"
              * --- Copia i dati rinnovo dal contratto
              this.oParentObject.w_ELESCRIN = "N"
              this.oParentObject.w_ELPERCON = LEFT( this.w_COPERCON, 3 )
            case this.w_COESCRIN = "M"
              * --- Copia i dati rinnovo dal modello
              * --- Read from MOD_ELEM
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MOD_ELEM_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MOD_ELEM_idx,2],.t.,this.MOD_ELEM_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MOESCRIN,MOPERCON,MORINCON,MONUMGIO"+;
                  " from "+i_cTable+" MOD_ELEM where ";
                      +"MOCODICE = "+cp_ToStrODBC(this.oParentObject.w_ELCODMOD);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MOESCRIN,MOPERCON,MORINCON,MONUMGIO;
                  from (i_cTable) where;
                      MOCODICE = this.oParentObject.w_ELCODMOD;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_MOESCRIN = NVL(cp_ToDate(_read_.MOESCRIN),cp_NullValue(_read_.MOESCRIN))
                this.w_MOPERCON = NVL(cp_ToDate(_read_.MOPERCON),cp_NullValue(_read_.MOPERCON))
                this.w_MORINCON = NVL(cp_ToDate(_read_.MORINCON),cp_NullValue(_read_.MORINCON))
                this.w_MONUMGIO = NVL(cp_ToDate(_read_.MONUMGIO),cp_NullValue(_read_.MONUMGIO))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.oParentObject.w_ELESCRIN = this.w_MOESCRIN
              this.oParentObject.w_ELPERCON = LEFT( this.w_MOPERCON, 3 )
          endcase
          * --- Rinnovo tacito
          do case
            case this.w_CORINCON = "S"
              * --- Legge da contratto
              this.oParentObject.w_ELRINCON = this.w_CORINCON
              this.oParentObject.w_ELNUMGIO = this.w_CONUMGIO
            case this.w_CORINCON = "N"
              * --- Legge da contratto
              this.oParentObject.w_ELRINCON = this.w_CORINCON
              this.oParentObject.w_ELNUMGIO = this.w_CONUMGIO
            case this.w_CORINCON = "M"
              * --- Legge da modello
              this.oParentObject.w_ELRINCON = this.w_MORINCON
              this.oParentObject.w_ELNUMGIO = this.w_MONUMGIO
          endcase
          * --- Descrizione della periodicit�
          this.oParentObject.w_MDDESCRI = SPACE( 50 )
          * --- Read from MODCLDAT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MODCLDAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MODCLDAT_idx,2],.t.,this.MODCLDAT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MDDESCRI"+;
              " from "+i_cTable+" MODCLDAT where ";
                  +"MDCODICE = "+cp_ToStrODBC(this.oParentObject.w_ELPERCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MDDESCRI;
              from (i_cTable) where;
                  MDCODICE = this.oParentObject.w_ELPERCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_MDDESCRI = NVL(cp_ToDate(_read_.MDDESCRI),cp_NullValue(_read_.MDDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Data limite disdetta
          this.oParentObject.w_ELDATDIS = this.oParentObject.w_ELDATFIN - this.oParentObject.w_ELNUMGIO
        endif
      case this.pExec="D"
        this.w_TEST = .T.
        this.w_TROVA = .T.
        if this.oParentObject.w_ATTEXIST
          * --- Select from GSAG_QCD
          do vq_exec with 'GSAG_QCD',this,'_Curs_GSAG_QCD','',.f.,.t.
          if used('_Curs_GSAG_QCD')
            select _Curs_GSAG_QCD
            locate for 1=1
            do while not(eof())
            this.w_ATDATINI = CP_TODATE(_Curs_GSAG_QCD.ATDATINI)
            this.w_ATDATFIN = CP_TODATE(_Curs_GSAG_QCD.ATDATFIN)
            if (this.w_ATDATINI<this.oParentObject.w_ELDATINI) OR (this.w_ATDATINI>this.oParentObject.w_ELDATFIN) OR (this.w_ATDATFIN<this.oParentObject.w_ELDATINI) OR (this.w_ATDATFIN>this.oParentObject.w_ELDATFIN)
              this.w_TROVA = .F.
            endif
              select _Curs_GSAG_QCD
              continue
            enddo
            use
          endif
          if !this.w_TROVA
            this.w_TEST = AH_YESNO( "Attenzione! Esistono una o pi� attivit� con data inizio o fine non compresa nel periodo di validit� dell'elemento contratto. Si desidera proseguire?" )
          endif
          if Not this.w_TEST
            this.oParentObject.w_RESCHK = -1
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='LIST_TMP'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='MOD_ELEM'
    this.cWorkTables[5]='CON_TRAS'
    this.cWorkTables[6]='MODCLDAT'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='UNIMIS'
    this.cWorkTables[9]='CON_TRAM'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_GSAG_QCD')
      use in _Curs_GSAG_QCD
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
