* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bld                                                        *
*              Generazione attivit� da caricamento attivit�                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-26                                                      *
* Last revis.: 2010-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Serial_Att_Orig,pTIPGEN
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bld",oParentObject,m.Serial_Att_Orig,m.pTIPGEN)
return(i_retval)

define class tgsag_bld as StdBatch
  * --- Local variables
  Serial_Att_Orig = space(20)
  pTIPGEN = space(1)
  w_OBJAAT = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia generazione attivit� da caricamento attivit�
    *     in area manuale Declare Variables in GSAG_AAT
    * --- Seriale attivit� di origine
    * --- Tipo generazione 
    *     R: ricorrenti
    *     S: collegate
    this.w_OBJAAT = this.oParentObject
    this.w_OBJAAT.cFunction = "Query"
    this.w_OBJAAT.EcpQuit()     
    this.w_OBJAAT = .NULL.
    this.w_OBJAAT = GSAG_AAT()
    * --- Controllo se ha passato il test di accesso
    if !(this.w_OBJAAT.bSec1)
      i_retcode = 'stop'
      return
    endif
    * --- Carico il record selezionato
    *     Se l'attivit� � riservata devo comunque aprirla in questo caso 
    *     (chi l'ha creata pu� vederla fino al termine della operazione)
    this.w_OBJAAT.w_bNoCheckRis = .t.
    this.w_OBJAAT.ECPFILTER()     
    this.w_OBJAAT.w_ATSERIAL = this.Serial_Att_Orig
    this.w_OBJAAT.ECPSAVE()     
    this.w_OBJAAT.w_bNoCheckRis = .f.
    if Not Empty(this.w_OBJAAT.w_ATOGGETT)
      if this.pTIPGEN="S"
        GSAG_KCL(this.w_OBJAAT)
      else
        GSAG_KAR(this.w_OBJAAT)
        this.w_OBJAAT.Loadrecwarn()     
        this.w_OBJAAT.refresh()     
      endif
    endif
    this.w_OBJAAT = .NULL.
  endproc


  proc Init(oParentObject,Serial_Att_Orig,pTIPGEN)
    this.Serial_Att_Orig=Serial_Att_Orig
    this.pTIPGEN=pTIPGEN
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Serial_Att_Orig,pTIPGEN"
endproc
