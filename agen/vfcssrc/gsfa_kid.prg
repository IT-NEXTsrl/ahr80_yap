* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_kid                                                        *
*              Elenco ID eventi                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-18                                                      *
* Last revis.: 2012-05-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsfa_kid",oParentObject))

* --- Class definition
define class tgsfa_kid as StdForm
  Top    = 9
  Left   = 10

  * --- Standard Properties
  Width  = 597
  Height = 481
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-28"
  HelpContextID=267027049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  OFF_NOMI_IDX = 0
  ANEVENTI_IDX = 0
  cPrg = "gsfa_kid"
  cComment = "Elenco ID eventi "
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_EV__ANNO = space(4)
  w_EVSERIAL = space(15)
  w_LIDRICH = space(15)
  w_CODNOM = space(15)
  w_NODESCRI = space(60)
  w_SER_EVE = space(10)
  w_STARIC = space(1)
  w_IDRICH = space(15)
  w_ATSEREVE = space(15)
  w_ATCODNOM = space(15)
  w_NOMNOM = space(10)
  w_LSTARIC = space(1)
  o_LSTARIC = space(1)
  w_STARIC1 = space(1)
  w_ZOOMID = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsfa_kidPag1","gsfa_kid",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLIDRICH_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMID = this.oPgFrm.Pages(1).oPag.ZOOMID
    DoDefault()
    proc Destroy()
      this.w_ZOOMID = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='ANEVENTI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_EV__ANNO=space(4)
      .w_EVSERIAL=space(15)
      .w_LIDRICH=space(15)
      .w_CODNOM=space(15)
      .w_NODESCRI=space(60)
      .w_SER_EVE=space(10)
      .w_STARIC=space(1)
      .w_IDRICH=space(15)
      .w_ATSEREVE=space(15)
      .w_ATCODNOM=space(15)
      .w_NOMNOM=space(10)
      .w_LSTARIC=space(1)
      .w_STARIC1=space(1)
      .w_EV__ANNO=oParentObject.w_EV__ANNO
      .w_IDRICH=oParentObject.w_IDRICH
      .w_ATSEREVE=oParentObject.w_ATSEREVE
      .w_ATCODNOM=oParentObject.w_ATCODNOM
      .oPgFrm.Page1.oPag.ZOOMID.Calculate(.w_LIDRICH)
          .DoRTCalc(1,1,.f.)
        .w_EVSERIAL = .w_ZOOMID.GETVAR('EVSERIAL')
          .DoRTCalc(3,3,.f.)
        .w_CODNOM = This.oparentobject .w_ATCODNOM
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODNOM))
          .link_1_6('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_SER_EVE))
          .link_1_10('Full')
        endif
        .w_STARIC = .w_ZOOMID.GETVAR('EVSTARIC')
        .w_IDRICH = .w_ZOOMID.GETVAR('EVIDRICH')
        .w_ATSEREVE = .w_ZOOMID.GETVAR('EVSERIAL')
        .w_ATCODNOM = .w_ZOOMID.GETVAR('EVNOMINA')
      .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .w_NOMNOM = IIF(Lower(This.oparentobject.class) ='tgsfa_aev','.w_EVNOMINA','.w_ATCODNOM')
        .w_LSTARIC = 'D'
        .w_STARIC1 = iif(.w_LSTARIC='T',' ',.w_LSTARIC)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_EV__ANNO=.w_EV__ANNO
      .oParentObject.w_IDRICH=.w_IDRICH
      .oParentObject.w_ATSEREVE=.w_ATSEREVE
      .oParentObject.w_ATCODNOM=.w_ATCODNOM
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMID.Calculate(.w_LIDRICH)
        .DoRTCalc(1,1,.t.)
            .w_EVSERIAL = .w_ZOOMID.GETVAR('EVSERIAL')
        .DoRTCalc(3,6,.t.)
            .w_STARIC = .w_ZOOMID.GETVAR('EVSTARIC')
            .w_IDRICH = .w_ZOOMID.GETVAR('EVIDRICH')
            .w_ATSEREVE = .w_ZOOMID.GETVAR('EVSERIAL')
            .w_ATCODNOM = .w_ZOOMID.GETVAR('EVNOMINA')
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
            .w_NOMNOM = IIF(Lower(This.oparentobject.class) ='tgsfa_aev','.w_EVNOMINA','.w_ATCODNOM')
        .DoRTCalc(12,12,.t.)
        if .o_LSTARIC<>.w_LSTARIC
            .w_STARIC1 = iif(.w_LSTARIC='T',' ',.w_LSTARIC)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMID.Calculate(.w_LIDRICH)
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSER_EVE_1_10.visible=!this.oPgFrm.Page1.oPag.oSER_EVE_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMID.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsfa_kid
    IF Cevent='w_zoomid selected'
     This.Notifyevent('Aggiorna')
     IF This.w_NOMNOM='ATCODNOM'
       This.oparentobject.w_NEWID=.f.
     Else
       This.oparentobject.w_EVFLIDPA='N'
     Endif
     This.ecpsave()
    endif
    IF Cevent='w_LIDRICH Changed' or Cevent='w_CODNOM Changed' or Cevent='w_LSTARIC Changed'
       This.w_STARIC1=iif(This.w_LSTARIC='T',' ',This.w_LSTARIC)
       This.Notifyevent('Ricerca')
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODNOM
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_1_6'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_NODESCRI = NVL(_Link_.NODESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_NODESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SER_EVE
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_lTable = "ANEVENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2], .t., this.ANEVENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SER_EVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ANEVENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EVSERIAL like "+cp_ToStrODBC(trim(this.w_SER_EVE)+"%");
                   +" and EV__ANNO="+cp_ToStrODBC(this.w_EV__ANNO);

          i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVNOMINA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EV__ANNO,EVSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EV__ANNO',this.w_EV__ANNO;
                     ,'EVSERIAL',trim(this.w_SER_EVE))
          select EV__ANNO,EVSERIAL,EVIDRICH,EVNOMINA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EV__ANNO,EVSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SER_EVE)==trim(_Link_.EVSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SER_EVE) and !this.bDontReportError
            deferred_cp_zoom('ANEVENTI','*','EV__ANNO,EVSERIAL',cp_AbsName(oSource.parent,'oSER_EVE_1_10'),i_cWhere,'',"",'GSFA_ZID.ANEVENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_EV__ANNO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVNOMINA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select EV__ANNO,EVSERIAL,EVIDRICH,EVNOMINA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVNOMINA";
                     +" from "+i_cTable+" "+i_lTable+" where EVSERIAL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and EV__ANNO="+cp_ToStrODBC(this.w_EV__ANNO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EV__ANNO',oSource.xKey(1);
                       ,'EVSERIAL',oSource.xKey(2))
            select EV__ANNO,EVSERIAL,EVIDRICH,EVNOMINA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SER_EVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVNOMINA";
                   +" from "+i_cTable+" "+i_lTable+" where EVSERIAL="+cp_ToStrODBC(this.w_SER_EVE);
                   +" and EV__ANNO="+cp_ToStrODBC(this.w_EV__ANNO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EV__ANNO',this.w_EV__ANNO;
                       ,'EVSERIAL',this.w_SER_EVE)
            select EV__ANNO,EVSERIAL,EVIDRICH,EVNOMINA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SER_EVE = NVL(_Link_.EVSERIAL,space(10))
      this.w_LIDRICH = NVL(_Link_.EVIDRICH,space(15))
      this.w_CODNOM = NVL(_Link_.EVNOMINA,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_SER_EVE = space(10)
      endif
      this.w_LIDRICH = space(15)
      this.w_CODNOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])+'\'+cp_ToStr(_Link_.EV__ANNO,1)+'\'+cp_ToStr(_Link_.EVSERIAL,1)
      cp_ShowWarn(i_cKey,this.ANEVENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SER_EVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLIDRICH_1_4.value==this.w_LIDRICH)
      this.oPgFrm.Page1.oPag.oLIDRICH_1_4.value=this.w_LIDRICH
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNOM_1_6.value==this.w_CODNOM)
      this.oPgFrm.Page1.oPag.oCODNOM_1_6.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oNODESCRI_1_7.value==this.w_NODESCRI)
      this.oPgFrm.Page1.oPag.oNODESCRI_1_7.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSER_EVE_1_10.value==this.w_SER_EVE)
      this.oPgFrm.Page1.oPag.oSER_EVE_1_10.value=this.w_SER_EVE
    endif
    if not(this.oPgFrm.Page1.oPag.oLSTARIC_1_17.RadioValue()==this.w_LSTARIC)
      this.oPgFrm.Page1.oPag.oLSTARIC_1_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LSTARIC = this.w_LSTARIC
    return

enddefine

* --- Define pages as container
define class tgsfa_kidPag1 as StdContainer
  Width  = 593
  height = 481
  stdWidth  = 593
  stdheight = 481
  resizeXpos=330
  resizeYpos=256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMID as cp_zoombox with uid="TCWQBJQGVA",left=3, top=70, width=583,height=360,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomOnZoom="",cZoomFile="GSFA_KID",bOptions=.f.,bAdvOptions=.f.,cTable="ANEVENTI",bQueryOnLoad=.f.,bReadOnly=.f.,cMenuFile="",bRetriveAllRows=.f.,;
    cEvent = "Ricerca,Blank",;
    nPag=1;
    , HelpContextID = 179406054

  add object oLIDRICH_1_4 as StdField with uid="WGVDXWHGGV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LIDRICH", cQueryName = "LIDRICH",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 133958582,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=73, Top=13, InputMask=replicate('X',15)

  add object oCODNOM_1_6 as StdField with uid="FBGQLGTKZA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 39325990,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=73, Top=39, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oNODESCRI_1_7 as StdField with uid="BZMNEDZEIF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 143594015,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=209, Top=39, InputMask=replicate('X',60)


  add object oBtn_1_9 as StdButton with uid="JMPPTWBRZQ",left=209, top=12, width=20,height=22,;
    caption="?", nPag=1;
    , ToolTipText = "Premere per selezionare ID richiesta";
    , HelpContextID = 267025946;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        LinkEventi(This.Parent.oContained)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSER_EVE_1_10 as StdField with uid="AFDHWPZGML",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SER_EVE", cQueryName = "SER_EVE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 181004326,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=183, Top=153, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="ANEVENTI", oKey_1_1="EV__ANNO", oKey_1_2="this.w_EV__ANNO", oKey_2_1="EVSERIAL", oKey_2_2="this.w_SER_EVE"

  func oSER_EVE_1_10.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  func oSER_EVE_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oSER_EVE_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSER_EVE_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ANEVENTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"EV__ANNO="+cp_ToStrODBC(this.Parent.oContained.w_EV__ANNO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"EV__ANNO="+cp_ToStr(this.Parent.oContained.w_EV__ANNO)
    endif
    do cp_zoom with 'ANEVENTI','*','EV__ANNO,EVSERIAL',cp_AbsName(this.parent,'oSER_EVE_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSFA_ZID.ANEVENTI_VZM',this.parent.oContained
  endproc


  add object oObj_1_15 as cp_runprogram with uid="PXXZWEIRWQ",left=5, top=537, width=173,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsag_ble(w_IDRICH,'w_ATSEREVE',This.Parent.oContained.oparentobject,w_NOMNOM)",;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 179406054


  add object oLSTARIC_1_17 as StdCombo with uid="NNSKIDFEAC",rtseq=12,rtrep=.f.,left=480,top=14,width=107,height=21;
    , ToolTipText = "Stato della richiesta  aperto, in corso, in attesa, chiuso";
    , HelpContextID = 243013046;
    , cFormVar="w_LSTARIC",RowSource=""+"Da chiudere,"+"Aperta,"+"In corso,"+"In attesa,"+"Chiusa,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLSTARIC_1_17.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'A',;
    iif(this.value =3,'I',;
    iif(this.value =4,'S',;
    iif(this.value =5,'C',;
    iif(this.value =6,'T',;
    space(1))))))))
  endfunc
  func oLSTARIC_1_17.GetRadio()
    this.Parent.oContained.w_LSTARIC = this.RadioValue()
    return .t.
  endfunc

  func oLSTARIC_1_17.SetRadio()
    this.Parent.oContained.w_LSTARIC=trim(this.Parent.oContained.w_LSTARIC)
    this.value = ;
      iif(this.Parent.oContained.w_LSTARIC=='D',1,;
      iif(this.Parent.oContained.w_LSTARIC=='A',2,;
      iif(this.Parent.oContained.w_LSTARIC=='I',3,;
      iif(this.Parent.oContained.w_LSTARIC=='S',4,;
      iif(this.Parent.oContained.w_LSTARIC=='C',5,;
      iif(this.Parent.oContained.w_LSTARIC=='T',6,;
      0))))))
  endfunc


  add object oBtn_1_20 as StdButton with uid="SANMOSDWKU",left=11, top=432, width=48,height=45,;
    CpPicture="bmp\visuali.ico", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare l'evento";
    , HelpContextID = 13256774;
    , Caption='\<Eventi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        OpenGest('A','gsfa_aev','EVSERIAL',.w_EVSERIAL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="WSJDSZZAJV",Visible=.t., Left=5, Top=15,;
    Alignment=1, Width=65, Height=18,;
    Caption="ID richiesta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="QKRTNOGLGK",Visible=.t., Left=5, Top=42,;
    Alignment=1, Width=65, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="PKLUAHGBDW",Visible=.t., Left=365, Top=14,;
    Alignment=1, Width=108, Height=18,;
    Caption="Stato  rich.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_kid','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsfa_kid
*--- Effettua il link su id eventi, lanciato dal bottone ...
Proc LinkEventi(oParent)
    l_obj = oParent.GetCtrl('w_SER_EVE')
    oParent.bDontReportError=.t.
    public i_lastindirectaction
    i_lastindirectaction='ecpZoom'	
    l_obj.setFocus()
	  l_obj.mzoom()
EndFunc
* --- Fine Area Manuale
