* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_krs                                                        *
*              Riserva                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-20                                                      *
* Last revis.: 2011-10-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_krs",oParentObject))

* --- Class definition
define class tgsag_krs as StdForm
  Top    = 90
  Left   = 135

  * --- Standard Properties
  Width  = 557
  Height = 186
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-10-20"
  HelpContextID=116008809
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  TIP_RISE_IDX = 0
  PAR_AGEN_IDX = 0
  cPrg = "gsag_krs"
  cComment = "Riserva"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPORIS = space(1)
  w_CONFERMA = space(1)
  w_FL_ATTCOMPL = space(1)
  o_FL_ATTCOMPL = space(1)
  w_FLGEDIT = space(1)
  w_ATCAUATT = space(20)
  w_ATOGGETTOR = space(254)
  w_DATINIOR = ctod('  /  /  ')
  w_OREINIOR = space(2)
  w_MININIOR = space(2)
  w_PAR_AGEN = space(5)
  w_PACHKCPL = space(1)
  w_ATSERIAL = space(15)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_krsPag1","gsag_krs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPORIS_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_RISE'
    this.cWorkTables[2]='PAR_AGEN'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPORIS=space(1)
      .w_CONFERMA=space(1)
      .w_FL_ATTCOMPL=space(1)
      .w_FLGEDIT=space(1)
      .w_ATCAUATT=space(20)
      .w_ATOGGETTOR=space(254)
      .w_DATINIOR=ctod("  /  /  ")
      .w_OREINIOR=space(2)
      .w_MININIOR=space(2)
      .w_PAR_AGEN=space(5)
      .w_PACHKCPL=space(1)
      .w_ATSERIAL=space(15)
      .w_TIPORIS=oParentObject.w_TIPORIS
      .w_CONFERMA=oParentObject.w_CONFERMA
      .w_FL_ATTCOMPL=oParentObject.w_FL_ATTCOMPL
      .w_ATCAUATT=oParentObject.w_ATCAUATT
      .w_ATOGGETTOR=oParentObject.w_ATOGGETTOR
      .w_DATINIOR=oParentObject.w_DATINIOR
      .w_OREINIOR=oParentObject.w_OREINIOR
      .w_MININIOR=oParentObject.w_MININIOR
      .w_ATSERIAL=oParentObject.w_ATSERIAL
          .DoRTCalc(1,3,.f.)
        .w_FLGEDIT = 'N'
          .DoRTCalc(5,9,.f.)
        .w_PAR_AGEN = i_CodAzi
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_PAR_AGEN))
          .link_1_20('Full')
        endif
    endwith
    this.DoRTCalc(11,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIPORIS=.w_TIPORIS
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.w_FL_ATTCOMPL=.w_FL_ATTCOMPL
      .oParentObject.w_ATCAUATT=.w_ATCAUATT
      .oParentObject.w_ATOGGETTOR=.w_ATOGGETTOR
      .oParentObject.w_DATINIOR=.w_DATINIOR
      .oParentObject.w_OREINIOR=.w_OREINIOR
      .oParentObject.w_MININIOR=.w_MININIOR
      .oParentObject.w_ATSERIAL=.w_ATSERIAL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_FL_ATTCOMPL<>.w_FL_ATTCOMPL
          .Calculate_CTUYDULNVV()
        endif
        .DoRTCalc(1,9,.t.)
          .link_1_20('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_CTUYDULNVV()
    with this
          * --- Controllo su check Completa attivit� selezionata
          GSAG_BCC(this;
              ,'S';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLGEDIT_1_7.visible=!this.oPgFrm.Page1.oPag.oFLGEDIT_1_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_krs
    if CEVENT='w_FLGEDIT Changed' and This.w_FLGEDIT='S'
       =opengest('M','GSAG_AAT','ATSERIAL',This.w_ATSERIAL)
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PAR_AGEN
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAR_AGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAR_AGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACHKCPL";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PAR_AGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PAR_AGEN)
            select PACODAZI,PACHKCPL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAR_AGEN = NVL(_Link_.PACODAZI,space(5))
      this.w_PACHKCPL = NVL(_Link_.PACHKCPL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PAR_AGEN = space(5)
      endif
      this.w_PACHKCPL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAR_AGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPORIS_1_1.RadioValue()==this.w_TIPORIS)
      this.oPgFrm.Page1.oPag.oTIPORIS_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFL_ATTCOMPL_1_6.RadioValue()==this.w_FL_ATTCOMPL)
      this.oPgFrm.Page1.oPag.oFL_ATTCOMPL_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGEDIT_1_7.RadioValue()==this.w_FLGEDIT)
      this.oPgFrm.Page1.oPag.oFLGEDIT_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATCAUATT_1_11.value==this.w_ATCAUATT)
      this.oPgFrm.Page1.oPag.oATCAUATT_1_11.value=this.w_ATCAUATT
    endif
    if not(this.oPgFrm.Page1.oPag.oATOGGETTOR_1_12.value==this.w_ATOGGETTOR)
      this.oPgFrm.Page1.oPag.oATOGGETTOR_1_12.value=this.w_ATOGGETTOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINIOR_1_14.value==this.w_DATINIOR)
      this.oPgFrm.Page1.oPag.oDATINIOR_1_14.value=this.w_DATINIOR
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINIOR_1_16.value==this.w_OREINIOR)
      this.oPgFrm.Page1.oPag.oOREINIOR_1_16.value=this.w_OREINIOR
    endif
    if not(this.oPgFrm.Page1.oPag.oMININIOR_1_17.value==this.w_MININIOR)
      this.oPgFrm.Page1.oPag.oMININIOR_1_17.value=this.w_MININIOR
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsag_krs
      IF i_bRes
          If Empty(.w_TIPORIS)
            ah_ErrorMsg("Selezionare il tipo riserva.")
            i_bRes = .f.
          EndIf
          if i_bRes AND .w_FL_ATTCOMPL='N' AND .w_PACHKCPL='S'
            i_bRes = ah_YesNo("Si � deciso di non completare l'attivit� selezionata. Procedi ugualmente?")
          endif
      ENDIF
      IF i_bRes
          .w_CONFERMA='S'
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FL_ATTCOMPL = this.w_FL_ATTCOMPL
    return

enddefine

* --- Define pages as container
define class tgsag_krsPag1 as StdContainer
  Width  = 553
  height = 186
  stdWidth  = 553
  stdheight = 186
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPORIS_1_1 as StdTableCombo with uid="ZCFJUUHZJU",rtseq=1,rtrep=.f.,left=109,top=99,width=123,height=21;
    , HelpContextID = 126494518;
    , cFormVar="w_TIPORIS",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='TIP_RISE',cKey='TRCODICE',cValue='TRDESTIP',cOrderBy='TRDESTIP',xDefault=space(1);
  , bGlobalFont=.t.



  add object oBtn_1_2 as StdButton with uid="ERHOXVEEGI",left=446, top=137, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 115980058;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_3 as StdButton with uid="TKCMXZXILU",left=501, top=137, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 108691386;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFL_ATTCOMPL_1_6 as StdCheck with uid="CMBGGUKYCE",rtseq=3,rtrep=.f.,left=251, top=99, caption="Completa l'attivit� selezionata",;
    ToolTipText = "Se attivo, verr� completata l'attivit� selezionata",;
    HelpContextID = 44183157,;
    cFormVar="w_FL_ATTCOMPL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFL_ATTCOMPL_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFL_ATTCOMPL_1_6.GetRadio()
    this.Parent.oContained.w_FL_ATTCOMPL = this.RadioValue()
    return .t.
  endfunc

  func oFL_ATTCOMPL_1_6.SetRadio()
    this.Parent.oContained.w_FL_ATTCOMPL=trim(this.Parent.oContained.w_FL_ATTCOMPL)
    this.value = ;
      iif(this.Parent.oContained.w_FL_ATTCOMPL=='S',1,;
      0)
  endfunc

  add object oFLGEDIT_1_7 as StdCheck with uid="DPGZVNZQJC",rtseq=4,rtrep=.f.,left=251, top=124, caption="Modifica attivit�",;
    ToolTipText = "Se attivo apre attivit� in modifica",;
    HelpContextID = 111122774,;
    cFormVar="w_FLGEDIT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGEDIT_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLGEDIT_1_7.GetRadio()
    this.Parent.oContained.w_FLGEDIT = this.RadioValue()
    return .t.
  endfunc

  func oFLGEDIT_1_7.SetRadio()
    this.Parent.oContained.w_FLGEDIT=trim(this.Parent.oContained.w_FLGEDIT)
    this.value = ;
      iif(this.Parent.oContained.w_FLGEDIT=='S',1,;
      0)
  endfunc

  func oFLGEDIT_1_7.mHide()
    with this.Parent.oContained
      return (.w_FL_ATTCOMPL<>'S')
    endwith
  endfunc

  add object oATCAUATT_1_11 as StdField with uid="LBGXFCJQQZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ATCAUATT", cQueryName = "ATCAUATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 262889818,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=79, Top=34, InputMask=replicate('X',20)

  add object oATOGGETTOR_1_12 as StdField with uid="ZBFVVQRHJA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ATOGGETTOR", cQueryName = "ATOGGETTOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 47347786,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=237, Top=34, InputMask=replicate('X',254)

  add object oDATINIOR_1_14 as StdField with uid="ZJMFUWULJM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATINIOR", cQueryName = "DATINIOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 146514296,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=79, Top=57

  add object oOREINIOR_1_16 as StdField with uid="LWBQPTRXKS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OREINIOR", cQueryName = "OREINIOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 146571208,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=237, Top=57, InputMask=replicate('X',2)

  add object oMININIOR_1_17 as StdField with uid="HOGLWIPZWG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MININIOR", cQueryName = "MININIOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 146536680,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=279, Top=57, InputMask=replicate('X',2)

  add object oStr_1_5 as StdString with uid="YGEBOCYRYM",Visible=.t., Left=24, Top=99,;
    Alignment=1, Width=80, Height=18,;
    Caption="Tipo riserva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="DDTKAFHBPY",Visible=.t., Left=25, Top=12,;
    Alignment=0, Width=197, Height=18,;
    Caption="Attivit� selezionata"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="ORKJUUCBVM",Visible=.t., Left=25, Top=34,;
    Alignment=1, Width=49, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="LMGOPCHWVW",Visible=.t., Left=25, Top=59,;
    Alignment=1, Width=49, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="JQFSIFZRPU",Visible=.t., Left=176, Top=59,;
    Alignment=1, Width=56, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="RSSICMDKLX",Visible=.t., Left=265, Top=59,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oBox_1_9 as StdBox with uid="MXBVHLEODS",left=19, top=28, width=530,height=55
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_krs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
