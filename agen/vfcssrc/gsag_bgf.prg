* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bgf                                                        *
*              Metodo generatore attivit� per determinazione prima data utile  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-04-27                                                      *
* Last revis.: 2011-05-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ATTIVITA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bgf",oParentObject,m.w_ATTIVITA)
return(i_retval)

define class tgsag_bgf as StdBatch
  * --- Local variables
  w_ATTIVITA = .NULL.
  w_PARTECIPANTI = .null.
  w_CALENDARI = .null.
  w_CODCALGEN = space(5)
  w_PACHKFES = space(1)
  w_OREINI = space(2)
  w_MININI = space(2)
  w_ANTICIPA = space(1)
  w_AVANTI = .f.
  w_CODCAL = space(5)
  w_FLSOSP = space(1)
  w_ELECALEN = space(10)
  w_GIORNO = ctot("")
  w_DATAGIORNO = ctod("  /  /  ")
  w_FIRSTDAY = ctot("")
  w_CALENDARIO = space(5)
  w_SOLOORARIO = ctot("")
  w_SHOW_MSG = .f.
  w_ANNO = 0
  w_DATA_ATT_RIF = ctot("")
  w_NUMGIOSOSP = 0
  w_NUMGIORNI = 0
  w_SAVE_GIORNO = ctot("")
  w_PRIMOGIOSOSP = ctot("")
  w_ULTGIOSOSP = ctot("")
  w_DESGIO = space(30)
  w_FLCHIU = space(1)
  w_CALFLSOSP = space(1)
  w_CAORAIN1 = space(5)
  w_CAORAFI1 = space(5)
  w_CAORAIN2 = space(5)
  w_CAORAFI2 = space(5)
  w_ORAIN1 = ctot("")
  w_ORAFI1 = ctot("")
  w_ORAIN2 = ctot("")
  w_ORAFI2 = ctot("")
  * --- WorkFile variables
  PAR_AGEN_idx=0
  DIPENDEN_idx=0
  CAL_AZIE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Determina il primo giorno disponibile sui calendari associati
    *     alle risosre dell'oggetto attivit� passato come parametro...
    this.w_ANTICIPA = "N"
    * --- Indica se il calcolo dei gg viene eseguito in avanti o a ritroso
    this.w_AVANTI = IIF(this.w_ATTIVITA.w_ATNUMGIO >= 0, .T., .F.)
    * --- Read from PAR_AGEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PACALEND,PACHKFES,PAFLANTI"+;
        " from "+i_cTable+" PAR_AGEN where ";
            +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PACALEND,PACHKFES,PAFLANTI;
        from (i_cTable) where;
            PACODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODCALGEN = NVL(cp_ToDate(_read_.PACALEND),cp_NullValue(_read_.PACALEND))
      this.w_PACHKFES = NVL(cp_ToDate(_read_.PACHKFES),cp_NullValue(_read_.PACHKFES))
      this.w_ANTICIPA = NVL(cp_ToDate(_read_.PAFLANTI),cp_NullValue(_read_.PAFLANTI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- In base al tipo di avvertimento specificato sui parametri agenda decido
    *     il comportamento, se nessuno non eseguo nessuna elaborazione
    *     Se sabato / domenica controllo solo il giorno se calendario verifico
    *     i calendari dei partecipanti se presenti altrimenti il calendario
    *     specificato nei parametri agenda...
    do case
      case this.w_PACHKFES="N"
        this.w_ATTIVITA.dStartDateTime = this.w_ATTIVITA.w_ATDATINI
      case this.w_PACHKFES="E" Or this.w_PACHKFES="D"
        * --- Controllo che giorno �, se Sabato o Domenica, a seconda del check
        *     sposto di 2 o un giorno avanti...
        this.w_ATTIVITA.dStartDateTime = this.w_ATTIVITA.w_ATDATINI
        if this.w_PACHKFES="E"
          * --- Sabato e domenica
          if DOW( this.w_ATTIVITA.w_ATDATINI )=7
            if this.w_AVANTI AND this.w_ANTICIPA="N"
              * --- Spostato a luned�
              this.w_ATTIVITA.dStartDateTime = this.w_ATTIVITA.w_ATDATINI+ 2*3600*24
            else
              * --- Spostato a venerd�
              this.w_ATTIVITA.dStartDateTime = this.w_ATTIVITA.w_ATDATINI - 3600*24
            endif
            this.w_ATTIVITA.cStartDateTimeError = ah_msgformat( "Sabato")
          endif
        endif
        * --- solo domenica
        if DOW( this.w_ATTIVITA.w_ATDATINI )=1
          if this.w_AVANTI AND this.w_ANTICIPA="N"
            * --- Spostato a luned�
            this.w_ATTIVITA.dStartDateTime = this.w_ATTIVITA.w_ATDATINI + 3600*24
          else
            * --- Spostato a venerd�
            this.w_ATTIVITA.dStartDateTime = this.w_ATTIVITA.w_ATDATINI - 2*3600*24
          endif
          this.w_ATTIVITA.cStartDateTimeError = ah_msgformat( "Domenica")
        endif
      otherwise
        this.w_PARTECIPANTI = this.w_ATTIVITA.w_OFF_PART
        this.w_PARTECIPANTI.GoTop()     
        do while Not this.w_PARTECIPANTI.Eof()
          * --- Ricerco il calendario legato al partecipante...
          * --- Read from DIPENDEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIPENDEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPCODCAL"+;
              " from "+i_cTable+" DIPENDEN where ";
                  +"DPCODICE = "+cp_ToStrODBC(this.w_PARTECIPANTI.PACODRIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPCODCAL;
              from (i_cTable) where;
                  DPCODICE = this.w_PARTECIPANTI.PACODRIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODCAL = NVL(cp_ToDate(_read_.DPCODCAL),cp_NullValue(_read_.DPCODCAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- se dipendente senza calendario passo il calendario generale
          this.w_CODCAL = IIF( EMPTY( this.w_CODCAL) , this.w_CODCALGEN , this.w_CODCAL )
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_PARTECIPANTI.Skip(1)     
        enddo
        this.w_PARTECIPANTI = .null.
        * --- Per ricercare le giornate valide come periodo di orario
        *     passo alla query una data fittizia (i_mindat + oraraio) da confrontare
        *     con i giorni del calendario escludendo la data ed includendo l'orario
        this.w_GIORNO = this.w_ATTIVITA.w_ATDATINI
        this.w_DATAGIORNO = TTOD(this.w_GIORNO)
        this.w_SOLOORARIO = DTOT(i_INIDAT)+( this.w_GIORNO -DTOT( TTOD( this.w_GIORNO) ) )
        * --- Se alterego considero anche i giorni di sospensione forense se il
        *     tipo attivit� lo prevede
        this.w_FLSOSP = IIF ( this.w_ATTIVITA.bSospForensi , "S" , "" )
        this.w_ELECALEN = this.w_CALENDARI.cCursor
        if this.w_AVANTI
          if (not(this.w_FLSOSP == "S") OR EMPTY(this.w_ATTIVITA.w_ATNUMGIO)) AND this.w_ANTICIPA="S"
            * --- Select from ..\AGEN\EXE\QUERY\GSAG_BGF1.VQR
            do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BGF1.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR','',.f.,.t.
            if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
              select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
              locate for 1=1
              do while not(eof())
              this.w_FIRSTDAY = Nvl( CAGIORNO , Cp_chartodatetime("") )
              this.w_CALENDARIO = SUBSTR(CACODCAL, AT("@", CACODCAL)+1)
                select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
                continue
              enddo
              use
            endif
          else
            * --- Select from ..\AGEN\EXE\QUERY\GSAG_BGF.VQR
            do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BGF.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR','',.f.,.t.
            if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR')
              select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
              locate for 1=1
              do while not(eof())
              this.w_FIRSTDAY = Nvl( CAGIORNO , Cp_chartodatetime("") )
              this.w_CALENDARIO = SUBSTR(CACODCAL, AT("@", CACODCAL)+1)
                select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
                continue
              enddo
              use
            endif
          endif
        else
          * --- Select from ..\AGEN\EXE\QUERY\GSAG_BGF1.VQR
          do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BGF1.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR','',.f.,.t.
          if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
            select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
            locate for 1=1
            do while not(eof())
            this.w_FIRSTDAY = Nvl( CAGIORNO , Cp_chartodatetime("") )
            this.w_CALENDARIO = SUBSTR(CACODCAL, AT("@", CACODCAL)+1)
              select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
              continue
            enddo
            use
          endif
        endif
        if this.w_FLSOSP="S" AND NOT EMPTY(this.w_ATTIVITA.w_ATNUMGIO)
          this.w_SHOW_MSG = .F.
          this.w_SAVE_GIORNO = this.w_GIORNO
          * --- Data iniziale dell'attivit� di riferimento
          this.w_DATA_ATT_RIF = this.w_ATTIVITA.w_ATDATINI - (this.w_ATTIVITA.w_ATNUMGIO * 3600 * 24)
          * --- Si deve suddividere il periodo (dalla data iniziale dell'attivit� di riferimento a quella della nuova attivit� collegata) nei
          *     vari anni solari e per ognuno di essi considerare il periodo di sospensione.
          * --- In presenza di un solo anno
          if YEAR(this.w_DATA_ATT_RIF) = YEAR(this.w_GIORNO)
            * --- Select from ..\AGEN\EXE\QUERY\GSAG1BGF.VQR
            do vq_exec with '..\AGEN\EXE\QUERY\GSAG1BGF.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR','',.f.,.t.
            if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR')
              select _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
              locate for 1=1
              do while not(eof())
              * --- Giorni iniziale e finale del periodo di sospensione
              this.w_PRIMOGIOSOSP = Nvl( GIORNOMIN , Cp_chartodatetime("") )
              this.w_ULTGIOSOSP = Nvl( GIORNOMAX , Cp_chartodatetime("") )
              this.w_NUMGIOSOSP = TTOD(this.w_ULTGIOSOSP) - TTOD(this.w_PRIMOGIOSOSP) +1
                select _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
                continue
              enddo
              use
            endif
            if NOT EMPTY(this.w_PRIMOGIOSOSP) AND NOT EMPTY(this.w_ULTGIOSOSP)
              if this.w_AVANTI
                do case
                  case this.w_DATA_ATT_RIF <= this.w_PRIMOGIOSOSP AND this.w_GIORNO >= this.w_ULTGIOSOSP
                    * --- Sospensione compresa nell'intervallo di date
                    this.w_NUMGIORNI = this.w_NUMGIOSOSP
                    this.w_SHOW_MSG = .T.
                  case this.w_DATA_ATT_RIF >= this.w_PRIMOGIOSOSP AND this.w_GIORNO <= this.w_ULTGIOSOSP
                    * --- Intervallo di date compreso nella sospensione
                    this.w_NUMGIORNI = TTOD(this.w_GIORNO) - TTOD(this.w_DATA_ATT_RIF)
                  case this.w_DATA_ATT_RIF <= this.w_PRIMOGIOSOSP AND this.w_GIORNO>=this.w_PRIMOGIOSOSP AND this.w_GIORNO<=this.w_ULTGIOSOSP
                    * --- Intervallo di date a cavallo del primo giorno di sospensione
                    this.w_NUMGIORNI = TTOD(this.w_GIORNO) - TTOD(this.w_PRIMOGIOSOSP)
                  case this.w_DATA_ATT_RIF>=this.w_PRIMOGIOSOSP AND this.w_DATA_ATT_RIF<=this.w_ULTGIOSOSP AND this.w_GIORNO>=this.w_ULTGIOSOSP
                    * --- Intervallo di date a cavallo dell'ultimo giorno di sospensione
                    this.w_NUMGIORNI = TTOD(this.w_ULTGIOSOSP) - TTOD(this.w_DATA_ATT_RIF) + 1
                    this.w_SHOW_MSG = .T.
                endcase
              else
                do case
                  case this.w_GIORNO <= this.w_PRIMOGIOSOSP AND this.w_DATA_ATT_RIF >= this.w_ULTGIOSOSP
                    * --- Sospensione compresa nell'intervallo di date
                    this.w_NUMGIORNI = this.w_NUMGIOSOSP
                    this.w_SHOW_MSG = .T.
                  case this.w_GIORNO >= this.w_PRIMOGIOSOSP AND this.w_DATA_ATT_RIF <= this.w_ULTGIOSOSP
                    * --- Intervallo di date compreso nella sospensione
                    this.w_NUMGIORNI = TTOD(this.w_DATA_ATT_RIF) - TTOD(this.w_GIORNO)
                  case this.w_GIORNO <= this.w_PRIMOGIOSOSP AND this.w_DATA_ATT_RIF >= this.w_PRIMOGIOSOSP AND this.w_DATA_ATT_RIF <= this.w_ULTGIOSOSP
                    * --- Intervallo di date a cavallo del primo giorno di sospensione
                    this.w_NUMGIORNI = TTOD(this.w_DATA_ATT_RIF) - TTOD(this.w_PRIMOGIOSOSP) + 1
                  case this.w_GIORNO>=this.w_PRIMOGIOSOSP AND this.w_GIORNO<=this.w_ULTGIOSOSP AND this.w_DATA_ATT_RIF>=this.w_ULTGIOSOSP
                    * --- Intervallo di date a cavallo dell'ultimo giorno di sospensione
                    this.w_NUMGIORNI = TTOD(this.w_ULTGIOSOSP) - TTOD(this.w_GIORNO) 
                    this.w_SHOW_MSG = .T.
                endcase
              endif
              if this.w_AVANTI
                this.w_FIRSTDAY = IIF(this.w_SAVE_GIORNO>=this.w_PRIMOGIOSOSP AND this.w_SAVE_GIORNO<=this.w_ULTGIOSOSP, this.w_ULTGIOSOSP+ (1 * 3600 * 24), this.w_SAVE_GIORNO) + (this.w_NUMGIORNI * 3600 * 24)
              else
                this.w_FIRSTDAY = IIF(this.w_SAVE_GIORNO>=this.w_PRIMOGIOSOSP AND this.w_SAVE_GIORNO<=this.w_ULTGIOSOSP, this.w_PRIMOGIOSOSP- (1 * 3600 * 24), this.w_SAVE_GIORNO) - (this.w_NUMGIORNI * 3600 * 24)
              endif
              * --- Se il giorno non � lavorativo si deve considerare quello successivo
              this.w_GIORNO = this.w_FIRSTDAY
              this.w_DATAGIORNO = TTOD(this.w_GIORNO)
              if this.w_AVANTI AND this.w_ANTICIPA="N"
                * --- Select from ..\AGEN\EXE\QUERY\GSAG_BGF.VQR
                do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BGF.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR','',.f.,.t.
                if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR')
                  select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
                  locate for 1=1
                  do while not(eof())
                  this.w_FIRSTDAY = Nvl( CAGIORNO , Cp_chartodatetime("") )
                    select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
                    continue
                  enddo
                  use
                endif
              else
                * --- Select from ..\AGEN\EXE\QUERY\GSAG_BGF1.VQR
                do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BGF1.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR','',.f.,.t.
                if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
                  select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
                  locate for 1=1
                  do while not(eof())
                  this.w_FIRSTDAY = Nvl( CAGIORNO , Cp_chartodatetime("") )
                    select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
                    continue
                  enddo
                  use
                endif
              endif
              * --- Si deve verificare se il nuovo giorno cade nella sospensione del nuovo anno
              if YEAR(this.w_FIRSTDAY) <> YEAR(this.w_SAVE_GIORNO)
                this.w_GIORNO = this.w_FIRSTDAY
                this.w_DATAGIORNO = TTOD(this.w_GIORNO)
                * --- Select from ..\AGEN\EXE\QUERY\GSAG1BGF.VQR
                do vq_exec with '..\AGEN\EXE\QUERY\GSAG1BGF.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR','',.f.,.t.
                if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR')
                  select _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
                  locate for 1=1
                  do while not(eof())
                  * --- Giorni iniziale e finale del periodo di sospensione
                  this.w_PRIMOGIOSOSP = Nvl( GIORNOMIN , Cp_chartodatetime("") )
                  this.w_ULTGIOSOSP = Nvl( GIORNOMAX , Cp_chartodatetime("") )
                    select _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
                    continue
                  enddo
                  use
                endif
                * --- Se il giorno cade nel periodo di sospensione del nuovo anno
                if NOT EMPTY(this.w_PRIMOGIOSOSP) AND NOT EMPTY(this.w_ULTGIOSOSP) AND this.w_FIRSTDAY>=this.w_PRIMOGIOSOSP AND this.w_FIRSTDAY<=this.w_ULTGIOSOSP
                  if this.w_AVANTI
                    this.w_NUMGIORNI = TTOD(this.w_FIRSTDAY) - TTOD(this.w_PRIMOGIOSOSP) + 1
                    this.w_FIRSTDAY = this.w_ULTGIOSOSP + (this.w_NUMGIORNI * 3600 * 24)
                  else
                    this.w_NUMGIORNI = TTOD(this.w_ULTGIOSOSP) - TTOD(this.w_FIRSTDAY) + 1
                    this.w_FIRSTDAY = this.w_PRIMOGIOSOSP - (this.w_NUMGIORNI * 3600 * 24)
                  endif
                  * --- Se il giorno non � lavorativo si deve considerare quello successivo
                  this.w_GIORNO = this.w_FIRSTDAY
                  this.w_DATAGIORNO = TTOD(this.w_GIORNO)
                  if this.w_AVANTI
                    * --- Select from ..\AGEN\EXE\QUERY\GSAG_BGF.VQR
                    do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BGF.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR','',.f.,.t.
                    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR')
                      select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
                      locate for 1=1
                      do while not(eof())
                      this.w_FIRSTDAY = Nvl( CAGIORNO , Cp_chartodatetime("") )
                        select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
                        continue
                      enddo
                      use
                    endif
                  else
                    * --- Select from ..\AGEN\EXE\QUERY\GSAG_BGF1.VQR
                    do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BGF1.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR','',.f.,.t.
                    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
                      select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
                      locate for 1=1
                      do while not(eof())
                      this.w_FIRSTDAY = Nvl( CAGIORNO , Cp_chartodatetime("") )
                        select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
                        continue
                      enddo
                      use
                    endif
                  endif
                endif
              endif
            endif
          else
            * --- In presenza di pi� anni
            if this.w_AVANTI
              FOR this.w_ANNO=YEAR(this.w_DATA_ATT_RIF) TO YEAR(this.w_SAVE_GIORNO)
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              ENDFOR
            else
              FOR this.w_ANNO=YEAR(this.w_DATA_ATT_RIF) TO YEAR(this.w_SAVE_GIORNO) STEP -1
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              ENDFOR
            endif
            if this.w_AVANTI
              this.w_FIRSTDAY = IIF(this.w_SAVE_GIORNO>=this.w_PRIMOGIOSOSP AND this.w_SAVE_GIORNO<=this.w_ULTGIOSOSP, this.w_ULTGIOSOSP+ (1 * 3600 * 24), this.w_SAVE_GIORNO) + (this.w_NUMGIORNI * 3600 * 24)
            else
              this.w_FIRSTDAY = IIF(this.w_SAVE_GIORNO>=this.w_PRIMOGIOSOSP AND this.w_SAVE_GIORNO<=this.w_ULTGIOSOSP, this.w_PRIMOGIOSOSP - (1 * 3600 * 24), this.w_SAVE_GIORNO) - (this.w_NUMGIORNI * 3600 * 24)
            endif
            * --- Se il giorno non � lavorativo si deve considerare quello successivo
            this.w_GIORNO = this.w_FIRSTDAY
            this.w_DATAGIORNO = TTOD(this.w_GIORNO)
            if this.w_AVANTI AND this.w_ANTICIPA="N"
              * --- Select from ..\AGEN\EXE\QUERY\GSAG_BGF.VQR
              do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BGF.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR','',.f.,.t.
              if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR')
                select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
                locate for 1=1
                do while not(eof())
                this.w_FIRSTDAY = Nvl( CAGIORNO , Cp_chartodatetime("") )
                  select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
                  continue
                enddo
                use
              endif
            else
              * --- Select from ..\AGEN\EXE\QUERY\GSAG_BGF1.VQR
              do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BGF1.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR','',.f.,.t.
              if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
                select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
                locate for 1=1
                do while not(eof())
                this.w_FIRSTDAY = Nvl( CAGIORNO , Cp_chartodatetime("") )
                  select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
                  continue
                enddo
                use
              endif
            endif
            * --- Si deve verificare se il nuovo giorno cade nella sospensione
            this.w_GIORNO = this.w_FIRSTDAY
            * --- Select from ..\AGEN\EXE\QUERY\GSAG1BGF.VQR
            do vq_exec with '..\AGEN\EXE\QUERY\GSAG1BGF.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR','',.f.,.t.
            if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR')
              select _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
              locate for 1=1
              do while not(eof())
              * --- Giorni iniziale e finale del periodo di sospensione
              this.w_PRIMOGIOSOSP = Nvl( GIORNOMIN , Cp_chartodatetime("") )
              this.w_ULTGIOSOSP = Nvl( GIORNOMAX , Cp_chartodatetime("") )
                select _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
                continue
              enddo
              use
            endif
            * --- Se il giorno cade nel periodo di sospensione del nuovo anno
            if NOT EMPTY(this.w_PRIMOGIOSOSP) AND NOT EMPTY(this.w_ULTGIOSOSP) AND this.w_FIRSTDAY>=this.w_PRIMOGIOSOSP AND this.w_FIRSTDAY<=this.w_ULTGIOSOSP
              if this.w_AVANTI
                this.w_NUMGIORNI = TTOD(this.w_FIRSTDAY) - TTOD(this.w_PRIMOGIOSOSP) + 1
                this.w_FIRSTDAY = this.w_ULTGIOSOSP + (this.w_NUMGIORNI * 3600 * 24)
              else
                this.w_NUMGIORNI = TTOD(this.w_ULTGIOSOSP) - TTOD(this.w_FIRSTDAY) + 1
                this.w_FIRSTDAY = this.w_PRIMOGIOSOSP - (this.w_NUMGIORNI * 3600 * 24)
              endif
              * --- Se il giorno non � lavorativo si deve considerare quello successivo
              this.w_GIORNO = this.w_FIRSTDAY
              this.w_DATAGIORNO = TTOD(this.w_GIORNO)
              if this.w_AVANTI
                * --- Select from ..\AGEN\EXE\QUERY\GSAG_BGF.VQR
                do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BGF.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR','',.f.,.t.
                if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR')
                  select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
                  locate for 1=1
                  do while not(eof())
                  this.w_FIRSTDAY = Nvl( CAGIORNO , Cp_chartodatetime("") )
                    select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
                    continue
                  enddo
                  use
                endif
              else
                * --- Select from ..\AGEN\EXE\QUERY\GSAG_BGF1.VQR
                do vq_exec with '..\AGEN\EXE\QUERY\GSAG_BGF1.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR','',.f.,.t.
                if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
                  select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
                  locate for 1=1
                  do while not(eof())
                  this.w_FIRSTDAY = Nvl( CAGIORNO , Cp_chartodatetime("") )
                    select _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
                    continue
                  enddo
                  use
                endif
              endif
            endif
          endif
          this.w_GIORNO = this.w_SAVE_GIORNO
        endif
        if NOT EMPTY(this.w_FIRSTDAY)
          * --- Vengono riassegnati le ore e i minuti alla nuova data calcolata
          this.w_OREINI = RIGHT("00"+ALLTRIM(STR(HOUR(this.w_ATTIVITA.w_ATDATINI), 2)), 2)
          this.w_MININI = RIGHT("00"+ALLTRIM(STR(MINUTE(this.w_ATTIVITA.w_ATDATINI), 2, 0)), 2)
          this.w_FIRSTDAY = cp_CharToDatetime(DTOC(TTOD(this.w_FIRSTDAY))+" "+this.w_OREINI+":"+this.w_MININI+":00")
        endif
        this.w_ATTIVITA.dStartDateTime = this.w_FIRSTDAY
        * --- Nel caso di data calcolata differente rispetto a quanto ricevuto ripasso
        *     i calendari per determinare il problema.
        *     Considero i casi:
        *     a) giorno festivo => restituisco la descrizion del giorno
        *     b) Sospensione forense => Se si desidera filtrare per questo dato restituisco una descrizione
        *     c) La data/ora inizio attivit� non � compresa all'interno dei periodi
        *     
        *     Scorro i calendari valutati per determinare il primo che da problemi...
        if this.w_GIORNO<>this.w_FIRSTDAY
          this.w_CALENDARI.GoTop()     
          do while Not this.w_CALENDARI.Eof()
            * --- Leggo il calendario per capire il perch� � stato escluso...
            * --- Read from CAL_AZIE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2],.t.,this.CAL_AZIE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CADESGIO,CAGIOLAV,CAFLSOSP,CAORAIN1,CAORAFI1,CAORAIN2,CAORAFI2"+;
                " from "+i_cTable+" CAL_AZIE where ";
                    +"CACODCAL = "+cp_ToStrODBC(this.w_CALENDARI.TCCODICE);
                    +" and CAGIORNO = "+cp_ToStrODBC(Ttod( this.w_GIORNO ));
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CADESGIO,CAGIOLAV,CAFLSOSP,CAORAIN1,CAORAFI1,CAORAIN2,CAORAFI2;
                from (i_cTable) where;
                    CACODCAL = this.w_CALENDARI.TCCODICE;
                    and CAGIORNO = Ttod( this.w_GIORNO );
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESGIO = NVL(cp_ToDate(_read_.CADESGIO),cp_NullValue(_read_.CADESGIO))
              this.w_FLCHIU = NVL(cp_ToDate(_read_.CAGIOLAV),cp_NullValue(_read_.CAGIOLAV))
              this.w_CALFLSOSP = NVL(cp_ToDate(_read_.CAFLSOSP),cp_NullValue(_read_.CAFLSOSP))
              this.w_CAORAIN1 = NVL(cp_ToDate(_read_.CAORAIN1),cp_NullValue(_read_.CAORAIN1))
              this.w_CAORAFI1 = NVL(cp_ToDate(_read_.CAORAFI1),cp_NullValue(_read_.CAORAFI1))
              this.w_CAORAIN2 = NVL(cp_ToDate(_read_.CAORAIN2),cp_NullValue(_read_.CAORAIN2))
              this.w_CAORAFI2 = NVL(cp_ToDate(_read_.CAORAFI2),cp_NullValue(_read_.CAORAFI2))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            do case
              case this.w_FLSOSP="S" AND this.w_CALFLSOSP="S"
                * --- Chiusura forense
                this.w_ATTIVITA.cStartDateTimeError = ah_msgformat( "Giorno di sospensione attivit� forense. Calendario %1" , ALLTRIM(this.w_CALENDARI.TCCODICE) )
                exit
              case this.w_FLSOSP="S" AND this.w_SHOW_MSG
                this.w_ATTIVITA.cStartDateTimeError = ah_msgformat( "Giorno calcolato con sospensione attivit� forense. Calendario %1" , ALLTRIM(this.w_CALENDARI.TCCODICE) )
                exit
              case this.w_FLCHIU<>"S"
                * --- Giorno non lavorativo riporto la descrizione associata al giorno...
                this.w_ATTIVITA.cStartDateTimeError = ah_msgformat( "Giorno non lavorativo: %1. Calendario %2" , ALLTRIM(this.w_DESGIO) , ALLTRIM(this.w_CALENDARI.TCCODICE) )
                exit
              otherwise
                * --- Intervallo fuori dal periodo ? faccio due calcoli grazie alla brillante idea
                *     di memorizzare gli intervalli orari con un CHAR (!!)
                *     Trasformo i 4 periodi in datetime...
                this.w_ORAIN1 = DTOT(tTOd( this.w_GIORNO ))+VAL(LEFT( this.w_CAORAIN1 ,2))*3600+VAL(RIGHT( this.w_CAORAIN1 ,2))*60
                this.w_ORAFI1 = DTOT(tTOd( this.w_GIORNO ))+VAL(LEFT( this.w_CAORAFI1 ,2))*3600+VAL(RIGHT( this.w_CAORAFI1 ,2))*60
                this.w_ORAIN2 = DTOT(tTOd( this.w_GIORNO ))+VAL(LEFT( this.w_CAORAIN2 ,2))*3600+VAL(RIGHT( this.w_CAORAIN2 ,2))*60
                this.w_ORAFI2 = DTOT(tTOd( this.w_GIORNO ))+VAL(LEFT( this.w_CAORAFI2 ,2))*3600+VAL(RIGHT( this.w_CAORAFI2 ,2))*60
                if Not ((this.w_ORAIN1<=this.w_GIORNO And this.w_ORAFI1>=this.w_GIORNO)Or (this.w_ORAIN2<=this.w_GIORNO And this.w_ORAFI2>=this.w_GIORNO) )
                  this.w_ATTIVITA.cStartDateTimeError = ah_msgformat( "Data inizio attivit� esterna al periodo definito sul calendario %1" , ALLTRIM(this.w_CALENDARI.TCCODICE) )
                  exit
                endif
            endcase
            this.w_CALENDARI.Skip(1)     
          enddo
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiunge il calendario se manca dai calendari incontrati
    *     Non esiste il metodo di ricerca devo "scadere" sul cursore...
    if Not Empty( this.w_CODCAL )
      * --- Se calendario pieno vado a verificare se gia presente...
      Select( this.w_CALENDARI.cCursor ) 
 Go Top 
 Locate For TCCODICE = this.w_CODCAL
      if Not Found()
        this.w_CALENDARI.AppendBlank()     
        this.w_CALENDARI.TCCODICE = this.w_CODCAL
        this.w_CALENDARI.SaveCurrentRecord()     
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_GIORNO = DTOT(CTOD("01"+"-"+"01"+"-"+str(this.w_ANNO,4,0)))
    * --- Select from ..\AGEN\EXE\QUERY\GSAG1BGF.VQR
    do vq_exec with '..\AGEN\EXE\QUERY\GSAG1BGF.VQR',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR','',.f.,.t.
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR')
      select _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
      locate for 1=1
      do while not(eof())
      * --- Giorni iniziale e finale del periodo di sospensione
      this.w_PRIMOGIOSOSP = Nvl( GIORNOMIN , Cp_chartodatetime("") )
      this.w_ULTGIOSOSP = Nvl( GIORNOMAX , Cp_chartodatetime("") )
      this.w_NUMGIOSOSP = TTOD(this.w_ULTGIOSOSP) - TTOD(this.w_PRIMOGIOSOSP) +1
        select _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
        continue
      enddo
      use
    endif
    if NOT EMPTY(this.w_PRIMOGIOSOSP) AND NOT EMPTY(this.w_ULTGIOSOSP)
      do case
        case this.w_ANNO=YEAR(this.w_DATA_ATT_RIF) 
          * --- Primo anno
          if this.w_AVANTI
            do case
              case this.w_DATA_ATT_RIF>=this.w_PRIMOGIOSOSP AND this.w_DATA_ATT_RIF<=this.w_ULTGIOSOSP
                * --- Data iniziale attivit� di rif. all'interno della sospensione
                this.w_NUMGIORNI = this.w_NUMGIORNI + (TTOD(this.w_ULTGIOSOSP) - TTOD(this.w_DATA_ATT_RIF)) + 1
                this.w_SHOW_MSG = .T.
              case this.w_DATA_ATT_RIF<this.w_PRIMOGIOSOSP
                * --- Data iniziale attivit� di rif. precedente alla sospensione
                this.w_NUMGIORNI = this.w_NUMGIORNI + this.w_NUMGIOSOSP
                this.w_SHOW_MSG = .T.
            endcase
          else
            do case
              case this.w_DATA_ATT_RIF>=this.w_PRIMOGIOSOSP AND this.w_DATA_ATT_RIF<=this.w_ULTGIOSOSP
                * --- Data iniziale attivit� di rif. all'interno della sospensione
                this.w_NUMGIORNI = this.w_NUMGIORNI + (TTOD(this.w_DATA_ATT_RIF) - TTOD(this.w_PRIMOGIOSOSP)) + 1
                this.w_SHOW_MSG = .T.
              case this.w_DATA_ATT_RIF>this.w_ULTGIOSOSP
                * --- Data iniziale attivit� di rif. successiva alla sospensione
                this.w_NUMGIORNI = this.w_NUMGIORNI + this.w_NUMGIOSOSP
                this.w_SHOW_MSG = .T.
            endcase
          endif
        case this.w_ANNO=YEAR(this.w_SAVE_GIORNO) 
          * --- Ultimo anno
          if this.w_AVANTI
            do case
              case this.w_SAVE_GIORNO>=this.w_PRIMOGIOSOSP AND this.w_SAVE_GIORNO<=this.w_ULTGIOSOSP
                * --- Data iniziale attivit� all'interno della sospensione
                this.w_NUMGIORNI = this.w_NUMGIORNI + (TTOD(this.w_SAVE_GIORNO) - TTOD(this.w_PRIMOGIOSOSP))
              case this.w_SAVE_GIORNO>this.w_ULTGIOSOSP
                * --- Data iniziale attivit� successiva alla sospensione
                this.w_NUMGIORNI = this.w_NUMGIORNI + this.w_NUMGIOSOSP
                this.w_SHOW_MSG = .T.
            endcase
          else
            do case
              case this.w_SAVE_GIORNO>=this.w_PRIMOGIOSOSP AND this.w_SAVE_GIORNO<=this.w_ULTGIOSOSP
                * --- Data iniziale attivit� all'interno della sospensione
                this.w_NUMGIORNI = this.w_NUMGIORNI + (TTOD(this.w_ULTGIOSOSP) - TTOD(this.w_SAVE_GIORNO))
              case this.w_SAVE_GIORNO<this.w_PRIMOGIOSOSP
                * --- Data iniziale attivit� precedente alla sospensione
                this.w_NUMGIORNI = this.w_NUMGIORNI + this.w_NUMGIOSOSP
                this.w_SHOW_MSG = .T.
            endcase
          endif
        otherwise
          * --- Anno intero
          this.w_NUMGIORNI = this.w_NUMGIORNI + this.w_NUMGIOSOSP
          this.w_SHOW_MSG = .T.
      endcase
    endif
  endproc


  proc Init(oParentObject,w_ATTIVITA)
    this.w_ATTIVITA=w_ATTIVITA
    this.w_CALENDARI=createobject("cp_MemoryCursor","TAB_CALE",this)
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PAR_AGEN'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='CAL_AZIE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_BGF1_d_VQR
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG1BGF_d_VQR
    endif
    return
  proc RemoveMemoryCursors()
    this.w_CALENDARI.Done()
  endproc
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ATTIVITA"
endproc
