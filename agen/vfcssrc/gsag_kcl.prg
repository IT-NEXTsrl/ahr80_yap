* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kcl                                                        *
*              Nuova attivit� collegata                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-23                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kcl",oParentObject))

* --- Class definition
define class tgsag_kcl as StdForm
  Top    = 15
  Left   = 10

  * --- Standard Properties
  Width  = 595
  Height = 246
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-13"
  HelpContextID=169203863
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=62

  * --- Constant Properties
  _IDX = 0
  CAUMATTI_IDX = 0
  PAR_AGEN_IDX = 0
  cPrg = "gsag_kcl"
  cComment = "Nuova attivit� collegata"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PAR_AGEN = space(5)
  w_RESCHK = 0
  w_TIPOATTCOLL = space(20)
  o_TIPOATTCOLL = space(20)
  w_CARAGGST = space(1)
  w_OGGATTCOLL = space(254)
  w_PACHKFES = space(1)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_ORAFIN = space(2)
  w_ANNOPROG = space(4)
  w_MINFIN = space(2)
  w_CAFLPDOC = space(1)
  w_GIOINI = space(3)
  w_GIOFIN = space(3)
  w_ATSERIAL_ORIG = space(20)
  w_ATCAITER_ORIG = space(20)
  w_ATNOTPIA_ORIG = space(0)
  w_ATCODPRA_ORIG = space(15)
  w_ATCENCOS_ORIG = space(15)
  w_ATCODNOM_ORIG = space(15)
  w_ATTELEFO_ORIG = space(18)
  w_ATCELLUL_ORIG = space(18)
  w_AT_EMAIL_ORIG = space(0)
  w_AT_EMPEC_ORIG = space(0)
  w_ATLOCALI_ORIG = space(30)
  w_AT__ENTE_ORIG = space(10)
  w_ATUFFICI_ORIG = space(10)
  w_ATCODSED_ORIG = space(5)
  w_ATPERSON_ORIG = space(60)
  w_DATINI_ORIG = ctod('  /  /  ')
  w_ORAINI_ORIG = space(2)
  w_MININI_ORIG = space(2)
  w_CADURORE = 0
  o_CADURORE = 0
  w_CADURMIN = 0
  o_CADURMIN = 0
  w_CADTIN = ctot('')
  o_CADTIN = ctot('')
  w_CAORASYS = space(1)
  w_CAMINPRE = 0
  w_CAFLPREA = space(1)
  w_ATCAUATT_ORIG = space(20)
  w_ATOGGETT_ORIG = space(254)
  w_DATINI_OR = ctod('  /  /  ')
  o_DATINI_OR = ctod('  /  /  ')
  w_ORAINI_OR = space(2)
  w_MININI_OR = space(2)
  w_DATFIN_OR = ctod('  /  /  ')
  o_DATFIN_OR = ctod('  /  /  ')
  w_ORAFIN_OR = space(2)
  w_MINFIN_OR = space(2)
  w_GIOINI_OR = space(3)
  w_GIOFIN_OR = space(3)
  w_ATCENRIC_ORIG = space(15)
  w_ATCOMRIC_ORIG = space(15)
  w_ATATTRIC_ORIG = space(15)
  w_ATATTCOS_ORIG = space(15)
  w_STATUS = space(1)
  w_ATSEREVE_ORIG = space(20)
  w_IDRICH_ORIG = space(15)
  w_ATCONTAT_ORIG = space(10)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_STARIC_ORIG = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_ATEVANNO_ORIG = space(4)
  * --- Area Manuale = Declare Variables
  * --- gsag_kcl
  proc Destroy()
   DODEFAULT()
   IF TYPE("_screen.ActiveForm")='O' AND !ISNULL(_screen.ActiveForm)
      RAISEEVENT(_screen.ActiveForm, "Activate")
   ENDIF
  ENDPROC
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kclPag1","gsag_kcl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOATTCOLL_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CAUMATTI'
    this.cWorkTables[2]='PAR_AGEN'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAG_BCZ with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PAR_AGEN=space(5)
      .w_RESCHK=0
      .w_TIPOATTCOLL=space(20)
      .w_CARAGGST=space(1)
      .w_OGGATTCOLL=space(254)
      .w_PACHKFES=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_ORAINI=space(2)
      .w_MININI=space(2)
      .w_DATFIN=ctod("  /  /  ")
      .w_ORAFIN=space(2)
      .w_ANNOPROG=space(4)
      .w_MINFIN=space(2)
      .w_CAFLPDOC=space(1)
      .w_GIOINI=space(3)
      .w_GIOFIN=space(3)
      .w_ATSERIAL_ORIG=space(20)
      .w_ATCAITER_ORIG=space(20)
      .w_ATNOTPIA_ORIG=space(0)
      .w_ATCODPRA_ORIG=space(15)
      .w_ATCENCOS_ORIG=space(15)
      .w_ATCODNOM_ORIG=space(15)
      .w_ATTELEFO_ORIG=space(18)
      .w_ATCELLUL_ORIG=space(18)
      .w_AT_EMAIL_ORIG=space(0)
      .w_AT_EMPEC_ORIG=space(0)
      .w_ATLOCALI_ORIG=space(30)
      .w_AT__ENTE_ORIG=space(10)
      .w_ATUFFICI_ORIG=space(10)
      .w_ATCODSED_ORIG=space(5)
      .w_ATPERSON_ORIG=space(60)
      .w_DATINI_ORIG=ctod("  /  /  ")
      .w_ORAINI_ORIG=space(2)
      .w_MININI_ORIG=space(2)
      .w_CADURORE=0
      .w_CADURMIN=0
      .w_CADTIN=ctot("")
      .w_CAORASYS=space(1)
      .w_CAMINPRE=0
      .w_CAFLPREA=space(1)
      .w_ATCAUATT_ORIG=space(20)
      .w_ATOGGETT_ORIG=space(254)
      .w_DATINI_OR=ctod("  /  /  ")
      .w_ORAINI_OR=space(2)
      .w_MININI_OR=space(2)
      .w_DATFIN_OR=ctod("  /  /  ")
      .w_ORAFIN_OR=space(2)
      .w_MINFIN_OR=space(2)
      .w_GIOINI_OR=space(3)
      .w_GIOFIN_OR=space(3)
      .w_ATCENRIC_ORIG=space(15)
      .w_ATCOMRIC_ORIG=space(15)
      .w_ATATTRIC_ORIG=space(15)
      .w_ATATTCOS_ORIG=space(15)
      .w_STATUS=space(1)
      .w_ATSEREVE_ORIG=space(20)
      .w_IDRICH_ORIG=space(15)
      .w_ATCONTAT_ORIG=space(10)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      .w_STARIC_ORIG=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_ATEVANNO_ORIG=space(4)
        .w_PAR_AGEN = i_CodAzi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PAR_AGEN))
          .link_1_1('Full')
        endif
        .w_RESCHK = 0
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_TIPOATTCOLL))
          .link_1_4('Full')
        endif
          .DoRTCalc(4,6,.f.)
        .w_DATINI = this.oParentObject.w_DATINI
        .w_ORAINI = iif(Vartype(this.oParentObject)='O' , this.oparentobject.w_ORAINI, ' ' )
        .w_MININI = iif(Vartype(this.oParentObject)='O' , this.oparentobject.w_MININI, ' ' )
        .w_DATFIN = .w_DATINI+INT( (VAL(.w_ORAINI)+.w_CADURORE+INT((VAL(.w_MININI)+.w_CADURMIN)/60)) / 24)
        .w_ORAFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_ORAINI)+.w_CADURORE+INT((VAL(.w_MININI)+.w_CADURMIN)/60)), 24))),2)
        .w_ANNOPROG = STR(YEAR(CP_TODATE(.w_DATFIN)), 4, 0)
        .w_MINFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_MININI)+.w_CADURMIN), 60),2)),2)
          .DoRTCalc(14,14,.f.)
        .w_GIOINI = iif(!EMPTY(.w_DATINI), LEFT(g_GIORNO[DOW(.w_DATINI)], 3) , SPACE(3))
        .w_GIOFIN = iif(!EMPTY(.w_DATFIN), LEFT(g_GIORNO[DOW(.w_DATFIN)], 3) , SPACE(3))
        .w_ATSERIAL_ORIG = this.oParentObject.w_ATSERIAL
        .w_ATCAITER_ORIG = this.oParentObject.w_ATCAITER
        .w_ATNOTPIA_ORIG = this.oParentObject.w_ATNOTPIA
        .w_ATCODPRA_ORIG = this.oParentObject.w_ATCODPRA
        .w_ATCENCOS_ORIG = this.oParentObject.w_ATCENCOS
        .w_ATCODNOM_ORIG = this.oParentObject.w_ATCODNOM
        .w_ATTELEFO_ORIG = this.oParentObject.w_ATTELEFO
        .w_ATCELLUL_ORIG = this.oParentObject.w_ATCELLUL
        .w_AT_EMAIL_ORIG = this.oParentObject.w_AT_EMAIL
        .w_AT_EMPEC_ORIG = this.oParentObject.w_AT_EMPEC
        .w_ATLOCALI_ORIG = this.oParentObject.w_ATLOCALI
        .w_AT__ENTE_ORIG = this.oParentObject.w_AT__ENTE
        .w_ATUFFICI_ORIG = this.oParentObject.w_ATUFFICI
        .w_ATCODSED_ORIG = this.oParentObject.w_ATCODSED
        .w_ATPERSON_ORIG = this.oParentObject.w_ATPERSON
        .w_DATINI_ORIG = this.oParentObject.w_DATINI
        .w_ORAINI_ORIG = this.oParentObject.w_ORAINI
        .w_MININI_ORIG = this.oParentObject.w_MININI
          .DoRTCalc(35,40,.f.)
        .w_ATCAUATT_ORIG = this.oParentObject.w_ATCAUATT
        .w_ATOGGETT_ORIG = this.oParentObject.w_ATOGGETT
        .w_DATINI_OR = this.oParentObject.w_DATINI
        .w_ORAINI_OR = this.oParentObject.w_ORAINI
        .w_MININI_OR = this.oParentObject.w_MININI
        .w_DATFIN_OR = this.oParentObject.w_DATFIN
        .w_ORAFIN_OR = this.oParentObject.w_ORAFIN
        .w_MINFIN_OR = this.oParentObject.w_MINFIN
        .w_GIOINI_OR = iif(!EMPTY(.w_DATINI_OR), LEFT(g_GIORNO[DOW(.w_DATINI_OR)], 3) , SPACE(3))
        .w_GIOFIN_OR = iif(!EMPTY(.w_DATFIN_OR), LEFT(g_GIORNO[DOW(.w_DATFIN_OR)], 3) , SPACE(3))
        .w_ATCENRIC_ORIG = this.oParentObject.w_ATCENRIC
        .w_ATCOMRIC_ORIG = this.oParentObject.w_ATCOMRIC
        .w_ATATTRIC_ORIG = this.oParentObject.w_ATATTRIC
        .w_ATATTCOS_ORIG = this.oParentObject.w_ATATTCOS
          .DoRTCalc(55,55,.f.)
        .w_ATSEREVE_ORIG = this.oParentObject.w_ATSEREVE
        .w_IDRICH_ORIG = this.oParentObject.w_IDRICH
        .w_ATCONTAT_ORIG = iif(Vartype(this.oParentObject)='O' , this.oParentObject.w_ATCONTAT, ' ' )
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_STARIC_ORIG = iif(Vartype(this.oParentObject)='O' , this.oParentObject.w_STARIC, ' ' )
          .DoRTCalc(61,61,.f.)
        .w_ATEVANNO_ORIG = this.oParentObject.w_ATEVANNO
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,7,.t.)
        if .o_CADTIN<>.w_CADTIN
            .w_ORAINI = iif(Vartype(this.oParentObject)='O' , this.oparentobject.w_ORAINI, ' ' )
        endif
        if .o_CADTIN<>.w_CADTIN
            .w_MININI = iif(Vartype(this.oParentObject)='O' , this.oparentobject.w_MININI, ' ' )
        endif
        if .o_TIPOATTCOLL<>.w_TIPOATTCOLL.or. .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI
            .w_DATFIN = .w_DATINI+INT( (VAL(.w_ORAINI)+.w_CADURORE+INT((VAL(.w_MININI)+.w_CADURMIN)/60)) / 24)
        endif
        if .o_CADURORE<>.w_CADURORE.or. .o_CADURMIN<>.w_CADURMIN.or. .o_DATFIN<>.w_DATFIN.or. .o_TIPOATTCOLL<>.w_TIPOATTCOLL.or. .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI
            .w_ORAFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_ORAINI)+.w_CADURORE+INT((VAL(.w_MININI)+.w_CADURMIN)/60)), 24))),2)
        endif
            .w_ANNOPROG = STR(YEAR(CP_TODATE(.w_DATFIN)), 4, 0)
        if .o_CADURMIN<>.w_CADURMIN.or. .o_DATFIN<>.w_DATFIN.or. .o_TIPOATTCOLL<>.w_TIPOATTCOLL.or. .o_MININI<>.w_MININI
            .w_MINFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_MININI)+.w_CADURMIN), 60),2)),2)
        endif
        .DoRTCalc(14,14,.t.)
        if .o_DATINI<>.w_DATINI
            .w_GIOINI = iif(!EMPTY(.w_DATINI), LEFT(g_GIORNO[DOW(.w_DATINI)], 3) , SPACE(3))
        endif
        if .o_TIPOATTCOLL<>.w_TIPOATTCOLL.or. .o_DATINI<>.w_DATINI
          .Calculate_CCHBZJCWXE()
        endif
        if .o_DATFIN<>.w_DATFIN
            .w_GIOFIN = iif(!EMPTY(.w_DATFIN), LEFT(g_GIORNO[DOW(.w_DATFIN)], 3) , SPACE(3))
        endif
        .DoRTCalc(17,48,.t.)
        if .o_DATINI_OR<>.w_DATINI_OR
            .w_GIOINI_OR = iif(!EMPTY(.w_DATINI_OR), LEFT(g_GIORNO[DOW(.w_DATINI_OR)], 3) , SPACE(3))
        endif
        if .o_DATFIN_OR<>.w_DATFIN_OR
            .w_GIOFIN_OR = iif(!EMPTY(.w_DATFIN_OR), LEFT(g_GIORNO[DOW(.w_DATFIN_OR)], 3) , SPACE(3))
        endif
        .DoRTCalc(51,57,.t.)
            .w_ATCONTAT_ORIG = iif(Vartype(this.oParentObject)='O' , this.oParentObject.w_ATCONTAT, ' ' )
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(59,62,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_CCHBZJCWXE()
    with this
          * --- Aggiorna ora e min iniziali e data finale
          .w_ORAINI = PADL(ALLTRIM(STR(HOUR(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN) ))),2,'0')
          .w_MININI = PADL(ALLTRIM(STR(MINUTE(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN) ))),2,'0')
          .w_DATFIN = .w_DATINI+INT( (VAL(.w_ORAINI)+.w_CADURORE+INT((VAL(.w_MININI)+.w_CADURMIN)/60)) / 24)
          .w_ORAFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_ORAINI)+.w_CADURORE+INT((VAL(.w_MININI)+.w_CADURMIN)/60)), 24))),2)
          .w_MINFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_MININI)+.w_CADURMIN), 60),2)),2)
          .w_ANNOPROG = STR(YEAR(CP_TODATE(.w_DATFIN)), 4, 0)
    endwith
  endproc
  proc Calculate_SZNPLURDLB()
    with this
          * --- Controlli a checkform
          GSAG_BC3(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_SZNPLURDLB()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PAR_AGEN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAR_AGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAR_AGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACHKFES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PAR_AGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PAR_AGEN)
            select PACODAZI,PACHKFES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAR_AGEN = NVL(_Link_.PACODAZI,space(5))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PAR_AGEN = space(5)
      endif
      this.w_PACHKFES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAR_AGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOATTCOLL
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOATTCOLL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_TIPOATTCOLL)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CADURORE,CADURMIN,CADATINI,CAORASYS,CAMINPRE,CAFLPREA,CASTAATT,CADTOBSO,CAFLPDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_TIPOATTCOLL))
          select CACODICE,CADESCRI,CARAGGST,CADURORE,CADURMIN,CADATINI,CAORASYS,CAMINPRE,CAFLPREA,CASTAATT,CADTOBSO,CAFLPDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOATTCOLL)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_TIPOATTCOLL)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CADURORE,CADURMIN,CADATINI,CAORASYS,CAMINPRE,CAFLPREA,CASTAATT,CADTOBSO,CAFLPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_TIPOATTCOLL)+"%");

            select CACODICE,CADESCRI,CARAGGST,CADURORE,CADURMIN,CADATINI,CAORASYS,CAMINPRE,CAFLPREA,CASTAATT,CADTOBSO,CAFLPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TIPOATTCOLL) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oTIPOATTCOLL_1_4'),i_cWhere,'GSAG_MCA',"Tipi attivita",'GSAG1AAT.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CADURORE,CADURMIN,CADATINI,CAORASYS,CAMINPRE,CAFLPREA,CASTAATT,CADTOBSO,CAFLPDOC";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CARAGGST,CADURORE,CADURMIN,CADATINI,CAORASYS,CAMINPRE,CAFLPREA,CASTAATT,CADTOBSO,CAFLPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOATTCOLL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST,CADURORE,CADURMIN,CADATINI,CAORASYS,CAMINPRE,CAFLPREA,CASTAATT,CADTOBSO,CAFLPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_TIPOATTCOLL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_TIPOATTCOLL)
            select CACODICE,CADESCRI,CARAGGST,CADURORE,CADURMIN,CADATINI,CAORASYS,CAMINPRE,CAFLPREA,CASTAATT,CADTOBSO,CAFLPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOATTCOLL = NVL(_Link_.CACODICE,space(20))
      this.w_OGGATTCOLL = NVL(_Link_.CADESCRI,space(254))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(1))
      this.w_CADURORE = NVL(_Link_.CADURORE,0)
      this.w_CADURMIN = NVL(_Link_.CADURMIN,0)
      this.w_CADTIN = NVL(_Link_.CADATINI,ctot(""))
      this.w_CAORASYS = NVL(_Link_.CAORASYS,space(1))
      this.w_CAMINPRE = NVL(_Link_.CAMINPRE,0)
      this.w_CAFLPREA = NVL(_Link_.CAFLPREA,space(1))
      this.w_STATUS = NVL(_Link_.CASTAATT,space(1))
      this.w_OBTEST = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_CAFLPDOC = NVL(_Link_.CAFLPDOC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOATTCOLL = space(20)
      endif
      this.w_OGGATTCOLL = space(254)
      this.w_CARAGGST = space(1)
      this.w_CADURORE = 0
      this.w_CADURMIN = 0
      this.w_CADTIN = ctot("")
      this.w_CAORASYS = space(1)
      this.w_CAMINPRE = 0
      this.w_CAFLPREA = space(1)
      this.w_STATUS = space(1)
      this.w_OBTEST = ctod("  /  /  ")
      this.w_CAFLPDOC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CARAGGST#'Z') AND (.w_OBTEST>.w_DTBSO_CAUMATTI OR EMPTY(.w_OBTEST))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Tipo attivit� incongruente o obsoleto")
        endif
        this.w_TIPOATTCOLL = space(20)
        this.w_OGGATTCOLL = space(254)
        this.w_CARAGGST = space(1)
        this.w_CADURORE = 0
        this.w_CADURMIN = 0
        this.w_CADTIN = ctot("")
        this.w_CAORASYS = space(1)
        this.w_CAMINPRE = 0
        this.w_CAFLPREA = space(1)
        this.w_STATUS = space(1)
        this.w_OBTEST = ctod("  /  /  ")
        this.w_CAFLPDOC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOATTCOLL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOATTCOLL_1_4.value==this.w_TIPOATTCOLL)
      this.oPgFrm.Page1.oPag.oTIPOATTCOLL_1_4.value=this.w_TIPOATTCOLL
    endif
    if not(this.oPgFrm.Page1.oPag.oOGGATTCOLL_1_6.value==this.w_OGGATTCOLL)
      this.oPgFrm.Page1.oPag.oOGGATTCOLL_1_6.value=this.w_OGGATTCOLL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_9.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_9.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_11.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_11.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_12.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_12.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_16.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_16.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_1_17.value==this.w_ORAFIN)
      this.oPgFrm.Page1.oPag.oORAFIN_1_17.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_19.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_19.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOINI_1_22.value==this.w_GIOINI)
      this.oPgFrm.Page1.oPag.oGIOINI_1_22.value=this.w_GIOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOFIN_1_24.value==this.w_GIOFIN)
      this.oPgFrm.Page1.oPag.oGIOFIN_1_24.value=this.w_GIOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oATCAUATT_ORIG_1_53.value==this.w_ATCAUATT_ORIG)
      this.oPgFrm.Page1.oPag.oATCAUATT_ORIG_1_53.value=this.w_ATCAUATT_ORIG
    endif
    if not(this.oPgFrm.Page1.oPag.oATOGGETT_ORIG_1_54.value==this.w_ATOGGETT_ORIG)
      this.oPgFrm.Page1.oPag.oATOGGETT_ORIG_1_54.value=this.w_ATOGGETT_ORIG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_OR_1_56.value==this.w_DATINI_OR)
      this.oPgFrm.Page1.oPag.oDATINI_OR_1_56.value=this.w_DATINI_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_OR_1_58.value==this.w_ORAINI_OR)
      this.oPgFrm.Page1.oPag.oORAINI_OR_1_58.value=this.w_ORAINI_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_OR_1_59.value==this.w_MININI_OR)
      this.oPgFrm.Page1.oPag.oMININI_OR_1_59.value=this.w_MININI_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_OR_1_63.value==this.w_DATFIN_OR)
      this.oPgFrm.Page1.oPag.oDATFIN_OR_1_63.value=this.w_DATFIN_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_OR_1_64.value==this.w_ORAFIN_OR)
      this.oPgFrm.Page1.oPag.oORAFIN_OR_1_64.value=this.w_ORAFIN_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_OR_1_65.value==this.w_MINFIN_OR)
      this.oPgFrm.Page1.oPag.oMINFIN_OR_1_65.value=this.w_MINFIN_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOINI_OR_1_67.value==this.w_GIOINI_OR)
      this.oPgFrm.Page1.oPag.oGIOINI_OR_1_67.value=this.w_GIOINI_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOFIN_OR_1_68.value==this.w_GIOFIN_OR)
      this.oPgFrm.Page1.oPag.oGIOFIN_OR_1_68.value=this.w_GIOFIN_OR
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_TIPOATTCOLL)) or not((.w_CARAGGST#'Z') AND (.w_OBTEST>.w_DTBSO_CAUMATTI OR EMPTY(.w_OBTEST))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOATTCOLL_1_4.SetFocus()
            i_bnoObbl = !empty(.w_TIPOATTCOLL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tipo attivit� incongruente o obsoleto")
          case   (empty(.w_OGGATTCOLL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOGGATTCOLL_1_6.SetFocus()
            i_bnoObbl = !empty(.w_OGGATTCOLL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_ORAINI) < 24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAINI_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   (empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_16.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_ORAFIN) < 24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAFIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   (empty(.w_DATFIN_OR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_OR_1_63.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN_OR)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_kcl
      * --- Controlli Finali
      IF i_bRes
         .w_RESCHK=0
         Ah_Msg('Controlli finali...',.T.)
         .NotifyEvent('ChkFinali')
         WAIT CLEAR
         IF .w_RESCHK<>0
           i_bRes=.f.
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOATTCOLL = this.w_TIPOATTCOLL
    this.o_DATINI = this.w_DATINI
    this.o_ORAINI = this.w_ORAINI
    this.o_MININI = this.w_MININI
    this.o_DATFIN = this.w_DATFIN
    this.o_CADURORE = this.w_CADURORE
    this.o_CADURMIN = this.w_CADURMIN
    this.o_CADTIN = this.w_CADTIN
    this.o_DATINI_OR = this.w_DATINI_OR
    this.o_DATFIN_OR = this.w_DATFIN_OR
    return

enddefine

* --- Define pages as container
define class tgsag_kclPag1 as StdContainer
  Width  = 591
  height = 246
  stdWidth  = 591
  stdheight = 246
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPOATTCOLL_1_4 as StdField with uid="TTYLZWPDVI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TIPOATTCOLL", cQueryName = "TIPOATTCOLL",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Tipo attivit� incongruente o obsoleto",;
    ToolTipText = "Tipo attivita collegata",;
    HelpContextID = 226543511,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=67, Top=127, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_TIPOATTCOLL"

  func oTIPOATTCOLL_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPOATTCOLL_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOATTCOLL_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oTIPOATTCOLL_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'GSAG1AAT.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oTIPOATTCOLL_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_TIPOATTCOLL
     i_obj.ecpSave()
  endproc

  add object oOGGATTCOLL_1_6 as StdField with uid="ULMTURTUIY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_OGGATTCOLL", cQueryName = "OGGATTCOLL",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto attivit� collegata",;
    HelpContextID = 207886859,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=224, Top=127, InputMask=replicate('X',254)

  add object oDATINI_1_9 as StdField with uid="MYGDNXHRTP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio attivit� collegata",;
    HelpContextID = 138698294,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=103, Top=157

  func oDATINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_PACHKFES='N' OR Chkfestivi(.w_DATINI, .w_PACHKFES ))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oORAINI_1_11 as StdField with uid="VGJIHEDZAE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora inizio attivit� collegata",;
    HelpContextID = 138624998,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=281, Top=157, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_12 as StdField with uid="NXNGNCXJWL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti inizio attivit� collegata",;
    HelpContextID = 138675910,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=323, Top=157, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_16 as StdField with uid="BMQMJCRZZX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine attivit� collegata",;
    HelpContextID = 217144886,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=103, Top=184

  func oDATFIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !( (.w_PACHKFES='N' OR Chkfestivi(.w_DATFIN , .w_PACHKFES )))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oORAFIN_1_17 as StdField with uid="TRWSSPBSKC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora fine attivit� collegata",;
    HelpContextID = 217071590,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=281, Top=184, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_19 as StdField with uid="OFENOOOJLC",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti fine attivit� collegata",;
    HelpContextID = 217122502,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=323, Top=184, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc

  add object oGIOINI_1_22 as StdField with uid="DPWOUXIATY",rtseq=15,rtrep=.f.,;
    cFormVar = "w_GIOINI", cQueryName = "GIOINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 138679910,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=67, Top=157, InputMask=replicate('X',3)

  add object oGIOFIN_1_24 as StdField with uid="EKMCUZZKKX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_GIOFIN", cQueryName = "GIOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 217126502,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=67, Top=184, InputMask=replicate('X',3)


  add object oBtn_1_25 as StdButton with uid="ERHOXVEEGI",left=484, top=197, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 169232614;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="TKCMXZXILU",left=537, top=197, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176521286;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oATCAUATT_ORIG_1_53 as StdField with uid="TXSDADBHUK",rtseq=41,rtrep=.f.,;
    cFormVar = "w_ATCAUATT_ORIG", cQueryName = "ATCAUATT_ORIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 177613238,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=67, Top=32, InputMask=replicate('X',20)

  add object oATOGGETT_ORIG_1_54 as StdField with uid="TZQSFQQQZB",rtseq=42,rtrep=.f.,;
    cFormVar = "w_ATOGGETT_ORIG", cQueryName = "ATOGGETT_ORIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 124742070,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=224, Top=32, InputMask=replicate('X',254)

  add object oDATINI_OR_1_56 as StdField with uid="PBNLMIZKKY",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DATINI_OR", cQueryName = "DATINI_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 138699685,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=103, Top=56

  add object oORAINI_OR_1_58 as StdField with uid="BZGWDFCLAT",rtseq=44,rtrep=.f.,;
    cFormVar = "w_ORAINI_OR", cQueryName = "ORAINI_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 138626389,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=281, Top=56, InputMask=replicate('X',2)

  add object oMININI_OR_1_59 as StdField with uid="TRJOELSXSC",rtseq=45,rtrep=.f.,;
    cFormVar = "w_MININI_OR", cQueryName = "MININI_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 138677301,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=323, Top=56, InputMask=replicate('X',2)

  add object oDATFIN_OR_1_63 as StdField with uid="SRJDXINBFP",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DATFIN_OR", cQueryName = "DATFIN_OR",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 217146277,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=103, Top=79

  add object oORAFIN_OR_1_64 as StdField with uid="EBIXKXULCN",rtseq=47,rtrep=.f.,;
    cFormVar = "w_ORAFIN_OR", cQueryName = "ORAFIN_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 217072981,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=281, Top=79, InputMask=replicate('X',2)

  add object oMINFIN_OR_1_65 as StdField with uid="VQMTKUWDZZ",rtseq=48,rtrep=.f.,;
    cFormVar = "w_MINFIN_OR", cQueryName = "MINFIN_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 217123893,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=323, Top=79, InputMask=replicate('X',2)

  add object oGIOINI_OR_1_67 as StdField with uid="CLWGZXHNUV",rtseq=49,rtrep=.f.,;
    cFormVar = "w_GIOINI_OR", cQueryName = "GIOINI_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 138681301,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=67, Top=56, InputMask=replicate('X',3)

  add object oGIOFIN_OR_1_68 as StdField with uid="HMZDOQVRPI",rtseq=50,rtrep=.f.,;
    cFormVar = "w_GIOFIN_OR", cQueryName = "GIOFIN_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 217127893,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=67, Top=79, InputMask=replicate('X',3)

  add object oStr_1_2 as StdString with uid="TKLRZQFOPL",Visible=.t., Left=31, Top=128,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="CDWWSMMEYG",Visible=.t., Left=16, Top=159,;
    Alignment=1, Width=46, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="BYZPFCFPKB",Visible=.t., Left=222, Top=159,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="FXUDOJAZEE",Visible=.t., Left=309, Top=159,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="GKUXCCVURZ",Visible=.t., Left=16, Top=186,;
    Alignment=1, Width=46, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="SAALUVLINU",Visible=.t., Left=222, Top=186,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="FHAFBDYBRG",Visible=.t., Left=309, Top=186,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="BMXCFAXAEX",Visible=.t., Left=31, Top=33,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="IVREAYBIIO",Visible=.t., Left=17, Top=58,;
    Alignment=1, Width=45, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="RDHFZDATNW",Visible=.t., Left=222, Top=58,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="NWZPATLZHT",Visible=.t., Left=309, Top=58,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="YDCJVCFCQQ",Visible=.t., Left=22, Top=81,;
    Alignment=1, Width=40, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="FZRJKEVHHA",Visible=.t., Left=222, Top=81,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="MVZCGHYUIT",Visible=.t., Left=309, Top=81,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="FXSKSMUOTY",Visible=.t., Left=17, Top=8,;
    Alignment=0, Width=105, Height=18,;
    Caption="Attivit� di origine"  ;
  , bGlobalFont=.t.

  add object oBox_1_51 as StdBox with uid="CMCUCARSOE",left=11, top=24, width=573,height=84
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kcl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
