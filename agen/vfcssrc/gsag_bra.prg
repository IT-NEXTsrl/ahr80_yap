* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bra                                                        *
*              Ricerca attivit�                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-06                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pTIPO,pCursor,pCAUATT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bra",oParentObject,m.pOPER,m.pTIPO,m.pCursor,m.pCAUATT)
return(i_retval)

define class tgsag_bra as StdBatch
  * --- Local variables
  pOPER = space(1)
  pTIPO = space(1)
  pCursor = space(10)
  pCAUATT = space(10)
  w_OBJECT = .NULL.
  w_OBJ = .NULL.
  w_ObjMDA = .NULL.
  w_FOUND = .f.
  NewCausale = space(10)
  w_DESNOM = space(60)
  w_OBJPRAT = .NULL.
  * --- WorkFile variables
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia l'anagrafica delle Attivit� all'interno della Ricerca attivit� (mask GSAG_KRA)
    * --- Parametri pOPER
    *     V :  Visualizza
    *     M: Modifica
    *     N: Nuovo
    *     E: da cruscotto eventi
    * --- Parametri pTIPO
    *     A  :  Attivit�
    *     P  : Preavvisi
    *     M : Memo
    *     Z: Prestazioni
    * --- Cursore evento
    * --- Tipo attivit� passato
    if !ISNULL(oParentObject)
    endif
    * --- Istanzio l'anagrafica
    if Vartype(this.pTipo)<>"C"
      this.pTIPO = "A"
    endif
    this.w_OBJECT = iif(this.pTIPO="Z",GSAG_APR(),GSAG_AAT())
    if this.pTIPO="Z"
      this.w_OBJECT.oPgFrm.pagecount = 2
    endif
    * --- Controllo se ha passato il test di accesso
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    do case
      case this.pOPER $ "VMPR"
        * --- Carico il record selezionato
        this.w_OBJECT.ECPFILTER()     
        do case
          case this.pTIPO = "A"
            this.w_OBJECT.w_ATSERIAL = this.oParentObject.w_ATSERIAL
          case this.pTIPO = "P"
            this.w_OBJECT.w_ATSERIAL = this.oParentObject.w_ATSERIAL2
          case this.pTIPO = "M"
            this.w_OBJECT.w_ATSERIAL = this.oParentObject.w_ATSERIAL3
          case this.pTIPO = "Z"
            this.w_OBJECT.w_PRSERIAL = this.oParentObject.w_ATSERIAL
        endcase
        this.w_OBJECT.ECPSAVE()     
        if (this.pTIPO="Z" AND Not Empty( this.w_OBJECT.w_PRSERIAL )) OR (this.pTIPO<>"Z" AND Not Empty( this.w_OBJECT.w_ATSERIAL ))
          * --- Se attivit� disponibile proseguo...
          if this.pOPER = "M" or this.pOPER = "R"
            * --- Vado in modifica
            this.w_OBJECT.ECPEDIT()     
          endif
          if (this.pOPER = "P" or this.pOPER = "R" ) AND this.pTIPO <> "Z"
            * --- Si posiziona sulla riga della prestazione selezionata
            this.w_OBJECT.oPgFrm.ActivePage = 2
            * --- Tenta di posizionarsi sulla riga della prestazione
            this.w_ObjMDA = this.w_OBJECT.GSAG_MDA
            this.w_FOUND = .F.
            SELECT (this.w_ObjMDA.cTrsName)
            if reccount()<>0
              * --- Si posiziona sulla prima riga
              this.w_ObjMDA.FirstRow()     
              do while ! this.w_ObjMDA.Eof_Trs() AND !this.w_FOUND
                this.w_ObjMDA.SetRow()     
                if this.w_ObjMDA.w_CPROWORD = this.oParentObject.w_PrestRowOrd
                  * --- E' la riga della prestazione, marca la posizione
                  this.w_ObjMDA.MarkPos()     
                  this.w_FOUND = .T.
                else
                  this.w_ObjMDA.NextRow()     
                endif
              enddo
              if this.w_FOUND
                * --- Si posiziona sulla riga su cui ha fatto la MarkPos
                this.w_ObjMDA.RePos()     
              endif
            endif
          endif
        else
          this.w_OBJECT.ECPQUIT()     
        endif
        this.w_OBJECT = NULL
      case this.pOPER $ "N-E"
        if !ISNULL(oParentObject) AND (UPPER(this.oParentObject.class)="TGSAG_KRA" or UPPER(this.oParentObject.class)="TGSFA_AEV")
          * --- Assegno alla var. w_MaskCalend l'oggetto rappresentato da GSAG_KRA
          this.w_OBJECT.w_MaskEleAtt = this.oParentObject
        endif
        * --- Vado in caricamento
        this.w_OBJECT.ECPLOAD()     
        * --- Possiamo effettuare il link sulla causale
        if Vartype(this.pCAUATT)="C" OR (this.pOPER="E" AND !ISNULL(oParentObject) AND !EMPTY(this.oParentObject.w_ATCAUATT))
          if Vartype(this.pCAUATT)="C"
            this.NewCausale = this.pCAUATT
          else
            this.NewCausale = this.oParentObject.w_ATCAUATT
          endif
          this.w_OBJECT.w_ATCAUATT = this.NewCausale
          = setvaluelinked ( "M" , this.w_object , "w_ATCAUATT" , this.NewCausale)
          * --- Viene attivato il controllo sul campo Pratica e su di esso eseguito il check
          this.w_OBJ = this.w_OBJECT.GetcTRL("w_ATCAUATT")
          this.w_OBJ.Check()     
          this.w_OBJ.Valid()     
          * --- Aggiornamento variabili nell'anagrafica
          this.w_OBJECT.SetControlsValue()     
          this.w_OBJECT.Mhidecontrols()     
          * --- Viene simulata la notifica dell'evento all'interno dell'anagrafica per poi fare eseguire la routine ad esso associata.
          this.w_OBJ = this.w_OBJECT.NotifyEvent("w_ATCAUATT Changed")
        endif
        * --- Se lanciato da cruscotto eventi
        if this.pOPER="E" 
          this.w_OBJECT.w_GENDAEV = .T.
          if !ISNULL(oParentObject) AND !EMPTY(this.oParentObject.w_EVIDRICH)
            gsag_ble(this,this.oParentObject.w_EVIDRICH,"w_ATSEREVE",this.w_OBJECT,"w_ATCODNOM")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Valorizzo i campi dell'attivit� come sono indicati negli eventi
            this.w_OBJECT.w_ATCODNOM = this.oParentObject.w_EVNOMINA
            = setvaluelinked ( "M" , this.w_object , "w_ATCODNOM" , this.oParentObject.w_EVNOMINA)
            this.w_OBJECT.w_ATCONTAT = this.oParentObject.w_EVCODPAR
            this.w_OBJECT.w_ATPERSON = left(this.oParentObject.w_EVRIFPER,60)
            this.w_OBJECT.w_ATLOCALI = this.oParentObject.w_EVLOCALI
            this.w_OBJECT.w_ATTELEFO = this.oParentObject.w_EV_PHONE
            this.w_OBJECT.w_ATCELLUL = left(this.oParentObject.w_EVNUMCEL,18)
            this.w_OBJECT.w_AT_EMAIL = this.oParentObject.w_EV_EMAIL
            this.w_OBJECT.w_AT_EMPEC = this.oParentObject.w_EV_EMPEC
            this.w_OBJECT.w_ATEVANNO = this.oParentObject.w_EV__ANNO
            * --- Aggiornamento variabili nell'anagrafica
            this.w_OBJECT.SetControlsValue()     
          else
            if !ISNULL(oParentObject) AND !EMPTY(this.oParentObject.w_EVNOMINA)
              this.w_OBJECT.w_ATCODNOM = this.oParentObject.w_EVNOMINA
              = setvaluelinked ( "M" , this.w_object , "w_ATCODNOM" , this.oParentObject.w_EVNOMINA)
              * --- Viene attivato il controllo sul campo Pratica e su di esso eseguito il check
              this.w_OBJ = this.w_OBJECT.GetcTRL("w_ATCODNOM")
              this.w_OBJ.Check()     
              * --- Valorizzo i campi dell'attivit� come sono indicati negli eventi
              this.w_OBJECT.w_ATCONTAT = this.oParentObject.w_EVCODPAR
              this.w_OBJECT.w_ATPERSON = left(this.oParentObject.w_EVRIFPER,60)
              this.w_OBJECT.w_ATLOCALI = this.oParentObject.w_EVLOCALI
              this.w_OBJECT.w_ATTELEFO = this.oParentObject.w_EV_PHONE
              this.w_OBJECT.w_ATCELLUL = left(this.oParentObject.w_EVNUMCEL,18)
              this.w_OBJECT.w_AT_EMAIL = this.oParentObject.w_EV_EMAIL
              this.w_OBJECT.w_AT_EMPEC = this.oParentObject.w_EV_EMPEC
              this.w_OBJECT.w_ATEVANNO = this.oParentObject.w_EV__ANNO
              * --- Read from OFF_NOMI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "NODESCRI"+;
                  " from "+i_cTable+" OFF_NOMI where ";
                      +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_EVNOMINA);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  NODESCRI;
                  from (i_cTable) where;
                      NOCODICE = this.oParentObject.w_EVNOMINA;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DESNOM = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_OBJECT.w_DESNOMIN = this.w_DESNOM
              * --- Aggiornamento variabili nell'anagrafica
              this.w_OBJECT.SetControlsValue()     
              * --- Viene simulata la notifica dell'evento all'interno dell'anagrafica per poi fare eseguire la routine ad esso associata.
              this.w_OBJ = this.w_OBJECT.NotifyEvent("w_ATCODNOM Changed")
            endif
          endif
          * --- ripristino la variabile per non interferire con il ricalcolo del prezzo dovuto ad altri eventi sulla maschera
          this.w_OBJECT.w_GENDAEV = .F.
        endif
        if !ISNULL(oParentObject) AND UPPER(this.oParentObject.class)="TGSAG_KRA"
          * --- Se in elenco attivit� la data di inizio selezione (campo "Da")
          *     1) � vuota oppure precedente alla data di sistema, 
          *     la data di inizio della nuova attivit� sar� uguale alla data di sistema
          *     2) � successiva alla data di sistema,
          *      la data di inizio della nuova attivit� sar� uguale alla data di inizio selezione letta dalla maschera di elenco attivit�.
          if Not Empty(this.w_OBJECT.w_MaskEleAtt.w_DATINI) 
            this.w_OBJECT.w_DATINI = iif(this.w_OBJECT.w_MaskEleAtt.w_DATINI>=i_datsys,this.w_OBJECT.w_MaskEleAtt.w_DATINI,i_datsys)
            this.w_OBJECT.w_DATFIN = this.w_OBJECT.w_DATINI
            this.w_OBJECT.SetControlsValue()     
          endif
        endif
        * --- Viene valorizzato il campo Pratica con quella impostata in maschera
        if !ISNULL(oParentObject) AND !EMPTY(this.oParentObject.w_CODPRA)
          * --- Commessa costi
          this.w_OBJECT.w_ATCODPRA = this.oParentObject.w_CODPRA
          this.w_OBJPRAT = this.w_OBJECT.getctrl("w_ATCODPRA")
          this.w_OBJPRAT.Check()     
          this.w_OBJPRAT = null
          if .F. AND this.w_OBJECT.BDONTREPORTERROR AND Isalt()
            this.w_OBJECT.cFunction = "Query"
            this.w_OBJECT.Ecpquit()     
            this.w_OBJECT = null
            i_retcode = 'stop'
            return
          else
            * --- Adesso il campo � visibile e quindi dobbiamo permettere il link immediatamente
            this.w_OBJ = this.w_OBJECT.NotifyEvent("Agglink")
            this.w_OBJECT.SetControlsValue()     
            * --- Viene simulata la notifica dell'evento all'interno dell'anagrafica per poi fare eseguire la routine ad esso associata.
            this.w_OBJ = this.w_OBJECT.NotifyEvent("w_ATCODPRA Changed")
          endif
          * --- Commessa ricavi
          this.w_OBJECT.w_ATCOMRIC = this.oParentObject.w_CODPRA
          * --- Adesso il campo � visibile e quindi dobbiamo permettere il link immediatamente
          this.w_OBJ = this.w_OBJECT.NotifyEvent("Agglink")
          this.w_OBJECT.SetControlsValue()     
          * --- Viene simulata la notifica dell'evento all'interno dell'anagrafica per poi fare eseguire la routine ad esso associata.
          this.w_OBJ = this.w_OBJECT.NotifyEvent("w_ATCOMRIC Changed")
        endif
        if Vartype(this.pCursor)="C" and g_AGFA="S"
          this.w_OBJECT.w_CUR_EVE = this.pCursor
        endif
        if Not Empty(this.w_OBJECT.w_ATCAUATT)
          this.w_OBJ = this.w_OBJECT.getctrl("w_ATCAUATT")
          this.w_OBJ.Setfocus()     
        endif
        this.w_OBJECT = .null.
        this.w_OBJ = .null.
    endcase
  endproc


  proc Init(oParentObject,pOPER,pTIPO,pCursor,pCAUATT)
    this.pOPER=pOPER
    this.pTIPO=pTIPO
    this.pCursor=pCursor
    this.pCAUATT=pCAUATT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_NOMI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pTIPO,pCursor,pCAUATT"
endproc
