* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bpp                                                        *
*              Elabora partecipanti                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-01-13                                                      *
* Last revis.: 2011-02-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper,pObjZoom,pCodPart,pParent
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bpp",oParentObject,m.pOper,m.pObjZoom,m.pCodPart,m.pParent)
return(i_retval)

define class tgsag_bpp as StdBatch
  * --- Local variables
  pOper = space(10)
  pObjZoom = .NULL.
  pCodPart = space(5)
  pParent = .NULL.
  CURSORE = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                               Elabora partecipanti 
    *     Lanciato da : GSAG_KRA, GSAG_KAG, GSAG_SAT
    this.CURSORE = this.pObjZoom.cCursor
    do case
      case this.pOper=="ATTIVAZOOM"
        * --- Eventi: Blank, w_PATIPRIS Changed
        * --- Esegue la query dello zoom
        if EMPTY(this.pCodPart)
          * --- Seleziona tutte le righe
          UPDATE (this.CURSORE) SET XCHK = 1
        else
          * --- Seleziona una riga
          UPDATE (this.CURSORE) SET XCHK = 1 WHERE DPCODICE=this.pCodPart
        endif
        Select (this.CURSORE) 
 Go Top
      case this.pOper=="SELRIGA_ELE" OR this.pOper=="SELRIGA"
        * --- Evento: w_CODPART Changed
        if NOT EMPTY(this.pCodPart)
          * --- Se selezionato il partecipante viene selezionata la relativa riga
          UPDATE (this.CURSORE) SET XCHK = 0
          UPDATE (this.CURSORE) SET XCHK = 1 WHERE DPCODICE=this.pCodPart
        else
          * --- Se azzerato il partecipante vengono selezionate tutte le righe
          UPDATE (this.CURSORE) SET XCHK = 1
        endif
        Select (this.CURSORE) 
 Go Top
        * --- Se si proviene da Elenco Attivit�
        if this.pOper=="SELRIGA_ELE"
          * --- Lancia zoom preavvisi
          this.pParent.notifyevent("CodpartChange1")
          * --- Lancia zoom attivit�
          this.pParent.notifyevent("CodpartChange2")
          * --- Attiva font in attivit�
          this.pParent.notifyevent("CodpartChange3")
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper,pObjZoom,pCodPart,pParent)
    this.pOper=pOper
    this.pObjZoom=pObjZoom
    this.pCodPart=pCodPart
    this.pParent=pParent
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper,pObjZoom,pCodPart,pParent"
endproc
