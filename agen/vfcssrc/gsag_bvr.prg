* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bvr                                                        *
*              Elaborazioni in inserimento provvisorio prestazioni             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-08-07                                                      *
* Last revis.: 2014-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bvr",oParentObject,m.pOper)
return(i_retval)

define class tgsag_bvr as StdBatch
  * --- Local variables
  pOper = space(5)
  w_ObjMTC = .NULL.
  w_CALCONC = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  UNIMIS_idx=0
  TAR_CONC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                                                  Elaborazioni in Inserimento provvisorio prestazioni
    * --- Variabili per lettura da ART_ICOL
    * --- Variabili per Unit� di misura
    do case
      case this.pOper == "LETTURA"
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARSTACOD,ARFLSPAN,ARPRESTA,ARFLUNIV,ARNUMPRE,AROPERAT,ARFLUSEP,ARMOLTIP,ARPREZUM,ARTIPRIG,ARTIPRI2"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_RCOD1ATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1,ARUNMIS2,ARFLSERG,ARTIPART,ARSTACOD,ARFLSPAN,ARPRESTA,ARFLUNIV,ARNUMPRE,AROPERAT,ARFLUSEP,ARMOLTIP,ARPREZUM,ARTIPRIG,ARTIPRI2;
            from (i_cTable) where;
                ARCODART = this.oParentObject.w_RCOD1ATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.oParentObject.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.oParentObject.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          this.oParentObject.w_FLSERG = NVL(cp_ToDate(_read_.ARFLSERG),cp_NullValue(_read_.ARFLSERG))
          this.oParentObject.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
          this.oParentObject.w_NOTIFICA = NVL(cp_ToDate(_read_.ARSTACOD),cp_NullValue(_read_.ARSTACOD))
          this.oParentObject.w_FLSPAN = NVL(cp_ToDate(_read_.ARFLSPAN),cp_NullValue(_read_.ARFLSPAN))
          this.oParentObject.w_PRESTA = NVL(cp_ToDate(_read_.ARPRESTA),cp_NullValue(_read_.ARPRESTA))
          this.oParentObject.w_FLUNIV = NVL(cp_ToDate(_read_.ARFLUNIV),cp_NullValue(_read_.ARFLUNIV))
          this.oParentObject.w_NUMPRE = NVL(cp_ToDate(_read_.ARNUMPRE),cp_NullValue(_read_.ARNUMPRE))
          this.oParentObject.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.oParentObject.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
          this.oParentObject.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.oParentObject.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
          this.oParentObject.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
          this.oParentObject.w_ARTIPRIG = NVL(cp_ToDate(_read_.ARTIPRIG),cp_NullValue(_read_.ARTIPRIG))
          this.oParentObject.w_ARTIPRI2 = NVL(cp_ToDate(_read_.ARTIPRI2),cp_NullValue(_read_.ARTIPRI2))
          this.oParentObject.w_TIPRIGCA = NVL(cp_ToDate(_read_.ARTIPRIG),cp_NullValue(_read_.ARTIPRIG))
          this.oParentObject.w_TIPRI2CA = NVL(cp_ToDate(_read_.ARTIPRI2),cp_NullValue(_read_.ARTIPRI2))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_PRUNIMIS = this.oParentObject.w_UNIMIS
        this.oParentObject.w_PRQTAMOV = IIF(this.oParentObject.w_TIPART="DE", 0, 1)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ,UMMODUM2"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UNIMIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ,UMMODUM2;
            from (i_cTable) where;
                UMCODICE = this.oParentObject.w_UNIMIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESUNI = NVL(cp_ToDate(_read_.UMDESCRI),cp_NullValue(_read_.UMDESCRI))
          this.oParentObject.w_CHKTEMP = NVL(cp_ToDate(_read_.UMFLTEMP),cp_NullValue(_read_.UMFLTEMP))
          this.oParentObject.w_DUR_ORE = NVL(cp_ToDate(_read_.UMDURORE),cp_NullValue(_read_.UMDURORE))
          this.oParentObject.w_FL_FRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          this.oParentObject.w_FLFRAZ1 = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          this.oParentObject.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pOper == "LEGGETARCONC"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ,UMMODUM2"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UNIMIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ,UMMODUM2;
            from (i_cTable) where;
                UMCODICE = this.oParentObject.w_UNIMIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESUNI = NVL(cp_ToDate(_read_.UMDESCRI),cp_NullValue(_read_.UMDESCRI))
          this.oParentObject.w_CHKTEMP = NVL(cp_ToDate(_read_.UMFLTEMP),cp_NullValue(_read_.UMFLTEMP))
          this.oParentObject.w_DUR_ORE = NVL(cp_ToDate(_read_.UMDURORE),cp_NullValue(_read_.UMDURORE))
          this.oParentObject.w_FL_FRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          this.oParentObject.w_FLFRAZ1 = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          this.oParentObject.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pOper == "IMPORT"
        * --- Lanciato dal bottone Import nelle Tariffe a tempo concordate (GSPR_MTC)
        this.w_ObjMTC = this.oParentObject.oParentObject
        this.w_ObjMTC.MarkPos()     
        * --- Select from TAR_CONC
        i_nConn=i_TableProp[this.TAR_CONC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TAR_CONC_idx,2],.t.,this.TAR_CONC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TAR_CONC ";
              +" where TAR_CONC.TCCODPRA="+cp_ToStrODBC(this.oParentObject.w_NUMPRA)+"";
               ,"_Curs_TAR_CONC")
        else
          select * from (i_cTable);
           where TAR_CONC.TCCODPRA=this.oParentObject.w_NUMPRA;
            into cursor _Curs_TAR_CONC
        endif
        if used('_Curs_TAR_CONC')
          select _Curs_TAR_CONC
          locate for 1=1
          do while not(eof())
          this.w_ObjMTC.AddRow()     
          this.w_ObjMTC.w_TCCODRIS = NVL(TCCODRIS, space(5))
          this.w_ObjMTC.w_TCCODPRE = NVL(TCCODPRE, space(20))
          this.w_ObjMTC.w_TCUNIMIS = TCUNIMIS
          this.w_ObjMTC.w_TCQTAMOV = TCQTAMOV
          this.w_ObjMTC.w_TCIMPORT = TCIMPORT
          this.w_ObjMTC.SaveRow()     
            select _Curs_TAR_CONC
            continue
          enddo
          use
        endif
        this.w_ObjMTC.RePos()     
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se sulla pratica � definita Tariffa concordata e
    *     se siamo in presenza di una tariffa a tempo
    if this.oParentObject.w_TARTEM = "C" AND this.oParentObject.w_PRESTA="P"
      * --- Dichiarazione array tariffe a tempo concordate
      DECLARE ARRTARCON (3,1)
      * --- Azzero l'array che verr� riempito dalla funzione Caltarcon()
      ARRTARCON(1)=0 
 ARRTARCON(2)= " " 
 ARRTARCON(3)=0
      this.w_CALCONC = CALTARCON(this.oParentObject.w_PRNUMPRA, this.oParentObject.w_DACODRES, this.oParentObject.w_DACODATT, @ARRTARCON)
      this.oParentObject.w_TARCON = ARRTARCON(1)
      this.oParentObject.w_PRUNIMIS = IIF(Not Empty(ARRTARCON(2)), ARRTARCON(2), this.oParentObject.w_PRUNIMIS)
      this.oParentObject.w_UNIMIS = IIF(Not Empty(ARRTARCON(2)), ARRTARCON(2), this.oParentObject.w_UNIMIS)
      this.oParentObject.w_UNMIS1 = IIF(Not Empty(ARRTARCON(2)), ARRTARCON(2), this.oParentObject.w_UNMIS1)
      this.oParentObject.w_PRQTAMOV = IIF(Not Empty(ARRTARCON(3)), ARRTARCON(3), this.oParentObject.w_PRQTAMOV)
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='UNIMIS'
    this.cWorkTables[3]='TAR_CONC'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_TAR_CONC')
      use in _Curs_TAR_CONC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
