* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mip                                                        *
*              Raggruppamenti attivit�                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-02-21                                                      *
* Last revis.: 2012-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_mip"))

* --- Class definition
define class tgsag_mip as StdTrsForm
  Top    = 12
  Left   = 28

  * --- Standard Properties
  Width  = 800
  Height = 380+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-30"
  HelpContextID=3528855
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PRO_ITER_IDX = 0
  CAUMATTI_IDX = 0
  cFile = "PRO_ITER"
  cKeySelect = "IPCODICE"
  cKeyWhere  = "IPCODICE=this.w_IPCODICE"
  cKeyDetail  = "IPCODICE=this.w_IPCODICE"
  cKeyWhereODBC = '"IPCODICE="+cp_ToStrODBC(this.w_IPCODICE)';

  cKeyDetailWhereODBC = '"IPCODICE="+cp_ToStrODBC(this.w_IPCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PRO_ITER.IPCODICE="+cp_ToStrODBC(this.w_IPCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PRO_ITER.CPROWORD '
  cPrg = "gsag_mip"
  cComment = "Raggruppamenti attivit�"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 15
  icon = "movi.ico"
  cAutoZoom = 'GSAG_MIP'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_IPCODICE = space(10)
  w_IPDESCRI = space(60)
  w_IPCODCAU = space(20)
  w_CPROWORD = 0
  w_IPDESCAU = space(254)
  w_IPNUMGIO = 0
  w_IPRIFPRO = 0
  w_CARAGGST = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_mip
  w_OBJMSK = .null.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PRO_ITER','gsag_mip')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_mipPag1","gsag_mip",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Raggruppamenti attivit�")
      .Pages(1).HelpContextID = 125469129
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIPCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CAUMATTI'
    this.cWorkTables[2]='PRO_ITER'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRO_ITER_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRO_ITER_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_IPCODICE = NVL(IPCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PRO_ITER where IPCODICE=KeySet.IPCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2],this.bLoadRecFilter,this.PRO_ITER_IDX,"gsag_mip")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRO_ITER')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRO_ITER.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PRO_ITER '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IPCODICE',this.w_IPCODICE  )
      select * from (i_cTable) PRO_ITER where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_IPCODICE = NVL(IPCODICE,space(10))
        .w_IPDESCRI = NVL(IPDESCRI,space(60))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PRO_ITER')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CARAGGST = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_IPCODCAU = NVL(IPCODCAU,space(20))
          if link_2_1_joined
            this.w_IPCODCAU = NVL(CACODICE201,NVL(this.w_IPCODCAU,space(20)))
            this.w_IPDESCAU = NVL(CADESCRI201,space(254))
            this.w_CARAGGST = NVL(CARAGGST201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_IPDESCAU = NVL(IPDESCAU,space(254))
          .w_IPNUMGIO = NVL(IPNUMGIO,0)
          .w_IPRIFPRO = NVL(IPRIFPRO,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_4.enabled = .oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_IPCODICE=space(10)
      .w_IPDESCRI=space(60)
      .w_IPCODCAU=space(20)
      .w_CPROWORD=10
      .w_IPDESCAU=space(254)
      .w_IPNUMGIO=0
      .w_IPRIFPRO=0
      .w_CARAGGST=space(1)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_IPCODCAU))
         .link_2_1('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(4,8,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRO_ITER')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsag_mip
    this.w_OBJMSK = this
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oIPCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oIPDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oBtn_1_4.enabled = .Page1.oPag.oBtn_1_4.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oIPCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oIPCODICE_1_1.enabled = .t.
        .Page1.oPag.oIPDESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PRO_ITER',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IPCODICE,"IPCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IPDESCRI,"IPDESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])
    i_lTable = "PRO_ITER"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PRO_ITER_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsag_sip with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_IPCODCAU C(20);
      ,t_CPROWORD N(5);
      ,t_IPDESCAU C(254);
      ,t_IPNUMGIO N(5);
      ,t_IPRIFPRO N(5);
      ,CPROWNUM N(10);
      ,t_CARAGGST C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mipbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIPCODCAU_2_1.controlsource=this.cTrsName+'.t_IPCODCAU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIPDESCAU_2_3.controlsource=this.cTrsName+'.t_IPDESCAU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIPNUMGIO_2_4.controlsource=this.cTrsName+'.t_IPNUMGIO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIPRIFPRO_2_5.controlsource=this.cTrsName+'.t_IPRIFPRO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(66)
    this.AddVLine(240)
    this.AddVLine(673)
    this.AddVLine(721)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPCODCAU_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])
      *
      * insert into PRO_ITER
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRO_ITER')
        i_extval=cp_InsertValODBCExtFlds(this,'PRO_ITER')
        i_cFldBody=" "+;
                  "(IPCODICE,IPDESCRI,IPCODCAU,CPROWORD,IPDESCAU"+;
                  ",IPNUMGIO,IPRIFPRO,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_IPCODICE)+","+cp_ToStrODBC(this.w_IPDESCRI)+","+cp_ToStrODBCNull(this.w_IPCODCAU)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_IPDESCAU)+;
             ","+cp_ToStrODBC(this.w_IPNUMGIO)+","+cp_ToStrODBC(this.w_IPRIFPRO)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRO_ITER')
        i_extval=cp_InsertValVFPExtFlds(this,'PRO_ITER')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'IPCODICE',this.w_IPCODICE)
        INSERT INTO (i_cTable) (;
                   IPCODICE;
                  ,IPDESCRI;
                  ,IPCODCAU;
                  ,CPROWORD;
                  ,IPDESCAU;
                  ,IPNUMGIO;
                  ,IPRIFPRO;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_IPCODICE;
                  ,this.w_IPDESCRI;
                  ,this.w_IPCODCAU;
                  ,this.w_CPROWORD;
                  ,this.w_IPDESCAU;
                  ,this.w_IPNUMGIO;
                  ,this.w_IPRIFPRO;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_IPCODCAU))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_ITER')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " IPDESCRI="+cp_ToStrODBC(this.w_IPDESCRI)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_ITER')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  IPDESCRI=this.w_IPDESCRI;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_IPCODCAU))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PRO_ITER
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_ITER')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " IPDESCRI="+cp_ToStrODBC(this.w_IPDESCRI)+;
                     ",IPCODCAU="+cp_ToStrODBCNull(this.w_IPCODCAU)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",IPDESCAU="+cp_ToStrODBC(this.w_IPDESCAU)+;
                     ",IPNUMGIO="+cp_ToStrODBC(this.w_IPNUMGIO)+;
                     ",IPRIFPRO="+cp_ToStrODBC(this.w_IPRIFPRO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_ITER')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      IPDESCRI=this.w_IPDESCRI;
                     ,IPCODCAU=this.w_IPCODCAU;
                     ,CPROWORD=this.w_CPROWORD;
                     ,IPDESCAU=this.w_IPDESCAU;
                     ,IPNUMGIO=this.w_IPNUMGIO;
                     ,IPRIFPRO=this.w_IPRIFPRO;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_IPCODCAU))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PRO_ITER
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_IPCODCAU))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRO_ITER_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_ITER_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CARAGGST with this.w_CARAGGST
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIPRIFPRO_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oIPRIFPRO_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.visible=!this.oPgFrm.Page1.oPag.oBtn_1_4.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IPCODCAU
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IPCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_IPCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_IPCODCAU))
          select CACODICE,CADESCRI,CARAGGST;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IPCODCAU)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_IPCODCAU)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_IPCODCAU)+"%");

            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IPCODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oIPCODCAU_2_1'),i_cWhere,'GSAG_MCA',"Tipi attivita",'GSAG1MIP.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IPCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_IPCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_IPCODCAU)
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IPCODCAU = NVL(_Link_.CACODICE,space(20))
      this.w_IPDESCAU = NVL(_Link_.CADESCRI,space(254))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_IPCODCAU = space(20)
      endif
      this.w_IPDESCAU = space(254)
      this.w_CARAGGST = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CARAGGST#'Z'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso")
        endif
        this.w_IPCODCAU = space(20)
        this.w_IPDESCAU = space(254)
        this.w_CARAGGST = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IPCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CACODICE as CACODICE201"+ ",link_2_1.CADESCRI as CADESCRI201"+ ",link_2_1.CARAGGST as CARAGGST201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on PRO_ITER.IPCODCAU=link_2_1.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and PRO_ITER.IPCODCAU=link_2_1.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oIPCODICE_1_1.value==this.w_IPCODICE)
      this.oPgFrm.Page1.oPag.oIPCODICE_1_1.value=this.w_IPCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oIPDESCRI_1_3.value==this.w_IPDESCRI)
      this.oPgFrm.Page1.oPag.oIPDESCRI_1_3.value=this.w_IPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPCODCAU_2_1.value==this.w_IPCODCAU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPCODCAU_2_1.value=this.w_IPCODCAU
      replace t_IPCODCAU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPCODCAU_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPDESCAU_2_3.value==this.w_IPDESCAU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPDESCAU_2_3.value=this.w_IPDESCAU
      replace t_IPDESCAU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPDESCAU_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPNUMGIO_2_4.value==this.w_IPNUMGIO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPNUMGIO_2_4.value=this.w_IPNUMGIO
      replace t_IPNUMGIO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPNUMGIO_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPRIFPRO_2_5.value==this.w_IPRIFPRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPRIFPRO_2_5.value=this.w_IPRIFPRO
      replace t_IPRIFPRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPRIFPRO_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'PRO_ITER')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_IPCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oIPCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_IPCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_CPROWORD)) AND not(Empty(t_IPCODCAU)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_CARAGGST#'Z') and not(empty(.w_IPCODCAU)) and (not(Empty(.w_CPROWORD)) AND not(Empty(.w_IPCODCAU)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPCODCAU_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore non ammesso")
        case   not(chkriga(.w_OBJMSK, .w_IPRIFPRO,.F.) and .w_IPRIFPRO < .w_CPROWORD) and (.w_IPNUMGIO # 0) and (not(Empty(.w_CPROWORD)) AND not(Empty(.w_IPCODCAU)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIPRIFPRO_2_5
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_CPROWORD)) AND not(Empty(.w_IPCODCAU))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND not(Empty(t_IPCODCAU)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_IPCODCAU=space(20)
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_IPDESCAU=space(254)
      .w_IPNUMGIO=0
      .w_IPRIFPRO=0
      .w_CARAGGST=space(1)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_IPCODCAU))
        .link_2_1('Full')
      endif
    endwith
    this.DoRTCalc(4,9,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_IPCODCAU = t_IPCODCAU
    this.w_CPROWORD = t_CPROWORD
    this.w_IPDESCAU = t_IPDESCAU
    this.w_IPNUMGIO = t_IPNUMGIO
    this.w_IPRIFPRO = t_IPRIFPRO
    this.w_CARAGGST = t_CARAGGST
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_IPCODCAU with this.w_IPCODCAU
    replace t_CPROWORD with this.w_CPROWORD
    replace t_IPDESCAU with this.w_IPDESCAU
    replace t_IPNUMGIO with this.w_IPNUMGIO
    replace t_IPRIFPRO with this.w_IPRIFPRO
    replace t_CARAGGST with this.w_CARAGGST
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mipPag1 as StdContainer
  Width  = 796
  height = 380
  stdWidth  = 796
  stdheight = 380
  resizeXpos=533
  resizeYpos=347
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIPCODICE_1_1 as StdField with uid="NTRGFMREIE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_IPCODICE", cQueryName = "IPCODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� collegate",;
    HelpContextID = 37134901,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=74, Top=25, InputMask=replicate('X',10)

  add object oIPDESCRI_1_3 as StdField with uid="CWNRHWHCUL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_IPDESCRI", cQueryName = "IPDESCRI",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 122720817,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=166, Top=25, InputMask=replicate('X',60)


  add object oBtn_1_4 as StdButton with uid="XVFSKZRQBE",left=740, top=15, width=48,height=45,;
    CpPicture="copy.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per duplicare il raggruppamento attivit�";
    , HelpContextID = 96906698;
    , tabstop=.f., Caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      do GSAG_KDR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction#"Query" OR (.cFunction="Query" AND EMPTY(.w_IPCODICE)))
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=15, top=71, width=774,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Progr.",Field2="IPCODCAU",Label2="Codice tipo",Field3="IPDESCAU",Label3="Oggetto da assegnare all'attivit�",Field4="IPNUMGIO",Label4="N. gg.",Field5="IPRIFPRO",Label5="Rif.prog.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 51184250

  add object oStr_1_2 as StdString with uid="ELNEZOVEUZ",Visible=.t., Left=22, Top=27,;
    Alignment=0, Width=43, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=5,top=90,;
    width=770+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=6,top=91,width=769+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*15*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CAUMATTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CAUMATTI'
        oDropInto=this.oBodyCol.oRow.oIPCODCAU_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsag_mipBodyRow as CPBodyRowCnt
  Width=760
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oIPCODCAU_2_1 as StdTrsField with uid="HAEJMNUUHI",rtseq=3,rtrep=.t.,;
    cFormVar="w_IPCODCAU",value=space(20),;
    ToolTipText = "Codice tipo",;
    HelpContextID = 137798181,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
   bGlobalFont=.t.,;
    Height=17, Width=167, Left=52, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_IPCODCAU"

  func oIPCODCAU_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oIPCODCAU_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oIPCODCAU_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oIPCODCAU_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'GSAG1MIP.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oIPCODCAU_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_IPCODCAU
    i_obj.ecpSave()
  endproc

  add object oCPROWORD_2_2 as StdTrsField with uid="ZMNGDPNNZJ",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Numero riga",;
    HelpContextID = 184922774,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], TabStop=.f.

  add object oIPDESCAU_2_3 as StdTrsField with uid="GDGIHEPEBY",rtseq=5,rtrep=.t.,;
    cFormVar="w_IPDESCAU",value=space(254),;
    ToolTipText = "Descrizione tipo",;
    HelpContextID = 122720805,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=426, Left=227, Top=0, InputMask=replicate('X',254)

  add object oIPNUMGIO_2_4 as StdTrsField with uid="YRWAGQDUHC",rtseq=6,rtrep=.t.,;
    cFormVar="w_IPNUMGIO",value=0,;
    ToolTipText = "Giorni di decorrenza",;
    HelpContextID = 207621589,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=660, Top=0, cSayPict=["@Z 99999"], cGetPict=["@Z 99999"]

  add object oIPRIFPRO_2_5 as StdTrsField with uid="YHYMZGGBLX",rtseq=7,rtrep=.t.,;
    cFormVar="w_IPRIFPRO",value=0,;
    ToolTipText = "Riferimento riga",;
    HelpContextID = 186364459,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=707, Top=0, cSayPict=["@Z 99999"], cGetPict=["@Z 99999"]

  func oIPRIFPRO_2_5.mCond()
    with this.Parent.oContained
      return (.w_IPNUMGIO # 0)
    endwith
  endfunc

  func oIPRIFPRO_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chkriga(.w_OBJMSK, .w_IPRIFPRO,.F.) and .w_IPRIFPRO < .w_CPROWORD)
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oIPCODCAU_2_1.When()
    return(.t.)
  proc oIPCODCAU_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oIPCODCAU_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=14
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mip','PRO_ITER','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IPCODICE=PRO_ITER.IPCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_mip
proc ChiusuraMaschera(pParent)
   pParent.w_OBJMSK=.null.
endproc
* --- Fine Area Manuale
