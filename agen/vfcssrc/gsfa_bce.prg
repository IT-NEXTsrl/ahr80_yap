* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bce                                                        *
*              Ricerca e visualizza corpo mail html                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-30                                                      *
* Last revis.: 2010-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bce",oParentObject,m.pOPER)
return(i_retval)

define class tgsfa_bce as StdBatch
  * --- Local variables
  pOPER = space(5)
  w_SERATTRI = space(15)
  w_IDCLADOC = space(15)
  w_IDMODALL = space(1)
  w_PATSTD = space(100)
  w_IDPATALL = space(254)
  w_TIPRAG = space(1)
  w_IDDATRAG = ctod("  /  /  ")
  w_IDNOMFIL = space(100)
  w_IDORIGIN = space(100)
  w_IDORIFIL = space(100)
  * --- WorkFile variables
  PRODINDI_idx=0
  PROMCLAS_idx=0
  PROMINDI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili per utilizzo batch document managment
    do case
      case this.pOPER=="GETBM"
        if this.oParentObject.w_DE__TIPO="M"
          * --- Read from PRODINDI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRODINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IDSERIAL"+;
              " from "+i_cTable+" PRODINDI where ";
                  +"IDVALATT = "+cp_ToStrODBC(this.oParentObject.w_EV__ANNO+this.oParentObject.w_EVSERIAL);
                  +" and IDTABKEY = "+cp_ToStrODBC("ANEVENTI");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IDSERIAL;
              from (i_cTable) where;
                  IDVALATT = this.oParentObject.w_EV__ANNO+this.oParentObject.w_EVSERIAL;
                  and IDTABKEY = "ANEVENTI";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERATTRI = NVL(cp_ToDate(_read_.IDSERIAL),cp_NullValue(_read_.IDSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from PRODINDI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRODINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" PRODINDI where ";
                  +"IDSERIAL = "+cp_ToStrODBC(this.w_SERATTRI);
                  +" and IDCODATT = "+cp_ToStrODBC("ZABODYMAIL");
                  +" and IDVALATT = "+cp_ToStrODBC("S");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  IDSERIAL = this.w_SERATTRI;
                  and IDCODATT = "ZABODYMAIL";
                  and IDVALATT = "S";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows>0
            * --- Read from PROMINDI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMINDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "IDCLADOC,IDMODALL,IDDATRAG,IDNOMFIL,IDORIGIN,IDORIFIL"+;
                " from "+i_cTable+" PROMINDI where ";
                    +"IDSERIAL = "+cp_ToStrODBC(this.w_SERATTRI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                IDCLADOC,IDMODALL,IDDATRAG,IDNOMFIL,IDORIGIN,IDORIFIL;
                from (i_cTable) where;
                    IDSERIAL = this.w_SERATTRI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_IDCLADOC = NVL(cp_ToDate(_read_.IDCLADOC),cp_NullValue(_read_.IDCLADOC))
              this.w_IDMODALL = NVL(cp_ToDate(_read_.IDMODALL),cp_NullValue(_read_.IDMODALL))
              this.w_IDDATRAG = NVL(cp_ToDate(_read_.IDDATRAG),cp_NullValue(_read_.IDDATRAG))
              this.w_IDNOMFIL = NVL(cp_ToDate(_read_.IDNOMFIL),cp_NullValue(_read_.IDNOMFIL))
              this.w_IDORIGIN = NVL(cp_ToDate(_read_.IDORIGIN),cp_NullValue(_read_.IDORIGIN))
              this.w_IDORIFIL = NVL(cp_ToDate(_read_.IDORIFIL),cp_NullValue(_read_.IDORIFIL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from PROMCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CDPATSTD,CDTIPRAG"+;
                " from "+i_cTable+" PROMCLAS where ";
                    +"CDCODCLA = "+cp_ToStrODBC(this.w_IDCLADOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CDPATSTD,CDTIPRAG;
                from (i_cTable) where;
                    CDCODCLA = this.w_IDCLADOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PATSTD = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
              this.w_TIPRAG = NVL(cp_ToDate(_read_.CDTIPRAG),cp_NullValue(_read_.CDTIPRAG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.oParentObject.w_HTML_URL = GSUT_BBI(this, "PATHALLEGATO")
            if Not Empty(this.oParentObject.w_HTML_URL) and Empty(JustDrive(this.oParentObject.w_HTML_URL)) and ! DIRECTORY(Alltrim(this.oParentObject.w_HTML_URL))
              * --- Non � stato indicato il driver nel path lo aggiungo
              this.oParentObject.w_HTML_URL = addbs(sys(5)+sys(2003))+Alltrim(this.oParentObject.w_HTML_URL)
            endif
          endif
        else
          this.oParentObject.w_HTML_URL = ""
          This.oparentobject.Gsfa_ace.w_HTML_URL=""
          This.oparentobject.Gsfa_ace.w_HTMLBROW.OBROWSER.Navigate(this.oParentObject.w_HTML_URL)
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PRODINDI'
    this.cWorkTables[2]='PROMCLAS'
    this.cWorkTables[3]='PROMINDI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
