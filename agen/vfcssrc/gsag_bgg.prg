* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bgg                                                        *
*              Aggiorna valori dettaglio inserimento provvisorio delle prestazioni*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-29                                                      *
* Last revis.: 2012-10-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bgg",oParentObject)
return(i_retval)

define class tgsag_bgg as StdBatch
  * --- Local variables
  w_ObjMPR = .NULL.
  w_OK = 0
  w_OKCOSTO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna valori dettaglio inserimento provvisorio delle prestazioni
    this.w_OK = 0
    this.w_OKCOSTO = 0
    this.w_ObjMPR = iif(isalt(),this.oParentObject.GSAG_MPR,this.oParentObject.GSAG_MPP)
    this.w_ObjMPR.w_NOCALC = .F.
    SELECT (this.w_ObjMPR.cTrsName)
    this.w_ObjMPR.MarkPos()     
    * --- Memorizzo il primo partecipante
    this.w_ObjMPR.FirstRow()     
    * --- Necessario per aggiornare le o_ 
    do while NOT this.w_ObjMPR.Eof_Trs()
      this.w_ObjMPR.SetRow()     
      if this.w_ObjMPR.FullRow() And this.w_ObjMPR.RowStatus() <> "D"
        this.w_ObjMPR.Savedependson()     
        * --- Aggiorna riga con valori di default
        this.w_ObjMPR.NotifyEvent("DefaultValue")     
        if this.w_OK=0 AND this.w_ObjMPR.w_DAPREZZO<>0 
          this.w_OK = iif(Ah_yesno("Si desidera aggiornare il prezzo su riga?"),1,2)
        endif
        if this.w_OKCOSTO=0 AND this.w_ObjMPR.w_DACOSUNI<>0 
          this.w_OKCOSTO = iif(Ah_yesno("Si desidera aggiornare il costo di riga?"),1,2)
        endif
        if this.w_OK=1 
          this.w_ObjMPR.NotifyEvent("Aggiorna")     
        endif
        if this.w_OKCOSTO=1
          this.w_ObjMPR.NotifyEvent("Riccosto")     
        endif
        this.w_ObjMPR.SaveRow(.t.)     
      endif
      this.w_ObjMPR.NextRow()     
    enddo
    this.w_ObjMPR.w_NOCALC = .T.
    this.w_ObjMPR.Refresh()     
    this.w_ObjMPR.bUpdated = .T.
    this.w_ObjMPR.RePos()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
