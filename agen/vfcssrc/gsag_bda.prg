* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bda                                                        *
*              Cancellazione attivit�                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-12                                                      *
* Last revis.: 2012-09-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSEATT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bda",oParentObject,m.pSEATT)
return(i_retval)

define class tgsag_bda as StdBatch
  * --- Local variables
  pSEATT = space(20)
  w_SEATT = space(20)
  w_RETVAL = space(254)
  w_ATRIFDOC = space(10)
  w_ATRIFMOV = space(10)
  w_bUnderTran = .f.
  w_RETVAL = space(254)
  * --- WorkFile variables
  OFFDATTI_idx=0
  COM_ATTI_idx=0
  OFF_PART_idx=0
  OFF_ATTI_idx=0
  MOVICOST_idx=0
  CDC_MANU_idx=0
  ATT_DCOL_idx=0
  ASS_ATEV_idx=0
  ELE_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per eliminazione massiva di un' attivit�
    this.w_RETVAL = ""
    this.w_SEATT = this.pSEATT
    * --- Elimina Attivit�
    this.w_bUnderTran = vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    * --- Select from ATT_DCOL
    i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2],.t.,this.ATT_DCOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ADSERDOC,ADTIPDOC,ADSERIAL  from "+i_cTable+" ATT_DCOL ";
          +" where ADSERIAL="+cp_ToStrODBC(this.pSEATT)+" and ADTIPDOC<>'D'";
           ,"_Curs_ATT_DCOL")
    else
      select ADSERDOC,ADTIPDOC,ADSERIAL from (i_cTable);
       where ADSERIAL=this.pSEATT and ADTIPDOC<>"D";
        into cursor _Curs_ATT_DCOL
    endif
    if used('_Curs_ATT_DCOL')
      select _Curs_ATT_DCOL
      locate for 1=1
      do while not(eof())
      this.w_ATRIFDOC = Nvl(_Curs_ATT_DCOL.ADSERDOC," ")
      * --- Simula eliminazione Documento
      this.w_RETVAL = gsar_bdk(.null.,this.w_ATRIFDOC,.t.)
      if Not Empty(this.w_RETVAL)
        exit
      endif
        select _Curs_ATT_DCOL
        continue
      enddo
      use
    endif
    if Empty(this.w_RETVAL)
      * --- Elimina Documento
      * --- Select from ATT_DCOL
      i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2],.t.,this.ATT_DCOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ADSERDOC,ADTIPDOC,ADSERIAL  from "+i_cTable+" ATT_DCOL ";
            +" where ADSERIAL="+cp_ToStrODBC(this.pSEATT)+" and ADTIPDOC<>'D'";
             ,"_Curs_ATT_DCOL")
      else
        select ADSERDOC,ADTIPDOC,ADSERIAL from (i_cTable);
         where ADSERIAL=this.pSEATT and ADTIPDOC<>"D";
          into cursor _Curs_ATT_DCOL
      endif
      if used('_Curs_ATT_DCOL')
        select _Curs_ATT_DCOL
        locate for 1=1
        do while not(eof())
        this.w_ATRIFDOC = Nvl(_Curs_ATT_DCOL.ADSERDOC," ")
        * --- Elimina Documento
        this.w_RETVAL = gsar_bdk(.null.,this.w_ATRIFDOC,.f.)
          select _Curs_ATT_DCOL
          continue
        enddo
        use
      endif
    endif
    if !Isahe()
      * --- Read from OFF_ATTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATRIFMOV"+;
          " from "+i_cTable+" OFF_ATTI where ";
              +"ATSERIAL = "+cp_ToStrODBC(this.pSEATT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATRIFMOV;
          from (i_cTable) where;
              ATSERIAL = this.pSEATT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ATRIFMOV = NVL(cp_ToDate(_read_.ATRIFMOV),cp_NullValue(_read_.ATRIFMOV))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if not empty(this.w_ATRIFMOV) and Empty(this.w_RETVAL)
        * --- Try
        local bErr_03838998
        bErr_03838998=bTrsErr
        this.Try_03838998()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_RETVAL = ah_MsgFormat("Errore cancellazione testata movimento di analitica .%0%1", MESSAGE())
        endif
        bTrsErr=bTrsErr or bErr_03838998
        * --- End
        if empty(this.w_RETVAL)
          * --- Try
          local bErr_03828938
          bErr_03828938=bTrsErr
          this.Try_03828938()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_RETVAL = ah_MsgFormat("Errore cancellazione righe movimento di analitica .%0%1", MESSAGE())
          endif
          bTrsErr=bTrsErr or bErr_03828938
          * --- End
        endif
      endif
    endif
    if EMPTY(this.w_RETVAL)
      * --- Try
      local bErr_03831258
      bErr_03831258=bTrsErr
      this.Try_03831258()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if this.w_bUnderTran
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          this.w_RETVAL = ah_MsgFormat("Errore durante cancellazione attivit�%0%1",MESSAGE())
        endif
      endif
      bTrsErr=bTrsErr or bErr_03831258
      * --- End
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc
  proc Try_03838998()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Elimina Movimento di analitica associato
    * --- Delete from MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_ATRIFMOV);
            +" and MRROWORD = "+cp_ToStrODBC(-1);
             )
    else
      delete from (i_cTable) where;
            MRSERIAL = this.w_ATRIFMOV;
            and MRROWORD = -1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_03828938()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from CDC_MANU
    i_nConn=i_TableProp[this.CDC_MANU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CDC_MANU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_ATRIFMOV);
             )
    else
      delete from (i_cTable) where;
            CMCODICE = this.w_ATRIFMOV;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_03831258()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.w_bUnderTran
      * --- begin transaction
      cp_BeginTrs()
    endif
    * --- Write into ELE_CONT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ELCONTRA,ELCODMOD,ELCODIMP,ELCODCOM,ELRINNOV"
      do vq_exec with 'gsag_bda',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
              +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
              +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
              +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
              +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ELQTACON = ELE_CONT.ELQTACON-_t2.DAQTAMOV";
          +i_ccchkf;
          +" from "+i_cTable+" ELE_CONT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
              +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
              +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
              +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
              +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT, "+i_cQueryTable+" _t2 set ";
          +"ELE_CONT.ELQTACON = ELE_CONT.ELQTACON-_t2.DAQTAMOV";
          +Iif(Empty(i_ccchkf),"",",ELE_CONT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ELE_CONT.ELCONTRA = t2.ELCONTRA";
              +" and "+"ELE_CONT.ELCODMOD = t2.ELCODMOD";
              +" and "+"ELE_CONT.ELCODIMP = t2.ELCODIMP";
              +" and "+"ELE_CONT.ELCODCOM = t2.ELCODCOM";
              +" and "+"ELE_CONT.ELRINNOV = t2.ELRINNOV";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT set (";
          +"ELQTACON";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"ELQTACON-t2.DAQTAMOV";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
              +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
              +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
              +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
              +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT set ";
          +"ELQTACON = ELE_CONT.ELQTACON-_t2.DAQTAMOV";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ELCONTRA = "+i_cQueryTable+".ELCONTRA";
              +" and "+i_cTable+".ELCODMOD = "+i_cQueryTable+".ELCODMOD";
              +" and "+i_cTable+".ELCODIMP = "+i_cQueryTable+".ELCODIMP";
              +" and "+i_cTable+".ELCODCOM = "+i_cQueryTable+".ELCODCOM";
              +" and "+i_cTable+".ELRINNOV = "+i_cQueryTable+".ELRINNOV";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ELQTACON = (select "+i_cTable+".ELQTACON-DAQTAMOV from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Delete from ATT_DCOL
    i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ADSERIAL = "+cp_ToStrODBC(this.w_SEATT);
             )
    else
      delete from (i_cTable) where;
            ADSERIAL = this.w_SEATT;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from OFFDATTI
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DASERIAL = "+cp_ToStrODBC(this.w_SEATT);
             )
    else
      delete from (i_cTable) where;
            DASERIAL = this.w_SEATT;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from COM_ATTI
    i_nConn=i_TableProp[this.COM_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COM_ATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CASERIAL = "+cp_ToStrODBC(this.w_SEATT);
             )
    else
      delete from (i_cTable) where;
            CASERIAL = this.w_SEATT;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from OFF_PART
    i_nConn=i_TableProp[this.OFF_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PASERIAL = "+cp_ToStrODBC(this.w_SEATT);
             )
    else
      delete from (i_cTable) where;
            PASERIAL = this.w_SEATT;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from OFF_ATTI
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_SEATT);
             )
    else
      delete from (i_cTable) where;
            ATSERIAL = this.w_SEATT;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ASS_ATEV
    i_nConn=i_TableProp[this.ASS_ATEV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATEV_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"AECODATT = "+cp_ToStrODBC(this.w_SEATT);
             )
    else
      delete from (i_cTable) where;
            AECODATT = this.w_SEATT;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if this.w_bUnderTran
      * --- commit
      cp_EndTrs(.t.)
    endif
    return


  proc Init(oParentObject,pSEATT)
    this.pSEATT=pSEATT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='OFFDATTI'
    this.cWorkTables[2]='COM_ATTI'
    this.cWorkTables[3]='OFF_PART'
    this.cWorkTables[4]='OFF_ATTI'
    this.cWorkTables[5]='MOVICOST'
    this.cWorkTables[6]='CDC_MANU'
    this.cWorkTables[7]='ATT_DCOL'
    this.cWorkTables[8]='ASS_ATEV'
    this.cWorkTables[9]='ELE_CONT'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_ATT_DCOL')
      use in _Curs_ATT_DCOL
    endif
    if used('_Curs_ATT_DCOL')
      use in _Curs_ATT_DCOL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSEATT"
endproc
