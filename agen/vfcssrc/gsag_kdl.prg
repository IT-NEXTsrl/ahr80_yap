* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kdl                                                        *
*              Dati di riga                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-22                                                      *
* Last revis.: 2015-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kdl",oParentObject))

* --- Class definition
define class tgsag_kdl as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 718
  Height = 309+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-30"
  HelpContextID=185981079
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=152

  * --- Constant Properties
  _IDX = 0
  LISTINI_IDX = 0
  TIP_DOCU_IDX = 0
  VOC_COST_IDX = 0
  CENCOST_IDX = 0
  ATTIVITA_IDX = 0
  CAN_TIER_IDX = 0
  CON_TRAM_IDX = 0
  CON_TRAS_IDX = 0
  MOD_ELEM_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  ELE_CONT_IDX = 0
  KEY_ARTI_IDX = 0
  cPrg = "gsag_kdl"
  cComment = "Dati di riga"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NUMSCO = 0
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_DATDOC = ctod('  /  /  ')
  w_CATCOM = space(3)
  w_MVCODVAL = space(3)
  w_MVFLVEAC = space(10)
  w_FLGLIS = space(1)
  w_DATALIST = ctod('  /  /  ')
  w_DASCONT1 = 0
  o_DASCONT1 = 0
  w_DASCONT2 = 0
  o_DASCONT2 = 0
  w_DASCONT3 = 0
  o_DASCONT3 = 0
  w_DASCONT4 = 0
  w_DAINICOM = ctod('  /  /  ')
  o_DAINICOM = ctod('  /  /  ')
  w_DAFINCOM = ctod('  /  /  ')
  o_DAFINCOM = ctod('  /  /  ')
  w_DACODLIS = space(5)
  w_DASCOLIS = space(5)
  w_DAPROLIS = space(5)
  w_DAPROSCO = space(5)
  w_DESLIS = space(40)
  w_DESLIS2 = space(40)
  w_VALLIS = space(3)
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_VALLIS2 = space(3)
  w_INILIS2 = ctod('  /  /  ')
  w_FINLIS2 = ctod('  /  /  ')
  w_TIPLIS = space(1)
  w_FINLIS3 = ctod('  /  /  ')
  w_INILIS3 = ctod('  /  /  ')
  w_VALLIS3 = space(3)
  w_FINLIS4 = ctod('  /  /  ')
  w_INILIS4 = ctod('  /  /  ')
  w_VALLIS4 = space(3)
  w_TIPSER = space(1)
  w_IVALIS = space(1)
  w_FLGCIC = space(1)
  w_FLGCIC2 = space(1)
  w_FLGCIC3 = space(1)
  w_FLGCIC4 = space(1)
  w_TIPLIS4 = space(1)
  o_TIPLIS4 = space(1)
  w_TIPLIS3 = space(1)
  o_TIPLIS3 = space(1)
  w_TIPLIS = space(1)
  w_TIPLIS2 = space(1)
  w_FLSTAT = space(1)
  w_F2STAT = space(1)
  w_F3STAT = space(1)
  w_F4STAT = space(1)
  w_MVFLSCOR = space(1)
  w_MVTSCLIS = space(5)
  w_MVTCOLIS = space(5)
  w_IVALISP = space(1)
  w_DACODNOM = space(15)
  w_PR__DATA = ctod('  /  /  ')
  w_COD1ATT = space(20)
  w_DACONCOD = space(15)
  w_DESCTR = space(50)
  w_BUFLANAL = space(1)
  w_TIPATT = space(10)
  w_CTCOMM = space(3)
  w_CODCLF = space(15)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DAVOCRIC = space(15)
  w_DESVOR = space(40)
  w_DACENRIC = space(15)
  w_DACOMRIC = space(15)
  o_DACOMRIC = space(15)
  w_DAATTRIC = space(15)
  w_CCDESRIC = space(40)
  w_DESCAN = space(100)
  w_DATCOINI = ctod('  /  /  ')
  o_DATCOINI = ctod('  /  /  ')
  w_DATCOFIN = ctod('  /  /  ')
  w_FLACOMAQ = space(1)
  w_LISACQ = space(5)
  w_FLGCOM = space(1)
  w_TIPVOC = space(1)
  w_FLANAL = space(1)
  w_CENOBSO = ctod('  /  /  ')
  w_CLASSE = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CODCOS = space(5)
  w_VOCOBSO = ctod('  /  /  ')
  w_VOCTIP = space(1)
  w_TIPVOR = space(1)
  w_FLDANA = space(1)
  w_VALACQ = space(3)
  w_INIACQ = ctod('  /  /  ')
  w_TIPACQ = space(1)
  w_FLSACQ = space(1)
  w_IVAACQ = space(1)
  w_FLGACQ = space(1)
  w_FINACQ = ctod('  /  /  ')
  w_CAUDOC = space(5)
  w_BUFLANAL = space(1)
  w_DAVOCCOS = space(15)
  w_DACENCOS = space(15)
  w_DACODCOM = space(15)
  o_DACODCOM = space(15)
  w_DAATTIVI = space(15)
  w_CCDESPIA = space(40)
  w_DESVOC = space(40)
  w_DESCOS = space(100)
  w_DATRIINI = ctod('  /  /  ')
  o_DATRIINI = ctod('  /  /  ')
  w_DATRIFIN = ctod('  /  /  ')
  w_DALISACQ = space(5)
  w_DESACQ = space(40)
  w_CLASSE = space(10)
  w_TIPATT = space(10)
  w_DATOBSO = ctod('  /  /  ')
  w_TIPVOC = space(1)
  w_CODCOS = space(5)
  w_VOCOBSO = ctod('  /  /  ')
  w_FLANAL = space(1)
  w_VOCTIP = space(1)
  w_TIPVOR = space(1)
  w_CENOBSO = ctod('  /  /  ')
  w_FLACQ = space(1)
  w_FLACOMAQ = space(1)
  w_DACONTRA = space(10)
  w_DACODMOD = space(10)
  w_DACODIMP = space(10)
  w_IMROWORD = 0
  w_DARINNOV = 0
  w_CODESCON = space(50)
  w_DAELMCNT = space(10)
  w_MODESCRI = space(60)
  w_ELQTAMOV = 0
  w_ELQTACON = 0
  w_QTA__RES = 0
  w_DAUNIMIS = space(3)
  w_AECODSER = space(41)
  w_AEDESART = space(40)
  w_ARCODSER = space(20)
  w_ARDESART = space(40)
  w_DACOCOMP = 0
  w_CODCLI = space(15)
  w_DADATINI = ctod('  /  /  ')
  w_DAPREZZO = 0
  w_DACODICE = 0
  w_DACODSER = space(20)
  w_FLUPDRIG = .F.
  w_DACODATT = space(20)
  w_DADESATT = space(40)
  w_ATTEXIST = .F.
  w_COTIPATT = space(20)
  w_DOCEXIST = .F.
  w_COTIPDOC = space(20)
  w_CODATINI = ctod('  /  /  ')
  w_CODATFIN = ctod('  /  /  ')
  w_IMDESCRI = space(50)
  w_IMDESCON = space(50)
  w_DAQTAMOV = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kdlPag1","gsag_kdl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Ricavi")
      .Pages(2).addobject("oPag","tgsag_kdlPag2","gsag_kdl",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Costi")
      .Pages(3).addobject("oPag","tgsag_kdlPag3","gsag_kdl",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Elemento contratto")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDASCONT1_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='VOC_COST'
    this.cWorkTables[4]='CENCOST'
    this.cWorkTables[5]='ATTIVITA'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='CON_TRAM'
    this.cWorkTables[8]='CON_TRAS'
    this.cWorkTables[9]='MOD_ELEM'
    this.cWorkTables[10]='IMP_MAST'
    this.cWorkTables[11]='IMP_DETT'
    this.cWorkTables[12]='ELE_CONT'
    this.cWorkTables[13]='KEY_ARTI'
    return(this.OpenAllTables(13))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NUMSCO=0
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_DATDOC=ctod("  /  /  ")
      .w_CATCOM=space(3)
      .w_MVCODVAL=space(3)
      .w_MVFLVEAC=space(10)
      .w_FLGLIS=space(1)
      .w_DATALIST=ctod("  /  /  ")
      .w_DASCONT1=0
      .w_DASCONT2=0
      .w_DASCONT3=0
      .w_DASCONT4=0
      .w_DAINICOM=ctod("  /  /  ")
      .w_DAFINCOM=ctod("  /  /  ")
      .w_DACODLIS=space(5)
      .w_DASCOLIS=space(5)
      .w_DAPROLIS=space(5)
      .w_DAPROSCO=space(5)
      .w_DESLIS=space(40)
      .w_DESLIS2=space(40)
      .w_VALLIS=space(3)
      .w_INILIS=ctod("  /  /  ")
      .w_FINLIS=ctod("  /  /  ")
      .w_VALLIS2=space(3)
      .w_INILIS2=ctod("  /  /  ")
      .w_FINLIS2=ctod("  /  /  ")
      .w_TIPLIS=space(1)
      .w_FINLIS3=ctod("  /  /  ")
      .w_INILIS3=ctod("  /  /  ")
      .w_VALLIS3=space(3)
      .w_FINLIS4=ctod("  /  /  ")
      .w_INILIS4=ctod("  /  /  ")
      .w_VALLIS4=space(3)
      .w_TIPSER=space(1)
      .w_IVALIS=space(1)
      .w_FLGCIC=space(1)
      .w_FLGCIC2=space(1)
      .w_FLGCIC3=space(1)
      .w_FLGCIC4=space(1)
      .w_TIPLIS4=space(1)
      .w_TIPLIS3=space(1)
      .w_TIPLIS=space(1)
      .w_TIPLIS2=space(1)
      .w_FLSTAT=space(1)
      .w_F2STAT=space(1)
      .w_F3STAT=space(1)
      .w_F4STAT=space(1)
      .w_MVFLSCOR=space(1)
      .w_MVTSCLIS=space(5)
      .w_MVTCOLIS=space(5)
      .w_IVALISP=space(1)
      .w_DACODNOM=space(15)
      .w_PR__DATA=ctod("  /  /  ")
      .w_COD1ATT=space(20)
      .w_DACONCOD=space(15)
      .w_DESCTR=space(50)
      .w_BUFLANAL=space(1)
      .w_TIPATT=space(10)
      .w_CTCOMM=space(3)
      .w_CODCLF=space(15)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DAVOCRIC=space(15)
      .w_DESVOR=space(40)
      .w_DACENRIC=space(15)
      .w_DACOMRIC=space(15)
      .w_DAATTRIC=space(15)
      .w_CCDESRIC=space(40)
      .w_DESCAN=space(100)
      .w_DATCOINI=ctod("  /  /  ")
      .w_DATCOFIN=ctod("  /  /  ")
      .w_FLACOMAQ=space(1)
      .w_LISACQ=space(5)
      .w_FLGCOM=space(1)
      .w_TIPVOC=space(1)
      .w_FLANAL=space(1)
      .w_CENOBSO=ctod("  /  /  ")
      .w_CLASSE=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CODCOS=space(5)
      .w_VOCOBSO=ctod("  /  /  ")
      .w_VOCTIP=space(1)
      .w_TIPVOR=space(1)
      .w_FLDANA=space(1)
      .w_VALACQ=space(3)
      .w_INIACQ=ctod("  /  /  ")
      .w_TIPACQ=space(1)
      .w_FLSACQ=space(1)
      .w_IVAACQ=space(1)
      .w_FLGACQ=space(1)
      .w_FINACQ=ctod("  /  /  ")
      .w_CAUDOC=space(5)
      .w_BUFLANAL=space(1)
      .w_DAVOCCOS=space(15)
      .w_DACENCOS=space(15)
      .w_DACODCOM=space(15)
      .w_DAATTIVI=space(15)
      .w_CCDESPIA=space(40)
      .w_DESVOC=space(40)
      .w_DESCOS=space(100)
      .w_DATRIINI=ctod("  /  /  ")
      .w_DATRIFIN=ctod("  /  /  ")
      .w_DALISACQ=space(5)
      .w_DESACQ=space(40)
      .w_CLASSE=space(10)
      .w_TIPATT=space(10)
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPVOC=space(1)
      .w_CODCOS=space(5)
      .w_VOCOBSO=ctod("  /  /  ")
      .w_FLANAL=space(1)
      .w_VOCTIP=space(1)
      .w_TIPVOR=space(1)
      .w_CENOBSO=ctod("  /  /  ")
      .w_FLACQ=space(1)
      .w_FLACOMAQ=space(1)
      .w_DACONTRA=space(10)
      .w_DACODMOD=space(10)
      .w_DACODIMP=space(10)
      .w_IMROWORD=0
      .w_DARINNOV=0
      .w_CODESCON=space(50)
      .w_DAELMCNT=space(10)
      .w_MODESCRI=space(60)
      .w_ELQTAMOV=0
      .w_ELQTACON=0
      .w_QTA__RES=0
      .w_DAUNIMIS=space(3)
      .w_AECODSER=space(41)
      .w_AEDESART=space(40)
      .w_ARCODSER=space(20)
      .w_ARDESART=space(40)
      .w_DACOCOMP=0
      .w_CODCLI=space(15)
      .w_DADATINI=ctod("  /  /  ")
      .w_DAPREZZO=0
      .w_DACODICE=0
      .w_DACODSER=space(20)
      .w_FLUPDRIG=.f.
      .w_DACODATT=space(20)
      .w_DADESATT=space(40)
      .w_ATTEXIST=.f.
      .w_COTIPATT=space(20)
      .w_DOCEXIST=.f.
      .w_COTIPDOC=space(20)
      .w_CODATINI=ctod("  /  /  ")
      .w_CODATFIN=ctod("  /  /  ")
      .w_IMDESCRI=space(50)
      .w_IMDESCON=space(50)
      .w_DAQTAMOV=0
      .w_NUMSCO=oParentObject.w_NUMSCO
      .w_MVCODVAL=oParentObject.w_MVCODVAL
      .w_MVFLVEAC=oParentObject.w_MVFLVEAC
      .w_FLGLIS=oParentObject.w_FLGLIS
      .w_DATALIST=oParentObject.w_DATALIST
      .w_DASCONT1=oParentObject.w_DASCONT1
      .w_DASCONT2=oParentObject.w_DASCONT2
      .w_DASCONT3=oParentObject.w_DASCONT3
      .w_DASCONT4=oParentObject.w_DASCONT4
      .w_DAINICOM=oParentObject.w_DAINICOM
      .w_DAFINCOM=oParentObject.w_DAFINCOM
      .w_DACODLIS=oParentObject.w_DACODLIS
      .w_DASCOLIS=oParentObject.w_DASCOLIS
      .w_DAPROLIS=oParentObject.w_DAPROLIS
      .w_DAPROSCO=oParentObject.w_DAPROSCO
      .w_TIPSER=oParentObject.w_TIPSER
      .w_DACODNOM=oParentObject.w_DACODNOM
      .w_PR__DATA=oParentObject.w_PR__DATA
      .w_COD1ATT=oParentObject.w_COD1ATT
      .w_DACONCOD=oParentObject.w_DACONCOD
      .w_BUFLANAL=oParentObject.w_BUFLANAL
      .w_DAVOCRIC=oParentObject.w_DAVOCRIC
      .w_DACENRIC=oParentObject.w_DACENRIC
      .w_DACOMRIC=oParentObject.w_DACOMRIC
      .w_DAATTRIC=oParentObject.w_DAATTRIC
      .w_DATCOINI=oParentObject.w_DATCOINI
      .w_DATCOFIN=oParentObject.w_DATCOFIN
      .w_FLACOMAQ=oParentObject.w_FLACOMAQ
      .w_LISACQ=oParentObject.w_LISACQ
      .w_FLGCOM=oParentObject.w_FLGCOM
      .w_FLANAL=oParentObject.w_FLANAL
      .w_FLDANA=oParentObject.w_FLDANA
      .w_BUFLANAL=oParentObject.w_BUFLANAL
      .w_DAVOCCOS=oParentObject.w_DAVOCCOS
      .w_DACENCOS=oParentObject.w_DACENCOS
      .w_DACODCOM=oParentObject.w_DACODCOM
      .w_DAATTIVI=oParentObject.w_DAATTIVI
      .w_DATRIINI=oParentObject.w_DATRIINI
      .w_DATRIFIN=oParentObject.w_DATRIFIN
      .w_DALISACQ=oParentObject.w_DALISACQ
      .w_FLANAL=oParentObject.w_FLANAL
      .w_FLACQ=oParentObject.w_FLACQ
      .w_FLACOMAQ=oParentObject.w_FLACOMAQ
      .w_DACONTRA=oParentObject.w_DACONTRA
      .w_DACODMOD=oParentObject.w_DACODMOD
      .w_DACODIMP=oParentObject.w_DACODIMP
      .w_DARINNOV=oParentObject.w_DARINNOV
      .w_DAUNIMIS=oParentObject.w_DAUNIMIS
      .w_DACOCOMP=oParentObject.w_DACOCOMP
      .w_CODCLI=oParentObject.w_CODCLI
      .w_DAPREZZO=oParentObject.w_DAPREZZO
      .w_DACODICE=oParentObject.w_DACODICE
      .w_DACODATT=oParentObject.w_DACODATT
      .w_DAQTAMOV=oParentObject.w_DAQTAMOV
          .DoRTCalc(1,1,.f.)
        .w_TIPCON = IIF(upper(this.oparentobject.class)='TCGSAG_MPR' or upper(this.oparentobject.class)='TGSFA_KTE' or upper(this.oparentobject.class)='TCGSAG_MPP',this.oparentobject.w_NOTIPCLI,this.oparentobject.oparentobject.w_ATTIPCLI)
        .w_CODCON = IIF(upper(this.oparentobject.class)='TCGSAG_MPR' or upper(this.oparentobject.class)='TGSFA_KTE' or upper(this.oparentobject.class)='TCGSAG_MPP',this.oparentobject.w_NOCODCLI,this.oparentobject.w_CODCLI)
        .w_DATDOC = IIF(upper(this.oparentobject.class)='TCGSAG_MPR' or upper(this.oparentobject.class)='TGSFA_KTE' or upper(this.oparentobject.class)='TCGSAG_MPP',this.oparentobject.w_PR__DATA,this.oparentobject.oparentobject.w_ATDATDOC)
        .w_CATCOM = IIF(upper(this.oparentobject.class)='TCGSAG_MPR' or upper(this.oparentobject.class)='TGSFA_KTE' or upper(this.oparentobject.class)='TCGSAG_MPP' or upper(this.oparentobject.class)='TCGSAG_MDA',this.oparentobject.w_CATCOM,this.oparentobject.oparentobject.w_CATCOM)
        .DoRTCalc(6,16,.f.)
        if not(empty(.w_DACODLIS))
          .link_1_16('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_DASCOLIS))
          .link_1_17('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_DAPROLIS))
          .link_1_18('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_DAPROSCO))
          .link_1_19('Full')
        endif
          .DoRTCalc(20,48,.f.)
        .w_MVFLSCOR = THIS.OPARENTOBJECT.w_FLSCOR
        .DoRTCalc(50,56,.f.)
        if not(empty(.w_DACONCOD))
          .link_1_68('Full')
        endif
          .DoRTCalc(57,58,.f.)
        .w_TIPATT = 'A'
          .DoRTCalc(60,63,.f.)
        .w_DAVOCRIC = .w_DAVOCRIC
        .DoRTCalc(64,64,.f.)
        if not(empty(.w_DAVOCRIC))
          .link_1_78('Full')
        endif
          .DoRTCalc(65,65,.f.)
        .w_DACENRIC = .w_DACENRIC
        .DoRTCalc(66,66,.f.)
        if not(empty(.w_DACENRIC))
          .link_1_81('Full')
        endif
        .w_DACOMRIC = .w_DACOMRIC
        .DoRTCalc(67,67,.f.)
        if not(empty(.w_DACOMRIC))
          .link_1_82('Full')
        endif
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_DAATTRIC))
          .link_1_83('Full')
        endif
          .DoRTCalc(69,78,.f.)
        .w_CLASSE = This.oparentobject.class
        .w_OBTEST = i_DATSYS
          .DoRTCalc(81,95,.f.)
        .w_DAVOCCOS = .w_DAVOCCOS
        .DoRTCalc(96,96,.f.)
        if not(empty(.w_DAVOCCOS))
          .link_2_1('Full')
        endif
        .w_DACENCOS = .w_DACENCOS
        .DoRTCalc(97,97,.f.)
        if not(empty(.w_DACENCOS))
          .link_2_2('Full')
        endif
        .DoRTCalc(98,98,.f.)
        if not(empty(.w_DACODCOM))
          .link_2_3('Full')
        endif
        .DoRTCalc(99,99,.f.)
        if not(empty(.w_DAATTIVI))
          .link_2_4('Full')
        endif
          .DoRTCalc(100,104,.f.)
        .w_DALISACQ = .w_LISACQ
        .DoRTCalc(105,105,.f.)
        if not(empty(.w_DALISACQ))
          .link_2_18('Full')
        endif
          .DoRTCalc(106,106,.f.)
        .w_CLASSE = This.oparentobject.class
        .w_TIPATT = 'A'
          .DoRTCalc(109,116,.f.)
        .w_FLACQ = This.oParentobject .w_FLACQ
        .DoRTCalc(118,119,.f.)
        if not(empty(.w_DACONTRA))
          .link_3_8('Full')
        endif
        .DoRTCalc(120,120,.f.)
        if not(empty(.w_DACODMOD))
          .link_3_9('Full')
        endif
        .DoRTCalc(121,121,.f.)
        if not(empty(.w_DACODIMP))
          .link_3_10('Full')
        endif
          .DoRTCalc(122,124,.f.)
        .w_DAELMCNT = .w_DACONTRA
        .DoRTCalc(125,125,.f.)
        if not(empty(.w_DAELMCNT))
          .link_3_14('Full')
        endif
          .DoRTCalc(126,128,.f.)
        .w_QTA__RES = .w_ELQTAMOV-.w_ELQTACON
          .DoRTCalc(130,130,.f.)
        .w_AECODSER = .w_DACODICE
        .DoRTCalc(131,131,.f.)
        if not(empty(.w_AECODSER))
          .link_3_23('Full')
        endif
          .DoRTCalc(132,132,.f.)
        .w_ARCODSER = .w_DACODATT
        .DoRTCalc(133,133,.f.)
        if not(empty(.w_ARCODSER))
          .link_3_25('Full')
        endif
        .DoRTCalc(134,135,.f.)
        if not(empty(.w_DACOCOMP))
          .link_3_32('Full')
        endif
          .DoRTCalc(136,136,.f.)
        .w_DADATINI = this.oParentObject.w_OB_TEST
          .DoRTCalc(138,139,.f.)
        .w_DACODSER = IIF(isAhe(), this.oParentObject.w_DACODICE, this.oParentObject.w_DACODATT )
        .w_FLUPDRIG = .F.
          .DoRTCalc(142,142,.f.)
        .w_DADESATT = IIF(Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP', this.oParentObject.w_PRDESPRE, this.oParentObject.w_DADESATT )
      .oPgFrm.Page3.oPag.oObj_3_52.Calculate()
    endwith
    this.DoRTCalc(144,152,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_93.enabled = this.oPgFrm.Page1.oPag.oBtn_1_93.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_94.enabled = this.oPgFrm.Page1.oPag.oBtn_1_94.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_16.enabled = this.oPgFrm.Page2.oPag.oBtn_2_16.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_17.enabled = this.oPgFrm.Page2.oPag.oBtn_2_17.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_7.enabled = this.oPgFrm.Page3.oPag.oBtn_3_7.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_27.enabled = this.oPgFrm.Page3.oPag.oBtn_3_27.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_28.enabled = this.oPgFrm.Page3.oPag.oBtn_3_28.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_29.enabled = this.oPgFrm.Page3.oPag.oBtn_3_29.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_30.enabled = this.oPgFrm.Page3.oPag.oBtn_3_30.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_31.enabled = this.oPgFrm.Page3.oPag.oBtn_3_31.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDASCONT1_1_10.enabled = i_bVal
      .Page1.oPag.oDASCONT2_1_11.enabled = i_bVal
      .Page1.oPag.oDASCONT3_1_12.enabled = i_bVal
      .Page1.oPag.oDASCONT4_1_13.enabled = i_bVal
      .Page1.oPag.oDAINICOM_1_14.enabled = i_bVal
      .Page1.oPag.oDAFINCOM_1_15.enabled = i_bVal
      .Page1.oPag.oDACODLIS_1_16.enabled = i_bVal
      .Page1.oPag.oDASCOLIS_1_17.enabled = i_bVal
      .Page1.oPag.oDAPROLIS_1_18.enabled = i_bVal
      .Page1.oPag.oDAPROSCO_1_19.enabled = i_bVal
      .Page1.oPag.oDACONCOD_1_68.enabled = i_bVal
      .Page1.oPag.oDAVOCRIC_1_78.enabled = i_bVal
      .Page1.oPag.oDACENRIC_1_81.enabled = i_bVal
      .Page1.oPag.oDACOMRIC_1_82.enabled = i_bVal
      .Page1.oPag.oDAATTRIC_1_83.enabled = i_bVal
      .Page1.oPag.oDATCOINI_1_89.enabled = i_bVal
      .Page1.oPag.oDATCOFIN_1_90.enabled = i_bVal
      .Page2.oPag.oDAVOCCOS_2_1.enabled = i_bVal
      .Page2.oPag.oDACENCOS_2_2.enabled = i_bVal
      .Page2.oPag.oDACODCOM_2_3.enabled = i_bVal
      .Page2.oPag.oDAATTIVI_2_4.enabled = i_bVal
      .Page2.oPag.oDATRIINI_2_12.enabled = i_bVal
      .Page2.oPag.oDATRIFIN_2_13.enabled = i_bVal
      .Page2.oPag.oDALISACQ_2_18.enabled = i_bVal
      .Page1.oPag.oBtn_1_67.enabled = .Page1.oPag.oBtn_1_67.mCond()
      .Page1.oPag.oBtn_1_93.enabled = .Page1.oPag.oBtn_1_93.mCond()
      .Page1.oPag.oBtn_1_94.enabled = .Page1.oPag.oBtn_1_94.mCond()
      .Page2.oPag.oBtn_2_16.enabled = .Page2.oPag.oBtn_2_16.mCond()
      .Page2.oPag.oBtn_2_17.enabled = .Page2.oPag.oBtn_2_17.mCond()
      .Page3.oPag.oBtn_3_7.enabled = .Page3.oPag.oBtn_3_7.mCond()
      .Page3.oPag.oBtn_3_19.enabled = i_bVal
      .Page3.oPag.oBtn_3_27.enabled = .Page3.oPag.oBtn_3_27.mCond()
      .Page3.oPag.oBtn_3_28.enabled = .Page3.oPag.oBtn_3_28.mCond()
      .Page3.oPag.oBtn_3_29.enabled = .Page3.oPag.oBtn_3_29.mCond()
      .Page3.oPag.oBtn_3_30.enabled = .Page3.oPag.oBtn_3_30.mCond()
      .Page3.oPag.oBtn_3_31.enabled = .Page3.oPag.oBtn_3_31.mCond()
      .Page3.oPag.oObj_3_52.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_NUMSCO=.w_NUMSCO
      .oParentObject.w_MVCODVAL=.w_MVCODVAL
      .oParentObject.w_MVFLVEAC=.w_MVFLVEAC
      .oParentObject.w_FLGLIS=.w_FLGLIS
      .oParentObject.w_DATALIST=.w_DATALIST
      .oParentObject.w_DASCONT1=.w_DASCONT1
      .oParentObject.w_DASCONT2=.w_DASCONT2
      .oParentObject.w_DASCONT3=.w_DASCONT3
      .oParentObject.w_DASCONT4=.w_DASCONT4
      .oParentObject.w_DAINICOM=.w_DAINICOM
      .oParentObject.w_DAFINCOM=.w_DAFINCOM
      .oParentObject.w_DACODLIS=.w_DACODLIS
      .oParentObject.w_DASCOLIS=.w_DASCOLIS
      .oParentObject.w_DAPROLIS=.w_DAPROLIS
      .oParentObject.w_DAPROSCO=.w_DAPROSCO
      .oParentObject.w_TIPSER=.w_TIPSER
      .oParentObject.w_DACODNOM=.w_DACODNOM
      .oParentObject.w_PR__DATA=.w_PR__DATA
      .oParentObject.w_COD1ATT=.w_COD1ATT
      .oParentObject.w_DACONCOD=.w_DACONCOD
      .oParentObject.w_BUFLANAL=.w_BUFLANAL
      .oParentObject.w_DAVOCRIC=.w_DAVOCRIC
      .oParentObject.w_DACENRIC=.w_DACENRIC
      .oParentObject.w_DACOMRIC=.w_DACOMRIC
      .oParentObject.w_DAATTRIC=.w_DAATTRIC
      .oParentObject.w_DATCOINI=.w_DATCOINI
      .oParentObject.w_DATCOFIN=.w_DATCOFIN
      .oParentObject.w_FLACOMAQ=.w_FLACOMAQ
      .oParentObject.w_LISACQ=.w_LISACQ
      .oParentObject.w_FLGCOM=.w_FLGCOM
      .oParentObject.w_FLANAL=.w_FLANAL
      .oParentObject.w_FLDANA=.w_FLDANA
      .oParentObject.w_BUFLANAL=.w_BUFLANAL
      .oParentObject.w_DAVOCCOS=.w_DAVOCCOS
      .oParentObject.w_DACENCOS=.w_DACENCOS
      .oParentObject.w_DACODCOM=.w_DACODCOM
      .oParentObject.w_DAATTIVI=.w_DAATTIVI
      .oParentObject.w_DATRIINI=.w_DATRIINI
      .oParentObject.w_DATRIFIN=.w_DATRIFIN
      .oParentObject.w_DALISACQ=.w_DALISACQ
      .oParentObject.w_FLANAL=.w_FLANAL
      .oParentObject.w_FLACQ=.w_FLACQ
      .oParentObject.w_FLACOMAQ=.w_FLACOMAQ
      .oParentObject.w_DACONTRA=.w_DACONTRA
      .oParentObject.w_DACODMOD=.w_DACODMOD
      .oParentObject.w_DACODIMP=.w_DACODIMP
      .oParentObject.w_DARINNOV=.w_DARINNOV
      .oParentObject.w_DAUNIMIS=.w_DAUNIMIS
      .oParentObject.w_DACOCOMP=.w_DACOCOMP
      .oParentObject.w_CODCLI=.w_CODCLI
      .oParentObject.w_DAPREZZO=.w_DAPREZZO
      .oParentObject.w_DACODICE=.w_DACODICE
      .oParentObject.w_DACODATT=.w_DACODATT
      .oParentObject.w_DAQTAMOV=.w_DAQTAMOV
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- gsag_kdl
    with this
       if .w_FLUPDRIG
         if isAhe()
           .oParentObject.NotifyEvent('AggiornaAhe')
         else
           .oParentObject.NotifyEvent('AggiornaAhr')
         endif
       endif
       if EMPTY(.oParentObject.w_DAPREZZO)
         .oParentObject.w_DAPREZZO = .w_DAPREZZO
       endif
    endwith
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
        if .o_DASCONT1<>.w_DASCONT1
            .w_DASCONT2 = 0
        endif
        if .o_DASCONT2<>.w_DASCONT2
            .w_DASCONT3 = 0
        endif
        if .o_DASCONT3<>.w_DASCONT3
            .w_DASCONT4 = 0
        endif
        .DoRTCalc(14,14,.t.)
        if .o_DAINICOM<>.w_DAINICOM
            .w_DAFINCOM = IIF(EMPTY(.w_DAINICOM),CP_CHARTODATE('  -  -  '), .w_DAFINCOM)
        endif
        .DoRTCalc(16,17,.t.)
        if .o_TIPLIS4<>.w_TIPLIS4
            .w_DAPROLIS = iif(.w_TIPLIS4='E' And Empty(.w_DAPROLIS), .w_DAPROSCO, .w_DAPROLIS)
          .link_1_18('Full')
        endif
        if .o_TIPLIS3<>.w_TIPLIS3
            .w_DAPROSCO = iif(.w_TIPLIS3='E' And Empty(.w_DAPROSCO), .w_DAPROLIS, .w_DAPROSCO)
          .link_1_19('Full')
        endif
        .DoRTCalc(20,48,.t.)
            .w_MVFLSCOR = THIS.OPARENTOBJECT.w_FLSCOR
        .DoRTCalc(50,58,.t.)
            .w_TIPATT = 'A'
        .DoRTCalc(60,67,.t.)
        if .o_DACOMRIC<>.w_DACOMRIC
            .w_DAATTRIC = SPACE(15)
          .link_1_83('Full')
        endif
        .DoRTCalc(69,70,.t.)
        if .o_DAINICOM<>.w_DAINICOM
            .w_DATCOINI = iif(g_FLGPNA = 'S', .w_DAINICOM, .w_DATCOINI)
        endif
        if .o_DAFINCOM<>.w_DAFINCOM.or. .o_DATCOINI<>.w_DATCOINI
            .w_DATCOFIN = iif(g_FLGPNA = 'S', .w_DAFINCOM, IIF(EMPTY(.w_DATCOINI),CP_CHARTODATE('  -  -  '), .w_DATCOFIN))
        endif
        .DoRTCalc(73,78,.t.)
            .w_CLASSE = This.oparentobject.class
            .w_OBTEST = i_DATSYS
        .DoRTCalc(81,98,.t.)
        if .o_DACODCOM<>.w_DACODCOM
            .w_DAATTIVI = SPACE(15)
          .link_2_4('Full')
        endif
        .DoRTCalc(100,102,.t.)
        if .o_DAINICOM<>.w_DAINICOM
            .w_DATRIINI = iif(g_FLGPNA = 'S', .w_DAINICOM, .w_DATRIINI)
        endif
        if .o_DAFINCOM<>.w_DAFINCOM.or. .o_DATRIINI<>.w_DATRIINI
            .w_DATRIFIN = iif(g_FLGPNA = 'S', .w_DAFINCOM, IIF(EMPTY(.w_DATRIINI),CP_CHARTODATE('  -  -  '), .w_DATRIFIN))
        endif
        .DoRTCalc(105,106,.t.)
            .w_CLASSE = This.oparentobject.class
            .w_TIPATT = 'A'
        .DoRTCalc(109,116,.t.)
            .w_FLACQ = This.oParentobject .w_FLACQ
        .DoRTCalc(118,118,.t.)
          .link_3_8('Full')
          .link_3_9('Full')
          .link_3_10('Full')
        .DoRTCalc(122,124,.t.)
            .w_DAELMCNT = .w_DACONTRA
          .link_3_14('Full')
        .DoRTCalc(126,128,.t.)
            .w_QTA__RES = .w_ELQTAMOV-.w_ELQTACON
        .DoRTCalc(130,130,.t.)
            .w_AECODSER = .w_DACODICE
          .link_3_23('Full')
        .DoRTCalc(132,132,.t.)
            .w_ARCODSER = .w_DACODATT
          .link_3_25('Full')
        .DoRTCalc(134,134,.t.)
          .link_3_32('Full')
        .DoRTCalc(136,136,.t.)
            .w_DADATINI = this.oParentObject.w_OB_TEST
        .DoRTCalc(138,139,.t.)
            .w_DACODSER = IIF(isAhe(), this.oParentObject.w_DACODICE, this.oParentObject.w_DACODATT )
        .DoRTCalc(141,142,.t.)
            .w_DADESATT = IIF(Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP', this.oParentObject.w_PRDESPRE, this.oParentObject.w_DADESATT )
        .oPgFrm.Page3.oPag.oObj_3_52.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(144,152,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page3.oPag.oObj_3_52.Calculate()
    endwith
  return

  proc Calculate_ATRHLQSOIB()
    with this
          * --- DaCostoaRicavo
          .w_DACENRIC = IIF(Empty(.w_DACENRIC),.w_DACENCOS,.w_DACENRIC)
          .link_1_81('Full')
    endwith
  endproc
  proc Calculate_LKWTWVRCIM()
    with this
          * --- DaRicavoaCosto
          .w_DACENCOS = IIF(Empty(.w_DACENCOS) ,.w_DACENRIC,.w_DACENCOS)
          .link_2_2('Full')
    endwith
  endproc
  proc Calculate_WXHVPRYZFW()
    with this
          * --- Elimina informazioni abbinamento elemento contratto
          .w_DACONTRA = Space(10)
          .link_3_8('Full')
          .w_DACODMOD = Space(10)
          .link_3_9('Full')
          .w_DACODIMP = Space(10)
          .link_3_10('Full')
          .w_DACOCOMP = 0
          .link_3_32('Full')
          .w_DARINNOV = 0
      if EMPTY(.w_DACODSER)
          .w_DACODATT = SPACE(20)
      endif
      if EMPTY(.w_DACODSER)
          .w_DACODICE = SPACE(41)
      endif
    endwith
  endproc
  proc Calculate_CVZBDAEIBL()
    with this
          * --- DaCommessaCostoaCommessaRicavo
          .w_DACOMRIC = IIF(Empty(.w_DACOMRIC),.w_DACODCOM,.w_DACOMRIC)
          .link_1_82('Full')
    endwith
  endproc
  proc Calculate_HJRAFQRQTJ()
    with this
          * --- DaCommessaRicavoaCommessaCosto
          .w_DACODCOM = IIF(Empty(.w_DACODCOM),.w_DACOMRIC,.w_DACODCOM)
          .link_2_3('Full')
    endwith
  endproc
  proc Calculate_VTCFAJXQFD()
    with this
          * --- DaAttivitaCostoaAttivitaRicavo
          .w_DAATTRIC = IIF(Empty(.w_DAATTRIC) AND .w_DACOMRIC=.w_DACODCOM AND g_PERCAN='S',.w_DAATTIVI,.w_DAATTRIC)
          .link_1_83('Full')
    endwith
  endproc
  proc Calculate_FSGXMSANDV()
    with this
          * --- DaAttivitaRicavoaAttivitaCosto
          .w_DAATTIVI = IIF(Empty(.w_DAATTIVI) AND .w_DACOMRIC=.w_DACODCOM AND g_PERCAN='S',.w_DAATTRIC,.w_DAATTIVI)
          .link_2_4('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDASCONT1_1_10.enabled = this.oPgFrm.Page1.oPag.oDASCONT1_1_10.mCond()
    this.oPgFrm.Page1.oPag.oDASCONT2_1_11.enabled = this.oPgFrm.Page1.oPag.oDASCONT2_1_11.mCond()
    this.oPgFrm.Page1.oPag.oDASCONT3_1_12.enabled = this.oPgFrm.Page1.oPag.oDASCONT3_1_12.mCond()
    this.oPgFrm.Page1.oPag.oDASCONT4_1_13.enabled = this.oPgFrm.Page1.oPag.oDASCONT4_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDAFINCOM_1_15.enabled = this.oPgFrm.Page1.oPag.oDAFINCOM_1_15.mCond()
    this.oPgFrm.Page1.oPag.oDACODLIS_1_16.enabled = this.oPgFrm.Page1.oPag.oDACODLIS_1_16.mCond()
    this.oPgFrm.Page1.oPag.oDASCOLIS_1_17.enabled = this.oPgFrm.Page1.oPag.oDASCOLIS_1_17.mCond()
    this.oPgFrm.Page1.oPag.oDAPROLIS_1_18.enabled = this.oPgFrm.Page1.oPag.oDAPROLIS_1_18.mCond()
    this.oPgFrm.Page1.oPag.oDAPROSCO_1_19.enabled = this.oPgFrm.Page1.oPag.oDAPROSCO_1_19.mCond()
    this.oPgFrm.Page1.oPag.oDACONCOD_1_68.enabled = this.oPgFrm.Page1.oPag.oDACONCOD_1_68.mCond()
    this.oPgFrm.Page1.oPag.oDACENRIC_1_81.enabled = this.oPgFrm.Page1.oPag.oDACENRIC_1_81.mCond()
    this.oPgFrm.Page1.oPag.oDAATTRIC_1_83.enabled = this.oPgFrm.Page1.oPag.oDAATTRIC_1_83.mCond()
    this.oPgFrm.Page1.oPag.oDATCOFIN_1_90.enabled = this.oPgFrm.Page1.oPag.oDATCOFIN_1_90.mCond()
    this.oPgFrm.Page2.oPag.oDACENCOS_2_2.enabled = this.oPgFrm.Page2.oPag.oDACENCOS_2_2.mCond()
    this.oPgFrm.Page2.oPag.oDAATTIVI_2_4.enabled = this.oPgFrm.Page2.oPag.oDAATTIVI_2_4.mCond()
    this.oPgFrm.Page2.oPag.oDATRIFIN_2_13.enabled = this.oPgFrm.Page2.oPag.oDATRIFIN_2_13.mCond()
    this.oPgFrm.Page2.oPag.oDALISACQ_2_18.enabled = this.oPgFrm.Page2.oPag.oDALISACQ_2_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_7.enabled = this.oPgFrm.Page3.oPag.oBtn_3_7.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_19.enabled = this.oPgFrm.Page3.oPag.oBtn_3_19.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_29.enabled = this.oPgFrm.Page3.oPag.oBtn_3_29.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_30.enabled = this.oPgFrm.Page3.oPag.oBtn_3_30.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_31.enabled = this.oPgFrm.Page3.oPag.oBtn_3_31.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show2
    i_show2=not(UPPER(This.oParentObject.Class)="TGSFA_KTE" OR UPPER(This.oParentObject.oparentobject.Class)="TGSAG_KPR" OR UPPER(This.oParentObject.oparentobject.Class)="TGSAG_KPP")
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Elemento contratto"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oDASCONT2_1_11.visible=!this.oPgFrm.Page1.oPag.oDASCONT2_1_11.mHide()
    this.oPgFrm.Page1.oPag.oDASCONT3_1_12.visible=!this.oPgFrm.Page1.oPag.oDASCONT3_1_12.mHide()
    this.oPgFrm.Page1.oPag.oDASCONT4_1_13.visible=!this.oPgFrm.Page1.oPag.oDASCONT4_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDAINICOM_1_14.visible=!this.oPgFrm.Page1.oPag.oDAINICOM_1_14.mHide()
    this.oPgFrm.Page1.oPag.oDAFINCOM_1_15.visible=!this.oPgFrm.Page1.oPag.oDAFINCOM_1_15.mHide()
    this.oPgFrm.Page1.oPag.oDACODLIS_1_16.visible=!this.oPgFrm.Page1.oPag.oDACODLIS_1_16.mHide()
    this.oPgFrm.Page1.oPag.oDASCOLIS_1_17.visible=!this.oPgFrm.Page1.oPag.oDASCOLIS_1_17.mHide()
    this.oPgFrm.Page1.oPag.oDAPROLIS_1_18.visible=!this.oPgFrm.Page1.oPag.oDAPROLIS_1_18.mHide()
    this.oPgFrm.Page1.oPag.oDAPROSCO_1_19.visible=!this.oPgFrm.Page1.oPag.oDAPROSCO_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS_1_20.visible=!this.oPgFrm.Page1.oPag.oDESLIS_1_20.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS2_1_21.visible=!this.oPgFrm.Page1.oPag.oDESLIS2_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_67.visible=!this.oPgFrm.Page1.oPag.oBtn_1_67.mHide()
    this.oPgFrm.Page1.oPag.oDACONCOD_1_68.visible=!this.oPgFrm.Page1.oPag.oDACONCOD_1_68.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    this.oPgFrm.Page1.oPag.oDESCTR_1_70.visible=!this.oPgFrm.Page1.oPag.oDESCTR_1_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_14.visible=!this.oPgFrm.Page2.oPag.oStr_2_14.mHide()
    this.oPgFrm.Page2.oPag.oDALISACQ_2_18.visible=!this.oPgFrm.Page2.oPag.oDALISACQ_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_19.visible=!this.oPgFrm.Page2.oPag.oStr_2_19.mHide()
    this.oPgFrm.Page2.oPag.oDESACQ_2_20.visible=!this.oPgFrm.Page2.oPag.oDESACQ_2_20.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_7.visible=!this.oPgFrm.Page3.oPag.oBtn_3_7.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_19.visible=!this.oPgFrm.Page3.oPag.oBtn_3_19.mHide()
    this.oPgFrm.Page3.oPag.oAECODSER_3_23.visible=!this.oPgFrm.Page3.oPag.oAECODSER_3_23.mHide()
    this.oPgFrm.Page3.oPag.oAEDESART_3_24.visible=!this.oPgFrm.Page3.oPag.oAEDESART_3_24.mHide()
    this.oPgFrm.Page3.oPag.oARCODSER_3_25.visible=!this.oPgFrm.Page3.oPag.oARCODSER_3_25.mHide()
    this.oPgFrm.Page3.oPag.oARDESART_3_26.visible=!this.oPgFrm.Page3.oPag.oARDESART_3_26.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_29.visible=!this.oPgFrm.Page3.oPag.oBtn_3_29.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_30.visible=!this.oPgFrm.Page3.oPag.oBtn_3_30.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_31.visible=!this.oPgFrm.Page3.oPag.oBtn_3_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_120.visible=!this.oPgFrm.Page1.oPag.oStr_1_120.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_35.visible=!this.oPgFrm.Page2.oPag.oStr_2_35.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_kdl
    If isAhe()
    	If cEvent='w_DACODLIS Changed' or cEvent='w_DAPROLIS Changed' or cEvent='w_DASCOLIS Changed' or cEvent='w_DAPROSCO Changed'
    		this.oparentobject.w_DACODLIS=This.w_DACODLIS
    		this.oparentobject.w_DASCOLIS=This.w_DASCOLIS
    		this.oparentobject.w_DAPROLIS=This.w_DAPROLIS
    		this.oparentobject.w_DAPROSCO=This.w_DAPROSCO
    		this.oparentobject.NotifyEvent('LisRig')
    	endif
    
    	If (cEvent='w_DAPROSCO Changed' or cEvent='w_DASCOLIS Changed')
    		this.w_DASCONT1=This.oparentobject.w_DASCONT1
    		this.w_DASCONT2=This.oparentobject.w_DASCONT2
    		this.w_DASCONT3=This.oparentobject.w_DASCONT3
    		this.w_DASCONT4=This.oparentobject.w_DASCONT4
    	Endif
    	If cEvent='w_DALISACQ Changed'
    		This.oparentobject.w_DALISACQ=this.w_DALISACQ
    		this.oparentobject.NotifyEvent('w_DALISACQ Changed')
    	Endif
      this.w_DAQTAMOV=This.oparentobject.w_DAQTAMOV
    EndIf
    If !IsAhe() and  cEvent='w_DACONCOD Changed'
       this.oparentobject.w_DACONCOD=This.w_DACONCOD
       if empty(This.w_DACONCOD)
        this.oparentobject.NotifyEvent('RicNoCont')
       ELSE
        this.oparentobject.NotifyEvent('Agglis')
       Endif
       this.w_DASCONT1=This.oparentobject.w_DASCONT1
    	 this.w_DASCONT2=This.oparentobject.w_DASCONT2
    	 this.w_DASCONT3=This.oparentobject.w_DASCONT3
    	 this.w_DASCONT4=This.oparentobject.w_DASCONT4
    Endif
    
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("DaCostoaRicavo")
          .Calculate_ATRHLQSOIB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaRicavoaCosto")
          .Calculate_LKWTWVRCIM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Disabbina")
          .Calculate_WXHVPRYZFW()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.oObj_3_52.Event(cEvent)
        if lower(cEvent)==lower("DaCommessaCostoaCommessaRicavo")
          .Calculate_CVZBDAEIBL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaCommessaRicavoaCommessaCosto")
          .Calculate_HJRAFQRQTJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaAttivitaCostoaAttivitaRicavo")
          .Calculate_VTCFAJXQFD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaAttivitaRicavoaAttivitaCosto")
          .Calculate_FSGXMSANDV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kdl
    * Valorizzo dati analitica
    if !Isalt()
     If Cevent='w_DACENRIC Changed'
       This.Notifyevent('DaRicavoaCosto')
     Endif
     If Cevent='w_DACOMRIC Changed' 
       This.Notifyevent('DaCommessaRicavoaCommessaCosto')
     Endif
     If Cevent='w_DACENCOS Changed' 
       This.Notifyevent('DaCostoaRicavo')
     Endif
     If Cevent='w_DACODCOM Changed'
       This.Notifyevent('DaCommessaCostoaCommessaRicavo')
     Endif
      If Cevent='w_DAATTIVI Changed'
       This.Notifyevent('DaAttivitaCostoaAttivitaRicavo')
     Endif
      If Cevent='w_DAATTRIC Changed'
       This.Notifyevent('DaAttivitaRicavoaAttivitaCosto')
     Endif
     if (Upper(this.w_CLASSE)='TCGSAG_MPR' or Upper(this.w_CLASSE)='TCGSAG_MPP') and (Cevent='w_DACOMRIC Changed' or Cevent='w_DAATTRIC Changed')
       This.oParentobject.Notifyevent('Aggcomrig')
     endif
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DACODLIS
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_DACODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_DACODLIS))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_DACODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_DACODLIS)+"%");

            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oDACODLIS_1_16'),i_cWhere,'',"ELENCO LISTINI",'listdocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_DACODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_DACODLIS)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC = NVL(_Link_.LSFLGCIC,space(1))
      this.w_FLSTAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DACODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_IVALIS = space(1)
      this.w_VALLIS = space(3)
      this.w_INILIS = ctod("  /  /  ")
      this.w_FINLIS = ctod("  /  /  ")
      this.w_TIPLIS = space(1)
      this.w_FLGCIC = space(1)
      this.w_FLSTAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAhe() OR CHKLISD(.w_DACODLIS, .w_IVALIS, .w_VALLIS, .w_INILIS, .w_FINLIS,.w_TIPLIS,'P',.w_FLGCIC, .w_FLSTAT,.w_MVFLSCOR, .w_MVCODVAL, .w_DATALIST,'V')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DACODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_IVALIS = space(1)
        this.w_VALLIS = space(3)
        this.w_INILIS = ctod("  /  /  ")
        this.w_FINLIS = ctod("  /  /  ")
        this.w_TIPLIS = space(1)
        this.w_FLGCIC = space(1)
        this.w_FLSTAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DASCOLIS
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DASCOLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_DASCOLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_DASCOLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DASCOLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_DASCOLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_DASCOLIS)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DASCOLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oDASCOLIS_1_17'),i_cWhere,'',"ELENCO LISTINI",'listdocu2.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DASCOLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_DASCOLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_DASCOLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DASCOLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS2 = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS2 = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS2 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS2 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS2 = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC2 = NVL(_Link_.LSFLGCIC,space(1))
      this.w_F2STAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DASCOLIS = space(5)
      endif
      this.w_DESLIS2 = space(40)
      this.w_VALLIS2 = space(3)
      this.w_INILIS2 = ctod("  /  /  ")
      this.w_FINLIS2 = ctod("  /  /  ")
      this.w_TIPLIS2 = space(1)
      this.w_FLGCIC2 = space(1)
      this.w_F2STAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAhe() OR CHKLISD(.w_DASCOLIS, 'L', .w_VALLIS2, .w_INILIS2, .w_FINLIS2,.w_TIPLIS2,'S',.w_FLGCIC2, .w_F2STAT,.w_MVFLSCOR, .w_MVCODVAL, .w_DATALIST,'V')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DASCOLIS = space(5)
        this.w_DESLIS2 = space(40)
        this.w_VALLIS2 = space(3)
        this.w_INILIS2 = ctod("  /  /  ")
        this.w_FINLIS2 = ctod("  /  /  ")
        this.w_TIPLIS2 = space(1)
        this.w_FLGCIC2 = space(1)
        this.w_F2STAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DASCOLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAPROLIS
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAPROLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_DAPROLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_DAPROLIS))
          select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAPROLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAPROLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oDAPROLIS_1_18'),i_cWhere,'',"ELENCO PROMOZIONI",'prodocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAPROLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_DAPROLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_DAPROLIS)
            select LSCODLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAPROLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_IVALISP = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALLIS3 = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS3 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS3 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS3 = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC3 = NVL(_Link_.LSFLGCIC,space(1))
      this.w_F3STAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DAPROLIS = space(5)
      endif
      this.w_IVALISP = space(1)
      this.w_VALLIS3 = space(3)
      this.w_INILIS3 = ctod("  /  /  ")
      this.w_FINLIS3 = ctod("  /  /  ")
      this.w_TIPLIS3 = space(1)
      this.w_FLGCIC3 = space(1)
      this.w_F3STAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAhe() OR CHKLISD(.w_DAPROLIS, .w_IVALISP, .w_VALLIS3, .w_INILIS3, .w_FINLIS3,.w_TIPLIS3,'P',.w_FLGCIC3, .w_F3STAT,.w_MVFLSCOR, .w_MVCODVAL,.w_DATALIST,'V',.w_DACODLIS,.w_DASCOLIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DAPROLIS = space(5)
        this.w_IVALISP = space(1)
        this.w_VALLIS3 = space(3)
        this.w_INILIS3 = ctod("  /  /  ")
        this.w_FINLIS3 = ctod("  /  /  ")
        this.w_TIPLIS3 = space(1)
        this.w_FLGCIC3 = space(1)
        this.w_F3STAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAPROLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAPROSCO
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAPROSCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_DAPROSCO)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_DAPROSCO))
          select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAPROSCO)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAPROSCO) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oDAPROSCO_1_19'),i_cWhere,'',"ELENCO PROMOZIONI",'prodocu2.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAPROSCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_DAPROSCO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_DAPROSCO)
            select LSCODLIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAPROSCO = NVL(_Link_.LSCODLIS,space(5))
      this.w_VALLIS4 = NVL(_Link_.LSVALLIS,space(3))
      this.w_INILIS4 = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS4 = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPLIS4 = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGCIC4 = NVL(_Link_.LSFLGCIC,space(1))
      this.w_F4STAT = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DAPROSCO = space(5)
      endif
      this.w_VALLIS4 = space(3)
      this.w_INILIS4 = ctod("  /  /  ")
      this.w_FINLIS4 = ctod("  /  /  ")
      this.w_TIPLIS4 = space(1)
      this.w_FLGCIC4 = space(1)
      this.w_F4STAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAhe() OR CHKLISD(.w_DAPROSCO, 'L', .w_VALLIS4, .w_INILIS4, .w_FINLIS4,.w_TIPLIS4,'S',.w_FLGCIC4, .w_F4STAT,.w_MVFLSCOR, .w_MVCODVAL, .w_DATALIST,'V', .w_DACODLIS, .w_DASCOLIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DAPROSCO = space(5)
        this.w_VALLIS4 = space(3)
        this.w_INILIS4 = ctod("  /  /  ")
        this.w_FINLIS4 = ctod("  /  /  ")
        this.w_TIPLIS4 = space(1)
        this.w_FLGCIC4 = space(1)
        this.w_F4STAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAPROSCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACONCOD
  func Link_1_68(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACONCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BCZ',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_DACONCOD)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COTIPCLF,CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COTIPCLF',this.w_TIPCON;
                     ,'CONUMERO',trim(this.w_DACONCOD))
          select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COTIPCLF,CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACONCOD)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStrODBC(trim(this.w_DACONCOD)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStr(trim(this.w_DACONCOD)+"%");
                   +" and COTIPCLF="+cp_ToStr(this.w_TIPCON);

            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACONCOD) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(oSource.parent,'oDACONCOD_1_68'),i_cWhere,'GSAR_BCZ',"Elenco contratti",'GSAGCKDL.CON_TRAM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',oSource.xKey(1);
                       ,'CONUMERO',oSource.xKey(2))
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACONCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_DACONCOD);
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',this.w_TIPCON;
                       ,'CONUMERO',this.w_DACONCOD)
            select COTIPCLF,CONUMERO,CODESCON,COCODCLF,COCATCOM,CODATINI,CODATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACONCOD = NVL(_Link_.CONUMERO,space(15))
      this.w_DESCTR = NVL(_Link_.CODESCON,space(50))
      this.w_CODCLF = NVL(_Link_.COCODCLF,space(15))
      this.w_CTCOMM = NVL(_Link_.COCATCOM,space(3))
      this.w_DATINI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACONCOD = space(15)
      endif
      this.w_DESCTR = space(50)
      this.w_CODCLF = space(15)
      this.w_CTCOMM = space(3)
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CODCON=.w_CODCLF OR .w_CATCOM=.w_CTCOMM) AND (.w_DATDOC>=.w_DATINI AND .w_DATDOC<=.w_DATFIN OR EMPTY(.w_DATDOC))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DACONCOD = space(15)
        this.w_DESCTR = space(50)
        this.w_CODCLF = space(15)
        this.w_CTCOMM = space(3)
        this.w_DATINI = ctod("  /  /  ")
        this.w_DATFIN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.COTIPCLF,1)+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACONCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAVOCRIC
  func Link_1_78(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAVOCRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_DAVOCRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_DAVOCRIC))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAVOCRIC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAVOCRIC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oDAVOCRIC_1_78'),i_cWhere,'GSCA_AVC',"Voci di costo o ricavo",'VOCZOOM6.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAVOCRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_DAVOCRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_DAVOCRIC)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAVOCRIC = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOR = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOR = NVL(_Link_.VCTIPVOC,space(1))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(5))
      this.w_VOCOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DAVOCRIC = space(15)
      endif
      this.w_DESVOR = space(40)
      this.w_TIPVOR = space(1)
      this.w_CODCOS = space(5)
      this.w_VOCOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOR='R' AND (.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
        endif
        this.w_DAVOCRIC = space(15)
        this.w_DESVOR = space(40)
        this.w_TIPVOR = space(1)
        this.w_CODCOS = space(5)
        this.w_VOCOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAVOCRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACENRIC
  func Link_1_81(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACENRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_DACENRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_DACENRIC))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACENRIC)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACENRIC) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oDACENRIC_1_81'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACENRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_DACENRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_DACENRIC)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACENRIC = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESRIC = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACENRIC = space(15)
      endif
      this.w_CCDESRIC = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DACENRIC = space(15)
        this.w_CCDESRIC = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACENRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACOMRIC
  func Link_1_82(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACOMRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_DACOMRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_DACOMRIC))
          select CNCODCAN,CNDTOBSO,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACOMRIC)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACOMRIC) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oDACOMRIC_1_82'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACOMRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_DACOMRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_DACOMRIC)
            select CNCODCAN,CNDTOBSO,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACOMRIC = NVL(_Link_.CNCODCAN,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_DACOMRIC = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DESCAN = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_DACOMRIC = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DESCAN = space(100)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACOMRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAATTRIC
  func Link_1_83(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAATTRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_DAATTRIC)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DACOMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_DACOMRIC;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_DAATTRIC))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAATTRIC)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAATTRIC) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oDAATTRIC_1_83'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DACOMRIC<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_DACOMRIC);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAATTRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_DAATTRIC);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DACOMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_DACOMRIC;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_DAATTRIC)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAATTRIC = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_DAATTRIC = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAATTRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAVOCCOS
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAVOCCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_DAVOCCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_DAVOCCOS))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAVOCCOS)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAVOCCOS) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oDAVOCCOS_2_1'),i_cWhere,'GSCA_AVC',"Voci di costo o ricavo",'VOCZOOM1.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAVOCCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_DAVOCCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_DAVOCCOS)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAVOCCOS = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(5))
      this.w_VOCOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DAVOCCOS = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_TIPVOC = space(1)
      this.w_CODCOS = space(5)
      this.w_VOCOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOC='C' AND (.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
        endif
        this.w_DAVOCCOS = space(15)
        this.w_DESVOC = space(40)
        this.w_TIPVOC = space(1)
        this.w_CODCOS = space(5)
        this.w_VOCOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAVOCCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACENCOS
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_DACENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_DACENCOS))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oDACENCOS_2_2'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_DACENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_DACENCOS)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACENCOS = space(15)
      endif
      this.w_CCDESPIA = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DACENCOS = space(15)
        this.w_CCDESPIA = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODCOM
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_DACODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_DACODCOM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oDACODCOM_2_3'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_DACODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_DACODCOM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOS = NVL(_Link_.CNDESCAN,space(100))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACODCOM = space(15)
      endif
      this.w_DESCOS = space(100)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_DACODCOM = space(15)
        this.w_DESCOS = space(100)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAATTIVI
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAATTIVI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_DAATTIVI)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DACODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_DACODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_DAATTIVI))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAATTIVI)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAATTIVI) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oDAATTIVI_2_4'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DACODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_DACODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAATTIVI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_DAATTIVI);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DACODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_DACODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_DAATTIVI)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAATTIVI = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_DAATTIVI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAATTIVI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DALISACQ
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DALISACQ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_DALISACQ)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_DALISACQ))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DALISACQ)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DALISACQ) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oDALISACQ_2_18'),i_cWhere,'',"",'listdocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DALISACQ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_DALISACQ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_DALISACQ)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DALISACQ = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESACQ = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVAACQ = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALACQ = NVL(_Link_.LSVALLIS,space(3))
      this.w_INIACQ = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINACQ = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPACQ = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGACQ = NVL(_Link_.LSFLGCIC,space(1))
      this.w_FLSACQ = NVL(_Link_.LSFLSTAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DALISACQ = space(5)
      endif
      this.w_DESACQ = space(40)
      this.w_IVAACQ = space(1)
      this.w_VALACQ = space(3)
      this.w_INIACQ = ctod("  /  /  ")
      this.w_FINACQ = ctod("  /  /  ")
      this.w_TIPACQ = space(1)
      this.w_FLGACQ = space(1)
      this.w_FLSACQ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAhe() OR (Isahe() and CHKLISD(.w_DALISACQ, .w_IVAACQ, .w_VALACQ, .w_INIACQ, .w_FINACQ,.w_TIPACQ,'P',.w_FLGACQ, .w_FLSACQ,'N', .w_MVCODVAL, .w_DATALIST,'A'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DALISACQ = space(5)
        this.w_DESACQ = space(40)
        this.w_IVAACQ = space(1)
        this.w_VALACQ = space(3)
        this.w_INIACQ = ctod("  /  /  ")
        this.w_FINACQ = ctod("  /  /  ")
        this.w_TIPACQ = space(1)
        this.w_FLGACQ = space(1)
        this.w_FLSACQ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DALISACQ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACONTRA
  func Link_3_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON,CODATINI,CODATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_DACONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_DACONTRA)
            select COSERIAL,CODESCON,CODATINI,CODATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACONTRA = NVL(_Link_.COSERIAL,space(10))
      this.w_CODESCON = NVL(_Link_.CODESCON,space(50))
      this.w_CODATINI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_CODATFIN = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACONTRA = space(10)
      endif
      this.w_CODESCON = space(50)
      this.w_CODATINI = ctod("  /  /  ")
      this.w_CODATFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODMOD
  func Link_3_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_DACODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_DACODMOD)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODMOD = NVL(_Link_.MOCODICE,space(10))
      this.w_MODESCRI = NVL(_Link_.MODESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_DACODMOD = space(10)
      endif
      this.w_MODESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODIMP
  func Link_3_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_DACODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_DACODIMP)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODIMP = NVL(_Link_.IMCODICE,space(10))
      this.w_IMDESCRI = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_DACODIMP = space(10)
      endif
      this.w_IMDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAELMCNT
  func Link_3_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
    i_lTable = "ELE_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2], .t., this.ELE_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAELMCNT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAELMCNT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODMOD,ELCODIMP,ELCODCOM,ELRINNOV,ELCONTRA,ELQTAMOV,ELQTACON";
                   +" from "+i_cTable+" "+i_lTable+" where ELCONTRA="+cp_ToStrODBC(this.w_DAELMCNT);
                   +" and ELCODMOD="+cp_ToStrODBC(this.w_DACODMOD);
                   +" and ELCODIMP="+cp_ToStrODBC(this.w_DACODIMP);
                   +" and ELCODCOM="+cp_ToStrODBC(this.w_DACOCOMP);
                   +" and ELRINNOV="+cp_ToStrODBC(this.w_DARINNOV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODMOD',this.w_DACODMOD;
                       ,'ELCODIMP',this.w_DACODIMP;
                       ,'ELCODCOM',this.w_DACOCOMP;
                       ,'ELRINNOV',this.w_DARINNOV;
                       ,'ELCONTRA',this.w_DAELMCNT)
            select ELCODMOD,ELCODIMP,ELCODCOM,ELRINNOV,ELCONTRA,ELQTAMOV,ELQTACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAELMCNT = NVL(_Link_.ELCONTRA,space(10))
      this.w_ELQTAMOV = NVL(_Link_.ELQTAMOV,0)
      this.w_ELQTACON = NVL(_Link_.ELQTACON,0)
    else
      if i_cCtrl<>'Load'
        this.w_DAELMCNT = space(10)
      endif
      this.w_ELQTAMOV = 0
      this.w_ELQTACON = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])+'\'+cp_ToStr(_Link_.ELCODMOD,1)+'\'+cp_ToStr(_Link_.ELCODIMP,1)+'\'+cp_ToStr(_Link_.ELCODCOM,1)+'\'+cp_ToStr(_Link_.ELRINNOV,1)+'\'+cp_ToStr(_Link_.ELCONTRA,1)
      cp_ShowWarn(i_cKey,this.ELE_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAELMCNT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AECODSER
  func Link_3_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AECODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AECODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_AECODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_AECODSER)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AECODSER = NVL(_Link_.CACODICE,space(41))
      this.w_AEDESART = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_AECODSER = space(41)
      endif
      this.w_AEDESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AECODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCODSER
  func Link_3_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ARCODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ARCODSER)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODSER = NVL(_Link_.CACODICE,space(20))
      this.w_ARDESART = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODSER = space(20)
      endif
      this.w_ARDESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACOCOMP
  func Link_3_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACOCOMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACOCOMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON,CPROWORD";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_DACOCOMP);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_DACODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_DACODIMP;
                       ,'CPROWNUM',this.w_DACOCOMP)
            select IMCODICE,CPROWNUM,IMDESCON,CPROWORD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACOCOMP = NVL(_Link_.CPROWNUM,0)
      this.w_IMDESCON = NVL(_Link_.IMDESCON,space(50))
      this.w_IMROWORD = NVL(_Link_.CPROWORD,0)
    else
      if i_cCtrl<>'Load'
        this.w_DACOCOMP = 0
      endif
      this.w_IMDESCON = space(50)
      this.w_IMROWORD = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACOCOMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDASCONT1_1_10.value==this.w_DASCONT1)
      this.oPgFrm.Page1.oPag.oDASCONT1_1_10.value=this.w_DASCONT1
    endif
    if not(this.oPgFrm.Page1.oPag.oDASCONT2_1_11.value==this.w_DASCONT2)
      this.oPgFrm.Page1.oPag.oDASCONT2_1_11.value=this.w_DASCONT2
    endif
    if not(this.oPgFrm.Page1.oPag.oDASCONT3_1_12.value==this.w_DASCONT3)
      this.oPgFrm.Page1.oPag.oDASCONT3_1_12.value=this.w_DASCONT3
    endif
    if not(this.oPgFrm.Page1.oPag.oDASCONT4_1_13.value==this.w_DASCONT4)
      this.oPgFrm.Page1.oPag.oDASCONT4_1_13.value=this.w_DASCONT4
    endif
    if not(this.oPgFrm.Page1.oPag.oDAINICOM_1_14.value==this.w_DAINICOM)
      this.oPgFrm.Page1.oPag.oDAINICOM_1_14.value=this.w_DAINICOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDAFINCOM_1_15.value==this.w_DAFINCOM)
      this.oPgFrm.Page1.oPag.oDAFINCOM_1_15.value=this.w_DAFINCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODLIS_1_16.value==this.w_DACODLIS)
      this.oPgFrm.Page1.oPag.oDACODLIS_1_16.value=this.w_DACODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDASCOLIS_1_17.value==this.w_DASCOLIS)
      this.oPgFrm.Page1.oPag.oDASCOLIS_1_17.value=this.w_DASCOLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPROLIS_1_18.value==this.w_DAPROLIS)
      this.oPgFrm.Page1.oPag.oDAPROLIS_1_18.value=this.w_DAPROLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPROSCO_1_19.value==this.w_DAPROSCO)
      this.oPgFrm.Page1.oPag.oDAPROSCO_1_19.value=this.w_DAPROSCO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_20.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_20.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS2_1_21.value==this.w_DESLIS2)
      this.oPgFrm.Page1.oPag.oDESLIS2_1_21.value=this.w_DESLIS2
    endif
    if not(this.oPgFrm.Page1.oPag.oDACONCOD_1_68.value==this.w_DACONCOD)
      this.oPgFrm.Page1.oPag.oDACONCOD_1_68.value=this.w_DACONCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCTR_1_70.value==this.w_DESCTR)
      this.oPgFrm.Page1.oPag.oDESCTR_1_70.value=this.w_DESCTR
    endif
    if not(this.oPgFrm.Page1.oPag.oDAVOCRIC_1_78.value==this.w_DAVOCRIC)
      this.oPgFrm.Page1.oPag.oDAVOCRIC_1_78.value=this.w_DAVOCRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVOR_1_80.value==this.w_DESVOR)
      this.oPgFrm.Page1.oPag.oDESVOR_1_80.value=this.w_DESVOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDACENRIC_1_81.value==this.w_DACENRIC)
      this.oPgFrm.Page1.oPag.oDACENRIC_1_81.value=this.w_DACENRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDACOMRIC_1_82.value==this.w_DACOMRIC)
      this.oPgFrm.Page1.oPag.oDACOMRIC_1_82.value=this.w_DACOMRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDAATTRIC_1_83.value==this.w_DAATTRIC)
      this.oPgFrm.Page1.oPag.oDAATTRIC_1_83.value=this.w_DAATTRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESRIC_1_84.value==this.w_CCDESRIC)
      this.oPgFrm.Page1.oPag.oCCDESRIC_1_84.value=this.w_CCDESRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_88.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_88.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCOINI_1_89.value==this.w_DATCOINI)
      this.oPgFrm.Page1.oPag.oDATCOINI_1_89.value=this.w_DATCOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCOFIN_1_90.value==this.w_DATCOFIN)
      this.oPgFrm.Page1.oPag.oDATCOFIN_1_90.value=this.w_DATCOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDAVOCCOS_2_1.value==this.w_DAVOCCOS)
      this.oPgFrm.Page2.oPag.oDAVOCCOS_2_1.value=this.w_DAVOCCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oDACENCOS_2_2.value==this.w_DACENCOS)
      this.oPgFrm.Page2.oPag.oDACENCOS_2_2.value=this.w_DACENCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oDACODCOM_2_3.value==this.w_DACODCOM)
      this.oPgFrm.Page2.oPag.oDACODCOM_2_3.value=this.w_DACODCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDAATTIVI_2_4.value==this.w_DAATTIVI)
      this.oPgFrm.Page2.oPag.oDAATTIVI_2_4.value=this.w_DAATTIVI
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDESPIA_2_5.value==this.w_CCDESPIA)
      this.oPgFrm.Page2.oPag.oCCDESPIA_2_5.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVOC_2_7.value==this.w_DESVOC)
      this.oPgFrm.Page2.oPag.oDESVOC_2_7.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOS_2_11.value==this.w_DESCOS)
      this.oPgFrm.Page2.oPag.oDESCOS_2_11.value=this.w_DESCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oDATRIINI_2_12.value==this.w_DATRIINI)
      this.oPgFrm.Page2.oPag.oDATRIINI_2_12.value=this.w_DATRIINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATRIFIN_2_13.value==this.w_DATRIFIN)
      this.oPgFrm.Page2.oPag.oDATRIFIN_2_13.value=this.w_DATRIFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDALISACQ_2_18.value==this.w_DALISACQ)
      this.oPgFrm.Page2.oPag.oDALISACQ_2_18.value=this.w_DALISACQ
    endif
    if not(this.oPgFrm.Page2.oPag.oDESACQ_2_20.value==this.w_DESACQ)
      this.oPgFrm.Page2.oPag.oDESACQ_2_20.value=this.w_DESACQ
    endif
    if not(this.oPgFrm.Page3.oPag.oDACONTRA_3_8.value==this.w_DACONTRA)
      this.oPgFrm.Page3.oPag.oDACONTRA_3_8.value=this.w_DACONTRA
    endif
    if not(this.oPgFrm.Page3.oPag.oDACODMOD_3_9.value==this.w_DACODMOD)
      this.oPgFrm.Page3.oPag.oDACODMOD_3_9.value=this.w_DACODMOD
    endif
    if not(this.oPgFrm.Page3.oPag.oDACODIMP_3_10.value==this.w_DACODIMP)
      this.oPgFrm.Page3.oPag.oDACODIMP_3_10.value=this.w_DACODIMP
    endif
    if not(this.oPgFrm.Page3.oPag.oIMROWORD_3_11.value==this.w_IMROWORD)
      this.oPgFrm.Page3.oPag.oIMROWORD_3_11.value=this.w_IMROWORD
    endif
    if not(this.oPgFrm.Page3.oPag.oDARINNOV_3_12.value==this.w_DARINNOV)
      this.oPgFrm.Page3.oPag.oDARINNOV_3_12.value=this.w_DARINNOV
    endif
    if not(this.oPgFrm.Page3.oPag.oCODESCON_3_13.value==this.w_CODESCON)
      this.oPgFrm.Page3.oPag.oCODESCON_3_13.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page3.oPag.oMODESCRI_3_15.value==this.w_MODESCRI)
      this.oPgFrm.Page3.oPag.oMODESCRI_3_15.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oELQTAMOV_3_16.value==this.w_ELQTAMOV)
      this.oPgFrm.Page3.oPag.oELQTAMOV_3_16.value=this.w_ELQTAMOV
    endif
    if not(this.oPgFrm.Page3.oPag.oELQTACON_3_17.value==this.w_ELQTACON)
      this.oPgFrm.Page3.oPag.oELQTACON_3_17.value=this.w_ELQTACON
    endif
    if not(this.oPgFrm.Page3.oPag.oQTA__RES_3_18.value==this.w_QTA__RES)
      this.oPgFrm.Page3.oPag.oQTA__RES_3_18.value=this.w_QTA__RES
    endif
    if not(this.oPgFrm.Page3.oPag.oDAUNIMIS_3_20.value==this.w_DAUNIMIS)
      this.oPgFrm.Page3.oPag.oDAUNIMIS_3_20.value=this.w_DAUNIMIS
    endif
    if not(this.oPgFrm.Page3.oPag.oAECODSER_3_23.value==this.w_AECODSER)
      this.oPgFrm.Page3.oPag.oAECODSER_3_23.value=this.w_AECODSER
    endif
    if not(this.oPgFrm.Page3.oPag.oAEDESART_3_24.value==this.w_AEDESART)
      this.oPgFrm.Page3.oPag.oAEDESART_3_24.value=this.w_AEDESART
    endif
    if not(this.oPgFrm.Page3.oPag.oARCODSER_3_25.value==this.w_ARCODSER)
      this.oPgFrm.Page3.oPag.oARCODSER_3_25.value=this.w_ARCODSER
    endif
    if not(this.oPgFrm.Page3.oPag.oARDESART_3_26.value==this.w_ARDESART)
      this.oPgFrm.Page3.oPag.oARDESART_3_26.value=this.w_ARDESART
    endif
    if not(this.oPgFrm.Page3.oPag.oIMDESCRI_3_49.value==this.w_IMDESCRI)
      this.oPgFrm.Page3.oPag.oIMDESCRI_3_49.value=this.w_IMDESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oIMDESCON_3_50.value==this.w_IMDESCON)
      this.oPgFrm.Page3.oPag.oIMDESCON_3_50.value=this.w_IMDESCON
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_DAINICOM<=.w_DAFINCOM) or (empty(.w_DAFINCOM)))  and not(not isahe())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAINICOM_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   ((empty(.w_DAFINCOM)) or not((.w_DAINICOM<=.w_DAFINCOM) or (empty(.w_DAINICOM))))  and not(not isahe())  and (NOT EMPTY(.w_DAINICOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAFINCOM_1_15.SetFocus()
            i_bnoObbl = !empty(.w_DAFINCOM)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not(!IsAhe() OR CHKLISD(.w_DACODLIS, .w_IVALIS, .w_VALLIS, .w_INILIS, .w_FINLIS,.w_TIPLIS,'P',.w_FLGCIC, .w_FLSTAT,.w_MVFLSCOR, .w_MVCODVAL, .w_DATALIST,'V'))  and not(! Isahe())  and (.w_TIPSER <> 'D' And .w_FLGLIS<>'S'  And Empty(.w_DAPROLIS+.w_DAPROSCO))  and not(empty(.w_DACODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACODLIS_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!IsAhe() OR CHKLISD(.w_DASCOLIS, 'L', .w_VALLIS2, .w_INILIS2, .w_FINLIS2,.w_TIPLIS2,'S',.w_FLGCIC2, .w_F2STAT,.w_MVFLSCOR, .w_MVCODVAL, .w_DATALIST,'V'))  and not(! Isahe())  and (.w_TIPSER <> 'D' And .w_FLGLIS<>'S'  And Empty(.w_DAPROLIS+.w_DAPROSCO))  and not(empty(.w_DASCOLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDASCOLIS_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!IsAhe() OR CHKLISD(.w_DAPROLIS, .w_IVALISP, .w_VALLIS3, .w_INILIS3, .w_FINLIS3,.w_TIPLIS3,'P',.w_FLGCIC3, .w_F3STAT,.w_MVFLSCOR, .w_MVCODVAL,.w_DATALIST,'V',.w_DACODLIS,.w_DASCOLIS))  and not(! Isahe())  and (.w_TIPSER <> 'D' And .w_FLGLIS<>'S' And !Empty(.w_DACODLIS+.w_DASCOLIS))  and not(empty(.w_DAPROLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAPROLIS_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!IsAhe() OR CHKLISD(.w_DAPROSCO, 'L', .w_VALLIS4, .w_INILIS4, .w_FINLIS4,.w_TIPLIS4,'S',.w_FLGCIC4, .w_F4STAT,.w_MVFLSCOR, .w_MVCODVAL, .w_DATALIST,'V', .w_DACODLIS, .w_DASCOLIS))  and not(! Isahe())  and (.w_TIPSER <> 'D' And .w_FLGLIS<>'S'  And !Empty(.w_DACODLIS+.w_DASCOLIS))  and not(empty(.w_DAPROSCO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAPROSCO_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_CODCON=.w_CODCLF OR .w_CATCOM=.w_CTCOMM) AND (.w_DATDOC>=.w_DATINI AND .w_DATDOC<=.w_DATFIN OR EMPTY(.w_DATDOC)))  and not(IsAhe())  and (not empty(.w_TIPCON))  and not(empty(.w_DACONCOD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACONCOD_1_68.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPVOR='R' AND (.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO)))  and not(empty(.w_DAVOCRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAVOCRIC_1_78.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
          case   not(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO))  and (!((((g_PERCCM<>'S' and Isahe()) or (g_PERCCR<>'S' and Isahr())) and g_PERCAN<>'S') or (IsAhe() and .w_BUFLANAL<>'S' and not empty(g_CODBUN) )))  and not(empty(.w_DACENRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACENRIC_1_81.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO)))  and not(empty(.w_DACOMRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACOMRIC_1_82.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
          case   not((.w_DATCOINI<=.w_DATCOFIN) or (empty(.w_DATCOFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCOINI_1_89.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   ((empty(.w_DATCOFIN)) or not((.w_DATCOINI<=.w_DATCOFIN) or (empty(.w_DATCOINI))))  and (NOT EMPTY(.w_DATCOINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCOFIN_1_90.SetFocus()
            i_bnoObbl = !empty(.w_DATCOFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not(.w_TIPVOC='C' AND (.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO)))  and not(empty(.w_DAVOCCOS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDAVOCCOS_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
          case   not(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO))  and (!((IsAhr() and g_ACQU<>'S') Or (IsAhe() AND (g_CACQ<>'S' or (.w_BUFLANAL<>'S' and not empty(g_CODBUN) ) )) or (((g_PERCCM<>'S' and Isahe()) or (g_PERCCR<>'S' and Isahr())) and g_PERCAN<>'S')))  and not(empty(.w_DACENCOS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDACENCOS_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(empty(.w_DACODCOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDACODCOM_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
          case   not((.w_DATRIINI<=.w_DATRIFIN) or (empty(.w_DATRIFIN)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATRIINI_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   ((empty(.w_DATRIFIN)) or not((.w_DATRIINI<=.w_DATRIFIN) or (empty(.w_DATRIFIN))))  and (NOT EMPTY(.w_DATRIINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATRIFIN_2_13.SetFocus()
            i_bnoObbl = !empty(.w_DATRIFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   not(!IsAhe() OR (Isahe() and CHKLISD(.w_DALISACQ, .w_IVAACQ, .w_VALACQ, .w_INIACQ, .w_FINACQ,.w_TIPACQ,'P',.w_FLGACQ, .w_FLSACQ,'N', .w_MVCODVAL, .w_DATALIST,'A')))  and not(! Isahe())  and (Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP')  and not(empty(.w_DALISACQ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDALISACQ_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DASCONT1 = this.w_DASCONT1
    this.o_DASCONT2 = this.w_DASCONT2
    this.o_DASCONT3 = this.w_DASCONT3
    this.o_DAINICOM = this.w_DAINICOM
    this.o_DAFINCOM = this.w_DAFINCOM
    this.o_TIPLIS4 = this.w_TIPLIS4
    this.o_TIPLIS3 = this.w_TIPLIS3
    this.o_DACOMRIC = this.w_DACOMRIC
    this.o_DATCOINI = this.w_DATCOINI
    this.o_DACODCOM = this.w_DACODCOM
    this.o_DATRIINI = this.w_DATRIINI
    return

enddefine

* --- Define pages as container
define class tgsag_kdlPag1 as StdContainer
  Width  = 714
  height = 309
  stdWidth  = 714
  stdheight = 309
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDASCONT1_1_10 as StdField with uid="ZEAGXKEMFN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DASCONT1", cQueryName = "DASCONT1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "1^%Maggiorazione (se positiva) o sconto (se negativa) di riga",;
    HelpContextID = 28422553,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=16, Top=33, cSayPict='"999.99"', cGetPict='"999.99"'

  func oDASCONT1_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMSCO>0)
    endwith
   endif
  endfunc

  add object oDASCONT2_1_11 as StdField with uid="LFNQNLOGQY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DASCONT2", cQueryName = "DASCONT2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^%Maggiorazione (se positiva) o sconto (se negativa) di riga",;
    HelpContextID = 28422552,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=83, Top=33, cSayPict='"999.99"', cGetPict='"999.99"'

  func oDASCONT2_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DASCONT1<>0)
    endwith
   endif
  endfunc

  func oDASCONT2_1_11.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<2)
    endwith
  endfunc

  add object oDASCONT3_1_12 as StdField with uid="CRQWZODZPN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DASCONT3", cQueryName = "DASCONT3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "3^%Maggiorazione (se positiva) o sconto (se negativa) di riga",;
    HelpContextID = 28422551,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=150, Top=33, cSayPict='"999.99"', cGetPict='"999.99"'

  func oDASCONT3_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DASCONT2<>0)
    endwith
   endif
  endfunc

  func oDASCONT3_1_12.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<3)
    endwith
  endfunc

  add object oDASCONT4_1_13 as StdField with uid="IIHPYQXCBN",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DASCONT4", cQueryName = "DASCONT4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "4^%Maggiorazione (se positiva) o sconto (se negativa) di riga",;
    HelpContextID = 28422550,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=217, Top=33, cSayPict='"999.99"', cGetPict='"999.99"'

  func oDASCONT4_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DASCONT3<>0)
    endwith
   endif
  endfunc

  func oDASCONT4_1_13.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<4)
    endwith
  endfunc

  add object oDAINICOM_1_14 as StdField with uid="ZHTGRDSKSG",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DAINICOM", cQueryName = "DAINICOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data di inizio competenza contabile",;
    HelpContextID = 49852035,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=523, Top=33

  func oDAINICOM_1_14.mHide()
    with this.Parent.oContained
      return (not isahe())
    endwith
  endfunc

  func oDAINICOM_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DAINICOM<=.w_DAFINCOM) or (empty(.w_DAFINCOM)))
    endwith
    return bRes
  endfunc

  add object oDAFINCOM_1_15 as StdField with uid="MKETVHQQDH",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DAFINCOM", cQueryName = "DAFINCOM",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data di fine competenza contabile",;
    HelpContextID = 54754947,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=628, Top=33

  func oDAFINCOM_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DAINICOM))
    endwith
   endif
  endfunc

  func oDAFINCOM_1_15.mHide()
    with this.Parent.oContained
      return (not isahe())
    endwith
  endfunc

  func oDAFINCOM_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DAINICOM<=.w_DAFINCOM) or (empty(.w_DAINICOM)))
    endwith
    return bRes
  endfunc

  add object oDACODLIS_1_16 as StdField with uid="ICMRWOPFLQ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DACODLIS", cQueryName = "DACODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino utilizzato per la determinazione del prezzo",;
    HelpContextID = 195645065,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=91, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_DACODLIS"

  func oDACODLIS_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSER <> 'D' And .w_FLGLIS<>'S'  And Empty(.w_DAPROLIS+.w_DAPROSCO))
    endwith
   endif
  endfunc

  func oDACODLIS_1_16.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  proc oDACODLIS_1_16.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oDACODLIS_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODLIS_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODLIS_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oDACODLIS_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO LISTINI",'listdocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oDASCOLIS_1_17 as StdField with uid="ZBIWFUOLGF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DASCOLIS", cQueryName = "DASCOLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice listino utilizzato per la determinazione degli sconti/maggiorazioni",;
    HelpContextID = 206458505,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=116, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_DASCOLIS"

  func oDASCOLIS_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSER <> 'D' And .w_FLGLIS<>'S'  And Empty(.w_DAPROLIS+.w_DAPROSCO))
    endwith
   endif
  endfunc

  func oDASCOLIS_1_17.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  proc oDASCOLIS_1_17.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oDASCOLIS_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oDASCOLIS_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDASCOLIS_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oDASCOLIS_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO LISTINI",'listdocu2.LISTINI_VZM',this.parent.oContained
  endproc

  add object oDAPROLIS_1_18 as StdField with uid="VRIDBZETVO",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DAPROLIS", cQueryName = "DAPROLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice promozione utilizzato per la determinazione del prezzo",;
    HelpContextID = 207429257,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=638, Top=91, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_DAPROLIS"

  func oDAPROLIS_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSER <> 'D' And .w_FLGLIS<>'S' And !Empty(.w_DACODLIS+.w_DASCOLIS))
    endwith
   endif
  endfunc

  func oDAPROLIS_1_18.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  proc oDAPROLIS_1_18.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oDAPROLIS_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAPROLIS_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAPROLIS_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oDAPROLIS_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO PROMOZIONI",'prodocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oDAPROSCO_1_19 as StdField with uid="DXITMYNBNF",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DAPROSCO", cQueryName = "DAPROSCO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice promozione utilizzato per la determinazione degli sconti/maggiorazioni",;
    HelpContextID = 212001147,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=638, Top=116, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_DAPROSCO"

  func oDAPROSCO_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSER <> 'D' And .w_FLGLIS<>'S'  And !Empty(.w_DACODLIS+.w_DASCOLIS))
    endwith
   endif
  endfunc

  func oDAPROSCO_1_19.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  proc oDAPROSCO_1_19.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oDAPROSCO_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAPROSCO_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAPROSCO_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oDAPROSCO_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO PROMOZIONI",'prodocu2.LISTINI_VZM',this.parent.oContained
  endproc

  add object oDESLIS_1_20 as StdField with uid="JVXIADCBCJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 218672586,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=177, Top=92, InputMask=replicate('X',40)

  func oDESLIS_1_20.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oDESLIS2_1_21 as StdField with uid="CFNLLJPOMY",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESLIS2", cQueryName = "DESLIS2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 218672586,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=177, Top=117, InputMask=replicate('X',40)

  func oDESLIS2_1_21.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc


  add object oBtn_1_67 as StdButton with uid="DGTXJYSTJS",left=320, top=13, width=48,height=45,;
    CpPicture="BMP\stampa.BMP", caption="", nPag=1;
    , ToolTipText = "Apre la stampa verifica listini applicabili e applicati";
    , HelpContextID = 209100250;
    , caption='\<Verifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_67.Click()
      do GSVE_SVL with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_67.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ( !Empty(.w_DACODLIS+.w_DASCOLIS))
      endwith
    endif
  endfunc

  func oBtn_1_67.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (! Isahe())
     endwith
    endif
  endfunc

  add object oDACONCOD_1_68 as StdField with uid="UXKUJTDYYZ",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DACONCOD", cQueryName = "DACONCOD",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice contratto utilizzato per la determinazione del prezzo",;
    HelpContextID = 55135866,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=150, Top=62, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CON_TRAM", cZoomOnZoom="GSAR_BCZ", oKey_1_1="COTIPCLF", oKey_1_2="this.w_TIPCON", oKey_2_1="CONUMERO", oKey_2_2="this.w_DACONCOD"

  func oDACONCOD_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_TIPCON))
    endwith
   endif
  endfunc

  func oDACONCOD_1_68.mHide()
    with this.Parent.oContained
      return (IsAhe())
    endwith
  endfunc

  func oDACONCOD_1_68.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_68('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACONCOD_1_68.ecpDrop(oSource)
    this.Parent.oContained.link_1_68('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACONCOD_1_68.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_TRAM_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(this.parent,'oDACONCOD_1_68'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BCZ',"Elenco contratti",'GSAGCKDL.CON_TRAM_VZM',this.parent.oContained
  endproc
  proc oDACONCOD_1_68.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BCZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.COTIPCLF=w_TIPCON
     i_obj.w_CONUMERO=this.parent.oContained.w_DACONCOD
     i_obj.ecpSave()
  endproc

  add object oDESCTR_1_70 as StdField with uid="HAYEYNZTMS",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESCTR", cQueryName = "DESCTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 224505290,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=286, Top=62, InputMask=replicate('X',50)

  func oDESCTR_1_70.mHide()
    with this.Parent.oContained
      return (IsAhe())
    endwith
  endfunc

  add object oDAVOCRIC_1_78 as StdField with uid="QXWDUFCHVC",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DAVOCRIC", cQueryName = "DAVOCRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di ricavo incongruente o obsoleto",;
    ToolTipText = "Codice voce di costo",;
    HelpContextID = 26902137,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=110, Top=167, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_DAVOCRIC"

  proc oDAVOCRIC_1_78.mBefore
    with this.Parent.oContained
      .w_VOCTIP='R'
    endwith
  endproc

  func oDAVOCRIC_1_78.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_78('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAVOCRIC_1_78.ecpDrop(oSource)
    this.Parent.oContained.link_1_78('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAVOCRIC_1_78.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oDAVOCRIC_1_78'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo o ricavo",'VOCZOOM6.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oDAVOCRIC_1_78.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_DAVOCRIC
     i_obj.ecpSave()
  endproc

  add object oDESVOR_1_80 as StdField with uid="TBESVEIPFD",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESVOR", cQueryName = "DESVOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 228502986,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=241, Top=167, InputMask=replicate('X',40)

  add object oDACENRIC_1_81 as StdField with uid="SLDUOBYZOH",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DACENRIC", cQueryName = "DACENRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 37703289,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=110, Top=194, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_DACENRIC"

  func oDACENRIC_1_81.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!((((g_PERCCM<>'S' and Isahe()) or (g_PERCCR<>'S' and Isahr())) and g_PERCAN<>'S') or (IsAhe() and .w_BUFLANAL<>'S' and not empty(g_CODBUN) )))
    endwith
   endif
  endfunc

  func oDACENRIC_1_81.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_81('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACENRIC_1_81.ecpDrop(oSource)
    this.Parent.oContained.link_1_81('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACENRIC_1_81.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oDACENRIC_1_81'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oDACENRIC_1_81.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_DACENRIC
     i_obj.ecpSave()
  endproc

  add object oDACOMRIC_1_82 as StdField with uid="OUCDIUEEYQ",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DACOMRIC", cQueryName = "DACOMRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 37310073,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=110, Top=221, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_DACOMRIC"

  func oDACOMRIC_1_82.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_82('Part',this)
      if .not. empty(.w_DAATTRIC)
        bRes2=.link_1_83('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDACOMRIC_1_82.ecpDrop(oSource)
    this.Parent.oContained.link_1_82('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACOMRIC_1_82.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oDACOMRIC_1_82'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oDACOMRIC_1_82.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_DACOMRIC
     i_obj.ecpSave()
  endproc

  add object oDAATTRIC_1_83 as StdField with uid="MOMGXSEJFJ",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DAATTRIC", cQueryName = "DAATTRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 44969593,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=582, Top=221, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_DACOMRIC", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_DAATTRIC"

  func oDAATTRIC_1_83.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DACOMRIC))
    endwith
   endif
  endfunc

  func oDAATTRIC_1_83.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_83('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAATTRIC_1_83.ecpDrop(oSource)
    this.Parent.oContained.link_1_83('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAATTRIC_1_83.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_DACOMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_DACOMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oDAATTRIC_1_83'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oDAATTRIC_1_83.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_DACOMRIC
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_DAATTRIC
     i_obj.ecpSave()
  endproc

  add object oCCDESRIC_1_84 as StdField with uid="TZYJTUVYJB",rtseq=69,rtrep=.t.,;
    cFormVar = "w_CCDESRIC", cQueryName = "CCDESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42950761,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=241, Top=194, InputMask=replicate('X',40)

  add object oDESCAN_1_88 as StdField with uid="XFJRSTNDWN",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 225333814,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=241, Top=221, InputMask=replicate('X',100)

  add object oDATCOINI_1_89 as StdField with uid="USQDCJMXXD",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DATCOINI", cQueryName = "DATCOINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data di inizio competenza",;
    HelpContextID = 156130943,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=110, Top=250

  func oDATCOINI_1_89.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATCOINI<=.w_DATCOFIN) or (empty(.w_DATCOFIN)))
    endwith
    return bRes
  endfunc

  add object oDATCOFIN_1_90 as StdField with uid="UQZRZTIDSO",rtseq=72,rtrep=.f.,;
    cFormVar = "w_DATCOFIN", cQueryName = "DATCOFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data di fine competenza",;
    HelpContextID = 105799300,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=241, Top=250

  func oDATCOFIN_1_90.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATCOINI))
    endwith
   endif
  endfunc

  func oDATCOFIN_1_90.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATCOINI<=.w_DATCOFIN) or (empty(.w_DATCOINI)))
    endwith
    return bRes
  endfunc


  add object oBtn_1_93 as StdButton with uid="JIKTSMTQQF",left=596, top=260, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 186009830;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_93.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_94 as StdButton with uid="UEUVKKPTNX",left=651, top=260, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193298502;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_94.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_22 as StdString with uid="UHOSUHUQRB",Visible=.t., Left=13, Top=116,;
    Alignment=1, Width=94, Height=18,;
    Caption="Listino sc/mag:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="OVCEGGNPWE",Visible=.t., Left=13, Top=91,;
    Alignment=1, Width=94, Height=18,;
    Caption="Listino prezzi:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="KOHASJEEDX",Visible=.t., Left=545, Top=91,;
    Alignment=1, Width=90, Height=18,;
    Caption="Promoz.prezzi:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="MRGVFXRUTH",Visible=.t., Left=522, Top=116,;
    Alignment=1, Width=113, Height=18,;
    Caption="Promoz.sc/mag:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="IQANBLKFBS",Visible=.t., Left=24, Top=8,;
    Alignment=0, Width=245, Height=18,;
    Caption="Sconti / maggiorazioni di riga"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_59 as StdString with uid="BTXCUYMVGK",Visible=.t., Left=72, Top=36,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<2)
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="ZHJAWJZPTZ",Visible=.t., Left=140, Top=36,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<3)
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="NKPLWNOYFT",Visible=.t., Left=206, Top=36,;
    Alignment=0, Width=9, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_NUMSCO<4)
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="AJZCPJPWGI",Visible=.t., Left=411, Top=33,;
    Alignment=1, Width=109, Height=18,;
    Caption="Comp. Co.Ge. dal:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (not isahe())
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="VFAQNBPKQS",Visible=.t., Left=603, Top=33,;
    Alignment=1, Width=23, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (not isahe())
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="OHORBFCDSY",Visible=.t., Left=29, Top=62,;
    Alignment=1, Width=115, Height=18,;
    Caption="Contratto di vendita:"  ;
  , bGlobalFont=.t.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_1_79 as StdString with uid="FRRXLKVZKC",Visible=.t., Left=16, Top=168,;
    Alignment=1, Width=91, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="JEGGTPWUNR",Visible=.t., Left=3, Top=196,;
    Alignment=1, Width=104, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="IGALBXRWFT",Visible=.t., Left=16, Top=221,;
    Alignment=1, Width=91, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="TFCUIMLGDD",Visible=.t., Left=533, Top=221,;
    Alignment=1, Width=46, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="WYGLBVODDX",Visible=.t., Left=4, Top=250,;
    Alignment=1, Width=103, Height=18,;
    Caption="Comp. Co.An. dal:"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (ISAHR())
    endwith
  endfunc

  add object oStr_1_92 as StdString with uid="OSFCXWIHKM",Visible=.t., Left=214, Top=250,;
    Alignment=1, Width=23, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_120 as StdString with uid="RHMJOKOQEE",Visible=.t., Left=14, Top=250,;
    Alignment=1, Width=93, Height=18,;
    Caption="Competenza dal:"  ;
  , bGlobalFont=.t.

  func oStr_1_120.mHide()
    with this.Parent.oContained
      return (ISAHE())
    endwith
  endfunc

  add object oBox_1_58 as StdBox with uid="AIHVZDJYZM",left=3, top=27, width=274,height=1

  add object oBox_1_95 as StdBox with uid="CTCLHJPGOB",left=3, top=158, width=707,height=2
enddefine
define class tgsag_kdlPag2 as StdContainer
  Width  = 714
  height = 309
  stdWidth  = 714
  stdheight = 309
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDAVOCCOS_2_1 as StdField with uid="BXYHJRRPSK",rtseq=96,rtrep=.f.,;
    cFormVar = "w_DAVOCCOS", cQueryName = "DAVOCCOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di ricavo incongruente o obsoleto",;
    ToolTipText = "Codice voce di costo",;
    HelpContextID = 43679369,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=110, Top=59, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_DAVOCCOS"

  proc oDAVOCCOS_2_1.mBefore
    with this.Parent.oContained
      .w_VOCTIP='C'
    endwith
  endproc

  func oDAVOCCOS_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAVOCCOS_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAVOCCOS_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oDAVOCCOS_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo o ricavo",'VOCZOOM1.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oDAVOCCOS_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_DAVOCCOS
     i_obj.ecpSave()
  endproc

  add object oDACENCOS_2_2 as StdField with uid="GWCBQUFVPT",rtseq=97,rtrep=.f.,;
    cFormVar = "w_DACENCOS", cQueryName = "DACENCOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 54480521,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=110, Top=88, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_DACENCOS"

  func oDACENCOS_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!((IsAhr() and g_ACQU<>'S') Or (IsAhe() AND (g_CACQ<>'S' or (.w_BUFLANAL<>'S' and not empty(g_CODBUN) ) )) or (((g_PERCCM<>'S' and Isahe()) or (g_PERCCR<>'S' and Isahr())) and g_PERCAN<>'S')))
    endwith
   endif
  endfunc

  func oDACENCOS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACENCOS_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACENCOS_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oDACENCOS_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oDACENCOS_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_DACENCOS
     i_obj.ecpSave()
  endproc

  add object oDACODCOM_2_3 as StdField with uid="CCYDSSPMTJ",rtseq=98,rtrep=.f.,;
    cFormVar = "w_DACODCOM", cQueryName = "DACODCOM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 44650115,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=110, Top=117, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_DACODCOM"

  func oDACODCOM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
      if .not. empty(.w_DAATTIVI)
        bRes2=.link_2_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDACODCOM_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODCOM_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oDACODCOM_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oDACODCOM_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_DACODCOM
     i_obj.ecpSave()
  endproc

  add object oDAATTIVI_2_4 as StdField with uid="LTBYOWJTQX",rtseq=99,rtrep=.f.,;
    cFormVar = "w_DAATTIVI", cQueryName = "DAATTIVI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 106025345,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=589, Top=117, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_DACODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_DAATTIVI"

  func oDAATTIVI_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DACODCOM))
    endwith
   endif
  endfunc

  func oDAATTIVI_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAATTIVI_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAATTIVI_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_DACODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_DACODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oDAATTIVI_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oDAATTIVI_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_DACODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_DAATTIVI
     i_obj.ecpSave()
  endproc

  add object oCCDESPIA_2_5 as StdField with uid="OQBJBQEOLA",rtseq=100,rtrep=.t.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 9396327,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=241, Top=88, InputMask=replicate('X',40)

  add object oDESVOC_2_7 as StdField with uid="RRXGULMIJI",rtseq=101,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 56709686,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=241, Top=59, InputMask=replicate('X',40)

  add object oDESCOS_2_11 as StdField with uid="EJPUQUFCEC",rtseq=102,rtrep=.f.,;
    cFormVar = "w_DESCOS", cQueryName = "DESCOS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 212970954,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=241, Top=117, InputMask=replicate('X',100)

  add object oDATRIINI_2_12 as StdField with uid="MHRGAUQUGY",rtseq=103,rtrep=.f.,;
    cFormVar = "w_DATRIINI", cQueryName = "DATRIINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    HelpContextID = 150822527,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=110, Top=146

  func oDATRIINI_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATRIINI<=.w_DATRIFIN) or (empty(.w_DATRIFIN)))
    endwith
    return bRes
  endfunc

  add object oDATRIFIN_2_13 as StdField with uid="EGNWXIJZNC",rtseq=104,rtrep=.f.,;
    cFormVar = "w_DATRIFIN", cQueryName = "DATRIFIN",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    HelpContextID = 100490884,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=241, Top=146

  func oDATRIFIN_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATRIINI))
    endwith
   endif
  endfunc

  func oDATRIFIN_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATRIINI<=.w_DATRIFIN) or (empty(.w_DATRIFIN)))
    endwith
    return bRes
  endfunc


  add object oBtn_2_16 as StdButton with uid="GVFZQNEPMN",left=607, top=260, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=2;
    , ToolTipText = "Ok";
    , HelpContextID = 186009830;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_16.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_17 as StdButton with uid="VEQTSANYNM",left=662, top=260, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193298502;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDALISACQ_2_18 as StdField with uid="QTAMQOOBBU",rtseq=105,rtrep=.f.,;
    cFormVar = "w_DALISACQ", cQueryName = "DALISACQ",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 241967481,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=18, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_DALISACQ"

  func oDALISACQ_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP')
    endwith
   endif
  endfunc

  func oDALISACQ_2_18.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  proc oDALISACQ_2_18.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='A'
    endwith
  endproc

  func oDALISACQ_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oDALISACQ_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDALISACQ_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oDALISACQ_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'listdocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oDESACQ_2_20 as StdField with uid="KFLVYRJENR",rtseq=106,rtrep=.f.,;
    cFormVar = "w_DESACQ", cQueryName = "DESACQ",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 259239370,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=179, Top=18, InputMask=replicate('X',40)

  func oDESACQ_2_20.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oStr_2_6 as StdString with uid="JJMWTYGWMS",Visible=.t., Left=16, Top=60,;
    Alignment=1, Width=91, Height=18,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="NDNECIPGFN",Visible=.t., Left=16, Top=90,;
    Alignment=1, Width=91, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="HTOLFADPTS",Visible=.t., Left=16, Top=117,;
    Alignment=1, Width=91, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="THZJZYTFBJ",Visible=.t., Left=534, Top=117,;
    Alignment=1, Width=53, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="AWISAVLBWH",Visible=.t., Left=4, Top=146,;
    Alignment=1, Width=103, Height=18,;
    Caption="Comp. Co.An. dal:"  ;
  , bGlobalFont=.t.

  func oStr_2_14.mHide()
    with this.Parent.oContained
      return (ISAHR())
    endwith
  endfunc

  add object oStr_2_15 as StdString with uid="MVAPYKYVQA",Visible=.t., Left=214, Top=146,;
    Alignment=1, Width=23, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="MGMKXCBPNK",Visible=.t., Left=9, Top=19,;
    Alignment=1, Width=98, Height=18,;
    Caption="Listino costi:"  ;
  , bGlobalFont=.t.

  func oStr_2_19.mHide()
    with this.Parent.oContained
      return (! Isahe())
    endwith
  endfunc

  add object oStr_2_35 as StdString with uid="RLFGMIRTWM",Visible=.t., Left=14, Top=146,;
    Alignment=1, Width=93, Height=18,;
    Caption="Competenza dal:"  ;
  , bGlobalFont=.t.

  func oStr_2_35.mHide()
    with this.Parent.oContained
      return (ISAHE())
    endwith
  endfunc

  add object oBox_2_21 as StdBox with uid="VQHDKVWRRY",left=8, top=48, width=700,height=2
enddefine
define class tgsag_kdlPag3 as StdContainer
  Width  = 714
  height = 309
  stdWidth  = 714
  stdheight = 309
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_3_7 as StdButton with uid="XULRZUMNRR",left=146, top=262, width=48,height=45,;
    CpPicture="BMP\abbina.BMP", caption="", nPag=3;
    , ToolTipText = "Permette di abbinare un elemento contratto alla prestazione attuale";
    , HelpContextID = 211460346;
    , caption='\<Abbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_7.Click()
      do GSAG_KEC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (oParentObject.cFunction<>'Query')
      endwith
    endif
  endfunc

  func oBtn_3_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!EMPTY(.w_DACONTRA))
     endwith
    endif
  endfunc

  add object oDACONTRA_3_8 as StdField with uid="SIASNDMDBU",rtseq=119,rtrep=.f.,;
    cFormVar = "w_DACONTRA", cQueryName = "DACONTRA",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 196522377,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=146, Top=10, InputMask=replicate('X',10), cLinkFile="CON_TRAS", oKey_1_1="COSERIAL", oKey_1_2="this.w_DACONTRA"

  func oDACONTRA_3_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDACODMOD_3_9 as StdField with uid="BYGAMPTQED",rtseq=120,rtrep=.f.,;
    cFormVar = "w_DACODMOD", cQueryName = "DACODMOD",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 212422266,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=146, Top=35, InputMask=replicate('X',10), cLinkFile="MOD_ELEM", oKey_1_1="MOCODICE", oKey_1_2="this.w_DACODMOD"

  func oDACODMOD_3_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DAELMCNT)
        bRes2=.link_3_14('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDACODIMP_3_10 as StdField with uid="LFGJEDROAJ",rtseq=121,rtrep=.f.,;
    cFormVar = "w_DACODIMP", cQueryName = "DACODIMP",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 145313414,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=146, Top=61, InputMask=replicate('X',10), cLinkFile="IMP_MAST", oKey_1_1="IMCODICE", oKey_1_2="this.w_DACODIMP"

  func oDACODIMP_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DAELMCNT)
        bRes2=.link_3_14('Full')
      endif
      if .not. empty(.w_DACOCOMP)
        bRes2=.link_3_32('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oIMROWORD_3_11 as StdField with uid="EJXDUJHYSZ",rtseq=122,rtrep=.f.,;
    cFormVar = "w_IMROWORD", cQueryName = "IMROWORD",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 2471222,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=146, Top=86

  add object oDARINNOV_3_12 as StdField with uid="QSCQLZNPJR",rtseq=123,rtrep=.f.,;
    cFormVar = "w_DARINNOV", cQueryName = "DARINNOV",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 239353484,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=146, Top=112

  func oDARINNOV_3_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DAELMCNT)
        bRes2=.link_3_14('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODESCON_3_13 as StdField with uid="PTSZYUUGUV",rtseq=124,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 59731060,;
   bGlobalFont=.t.,;
    Height=21, Width=474, Left=232, Top=10, InputMask=replicate('X',50)

  add object oMODESCRI_3_15 as StdField with uid="BIZHFQLCSX",rtseq=126,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 208704241,;
   bGlobalFont=.t.,;
    Height=21, Width=474, Left=232, Top=35, InputMask=replicate('X',60)

  add object oELQTAMOV_3_16 as StdField with uid="NIAHKQTYTL",rtseq=127,rtrep=.f.,;
    cFormVar = "w_ELQTAMOV", cQueryName = "ELQTAMOV",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 209664412,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=146, Top=187, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  add object oELQTACON_3_17 as StdField with uid="GWTFWEPNKA",rtseq=128,rtrep=.f.,;
    cFormVar = "w_ELQTACON", cQueryName = "ELQTACON",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 41892244,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=146, Top=212, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  add object oQTA__RES_3_18 as StdField with uid="YQXDOMDMXE",rtseq=129,rtrep=.f.,;
    cFormVar = "w_QTA__RES", cQueryName = "QTA__RES",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211205543,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=146, Top=237, cSayPict='"99999999.999"', cGetPict='"99999999.999"'


  add object oBtn_3_19 as StdButton with uid="TYFCXGJXKY",left=146, top=262, width=48,height=45,;
    CpPicture="BMP\disabbina.BMP", caption="", nPag=3;
    , ToolTipText = "Permette di disabbinare un elemento contratto alla prestazione attuale";
    , HelpContextID = 60718260;
    , caption='\<Disabbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_19.Click()
      this.parent.oContained.NotifyEvent("Disabbina")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (oParentObject.cFunction<>'Query')
      endwith
    endif
  endfunc

  func oBtn_3_19.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DACONTRA))
     endwith
    endif
  endfunc

  add object oDAUNIMIS_3_20 as StdField with uid="RWLHCOWLKL",rtseq=130,rtrep=.f.,;
    cFormVar = "w_DAUNIMIS", cQueryName = "DAUNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 217673353,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=146, Top=162, InputMask=replicate('X',3)

  add object oAECODSER_3_23 as StdField with uid="ZVYPGPACJJ",rtseq=131,rtrep=.f.,;
    cFormVar = "w_AECODSER", cQueryName = "AECODSER",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(41), bMultilanguage =  .f.,;
    HelpContextID = 223784360,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=146, Top=137, InputMask=replicate('X',41), cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_AECODSER"

  func oAECODSER_3_23.mHide()
    with this.Parent.oContained
      return (!isAHE())
    endwith
  endfunc

  func oAECODSER_3_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oAEDESART_3_24 as StdField with uid="FMHTGZQYZP",rtseq=132,rtrep=.f.,;
    cFormVar = "w_AEDESART", cQueryName = "AEDESART",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 242261414,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=364, Top=137, InputMask=replicate('X',40)

  func oAEDESART_3_24.mHide()
    with this.Parent.oContained
      return (!isAHE())
    endwith
  endfunc

  add object oARCODSER_3_25 as StdField with uid="WSNSINAFTN",rtseq=133,rtrep=.f.,;
    cFormVar = "w_ARCODSER", cQueryName = "ARCODSER",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 223781032,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=146, Top=137, InputMask=replicate('X',20), cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_ARCODSER"

  func oARCODSER_3_25.mHide()
    with this.Parent.oContained
      return (isAHE())
    endwith
  endfunc

  func oARCODSER_3_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oARDESART_3_26 as StdField with uid="ZPVIHFHZDA",rtseq=134,rtrep=.f.,;
    cFormVar = "w_ARDESART", cQueryName = "ARDESART",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 242258086,;
   bGlobalFont=.t.,;
    Height=21, Width=402, Left=304, Top=137, InputMask=replicate('X',40)

  func oARDESART_3_26.mHide()
    with this.Parent.oContained
      return (isAHE())
    endwith
  endfunc


  add object oBtn_3_27 as StdButton with uid="LWAAPZUJWV",left=605, top=262, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=3;
    , ToolTipText = "Ok";
    , HelpContextID = 186009830;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_27.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_28 as StdButton with uid="ZQUFNUGKVA",left=660, top=262, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193298502;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_28.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_29 as StdButton with uid="KSUWNHATXF",left=605, top=212, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per accedere alla visualizzazione delle attivit� generate";
    , HelpContextID = 228778096;
    , Caption='\<Vis. Att.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_29.Click()
      with this.Parent.oContained
        GSAG2BZM(this.Parent.oContained,"AO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_DACONTRA))
      endwith
    endif
  endfunc

  func oBtn_3_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_ATTEXIST)
     endwith
    endif
  endfunc


  add object oBtn_3_30 as StdButton with uid="CGHHCAFYSP",left=659, top=211, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per accedere alla visualizzazione dei documenti generati";
    , HelpContextID = 228778096;
    , Caption='\<Vis. Doc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_30.Click()
      with this.Parent.oContained
        GSAG2BZM(this.Parent.oContained,"DO") 
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_DACONTRA))
      endwith
    endif
  endfunc

  func oBtn_3_30.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_DOCEXIST)
     endwith
    endif
  endfunc


  add object oBtn_3_31 as StdButton with uid="QKEXAHHHPN",left=195, top=262, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per accedere all'elemento contratto associato";
    , HelpContextID = 228778096;
    , Caption='\<Vis.elem.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_31.Click()
      with this.Parent.oContained
        opengest("A","GSAG_AEL","ELCODMOD", .w_DACODMOD ,"ELCODCLI",.w_CODCLI,"ELCONTRA",.w_DACONTRA,"ELCODIMP",.w_DACODIMP,"ELCODCOM",.w_DACOCOMP,"ELRINNOV",.w_DARINNOV)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_DACONTRA))
      endwith
    endif
  endfunc

  func oBtn_3_31.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_DACONTRA))
     endwith
    endif
  endfunc

  add object oIMDESCRI_3_49 as StdField with uid="HSYGHCCPDU",rtseq=150,rtrep=.f.,;
    cFormVar = "w_IMDESCRI", cQueryName = "IMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208704817,;
   bGlobalFont=.t.,;
    Height=21, Width=474, Left=232, Top=61, InputMask=replicate('X',50)

  add object oIMDESCON_3_50 as StdField with uid="JQMBWCWMKC",rtseq=151,rtrep=.f.,;
    cFormVar = "w_IMDESCON", cQueryName = "IMDESCON",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 59730644,;
   bGlobalFont=.t.,;
    Height=21, Width=474, Left=232, Top=86, InputMask=replicate('X',50)


  add object oObj_3_52 as cp_runprogram with uid="WMCMDQULXA",left=315, top=332, width=220,height=19,;
    caption='GSAG2BZM(XR)',;
   bGlobalFont=.t.,;
    prg="GSAG2BZM('XR')",;
    cEvent = "Blank",;
    nPag=3;
    , ToolTipText = "Aggiorna gli elementi contratto";
    , HelpContextID = 11516723

  add object oStr_3_1 as StdString with uid="XGNOHOVNMK",Visible=.t., Left=7, Top=13,;
    Alignment=1, Width=134, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_2 as StdString with uid="ELZSHFBFOO",Visible=.t., Left=7, Top=36,;
    Alignment=1, Width=134, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_3_3 as StdString with uid="ITQURESLBJ",Visible=.t., Left=7, Top=114,;
    Alignment=1, Width=134, Height=18,;
    Caption="Rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="LNBFHYAABV",Visible=.t., Left=7, Top=189,;
    Alignment=1, Width=134, Height=18,;
    Caption="Quantit� totale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_5 as StdString with uid="ABFZLPRRLP",Visible=.t., Left=7, Top=214,;
    Alignment=1, Width=134, Height=18,;
    Caption="Quantit� consumata:"  ;
  , bGlobalFont=.t.

  add object oStr_3_6 as StdString with uid="LPDEAPTJSU",Visible=.t., Left=7, Top=239,;
    Alignment=1, Width=134, Height=18,;
    Caption="Quantit� residua:"  ;
  , bGlobalFont=.t.

  add object oStr_3_21 as StdString with uid="UQBPADTBGU",Visible=.t., Left=7, Top=139,;
    Alignment=1, Width=134, Height=18,;
    Caption="Prestazione su riga:"  ;
  , bGlobalFont=.t.

  add object oStr_3_22 as StdString with uid="QLFGKLYHSP",Visible=.t., Left=7, Top=164,;
    Alignment=1, Width=134, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_3_47 as StdString with uid="XSKMMQIAMO",Visible=.t., Left=6, Top=62,;
    Alignment=1, Width=134, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_48 as StdString with uid="LAMDYYUUEA",Visible=.t., Left=6, Top=88,;
    Alignment=1, Width=134, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kdl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
