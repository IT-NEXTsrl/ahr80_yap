* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bp1                                                        *
*              Carica valori di default su impianti da inserimento provvisorio delle prestazioni*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-26                                                      *
* Last revis.: 2013-06-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bp1",oParentObject)
return(i_retval)

define class tgsag_bp1 as StdBatch
  * --- Local variables
  w_SETLINKED = .f.
  w_PADRE = .NULL.
  w_NONNO = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica valori di default da inserimento provvisorio delle impianti
    this.oParentObject.w_PRCODVAL = g_PERVAL
    this.w_PADRE = this.oParentObject
    this.w_NONNO = this.oParentObject.oParentObject
    if NOT EMPTY(this.w_NONNO.w_ATCODPRA)
      this.oParentObject.w_PRNUMPRA = this.w_NONNO.w_ATCODPRA
      this.w_SETLINKED = SetValueLinked("FF",this.w_PADRE,"w_PRNUMPRA",this.oParentObject.w_PRNUMPRA)
      this.oParentObject.w_DACODCOM = this.w_NONNO.w_ATCODPRA
      this.w_SETLINKED = SetValueLinked("FF",this.w_PADRE,"w_DACODCOM",this.oParentObject.w_DACODCOM)
    endif
    if NOT EMPTY(this.w_NONNO.w_ATCOMRIC)
      this.oParentObject.w_DACOMRIC = this.w_NONNO.w_ATCOMRIC
      this.w_SETLINKED = SetValueLinked("FF",this.w_PADRE,"w_DACOMRIC",this.oParentObject.w_DACOMRIC)
    endif
    if NOT EMPTY(this.w_NONNO.w_ATATTRIC)
      this.oParentObject.w_DAATTRIC = this.w_NONNO.w_ATATTRIC
      this.w_SETLINKED = SetValueLinked("FF",this.w_PADRE,"w_DAATTRIC",this.oParentObject.w_DAATTRIC)
    endif
    if NOT EMPTY(this.w_NONNO.w_ATCENCOS) AND (this.w_NONNO.CLASS="Tgsag_kpr" OR this.w_NONNO.CLASS="Tgsag_kpp")
      this.oParentObject.w_DACENCOS = this.w_NONNO.w_ATCENCOS
      this.w_SETLINKED = SetValueLinked("M",this.w_PADRE,"w_DACENCOS",this.oParentObject.w_DACENCOS)
    endif
    if NOT EMPTY(this.w_NONNO.w_ATCENRIC) AND (this.w_NONNO.CLASS="Tgsag_kpr" OR this.w_NONNO.CLASS="Tgsag_kpp")
      this.oParentObject.w_DACENRIC = this.w_NONNO.w_ATCENRIC
      this.w_SETLINKED = SetValueLinked("M",this.w_PADRE,"w_DACENRIC",this.oParentObject.w_DACENRIC)
    endif
    if NOT EMPTY(this.w_NONNO.w_DAATTIVI)
      this.oParentObject.w_DAATTIVI = this.w_NONNO.w_DAATTIVI
      this.oParentObject.o_DACODCOM = this.w_NONNO.w_ATCODPRA
      this.w_SETLINKED = SetValueLinked("M",this.w_PADRE,"w_DAATTIVI",this.oParentObject.w_DAATTIVI)
    endif
    if NOT EMPTY(this.w_NONNO.w_ATATTRIC)
      this.oParentObject.w_DAATTRIC = this.w_NONNO.w_ATATTRIC
      this.w_SETLINKED = SetValueLinked("M",this.w_PADRE,"w_DAATTRIC",this.oParentObject.w_DAATTRIC)
    endif
    if NOT EMPTY(this.w_NONNO.w_DACODNOM)
      this.oParentObject.w_DACODNOM = this.w_NONNO.w_DACODNOM
      this.w_SETLINKED = SetValueLinked("M",this.w_PADRE,"w_DACODNOM",this.oParentObject.w_DACODNOM)
    endif
    if NOT EMPTY(this.w_NONNO.w_DACODSED)
      this.oParentObject.w_DACODSED = this.w_NONNO.w_DACODSED
      this.w_SETLINKED = SetValueLinked("M",this.w_PADRE,"w_DACODSED",this.oParentObject.w_DACODSED)
    endif
    if not isahe() and NOT EMPTY(this.w_NONNO.w_ATCODLIS)
      this.oParentObject.w_PRCODLIS = this.w_NONNO.w_ATCODLIS
      this.w_SETLINKED = SetValueLinked("M",this.w_PADRE,"w_PRCODLIS",this.oParentObject.w_PRCODLIS, g_PERVAL)
    endif
    if NOT EMPTY(this.w_NONNO.w_ATLISACQ)
      this.oParentObject.w_DALISACQ = this.w_NONNO.w_ATLISACQ
      this.w_SETLINKED = SetValueLinked("M",this.w_PADRE,"w_DALISACQ",this.oParentObject.w_DALISACQ)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
