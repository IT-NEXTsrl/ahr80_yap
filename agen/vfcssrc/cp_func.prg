* --- Container for functions
* --- START GSAG_BKT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bkt                                                        *
*              Controlli in cancellazione                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-28                                                      *
* Last revis.: 2010-02-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func gsag_bkt
param pPADRE,pROWNUM

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_LEGAME
  m.w_LEGAME=0
  private w_NUMREC
  m.w_NUMREC=0
  private w_OK
  m.w_OK=.f.
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "gsag_bkt"
if vartype(__gsag_bkt_hook__)='O'
  __gsag_bkt_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'gsag_bkt('+Transform(pPADRE)+','+Transform(pROWNUM)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'gsag_bkt')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
gsag_bkt_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'gsag_bkt('+Transform(pPADRE)+','+Transform(pROWNUM)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'gsag_bkt')
Endif
*--- Activity log
if vartype(__gsag_bkt_hook__)='O'
  __gsag_bkt_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure gsag_bkt_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Controlli in cancellazione lanciati dalla gestione impianti (GSAG_MIM)
  m.w_OK = .T.
  m.w_LEGAME = m.pROWNUM
  * --- Blocco l'eliminazione della riga
  m.w_NUMREC = m.pPADRE.Search("nvl(t_IMLEGAME, 0)="+cp_ToStrODBC(m.w_LEGAME) + " and not deleted()")
  if m.w_NUMREC<>-1
    m.w_OK = .F.
  endif
  i_retcode = 'stop'
  i_retval = m.w_OK
  return
endproc


* --- END GSAG_BKT
* --- START ORALEGALE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: oralegale                                                       *
*              Ora legale                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-05                                                      *
* Last revis.: 2009-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func oralegale
param DataParam

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_IniOraLeg
  m.w_IniOraLeg=ctod("  /  /  ")
  private w_FinOraLeg
  m.w_FinOraLeg=ctod("  /  /  ")
  private w_ValToRet
  m.w_ValToRet=.f.
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "oralegale"
if vartype(__oralegale_hook__)='O'
  __oralegale_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'oralegale('+Transform(DataParam)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'oralegale')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
oralegale_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'oralegale('+Transform(DataParam)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'oralegale')
Endif
*--- Activity log
if vartype(__oralegale_hook__)='O'
  __oralegale_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure oralegale_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Restituisce .T. se la data passata come parametro cade in un periodo di ora legale
  * --- La data inizio dell'ora legale � l'ultima domenica di marzo e finisce l'ultima domenica di ottobre
  do case
    case Month(m.DataParam)<=2 OR Month(m.DataParam)>=11
      * --- Gennaio, febbraio, novembre, dicembre.
      *     Certamente non � ora legale
      m.w_ValToRet = .F.
    case Month(m.DataParam)>=4 AND Month(m.DataParam)<=10
      * --- Aprile, maggio, giugno, luglio, agosto, settembre.
      *     Certamente � ora legale
      m.w_ValToRet = .T.
    otherwise
      * --- Marzo e ottobre, devo controllare l'intervallo
      m.w_IniOraLeg = CTOD("31-03-"+ALLTRIM(STR(YEAR(m.DataParam))))
      do while DOW(m.w_IniOraLeg,1)<>1
        m.w_IniOraLeg = m.w_IniOraLeg - 1
      enddo
      m.w_FinOraLeg = CTOD("31-10-"+ALLTRIM(STR(YEAR(m.DataParam))))
      do while DOW(m.w_FinOraLeg,1)<>1
        m.w_FinOraLeg = m.w_FinOraLeg - 1
      enddo
      m.w_ValToRet = IIF(m.w_IniOraLeg<=m.DataParam AND m.w_FinOraLeg>=m.DataParam,.T.,.F.)
  endcase
  i_retcode = 'stop'
  i_retval = m.w_ValToRet
  return
endproc


* --- END ORALEGALE
* --- START ISRISATT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: isrisatt                                                        *
*              Verifica se l'attivit� � riservata                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-04-16                                                      *
* Last revis.: 2009-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func isrisatt
param pCodAtt,pRiservata,pForm

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_GESRIS
  m.w_GESRIS=space(1)
  private w_RESULT
  m.w_RESULT=.f.
* --- WorkFile variables
  private PAR_AGEN_idx
  PAR_AGEN_idx=0
  private OFF_PART_idx
  OFF_PART_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "isrisatt"
if vartype(__isrisatt_hook__)='O'
  __isrisatt_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'isrisatt('+Transform(pCodAtt)+','+Transform(pRiservata)+','+Transform(pForm)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'isrisatt')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if isrisatt_OpenTables()
  isrisatt_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_KAR')
  use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_KAR
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'isrisatt('+Transform(pCodAtt)+','+Transform(pRiservata)+','+Transform(pForm)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'isrisatt')
Endif
*--- Activity log
if vartype(__isrisatt_hook__)='O'
  __isrisatt_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure isrisatt_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Decide se i_CODUTE pu� accedere all'attivit�...
  *     pForms passatta per problema della canview che in filtro
  *     a causa del lockscreen mostra cmq il contenuto del record
  m.w_RESULT = .T.
  if m.pRiservata="S"
    * --- Read from PAR_AGEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[PAR_AGEN_idx,2],.t.,PAR_AGEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PAGESRIS"+;
        " from "+i_cTable+" PAR_AGEN where ";
            +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PAGESRIS;
        from (i_cTable) where;
            PACODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_GESRIS = NVL(cp_ToDate(_read_.PAGESRIS),cp_NullValue(_read_.PAGESRIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if m.w_GESRIS="S"
      * --- Select from ..\AGEN\EXE\QUERY\GSAG_KAR
      do vq_exec with '..\AGEN\EXE\QUERY\GSAG_KAR',createobject('cp_getfuncvar'),'_Curs__d__d__AGEN_EXE_QUERY_GSAG_KAR','',.f.,.t.
      if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_KAR')
        select _Curs__d__d__AGEN_EXE_QUERY_GSAG_KAR
        locate for 1=1
        do while not(eof())
        m.w_RESULT = Nvl( _Curs__d__d__AGEN_EXE_QUERY_GSAG_KAR.conta , 0 )>0
          select _Curs__d__d__AGEN_EXE_QUERY_GSAG_KAR
          continue
        enddo
        use
      endif
    endif
  endif
  if not m.w_RESULT And vartype( m.pForm )="O"
    m.pForm.LOCKSCREEN=.F.
  endif
  i_retcode = 'stop'
  i_retval = m.w_RESULT
  return
endproc


  function isrisatt_OpenTables()
    dimension i_cWorkTables[max(1,2)]
    i_cWorkTables[1]='PAR_AGEN'
    i_cWorkTables[2]='OFF_PART'
    return(cp_OpenFuncTables(2))
* --- END ISRISATT
* --- START INITSTATO
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: initstato                                                       *
*              Valorizza stato attivit� di default                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-17                                                      *
* Last revis.: 2009-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func initstato

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_StatoRet
  m.w_StatoRet=space(1)
* --- WorkFile variables
  private PAR_AGEN_idx
  PAR_AGEN_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "initstato"
if vartype(__initstato_hook__)='O'
  __initstato_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'initstato()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'initstato')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if initstato_OpenTables()
  initstato_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'initstato()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'initstato')
Endif
*--- Activity log
if vartype(__initstato_hook__)='O'
  __initstato_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure initstato_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * ---                                                    Inizializza lo stato attivit� in Agenda e in Elenco/Stampa attivit�
  * --- Read from PAR_AGEN
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PAR_AGEN_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PAR_AGEN_idx,2],.t.,PAR_AGEN_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "PAST_ATT"+;
      " from "+i_cTable+" PAR_AGEN where ";
          +"PACODAZI = "+cp_ToStrODBC(i_codazi);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      PAST_ATT;
      from (i_cTable) where;
          PACODAZI = i_codazi;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_StatoRet = NVL(cp_ToDate(_read_.PAST_ATT),cp_NullValue(_read_.PAST_ATT))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if EMPTY(m.w_StatoRet)
    * --- Valore di default: Non evasa
    m.w_StatoRet = "N"
  endif
  i_retcode = 'stop'
  i_retval = m.w_StatoRet
  return
endproc


  function initstato_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='PAR_AGEN'
    return(cp_OpenFuncTables(1))
* --- END INITSTATO
* --- START AGGROWNUM
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: aggrownum                                                       *
*              Aggiorna riga in inserimento provvisorio                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-04                                                      *
* Last revis.: 2010-03-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func aggrownum
param w_CODRES,w_GRURES,w_ROWNUM

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_MAXROW
  m.w_MAXROW=0
* --- WorkFile variables
  private RAP_PRES_idx
  RAP_PRES_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "aggrownum"
if vartype(__aggrownum_hook__)='O'
  __aggrownum_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'aggrownum('+Transform(w_CODRES)+','+Transform(w_GRURES)+','+Transform(w_ROWNUM)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'aggrownum')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if aggrownum_OpenTables()
  aggrownum_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_RAP_PRES')
  use in _Curs_RAP_PRES
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'aggrownum('+Transform(w_CODRES)+','+Transform(w_GRURES)+','+Transform(w_ROWNUM)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'aggrownum')
Endif
*--- Activity log
if vartype(__aggrownum_hook__)='O'
  __aggrownum_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure aggrownum_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Codice responsabile
  * --- Codice gruppo
  * --- rownum di aggiornamento
  * --- Select from RAP_PRES
  i_nConn=i_TableProp[RAP_PRES_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[RAP_PRES_idx,2],.t.,RAP_PRES_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS MAXROW from "+i_cTable+" RAP_PRES ";
        +" where DACODRES="+cp_ToStrODBC(m.w_CODRES)+" AND DAGRURES= "+cp_ToStrODBC(m.w_GRURES)+"";
         ,"_Curs_RAP_PRES")
  else
    select MAX(CPROWNUM) AS MAXROW from (i_cTable);
     where DACODRES=m.w_CODRES AND DAGRURES= m.w_GRURES;
      into cursor _Curs_RAP_PRES
  endif
  if used('_Curs_RAP_PRES')
    select _Curs_RAP_PRES
    locate for 1=1
    do while not(eof())
    m.w_MAXROW = NVL(_Curs_RAP_PRES.MAXROW,0)
    exit
      select _Curs_RAP_PRES
      continue
    enddo
    use
  endif
  m.w_MAXROW = m.w_MAXROW+1
  if m.w_MAXROW=0 or m.w_MAXROW<m.w_ROWNUM
    * --- Se il rownum cercato nel database � a zero o inferiore a quanto sono arrivato
    *     mantengo il cprownum
    m.w_MAXROW = m.w_ROWNUM
  endif
  i_retcode = 'stop'
  i_retval = m.w_MAXROW
  return
endproc


  function aggrownum_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='RAP_PRES'
    return(cp_OpenFuncTables(1))
* --- END AGGROWNUM
* --- START GETTIPASS
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gettipass                                                       *
*              Determina tipo attivit� da applicare agli eventi                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-30                                                      *
* Last revis.: 2010-06-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func gettipass
param pTIPEVE,pTIPO

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_DRIVER
  m.w_DRIVER=space(10)
  private w_DE__TIPO
  m.w_DE__TIPO=space(1)
  private w_READAZI
  m.w_READAZI=space(5)
  private w_TIPATT
  m.w_TIPATT=space(10)
  private w_TIPMAIL
  m.w_TIPMAIL=space(10)
  private w_TIPTEL
  m.w_TIPTEL=space(10)
  private w_TIPGEN
  m.w_TIPGEN=space(10)
  private w_TIPAPF
  m.w_TIPAPF=space(10)
  private w_TIPASS
  m.w_TIPASS=space(10)
  private w_CAUPRE
  m.w_CAUPRE=space(10)
  private w_CAUDOC
  m.w_CAUDOC=space(20)
* --- WorkFile variables
  private TIPEVENT_idx
  TIPEVENT_idx=0
  private DRVEVENT_idx
  DRVEVENT_idx=0
  private PAR_AGEN_idx
  PAR_AGEN_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "gettipass"
if vartype(__gettipass_hook__)='O'
  __gettipass_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'gettipass('+Transform(pTIPEVE)+','+Transform(pTIPO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'gettipass')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if gettipass_OpenTables()
  gettipass_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'gettipass('+Transform(pTIPEVE)+','+Transform(pTIPO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'gettipass')
Endif
*--- Activity log
if vartype(__gettipass_hook__)='O'
  __gettipass_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure gettipass_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Dato un tipo evento sceglie tipo attivit� da applicare in funzione
  *     del driver associato
  * --- Tipo evento
  * --- M: Mail
  *     A: Altro
  *     T: Telefonata
  *     F: Fax
  *     P: Presa in carico\smistamento
  *     I: Inserimento provvisorio
  *     D: Documento
  m.w_READAZI = i_CODAZI
  if Vartype(m.pTIPO)="L"
    * --- Read from TIPEVENT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[TIPEVENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[TIPEVENT_idx,2],.t.,TIPEVENT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TEDRIVER"+;
        " from "+i_cTable+" TIPEVENT where ";
            +"TETIPEVE = "+cp_ToStrODBC(m.pTIPEVE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TEDRIVER;
        from (i_cTable) where;
            TETIPEVE = m.pTIPEVE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      m.w_DRIVER = NVL(cp_ToDate(_read_.TEDRIVER),cp_NullValue(_read_.TEDRIVER))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not Empty(m.w_DRIVER)
      * --- Read from DRVEVENT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[DRVEVENT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[DRVEVENT_idx,2],.t.,DRVEVENT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DE__TIPO"+;
          " from "+i_cTable+" DRVEVENT where ";
              +"DEDRIVER = "+cp_ToStrODBC(m.w_DRIVER);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DE__TIPO;
          from (i_cTable) where;
              DEDRIVER = m.w_DRIVER;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        m.w_DE__TIPO = NVL(cp_ToDate(_read_.DE__TIPO),cp_NullValue(_read_.DE__TIPO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
  else
    m.w_DE__TIPO = m.pTIPO
  endif
  * --- Read from PAR_AGEN
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PAR_AGEN_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PAR_AGEN_idx,2],.t.,PAR_AGEN_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "PATIPAPI,PATIPTEL,PATIPGEN,PATIPAPF,PATIPASS,PACAUPRE,PATIPCOS"+;
      " from "+i_cTable+" PAR_AGEN where ";
          +"PACODAZI = "+cp_ToStrODBC(m.w_READAZI);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      PATIPAPI,PATIPTEL,PATIPGEN,PATIPAPF,PATIPASS,PACAUPRE,PATIPCOS;
      from (i_cTable) where;
          PACODAZI = m.w_READAZI;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_TIPMAIL = NVL(cp_ToDate(_read_.PATIPAPI),cp_NullValue(_read_.PATIPAPI))
    m.w_TIPTEL = NVL(cp_ToDate(_read_.PATIPTEL),cp_NullValue(_read_.PATIPTEL))
    m.w_TIPGEN = NVL(cp_ToDate(_read_.PATIPGEN),cp_NullValue(_read_.PATIPGEN))
    m.w_TIPAPF = NVL(cp_ToDate(_read_.PATIPAPF),cp_NullValue(_read_.PATIPAPF))
    m.w_TIPASS = NVL(cp_ToDate(_read_.PATIPASS),cp_NullValue(_read_.PATIPASS))
    m.w_CAUPRE = NVL(cp_ToDate(_read_.PACAUPRE),cp_NullValue(_read_.PACAUPRE))
    m.w_CAUDOC = NVL(cp_ToDate(_read_.PATIPCOS),cp_NullValue(_read_.PATIPCOS))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  do case
    case m.w_DE__TIPO="M"
      m.w_TIPATT = m.w_TIPMAIL
    case m.w_DE__TIPO="T"
      m.w_TIPATT = m.w_TIPTEL
    case m.w_DE__TIPO="A"
      m.w_TIPATT = m.w_TIPGEN
    case m.w_DE__TIPO="P"
      m.w_TIPATT = m.w_TIPAPF
    case m.w_DE__TIPO="F"
      m.w_TIPATT = m.w_TIPASS
    case m.w_DE__TIPO="I"
      m.w_TIPATT = m.w_CAUPRE
    case m.w_DE__TIPO="D"
      m.w_TIPATT = m.w_CAUDOC
  endcase
  i_retcode = 'stop'
  i_retval = m.w_TIPATT
  return
endproc


  function gettipass_OpenTables()
    dimension i_cWorkTables[max(1,3)]
    i_cWorkTables[1]='TIPEVENT'
    i_cWorkTables[2]='DRVEVENT'
    i_cWorkTables[3]='PAR_AGEN'
    return(cp_OpenFuncTables(3))
* --- END GETTIPASS
* --- START CHKPHONE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chkphone                                                        *
*              Controllo se numero di telefono utilizzabile                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-09                                                      *
* Last revis.: 2010-04-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func chkphone
param pTELEFONO,pTIPO

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_VALID
  m.w_VALID=.f.
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "chkphone"
if vartype(__chkphone_hook__)='O'
  __chkphone_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'chkphone('+Transform(pTELEFONO)+','+Transform(pTIPO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chkphone')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
chkphone_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'chkphone('+Transform(pTELEFONO)+','+Transform(pTIPO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chkphone')
Endif
*--- Activity log
if vartype(__chkphone_hook__)='O'
  __chkphone_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure chkphone_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Controllo se numero di telefono utilizzabile
  m.w_VALID = .f.
  if m.pTIPO="P"
    * --- Select from ..\agen\exe\query\chkphone1
    do vq_exec with '..\agen\exe\query\chkphone1',createobject('cp_getfuncvar'),'_Curs__d__d__agen_exe_query_chkphone1','',.f.,.t.
    if used('_Curs__d__d__agen_exe_query_chkphone1')
      select _Curs__d__d__agen_exe_query_chkphone1
      locate for 1=1
      do while not(eof())
      m.w_VALID = not empty(NVL(DPCODICE,space(5)))
        select _Curs__d__d__agen_exe_query_chkphone1
        continue
      enddo
      use
    endif
  else
    * --- Select from ..\agen\exe\query\chkphone
    do vq_exec with '..\agen\exe\query\chkphone',createobject('cp_getfuncvar'),'_Curs__d__d__agen_exe_query_chkphone','',.f.,.t.
    if used('_Curs__d__d__agen_exe_query_chkphone')
      select _Curs__d__d__agen_exe_query_chkphone
      locate for 1=1
      do while not(eof())
      m.w_VALID = NVL(UTCODICE,0)<>0
        select _Curs__d__d__agen_exe_query_chkphone
        continue
      enddo
      use
    endif
  endif
  i_retcode = 'stop'
  i_retval = m.w_VALID
  return
endproc


* --- END CHKPHONE
* --- START ELABCURATT
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: elabcuratt                                                      *
*              Elabora cursore stampa attivit�                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-02-01                                                      *
* Last revis.: 2011-02-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func elabcuratt

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CNCODCAN
  m.w_CNCODCAN=space(15)
  private w_TIPO
  m.w_TIPO=space(1)
  private w_CONTA
  m.w_CONTA=0
  private w_DATA_ATT
  m.w_DATA_ATT=ctot("")
  private w_DATAUDIENZA
  m.w_DATAUDIENZA=ctot("")
  private w_UDIENZASERIAL
  m.w_UDIENZASERIAL=space(10)
* --- WorkFile variables
  private OFF_ATTI_idx
  OFF_ATTI_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "elabcuratt"
if vartype(__elabcuratt_hook__)='O'
  __elabcuratt_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'elabcuratt()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'elabcuratt')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if elabcuratt_OpenTables()
  elabcuratt_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_GSPR2ALU')
  use in _Curs_GSPR2ALU
endif
if used('_Curs__d__d__AGEN_EXE_QUERY_ULT_UDI')
  use in _Curs__d__d__AGEN_EXE_QUERY_ULT_UDI
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'elabcuratt()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'elabcuratt')
Endif
*--- Activity log
if vartype(__elabcuratt_hook__)='O'
  __elabcuratt_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure elabcuratt_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * ---                                       Elabora cursore di stampa attivit�
  m.w_CONTA = 0
  * --- Se in maschera � attivo il check Visualizza udienze
  if ISALT() AND Used(g_CURCURSOR) AND VISUDI="S"
    * --- Se l'attivit� � associata ad una pratica e se non � di tipo Udienza/Da inser.prestaz./Assenza/Nota
    if NOT EMPTY(ATCODPRA) AND NOT CARAGGST$"UAZM"
      m.w_DATA_ATT = &g_CurCursor..ATDATINI
      m.w_CNCODCAN = NVL( &g_CurCursor..ATCODPRA,"")
      do while m.w_CONTA<3 and Empty(m.w_UDIENZASERIAL)
        m.w_CONTA = m.w_CONTA +1
        m.w_TIPO = Icase(m.w_CONTA=1,"A",m.w_CONTA=2,"B","C")
        elabcuratt_Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      enddo
      SELECT (g_CurCursor)
      * --- Se la data della prossima udienza � successiva a quellq dell'attivit� in esame
      if NOT EMPTY(m.w_DATAUDIENZA) AND TTOD(m.w_DATAUDIENZA)>=TTOD(m.w_DATA_ATT)
        * --- Data della prossima udienza non evasa
        REPLACE PROUDI WITH m.w_DATAUDIENZA
      endif
      m.w_DATAUDIENZA = CTOT("  -  -  ")
      * --- Select from ..\AGEN\EXE\QUERY\ULT_UDI
      do vq_exec with '..\AGEN\EXE\QUERY\ULT_UDI',createobject('cp_getfuncvar'),'_Curs__d__d__AGEN_EXE_QUERY_ULT_UDI','',.f.,.t.
      if used('_Curs__d__d__AGEN_EXE_QUERY_ULT_UDI')
        select _Curs__d__d__AGEN_EXE_QUERY_ULT_UDI
        locate for 1=1
        do while not(eof())
        m.w_DATAUDIENZA = _Curs__d__d__AGEN_EXE_QUERY_ULT_UDI.ATDATINI
          select _Curs__d__d__AGEN_EXE_QUERY_ULT_UDI
          continue
        enddo
        use
      endif
      SELECT (g_CurCursor)
      * --- Se la data dell'ultima udienza � inferiore a quella dell'attivit� in esame
      if NOT EMPTY(m.w_DATAUDIENZA) AND TTOD(m.w_DATAUDIENZA)<TTOD(m.w_DATA_ATT)
        * --- Data dell'ultima udienza evasa/completata
        REPLACE ULTUDI WITH m.w_DATAUDIENZA
      endif
    endif
  endif
  i_retcode = 'stop'
  i_retval = .T.
  return
endproc


procedure elabcuratt_Pag2
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Select from GSPR2ALU
  do vq_exec with 'GSPR2ALU',createobject('cp_getfuncvar'),'_Curs_GSPR2ALU','',.f.,.t.
  if used('_Curs_GSPR2ALU')
    select _Curs_GSPR2ALU
    locate for 1=1
    do while not(eof())
    m.w_DATAUDIENZA = _Curs_GSPR2ALU.ATDATINI
    m.w_UDIENZASERIAL = _Curs_GSPR2ALU.ATSERIAL
    exit
      select _Curs_GSPR2ALU
      continue
    enddo
    use
  endif
endproc


  function elabcuratt_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='OFF_ATTI'
    return(cp_OpenFuncTables(1))
* --- END ELABCURATT
* --- START ACCEDIMASK
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: accedimask                                                      *
*              Controllo accesso maschera                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-02-04                                                      *
* Last revis.: 2013-09-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func accedimask

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_RET
  m.w_RET=.f.
  private w_PAFLVISI
  m.w_PAFLVISI=space(1)
* --- WorkFile variables
  private PAR_AGEN_idx
  PAR_AGEN_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "accedimask"
if vartype(__accedimask_hook__)='O'
  __accedimask_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'accedimask()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'accedimask')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if accedimask_OpenTables()
  accedimask_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'accedimask()', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'accedimask')
Endif
*--- Activity log
if vartype(__accedimask_hook__)='O'
  __accedimask_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure accedimask_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Controllo accesso maschera di Stampa attivit� multistudio
  m.w_RET = .F.
  * --- Legge dai Parametri Attivit� il flag Modalit� di visualizzazione
  * --- Read from PAR_AGEN
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[PAR_AGEN_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PAR_AGEN_idx,2],.t.,PAR_AGEN_idx)
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "PAFLVISI"+;
      " from "+i_cTable+" PAR_AGEN where ";
          +"PACODAZI = "+cp_ToStrODBC(i_codazi);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      PAFLVISI;
      from (i_cTable) where;
          PACODAZI = i_codazi;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_PAFLVISI = NVL(cp_ToDate(_read_.PAFLVISI),cp_NullValue(_read_.PAFLVISI))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  * --- Se la Modalit� di visualizzazione �  'libera'
  if m.w_PAFLVISI = "L"
    m.w_RET = .T.
  endif
  i_retcode = 'stop'
  i_retval = m.w_RET
  return
endproc


  function accedimask_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='PAR_AGEN'
    return(cp_OpenFuncTables(1))
* --- END ACCEDIMASK
