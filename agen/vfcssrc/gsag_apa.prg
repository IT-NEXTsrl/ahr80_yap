* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_apa                                                        *
*              Parametri agenda                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-11                                                      *
* Last revis.: 2015-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsag_apa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsag_apa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsag_apa")
  return

* --- Class definition
define class tgsag_apa as StdPCForm
  Width  = 701
  Height = 537+35
  Top    = 7
  Left   = 10
  cComment = "Parametri agenda"
  cPrg = "gsag_apa"
  HelpContextID=160049001
  add object cnt as tcgsag_apa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsag_apa as PCContext
  w_ATTIVACQ = space(10)
  w_PACODAZI = space(5)
  w_PERVAL = space(3)
  w_CHECK = space(1)
  w_MVCODVAL = space(3)
  w_MVFLVEAC = space(1)
  w_DATALIST = space(8)
  w_PACODLIS = space(5)
  w_PACODLIS = space(5)
  w_PALISACQ = space(5)
  w_RDESLIS = space(40)
  w_PANOFLAT = space(1)
  w_PADATATP = 0
  w_PADATATS = 0
  w_PAFLCOSE = space(1)
  w_PADACOSE = 0
  w_PA_ACOSE = 0
  w_PANOFLPR = space(1)
  w_PADATPRP = 0
  w_PADATPRS = 0
  w_PAGIOSCA = 0
  w_PAFLRDOC = space(1)
  w_PACHKFES = space(1)
  w_PACHKPRE = space(1)
  w_PACALEND = space(5)
  w_PACHKCAR = space(1)
  w_PAVISTRC = space(1)
  w_PATIPAPI = space(20)
  w_CARAGGST = space(1)
  w_PATIPAPF = space(20)
  w_PATIPCOS = space(20)
  w_PATIPNOT = space(20)
  w_PATIPTEL = space(20)
  w_PATIPASS = space(20)
  w_PATIPGEN = space(20)
  w_CATEG = space(1)
  w_PAFLVISI = space(1)
  w_PAFLANTI = space(1)
  w_OLDFLVISI = space(10)
  w_FLSCON = space(1)
  w_DESCAL = space(40)
  w_CICLO = space(1)
  w_DESACQ = space(40)
  w_EDESLIS = space(40)
  w_FLGCIC = space(1)
  w_TIPLIS = space(1)
  w_PAGESRIS = space(1)
  w_PADESRIS = space(254)
  w_PAST_ATT = space(1)
  w_PAOGGCRE = space(254)
  w_PATXTCRE = space(10)
  w_PAPSTCRE = space(10)
  w_PACHKCPL = space(1)
  w_PACHKING = space(1)
  w_PADATINI = space(8)
  w_PAGIOCHK = 0
  w_PACODMDC = space(3)
  w_PCCAUABB = space(5)
  w_PACALDAT = space(1)
  w_PACHKATT = 0
  w_AFLVEAC = space(1)
  w_DESCAU = space(35)
  w_TDFLVEAC = space(1)
  w_CAT_DOC = space(2)
  w_FLSCON2 = space(1)
  w_IVALIS = space(1)
  w_PAFLESAT = space(1)
  w_PAFLDTRP = space(1)
  w_PAATTRIS = space(20)
  w_PAFLSIAT = space(1)
  w_PAFLPRES = space(1)
  w_DTBSO_CAUMATTI = space(8)
  w_CACHKOBB = space(1)
  w_PACODMOD = space(20)
  w_DESCRI = space(40)
  w_PACODNUM = space(1)
  w_PCNOFLDC = space(1)
  w_PACODDOC = space(3)
  w_PCNOFLAT = space(1)
  w_PACODPER = space(3)
  w_PADESSUP = space(254)
  w_PCTIPDOC = space(5)
  w_PCTIPATT = space(20)
  w_PCGRUPAR = space(5)
  w_DESDOC = space(35)
  w_DESATT = space(254)
  w_PATIPRIS = space(1)
  w_CARAGGST = space(10)
  w_CATDOC = space(2)
  w_FLVEAC = space(1)
  w_TIPRIS = space(1)
  w_OB_TEST = space(8)
  w_DESGRU = space(60)
  w_PCESCRIN = space(1)
  w_PCPERCON = space(3)
  w_MDDESCRI = space(50)
  w_PCRINCON = space(1)
  w_PCNUMGIO = 0
  w_DESMDO = space(50)
  w_DESMAT = space(50)
  w_PACAUATT = space(20)
  w_CauDescri = space(254)
  w_PACAUATT = space(20)
  w_PACAUPRE = space(20)
  w_PATIPUDI = space(20)
  w_PACOSGIU = space(20)
  w_PASTRING = space(14)
  w_PATIPCTR = space(20)
  w_PANGGINC = 0
  w_PATIPTEL = space(20)
  w_PATIPASS = space(20)
  w_PATIPAPI = space(20)
  w_PATIPCOS = space(20)
  w_PATIPGEN = space(20)
  w_PATIPAPF = space(20)
  w_DES_CAU = space(254)
  w_DES_GEN = space(254)
  w_DES_TEL = space(254)
  w_DES_CHI = space(254)
  w_DES_API = space(254)
  w_DES_APF = space(254)
  w_DES_DOC = space(254)
  w_DESCOSGIU = space(254)
  w_TIPPOLU = space(1)
  w_PADESINC = space(254)
  w_PAOGGOUT = space(254)
  w_PAFLAPPU = space(1)
  w_PAFLATGE = space(1)
  w_PAFLUDIE = space(1)
  w_PAFLSEST = space(1)
  w_PAFLASSE = space(1)
  w_PAFLINPR = space(1)
  w_PAFLNOTE = space(1)
  w_PAFLDAFA = space(1)
  w_PAFLAPP1 = space(1)
  w_PAFLATG1 = space(1)
  w_PAFLUDI1 = space(1)
  w_PAFLSES1 = space(1)
  w_PAFLASS1 = space(1)
  w_PAFLINP1 = space(1)
  w_PAFLNOT1 = space(1)
  w_PAFLDAF1 = space(1)
  w_PAFLAPP2 = space(1)
  w_PAFLATG2 = space(1)
  w_PAFLUDI2 = space(1)
  w_PAFLSES2 = space(1)
  w_PAFLASS2 = space(1)
  w_PAFLINP2 = space(1)
  w_PAFLNOT2 = space(1)
  w_PAFLDAF2 = space(1)
  w_PAFLAPP3 = space(1)
  w_PAFLATG3 = space(1)
  w_PAFLUDI3 = space(1)
  w_PAFLSES3 = space(1)
  w_PAFLASS3 = space(1)
  w_PAFLINP3 = space(1)
  w_PAFLNOT3 = space(1)
  w_PAFLDAF3 = space(1)
  w_PAOGGMOD = space(254)
  w_PATXTMOD = space(10)
  w_PAPSTMOD = space(10)
  w_PAOGGDEL = space(254)
  w_PATXTDEL = space(10)
  w_PAPSTDEL = space(10)
  w_TDFLINTE = space(1)
  w_PAFLR_AG = space(1)
  w_PADATFIS = space(1)
  w_PADESATT = space(10)
  w_PADESDOC = space(10)
  proc Save(oFrom)
    this.w_ATTIVACQ = oFrom.w_ATTIVACQ
    this.w_PACODAZI = oFrom.w_PACODAZI
    this.w_PERVAL = oFrom.w_PERVAL
    this.w_CHECK = oFrom.w_CHECK
    this.w_MVCODVAL = oFrom.w_MVCODVAL
    this.w_MVFLVEAC = oFrom.w_MVFLVEAC
    this.w_DATALIST = oFrom.w_DATALIST
    this.w_PACODLIS = oFrom.w_PACODLIS
    this.w_PACODLIS = oFrom.w_PACODLIS
    this.w_PALISACQ = oFrom.w_PALISACQ
    this.w_RDESLIS = oFrom.w_RDESLIS
    this.w_PANOFLAT = oFrom.w_PANOFLAT
    this.w_PADATATP = oFrom.w_PADATATP
    this.w_PADATATS = oFrom.w_PADATATS
    this.w_PAFLCOSE = oFrom.w_PAFLCOSE
    this.w_PADACOSE = oFrom.w_PADACOSE
    this.w_PA_ACOSE = oFrom.w_PA_ACOSE
    this.w_PANOFLPR = oFrom.w_PANOFLPR
    this.w_PADATPRP = oFrom.w_PADATPRP
    this.w_PADATPRS = oFrom.w_PADATPRS
    this.w_PAGIOSCA = oFrom.w_PAGIOSCA
    this.w_PAFLRDOC = oFrom.w_PAFLRDOC
    this.w_PACHKFES = oFrom.w_PACHKFES
    this.w_PACHKPRE = oFrom.w_PACHKPRE
    this.w_PACALEND = oFrom.w_PACALEND
    this.w_PACHKCAR = oFrom.w_PACHKCAR
    this.w_PAVISTRC = oFrom.w_PAVISTRC
    this.w_PATIPAPI = oFrom.w_PATIPAPI
    this.w_CARAGGST = oFrom.w_CARAGGST
    this.w_PATIPAPF = oFrom.w_PATIPAPF
    this.w_PATIPCOS = oFrom.w_PATIPCOS
    this.w_PATIPNOT = oFrom.w_PATIPNOT
    this.w_PATIPTEL = oFrom.w_PATIPTEL
    this.w_PATIPASS = oFrom.w_PATIPASS
    this.w_PATIPGEN = oFrom.w_PATIPGEN
    this.w_CATEG = oFrom.w_CATEG
    this.w_PAFLVISI = oFrom.w_PAFLVISI
    this.w_PAFLANTI = oFrom.w_PAFLANTI
    this.w_OLDFLVISI = oFrom.w_OLDFLVISI
    this.w_FLSCON = oFrom.w_FLSCON
    this.w_DESCAL = oFrom.w_DESCAL
    this.w_CICLO = oFrom.w_CICLO
    this.w_DESACQ = oFrom.w_DESACQ
    this.w_EDESLIS = oFrom.w_EDESLIS
    this.w_FLGCIC = oFrom.w_FLGCIC
    this.w_TIPLIS = oFrom.w_TIPLIS
    this.w_PAGESRIS = oFrom.w_PAGESRIS
    this.w_PADESRIS = oFrom.w_PADESRIS
    this.w_PAST_ATT = oFrom.w_PAST_ATT
    this.w_PAOGGCRE = oFrom.w_PAOGGCRE
    this.w_PATXTCRE = oFrom.w_PATXTCRE
    this.w_PAPSTCRE = oFrom.w_PAPSTCRE
    this.w_PACHKCPL = oFrom.w_PACHKCPL
    this.w_PACHKING = oFrom.w_PACHKING
    this.w_PADATINI = oFrom.w_PADATINI
    this.w_PAGIOCHK = oFrom.w_PAGIOCHK
    this.w_PACODMDC = oFrom.w_PACODMDC
    this.w_PCCAUABB = oFrom.w_PCCAUABB
    this.w_PACALDAT = oFrom.w_PACALDAT
    this.w_PACHKATT = oFrom.w_PACHKATT
    this.w_AFLVEAC = oFrom.w_AFLVEAC
    this.w_DESCAU = oFrom.w_DESCAU
    this.w_TDFLVEAC = oFrom.w_TDFLVEAC
    this.w_CAT_DOC = oFrom.w_CAT_DOC
    this.w_FLSCON2 = oFrom.w_FLSCON2
    this.w_IVALIS = oFrom.w_IVALIS
    this.w_PAFLESAT = oFrom.w_PAFLESAT
    this.w_PAFLDTRP = oFrom.w_PAFLDTRP
    this.w_PAATTRIS = oFrom.w_PAATTRIS
    this.w_PAFLSIAT = oFrom.w_PAFLSIAT
    this.w_PAFLPRES = oFrom.w_PAFLPRES
    this.w_DTBSO_CAUMATTI = oFrom.w_DTBSO_CAUMATTI
    this.w_CACHKOBB = oFrom.w_CACHKOBB
    this.w_PACODMOD = oFrom.w_PACODMOD
    this.w_DESCRI = oFrom.w_DESCRI
    this.w_PACODNUM = oFrom.w_PACODNUM
    this.w_PCNOFLDC = oFrom.w_PCNOFLDC
    this.w_PACODDOC = oFrom.w_PACODDOC
    this.w_PCNOFLAT = oFrom.w_PCNOFLAT
    this.w_PACODPER = oFrom.w_PACODPER
    this.w_PADESSUP = oFrom.w_PADESSUP
    this.w_PCTIPDOC = oFrom.w_PCTIPDOC
    this.w_PCTIPATT = oFrom.w_PCTIPATT
    this.w_PCGRUPAR = oFrom.w_PCGRUPAR
    this.w_DESDOC = oFrom.w_DESDOC
    this.w_DESATT = oFrom.w_DESATT
    this.w_PATIPRIS = oFrom.w_PATIPRIS
    this.w_CARAGGST = oFrom.w_CARAGGST
    this.w_CATDOC = oFrom.w_CATDOC
    this.w_FLVEAC = oFrom.w_FLVEAC
    this.w_TIPRIS = oFrom.w_TIPRIS
    this.w_OB_TEST = oFrom.w_OB_TEST
    this.w_DESGRU = oFrom.w_DESGRU
    this.w_PCESCRIN = oFrom.w_PCESCRIN
    this.w_PCPERCON = oFrom.w_PCPERCON
    this.w_MDDESCRI = oFrom.w_MDDESCRI
    this.w_PCRINCON = oFrom.w_PCRINCON
    this.w_PCNUMGIO = oFrom.w_PCNUMGIO
    this.w_DESMDO = oFrom.w_DESMDO
    this.w_DESMAT = oFrom.w_DESMAT
    this.w_PACAUATT = oFrom.w_PACAUATT
    this.w_CauDescri = oFrom.w_CauDescri
    this.w_PACAUATT = oFrom.w_PACAUATT
    this.w_PACAUPRE = oFrom.w_PACAUPRE
    this.w_PATIPUDI = oFrom.w_PATIPUDI
    this.w_PACOSGIU = oFrom.w_PACOSGIU
    this.w_PASTRING = oFrom.w_PASTRING
    this.w_PATIPCTR = oFrom.w_PATIPCTR
    this.w_PANGGINC = oFrom.w_PANGGINC
    this.w_PATIPTEL = oFrom.w_PATIPTEL
    this.w_PATIPASS = oFrom.w_PATIPASS
    this.w_PATIPAPI = oFrom.w_PATIPAPI
    this.w_PATIPCOS = oFrom.w_PATIPCOS
    this.w_PATIPGEN = oFrom.w_PATIPGEN
    this.w_PATIPAPF = oFrom.w_PATIPAPF
    this.w_DES_CAU = oFrom.w_DES_CAU
    this.w_DES_GEN = oFrom.w_DES_GEN
    this.w_DES_TEL = oFrom.w_DES_TEL
    this.w_DES_CHI = oFrom.w_DES_CHI
    this.w_DES_API = oFrom.w_DES_API
    this.w_DES_APF = oFrom.w_DES_APF
    this.w_DES_DOC = oFrom.w_DES_DOC
    this.w_DESCOSGIU = oFrom.w_DESCOSGIU
    this.w_TIPPOLU = oFrom.w_TIPPOLU
    this.w_PADESINC = oFrom.w_PADESINC
    this.w_PAOGGOUT = oFrom.w_PAOGGOUT
    this.w_PAFLAPPU = oFrom.w_PAFLAPPU
    this.w_PAFLATGE = oFrom.w_PAFLATGE
    this.w_PAFLUDIE = oFrom.w_PAFLUDIE
    this.w_PAFLSEST = oFrom.w_PAFLSEST
    this.w_PAFLASSE = oFrom.w_PAFLASSE
    this.w_PAFLINPR = oFrom.w_PAFLINPR
    this.w_PAFLNOTE = oFrom.w_PAFLNOTE
    this.w_PAFLDAFA = oFrom.w_PAFLDAFA
    this.w_PAFLAPP1 = oFrom.w_PAFLAPP1
    this.w_PAFLATG1 = oFrom.w_PAFLATG1
    this.w_PAFLUDI1 = oFrom.w_PAFLUDI1
    this.w_PAFLSES1 = oFrom.w_PAFLSES1
    this.w_PAFLASS1 = oFrom.w_PAFLASS1
    this.w_PAFLINP1 = oFrom.w_PAFLINP1
    this.w_PAFLNOT1 = oFrom.w_PAFLNOT1
    this.w_PAFLDAF1 = oFrom.w_PAFLDAF1
    this.w_PAFLAPP2 = oFrom.w_PAFLAPP2
    this.w_PAFLATG2 = oFrom.w_PAFLATG2
    this.w_PAFLUDI2 = oFrom.w_PAFLUDI2
    this.w_PAFLSES2 = oFrom.w_PAFLSES2
    this.w_PAFLASS2 = oFrom.w_PAFLASS2
    this.w_PAFLINP2 = oFrom.w_PAFLINP2
    this.w_PAFLNOT2 = oFrom.w_PAFLNOT2
    this.w_PAFLDAF2 = oFrom.w_PAFLDAF2
    this.w_PAFLAPP3 = oFrom.w_PAFLAPP3
    this.w_PAFLATG3 = oFrom.w_PAFLATG3
    this.w_PAFLUDI3 = oFrom.w_PAFLUDI3
    this.w_PAFLSES3 = oFrom.w_PAFLSES3
    this.w_PAFLASS3 = oFrom.w_PAFLASS3
    this.w_PAFLINP3 = oFrom.w_PAFLINP3
    this.w_PAFLNOT3 = oFrom.w_PAFLNOT3
    this.w_PAFLDAF3 = oFrom.w_PAFLDAF3
    this.w_PAOGGMOD = oFrom.w_PAOGGMOD
    this.w_PATXTMOD = oFrom.w_PATXTMOD
    this.w_PAPSTMOD = oFrom.w_PAPSTMOD
    this.w_PAOGGDEL = oFrom.w_PAOGGDEL
    this.w_PATXTDEL = oFrom.w_PATXTDEL
    this.w_PAPSTDEL = oFrom.w_PAPSTDEL
    this.w_TDFLINTE = oFrom.w_TDFLINTE
    this.w_PAFLR_AG = oFrom.w_PAFLR_AG
    this.w_PADATFIS = oFrom.w_PADATFIS
    this.w_PADESATT = oFrom.w_PADESATT
    this.w_PADESDOC = oFrom.w_PADESDOC
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_ATTIVACQ = this.w_ATTIVACQ
    oTo.w_PACODAZI = this.w_PACODAZI
    oTo.w_PERVAL = this.w_PERVAL
    oTo.w_CHECK = this.w_CHECK
    oTo.w_MVCODVAL = this.w_MVCODVAL
    oTo.w_MVFLVEAC = this.w_MVFLVEAC
    oTo.w_DATALIST = this.w_DATALIST
    oTo.w_PACODLIS = this.w_PACODLIS
    oTo.w_PACODLIS = this.w_PACODLIS
    oTo.w_PALISACQ = this.w_PALISACQ
    oTo.w_RDESLIS = this.w_RDESLIS
    oTo.w_PANOFLAT = this.w_PANOFLAT
    oTo.w_PADATATP = this.w_PADATATP
    oTo.w_PADATATS = this.w_PADATATS
    oTo.w_PAFLCOSE = this.w_PAFLCOSE
    oTo.w_PADACOSE = this.w_PADACOSE
    oTo.w_PA_ACOSE = this.w_PA_ACOSE
    oTo.w_PANOFLPR = this.w_PANOFLPR
    oTo.w_PADATPRP = this.w_PADATPRP
    oTo.w_PADATPRS = this.w_PADATPRS
    oTo.w_PAGIOSCA = this.w_PAGIOSCA
    oTo.w_PAFLRDOC = this.w_PAFLRDOC
    oTo.w_PACHKFES = this.w_PACHKFES
    oTo.w_PACHKPRE = this.w_PACHKPRE
    oTo.w_PACALEND = this.w_PACALEND
    oTo.w_PACHKCAR = this.w_PACHKCAR
    oTo.w_PAVISTRC = this.w_PAVISTRC
    oTo.w_PATIPAPI = this.w_PATIPAPI
    oTo.w_CARAGGST = this.w_CARAGGST
    oTo.w_PATIPAPF = this.w_PATIPAPF
    oTo.w_PATIPCOS = this.w_PATIPCOS
    oTo.w_PATIPNOT = this.w_PATIPNOT
    oTo.w_PATIPTEL = this.w_PATIPTEL
    oTo.w_PATIPASS = this.w_PATIPASS
    oTo.w_PATIPGEN = this.w_PATIPGEN
    oTo.w_CATEG = this.w_CATEG
    oTo.w_PAFLVISI = this.w_PAFLVISI
    oTo.w_PAFLANTI = this.w_PAFLANTI
    oTo.w_OLDFLVISI = this.w_OLDFLVISI
    oTo.w_FLSCON = this.w_FLSCON
    oTo.w_DESCAL = this.w_DESCAL
    oTo.w_CICLO = this.w_CICLO
    oTo.w_DESACQ = this.w_DESACQ
    oTo.w_EDESLIS = this.w_EDESLIS
    oTo.w_FLGCIC = this.w_FLGCIC
    oTo.w_TIPLIS = this.w_TIPLIS
    oTo.w_PAGESRIS = this.w_PAGESRIS
    oTo.w_PADESRIS = this.w_PADESRIS
    oTo.w_PAST_ATT = this.w_PAST_ATT
    oTo.w_PAOGGCRE = this.w_PAOGGCRE
    oTo.w_PATXTCRE = this.w_PATXTCRE
    oTo.w_PAPSTCRE = this.w_PAPSTCRE
    oTo.w_PACHKCPL = this.w_PACHKCPL
    oTo.w_PACHKING = this.w_PACHKING
    oTo.w_PADATINI = this.w_PADATINI
    oTo.w_PAGIOCHK = this.w_PAGIOCHK
    oTo.w_PACODMDC = this.w_PACODMDC
    oTo.w_PCCAUABB = this.w_PCCAUABB
    oTo.w_PACALDAT = this.w_PACALDAT
    oTo.w_PACHKATT = this.w_PACHKATT
    oTo.w_AFLVEAC = this.w_AFLVEAC
    oTo.w_DESCAU = this.w_DESCAU
    oTo.w_TDFLVEAC = this.w_TDFLVEAC
    oTo.w_CAT_DOC = this.w_CAT_DOC
    oTo.w_FLSCON2 = this.w_FLSCON2
    oTo.w_IVALIS = this.w_IVALIS
    oTo.w_PAFLESAT = this.w_PAFLESAT
    oTo.w_PAFLDTRP = this.w_PAFLDTRP
    oTo.w_PAATTRIS = this.w_PAATTRIS
    oTo.w_PAFLSIAT = this.w_PAFLSIAT
    oTo.w_PAFLPRES = this.w_PAFLPRES
    oTo.w_DTBSO_CAUMATTI = this.w_DTBSO_CAUMATTI
    oTo.w_CACHKOBB = this.w_CACHKOBB
    oTo.w_PACODMOD = this.w_PACODMOD
    oTo.w_DESCRI = this.w_DESCRI
    oTo.w_PACODNUM = this.w_PACODNUM
    oTo.w_PCNOFLDC = this.w_PCNOFLDC
    oTo.w_PACODDOC = this.w_PACODDOC
    oTo.w_PCNOFLAT = this.w_PCNOFLAT
    oTo.w_PACODPER = this.w_PACODPER
    oTo.w_PADESSUP = this.w_PADESSUP
    oTo.w_PCTIPDOC = this.w_PCTIPDOC
    oTo.w_PCTIPATT = this.w_PCTIPATT
    oTo.w_PCGRUPAR = this.w_PCGRUPAR
    oTo.w_DESDOC = this.w_DESDOC
    oTo.w_DESATT = this.w_DESATT
    oTo.w_PATIPRIS = this.w_PATIPRIS
    oTo.w_CARAGGST = this.w_CARAGGST
    oTo.w_CATDOC = this.w_CATDOC
    oTo.w_FLVEAC = this.w_FLVEAC
    oTo.w_TIPRIS = this.w_TIPRIS
    oTo.w_OB_TEST = this.w_OB_TEST
    oTo.w_DESGRU = this.w_DESGRU
    oTo.w_PCESCRIN = this.w_PCESCRIN
    oTo.w_PCPERCON = this.w_PCPERCON
    oTo.w_MDDESCRI = this.w_MDDESCRI
    oTo.w_PCRINCON = this.w_PCRINCON
    oTo.w_PCNUMGIO = this.w_PCNUMGIO
    oTo.w_DESMDO = this.w_DESMDO
    oTo.w_DESMAT = this.w_DESMAT
    oTo.w_PACAUATT = this.w_PACAUATT
    oTo.w_CauDescri = this.w_CauDescri
    oTo.w_PACAUATT = this.w_PACAUATT
    oTo.w_PACAUPRE = this.w_PACAUPRE
    oTo.w_PATIPUDI = this.w_PATIPUDI
    oTo.w_PACOSGIU = this.w_PACOSGIU
    oTo.w_PASTRING = this.w_PASTRING
    oTo.w_PATIPCTR = this.w_PATIPCTR
    oTo.w_PANGGINC = this.w_PANGGINC
    oTo.w_PATIPTEL = this.w_PATIPTEL
    oTo.w_PATIPASS = this.w_PATIPASS
    oTo.w_PATIPAPI = this.w_PATIPAPI
    oTo.w_PATIPCOS = this.w_PATIPCOS
    oTo.w_PATIPGEN = this.w_PATIPGEN
    oTo.w_PATIPAPF = this.w_PATIPAPF
    oTo.w_DES_CAU = this.w_DES_CAU
    oTo.w_DES_GEN = this.w_DES_GEN
    oTo.w_DES_TEL = this.w_DES_TEL
    oTo.w_DES_CHI = this.w_DES_CHI
    oTo.w_DES_API = this.w_DES_API
    oTo.w_DES_APF = this.w_DES_APF
    oTo.w_DES_DOC = this.w_DES_DOC
    oTo.w_DESCOSGIU = this.w_DESCOSGIU
    oTo.w_TIPPOLU = this.w_TIPPOLU
    oTo.w_PADESINC = this.w_PADESINC
    oTo.w_PAOGGOUT = this.w_PAOGGOUT
    oTo.w_PAFLAPPU = this.w_PAFLAPPU
    oTo.w_PAFLATGE = this.w_PAFLATGE
    oTo.w_PAFLUDIE = this.w_PAFLUDIE
    oTo.w_PAFLSEST = this.w_PAFLSEST
    oTo.w_PAFLASSE = this.w_PAFLASSE
    oTo.w_PAFLINPR = this.w_PAFLINPR
    oTo.w_PAFLNOTE = this.w_PAFLNOTE
    oTo.w_PAFLDAFA = this.w_PAFLDAFA
    oTo.w_PAFLAPP1 = this.w_PAFLAPP1
    oTo.w_PAFLATG1 = this.w_PAFLATG1
    oTo.w_PAFLUDI1 = this.w_PAFLUDI1
    oTo.w_PAFLSES1 = this.w_PAFLSES1
    oTo.w_PAFLASS1 = this.w_PAFLASS1
    oTo.w_PAFLINP1 = this.w_PAFLINP1
    oTo.w_PAFLNOT1 = this.w_PAFLNOT1
    oTo.w_PAFLDAF1 = this.w_PAFLDAF1
    oTo.w_PAFLAPP2 = this.w_PAFLAPP2
    oTo.w_PAFLATG2 = this.w_PAFLATG2
    oTo.w_PAFLUDI2 = this.w_PAFLUDI2
    oTo.w_PAFLSES2 = this.w_PAFLSES2
    oTo.w_PAFLASS2 = this.w_PAFLASS2
    oTo.w_PAFLINP2 = this.w_PAFLINP2
    oTo.w_PAFLNOT2 = this.w_PAFLNOT2
    oTo.w_PAFLDAF2 = this.w_PAFLDAF2
    oTo.w_PAFLAPP3 = this.w_PAFLAPP3
    oTo.w_PAFLATG3 = this.w_PAFLATG3
    oTo.w_PAFLUDI3 = this.w_PAFLUDI3
    oTo.w_PAFLSES3 = this.w_PAFLSES3
    oTo.w_PAFLASS3 = this.w_PAFLASS3
    oTo.w_PAFLINP3 = this.w_PAFLINP3
    oTo.w_PAFLNOT3 = this.w_PAFLNOT3
    oTo.w_PAFLDAF3 = this.w_PAFLDAF3
    oTo.w_PAOGGMOD = this.w_PAOGGMOD
    oTo.w_PATXTMOD = this.w_PATXTMOD
    oTo.w_PAPSTMOD = this.w_PAPSTMOD
    oTo.w_PAOGGDEL = this.w_PAOGGDEL
    oTo.w_PATXTDEL = this.w_PATXTDEL
    oTo.w_PAPSTDEL = this.w_PAPSTDEL
    oTo.w_TDFLINTE = this.w_TDFLINTE
    oTo.w_PAFLR_AG = this.w_PAFLR_AG
    oTo.w_PADATFIS = this.w_PADATFIS
    oTo.w_PADESATT = this.w_PADESATT
    oTo.w_PADESDOC = this.w_PADESDOC
    PCContext::Load(oTo)
enddefine

define class tcgsag_apa as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 701
  Height = 537+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-30"
  HelpContextID=160049001
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=169

  * --- Constant Properties
  PAR_AGEN_IDX = 0
  LISTINI_IDX = 0
  AZIENDA_IDX = 0
  CAUMATTI_IDX = 0
  MODMATTR_IDX = 0
  TAB_CALE_IDX = 0
  MODCLDAT_IDX = 0
  TIP_DOCU_IDX = 0
  DIPENDEN_IDX = 0
  cFile = "PAR_AGEN"
  cKeySelect = "PACODAZI"
  cKeyWhere  = "PACODAZI=this.w_PACODAZI"
  cKeyWhereODBC = '"PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';

  cKeyWhereODBCqualified = '"PAR_AGEN.PACODAZI="+cp_ToStrODBC(this.w_PACODAZI)';

  cPrg = "gsag_apa"
  cComment = "Parametri agenda"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ATTIVACQ = space(10)
  w_PACODAZI = space(5)
  o_PACODAZI = space(5)
  w_PERVAL = space(3)
  w_CHECK = .F.
  w_MVCODVAL = space(3)
  w_MVFLVEAC = space(1)
  w_DATALIST = ctod('  /  /  ')
  w_PACODLIS = space(5)
  w_PACODLIS = space(5)
  w_PALISACQ = space(5)
  w_RDESLIS = space(40)
  w_PANOFLAT = space(1)
  o_PANOFLAT = space(1)
  w_PADATATP = 0
  w_PADATATS = 0
  w_PAFLCOSE = space(1)
  o_PAFLCOSE = space(1)
  w_PADACOSE = 0
  w_PA_ACOSE = 0
  w_PANOFLPR = space(1)
  o_PANOFLPR = space(1)
  w_PADATPRP = 0
  w_PADATPRS = 0
  w_PAGIOSCA = 0
  w_PAFLRDOC = space(1)
  w_PACHKFES = space(1)
  o_PACHKFES = space(1)
  w_PACHKPRE = space(1)
  w_PACALEND = space(5)
  w_PACHKCAR = space(1)
  w_PAVISTRC = space(1)
  w_PATIPAPI = space(20)
  w_CARAGGST = space(1)
  w_PATIPAPF = space(20)
  w_PATIPCOS = space(20)
  w_PATIPNOT = space(20)
  w_PATIPTEL = space(20)
  w_PATIPASS = space(20)
  w_PATIPGEN = space(20)
  w_CATEG = space(1)
  w_PAFLVISI = space(1)
  w_PAFLANTI = space(1)
  w_OLDFLVISI = space(10)
  w_FLSCON = space(1)
  w_DESCAL = space(40)
  w_CICLO = space(1)
  w_DESACQ = space(40)
  w_EDESLIS = space(40)
  w_FLGCIC = space(1)
  w_TIPLIS = space(1)
  w_PAGESRIS = space(1)
  o_PAGESRIS = space(1)
  w_PADESRIS = space(254)
  w_PAST_ATT = space(1)
  w_PAOGGCRE = space(254)
  w_PATXTCRE = space(0)
  w_PAPSTCRE = space(0)
  w_PACHKCPL = space(1)
  w_PACHKING = space(1)
  o_PACHKING = space(1)
  w_PADATINI = ctod('  /  /  ')
  w_PAGIOCHK = 0
  w_PACODMDC = space(3)
  w_PCCAUABB = space(5)
  w_PACALDAT = space(1)
  w_PACHKATT = 0
  w_AFLVEAC = space(1)
  w_DESCAU = space(35)
  w_TDFLVEAC = space(1)
  w_CAT_DOC = space(2)
  w_FLSCON2 = space(1)
  w_IVALIS = space(1)
  w_PAFLESAT = space(1)
  o_PAFLESAT = space(1)
  w_PAFLDTRP = space(1)
  w_PAATTRIS = space(20)
  w_PAFLSIAT = space(1)
  w_PAFLPRES = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_CACHKOBB = space(1)
  w_PACODMOD = space(20)
  w_DESCRI = space(40)
  w_PACODNUM = space(1)
  w_PCNOFLDC = space(1)
  w_PACODDOC = space(3)
  o_PACODDOC = space(3)
  w_PCNOFLAT = space(1)
  w_PACODPER = space(3)
  o_PACODPER = space(3)
  w_PADESSUP = space(254)
  w_PCTIPDOC = space(5)
  w_PCTIPATT = space(20)
  w_PCGRUPAR = space(5)
  w_DESDOC = space(35)
  w_DESATT = space(254)
  w_PATIPRIS = space(1)
  w_CARAGGST = space(10)
  w_CATDOC = space(2)
  w_FLVEAC = space(1)
  w_TIPRIS = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_DESGRU = space(60)
  w_PCESCRIN = space(1)
  w_PCPERCON = space(3)
  w_MDDESCRI = space(50)
  w_PCRINCON = space(1)
  w_PCNUMGIO = 0
  w_DESMDO = space(50)
  w_DESMAT = space(50)
  w_PACAUATT = space(20)
  w_CauDescri = space(254)
  w_PACAUATT = space(20)
  w_PACAUPRE = space(20)
  w_PATIPUDI = space(20)
  w_PACOSGIU = space(20)
  o_PACOSGIU = space(20)
  w_PASTRING = space(14)
  w_PATIPCTR = space(20)
  o_PATIPCTR = space(20)
  w_PANGGINC = 0
  w_PATIPTEL = space(20)
  w_PATIPASS = space(20)
  w_PATIPAPI = space(20)
  w_PATIPCOS = space(20)
  w_PATIPGEN = space(20)
  w_PATIPAPF = space(20)
  w_DES_CAU = space(254)
  w_DES_GEN = space(254)
  w_DES_TEL = space(254)
  w_DES_CHI = space(254)
  w_DES_API = space(254)
  w_DES_APF = space(254)
  w_DES_DOC = space(254)
  w_DESCOSGIU = space(254)
  w_TIPPOLU = space(1)
  w_PADESINC = space(254)
  w_PAOGGOUT = space(254)
  w_PAFLAPPU = space(1)
  w_PAFLATGE = space(1)
  w_PAFLUDIE = space(1)
  w_PAFLSEST = space(1)
  w_PAFLASSE = space(1)
  w_PAFLINPR = space(1)
  w_PAFLNOTE = space(1)
  w_PAFLDAFA = space(1)
  w_PAFLAPP1 = space(1)
  w_PAFLATG1 = space(1)
  w_PAFLUDI1 = space(1)
  w_PAFLSES1 = space(1)
  w_PAFLASS1 = space(1)
  w_PAFLINP1 = space(1)
  w_PAFLNOT1 = space(1)
  w_PAFLDAF1 = space(1)
  w_PAFLAPP2 = space(1)
  w_PAFLATG2 = space(1)
  w_PAFLUDI2 = space(1)
  w_PAFLSES2 = space(1)
  w_PAFLASS2 = space(1)
  w_PAFLINP2 = space(1)
  w_PAFLNOT2 = space(1)
  w_PAFLDAF2 = space(1)
  w_PAFLAPP3 = space(1)
  w_PAFLATG3 = space(1)
  w_PAFLUDI3 = space(1)
  w_PAFLSES3 = space(1)
  w_PAFLASS3 = space(1)
  w_PAFLINP3 = space(1)
  w_PAFLNOT3 = space(1)
  w_PAFLDAF3 = space(1)
  w_PAOGGMOD = space(254)
  w_PATXTMOD = space(0)
  w_PAPSTMOD = space(0)
  w_PAOGGDEL = space(254)
  w_PATXTDEL = space(0)
  w_PAPSTDEL = space(0)
  w_TDFLINTE = space(1)
  w_PAFLR_AG = space(1)
  w_PADATFIS = space(1)
  w_PADESATT = space(0)
  w_PADESDOC = space(0)

  * --- Children pointers
  gsag_mat = .NULL.
  GSAG_MCD = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=7, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_apaPag1","gsag_apa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).HelpContextID = 16637237
      .Pages(2).addobject("oPag","tgsag_apaPag2","gsag_apa",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Avvisi creazione")
      .Pages(2).HelpContextID = 220671982
      .Pages(3).addobject("oPag","tgsag_apaPag3","gsag_apa",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Avvisi modifica")
      .Pages(3).HelpContextID = 18067043
      .Pages(4).addobject("oPag","tgsag_apaPag4","gsag_apa",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Avvisi cancellazione")
      .Pages(4).HelpContextID = 179219843
      .Pages(5).addobject("oPag","tgsag_apaPag5","gsag_apa",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Selezione categorie")
      .Pages(5).HelpContextID = 198174093
      .Pages(6).addobject("oPag","tgsag_apaPag6","gsag_apa",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Tipi attivit�")
      .Pages(6).HelpContextID = 140596282
      .Pages(7).addobject("oPag","tgsag_apaPag7","gsag_apa",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Impianti e contratti")
      .Pages(7).HelpContextID = 256086927
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPACODLIS_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsag_apa
    * --- nMaxFieldsJoin di default vale 200
    this.parent.nMaxFieldsJoin = iif(lower(i_ServerConn[1,6])="oracle", 150, this.parent.nMaxFieldsJoin)
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='LISTINI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='CAUMATTI'
    this.cWorkTables[4]='MODMATTR'
    this.cWorkTables[5]='TAB_CALE'
    this.cWorkTables[6]='MODCLDAT'
    this.cWorkTables[7]='TIP_DOCU'
    this.cWorkTables[8]='DIPENDEN'
    this.cWorkTables[9]='PAR_AGEN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_AGEN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_AGEN_IDX,3]
  return

  function CreateChildren()
    this.gsag_mat = CREATEOBJECT('stdDynamicChild',this,'gsag_mat',this.oPgFrm.Page6.oPag.oLinkPC_6_11)
    this.GSAG_MCD = CREATEOBJECT('stdDynamicChild',this,'GSAG_MCD',this.oPgFrm.Page6.oPag.oLinkPC_6_33)
    return

  procedure NewContext()
    return(createobject('tsgsag_apa'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.gsag_mat)
      this.gsag_mat.DestroyChildrenChain()
    endif
    if !ISNULL(this.GSAG_MCD)
      this.GSAG_MCD.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.gsag_mat.HideChildrenChain()
    this.GSAG_MCD.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.gsag_mat.ShowChildrenChain()
    this.GSAG_MCD.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.gsag_mat)
      this.gsag_mat.DestroyChildrenChain()
      this.gsag_mat=.NULL.
    endif
    this.oPgFrm.Page6.oPag.RemoveObject('oLinkPC_6_11')
    if !ISNULL(this.GSAG_MCD)
      this.GSAG_MCD.DestroyChildrenChain()
      this.GSAG_MCD=.NULL.
    endif
    this.oPgFrm.Page6.oPag.RemoveObject('oLinkPC_6_33')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gsag_mat.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAG_MCD.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gsag_mat.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAG_MCD.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gsag_mat.NewDocument()
    this.GSAG_MCD.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.gsag_mat.SetKey(;
            .w_PACODAZI,"PTCODAZI";
            )
      this.GSAG_MCD.SetKey(;
            .w_PACODAZI,"CDCODAZI";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .gsag_mat.ChangeRow(this.cRowID+'      1',1;
             ,.w_PACODAZI,"PTCODAZI";
             )
      .GSAG_MCD.ChangeRow(this.cRowID+'      1',1;
             ,.w_PACODAZI,"CDCODAZI";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.gsag_mat)
        i_f=.gsag_mat.BuildFilter()
        if !(i_f==.gsag_mat.cQueryFilter)
          i_fnidx=.gsag_mat.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.gsag_mat.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.gsag_mat.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.gsag_mat.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.gsag_mat.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAG_MCD)
        i_f=.GSAG_MCD.BuildFilter()
        if !(i_f==.GSAG_MCD.cQueryFilter)
          i_fnidx=.GSAG_MCD.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAG_MCD.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAG_MCD.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAG_MCD.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAG_MCD.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    local link_1_75_joined
    link_1_75_joined=.f.
    local link_7_4_joined
    link_7_4_joined=.f.
    local link_7_9_joined
    link_7_9_joined=.f.
    local link_7_11_joined
    link_7_11_joined=.f.
    local link_7_13_joined
    link_7_13_joined=.f.
    local link_7_15_joined
    link_7_15_joined=.f.
    local link_7_17_joined
    link_7_17_joined=.f.
    local link_7_38_joined
    link_7_38_joined=.f.
    local link_6_1_joined
    link_6_1_joined=.f.
    local link_6_4_joined
    link_6_4_joined=.f.
    local link_6_6_joined
    link_6_6_joined=.f.
    local link_6_7_joined
    link_6_7_joined=.f.
    local link_6_9_joined
    link_6_9_joined=.f.
    local link_6_12_joined
    link_6_12_joined=.f.
    local link_6_13_joined
    link_6_13_joined=.f.
    local link_6_14_joined
    link_6_14_joined=.f.
    local link_6_15_joined
    link_6_15_joined=.f.
    local link_6_16_joined
    link_6_16_joined=.f.
    local link_6_17_joined
    link_6_17_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAR_AGEN where PACODAZI=KeySet.PACODAZI
    *
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_AGEN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_AGEN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_AGEN '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_75_joined=this.AddJoinedLink_1_75(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_7_4_joined=this.AddJoinedLink_7_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_7_9_joined=this.AddJoinedLink_7_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_7_11_joined=this.AddJoinedLink_7_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_7_13_joined=this.AddJoinedLink_7_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_7_15_joined=this.AddJoinedLink_7_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_7_17_joined=this.AddJoinedLink_7_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_7_38_joined=this.AddJoinedLink_7_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_1_joined=this.AddJoinedLink_6_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_4_joined=this.AddJoinedLink_6_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_6_joined=this.AddJoinedLink_6_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_7_joined=this.AddJoinedLink_6_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_9_joined=this.AddJoinedLink_6_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_12_joined=this.AddJoinedLink_6_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_13_joined=this.AddJoinedLink_6_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_14_joined=this.AddJoinedLink_6_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_15_joined=this.AddJoinedLink_6_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_16_joined=this.AddJoinedLink_6_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_17_joined=this.AddJoinedLink_6_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PERVAL = g_PERVAL
        .w_CHECK = .T.
        .w_MVFLVEAC = space(1)
        .w_RDESLIS = space(40)
        .w_CARAGGST = space(1)
        .w_CATEG = 'Z'
        .w_FLSCON = space(1)
        .w_DESCAL = space(40)
        .w_CICLO = 'E'
        .w_DESACQ = space(40)
        .w_EDESLIS = space(40)
        .w_FLGCIC = space(1)
        .w_TIPLIS = space(1)
        .w_AFLVEAC = space(1)
        .w_DESCAU = space(35)
        .w_TDFLVEAC = 'V'
        .w_CAT_DOC = space(2)
        .w_FLSCON2 = space(1)
        .w_IVALIS = space(1)
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_CACHKOBB = space(1)
        .w_DESCRI = space(40)
        .w_DESDOC = space(35)
        .w_DESATT = space(254)
        .w_PATIPRIS = 'G'
        .w_CARAGGST = space(10)
        .w_CATDOC = space(2)
        .w_FLVEAC = space(1)
        .w_TIPRIS = space(1)
        .w_OB_TEST = I_DATSYS
        .w_DESGRU = space(60)
        .w_MDDESCRI = space(50)
        .w_DESMDO = space(50)
        .w_DESMAT = space(50)
        .w_CauDescri = space(254)
        .w_DES_CAU = space(254)
        .w_DES_GEN = space(254)
        .w_DES_TEL = space(254)
        .w_DES_CHI = space(254)
        .w_DES_API = space(254)
        .w_DES_APF = space(254)
        .w_DES_DOC = space(254)
        .w_DESCOSGIU = space(254)
        .w_TIPPOLU = space(1)
        .w_PADESINC = space(254)
        .w_TDFLINTE = space(1)
        .w_ATTIVACQ = iif((Isahe() and g_CACQ='S') or (Isahr() and g_ACQU='S'),'S','N')
        .w_PACODAZI = NVL(PACODAZI,space(5))
          * evitabile
          *.link_1_2('Load')
        .w_MVCODVAL = g_PERVAL
        .w_DATALIST = i_DATSYS
        .w_PACODLIS = NVL(PACODLIS,space(5))
          .link_1_8('Load')
        .w_PACODLIS = NVL(PACODLIS,space(5))
          if link_1_10_joined
            this.w_PACODLIS = NVL(LSCODLIS110,NVL(this.w_PACODLIS,space(5)))
            this.w_EDESLIS = NVL(LSDESLIS110,space(40))
            this.w_FLGCIC = NVL(LSFLGCIC110,space(1))
            this.w_TIPLIS = NVL(LSTIPLIS110,space(1))
            this.w_FLSCON = NVL(LSFLSCON110,space(1))
          else
          .link_1_10('Load')
          endif
        .w_PALISACQ = NVL(PALISACQ,space(5))
          if link_1_11_joined
            this.w_PALISACQ = NVL(LSCODLIS111,NVL(this.w_PALISACQ,space(5)))
            this.w_DESACQ = NVL(LSDESLIS111,space(40))
            this.w_CICLO = NVL(LSFLGCIC111,space(1))
            this.w_TIPLIS = NVL(LSTIPLIS111,space(1))
            this.w_FLSCON2 = NVL(LSFLSCON111,space(1))
            this.w_IVALIS = NVL(LSIVALIS111,space(1))
          else
          .link_1_11('Load')
          endif
        .w_PANOFLAT = NVL(PANOFLAT,space(1))
        .w_PADATATP = NVL(PADATATP,0)
        .w_PADATATS = NVL(PADATATS,0)
        .w_PAFLCOSE = NVL(PAFLCOSE,space(1))
        .w_PADACOSE = NVL(PADACOSE,0)
        .w_PA_ACOSE = NVL(PA_ACOSE,0)
        .w_PANOFLPR = NVL(PANOFLPR,space(1))
        .w_PADATPRP = NVL(PADATPRP,0)
        .w_PADATPRS = NVL(PADATPRS,0)
        .w_PAGIOSCA = NVL(PAGIOSCA,0)
        .w_PAFLRDOC = NVL(PAFLRDOC,space(1))
        .w_PACHKFES = NVL(PACHKFES,space(1))
        .w_PACHKPRE = NVL(PACHKPRE,space(1))
        .w_PACALEND = NVL(PACALEND,space(5))
          if link_1_26_joined
            this.w_PACALEND = NVL(TCCODICE126,NVL(this.w_PACALEND,space(5)))
            this.w_DESCAL = NVL(TCDESCRI126,space(40))
          else
          .link_1_26('Load')
          endif
        .w_PACHKCAR = NVL(PACHKCAR,space(1))
        .w_PAVISTRC = NVL(PAVISTRC,space(1))
        .w_PATIPAPI = NVL(PATIPAPI,space(20))
        .w_PATIPAPF = NVL(PATIPAPF,space(20))
        .w_PATIPCOS = NVL(PATIPCOS,space(20))
        .w_PATIPNOT = NVL(PATIPNOT,space(20))
        .w_PATIPTEL = NVL(PATIPTEL,space(20))
        .w_PATIPASS = NVL(PATIPASS,space(20))
        .w_PATIPGEN = NVL(PATIPGEN,space(20))
        .w_PAFLVISI = NVL(PAFLVISI,space(1))
        .w_PAFLANTI = NVL(PAFLANTI,space(1))
        .w_OLDFLVISI = .w_PAFLVISI
        .w_PAGESRIS = NVL(PAGESRIS,space(1))
        .w_PADESRIS = NVL(PADESRIS,space(254))
        .w_PAST_ATT = NVL(PAST_ATT,space(1))
        .w_PAOGGCRE = NVL(PAOGGCRE,space(254))
        .w_PATXTCRE = NVL(PATXTCRE,space(0))
        .w_PAPSTCRE = NVL(PAPSTCRE,space(0))
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page2.oPag.oObj_2_7.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .w_PACHKCPL = NVL(PACHKCPL,space(1))
        .w_PACHKING = NVL(PACHKING,space(1))
        .w_PADATINI = NVL(cp_ToDate(PADATINI),ctod("  /  /  "))
        .w_PAGIOCHK = NVL(PAGIOCHK,0)
        .w_PACODMDC = NVL(PACODMDC,space(3))
          * evitabile
          *.link_1_74('Load')
        .w_PCCAUABB = NVL(PCCAUABB,space(5))
          if link_1_75_joined
            this.w_PCCAUABB = NVL(TDTIPDOC175,NVL(this.w_PCCAUABB,space(5)))
            this.w_DESCAU = NVL(TDDESDOC175,space(35))
            this.w_AFLVEAC = NVL(TDFLVEAC175,space(1))
            this.w_TDFLINTE = NVL(TDFLINTE175,space(1))
          else
          .link_1_75('Load')
          endif
        .w_PACALDAT = NVL(PACALDAT,space(1))
        .w_PACHKATT = NVL(PACHKATT,0)
        .w_PAFLESAT = NVL(PAFLESAT,space(1))
        .w_PAFLDTRP = NVL(PAFLDTRP,space(1))
        .w_PAATTRIS = NVL(PAATTRIS,space(20))
          * evitabile
          *.link_1_92('Load')
        .w_PAFLSIAT = NVL(PAFLSIAT,space(1))
        .w_PAFLPRES = NVL(PAFLPRES,space(1))
        .oPgFrm.Page2.oPag.oObj_2_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .w_PACODMOD = NVL(PACODMOD,space(20))
          if link_7_4_joined
            this.w_PACODMOD = NVL(MACODICE704,NVL(this.w_PACODMOD,space(20)))
            this.w_DESCRI = NVL(MADESCRI704,space(40))
          else
          .link_7_4('Load')
          endif
        .w_PACODNUM = NVL(PACODNUM,space(1))
        .w_PCNOFLDC = NVL(PCNOFLDC,space(1))
        .w_PACODDOC = NVL(PACODDOC,space(3))
          if link_7_9_joined
            this.w_PACODDOC = NVL(MDCODICE709,NVL(this.w_PACODDOC,space(3)))
            this.w_DESMDO = NVL(MDDESCRI709,space(50))
          else
          .link_7_9('Load')
          endif
        .w_PCNOFLAT = NVL(PCNOFLAT,space(1))
        .w_PACODPER = NVL(PACODPER,space(3))
          if link_7_11_joined
            this.w_PACODPER = NVL(MDCODICE711,NVL(this.w_PACODPER,space(3)))
            this.w_DESMAT = NVL(MDDESCRI711,space(50))
          else
          .link_7_11('Load')
          endif
        .w_PADESSUP = NVL(PADESSUP,space(254))
        .w_PCTIPDOC = NVL(PCTIPDOC,space(5))
          if link_7_13_joined
            this.w_PCTIPDOC = NVL(TDTIPDOC713,NVL(this.w_PCTIPDOC,space(5)))
            this.w_DESDOC = NVL(TDDESDOC713,space(35))
            this.w_CATDOC = NVL(TDCATDOC713,space(2))
            this.w_FLVEAC = NVL(TDFLVEAC713,space(1))
          else
          .link_7_13('Load')
          endif
        .w_PCTIPATT = NVL(PCTIPATT,space(20))
          if link_7_15_joined
            this.w_PCTIPATT = NVL(CACODICE715,NVL(this.w_PCTIPATT,space(20)))
            this.w_DESATT = NVL(CADESCRI715,space(254))
            this.w_CARAGGST = NVL(CARAGGST715,space(10))
          else
          .link_7_15('Load')
          endif
        .w_PCGRUPAR = NVL(PCGRUPAR,space(5))
          if link_7_17_joined
            this.w_PCGRUPAR = NVL(DPCODICE717,NVL(this.w_PCGRUPAR,space(5)))
            this.w_TIPRIS = NVL(DPTIPRIS717,space(1))
            this.w_DESGRU = NVL(DPDESCRI717,space(60))
          else
          .link_7_17('Load')
          endif
        .w_PCESCRIN = NVL(PCESCRIN,space(1))
        .w_PCPERCON = NVL(PCPERCON,space(3))
          if link_7_38_joined
            this.w_PCPERCON = NVL(MDCODICE738,NVL(this.w_PCPERCON,space(3)))
            this.w_MDDESCRI = NVL(MDDESCRI738,space(50))
          else
          .link_7_38('Load')
          endif
        .w_PCRINCON = NVL(PCRINCON,space(1))
        .w_PCNUMGIO = NVL(PCNUMGIO,0)
        .w_PACAUATT = NVL(PACAUATT,space(20))
          if link_6_1_joined
            this.w_PACAUATT = NVL(CACODICE601,NVL(this.w_PACAUATT,space(20)))
            this.w_CauDescri = NVL(CADESCRI601,space(254))
            this.w_CACHKOBB = NVL(CACHKOBB601,space(1))
          else
          .link_6_1('Load')
          endif
        .w_PACAUATT = NVL(PACAUATT,space(20))
          if link_6_4_joined
            this.w_PACAUATT = NVL(CACODICE604,NVL(this.w_PACAUATT,space(20)))
            this.w_CauDescri = NVL(CADESCRI604,space(254))
            this.w_CACHKOBB = NVL(CACHKOBB604,space(1))
          else
          .link_6_4('Load')
          endif
        .w_PACAUPRE = NVL(PACAUPRE,space(20))
          .link_6_5('Load')
        .w_PATIPUDI = NVL(PATIPUDI,space(20))
          if link_6_6_joined
            this.w_PATIPUDI = NVL(CACODICE606,NVL(this.w_PATIPUDI,space(20)))
            this.w_TIPPOLU = NVL(CARAGGST606,space(1))
          else
          .link_6_6('Load')
          endif
        .w_PACOSGIU = NVL(PACOSGIU,space(20))
          if link_6_7_joined
            this.w_PACOSGIU = NVL(CACODICE607,NVL(this.w_PACOSGIU,space(20)))
            this.w_DESCOSGIU = NVL(CADESCRI607,space(254))
            this.w_CARAGGST = NVL(CARAGGST607,space(10))
          else
          .link_6_7('Load')
          endif
        .w_PASTRING = NVL(PASTRING,space(14))
        .w_PATIPCTR = NVL(PATIPCTR,space(20))
          if link_6_9_joined
            this.w_PATIPCTR = NVL(CACODICE609,NVL(this.w_PATIPCTR,space(20)))
            this.w_PADESINC = NVL(CADESCRI609,space(254))
          else
          .link_6_9('Load')
          endif
        .w_PANGGINC = NVL(PANGGINC,0)
        .w_PATIPTEL = NVL(PATIPTEL,space(20))
          if link_6_12_joined
            this.w_PATIPTEL = NVL(CACODICE612,NVL(this.w_PATIPTEL,space(20)))
            this.w_DES_TEL = NVL(CADESCRI612,space(254))
          else
          .link_6_12('Load')
          endif
        .w_PATIPASS = NVL(PATIPASS,space(20))
          if link_6_13_joined
            this.w_PATIPASS = NVL(CACODICE613,NVL(this.w_PATIPASS,space(20)))
            this.w_DES_CHI = NVL(CADESCRI613,space(254))
          else
          .link_6_13('Load')
          endif
        .w_PATIPAPI = NVL(PATIPAPI,space(20))
          if link_6_14_joined
            this.w_PATIPAPI = NVL(CACODICE614,NVL(this.w_PATIPAPI,space(20)))
            this.w_DES_API = NVL(CADESCRI614,space(254))
          else
          .link_6_14('Load')
          endif
        .w_PATIPCOS = NVL(PATIPCOS,space(20))
          if link_6_15_joined
            this.w_PATIPCOS = NVL(CACODICE615,NVL(this.w_PATIPCOS,space(20)))
            this.w_DES_DOC = NVL(CADESCRI615,space(254))
          else
          .link_6_15('Load')
          endif
        .w_PATIPGEN = NVL(PATIPGEN,space(20))
          if link_6_16_joined
            this.w_PATIPGEN = NVL(CACODICE616,NVL(this.w_PATIPGEN,space(20)))
            this.w_DES_GEN = NVL(CADESCRI616,space(254))
          else
          .link_6_16('Load')
          endif
        .w_PATIPAPF = NVL(PATIPAPF,space(20))
          if link_6_17_joined
            this.w_PATIPAPF = NVL(CACODICE617,NVL(this.w_PATIPAPF,space(20)))
            this.w_DES_APF = NVL(CADESCRI617,space(254))
          else
          .link_6_17('Load')
          endif
        .w_PAOGGOUT = NVL(PAOGGOUT,space(254))
        .oPgFrm.Page5.oPag.oObj_5_1.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page5.oPag.oObj_5_2.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page5.oPag.oObj_5_3.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .w_PAFLAPPU = NVL(PAFLAPPU,space(1))
        .w_PAFLATGE = NVL(PAFLATGE,space(1))
        .w_PAFLUDIE = NVL(PAFLUDIE,space(1))
        .w_PAFLSEST = NVL(PAFLSEST,space(1))
        .w_PAFLASSE = NVL(PAFLASSE,space(1))
        .w_PAFLINPR = NVL(PAFLINPR,space(1))
        .w_PAFLNOTE = NVL(PAFLNOTE,space(1))
        .w_PAFLDAFA = NVL(PAFLDAFA,space(1))
        .w_PAFLAPP1 = NVL(PAFLAPP1,space(1))
        .w_PAFLATG1 = NVL(PAFLATG1,space(1))
        .w_PAFLUDI1 = NVL(PAFLUDI1,space(1))
        .w_PAFLSES1 = NVL(PAFLSES1,space(1))
        .w_PAFLASS1 = NVL(PAFLASS1,space(1))
        .w_PAFLINP1 = NVL(PAFLINP1,space(1))
        .w_PAFLNOT1 = NVL(PAFLNOT1,space(1))
        .w_PAFLDAF1 = NVL(PAFLDAF1,space(1))
        .w_PAFLAPP2 = NVL(PAFLAPP2,space(1))
        .w_PAFLATG2 = NVL(PAFLATG2,space(1))
        .w_PAFLUDI2 = NVL(PAFLUDI2,space(1))
        .w_PAFLSES2 = NVL(PAFLSES2,space(1))
        .w_PAFLASS2 = NVL(PAFLASS2,space(1))
        .w_PAFLINP2 = NVL(PAFLINP2,space(1))
        .w_PAFLNOT2 = NVL(PAFLNOT2,space(1))
        .w_PAFLDAF2 = NVL(PAFLDAF2,space(1))
        .w_PAFLAPP3 = NVL(PAFLAPP3,space(1))
        .w_PAFLATG3 = NVL(PAFLATG3,space(1))
        .w_PAFLUDI3 = NVL(PAFLUDI3,space(1))
        .w_PAFLSES3 = NVL(PAFLSES3,space(1))
        .w_PAFLASS3 = NVL(PAFLASS3,space(1))
        .w_PAFLINP3 = NVL(PAFLINP3,space(1))
        .w_PAFLNOT3 = NVL(PAFLNOT3,space(1))
        .w_PAFLDAF3 = NVL(PAFLDAF3,space(1))
        .oPgFrm.Page6.oPag.oObj_6_45.Calculate(IIF(IsAlt(),"GSAG_APA_Outl2","GSAG_APA_Outl"))
        .w_PAOGGMOD = NVL(PAOGGMOD,space(254))
        .w_PATXTMOD = NVL(PATXTMOD,space(0))
        .w_PAPSTMOD = NVL(PAPSTMOD,space(0))
        .w_PAOGGDEL = NVL(PAOGGDEL,space(254))
        .w_PATXTDEL = NVL(PATXTDEL,space(0))
        .w_PAPSTDEL = NVL(PAPSTDEL,space(0))
        .oPgFrm.Page3.oPag.oObj_3_9.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page3.oPag.oObj_3_10.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page3.oPag.oObj_3_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page4.oPag.oObj_4_9.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page4.oPag.oObj_4_10.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page4.oPag.oObj_4_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .w_PAFLR_AG = NVL(PAFLR_AG,space(1))
        .w_PADATFIS = NVL(PADATFIS,space(1))
        .w_PADESATT = NVL(PADESATT,space(0))
        .w_PADESDOC = NVL(PADESDOC,space(0))
        cp_LoadRecExtFlds(this,'PAR_AGEN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_ATTIVACQ = space(10)
      .w_PACODAZI = space(5)
      .w_PERVAL = space(3)
      .w_CHECK = .f.
      .w_MVCODVAL = space(3)
      .w_MVFLVEAC = space(1)
      .w_DATALIST = ctod("  /  /  ")
      .w_PACODLIS = space(5)
      .w_PACODLIS = space(5)
      .w_PALISACQ = space(5)
      .w_RDESLIS = space(40)
      .w_PANOFLAT = space(1)
      .w_PADATATP = 0
      .w_PADATATS = 0
      .w_PAFLCOSE = space(1)
      .w_PADACOSE = 0
      .w_PA_ACOSE = 0
      .w_PANOFLPR = space(1)
      .w_PADATPRP = 0
      .w_PADATPRS = 0
      .w_PAGIOSCA = 0
      .w_PAFLRDOC = space(1)
      .w_PACHKFES = space(1)
      .w_PACHKPRE = space(1)
      .w_PACALEND = space(5)
      .w_PACHKCAR = space(1)
      .w_PAVISTRC = space(1)
      .w_PATIPAPI = space(20)
      .w_CARAGGST = space(1)
      .w_PATIPAPF = space(20)
      .w_PATIPCOS = space(20)
      .w_PATIPNOT = space(20)
      .w_PATIPTEL = space(20)
      .w_PATIPASS = space(20)
      .w_PATIPGEN = space(20)
      .w_CATEG = space(1)
      .w_PAFLVISI = space(1)
      .w_PAFLANTI = space(1)
      .w_OLDFLVISI = space(10)
      .w_FLSCON = space(1)
      .w_DESCAL = space(40)
      .w_CICLO = space(1)
      .w_DESACQ = space(40)
      .w_EDESLIS = space(40)
      .w_FLGCIC = space(1)
      .w_TIPLIS = space(1)
      .w_PAGESRIS = space(1)
      .w_PADESRIS = space(254)
      .w_PAST_ATT = space(1)
      .w_PAOGGCRE = space(254)
      .w_PATXTCRE = space(0)
      .w_PAPSTCRE = space(0)
      .w_PACHKCPL = space(1)
      .w_PACHKING = space(1)
      .w_PADATINI = ctod("  /  /  ")
      .w_PAGIOCHK = 0
      .w_PACODMDC = space(3)
      .w_PCCAUABB = space(5)
      .w_PACALDAT = space(1)
      .w_PACHKATT = 0
      .w_AFLVEAC = space(1)
      .w_DESCAU = space(35)
      .w_TDFLVEAC = space(1)
      .w_CAT_DOC = space(2)
      .w_FLSCON2 = space(1)
      .w_IVALIS = space(1)
      .w_PAFLESAT = space(1)
      .w_PAFLDTRP = space(1)
      .w_PAATTRIS = space(20)
      .w_PAFLSIAT = space(1)
      .w_PAFLPRES = space(1)
      .w_DTBSO_CAUMATTI = ctod("  /  /  ")
      .w_CACHKOBB = space(1)
      .w_PACODMOD = space(20)
      .w_DESCRI = space(40)
      .w_PACODNUM = space(1)
      .w_PCNOFLDC = space(1)
      .w_PACODDOC = space(3)
      .w_PCNOFLAT = space(1)
      .w_PACODPER = space(3)
      .w_PADESSUP = space(254)
      .w_PCTIPDOC = space(5)
      .w_PCTIPATT = space(20)
      .w_PCGRUPAR = space(5)
      .w_DESDOC = space(35)
      .w_DESATT = space(254)
      .w_PATIPRIS = space(1)
      .w_CARAGGST = space(10)
      .w_CATDOC = space(2)
      .w_FLVEAC = space(1)
      .w_TIPRIS = space(1)
      .w_OB_TEST = ctod("  /  /  ")
      .w_DESGRU = space(60)
      .w_PCESCRIN = space(1)
      .w_PCPERCON = space(3)
      .w_MDDESCRI = space(50)
      .w_PCRINCON = space(1)
      .w_PCNUMGIO = 0
      .w_DESMDO = space(50)
      .w_DESMAT = space(50)
      .w_PACAUATT = space(20)
      .w_CauDescri = space(254)
      .w_PACAUATT = space(20)
      .w_PACAUPRE = space(20)
      .w_PATIPUDI = space(20)
      .w_PACOSGIU = space(20)
      .w_PASTRING = space(14)
      .w_PATIPCTR = space(20)
      .w_PANGGINC = 0
      .w_PATIPTEL = space(20)
      .w_PATIPASS = space(20)
      .w_PATIPAPI = space(20)
      .w_PATIPCOS = space(20)
      .w_PATIPGEN = space(20)
      .w_PATIPAPF = space(20)
      .w_DES_CAU = space(254)
      .w_DES_GEN = space(254)
      .w_DES_TEL = space(254)
      .w_DES_CHI = space(254)
      .w_DES_API = space(254)
      .w_DES_APF = space(254)
      .w_DES_DOC = space(254)
      .w_DESCOSGIU = space(254)
      .w_TIPPOLU = space(1)
      .w_PADESINC = space(254)
      .w_PAOGGOUT = space(254)
      .w_PAFLAPPU = space(1)
      .w_PAFLATGE = space(1)
      .w_PAFLUDIE = space(1)
      .w_PAFLSEST = space(1)
      .w_PAFLASSE = space(1)
      .w_PAFLINPR = space(1)
      .w_PAFLNOTE = space(1)
      .w_PAFLDAFA = space(1)
      .w_PAFLAPP1 = space(1)
      .w_PAFLATG1 = space(1)
      .w_PAFLUDI1 = space(1)
      .w_PAFLSES1 = space(1)
      .w_PAFLASS1 = space(1)
      .w_PAFLINP1 = space(1)
      .w_PAFLNOT1 = space(1)
      .w_PAFLDAF1 = space(1)
      .w_PAFLAPP2 = space(1)
      .w_PAFLATG2 = space(1)
      .w_PAFLUDI2 = space(1)
      .w_PAFLSES2 = space(1)
      .w_PAFLASS2 = space(1)
      .w_PAFLINP2 = space(1)
      .w_PAFLNOT2 = space(1)
      .w_PAFLDAF2 = space(1)
      .w_PAFLAPP3 = space(1)
      .w_PAFLATG3 = space(1)
      .w_PAFLUDI3 = space(1)
      .w_PAFLSES3 = space(1)
      .w_PAFLASS3 = space(1)
      .w_PAFLINP3 = space(1)
      .w_PAFLNOT3 = space(1)
      .w_PAFLDAF3 = space(1)
      .w_PAOGGMOD = space(254)
      .w_PATXTMOD = space(0)
      .w_PAPSTMOD = space(0)
      .w_PAOGGDEL = space(254)
      .w_PATXTDEL = space(0)
      .w_PAPSTDEL = space(0)
      .w_TDFLINTE = space(1)
      .w_PAFLR_AG = space(1)
      .w_PADATFIS = space(1)
      .w_PADESATT = space(0)
      .w_PADESDOC = space(0)
      if .cFunction<>"Filter"
        .w_ATTIVACQ = iif((Isahe() and g_CACQ='S') or (Isahr() and g_ACQU='S'),'S','N')
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_PACODAZI))
          .link_1_2('Full')
          endif
        .w_PERVAL = g_PERVAL
        .w_CHECK = .T.
        .w_MVCODVAL = g_PERVAL
          .DoRTCalc(6,6,.f.)
        .w_DATALIST = i_DATSYS
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_PACODLIS))
          .link_1_8('Full')
          endif
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_PACODLIS))
          .link_1_10('Full')
          endif
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_PALISACQ))
          .link_1_11('Full')
          endif
          .DoRTCalc(11,11,.f.)
        .w_PANOFLAT = 'N'
        .w_PADATATP = IIF(.w_PANOFLAT$'ENI', 0, .w_PADATATP)
        .w_PADATATS = IIF(.w_PANOFLAT$ 'ENF', 0, .w_PADATATS)
        .w_PAFLCOSE = 'E'
        .w_PADACOSE = IIF(.w_PAFLCOSE$'ENI', 0, .w_PADACOSE)
        .w_PA_ACOSE = IIF(.w_PAFLCOSE$ 'ENF', 0, .w_PA_ACOSE)
        .w_PANOFLPR = 'I'
        .w_PADATPRP = IIF(.w_PANOFLPR$'ENI', 0, .w_PADATPRP)
        .w_PADATPRS = IIF(.w_PANOFLPR$ 'ENF', 0, .w_PADATPRS)
          .DoRTCalc(21,24,.f.)
        .w_PACALEND = iif(.w_PACHKFES='C',GETPREDCAL(),Space(5))
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_PACALEND))
          .link_1_26('Full')
          endif
          .DoRTCalc(26,26,.f.)
        .w_PAVISTRC = ' '
          .DoRTCalc(28,35,.f.)
        .w_CATEG = 'Z'
        .w_PAFLVISI = 'L'
          .DoRTCalc(38,38,.f.)
        .w_OLDFLVISI = .w_PAFLVISI
          .DoRTCalc(40,41,.f.)
        .w_CICLO = 'E'
          .DoRTCalc(43,47,.f.)
        .w_PADESRIS = IIF( .w_PAGESRIS<>'S' , '' , Cp_translate('Riservato'))
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page2.oPag.oObj_2_7.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .DoRTCalc(49,57,.f.)
          if not(empty(.w_PACODMDC))
          .link_1_74('Full')
          endif
        .DoRTCalc(58,58,.f.)
          if not(empty(.w_PCCAUABB))
          .link_1_75('Full')
          endif
          .DoRTCalc(59,62,.f.)
        .w_TDFLVEAC = 'V'
          .DoRTCalc(64,68,.f.)
        .w_PAATTRIS = Space(20)
        .DoRTCalc(69,69,.f.)
          if not(empty(.w_PAATTRIS))
          .link_1_92('Full')
          endif
          .DoRTCalc(70,71,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
        .oPgFrm.Page2.oPag.oObj_2_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .DoRTCalc(73,74,.f.)
          if not(empty(.w_PACODMOD))
          .link_7_4('Full')
          endif
          .DoRTCalc(75,75,.f.)
        .w_PACODNUM = 'N'
        .w_PCNOFLDC = iif(empty(.w_PACODDOC),'N', .w_PCNOFLDC)
        .DoRTCalc(78,78,.f.)
          if not(empty(.w_PACODDOC))
          .link_7_9('Full')
          endif
        .w_PCNOFLAT = iif(empty(.w_PACODPER),'N', .w_PCNOFLAT)
        .DoRTCalc(80,80,.f.)
          if not(empty(.w_PACODPER))
          .link_7_11('Full')
          endif
        .DoRTCalc(81,82,.f.)
          if not(empty(.w_PCTIPDOC))
          .link_7_13('Full')
          endif
        .DoRTCalc(83,83,.f.)
          if not(empty(.w_PCTIPATT))
          .link_7_15('Full')
          endif
        .DoRTCalc(84,84,.f.)
          if not(empty(.w_PCGRUPAR))
          .link_7_17('Full')
          endif
          .DoRTCalc(85,86,.f.)
        .w_PATIPRIS = 'G'
          .DoRTCalc(88,91,.f.)
        .w_OB_TEST = I_DATSYS
          .DoRTCalc(93,93,.f.)
        .w_PCESCRIN = "N"
        .DoRTCalc(95,95,.f.)
          if not(empty(.w_PCPERCON))
          .link_7_38('Full')
          endif
          .DoRTCalc(96,96,.f.)
        .w_PCRINCON = "N"
        .DoRTCalc(98,101,.f.)
          if not(empty(.w_PACAUATT))
          .link_6_1('Full')
          endif
        .DoRTCalc(102,103,.f.)
          if not(empty(.w_PACAUATT))
          .link_6_4('Full')
          endif
        .DoRTCalc(104,104,.f.)
          if not(empty(.w_PACAUPRE))
          .link_6_5('Full')
          endif
        .DoRTCalc(105,105,.f.)
          if not(empty(.w_PATIPUDI))
          .link_6_6('Full')
          endif
        .DoRTCalc(106,106,.f.)
          if not(empty(.w_PACOSGIU))
          .link_6_7('Full')
          endif
        .DoRTCalc(107,108,.f.)
          if not(empty(.w_PATIPCTR))
          .link_6_9('Full')
          endif
        .DoRTCalc(109,110,.f.)
          if not(empty(.w_PATIPTEL))
          .link_6_12('Full')
          endif
        .DoRTCalc(111,111,.f.)
          if not(empty(.w_PATIPASS))
          .link_6_13('Full')
          endif
        .DoRTCalc(112,112,.f.)
          if not(empty(.w_PATIPAPI))
          .link_6_14('Full')
          endif
        .DoRTCalc(113,113,.f.)
          if not(empty(.w_PATIPCOS))
          .link_6_15('Full')
          endif
        .DoRTCalc(114,114,.f.)
          if not(empty(.w_PATIPGEN))
          .link_6_16('Full')
          endif
        .DoRTCalc(115,115,.f.)
          if not(empty(.w_PATIPAPF))
          .link_6_17('Full')
          endif
        .oPgFrm.Page5.oPag.oObj_5_1.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page5.oPag.oObj_5_2.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page5.oPag.oObj_5_3.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page6.oPag.oObj_6_45.Calculate(IIF(IsAlt(),"GSAG_APA_Outl2","GSAG_APA_Outl"))
        .oPgFrm.Page3.oPag.oObj_3_9.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page3.oPag.oObj_3_10.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page3.oPag.oObj_3_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page4.oPag.oObj_4_9.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page4.oPag.oObj_4_10.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page4.oPag.oObj_4_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
          .DoRTCalc(116,166,.f.)
        .w_PADATFIS = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_AGEN')
    this.DoRTCalc(168,169,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPACODLIS_1_8.enabled = i_bVal
      .Page1.oPag.oPACODLIS_1_10.enabled = i_bVal
      .Page1.oPag.oPALISACQ_1_11.enabled = i_bVal
      .Page1.oPag.oPANOFLAT_1_13.enabled = i_bVal
      .Page1.oPag.oPADATATP_1_14.enabled = i_bVal
      .Page1.oPag.oPADATATS_1_15.enabled = i_bVal
      .Page1.oPag.oPAFLCOSE_1_16.enabled = i_bVal
      .Page1.oPag.oPADACOSE_1_17.enabled = i_bVal
      .Page1.oPag.oPA_ACOSE_1_18.enabled = i_bVal
      .Page1.oPag.oPANOFLPR_1_19.enabled = i_bVal
      .Page1.oPag.oPADATPRP_1_20.enabled = i_bVal
      .Page1.oPag.oPADATPRS_1_21.enabled = i_bVal
      .Page1.oPag.oPAGIOSCA_1_22.enabled = i_bVal
      .Page1.oPag.oPAFLRDOC_1_23.enabled = i_bVal
      .Page1.oPag.oPACHKFES_1_24.enabled = i_bVal
      .Page1.oPag.oPACHKPRE_1_25.enabled = i_bVal
      .Page1.oPag.oPACALEND_1_26.enabled = i_bVal
      .Page1.oPag.oPACHKCAR_1_27.enabled = i_bVal
      .Page1.oPag.oPAVISTRC_1_28.enabled = i_bVal
      .Page1.oPag.oPAFLVISI_1_45.enabled = i_bVal
      .Page1.oPag.oPAFLANTI_1_46.enabled = i_bVal
      .Page1.oPag.oPAGESRIS_1_61.enabled = i_bVal
      .Page1.oPag.oPADESRIS_1_62.enabled = i_bVal
      .Page1.oPag.oPAST_ATT_1_63.enabled = i_bVal
      .Page2.oPag.oPAOGGCRE_2_1.enabled = i_bVal
      .Page2.oPag.oPATXTCRE_2_2.enabled = i_bVal
      .Page2.oPag.oPAPSTCRE_2_3.enabled = i_bVal
      .Page1.oPag.oPACHKCPL_1_69.enabled = i_bVal
      .Page1.oPag.oPACHKING_1_70.enabled = i_bVal
      .Page1.oPag.oPADATINI_1_71.enabled = i_bVal
      .Page1.oPag.oPAGIOCHK_1_72.enabled = i_bVal
      .Page1.oPag.oPACODMDC_1_74.enabled = i_bVal
      .Page1.oPag.oPCCAUABB_1_75.enabled = i_bVal
      .Page1.oPag.oPACALDAT_1_76.enabled = i_bVal
      .Page1.oPag.oPACHKATT_1_77.enabled = i_bVal
      .Page1.oPag.oPAFLESAT_1_90.enabled = i_bVal
      .Page1.oPag.oPAFLDTRP_1_91.enabled = i_bVal
      .Page1.oPag.oPAATTRIS_1_92.enabled = i_bVal
      .Page1.oPag.oPAFLSIAT_1_93.enabled = i_bVal
      .Page1.oPag.oPAFLPRES_1_94.enabled = i_bVal
      .Page7.oPag.oPACODMOD_7_4.enabled = i_bVal
      .Page7.oPag.oPACODNUM_7_7.enabled = i_bVal
      .Page7.oPag.oPCNOFLDC_7_8.enabled = i_bVal
      .Page7.oPag.oPACODDOC_7_9.enabled = i_bVal
      .Page7.oPag.oPCNOFLAT_7_10.enabled = i_bVal
      .Page7.oPag.oPACODPER_7_11.enabled = i_bVal
      .Page7.oPag.oPADESSUP_7_12.enabled = i_bVal
      .Page7.oPag.oPCTIPDOC_7_13.enabled = i_bVal
      .Page7.oPag.oPCTIPATT_7_15.enabled = i_bVal
      .Page7.oPag.oPCGRUPAR_7_17.enabled = i_bVal
      .Page7.oPag.oPCESCRIN_7_37.enabled = i_bVal
      .Page7.oPag.oPCPERCON_7_38.enabled = i_bVal
      .Page7.oPag.oPCRINCON_7_41.enabled = i_bVal
      .Page7.oPag.oPCNUMGIO_7_42.enabled = i_bVal
      .Page6.oPag.oPACAUATT_6_1.enabled = i_bVal
      .Page6.oPag.oPACAUATT_6_4.enabled = i_bVal
      .Page6.oPag.oPACAUPRE_6_5.enabled = i_bVal
      .Page6.oPag.oPACOSGIU_6_7.enabled = i_bVal
      .Page6.oPag.oPASTRING_6_8.enabled = i_bVal
      .Page6.oPag.oPATIPCTR_6_9.enabled = i_bVal
      .Page6.oPag.oPANGGINC_6_10.enabled = i_bVal
      .Page6.oPag.oPATIPTEL_6_12.enabled = i_bVal
      .Page6.oPag.oPATIPASS_6_13.enabled = i_bVal
      .Page6.oPag.oPATIPAPI_6_14.enabled = i_bVal
      .Page6.oPag.oPATIPCOS_6_15.enabled = i_bVal
      .Page6.oPag.oPATIPGEN_6_16.enabled = i_bVal
      .Page6.oPag.oPATIPAPF_6_17.enabled = i_bVal
      .Page6.oPag.oPAOGGOUT_6_44.enabled = i_bVal
      .Page5.oPag.oPAFLAPPU_5_6.enabled = i_bVal
      .Page5.oPag.oPAFLATGE_5_7.enabled = i_bVal
      .Page5.oPag.oPAFLUDIE_5_8.enabled = i_bVal
      .Page5.oPag.oPAFLSEST_5_9.enabled = i_bVal
      .Page5.oPag.oPAFLASSE_5_10.enabled = i_bVal
      .Page5.oPag.oPAFLINPR_5_11.enabled = i_bVal
      .Page5.oPag.oPAFLNOTE_5_12.enabled = i_bVal
      .Page5.oPag.oPAFLDAFA_5_13.enabled = i_bVal
      .Page5.oPag.oPAFLAPP1_5_16.enabled = i_bVal
      .Page5.oPag.oPAFLATG1_5_17.enabled = i_bVal
      .Page5.oPag.oPAFLUDI1_5_18.enabled = i_bVal
      .Page5.oPag.oPAFLSES1_5_19.enabled = i_bVal
      .Page5.oPag.oPAFLASS1_5_20.enabled = i_bVal
      .Page5.oPag.oPAFLINP1_5_21.enabled = i_bVal
      .Page5.oPag.oPAFLNOT1_5_22.enabled = i_bVal
      .Page5.oPag.oPAFLDAF1_5_23.enabled = i_bVal
      .Page5.oPag.oPAFLAPP2_5_26.enabled = i_bVal
      .Page5.oPag.oPAFLATG2_5_27.enabled = i_bVal
      .Page5.oPag.oPAFLUDI2_5_28.enabled = i_bVal
      .Page5.oPag.oPAFLSES2_5_29.enabled = i_bVal
      .Page5.oPag.oPAFLASS2_5_30.enabled = i_bVal
      .Page5.oPag.oPAFLINP2_5_31.enabled = i_bVal
      .Page5.oPag.oPAFLNOT2_5_32.enabled = i_bVal
      .Page5.oPag.oPAFLDAF2_5_33.enabled = i_bVal
      .Page5.oPag.oPAFLAPP3_5_36.enabled = i_bVal
      .Page5.oPag.oPAFLATG3_5_37.enabled = i_bVal
      .Page5.oPag.oPAFLUDI3_5_38.enabled = i_bVal
      .Page5.oPag.oPAFLSES3_5_39.enabled = i_bVal
      .Page5.oPag.oPAFLASS3_5_40.enabled = i_bVal
      .Page5.oPag.oPAFLINP3_5_41.enabled = i_bVal
      .Page5.oPag.oPAFLNOT3_5_42.enabled = i_bVal
      .Page5.oPag.oPAFLDAF3_5_43.enabled = i_bVal
      .Page3.oPag.oPAOGGMOD_3_1.enabled = i_bVal
      .Page3.oPag.oPATXTMOD_3_2.enabled = i_bVal
      .Page3.oPag.oPAPSTMOD_3_3.enabled = i_bVal
      .Page4.oPag.oPAOGGDEL_4_1.enabled = i_bVal
      .Page4.oPag.oPATXTDEL_4_2.enabled = i_bVal
      .Page4.oPag.oPAPSTDEL_4_3.enabled = i_bVal
      .Page1.oPag.oPAFLR_AG_1_102.enabled = i_bVal
      .Page7.oPag.oPADATFIS_7_48.enabled = i_bVal
      .Page7.oPag.oPADESATT_7_49.enabled = i_bVal
      .Page7.oPag.oPADESDOC_7_51.enabled = i_bVal
    endwith
    this.gsag_mat.SetStatus(i_cOp)
    this.GSAG_MCD.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PAR_AGEN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gsag_mat.SetChildrenStatus(i_cOp)
  *  this.GSAG_MCD.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODAZI,"PACODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODLIS,"PACODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODLIS,"PACODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PALISACQ,"PALISACQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PANOFLAT,"PANOFLAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATATP,"PADATATP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATATS,"PADATATS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLCOSE,"PAFLCOSE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADACOSE,"PADACOSE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PA_ACOSE,"PA_ACOSE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PANOFLPR,"PANOFLPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATPRP,"PADATPRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATPRS,"PADATPRS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAGIOSCA,"PAGIOSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLRDOC,"PAFLRDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACHKFES,"PACHKFES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACHKPRE,"PACHKPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACALEND,"PACALEND",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACHKCAR,"PACHKCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAVISTRC,"PAVISTRC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPAPI,"PATIPAPI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPAPF,"PATIPAPF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPCOS,"PATIPCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPNOT,"PATIPNOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPTEL,"PATIPTEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPASS,"PATIPASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPGEN,"PATIPGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLVISI,"PAFLVISI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLANTI,"PAFLANTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAGESRIS,"PAGESRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADESRIS,"PADESRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAST_ATT,"PAST_ATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAOGGCRE,"PAOGGCRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATXTCRE,"PATXTCRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPSTCRE,"PAPSTCRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACHKCPL,"PACHKCPL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACHKING,"PACHKING",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATINI,"PADATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAGIOCHK,"PAGIOCHK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODMDC,"PACODMDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCCAUABB,"PCCAUABB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACALDAT,"PACALDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACHKATT,"PACHKATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLESAT,"PAFLESAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLDTRP,"PAFLDTRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAATTRIS,"PAATTRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLSIAT,"PAFLSIAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLPRES,"PAFLPRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODMOD,"PACODMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODNUM,"PACODNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCNOFLDC,"PCNOFLDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODDOC,"PACODDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCNOFLAT,"PCNOFLAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODPER,"PACODPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADESSUP,"PADESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPDOC,"PCTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCTIPATT,"PCTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCGRUPAR,"PCGRUPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCESCRIN,"PCESCRIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCPERCON,"PCPERCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCRINCON,"PCRINCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCNUMGIO,"PCNUMGIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACAUATT,"PACAUATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACAUATT,"PACAUATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACAUPRE,"PACAUPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPUDI,"PATIPUDI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACOSGIU,"PACOSGIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASTRING,"PASTRING",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPCTR,"PATIPCTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PANGGINC,"PANGGINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPTEL,"PATIPTEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPASS,"PATIPASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPAPI,"PATIPAPI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPCOS,"PATIPCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPGEN,"PATIPGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATIPAPF,"PATIPAPF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAOGGOUT,"PAOGGOUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLAPPU,"PAFLAPPU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLATGE,"PAFLATGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLUDIE,"PAFLUDIE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLSEST,"PAFLSEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLASSE,"PAFLASSE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLINPR,"PAFLINPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLNOTE,"PAFLNOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLDAFA,"PAFLDAFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLAPP1,"PAFLAPP1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLATG1,"PAFLATG1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLUDI1,"PAFLUDI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLSES1,"PAFLSES1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLASS1,"PAFLASS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLINP1,"PAFLINP1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLNOT1,"PAFLNOT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLDAF1,"PAFLDAF1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLAPP2,"PAFLAPP2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLATG2,"PAFLATG2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLUDI2,"PAFLUDI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLSES2,"PAFLSES2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLASS2,"PAFLASS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLINP2,"PAFLINP2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLNOT2,"PAFLNOT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLDAF2,"PAFLDAF2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLAPP3,"PAFLAPP3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLATG3,"PAFLATG3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLUDI3,"PAFLUDI3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLSES3,"PAFLSES3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLASS3,"PAFLASS3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLINP3,"PAFLINP3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLNOT3,"PAFLNOT3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLDAF3,"PAFLDAF3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAOGGMOD,"PAOGGMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATXTMOD,"PATXTMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPSTMOD,"PAPSTMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAOGGDEL,"PAOGGDEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PATXTDEL,"PATXTDEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAPSTDEL,"PAPSTDEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLR_AG,"PAFLR_AG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADATFIS,"PADATFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADESATT,"PADESATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADESDOC,"PADESDOC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAR_AGEN_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAR_AGEN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_AGEN')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_AGEN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PACODAZI,PACODLIS,PALISACQ,PANOFLAT,PADATATP"+;
                  ",PADATATS,PAFLCOSE,PADACOSE,PA_ACOSE,PANOFLPR"+;
                  ",PADATPRP,PADATPRS,PAGIOSCA,PAFLRDOC,PACHKFES"+;
                  ",PACHKPRE,PACALEND,PACHKCAR,PAVISTRC,PATIPAPI"+;
                  ",PATIPAPF,PATIPCOS,PATIPNOT,PATIPTEL,PATIPASS"+;
                  ",PATIPGEN,PAFLVISI,PAFLANTI,PAGESRIS,PADESRIS"+;
                  ",PAST_ATT,PAOGGCRE,PATXTCRE,PAPSTCRE,PACHKCPL"+;
                  ",PACHKING,PADATINI,PAGIOCHK,PACODMDC,PCCAUABB"+;
                  ",PACALDAT,PACHKATT,PAFLESAT,PAFLDTRP,PAATTRIS"+;
                  ",PAFLSIAT,PAFLPRES,PACODMOD,PACODNUM,PCNOFLDC"+;
                  ",PACODDOC,PCNOFLAT,PACODPER,PADESSUP,PCTIPDOC"+;
                  ",PCTIPATT,PCGRUPAR,PCESCRIN,PCPERCON,PCRINCON"+;
                  ",PCNUMGIO,PACAUATT,PACAUPRE,PATIPUDI,PACOSGIU"+;
                  ",PASTRING,PATIPCTR,PANGGINC,PAOGGOUT,PAFLAPPU"+;
                  ",PAFLATGE,PAFLUDIE,PAFLSEST,PAFLASSE,PAFLINPR"+;
                  ",PAFLNOTE,PAFLDAFA,PAFLAPP1,PAFLATG1,PAFLUDI1"+;
                  ",PAFLSES1,PAFLASS1,PAFLINP1,PAFLNOT1,PAFLDAF1"+;
                  ",PAFLAPP2,PAFLATG2,PAFLUDI2,PAFLSES2,PAFLASS2"+;
                  ",PAFLINP2,PAFLNOT2,PAFLDAF2,PAFLAPP3,PAFLATG3"+;
                  ",PAFLUDI3,PAFLSES3,PAFLASS3,PAFLINP3,PAFLNOT3"+;
                  ",PAFLDAF3,PAOGGMOD,PATXTMOD,PAPSTMOD,PAOGGDEL"+;
                  ",PATXTDEL,PAPSTDEL,PAFLR_AG,PADATFIS,PADESATT"+;
                  ",PADESDOC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_PACODAZI)+;
                  ","+cp_ToStrODBCNull(this.w_PACODLIS)+;
                  ","+cp_ToStrODBCNull(this.w_PALISACQ)+;
                  ","+cp_ToStrODBC(this.w_PANOFLAT)+;
                  ","+cp_ToStrODBC(this.w_PADATATP)+;
                  ","+cp_ToStrODBC(this.w_PADATATS)+;
                  ","+cp_ToStrODBC(this.w_PAFLCOSE)+;
                  ","+cp_ToStrODBC(this.w_PADACOSE)+;
                  ","+cp_ToStrODBC(this.w_PA_ACOSE)+;
                  ","+cp_ToStrODBC(this.w_PANOFLPR)+;
                  ","+cp_ToStrODBC(this.w_PADATPRP)+;
                  ","+cp_ToStrODBC(this.w_PADATPRS)+;
                  ","+cp_ToStrODBC(this.w_PAGIOSCA)+;
                  ","+cp_ToStrODBC(this.w_PAFLRDOC)+;
                  ","+cp_ToStrODBC(this.w_PACHKFES)+;
                  ","+cp_ToStrODBC(this.w_PACHKPRE)+;
                  ","+cp_ToStrODBCNull(this.w_PACALEND)+;
                  ","+cp_ToStrODBC(this.w_PACHKCAR)+;
                  ","+cp_ToStrODBC(this.w_PAVISTRC)+;
                  ","+cp_ToStrODBC(this.w_PATIPAPI)+;
                  ","+cp_ToStrODBC(this.w_PATIPAPF)+;
                  ","+cp_ToStrODBC(this.w_PATIPCOS)+;
                  ","+cp_ToStrODBC(this.w_PATIPNOT)+;
                  ","+cp_ToStrODBC(this.w_PATIPTEL)+;
                  ","+cp_ToStrODBC(this.w_PATIPASS)+;
                  ","+cp_ToStrODBC(this.w_PATIPGEN)+;
                  ","+cp_ToStrODBC(this.w_PAFLVISI)+;
                  ","+cp_ToStrODBC(this.w_PAFLANTI)+;
                  ","+cp_ToStrODBC(this.w_PAGESRIS)+;
                  ","+cp_ToStrODBC(this.w_PADESRIS)+;
                  ","+cp_ToStrODBC(this.w_PAST_ATT)+;
                  ","+cp_ToStrODBC(this.w_PAOGGCRE)+;
                  ","+cp_ToStrODBC(this.w_PATXTCRE)+;
                  ","+cp_ToStrODBC(this.w_PAPSTCRE)+;
                  ","+cp_ToStrODBC(this.w_PACHKCPL)+;
                  ","+cp_ToStrODBC(this.w_PACHKING)+;
                  ","+cp_ToStrODBC(this.w_PADATINI)+;
                  ","+cp_ToStrODBC(this.w_PAGIOCHK)+;
                  ","+cp_ToStrODBCNull(this.w_PACODMDC)+;
                  ","+cp_ToStrODBCNull(this.w_PCCAUABB)+;
                  ","+cp_ToStrODBC(this.w_PACALDAT)+;
                  ","+cp_ToStrODBC(this.w_PACHKATT)+;
                  ","+cp_ToStrODBC(this.w_PAFLESAT)+;
                  ","+cp_ToStrODBC(this.w_PAFLDTRP)+;
                  ","+cp_ToStrODBCNull(this.w_PAATTRIS)+;
                  ","+cp_ToStrODBC(this.w_PAFLSIAT)+;
                  ","+cp_ToStrODBC(this.w_PAFLPRES)+;
                  ","+cp_ToStrODBCNull(this.w_PACODMOD)+;
                  ","+cp_ToStrODBC(this.w_PACODNUM)+;
                  ","+cp_ToStrODBC(this.w_PCNOFLDC)+;
                  ","+cp_ToStrODBCNull(this.w_PACODDOC)+;
                  ","+cp_ToStrODBC(this.w_PCNOFLAT)+;
                  ","+cp_ToStrODBCNull(this.w_PACODPER)+;
                  ","+cp_ToStrODBC(this.w_PADESSUP)+;
                  ","+cp_ToStrODBCNull(this.w_PCTIPDOC)+;
                  ","+cp_ToStrODBCNull(this.w_PCTIPATT)+;
                  ","+cp_ToStrODBCNull(this.w_PCGRUPAR)+;
                  ","+cp_ToStrODBC(this.w_PCESCRIN)+;
                  ","+cp_ToStrODBCNull(this.w_PCPERCON)+;
                  ","+cp_ToStrODBC(this.w_PCRINCON)+;
                  ","+cp_ToStrODBC(this.w_PCNUMGIO)+;
                  ","+cp_ToStrODBCNull(this.w_PACAUATT)+;
                  ","+cp_ToStrODBCNull(this.w_PACAUPRE)+;
                  ","+cp_ToStrODBCNull(this.w_PATIPUDI)+;
                  ","+cp_ToStrODBCNull(this.w_PACOSGIU)+;
                  ","+cp_ToStrODBC(this.w_PASTRING)+;
                  ","+cp_ToStrODBCNull(this.w_PATIPCTR)+;
                  ","+cp_ToStrODBC(this.w_PANGGINC)+;
                  ","+cp_ToStrODBC(this.w_PAOGGOUT)+;
                  ","+cp_ToStrODBC(this.w_PAFLAPPU)+;
                  ","+cp_ToStrODBC(this.w_PAFLATGE)+;
                  ","+cp_ToStrODBC(this.w_PAFLUDIE)+;
                  ","+cp_ToStrODBC(this.w_PAFLSEST)+;
                  ","+cp_ToStrODBC(this.w_PAFLASSE)+;
                  ","+cp_ToStrODBC(this.w_PAFLINPR)+;
                  ","+cp_ToStrODBC(this.w_PAFLNOTE)+;
                  ","+cp_ToStrODBC(this.w_PAFLDAFA)+;
                  ","+cp_ToStrODBC(this.w_PAFLAPP1)+;
                  ","+cp_ToStrODBC(this.w_PAFLATG1)+;
                  ","+cp_ToStrODBC(this.w_PAFLUDI1)+;
                  ","+cp_ToStrODBC(this.w_PAFLSES1)+;
                  ","+cp_ToStrODBC(this.w_PAFLASS1)+;
                  ","+cp_ToStrODBC(this.w_PAFLINP1)+;
                  ","+cp_ToStrODBC(this.w_PAFLNOT1)+;
                  ","+cp_ToStrODBC(this.w_PAFLDAF1)+;
                  ","+cp_ToStrODBC(this.w_PAFLAPP2)+;
                  ","+cp_ToStrODBC(this.w_PAFLATG2)+;
                  ","+cp_ToStrODBC(this.w_PAFLUDI2)+;
                  ","+cp_ToStrODBC(this.w_PAFLSES2)+;
                  ","+cp_ToStrODBC(this.w_PAFLASS2)+;
                  ","+cp_ToStrODBC(this.w_PAFLINP2)+;
                  ","+cp_ToStrODBC(this.w_PAFLNOT2)+;
                  ","+cp_ToStrODBC(this.w_PAFLDAF2)+;
                  ","+cp_ToStrODBC(this.w_PAFLAPP3)+;
                  ","+cp_ToStrODBC(this.w_PAFLATG3)+;
                  ","+cp_ToStrODBC(this.w_PAFLUDI3)+;
                  ","+cp_ToStrODBC(this.w_PAFLSES3)+;
                  ","+cp_ToStrODBC(this.w_PAFLASS3)+;
                  ","+cp_ToStrODBC(this.w_PAFLINP3)+;
                  ","+cp_ToStrODBC(this.w_PAFLNOT3)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_PAFLDAF3)+;
                  ","+cp_ToStrODBC(this.w_PAOGGMOD)+;
                  ","+cp_ToStrODBC(this.w_PATXTMOD)+;
                  ","+cp_ToStrODBC(this.w_PAPSTMOD)+;
                  ","+cp_ToStrODBC(this.w_PAOGGDEL)+;
                  ","+cp_ToStrODBC(this.w_PATXTDEL)+;
                  ","+cp_ToStrODBC(this.w_PAPSTDEL)+;
                  ","+cp_ToStrODBC(this.w_PAFLR_AG)+;
                  ","+cp_ToStrODBC(this.w_PADATFIS)+;
                  ","+cp_ToStrODBC(this.w_PADESATT)+;
                  ","+cp_ToStrODBC(this.w_PADESDOC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_AGEN')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_AGEN')
        cp_CheckDeletedKey(i_cTable,0,'PACODAZI',this.w_PACODAZI)
        INSERT INTO (i_cTable);
              (PACODAZI,PACODLIS,PALISACQ,PANOFLAT,PADATATP,PADATATS,PAFLCOSE,PADACOSE,PA_ACOSE,PANOFLPR,PADATPRP,PADATPRS,PAGIOSCA,PAFLRDOC,PACHKFES,PACHKPRE,PACALEND,PACHKCAR,PAVISTRC,PATIPAPI,PATIPAPF,PATIPCOS,PATIPNOT,PATIPTEL,PATIPASS,PATIPGEN,PAFLVISI,PAFLANTI,PAGESRIS,PADESRIS,PAST_ATT,PAOGGCRE,PATXTCRE,PAPSTCRE,PACHKCPL,PACHKING,PADATINI,PAGIOCHK,PACODMDC,PCCAUABB,PACALDAT,PACHKATT,PAFLESAT,PAFLDTRP,PAATTRIS,PAFLSIAT,PAFLPRES,PACODMOD,PACODNUM,PCNOFLDC,PACODDOC,PCNOFLAT,PACODPER,PADESSUP,PCTIPDOC,PCTIPATT,PCGRUPAR,PCESCRIN,PCPERCON,PCRINCON,PCNUMGIO,PACAUATT,PACAUPRE,PATIPUDI,PACOSGIU,PASTRING,PATIPCTR,PANGGINC,PAOGGOUT,PAFLAPPU,PAFLATGE,PAFLUDIE,PAFLSEST,PAFLASSE,PAFLINPR,PAFLNOTE,PAFLDAFA,PAFLAPP1,PAFLATG1,PAFLUDI1,PAFLSES1,PAFLASS1,PAFLINP1,PAFLNOT1,PAFLDAF1,PAFLAPP2,PAFLATG2,PAFLUDI2,PAFLSES2,PAFLASS2,PAFLINP2,PAFLNOT2,PAFLDAF2,PAFLAPP3,PAFLATG3,PAFLUDI3,PAFLSES3,PAFLASS3,PAFLINP3,PAFLNOT3,PAFLDAF3,PAOGGMOD,PATXTMOD,PAPSTMOD,PAOGGDEL,PATXTDEL,PAPSTDEL,PAFLR_AG,PADATFIS,PADESATT,PADESDOC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PACODAZI;
                  ,this.w_PACODLIS;
                  ,this.w_PALISACQ;
                  ,this.w_PANOFLAT;
                  ,this.w_PADATATP;
                  ,this.w_PADATATS;
                  ,this.w_PAFLCOSE;
                  ,this.w_PADACOSE;
                  ,this.w_PA_ACOSE;
                  ,this.w_PANOFLPR;
                  ,this.w_PADATPRP;
                  ,this.w_PADATPRS;
                  ,this.w_PAGIOSCA;
                  ,this.w_PAFLRDOC;
                  ,this.w_PACHKFES;
                  ,this.w_PACHKPRE;
                  ,this.w_PACALEND;
                  ,this.w_PACHKCAR;
                  ,this.w_PAVISTRC;
                  ,this.w_PATIPAPI;
                  ,this.w_PATIPAPF;
                  ,this.w_PATIPCOS;
                  ,this.w_PATIPNOT;
                  ,this.w_PATIPTEL;
                  ,this.w_PATIPASS;
                  ,this.w_PATIPGEN;
                  ,this.w_PAFLVISI;
                  ,this.w_PAFLANTI;
                  ,this.w_PAGESRIS;
                  ,this.w_PADESRIS;
                  ,this.w_PAST_ATT;
                  ,this.w_PAOGGCRE;
                  ,this.w_PATXTCRE;
                  ,this.w_PAPSTCRE;
                  ,this.w_PACHKCPL;
                  ,this.w_PACHKING;
                  ,this.w_PADATINI;
                  ,this.w_PAGIOCHK;
                  ,this.w_PACODMDC;
                  ,this.w_PCCAUABB;
                  ,this.w_PACALDAT;
                  ,this.w_PACHKATT;
                  ,this.w_PAFLESAT;
                  ,this.w_PAFLDTRP;
                  ,this.w_PAATTRIS;
                  ,this.w_PAFLSIAT;
                  ,this.w_PAFLPRES;
                  ,this.w_PACODMOD;
                  ,this.w_PACODNUM;
                  ,this.w_PCNOFLDC;
                  ,this.w_PACODDOC;
                  ,this.w_PCNOFLAT;
                  ,this.w_PACODPER;
                  ,this.w_PADESSUP;
                  ,this.w_PCTIPDOC;
                  ,this.w_PCTIPATT;
                  ,this.w_PCGRUPAR;
                  ,this.w_PCESCRIN;
                  ,this.w_PCPERCON;
                  ,this.w_PCRINCON;
                  ,this.w_PCNUMGIO;
                  ,this.w_PACAUATT;
                  ,this.w_PACAUPRE;
                  ,this.w_PATIPUDI;
                  ,this.w_PACOSGIU;
                  ,this.w_PASTRING;
                  ,this.w_PATIPCTR;
                  ,this.w_PANGGINC;
                  ,this.w_PAOGGOUT;
                  ,this.w_PAFLAPPU;
                  ,this.w_PAFLATGE;
                  ,this.w_PAFLUDIE;
                  ,this.w_PAFLSEST;
                  ,this.w_PAFLASSE;
                  ,this.w_PAFLINPR;
                  ,this.w_PAFLNOTE;
                  ,this.w_PAFLDAFA;
                  ,this.w_PAFLAPP1;
                  ,this.w_PAFLATG1;
                  ,this.w_PAFLUDI1;
                  ,this.w_PAFLSES1;
                  ,this.w_PAFLASS1;
                  ,this.w_PAFLINP1;
                  ,this.w_PAFLNOT1;
                  ,this.w_PAFLDAF1;
                  ,this.w_PAFLAPP2;
                  ,this.w_PAFLATG2;
                  ,this.w_PAFLUDI2;
                  ,this.w_PAFLSES2;
                  ,this.w_PAFLASS2;
                  ,this.w_PAFLINP2;
                  ,this.w_PAFLNOT2;
                  ,this.w_PAFLDAF2;
                  ,this.w_PAFLAPP3;
                  ,this.w_PAFLATG3;
                  ,this.w_PAFLUDI3;
                  ,this.w_PAFLSES3;
                  ,this.w_PAFLASS3;
                  ,this.w_PAFLINP3;
                  ,this.w_PAFLNOT3;
                  ,this.w_PAFLDAF3;
                  ,this.w_PAOGGMOD;
                  ,this.w_PATXTMOD;
                  ,this.w_PAPSTMOD;
                  ,this.w_PAOGGDEL;
                  ,this.w_PATXTDEL;
                  ,this.w_PAPSTDEL;
                  ,this.w_PAFLR_AG;
                  ,this.w_PADATFIS;
                  ,this.w_PADESATT;
                  ,this.w_PADESDOC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAR_AGEN_IDX,i_nConn)
      *
      * update PAR_AGEN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_AGEN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PACODLIS="+cp_ToStrODBCNull(this.w_PACODLIS)+;
             ",PALISACQ="+cp_ToStrODBCNull(this.w_PALISACQ)+;
             ",PANOFLAT="+cp_ToStrODBC(this.w_PANOFLAT)+;
             ",PADATATP="+cp_ToStrODBC(this.w_PADATATP)+;
             ",PADATATS="+cp_ToStrODBC(this.w_PADATATS)+;
             ",PAFLCOSE="+cp_ToStrODBC(this.w_PAFLCOSE)+;
             ",PADACOSE="+cp_ToStrODBC(this.w_PADACOSE)+;
             ",PA_ACOSE="+cp_ToStrODBC(this.w_PA_ACOSE)+;
             ",PANOFLPR="+cp_ToStrODBC(this.w_PANOFLPR)+;
             ",PADATPRP="+cp_ToStrODBC(this.w_PADATPRP)+;
             ",PADATPRS="+cp_ToStrODBC(this.w_PADATPRS)+;
             ",PAGIOSCA="+cp_ToStrODBC(this.w_PAGIOSCA)+;
             ",PAFLRDOC="+cp_ToStrODBC(this.w_PAFLRDOC)+;
             ",PACHKFES="+cp_ToStrODBC(this.w_PACHKFES)+;
             ",PACHKPRE="+cp_ToStrODBC(this.w_PACHKPRE)+;
             ",PACALEND="+cp_ToStrODBCNull(this.w_PACALEND)+;
             ",PACHKCAR="+cp_ToStrODBC(this.w_PACHKCAR)+;
             ",PAVISTRC="+cp_ToStrODBC(this.w_PAVISTRC)+;
             ",PATIPAPI="+cp_ToStrODBC(this.w_PATIPAPI)+;
             ",PATIPAPF="+cp_ToStrODBC(this.w_PATIPAPF)+;
             ",PATIPCOS="+cp_ToStrODBC(this.w_PATIPCOS)+;
             ",PATIPNOT="+cp_ToStrODBC(this.w_PATIPNOT)+;
             ",PATIPTEL="+cp_ToStrODBC(this.w_PATIPTEL)+;
             ",PATIPASS="+cp_ToStrODBC(this.w_PATIPASS)+;
             ",PATIPGEN="+cp_ToStrODBC(this.w_PATIPGEN)+;
             ",PAFLVISI="+cp_ToStrODBC(this.w_PAFLVISI)+;
             ",PAFLANTI="+cp_ToStrODBC(this.w_PAFLANTI)+;
             ",PAGESRIS="+cp_ToStrODBC(this.w_PAGESRIS)+;
             ",PADESRIS="+cp_ToStrODBC(this.w_PADESRIS)+;
             ",PAST_ATT="+cp_ToStrODBC(this.w_PAST_ATT)+;
             ",PAOGGCRE="+cp_ToStrODBC(this.w_PAOGGCRE)+;
             ",PATXTCRE="+cp_ToStrODBC(this.w_PATXTCRE)+;
             ",PAPSTCRE="+cp_ToStrODBC(this.w_PAPSTCRE)+;
             ",PACHKCPL="+cp_ToStrODBC(this.w_PACHKCPL)+;
             ",PACHKING="+cp_ToStrODBC(this.w_PACHKING)+;
             ",PADATINI="+cp_ToStrODBC(this.w_PADATINI)+;
             ",PAGIOCHK="+cp_ToStrODBC(this.w_PAGIOCHK)+;
             ",PACODMDC="+cp_ToStrODBCNull(this.w_PACODMDC)+;
             ",PCCAUABB="+cp_ToStrODBCNull(this.w_PCCAUABB)+;
             ",PACALDAT="+cp_ToStrODBC(this.w_PACALDAT)+;
             ",PACHKATT="+cp_ToStrODBC(this.w_PACHKATT)+;
             ",PAFLESAT="+cp_ToStrODBC(this.w_PAFLESAT)+;
             ",PAFLDTRP="+cp_ToStrODBC(this.w_PAFLDTRP)+;
             ",PAATTRIS="+cp_ToStrODBCNull(this.w_PAATTRIS)+;
             ",PAFLSIAT="+cp_ToStrODBC(this.w_PAFLSIAT)+;
             ",PAFLPRES="+cp_ToStrODBC(this.w_PAFLPRES)+;
             ",PACODMOD="+cp_ToStrODBCNull(this.w_PACODMOD)+;
             ",PACODNUM="+cp_ToStrODBC(this.w_PACODNUM)+;
             ",PCNOFLDC="+cp_ToStrODBC(this.w_PCNOFLDC)+;
             ",PACODDOC="+cp_ToStrODBCNull(this.w_PACODDOC)+;
             ",PCNOFLAT="+cp_ToStrODBC(this.w_PCNOFLAT)+;
             ",PACODPER="+cp_ToStrODBCNull(this.w_PACODPER)+;
             ",PADESSUP="+cp_ToStrODBC(this.w_PADESSUP)+;
             ",PCTIPDOC="+cp_ToStrODBCNull(this.w_PCTIPDOC)+;
             ",PCTIPATT="+cp_ToStrODBCNull(this.w_PCTIPATT)+;
             ",PCGRUPAR="+cp_ToStrODBCNull(this.w_PCGRUPAR)+;
             ",PCESCRIN="+cp_ToStrODBC(this.w_PCESCRIN)+;
             ",PCPERCON="+cp_ToStrODBCNull(this.w_PCPERCON)+;
             ",PCRINCON="+cp_ToStrODBC(this.w_PCRINCON)+;
             ",PCNUMGIO="+cp_ToStrODBC(this.w_PCNUMGIO)+;
             ",PACAUATT="+cp_ToStrODBCNull(this.w_PACAUATT)+;
             ",PACAUPRE="+cp_ToStrODBCNull(this.w_PACAUPRE)+;
             ",PATIPUDI="+cp_ToStrODBCNull(this.w_PATIPUDI)+;
             ",PACOSGIU="+cp_ToStrODBCNull(this.w_PACOSGIU)+;
             ",PASTRING="+cp_ToStrODBC(this.w_PASTRING)+;
             ",PATIPCTR="+cp_ToStrODBCNull(this.w_PATIPCTR)+;
             ",PANGGINC="+cp_ToStrODBC(this.w_PANGGINC)+;
             ",PAOGGOUT="+cp_ToStrODBC(this.w_PAOGGOUT)+;
             ",PAFLAPPU="+cp_ToStrODBC(this.w_PAFLAPPU)+;
             ",PAFLATGE="+cp_ToStrODBC(this.w_PAFLATGE)+;
             ",PAFLUDIE="+cp_ToStrODBC(this.w_PAFLUDIE)+;
             ",PAFLSEST="+cp_ToStrODBC(this.w_PAFLSEST)+;
             ",PAFLASSE="+cp_ToStrODBC(this.w_PAFLASSE)+;
             ",PAFLINPR="+cp_ToStrODBC(this.w_PAFLINPR)+;
             ",PAFLNOTE="+cp_ToStrODBC(this.w_PAFLNOTE)+;
             ",PAFLDAFA="+cp_ToStrODBC(this.w_PAFLDAFA)+;
             ",PAFLAPP1="+cp_ToStrODBC(this.w_PAFLAPP1)+;
             ",PAFLATG1="+cp_ToStrODBC(this.w_PAFLATG1)+;
             ",PAFLUDI1="+cp_ToStrODBC(this.w_PAFLUDI1)+;
             ",PAFLSES1="+cp_ToStrODBC(this.w_PAFLSES1)+;
             ",PAFLASS1="+cp_ToStrODBC(this.w_PAFLASS1)+;
             ",PAFLINP1="+cp_ToStrODBC(this.w_PAFLINP1)+;
             ",PAFLNOT1="+cp_ToStrODBC(this.w_PAFLNOT1)+;
             ",PAFLDAF1="+cp_ToStrODBC(this.w_PAFLDAF1)+;
             ",PAFLAPP2="+cp_ToStrODBC(this.w_PAFLAPP2)+;
             ",PAFLATG2="+cp_ToStrODBC(this.w_PAFLATG2)+;
             ",PAFLUDI2="+cp_ToStrODBC(this.w_PAFLUDI2)+;
             ",PAFLSES2="+cp_ToStrODBC(this.w_PAFLSES2)+;
             ",PAFLASS2="+cp_ToStrODBC(this.w_PAFLASS2)+;
             ",PAFLINP2="+cp_ToStrODBC(this.w_PAFLINP2)+;
             ",PAFLNOT2="+cp_ToStrODBC(this.w_PAFLNOT2)+;
             ",PAFLDAF2="+cp_ToStrODBC(this.w_PAFLDAF2)+;
             ",PAFLAPP3="+cp_ToStrODBC(this.w_PAFLAPP3)+;
             ",PAFLATG3="+cp_ToStrODBC(this.w_PAFLATG3)+;
             ",PAFLUDI3="+cp_ToStrODBC(this.w_PAFLUDI3)+;
             ",PAFLSES3="+cp_ToStrODBC(this.w_PAFLSES3)+;
             ",PAFLASS3="+cp_ToStrODBC(this.w_PAFLASS3)+;
             ",PAFLINP3="+cp_ToStrODBC(this.w_PAFLINP3)+;
             ",PAFLNOT3="+cp_ToStrODBC(this.w_PAFLNOT3)+;
             ",PAFLDAF3="+cp_ToStrODBC(this.w_PAFLDAF3)+;
             ""
             i_nnn=i_nnn+;
             ",PAOGGMOD="+cp_ToStrODBC(this.w_PAOGGMOD)+;
             ",PATXTMOD="+cp_ToStrODBC(this.w_PATXTMOD)+;
             ",PAPSTMOD="+cp_ToStrODBC(this.w_PAPSTMOD)+;
             ",PAOGGDEL="+cp_ToStrODBC(this.w_PAOGGDEL)+;
             ",PATXTDEL="+cp_ToStrODBC(this.w_PATXTDEL)+;
             ",PAPSTDEL="+cp_ToStrODBC(this.w_PAPSTDEL)+;
             ",PAFLR_AG="+cp_ToStrODBC(this.w_PAFLR_AG)+;
             ",PADATFIS="+cp_ToStrODBC(this.w_PADATFIS)+;
             ",PADESATT="+cp_ToStrODBC(this.w_PADESATT)+;
             ",PADESDOC="+cp_ToStrODBC(this.w_PADESDOC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_AGEN')
        i_cWhere = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  )
        UPDATE (i_cTable) SET;
              PACODLIS=this.w_PACODLIS;
             ,PALISACQ=this.w_PALISACQ;
             ,PANOFLAT=this.w_PANOFLAT;
             ,PADATATP=this.w_PADATATP;
             ,PADATATS=this.w_PADATATS;
             ,PAFLCOSE=this.w_PAFLCOSE;
             ,PADACOSE=this.w_PADACOSE;
             ,PA_ACOSE=this.w_PA_ACOSE;
             ,PANOFLPR=this.w_PANOFLPR;
             ,PADATPRP=this.w_PADATPRP;
             ,PADATPRS=this.w_PADATPRS;
             ,PAGIOSCA=this.w_PAGIOSCA;
             ,PAFLRDOC=this.w_PAFLRDOC;
             ,PACHKFES=this.w_PACHKFES;
             ,PACHKPRE=this.w_PACHKPRE;
             ,PACALEND=this.w_PACALEND;
             ,PACHKCAR=this.w_PACHKCAR;
             ,PAVISTRC=this.w_PAVISTRC;
             ,PATIPAPI=this.w_PATIPAPI;
             ,PATIPAPF=this.w_PATIPAPF;
             ,PATIPCOS=this.w_PATIPCOS;
             ,PATIPNOT=this.w_PATIPNOT;
             ,PATIPTEL=this.w_PATIPTEL;
             ,PATIPASS=this.w_PATIPASS;
             ,PATIPGEN=this.w_PATIPGEN;
             ,PAFLVISI=this.w_PAFLVISI;
             ,PAFLANTI=this.w_PAFLANTI;
             ,PAGESRIS=this.w_PAGESRIS;
             ,PADESRIS=this.w_PADESRIS;
             ,PAST_ATT=this.w_PAST_ATT;
             ,PAOGGCRE=this.w_PAOGGCRE;
             ,PATXTCRE=this.w_PATXTCRE;
             ,PAPSTCRE=this.w_PAPSTCRE;
             ,PACHKCPL=this.w_PACHKCPL;
             ,PACHKING=this.w_PACHKING;
             ,PADATINI=this.w_PADATINI;
             ,PAGIOCHK=this.w_PAGIOCHK;
             ,PACODMDC=this.w_PACODMDC;
             ,PCCAUABB=this.w_PCCAUABB;
             ,PACALDAT=this.w_PACALDAT;
             ,PACHKATT=this.w_PACHKATT;
             ,PAFLESAT=this.w_PAFLESAT;
             ,PAFLDTRP=this.w_PAFLDTRP;
             ,PAATTRIS=this.w_PAATTRIS;
             ,PAFLSIAT=this.w_PAFLSIAT;
             ,PAFLPRES=this.w_PAFLPRES;
             ,PACODMOD=this.w_PACODMOD;
             ,PACODNUM=this.w_PACODNUM;
             ,PCNOFLDC=this.w_PCNOFLDC;
             ,PACODDOC=this.w_PACODDOC;
             ,PCNOFLAT=this.w_PCNOFLAT;
             ,PACODPER=this.w_PACODPER;
             ,PADESSUP=this.w_PADESSUP;
             ,PCTIPDOC=this.w_PCTIPDOC;
             ,PCTIPATT=this.w_PCTIPATT;
             ,PCGRUPAR=this.w_PCGRUPAR;
             ,PCESCRIN=this.w_PCESCRIN;
             ,PCPERCON=this.w_PCPERCON;
             ,PCRINCON=this.w_PCRINCON;
             ,PCNUMGIO=this.w_PCNUMGIO;
             ,PACAUATT=this.w_PACAUATT;
             ,PACAUPRE=this.w_PACAUPRE;
             ,PATIPUDI=this.w_PATIPUDI;
             ,PACOSGIU=this.w_PACOSGIU;
             ,PASTRING=this.w_PASTRING;
             ,PATIPCTR=this.w_PATIPCTR;
             ,PANGGINC=this.w_PANGGINC;
             ,PAOGGOUT=this.w_PAOGGOUT;
             ,PAFLAPPU=this.w_PAFLAPPU;
             ,PAFLATGE=this.w_PAFLATGE;
             ,PAFLUDIE=this.w_PAFLUDIE;
             ,PAFLSEST=this.w_PAFLSEST;
             ,PAFLASSE=this.w_PAFLASSE;
             ,PAFLINPR=this.w_PAFLINPR;
             ,PAFLNOTE=this.w_PAFLNOTE;
             ,PAFLDAFA=this.w_PAFLDAFA;
             ,PAFLAPP1=this.w_PAFLAPP1;
             ,PAFLATG1=this.w_PAFLATG1;
             ,PAFLUDI1=this.w_PAFLUDI1;
             ,PAFLSES1=this.w_PAFLSES1;
             ,PAFLASS1=this.w_PAFLASS1;
             ,PAFLINP1=this.w_PAFLINP1;
             ,PAFLNOT1=this.w_PAFLNOT1;
             ,PAFLDAF1=this.w_PAFLDAF1;
             ,PAFLAPP2=this.w_PAFLAPP2;
             ,PAFLATG2=this.w_PAFLATG2;
             ,PAFLUDI2=this.w_PAFLUDI2;
             ,PAFLSES2=this.w_PAFLSES2;
             ,PAFLASS2=this.w_PAFLASS2;
             ,PAFLINP2=this.w_PAFLINP2;
             ,PAFLNOT2=this.w_PAFLNOT2;
             ,PAFLDAF2=this.w_PAFLDAF2;
             ,PAFLAPP3=this.w_PAFLAPP3;
             ,PAFLATG3=this.w_PAFLATG3;
             ,PAFLUDI3=this.w_PAFLUDI3;
             ,PAFLSES3=this.w_PAFLSES3;
             ,PAFLASS3=this.w_PAFLASS3;
             ,PAFLINP3=this.w_PAFLINP3;
             ,PAFLNOT3=this.w_PAFLNOT3;
             ,PAFLDAF3=this.w_PAFLDAF3;
             ,PAOGGMOD=this.w_PAOGGMOD;
             ,PATXTMOD=this.w_PATXTMOD;
             ,PAPSTMOD=this.w_PAPSTMOD;
             ,PAOGGDEL=this.w_PAOGGDEL;
             ,PATXTDEL=this.w_PATXTDEL;
             ,PAPSTDEL=this.w_PAPSTDEL;
             ,PAFLR_AG=this.w_PAFLR_AG;
             ,PADATFIS=this.w_PADATFIS;
             ,PADESATT=this.w_PADESATT;
             ,PADESDOC=this.w_PADESDOC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- gsag_mat : Saving
      this.gsag_mat.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PACODAZI,"PTCODAZI";
             )
      this.gsag_mat.mReplace()
      * --- GSAG_MCD : Saving
      this.GSAG_MCD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PACODAZI,"CDCODAZI";
             )
      this.GSAG_MCD.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsag_apa
    * aggiorno variabili globali
    g_gesris=this.w_pagesris
    g_rismsg=this.w_padesris
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- gsag_mat : Deleting
    this.gsag_mat.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PACODAZI,"PTCODAZI";
           )
    this.gsag_mat.mDelete()
    * --- GSAG_MCD : Deleting
    this.GSAG_MCD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PACODAZI,"CDCODAZI";
           )
    this.GSAG_MCD.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAR_AGEN_IDX,i_nConn)
      *
      * delete PAR_AGEN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PACODAZI',this.w_PACODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    if i_bUpd
      with this
            .w_ATTIVACQ = iif((Isahe() and g_CACQ='S') or (Isahr() and g_ACQU='S'),'S','N')
          .link_1_2('Full')
        .DoRTCalc(3,4,.t.)
            .w_MVCODVAL = g_PERVAL
        .DoRTCalc(6,6,.t.)
            .w_DATALIST = i_DATSYS
        .DoRTCalc(8,12,.t.)
        if .o_PANOFLAT<>.w_PANOFLAT
            .w_PADATATP = IIF(.w_PANOFLAT$'ENI', 0, .w_PADATATP)
        endif
        if .o_PANOFLAT<>.w_PANOFLAT
            .w_PADATATS = IIF(.w_PANOFLAT$ 'ENF', 0, .w_PADATATS)
        endif
        .DoRTCalc(15,15,.t.)
        if .o_PAFLCOSE<>.w_PAFLCOSE
            .w_PADACOSE = IIF(.w_PAFLCOSE$'ENI', 0, .w_PADACOSE)
        endif
        if .o_PAFLCOSE<>.w_PAFLCOSE
            .w_PA_ACOSE = IIF(.w_PAFLCOSE$ 'ENF', 0, .w_PA_ACOSE)
        endif
        .DoRTCalc(18,18,.t.)
        if .o_PANOFLPR<>.w_PANOFLPR
            .w_PADATPRP = IIF(.w_PANOFLPR$'ENI', 0, .w_PADATPRP)
        endif
        if .o_PANOFLPR<>.w_PANOFLPR
            .w_PADATPRS = IIF(.w_PANOFLPR$ 'ENF', 0, .w_PADATPRS)
        endif
        .DoRTCalc(21,24,.t.)
        if .o_PACHKFES<>.w_PACHKFES
            .w_PACALEND = iif(.w_PACHKFES='C',GETPREDCAL(),Space(5))
          .link_1_26('Full')
        endif
        .DoRTCalc(26,38,.t.)
        if .o_PACODAZI<>.w_PACODAZI
            .w_OLDFLVISI = .w_PAFLVISI
        endif
        .DoRTCalc(40,47,.t.)
        if .o_PAGESRIS<>.w_PAGESRIS
            .w_PADESRIS = IIF( .w_PAGESRIS<>'S' , '' , Cp_translate('Riservato'))
        endif
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page2.oPag.oObj_2_7.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        if .o_PACHKING<>.w_PACHKING
          .Calculate_HDGHWRKUWL()
        endif
        if .o_PACOSGIU<>.w_PACOSGIU
          .Calculate_ZWPRYSLSYX()
        endif
        .DoRTCalc(49,68,.t.)
        if .o_PAFLESAT<>.w_PAFLESAT
            .w_PAATTRIS = Space(20)
          .link_1_92('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_2_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .DoRTCalc(70,76,.t.)
        if .o_PACODDOC<>.w_PACODDOC
            .w_PCNOFLDC = iif(empty(.w_PACODDOC),'N', .w_PCNOFLDC)
        endif
        .DoRTCalc(78,78,.t.)
        if .o_PACODPER<>.w_PACODPER
            .w_PCNOFLAT = iif(empty(.w_PACODPER),'N', .w_PCNOFLAT)
        endif
        .DoRTCalc(80,104,.t.)
          .link_6_6('Full')
        if .o_PATIPCTR<>.w_PATIPCTR
          .Calculate_KHYPUAKKDE()
        endif
        .oPgFrm.Page5.oPag.oObj_5_1.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page5.oPag.oObj_5_2.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page5.oPag.oObj_5_3.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page6.oPag.oObj_6_45.Calculate(IIF(IsAlt(),"GSAG_APA_Outl2","GSAG_APA_Outl"))
        .oPgFrm.Page3.oPag.oObj_3_9.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page3.oPag.oObj_3_10.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page3.oPag.oObj_3_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page4.oPag.oObj_4_9.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page4.oPag.oObj_4_10.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page4.oPag.oObj_4_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(106,169,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_6.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page2.oPag.oObj_2_7.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page2.oPag.oObj_2_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page5.oPag.oObj_5_1.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page5.oPag.oObj_5_2.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page5.oPag.oObj_5_3.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page6.oPag.oObj_6_45.Calculate(IIF(IsAlt(),"GSAG_APA_Outl2","GSAG_APA_Outl"))
        .oPgFrm.Page3.oPag.oObj_3_9.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page3.oPag.oObj_3_10.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page3.oPag.oObj_3_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
        .oPgFrm.Page4.oPag.oObj_4_9.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page4.oPag.oObj_4_10.Calculate(IIF(IsAlt(),"GSAG_APA_E_Mail2","GSAG_APA_E_Mail"))
        .oPgFrm.Page4.oPag.oObj_4_11.Calculate(IIF(IsAlt(),"GSAG_APA_Postin2","GSAG_APA_Postin"))
    endwith
  return

  proc Calculate_TGFMBVZEAZ()
    with this
          * --- Verifica modalit� di visualizzazione impostata
          gsag_bpa(this;
              ,'A';
             )
    endwith
  endproc
  proc Calculate_JJYKXKYSKY()
    with this
          * --- Controlli al cambio modalit� di visualizzazione
          gsag_bpa(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_GMBHURYYXE()
    with this
          * --- Controlli al cambio modalit� di riservato
          gsag_bpa(this;
              ,'R';
             )
    endwith
  endproc
  proc Calculate_HDGHWRKUWL()
    with this
          * --- Azzera campi
          .w_PAGIOCHK = IIF(.w_PACHKING<>'A', 0, .w_PAGIOCHK)
          .w_PADATINI = IIF(.w_PACHKING<>'F', Cp_CharToDate('  -  -    '), .w_PADATINI)
    endwith
  endproc
  proc Calculate_ZWPRYSLSYX()
    with this
          * --- Azzera w_PASTRING
          .w_PASTRING = IIF(EMPTY(.w_PACOSGIU), space(14), .w_PASTRING)
    endwith
  endproc
  proc Calculate_HBWXDEHEWF()
    with this
          * --- Sbianca periodicit� - rinnovo tacito - giorni di preavviso
          .w_PCPERCON = IIF( .w_PCESCRIN = "S", "     ", .w_PCPERCON )
          .w_PCRINCON = IIF( .w_PCESCRIN = "S", "N", .w_PCRINCON )
          .w_PCNUMGIO = IIF( .w_PCESCRIN = "S", 0, .w_PCNUMGIO )
          .w_MDDESCRI = IIF( .w_PCESCRIN = "S", SPACE( 50 ), .w_MDDESCRI )
          .w_PADATFIS = IIF( .w_PCESCRIN = "S", " ", .w_PADATFIS )
    endwith
  endproc
  proc Calculate_KHYPUAKKDE()
    with this
          * --- Aggiorna PANGGINC
          .w_PANGGINC = IIF(empty(.w_PATIPCTR), 0, .w_PANGGINC)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPADATATP_1_14.enabled = this.oPgFrm.Page1.oPag.oPADATATP_1_14.mCond()
    this.oPgFrm.Page1.oPag.oPADATATS_1_15.enabled = this.oPgFrm.Page1.oPag.oPADATATS_1_15.mCond()
    this.oPgFrm.Page1.oPag.oPADACOSE_1_17.enabled = this.oPgFrm.Page1.oPag.oPADACOSE_1_17.mCond()
    this.oPgFrm.Page1.oPag.oPA_ACOSE_1_18.enabled = this.oPgFrm.Page1.oPag.oPA_ACOSE_1_18.mCond()
    this.oPgFrm.Page1.oPag.oPADATPRP_1_20.enabled = this.oPgFrm.Page1.oPag.oPADATPRP_1_20.mCond()
    this.oPgFrm.Page1.oPag.oPADATPRS_1_21.enabled = this.oPgFrm.Page1.oPag.oPADATPRS_1_21.mCond()
    this.oPgFrm.Page1.oPag.oPACALEND_1_26.enabled = this.oPgFrm.Page1.oPag.oPACALEND_1_26.mCond()
    this.oPgFrm.Page1.oPag.oPADESRIS_1_62.enabled = this.oPgFrm.Page1.oPag.oPADESRIS_1_62.mCond()
    this.oPgFrm.Page7.oPag.oPCPERCON_7_38.enabled = this.oPgFrm.Page7.oPag.oPCPERCON_7_38.mCond()
    this.oPgFrm.Page7.oPag.oPCRINCON_7_41.enabled = this.oPgFrm.Page7.oPag.oPCRINCON_7_41.mCond()
    this.oPgFrm.Page7.oPag.oPCNUMGIO_7_42.enabled = this.oPgFrm.Page7.oPag.oPCNUMGIO_7_42.mCond()
    this.oPgFrm.Page6.oPag.oPASTRING_6_8.enabled = this.oPgFrm.Page6.oPag.oPASTRING_6_8.mCond()
    this.oPgFrm.Page6.oPag.oPANGGINC_6_10.enabled = this.oPgFrm.Page6.oPag.oPANGGINC_6_10.mCond()
    this.oPgFrm.Page7.oPag.oPADATFIS_7_48.enabled = this.oPgFrm.Page7.oPag.oPADATFIS_7_48.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(7).enabled=not(IsAlt())
    local i_show6
    i_show6=not(IsAlt())
    this.oPgFrm.Pages(7).enabled=i_show6 and not(IsAlt())
    this.oPgFrm.Pages(7).caption=iif(i_show6,cp_translate("Impianti e contratti"),"")
    this.oPgFrm.Pages(7).oPag.visible=this.oPgFrm.Pages(7).enabled
    this.oPgFrm.Page1.oPag.oPACODLIS_1_8.visible=!this.oPgFrm.Page1.oPag.oPACODLIS_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oPACODLIS_1_10.visible=!this.oPgFrm.Page1.oPag.oPACODLIS_1_10.mHide()
    this.oPgFrm.Page1.oPag.oPALISACQ_1_11.visible=!this.oPgFrm.Page1.oPag.oPALISACQ_1_11.mHide()
    this.oPgFrm.Page1.oPag.oRDESLIS_1_12.visible=!this.oPgFrm.Page1.oPag.oRDESLIS_1_12.mHide()
    this.oPgFrm.Page1.oPag.oPADATATP_1_14.visible=!this.oPgFrm.Page1.oPag.oPADATATP_1_14.mHide()
    this.oPgFrm.Page1.oPag.oPADATATS_1_15.visible=!this.oPgFrm.Page1.oPag.oPADATATS_1_15.mHide()
    this.oPgFrm.Page1.oPag.oPADACOSE_1_17.visible=!this.oPgFrm.Page1.oPag.oPADACOSE_1_17.mHide()
    this.oPgFrm.Page1.oPag.oPA_ACOSE_1_18.visible=!this.oPgFrm.Page1.oPag.oPA_ACOSE_1_18.mHide()
    this.oPgFrm.Page1.oPag.oPADATPRP_1_20.visible=!this.oPgFrm.Page1.oPag.oPADATPRP_1_20.mHide()
    this.oPgFrm.Page1.oPag.oPADATPRS_1_21.visible=!this.oPgFrm.Page1.oPag.oPADATPRS_1_21.mHide()
    this.oPgFrm.Page1.oPag.oPAGIOSCA_1_22.visible=!this.oPgFrm.Page1.oPag.oPAGIOSCA_1_22.mHide()
    this.oPgFrm.Page1.oPag.oPAFLRDOC_1_23.visible=!this.oPgFrm.Page1.oPag.oPAFLRDOC_1_23.mHide()
    this.oPgFrm.Page1.oPag.oPAVISTRC_1_28.visible=!this.oPgFrm.Page1.oPag.oPAVISTRC_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oPAFLANTI_1_46.visible=!this.oPgFrm.Page1.oPag.oPAFLANTI_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oDESACQ_1_56.visible=!this.oPgFrm.Page1.oPag.oDESACQ_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oEDESLIS_1_58.visible=!this.oPgFrm.Page1.oPag.oEDESLIS_1_58.mHide()
    this.oPgFrm.Page1.oPag.oPAST_ATT_1_63.visible=!this.oPgFrm.Page1.oPag.oPAST_ATT_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oPACHKCPL_1_69.visible=!this.oPgFrm.Page1.oPag.oPACHKCPL_1_69.mHide()
    this.oPgFrm.Page1.oPag.oPADATINI_1_71.visible=!this.oPgFrm.Page1.oPag.oPADATINI_1_71.mHide()
    this.oPgFrm.Page1.oPag.oPAGIOCHK_1_72.visible=!this.oPgFrm.Page1.oPag.oPAGIOCHK_1_72.mHide()
    this.oPgFrm.Page1.oPag.oPCCAUABB_1_75.visible=!this.oPgFrm.Page1.oPag.oPCCAUABB_1_75.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_79.visible=!this.oPgFrm.Page1.oPag.oStr_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_80.visible=!this.oPgFrm.Page1.oPag.oStr_1_80.mHide()
    this.oPgFrm.Page1.oPag.oDESCAU_1_82.visible=!this.oPgFrm.Page1.oPag.oDESCAU_1_82.mHide()
    this.oPgFrm.Page1.oPag.oPAFLESAT_1_90.visible=!this.oPgFrm.Page1.oPag.oPAFLESAT_1_90.mHide()
    this.oPgFrm.Page1.oPag.oPAATTRIS_1_92.visible=!this.oPgFrm.Page1.oPag.oPAATTRIS_1_92.mHide()
    this.oPgFrm.Page1.oPag.oPAFLPRES_1_94.visible=!this.oPgFrm.Page1.oPag.oPAFLPRES_1_94.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_95.visible=!this.oPgFrm.Page1.oPag.oStr_1_95.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_99.visible=!this.oPgFrm.Page1.oPag.oStr_1_99.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_100.visible=!this.oPgFrm.Page1.oPag.oStr_1_100.mHide()
    this.oPgFrm.Page7.oPag.oPACODMOD_7_4.visible=!this.oPgFrm.Page7.oPag.oPACODMOD_7_4.mHide()
    this.oPgFrm.Page7.oPag.oStr_7_5.visible=!this.oPgFrm.Page7.oPag.oStr_7_5.mHide()
    this.oPgFrm.Page7.oPag.oDESCRI_7_6.visible=!this.oPgFrm.Page7.oPag.oDESCRI_7_6.mHide()
    this.oPgFrm.Page7.oPag.oPACODNUM_7_7.visible=!this.oPgFrm.Page7.oPag.oPACODNUM_7_7.mHide()
    this.oPgFrm.Page6.oPag.oPACAUATT_6_1.visible=!this.oPgFrm.Page6.oPag.oPACAUATT_6_1.mHide()
    this.oPgFrm.Page6.oPag.oPACAUATT_6_4.visible=!this.oPgFrm.Page6.oPag.oPACAUATT_6_4.mHide()
    this.oPgFrm.Page6.oPag.oPACOSGIU_6_7.visible=!this.oPgFrm.Page6.oPag.oPACOSGIU_6_7.mHide()
    this.oPgFrm.Page6.oPag.oPASTRING_6_8.visible=!this.oPgFrm.Page6.oPag.oPASTRING_6_8.mHide()
    this.oPgFrm.Page6.oPag.oPATIPCTR_6_9.visible=!this.oPgFrm.Page6.oPag.oPATIPCTR_6_9.mHide()
    this.oPgFrm.Page6.oPag.oPANGGINC_6_10.visible=!this.oPgFrm.Page6.oPag.oPANGGINC_6_10.mHide()
    this.oPgFrm.Page6.oPag.oPATIPTEL_6_12.visible=!this.oPgFrm.Page6.oPag.oPATIPTEL_6_12.mHide()
    this.oPgFrm.Page6.oPag.oPATIPASS_6_13.visible=!this.oPgFrm.Page6.oPag.oPATIPASS_6_13.mHide()
    this.oPgFrm.Page6.oPag.oPATIPAPI_6_14.visible=!this.oPgFrm.Page6.oPag.oPATIPAPI_6_14.mHide()
    this.oPgFrm.Page6.oPag.oPATIPCOS_6_15.visible=!this.oPgFrm.Page6.oPag.oPATIPCOS_6_15.mHide()
    this.oPgFrm.Page6.oPag.oPATIPGEN_6_16.visible=!this.oPgFrm.Page6.oPag.oPATIPGEN_6_16.mHide()
    this.oPgFrm.Page6.oPag.oPATIPAPF_6_17.visible=!this.oPgFrm.Page6.oPag.oPATIPAPF_6_17.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_20.visible=!this.oPgFrm.Page6.oPag.oStr_6_20.mHide()
    this.oPgFrm.Page6.oPag.oDES_GEN_6_21.visible=!this.oPgFrm.Page6.oPag.oDES_GEN_6_21.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_22.visible=!this.oPgFrm.Page6.oPag.oStr_6_22.mHide()
    this.oPgFrm.Page6.oPag.oDES_TEL_6_23.visible=!this.oPgFrm.Page6.oPag.oDES_TEL_6_23.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_24.visible=!this.oPgFrm.Page6.oPag.oStr_6_24.mHide()
    this.oPgFrm.Page6.oPag.oDES_CHI_6_25.visible=!this.oPgFrm.Page6.oPag.oDES_CHI_6_25.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_26.visible=!this.oPgFrm.Page6.oPag.oStr_6_26.mHide()
    this.oPgFrm.Page6.oPag.oDES_API_6_27.visible=!this.oPgFrm.Page6.oPag.oDES_API_6_27.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_28.visible=!this.oPgFrm.Page6.oPag.oStr_6_28.mHide()
    this.oPgFrm.Page6.oPag.oDES_APF_6_29.visible=!this.oPgFrm.Page6.oPag.oDES_APF_6_29.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_30.visible=!this.oPgFrm.Page6.oPag.oStr_6_30.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_32.visible=!this.oPgFrm.Page6.oPag.oStr_6_32.mHide()
    this.oPgFrm.Page6.oPag.oLinkPC_6_33.visible=!this.oPgFrm.Page6.oPag.oLinkPC_6_33.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_34.visible=!this.oPgFrm.Page6.oPag.oStr_6_34.mHide()
    this.oPgFrm.Page6.oPag.oDES_DOC_6_35.visible=!this.oPgFrm.Page6.oPag.oDES_DOC_6_35.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_36.visible=!this.oPgFrm.Page6.oPag.oStr_6_36.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_38.visible=!this.oPgFrm.Page6.oPag.oStr_6_38.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_40.visible=!this.oPgFrm.Page6.oPag.oStr_6_40.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_41.visible=!this.oPgFrm.Page6.oPag.oStr_6_41.mHide()
    this.oPgFrm.Page6.oPag.oPADESINC_6_43.visible=!this.oPgFrm.Page6.oPag.oPADESINC_6_43.mHide()
    this.oPgFrm.Page6.oPag.oPAOGGOUT_6_44.visible=!this.oPgFrm.Page6.oPag.oPAOGGOUT_6_44.mHide()
    this.oPgFrm.Page5.oPag.oPAFLUDIE_5_8.visible=!this.oPgFrm.Page5.oPag.oPAFLUDIE_5_8.mHide()
    this.oPgFrm.Page5.oPag.oPAFLUDI1_5_18.visible=!this.oPgFrm.Page5.oPag.oPAFLUDI1_5_18.mHide()
    this.oPgFrm.Page5.oPag.oPAFLUDI2_5_28.visible=!this.oPgFrm.Page5.oPag.oPAFLUDI2_5_28.mHide()
    this.oPgFrm.Page5.oPag.oPAFLUDI3_5_38.visible=!this.oPgFrm.Page5.oPag.oPAFLUDI3_5_38.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_46.visible=!this.oPgFrm.Page6.oPag.oStr_6_46.mHide()
    this.oPgFrm.Page1.oPag.oPAFLR_AG_1_102.visible=!this.oPgFrm.Page1.oPag.oPAFLR_AG_1_102.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Insert end") or lower(cEvent)==lower("Update end")
          .Calculate_TGFMBVZEAZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_PAFLVISI Changed")
          .Calculate_JJYKXKYSKY()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_6.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_7.Event(cEvent)
        if lower(cEvent)==lower("w_PAGESRIS Changed")
          .Calculate_GMBHURYYXE()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_11.Event(cEvent)
        if lower(cEvent)==lower("w_PCESCRIN Changed")
          .Calculate_HBWXDEHEWF()
          bRefresh=.t.
        endif
      .oPgFrm.Page5.oPag.oObj_5_1.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_2.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_3.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_45.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_9.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_10.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_11.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_9.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_10.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PACODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_PACODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_PACODAZI)
            select AZCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODAZI = NVL(_Link_.AZCODAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PACODAZI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACODLIS
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_PACODLIS)+"%");
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_PERVAL);

          i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSVALLIS,LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSVALLIS',this.w_PERVAL;
                     ,'LSCODLIS',trim(this.w_PACODLIS))
          select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSVALLIS,LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_PACODLIS)+"%");
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_PERVAL);

            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_PACODLIS)+"%");
                   +" and LSVALLIS="+cp_ToStr(this.w_PERVAL);

            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(oSource.parent,'oPACODLIS_1_8'),i_cWhere,'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PERVAL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, listino gestito a sconti")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LSVALLIS="+cp_ToStrODBC(this.w_PERVAL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',oSource.xKey(1);
                       ,'LSCODLIS',oSource.xKey(2))
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_PACODLIS);
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_PERVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',this.w_PERVAL;
                       ,'LSCODLIS',this.w_PACODLIS)
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_RDESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_FLSCON = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PACODLIS = space(5)
      endif
      this.w_RDESLIS = space(40)
      this.w_FLSCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSVALLIS,1)+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACODLIS
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_PACODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_PACODLIS))
          select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_PACODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_PACODLIS)+"%");

            select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oPACODLIS_1_10'),i_cWhere,'',"Listini",'listdocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_PACODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_PACODLIS)
            select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_EDESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_FLGCIC = NVL(_Link_.LSFLGCIC,space(1))
      this.w_TIPLIS = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLSCON = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PACODLIS = space(5)
      endif
      this.w_EDESLIS = space(40)
      this.w_FLGCIC = space(1)
      this.w_TIPLIS = space(1)
      this.w_FLSCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_FLGCIC $ 'V-E' and .w_TIPLIS $ 'P-E' and IsAhe() ) or !Isahe()
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, listino non valido")
        endif
        this.w_PACODLIS = space(5)
        this.w_EDESLIS = space(40)
        this.w_FLGCIC = space(1)
        this.w_TIPLIS = space(1)
        this.w_FLSCON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.LSCODLIS as LSCODLIS110"+ ",link_1_10.LSDESLIS as LSDESLIS110"+ ",link_1_10.LSFLGCIC as LSFLGCIC110"+ ",link_1_10.LSTIPLIS as LSTIPLIS110"+ ",link_1_10.LSFLSCON as LSFLSCON110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on PAR_AGEN.PACODLIS=link_1_10.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and PAR_AGEN.PACODLIS=link_1_10.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PALISACQ
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PALISACQ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_PALISACQ)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_PALISACQ))
          select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PALISACQ)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_PALISACQ)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_PALISACQ)+"%");

            select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PALISACQ) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oPALISACQ_1_11'),i_cWhere,'',"Listini",'listdocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PALISACQ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_PALISACQ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_PALISACQ)
            select LSCODLIS,LSDESLIS,LSFLGCIC,LSTIPLIS,LSFLSCON,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PALISACQ = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESACQ = NVL(_Link_.LSDESLIS,space(40))
      this.w_CICLO = NVL(_Link_.LSFLGCIC,space(1))
      this.w_TIPLIS = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLSCON2 = NVL(_Link_.LSFLSCON,space(1))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PALISACQ = space(5)
      endif
      this.w_DESACQ = space(40)
      this.w_CICLO = space(1)
      this.w_TIPLIS = space(1)
      this.w_FLSCON2 = space(1)
      this.w_IVALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_CICLO $ 'A-E'  AND .w_TIPLIS='P' ) or (Isahr() AND .w_FLSCON2<>'S' )) AND .w_IVALIS<>'L'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, listino inesistente o di tipo Iva inclusa o di tipo sconti")
        endif
        this.w_PALISACQ = space(5)
        this.w_DESACQ = space(40)
        this.w_CICLO = space(1)
        this.w_TIPLIS = space(1)
        this.w_FLSCON2 = space(1)
        this.w_IVALIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PALISACQ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.LSCODLIS as LSCODLIS111"+ ",link_1_11.LSDESLIS as LSDESLIS111"+ ",link_1_11.LSFLGCIC as LSFLGCIC111"+ ",link_1_11.LSTIPLIS as LSTIPLIS111"+ ",link_1_11.LSFLSCON as LSFLSCON111"+ ",link_1_11.LSIVALIS as LSIVALIS111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on PAR_AGEN.PALISACQ=link_1_11.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and PAR_AGEN.PALISACQ=link_1_11.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACALEND
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACALEND) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATL',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_PACALEND)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_PACALEND))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACALEND)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACALEND) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oPACALEND_1_26'),i_cWhere,'GSAR_ATL',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACALEND)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_PACALEND);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_PACALEND)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACALEND = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAL = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PACALEND = space(5)
      endif
      this.w_DESCAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACALEND Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_CALE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.TCCODICE as TCCODICE126"+ ",link_1_26.TCDESCRI as TCDESCRI126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on PAR_AGEN.PACALEND=link_1_26.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and PAR_AGEN.PACALEND=link_1_26.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODMDC
  func Link_1_74(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODMDC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_PACODMDC)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_PACODMDC))
          select MDCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODMDC)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODMDC) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oPACODMDC_1_74'),i_cWhere,'GSAR_AMD',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODMDC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_PACODMDC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_PACODMDC)
            select MDCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODMDC = NVL(_Link_.MDCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PACODMDC = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODMDC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PCCAUABB
  func Link_1_75(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCCAUABB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PCCAUABB)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PCCAUABB))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCCAUABB)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStrODBC(trim(this.w_PCCAUABB)+"%");

            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStr(trim(this.w_PCCAUABB)+"%");

            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PCCAUABB) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPCCAUABB_1_75'),i_cWhere,'GSVE_ATD',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCCAUABB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PCCAUABB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PCCAUABB)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCCAUABB = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAU = NVL(_Link_.TDDESDOC,space(35))
      this.w_AFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_TDFLINTE = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PCCAUABB = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_AFLVEAC = space(1)
      this.w_TDFLINTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TDFLINTE<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale incongruente")
        endif
        this.w_PCCAUABB = space(5)
        this.w_DESCAU = space(35)
        this.w_AFLVEAC = space(1)
        this.w_TDFLINTE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCCAUABB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_75(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_75.TDTIPDOC as TDTIPDOC175"+ ",link_1_75.TDDESDOC as TDDESDOC175"+ ",link_1_75.TDFLVEAC as TDFLVEAC175"+ ",link_1_75.TDFLINTE as TDFLINTE175"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_75 on PAR_AGEN.PCCAUABB=link_1_75.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_75"
          i_cKey=i_cKey+'+" and PAR_AGEN.PCCAUABB=link_1_75.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PAATTRIS
  func Link_1_92(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAATTRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PAATTRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PAATTRIS))
          select CACODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAATTRIS)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PAATTRIS) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPAATTRIS_1_92'),i_cWhere,'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAATTRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PAATTRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PAATTRIS)
            select CACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAATTRIS = NVL(_Link_.CACODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PAATTRIS = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PAFLESAT='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PAATTRIS = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAATTRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACODMOD
  func Link_7_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODMATTR_IDX,3]
    i_lTable = "MODMATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODMATTR_IDX,2], .t., this.MODMATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODMATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MMA',True,'MODMATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_PACODMOD)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_PACODMOD))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODMOD)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODMOD) and !this.bDontReportError
            deferred_cp_zoom('MODMATTR','*','MACODICE',cp_AbsName(oSource.parent,'oPACODMOD_7_4'),i_cWhere,'GSAR_MMA',"Modelli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_PACODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_PACODMOD)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODMOD = NVL(_Link_.MACODICE,space(20))
      this.w_DESCRI = NVL(_Link_.MADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PACODMOD = space(20)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODMATTR_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MODMATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_7_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODMATTR_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODMATTR_IDX,2])
      i_cNewSel = i_cSel+ ",link_7_4.MACODICE as MACODICE704"+ ",link_7_4.MADESCRI as MADESCRI704"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_7_4 on PAR_AGEN.PACODMOD=link_7_4.MACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_7_4"
          i_cKey=i_cKey+'+" and PAR_AGEN.PACODMOD=link_7_4.MACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODDOC
  func Link_7_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_PACODDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_PACODDOC))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODDOC)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODDOC) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oPACODDOC_7_9'),i_cWhere,'GSAR_AMD',"Metodi di calcolo periodicit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_PACODDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_PACODDOC)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODDOC = NVL(_Link_.MDCODICE,space(3))
      this.w_DESMDO = NVL(_Link_.MDDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_PACODDOC = space(3)
      endif
      this.w_DESMDO = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_7_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODCLDAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_7_9.MDCODICE as MDCODICE709"+ ",link_7_9.MDDESCRI as MDDESCRI709"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_7_9 on PAR_AGEN.PACODDOC=link_7_9.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_7_9"
          i_cKey=i_cKey+'+" and PAR_AGEN.PACODDOC=link_7_9.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACODPER
  func Link_7_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODPER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_PACODPER)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_PACODPER))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODPER)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PACODPER) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oPACODPER_7_11'),i_cWhere,'GSAR_AMD',"Metodi di calcolo periodicit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODPER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_PACODPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_PACODPER)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODPER = NVL(_Link_.MDCODICE,space(3))
      this.w_DESMAT = NVL(_Link_.MDDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_PACODPER = space(3)
      endif
      this.w_DESMAT = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODPER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_7_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODCLDAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_7_11.MDCODICE as MDCODICE711"+ ",link_7_11.MDDESCRI as MDDESCRI711"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_7_11 on PAR_AGEN.PACODPER=link_7_11.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_7_11"
          i_cKey=i_cKey+'+" and PAR_AGEN.PACODPER=link_7_11.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PCTIPDOC
  func Link_7_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PCTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PCTIPDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStrODBC(trim(this.w_PCTIPDOC)+"%");

            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TDDESDOC like "+cp_ToStr(trim(this.w_PCTIPDOC)+"%");

            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PCTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPCTIPDOC_7_13'),i_cWhere,'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PCTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PCTIPDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PCTIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDOC<>'OR' AND .w_FLVEAC='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti")
        endif
        this.w_PCTIPDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_7_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_7_13.TDTIPDOC as TDTIPDOC713"+ ",link_7_13.TDDESDOC as TDDESDOC713"+ ",link_7_13.TDCATDOC as TDCATDOC713"+ ",link_7_13.TDFLVEAC as TDFLVEAC713"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_7_13 on PAR_AGEN.PCTIPDOC=link_7_13.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_7_13"
          i_cKey=i_cKey+'+" and PAR_AGEN.PCTIPDOC=link_7_13.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PCTIPATT
  func Link_7_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCTIPATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PCTIPATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PCTIPATT))
          select CACODICE,CADESCRI,CARAGGST;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCTIPATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PCTIPATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PCTIPATT)+"%");

            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PCTIPATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPCTIPATT_7_15'),i_cWhere,'GSAG_MCA',"Tipi attivita",'GSAG1AAT.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCTIPATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PCTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PCTIPATT)
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCTIPATT = NVL(_Link_.CACODICE,space(20))
      this.w_DESATT = NVL(_Link_.CADESCRI,space(254))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PCTIPATT = space(20)
      endif
      this.w_DESATT = space(254)
      this.w_CARAGGST = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCTIPATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_7_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_7_15.CACODICE as CACODICE715"+ ",link_7_15.CADESCRI as CADESCRI715"+ ",link_7_15.CARAGGST as CARAGGST715"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_7_15 on PAR_AGEN.PCTIPATT=link_7_15.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_7_15"
          i_cKey=i_cKey+'+" and PAR_AGEN.PCTIPATT=link_7_15.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PCGRUPAR
  func Link_7_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCGRUPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_PCGRUPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_PCGRUPAR))
          select DPCODICE,DPTIPRIS,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCGRUPAR)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCGRUPAR) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oPCGRUPAR_7_17'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCGRUPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_PCGRUPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_PCGRUPAR)
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCGRUPAR = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DESGRU = NVL(_Link_.DPDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_PCGRUPAR = space(5)
      endif
      this.w_TIPRIS = space(1)
      this.w_DESGRU = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIS='G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PCGRUPAR = space(5)
        this.w_TIPRIS = space(1)
        this.w_DESGRU = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCGRUPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_7_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_7_17.DPCODICE as DPCODICE717"+ ",link_7_17.DPTIPRIS as DPTIPRIS717"+ ",link_7_17.DPDESCRI as DPDESCRI717"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_7_17 on PAR_AGEN.PCGRUPAR=link_7_17.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_7_17"
          i_cKey=i_cKey+'+" and PAR_AGEN.PCGRUPAR=link_7_17.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PCPERCON
  func Link_7_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCPERCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_PCPERCON)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_PCPERCON))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCPERCON)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStrODBC(trim(this.w_PCPERCON)+"%");

            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStr(trim(this.w_PCPERCON)+"%");

            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PCPERCON) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oPCPERCON_7_38'),i_cWhere,'GSAR_AMD',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCPERCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_PCPERCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_PCPERCON)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCPERCON = NVL(_Link_.MDCODICE,space(3))
      this.w_MDDESCRI = NVL(_Link_.MDDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_PCPERCON = space(3)
      endif
      this.w_MDDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCPERCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_7_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODCLDAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_7_38.MDCODICE as MDCODICE738"+ ",link_7_38.MDDESCRI as MDDESCRI738"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_7_38 on PAR_AGEN.PCPERCON=link_7_38.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_7_38"
          i_cKey=i_cKey+'+" and PAR_AGEN.PCPERCON=link_7_38.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACAUATT
  func Link_6_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACAUATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PACAUATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKOBB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PACAUATT))
          select CACODICE,CADESCRI,CACHKOBB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACAUATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PACAUATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKOBB";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PACAUATT)+"%");

            select CACODICE,CADESCRI,CACHKOBB;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACAUATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPACAUATT_6_1'),i_cWhere,'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKOBB";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CACHKOBB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACAUATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKOBB";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PACAUATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PACAUATT)
            select CACODICE,CADESCRI,CACHKOBB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACAUATT = NVL(_Link_.CACODICE,space(20))
      this.w_CauDescri = NVL(_Link_.CADESCRI,space(254))
      this.w_CACHKOBB = NVL(_Link_.CACHKOBB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PACAUATT = space(20)
      endif
      this.w_CauDescri = space(254)
      this.w_CACHKOBB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CACHKOBB<>'P'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Scegliere un tipo di attivit� che non preveda l'obbligatoriet� della commessa")
        endif
        this.w_PACAUATT = space(20)
        this.w_CauDescri = space(254)
        this.w_CACHKOBB = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACAUATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_1.CACODICE as CACODICE601"+ ",link_6_1.CADESCRI as CADESCRI601"+ ",link_6_1.CACHKOBB as CACHKOBB601"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_1 on PAR_AGEN.PACAUATT=link_6_1.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_1"
          i_cKey=i_cKey+'+" and PAR_AGEN.PACAUATT=link_6_1.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACAUATT
  func Link_6_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACAUATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PACAUATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKOBB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PACAUATT))
          select CACODICE,CADESCRI,CACHKOBB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACAUATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PACAUATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKOBB";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PACAUATT)+"%");

            select CACODICE,CADESCRI,CACHKOBB;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACAUATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPACAUATT_6_4'),i_cWhere,'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKOBB";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CACHKOBB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACAUATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACHKOBB";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PACAUATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PACAUATT)
            select CACODICE,CADESCRI,CACHKOBB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACAUATT = NVL(_Link_.CACODICE,space(20))
      this.w_CauDescri = NVL(_Link_.CADESCRI,space(254))
      this.w_CACHKOBB = NVL(_Link_.CACHKOBB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PACAUATT = space(20)
      endif
      this.w_CauDescri = space(254)
      this.w_CACHKOBB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CACHKOBB='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Scegliere un tipo di attivit� che non preveda l'obbligatoriet� della pratica")
        endif
        this.w_PACAUATT = space(20)
        this.w_CauDescri = space(254)
        this.w_CACHKOBB = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACAUATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_4.CACODICE as CACODICE604"+ ",link_6_4.CADESCRI as CADESCRI604"+ ",link_6_4.CACHKOBB as CACHKOBB604"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_4 on PAR_AGEN.PACAUATT=link_6_4.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_4"
          i_cKey=i_cKey+'+" and PAR_AGEN.PACAUATT=link_6_4.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACAUPRE
  func Link_6_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACAUPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PACAUPRE)+"%");
                   +" and CARAGGST="+cp_ToStrODBC(this.w_CATEG);

          i_ret=cp_SQL(i_nConn,"select CARAGGST,CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CARAGGST,CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CARAGGST',this.w_CATEG;
                     ,'CACODICE',trim(this.w_PACAUPRE))
          select CARAGGST,CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CARAGGST,CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACAUPRE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PACAUPRE)+"%");
                   +" and CARAGGST="+cp_ToStrODBC(this.w_CATEG);

            i_ret=cp_SQL(i_nConn,"select CARAGGST,CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PACAUPRE)+"%");
                   +" and CARAGGST="+cp_ToStr(this.w_CATEG);

            select CARAGGST,CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACAUPRE) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CARAGGST,CACODICE',cp_AbsName(oSource.parent,'oPACAUPRE_6_5'),i_cWhere,'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CATEG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CARAGGST,CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CARAGGST,CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CARAGGST,CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CARAGGST="+cp_ToStrODBC(this.w_CATEG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CARAGGST',oSource.xKey(1);
                       ,'CACODICE',oSource.xKey(2))
            select CARAGGST,CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACAUPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CARAGGST,CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PACAUPRE);
                   +" and CARAGGST="+cp_ToStrODBC(this.w_CATEG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CARAGGST',this.w_CATEG;
                       ,'CACODICE',this.w_PACAUPRE)
            select CARAGGST,CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACAUPRE = NVL(_Link_.CACODICE,space(20))
      this.w_DES_CAU = NVL(_Link_.CADESCRI,space(254))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PACAUPRE = space(20)
      endif
      this.w_DES_CAU = space(254)
      this.w_CARAGGST = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!EMPTY(.w_DES_CAU)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso")
        endif
        this.w_PACAUPRE = space(20)
        this.w_DES_CAU = space(254)
        this.w_CARAGGST = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CARAGGST,1)+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACAUPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PATIPUDI
  func Link_6_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPUDI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPUDI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PATIPUDI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PATIPUDI)
            select CACODICE,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPUDI = NVL(_Link_.CACODICE,space(20))
      this.w_TIPPOLU = NVL(_Link_.CARAGGST,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPUDI = space(20)
      endif
      this.w_TIPPOLU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPUDI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_6.CACODICE as CACODICE606"+ ",link_6_6.CARAGGST as CARAGGST606"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_6 on PAR_AGEN.PATIPUDI=link_6_6.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_6"
          i_cKey=i_cKey+'+" and PAR_AGEN.PATIPUDI=link_6_6.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACOSGIU
  func Link_6_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACOSGIU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PACOSGIU)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PACOSGIU))
          select CACODICE,CADESCRI,CARAGGST;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACOSGIU)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PACOSGIU)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PACOSGIU)+"%");

            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACOSGIU) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPACOSGIU_6_7'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'GSAG_MAT.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACOSGIU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PACOSGIU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PACOSGIU)
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACOSGIU = NVL(_Link_.CACODICE,space(20))
      this.w_DESCOSGIU = NVL(_Link_.CADESCRI,space(254))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PACOSGIU = space(20)
      endif
      this.w_DESCOSGIU = space(254)
      this.w_CARAGGST = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CARAGGST#'Z'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso")
        endif
        this.w_PACOSGIU = space(20)
        this.w_DESCOSGIU = space(254)
        this.w_CARAGGST = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACOSGIU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_7.CACODICE as CACODICE607"+ ",link_6_7.CADESCRI as CADESCRI607"+ ",link_6_7.CARAGGST as CARAGGST607"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_7 on PAR_AGEN.PACOSGIU=link_6_7.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_7"
          i_cKey=i_cKey+'+" and PAR_AGEN.PACOSGIU=link_6_7.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PATIPCTR
  func Link_6_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPCTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PATIPCTR)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PATIPCTR))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPCTR)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PATIPCTR) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPATIPCTR_6_9'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPCTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PATIPCTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PATIPCTR)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPCTR = NVL(_Link_.CACODICE,space(20))
      this.w_PADESINC = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPCTR = space(20)
      endif
      this.w_PADESINC = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPCTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_9.CACODICE as CACODICE609"+ ",link_6_9.CADESCRI as CADESCRI609"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_9 on PAR_AGEN.PATIPCTR=link_6_9.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_9"
          i_cKey=i_cKey+'+" and PAR_AGEN.PATIPCTR=link_6_9.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PATIPTEL
  func Link_6_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPTEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PATIPTEL)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PATIPTEL))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPTEL)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PATIPTEL)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PATIPTEL)+"%");

            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PATIPTEL) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPATIPTEL_6_12'),i_cWhere,'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPTEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PATIPTEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PATIPTEL)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPTEL = NVL(_Link_.CACODICE,space(20))
      this.w_DES_TEL = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPTEL = space(20)
      endif
      this.w_DES_TEL = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPTEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_12.CACODICE as CACODICE612"+ ",link_6_12.CADESCRI as CADESCRI612"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_12 on PAR_AGEN.PATIPTEL=link_6_12.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_12"
          i_cKey=i_cKey+'+" and PAR_AGEN.PATIPTEL=link_6_12.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PATIPASS
  func Link_6_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPASS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PATIPASS)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PATIPASS))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPASS)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PATIPASS)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PATIPASS)+"%");

            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PATIPASS) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPATIPASS_6_13'),i_cWhere,'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPASS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PATIPASS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PATIPASS)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPASS = NVL(_Link_.CACODICE,space(20))
      this.w_DES_CHI = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPASS = space(20)
      endif
      this.w_DES_CHI = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPASS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_13.CACODICE as CACODICE613"+ ",link_6_13.CADESCRI as CADESCRI613"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_13 on PAR_AGEN.PATIPASS=link_6_13.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_13"
          i_cKey=i_cKey+'+" and PAR_AGEN.PATIPASS=link_6_13.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PATIPAPI
  func Link_6_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPAPI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PATIPAPI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PATIPAPI))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPAPI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PATIPAPI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PATIPAPI)+"%");

            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PATIPAPI) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPATIPAPI_6_14'),i_cWhere,'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPAPI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PATIPAPI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PATIPAPI)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPAPI = NVL(_Link_.CACODICE,space(20))
      this.w_DES_API = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPAPI = space(20)
      endif
      this.w_DES_API = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPAPI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_14.CACODICE as CACODICE614"+ ",link_6_14.CADESCRI as CADESCRI614"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_14 on PAR_AGEN.PATIPAPI=link_6_14.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_14"
          i_cKey=i_cKey+'+" and PAR_AGEN.PATIPAPI=link_6_14.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PATIPCOS
  func Link_6_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PATIPCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PATIPCOS))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPCOS)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PATIPCOS)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PATIPCOS)+"%");

            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PATIPCOS) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPATIPCOS_6_15'),i_cWhere,'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PATIPCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PATIPCOS)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPCOS = NVL(_Link_.CACODICE,space(20))
      this.w_DES_DOC = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPCOS = space(20)
      endif
      this.w_DES_DOC = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_15.CACODICE as CACODICE615"+ ",link_6_15.CADESCRI as CADESCRI615"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_15 on PAR_AGEN.PATIPCOS=link_6_15.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_15"
          i_cKey=i_cKey+'+" and PAR_AGEN.PATIPCOS=link_6_15.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PATIPGEN
  func Link_6_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PATIPGEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PATIPGEN))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPGEN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PATIPGEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PATIPGEN)+"%");

            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PATIPGEN) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPATIPGEN_6_16'),i_cWhere,'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PATIPGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PATIPGEN)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPGEN = NVL(_Link_.CACODICE,space(20))
      this.w_DES_GEN = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPGEN = space(20)
      endif
      this.w_DES_GEN = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_16.CACODICE as CACODICE616"+ ",link_6_16.CADESCRI as CADESCRI616"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_16 on PAR_AGEN.PATIPGEN=link_6_16.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_16"
          i_cKey=i_cKey+'+" and PAR_AGEN.PATIPGEN=link_6_16.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PATIPAPF
  func Link_6_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PATIPAPF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PATIPAPF)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PATIPAPF))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PATIPAPF)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PATIPAPF)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PATIPAPF)+"%");

            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PATIPAPF) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPATIPAPF_6_17'),i_cWhere,'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PATIPAPF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PATIPAPF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PATIPAPF)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PATIPAPF = NVL(_Link_.CACODICE,space(20))
      this.w_DES_APF = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PATIPAPF = space(20)
      endif
      this.w_DES_APF = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PATIPAPF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_17.CACODICE as CACODICE617"+ ",link_6_17.CADESCRI as CADESCRI617"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_17 on PAR_AGEN.PATIPAPF=link_6_17.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_17"
          i_cKey=i_cKey+'+" and PAR_AGEN.PATIPAPF=link_6_17.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPACODLIS_1_8.value==this.w_PACODLIS)
      this.oPgFrm.Page1.oPag.oPACODLIS_1_8.value=this.w_PACODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPACODLIS_1_10.value==this.w_PACODLIS)
      this.oPgFrm.Page1.oPag.oPACODLIS_1_10.value=this.w_PACODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPALISACQ_1_11.value==this.w_PALISACQ)
      this.oPgFrm.Page1.oPag.oPALISACQ_1_11.value=this.w_PALISACQ
    endif
    if not(this.oPgFrm.Page1.oPag.oRDESLIS_1_12.value==this.w_RDESLIS)
      this.oPgFrm.Page1.oPag.oRDESLIS_1_12.value=this.w_RDESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPANOFLAT_1_13.RadioValue()==this.w_PANOFLAT)
      this.oPgFrm.Page1.oPag.oPANOFLAT_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPADATATP_1_14.value==this.w_PADATATP)
      this.oPgFrm.Page1.oPag.oPADATATP_1_14.value=this.w_PADATATP
    endif
    if not(this.oPgFrm.Page1.oPag.oPADATATS_1_15.value==this.w_PADATATS)
      this.oPgFrm.Page1.oPag.oPADATATS_1_15.value=this.w_PADATATS
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLCOSE_1_16.RadioValue()==this.w_PAFLCOSE)
      this.oPgFrm.Page1.oPag.oPAFLCOSE_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPADACOSE_1_17.value==this.w_PADACOSE)
      this.oPgFrm.Page1.oPag.oPADACOSE_1_17.value=this.w_PADACOSE
    endif
    if not(this.oPgFrm.Page1.oPag.oPA_ACOSE_1_18.value==this.w_PA_ACOSE)
      this.oPgFrm.Page1.oPag.oPA_ACOSE_1_18.value=this.w_PA_ACOSE
    endif
    if not(this.oPgFrm.Page1.oPag.oPANOFLPR_1_19.RadioValue()==this.w_PANOFLPR)
      this.oPgFrm.Page1.oPag.oPANOFLPR_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPADATPRP_1_20.value==this.w_PADATPRP)
      this.oPgFrm.Page1.oPag.oPADATPRP_1_20.value=this.w_PADATPRP
    endif
    if not(this.oPgFrm.Page1.oPag.oPADATPRS_1_21.value==this.w_PADATPRS)
      this.oPgFrm.Page1.oPag.oPADATPRS_1_21.value=this.w_PADATPRS
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGIOSCA_1_22.value==this.w_PAGIOSCA)
      this.oPgFrm.Page1.oPag.oPAGIOSCA_1_22.value=this.w_PAGIOSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLRDOC_1_23.RadioValue()==this.w_PAFLRDOC)
      this.oPgFrm.Page1.oPag.oPAFLRDOC_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPACHKFES_1_24.RadioValue()==this.w_PACHKFES)
      this.oPgFrm.Page1.oPag.oPACHKFES_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPACHKPRE_1_25.RadioValue()==this.w_PACHKPRE)
      this.oPgFrm.Page1.oPag.oPACHKPRE_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPACALEND_1_26.value==this.w_PACALEND)
      this.oPgFrm.Page1.oPag.oPACALEND_1_26.value=this.w_PACALEND
    endif
    if not(this.oPgFrm.Page1.oPag.oPACHKCAR_1_27.RadioValue()==this.w_PACHKCAR)
      this.oPgFrm.Page1.oPag.oPACHKCAR_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAVISTRC_1_28.RadioValue()==this.w_PAVISTRC)
      this.oPgFrm.Page1.oPag.oPAVISTRC_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLVISI_1_45.RadioValue()==this.w_PAFLVISI)
      this.oPgFrm.Page1.oPag.oPAFLVISI_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLANTI_1_46.RadioValue()==this.w_PAFLANTI)
      this.oPgFrm.Page1.oPag.oPAFLANTI_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAL_1_53.value==this.w_DESCAL)
      this.oPgFrm.Page1.oPag.oDESCAL_1_53.value=this.w_DESCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESACQ_1_56.value==this.w_DESACQ)
      this.oPgFrm.Page1.oPag.oDESACQ_1_56.value=this.w_DESACQ
    endif
    if not(this.oPgFrm.Page1.oPag.oEDESLIS_1_58.value==this.w_EDESLIS)
      this.oPgFrm.Page1.oPag.oEDESLIS_1_58.value=this.w_EDESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGESRIS_1_61.RadioValue()==this.w_PAGESRIS)
      this.oPgFrm.Page1.oPag.oPAGESRIS_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPADESRIS_1_62.value==this.w_PADESRIS)
      this.oPgFrm.Page1.oPag.oPADESRIS_1_62.value=this.w_PADESRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPAST_ATT_1_63.RadioValue()==this.w_PAST_ATT)
      this.oPgFrm.Page1.oPag.oPAST_ATT_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAOGGCRE_2_1.value==this.w_PAOGGCRE)
      this.oPgFrm.Page2.oPag.oPAOGGCRE_2_1.value=this.w_PAOGGCRE
    endif
    if not(this.oPgFrm.Page2.oPag.oPATXTCRE_2_2.value==this.w_PATXTCRE)
      this.oPgFrm.Page2.oPag.oPATXTCRE_2_2.value=this.w_PATXTCRE
    endif
    if not(this.oPgFrm.Page2.oPag.oPAPSTCRE_2_3.value==this.w_PAPSTCRE)
      this.oPgFrm.Page2.oPag.oPAPSTCRE_2_3.value=this.w_PAPSTCRE
    endif
    if not(this.oPgFrm.Page1.oPag.oPACHKCPL_1_69.RadioValue()==this.w_PACHKCPL)
      this.oPgFrm.Page1.oPag.oPACHKCPL_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPACHKING_1_70.RadioValue()==this.w_PACHKING)
      this.oPgFrm.Page1.oPag.oPACHKING_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPADATINI_1_71.value==this.w_PADATINI)
      this.oPgFrm.Page1.oPag.oPADATINI_1_71.value=this.w_PADATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGIOCHK_1_72.value==this.w_PAGIOCHK)
      this.oPgFrm.Page1.oPag.oPAGIOCHK_1_72.value=this.w_PAGIOCHK
    endif
    if not(this.oPgFrm.Page1.oPag.oPACODMDC_1_74.RadioValue()==this.w_PACODMDC)
      this.oPgFrm.Page1.oPag.oPACODMDC_1_74.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPCCAUABB_1_75.value==this.w_PCCAUABB)
      this.oPgFrm.Page1.oPag.oPCCAUABB_1_75.value=this.w_PCCAUABB
    endif
    if not(this.oPgFrm.Page1.oPag.oPACALDAT_1_76.RadioValue()==this.w_PACALDAT)
      this.oPgFrm.Page1.oPag.oPACALDAT_1_76.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPACHKATT_1_77.value==this.w_PACHKATT)
      this.oPgFrm.Page1.oPag.oPACHKATT_1_77.value=this.w_PACHKATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_82.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_82.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLESAT_1_90.RadioValue()==this.w_PAFLESAT)
      this.oPgFrm.Page1.oPag.oPAFLESAT_1_90.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLDTRP_1_91.RadioValue()==this.w_PAFLDTRP)
      this.oPgFrm.Page1.oPag.oPAFLDTRP_1_91.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAATTRIS_1_92.value==this.w_PAATTRIS)
      this.oPgFrm.Page1.oPag.oPAATTRIS_1_92.value=this.w_PAATTRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLSIAT_1_93.RadioValue()==this.w_PAFLSIAT)
      this.oPgFrm.Page1.oPag.oPAFLSIAT_1_93.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLPRES_1_94.RadioValue()==this.w_PAFLPRES)
      this.oPgFrm.Page1.oPag.oPAFLPRES_1_94.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oPACODMOD_7_4.value==this.w_PACODMOD)
      this.oPgFrm.Page7.oPag.oPACODMOD_7_4.value=this.w_PACODMOD
    endif
    if not(this.oPgFrm.Page7.oPag.oDESCRI_7_6.value==this.w_DESCRI)
      this.oPgFrm.Page7.oPag.oDESCRI_7_6.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page7.oPag.oPACODNUM_7_7.RadioValue()==this.w_PACODNUM)
      this.oPgFrm.Page7.oPag.oPACODNUM_7_7.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oPCNOFLDC_7_8.RadioValue()==this.w_PCNOFLDC)
      this.oPgFrm.Page7.oPag.oPCNOFLDC_7_8.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oPACODDOC_7_9.value==this.w_PACODDOC)
      this.oPgFrm.Page7.oPag.oPACODDOC_7_9.value=this.w_PACODDOC
    endif
    if not(this.oPgFrm.Page7.oPag.oPCNOFLAT_7_10.RadioValue()==this.w_PCNOFLAT)
      this.oPgFrm.Page7.oPag.oPCNOFLAT_7_10.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oPACODPER_7_11.value==this.w_PACODPER)
      this.oPgFrm.Page7.oPag.oPACODPER_7_11.value=this.w_PACODPER
    endif
    if not(this.oPgFrm.Page7.oPag.oPADESSUP_7_12.value==this.w_PADESSUP)
      this.oPgFrm.Page7.oPag.oPADESSUP_7_12.value=this.w_PADESSUP
    endif
    if not(this.oPgFrm.Page7.oPag.oPCTIPDOC_7_13.value==this.w_PCTIPDOC)
      this.oPgFrm.Page7.oPag.oPCTIPDOC_7_13.value=this.w_PCTIPDOC
    endif
    if not(this.oPgFrm.Page7.oPag.oPCTIPATT_7_15.value==this.w_PCTIPATT)
      this.oPgFrm.Page7.oPag.oPCTIPATT_7_15.value=this.w_PCTIPATT
    endif
    if not(this.oPgFrm.Page7.oPag.oPCGRUPAR_7_17.value==this.w_PCGRUPAR)
      this.oPgFrm.Page7.oPag.oPCGRUPAR_7_17.value=this.w_PCGRUPAR
    endif
    if not(this.oPgFrm.Page7.oPag.oDESDOC_7_19.value==this.w_DESDOC)
      this.oPgFrm.Page7.oPag.oDESDOC_7_19.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page7.oPag.oDESATT_7_20.value==this.w_DESATT)
      this.oPgFrm.Page7.oPag.oDESATT_7_20.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page7.oPag.oDESGRU_7_36.value==this.w_DESGRU)
      this.oPgFrm.Page7.oPag.oDESGRU_7_36.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page7.oPag.oPCESCRIN_7_37.RadioValue()==this.w_PCESCRIN)
      this.oPgFrm.Page7.oPag.oPCESCRIN_7_37.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oPCPERCON_7_38.value==this.w_PCPERCON)
      this.oPgFrm.Page7.oPag.oPCPERCON_7_38.value=this.w_PCPERCON
    endif
    if not(this.oPgFrm.Page7.oPag.oMDDESCRI_7_39.value==this.w_MDDESCRI)
      this.oPgFrm.Page7.oPag.oMDDESCRI_7_39.value=this.w_MDDESCRI
    endif
    if not(this.oPgFrm.Page7.oPag.oPCRINCON_7_41.RadioValue()==this.w_PCRINCON)
      this.oPgFrm.Page7.oPag.oPCRINCON_7_41.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oPCNUMGIO_7_42.value==this.w_PCNUMGIO)
      this.oPgFrm.Page7.oPag.oPCNUMGIO_7_42.value=this.w_PCNUMGIO
    endif
    if not(this.oPgFrm.Page7.oPag.oDESMDO_7_46.value==this.w_DESMDO)
      this.oPgFrm.Page7.oPag.oDESMDO_7_46.value=this.w_DESMDO
    endif
    if not(this.oPgFrm.Page7.oPag.oDESMAT_7_47.value==this.w_DESMAT)
      this.oPgFrm.Page7.oPag.oDESMAT_7_47.value=this.w_DESMAT
    endif
    if not(this.oPgFrm.Page6.oPag.oPACAUATT_6_1.value==this.w_PACAUATT)
      this.oPgFrm.Page6.oPag.oPACAUATT_6_1.value=this.w_PACAUATT
    endif
    if not(this.oPgFrm.Page6.oPag.oCauDescri_6_3.value==this.w_CauDescri)
      this.oPgFrm.Page6.oPag.oCauDescri_6_3.value=this.w_CauDescri
    endif
    if not(this.oPgFrm.Page6.oPag.oPACAUATT_6_4.value==this.w_PACAUATT)
      this.oPgFrm.Page6.oPag.oPACAUATT_6_4.value=this.w_PACAUATT
    endif
    if not(this.oPgFrm.Page6.oPag.oPACAUPRE_6_5.value==this.w_PACAUPRE)
      this.oPgFrm.Page6.oPag.oPACAUPRE_6_5.value=this.w_PACAUPRE
    endif
    if not(this.oPgFrm.Page6.oPag.oPACOSGIU_6_7.value==this.w_PACOSGIU)
      this.oPgFrm.Page6.oPag.oPACOSGIU_6_7.value=this.w_PACOSGIU
    endif
    if not(this.oPgFrm.Page6.oPag.oPASTRING_6_8.value==this.w_PASTRING)
      this.oPgFrm.Page6.oPag.oPASTRING_6_8.value=this.w_PASTRING
    endif
    if not(this.oPgFrm.Page6.oPag.oPATIPCTR_6_9.value==this.w_PATIPCTR)
      this.oPgFrm.Page6.oPag.oPATIPCTR_6_9.value=this.w_PATIPCTR
    endif
    if not(this.oPgFrm.Page6.oPag.oPANGGINC_6_10.value==this.w_PANGGINC)
      this.oPgFrm.Page6.oPag.oPANGGINC_6_10.value=this.w_PANGGINC
    endif
    if not(this.oPgFrm.Page6.oPag.oPATIPTEL_6_12.value==this.w_PATIPTEL)
      this.oPgFrm.Page6.oPag.oPATIPTEL_6_12.value=this.w_PATIPTEL
    endif
    if not(this.oPgFrm.Page6.oPag.oPATIPASS_6_13.value==this.w_PATIPASS)
      this.oPgFrm.Page6.oPag.oPATIPASS_6_13.value=this.w_PATIPASS
    endif
    if not(this.oPgFrm.Page6.oPag.oPATIPAPI_6_14.value==this.w_PATIPAPI)
      this.oPgFrm.Page6.oPag.oPATIPAPI_6_14.value=this.w_PATIPAPI
    endif
    if not(this.oPgFrm.Page6.oPag.oPATIPCOS_6_15.value==this.w_PATIPCOS)
      this.oPgFrm.Page6.oPag.oPATIPCOS_6_15.value=this.w_PATIPCOS
    endif
    if not(this.oPgFrm.Page6.oPag.oPATIPGEN_6_16.value==this.w_PATIPGEN)
      this.oPgFrm.Page6.oPag.oPATIPGEN_6_16.value=this.w_PATIPGEN
    endif
    if not(this.oPgFrm.Page6.oPag.oPATIPAPF_6_17.value==this.w_PATIPAPF)
      this.oPgFrm.Page6.oPag.oPATIPAPF_6_17.value=this.w_PATIPAPF
    endif
    if not(this.oPgFrm.Page6.oPag.oDES_CAU_6_19.value==this.w_DES_CAU)
      this.oPgFrm.Page6.oPag.oDES_CAU_6_19.value=this.w_DES_CAU
    endif
    if not(this.oPgFrm.Page6.oPag.oDES_GEN_6_21.value==this.w_DES_GEN)
      this.oPgFrm.Page6.oPag.oDES_GEN_6_21.value=this.w_DES_GEN
    endif
    if not(this.oPgFrm.Page6.oPag.oDES_TEL_6_23.value==this.w_DES_TEL)
      this.oPgFrm.Page6.oPag.oDES_TEL_6_23.value=this.w_DES_TEL
    endif
    if not(this.oPgFrm.Page6.oPag.oDES_CHI_6_25.value==this.w_DES_CHI)
      this.oPgFrm.Page6.oPag.oDES_CHI_6_25.value=this.w_DES_CHI
    endif
    if not(this.oPgFrm.Page6.oPag.oDES_API_6_27.value==this.w_DES_API)
      this.oPgFrm.Page6.oPag.oDES_API_6_27.value=this.w_DES_API
    endif
    if not(this.oPgFrm.Page6.oPag.oDES_APF_6_29.value==this.w_DES_APF)
      this.oPgFrm.Page6.oPag.oDES_APF_6_29.value=this.w_DES_APF
    endif
    if not(this.oPgFrm.Page6.oPag.oDES_DOC_6_35.value==this.w_DES_DOC)
      this.oPgFrm.Page6.oPag.oDES_DOC_6_35.value=this.w_DES_DOC
    endif
    if not(this.oPgFrm.Page6.oPag.oPADESINC_6_43.value==this.w_PADESINC)
      this.oPgFrm.Page6.oPag.oPADESINC_6_43.value=this.w_PADESINC
    endif
    if not(this.oPgFrm.Page6.oPag.oPAOGGOUT_6_44.value==this.w_PAOGGOUT)
      this.oPgFrm.Page6.oPag.oPAOGGOUT_6_44.value=this.w_PAOGGOUT
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLAPPU_5_6.RadioValue()==this.w_PAFLAPPU)
      this.oPgFrm.Page5.oPag.oPAFLAPPU_5_6.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLATGE_5_7.RadioValue()==this.w_PAFLATGE)
      this.oPgFrm.Page5.oPag.oPAFLATGE_5_7.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLUDIE_5_8.RadioValue()==this.w_PAFLUDIE)
      this.oPgFrm.Page5.oPag.oPAFLUDIE_5_8.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLSEST_5_9.RadioValue()==this.w_PAFLSEST)
      this.oPgFrm.Page5.oPag.oPAFLSEST_5_9.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLASSE_5_10.RadioValue()==this.w_PAFLASSE)
      this.oPgFrm.Page5.oPag.oPAFLASSE_5_10.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLINPR_5_11.RadioValue()==this.w_PAFLINPR)
      this.oPgFrm.Page5.oPag.oPAFLINPR_5_11.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLNOTE_5_12.RadioValue()==this.w_PAFLNOTE)
      this.oPgFrm.Page5.oPag.oPAFLNOTE_5_12.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLDAFA_5_13.RadioValue()==this.w_PAFLDAFA)
      this.oPgFrm.Page5.oPag.oPAFLDAFA_5_13.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLAPP1_5_16.RadioValue()==this.w_PAFLAPP1)
      this.oPgFrm.Page5.oPag.oPAFLAPP1_5_16.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLATG1_5_17.RadioValue()==this.w_PAFLATG1)
      this.oPgFrm.Page5.oPag.oPAFLATG1_5_17.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLUDI1_5_18.RadioValue()==this.w_PAFLUDI1)
      this.oPgFrm.Page5.oPag.oPAFLUDI1_5_18.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLSES1_5_19.RadioValue()==this.w_PAFLSES1)
      this.oPgFrm.Page5.oPag.oPAFLSES1_5_19.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLASS1_5_20.RadioValue()==this.w_PAFLASS1)
      this.oPgFrm.Page5.oPag.oPAFLASS1_5_20.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLINP1_5_21.RadioValue()==this.w_PAFLINP1)
      this.oPgFrm.Page5.oPag.oPAFLINP1_5_21.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLNOT1_5_22.RadioValue()==this.w_PAFLNOT1)
      this.oPgFrm.Page5.oPag.oPAFLNOT1_5_22.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLDAF1_5_23.RadioValue()==this.w_PAFLDAF1)
      this.oPgFrm.Page5.oPag.oPAFLDAF1_5_23.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLAPP2_5_26.RadioValue()==this.w_PAFLAPP2)
      this.oPgFrm.Page5.oPag.oPAFLAPP2_5_26.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLATG2_5_27.RadioValue()==this.w_PAFLATG2)
      this.oPgFrm.Page5.oPag.oPAFLATG2_5_27.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLUDI2_5_28.RadioValue()==this.w_PAFLUDI2)
      this.oPgFrm.Page5.oPag.oPAFLUDI2_5_28.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLSES2_5_29.RadioValue()==this.w_PAFLSES2)
      this.oPgFrm.Page5.oPag.oPAFLSES2_5_29.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLASS2_5_30.RadioValue()==this.w_PAFLASS2)
      this.oPgFrm.Page5.oPag.oPAFLASS2_5_30.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLINP2_5_31.RadioValue()==this.w_PAFLINP2)
      this.oPgFrm.Page5.oPag.oPAFLINP2_5_31.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLNOT2_5_32.RadioValue()==this.w_PAFLNOT2)
      this.oPgFrm.Page5.oPag.oPAFLNOT2_5_32.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLDAF2_5_33.RadioValue()==this.w_PAFLDAF2)
      this.oPgFrm.Page5.oPag.oPAFLDAF2_5_33.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLAPP3_5_36.RadioValue()==this.w_PAFLAPP3)
      this.oPgFrm.Page5.oPag.oPAFLAPP3_5_36.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLATG3_5_37.RadioValue()==this.w_PAFLATG3)
      this.oPgFrm.Page5.oPag.oPAFLATG3_5_37.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLUDI3_5_38.RadioValue()==this.w_PAFLUDI3)
      this.oPgFrm.Page5.oPag.oPAFLUDI3_5_38.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLSES3_5_39.RadioValue()==this.w_PAFLSES3)
      this.oPgFrm.Page5.oPag.oPAFLSES3_5_39.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLASS3_5_40.RadioValue()==this.w_PAFLASS3)
      this.oPgFrm.Page5.oPag.oPAFLASS3_5_40.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLINP3_5_41.RadioValue()==this.w_PAFLINP3)
      this.oPgFrm.Page5.oPag.oPAFLINP3_5_41.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLNOT3_5_42.RadioValue()==this.w_PAFLNOT3)
      this.oPgFrm.Page5.oPag.oPAFLNOT3_5_42.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oPAFLDAF3_5_43.RadioValue()==this.w_PAFLDAF3)
      this.oPgFrm.Page5.oPag.oPAFLDAF3_5_43.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPAOGGMOD_3_1.value==this.w_PAOGGMOD)
      this.oPgFrm.Page3.oPag.oPAOGGMOD_3_1.value=this.w_PAOGGMOD
    endif
    if not(this.oPgFrm.Page3.oPag.oPATXTMOD_3_2.value==this.w_PATXTMOD)
      this.oPgFrm.Page3.oPag.oPATXTMOD_3_2.value=this.w_PATXTMOD
    endif
    if not(this.oPgFrm.Page3.oPag.oPAPSTMOD_3_3.value==this.w_PAPSTMOD)
      this.oPgFrm.Page3.oPag.oPAPSTMOD_3_3.value=this.w_PAPSTMOD
    endif
    if not(this.oPgFrm.Page4.oPag.oPAOGGDEL_4_1.value==this.w_PAOGGDEL)
      this.oPgFrm.Page4.oPag.oPAOGGDEL_4_1.value=this.w_PAOGGDEL
    endif
    if not(this.oPgFrm.Page4.oPag.oPATXTDEL_4_2.value==this.w_PATXTDEL)
      this.oPgFrm.Page4.oPag.oPATXTDEL_4_2.value=this.w_PATXTDEL
    endif
    if not(this.oPgFrm.Page4.oPag.oPAPSTDEL_4_3.value==this.w_PAPSTDEL)
      this.oPgFrm.Page4.oPag.oPAPSTDEL_4_3.value=this.w_PAPSTDEL
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLR_AG_1_102.RadioValue()==this.w_PAFLR_AG)
      this.oPgFrm.Page1.oPag.oPAFLR_AG_1_102.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oPADATFIS_7_48.RadioValue()==this.w_PADATFIS)
      this.oPgFrm.Page7.oPag.oPADATFIS_7_48.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oPADESATT_7_49.value==this.w_PADESATT)
      this.oPgFrm.Page7.oPag.oPADESATT_7_49.value=this.w_PADESATT
    endif
    if not(this.oPgFrm.Page7.oPag.oPADESDOC_7_51.value==this.w_PADESDOC)
      this.oPgFrm.Page7.oPag.oPADESDOC_7_51.value=this.w_PADESDOC
    endif
    cp_SetControlsValueExtFlds(this,'PAR_AGEN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_FLGCIC $ 'V-E' and .w_TIPLIS $ 'P-E' and IsAhe() ) or !Isahe())  and not(!Isahe())  and not(empty(.w_PACODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPACODLIS_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, listino non valido")
          case   not(((.w_CICLO $ 'A-E'  AND .w_TIPLIS='P' ) or (Isahr() AND .w_FLSCON2<>'S' )) AND .w_IVALIS<>'L')  and not(isalt()  or  .w_ATTIVACQ='N' )  and not(empty(.w_PALISACQ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPALISACQ_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, listino inesistente o di tipo Iva inclusa o di tipo sconti")
          case   not(.w_PAGIOSCA>=0)  and not(!isalt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPAGIOSCA_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, numero giorni negativo")
          case   (empty(.w_PACALEND))  and (.w_PACHKFES='C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPACALEND_1_26.SetFocus()
            i_bnoObbl = !empty(.w_PACALEND)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PADATINI))  and not(.w_PACHKING<>'F')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPADATINI_1_71.SetFocus()
            i_bnoObbl = !empty(.w_PADATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PAGIOCHK)) or not(.w_PAGIOCHK > 0))  and not(.w_PACHKING<>'A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPAGIOCHK_1_72.SetFocus()
            i_bnoObbl = !empty(.w_PAGIOCHK)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore positivo")
          case   not(.w_TDFLINTE<>'F')  and not(Isalt())  and not(empty(.w_PCCAUABB))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPCCAUABB_1_75.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale incongruente")
          case   not(.w_PACHKATT>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPACHKATT_1_77.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PAFLESAT='S')  and not(!IsAlt() OR .w_PAFLESAT<>'S')  and not(empty(.w_PAATTRIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPAATTRIS_1_92.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CATDOC<>'OR' AND .w_FLVEAC='V')  and not(empty(.w_PCTIPDOC))
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oPCTIPDOC_7_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti")
          case   not(.w_TIPRIS='G')  and not(empty(.w_PCGRUPAR))
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oPCGRUPAR_7_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CACHKOBB<>'P')  and not(IsAlt())  and not(empty(.w_PACAUATT))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oPACAUATT_6_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Scegliere un tipo di attivit� che non preveda l'obbligatoriet� della commessa")
          case   not(.w_CACHKOBB='N')  and not(!IsAlt())  and not(empty(.w_PACAUATT))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oPACAUATT_6_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Scegliere un tipo di attivit� che non preveda l'obbligatoriet� della pratica")
          case   not(!EMPTY(.w_DES_CAU))  and not(empty(.w_PACAUPRE))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oPACAUPRE_6_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso")
          case   not(.w_CARAGGST#'Z')  and not(NOT ISALT())  and not(empty(.w_PACOSGIU))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oPACOSGIU_6_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso")
          case   (empty(.w_PASTRING))  and not(NOT ISALT())  and (NOT EMPTY(.w_PACOSGIU))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oPASTRING_6_8.SetFocus()
            i_bnoObbl = !empty(.w_PASTRING)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PANGGINC > 0)  and not(NOT ISALT())  and (NOT EMPTY(.w_PATIPCTR))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oPANGGINC_6_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .gsag_mat.CheckForm()
      if i_bres
        i_bres=  .gsag_mat.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=6
        endif
      endif
      *i_bRes = i_bRes .and. .GSAG_MCD.CheckForm()
      if i_bres
        i_bres=  .GSAG_MCD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=6
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsag_apa
      This.Notifyevent('ChkFinali')
      If ! This.w_check
       i_bres=.F.
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PACODAZI = this.w_PACODAZI
    this.o_PANOFLAT = this.w_PANOFLAT
    this.o_PAFLCOSE = this.w_PAFLCOSE
    this.o_PANOFLPR = this.w_PANOFLPR
    this.o_PACHKFES = this.w_PACHKFES
    this.o_PAGESRIS = this.w_PAGESRIS
    this.o_PACHKING = this.w_PACHKING
    this.o_PAFLESAT = this.w_PAFLESAT
    this.o_PACODDOC = this.w_PACODDOC
    this.o_PACODPER = this.w_PACODPER
    this.o_PACOSGIU = this.w_PACOSGIU
    this.o_PATIPCTR = this.w_PATIPCTR
    * --- gsag_mat : Depends On
    this.gsag_mat.SaveDependsOn()
    * --- GSAG_MCD : Depends On
    this.GSAG_MCD.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsag_apaPag1 as StdContainer
  Width  = 706
  height = 537
  stdWidth  = 706
  stdheight = 537
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPACODLIS_1_8 as StdField with uid="NYOZCWZLAG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PACODLIS", cQueryName = "PACODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, listino gestito a sconti",;
    ToolTipText = "Listino preferenziale (da utilizzare nel caricamento di una nuova attivit�)",;
    HelpContextID = 118050633,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=224, Top=15, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSVALLIS", oKey_1_2="this.w_PERVAL", oKey_2_1="LSCODLIS", oKey_2_2="this.w_PACODLIS"

  func oPACODLIS_1_8.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oPACODLIS_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODLIS_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODLIS_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LISTINI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStrODBC(this.Parent.oContained.w_PERVAL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStr(this.Parent.oContained.w_PERVAL)
    endif
    do cp_zoom with 'LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(this.parent,'oPACODLIS_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oPACODLIS_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LSVALLIS=w_PERVAL
     i_obj.w_LSCODLIS=this.parent.oContained.w_PACODLIS
     i_obj.ecpSave()
  endproc

  add object oPACODLIS_1_10 as StdField with uid="PJSSIWLPZC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PACODLIS", cQueryName = "PACODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, listino non valido",;
    ToolTipText = "Listino ricavi da utilizzare per i nominativi che non sono ancora diventati client",;
    HelpContextID = 118050633,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=223, Top=15, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_PACODLIS"

  func oPACODLIS_1_10.mHide()
    with this.Parent.oContained
      return (!Isahe())
    endwith
  endfunc

  proc oPACODLIS_1_10.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='V'
    endwith
  endproc

  func oPACODLIS_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODLIS_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODLIS_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oPACODLIS_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Listini",'listdocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oPALISACQ_1_11 as StdField with uid="XADEJTRACI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PALISACQ", cQueryName = "PALISACQ",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, listino inesistente o di tipo Iva inclusa o di tipo sconti",;
    ToolTipText = "Listino costi (da utilizzare nel caricamento di una nuova attivit� per determinare il costo interno)",;
    HelpContextID = 51126457,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=223, Top=38, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_PALISACQ"

  func oPALISACQ_1_11.mHide()
    with this.Parent.oContained
      return (isalt()  or  .w_ATTIVACQ='N' )
    endwith
  endfunc

  proc oPALISACQ_1_11.mBefore
    with this.Parent.oContained
      .w_MVFLVEAC='A'
    endwith
  endproc

  func oPALISACQ_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPALISACQ_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPALISACQ_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oPALISACQ_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Listini",'listdocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oRDESLIS_1_12 as StdField with uid="RSDGDVOBWW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_RDESLIS", cQueryName = "RDESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 76378646,;
   bGlobalFont=.t.,;
    Height=21, Width=386, Left=297, Top=15, InputMask=replicate('X',40)

  func oRDESLIS_1_12.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc


  add object oPANOFLAT_1_13 as StdCombo with uid="PYKMLGYMOI",rtseq=12,rtrep=.f.,left=223,top=61,width=185,height=21;
    , ToolTipText = "Permette di definire il range di date predefinito per la ricerca delle attivit� sulla gestione elenco attivit�";
    , HelpContextID = 148242614;
    , cFormVar="w_PANOFLAT",RowSource=""+"Intervallo,"+"Data odierna,"+"Prima data vuota,"+"Seconda data vuota,"+"Entrambe date vuote,"+"Settimana corrente,"+"Settimana corrente+successiva,"+"Settimana corrente+precedente,"+"Settimana corrente+prec.+succ.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPANOFLAT_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    iif(this.value =5,'E',;
    iif(this.value =6,'A',;
    iif(this.value =7,'B',;
    iif(this.value =8,'C',;
    iif(this.value =9,'D',;
    space(1)))))))))))
  endfunc
  func oPANOFLAT_1_13.GetRadio()
    this.Parent.oContained.w_PANOFLAT = this.RadioValue()
    return .t.
  endfunc

  func oPANOFLAT_1_13.SetRadio()
    this.Parent.oContained.w_PANOFLAT=trim(this.Parent.oContained.w_PANOFLAT)
    this.value = ;
      iif(this.Parent.oContained.w_PANOFLAT=='S',1,;
      iif(this.Parent.oContained.w_PANOFLAT=='N',2,;
      iif(this.Parent.oContained.w_PANOFLAT=='I',3,;
      iif(this.Parent.oContained.w_PANOFLAT=='F',4,;
      iif(this.Parent.oContained.w_PANOFLAT=='E',5,;
      iif(this.Parent.oContained.w_PANOFLAT=='A',6,;
      iif(this.Parent.oContained.w_PANOFLAT=='B',7,;
      iif(this.Parent.oContained.w_PANOFLAT=='C',8,;
      iif(this.Parent.oContained.w_PANOFLAT=='D',9,;
      0)))))))))
  endfunc

  add object oPADATATP_1_14 as StdField with uid="VBYREBKRJF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PADATATP", cQueryName = "PADATATP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data di sistema",;
    HelpContextID = 217800518,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=517, Top=61

  func oPADATATP_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PANOFLAT$ 'SF')
    endwith
   endif
  endfunc

  func oPADATATP_1_14.mHide()
    with this.Parent.oContained
      return (.w_PANOFLAT$ 'IEABCD')
    endwith
  endfunc

  add object oPADATATS_1_15 as StdField with uid="DXZRHDMIEW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PADATATS", cQueryName = "PADATATS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data di sistema",;
    HelpContextID = 217800521,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=653, Top=61

  func oPADATATS_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PANOFLAT$ 'SI')
    endwith
   endif
  endfunc

  func oPADATATS_1_15.mHide()
    with this.Parent.oContained
      return (.w_PANOFLAT$ 'EFABCD')
    endwith
  endfunc


  add object oPAFLCOSE_1_16 as StdCombo with uid="NZOYZUDEMM",rtseq=15,rtrep=.f.,left=223,top=84,width=185,height=22;
    , ToolTipText = "Permette di definire il range di date predefinito per la ricerca delle attivit� sulla gestione elenco attivit�";
    , HelpContextID = 167149371;
    , cFormVar="w_PAFLCOSE",RowSource=""+"Intervallo,"+"Data odierna,"+"Prima data vuota,"+"Seconda data vuota,"+"Entrambe date vuote,"+"Settimana corrente,"+"Settimana corrente+successiva,"+"Settimana corrente+precedente,"+"Settimana corrente+prec.+succ.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPAFLCOSE_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    iif(this.value =5,'E',;
    iif(this.value =6,'A',;
    iif(this.value =7,'B',;
    iif(this.value =8,'C',;
    iif(this.value =9,'D',;
    space(1)))))))))))
  endfunc
  func oPAFLCOSE_1_16.GetRadio()
    this.Parent.oContained.w_PAFLCOSE = this.RadioValue()
    return .t.
  endfunc

  func oPAFLCOSE_1_16.SetRadio()
    this.Parent.oContained.w_PAFLCOSE=trim(this.Parent.oContained.w_PAFLCOSE)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLCOSE=='S',1,;
      iif(this.Parent.oContained.w_PAFLCOSE=='N',2,;
      iif(this.Parent.oContained.w_PAFLCOSE=='I',3,;
      iif(this.Parent.oContained.w_PAFLCOSE=='F',4,;
      iif(this.Parent.oContained.w_PAFLCOSE=='E',5,;
      iif(this.Parent.oContained.w_PAFLCOSE=='A',6,;
      iif(this.Parent.oContained.w_PAFLCOSE=='B',7,;
      iif(this.Parent.oContained.w_PAFLCOSE=='C',8,;
      iif(this.Parent.oContained.w_PAFLCOSE=='D',9,;
      0)))))))))
  endfunc

  add object oPADACOSE_1_17 as StdField with uid="JSMDYAARPO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PADACOSE", cQueryName = "PADACOSE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data di sistema",;
    HelpContextID = 166420283,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=517, Top=84

  func oPADACOSE_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PAFLCOSE$ 'SF')
    endwith
   endif
  endfunc

  func oPADACOSE_1_17.mHide()
    with this.Parent.oContained
      return (.w_PAFLCOSE$ 'IEABCD')
    endwith
  endfunc

  add object oPA_ACOSE_1_18 as StdField with uid="VJTYTHKPSR",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PA_ACOSE", cQueryName = "PA_ACOSE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data di sistema",;
    HelpContextID = 166530875,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=653, Top=84

  func oPA_ACOSE_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PAFLCOSE$ 'SI')
    endwith
   endif
  endfunc

  func oPA_ACOSE_1_18.mHide()
    with this.Parent.oContained
      return (.w_PAFLCOSE$ 'EFABCD')
    endwith
  endfunc


  add object oPANOFLPR_1_19 as StdCombo with uid="JVQFRCYOSY",rtseq=18,rtrep=.f.,left=223,top=107,width=185,height=21;
    , ToolTipText = "Permette di definire il range di date predefinito per la ricerca delle attivit� sulla gestione elenco prestazioni";
    , HelpContextID = 148242616;
    , cFormVar="w_PANOFLPR",RowSource=""+"Intervallo,"+"Data odierna,"+"Prima data vuota,"+"Seconda data vuota,"+"Entrambe date vuote,"+"Settimana corrente,"+"Settimana corrente+successiva,"+"Settimana corrente+precedente,"+"Settimana corrente+prec.+succ.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPANOFLPR_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    iif(this.value =5,'E',;
    iif(this.value =6,'A',;
    iif(this.value =7,'B',;
    iif(this.value =8,'C',;
    iif(this.value =9,'D',;
    space(1)))))))))))
  endfunc
  func oPANOFLPR_1_19.GetRadio()
    this.Parent.oContained.w_PANOFLPR = this.RadioValue()
    return .t.
  endfunc

  func oPANOFLPR_1_19.SetRadio()
    this.Parent.oContained.w_PANOFLPR=trim(this.Parent.oContained.w_PANOFLPR)
    this.value = ;
      iif(this.Parent.oContained.w_PANOFLPR=='S',1,;
      iif(this.Parent.oContained.w_PANOFLPR=='N',2,;
      iif(this.Parent.oContained.w_PANOFLPR=='I',3,;
      iif(this.Parent.oContained.w_PANOFLPR=='F',4,;
      iif(this.Parent.oContained.w_PANOFLPR=='E',5,;
      iif(this.Parent.oContained.w_PANOFLPR=='A',6,;
      iif(this.Parent.oContained.w_PANOFLPR=='B',7,;
      iif(this.Parent.oContained.w_PANOFLPR=='C',8,;
      iif(this.Parent.oContained.w_PANOFLPR=='D',9,;
      0)))))))))
  endfunc

  add object oPADATPRP_1_20 as StdField with uid="THSIHZMUBH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PADATPRP", cQueryName = "PADATPRP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data di sistema per filtro prestazioni",;
    HelpContextID = 67412154,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=517, Top=107

  func oPADATPRP_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PANOFLPR$ 'SF')
    endwith
   endif
  endfunc

  func oPADATPRP_1_20.mHide()
    with this.Parent.oContained
      return (.w_PANOFLPR$ 'IEABCD')
    endwith
  endfunc

  add object oPADATPRS_1_21 as StdField with uid="TKLGRXWMBB",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PADATPRS", cQueryName = "PADATPRS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data di sistema per filtro prestazioni",;
    HelpContextID = 67412151,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=653, Top=107

  func oPADATPRS_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PANOFLPR$ 'SI')
    endwith
   endif
  endfunc

  func oPADATPRS_1_21.mHide()
    with this.Parent.oContained
      return (.w_PANOFLPR$ 'EFABCD')
    endwith
  endfunc

  add object oPAGIOSCA_1_22 as StdField with uid="QASKVIJXHF",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PAGIOSCA", cQueryName = "PAGIOSCA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, numero giorni negativo",;
    ToolTipText = "giorni di determinazione attivit� scadute rispetto alla data di sistema",;
    HelpContextID = 246648631,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=653, Top=130, cSayPict='"999"', cGetPict='"999"'

  func oPAGIOSCA_1_22.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc

  func oPAGIOSCA_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PAGIOSCA>=0)
    endwith
    return bRes
  endfunc

  add object oPAFLRDOC_1_23 as StdCheck with uid="REFSTVPCAP",rtseq=22,rtrep=.f.,left=223, top=130, caption="Ricerca per data documento",;
    ToolTipText = "Se attivo, in stampa ed in elenco prestazioni viene eseguito il filtro sulla data del documento",;
    HelpContextID = 1671367,;
    cFormVar="w_PAFLRDOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLRDOC_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLRDOC_1_23.GetRadio()
    this.Parent.oContained.w_PAFLRDOC = this.RadioValue()
    return .t.
  endfunc

  func oPAFLRDOC_1_23.SetRadio()
    this.Parent.oContained.w_PAFLRDOC=trim(this.Parent.oContained.w_PAFLRDOC)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLRDOC=='S',1,;
      0)
  endfunc

  func oPAFLRDOC_1_23.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc


  add object oPACHKFES_1_24 as StdCombo with uid="LPBPXOXDLG",rtseq=23,rtrep=.f.,left=223,top=153,width=182,height=21;
    , ToolTipText = "Questa combo box permette di definire se nel momento di selezione delle date per la creazione/modifica delle attivit� in agenda deve comparire un messaggio di warning";
    , HelpContextID = 24268617;
    , cFormVar="w_PACHKFES",RowSource=""+"Sabato e domenica,"+"Domenica,"+"Da calendario,"+"Nessun avvertimento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPACHKFES_1_24.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'D',;
    iif(this.value =3,'C',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oPACHKFES_1_24.GetRadio()
    this.Parent.oContained.w_PACHKFES = this.RadioValue()
    return .t.
  endfunc

  func oPACHKFES_1_24.SetRadio()
    this.Parent.oContained.w_PACHKFES=trim(this.Parent.oContained.w_PACHKFES)
    this.value = ;
      iif(this.Parent.oContained.w_PACHKFES=='E',1,;
      iif(this.Parent.oContained.w_PACHKFES=='D',2,;
      iif(this.Parent.oContained.w_PACHKFES=='C',3,;
      iif(this.Parent.oContained.w_PACHKFES=='N',4,;
      0))))
  endfunc

  add object oPACHKPRE_1_25 as StdCheck with uid="WLYJSJGQQP",rtseq=24,rtrep=.f.,left=425, top=153, caption="Controllo su durata prestazioni",;
    ToolTipText = "Se attivo in fase di completamento attivit� esegue controllo sulla somma delle durate prestazioni",;
    HelpContextID = 76394693,;
    cFormVar="w_PACHKPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPACHKPRE_1_25.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPACHKPRE_1_25.GetRadio()
    this.Parent.oContained.w_PACHKPRE = this.RadioValue()
    return .t.
  endfunc

  func oPACHKPRE_1_25.SetRadio()
    this.Parent.oContained.w_PACHKPRE=trim(this.Parent.oContained.w_PACHKPRE)
    this.value = ;
      iif(this.Parent.oContained.w_PACHKPRE=='S',1,;
      0)
  endfunc

  add object oPACALEND_1_26 as StdField with uid="LAMDYZJDBV",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PACALEND", cQueryName = "PACALEND",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Calendario da utilizzare per i controlli sui partecipanti",;
    HelpContextID = 260354246,;
   bGlobalFont=.t.,;
    Height=21, Width=84, Left=223, Top=176, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", cZoomOnZoom="GSAR_ATL", oKey_1_1="TCCODICE", oKey_1_2="this.w_PACALEND"

  func oPACALEND_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PACHKFES='C')
    endwith
   endif
  endfunc

  func oPACALEND_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACALEND_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACALEND_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oPACALEND_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATL',"",'',this.parent.oContained
  endproc
  proc oPACALEND_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_PACALEND
     i_obj.ecpSave()
  endproc


  add object oPACHKCAR_1_27 as StdCombo with uid="BZHKBZSCHL",rtseq=26,rtrep=.f.,left=223,top=199,width=182,height=21;
    , ToolTipText = "Permette di definire se la procedura in fase di conferma delle attivit� deve effettuare dei controlli circa la disponibilit� dei partecipanti";
    , HelpContextID = 26063032;
    , cFormVar="w_PACHKCAR",RowSource=""+"Bloccante,"+"Solo avvertimento,"+"Nessun avvertimento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPACHKCAR_1_27.RadioValue()
    return(iif(this.value =1,'B',;
    iif(this.value =2,'A',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oPACHKCAR_1_27.GetRadio()
    this.Parent.oContained.w_PACHKCAR = this.RadioValue()
    return .t.
  endfunc

  func oPACHKCAR_1_27.SetRadio()
    this.Parent.oContained.w_PACHKCAR=trim(this.Parent.oContained.w_PACHKCAR)
    this.value = ;
      iif(this.Parent.oContained.w_PACHKCAR=='B',1,;
      iif(this.Parent.oContained.w_PACHKCAR=='A',2,;
      iif(this.Parent.oContained.w_PACHKCAR=='N',3,;
      0)))
  endfunc

  add object oPAVISTRC_1_28 as StdCheck with uid="AQBXHISHYZ",rtseq=27,rtrep=.f.,left=425, top=199, caption="Visualizza la tracciabilit� delle attivit�",;
    ToolTipText = "Visualizzazione tracciabilit� delle attivit�",;
    HelpContextID = 753863,;
    cFormVar="w_PAVISTRC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAVISTRC_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPAVISTRC_1_28.GetRadio()
    this.Parent.oContained.w_PAVISTRC = this.RadioValue()
    return .t.
  endfunc

  func oPAVISTRC_1_28.SetRadio()
    this.Parent.oContained.w_PAVISTRC=trim(this.Parent.oContained.w_PAVISTRC)
    this.value = ;
      iif(this.Parent.oContained.w_PAVISTRC=='S',1,;
      0)
  endfunc

  func oPAVISTRC_1_28.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc


  add object oPAFLVISI_1_45 as StdCombo with uid="AAVYSTBGIC",rtseq=37,rtrep=.f.,left=223,top=222,width=182,height=21;
    , ToolTipText = "Modalit� di visualizzazione delle attivit�";
    , HelpContextID = 86409023;
    , cFormVar="w_PAFLVISI",RowSource=""+"Libera,"+"Da struttura aziendale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPAFLVISI_1_45.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oPAFLVISI_1_45.GetRadio()
    this.Parent.oContained.w_PAFLVISI = this.RadioValue()
    return .t.
  endfunc

  func oPAFLVISI_1_45.SetRadio()
    this.Parent.oContained.w_PAFLVISI=trim(this.Parent.oContained.w_PAFLVISI)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLVISI=='L',1,;
      iif(this.Parent.oContained.w_PAFLVISI=='S',2,;
      0))
  endfunc

  add object oPAFLANTI_1_46 as StdCheck with uid="IKLZXLKCSC",rtseq=38,rtrep=.f.,left=425, top=222, caption="Calcola termine a ritroso se giorno festivo",;
    ToolTipText = "Se attivo, per il compimento degli atti processuali che scadono in un giorno festivo  viene anticipato il calcolo della data al primo giorno antecedente non festivo ",;
    HelpContextID = 148275007,;
    cFormVar="w_PAFLANTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLANTI_1_46.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLANTI_1_46.GetRadio()
    this.Parent.oContained.w_PAFLANTI = this.RadioValue()
    return .t.
  endfunc

  func oPAFLANTI_1_46.SetRadio()
    this.Parent.oContained.w_PAFLANTI=trim(this.Parent.oContained.w_PAFLANTI)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLANTI=='S',1,;
      0)
  endfunc

  func oPAFLANTI_1_46.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oDESCAL_1_53 as StdField with uid="QQDKEHKLHI",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCAL", cQueryName = "DESCAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 154250698,;
   bGlobalFont=.t.,;
    Height=21, Width=361, Left=322, Top=176, InputMask=replicate('X',40)

  add object oDESACQ_1_56 as StdField with uid="VGNHUCIBNC",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESACQ", cQueryName = "DESACQ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 68398538,;
   bGlobalFont=.t.,;
    Height=21, Width=394, Left=289, Top=38, InputMask=replicate('X',40)

  func oDESACQ_1_56.mHide()
    with this.Parent.oContained
      return (isalt()  or  .w_ATTIVACQ='N' )
    endwith
  endfunc

  add object oEDESLIS_1_58 as StdField with uid="BDVRAWGGYO",rtseq=44,rtrep=.f.,;
    cFormVar = "w_EDESLIS", cQueryName = "EDESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 76378438,;
   bGlobalFont=.t.,;
    Height=21, Width=394, Left=289, Top=15, InputMask=replicate('X',40)

  func oEDESLIS_1_58.mHide()
    with this.Parent.oContained
      return (!Isahe())
    endwith
  endfunc

  add object oPAGESRIS_1_61 as StdCheck with uid="EVKBQSTNXE",rtseq=47,rtrep=.f.,left=223, top=245, caption="Gestione attivit� riservate/nascoste",;
    ToolTipText = "Se attivo nelle visualizzazioni delle attivit� non saranno mostrati dati sensibili come descrizione attivit�, nominativo, ecc. oppure nasconde integralmente l'attivit� dalla visualizzazione",;
    HelpContextID = 233803593,;
    cFormVar="w_PAGESRIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGESRIS_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPAGESRIS_1_61.GetRadio()
    this.Parent.oContained.w_PAGESRIS = this.RadioValue()
    return .t.
  endfunc

  func oPAGESRIS_1_61.SetRadio()
    this.Parent.oContained.w_PAGESRIS=trim(this.Parent.oContained.w_PAGESRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PAGESRIS=='S',1,;
      0)
  endfunc

  add object oPADESRIS_1_62 as StdField with uid="EFVLOKMOUV",rtseq=48,rtrep=.f.,;
    cFormVar = "w_PADESRIS", cQueryName = "PADESRIS",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Testo visualizzato nel caso il dato sia riservato",;
    HelpContextID = 233791305,;
   bGlobalFont=.t.,;
    Height=21, Width=459, Left=223, Top=268, InputMask=replicate('X',254)

  func oPADESRIS_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PAGESRIS='S')
    endwith
   endif
  endfunc


  add object oPAST_ATT_1_63 as StdCombo with uid="UTKJCWQSJI",rtseq=49,rtrep=.f.,left=223,top=291,width=145,height=21;
    , ToolTipText = "Permette di definire lo stato attivit� da proporre in agenda e nelle maschere di elenco/stampa attivit�";
    , HelpContextID = 230641482;
    , cFormVar="w_PAST_ATT",RowSource=""+"Non evasa,"+"Da svolgere,"+"In corso,"+"Provvisoria,"+"Evasa,"+"Completata,"+"Evasa o completata,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPAST_ATT_1_63.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'T',;
    iif(this.value =5,'F',;
    iif(this.value =6,'P',;
    iif(this.value =7,'B',;
    iif(this.value =8,'K',;
    space(1))))))))))
  endfunc
  func oPAST_ATT_1_63.GetRadio()
    this.Parent.oContained.w_PAST_ATT = this.RadioValue()
    return .t.
  endfunc

  func oPAST_ATT_1_63.SetRadio()
    this.Parent.oContained.w_PAST_ATT=trim(this.Parent.oContained.w_PAST_ATT)
    this.value = ;
      iif(this.Parent.oContained.w_PAST_ATT=='N',1,;
      iif(this.Parent.oContained.w_PAST_ATT=='D',2,;
      iif(this.Parent.oContained.w_PAST_ATT=='I',3,;
      iif(this.Parent.oContained.w_PAST_ATT=='T',4,;
      iif(this.Parent.oContained.w_PAST_ATT=='F',5,;
      iif(this.Parent.oContained.w_PAST_ATT=='P',6,;
      iif(this.Parent.oContained.w_PAST_ATT=='B',7,;
      iif(this.Parent.oContained.w_PAST_ATT=='K',8,;
      0))))))))
  endfunc

  func oPAST_ATT_1_63.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oPACHKCPL_1_69 as StdCheck with uid="AQGYEIZWFP",rtseq=53,rtrep=.f.,left=425, top=291, caption="Controllo in completamento del rinvio/riserva",;
    ToolTipText = "Se attivo, appare messaggio di avviso nel caso non venga completato un rinvio o una riserva",;
    HelpContextID = 26063038,;
    cFormVar="w_PACHKCPL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPACHKCPL_1_69.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPACHKCPL_1_69.GetRadio()
    this.Parent.oContained.w_PACHKCPL = this.RadioValue()
    return .t.
  endfunc

  func oPACHKCPL_1_69.SetRadio()
    this.Parent.oContained.w_PACHKCPL=trim(this.Parent.oContained.w_PACHKCPL)
    this.value = ;
      iif(this.Parent.oContained.w_PACHKCPL=='S',1,;
      0)
  endfunc

  func oPACHKCPL_1_69.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc


  add object oPACHKING_1_70 as StdCombo with uid="EYCNDVXTHG",rtseq=54,rtrep=.f.,left=223,top=314,width=145,height=21;
    , ToolTipText = "Inserire il tipo di controllo sul caricamento del resoconto giornaliero dell'utente da utilizzare all'ingresso della procedura";
    , HelpContextID = 193835203;
    , cFormVar="w_PACHKING",RowSource=""+"Data persona,"+"Data fissa,"+"Giorni indietro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPACHKING_1_70.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'F',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oPACHKING_1_70.GetRadio()
    this.Parent.oContained.w_PACHKING = this.RadioValue()
    return .t.
  endfunc

  func oPACHKING_1_70.SetRadio()
    this.Parent.oContained.w_PACHKING=trim(this.Parent.oContained.w_PACHKING)
    this.value = ;
      iif(this.Parent.oContained.w_PACHKING=='P',1,;
      iif(this.Parent.oContained.w_PACHKING=='F',2,;
      iif(this.Parent.oContained.w_PACHKING=='A',3,;
      0)))
  endfunc

  add object oPADATINI_1_71 as StdField with uid="HRNWMUEISA",rtseq=55,rtrep=.f.,;
    cFormVar = "w_PADATINI", cQueryName = "PADATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Inserire la data da cui avr� inizio il controllo dei resoconti giornalieri degli utenti",;
    HelpContextID = 184852673,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=379, Top=314

  func oPADATINI_1_71.mHide()
    with this.Parent.oContained
      return (.w_PACHKING<>'F')
    endwith
  endfunc

  add object oPAGIOCHK_1_72 as StdField with uid="RRKRVZQFSL",rtseq=56,rtrep=.f.,;
    cFormVar = "w_PAGIOCHK", cQueryName = "PAGIOCHK",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore positivo",;
    ToolTipText = "Inserire il numero di giorni precedenti alla data del client per determinare quella da cui avr� inizio il controllo dei resoconti giornalieri degli utenti",;
    HelpContextID = 246648641,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=379, Top=314, cSayPict="'999'", cGetPict="'999'"

  func oPAGIOCHK_1_72.mHide()
    with this.Parent.oContained
      return (.w_PACHKING<>'A')
    endwith
  endfunc

  func oPAGIOCHK_1_72.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PAGIOCHK > 0)
    endwith
    return bRes
  endfunc


  add object oPACODMDC_1_74 as StdTableCombo with uid="KYJSSLDWXE",rtseq=57,rtrep=.f.,left=223,top=337,width=257,height=21;
    , ToolTipText = "Metodo di calcolo della periodicit� predefinito per le attivit� ricorrenti";
    , HelpContextID = 134827833;
    , cFormVar="w_PACODMDC",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="MODCLDAT";
    , cTable='MODCLDAT',cKey='MDCODICE',cValue='MDDESCRI',cOrderBy='MDDESCRI',xDefault=space(3);
  , bGlobalFont=.t.


  func oPACODMDC_1_74.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_74('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODMDC_1_74.ecpDrop(oSource)
    this.Parent.oContained.link_1_74('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oPCCAUABB_1_75 as StdField with uid="QZPZQIKAJR",rtseq=58,rtrep=.f.,;
    cFormVar = "w_PCCAUABB", cQueryName = "PCCAUABB",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale incongruente",;
    ToolTipText = "Causale nuovo documento abbinato",;
    HelpContextID = 49589960,;
   bGlobalFont=.t.,;
    Height=21, Width=66, Left=223, Top=360, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PCCAUABB"

  func oPCCAUABB_1_75.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oPCCAUABB_1_75.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_75('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCCAUABB_1_75.ecpDrop(oSource)
    this.Parent.oContained.link_1_75('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCCAUABB_1_75.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPCCAUABB_1_75'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Tipi documenti",'GSAG0MCA.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPCCAUABB_1_75.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PCCAUABB
     i_obj.ecpSave()
  endproc


  add object oPACALDAT_1_76 as StdCombo with uid="NIWXRKTMQX",rtseq=59,rtrep=.f.,left=223,top=383,width=145,height=21;
    , ToolTipText = "Selezionare il metodo di calcolo della data inizio attivit� da proporre nella maschera Nuove attivit� collegate";
    , HelpContextID = 8695990;
    , cFormVar="w_PACALDAT",RowSource=""+"Da tipo attivit�,"+"Con conferma,"+"Automatica,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPACALDAT_1_76.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    iif(this.value =3,'A',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oPACALDAT_1_76.GetRadio()
    this.Parent.oContained.w_PACALDAT = this.RadioValue()
    return .t.
  endfunc

  func oPACALDAT_1_76.SetRadio()
    this.Parent.oContained.w_PACALDAT=trim(this.Parent.oContained.w_PACALDAT)
    this.value = ;
      iif(this.Parent.oContained.w_PACALDAT=='T',1,;
      iif(this.Parent.oContained.w_PACALDAT=='C',2,;
      iif(this.Parent.oContained.w_PACALDAT=='A',3,;
      iif(this.Parent.oContained.w_PACALDAT=='N',4,;
      0))))
  endfunc

  add object oPACHKATT_1_77 as StdField with uid="VXXODDYXDN",rtseq=60,rtrep=.f.,;
    cFormVar = "w_PACHKATT", cQueryName = "PACHKATT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni successivi alla data di sistema rispetto ai quali viene eseguito un controllo sulla data di una nuova attivit�",;
    HelpContextID = 208817994,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=649, Top=383, cSayPict='"999"', cGetPict='"999"'

  func oPACHKATT_1_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PACHKATT>=0)
    endwith
    return bRes
  endfunc

  add object oDESCAU_1_82 as StdField with uid="XAVKXDJLYN",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 3255754,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=292, Top=360, InputMask=replicate('X',35)

  func oDESCAU_1_82.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPAFLESAT_1_90 as StdCheck with uid="YAXSJMQZIX",rtseq=67,rtrep=.f.,left=223, top=408, caption="Controllo inesist. attivit� per pratica",;
    ToolTipText = "Se attivo, evadendo/completando/eliminando un'attivit� associata ad una pratica apparir� un messaggio che segnala l'inesistenza di attivit� non evase ad essa associate.",;
    HelpContextID = 32080054,;
    cFormVar="w_PAFLESAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLESAT_1_90.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLESAT_1_90.GetRadio()
    this.Parent.oContained.w_PAFLESAT = this.RadioValue()
    return .t.
  endfunc

  func oPAFLESAT_1_90.SetRadio()
    this.Parent.oContained.w_PAFLESAT=trim(this.Parent.oContained.w_PAFLESAT)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLESAT=='S',1,;
      0)
  endfunc

  func oPAFLESAT_1_90.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oPAFLDTRP_1_91 as StdCheck with uid="NUZJVYBATT",rtseq=68,rtrep=.f.,left=223, top=429, caption="Inserisci la data maggiore in inserimento provvisorio prestazioni",;
    ToolTipText = "Se attivo, fa in modo che caricando una nuova riga inserisce la data maggiore tra quelle utilizzate. Diversamente sar� riportata la data odierna",;
    HelpContextID = 16351418,;
    cFormVar="w_PAFLDTRP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLDTRP_1_91.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLDTRP_1_91.GetRadio()
    this.Parent.oContained.w_PAFLDTRP = this.RadioValue()
    return .t.
  endfunc

  func oPAFLDTRP_1_91.SetRadio()
    this.Parent.oContained.w_PAFLDTRP=trim(this.Parent.oContained.w_PAFLDTRP)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLDTRP=='S',1,;
      0)
  endfunc

  add object oPAATTRIS_1_92 as StdField with uid="FCNUCRPYUW",rtseq=69,rtrep=.f.,;
    cFormVar = "w_PAATTRIS", cQueryName = "PAATTRIS",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo di attivit� predefinito da utilizzare per caricare una nuova attivit� se il controllo di inesistenza avr� rilevato che non esistono attivit� da evadere, in corso o provvisorie",;
    HelpContextID = 235810633,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=530, Top=408, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PAATTRIS"

  func oPAATTRIS_1_92.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR .w_PAFLESAT<>'S')
    endwith
  endfunc

  func oPAATTRIS_1_92.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_92('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAATTRIS_1_92.ecpDrop(oSource)
    this.Parent.oContained.link_1_92('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAATTRIS_1_92.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPAATTRIS_1_92'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPAATTRIS_1_92.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PAATTRIS
     i_obj.ecpSave()
  endproc

  add object oPAFLSIAT_1_93 as StdCheck with uid="SDWZYMEBIE",rtseq=70,rtrep=.f.,left=223, top=450, caption="Genera singola attivit�",;
    ToolTipText = "Se attivo, verr� generata una singola attivit� per ogni prestazione (in fase di inserimento definitivo delle prestazioni provvisorie)",;
    HelpContextID = 185172150,;
    cFormVar="w_PAFLSIAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLSIAT_1_93.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLSIAT_1_93.GetRadio()
    this.Parent.oContained.w_PAFLSIAT = this.RadioValue()
    return .t.
  endfunc

  func oPAFLSIAT_1_93.SetRadio()
    this.Parent.oContained.w_PAFLSIAT=trim(this.Parent.oContained.w_PAFLSIAT)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLSIAT=='S',1,;
      0)
  endfunc

  add object oPAFLPRES_1_94 as StdCheck with uid="EXPPOOODHM",rtseq=71,rtrep=.f.,left=223, top=471, caption="Controllo su inserimento/variazione prestazioni",;
    ToolTipText = "Se attivo, abilita il controllo sulle persone relativo all'inserimento/variazione delle prestazioni oltre un intervallo di date",;
    HelpContextID = 231112521,;
    cFormVar="w_PAFLPRES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLPRES_1_94.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLPRES_1_94.GetRadio()
    this.Parent.oContained.w_PAFLPRES = this.RadioValue()
    return .t.
  endfunc

  func oPAFLPRES_1_94.SetRadio()
    this.Parent.oContained.w_PAFLPRES=trim(this.Parent.oContained.w_PAFLPRES)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLPRES=='S',1,;
      0)
  endfunc

  func oPAFLPRES_1_94.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oPAFLR_AG_1_102 as StdCheck with uid="EKEKUHCHVG",rtseq=166,rtrep=.f.,left=223, top=492, caption="Abilita ricerca per descrizione aggiuntiva in stampa ed elenco prestazioni",;
    ToolTipText = "Se attivo, la stringa inserita per la ricerca per descrizione sar� utilizzata anche per la descrizione aggiuntiva",;
    HelpContextID = 85557443,;
    cFormVar="w_PAFLR_AG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLR_AG_1_102.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAFLR_AG_1_102.GetRadio()
    this.Parent.oContained.w_PAFLR_AG = this.RadioValue()
    return .t.
  endfunc

  func oPAFLR_AG_1_102.SetRadio()
    this.Parent.oContained.w_PAFLR_AG=trim(this.Parent.oContained.w_PAFLR_AG)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLR_AG=='S',1,;
      0)
  endfunc

  func oPAFLR_AG_1_102.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="VFZMZNDZLK",Visible=.t., Left=4, Top=15,;
    Alignment=1, Width=215, Height=18,;
    Caption="Listino preferenziale:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="BSOGHKRNWR",Visible=.t., Left=4, Top=61,;
    Alignment=1, Width=215, Height=18,;
    Caption="Filtri data ricerca attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="HUZENCKCWV",Visible=.t., Left=416, Top=61,;
    Alignment=1, Width=100, Height=18,;
    Caption="Giorni indietro:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_PANOFLAT$ 'IEABCD')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="CJAGLJRGTY",Visible=.t., Left=556, Top=61,;
    Alignment=1, Width=96, Height=18,;
    Caption="Giorni in avanti:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_PANOFLAT$ 'EFABCD')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="FDHSZGATKE",Visible=.t., Left=4, Top=107,;
    Alignment=1, Width=215, Height=18,;
    Caption="Filtri data ricerca prestazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="XBMKUZEDWD",Visible=.t., Left=416, Top=107,;
    Alignment=1, Width=100, Height=18,;
    Caption="Giorni indietro:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_PANOFLPR$ 'IEABCD')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="SZICGJABAD",Visible=.t., Left=556, Top=107,;
    Alignment=1, Width=96, Height=18,;
    Caption="Giorni in avanti:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.w_PANOFLPR$ 'EFABCD')
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="DAQOAZXKBC",Visible=.t., Left=4, Top=153,;
    Alignment=1, Width=215, Height=18,;
    Caption="Avvertimento su:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="NVQMJWCEOU",Visible=.t., Left=4, Top=222,;
    Alignment=1, Width=215, Height=18,;
    Caption="Modalit� di visualizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="JYCIUJITUO",Visible=.t., Left=4, Top=199,;
    Alignment=1, Width=215, Height=18,;
    Caption="Avvertimento calendario partecipanti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="NOVZGESAOQ",Visible=.t., Left=4, Top=176,;
    Alignment=1, Width=215, Height=18,;
    Caption="Calendario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="WVHQLXPDHO",Visible=.t., Left=4, Top=38,;
    Alignment=1, Width=215, Height=18,;
    Caption="Listino costi:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (isalt()  or  .w_ATTIVACQ='N' )
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="GMKEFFXBMY",Visible=.t., Left=4, Top=15,;
    Alignment=1, Width=215, Height=18,;
    Caption="Listino ricavi:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="MADNPRPOKO",Visible=.t., Left=4, Top=268,;
    Alignment=1, Width=215, Height=18,;
    Caption="Testo sostitutivo dati riservati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="IUSCGACWLB",Visible=.t., Left=4, Top=291,;
    Alignment=1, Width=215, Height=18,;
    Caption="Stato attivit� predefinito:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="DTABAJXZWT",Visible=.t., Left=4, Top=314,;
    Alignment=1, Width=215, Height=18,;
    Caption="Controllo ingresso procedura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="TQKSAXIOJS",Visible=.t., Left=4, Top=337,;
    Alignment=1, Width=215, Height=18,;
    Caption="Metodo calcolo periodicit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="KVFXFFQATH",Visible=.t., Left=532, Top=130,;
    Alignment=1, Width=120, Height=18,;
    Caption="Giorni attivit� scadute:"  ;
  , bGlobalFont=.t.

  func oStr_1_79.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="EEALFUIMVT",Visible=.t., Left=4, Top=360,;
    Alignment=1, Width=215, Height=18,;
    Caption="Causale doc. di abbinamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_80.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_87 as StdString with uid="LTHMCOKXWX",Visible=.t., Left=4, Top=383,;
    Alignment=1, Width=215, Height=18,;
    Caption="Calcolo data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="BIXEAKJNZY",Visible=.t., Left=372, Top=383,;
    Alignment=1, Width=273, Height=18,;
    Caption="Avvertimento caricamento data superiore di giorni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_95 as StdString with uid="SDVEJPVBBO",Visible=.t., Left=451, Top=408,;
    Alignment=0, Width=75, Height=18,;
    Caption="Nuova attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_95.mHide()
    with this.Parent.oContained
      return (.w_PAFLESAT<>'S')
    endwith
  endfunc

  add object oStr_1_98 as StdString with uid="UPEVAAACPJ",Visible=.t., Left=4, Top=84,;
    Alignment=1, Width=215, Height=18,;
    Caption="Filtri data ricerca cose da fare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_99 as StdString with uid="VHJCXUYSYO",Visible=.t., Left=416, Top=84,;
    Alignment=1, Width=100, Height=18,;
    Caption="Giorni indietro:"  ;
  , bGlobalFont=.t.

  func oStr_1_99.mHide()
    with this.Parent.oContained
      return (.w_PAFLCOSE$ 'IEABCD')
    endwith
  endfunc

  add object oStr_1_100 as StdString with uid="XUGWGCGRHB",Visible=.t., Left=556, Top=84,;
    Alignment=1, Width=96, Height=18,;
    Caption="Giorni in avanti:"  ;
  , bGlobalFont=.t.

  func oStr_1_100.mHide()
    with this.Parent.oContained
      return (.w_PAFLCOSE$ 'EFABCD')
    endwith
  endfunc
enddefine
define class tgsag_apaPag2 as StdContainer
  Width  = 706
  height = 537
  stdWidth  = 706
  stdheight = 537
  resizeXpos=434
  resizeYpos=192
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPAOGGCRE_2_1 as StdField with uid="YYJGPKOTYT",rtseq=50,rtrep=.f.,;
    cFormVar = "w_PAOGGCRE", cQueryName = "PAOGGCRE",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto per e-mail creazione attivit�",;
    HelpContextID = 30273733,;
   bGlobalFont=.t.,;
    Height=21, Width=533, Left=158, Top=39, InputMask=replicate('X',254), cMenuFile="GSAG_APA_E_Mail"

  add object oPATXTCRE_2_2 as StdMemo with uid="KMNNSYUTJE",rtseq=51,rtrep=.f.,;
    cFormVar = "w_PATXTCRE", cQueryName = "PATXTCRE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo per avvisi creazione attivit�",;
    HelpContextID = 15507653,;
   bGlobalFont=.t.,;
    Height=227, Width=533, Left=158, Top=67, cMenuFile="GSAG_APA_E_Mail"

  add object oPAPSTCRE_2_3 as StdMemo with uid="CXLKBPVYJS",rtseq=52,rtrep=.f.,;
    cFormVar = "w_PAPSTCRE", cQueryName = "PAPSTCRE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo per post-in creazione attivit�",;
    HelpContextID = 15851717,;
   bGlobalFont=.t.,;
    Height=227, Width=533, Left=158, Top=301, cMenuFile="GSAG_APA_Postin"


  add object oObj_2_6 as cp_setobjprop with uid="DWMRAZCEYD",left=9, top=557, width=170,height=21,;
    caption='Tasto dx su PAOGGCRE',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PAOGGCRE",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 72588389


  add object oObj_2_7 as cp_setobjprop with uid="QGTRHOMYDN",left=9, top=579, width=170,height=21,;
    caption='Tasto dx su PATXTCRE',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PATXTCRE",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 72588614


  add object oObj_2_11 as cp_setobjprop with uid="JQVWTSHMNH",left=9, top=601, width=170,height=21,;
    caption='Tasto dx su PAPSTCRE',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PAPSTCRE",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 72588609

  add object oStr_2_4 as StdString with uid="RROACVETMX",Visible=.t., Left=5, Top=65,;
    Alignment=1, Width=149, Height=18,;
    Caption="Testo e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="WTLOCWVWHK",Visible=.t., Left=5, Top=41,;
    Alignment=1, Width=149, Height=18,;
    Caption="Oggetto e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="BLBSMVKIXE",Visible=.t., Left=8, Top=11,;
    Alignment=0, Width=231, Height=18,;
    Caption="Avvisi creazione attivit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_10 as StdString with uid="YPASDZGIHA",Visible=.t., Left=5, Top=301,;
    Alignment=1, Width=149, Height=18,;
    Caption="Testo post-in:"  ;
  , bGlobalFont=.t.

  add object oBox_2_9 as StdBox with uid="KSMOIYVBPN",left=5, top=31, width=687,height=2
enddefine
define class tgsag_apaPag3 as StdContainer
  Width  = 706
  height = 537
  stdWidth  = 706
  stdheight = 537
  resizeXpos=402
  resizeYpos=204
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPAOGGMOD_3_1 as StdField with uid="TJDDKBLHJG",rtseq=159,rtrep=.f.,;
    cFormVar = "w_PAOGGMOD", cQueryName = "PAOGGMOD",;
    bObbl = .f. , nPag = 3, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto per e-mail modifica attivit�",;
    HelpContextID = 130937030,;
   bGlobalFont=.t.,;
    Height=21, Width=533, Left=158, Top=39, InputMask=replicate('X',254), cMenuFile="GSAG_APA_E_Mail"

  add object oPATXTMOD_3_2 as StdMemo with uid="WIQYKSENXE",rtseq=160,rtrep=.f.,;
    cFormVar = "w_PATXTMOD", cQueryName = "PATXTMOD",;
    bObbl = .f. , nPag = 3, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo per avvisi modifica attivit�",;
    HelpContextID = 116170950,;
   bGlobalFont=.t.,;
    Height=227, Width=533, Left=158, Top=67, cMenuFile="GSAG_APA_E_Mail"

  add object oPAPSTMOD_3_3 as StdMemo with uid="HZSHVPAQZE",rtseq=161,rtrep=.f.,;
    cFormVar = "w_PAPSTMOD", cQueryName = "PAPSTMOD",;
    bObbl = .f. , nPag = 3, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo per post-in modifica attivit�",;
    HelpContextID = 116515014,;
   bGlobalFont=.t.,;
    Height=227, Width=533, Left=158, Top=301, cMenuFile="GSAG_APA_Postin"


  add object oObj_3_9 as cp_setobjprop with uid="PVWYMTJTSC",left=145, top=596, width=170,height=21,;
    caption='Tasto dx su PAOGGMOD',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PAOGGMOD",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 72513125


  add object oObj_3_10 as cp_setobjprop with uid="QWTVHSBKHU",left=145, top=633, width=170,height=21,;
    caption='Tasto dx su PATXTMOD',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PATXTMOD",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 72513350


  add object oObj_3_11 as cp_setobjprop with uid="QXCGHLNPTV",left=145, top=613, width=170,height=21,;
    caption='Tasto dx su PAPSTMOD',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PAPSTMOD",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 72513345

  add object oStr_3_4 as StdString with uid="OPIWEBHFZJ",Visible=.t., Left=5, Top=65,;
    Alignment=1, Width=149, Height=18,;
    Caption="Testo e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_3_5 as StdString with uid="BYYJTHVPPM",Visible=.t., Left=5, Top=41,;
    Alignment=1, Width=149, Height=18,;
    Caption="Oggetto e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_3_6 as StdString with uid="NIGORHEYRH",Visible=.t., Left=7, Top=11,;
    Alignment=0, Width=231, Height=18,;
    Caption="Avvisi modifica attivit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_8 as StdString with uid="TDDHIDTFKE",Visible=.t., Left=5, Top=301,;
    Alignment=1, Width=149, Height=18,;
    Caption="Testo post-in:"  ;
  , bGlobalFont=.t.

  add object oBox_3_7 as StdBox with uid="JFGWTTRUHT",left=5, top=31, width=687,height=2
enddefine
define class tgsag_apaPag4 as StdContainer
  Width  = 706
  height = 537
  stdWidth  = 706
  stdheight = 537
  resizeXpos=601
  resizeYpos=209
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPAOGGDEL_4_1 as StdField with uid="JCXIMPODUI",rtseq=162,rtrep=.f.,;
    cFormVar = "w_PAOGGDEL", cQueryName = "PAOGGDEL",;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto per e-mail cancellazione attivit�",;
    HelpContextID = 254938946,;
   bGlobalFont=.t.,;
    Height=21, Width=533, Left=158, Top=39, InputMask=replicate('X',254), cMenuFile="GSAG_APA_E_Mail"

  add object oPATXTDEL_4_2 as StdMemo with uid="KHRRHQRBGX",rtseq=163,rtrep=.f.,;
    cFormVar = "w_PATXTDEL", cQueryName = "PATXTDEL",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo per avvisi cancellazione attivit�",;
    HelpContextID = 1269570,;
   bGlobalFont=.t.,;
    Height=227, Width=533, Left=158, Top=67, cMenuFile="GSAG_APA_E_Mail"

  add object oPAPSTDEL_4_3 as StdMemo with uid="FOBGKVFBWW",rtseq=164,rtrep=.f.,;
    cFormVar = "w_PAPSTDEL", cQueryName = "PAPSTDEL",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo per post-in cancellazione attivit�",;
    HelpContextID = 925506,;
   bGlobalFont=.t.,;
    Height=227, Width=533, Left=158, Top=301, cMenuFile="GSAG_APA_Postin"


  add object oObj_4_9 as cp_setobjprop with uid="IHKEHBTRMV",left=15, top=558, width=170,height=21,;
    caption='Tasto dx su PAOGGDEL',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PAOGGDEL",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 72994149


  add object oObj_4_10 as cp_setobjprop with uid="PBDQJZSXUZ",left=15, top=597, width=170,height=21,;
    caption='Tasto dx su PATXTDEL',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PATXTDEL",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 72994374


  add object oObj_4_11 as cp_setobjprop with uid="ECYSLUOEIP",left=15, top=577, width=170,height=21,;
    caption='Tasto dx su PAPSTDEL',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PAPSTDEL",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 72994369

  add object oStr_4_4 as StdString with uid="FIDRTPOIVZ",Visible=.t., Left=5, Top=41,;
    Alignment=1, Width=149, Height=18,;
    Caption="Oggetto e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_4_5 as StdString with uid="DTOSBAMTYG",Visible=.t., Left=5, Top=66,;
    Alignment=1, Width=149, Height=18,;
    Caption="Testo e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_4_6 as StdString with uid="DOSCTJMYXO",Visible=.t., Left=8, Top=11,;
    Alignment=0, Width=231, Height=18,;
    Caption="Avvisi cancellazione attivit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_8 as StdString with uid="RYFMHBTVZY",Visible=.t., Left=5, Top=301,;
    Alignment=1, Width=149, Height=18,;
    Caption="Testo post-in:"  ;
  , bGlobalFont=.t.

  add object oBox_4_7 as StdBox with uid="CCWNYVBBCN",left=5, top=31, width=687,height=2
enddefine
define class tgsag_apaPag5 as StdContainer
  Width  = 706
  height = 537
  stdWidth  = 706
  stdheight = 537
  resizeXpos=472
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_5_1 as cp_setobjprop with uid="RSWVRNZHDD",left=56, top=559, width=170,height=21,;
    caption='Tasto dx su PAPSTCRE',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PAPSTCRE",;
    cEvent = "Init",;
    nPag=5;
    , HelpContextID = 72588609


  add object oObj_5_2 as cp_setobjprop with uid="SIWTYXCAKG",left=56, top=580, width=170,height=21,;
    caption='Tasto dx su PAPSTMOD',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PAPSTMOD",;
    cEvent = "Init",;
    nPag=5;
    , HelpContextID = 72513345


  add object oObj_5_3 as cp_setobjprop with uid="WQPNVCOZBV",left=56, top=601, width=170,height=21,;
    caption='Tasto dx su PAPSTDEL',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PAPSTDEL",;
    cEvent = "Init",;
    nPag=5;
    , HelpContextID = 72994369

  add object oPAFLAPPU_5_6 as StdCheck with uid="WZPCTEPCRB",rtseq=127,rtrep=.f.,left=37, top=40, caption="Appuntamenti",;
    ToolTipText = "Se attivo, viene selezionato il flag Appuntamenti in agenda",;
    HelpContextID = 86606005,;
    cFormVar="w_PAFLAPPU", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLAPPU_5_6.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAFLAPPU_5_6.GetRadio()
    this.Parent.oContained.w_PAFLAPPU = this.RadioValue()
    return .t.
  endfunc

  func oPAFLAPPU_5_6.SetRadio()
    this.Parent.oContained.w_PAFLAPPU=trim(this.Parent.oContained.w_PAFLAPPU)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLAPPU=='S',1,;
      0)
  endfunc

  add object oPAFLATGE_5_7 as StdCheck with uid="WPMIJYDZSS",rtseq=128,rtrep=.f.,left=37, top=61, caption="Attivit� generiche",;
    ToolTipText = "Se attivo, viene selezionato il flag Attivit� generiche in agenda",;
    HelpContextID = 248938299,;
    cFormVar="w_PAFLATGE", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLATGE_5_7.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oPAFLATGE_5_7.GetRadio()
    this.Parent.oContained.w_PAFLATGE = this.RadioValue()
    return .t.
  endfunc

  func oPAFLATGE_5_7.SetRadio()
    this.Parent.oContained.w_PAFLATGE=trim(this.Parent.oContained.w_PAFLATGE)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLATGE=='G',1,;
      0)
  endfunc

  add object oPAFLUDIE_5_8 as StdCheck with uid="YXWHOTPEXB",rtseq=129,rtrep=.f.,left=37, top=82, caption="Udienze",;
    ToolTipText = "Se attivo, viene selezionato il flag Udienze in agenda",;
    HelpContextID = 1474363,;
    cFormVar="w_PAFLUDIE", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLUDIE_5_8.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oPAFLUDIE_5_8.GetRadio()
    this.Parent.oContained.w_PAFLUDIE = this.RadioValue()
    return .t.
  endfunc

  func oPAFLUDIE_5_8.SetRadio()
    this.Parent.oContained.w_PAFLUDIE=trim(this.Parent.oContained.w_PAFLUDIE)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLUDIE=='U',1,;
      0)
  endfunc

  func oPAFLUDIE_5_8.mHide()
    with this.Parent.oContained
      return (!ISALT())
    endwith
  endfunc

  add object oPAFLSEST_5_9 as StdCheck with uid="NBYRVAHNOI",rtseq=130,rtrep=.f.,left=221, top=40, caption="Sessioni telefoniche",;
    ToolTipText = "Se attivo, viene selezionato il flag Sessioni telefoniche in agenda",;
    HelpContextID = 16154442,;
    cFormVar="w_PAFLSEST", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLSEST_5_9.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oPAFLSEST_5_9.GetRadio()
    this.Parent.oContained.w_PAFLSEST = this.RadioValue()
    return .t.
  endfunc

  func oPAFLSEST_5_9.SetRadio()
    this.Parent.oContained.w_PAFLSEST=trim(this.Parent.oContained.w_PAFLSEST)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLSEST=='T',1,;
      0)
  endfunc

  add object oPAFLASSE_5_10 as StdCheck with uid="EVMASSYSNT",rtseq=131,rtrep=.f.,left=221, top=61, caption="Assenze",;
    ToolTipText = "Se attivo, viene selezionato il flag Assenze in agenda",;
    HelpContextID = 232161083,;
    cFormVar="w_PAFLASSE", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLASSE_5_10.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oPAFLASSE_5_10.GetRadio()
    this.Parent.oContained.w_PAFLASSE = this.RadioValue()
    return .t.
  endfunc

  func oPAFLASSE_5_10.SetRadio()
    this.Parent.oContained.w_PAFLASSE=trim(this.Parent.oContained.w_PAFLASSE)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLASSE=='A',1,;
      0)
  endfunc

  add object oPAFLINPR_5_11 as StdCheck with uid="YYAEBSSJBO",rtseq=132,rtrep=.f.,left=221, top=82, caption="Da inserimento prestazioni",;
    ToolTipText = "Se attivo, viene selezionato il flag Da inserimento prestazioni in agenda",;
    HelpContextID = 111771832,;
    cFormVar="w_PAFLINPR", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLINPR_5_11.RadioValue()
    return(iif(this.value =1,'Z',;
    ' '))
  endfunc
  func oPAFLINPR_5_11.GetRadio()
    this.Parent.oContained.w_PAFLINPR = this.RadioValue()
    return .t.
  endfunc

  func oPAFLINPR_5_11.SetRadio()
    this.Parent.oContained.w_PAFLINPR=trim(this.Parent.oContained.w_PAFLINPR)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLINPR=='Z',1,;
      0)
  endfunc

  add object oPAFLNOTE_5_12 as StdCheck with uid="JEXGLEEQTK",rtseq=133,rtrep=.f.,left=453, top=40, caption="Note",;
    ToolTipText = "Se attivo, viene selezionato il flag Note in agenda",;
    HelpContextID = 178683707,;
    cFormVar="w_PAFLNOTE", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLNOTE_5_12.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oPAFLNOTE_5_12.GetRadio()
    this.Parent.oContained.w_PAFLNOTE = this.RadioValue()
    return .t.
  endfunc

  func oPAFLNOTE_5_12.SetRadio()
    this.Parent.oContained.w_PAFLNOTE=trim(this.Parent.oContained.w_PAFLNOTE)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLNOTE=='M',1,;
      0)
  endfunc

  add object oPAFLDAFA_5_13 as StdCheck with uid="NVJEAHLKTW",rtseq=134,rtrep=.f.,left=453, top=61, caption="Cose da fare",;
    ToolTipText = "Se attivo, viene selezionato il flag Cose da fare in agenda",;
    HelpContextID = 201752375,;
    cFormVar="w_PAFLDAFA", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLDAFA_5_13.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oPAFLDAFA_5_13.GetRadio()
    this.Parent.oContained.w_PAFLDAFA = this.RadioValue()
    return .t.
  endfunc

  func oPAFLDAFA_5_13.SetRadio()
    this.Parent.oContained.w_PAFLDAFA=trim(this.Parent.oContained.w_PAFLDAFA)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLDAFA=='D',1,;
      0)
  endfunc

  add object oPAFLAPP1_5_16 as StdCheck with uid="ICVCDGFYWB",rtseq=135,rtrep=.f.,left=37, top=147, caption="Appuntamenti",;
    ToolTipText = "Se attivo, viene selezionato il flag Appuntamenti in elenco attivit�",;
    HelpContextID = 86606041,;
    cFormVar="w_PAFLAPP1", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLAPP1_5_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAFLAPP1_5_16.GetRadio()
    this.Parent.oContained.w_PAFLAPP1 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLAPP1_5_16.SetRadio()
    this.Parent.oContained.w_PAFLAPP1=trim(this.Parent.oContained.w_PAFLAPP1)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLAPP1=='S',1,;
      0)
  endfunc

  add object oPAFLATG1_5_17 as StdCheck with uid="ECZMWREHCR",rtseq=136,rtrep=.f.,left=37, top=168, caption="Attivit� generiche",;
    ToolTipText = "Se attivo, viene selezionato il flag Attivit� generiche in elenco attivit�",;
    HelpContextID = 248938279,;
    cFormVar="w_PAFLATG1", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLATG1_5_17.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oPAFLATG1_5_17.GetRadio()
    this.Parent.oContained.w_PAFLATG1 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLATG1_5_17.SetRadio()
    this.Parent.oContained.w_PAFLATG1=trim(this.Parent.oContained.w_PAFLATG1)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLATG1=='G',1,;
      0)
  endfunc

  add object oPAFLUDI1_5_18 as StdCheck with uid="QWFLXROBRI",rtseq=137,rtrep=.f.,left=37, top=189, caption="Udienze",;
    ToolTipText = "Se attivo, viene selezionato il flag Udienze in elenco attivit�",;
    HelpContextID = 1474343,;
    cFormVar="w_PAFLUDI1", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLUDI1_5_18.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oPAFLUDI1_5_18.GetRadio()
    this.Parent.oContained.w_PAFLUDI1 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLUDI1_5_18.SetRadio()
    this.Parent.oContained.w_PAFLUDI1=trim(this.Parent.oContained.w_PAFLUDI1)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLUDI1=='U',1,;
      0)
  endfunc

  func oPAFLUDI1_5_18.mHide()
    with this.Parent.oContained
      return (!ISALT())
    endwith
  endfunc

  add object oPAFLSES1_5_19 as StdCheck with uid="NLCYCYJFXJ",rtseq=138,rtrep=.f.,left=221, top=147, caption="Sessioni telefoniche",;
    ToolTipText = "Se attivo, viene selezionato il flag Sessioni telefoniche in elenco attivit�",;
    HelpContextID = 16154407,;
    cFormVar="w_PAFLSES1", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLSES1_5_19.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oPAFLSES1_5_19.GetRadio()
    this.Parent.oContained.w_PAFLSES1 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLSES1_5_19.SetRadio()
    this.Parent.oContained.w_PAFLSES1=trim(this.Parent.oContained.w_PAFLSES1)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLSES1=='T',1,;
      0)
  endfunc

  add object oPAFLASS1_5_20 as StdCheck with uid="FDUACPMJZS",rtseq=139,rtrep=.f.,left=221, top=168, caption="Assenze",;
    ToolTipText = "Se attivo, viene selezionato il flag Assenze in elenco attivit�",;
    HelpContextID = 232161063,;
    cFormVar="w_PAFLASS1", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLASS1_5_20.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oPAFLASS1_5_20.GetRadio()
    this.Parent.oContained.w_PAFLASS1 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLASS1_5_20.SetRadio()
    this.Parent.oContained.w_PAFLASS1=trim(this.Parent.oContained.w_PAFLASS1)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLASS1=='A',1,;
      0)
  endfunc

  add object oPAFLINP1_5_21 as StdCheck with uid="BZPNONHXBC",rtseq=140,rtrep=.f.,left=221, top=189, caption="Da inserimento prestazioni",;
    ToolTipText = "Se attivo, viene selezionato il flag Da inserimento prestazioni in elenco attivit�",;
    HelpContextID = 111771865,;
    cFormVar="w_PAFLINP1", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLINP1_5_21.RadioValue()
    return(iif(this.value =1,'Z',;
    ' '))
  endfunc
  func oPAFLINP1_5_21.GetRadio()
    this.Parent.oContained.w_PAFLINP1 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLINP1_5_21.SetRadio()
    this.Parent.oContained.w_PAFLINP1=trim(this.Parent.oContained.w_PAFLINP1)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLINP1=='Z',1,;
      0)
  endfunc

  add object oPAFLNOT1_5_22 as StdCheck with uid="NUOFTZMHJW",rtseq=141,rtrep=.f.,left=453, top=147, caption="Note",;
    ToolTipText = "Se attivo, viene selezionato il flag Note in elenco attivit�",;
    HelpContextID = 178683687,;
    cFormVar="w_PAFLNOT1", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLNOT1_5_22.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oPAFLNOT1_5_22.GetRadio()
    this.Parent.oContained.w_PAFLNOT1 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLNOT1_5_22.SetRadio()
    this.Parent.oContained.w_PAFLNOT1=trim(this.Parent.oContained.w_PAFLNOT1)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLNOT1=='M',1,;
      0)
  endfunc

  add object oPAFLDAF1_5_23 as StdCheck with uid="JXHIAUUBRE",rtseq=142,rtrep=.f.,left=453, top=168, caption="Cose da fare",;
    ToolTipText = "Se attivo, viene selezionato il flag Cose da fare in elenco attivit�",;
    HelpContextID = 201752359,;
    cFormVar="w_PAFLDAF1", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLDAF1_5_23.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oPAFLDAF1_5_23.GetRadio()
    this.Parent.oContained.w_PAFLDAF1 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLDAF1_5_23.SetRadio()
    this.Parent.oContained.w_PAFLDAF1=trim(this.Parent.oContained.w_PAFLDAF1)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLDAF1=='D',1,;
      0)
  endfunc

  add object oPAFLAPP2_5_26 as StdCheck with uid="RRTSFMTOQM",rtseq=143,rtrep=.f.,left=37, top=256, caption="Appuntamenti",;
    ToolTipText = "Se attivo, viene selezionato il flag Appuntamenti in confronto agende giornaliere",;
    HelpContextID = 86606040,;
    cFormVar="w_PAFLAPP2", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLAPP2_5_26.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAFLAPP2_5_26.GetRadio()
    this.Parent.oContained.w_PAFLAPP2 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLAPP2_5_26.SetRadio()
    this.Parent.oContained.w_PAFLAPP2=trim(this.Parent.oContained.w_PAFLAPP2)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLAPP2=='S',1,;
      0)
  endfunc

  add object oPAFLATG2_5_27 as StdCheck with uid="SQWAKHDGKV",rtseq=144,rtrep=.f.,left=37, top=277, caption="Attivit� generiche",;
    ToolTipText = "Se attivo, viene selezionato il flag Attivit� generiche in confronto agende giornaliere",;
    HelpContextID = 248938280,;
    cFormVar="w_PAFLATG2", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLATG2_5_27.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oPAFLATG2_5_27.GetRadio()
    this.Parent.oContained.w_PAFLATG2 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLATG2_5_27.SetRadio()
    this.Parent.oContained.w_PAFLATG2=trim(this.Parent.oContained.w_PAFLATG2)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLATG2=='G',1,;
      0)
  endfunc

  add object oPAFLUDI2_5_28 as StdCheck with uid="WEMGZIZEGM",rtseq=145,rtrep=.f.,left=37, top=298, caption="Udienze",;
    ToolTipText = "Se attivo, viene selezionato il flag Udienze in confronto agende giornaliere",;
    HelpContextID = 1474344,;
    cFormVar="w_PAFLUDI2", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLUDI2_5_28.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oPAFLUDI2_5_28.GetRadio()
    this.Parent.oContained.w_PAFLUDI2 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLUDI2_5_28.SetRadio()
    this.Parent.oContained.w_PAFLUDI2=trim(this.Parent.oContained.w_PAFLUDI2)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLUDI2=='U',1,;
      0)
  endfunc

  func oPAFLUDI2_5_28.mHide()
    with this.Parent.oContained
      return (!ISALT())
    endwith
  endfunc

  add object oPAFLSES2_5_29 as StdCheck with uid="DRYKQLFPPK",rtseq=146,rtrep=.f.,left=221, top=256, caption="Sessioni telefoniche",;
    ToolTipText = "Se attivo, viene selezionato il flag Sessioni telefoniche in confronto agende giornaliere",;
    HelpContextID = 16154408,;
    cFormVar="w_PAFLSES2", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLSES2_5_29.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oPAFLSES2_5_29.GetRadio()
    this.Parent.oContained.w_PAFLSES2 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLSES2_5_29.SetRadio()
    this.Parent.oContained.w_PAFLSES2=trim(this.Parent.oContained.w_PAFLSES2)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLSES2=='T',1,;
      0)
  endfunc

  add object oPAFLASS2_5_30 as StdCheck with uid="RNMFBQUDRP",rtseq=147,rtrep=.f.,left=221, top=277, caption="Assenze",;
    ToolTipText = "Se attivo, viene selezionato il flag Assenze in confronto agende giornaliere",;
    HelpContextID = 232161064,;
    cFormVar="w_PAFLASS2", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLASS2_5_30.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oPAFLASS2_5_30.GetRadio()
    this.Parent.oContained.w_PAFLASS2 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLASS2_5_30.SetRadio()
    this.Parent.oContained.w_PAFLASS2=trim(this.Parent.oContained.w_PAFLASS2)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLASS2=='A',1,;
      0)
  endfunc

  add object oPAFLINP2_5_31 as StdCheck with uid="BFFUNRNQTY",rtseq=148,rtrep=.f.,left=221, top=298, caption="Da inserimento prestazioni",;
    ToolTipText = "Se attivo, viene selezionato il flag Da inserimento prestazioni in agenda",;
    HelpContextID = 111771864,;
    cFormVar="w_PAFLINP2", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLINP2_5_31.RadioValue()
    return(iif(this.value =1,'Z',;
    ' '))
  endfunc
  func oPAFLINP2_5_31.GetRadio()
    this.Parent.oContained.w_PAFLINP2 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLINP2_5_31.SetRadio()
    this.Parent.oContained.w_PAFLINP2=trim(this.Parent.oContained.w_PAFLINP2)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLINP2=='Z',1,;
      0)
  endfunc

  add object oPAFLNOT2_5_32 as StdCheck with uid="NBXNTGWGPP",rtseq=149,rtrep=.f.,left=453, top=256, caption="Note",;
    ToolTipText = "Se attivo, viene selezionato il flag Note in confronto agende giornaliere",;
    HelpContextID = 178683688,;
    cFormVar="w_PAFLNOT2", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLNOT2_5_32.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oPAFLNOT2_5_32.GetRadio()
    this.Parent.oContained.w_PAFLNOT2 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLNOT2_5_32.SetRadio()
    this.Parent.oContained.w_PAFLNOT2=trim(this.Parent.oContained.w_PAFLNOT2)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLNOT2=='M',1,;
      0)
  endfunc

  add object oPAFLDAF2_5_33 as StdCheck with uid="CLCOJPRYLN",rtseq=150,rtrep=.f.,left=453, top=277, caption="Cose da fare",;
    ToolTipText = "Se attivo, viene selezionato il flag Cose da fare in confronto agende giornaliere",;
    HelpContextID = 201752360,;
    cFormVar="w_PAFLDAF2", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLDAF2_5_33.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oPAFLDAF2_5_33.GetRadio()
    this.Parent.oContained.w_PAFLDAF2 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLDAF2_5_33.SetRadio()
    this.Parent.oContained.w_PAFLDAF2=trim(this.Parent.oContained.w_PAFLDAF2)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLDAF2=='D',1,;
      0)
  endfunc

  add object oPAFLAPP3_5_36 as StdCheck with uid="EUOVIZUMHJ",rtseq=151,rtrep=.f.,left=37, top=357, caption="Appuntamenti",;
    ToolTipText = "Se attivo, viene selezionato il flag Appuntamenti in confronto agende giornaliere",;
    HelpContextID = 86606039,;
    cFormVar="w_PAFLAPP3", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLAPP3_5_36.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPAFLAPP3_5_36.GetRadio()
    this.Parent.oContained.w_PAFLAPP3 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLAPP3_5_36.SetRadio()
    this.Parent.oContained.w_PAFLAPP3=trim(this.Parent.oContained.w_PAFLAPP3)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLAPP3=='S',1,;
      0)
  endfunc

  add object oPAFLATG3_5_37 as StdCheck with uid="ZWPUJRHNNR",rtseq=152,rtrep=.f.,left=37, top=378, caption="Attivit� generiche",;
    ToolTipText = "Se attivo, viene selezionato il flag Attivit� generiche in confronto agende giornaliere",;
    HelpContextID = 248938281,;
    cFormVar="w_PAFLATG3", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLATG3_5_37.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oPAFLATG3_5_37.GetRadio()
    this.Parent.oContained.w_PAFLATG3 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLATG3_5_37.SetRadio()
    this.Parent.oContained.w_PAFLATG3=trim(this.Parent.oContained.w_PAFLATG3)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLATG3=='G',1,;
      0)
  endfunc

  add object oPAFLUDI3_5_38 as StdCheck with uid="STWFKGUWDV",rtseq=153,rtrep=.f.,left=37, top=399, caption="Udienze",;
    ToolTipText = "Se attivo, viene selezionato il flag Udienze in confronto agende giornaliere",;
    HelpContextID = 1474345,;
    cFormVar="w_PAFLUDI3", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLUDI3_5_38.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oPAFLUDI3_5_38.GetRadio()
    this.Parent.oContained.w_PAFLUDI3 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLUDI3_5_38.SetRadio()
    this.Parent.oContained.w_PAFLUDI3=trim(this.Parent.oContained.w_PAFLUDI3)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLUDI3=='U',1,;
      0)
  endfunc

  func oPAFLUDI3_5_38.mHide()
    with this.Parent.oContained
      return (!ISALT())
    endwith
  endfunc

  add object oPAFLSES3_5_39 as StdCheck with uid="OADLXRCFGV",rtseq=154,rtrep=.f.,left=221, top=357, caption="Sessioni telefoniche",;
    ToolTipText = "Se attivo, viene selezionato il flag Sessioni telefoniche in confronto agende giornaliere",;
    HelpContextID = 16154409,;
    cFormVar="w_PAFLSES3", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLSES3_5_39.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oPAFLSES3_5_39.GetRadio()
    this.Parent.oContained.w_PAFLSES3 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLSES3_5_39.SetRadio()
    this.Parent.oContained.w_PAFLSES3=trim(this.Parent.oContained.w_PAFLSES3)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLSES3=='T',1,;
      0)
  endfunc

  add object oPAFLASS3_5_40 as StdCheck with uid="KITLYSYVXL",rtseq=155,rtrep=.f.,left=221, top=378, caption="Assenze",;
    ToolTipText = "Se attivo, viene selezionato il flag Assenze in confronto agende giornaliere",;
    HelpContextID = 232161065,;
    cFormVar="w_PAFLASS3", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLASS3_5_40.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oPAFLASS3_5_40.GetRadio()
    this.Parent.oContained.w_PAFLASS3 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLASS3_5_40.SetRadio()
    this.Parent.oContained.w_PAFLASS3=trim(this.Parent.oContained.w_PAFLASS3)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLASS3=='A',1,;
      0)
  endfunc

  add object oPAFLINP3_5_41 as StdCheck with uid="QHIESHGDEE",rtseq=156,rtrep=.f.,left=221, top=399, caption="Da inserimento prestazioni",;
    ToolTipText = "Se attivo, viene selezionato il flag Da inserimento prestazioni in agenda",;
    HelpContextID = 111771863,;
    cFormVar="w_PAFLINP3", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLINP3_5_41.RadioValue()
    return(iif(this.value =1,'Z',;
    ' '))
  endfunc
  func oPAFLINP3_5_41.GetRadio()
    this.Parent.oContained.w_PAFLINP3 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLINP3_5_41.SetRadio()
    this.Parent.oContained.w_PAFLINP3=trim(this.Parent.oContained.w_PAFLINP3)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLINP3=='Z',1,;
      0)
  endfunc

  add object oPAFLNOT3_5_42 as StdCheck with uid="YBUSJZKDGX",rtseq=157,rtrep=.f.,left=453, top=357, caption="Note",;
    ToolTipText = "Se attivo, viene selezionato il flag Note in confronto agende giornaliere",;
    HelpContextID = 178683689,;
    cFormVar="w_PAFLNOT3", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLNOT3_5_42.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oPAFLNOT3_5_42.GetRadio()
    this.Parent.oContained.w_PAFLNOT3 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLNOT3_5_42.SetRadio()
    this.Parent.oContained.w_PAFLNOT3=trim(this.Parent.oContained.w_PAFLNOT3)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLNOT3=='M',1,;
      0)
  endfunc

  add object oPAFLDAF3_5_43 as StdCheck with uid="BJPRSYKZNI",rtseq=158,rtrep=.f.,left=453, top=378, caption="Cose da fare",;
    ToolTipText = "Se attivo, viene selezionato il flag Cose da fare in confronto agende giornaliere",;
    HelpContextID = 201752361,;
    cFormVar="w_PAFLDAF3", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oPAFLDAF3_5_43.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oPAFLDAF3_5_43.GetRadio()
    this.Parent.oContained.w_PAFLDAF3 = this.RadioValue()
    return .t.
  endfunc

  func oPAFLDAF3_5_43.SetRadio()
    this.Parent.oContained.w_PAFLDAF3=trim(this.Parent.oContained.w_PAFLDAF3)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLDAF3=='D',1,;
      0)
  endfunc

  add object oStr_5_5 as StdString with uid="ZTQSQBPRVB",Visible=.t., Left=9, Top=16,;
    Alignment=0, Width=66, Height=18,;
    Caption="Agenda"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_15 as StdString with uid="EYFAJIUIOE",Visible=.t., Left=9, Top=123,;
    Alignment=0, Width=108, Height=18,;
    Caption="Elenco attivit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_25 as StdString with uid="WACBSXMUYA",Visible=.t., Left=9, Top=232,;
    Alignment=0, Width=165, Height=18,;
    Caption="Confronto agende giornaliere"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_35 as StdString with uid="JPCKLGZKVC",Visible=.t., Left=9, Top=333,;
    Alignment=0, Width=165, Height=18,;
    Caption="Stampa attivit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_5_4 as StdBox with uid="KBUUJGTGFK",left=9, top=31, width=687,height=2

  add object oBox_5_14 as StdBox with uid="OKFOEEAXZW",left=9, top=138, width=687,height=2

  add object oBox_5_24 as StdBox with uid="QVLMEZPCUK",left=9, top=247, width=687,height=2

  add object oBox_5_34 as StdBox with uid="OBFMKTJIZK",left=9, top=348, width=687,height=2
enddefine
define class tgsag_apaPag6 as StdContainer
  Width  = 706
  height = 537
  stdWidth  = 706
  stdheight = 537
  resizeXpos=540
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPACAUATT_6_1 as StdField with uid="UFBAZWUIJU",rtseq=101,rtrep=.f.,;
    cFormVar = "w_PACAUATT", cQueryName = "PACAUATT",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Scegliere un tipo di attivit� che non preveda l'obbligatoriet� della commessa",;
    ToolTipText = "Tipo di attivit� che sar� utilizzato dalla procedura quando dall'agenda si carica una nuova attivit� inserendo solo la descrizione",;
    HelpContextID = 218845002,;
   bGlobalFont=.t.,;
    Height=21, Width=182, Left=180, Top=12, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PACAUATT"

  func oPACAUATT_6_1.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oPACAUATT_6_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACAUATT_6_1.ecpDrop(oSource)
    this.Parent.oContained.link_6_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACAUATT_6_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPACAUATT_6_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPACAUATT_6_1.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PACAUATT
     i_obj.ecpSave()
  endproc

  add object oCauDescri_6_3 as StdField with uid="APGORNVOAD",rtseq=102,rtrep=.f.,;
    cFormVar = "w_CauDescri", cQueryName = "CauDescri",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 1152296,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=366, Top=12, InputMask=replicate('X',254)

  add object oPACAUATT_6_4 as StdField with uid="MKNMVMDIDD",rtseq=103,rtrep=.f.,;
    cFormVar = "w_PACAUATT", cQueryName = "PACAUATT",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Scegliere un tipo di attivit� che non preveda l'obbligatoriet� della pratica",;
    ToolTipText = "Tipo di attivit� che sar� utilizzato dalla procedura quando dall'agenda si carica una nuova attivit� inserendo solo la descrizione",;
    HelpContextID = 218845002,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=180, Top=12, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PACAUATT"

  func oPACAUATT_6_4.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  func oPACAUATT_6_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACAUATT_6_4.ecpDrop(oSource)
    this.Parent.oContained.link_6_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACAUATT_6_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPACAUATT_6_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPACAUATT_6_4.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PACAUATT
     i_obj.ecpSave()
  endproc

  add object oPACAUPRE_6_5 as StdField with uid="QFPGQXABKU",rtseq=104,rtrep=.f.,;
    cFormVar = "w_PACAUPRE", cQueryName = "PACAUPRE",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Tipo di attivit� che sar� utilizzato dalla procedura per la creazione automatica di un'attivit� (dalla procedura di inserimento provvisorio prestazioni)",;
    HelpContextID = 66367685,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=180, Top=36, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CARAGGST", oKey_1_2="this.w_CATEG", oKey_2_1="CACODICE", oKey_2_2="this.w_PACAUPRE"

  func oPACAUPRE_6_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACAUPRE_6_5.ecpDrop(oSource)
    this.Parent.oContained.link_6_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACAUPRE_6_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAUMATTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CARAGGST="+cp_ToStrODBC(this.Parent.oContained.w_CATEG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CARAGGST="+cp_ToStr(this.Parent.oContained.w_CATEG)
    endif
    do cp_zoom with 'CAUMATTI','*','CARAGGST,CACODICE',cp_AbsName(this.parent,'oPACAUPRE_6_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPACAUPRE_6_5.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CARAGGST=w_CATEG
     i_obj.w_CACODICE=this.parent.oContained.w_PACAUPRE
     i_obj.ecpSave()
  endproc

  add object oPACOSGIU_6_7 as StdField with uid="VMQFUJWSRH",rtseq=106,rtrep=.f.,;
    cFormVar = "w_PACOSGIU", cQueryName = "PACOSGIU",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Tipo di attivit� da inserire sulla copertina della pratica (la prima attivit� di questo tipo non evasa)",;
    HelpContextID = 49893195,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=180, Top=60, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PACOSGIU"

  func oPACOSGIU_6_7.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oPACOSGIU_6_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACOSGIU_6_7.ecpDrop(oSource)
    this.Parent.oContained.link_6_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACOSGIU_6_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPACOSGIU_6_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'GSAG_MAT.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPACOSGIU_6_7.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PACOSGIU
     i_obj.ecpSave()
  endproc

  add object oPASTRING_6_8 as StdField with uid="KZCZVMOPQA",rtseq=107,rtrep=.f.,;
    cFormVar = "w_PASTRING", cQueryName = "PASTRING",;
    bObbl = .t. , nPag = 6, value=space(14), bMultilanguage =  .f.,;
    ToolTipText = "Etichetta da utilizzare sulla copertina della pratica relativamente all'attivit� in evidenza sopra indicata",;
    HelpContextID = 185643203,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=583, Top=60, InputMask=replicate('X',14)

  func oPASTRING_6_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_PACOSGIU))
    endwith
   endif
  endfunc

  func oPASTRING_6_8.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oPATIPCTR_6_9 as StdField with uid="XSLOVJZQAG",rtseq=108,rtrep=.f.,;
    cFormVar = "w_PATIPCTR", cQueryName = "PATIPCTR",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo di attivit� che sar� utilizzato dalla procedura per la creazione di un'attivit� di controllo dell'incasso di una proforma in fase di emissione di quest'ultima.",;
    HelpContextID = 247750472,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=180, Top=84, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PATIPCTR"

  func oPATIPCTR_6_9.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oPATIPCTR_6_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPCTR_6_9.ecpDrop(oSource)
    this.Parent.oContained.link_6_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPCTR_6_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPATIPCTR_6_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPATIPCTR_6_9.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PATIPCTR
     i_obj.ecpSave()
  endproc

  add object oPANGGINC_6_10 as StdField with uid="UJWCSQMXSU",rtseq=109,rtrep=.f.,;
    cFormVar = "w_PANGGINC", cQueryName = "PANGGINC",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni per determinare la data dell'attivit� di controllo incassi (sommando il numero di giorni alla data di sistema)",;
    HelpContextID = 198049991,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=653, Top=84, cSayPict="'9999'", cGetPict="'9999'"

  func oPANGGINC_6_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_PATIPCTR))
    endwith
   endif
  endfunc

  func oPANGGINC_6_10.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oPANGGINC_6_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PANGGINC > 0)
    endwith
    return bRes
  endfunc


  add object oLinkPC_6_11 as stdDynamicChildContainer with uid="RYZWOZCNIE",left=-4, top=177, width=699, height=81, bOnScreen=.t.;


  add object oPATIPTEL_6_12 as StdField with uid="WQSUBSGMFY",rtseq=110,rtrep=.f.,;
    cFormVar = "w_PATIPTEL", cQueryName = "PATIPTEL",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Tipo attivit� per nuova sessione telefonica",;
    HelpContextID = 264527682,;
   bGlobalFont=.t.,;
    Height=21, Width=182, Left=180, Top=279, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PATIPTEL"

  func oPATIPTEL_6_12.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  func oPATIPTEL_6_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPTEL_6_12.ecpDrop(oSource)
    this.Parent.oContained.link_6_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPTEL_6_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPATIPTEL_6_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPATIPTEL_6_12.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PATIPTEL
     i_obj.ecpSave()
  endproc

  add object oPATIPASS_6_13 as StdField with uid="AHYKWBLXXJ",rtseq=111,rtrep=.f.,;
    cFormVar = "w_PATIPASS", cQueryName = "PATIPASS",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Tipo attivit� per fax",;
    HelpContextID = 54239415,;
   bGlobalFont=.t.,;
    Height=21, Width=182, Left=180, Top=302, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PATIPASS"

  func oPATIPASS_6_13.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  func oPATIPASS_6_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPASS_6_13.ecpDrop(oSource)
    this.Parent.oContained.link_6_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPASS_6_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPATIPASS_6_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPATIPASS_6_13.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PATIPASS
     i_obj.ecpSave()
  endproc

  add object oPATIPAPI_6_14 as StdField with uid="QVFARDRGNA",rtseq=112,rtrep=.f.,;
    cFormVar = "w_PATIPAPI", cQueryName = "PATIPAPI",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Tipo attivit� per email",;
    HelpContextID = 54239425,;
   bGlobalFont=.t.,;
    Height=21, Width=182, Left=180, Top=325, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PATIPAPI"

  func oPATIPAPI_6_14.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  func oPATIPAPI_6_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPAPI_6_14.ecpDrop(oSource)
    this.Parent.oContained.link_6_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPAPI_6_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPATIPAPI_6_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPATIPAPI_6_14.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PATIPAPI
     i_obj.ecpSave()
  endproc

  add object oPATIPCOS_6_15 as StdField with uid="DAUZBRUQSY",rtseq=113,rtrep=.f.,;
    cFormVar = "w_PATIPCOS", cQueryName = "PATIPCOS",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Tipo per nuova attivit� documento",;
    HelpContextID = 20684983,;
   bGlobalFont=.t.,;
    Height=21, Width=182, Left=180, Top=348, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PATIPCOS"

  func oPATIPCOS_6_15.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  func oPATIPCOS_6_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPCOS_6_15.ecpDrop(oSource)
    this.Parent.oContained.link_6_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPCOS_6_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPATIPCOS_6_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPATIPCOS_6_15.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PATIPCOS
     i_obj.ecpSave()
  endproc

  add object oPATIPGEN_6_16 as StdField with uid="UAQOSSQOFU",rtseq=114,rtrep=.f.,;
    cFormVar = "w_PATIPGEN", cQueryName = "PATIPGEN",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Tipo per nuova attivit� altro",;
    HelpContextID = 46423876,;
   bGlobalFont=.t.,;
    Height=21, Width=182, Left=180, Top=371, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PATIPGEN"

  func oPATIPGEN_6_16.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  func oPATIPGEN_6_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPGEN_6_16.ecpDrop(oSource)
    this.Parent.oContained.link_6_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPGEN_6_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPATIPGEN_6_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPATIPGEN_6_16.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PATIPGEN
     i_obj.ecpSave()
  endproc

  add object oPATIPAPF_6_17 as StdField with uid="PCCZVNLAUD",rtseq=115,rtrep=.f.,;
    cFormVar = "w_PATIPAPF", cQueryName = "PATIPAPF",;
    bObbl = .f. , nPag = 6, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Tipo attivit� per presa in carico/smistamento",;
    HelpContextID = 54239428,;
   bGlobalFont=.t.,;
    Height=21, Width=182, Left=180, Top=394, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PATIPAPF"

  func oPATIPAPF_6_17.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  func oPATIPAPF_6_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oPATIPAPF_6_17.ecpDrop(oSource)
    this.Parent.oContained.link_6_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPATIPAPF_6_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPATIPAPF_6_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPATIPAPF_6_17.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PATIPAPF
     i_obj.ecpSave()
  endproc

  add object oDES_CAU_6_19 as StdField with uid="WNCBZMSAHY",rtseq=116,rtrep=.f.,;
    cFormVar = "w_DES_CAU", cQueryName = "DES_CAU",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 202002998,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=366, Top=36, InputMask=replicate('X',254)

  add object oDES_GEN_6_21 as StdField with uid="XJAPFEZTIW",rtseq=117,rtrep=.f.,;
    cFormVar = "w_DES_GEN", cQueryName = "DES_GEN",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 263564746,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=366, Top=371, InputMask=replicate('X',254)

  func oDES_GEN_6_21.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oDES_TEL_6_23 as StdField with uid="KNNARVJSZK",rtseq=118,rtrep=.f.,;
    cFormVar = "w_DES_TEL", cQueryName = "DES_TEL",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 249933258,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=366, Top=279, InputMask=replicate('X',254)

  func oDES_TEL_6_23.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oDES_CHI_6_25 as StdField with uid="BQILWDAYFZ",rtseq=119,rtrep=.f.,;
    cFormVar = "w_DES_CHI", cQueryName = "DES_CHI",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 51008054,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=366, Top=302, InputMask=replicate('X',254)

  func oDES_CHI_6_25.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oDES_API_6_27 as StdField with uid="OILSQPLHCD",rtseq=120,rtrep=.f.,;
    cFormVar = "w_DES_API", cQueryName = "DES_API",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 183128630,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=366, Top=325, InputMask=replicate('X',254)

  func oDES_API_6_27.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oDES_APF_6_29 as StdField with uid="VVTSBWLHSK",rtseq=121,rtrep=.f.,;
    cFormVar = "w_DES_APF", cQueryName = "DES_APF",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 183128630,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=366, Top=394, InputMask=replicate('X',254)

  func oDES_APF_6_29.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc


  add object oLinkPC_6_33 as stdDynamicChildContainer with uid="SFSRGUNHTD",left=-4, top=439, width=699, height=81, bOnScreen=.t.;


  func oLinkPC_6_33.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_AGFA<>'S' OR ISALT())
     endwith
    endif
  endfunc

  add object oDES_DOC_6_35 as StdField with uid="VKHLRMGJQF",rtseq=122,rtrep=.f.,;
    cFormVar = "w_DES_DOC", cQueryName = "DES_DOC",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 169497142,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=366, Top=348, InputMask=replicate('X',254)

  func oDES_DOC_6_35.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oPADESINC_6_43 as StdField with uid="IEFOCXGWSC",rtseq=125,rtrep=.f.,;
    cFormVar = "w_PADESINC", cQueryName = "PADESINC",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto dell'attivit� di controllo incassi",;
    HelpContextID = 185639111,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=366, Top=84, InputMask=replicate('X',254)

  func oPADESINC_6_43.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oPAOGGOUT_6_44 as StdField with uid="YOZWIYSILU",rtseq=126,rtrep=.f.,;
    cFormVar = "w_PAOGGOUT", cQueryName = "PAOGGOUT",;
    bObbl = .f. , nPag = 6, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto appuntamento Outlook",;
    HelpContextID = 171052874,;
   bGlobalFont=.t.,;
    Height=21, Width=514, Left=180, Top=108, InputMask=replicate('X',254), cMenuFile="GSAG_APA_Outl"

  func oPAOGGOUT_6_44.mHide()
    with this.Parent.oContained
      return (!isalt())
    endwith
  endfunc


  add object oObj_6_45 as cp_setobjprop with uid="KYLVNFTCVA",left=741, top=258, width=170,height=21,;
    caption='Tasto dx su PAOGGOUT',;
   bGlobalFont=.t.,;
    cProp="cMenuFile",cObj="w_PAOGGOUT",;
    cEvent = "Init",;
    nPag=6;
    , HelpContextID = 73586789

  add object oStr_6_2 as StdString with uid="MAOAIWTKNV",Visible=.t., Left=39, Top=13,;
    Alignment=1, Width=138, Height=18,;
    Caption="Agenda:"  ;
  , bGlobalFont=.t.

  add object oStr_6_18 as StdString with uid="YIPHIKXMUK",Visible=.t., Left=9, Top=37,;
    Alignment=1, Width=168, Height=18,;
    Caption="Inserimento prestazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_6_20 as StdString with uid="XXCLZSRIAZ",Visible=.t., Left=65, Top=373,;
    Alignment=1, Width=110, Height=18,;
    Caption="Altro:"  ;
  , bGlobalFont=.t.

  func oStr_6_20.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oStr_6_22 as StdString with uid="ZTNREJUWVT",Visible=.t., Left=25, Top=281,;
    Alignment=1, Width=150, Height=18,;
    Caption="Telefonate:"  ;
  , bGlobalFont=.t.

  func oStr_6_22.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oStr_6_24 as StdString with uid="JEVCQICXRQ",Visible=.t., Left=62, Top=303,;
    Alignment=1, Width=113, Height=18,;
    Caption="Fax:"  ;
  , bGlobalFont=.t.

  func oStr_6_24.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oStr_6_26 as StdString with uid="YRIUFRRBGL",Visible=.t., Left=92, Top=327,;
    Alignment=1, Width=83, Height=18,;
    Caption="E-mail:"  ;
  , bGlobalFont=.t.

  func oStr_6_26.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oStr_6_28 as StdString with uid="YLCPEELPES",Visible=.t., Left=14, Top=396,;
    Alignment=1, Width=161, Height=18,;
    Caption="Presa in carico/smistamento:"  ;
  , bGlobalFont=.t.

  func oStr_6_28.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oStr_6_30 as StdString with uid="WEGKIGNMUD",Visible=.t., Left=8, Top=262,;
    Alignment=0, Width=172, Height=18,;
    Caption="Tipi attivit� per gestione eventi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_6_30.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oStr_6_31 as StdString with uid="NXFCWHEQMS",Visible=.t., Left=8, Top=159,;
    Alignment=0, Width=203, Height=18,;
    Caption="Definizione suffissi per voci di men�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_32 as StdString with uid="XDGYIUNQAS",Visible=.t., Left=8, Top=420,;
    Alignment=0, Width=227, Height=18,;
    Caption="Causali documento per creazione eventi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_6_32.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S' OR ISALT())
    endwith
  endfunc

  add object oStr_6_34 as StdString with uid="LHIIFUVGUS",Visible=.t., Left=65, Top=350,;
    Alignment=1, Width=110, Height=18,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  func oStr_6_34.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oStr_6_36 as StdString with uid="TBOYSBMXOJ",Visible=.t., Left=8, Top=61,;
    Alignment=1, Width=169, Height=18,;
    Caption="In evidenza sulla pratica:"  ;
  , bGlobalFont=.t.

  func oStr_6_36.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_6_38 as StdString with uid="ILNMPEYYMB",Visible=.t., Left=411, Top=61,;
    Alignment=1, Width=169, Height=18,;
    Caption="Etichetta attivit� in evidenza:"  ;
  , bGlobalFont=.t.

  func oStr_6_38.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_6_40 as StdString with uid="XFXVOKOWAF",Visible=.t., Left=34, Top=85,;
    Alignment=1, Width=143, Height=18,;
    Caption="Attivit� di controllo incassi:"  ;
  , bGlobalFont=.t.

  func oStr_6_40.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_6_41 as StdString with uid="HKHDEWHPQU",Visible=.t., Left=616, Top=85,;
    Alignment=1, Width=36, Height=18,;
    Caption="Giorni:"  ;
  , bGlobalFont=.t.

  func oStr_6_41.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_6_46 as StdString with uid="LVPJZSANUR",Visible=.t., Left=3, Top=109,;
    Alignment=1, Width=174, Height=18,;
    Caption="Oggetto appuntamento Outlook:"  ;
  , bGlobalFont=.t.

  func oStr_6_46.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsag_mat",lower(this.oContained.gsag_mat.class))=0
        this.oContained.gsag_mat.createrealchild()
      endif
      if type('this.oContained')='O' and at("gsag_mcd",lower(this.oContained.GSAG_MCD.class))=0
        this.oContained.GSAG_MCD.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsag_apaPag7 as StdContainer
  Width  = 706
  height = 537
  stdWidth  = 706
  stdheight = 537
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPACODMOD_7_4 as StdField with uid="TDOCYKDZXO",rtseq=74,rtrep=.f.,;
    cFormVar = "w_PACODMOD", cQueryName = "PACODMOD",;
    bObbl = .f. , nPag = 7, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Modello attributo da utilizzare come default nel caricamento degli impianti dall�apposita gestione",;
    HelpContextID = 133607622,;
   bGlobalFont=.t.,;
    Height=21, Width=182, Left=179, Top=36, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="MODMATTR", cZoomOnZoom="GSAR_MMA", oKey_1_1="MACODICE", oKey_1_2="this.w_PACODMOD"

  func oPACODMOD_7_4.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oPACODMOD_7_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODMOD_7_4.ecpDrop(oSource)
    this.Parent.oContained.link_7_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODMOD_7_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODMATTR','*','MACODICE',cp_AbsName(this.parent,'oPACODMOD_7_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MMA',"Modelli",'',this.parent.oContained
  endproc
  proc oPACODMOD_7_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_PACODMOD
     i_obj.ecpSave()
  endproc

  add object oDESCRI_7_6 as StdField with uid="BTZVSGYTEQ",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 186756554,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=365, Top=36, InputMask=replicate('X',40)

  func oDESCRI_7_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oPACODNUM_7_7 as StdCheck with uid="BLULMNKDSB",rtseq=76,rtrep=.f.,left=179, top=58, caption="Codifica impianti numerica",;
    ToolTipText = "Se attivo, determina la creazione di impianti con codice numerico",;
    HelpContextID = 151605059,;
    cFormVar="w_PACODNUM", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oPACODNUM_7_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPACODNUM_7_7.GetRadio()
    this.Parent.oContained.w_PACODNUM = this.RadioValue()
    return .t.
  endfunc

  func oPACODNUM_7_7.SetRadio()
    this.Parent.oContained.w_PACODNUM=trim(this.Parent.oContained.w_PACODNUM)
    this.value = ;
      iif(this.Parent.oContained.w_PACODNUM=='S',1,;
      0)
  endfunc

  func oPACODNUM_7_7.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oPCNOFLDC_7_8 as StdCombo with uid="LEWJNSZKVC",rtseq=77,rtrep=.f.,left=179,top=106,width=155,height=21;
    , ToolTipText = "Tipo di selezione sul filtro di date per documento";
    , HelpContextID = 120193337;
    , cFormVar="w_PCNOFLDC",RowSource=""+"Data odierna,"+"Inizio settimana,"+"Inizio mese,"+"Inizio anno", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oPCNOFLDC_7_8.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'I',;
    iif(this.value =3,'F',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oPCNOFLDC_7_8.GetRadio()
    this.Parent.oContained.w_PCNOFLDC = this.RadioValue()
    return .t.
  endfunc

  func oPCNOFLDC_7_8.SetRadio()
    this.Parent.oContained.w_PCNOFLDC=trim(this.Parent.oContained.w_PCNOFLDC)
    this.value = ;
      iif(this.Parent.oContained.w_PCNOFLDC=='N',1,;
      iif(this.Parent.oContained.w_PCNOFLDC=='I',2,;
      iif(this.Parent.oContained.w_PCNOFLDC=='F',3,;
      iif(this.Parent.oContained.w_PCNOFLDC=='E',4,;
      0))))
  endfunc

  add object oPACODDOC_7_9 as StdField with uid="SYYFRTUQCU",rtseq=78,rtrep=.f.,;
    cFormVar = "w_PACODDOC", cQueryName = "PACODDOC",;
    bObbl = .f. , nPag = 7, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Periodicit� documenti",;
    HelpContextID = 16167111,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=453, Top=108, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_PACODDOC"

  func oPACODDOC_7_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODDOC_7_9.ecpDrop(oSource)
    this.Parent.oContained.link_7_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODDOC_7_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oPACODDOC_7_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo periodicit�",'',this.parent.oContained
  endproc
  proc oPACODDOC_7_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_PACODDOC
     i_obj.ecpSave()
  endproc


  add object oPCNOFLAT_7_10 as StdCombo with uid="XSJKLNTBEX",rtseq=79,rtrep=.f.,left=179,top=131,width=155,height=21;
    , ToolTipText = "Tipo di selezione sul filtro di date per attivit�";
    , HelpContextID = 148242102;
    , cFormVar="w_PCNOFLAT",RowSource=""+"Data odierna,"+"Inizio settimana,"+"Inizio mese,"+"Inizio anno", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oPCNOFLAT_7_10.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'I',;
    iif(this.value =3,'F',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oPCNOFLAT_7_10.GetRadio()
    this.Parent.oContained.w_PCNOFLAT = this.RadioValue()
    return .t.
  endfunc

  func oPCNOFLAT_7_10.SetRadio()
    this.Parent.oContained.w_PCNOFLAT=trim(this.Parent.oContained.w_PCNOFLAT)
    this.value = ;
      iif(this.Parent.oContained.w_PCNOFLAT=='N',1,;
      iif(this.Parent.oContained.w_PCNOFLAT=='I',2,;
      iif(this.Parent.oContained.w_PCNOFLAT=='F',3,;
      iif(this.Parent.oContained.w_PCNOFLAT=='E',4,;
      0))))
  endfunc

  add object oPACODPER_7_11 as StdField with uid="BKDANIRWSB",rtseq=80,rtrep=.f.,;
    cFormVar = "w_PACODPER", cQueryName = "PACODPER",;
    bObbl = .f. , nPag = 7, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Periodicit� attivit�",;
    HelpContextID = 185159496,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=453, Top=133, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_PACODPER"

  func oPACODPER_7_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODPER_7_11.ecpDrop(oSource)
    this.Parent.oContained.link_7_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODPER_7_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oPACODPER_7_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo periodicit�",'',this.parent.oContained
  endproc
  proc oPACODPER_7_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_PACODPER
     i_obj.ecpSave()
  endproc

  add object oPADESSUP_7_12 as StdField with uid="OPDFDOMFNK",rtseq=81,rtrep=.f.,;
    cFormVar = "w_PADESSUP", cQueryName = "PADESSUP",;
    bObbl = .f. , nPag = 7, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione parametrica",;
    HelpContextID = 250568518,;
   bGlobalFont=.t.,;
    Height=21, Width=509, Left=179, Top=156, InputMask=replicate('X',254)

  add object oPCTIPDOC_7_13 as StdField with uid="FBDBDBSDSK",rtseq=82,rtrep=.f.,;
    cFormVar = "w_PCTIPDOC", cQueryName = "PCTIPDOC",;
    bObbl = .f. , nPag = 7, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente o di tipo ordine e/o del ciclo acquisti",;
    ToolTipText = "Causale documento",;
    HelpContextID = 3907271,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=323, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PCTIPDOC"

  func oPCTIPDOC_7_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCTIPDOC_7_13.ecpDrop(oSource)
    this.Parent.oContained.link_7_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCTIPDOC_7_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPCTIPDOC_7_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'GSAG_CAU.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPCTIPDOC_7_13.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PCTIPDOC
     i_obj.ecpSave()
  endproc

  add object oPCTIPATT_7_15 as StdField with uid="EBIXXZOTWW",rtseq=83,rtrep=.f.,;
    cFormVar = "w_PCTIPATT", cQueryName = "PCTIPATT",;
    bObbl = .f. , nPag = 7, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 214196554,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=179, Top=208, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PCTIPATT"

  func oPCTIPATT_7_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCTIPATT_7_15.ecpDrop(oSource)
    this.Parent.oContained.link_7_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCTIPATT_7_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPCTIPATT_7_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'GSAG1AAT.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPCTIPATT_7_15.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PCTIPATT
     i_obj.ecpSave()
  endproc

  add object oPCGRUPAR_7_17 as StdField with uid="MVMQNGRJAH",rtseq=84,rtrep=.f.,;
    cFormVar = "w_PCGRUPAR", cQueryName = "PCGRUPAR",;
    bObbl = .f. , nPag = 7, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo partecipante attivit�",;
    HelpContextID = 65236664,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=233, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_PCGRUPAR"

  func oPCGRUPAR_7_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCGRUPAR_7_17.ecpDrop(oSource)
    this.Parent.oContained.link_7_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCGRUPAR_7_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oPCGRUPAR_7_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oPCGRUPAR_7_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_PCGRUPAR
     i_obj.ecpSave()
  endproc

  add object oDESDOC_7_19 as StdField with uid="BKBUEFEDHC",rtseq=85,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 22064586,;
   bGlobalFont=.t.,;
    Height=21, Width=445, Left=243, Top=323, InputMask=replicate('X',35)

  add object oDESATT_7_20 as StdField with uid="NCTZGTLNPP",rtseq=86,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 241098,;
   bGlobalFont=.t.,;
    Height=21, Width=337, Left=351, Top=208, InputMask=replicate('X',254)

  add object oDESGRU_7_36 as StdField with uid="BPCHZTDEQP",rtseq=93,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 253603274,;
   bGlobalFont=.t.,;
    Height=21, Width=441, Left=247, Top=233, InputMask=replicate('X',60)

  add object oPCESCRIN_7_37 as StdCheck with uid="SFRDVXIRRC",rtseq=94,rtrep=.f.,left=181, top=438, caption="Esclude rinnovo",;
    ToolTipText = "Eslude rinnovi",;
    HelpContextID = 217936196,;
    cFormVar="w_PCESCRIN", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oPCESCRIN_7_37.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPCESCRIN_7_37.GetRadio()
    this.Parent.oContained.w_PCESCRIN = this.RadioValue()
    return .t.
  endfunc

  func oPCESCRIN_7_37.SetRadio()
    this.Parent.oContained.w_PCESCRIN=trim(this.Parent.oContained.w_PCESCRIN)
    this.value = ;
      iif(this.Parent.oContained.w_PCESCRIN=='S',1,;
      0)
  endfunc

  add object oPCPERCON_7_38 as StdField with uid="PCPYKTKNAF",rtseq=95,rtrep=.f.,;
    cFormVar = "w_PCPERCON", cQueryName = "PCPERCON",;
    bObbl = .f. , nPag = 7, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Periodicit� rinnovo contratti",;
    HelpContextID = 18865852,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=180, Top=462, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_PCPERCON"

  func oPCPERCON_7_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCESCRIN = "N" )
    endwith
   endif
  endfunc

  func oPCPERCON_7_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCPERCON_7_38.ecpDrop(oSource)
    this.Parent.oContained.link_7_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCPERCON_7_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oPCPERCON_7_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oPCPERCON_7_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_PCPERCON
     i_obj.ecpSave()
  endproc

  add object oMDDESCRI_7_39 as StdField with uid="SYOXWKZIIL",rtseq=96,rtrep=.f.,;
    cFormVar = "w_MDDESCRI", cQueryName = "MDDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 17866225,;
   bGlobalFont=.t.,;
    Height=21, Width=439, Left=249, Top=462, InputMask=replicate('X',50)

  add object oPCRINCON_7_41 as StdCheck with uid="ASMOIOOXOX",rtseq=97,rtrep=.f.,left=181, top=485, caption="Rinnovo tacito",;
    ToolTipText = "Rinnovo tacito",;
    HelpContextID = 22789820,;
    cFormVar="w_PCRINCON", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oPCRINCON_7_41.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPCRINCON_7_41.GetRadio()
    this.Parent.oContained.w_PCRINCON = this.RadioValue()
    return .t.
  endfunc

  func oPCRINCON_7_41.SetRadio()
    this.Parent.oContained.w_PCRINCON=trim(this.Parent.oContained.w_PCRINCON)
    this.value = ;
      iif(this.Parent.oContained.w_PCRINCON=='S',1,;
      0)
  endfunc

  func oPCRINCON_7_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCESCRIN <> "S" )
    endwith
   endif
  endfunc

  add object oPCNUMGIO_7_42 as StdField with uid="GGPAOVIDJS",rtseq=98,rtrep=.f.,;
    cFormVar = "w_PCNUMGIO", cQueryName = "PCNUMGIO",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni entro i quali comunicare la disdetta",;
    HelpContextID = 44040517,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=458, Top=485

  func oPCNUMGIO_7_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCESCRIN <> "S"  AND .w_PCRINCON = "S")
    endwith
   endif
  endfunc

  add object oDESMDO_7_46 as StdField with uid="FMLVOCFZTY",rtseq=99,rtrep=.f.,;
    cFormVar = "w_DESMDO", cQueryName = "DESMDO",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 100117962,;
   bGlobalFont=.t.,;
    Height=21, Width=178, Left=510, Top=108, InputMask=replicate('X',50)

  add object oDESMAT_7_47 as StdField with uid="ZRLNKWPMXK",rtseq=100,rtrep=.f.,;
    cFormVar = "w_DESMAT", cQueryName = "DESMAT",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 19377610,;
   bGlobalFont=.t.,;
    Height=21, Width=178, Left=510, Top=133, InputMask=replicate('X',50)

  add object oPADATFIS_7_48 as StdCheck with uid="JQCQFMAEKT",rtseq=167,rtrep=.f.,left=571, top=485, caption="Data iniziale fissa",;
    HelpContextID = 33251145,;
    cFormVar="w_PADATFIS", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oPADATFIS_7_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPADATFIS_7_48.GetRadio()
    this.Parent.oContained.w_PADATFIS = this.RadioValue()
    return .t.
  endfunc

  func oPADATFIS_7_48.SetRadio()
    this.Parent.oContained.w_PADATFIS=trim(this.Parent.oContained.w_PADATFIS)
    this.value = ;
      iif(this.Parent.oContained.w_PADATFIS=='S',1,;
      0)
  endfunc

  func oPADATFIS_7_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PCESCRIN <> "S" )
    endwith
   endif
  endfunc

  add object oPADESATT_7_49 as StdMemo with uid="JJXNLMQLIF",rtseq=168,rtrep=.f.,;
    cFormVar = "w_PADESATT", cQueryName = "PADESATT",;
    bObbl = .f. , nPag = 7, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione servizio attivit� parametrica",;
    HelpContextID = 217014090,;
   bGlobalFont=.t.,;
    Height=61, Width=509, Left=179, Top=258

  add object oPADESDOC_7_51 as StdMemo with uid="IESLXLLIJP",rtseq=169,rtrep=.f.,;
    cFormVar = "w_PADESDOC", cQueryName = "PADESDOC",;
    bObbl = .f. , nPag = 7, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione servizio documento parametrica ",;
    HelpContextID = 1089735,;
   bGlobalFont=.t.,;
    Height=61, Width=509, Left=179, Top=348

  add object oStr_7_1 as StdString with uid="HABMHIVXQZ",Visible=.t., Left=11, Top=15,;
    Alignment=0, Width=172, Height=18,;
    Caption="Impianti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_5 as StdString with uid="WGJHTFHYEG",Visible=.t., Left=23, Top=36,;
    Alignment=1, Width=152, Height=18,;
    Caption="Modello attributo:"  ;
  , bGlobalFont=.t.

  func oStr_7_5.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_7_14 as StdString with uid="SXTKKVSNIN",Visible=.t., Left=62, Top=323,;
    Alignment=1, Width=115, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_7_16 as StdString with uid="EFCZRSRDGA",Visible=.t., Left=97, Top=208,;
    Alignment=1, Width=80, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_7_18 as StdString with uid="LQHAEXHRQS",Visible=.t., Left=123, Top=233,;
    Alignment=1, Width=54, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_7_23 as StdString with uid="QIVKWEQOIL",Visible=.t., Left=8, Top=133,;
    Alignment=1, Width=167, Height=18,;
    Caption="Data generazione attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_7_24 as StdString with uid="XDSJWKCVRH",Visible=.t., Left=335, Top=133,;
    Alignment=1, Width=116, Height=18,;
    Caption="Periodicit� att.:"  ;
  , bGlobalFont=.t.

  add object oStr_7_25 as StdString with uid="TYPTJLSQGY",Visible=.t., Left=0, Top=108,;
    Alignment=1, Width=175, Height=18,;
    Caption="Data generazione documenti:"  ;
  , bGlobalFont=.t.

  add object oStr_7_26 as StdString with uid="SMXUAMCWQM",Visible=.t., Left=335, Top=108,;
    Alignment=1, Width=116, Height=18,;
    Caption="Periodicit� doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_7_27 as StdString with uid="WWAQXVXXJF",Visible=.t., Left=11, Top=83,;
    Alignment=0, Width=181, Height=18,;
    Caption="Parametri generazione contratti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_29 as StdString with uid="OOEIBWUNRF",Visible=.t., Left=11, Top=185,;
    Alignment=0, Width=204, Height=18,;
    Caption="Parametri modelli elementi contratti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_34 as StdString with uid="HMUSRRGUGU",Visible=.t., Left=93, Top=156,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_7_40 as StdString with uid="JOMWBBMPTU",Visible=.t., Left=46, Top=464,;
    Alignment=1, Width=130, Height=18,;
    Caption="Periodicit� rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_7_43 as StdString with uid="TSFVLERUOH",Visible=.t., Left=313, Top=488,;
    Alignment=1, Width=144, Height=18,;
    Caption="Giorni preavviso disdetta:"  ;
  , bGlobalFont=.t.

  add object oStr_7_45 as StdString with uid="OUUUSRZKUG",Visible=.t., Left=11, Top=417,;
    Alignment=0, Width=204, Height=18,;
    Caption="Parametri rinnovi contratti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_50 as StdString with uid="OKOZCMFPKF",Visible=.t., Left=50, Top=258,;
    Alignment=1, Width=127, Height=18,;
    Caption="Descr. servizio attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_7_52 as StdString with uid="XEAKIECWXJ",Visible=.t., Left=22, Top=351,;
    Alignment=1, Width=155, Height=18,;
    Caption="Descr. servizio documento:"  ;
  , bGlobalFont=.t.

  add object oBox_7_2 as StdBox with uid="OSODJPSVHJ",left=11, top=31, width=687,height=2

  add object oBox_7_3 as StdBox with uid="PPYDFEEKSR",left=11, top=434, width=687,height=2

  add object oBox_7_28 as StdBox with uid="VZKVRVMZFU",left=11, top=99, width=687,height=2

  add object oBox_7_30 as StdBox with uid="NEZQIRNXPO",left=11, top=202, width=687,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_apa','PAR_AGEN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PACODAZI=PAR_AGEN.PACODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
