* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_kie                                                        *
*              Importazione eventi                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-01                                                      *
* Last revis.: 2012-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsfa_kie",oParentObject))

* --- Class definition
define class tgsfa_kie as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 786
  Height = 530+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-04-05"
  HelpContextID=267027049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  TIPEVENT_IDX = 0
  cPrg = "gsfa_kie"
  cComment = "Importazione eventi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_IETIPRIS = space(1)
  o_IETIPRIS = space(1)
  w_ZoomName = space(20)
  w_IECODRIS = space(5)
  w_DPDESCRI = space(60)
  w_DPCOGNOM = space(50)
  w_DPNOME = space(50)
  w_DPTIPRIS = space(1)
  w_IEDESRIS = space(40)
  w_IETIPEVE = space(10)
  o_IETIPEVE = space(10)
  w_IEDESCRI = space(50)
  w_MSG = space(0)
  w_IENUMSEL = 0
  o_IENUMSEL = 0
  w_EVENSELE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsfa_kiePag1","gsfa_kie",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Impostazioni import")
      .Pages(2).addobject("oPag","tgsfa_kiePag2","gsfa_kie",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Messaggi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIETIPRIS_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_EVENSELE = this.oPgFrm.Pages(1).oPag.EVENSELE
    DoDefault()
    proc Destroy()
      this.w_EVENSELE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='TIPEVENT'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSFA_BIE(this,"IMPORTEVEN")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_IETIPRIS=space(1)
      .w_ZoomName=space(20)
      .w_IECODRIS=space(5)
      .w_DPDESCRI=space(60)
      .w_DPCOGNOM=space(50)
      .w_DPNOME=space(50)
      .w_DPTIPRIS=space(1)
      .w_IEDESRIS=space(40)
      .w_IETIPEVE=space(10)
      .w_IEDESCRI=space(50)
      .w_MSG=space(0)
      .w_IENUMSEL=0
        .w_IETIPRIS = 'P'
        .w_ZoomName = iif(.w_IETIPRIS='G','GSARGADP','')
        .w_IECODRIS = SPACE(5)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_IECODRIS))
          .link_1_6('Full')
        endif
          .DoRTCalc(4,7,.f.)
        .w_IEDESRIS = IIF(.w_DPTIPRIS='P', ALLTRIM(.w_DPCOGNOM)+" "+ALLTRIM(.w_DPNOME), .w_DPDESCRI)
        .w_IETIPEVE = IIF(.w_IENUMSEL<2, .w_IETIPEVE, "")
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_IETIPEVE))
          .link_1_13('Full')
        endif
      .oPgFrm.Page1.oPag.EVENSELE.Calculate()
    endwith
    this.DoRTCalc(10,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_IETIPRIS<>.w_IETIPRIS
            .w_ZoomName = iif(.w_IETIPRIS='G','GSARGADP','')
        endif
        if .o_IETIPRIS<>.w_IETIPRIS
            .w_IECODRIS = SPACE(5)
          .link_1_6('Full')
        endif
        .DoRTCalc(4,7,.t.)
            .w_IEDESRIS = IIF(.w_DPTIPRIS='P', ALLTRIM(.w_DPCOGNOM)+" "+ALLTRIM(.w_DPNOME), .w_DPDESCRI)
        if .o_IENUMSEL<>.w_IENUMSEL
            .w_IETIPEVE = IIF(.w_IENUMSEL<2, .w_IETIPEVE, "")
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.EVENSELE.Calculate()
        if .o_IETIPEVE<>.w_IETIPEVE
          .Calculate_ZOHIGXFNFM()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.EVENSELE.Calculate()
    endwith
  return

  proc Calculate_ZOHIGXFNFM()
    with this
          * --- Selezione automatica evento da filtrare
          gsfa_bie(this;
              ,"SELECTAUTO";
             )
    endwith
  endproc
  proc Calculate_RMSPWLEBEG()
    with this
          * --- Conteggio eventi selezionati
          gsfa_bie(this;
              ,"SELECTEVEN";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIETIPEVE_1_13.enabled = this.oPgFrm.Page1.oPag.oIETIPEVE_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.EVENSELE.Event(cEvent)
        if lower(cEvent)==lower("w_EVENSELE row checked") or lower(cEvent)==lower("w_EVENSELE row unchecked")
          .Calculate_RMSPWLEBEG()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IECODRIS
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IECODRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_IECODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_IETIPRIS);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_IETIPRIS;
                     ,'DPCODICE',trim(this.w_IECODRIS))
          select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IECODRIS)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_IECODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_IETIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_IECODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_IETIPRIS);

            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_IECODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_IETIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_IECODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_IETIPRIS);

            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_IECODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_IETIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_IECODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_IETIPRIS);

            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_IECODRIS) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oIECODRIS_1_6'),i_cWhere,'GSAR_BDZ',"Persone/Gruppi",''+ Alltrim(this.Parent.oContained.w_ZoomName)+'.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_IETIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Persona/Gruppo inesistente o incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_IETIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IECODRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_IECODRIS);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_IETIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_IETIPRIS;
                       ,'DPCODICE',this.w_IECODRIS)
            select DPTIPRIS,DPCODICE,DPDESCRI,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IECODRIS = NVL(_Link_.DPCODICE,space(5))
      this.w_DPDESCRI = NVL(_Link_.DPDESCRI,space(60))
      this.w_DPCOGNOM = NVL(_Link_.DPCOGNOM,space(50))
      this.w_DPNOME = NVL(_Link_.DPNOME,space(50))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_IECODRIS = space(5)
      endif
      this.w_DPDESCRI = space(60)
      this.w_DPCOGNOM = space(50)
      this.w_DPNOME = space(50)
      this.w_DPTIPRIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_IECODRIS) OR .w_DPTIPRIS=.w_IETIPRIS
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Persona/Gruppo inesistente o incongruente")
        endif
        this.w_IECODRIS = space(5)
        this.w_DPDESCRI = space(60)
        this.w_DPCOGNOM = space(50)
        this.w_DPNOME = space(50)
        this.w_DPTIPRIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IECODRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IETIPEVE
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IETIPEVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_IETIPEVE)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_IETIPEVE))
          select TETIPEVE,TEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IETIPEVE)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IETIPEVE) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oIETIPEVE_1_13'),i_cWhere,'',"Eventi",'GSFA_KIE.TIPEVENT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IETIPEVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_IETIPEVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_IETIPEVE)
            select TETIPEVE,TEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IETIPEVE = NVL(_Link_.TETIPEVE,space(10))
      this.w_IEDESCRI = NVL(_Link_.TEDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IETIPEVE = space(10)
      endif
      this.w_IEDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IETIPEVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIETIPRIS_1_4.RadioValue()==this.w_IETIPRIS)
      this.oPgFrm.Page1.oPag.oIETIPRIS_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIECODRIS_1_6.value==this.w_IECODRIS)
      this.oPgFrm.Page1.oPag.oIECODRIS_1_6.value=this.w_IECODRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oIEDESRIS_1_11.value==this.w_IEDESRIS)
      this.oPgFrm.Page1.oPag.oIEDESRIS_1_11.value=this.w_IEDESRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oIETIPEVE_1_13.value==this.w_IETIPEVE)
      this.oPgFrm.Page1.oPag.oIETIPEVE_1_13.value=this.w_IETIPEVE
    endif
    if not(this.oPgFrm.Page1.oPag.oIEDESCRI_1_14.value==this.w_IEDESCRI)
      this.oPgFrm.Page1.oPag.oIEDESCRI_1_14.value=this.w_IEDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oMSG_2_1.value==this.w_MSG)
      this.oPgFrm.Page2.oPag.oMSG_2_1.value=this.w_MSG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_IECODRIS) OR .w_DPTIPRIS=.w_IETIPRIS)  and not(empty(.w_IECODRIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIECODRIS_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Persona/Gruppo inesistente o incongruente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IETIPRIS = this.w_IETIPRIS
    this.o_IETIPEVE = this.w_IETIPEVE
    this.o_IENUMSEL = this.w_IENUMSEL
    return

enddefine

* --- Define pages as container
define class tgsfa_kiePag1 as StdContainer
  Width  = 782
  height = 530
  stdWidth  = 782
  stdheight = 530
  resizeXpos=601
  resizeYpos=366
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="ERHOXVEEGI",left=670, top=482, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 266998298;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      with this.Parent.oContained
        GSFA_BIE(this.Parent.oContained,"IMPORTEVEN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_2 as StdButton with uid="TKCMXZXILU",left=723, top=482, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 259709626;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oIETIPRIS_1_4 as StdCombo with uid="CNGGPXTLVW",rtseq=1,rtrep=.f.,left=191,top=10,width=138,height=21;
    , ToolTipText = "Permette di selezionare il tipo di risorsa su cui effettuare il filtro";
    , HelpContextID = 144439335;
    , cFormVar="w_IETIPRIS",RowSource=""+"Persona,"+"Gruppo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oIETIPRIS_1_4.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    space(1))))
  endfunc
  func oIETIPRIS_1_4.GetRadio()
    this.Parent.oContained.w_IETIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oIETIPRIS_1_4.SetRadio()
    this.Parent.oContained.w_IETIPRIS=trim(this.Parent.oContained.w_IETIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_IETIPRIS=='P',1,;
      iif(this.Parent.oContained.w_IETIPRIS=='G',2,;
      0))
  endfunc

  func oIETIPRIS_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_IECODRIS)
        bRes2=.link_1_6('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oIECODRIS_1_6 as StdField with uid="FTVRCUGSXW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_IECODRIS", cQueryName = "IECODRIS",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Persona/Gruppo inesistente o incongruente",;
    ToolTipText = "Persona o gruppo utilizzato come filtro per i percorsi associati ai tipi eventi da analizzare",;
    HelpContextID = 156698663,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=335, Top=8, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_IETIPRIS", oKey_2_1="DPCODICE", oKey_2_2="this.w_IECODRIS"

  func oIECODRIS_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oIECODRIS_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIECODRIS_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_IETIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_IETIPRIS)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oIECODRIS_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone/Gruppi",''+ Alltrim(this.Parent.oContained.w_ZoomName)+'.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oIECODRIS_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_IETIPRIS
     i_obj.w_DPCODICE=this.parent.oContained.w_IECODRIS
     i_obj.ecpSave()
  endproc

  add object oIEDESRIS_1_11 as StdField with uid="QOCLREFHSI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IEDESRIS", cQueryName = "IEDESRIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 141621287,;
   bGlobalFont=.t.,;
    Height=21, Width=364, Left=407, Top=8, InputMask=replicate('X',40)

  add object oIETIPEVE_1_13 as StdField with uid="CESLBXIODI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_IETIPEVE", cQueryName = "IETIPEVE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice evento da utilizzare come filtro per importazione eventi",;
    HelpContextID = 174327755,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=191, Top=36, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", oKey_1_1="TETIPEVE", oKey_1_2="this.w_IETIPEVE"

  func oIETIPEVE_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IENUMSEL<2)
    endwith
   endif
  endfunc

  func oIETIPEVE_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oIETIPEVE_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIETIPEVE_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oIETIPEVE_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Eventi",'GSFA_KIE.TIPEVENT_VZM',this.parent.oContained
  endproc

  add object oIEDESCRI_1_14 as StdField with uid="TNCNQRJAIF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_IEDESCRI", cQueryName = "IEDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 143591375,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=292, Top=36, InputMask=replicate('X',50)


  add object EVENSELE as cp_szoombox with uid="WPLWNEOPUC",left=23, top=87, width=747,height=391,;
    caption='EVENSELE',;
   bGlobalFont=.t.,;
    cTable="TIPEVENT",cZoomFile="GSFA_KIE",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 90691445


  add object oBtn_1_17 as StdButton with uid="GQMUPTTTCY",left=23, top=482, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti i tipi eventi";
    , HelpContextID = 92354326;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSFA_BIE(this.Parent.oContained,"SELECT_S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="ZCYHLDNLVF",left=74, top=482, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti i tipi eventi";
    , HelpContextID = 92354326;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSFA_BIE(this.Parent.oContained,"SELECT_D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="GUQBLQMZBT",left=124, top=482, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertite la selezione dei tipi eventi";
    , HelpContextID = 92354326;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSFA_BIE(this.Parent.oContained,"SELECT_I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="CIAOXKBUQP",left=723, top=38, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 178330646;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="FHEXWORZQX",Visible=.t., Left=15, Top=9,;
    Alignment=1, Width=171, Height=18,;
    Caption="Filtra eventi per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="LMJCNXSAUP",Visible=.t., Left=15, Top=38,;
    Alignment=1, Width=171, Height=18,;
    Caption="Tipo evento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="BFVWUIFKQA",Visible=.t., Left=24, Top=66,;
    Alignment=0, Width=607, Height=18,;
    Caption="Tipi eventi da utilizzare per importazione eventi"  ;
  , bGlobalFont=.t.
enddefine
define class tgsfa_kiePag2 as StdContainer
  Width  = 782
  height = 530
  stdWidth  = 782
  stdheight = 530
  resizeXpos=606
  resizeYpos=367
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMSG_2_1 as StdMemo with uid="HOXTXOVMVD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 266713658,;
   bGlobalFont=.t.,;
    Height=515, Width=768, Left=7, Top=7, tabstop = .f., readonly = .t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_kie','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
