* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_apr                                                        *
*              Manutenzione prestazioni                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-02                                                      *
* Last revis.: 2014-10-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_apr"))

* --- Class definition
define class tgsag_apr as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 713
  Height = 532+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-13"
  HelpContextID=160049001
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=176

  * --- Constant Properties
  PRE_STAZ_IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  KEY_ARTI_IDX = 0
  OFF_NOMI_IDX = 0
  ART_ICOL_IDX = 0
  PNT_MAST_IDX = 0
  DOC_MAST_IDX = 0
  UNIMIS_IDX = 0
  VOC_COST_IDX = 0
  CENCOST_IDX = 0
  VALUTE_IDX = 0
  LISTINI_IDX = 0
  PRA_CONT_IDX = 0
  PAR_AGEN_IDX = 0
  CAUMATTI_IDX = 0
  PAR_ALTE_IDX = 0
  PRA_ENTI_IDX = 0
  cFile = "PRE_STAZ"
  cKeySelect = "PRSERIAL"
  cKeyWhere  = "PRSERIAL=this.w_PRSERIAL"
  cKeyWhereODBC = '"PRSERIAL="+cp_ToStrODBC(this.w_PRSERIAL)';

  cKeyWhereODBCqualified = '"PRE_STAZ.PRSERIAL="+cp_ToStrODBC(this.w_PRSERIAL)';

  cPrg = "gsag_apr"
  cComment = "Manutenzione prestazioni"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LetturaParAgen = space(5)
  o_LetturaParAgen = space(5)
  w_OB_TEST = ctod('  /  /  ')
  w_OBTEST = space(10)
  w_LeggeParAlte = space(5)
  o_LeggeParAlte = space(5)
  w_FLGZER = space(1)
  w_DECTOT = 0
  w_PRNUMPRA = space(15)
  o_PRNUMPRA = space(15)
  w_PACAUPRE = space(20)
  w_PRCODRES = space(5)
  o_PRCODRES = space(5)
  w_DTIPRIG = space(1)
  w_PR__DATA = ctod('  /  /  ')
  o_PR__DATA = ctod('  /  /  ')
  w_FLSERG = space(1)
  w_PRCODATT = space(20)
  o_PRCODATT = space(20)
  w_CODART = space(20)
  o_CODART = space(20)
  w_PRDESPRE = space(40)
  w_PRUNIMIS = space(3)
  o_PRUNIMIS = space(3)
  w_PRQTAMOV = 0
  o_PRQTAMOV = 0
  w_PRPREZZO = 0
  o_PRPREZZO = 0
  w_PRTIPRIG = space(1)
  w_PRTIPRI2 = space(1)
  w_PROREEFF = 0
  o_PROREEFF = 0
  w_PRMINEFF = 0
  o_PRMINEFF = 0
  w_PRCOSINT = 0
  w_PRDESAGG = space(0)
  w_PRCODNOM = space(15)
  w_PRNUMPRE = 0
  w_PRCODVAL = space(3)
  o_PRCODVAL = space(3)
  w_PRCODLIS = space(5)
  o_PRCODLIS = space(5)
  w_UMDESCRI = space(35)
  w_PRPREMIN = 0
  w_PRPREMAX = 0
  w_PRGAZUFF = space(6)
  w_NUMPRO = 0
  w_ALFPRO = space(10)
  w_DATPRO = ctod('  /  /  ')
  w_NUMFAT = 0
  w_ALFFAT = space(10)
  w_DATFAT = ctod('  /  /  ')
  w_NUMNOT = 0
  w_ALFNOT = space(10)
  w_DATNOT = ctod('  /  /  ')
  w_PRGRURES = space(5)
  w_PRCODOPE = 0
  w_PRDATMOD = ctod('  /  /  ')
  w_LICOSTO = 0
  w_PRRIFFAT = space(10)
  w_PRRIFPRO = space(10)
  w_PRRIFNOT = space(10)
  w_PRRIFBOZ = space(20)
  w_PRROWBOZ = 0
  w_RIFBOZ = space(10)
  w_PRROWNOT = 0
  w_PRVOCCOS = space(15)
  w_PRCENCOS = space(15)
  w_RIFFAT = space(10)
  w_RIFPRO = space(10)
  w_RIFNOT = space(10)
  w_PRFLDEFF = space(1)
  w_PRATTIVI = space(15)
  w_PR_SEGNO = space(1)
  w_PRINICOM = ctod('  /  /  ')
  w_PRFINCOM = ctod('  /  /  ')
  w_PRTCOINI = ctod('  /  /  ')
  w_PRTCOFIN = ctod('  /  /  ')
  w_PRCODSED = space(5)
  w_PRCOSUNI = 0
  o_PRCOSUNI = 0
  w_PRRIFPRE = 0
  w_PRRIGPRE = space(1)
  w_DESCAN = space(100)
  w_PRESTA = space(1)
  w_NODESCR = space(60)
  w_PRSERAGG = space(10)
  w_PRQTAUM1 = 0
  w_PRTIPDOC = space(5)
  w_PRROWFAT = 0
  w_PRROWPRO = 0
  w_PRROWFAA = 0
  w_PRROWPRA = 0
  w_PRRIFCON = space(10)
  w_DENOMRES = space(50)
  w_COGNRESP = space(10)
  w_NOMRESP = space(10)
  w_DATOBSO = ctod('  /  /  ')
  w_VOCCOSDE = space(40)
  w_CENCOSDE = space(40)
  w_PRSERATT = space(10)
  w_PRCODART = space(20)
  w_COST_ORA = 0
  w_CNFLAPON = space(10)
  w_CN__ENTE = space(10)
  w_CNIMPORT = 0
  w_CNUFFICI = space(10)
  w_CNFLVALO = space(1)
  w_CNPARASS = 0
  w_CNFLVALO1 = space(1)
  w_CNCALDIR = space(1)
  w_CNCOECAL = 0
  w_CNFLAMPA = space(1)
  w_CNTARTEM = space(1)
  w_CNTARCON = 0
  w_CONTRACN = space(5)
  w_TIPCONCO = space(1)
  w_LISCOLCO = space(5)
  w_STATUSCO = space(1)
  w_DECUNI = 0
  w_OPERAT = space(1)
  w_FLUSEP = space(1)
  w_UNMIS2 = space(3)
  w_MOLTIP = 0
  w_UNMIS1 = space(3)
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(1)
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_PRVALRIG = 0
  w_NOTIFICA = space(1)
  w_PRROWATT = 0
  w_CODLIS = space(5)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTCV = 0
  w_UTDV = ctot('')
  w_TIPART = space(2)
  w_CAUDOC = space(5)
  w_NUMBOZ = 0
  w_ALFBOZ = space(10)
  w_DATBOZ = ctod('  /  /  ')
  w_PRRIFFAA = space(10)
  w_PRRIFPRA = space(10)
  w_NUMFAA = 0
  w_ALFFAA = space(10)
  w_DATFAA = ctod('  /  /  ')
  w_NUMPRA = 0
  w_ALFPRA = space(10)
  w_DATPRA = ctod('  /  /  ')
  w_RIFFAA = space(10)
  w_RIFPRA = space(10)
  w_FLPRES = space(1)
  w_CHKDATAPRE = space(1)
  w_GG_PRE = 0
  w_GG_SUC = 0
  w_DATAPREC = ctod('  /  /  ')
  w_DATASUCC = ctod('  /  /  ')
  w_GENPRE = space(1)
  w_PRSERIAL = space(10)
  o_PRSERIAL = space(10)
  w_DUR_ORE = 0
  w_CHKTEMP = space(1)
  w_CNCODLIS = space(5)
  w_CNCODVAL = space(3)
  w_EditCodPra = .F.
  w_TIPRIS = space(1)
  w_PRCODESE = space(4)
  w_CollegaPre = space(1)
  w_CNMATOBB = space(1)
  w_CNASSCTP = space(1)
  w_CNCOMPLX = space(1)
  w_CNPROFMT = space(1)
  w_CNESIPOS = space(1)
  w_CNPERPLX = 0
  w_CNPERPOS = 0
  w_DETIPPRA = space(10)
  w_TIPRIG = space(1)
  w_TIPRI2 = space(1)
  w_MESS = space(10)
  w_CNFLVMLQ = space(1)
  w_OLD_PR__DATA = ctod('  /  /  ')
  w_OLD_PRCODRES = space(5)
  w_OLD_PRTIPRIG = space(1)
  w_OLD_PRTIPRI2 = space(1)
  w_OLD_PRNUMPRA = space(15)
  w_OLD_PRESTA = space(1)
  w_VOCRIC = space(15)
  w_TIPOENTE = space(1)
  w_SERPRE = space(41)
  w_RESCHK = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PRSERIAL = this.W_PRSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PRNUMPRA = this.W_PRNUMPRA
  op_PRNUMPRE = this.W_PRNUMPRE

  * --- Children pointers
  gsag_mmp = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_apr
  Close=.f.
  EditCodPra=.f.
  Serpad=' '
  PROCEDURE ecpSave()
      DoDefault()
      if this.w_RESCHK<>-1 AND This.Close
        * Controlli favorevoli, pu� procedere con il salvataggio
        this.cFunction="Query"
        this.ecpQuit()
      endif
  ENDPROC
  PROCEDURE ecpDelete()
      Local Objpr
      DoDefault()
      if Not Empty(This.Serpad)
        Objpr=Gsag_Apr()
        Objpr.Ecpfilter()
        Objpr.w_PRSERIAL=This.Serpad
        Objpr.EcpSave()
        Objpr=.Null.
       This.Serpad=' '
      Endif
      this.cFunction="Query"
      this.ecpQuit()
  ENDPROC
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PRE_STAZ','gsag_apr')
    stdPageFrame::Init()
    *set procedure to gsag_mmp additive
    with this
      .Pages(1).addobject("oPag","tgsag_aprPag1","gsag_apr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Prestazioni")
      .Pages(1).HelpContextID = 13195953
      .Pages(2).addobject("oPag","tgsag_aprPag2","gsag_apr",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Analitica")
      .Pages(2).HelpContextID = 243768967
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPRSERIAL_1_196
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure gsag_mmp
    * --- Area Manuale = Init Page Frame
    * --- gsag_apr
     if VARTYPE(g_SCHEDULER)='C' AND g_SCHEDULER='S'
       this.parent.left=_screen.width+1
     endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[18]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='OFF_NOMI'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='PNT_MAST'
    this.cWorkTables[7]='DOC_MAST'
    this.cWorkTables[8]='UNIMIS'
    this.cWorkTables[9]='VOC_COST'
    this.cWorkTables[10]='CENCOST'
    this.cWorkTables[11]='VALUTE'
    this.cWorkTables[12]='LISTINI'
    this.cWorkTables[13]='PRA_CONT'
    this.cWorkTables[14]='PAR_AGEN'
    this.cWorkTables[15]='CAUMATTI'
    this.cWorkTables[16]='PAR_ALTE'
    this.cWorkTables[17]='PRA_ENTI'
    this.cWorkTables[18]='PRE_STAZ'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(18))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRE_STAZ_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRE_STAZ_IDX,3]
  return

  function CreateChildren()
    this.gsag_mmp = CREATEOBJECT('stdLazyChild',this,'gsag_mmp')
    return

  procedure DestroyChildren()
    if !ISNULL(this.gsag_mmp)
      this.gsag_mmp.DestroyChildrenChain()
      this.gsag_mmp=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gsag_mmp.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gsag_mmp.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gsag_mmp.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.gsag_mmp.SetKey(;
            .w_PRSERIAL,"DPSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .gsag_mmp.ChangeRow(this.cRowID+'      1',1;
             ,.w_PRSERIAL,"DPSERIAL";
             )
    endwith
    return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PRSERIAL = NVL(PRSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_27_joined
    link_1_27_joined=.f.
    local link_1_29_joined
    link_1_29_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PRE_STAZ where PRSERIAL=KeySet.PRSERIAL
    *
    i_nConn = i_TableProp[this.PRE_STAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_STAZ_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRE_STAZ')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRE_STAZ.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PRE_STAZ '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_27_joined=this.AddJoinedLink_1_27(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_29_joined=this.AddJoinedLink_1_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRSERIAL',this.w_PRSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OB_TEST = i_datsys
        .w_OBTEST = i_DATSYS
        .w_FLGZER = space(1)
        .w_DECTOT = 0
        .w_PACAUPRE = space(20)
        .w_DTIPRIG = space(1)
        .w_FLSERG = space(1)
        .w_CODART = space(20)
        .w_UMDESCRI = space(35)
        .w_NUMPRO = 0
        .w_ALFPRO = space(10)
        .w_DATPRO = ctod("  /  /  ")
        .w_NUMFAT = 0
        .w_ALFFAT = space(10)
        .w_DATFAT = ctod("  /  /  ")
        .w_NUMNOT = 0
        .w_ALFNOT = space(10)
        .w_DATNOT = ctod("  /  /  ")
        .w_LICOSTO = 0
        .w_DESCAN = space(100)
        .w_PRESTA = space(1)
        .w_NODESCR = space(60)
        .w_COGNRESP = space(10)
        .w_NOMRESP = space(10)
        .w_DATOBSO = ctod("  /  /  ")
        .w_VOCCOSDE = space(40)
        .w_CENCOSDE = space(40)
        .w_PRCODART = space(20)
        .w_COST_ORA = 0
        .w_CNFLAPON = space(10)
        .w_CN__ENTE = space(10)
        .w_CNIMPORT = 0
        .w_CNUFFICI = space(10)
        .w_CNFLVALO = space(1)
        .w_CNPARASS = 0
        .w_CNFLVALO1 = space(1)
        .w_CNCALDIR = space(1)
        .w_CNCOECAL = 0
        .w_CNFLAMPA = space(1)
        .w_CNTARTEM = space(1)
        .w_CNTARCON = 0
        .w_CONTRACN = space(5)
        .w_TIPCONCO = space(1)
        .w_LISCOLCO = space(5)
        .w_STATUSCO = space(1)
        .w_DECUNI = 0
        .w_OPERAT = space(1)
        .w_FLUSEP = space(1)
        .w_UNMIS2 = space(3)
        .w_MOLTIP = 0
        .w_UNMIS1 = space(3)
        .w_FLFRAZ1 = space(1)
        .w_MODUM2 = space(1)
        .w_UNMIS3 = space(3)
        .w_OPERA3 = space(1)
        .w_MOLTI3 = 0
        .w_NOTIFICA = space(1)
        .w_CODLIS = space(5)
        .w_TIPART = space(2)
        .w_CAUDOC = space(5)
        .w_NUMBOZ = 0
        .w_ALFBOZ = space(10)
        .w_DATBOZ = ctod("  /  /  ")
        .w_NUMFAA = 0
        .w_ALFFAA = space(10)
        .w_DATFAA = ctod("  /  /  ")
        .w_NUMPRA = 0
        .w_ALFPRA = space(10)
        .w_DATPRA = ctod("  /  /  ")
        .w_FLPRES = space(1)
        .w_CHKDATAPRE = space(1)
        .w_GG_PRE = 0
        .w_GG_SUC = 0
        .w_GENPRE = space(1)
        .w_DUR_ORE = 0
        .w_CHKTEMP = space(1)
        .w_CNCODLIS = space(5)
        .w_CNCODVAL = space(3)
        .w_EditCodPra = .F.
        .w_TIPRIS = space(1)
        .w_CNMATOBB = space(1)
        .w_CNASSCTP = space(1)
        .w_CNCOMPLX = space(1)
        .w_CNPROFMT = space(1)
        .w_CNESIPOS = space(1)
        .w_CNPERPLX = 0
        .w_CNPERPOS = 0
        .w_DETIPPRA = space(10)
        .w_TIPRIG = space(1)
        .w_TIPRI2 = space(1)
        .w_MESS = space(10)
        .w_CNFLVMLQ = space(1)
        .w_OLD_PR__DATA = ctod("  /  /  ")
        .w_OLD_PRCODRES = space(5)
        .w_OLD_PRTIPRIG = space(1)
        .w_OLD_PRTIPRI2 = space(1)
        .w_OLD_PRNUMPRA = space(15)
        .w_OLD_PRESTA = space(1)
        .w_VOCRIC = space(15)
        .w_TIPOENTE = space(1)
        .w_SERPRE = space(41)
        .w_RESCHK = 0
        .w_LetturaParAgen = i_CodAzi
          .link_1_1('Load')
        .w_LeggeParAlte = i_codazi
          .link_1_4('Load')
        .w_PRNUMPRA = NVL(PRNUMPRA,space(15))
        .op_PRNUMPRA = .w_PRNUMPRA
          if link_1_7_joined
            this.w_PRNUMPRA = NVL(CNCODCAN107,NVL(this.w_PRNUMPRA,space(15)))
            this.w_DESCAN = NVL(CNDESCAN107,space(100))
            this.w_CNFLAPON = NVL(CNFLAPON107,space(10))
            this.w_CN__ENTE = NVL(CN__ENTE107,space(10))
            this.w_CNIMPORT = NVL(CNIMPORT107,0)
            this.w_CNUFFICI = NVL(CNUFFICI107,space(10))
            this.w_CNFLVALO = NVL(CNFLVALO107,space(1))
            this.w_CNFLVALO1 = NVL(CNFLDIND107,space(1))
            this.w_CNCALDIR = NVL(CNCALDIR107,space(1))
            this.w_CNCOECAL = NVL(CNCOECAL107,0)
            this.w_CNPARASS = NVL(CNPARASS107,0)
            this.w_CNFLAMPA = NVL(CNFLAMPA107,space(1))
            this.w_CNTARTEM = NVL(CNTARTEM107,space(1))
            this.w_CNTARCON = NVL(CNTARCON107,0)
            this.w_CONTRACN = NVL(CNCONTRA107,space(5))
            this.w_CNCODLIS = NVL(CNCODLIS107,space(5))
            this.w_CNCODVAL = NVL(CNCODVAL107,space(3))
            this.w_CNMATOBB = NVL(CNMATOBB107,space(1))
            this.w_CNASSCTP = NVL(CNASSCTP107,space(1))
            this.w_CNCOMPLX = NVL(CNCOMPLX107,space(1))
            this.w_CNPROFMT = NVL(CNPROFMT107,space(1))
            this.w_CNESIPOS = NVL(CNESIPOS107,space(1))
            this.w_CNPERPLX = NVL(CNPERPLX107,0)
            this.w_CNPERPOS = NVL(CNPERPOS107,0)
            this.w_DETIPPRA = NVL(CNTIPPRA107,space(10))
            this.w_CNFLVMLQ = NVL(CNFLVMLQ107,space(1))
          else
          .link_1_7('Load')
          endif
          .link_1_8('Load')
        .w_PRCODRES = NVL(PRCODRES,space(5))
          if link_1_9_joined
            this.w_PRCODRES = NVL(DPCODICE109,NVL(this.w_PRCODRES,space(5)))
            this.w_COGNRESP = NVL(DPCOGNOM109,space(10))
            this.w_NOMRESP = NVL(DPNOME109,space(10))
            this.w_DATOBSO = NVL(cp_ToDate(DPDTOBSO109),ctod("  /  /  "))
            this.w_COST_ORA = NVL(DPCOSORA109,0)
            this.w_PRCOSUNI = NVL(DPCOSORA109,0)
            this.w_CHKDATAPRE = NVL(DPCTRPRE109,space(1))
            this.w_GG_PRE = NVL(DPGG_PRE109,0)
            this.w_GG_SUC = NVL(DPGG_SUC109,0)
            this.w_TIPRIS = NVL(DPTIPRIS109,space(1))
            this.w_DTIPRIG = NVL(DPTIPRIG109,space(1))
            this.w_SERPRE = NVL(DPSERPRE109,space(41))
          else
          .link_1_9('Load')
          endif
        .w_PR__DATA = NVL(cp_ToDate(PR__DATA),ctod("  /  /  "))
        .w_PRCODATT = NVL(PRCODATT,space(20))
          if link_1_13_joined
            this.w_PRCODATT = NVL(CACODICE113,NVL(this.w_PRCODATT,space(20)))
            this.w_PRDESPRE = NVL(CADESART113,space(40))
            this.w_CODART = NVL(CACODART113,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS113,space(3))
            this.w_OPERA3 = NVL(CAOPERAT113,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP113,0)
            this.w_PRDESAGG = NVL(CADESSUP113,space(0))
            this.w_DATOBSO = NVL(cp_ToDate(CADTOBSO113),ctod("  /  /  "))
          else
          .link_1_13('Load')
          endif
          .link_1_14('Load')
        .w_PRDESPRE = NVL(PRDESPRE,space(40))
        .w_PRUNIMIS = NVL(PRUNIMIS,space(3))
          if link_1_16_joined
            this.w_PRUNIMIS = NVL(UMCODICE116,NVL(this.w_PRUNIMIS,space(3)))
            this.w_UMDESCRI = NVL(UMDESCRI116,space(35))
            this.w_CHKTEMP = NVL(UMFLTEMP116,space(1))
            this.w_DUR_ORE = NVL(UMDURORE116,0)
          else
          .link_1_16('Load')
          endif
        .w_PRQTAMOV = NVL(PRQTAMOV,0)
        .w_PRPREZZO = NVL(PRPREZZO,0)
        .w_PRTIPRIG = NVL(PRTIPRIG,space(1))
        .w_PRTIPRI2 = NVL(PRTIPRI2,space(1))
        .w_PROREEFF = NVL(PROREEFF,0)
        .w_PRMINEFF = NVL(PRMINEFF,0)
        .w_PRCOSINT = NVL(PRCOSINT,0)
        .w_PRDESAGG = NVL(PRDESAGG,space(0))
        .w_PRCODNOM = NVL(PRCODNOM,space(15))
          if link_1_27_joined
            this.w_PRCODNOM = NVL(NOCODICE127,NVL(this.w_PRCODNOM,space(15)))
            this.w_NODESCR = NVL(NODESCRI127,space(60))
          else
          .link_1_27('Load')
          endif
        .w_PRNUMPRE = NVL(PRNUMPRE,0)
        .op_PRNUMPRE = .w_PRNUMPRE
        .w_PRCODVAL = NVL(PRCODVAL,space(3))
          if link_1_29_joined
            this.w_PRCODVAL = NVL(VACODVAL129,NVL(this.w_PRCODVAL,space(3)))
            this.w_DECTOT = NVL(VADECTOT129,0)
            this.w_DECUNI = NVL(VADECUNI129,0)
          else
          .link_1_29('Load')
          endif
        .w_PRCODLIS = NVL(PRCODLIS,space(5))
          * evitabile
          *.link_1_30('Load')
        .w_PRPREMIN = NVL(PRPREMIN,0)
        .w_PRPREMAX = NVL(PRPREMAX,0)
        .w_PRGAZUFF = NVL(PRGAZUFF,space(6))
        .w_PRGRURES = NVL(PRGRURES,space(5))
        .w_PRCODOPE = NVL(PRCODOPE,0)
        .w_PRDATMOD = NVL(cp_ToDate(PRDATMOD),ctod("  /  /  "))
        .w_PRRIFFAT = NVL(PRRIFFAT,space(10))
          .link_1_57('Load')
        .w_PRRIFPRO = NVL(PRRIFPRO,space(10))
          .link_1_58('Load')
        .w_PRRIFNOT = NVL(PRRIFNOT,space(10))
          .link_1_59('Load')
        .w_PRRIFBOZ = NVL(PRRIFBOZ,space(20))
          .link_1_60('Load')
        .w_PRROWBOZ = NVL(PRROWBOZ,0)
        .w_RIFBOZ = IIF( .w_PRRIFBOZ='@@@@@@@@@@',Space(10),.w_PRRIFBOZ)
        .w_PRROWNOT = NVL(PRROWNOT,0)
        .w_PRVOCCOS = NVL(PRVOCCOS,space(15))
          if link_2_1_joined
            this.w_PRVOCCOS = NVL(VCCODICE201,NVL(this.w_PRVOCCOS,space(15)))
            this.w_VOCCOSDE = NVL(VCDESCRI201,space(40))
          else
          .link_2_1('Load')
          endif
        .w_PRCENCOS = NVL(PRCENCOS,space(15))
          if link_2_2_joined
            this.w_PRCENCOS = NVL(CC_CONTO202,NVL(this.w_PRCENCOS,space(15)))
            this.w_CENCOSDE = NVL(CCDESPIA202,space(40))
          else
          .link_2_2('Load')
          endif
        .w_RIFFAT = IIF( .w_PRRIFFAT='@@@@@@@@@@',Space(10),.w_PRRIFFAT)
        .w_RIFPRO = IIF( .w_PRRIFPRO='@@@@@@@@@@',Space(10),.w_PRRIFPRO)
        .w_RIFNOT = IIF( .w_PRRIFNOT='@@@@@@@@@@',Space(10),.w_PRRIFNOT)
        .w_PRFLDEFF = NVL(PRFLDEFF,space(1))
        .w_PRATTIVI = NVL(PRATTIVI,space(15))
        .w_PR_SEGNO = NVL(PR_SEGNO,space(1))
        .w_PRINICOM = NVL(cp_ToDate(PRINICOM),ctod("  /  /  "))
        .w_PRFINCOM = NVL(cp_ToDate(PRFINCOM),ctod("  /  /  "))
        .w_PRTCOINI = NVL(cp_ToDate(PRTCOINI),ctod("  /  /  "))
        .w_PRTCOFIN = NVL(cp_ToDate(PRTCOFIN),ctod("  /  /  "))
        .w_PRCODSED = NVL(PRCODSED,space(5))
        .w_PRCOSUNI = NVL(PRCOSUNI,0)
        .w_PRRIFPRE = NVL(PRRIFPRE,0)
        .w_PRRIGPRE = NVL(PRRIGPRE,space(1))
        .w_PRSERAGG = NVL(PRSERAGG,space(10))
        .w_PRQTAUM1 = NVL(PRQTAUM1,0)
        .w_PRTIPDOC = NVL(PRTIPDOC,space(5))
        .w_PRROWFAT = NVL(PRROWFAT,0)
        .w_PRROWPRO = NVL(PRROWPRO,0)
          * evitabile
          *.link_1_93('Load')
        .w_PRROWFAA = NVL(PRROWFAA,0)
        .w_PRROWPRA = NVL(PRROWPRA,0)
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .w_PRRIFCON = NVL(PRRIFCON,space(10))
        .w_DENOMRES = alltrim(.w_COGNRESP)+' '+alltrim(.w_NOMRESP)
        .w_PRSERATT = NVL(PRSERATT,space(10))
          .link_1_130('Load')
          .link_1_141('Load')
          .link_1_150('Load')
        .w_PRVALRIG = NVL(PRVALRIG,0)
        .w_PRROWATT = NVL(PRROWATT,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_PRRIFFAA = NVL(PRRIFFAA,space(10))
          .link_1_172('Load')
        .w_PRRIFPRA = NVL(PRRIFPRA,space(10))
          .link_1_173('Load')
        .w_RIFFAA = IIF( .w_PRRIFFAA='@@@@@@@@@@',Space(10),.w_PRRIFFAA)
        .w_RIFPRA = IIF( .w_PRRIFPRA='@@@@@@@@@@',Space(10),.w_PRRIFPRA)
        .w_DATAPREC = Date() - .w_GG_PRE
        .w_DATASUCC = Date() + .w_GG_SUC
        .w_PRSERIAL = NVL(PRSERIAL,space(10))
        .op_PRSERIAL = .w_PRSERIAL
        .w_PRCODESE = NVL(PRCODESE,space(4))
        .w_CollegaPre = IIF(EMPTY(.w_PRSERAGG) AND .w_PRRIGPRE<>'S' AND Chkprest(.w_PRCODATT,'A-S'),'S','N')
        .oPgFrm.Page1.oPag.oObj_1_224.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'PRE_STAZ')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_99.enabled = this.oPgFrm.Page1.oPag.oBtn_1_99.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_100.enabled = this.oPgFrm.Page1.oPag.oBtn_1_100.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_126.enabled = this.oPgFrm.Page1.oPag.oBtn_1_126.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_169.enabled = this.oPgFrm.Page1.oPag.oBtn_1_169.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_177.enabled = this.oPgFrm.Page1.oPag.oBtn_1_177.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_183.enabled = this.oPgFrm.Page1.oPag.oBtn_1_183.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_203.enabled = this.oPgFrm.Page1.oPag.oBtn_1_203.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_207.enabled = this.oPgFrm.Page1.oPag.oBtn_1_207.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LetturaParAgen = space(5)
      .w_OB_TEST = ctod("  /  /  ")
      .w_OBTEST = space(10)
      .w_LeggeParAlte = space(5)
      .w_FLGZER = space(1)
      .w_DECTOT = 0
      .w_PRNUMPRA = space(15)
      .w_PACAUPRE = space(20)
      .w_PRCODRES = space(5)
      .w_DTIPRIG = space(1)
      .w_PR__DATA = ctod("  /  /  ")
      .w_FLSERG = space(1)
      .w_PRCODATT = space(20)
      .w_CODART = space(20)
      .w_PRDESPRE = space(40)
      .w_PRUNIMIS = space(3)
      .w_PRQTAMOV = 0
      .w_PRPREZZO = 0
      .w_PRTIPRIG = space(1)
      .w_PRTIPRI2 = space(1)
      .w_PROREEFF = 0
      .w_PRMINEFF = 0
      .w_PRCOSINT = 0
      .w_PRDESAGG = space(0)
      .w_PRCODNOM = space(15)
      .w_PRNUMPRE = 0
      .w_PRCODVAL = space(3)
      .w_PRCODLIS = space(5)
      .w_UMDESCRI = space(35)
      .w_PRPREMIN = 0
      .w_PRPREMAX = 0
      .w_PRGAZUFF = space(6)
      .w_NUMPRO = 0
      .w_ALFPRO = space(10)
      .w_DATPRO = ctod("  /  /  ")
      .w_NUMFAT = 0
      .w_ALFFAT = space(10)
      .w_DATFAT = ctod("  /  /  ")
      .w_NUMNOT = 0
      .w_ALFNOT = space(10)
      .w_DATNOT = ctod("  /  /  ")
      .w_PRGRURES = space(5)
      .w_PRCODOPE = 0
      .w_PRDATMOD = ctod("  /  /  ")
      .w_LICOSTO = 0
      .w_PRRIFFAT = space(10)
      .w_PRRIFPRO = space(10)
      .w_PRRIFNOT = space(10)
      .w_PRRIFBOZ = space(20)
      .w_PRROWBOZ = 0
      .w_RIFBOZ = space(10)
      .w_PRROWNOT = 0
      .w_PRVOCCOS = space(15)
      .w_PRCENCOS = space(15)
      .w_RIFFAT = space(10)
      .w_RIFPRO = space(10)
      .w_RIFNOT = space(10)
      .w_PRFLDEFF = space(1)
      .w_PRATTIVI = space(15)
      .w_PR_SEGNO = space(1)
      .w_PRINICOM = ctod("  /  /  ")
      .w_PRFINCOM = ctod("  /  /  ")
      .w_PRTCOINI = ctod("  /  /  ")
      .w_PRTCOFIN = ctod("  /  /  ")
      .w_PRCODSED = space(5)
      .w_PRCOSUNI = 0
      .w_PRRIFPRE = 0
      .w_PRRIGPRE = space(1)
      .w_DESCAN = space(100)
      .w_PRESTA = space(1)
      .w_NODESCR = space(60)
      .w_PRSERAGG = space(10)
      .w_PRQTAUM1 = 0
      .w_PRTIPDOC = space(5)
      .w_PRROWFAT = 0
      .w_PRROWPRO = 0
      .w_PRROWFAA = 0
      .w_PRROWPRA = 0
      .w_PRRIFCON = space(10)
      .w_DENOMRES = space(50)
      .w_COGNRESP = space(10)
      .w_NOMRESP = space(10)
      .w_DATOBSO = ctod("  /  /  ")
      .w_VOCCOSDE = space(40)
      .w_CENCOSDE = space(40)
      .w_PRSERATT = space(10)
      .w_PRCODART = space(20)
      .w_COST_ORA = 0
      .w_CNFLAPON = space(10)
      .w_CN__ENTE = space(10)
      .w_CNIMPORT = 0
      .w_CNUFFICI = space(10)
      .w_CNFLVALO = space(1)
      .w_CNPARASS = 0
      .w_CNFLVALO1 = space(1)
      .w_CNCALDIR = space(1)
      .w_CNCOECAL = 0
      .w_CNFLAMPA = space(1)
      .w_CNTARTEM = space(1)
      .w_CNTARCON = 0
      .w_CONTRACN = space(5)
      .w_TIPCONCO = space(1)
      .w_LISCOLCO = space(5)
      .w_STATUSCO = space(1)
      .w_DECUNI = 0
      .w_OPERAT = space(1)
      .w_FLUSEP = space(1)
      .w_UNMIS2 = space(3)
      .w_MOLTIP = 0
      .w_UNMIS1 = space(3)
      .w_FLFRAZ1 = space(1)
      .w_MODUM2 = space(1)
      .w_UNMIS3 = space(3)
      .w_OPERA3 = space(1)
      .w_MOLTI3 = 0
      .w_PRVALRIG = 0
      .w_NOTIFICA = space(1)
      .w_PRROWATT = 0
      .w_CODLIS = space(5)
      .w_UTCC = 0
      .w_UTDC = ctot("")
      .w_UTCV = 0
      .w_UTDV = ctot("")
      .w_TIPART = space(2)
      .w_CAUDOC = space(5)
      .w_NUMBOZ = 0
      .w_ALFBOZ = space(10)
      .w_DATBOZ = ctod("  /  /  ")
      .w_PRRIFFAA = space(10)
      .w_PRRIFPRA = space(10)
      .w_NUMFAA = 0
      .w_ALFFAA = space(10)
      .w_DATFAA = ctod("  /  /  ")
      .w_NUMPRA = 0
      .w_ALFPRA = space(10)
      .w_DATPRA = ctod("  /  /  ")
      .w_RIFFAA = space(10)
      .w_RIFPRA = space(10)
      .w_FLPRES = space(1)
      .w_CHKDATAPRE = space(1)
      .w_GG_PRE = 0
      .w_GG_SUC = 0
      .w_DATAPREC = ctod("  /  /  ")
      .w_DATASUCC = ctod("  /  /  ")
      .w_GENPRE = space(1)
      .w_PRSERIAL = space(10)
      .w_DUR_ORE = 0
      .w_CHKTEMP = space(1)
      .w_CNCODLIS = space(5)
      .w_CNCODVAL = space(3)
      .w_EditCodPra = .f.
      .w_TIPRIS = space(1)
      .w_PRCODESE = space(4)
      .w_CollegaPre = space(1)
      .w_CNMATOBB = space(1)
      .w_CNASSCTP = space(1)
      .w_CNCOMPLX = space(1)
      .w_CNPROFMT = space(1)
      .w_CNESIPOS = space(1)
      .w_CNPERPLX = 0
      .w_CNPERPOS = 0
      .w_DETIPPRA = space(10)
      .w_TIPRIG = space(1)
      .w_TIPRI2 = space(1)
      .w_MESS = space(10)
      .w_CNFLVMLQ = space(1)
      .w_OLD_PR__DATA = ctod("  /  /  ")
      .w_OLD_PRCODRES = space(5)
      .w_OLD_PRTIPRIG = space(1)
      .w_OLD_PRTIPRI2 = space(1)
      .w_OLD_PRNUMPRA = space(15)
      .w_OLD_PRESTA = space(1)
      .w_VOCRIC = space(15)
      .w_TIPOENTE = space(1)
      .w_SERPRE = space(41)
      .w_RESCHK = 0
      if .cFunction<>"Filter"
        .w_LetturaParAgen = i_CodAzi
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_LetturaParAgen))
          .link_1_1('Full')
          endif
        .w_OB_TEST = i_datsys
        .w_OBTEST = i_DATSYS
        .w_LeggeParAlte = i_codazi
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_LeggeParAlte))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,7,.f.)
          if not(empty(.w_PRNUMPRA))
          .link_1_7('Full')
          endif
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_PACAUPRE))
          .link_1_8('Full')
          endif
        .w_PRCODRES = ReadResp(i_codute, "C", .w_PRNUMPRA)
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_PRCODRES))
          .link_1_9('Full')
          endif
          .DoRTCalc(10,10,.f.)
        .w_PR__DATA = i_DATSYS
          .DoRTCalc(12,12,.f.)
        .w_PRCODATT = IIF(NOT EMPTY(.w_SERPRE), .w_SERPRE, .w_PRCODATT)
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_PRCODATT))
          .link_1_13('Full')
          endif
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_CODART))
          .link_1_14('Full')
          endif
          .DoRTCalc(15,15,.f.)
        .w_PRUNIMIS = .w_UNMIS1
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_PRUNIMIS))
          .link_1_16('Full')
          endif
        .w_PRQTAMOV = IIF(.w_TIPART='DE', 0, IIF(.w_PRQTAMOV>0,.w_PRQTAMOV,1))
          .DoRTCalc(18,18,.f.)
        .w_PRTIPRIG = iCASE(.w_TIPART='FO' and .w_PRPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N',Not empty(.w_TIPRIG),.w_TIPRIG, Not empty(.w_PRTIPRIG),.w_PRTIPRIG,.w_DTIPRIG)
        .w_PRTIPRI2 = ICASE(.w_TIPART='FO' and .w_PRPREZZO=0 and .w_FLGZER='N', 'N', .w_PRPREZZO<>0, .w_TIPRI2, Not Empty(.w_TIPRI2), .w_TIPRI2, 'N')
        .w_PROREEFF = min(IIF(.w_CHKTEMP='S', INT(.w_DUR_ORE * .w_PRQTAMOV), 0),999)
        .w_PRMINEFF = IIF(.w_CHKTEMP='S', INT(cp_round((.w_DUR_ORE * .w_PRQTAMOV - INT(.w_DUR_ORE * .w_PRQTAMOV)) * 60,0)), 0)
        .w_PRCOSINT = IIF(.w_LICOSTO>0 and .w_COST_ORA=0 ,.w_PRCOSUNI*.w_PRQTAMOV,(.w_PROREEFF+(.w_PRMINEFF/60))*.w_PRCOSUNI)
        .DoRTCalc(24,25,.f.)
          if not(empty(.w_PRCODNOM))
          .link_1_27('Full')
          endif
          .DoRTCalc(26,26,.f.)
        .w_PRCODVAL = iif(Not Empty(.w_CNCODVAL),.w_CNCODVAL,g_perval)
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_PRCODVAL))
          .link_1_29('Full')
          endif
        .w_PRCODLIS = iif(Not Empty(.w_CNCODLIS),.w_CNCODLIS,.w_CODLIS)
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_PRCODLIS))
          .link_1_30('Full')
          endif
          .DoRTCalc(29,42,.f.)
        .w_PRCODOPE = i_codute
        .DoRTCalc(44,46,.f.)
          if not(empty(.w_PRRIFFAT))
          .link_1_57('Full')
          endif
        .DoRTCalc(47,47,.f.)
          if not(empty(.w_PRRIFPRO))
          .link_1_58('Full')
          endif
        .DoRTCalc(48,48,.f.)
          if not(empty(.w_PRRIFNOT))
          .link_1_59('Full')
          endif
        .DoRTCalc(49,49,.f.)
          if not(empty(.w_PRRIFBOZ))
          .link_1_60('Full')
          endif
          .DoRTCalc(50,50,.f.)
        .w_RIFBOZ = IIF( .w_PRRIFBOZ='@@@@@@@@@@',Space(10),.w_PRRIFBOZ)
          .DoRTCalc(52,52,.f.)
        .w_PRVOCCOS = .w_VOCRIC
        .DoRTCalc(53,53,.f.)
          if not(empty(.w_PRVOCCOS))
          .link_2_1('Full')
          endif
        .DoRTCalc(54,54,.f.)
          if not(empty(.w_PRCENCOS))
          .link_2_2('Full')
          endif
        .w_RIFFAT = IIF( .w_PRRIFFAT='@@@@@@@@@@',Space(10),.w_PRRIFFAT)
        .w_RIFPRO = IIF( .w_PRRIFPRO='@@@@@@@@@@',Space(10),.w_PRRIFPRO)
        .w_RIFNOT = IIF( .w_PRRIFNOT='@@@@@@@@@@',Space(10),.w_PRRIFNOT)
          .DoRTCalc(58,59,.f.)
        .w_PR_SEGNO = 'A'
          .DoRTCalc(61,72,.f.)
        .w_PRQTAUM1 = CALQTA(.w_PRQTAMOV,.w_PRUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ', .w_UNMIS3, .w_OPERA3, .w_MOLTI3)
        .w_PRTIPDOC = .w_CAUDOC
        .DoRTCalc(75,76,.f.)
          if not(empty(.w_PRROWPRO))
          .link_1_93('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
          .DoRTCalc(77,79,.f.)
        .w_DENOMRES = alltrim(.w_COGNRESP)+' '+alltrim(.w_NOMRESP)
        .DoRTCalc(81,90,.f.)
          if not(empty(.w_CN__ENTE))
          .link_1_130('Full')
          endif
        .DoRTCalc(91,101,.f.)
          if not(empty(.w_CONTRACN))
          .link_1_141('Full')
          endif
        .DoRTCalc(102,110,.f.)
          if not(empty(.w_UNMIS1))
          .link_1_150('Full')
          endif
          .DoRTCalc(111,115,.f.)
        .w_PRVALRIG = CAVALRIG(.w_PRPREZZO,.w_PRQTAMOV, 0,0,0,0,IIF(.w_NOTIFICA='S', 0, .w_DECTOT))
        .DoRTCalc(117,129,.f.)
          if not(empty(.w_PRRIFFAA))
          .link_1_172('Full')
          endif
        .DoRTCalc(130,130,.f.)
          if not(empty(.w_PRRIFPRA))
          .link_1_173('Full')
          endif
          .DoRTCalc(131,136,.f.)
        .w_RIFFAA = IIF( .w_PRRIFFAA='@@@@@@@@@@',Space(10),.w_PRRIFFAA)
        .w_RIFPRA = IIF( .w_PRRIFPRA='@@@@@@@@@@',Space(10),.w_PRRIFPRA)
          .DoRTCalc(139,142,.f.)
        .w_DATAPREC = Date() - .w_GG_PRE
        .w_DATASUCC = Date() + .w_GG_SUC
          .DoRTCalc(145,150,.f.)
        .w_EditCodPra = .F.
          .DoRTCalc(152,152,.f.)
        .w_PRCODESE = g_CODESE
        .w_CollegaPre = IIF(EMPTY(.w_PRSERAGG) AND .w_PRRIGPRE<>'S' AND Chkprest(.w_PRCODATT,'A-S'),'S','N')
        .oPgFrm.Page1.oPag.oObj_1_224.Calculate()
          .DoRTCalc(155,175,.f.)
        .w_RESCHK = 0
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRE_STAZ')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_48.enabled = this.oPgFrm.Page1.oPag.oBtn_1_48.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_99.enabled = this.oPgFrm.Page1.oPag.oBtn_1_99.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_100.enabled = this.oPgFrm.Page1.oPag.oBtn_1_100.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_126.enabled = this.oPgFrm.Page1.oPag.oBtn_1_126.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_169.enabled = this.oPgFrm.Page1.oPag.oBtn_1_169.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_177.enabled = this.oPgFrm.Page1.oPag.oBtn_1_177.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_183.enabled = this.oPgFrm.Page1.oPag.oBtn_1_183.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_203.enabled = this.oPgFrm.Page1.oPag.oBtn_1_203.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_207.enabled = this.oPgFrm.Page1.oPag.oBtn_1_207.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRE_STAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_STAZ_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEPRE","i_codazi,w_PRSERIAL")
      cp_AskTableProg(this,i_nConn,"NUPRE","i_codazi,w_PRNUMPRA,w_PRNUMPRE")
      .op_codazi = .w_codazi
      .op_PRSERIAL = .w_PRSERIAL
      .op_codazi = .w_codazi
      .op_PRNUMPRA = .w_PRNUMPRA
      .op_PRNUMPRE = .w_PRNUMPRE
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPRSERIAL_1_196.enabled = !i_bVal
      .Page1.oPag.oPRNUMPRA_1_7.enabled = i_bVal
      .Page1.oPag.oPRCODRES_1_9.enabled = i_bVal
      .Page1.oPag.oPR__DATA_1_11.enabled = i_bVal
      .Page1.oPag.oPRCODATT_1_13.enabled = i_bVal
      .Page1.oPag.oPRDESPRE_1_15.enabled = i_bVal
      .Page1.oPag.oPRUNIMIS_1_16.enabled = i_bVal
      .Page1.oPag.oPRQTAMOV_1_17.enabled = i_bVal
      .Page1.oPag.oPRPREZZO_1_18.enabled = i_bVal
      .Page1.oPag.oPRTIPRIG_1_21.enabled = i_bVal
      .Page1.oPag.oPRTIPRI2_1_22.enabled = i_bVal
      .Page1.oPag.oPROREEFF_1_23.enabled = i_bVal
      .Page1.oPag.oPRMINEFF_1_24.enabled = i_bVal
      .Page1.oPag.oPRCOSINT_1_25.enabled = i_bVal
      .Page1.oPag.oPRDESAGG_1_26.enabled = i_bVal
      .Page1.oPag.oPRNUMPRE_1_28.enabled = i_bVal
      .Page1.oPag.oPRCODLIS_1_30.enabled = i_bVal
      .Page2.oPag.oPRVOCCOS_2_1.enabled = i_bVal
      .Page2.oPag.oPRCENCOS_2_2.enabled = i_bVal
      .Page2.oPag.oPRATTIVI_2_5.enabled = i_bVal
      .Page2.oPag.oPR_SEGNO_2_6.enabled = i_bVal
      .Page2.oPag.oPRINICOM_2_9.enabled = i_bVal
      .Page2.oPag.oPRFINCOM_2_11.enabled = i_bVal
      .Page1.oPag.oBtn_1_40.enabled = .Page1.oPag.oBtn_1_40.mCond()
      .Page1.oPag.oBtn_1_44.enabled = .Page1.oPag.oBtn_1_44.mCond()
      .Page1.oPag.oBtn_1_48.enabled = .Page1.oPag.oBtn_1_48.mCond()
      .Page1.oPag.oBtn_1_99.enabled = .Page1.oPag.oBtn_1_99.mCond()
      .Page1.oPag.oBtn_1_100.enabled = .Page1.oPag.oBtn_1_100.mCond()
      .Page1.oPag.oBtn_1_126.enabled = .Page1.oPag.oBtn_1_126.mCond()
      .Page1.oPag.oBtn_1_169.enabled = .Page1.oPag.oBtn_1_169.mCond()
      .Page1.oPag.oBtn_1_177.enabled = .Page1.oPag.oBtn_1_177.mCond()
      .Page1.oPag.oBtn_1_183.enabled = .Page1.oPag.oBtn_1_183.mCond()
      .Page1.oPag.oBtn_1_203.enabled = .Page1.oPag.oBtn_1_203.mCond()
      .Page1.oPag.oBtn_1_207.enabled = .Page1.oPag.oBtn_1_207.mCond()
      .Page1.oPag.oObj_1_96.enabled = i_bVal
      .Page1.oPag.oObj_1_224.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oPRNUMPRA_1_7.enabled = .t.
        .Page1.oPag.oPRCODRES_1_9.enabled = .t.
        .Page1.oPag.oPRCODATT_1_13.enabled = .t.
        .Page1.oPag.oPRNUMPRE_1_28.enabled = .t.
      endif
    endwith
    this.gsag_mmp.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PRE_STAZ',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gsag_mmp.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRE_STAZ_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRNUMPRA,"PRNUMPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODRES,"PRCODRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PR__DATA,"PR__DATA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODATT,"PRCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDESPRE,"PRDESPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRUNIMIS,"PRUNIMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRQTAMOV,"PRQTAMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPREZZO,"PRPREZZO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPRIG,"PRTIPRIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPRI2,"PRTIPRI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PROREEFF,"PROREEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRMINEFF,"PRMINEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCOSINT,"PRCOSINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDESAGG,"PRDESAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODNOM,"PRCODNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRNUMPRE,"PRNUMPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODVAL,"PRCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODLIS,"PRCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPREMIN,"PRPREMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPREMAX,"PRPREMAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGAZUFF,"PRGAZUFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRGRURES,"PRGRURES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODOPE,"PRCODOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATMOD,"PRDATMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRRIFFAT,"PRRIFFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRRIFPRO,"PRRIFPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRRIFNOT,"PRRIFNOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRRIFBOZ,"PRRIFBOZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRROWBOZ,"PRROWBOZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRROWNOT,"PRROWNOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRVOCCOS,"PRVOCCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCENCOS,"PRCENCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRFLDEFF,"PRFLDEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRATTIVI,"PRATTIVI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PR_SEGNO,"PR_SEGNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRINICOM,"PRINICOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRFINCOM,"PRFINCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTCOINI,"PRTCOINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTCOFIN,"PRTCOFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODSED,"PRCODSED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCOSUNI,"PRCOSUNI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRRIFPRE,"PRRIFPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRRIGPRE,"PRRIGPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRSERAGG,"PRSERAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRQTAUM1,"PRQTAUM1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPDOC,"PRTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRROWFAT,"PRROWFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRROWPRO,"PRROWPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRROWFAA,"PRROWFAA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRROWPRA,"PRROWPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRRIFCON,"PRRIFCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRSERATT,"PRSERATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRVALRIG,"PRVALRIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRROWATT,"PRROWATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRRIFFAA,"PRRIFFAA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRRIFPRA,"PRRIFPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRSERIAL,"PRSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODESE,"PRCODESE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRE_STAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_STAZ_IDX,2])
    i_lTable = "PRE_STAZ"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PRE_STAZ_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRE_STAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_STAZ_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PRE_STAZ_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEPRE","i_codazi,w_PRSERIAL")
          cp_NextTableProg(this,i_nConn,"NUPRE","i_codazi,w_PRNUMPRA,w_PRNUMPRE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PRE_STAZ
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRE_STAZ')
        i_extval=cp_InsertValODBCExtFlds(this,'PRE_STAZ')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PRNUMPRA,PRCODRES,PR__DATA,PRCODATT,PRDESPRE"+;
                  ",PRUNIMIS,PRQTAMOV,PRPREZZO,PRTIPRIG,PRTIPRI2"+;
                  ",PROREEFF,PRMINEFF,PRCOSINT,PRDESAGG,PRCODNOM"+;
                  ",PRNUMPRE,PRCODVAL,PRCODLIS,PRPREMIN,PRPREMAX"+;
                  ",PRGAZUFF,PRGRURES,PRCODOPE,PRDATMOD,PRRIFFAT"+;
                  ",PRRIFPRO,PRRIFNOT,PRRIFBOZ,PRROWBOZ,PRROWNOT"+;
                  ",PRVOCCOS,PRCENCOS,PRFLDEFF,PRATTIVI,PR_SEGNO"+;
                  ",PRINICOM,PRFINCOM,PRTCOINI,PRTCOFIN,PRCODSED"+;
                  ",PRCOSUNI,PRRIFPRE,PRRIGPRE,PRSERAGG,PRQTAUM1"+;
                  ",PRTIPDOC,PRROWFAT,PRROWPRO,PRROWFAA,PRROWPRA"+;
                  ",PRRIFCON,PRSERATT,PRVALRIG,PRROWATT,UTCC"+;
                  ",UTDC,UTCV,UTDV,PRRIFFAA,PRRIFPRA"+;
                  ",PRSERIAL,PRCODESE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_PRNUMPRA)+;
                  ","+cp_ToStrODBCNull(this.w_PRCODRES)+;
                  ","+cp_ToStrODBC(this.w_PR__DATA)+;
                  ","+cp_ToStrODBCNull(this.w_PRCODATT)+;
                  ","+cp_ToStrODBC(this.w_PRDESPRE)+;
                  ","+cp_ToStrODBCNull(this.w_PRUNIMIS)+;
                  ","+cp_ToStrODBC(this.w_PRQTAMOV)+;
                  ","+cp_ToStrODBC(this.w_PRPREZZO)+;
                  ","+cp_ToStrODBC(this.w_PRTIPRIG)+;
                  ","+cp_ToStrODBC(this.w_PRTIPRI2)+;
                  ","+cp_ToStrODBC(this.w_PROREEFF)+;
                  ","+cp_ToStrODBC(this.w_PRMINEFF)+;
                  ","+cp_ToStrODBC(this.w_PRCOSINT)+;
                  ","+cp_ToStrODBC(this.w_PRDESAGG)+;
                  ","+cp_ToStrODBCNull(this.w_PRCODNOM)+;
                  ","+cp_ToStrODBC(this.w_PRNUMPRE)+;
                  ","+cp_ToStrODBCNull(this.w_PRCODVAL)+;
                  ","+cp_ToStrODBCNull(this.w_PRCODLIS)+;
                  ","+cp_ToStrODBC(this.w_PRPREMIN)+;
                  ","+cp_ToStrODBC(this.w_PRPREMAX)+;
                  ","+cp_ToStrODBC(this.w_PRGAZUFF)+;
                  ","+cp_ToStrODBC(this.w_PRGRURES)+;
                  ","+cp_ToStrODBC(this.w_PRCODOPE)+;
                  ","+cp_ToStrODBC(this.w_PRDATMOD)+;
                  ","+cp_ToStrODBCNull(this.w_PRRIFFAT)+;
                  ","+cp_ToStrODBCNull(this.w_PRRIFPRO)+;
                  ","+cp_ToStrODBCNull(this.w_PRRIFNOT)+;
                  ","+cp_ToStrODBCNull(this.w_PRRIFBOZ)+;
                  ","+cp_ToStrODBC(this.w_PRROWBOZ)+;
                  ","+cp_ToStrODBC(this.w_PRROWNOT)+;
                  ","+cp_ToStrODBCNull(this.w_PRVOCCOS)+;
                  ","+cp_ToStrODBCNull(this.w_PRCENCOS)+;
                  ","+cp_ToStrODBC(this.w_PRFLDEFF)+;
                  ","+cp_ToStrODBC(this.w_PRATTIVI)+;
                  ","+cp_ToStrODBC(this.w_PR_SEGNO)+;
                  ","+cp_ToStrODBC(this.w_PRINICOM)+;
                  ","+cp_ToStrODBC(this.w_PRFINCOM)+;
                  ","+cp_ToStrODBC(this.w_PRTCOINI)+;
                  ","+cp_ToStrODBC(this.w_PRTCOFIN)+;
                  ","+cp_ToStrODBC(this.w_PRCODSED)+;
                  ","+cp_ToStrODBC(this.w_PRCOSUNI)+;
                  ","+cp_ToStrODBC(this.w_PRRIFPRE)+;
                  ","+cp_ToStrODBC(this.w_PRRIGPRE)+;
                  ","+cp_ToStrODBC(this.w_PRSERAGG)+;
                  ","+cp_ToStrODBC(this.w_PRQTAUM1)+;
                  ","+cp_ToStrODBC(this.w_PRTIPDOC)+;
                  ","+cp_ToStrODBC(this.w_PRROWFAT)+;
                  ","+cp_ToStrODBCNull(this.w_PRROWPRO)+;
                  ","+cp_ToStrODBC(this.w_PRROWFAA)+;
                  ","+cp_ToStrODBC(this.w_PRROWPRA)+;
                  ","+cp_ToStrODBC(this.w_PRRIFCON)+;
                  ","+cp_ToStrODBC(this.w_PRSERATT)+;
                  ","+cp_ToStrODBC(this.w_PRVALRIG)+;
                  ","+cp_ToStrODBC(this.w_PRROWATT)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_PRRIFFAA)+;
                  ","+cp_ToStrODBCNull(this.w_PRRIFPRA)+;
                  ","+cp_ToStrODBC(this.w_PRSERIAL)+;
                  ","+cp_ToStrODBC(this.w_PRCODESE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRE_STAZ')
        i_extval=cp_InsertValVFPExtFlds(this,'PRE_STAZ')
        cp_CheckDeletedKey(i_cTable,0,'PRSERIAL',this.w_PRSERIAL)
        INSERT INTO (i_cTable);
              (PRNUMPRA,PRCODRES,PR__DATA,PRCODATT,PRDESPRE,PRUNIMIS,PRQTAMOV,PRPREZZO,PRTIPRIG,PRTIPRI2,PROREEFF,PRMINEFF,PRCOSINT,PRDESAGG,PRCODNOM,PRNUMPRE,PRCODVAL,PRCODLIS,PRPREMIN,PRPREMAX,PRGAZUFF,PRGRURES,PRCODOPE,PRDATMOD,PRRIFFAT,PRRIFPRO,PRRIFNOT,PRRIFBOZ,PRROWBOZ,PRROWNOT,PRVOCCOS,PRCENCOS,PRFLDEFF,PRATTIVI,PR_SEGNO,PRINICOM,PRFINCOM,PRTCOINI,PRTCOFIN,PRCODSED,PRCOSUNI,PRRIFPRE,PRRIGPRE,PRSERAGG,PRQTAUM1,PRTIPDOC,PRROWFAT,PRROWPRO,PRROWFAA,PRROWPRA,PRRIFCON,PRSERATT,PRVALRIG,PRROWATT,UTCC,UTDC,UTCV,UTDV,PRRIFFAA,PRRIFPRA,PRSERIAL,PRCODESE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PRNUMPRA;
                  ,this.w_PRCODRES;
                  ,this.w_PR__DATA;
                  ,this.w_PRCODATT;
                  ,this.w_PRDESPRE;
                  ,this.w_PRUNIMIS;
                  ,this.w_PRQTAMOV;
                  ,this.w_PRPREZZO;
                  ,this.w_PRTIPRIG;
                  ,this.w_PRTIPRI2;
                  ,this.w_PROREEFF;
                  ,this.w_PRMINEFF;
                  ,this.w_PRCOSINT;
                  ,this.w_PRDESAGG;
                  ,this.w_PRCODNOM;
                  ,this.w_PRNUMPRE;
                  ,this.w_PRCODVAL;
                  ,this.w_PRCODLIS;
                  ,this.w_PRPREMIN;
                  ,this.w_PRPREMAX;
                  ,this.w_PRGAZUFF;
                  ,this.w_PRGRURES;
                  ,this.w_PRCODOPE;
                  ,this.w_PRDATMOD;
                  ,this.w_PRRIFFAT;
                  ,this.w_PRRIFPRO;
                  ,this.w_PRRIFNOT;
                  ,this.w_PRRIFBOZ;
                  ,this.w_PRROWBOZ;
                  ,this.w_PRROWNOT;
                  ,this.w_PRVOCCOS;
                  ,this.w_PRCENCOS;
                  ,this.w_PRFLDEFF;
                  ,this.w_PRATTIVI;
                  ,this.w_PR_SEGNO;
                  ,this.w_PRINICOM;
                  ,this.w_PRFINCOM;
                  ,this.w_PRTCOINI;
                  ,this.w_PRTCOFIN;
                  ,this.w_PRCODSED;
                  ,this.w_PRCOSUNI;
                  ,this.w_PRRIFPRE;
                  ,this.w_PRRIGPRE;
                  ,this.w_PRSERAGG;
                  ,this.w_PRQTAUM1;
                  ,this.w_PRTIPDOC;
                  ,this.w_PRROWFAT;
                  ,this.w_PRROWPRO;
                  ,this.w_PRROWFAA;
                  ,this.w_PRROWPRA;
                  ,this.w_PRRIFCON;
                  ,this.w_PRSERATT;
                  ,this.w_PRVALRIG;
                  ,this.w_PRROWATT;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTCV;
                  ,this.w_UTDV;
                  ,this.w_PRRIFFAA;
                  ,this.w_PRRIFPRA;
                  ,this.w_PRSERIAL;
                  ,this.w_PRCODESE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PRE_STAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_STAZ_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PRE_STAZ_IDX,i_nConn)
      *
      * update PRE_STAZ
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PRE_STAZ')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PRNUMPRA="+cp_ToStrODBCNull(this.w_PRNUMPRA)+;
             ",PRCODRES="+cp_ToStrODBCNull(this.w_PRCODRES)+;
             ",PR__DATA="+cp_ToStrODBC(this.w_PR__DATA)+;
             ",PRCODATT="+cp_ToStrODBCNull(this.w_PRCODATT)+;
             ",PRDESPRE="+cp_ToStrODBC(this.w_PRDESPRE)+;
             ",PRUNIMIS="+cp_ToStrODBCNull(this.w_PRUNIMIS)+;
             ",PRQTAMOV="+cp_ToStrODBC(this.w_PRQTAMOV)+;
             ",PRPREZZO="+cp_ToStrODBC(this.w_PRPREZZO)+;
             ",PRTIPRIG="+cp_ToStrODBC(this.w_PRTIPRIG)+;
             ",PRTIPRI2="+cp_ToStrODBC(this.w_PRTIPRI2)+;
             ",PROREEFF="+cp_ToStrODBC(this.w_PROREEFF)+;
             ",PRMINEFF="+cp_ToStrODBC(this.w_PRMINEFF)+;
             ",PRCOSINT="+cp_ToStrODBC(this.w_PRCOSINT)+;
             ",PRDESAGG="+cp_ToStrODBC(this.w_PRDESAGG)+;
             ",PRCODNOM="+cp_ToStrODBCNull(this.w_PRCODNOM)+;
             ",PRNUMPRE="+cp_ToStrODBC(this.w_PRNUMPRE)+;
             ",PRCODVAL="+cp_ToStrODBCNull(this.w_PRCODVAL)+;
             ",PRCODLIS="+cp_ToStrODBCNull(this.w_PRCODLIS)+;
             ",PRPREMIN="+cp_ToStrODBC(this.w_PRPREMIN)+;
             ",PRPREMAX="+cp_ToStrODBC(this.w_PRPREMAX)+;
             ",PRGAZUFF="+cp_ToStrODBC(this.w_PRGAZUFF)+;
             ",PRGRURES="+cp_ToStrODBC(this.w_PRGRURES)+;
             ",PRCODOPE="+cp_ToStrODBC(this.w_PRCODOPE)+;
             ",PRDATMOD="+cp_ToStrODBC(this.w_PRDATMOD)+;
             ",PRRIFFAT="+cp_ToStrODBCNull(this.w_PRRIFFAT)+;
             ",PRRIFPRO="+cp_ToStrODBCNull(this.w_PRRIFPRO)+;
             ",PRRIFNOT="+cp_ToStrODBCNull(this.w_PRRIFNOT)+;
             ",PRRIFBOZ="+cp_ToStrODBCNull(this.w_PRRIFBOZ)+;
             ",PRROWBOZ="+cp_ToStrODBC(this.w_PRROWBOZ)+;
             ",PRROWNOT="+cp_ToStrODBC(this.w_PRROWNOT)+;
             ",PRVOCCOS="+cp_ToStrODBCNull(this.w_PRVOCCOS)+;
             ",PRCENCOS="+cp_ToStrODBCNull(this.w_PRCENCOS)+;
             ",PRFLDEFF="+cp_ToStrODBC(this.w_PRFLDEFF)+;
             ",PRATTIVI="+cp_ToStrODBC(this.w_PRATTIVI)+;
             ",PR_SEGNO="+cp_ToStrODBC(this.w_PR_SEGNO)+;
             ",PRINICOM="+cp_ToStrODBC(this.w_PRINICOM)+;
             ",PRFINCOM="+cp_ToStrODBC(this.w_PRFINCOM)+;
             ",PRTCOINI="+cp_ToStrODBC(this.w_PRTCOINI)+;
             ",PRTCOFIN="+cp_ToStrODBC(this.w_PRTCOFIN)+;
             ",PRCODSED="+cp_ToStrODBC(this.w_PRCODSED)+;
             ",PRCOSUNI="+cp_ToStrODBC(this.w_PRCOSUNI)+;
             ",PRRIFPRE="+cp_ToStrODBC(this.w_PRRIFPRE)+;
             ",PRRIGPRE="+cp_ToStrODBC(this.w_PRRIGPRE)+;
             ",PRSERAGG="+cp_ToStrODBC(this.w_PRSERAGG)+;
             ",PRQTAUM1="+cp_ToStrODBC(this.w_PRQTAUM1)+;
             ",PRTIPDOC="+cp_ToStrODBC(this.w_PRTIPDOC)+;
             ",PRROWFAT="+cp_ToStrODBC(this.w_PRROWFAT)+;
             ",PRROWPRO="+cp_ToStrODBCNull(this.w_PRROWPRO)+;
             ",PRROWFAA="+cp_ToStrODBC(this.w_PRROWFAA)+;
             ",PRROWPRA="+cp_ToStrODBC(this.w_PRROWPRA)+;
             ",PRRIFCON="+cp_ToStrODBC(this.w_PRRIFCON)+;
             ",PRSERATT="+cp_ToStrODBC(this.w_PRSERATT)+;
             ",PRVALRIG="+cp_ToStrODBC(this.w_PRVALRIG)+;
             ",PRROWATT="+cp_ToStrODBC(this.w_PRROWATT)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",PRRIFFAA="+cp_ToStrODBCNull(this.w_PRRIFFAA)+;
             ",PRRIFPRA="+cp_ToStrODBCNull(this.w_PRRIFPRA)+;
             ",PRCODESE="+cp_ToStrODBC(this.w_PRCODESE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PRE_STAZ')
        i_cWhere = cp_PKFox(i_cTable  ,'PRSERIAL',this.w_PRSERIAL  )
        UPDATE (i_cTable) SET;
              PRNUMPRA=this.w_PRNUMPRA;
             ,PRCODRES=this.w_PRCODRES;
             ,PR__DATA=this.w_PR__DATA;
             ,PRCODATT=this.w_PRCODATT;
             ,PRDESPRE=this.w_PRDESPRE;
             ,PRUNIMIS=this.w_PRUNIMIS;
             ,PRQTAMOV=this.w_PRQTAMOV;
             ,PRPREZZO=this.w_PRPREZZO;
             ,PRTIPRIG=this.w_PRTIPRIG;
             ,PRTIPRI2=this.w_PRTIPRI2;
             ,PROREEFF=this.w_PROREEFF;
             ,PRMINEFF=this.w_PRMINEFF;
             ,PRCOSINT=this.w_PRCOSINT;
             ,PRDESAGG=this.w_PRDESAGG;
             ,PRCODNOM=this.w_PRCODNOM;
             ,PRNUMPRE=this.w_PRNUMPRE;
             ,PRCODVAL=this.w_PRCODVAL;
             ,PRCODLIS=this.w_PRCODLIS;
             ,PRPREMIN=this.w_PRPREMIN;
             ,PRPREMAX=this.w_PRPREMAX;
             ,PRGAZUFF=this.w_PRGAZUFF;
             ,PRGRURES=this.w_PRGRURES;
             ,PRCODOPE=this.w_PRCODOPE;
             ,PRDATMOD=this.w_PRDATMOD;
             ,PRRIFFAT=this.w_PRRIFFAT;
             ,PRRIFPRO=this.w_PRRIFPRO;
             ,PRRIFNOT=this.w_PRRIFNOT;
             ,PRRIFBOZ=this.w_PRRIFBOZ;
             ,PRROWBOZ=this.w_PRROWBOZ;
             ,PRROWNOT=this.w_PRROWNOT;
             ,PRVOCCOS=this.w_PRVOCCOS;
             ,PRCENCOS=this.w_PRCENCOS;
             ,PRFLDEFF=this.w_PRFLDEFF;
             ,PRATTIVI=this.w_PRATTIVI;
             ,PR_SEGNO=this.w_PR_SEGNO;
             ,PRINICOM=this.w_PRINICOM;
             ,PRFINCOM=this.w_PRFINCOM;
             ,PRTCOINI=this.w_PRTCOINI;
             ,PRTCOFIN=this.w_PRTCOFIN;
             ,PRCODSED=this.w_PRCODSED;
             ,PRCOSUNI=this.w_PRCOSUNI;
             ,PRRIFPRE=this.w_PRRIFPRE;
             ,PRRIGPRE=this.w_PRRIGPRE;
             ,PRSERAGG=this.w_PRSERAGG;
             ,PRQTAUM1=this.w_PRQTAUM1;
             ,PRTIPDOC=this.w_PRTIPDOC;
             ,PRROWFAT=this.w_PRROWFAT;
             ,PRROWPRO=this.w_PRROWPRO;
             ,PRROWFAA=this.w_PRROWFAA;
             ,PRROWPRA=this.w_PRROWPRA;
             ,PRRIFCON=this.w_PRRIFCON;
             ,PRSERATT=this.w_PRSERATT;
             ,PRVALRIG=this.w_PRVALRIG;
             ,PRROWATT=this.w_PRROWATT;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTCV=this.w_UTCV;
             ,UTDV=this.w_UTDV;
             ,PRRIFFAA=this.w_PRRIFFAA;
             ,PRRIFPRA=this.w_PRRIFPRA;
             ,PRCODESE=this.w_PRCODESE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- gsag_mmp : Saving
      this.gsag_mmp.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PRSERIAL,"DPSERIAL";
             )
      this.gsag_mmp.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsag_apr
    This.Notifyevent('ControlliFinali')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- gsag_mmp : Deleting
    this.gsag_mmp.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PRSERIAL,"DPSERIAL";
           )
    this.gsag_mmp.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PRE_STAZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_STAZ_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PRE_STAZ_IDX,i_nConn)
      *
      * delete PRE_STAZ
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PRSERIAL',this.w_PRSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsag_apr
    This.Notifyevent('ControlliFinali')
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRE_STAZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_STAZ_IDX,2])
    if i_bUpd
      with this
        if .o_LetturaParAgen<>.w_LetturaParAgen
            .w_LetturaParAgen = i_CodAzi
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.t.)
        if .o_LeggeParAlte<>.w_LeggeParAlte
            .w_LeggeParAlte = i_codazi
          .link_1_4('Full')
        endif
        .DoRTCalc(5,7,.t.)
          .link_1_8('Full')
        .DoRTCalc(9,12,.t.)
        if .o_PRCODRES<>.w_PRCODRES
            .w_PRCODATT = IIF(NOT EMPTY(.w_SERPRE), .w_SERPRE, .w_PRCODATT)
          .link_1_13('Full')
        endif
          .link_1_14('Full')
        .DoRTCalc(15,15,.t.)
        if .o_CODART<>.w_CODART
            .w_PRUNIMIS = .w_UNMIS1
          .link_1_16('Full')
        endif
        if .o_PRCODATT<>.w_PRCODATT
            .w_PRQTAMOV = IIF(.w_TIPART='DE', 0, IIF(.w_PRQTAMOV>0,.w_PRQTAMOV,1))
        endif
        if .o_PRCODATT<>.w_PRCODATT.or. .o_PRNUMPRA<>.w_PRNUMPRA.or. .o_PRCODRES<>.w_PRCODRES
          .Calculate_ZQZFHGDZEY()
        endif
        if .o_PRCODATT<>.w_PRCODATT.or. .o_PRNUMPRA<>.w_PRNUMPRA.or. .o_PRCODLIS<>.w_PRCODLIS.or. .o_PRCODVAL<>.w_PRCODVAL.or. .o_PRUNIMIS<>.w_PRUNIMIS
          .Calculate_PKDJTPPNVI()
        endif
        .DoRTCalc(18,18,.t.)
        if .o_PRCODATT<>.w_PRCODATT.or. .o_PRCODRES<>.w_PRCODRES.or. .o_PRPREZZO<>.w_PRPREZZO
            .w_PRTIPRIG = iCASE(.w_TIPART='FO' and .w_PRPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N',Not empty(.w_TIPRIG),.w_TIPRIG, Not empty(.w_PRTIPRIG),.w_PRTIPRIG,.w_DTIPRIG)
        endif
        if .o_PRCODATT<>.w_PRCODATT.or. .o_PRPREZZO<>.w_PRPREZZO
            .w_PRTIPRI2 = ICASE(.w_TIPART='FO' and .w_PRPREZZO=0 and .w_FLGZER='N', 'N', .w_PRPREZZO<>0, .w_TIPRI2, Not Empty(.w_TIPRI2), .w_TIPRI2, 'N')
        endif
        if .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_PRCODATT<>.w_PRCODATT.or. .o_PRUNIMIS<>.w_PRUNIMIS
            .w_PROREEFF = min(IIF(.w_CHKTEMP='S', INT(.w_DUR_ORE * .w_PRQTAMOV), 0),999)
        endif
        if .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_PRCODATT<>.w_PRCODATT.or. .o_PRUNIMIS<>.w_PRUNIMIS
            .w_PRMINEFF = IIF(.w_CHKTEMP='S', INT(cp_round((.w_DUR_ORE * .w_PRQTAMOV - INT(.w_DUR_ORE * .w_PRQTAMOV)) * 60,0)), 0)
        endif
        if .o_PROREEFF<>.w_PROREEFF.or. .o_PRMINEFF<>.w_PRMINEFF.or. .o_PRCOSUNI<>.w_PRCOSUNI
            .w_PRCOSINT = IIF(.w_LICOSTO>0 and .w_COST_ORA=0 ,.w_PRCOSUNI*.w_PRQTAMOV,(.w_PROREEFF+(.w_PRMINEFF/60))*.w_PRCOSUNI)
        endif
        .DoRTCalc(24,24,.t.)
          .link_1_27('Full')
        .DoRTCalc(26,26,.t.)
        if .o_PRNUMPRA<>.w_PRNUMPRA
            .w_PRCODVAL = iif(Not Empty(.w_CNCODVAL),.w_CNCODVAL,g_perval)
          .link_1_29('Full')
        endif
        if .o_PRNUMPRA<>.w_PRNUMPRA
            .w_PRCODLIS = iif(Not Empty(.w_CNCODLIS),.w_CNCODLIS,.w_CODLIS)
          .link_1_30('Full')
        endif
        .DoRTCalc(29,45,.t.)
          .link_1_57('Full')
          .link_1_58('Full')
          .link_1_59('Full')
          .link_1_60('Full')
        .DoRTCalc(50,50,.t.)
        if .o_PRSERIAL<>.w_PRSERIAL
            .w_RIFBOZ = IIF( .w_PRRIFBOZ='@@@@@@@@@@',Space(10),.w_PRRIFBOZ)
        endif
        .DoRTCalc(52,52,.t.)
        if .o_PRCODATT<>.w_PRCODATT
            .w_PRVOCCOS = .w_VOCRIC
          .link_2_1('Full')
        endif
        .DoRTCalc(54,54,.t.)
        if .o_PRSERIAL<>.w_PRSERIAL
            .w_RIFFAT = IIF( .w_PRRIFFAT='@@@@@@@@@@',Space(10),.w_PRRIFFAT)
        endif
        if .o_PRSERIAL<>.w_PRSERIAL
            .w_RIFPRO = IIF( .w_PRRIFPRO='@@@@@@@@@@',Space(10),.w_PRRIFPRO)
        endif
        if .o_PRSERIAL<>.w_PRSERIAL
            .w_RIFNOT = IIF( .w_PRRIFNOT='@@@@@@@@@@',Space(10),.w_PRRIFNOT)
        endif
        .DoRTCalc(58,72,.t.)
        if .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_PRUNIMIS<>.w_PRUNIMIS
            .w_PRQTAUM1 = CALQTA(.w_PRQTAMOV,.w_PRUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ', .w_UNMIS3, .w_OPERA3, .w_MOLTI3)
        endif
        .DoRTCalc(74,75,.t.)
          .link_1_93('Full')
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .DoRTCalc(77,79,.t.)
        if .o_PRCODRES<>.w_PRCODRES
            .w_DENOMRES = alltrim(.w_COGNRESP)+' '+alltrim(.w_NOMRESP)
        endif
        if .o_PRCODATT<>.w_PRCODATT
          .Calculate_GWLMLXDBXB()
        endif
        .DoRTCalc(81,89,.t.)
          .link_1_130('Full')
        .DoRTCalc(91,100,.t.)
          .link_1_141('Full')
        .DoRTCalc(102,109,.t.)
          .link_1_150('Full')
        .DoRTCalc(111,115,.t.)
        if .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_PRPREZZO<>.w_PRPREZZO
            .w_PRVALRIG = CAVALRIG(.w_PRPREZZO,.w_PRQTAMOV, 0,0,0,0,IIF(.w_NOTIFICA='S', 0, .w_DECTOT))
        endif
        .DoRTCalc(117,128,.t.)
          .link_1_172('Full')
          .link_1_173('Full')
        .DoRTCalc(131,136,.t.)
        if .o_PRSERIAL<>.w_PRSERIAL
            .w_RIFFAA = IIF( .w_PRRIFFAA='@@@@@@@@@@',Space(10),.w_PRRIFFAA)
        endif
        if .o_PRSERIAL<>.w_PRSERIAL
            .w_RIFPRA = IIF( .w_PRRIFPRA='@@@@@@@@@@',Space(10),.w_PRRIFPRA)
        endif
        .DoRTCalc(139,142,.t.)
        if .o_PRCODRES<>.w_PRCODRES
            .w_DATAPREC = Date() - .w_GG_PRE
        endif
        if .o_PRCODRES<>.w_PRCODRES
            .w_DATASUCC = Date() + .w_GG_SUC
        endif
        if .o_PRCODATT<>.w_PRCODATT.or. .o_PRQTAMOV<>.w_PRQTAMOV
          .Calculate_LDKASKAOZX()
        endif
        .DoRTCalc(145,153,.t.)
            .w_CollegaPre = IIF(EMPTY(.w_PRSERAGG) AND .w_PRRIGPRE<>'S' AND Chkprest(.w_PRCODATT,'A-S'),'S','N')
        if .o_PRNUMPRA<>.w_PRNUMPRA
          .Calculate_DGRCNCEERA()
        endif
        if .o_PRCODRES<>.w_PRCODRES.or. .o_PR__DATA<>.w_PR__DATA
          .Calculate_CRUXEZNSYE()
        endif
        .oPgFrm.Page1.oPag.oObj_1_224.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEPRE","i_codazi,w_PRSERIAL")
          .op_PRSERIAL = .w_PRSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_PRNUMPRA<>.w_PRNUMPRA
           cp_AskTableProg(this,i_nConn,"NUPRE","i_codazi,w_PRNUMPRA,w_PRNUMPRE")
          .op_PRNUMPRE = .w_PRNUMPRE
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_PRNUMPRA = .w_PRNUMPRA
      endwith
      this.DoRTCalc(155,176,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_224.Calculate()
    endwith
  return

  proc Calculate_ZQZFHGDZEY()
    with this
          * --- Rilegge tariffe concordate - GSAG_BPK('T')
          GSAG_BPK(this;
              ,'T';
             )
    endwith
  endproc
  proc Calculate_PKDJTPPNVI()
    with this
          * --- Ricalcola prezzo - GSAG_BCD('1')
          gsag_bcd(this;
              ,'1';
             )
    endwith
  endproc
  proc Calculate_GWLMLXDBXB()
    with this
          * --- Valorizza descrizione unit� di misura
          .w_PRUNIMIS = .w_PRUNIMIS
          .link_1_16('Full')
    endwith
  endproc
  proc Calculate_LDKASKAOZX()
    with this
          * --- Ricalcolo durata eff. (ore e minuti)
          .w_PROREEFF = Min(IIF(.w_CHKTEMP='S', INT(.w_DUR_ORE * .w_PRQTAMOV), .w_PROREEFF),999)
          .w_PRMINEFF = IIF(.w_CHKTEMP='S', INT(cp_Round((.w_DUR_ORE * .w_PRQTAMOV - INT(.w_DUR_ORE * .w_PRQTAMOV)) * 60,0)), .w_PRMINEFF)
          .w_PRCOSUNI = IIF(.w_LICOSTO>0 OR (.w_COST_ORA=0 and .w_CHKTEMP<>'S' and .w_TIPRIS$ 'R-P'),.w_LICOSTO,.w_COST_ORA)
          .w_PRCOSINT = IIF(.w_LICOSTO>0 and .w_COST_ORA=0 ,.w_PRCOSUNI*.w_PRQTAMOV,(.w_PROREEFF+(.w_PRMINEFF/60))*.w_PRCOSUNI)
    endwith
  endproc
  proc Calculate_VJUJKLFGEL()
    with this
          * --- LinkRes da Gsag_bck
          .w_PRCODRES = .w_PRCODRES
          .link_1_9('Full')
    endwith
  endproc
  proc Calculate_DGRCNCEERA()
    with this
          * --- Aggiornamento progressivo
          AggProg(this;
             )
    endwith
  endproc
  proc Calculate_CRUXEZNSYE()
    with this
          * --- Ricalcola prezzo - GSAG_BCD('5')
          gsag_bcd(this;
              ,'5';
             )
    endwith
  endproc
  proc Calculate_VCZSDEISXA()
    with this
          * --- Inizializza variabili OLD_
          .w_OLD_PR__DATA = .w_PR__DATA
          .w_OLD_PRCODRES = .w_PRCODRES
          .w_OLD_PRTIPRIG = .w_PRTIPRIG
          .w_OLD_PRTIPRI2 = .w_PRTIPRI2
          .w_OLD_PRNUMPRA = .w_PRNUMPRA
          .w_OLD_PRESTA = .w_PRESTA
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPRNUMPRA_1_7.enabled = this.oPgFrm.Page1.oPag.oPRNUMPRA_1_7.mCond()
    this.oPgFrm.Page1.oPag.oPRCODATT_1_13.enabled = this.oPgFrm.Page1.oPag.oPRCODATT_1_13.mCond()
    this.oPgFrm.Page1.oPag.oPRUNIMIS_1_16.enabled = this.oPgFrm.Page1.oPag.oPRUNIMIS_1_16.mCond()
    this.oPgFrm.Page1.oPag.oPRQTAMOV_1_17.enabled = this.oPgFrm.Page1.oPag.oPRQTAMOV_1_17.mCond()
    this.oPgFrm.Page1.oPag.oPRCODLIS_1_30.enabled = this.oPgFrm.Page1.oPag.oPRCODLIS_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_100.enabled = this.oPgFrm.Page1.oPag.oBtn_1_100.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_126.enabled = this.oPgFrm.Page1.oPag.oBtn_1_126.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_203.enabled = this.oPgFrm.Page1.oPag.oBtn_1_203.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_207.enabled = this.oPgFrm.Page1.oPag.oBtn_1_207.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(g_COAN <> 'S')
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Analitica"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.oPgFrm.Page1.oPag.oLinkPC_1_36.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_36.mHide()
    this.oPgFrm.Page1.oPag.oNUMPRO_1_37.visible=!this.oPgFrm.Page1.oPag.oNUMPRO_1_37.mHide()
    this.oPgFrm.Page1.oPag.oALFPRO_1_38.visible=!this.oPgFrm.Page1.oPag.oALFPRO_1_38.mHide()
    this.oPgFrm.Page1.oPag.oDATPRO_1_39.visible=!this.oPgFrm.Page1.oPag.oDATPRO_1_39.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_40.visible=!this.oPgFrm.Page1.oPag.oBtn_1_40.mHide()
    this.oPgFrm.Page1.oPag.oNUMFAT_1_41.visible=!this.oPgFrm.Page1.oPag.oNUMFAT_1_41.mHide()
    this.oPgFrm.Page1.oPag.oALFFAT_1_42.visible=!this.oPgFrm.Page1.oPag.oALFFAT_1_42.mHide()
    this.oPgFrm.Page1.oPag.oDATFAT_1_43.visible=!this.oPgFrm.Page1.oPag.oDATFAT_1_43.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_44.visible=!this.oPgFrm.Page1.oPag.oBtn_1_44.mHide()
    this.oPgFrm.Page1.oPag.oNUMNOT_1_45.visible=!this.oPgFrm.Page1.oPag.oNUMNOT_1_45.mHide()
    this.oPgFrm.Page1.oPag.oALFNOT_1_46.visible=!this.oPgFrm.Page1.oPag.oALFNOT_1_46.mHide()
    this.oPgFrm.Page1.oPag.oDATNOT_1_47.visible=!this.oPgFrm.Page1.oPag.oDATNOT_1_47.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_48.visible=!this.oPgFrm.Page1.oPag.oBtn_1_48.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_99.visible=!this.oPgFrm.Page1.oPag.oBtn_1_99.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_100.visible=!this.oPgFrm.Page1.oPag.oBtn_1_100.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_105.visible=!this.oPgFrm.Page1.oPag.oStr_1_105.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_107.visible=!this.oPgFrm.Page1.oPag.oStr_1_107.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_109.visible=!this.oPgFrm.Page1.oPag.oStr_1_109.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_126.visible=!this.oPgFrm.Page1.oPag.oBtn_1_126.mHide()
    this.oPgFrm.Page1.oPag.oNUMBOZ_1_166.visible=!this.oPgFrm.Page1.oPag.oNUMBOZ_1_166.mHide()
    this.oPgFrm.Page1.oPag.oALFBOZ_1_167.visible=!this.oPgFrm.Page1.oPag.oALFBOZ_1_167.mHide()
    this.oPgFrm.Page1.oPag.oDATBOZ_1_168.visible=!this.oPgFrm.Page1.oPag.oDATBOZ_1_168.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_169.visible=!this.oPgFrm.Page1.oPag.oBtn_1_169.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_170.visible=!this.oPgFrm.Page1.oPag.oStr_1_170.mHide()
    this.oPgFrm.Page1.oPag.oNUMFAA_1_174.visible=!this.oPgFrm.Page1.oPag.oNUMFAA_1_174.mHide()
    this.oPgFrm.Page1.oPag.oALFFAA_1_175.visible=!this.oPgFrm.Page1.oPag.oALFFAA_1_175.mHide()
    this.oPgFrm.Page1.oPag.oDATFAA_1_176.visible=!this.oPgFrm.Page1.oPag.oDATFAA_1_176.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_177.visible=!this.oPgFrm.Page1.oPag.oBtn_1_177.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_178.visible=!this.oPgFrm.Page1.oPag.oStr_1_178.mHide()
    this.oPgFrm.Page1.oPag.oNUMPRA_1_180.visible=!this.oPgFrm.Page1.oPag.oNUMPRA_1_180.mHide()
    this.oPgFrm.Page1.oPag.oALFPRA_1_181.visible=!this.oPgFrm.Page1.oPag.oALFPRA_1_181.mHide()
    this.oPgFrm.Page1.oPag.oDATPRA_1_182.visible=!this.oPgFrm.Page1.oPag.oDATPRA_1_182.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_183.visible=!this.oPgFrm.Page1.oPag.oBtn_1_183.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_184.visible=!this.oPgFrm.Page1.oPag.oStr_1_184.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_195.visible=!this.oPgFrm.Page1.oPag.oStr_1_195.mHide()
    this.oPgFrm.Page1.oPag.oPRSERIAL_1_196.visible=!this.oPgFrm.Page1.oPag.oPRSERIAL_1_196.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_203.visible=!this.oPgFrm.Page1.oPag.oBtn_1_203.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_207.visible=!this.oPgFrm.Page1.oPag.oBtn_1_207.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_apr
    if cevent='w_PRPREZZO Changed' and (This.w_TIPART='FO' and This.w_PRRIGPRE='S' and This.w_PRPREZZO=0)
       Ah_errormsg("Attenzione, riga collegata! Impossibile azzerare la tariffa")
       This.w_PRPREZZO=This.o_PRPREZZO
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_96.Event(cEvent)
        if lower(cEvent)==lower("LinkRes")
          .Calculate_VJUJKLFGEL()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_224.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_VCZSDEISXA()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LetturaParAgen
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LetturaParAgen) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LetturaParAgen)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACODLIS,PACAUPRE,PAFLPRES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LetturaParAgen);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LetturaParAgen)
            select PACODAZI,PACODLIS,PACAUPRE,PAFLPRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LetturaParAgen = NVL(_Link_.PACODAZI,space(5))
      this.w_CODLIS = NVL(_Link_.PACODLIS,space(5))
      this.w_PACAUPRE = NVL(_Link_.PACAUPRE,space(20))
      this.w_FLPRES = NVL(_Link_.PAFLPRES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LetturaParAgen = space(5)
      endif
      this.w_CODLIS = space(5)
      this.w_PACAUPRE = space(20)
      this.w_FLPRES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LetturaParAgen Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LeggeParAlte
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LeggeParAlte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LeggeParAlte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAGENPRE,PACAUPRE,PAFLGZER";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LeggeParAlte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LeggeParAlte)
            select PACODAZI,PAGENPRE,PACAUPRE,PAFLGZER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LeggeParAlte = NVL(_Link_.PACODAZI,space(5))
      this.w_GENPRE = NVL(_Link_.PAGENPRE,space(1))
      this.w_CAUDOC = NVL(_Link_.PACAUPRE,space(5))
      this.w_FLGZER = NVL(_Link_.PAFLGZER,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LeggeParAlte = space(5)
      endif
      this.w_GENPRE = space(1)
      this.w_CAUDOC = space(5)
      this.w_FLGZER = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LeggeParAlte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRNUMPRA
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRNUMPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_PRNUMPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNFLAPON,CN__ENTE,CNIMPORT,CNUFFICI,CNFLVALO,CNFLDIND,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNTARTEM,CNTARCON,CNCONTRA,CNCODLIS,CNCODVAL,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNTIPPRA,CNFLVMLQ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_PRNUMPRA))
          select CNCODCAN,CNDESCAN,CNFLAPON,CN__ENTE,CNIMPORT,CNUFFICI,CNFLVALO,CNFLDIND,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNTARTEM,CNTARCON,CNCONTRA,CNCODLIS,CNCODVAL,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNTIPPRA,CNFLVMLQ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRNUMPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_PRNUMPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNFLAPON,CN__ENTE,CNIMPORT,CNUFFICI,CNFLVALO,CNFLDIND,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNTARTEM,CNTARCON,CNCONTRA,CNCODLIS,CNCODVAL,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNTIPPRA,CNFLVMLQ";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_PRNUMPRA)+"%");

            select CNCODCAN,CNDESCAN,CNFLAPON,CN__ENTE,CNIMPORT,CNUFFICI,CNFLVALO,CNFLDIND,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNTARTEM,CNTARCON,CNCONTRA,CNCODLIS,CNCODVAL,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNTIPPRA,CNFLVMLQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRNUMPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oPRNUMPRA_1_7'),i_cWhere,'GSPR_ACN',"Fascicolo pratiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNFLAPON,CN__ENTE,CNIMPORT,CNUFFICI,CNFLVALO,CNFLDIND,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNTARTEM,CNTARCON,CNCONTRA,CNCODLIS,CNCODVAL,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNTIPPRA,CNFLVMLQ";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNFLAPON,CN__ENTE,CNIMPORT,CNUFFICI,CNFLVALO,CNFLDIND,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNTARTEM,CNTARCON,CNCONTRA,CNCODLIS,CNCODVAL,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNTIPPRA,CNFLVMLQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRNUMPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNFLAPON,CN__ENTE,CNIMPORT,CNUFFICI,CNFLVALO,CNFLDIND,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNTARTEM,CNTARCON,CNCONTRA,CNCODLIS,CNCODVAL,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNTIPPRA,CNFLVMLQ";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_PRNUMPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_PRNUMPRA)
            select CNCODCAN,CNDESCAN,CNFLAPON,CN__ENTE,CNIMPORT,CNUFFICI,CNFLVALO,CNFLDIND,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CNTARTEM,CNTARCON,CNCONTRA,CNCODLIS,CNCODVAL,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNTIPPRA,CNFLVMLQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRNUMPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(100))
      this.w_CNFLAPON = NVL(_Link_.CNFLAPON,space(10))
      this.w_CN__ENTE = NVL(_Link_.CN__ENTE,space(10))
      this.w_CNIMPORT = NVL(_Link_.CNIMPORT,0)
      this.w_CNUFFICI = NVL(_Link_.CNUFFICI,space(10))
      this.w_CNFLVALO = NVL(_Link_.CNFLVALO,space(1))
      this.w_CNFLVALO1 = NVL(_Link_.CNFLDIND,space(1))
      this.w_CNCALDIR = NVL(_Link_.CNCALDIR,space(1))
      this.w_CNCOECAL = NVL(_Link_.CNCOECAL,0)
      this.w_CNPARASS = NVL(_Link_.CNPARASS,0)
      this.w_CNFLAMPA = NVL(_Link_.CNFLAMPA,space(1))
      this.w_CNTARTEM = NVL(_Link_.CNTARTEM,space(1))
      this.w_CNTARCON = NVL(_Link_.CNTARCON,0)
      this.w_CONTRACN = NVL(_Link_.CNCONTRA,space(5))
      this.w_CNCODLIS = NVL(_Link_.CNCODLIS,space(5))
      this.w_CNCODVAL = NVL(_Link_.CNCODVAL,space(3))
      this.w_CNMATOBB = NVL(_Link_.CNMATOBB,space(1))
      this.w_CNASSCTP = NVL(_Link_.CNASSCTP,space(1))
      this.w_CNCOMPLX = NVL(_Link_.CNCOMPLX,space(1))
      this.w_CNPROFMT = NVL(_Link_.CNPROFMT,space(1))
      this.w_CNESIPOS = NVL(_Link_.CNESIPOS,space(1))
      this.w_CNPERPLX = NVL(_Link_.CNPERPLX,0)
      this.w_CNPERPOS = NVL(_Link_.CNPERPOS,0)
      this.w_DETIPPRA = NVL(_Link_.CNTIPPRA,space(10))
      this.w_CNFLVMLQ = NVL(_Link_.CNFLVMLQ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PRNUMPRA = space(15)
      endif
      this.w_DESCAN = space(100)
      this.w_CNFLAPON = space(10)
      this.w_CN__ENTE = space(10)
      this.w_CNIMPORT = 0
      this.w_CNUFFICI = space(10)
      this.w_CNFLVALO = space(1)
      this.w_CNFLVALO1 = space(1)
      this.w_CNCALDIR = space(1)
      this.w_CNCOECAL = 0
      this.w_CNPARASS = 0
      this.w_CNFLAMPA = space(1)
      this.w_CNTARTEM = space(1)
      this.w_CNTARCON = 0
      this.w_CONTRACN = space(5)
      this.w_CNCODLIS = space(5)
      this.w_CNCODVAL = space(3)
      this.w_CNMATOBB = space(1)
      this.w_CNASSCTP = space(1)
      this.w_CNCOMPLX = space(1)
      this.w_CNPROFMT = space(1)
      this.w_CNESIPOS = space(1)
      this.w_CNPERPLX = 0
      this.w_CNPERPOS = 0
      this.w_DETIPPRA = space(10)
      this.w_CNFLVMLQ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRNUMPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 26 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+26<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.CNCODCAN as CNCODCAN107"+ ",link_1_7.CNDESCAN as CNDESCAN107"+ ",link_1_7.CNFLAPON as CNFLAPON107"+ ",link_1_7.CN__ENTE as CN__ENTE107"+ ",link_1_7.CNIMPORT as CNIMPORT107"+ ",link_1_7.CNUFFICI as CNUFFICI107"+ ",link_1_7.CNFLVALO as CNFLVALO107"+ ",link_1_7.CNFLDIND as CNFLDIND107"+ ",link_1_7.CNCALDIR as CNCALDIR107"+ ",link_1_7.CNCOECAL as CNCOECAL107"+ ",link_1_7.CNPARASS as CNPARASS107"+ ",link_1_7.CNFLAMPA as CNFLAMPA107"+ ",link_1_7.CNTARTEM as CNTARTEM107"+ ",link_1_7.CNTARCON as CNTARCON107"+ ",link_1_7.CNCONTRA as CNCONTRA107"+ ",link_1_7.CNCODLIS as CNCODLIS107"+ ",link_1_7.CNCODVAL as CNCODVAL107"+ ",link_1_7.CNMATOBB as CNMATOBB107"+ ",link_1_7.CNASSCTP as CNASSCTP107"+ ",link_1_7.CNCOMPLX as CNCOMPLX107"+ ",link_1_7.CNPROFMT as CNPROFMT107"+ ",link_1_7.CNESIPOS as CNESIPOS107"+ ",link_1_7.CNPERPLX as CNPERPLX107"+ ",link_1_7.CNPERPOS as CNPERPOS107"+ ",link_1_7.CNTIPPRA as CNTIPPRA107"+ ",link_1_7.CNFLVMLQ as CNFLVMLQ107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on PRE_STAZ.PRNUMPRA=link_1_7.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+26
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and PRE_STAZ.PRNUMPRA=link_1_7.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+26
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PACAUPRE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACAUPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACAUPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PACAUPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PACAUPRE)
            select CACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACAUPRE = NVL(_Link_.CACODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PACAUPRE = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACAUPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRCODRES
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODRES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_PRCODRES)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPCOSORA,DPCTRPRE,DPGG_PRE,DPGG_SUC,DPTIPRIS,DPTIPRIG,DPSERPRE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_PRCODRES))
          select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPCOSORA,DPCTRPRE,DPGG_PRE,DPGG_SUC,DPTIPRIS,DPTIPRIG,DPSERPRE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODRES)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCODRES) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oPRCODRES_1_9'),i_cWhere,'GSAR_BDZ',"Responsabili",'GSAG_KPR.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPCOSORA,DPCTRPRE,DPGG_PRE,DPGG_SUC,DPTIPRIS,DPTIPRIG,DPSERPRE";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPCOSORA,DPCTRPRE,DPGG_PRE,DPGG_SUC,DPTIPRIS,DPTIPRIG,DPSERPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODRES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPCOSORA,DPCTRPRE,DPGG_PRE,DPGG_SUC,DPTIPRIS,DPTIPRIG,DPSERPRE";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_PRCODRES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_PRCODRES)
            select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPCOSORA,DPCTRPRE,DPGG_PRE,DPGG_SUC,DPTIPRIS,DPTIPRIG,DPSERPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODRES = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNRESP = NVL(_Link_.DPCOGNOM,space(10))
      this.w_NOMRESP = NVL(_Link_.DPNOME,space(10))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
      this.w_COST_ORA = NVL(_Link_.DPCOSORA,0)
      this.w_PRCOSUNI = NVL(_Link_.DPCOSORA,0)
      this.w_CHKDATAPRE = NVL(_Link_.DPCTRPRE,space(1))
      this.w_GG_PRE = NVL(_Link_.DPGG_PRE,0)
      this.w_GG_SUC = NVL(_Link_.DPGG_SUC,0)
      this.w_TIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DTIPRIG = NVL(_Link_.DPTIPRIG,space(1))
      this.w_SERPRE = NVL(_Link_.DPSERPRE,space(41))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODRES = space(5)
      endif
      this.w_COGNRESP = space(10)
      this.w_NOMRESP = space(10)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_COST_ORA = 0
      this.w_PRCOSUNI = 0
      this.w_CHKDATAPRE = space(1)
      this.w_GG_PRE = 0
      this.w_GG_SUC = 0
      this.w_TIPRIS = space(1)
      this.w_DTIPRIG = space(1)
      this.w_SERPRE = space(41)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST) AND .w_TIPRIS='P'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore inesistente o obsoleto")
        endif
        this.w_PRCODRES = space(5)
        this.w_COGNRESP = space(10)
        this.w_NOMRESP = space(10)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_COST_ORA = 0
        this.w_PRCOSUNI = 0
        this.w_CHKDATAPRE = space(1)
        this.w_GG_PRE = 0
        this.w_GG_SUC = 0
        this.w_TIPRIS = space(1)
        this.w_DTIPRIG = space(1)
        this.w_SERPRE = space(41)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODRES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 12 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+12<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.DPCODICE as DPCODICE109"+ ",link_1_9.DPCOGNOM as DPCOGNOM109"+ ",link_1_9.DPNOME as DPNOME109"+ ",link_1_9.DPDTOBSO as DPDTOBSO109"+ ",link_1_9.DPCOSORA as DPCOSORA109"+ ",link_1_9.DPCOSORA as DPCOSORA109"+ ",link_1_9.DPCTRPRE as DPCTRPRE109"+ ",link_1_9.DPGG_PRE as DPGG_PRE109"+ ",link_1_9.DPGG_SUC as DPGG_SUC109"+ ",link_1_9.DPTIPRIS as DPTIPRIS109"+ ",link_1_9.DPTIPRIG as DPTIPRIG109"+ ",link_1_9.DPSERPRE as DPSERPRE109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on PRE_STAZ.PRCODRES=link_1_9.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+12
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and PRE_STAZ.PRCODRES=link_1_9.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+12
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCODATT
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PRCODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESSUP,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PRCODATT))
          select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESSUP,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_PRCODATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESSUP,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_PRCODATT)+"%");

            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESSUP,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRCODATT) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPRCODATT_1_13'),i_cWhere,'GSMA_BZA',"Prestazioni",'INSPRE.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESSUP,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESSUP,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESSUP,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PRCODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PRCODATT)
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESSUP,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODATT = NVL(_Link_.CACODICE,space(20))
      this.w_PRDESPRE = NVL(_Link_.CADESART,space(40))
      this.w_CODART = NVL(_Link_.CACODART,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_PRDESAGG = NVL(_Link_.CADESSUP,space(0))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODATT = space(20)
      endif
      this.w_PRDESPRE = space(40)
      this.w_CODART = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_PRDESAGG = space(0)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore obsoleto")
        endif
        this.w_PRCODATT = space(20)
        this.w_PRDESPRE = space(40)
        this.w_CODART = space(20)
        this.w_UNMIS3 = space(3)
        this.w_OPERA3 = space(1)
        this.w_MOLTI3 = 0
        this.w_PRDESAGG = space(0)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.CACODICE as CACODICE113"+ ",link_1_13.CADESART as CADESART113"+ ",link_1_13.CACODART as CACODART113"+ ",link_1_13.CAUNIMIS as CAUNIMIS113"+ ",link_1_13.CAOPERAT as CAOPERAT113"+ ",link_1_13.CAMOLTIP as CAMOLTIP113"+ ",link_1_13.CADESSUP as CADESSUP113"+ ",link_1_13.CADTOBSO as CADTOBSO113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on PRE_STAZ.PRCODATT=link_1_13.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and PRE_STAZ.PRCODATT=link_1_13.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARPRESTA,AROPERAT,ARUNMIS2,ARMOLTIP,ARFLUSEP,ARUNMIS1,ARSTACOD,ARTIPART,ARFLSERG,ARTIPRIG,ARTIPRI2,ARVOCRIC";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARPRESTA,AROPERAT,ARUNMIS2,ARMOLTIP,ARFLUSEP,ARUNMIS1,ARSTACOD,ARTIPART,ARFLSERG,ARTIPRIG,ARTIPRI2,ARVOCRIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_PRESTA = NVL(_Link_.ARPRESTA,space(1))
      this.w_PRCODART = NVL(_Link_.ARCODART,space(20))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_NOTIFICA = NVL(_Link_.ARSTACOD,space(1))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_TIPRIG = NVL(_Link_.ARTIPRIG,space(1))
      this.w_TIPRI2 = NVL(_Link_.ARTIPRI2,space(1))
      this.w_VOCRIC = NVL(_Link_.ARVOCRIC,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_PRESTA = space(1)
      this.w_PRCODART = space(20)
      this.w_OPERAT = space(1)
      this.w_UNMIS2 = space(3)
      this.w_MOLTIP = 0
      this.w_FLUSEP = space(1)
      this.w_UNMIS1 = space(3)
      this.w_NOTIFICA = space(1)
      this.w_TIPART = space(2)
      this.w_FLSERG = space(1)
      this.w_TIPRIG = space(1)
      this.w_TIPRI2 = space(1)
      this.w_VOCRIC = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRUNIMIS
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_PRUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_PRUNIMIS))
          select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oPRUNIMIS_1_16'),i_cWhere,'',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_PRUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_PRUNIMIS)
            select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_UMDESCRI = NVL(_Link_.UMDESCRI,space(35))
      this.w_CHKTEMP = NVL(_Link_.UMFLTEMP,space(1))
      this.w_DUR_ORE = NVL(_Link_.UMDURORE,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRUNIMIS = space(3)
      endif
      this.w_UMDESCRI = space(35)
      this.w_CHKTEMP = space(1)
      this.w_DUR_ORE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSERG='S' OR .w_PRUNIMIS=.w_UNMIS1 OR .w_PRUNIMIS=.w_UNMIS2 OR .w_PRUNIMIS=.w_UNMIS3
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PRUNIMIS = space(3)
        this.w_UMDESCRI = space(35)
        this.w_CHKTEMP = space(1)
        this.w_DUR_ORE = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.UMCODICE as UMCODICE116"+ ",link_1_16.UMDESCRI as UMDESCRI116"+ ",link_1_16.UMFLTEMP as UMFLTEMP116"+ ",link_1_16.UMDURORE as UMDURORE116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on PRE_STAZ.PRUNIMIS=link_1_16.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and PRE_STAZ.PRUNIMIS=link_1_16.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCODNOM
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_PRCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_PRCODNOM)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_NODESCR = NVL(_Link_.NODESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODNOM = space(15)
      endif
      this.w_NODESCR = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_27(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFF_NOMI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_27.NOCODICE as NOCODICE127"+ ",link_1_27.NODESCRI as NODESCRI127"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_27 on PRE_STAZ.PRCODNOM=link_1_27.NOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_27"
          i_cKey=i_cKey+'+" and PRE_STAZ.PRCODNOM=link_1_27.NOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCODVAL
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PRCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PRCODVAL)
            select VACODVAL,VADECTOT,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRCODVAL = space(3)
      endif
      this.w_DECTOT = 0
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_29.VACODVAL as VACODVAL129"+ ",link_1_29.VADECTOT as VADECTOT129"+ ",link_1_29.VADECUNI as VADECUNI129"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_29 on PRE_STAZ.PRCODVAL=link_1_29.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_29"
          i_cKey=i_cKey+'+" and PRE_STAZ.PRCODVAL=link_1_29.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCODLIS
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_ali',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_PRCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_PRCODLIS))
          select LSCODLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oPRCODLIS_1_30'),i_cWhere,'gsar_ali',"Listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_PRCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_PRCODLIS)
            select LSCODLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODLIS = NVL(_Link_.LSCODLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODLIS = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRRIFFAT
  func Link_1_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRRIFFAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRRIFFAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_PRRIFFAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_PRRIFFAT)
            select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_PRRIFFAT = NVL(_Link_.MVSERIAL,space(10))
      this.w_ALFFAT = NVL(_Link_.MVALFDOC,space(10))
      this.w_NUMFAT = NVL(_Link_.MVNUMDOC,0)
      this.w_DATFAT = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
    else
      this.w_ALFFAT = space(10)
      this.w_NUMFAT = 0
      this.w_DATFAT = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRRIFFAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRRIFPRO
  func Link_1_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRRIFPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRRIFPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_PRRIFPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_PRRIFPRO)
            select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_PRRIFPRO = NVL(_Link_.MVSERIAL,space(10))
      this.w_ALFPRO = NVL(_Link_.MVALFDOC,space(10))
      this.w_NUMPRO = NVL(_Link_.MVNUMDOC,0)
      this.w_DATPRO = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
    else
      this.w_ALFPRO = space(10)
      this.w_NUMPRO = 0
      this.w_DATPRO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRRIFPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRRIFNOT
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRRIFNOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRRIFNOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_PRRIFNOT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_PRRIFNOT)
            select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_PRRIFNOT = NVL(_Link_.MVSERIAL,space(10))
      this.w_ALFNOT = NVL(_Link_.MVALFDOC,space(10))
      this.w_NUMNOT = NVL(_Link_.MVNUMDOC,0)
      this.w_DATNOT = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
    else
      this.w_ALFNOT = space(10)
      this.w_NUMNOT = 0
      this.w_DATNOT = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRRIFNOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRRIFBOZ
  func Link_1_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRRIFBOZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRRIFBOZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_PRRIFBOZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_PRRIFBOZ)
            select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_PRRIFBOZ = NVL(_Link_.MVSERIAL,space(20))
      this.w_ALFBOZ = NVL(_Link_.MVALFDOC,space(10))
      this.w_NUMBOZ = NVL(_Link_.MVNUMDOC,0)
      this.w_DATBOZ = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
    else
      this.w_ALFBOZ = space(10)
      this.w_NUMBOZ = 0
      this.w_DATBOZ = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRRIFBOZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRVOCCOS
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRVOCCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_PRVOCCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_PRVOCCOS))
          select VCCODICE,VCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRVOCCOS)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRVOCCOS) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oPRVOCCOS_2_1'),i_cWhere,'GSCA_AVC',"Voce di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRVOCCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_PRVOCCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_PRVOCCOS)
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRVOCCOS = NVL(_Link_.VCCODICE,space(15))
      this.w_VOCCOSDE = NVL(_Link_.VCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PRVOCCOS = space(15)
      endif
      this.w_VOCCOSDE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRVOCCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.VCCODICE as VCCODICE201"+ ",link_2_1.VCDESCRI as VCDESCRI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on PRE_STAZ.PRVOCCOS=link_2_1.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and PRE_STAZ.PRVOCCOS=link_2_1.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCENCOS
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PRCENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PRCENCOS))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPRCENCOS_2_2'),i_cWhere,'GSCA_ACC',"Centro di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PRCENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PRCENCOS)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_CENCOSDE = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PRCENCOS = space(15)
      endif
      this.w_CENCOSDE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CC_CONTO as CC_CONTO202"+ ",link_2_2.CCDESPIA as CCDESPIA202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on PRE_STAZ.PRCENCOS=link_2_2.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and PRE_STAZ.PRCENCOS=link_2_2.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRROWPRO
  func Link_1_93(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRROWPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRROWPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_PRROWPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_PRROWPRO)
            select MVSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRROWPRO = NVL(_Link_.MVSERIAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRROWPRO = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRROWPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CN__ENTE
  func Link_1_130(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CN__ENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CN__ENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EP__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_CN__ENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_CN__ENTE)
            select EPCODICE,EP__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CN__ENTE = NVL(_Link_.EPCODICE,space(10))
      this.w_TIPOENTE = NVL(_Link_.EP__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CN__ENTE = space(10)
      endif
      this.w_TIPOENTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CN__ENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTRACN
  func Link_1_141(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_CONT_IDX,3]
    i_lTable = "PRA_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_CONT_IDX,2], .t., this.PRA_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTRACN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTRACN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPCODCON,CPTIPCON,CPLISCOL,CPSTATUS";
                   +" from "+i_cTable+" "+i_lTable+" where CPCODCON="+cp_ToStrODBC(this.w_CONTRACN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPCODCON',this.w_CONTRACN)
            select CPCODCON,CPTIPCON,CPLISCOL,CPSTATUS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTRACN = NVL(_Link_.CPCODCON,space(5))
      this.w_TIPCONCO = NVL(_Link_.CPTIPCON,space(1))
      this.w_LISCOLCO = NVL(_Link_.CPLISCOL,space(5))
      this.w_STATUSCO = NVL(_Link_.CPSTATUS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONTRACN = space(5)
      endif
      this.w_TIPCONCO = space(1)
      this.w_LISCOLCO = space(5)
      this.w_STATUSCO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CPCODCON,1)
      cp_ShowWarn(i_cKey,this.PRA_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTRACN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_1_150(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ1 = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_FLFRAZ1 = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRRIFFAA
  func Link_1_172(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRRIFFAA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRRIFFAA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_PRRIFFAA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_PRRIFFAA)
            select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_PRRIFFAA = NVL(_Link_.MVSERIAL,space(10))
      this.w_ALFFAA = NVL(_Link_.MVALFDOC,space(10))
      this.w_NUMFAA = NVL(_Link_.MVNUMDOC,0)
      this.w_DATFAA = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
    else
      this.w_ALFFAA = space(10)
      this.w_NUMFAA = 0
      this.w_DATFAA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRRIFFAA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRRIFPRA
  func Link_1_173(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRRIFPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRRIFPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_PRRIFPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_PRRIFPRA)
            select MVSERIAL,MVALFDOC,MVNUMDOC,MVDATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_PRRIFPRA = NVL(_Link_.MVSERIAL,space(10))
      this.w_ALFPRA = NVL(_Link_.MVALFDOC,space(10))
      this.w_NUMPRA = NVL(_Link_.MVNUMDOC,0)
      this.w_DATPRA = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
    else
      this.w_ALFPRA = space(10)
      this.w_NUMPRA = 0
      this.w_DATPRA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRRIFPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPRNUMPRA_1_7.value==this.w_PRNUMPRA)
      this.oPgFrm.Page1.oPag.oPRNUMPRA_1_7.value=this.w_PRNUMPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODRES_1_9.value==this.w_PRCODRES)
      this.oPgFrm.Page1.oPag.oPRCODRES_1_9.value=this.w_PRCODRES
    endif
    if not(this.oPgFrm.Page1.oPag.oPR__DATA_1_11.value==this.w_PR__DATA)
      this.oPgFrm.Page1.oPag.oPR__DATA_1_11.value=this.w_PR__DATA
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODATT_1_13.value==this.w_PRCODATT)
      this.oPgFrm.Page1.oPag.oPRCODATT_1_13.value=this.w_PRCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDESPRE_1_15.value==this.w_PRDESPRE)
      this.oPgFrm.Page1.oPag.oPRDESPRE_1_15.value=this.w_PRDESPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oPRUNIMIS_1_16.value==this.w_PRUNIMIS)
      this.oPgFrm.Page1.oPag.oPRUNIMIS_1_16.value=this.w_PRUNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oPRQTAMOV_1_17.value==this.w_PRQTAMOV)
      this.oPgFrm.Page1.oPag.oPRQTAMOV_1_17.value=this.w_PRQTAMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPREZZO_1_18.value==this.w_PRPREZZO)
      this.oPgFrm.Page1.oPag.oPRPREZZO_1_18.value=this.w_PRPREZZO
    endif
    if not(this.oPgFrm.Page1.oPag.oPRTIPRIG_1_21.RadioValue()==this.w_PRTIPRIG)
      this.oPgFrm.Page1.oPag.oPRTIPRIG_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRTIPRI2_1_22.RadioValue()==this.w_PRTIPRI2)
      this.oPgFrm.Page1.oPag.oPRTIPRI2_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROREEFF_1_23.value==this.w_PROREEFF)
      this.oPgFrm.Page1.oPag.oPROREEFF_1_23.value=this.w_PROREEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oPRMINEFF_1_24.value==this.w_PRMINEFF)
      this.oPgFrm.Page1.oPag.oPRMINEFF_1_24.value=this.w_PRMINEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCOSINT_1_25.value==this.w_PRCOSINT)
      this.oPgFrm.Page1.oPag.oPRCOSINT_1_25.value=this.w_PRCOSINT
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDESAGG_1_26.value==this.w_PRDESAGG)
      this.oPgFrm.Page1.oPag.oPRDESAGG_1_26.value=this.w_PRDESAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODNOM_1_27.value==this.w_PRCODNOM)
      this.oPgFrm.Page1.oPag.oPRCODNOM_1_27.value=this.w_PRCODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oPRNUMPRE_1_28.value==this.w_PRNUMPRE)
      this.oPgFrm.Page1.oPag.oPRNUMPRE_1_28.value=this.w_PRNUMPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODVAL_1_29.value==this.w_PRCODVAL)
      this.oPgFrm.Page1.oPag.oPRCODVAL_1_29.value=this.w_PRCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODLIS_1_30.value==this.w_PRCODLIS)
      this.oPgFrm.Page1.oPag.oPRCODLIS_1_30.value=this.w_PRCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oUMDESCRI_1_31.value==this.w_UMDESCRI)
      this.oPgFrm.Page1.oPag.oUMDESCRI_1_31.value=this.w_UMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPREMIN_1_33.value==this.w_PRPREMIN)
      this.oPgFrm.Page1.oPag.oPRPREMIN_1_33.value=this.w_PRPREMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPREMAX_1_34.value==this.w_PRPREMAX)
      this.oPgFrm.Page1.oPag.oPRPREMAX_1_34.value=this.w_PRPREMAX
    endif
    if not(this.oPgFrm.Page1.oPag.oPRGAZUFF_1_35.value==this.w_PRGAZUFF)
      this.oPgFrm.Page1.oPag.oPRGAZUFF_1_35.value=this.w_PRGAZUFF
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPRO_1_37.value==this.w_NUMPRO)
      this.oPgFrm.Page1.oPag.oNUMPRO_1_37.value=this.w_NUMPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oALFPRO_1_38.value==this.w_ALFPRO)
      this.oPgFrm.Page1.oPag.oALFPRO_1_38.value=this.w_ALFPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATPRO_1_39.value==this.w_DATPRO)
      this.oPgFrm.Page1.oPag.oDATPRO_1_39.value=this.w_DATPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFAT_1_41.value==this.w_NUMFAT)
      this.oPgFrm.Page1.oPag.oNUMFAT_1_41.value=this.w_NUMFAT
    endif
    if not(this.oPgFrm.Page1.oPag.oALFFAT_1_42.value==this.w_ALFFAT)
      this.oPgFrm.Page1.oPag.oALFFAT_1_42.value=this.w_ALFFAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFAT_1_43.value==this.w_DATFAT)
      this.oPgFrm.Page1.oPag.oDATFAT_1_43.value=this.w_DATFAT
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMNOT_1_45.value==this.w_NUMNOT)
      this.oPgFrm.Page1.oPag.oNUMNOT_1_45.value=this.w_NUMNOT
    endif
    if not(this.oPgFrm.Page1.oPag.oALFNOT_1_46.value==this.w_ALFNOT)
      this.oPgFrm.Page1.oPag.oALFNOT_1_46.value=this.w_ALFNOT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATNOT_1_47.value==this.w_DATNOT)
      this.oPgFrm.Page1.oPag.oDATNOT_1_47.value=this.w_DATNOT
    endif
    if not(this.oPgFrm.Page2.oPag.oPRVOCCOS_2_1.value==this.w_PRVOCCOS)
      this.oPgFrm.Page2.oPag.oPRVOCCOS_2_1.value=this.w_PRVOCCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oPRCENCOS_2_2.value==this.w_PRCENCOS)
      this.oPgFrm.Page2.oPag.oPRCENCOS_2_2.value=this.w_PRCENCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oPRATTIVI_2_5.value==this.w_PRATTIVI)
      this.oPgFrm.Page2.oPag.oPRATTIVI_2_5.value=this.w_PRATTIVI
    endif
    if not(this.oPgFrm.Page2.oPag.oPR_SEGNO_2_6.RadioValue()==this.w_PR_SEGNO)
      this.oPgFrm.Page2.oPag.oPR_SEGNO_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRINICOM_2_9.value==this.w_PRINICOM)
      this.oPgFrm.Page2.oPag.oPRINICOM_2_9.value=this.w_PRINICOM
    endif
    if not(this.oPgFrm.Page2.oPag.oPRFINCOM_2_11.value==this.w_PRFINCOM)
      this.oPgFrm.Page2.oPag.oPRFINCOM_2_11.value=this.w_PRFINCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_86.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_86.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oNODESCR_1_88.value==this.w_NODESCR)
      this.oPgFrm.Page1.oPag.oNODESCR_1_88.value=this.w_NODESCR
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOMRES_1_120.value==this.w_DENOMRES)
      this.oPgFrm.Page1.oPag.oDENOMRES_1_120.value=this.w_DENOMRES
    endif
    if not(this.oPgFrm.Page2.oPag.oVOCCOSDE_2_15.value==this.w_VOCCOSDE)
      this.oPgFrm.Page2.oPag.oVOCCOSDE_2_15.value=this.w_VOCCOSDE
    endif
    if not(this.oPgFrm.Page2.oPag.oCENCOSDE_2_16.value==this.w_CENCOSDE)
      this.oPgFrm.Page2.oPag.oCENCOSDE_2_16.value=this.w_CENCOSDE
    endif
    if not(this.oPgFrm.Page1.oPag.oPRVALRIG_1_156.value==this.w_PRVALRIG)
      this.oPgFrm.Page1.oPag.oPRVALRIG_1_156.value=this.w_PRVALRIG
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMBOZ_1_166.value==this.w_NUMBOZ)
      this.oPgFrm.Page1.oPag.oNUMBOZ_1_166.value=this.w_NUMBOZ
    endif
    if not(this.oPgFrm.Page1.oPag.oALFBOZ_1_167.value==this.w_ALFBOZ)
      this.oPgFrm.Page1.oPag.oALFBOZ_1_167.value=this.w_ALFBOZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDATBOZ_1_168.value==this.w_DATBOZ)
      this.oPgFrm.Page1.oPag.oDATBOZ_1_168.value=this.w_DATBOZ
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFAA_1_174.value==this.w_NUMFAA)
      this.oPgFrm.Page1.oPag.oNUMFAA_1_174.value=this.w_NUMFAA
    endif
    if not(this.oPgFrm.Page1.oPag.oALFFAA_1_175.value==this.w_ALFFAA)
      this.oPgFrm.Page1.oPag.oALFFAA_1_175.value=this.w_ALFFAA
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFAA_1_176.value==this.w_DATFAA)
      this.oPgFrm.Page1.oPag.oDATFAA_1_176.value=this.w_DATFAA
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPRA_1_180.value==this.w_NUMPRA)
      this.oPgFrm.Page1.oPag.oNUMPRA_1_180.value=this.w_NUMPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oALFPRA_1_181.value==this.w_ALFPRA)
      this.oPgFrm.Page1.oPag.oALFPRA_1_181.value=this.w_ALFPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDATPRA_1_182.value==this.w_DATPRA)
      this.oPgFrm.Page1.oPag.oDATPRA_1_182.value=this.w_DATPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPRSERIAL_1_196.value==this.w_PRSERIAL)
      this.oPgFrm.Page1.oPag.oPRSERIAL_1_196.value=this.w_PRSERIAL
    endif
    cp_SetControlsValueExtFlds(this,'PRE_STAZ')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_CNPARASS <=1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Pratica con pi� parti in assistenza, per caricare le prestazioni utilizzare l'inserimento provvisorio delle prestazioni")
          case   (empty(.w_PRNUMPRA))  and (( ! .Close OR .w_EditCodPra or Empty(.w_PRNUMPRA)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRNUMPRA_1_7.SetFocus()
            i_bnoObbl = !empty(.w_PRNUMPRA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PRCODRES)) or not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST) AND .w_TIPRIS='P'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRCODRES_1_9.SetFocus()
            i_bnoObbl = !empty(.w_PRCODRES)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore inesistente o obsoleto")
          case   (empty(.w_PR__DATA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPR__DATA_1_11.SetFocus()
            i_bnoObbl = !empty(.w_PR__DATA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PRCODATT)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST))  and (.w_PRRIGPRE<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRCODATT_1_13.SetFocus()
            i_bnoObbl = !empty(.w_PRCODATT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore obsoleto")
          case   not(.w_FLSERG='S' OR .w_PRUNIMIS=.w_UNMIS1 OR .w_PRUNIMIS=.w_UNMIS2 OR .w_PRUNIMIS=.w_UNMIS3)  and (.w_TIPART = 'FM' AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S'))  and not(empty(.w_PRUNIMIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRUNIMIS_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PRQTAMOV))  and (.w_TIPART = 'FM')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRQTAMOV_1_17.SetFocus()
            i_bnoObbl = !empty(.w_PRQTAMOV)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .gsag_mmp.CheckForm()
      if i_bres
        i_bres=  .gsag_mmp.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsag_apr
      * --- Controlli Finali
      .w_RESCHK=0
      IF i_bRes
          * -- Controlla se l'utente non � amministratore e se � attivo il relativo check 
          * -- nei Parametri attivit� e nelle Persone (responsabile)
          IF NOT Cp_IsAdministrator() AND .w_FLPRES = 'S' AND .w_CHKDATAPRE = 'S'
            IF .w_PR__DATA < .w_DATAPREC OR .w_PR__DATA > .w_DATASUCC
              i_bnoChk = .f.
              i_bRes = .f.
              i_cErrorMsg = Ah_MsgFormat("Data non consentita dai controlli relativi al responsabile")
            ENDIF
          ENDIF
      ENDIF
      IF i_bRes
          IF .cFunction='Edit' AND NOT EMPTY(.w_PRSERAGG) AND .w_OLD_PRESTA<>.w_PRESTA
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = Ah_MsgFormat("Non � possibile variare la tipologia di una spesa o di un'anticipazione collegata ad una prestazione.")
          ENDIF
      ENDIF
      IF !i_bRes
         .w_RESCHK=-1
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LetturaParAgen = this.w_LetturaParAgen
    this.o_LeggeParAlte = this.w_LeggeParAlte
    this.o_PRNUMPRA = this.w_PRNUMPRA
    this.o_PRCODRES = this.w_PRCODRES
    this.o_PR__DATA = this.w_PR__DATA
    this.o_PRCODATT = this.w_PRCODATT
    this.o_CODART = this.w_CODART
    this.o_PRUNIMIS = this.w_PRUNIMIS
    this.o_PRQTAMOV = this.w_PRQTAMOV
    this.o_PRPREZZO = this.w_PRPREZZO
    this.o_PROREEFF = this.w_PROREEFF
    this.o_PRMINEFF = this.w_PRMINEFF
    this.o_PRCODVAL = this.w_PRCODVAL
    this.o_PRCODLIS = this.w_PRCODLIS
    this.o_PRCOSUNI = this.w_PRCOSUNI
    this.o_PRSERIAL = this.w_PRSERIAL
    * --- gsag_mmp : Depends On
    this.gsag_mmp.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=GSAG_BPK( This , 'M'  )
    if !i_res
      cp_ErrorMsg(thisform.msgFmt(""+This.w_mess+""))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=GSAG_BPK( This , 'D'  )
    if !i_res
      cp_ErrorMsg(thisform.msgFmt(""+This.w_mess+""))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsag_aprPag1 as StdContainer
  Width  = 709
  height = 534
  stdWidth  = 709
  stdheight = 534
  resizeXpos=317
  resizeYpos=245
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRNUMPRA_1_7 as StdField with uid="UBBCXRJFWS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PRNUMPRA", cQueryName = "PRNUMPRA",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 195039287,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=106, Top=15, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSPR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_PRNUMPRA"

  func oPRNUMPRA_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (( ! .Close OR .w_EditCodPra or Empty(.w_PRNUMPRA)))
    endwith
   endif
  endfunc

  func oPRNUMPRA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRNUMPRA_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRNUMPRA_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oPRNUMPRA_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPR_ACN',"Fascicolo pratiche",'',this.parent.oContained
  endproc
  proc oPRNUMPRA_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSPR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_PRNUMPRA
     i_obj.ecpSave()
  endproc

  add object oPRCODRES_1_9 as StdField with uid="YQGWZHEEZU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PRCODRES", cQueryName = "PRCODRES",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valore inesistente o obsoleto",;
    HelpContextID = 218718281,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=106, Top=44, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_PRCODRES"

  func oPRCODRES_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODRES_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCODRES_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oPRCODRES_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Responsabili",'GSAG_KPR.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oPRCODRES_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_PRCODRES
     i_obj.ecpSave()
  endproc

  add object oPR__DATA_1_11 as StdField with uid="TIXSTWXUUP",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PR__DATA", cQueryName = "PR__DATA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 203104311,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=532, Top=44

  proc oPR__DATA_1_11.mBefore
    with this.Parent.oContained
      this.cQueryName = "PRNUMPRA,PRNUMPRE"
    endwith
  endproc

  add object oPRCODATT_1_13 as StdField with uid="NBJBBXMTXP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PRCODATT", cQueryName = "PRCODATT",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Valore obsoleto",;
    HelpContextID = 201941066,;
   bGlobalFont=.t.,;
    Height=21, Width=99, Left=7, Top=101, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_PRCODATT"

  func oPRCODATT_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRRIGPRE<>'S')
    endwith
   endif
  endfunc

  func oPRCODATT_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODATT_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCODATT_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPRCODATT_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",'INSPRE.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oPRCODATT_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PRCODATT
     i_obj.ecpSave()
  endproc

  add object oPRDESPRE_1_15 as StdField with uid="GRMBFBBLSJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PRDESPRE", cQueryName = "PRDESPRE",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 200241211,;
   bGlobalFont=.t.,;
    Height=21, Width=313, Left=111, Top=101, InputMask=replicate('X',40)

  add object oPRUNIMIS_1_16 as StdField with uid="MFCVQVBYMV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PRUNIMIS", cQueryName = "PRUNIMIS",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 140083273,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=428, Top=101, InputMask=replicate('X',3), bHasZoom = .t. , TabStop=.f., cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_PRUNIMIS"

  func oPRUNIMIS_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPART = 'FM' AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S'))
    endwith
   endif
  endfunc

  func oPRUNIMIS_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRUNIMIS_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRUNIMIS_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oPRUNIMIS_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� di misura",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc

  add object oPRQTAMOV_1_17 as StdField with uid="VZVYBFHEHP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PRQTAMOV", cQueryName = "PRQTAMOV",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 136363956,;
   bGlobalFont=.t.,;
    Height=21, Width=95, Left=467, Top=101, cSayPict="'@Z '+v_PQ(11)", cGetPict="'@Z '+v_GQ(11)", bHasZoom = .t. 

  func oPRQTAMOV_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPART = 'FM')
    endwith
   endif
  endfunc

  proc oPRQTAMOV_1_17.mZoom
      with this.Parent.oContained
        GSAG_BOM(this.Parent.oContained,"A")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPRPREZZO_1_18 as StdField with uid="GMOXRDSTEX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PRPREZZO", cQueryName = "PRPREZZO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 182636475,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=567, Top=101, cSayPict="v_PV(38+VVU)", cGetPict="v_GV(38+VVU)", bHasZoom = .t. 

  proc oPRPREZZO_1_18.mZoom
      with this.Parent.oContained
        gsag_bmp(this.Parent.oContained,"P",.w_PRPREZZO,.w_PRPREMAX,.w_PRPREMIN,.w_CODART)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oPRTIPRIG_1_21 as StdCombo with uid="QWATWYPCWE",rtseq=19,rtrep=.f.,left=106,top=139,width=107,height=22;
    , HelpContextID = 230977597;
    , cFormVar="w_PRTIPRIG",RowSource=""+"Non fatturabile,"+"Fatturabile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRTIPRIG_1_21.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oPRTIPRIG_1_21.GetRadio()
    this.Parent.oContained.w_PRTIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPRIG_1_21.SetRadio()
    this.Parent.oContained.w_PRTIPRIG=trim(this.Parent.oContained.w_PRTIPRIG)
    this.value = ;
      iif(this.Parent.oContained.w_PRTIPRIG=='N',1,;
      iif(this.Parent.oContained.w_PRTIPRIG=='D',2,;
      0))
  endfunc


  add object oPRTIPRI2_1_22 as StdCombo with uid="LZPXSQHYWD",rtseq=20,rtrep=.f.,left=106,top=167,width=125,height=22;
    , HelpContextID = 230977576;
    , cFormVar="w_PRTIPRI2",RowSource=""+"Senza nota spese,"+"Con nota spese", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRTIPRI2_1_22.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oPRTIPRI2_1_22.GetRadio()
    this.Parent.oContained.w_PRTIPRI2 = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPRI2_1_22.SetRadio()
    this.Parent.oContained.w_PRTIPRI2=trim(this.Parent.oContained.w_PRTIPRI2)
    this.value = ;
      iif(this.Parent.oContained.w_PRTIPRI2=='N',1,;
      iif(this.Parent.oContained.w_PRTIPRI2=='D',2,;
      0))
  endfunc

  add object oPROREEFF_1_23 as StdField with uid="QVXNOFEDCZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PROREEFF", cQueryName = "PROREEFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 1908796,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=378, Top=165, cSayPict='"999"', cGetPict='"999"'

  add object oPRMINEFF_1_24 as StdField with uid="FWTFIGVZTN",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PRMINEFF", cQueryName = "PRMINEFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 10747964,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=416, Top=165, cSayPict='"99"', cGetPict='"99"'

  add object oPRCOSINT_1_25 as StdField with uid="BFHRTTAYYX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PRCOSINT", cQueryName = "PRCOSINT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 184983478,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=560, Top=165, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oPRDESAGG_1_26 as StdMemo with uid="TRUVBNFTPV",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PRDESAGG", cQueryName = "PRDESAGG",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 217018429,;
   bGlobalFont=.t.,;
    Height=63, Width=593, Left=106, Top=195

  add object oPRCODNOM_1_27 as StdField with uid="JUPXYRTVQK",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PRCODNOM", cQueryName = "PRCODNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 116826045,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=106, Top=264, InputMask=replicate('X',15), cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_PRCODNOM"

  func oPRCODNOM_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oPRNUMPRE_1_28 as StdField with uid="RMRUXOTMXZ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_PRNUMPRE", cQueryName = "PRNUMPRE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo prestazione",;
    HelpContextID = 195039291,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=560, Top=15

  add object oPRCODVAL_1_29 as StdField with uid="FPFMAWXHMN",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PRCODVAL", cQueryName = "PRCODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 17391682,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=651, Top=15, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="gsar_avl", oKey_1_1="VACODVAL", oKey_1_2="this.w_PRCODVAL"

  func oPRCODVAL_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oPRCODLIS_1_30 as StdField with uid="LUYYTCZLUT",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PRCODLIS", cQueryName = "PRCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 118054985,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=651, Top=43, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="gsar_ali", oKey_1_1="LSCODLIS", oKey_1_2="this.w_PRCODLIS"

  func oPRCODLIS_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oPRCODLIS_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODLIS_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCODLIS_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oPRCODLIS_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_ali',"Listini",'',this.parent.oContained
  endproc
  proc oPRCODLIS_1_30.mZoomOnZoom
    local i_obj
    i_obj=gsar_ali()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_PRCODLIS
     i_obj.ecpSave()
  endproc

  add object oUMDESCRI_1_31 as StdField with uid="TWDFVRCPLA",rtseq=29,rtrep=.f.,;
    cFormVar = "w_UMDESCRI", cQueryName = "UMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 17863793,;
   bGlobalFont=.t.,;
    Height=21, Width=198, Left=309, Top=137, InputMask=replicate('X',35)

  add object oPRPREMIN_1_33 as StdField with uid="LAQWDFOWZD",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PRPREMIN", cQueryName = "PRPREMIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 136130628,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=106, Top=291, cSayPict='"999999999999.99999"', cGetPict='"999999999999.99999"'

  add object oPRPREMAX_1_34 as StdField with uid="FFEIHZQJKB",rtseq=31,rtrep=.f.,;
    cFormVar = "w_PRPREMAX", cQueryName = "PRPREMAX",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 132304818,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=343, Top=291, cSayPict='"999999999999.99999"', cGetPict='"999999999999.99999"'

  add object oPRGAZUFF_1_35 as StdField with uid="LXKOKLQFNA",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PRGAZUFF", cQueryName = "PRGAZUFF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    HelpContextID = 22782012,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=583, Top=291, InputMask=replicate('X',6)


  add object oLinkPC_1_36 as StdButton with uid="HJYPHPXMKB",left=649, top=271, width=48,height=45,;
    CpPicture="bmp\DM_analitica.bmp", caption="", nPag=1;
    , HelpContextID = 41853194;
    , TabStop=.f.,Caption='\<Parti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_36.Click()
      this.Parent.oContained.gsag_mmp.LinkPCClick()
    endproc

  func oLinkPC_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_PRRIFFAT<>'@@@@@@@@@@')
     endwith
    endif
  endfunc

  add object oNUMPRO_1_37 as StdField with uid="UCPSLKELHM",rtseq=33,rtrep=.f.,;
    cFormVar = "w_NUMPRO", cQueryName = "NUMPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 85261610,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=264, Top=372

  func oNUMPRO_1_37.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFPRO))
    endwith
  endfunc

  add object oALFPRO_1_38 as StdField with uid="SCUQDCTRAN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_ALFPRO", cQueryName = "ALFPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 85292794,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=393, Top=372, InputMask=replicate('X',10)

  func oALFPRO_1_38.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFPRO))
    endwith
  endfunc

  add object oDATPRO_1_39 as StdField with uid="VWYXZFQLSD",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DATPRO", cQueryName = "DATPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 85238218,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=498, Top=372

  func oDATPRO_1_39.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFPRO))
    endwith
  endfunc


  add object oBtn_1_40 as StdButton with uid="NBFOQRUQXX",left=577, top=371, width=22,height=23,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per accedere alla proforma";
    , HelpContextID = 159847978;
    , Caption='...';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      with this.Parent.oContained
        gsar_bzm(this.Parent.oContained,.w_PRRIFPRO,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_RIFPRO))
     endwith
    endif
  endfunc

  add object oNUMFAT_1_41 as StdField with uid="HBWEACIAGU",rtseq=36,rtrep=.f.,;
    cFormVar = "w_NUMFAT", cQueryName = "NUMFAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 19856682,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=264, Top=398

  func oNUMFAT_1_41.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFFAT))
    endwith
  endfunc

  add object oALFFAT_1_42 as StdField with uid="NFHWJFSKQI",rtseq=37,rtrep=.f.,;
    cFormVar = "w_ALFFAT", cQueryName = "ALFFAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 19887866,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=393, Top=398, InputMask=replicate('X',10)

  func oALFFAT_1_42.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFFAT))
    endwith
  endfunc

  add object oDATFAT_1_43 as StdField with uid="YAGPKEVERV",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DATFAT", cQueryName = "DATFAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 19833290,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=498, Top=398

  func oDATFAT_1_43.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFFAT))
    endwith
  endfunc


  add object oBtn_1_44 as StdButton with uid="WEQURHEGGT",left=577, top=397, width=22,height=23,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per accedere alla fattura";
    , HelpContextID = 159847978;
    , Caption='...';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      with this.Parent.oContained
        gsar_bzm(this.Parent.oContained,.w_PRRIFFAT,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_44.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_RIFFAT))
     endwith
    endif
  endfunc

  add object oNUMNOT_1_45 as StdField with uid="VCJBJCMAVF",rtseq=39,rtrep=.f.,;
    cFormVar = "w_NUMNOT", cQueryName = "NUMNOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 4652330,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=264, Top=476

  func oNUMNOT_1_45.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFNOT))
    endwith
  endfunc

  add object oALFNOT_1_46 as StdField with uid="ZLCRUFYACV",rtseq=40,rtrep=.f.,;
    cFormVar = "w_ALFNOT", cQueryName = "ALFNOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 4683514,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=393, Top=476, InputMask=replicate('X',10)

  func oALFNOT_1_46.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFNOT))
    endwith
  endfunc

  add object oDATNOT_1_47 as StdField with uid="IUQXKZHGBI",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DATNOT", cQueryName = "DATNOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 4628938,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=498, Top=479

  func oDATNOT_1_47.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFNOT))
    endwith
  endfunc


  add object oBtn_1_48 as StdButton with uid="ZHJVESGIIP",left=577, top=478, width=22,height=23,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per accedere alla nota spese";
    , HelpContextID = 159847978;
    , Caption='...';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_48.Click()
      with this.Parent.oContained
        gsar_bzm(this.Parent.oContained,.w_PRRIFNOT,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_48.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_RIFNOT))
     endwith
    endif
  endfunc

  add object oDESCAN_1_86 as StdField with uid="PFMGKREDTK",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 120696266,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=245, Top=15, InputMask=replicate('X',100)

  add object oNODESCR_1_88 as StdField with uid="XZHUREMLEA",rtseq=71,rtrep=.f.,;
    cFormVar = "w_NODESCR", cQueryName = "NODESCR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 17863466,;
   bGlobalFont=.t.,;
    Height=21, Width=299, Left=227, Top=264, InputMask=replicate('X',60)


  add object oObj_1_96 as cp_runprogram with uid="AECCZYAARG",left=271, top=556, width=208,height=31,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="Gsag_bpk('K')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 17948646


  add object oBtn_1_99 as StdButton with uid="LAWWCIBYZX",left=649, top=402, width=48,height=45,;
    CpPicture="bmp\Elabora.ico", caption="", nPag=1;
    , ToolTipText = "Permette di contabilizzare le anticipazioni contenute in questa attivit�";
    , HelpContextID = 9810654;
    , Caption='C\<ont.Ant.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_99.Click()
      with this.Parent.oContained
        do Contabilizza with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_99.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Not Empty(.w_PRRIFCON) or Empty(.w_PRSERIAL) OR ! .w_PRESTA $ 'S-A')
     endwith
    endif
  endfunc


  add object oBtn_1_100 as StdButton with uid="RWVHEPYZPP",left=649, top=402, width=48,height=45,;
    CpPicture="BMP\teso.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per per accedere alla registrazione contabile associata";
    , HelpContextID = 14029834;
    , TabStop=.f.,Caption='\<Primanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_100.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_PRRIFCON)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_100.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!(EMPTY(.w_PRRIFCON) or g_COGE<>'S'))
      endwith
    endif
  endfunc

  func oBtn_1_100.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_PRRIFCON) or g_COGE<>'S')
     endwith
    endif
  endfunc

  add object oDENOMRES_1_120 as StdField with uid="EXHSESXJWG",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DENOMRES", cQueryName = "DENOMRES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 228197001,;
   bGlobalFont=.t.,;
    Height=21, Width=262, Left=187, Top=44, InputMask=replicate('X',50)


  add object oBtn_1_126 as StdButton with uid="DMNQXIWVZQ",left=649, top=351, width=48,height=45,;
    CpPicture="bmp\DM_agenda.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza attivit� ";
    , HelpContextID = 122065382;
    , TabStop=.f.,Caption='\<Attivit�';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_126.Click()
      with this.Parent.oContained
        =opengest('A','GSAG_AAT','ATSERIAL',.w_PRSERATT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_126.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_PRSERATT))
      endwith
    endif
  endfunc

  func oBtn_1_126.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_PRSERATT))
     endwith
    endif
  endfunc

  add object oPRVALRIG_1_156 as StdField with uid="RGYCEUWHUC",rtseq=116,rtrep=.f.,;
    cFormVar = "w_PRVALRIG", cQueryName = "PRVALRIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 226267197,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=560, Top=137, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  add object oNUMBOZ_1_166 as StdField with uid="NSBFOUQXZN",rtseq=126,rtrep=.f.,;
    cFormVar = "w_NUMBOZ", cQueryName = "NUMBOZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 173210922,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=264, Top=346

  func oNUMBOZ_1_166.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFBOZ))
    endwith
  endfunc

  add object oALFBOZ_1_167 as StdField with uid="IPYHFTBFAE",rtseq=127,rtrep=.f.,;
    cFormVar = "w_ALFBOZ", cQueryName = "ALFBOZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 173242106,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=393, Top=346, InputMask=replicate('X',10)

  func oALFBOZ_1_167.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFBOZ))
    endwith
  endfunc

  add object oDATBOZ_1_168 as StdField with uid="KGGQDAJQJK",rtseq=128,rtrep=.f.,;
    cFormVar = "w_DATBOZ", cQueryName = "DATBOZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 173187530,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=499, Top=346

  func oDATBOZ_1_168.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFBOZ))
    endwith
  endfunc


  add object oBtn_1_169 as StdButton with uid="JTBPSFOFPO",left=577, top=345, width=22,height=23,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per accedere alla bozza";
    , HelpContextID = 159847978;
    , Caption='...';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_169.Click()
      with this.Parent.oContained
        gsar_bzm(this.Parent.oContained,.w_PRRIFBOZ,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_169.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_RIFBOZ))
     endwith
    endif
  endfunc

  add object oNUMFAA_1_174 as StdField with uid="YDOKJQVUKD",rtseq=131,rtrep=.f.,;
    cFormVar = "w_NUMFAA", cQueryName = "NUMFAA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 70188330,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=264, Top=450

  func oNUMFAA_1_174.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFFAA))
    endwith
  endfunc

  add object oALFFAA_1_175 as StdField with uid="YNXPWMREOR",rtseq=132,rtrep=.f.,;
    cFormVar = "w_ALFFAA", cQueryName = "ALFFAA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 70219514,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=393, Top=450, InputMask=replicate('X',10)

  func oALFFAA_1_175.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFFAA))
    endwith
  endfunc

  add object oDATFAA_1_176 as StdField with uid="CATTYTUAEI",rtseq=133,rtrep=.f.,;
    cFormVar = "w_DATFAA", cQueryName = "DATFAA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 70164938,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=498, Top=451

  func oDATFAA_1_176.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFFAA))
    endwith
  endfunc


  add object oBtn_1_177 as StdButton with uid="IZEGYPKKRQ",left=577, top=449, width=22,height=23,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per accedere alla bozza";
    , HelpContextID = 159847978;
    , Caption='...';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_177.Click()
      with this.Parent.oContained
        gsar_bzm(this.Parent.oContained,.w_PRRIFFAA,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_177.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_RIFFAA))
     endwith
    endif
  endfunc

  add object oNUMPRA_1_180 as StdField with uid="VDQUXQBPAP",rtseq=134,rtrep=.f.,;
    cFormVar = "w_NUMPRA", cQueryName = "NUMPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 51707178,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=264, Top=424

  func oNUMPRA_1_180.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFPRA))
    endwith
  endfunc

  add object oALFPRA_1_181 as StdField with uid="DTYDILHSHL",rtseq=135,rtrep=.f.,;
    cFormVar = "w_ALFPRA", cQueryName = "ALFPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 51738362,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=393, Top=424, InputMask=replicate('X',10)

  func oALFPRA_1_181.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFPRA))
    endwith
  endfunc

  add object oDATPRA_1_182 as StdField with uid="JSWFUWWTWR",rtseq=136,rtrep=.f.,;
    cFormVar = "w_DATPRA", cQueryName = "DATPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 51683786,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=498, Top=425

  func oDATPRA_1_182.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFPRA))
    endwith
  endfunc


  add object oBtn_1_183 as StdButton with uid="QJEQKXQTBF",left=577, top=423, width=22,height=23,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per accedere alla bozza";
    , HelpContextID = 159847978;
    , Caption='...';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_183.Click()
      with this.Parent.oContained
        gsar_bzm(this.Parent.oContained,.w_PRRIFPRA,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_183.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_RIFPRA))
     endwith
    endif
  endfunc

  add object oPRSERIAL_1_196 as StdField with uid="UHXZUGAHMP",rtseq=146,rtrep=.f.,;
    cFormVar = "w_PRSERIAL", cQueryName = "PRSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 186621886,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=616, Top=512, InputMask=replicate('X',10)

  func oPRSERIAL_1_196.mHide()
    with this.Parent.oContained
      return (.w_GENPRE<>'S')
    endwith
  endfunc


  add object oBtn_1_203 as StdButton with uid="BDUJDQARAZ",left=649, top=453, width=48,height=45,;
    CpPicture="bmp\pagat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza le spese e le anticipazioni collegate";
    , HelpContextID = 146513446;
    , TabStop=.f.,Caption='\<Spe/Ant';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_203.Click()
      do GSAG_KST with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_203.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_PRSERIAL) AND .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_203.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_PRSERIAL) OR .cFunction<>'Query' OR (EMPTY(.w_PRSERAGG) AND .w_PRRIGPRE<>'S'))
     endwith
    endif
  endfunc


  add object oBtn_1_207 as StdButton with uid="UHXERRQKPE",left=649, top=453, width=48,height=45,;
    CpPicture="bmp\legami.ico", caption="", nPag=1;
    , ToolTipText = "Premere per collegare la spesa o l'anticipazione ad una prestazione";
    , HelpContextID = 70847526;
    , TabStop=.f.,Caption='\<Collega';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_207.Click()
      do GSAG_KST with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_207.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_PRSERIAL) AND .cFunction='Query' AND !EMPTY(.w_PRNUMPRA))
      endwith
    endif
  endfunc

  func oBtn_1_207.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_PRSERIAL) OR .cFunction<>'Query' OR !EMPTY(.w_PRSERAGG) OR .w_PRRIGPRE='S' OR !Chkprest(.w_PRCODATT,'A-S'))
     endwith
    endif
  endfunc


  add object oObj_1_224 as cp_runprogram with uid="LLSSAFOIRM",left=528, top=726, width=166,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="Gsag_bpk('A')",;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 17948646

  add object oStr_1_32 as StdString with uid="UBKYJPQFFM",Visible=.t., Left=-15, Top=46,;
    Alignment=1, Width=118, Height=18,;
    Caption="Responsabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="UKPXCAVFHU",Visible=.t., Left=15, Top=18,;
    Alignment=1, Width=88, Height=18,;
    Caption="Numero pratica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="ZQFSOCEJTT",Visible=.t., Left=482, Top=46,;
    Alignment=1, Width=48, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="TAGSQOOKKC",Visible=.t., Left=7, Top=81,;
    Alignment=2, Width=99, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="FHXNUMFFDY",Visible=.t., Left=-13, Top=195,;
    Alignment=1, Width=116, Height=18,;
    Caption="Descr. aggiuntiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="UDOWEMKTUZ",Visible=.t., Left=429, Top=81,;
    Alignment=2, Width=34, Height=18,;
    Caption="U.M."  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="FZVCOEXRFH",Visible=.t., Left=568, Top=81,;
    Alignment=2, Width=139, Height=18,;
    Caption="Tariffa"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="TEVMHLQKCO",Visible=.t., Left=509, Top=140,;
    Alignment=1, Width=45, Height=18,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="KWIVORUZNP",Visible=.t., Left=250, Top=168,;
    Alignment=1, Width=125, Height=18,;
    Caption="Durata effettiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="SFZEIHNCDK",Visible=.t., Left=412, Top=169,;
    Alignment=0, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="JTVDRTLXGS",Visible=.t., Left=477, Top=168,;
    Alignment=1, Width=77, Height=18,;
    Caption="Costo interno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="QLVZCYKVUC",Visible=.t., Left=33, Top=293,;
    Alignment=1, Width=71, Height=18,;
    Caption="Tariffa min.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="IVFYMDXQLB",Visible=.t., Left=244, Top=293,;
    Alignment=1, Width=95, Height=18,;
    Caption="Tariffa max.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="XBEJCIYQRB",Visible=.t., Left=444, Top=293,;
    Alignment=1, Width=136, Height=18,;
    Caption="Gazzetta ufficiale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="UAKDMRERPG",Visible=.t., Left=609, Top=18,;
    Alignment=1, Width=40, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="YDGURUUBAE",Visible=.t., Left=609, Top=45,;
    Alignment=1, Width=40, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="PQLTQHOLCM",Visible=.t., Left=28, Top=265,;
    Alignment=1, Width=75, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="LDMCJXRXRH",Visible=.t., Left=18, Top=140,;
    Alignment=1, Width=85, Height=18,;
    Caption="Tipologia riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="LJCUGRRJKU",Visible=.t., Left=-54, Top=168,;
    Alignment=1, Width=157, Height=18,;
    Caption="Nota spese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="RIJQKTVKGO",Visible=.t., Left=521, Top=18,;
    Alignment=1, Width=36, Height=18,;
    Caption="Prog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_102 as StdString with uid="CNAJHXKPKR",Visible=.t., Left=151, Top=323,;
    Alignment=0, Width=105, Height=18,;
    Caption="Documenti emessi"  ;
  , bGlobalFont=.t.

  add object oStr_1_103 as StdString with uid="AKKYVMZJFB",Visible=.t., Left=365, Top=323,;
    Alignment=0, Width=45, Height=18,;
    Caption="Numero"  ;
  , bGlobalFont=.t.

  add object oStr_1_104 as StdString with uid="UNIOEEQAHO",Visible=.t., Left=517, Top=323,;
    Alignment=0, Width=45, Height=18,;
    Caption="Data"  ;
  , bGlobalFont=.t.

  add object oStr_1_105 as StdString with uid="OPXQWUVNIB",Visible=.t., Left=384, Top=374,;
    Alignment=0, Width=15, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_105.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFPRO))
    endwith
  endfunc

  add object oStr_1_106 as StdString with uid="JUMBCYKWBF",Visible=.t., Left=135, Top=373,;
    Alignment=1, Width=98, Height=18,;
    Caption="Proforma"  ;
  , bGlobalFont=.t.

  add object oStr_1_107 as StdString with uid="ZILNNDWOLK",Visible=.t., Left=384, Top=400,;
    Alignment=0, Width=15, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_107.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFFAT))
    endwith
  endfunc

  add object oStr_1_108 as StdString with uid="USSGEMEALW",Visible=.t., Left=129, Top=399,;
    Alignment=1, Width=104, Height=18,;
    Caption="Fattura"  ;
  , bGlobalFont=.t.

  add object oStr_1_109 as StdString with uid="FJQXZQFCQQ",Visible=.t., Left=384, Top=480,;
    Alignment=0, Width=15, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_109.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFNOT))
    endwith
  endfunc

  add object oStr_1_110 as StdString with uid="PCMDHLVFCI",Visible=.t., Left=138, Top=477,;
    Alignment=1, Width=95, Height=18,;
    Caption="Nota spese"  ;
  , bGlobalFont=.t.

  add object oStr_1_111 as StdString with uid="EDGDXPWQMX",Visible=.t., Left=468, Top=81,;
    Alignment=2, Width=95, Height=18,;
    Caption="Quantit�"  ;
  , bGlobalFont=.t.

  add object oStr_1_112 as StdString with uid="NRSJANEFII",Visible=.t., Left=111, Top=81,;
    Alignment=2, Width=313, Height=18,;
    Caption="Descrizione"  ;
  , bGlobalFont=.t.

  add object oStr_1_119 as StdString with uid="OTHPGGKXQI",Visible=.t., Left=218, Top=140,;
    Alignment=2, Width=90, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_170 as StdString with uid="IUBKFRGZBK",Visible=.t., Left=384, Top=349,;
    Alignment=0, Width=15, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_170.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFBOZ))
    endwith
  endfunc

  add object oStr_1_171 as StdString with uid="QWXQPPHCMF",Visible=.t., Left=139, Top=347,;
    Alignment=1, Width=95, Height=18,;
    Caption="Bozza"  ;
  , bGlobalFont=.t.

  add object oStr_1_178 as StdString with uid="MLFHVUZWVX",Visible=.t., Left=384, Top=454,;
    Alignment=0, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_178.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFFAA))
    endwith
  endfunc

  add object oStr_1_179 as StdString with uid="GYINABMCYG",Visible=.t., Left=138, Top=451,;
    Alignment=1, Width=95, Height=18,;
    Caption="Fattura d'acconto"  ;
  , bGlobalFont=.t.

  add object oStr_1_184 as StdString with uid="SADJSSJXUI",Visible=.t., Left=384, Top=428,;
    Alignment=0, Width=15, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_184.mHide()
    with this.Parent.oContained
      return (Empty(.w_RIFPRA))
    endwith
  endfunc

  add object oStr_1_185 as StdString with uid="OKDBALOXZU",Visible=.t., Left=123, Top=425,;
    Alignment=1, Width=110, Height=18,;
    Caption="Proforma d'acconto"  ;
  , bGlobalFont=.t.

  add object oStr_1_195 as StdString with uid="GESGSMZYYC",Visible=.t., Left=530, Top=514,;
    Alignment=1, Width=82, Height=18,;
    Caption="ID prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_195.mHide()
    with this.Parent.oContained
      return (.w_GENPRE<>'S')
    endwith
  endfunc

  add object oBox_1_101 as StdBox with uid="VWWVFWYPFW",left=106, top=339, width=503,height=168

  add object oBox_1_113 as StdBox with uid="JYGKDPBEGM",left=4, top=80, width=705,height=50

  add object oBox_1_114 as StdBox with uid="YGIRSDTXRA",left=5, top=97, width=702,height=1

  add object oBox_1_115 as StdBox with uid="NVMYZPPSCT",left=108, top=80, width=1,height=48

  add object oBox_1_116 as StdBox with uid="UIXEPIJELD",left=425, top=80, width=1,height=48

  add object oBox_1_117 as StdBox with uid="MVZETKEVHY",left=464, top=80, width=1,height=48

  add object oBox_1_118 as StdBox with uid="GOKMBYUIJU",left=564, top=80, width=1,height=48
enddefine
define class tgsag_aprPag2 as StdContainer
  Width  = 709
  height = 534
  stdWidth  = 709
  stdheight = 534
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRVOCCOS_2_1 as StdField with uid="ZXVYGREVGS",rtseq=53,rtrep=.f.,;
    cFormVar = "w_PRVOCCOS", cQueryName = "PRVOCCOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 33910711,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=174, Top=34, InputMask=replicate('X',15), cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_PRVOCCOS"

  func oPRVOCCOS_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRVOCCOS_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oPRCENCOS_2_2 as StdField with uid="CZGBVWGSNA",rtseq=54,rtrep=.f.,;
    cFormVar = "w_PRCENCOS", cQueryName = "PRCENCOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 23109559,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=174, Top=63, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PRCENCOS"

  func oPRCENCOS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCENCOS_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCENCOS_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPRCENCOS_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centro di costo/ricavo",'',this.parent.oContained
  endproc
  proc oPRCENCOS_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_PRCENCOS
     i_obj.ecpSave()
  endproc

  add object oPRATTIVI_2_5 as StdField with uid="TBUTFPISJS",rtseq=59,rtrep=.f.,;
    cFormVar = "w_PRATTIVI", cQueryName = "PRATTIVI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 84820031,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=174, Top=92, InputMask=replicate('X',15)


  add object oPR_SEGNO_2_6 as StdCombo with uid="AKXHGPJEUJ",rtseq=60,rtrep=.f.,left=541,top=121,width=114,height=22;
    , HelpContextID = 232841147;
    , cFormVar="w_PR_SEGNO",RowSource=""+"Avere,"+"Dare", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPR_SEGNO_2_6.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oPR_SEGNO_2_6.GetRadio()
    this.Parent.oContained.w_PR_SEGNO = this.RadioValue()
    return .t.
  endfunc

  func oPR_SEGNO_2_6.SetRadio()
    this.Parent.oContained.w_PR_SEGNO=trim(this.Parent.oContained.w_PR_SEGNO)
    this.value = ;
      iif(this.Parent.oContained.w_PR_SEGNO=='A',1,;
      iif(this.Parent.oContained.w_PR_SEGNO=='D',2,;
      0))
  endfunc

  add object oPRINICOM_2_9 as StdField with uid="VRBCZODXTS",rtseq=61,rtrep=.f.,;
    cFormVar = "w_PRINICOM", cQueryName = "PRINICOM",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 27738045,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=174, Top=121, bHasZoom = .t. 

  proc oPRINICOM_2_9.mZoom
    
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPRFINCOM_2_11 as StdField with uid="IEKLJZMTOF",rtseq=62,rtrep=.f.,;
    cFormVar = "w_PRFINCOM", cQueryName = "PRFINCOM",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 22835133,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=306, Top=121, bHasZoom = .t. 

  proc oPRFINCOM_2_11.mZoom
    
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oVOCCOSDE_2_15 as StdField with uid="RJLBYFWNAE",rtseq=84,rtrep=.f.,;
    cFormVar = "w_VOCCOSDE", cQueryName = "VOCCOSDE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 246242715,;
   bGlobalFont=.t.,;
    Height=21, Width=361, Left=294, Top=34, InputMask=replicate('X',40)

  add object oCENCOSDE_2_16 as StdField with uid="IKOGHXDDTL",rtseq=85,rtrep=.f.,;
    cFormVar = "w_CENCOSDE", cQueryName = "CENCOSDE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 246284907,;
   bGlobalFont=.t.,;
    Height=21, Width=361, Left=294, Top=63, InputMask=replicate('X',40)

  add object oStr_2_3 as StdString with uid="PVYKYJRUOK",Visible=.t., Left=50, Top=63,;
    Alignment=1, Width=121, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="NSUNKZRBGC",Visible=.t., Left=91, Top=34,;
    Alignment=1, Width=80, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="BLLQIMSTVS",Visible=.t., Left=460, Top=122,;
    Alignment=1, Width=77, Height=18,;
    Caption="Segno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="MJNBVIAPYC",Visible=.t., Left=92, Top=93,;
    Alignment=1, Width=79, Height=18,;
    Caption="Codice attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="YKMSCRGKFG",Visible=.t., Left=41, Top=122,;
    Alignment=1, Width=130, Height=18,;
    Caption="Competenza da:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="UPCKXUHQMC",Visible=.t., Left=261, Top=122,;
    Alignment=1, Width=42, Height=18,;
    Caption="a:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_apr','PRE_STAZ','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRSERIAL=PRE_STAZ.PRSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_apr
Proc Contabilizza(pParent)
   Local oGsag_kga
   exec="oGsag_kga=gsal_kga()"
   &exec
   oGsag_kga.left=_screen.width+1
   oGsag_kga.w_MVSERIAL=pParent.w_PRSERIAL
   oGsag_kga.w_INIDATA=pParent.w_PR__DATA
   oGsag_kga.ecpsave()
   oGsag_kga=.Null.
   pParent.Loadrecwarn()
Endproc

Proc AggProg(pParent)
  if pParent.op_codazi<> pParent.w_codazi .or. pParent.op_PRNUMPRA<> pParent.w_PRNUMPRA
    i_nConn = i_TableProp[pParent.PRE_STAZ_IDX,3]
    cp_AskTableProg(pParent,i_nConn,"NUPRE","i_codazi,w_PRNUMPRA,w_PRNUMPRE")
    pParent.w_PRNUMPRE=pParent.w_PRNUMPRE+icase(pParent.w_PRNUMPRE=2,8,9)
    pParent.op_codazi = pParent.w_codazi
    pParent.op_PRNUMPRA = pParent.w_PRNUMPRA
  endif
Endproc
* --- Fine Area Manuale
