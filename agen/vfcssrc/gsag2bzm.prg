* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag2bzm                                                        *
*              Lancia elenco attivit�/documenti da contratti                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-10-06                                                      *
* Last revis.: 2011-10-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag2bzm",oParentObject,m.pOper)
return(i_retval)

define class tgsag2bzm as StdBatch
  * --- Local variables
  pOper = space(2)
  w_PROG = .NULL.
  w_OBJ = .NULL.
  w_TIPATT = space(10)
  w_SERIAL = space(10)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_DATFINDOC = ctod("  /  /  ")
  w_ELCODMOD = space(10)
  w_ELCODIMP = space(10)
  w_ELCODCOM = 0
  w_ELRINNOV = 0
  w_CODCON = space(15)
  w_PADRE = .NULL.
  w_CURSERAT = space(20)
  w_MOTIPCON = space(1)
  w_ELTIPCON = space(1)
  w_RETVAL = 0
  w_TIPO = space(1)
  * --- WorkFile variables
  DOC_GENE_idx=0
  MOD_ELEM_idx=0
  ANEVENTI_idx=0
  TIP_RAGG_idx=0
  TIPEVENT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = This.oParentObject
    * --- Attivit�
    do case
      case upper(this.w_PADRE.class)="TGSAG_ACA"
        if this.pOper="AO"
          * --- Visualizzo il men� per la scelta del tipo contratto (pacchetto/canone)
          DIMENSION ARRVOCI(2)
          ARRVOCI[1]="Canone"
          ARRVOCI[2]="Pacchetto"
          this.w_RETVAL = Addmenu(@ARRVOCI)
          if this.w_RETVAL=1
            this.w_ELTIPCON = "C"
          else
            this.w_ELTIPCON = "P"
          endif
        endif
        this.w_TIPATT = ""
        this.w_SERIAL = this.w_PADRE.w_COSERIAL
        this.w_DATINI = CP_CHARTODATE("  -  -  ")
        this.w_DATFIN = this.w_PADRE.w_CODATFIN + 30
      case upper(this.w_PADRE.class)="TGSAG_KDA" OR upper(this.w_PADRE.class)="TGSAG_KDL"
        this.w_TIPATT = ""
        this.w_SERIAL = this.w_PADRE.oParentObject.w_DACONTRA
        this.w_DATINI = CP_CHARTODATE("  -  -  ")
        this.w_DATFIN = this.w_PADRE.w_CODATFIN
        this.w_ELCODMOD = this.w_PADRE.oParentObject.w_DACODMOD
        this.w_ELCODIMP = this.w_PADRE.oParentObject.w_DACODIMP
        this.w_ELCODCOM = this.w_PADRE.oParentObject.w_DACOCOMP
        this.w_ELRINNOV = this.w_PADRE.oParentObject.w_DARINNOV
        if uPPER(this.w_PADRE.oParentObject.CLASS)="TGSAG_MDA"
          this.w_CURSERAT = this.w_PADRE.oParentObject.w_DASERIAL
        endif
        this.w_ELTIPCON = "P"
      otherwise
        this.w_TIPATT = ""
        this.w_SERIAL = this.w_PADRE.w_ELCONTRA
        this.w_DATINI = CP_CHARTODATE("  -  -  ")
        this.w_DATFIN = this.w_PADRE.w_ELDATGAT
        this.w_ELCODMOD = this.w_PADRE.w_ELCODMOD
        this.w_ELCODIMP = this.w_PADRE.w_ELCODIMP
        this.w_ELCODCOM = this.w_PADRE.w_ELCODCOM
        this.w_ELRINNOV = this.w_PADRE.w_ELRINNOV
        this.w_ELTIPCON = this.w_PADRE.w_MOTIPCON
    endcase
    * --- Apre masch.visualizza Attivit�
    if this.pOper="AO"
      this.w_PROG = GSAG_KRA()
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_PROG.bSec1)
        * --- Messaggio di errore
        Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
        i_retcode = 'stop'
        return
      endif
      this.w_PROG.w_CACODICE = ""
      this.w_PROG.w_ELTIPCON = this.w_ELTIPCON
      SetValueLinked("M", this.w_PROG, "w_CONTRA", this.w_SERIAL )
      SetValueLinked("M", this.w_PROG, "w_ELCODMOD", this.w_ELCODMOD )
      SetValueLinked("M", this.w_PROG, "w_ELCODIMP", this.w_ELCODIMP )
      this.w_PROG.w_ELCODCOM = this.w_ELCODCOM
      this.w_PROG.w_ELRINNOV = this.w_ELRINNOV
      this.w_PROG.w_STATO = "K"
      this.w_PROG.w_PRIORITA = 0
      this.w_PROG.w_DATINI = CP_CHARTODATE("  -  -  ")
      this.w_PROG.w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(this.w_DATINI), ALLTR(STR(DAY(this.w_DATINI)))+"-"+ALLTR(STR(MONTH(this.w_DATINI)))+"-"+ALLTR(STR(YEAR(this.w_DATINI)))+" "+this.w_PROG.w_OREINI+":"+this.w_PROG.w_MININI+":00", DTOC(i_inidat)+" 00:00:00"), "dd-mm-yyyy hh:nn:ss")
      this.w_PROG.w_DATFIN = CP_CHARTODATE("  -  -  ")
      this.w_PROG.w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(this.w_DATFIN), ALLTR(STR(DAY(this.w_DATFIN)))+"-"+ALLTR(STR(MONTH(this.w_DATFIN)))+"-"+ALLTR(STR(YEAR(this.w_DATFIN)))+" "+this.w_PROG.w_OREFIN+":"+this.w_PROG.w_MINFIN+":00", DTOC(i_findat)+" 23:59:59"), "dd-mm-yyyy hh:nn:ss")
      this.w_PROG.w_PATIPRIS = ""
      this.w_PROG.w_PRIORITA = 0
      this.w_PROG.NotifyEvent("Ricerca")     
    endif
    do case
      case upper(this.w_PADRE.class)="TGSAG_ACA"
        this.w_SERIAL = this.w_PADRE.w_COSERIAL
        this.w_DATINI = G_DATEUR
        this.w_DATFIN = this.w_PADRE.w_CODATFIN + 30
        this.w_CODCON = this.w_PADRE.w_COCODCON
      case upper(this.w_PADRE.class)="TGSAG_KDA" OR upper(this.w_PADRE.class)="TGSAG_KDL"
      otherwise
        this.w_SERIAL = this.w_PADRE.w_ELCONTRA
        this.w_DATINI = G_DATEUR
        this.w_CODCON = this.w_PADRE.w_ELCODCLI
        this.w_DATFINDOC = this.w_PADRE.w_ELDATFIN + this.w_PADRE.w_ELGIODOC
        this.w_ELCODMOD = this.w_PADRE.w_ELCODMOD
        this.w_ELCODIMP = this.w_PADRE.w_ELCODIMP
        this.w_ELCODCOM = this.w_PADRE.w_ELCODCOM
        this.w_ELRINNOV = this.w_PADRE.w_ELRINNOV
    endcase
    if this.pOper="DO"
      * --- Apre masch.VISUALIZZA DOCUMENTI DI VENDITA
      this.w_PROG = GSVE_SZM()
      if Isahe()
        this.w_PROG.w_ZoomDoc.cCpQueryName = "GSAGCSZM"
      else
        this.w_PROG.w_ZoomDoc.cCpQueryName = "GSAGBSZM"
      endif
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_PROG.bSec1)
        * --- Messaggio di errore
        Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
        i_retcode = 'stop'
        return
      endif
      if Isahr()
        this.w_PROG.w_DESAPP = ""
      endif
      this.w_PROG.w_CONTRA = this.w_SERIAL
      this.w_PROG.w_ELCODMOD = this.w_ELCODMOD
      this.w_PROG.w_ELCODIMP = this.w_ELCODIMP
      this.w_PROG.w_ELCODCOM = this.w_ELCODCOM
      this.w_PROG.w_ELRINNOV = this.w_ELRINNOV
      this.w_OBJ = this.w_PROG.GetcTRL("w_CLISEL")
      this.w_OBJ.Check()     
      this.w_PROG.w_CODMAG = ""
      this.w_PROG.w_DOCINI = this.w_DATINI
      this.w_PROG.w_DOCFIN = iif(not empty(this.w_DATFINDOC), this.w_DATFINDOC, this.w_DATFIN)
      this.w_PROG.w_DATINI = this.w_DATINI
      this.w_PROG.w_DATFIN = iif(not empty(this.w_DATFINDOC), this.w_DATFINDOC, this.w_DATFIN)
      this.w_PROG.w_MAGALT = "X"
      this.w_PROG.NotifyEvent("Lanciattivity")     
    endif
    if this.pOper="XR" and ( (this.w_PADRE.cFunction="Query" or this.w_PADRE.cFunction="Filter") OR ( upper(this.w_PADRE.class)="TGSAG_KDA" OR upper(this.w_PADRE.class)="TGSAG_KDL" ) )
      do case
        case upper(this.w_PADRE.class)="TCGSAG_MEC" or upper(this.w_PADRE.class)="TCGSAG_MDE"
          this.w_CODCON = this.w_PADRE.w_ELCODCLI
          this.w_PADRE.MarkPos()     
          this.w_PADRE.FirstRow()     
          do while Not this.w_PADRE.Eof_Trs()
            this.oParentObject.w_ATTEXIST = .F.
            this.oParentObject.w_DOCEXIST = .F.
            this.w_PADRE.SetRow()     
            this.w_TIPATT = ""
            this.w_SERIAL = this.w_PADRE.w_ELCONTRA
            this.w_ELCODMOD = this.w_PADRE.w_ELCODMOD
            this.w_ELCODIMP = this.w_PADRE.w_ELCODIMP
            this.w_ELCODCOM = this.w_PADRE.w_ELCODCOM
            this.w_ELRINNOV = this.w_PADRE.w_ELRINNOV
            this.w_DATINI = G_DATEUR
            this.w_DATFIN = this.w_PADRE.w_ELDATGAT
            this.w_DATFINDOC = this.w_PADRE.w_ELDATFIN + this.w_PADRE.w_ELGIODOC
            if this.w_PADRE.FullRow() and not empty(this.w_SERIAL)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PADRE.Set("w_ATTEXIST", this.oParentObject.w_ATTEXIST)     
              this.w_PADRE.Set("w_DOCEXIST", this.oParentObject.w_DOCEXIST)     
            endif
            this.w_PADRE.NextRow()     
          enddo
          this.w_PADRE.RePos(.T.)     
        otherwise
          if not empty(this.w_SERIAL)
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
      endcase
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    L_ctrl=this.w_PADRE.GetCtrl(Ah_MsgFormat("\<Vis. Att."))
    L_ctrl2=this.w_PADRE.GetCtrl(Ah_MsgFormat("\<Vis. Doc."))
    * --- Verifica se esistono documenti o attivit� generati da questo contratto
    * --- Select from gsag2kra
    do vq_exec with 'gsag2kra',this,'_Curs_gsag2kra','',.f.,.t.
    if used('_Curs_gsag2kra')
      select _Curs_gsag2kra
      locate for 1=1
      do while not(eof())
      this.w_TIPO = NVL(TIPO," ")
      do case
        case this.w_TIPO="A"
          this.oParentObject.w_ATTEXIST = .T.
          L_ctrl.VISIBLE=.T.
        case this.w_TIPO="D"
          this.oParentObject.w_DOCEXIST = .T.
          L_ctrl2.VISIBLE=.T.
      endcase
        select _Curs_gsag2kra
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DOC_GENE'
    this.cWorkTables[2]='MOD_ELEM'
    this.cWorkTables[3]='ANEVENTI'
    this.cWorkTables[4]='TIP_RAGG'
    this.cWorkTables[5]='TIPEVENT'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_gsag2kra')
      use in _Curs_gsag2kra
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
