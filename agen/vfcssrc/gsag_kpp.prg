* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kpp                                                        *
*              Inserimento provvisorio delle prestazioni                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-16                                                      *
* Last revis.: 2014-02-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kpp",oParentObject))

* --- Class definition
define class tgsag_kpp as StdForm
  Top    = 2
  Left   = 10

  * --- Standard Properties
  Width  = 835
  Height = 573+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-24"
  HelpContextID=118872215
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=126

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  OFF_NOMI_IDX = 0
  ATTIVITA_IDX = 0
  CAN_TIER_IDX = 0
  CENCOST_IDX = 0
  DES_DIVE_IDX = 0
  PAR_AGEN_IDX = 0
  CAUMATTI_IDX = 0
  LISTINI_IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  KEY_ARTI_IDX = 0
  PRA_ENTI_IDX = 0
  PRE_ITER_IDX = 0
  CAM_AGAZ_IDX = 0
  BUSIUNIT_IDX = 0
  cPrg = "gsag_kpp"
  cComment = "Inserimento provvisorio delle prestazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ISAHE = .F.
  w_APCODAZI = space(5)
  w_KEYLISTB = space(10)
  w_PARAMAGE = space(5)
  o_PARAMAGE = space(5)
  w_CODLIS = space(5)
  w_EDITCODPRA = .F.
  o_EDITCODPRA = .F.
  w_PACAUPRE = space(20)
  o_PACAUPRE = space(20)
  w_TipoPersona = space(1)
  w_CODRESP = space(5)
  o_CODRESP = space(5)
  w_LGRUPART = space(5)
  w_CENCOS = space(10)
  o_CENCOS = space(10)
  w_GRUPART = space(5)
  o_GRUPART = space(5)
  w_COGNOME = space(40)
  o_COGNOME = space(40)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_TIPOENTE = space(1)
  w_ENTE = space(10)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_CODSER = space(20)
  w_DesSer = space(40)
  w_FLDTRP = space(10)
  o_FLDTRP = space(10)
  w_CAUDOC = space(5)
  w_NOME = space(40)
  o_NOME = space(40)
  w_RESCHK = 0
  w_SELRAGG = space(10)
  w_DESCRIZ = space(80)
  w_CodPratica = space(20)
  o_CodPratica = space(20)
  w_VISMES = 0
  w_DPTIPRIS = space(1)
  w_DPDESGRU = space(40)
  w_TIPOGRUP = space(1)
  w_PAFLVISI = space(1)
  w_ATDURORE = 0
  o_ATDURORE = 0
  w_ATDURMIN = 0
  o_ATDURMIN = 0
  w_CADTIN = ctot('')
  o_CADTIN = ctot('')
  w_CAORASYS = space(1)
  w_STATUS = space(1)
  w_VOCECR = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CAUACQ = space(5)
  w_FLGLIS = space(1)
  w_FLGCOM = space(1)
  w_PRZVAC = space(1)
  w_FLINTE = space(1)
  w_MVFLVEAC = space(1)
  w_LISACQ = space(5)
  w_FLANAL = space(1)
  w_GENSINGATT = space(1)
  w_SELEZ = space(10)
  o_SELEZ = space(10)
  w_CODPRA = space(20)
  w_Vuota = .F.
  w_HASEVENT = .F.
  w_HASEVCOP = space(50)
  w_FLCCRAUTO = space(1)
  w_DATALIST = ctod('  /  /  ')
  w_PACHKATT = 0
  w_COMODO = space(10)
  w_CODBUN = space(10)
  w_BUFLANAL = space(1)
  w_NODESCRI = space(60)
  w_CNDATFIN = ctod('  /  /  ')
  w_DATAOBSO = ctod('  /  /  ')
  w_CODNIMIN = space(15)
  w_Ordinamento = space(10)
  w_GENPRE = space(1)
  w_NUMSCO = 0
  w_DM_FLCompleta = space(1)
  w_ISALT = .F.
  w_ATCENCOS = space(15)
  o_ATCENCOS = space(15)
  w_ATCODPRA = space(15)
  o_ATCODPRA = space(15)
  w_DAATTIVI = space(15)
  o_DAATTIVI = space(15)
  w_ATCENRIC = space(15)
  o_ATCENRIC = space(15)
  w_ATCOMRIC = space(15)
  o_ATCOMRIC = space(15)
  w_ATATTRIC = space(15)
  o_ATATTRIC = space(15)
  w_DACODNOM = space(15)
  o_DACODNOM = space(15)
  w_ATCODVAL = space(3)
  o_ATCODVAL = space(3)
  w_DATLIS = ctod('  /  /  ')
  o_DATLIS = ctod('  /  /  ')
  w_ATCODLIS = space(5)
  w_ATLISACQ = space(5)
  w_ATPERSON = space(60)
  w_ATSTATUS = space(1)
  w_DESCAN = space(100)
  w_CCDESPIA = space(40)
  w_ATDESCRI = space(30)
  w_NUMRIDEF = 0
  w_CODRES = space(5)
  w_GRURES = space(5)
  w_DATOBSNOM = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPATT = space(10)
  w_CENOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_NOCODCLI = space(15)
  w_DACODSED = space(5)
  w_NOTIPCLI = space(1)
  w_NOTIPNOM = space(1)
  w_OFDATDOC = ctod('  /  /  ')
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_ORAFIN = space(2)
  w_MINFIN = space(2)
  w_DESLIS = space(40)
  w_SCOLIS = space(1)
  w_VALLIS = space(3)
  w_CICLO = space(1)
  w_NULLA = space(10)
  w_DESCOM = space(100)
  w_CCDESRIC = space(40)
  w_ATDESRIC = space(30)
  w_FLSCOR = space(1)
  w_ATTIPCLI = space(1)
  w_NCODLIS = space(5)
  w_NUMLIS = space(5)
  w_DESLISA = space(40)
  w_FINACQ = ctod('  /  /  ')
  w_TIPACQ = space(1)
  w_FLGACQ = space(1)
  w_FLSACQ = ctod('  /  /  ')
  w_INIACQ = ctod('  /  /  ')
  w_VALACQ = space(3)
  w_FLSCOAC = space(1)
  w_IVAACQ = space(1)
  w_MVCODVAL = space(3)
  w_ATCAUDOC = space(5)
  w_ATCAUACQ = space(5)
  w_TIPCLIMIN = space(1)

  * --- Children pointers
  GSAG_MPP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_kpp
  Func ah_HasCPEvents(i_cOp)
  	This.w_HASEVCOP=i_cop
    this.w_HASEVENT=.t.
  	If (isAlt() And Upper(This.cFunction)$"EDIT|LOAD" And Upper(i_cop)='ECPF6')
  		this.gsag_mpr.NotifyEvent('HasEvent')
  		return(this.w_HASEVENT)
  	Else
  		return(.t.)
  	endif
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSAG_MPP additive
    with this
      .Pages(1).addobject("oPag","tgsag_kppPag1","gsag_kpp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsag_kppPag2","gsag_kpp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Default")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODRESP_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSAG_MPP
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[16]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='ATTIVITA'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='CENCOST'
    this.cWorkTables[6]='DES_DIVE'
    this.cWorkTables[7]='PAR_AGEN'
    this.cWorkTables[8]='CAUMATTI'
    this.cWorkTables[9]='LISTINI'
    this.cWorkTables[10]='TIP_DOCU'
    this.cWorkTables[11]='CONTI'
    this.cWorkTables[12]='KEY_ARTI'
    this.cWorkTables[13]='PRA_ENTI'
    this.cWorkTables[14]='PRE_ITER'
    this.cWorkTables[15]='CAM_AGAZ'
    this.cWorkTables[16]='BUSIUNIT'
    return(this.OpenAllTables(16))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSAG_MPP = CREATEOBJECT('stdDynamicChild',this,'GSAG_MPP',this.oPgFrm.Page1.oPag.oLinkPC_1_24)
    this.GSAG_MPP.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAG_MPP)
      this.GSAG_MPP.DestroyChildrenChain()
      this.GSAG_MPP=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_24')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAG_MPP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAG_MPP.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAG_MPP.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAG_MPP.SetKey(;
            .w_CODRESP,"DACODRES";
            ,.w_GRUPART,"DAGRURES";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAG_MPP.ChangeRow(this.cRowID+'      1',1;
             ,.w_CODRESP,"DACODRES";
             ,.w_GRUPART,"DAGRURES";
             )
      .WriteTo_GSAG_MPP()
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAG_MPP)
        i_f=.GSAG_MPP.BuildFilter()
        if !(i_f==.GSAG_MPP.cQueryFilter)
          i_fnidx=.GSAG_MPP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAG_MPP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAG_MPP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAG_MPP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAG_MPP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSAG_MPP()
  if at('gsag_mpp',lower(this.GSAG_MPP.class))<>0
    if this.GSAG_MPP.w_CodPratica<>this.w_CodPratica or this.GSAG_MPP.w_DCODCEN<>this.w_CENCOS or this.GSAG_MPP.w_DATINI<>this.w_DATINI or this.GSAG_MPP.w_DATFIN<>this.w_DATFIN or this.GSAG_MPP.w_EDITCODPRA<>this.w_EDITCODPRA or this.GSAG_MPP.w_CENRIC<>this.w_ATCENRIC or this.GSAG_MPP.w_FLDTRP<>this.w_FLDTRP
      this.GSAG_MPP.w_CodPratica = this.w_CodPratica
      this.GSAG_MPP.w_DCODCEN = this.w_CENCOS
      this.GSAG_MPP.w_DATINI = this.w_DATINI
      this.GSAG_MPP.w_DATFIN = this.w_DATFIN
      this.GSAG_MPP.w_EDITCODPRA = this.w_EDITCODPRA
      this.GSAG_MPP.w_CENRIC = this.w_ATCENRIC
      this.GSAG_MPP.w_FLDTRP = this.w_FLDTRP
      this.GSAG_MPP.mCalc(.t.)
    endif
  endif
  return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSAG_MPP(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        AggioProvv(this,"B")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSAG_MPP.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSAG_MPP(i_ask)
    if this.GSAG_MPP.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP))
      cp_BeginTrs()
      this.GSAG_MPP.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ISAHE=.f.
      .w_APCODAZI=space(5)
      .w_KEYLISTB=space(10)
      .w_PARAMAGE=space(5)
      .w_CODLIS=space(5)
      .w_EDITCODPRA=.f.
      .w_PACAUPRE=space(20)
      .w_TipoPersona=space(1)
      .w_CODRESP=space(5)
      .w_LGRUPART=space(5)
      .w_CENCOS=space(10)
      .w_GRUPART=space(5)
      .w_COGNOME=space(40)
      .w_DATINI=ctod("  /  /  ")
      .w_TIPOENTE=space(1)
      .w_ENTE=space(10)
      .w_DATFIN=ctod("  /  /  ")
      .w_CODSER=space(20)
      .w_DesSer=space(40)
      .w_FLDTRP=space(10)
      .w_CAUDOC=space(5)
      .w_NOME=space(40)
      .w_RESCHK=0
      .w_SELRAGG=space(10)
      .w_DESCRIZ=space(80)
      .w_CodPratica=space(20)
      .w_VISMES=0
      .w_DPTIPRIS=space(1)
      .w_DPDESGRU=space(40)
      .w_TIPOGRUP=space(1)
      .w_PAFLVISI=space(1)
      .w_ATDURORE=0
      .w_ATDURMIN=0
      .w_CADTIN=ctot("")
      .w_CAORASYS=space(1)
      .w_STATUS=space(1)
      .w_VOCECR=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CAUACQ=space(5)
      .w_FLGLIS=space(1)
      .w_FLGCOM=space(1)
      .w_PRZVAC=space(1)
      .w_FLINTE=space(1)
      .w_MVFLVEAC=space(1)
      .w_LISACQ=space(5)
      .w_FLANAL=space(1)
      .w_GENSINGATT=space(1)
      .w_SELEZ=space(10)
      .w_CODPRA=space(20)
      .w_Vuota=.f.
      .w_HASEVENT=.f.
      .w_HASEVCOP=space(50)
      .w_FLCCRAUTO=space(1)
      .w_DATALIST=ctod("  /  /  ")
      .w_PACHKATT=0
      .w_COMODO=space(10)
      .w_CODBUN=space(10)
      .w_BUFLANAL=space(1)
      .w_NODESCRI=space(60)
      .w_CNDATFIN=ctod("  /  /  ")
      .w_DATAOBSO=ctod("  /  /  ")
      .w_CODNIMIN=space(15)
      .w_Ordinamento=space(10)
      .w_GENPRE=space(1)
      .w_NUMSCO=0
      .w_DM_FLCompleta=space(1)
      .w_ISALT=.f.
      .w_ATCENCOS=space(15)
      .w_ATCODPRA=space(15)
      .w_DAATTIVI=space(15)
      .w_ATCENRIC=space(15)
      .w_ATCOMRIC=space(15)
      .w_ATATTRIC=space(15)
      .w_DACODNOM=space(15)
      .w_ATCODVAL=space(3)
      .w_DATLIS=ctod("  /  /  ")
      .w_ATCODLIS=space(5)
      .w_ATLISACQ=space(5)
      .w_ATPERSON=space(60)
      .w_ATSTATUS=space(1)
      .w_DESCAN=space(100)
      .w_CCDESPIA=space(40)
      .w_ATDESCRI=space(30)
      .w_NUMRIDEF=0
      .w_CODRES=space(5)
      .w_GRURES=space(5)
      .w_DATOBSNOM=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPATT=space(10)
      .w_CENOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_NOCODCLI=space(15)
      .w_DACODSED=space(5)
      .w_NOTIPCLI=space(1)
      .w_NOTIPNOM=space(1)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_ORAINI=space(2)
      .w_MININI=space(2)
      .w_ORAFIN=space(2)
      .w_MINFIN=space(2)
      .w_DESLIS=space(40)
      .w_SCOLIS=space(1)
      .w_VALLIS=space(3)
      .w_CICLO=space(1)
      .w_NULLA=space(10)
      .w_DESCOM=space(100)
      .w_CCDESRIC=space(40)
      .w_ATDESRIC=space(30)
      .w_FLSCOR=space(1)
      .w_ATTIPCLI=space(1)
      .w_NCODLIS=space(5)
      .w_NUMLIS=space(5)
      .w_DESLISA=space(40)
      .w_FINACQ=ctod("  /  /  ")
      .w_TIPACQ=space(1)
      .w_FLGACQ=space(1)
      .w_FLSACQ=ctod("  /  /  ")
      .w_INIACQ=ctod("  /  /  ")
      .w_VALACQ=space(3)
      .w_FLSCOAC=space(1)
      .w_IVAACQ=space(1)
      .w_MVCODVAL=space(3)
      .w_ATCAUDOC=space(5)
      .w_ATCAUACQ=space(5)
      .w_TIPCLIMIN=space(1)
        .w_ISAHE = IsAHE()
        .w_APCODAZI = i_CODAZI
        .w_KEYLISTB = SYS(2015)
        .w_PARAMAGE = .w_APCODAZI
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PARAMAGE))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_EDITCODPRA = .T.
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_PACAUPRE))
          .link_1_7('Full')
        endif
        .w_TipoPersona = 'P'
        .w_CODRESP = ReadDipend(i_codute, "C")
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODRESP))
          .link_1_10('Full')
        endif
          .DoRTCalc(10,11,.f.)
        .w_GRUPART = .w_LGRUPART
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_GRUPART))
          .link_1_13('Full')
        endif
        .DoRTCalc(13,16,.f.)
        if not(empty(.w_ENTE))
          .link_1_17('Full')
        endif
        .DoRTCalc(17,21,.f.)
        if not(empty(.w_CAUDOC))
          .link_1_22('Full')
        endif
      .GSAG_MPP.NewDocument()
      .GSAG_MPP.ChangeRow('1',1,.w_CODRESP,"DACODRES",.w_GRUPART,"DAGRURES")
      if not(.GSAG_MPP.bLoaded)
        .GSAG_MPP.SetKey(.w_CODRESP,"DACODRES",.w_GRUPART,"DAGRURES")
      endif
          .DoRTCalc(22,22,.f.)
        .w_RESCHK = 0
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_SELRAGG))
          .link_1_29('Full')
        endif
        .w_DESCRIZ = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
          .DoRTCalc(26,26,.f.)
        .w_VISMES = 0
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate(IIF(IsAlt(), "Codice responsabile", "Codice partecipante"))
          .DoRTCalc(28,36,.f.)
        .w_VOCECR = Docgesana(.w_CAUDOC,'V')
        .w_OB_TEST = i_datsys
      .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
          .DoRTCalc(39,40,.f.)
        .w_FLGLIS = 'N'
        .w_FLGCOM = Docgesana(.w_CAUDOC,'C')
          .DoRTCalc(43,48,.f.)
        .w_SELEZ = 'S'
        .w_CODPRA = .w_codpratica
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_CODPRA))
          .link_1_70('Full')
        endif
          .DoRTCalc(51,53,.f.)
        .w_FLCCRAUTO = Docgesana(.w_CAUDOC,'R')
      .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
          .DoRTCalc(55,57,.f.)
        .w_CODBUN = g_CODBUN
        .DoRTCalc(58,58,.f.)
        if not(empty(.w_CODBUN))
          .link_1_79('Full')
        endif
        .DoRTCalc(59,63,.f.)
        if not(empty(.w_CODNIMIN))
          .link_1_84('Full')
        endif
        .w_Ordinamento = 'N'
        .w_GENPRE = 'N'
          .DoRTCalc(66,66,.f.)
        .w_DM_FLCompleta = 'N'
        .w_ISALT = IsAlt()
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_ATCENCOS))
          .link_2_1('Full')
        endif
        .DoRTCalc(70,70,.f.)
        if not(empty(.w_ATCODPRA))
          .link_2_2('Full')
        endif
        .w_DAATTIVI = SPACE(15)
        .DoRTCalc(71,71,.f.)
        if not(empty(.w_DAATTIVI))
          .link_2_3('Full')
        endif
        .DoRTCalc(72,72,.f.)
        if not(empty(.w_ATCENRIC))
          .link_2_4('Full')
        endif
        .DoRTCalc(73,73,.f.)
        if not(empty(.w_ATCOMRIC))
          .link_2_5('Full')
        endif
        .w_ATATTRIC = SPACE(15)
        .DoRTCalc(74,74,.f.)
        if not(empty(.w_ATATTRIC))
          .link_2_6('Full')
        endif
        .DoRTCalc(75,75,.f.)
        if not(empty(.w_DACODNOM))
          .link_2_7('Full')
        endif
        .w_ATCODVAL = g_PERVAL
        .w_DATLIS = i_datsys
        .w_ATCODLIS = IIF(EMPTY(.w_NUMLIS), IIF(Not Empty(.w_NCODLIS),.w_NCODLIS,.w_CODLIS), .w_NUMLIS)
        .DoRTCalc(78,78,.f.)
        if not(empty(.w_ATCODLIS))
          .link_2_10('Full')
        endif
        .w_ATLISACQ = IIF(EMPTY(.w_NUMLIS), IIF(Empty(.w_NCODLIS), .w_LISACQ, .w_NCODLIS), .w_NUMLIS)
        .DoRTCalc(79,79,.f.)
        if not(empty(.w_ATLISACQ))
          .link_2_11('Full')
        endif
          .DoRTCalc(80,80,.f.)
        .w_ATSTATUS = .w_STATUS
          .DoRTCalc(82,84,.f.)
        .w_NUMRIDEF = -1
        .w_CODRES = 'XXXXX'
        .w_GRURES = 'XXXXX'
          .DoRTCalc(88,89,.f.)
        .w_TIPATT = 'A'
          .DoRTCalc(91,91,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(93,93,.f.)
        if not(empty(.w_NOCODCLI))
          .link_2_29('Full')
        endif
        .w_DACODSED = SPACE(5)
        .DoRTCalc(94,94,.f.)
        if not(empty(.w_DACODSED))
          .link_2_30('Full')
        endif
          .DoRTCalc(95,96,.f.)
        .w_OFDATDOC = i_DatSys
        .w_ORAINI = PADL(ALLTRIM(STR(HOUR(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        .w_MININI = PADL(ALLTRIM(STR(MINUTE(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        .w_ORAFIN = RIGHT('00'+ALLTRIM(STR(MOD((VAL(.w_ORAINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)), 24))),2)
        .w_MINFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_MININI)+.w_ATDURMIN), 60),2)),2)
          .DoRTCalc(102,104,.f.)
        .w_CICLO = 'E'
      .oPgFrm.Page2.oPag.oObj_2_49.Calculate()
        .w_NULLA = 'X'
          .DoRTCalc(107,110,.f.)
        .w_ATTIPCLI = .w_NOTIPCLI
          .DoRTCalc(112,122,.f.)
        .w_MVCODVAL = g_PERVAL
        .w_ATCAUDOC = .w_CAUDOC
        .w_ATCAUACQ = .w_CAUACQ
    endwith
    this.DoRTCalc(126,126,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_46.enabled = this.oPgFrm.Page2.oPag.oBtn_2_46.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSAG_MPP.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAG_MPP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_PARAMAGE<>.w_PARAMAGE
          .link_1_4('Full')
        endif
        .DoRTCalc(5,6,.t.)
          .link_1_7('Full')
        .DoRTCalc(8,11,.t.)
        if .o_CODRESP<>.w_CODRESP
            .w_GRUPART = .w_LGRUPART
          .link_1_13('Full')
        endif
        .DoRTCalc(13,15,.t.)
          .link_1_17('Full')
        .DoRTCalc(17,20,.t.)
          .link_1_22('Full')
        if  .o_CodPratica<>.w_CodPratica.or. .o_CENCOS<>.w_CENCOS.or. .o_DATINI<>.w_DATINI.or. .o_DATFIN<>.w_DATFIN.or. .o_EDITCODPRA<>.w_EDITCODPRA.or. .o_ATCENRIC<>.w_ATCENRIC.or. .o_FLDTRP<>.w_FLDTRP
          .WriteTo_GSAG_MPP()
        endif
        .DoRTCalc(22,23,.t.)
          .link_1_29('Full')
        if .o_CODRESP<>.w_CODRESP.or. .o_COGNOME<>.w_COGNOME.or. .o_NOME<>.w_NOME
            .w_DESCRIZ = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        endif
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(IIF(IsAlt(), "Codice responsabile", "Codice partecipante"))
        .DoRTCalc(26,36,.t.)
            .w_VOCECR = Docgesana(.w_CAUDOC,'V')
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .DoRTCalc(38,41,.t.)
            .w_FLGCOM = Docgesana(.w_CAUDOC,'C')
        if .o_SELEZ<>.w_SELEZ
          .Calculate_REMLWYCYIM()
        endif
        .DoRTCalc(43,49,.t.)
        if .o_codpratica<>.w_codpratica
            .w_CODPRA = .w_codpratica
          .link_1_70('Full')
        endif
        .DoRTCalc(51,53,.t.)
            .w_FLCCRAUTO = Docgesana(.w_CAUDOC,'R')
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .DoRTCalc(55,57,.t.)
          .link_1_79('Full')
        .DoRTCalc(59,70,.t.)
        if .o_ATCODPRA<>.w_ATCODPRA
            .w_DAATTIVI = SPACE(15)
          .link_2_3('Full')
        endif
        .DoRTCalc(72,73,.t.)
        if .o_ATCOMRIC<>.w_ATCOMRIC
            .w_ATATTRIC = SPACE(15)
          .link_2_6('Full')
        endif
        .DoRTCalc(75,77,.t.)
        if .o_ATCODPRA<>.w_ATCODPRA.or. .o_ATCODVAL<>.w_ATCODVAL.or. .o_DACODNOM<>.w_DACODNOM
            .w_ATCODLIS = IIF(EMPTY(.w_NUMLIS), IIF(Not Empty(.w_NCODLIS),.w_NCODLIS,.w_CODLIS), .w_NUMLIS)
          .link_2_10('Full')
        endif
        if .o_ATCODVAL<>.w_ATCODVAL
            .w_ATLISACQ = IIF(EMPTY(.w_NUMLIS), IIF(Empty(.w_NCODLIS), .w_LISACQ, .w_NCODLIS), .w_NUMLIS)
          .link_2_11('Full')
        endif
        .DoRTCalc(80,80,.t.)
        if .o_PACAUPRE<>.w_PACAUPRE
            .w_ATSTATUS = .w_STATUS
        endif
        .DoRTCalc(82,89,.t.)
            .w_TIPATT = 'A'
        .DoRTCalc(91,91,.t.)
            .w_OBTEST = i_DATSYS
          .link_2_29('Full')
        if .o_DACODNOM<>.w_DACODNOM
            .w_DACODSED = SPACE(5)
          .link_2_30('Full')
        endif
        .DoRTCalc(95,97,.t.)
        if .o_CADTIN<>.w_CADTIN
            .w_ORAINI = PADL(ALLTRIM(STR(HOUR(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        endif
        if .o_CADTIN<>.w_CADTIN
            .w_MININI = PADL(ALLTRIM(STR(MINUTE(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        endif
        if .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI.or. .o_ATDURORE<>.w_ATDURORE.or. .o_ATDURMIN<>.w_ATDURMIN
            .w_ORAFIN = RIGHT('00'+ALLTRIM(STR(MOD((VAL(.w_ORAINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)), 24))),2)
        endif
        if .o_MININI<>.w_MININI.or. .o_ATDURMIN<>.w_ATDURMIN
            .w_MINFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_MININI)+.w_ATDURMIN), 60),2)),2)
        endif
        .oPgFrm.Page2.oPag.oObj_2_49.Calculate()
        .DoRTCalc(102,105,.t.)
        if .o_DATLIS<>.w_DATLIS.or. .o_PACAUPRE<>.w_PACAUPRE
            .w_NULLA = 'X'
        endif
        .DoRTCalc(107,110,.t.)
        if .o_DACODNOM<>.w_DACODNOM
            .w_ATTIPCLI = .w_NOTIPCLI
        endif
        .DoRTCalc(112,123,.t.)
            .w_ATCAUDOC = .w_CAUDOC
            .w_ATCAUACQ = .w_CAUACQ
        if .o_ATCENCOS<>.w_ATCENCOS
          .Calculate_ATRHLQSOIB()
        endif
        if .o_ATCENRIC<>.w_ATCENRIC
          .Calculate_LKWTWVRCIM()
        endif
        if .o_ATCODPRA<>.w_ATCODPRA
          .Calculate_GRHMXUTBGE()
        endif
        if .o_ATCOMRIC<>.w_ATCOMRIC
          .Calculate_BBBMDLAJSR()
        endif
        if .o_DAATTIVI<>.w_DAATTIVI
          .Calculate_CISCBKOBPM()
        endif
        if .o_ATATTRIC<>.w_ATATTRIC
          .Calculate_WYSQLBAMYK()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_CODRESP<>.o_CODRESP or .w_GRUPART<>.o_GRUPART
          .Save_GSAG_MPP(.t.)
          .GSAG_MPP.NewDocument()
          .GSAG_MPP.ChangeRow('1',1,.w_CODRESP,"DACODRES",.w_GRUPART,"DAGRURES")
          if not(.GSAG_MPP.bLoaded)
            .GSAG_MPP.SetKey(.w_CODRESP,"DACODRES",.w_GRUPART,"DAGRURES")
          endif
        endif
      endwith
      this.DoRTCalc(126,126,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate(IIF(IsAlt(), "Codice responsabile", "Codice partecipante"))
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_49.Calculate()
    endwith
  return

  proc Calculate_WRRIDXBOZM()
    with this
          * --- GSAG_BCK(A) - Controlli a checkform
          GSAG_BCK(this;
              ,'A';
             )
    endwith
  endproc
  proc Calculate_LIXQOEHHNT()
    with this
          * --- GSAG_BCK(A) - Controlli a checkform
          GSAG_BCK(this;
              ,'A';
             )
    endwith
  endproc
  proc Calculate_REMLWYCYIM()
    with this
          * --- GSAG_BCK(F) - Modificato flag di spunta
          GSAG_BCK(this;
              ,'F';
             )
    endwith
  endproc
  proc Calculate_AUHWFDEDUT()
    with this
          * --- Aggiorna prima di ricerca
          .w_Vuota = AggioProvv(this,'Z')
    endwith
  endproc
  proc Calculate_ATRHLQSOIB()
    with this
          * --- DaCostoaRicavo
          .w_ATCENRIC = IIF(Empty(.w_ATCENRIC) ,.w_ATCENCOS,.w_ATCENRIC)
          .link_2_4('Full')
    endwith
  endproc
  proc Calculate_LKWTWVRCIM()
    with this
          * --- DaRicavoaCosto
          .w_ATCENCOS = IIF(Empty(.w_ATCENCOS) AND g_COAN = 'S',.w_ATCENRIC,.w_ATCENCOS)
          .link_2_1('Full')
    endwith
  endproc
  proc Calculate_GRHMXUTBGE()
    with this
          * --- DaCommessaCostoaRicavo
          .w_ATCOMRIC = IIF(Empty(.w_ATCOMRIC),.w_ATCODPRA,.w_ATCOMRIC)
          .link_2_5('Full')
    endwith
  endproc
  proc Calculate_BBBMDLAJSR()
    with this
          * --- DaCommessaRicavoaCosto
          .w_ATCODPRA = IIF(Empty(.w_ATCODPRA),.w_ATCOMRIC,.w_ATCODPRA)
          .link_2_2('Full')
    endwith
  endproc
  proc Calculate_CISCBKOBPM()
    with this
          * --- DaAttivitaCostoaRicavo
          .w_ATATTRIC = IIF(Empty(.w_ATATTRIC) AND .w_ATCOMRIC=.w_ATCODPRA, .w_DAATTIVI, .w_ATATTRIC)
          .link_2_6('Full')
    endwith
  endproc
  proc Calculate_WYSQLBAMYK()
    with this
          * --- DaAttivitaRicavoaCosto
          .w_DAATTIVI = IIF(Empty(.w_DAATTIVI) AND .w_ATCOMRIC=.w_ATCODPRA, .w_ATATTRIC, .w_DAATTIVI)
          .link_2_3('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oATCODPRA_2_2.enabled = this.oPgFrm.Page2.oPag.oATCODPRA_2_2.mCond()
    this.oPgFrm.Page2.oPag.oDAATTIVI_2_3.enabled = this.oPgFrm.Page2.oPag.oDAATTIVI_2_3.mCond()
    this.oPgFrm.Page2.oPag.oATCOMRIC_2_5.enabled = this.oPgFrm.Page2.oPag.oATCOMRIC_2_5.mCond()
    this.oPgFrm.Page2.oPag.oATATTRIC_2_6.enabled = this.oPgFrm.Page2.oPag.oATATTRIC_2_6.mCond()
    this.GSAG_MPP.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(isalt())
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Default"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.oPgFrm.Page2.oPag.oDATLIS_2_9.visible=!this.oPgFrm.Page2.oPag.oDATLIS_2_9.mHide()
    this.oPgFrm.Page2.oPag.oATCODLIS_2_10.visible=!this.oPgFrm.Page2.oPag.oATCODLIS_2_10.mHide()
    this.oPgFrm.Page2.oPag.oCCDESPIA_2_16.visible=!this.oPgFrm.Page2.oPag.oCCDESPIA_2_16.mHide()
    this.oPgFrm.Page2.oPag.oDESLIS_2_43.visible=!this.oPgFrm.Page2.oPag.oDESLIS_2_43.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    this.oPgFrm.Page2.oPag.oCCDESRIC_2_53.visible=!this.oPgFrm.Page2.oPag.oCCDESRIC_2_53.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_71.visible=!this.oPgFrm.Page2.oPag.oStr_2_71.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_WRRIDXBOZM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_LIXQOEHHNT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AggiornaXRicerca")
          .Calculate_AUHWFDEDUT()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_49.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kpp
    if cevent='w_SELRAGG Changed' and Not Empty(This.w_SELRAGG)
       Local l_obj
       l_obj=GSAG_KPM(This)
       l_obj.Ecpsave()
       l_obj=.Null.
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PARAMAGE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PARAMAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PARAMAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLVISI,PACAUPRE,PACODLIS,PALISACQ,PAFLSIAT,PACHKATT,PAFLDTRP";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PARAMAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PARAMAGE)
            select PACODAZI,PAFLVISI,PACAUPRE,PACODLIS,PALISACQ,PAFLSIAT,PACHKATT,PAFLDTRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PARAMAGE = NVL(_Link_.PACODAZI,space(5))
      this.w_PAFLVISI = NVL(_Link_.PAFLVISI,space(1))
      this.w_PACAUPRE = NVL(_Link_.PACAUPRE,space(20))
      this.w_CODLIS = NVL(_Link_.PACODLIS,space(5))
      this.w_LISACQ = NVL(_Link_.PALISACQ,space(5))
      this.w_GENSINGATT = NVL(_Link_.PAFLSIAT,space(1))
      this.w_PACHKATT = NVL(_Link_.PACHKATT,0)
      this.w_FLDTRP = NVL(_Link_.PAFLDTRP,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PARAMAGE = space(5)
      endif
      this.w_PAFLVISI = space(1)
      this.w_PACAUPRE = space(20)
      this.w_CODLIS = space(5)
      this.w_LISACQ = space(5)
      this.w_GENSINGATT = space(1)
      this.w_PACHKATT = 0
      this.w_FLDTRP = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PARAMAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACAUPRE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACAUPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACAUPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CASTAATT,CADURORE,CADURMIN,CADATINI,CAORASYS,CACAUACQ,CACAUDOC,CAFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PACAUPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PACAUPRE)
            select CACODICE,CASTAATT,CADURORE,CADURMIN,CADATINI,CAORASYS,CACAUACQ,CACAUDOC,CAFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACAUPRE = NVL(_Link_.CACODICE,space(20))
      this.w_STATUS = NVL(_Link_.CASTAATT,space(1))
      this.w_ATDURORE = NVL(_Link_.CADURORE,0)
      this.w_ATDURMIN = NVL(_Link_.CADURMIN,0)
      this.w_CADTIN = NVL(_Link_.CADATINI,ctot(""))
      this.w_CAORASYS = NVL(_Link_.CAORASYS,space(1))
      this.w_CAUACQ = NVL(_Link_.CACAUACQ,space(5))
      this.w_CAUDOC = NVL(_Link_.CACAUDOC,space(5))
      this.w_FLANAL = NVL(_Link_.CAFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PACAUPRE = space(20)
      endif
      this.w_STATUS = space(1)
      this.w_ATDURORE = 0
      this.w_ATDURMIN = 0
      this.w_CADTIN = ctot("")
      this.w_CAORASYS = space(1)
      this.w_CAUACQ = space(5)
      this.w_CAUDOC = space(5)
      this.w_FLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACAUPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRESP
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRESP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODRESP)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_CODRESP))
          select DPCODICE,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPGRUPRE,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODRESP)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODRESP)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODRESP)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODRESP)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODRESP)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODRESP) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oCODRESP_1_10'),i_cWhere,'GSAR_BDZ',"Partecipanti",'GSAG_KPR.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRESP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODRESP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODRESP)
            select DPCODICE,DPCOGNOM,DPNOME,DPCODCEN,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRESP = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOME = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME = NVL(_Link_.DPNOME,space(40))
      this.w_CENCOS = NVL(_Link_.DPCODCEN,space(10))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_LGRUPART = NVL(_Link_.DPGRUPRE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODRESP = space(5)
      endif
      this.w_COGNOME = space(40)
      this.w_NOME = space(40)
      this.w_CENCOS = space(10)
      this.w_DPTIPRIS = space(1)
      this.w_LGRUPART = space(5)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODRESP) OR .w_TipoPersona=.w_DPTIPRIS) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODRESP = space(5)
        this.w_COGNOME = space(40)
        this.w_NOME = space(40)
        this.w_CENCOS = space(10)
        this.w_DPTIPRIS = space(1)
        this.w_LGRUPART = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRESP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPART
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_GRUPART))
          select DPCODICE,DPTIPRIS,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oGRUPART_1_13'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPART)
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPART = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPOGRUP = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DPDESGRU = NVL(_Link_.DPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPART = space(5)
      endif
      this.w_TIPOGRUP = space(1)
      this.w_DPDESGRU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODRESP , .w_GRUPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo inesistente o non associato al partecipante o obsoleto")
        endif
        this.w_GRUPART = space(5)
        this.w_TIPOGRUP = space(1)
        this.w_DPDESGRU = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ENTE
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EP__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_ENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_ENTE)
            select EPCODICE,EP__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ENTE = NVL(_Link_.EPCODICE,space(10))
      this.w_TIPOENTE = NVL(_Link_.EP__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ENTE = space(10)
      endif
      this.w_TIPOENTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUDOC
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDPRZVAC,TDFLINTE,TDFLVEAC,TDNUMSCO";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUDOC)
            select TDTIPDOC,TDPRZVAC,TDFLINTE,TDFLVEAC,TDNUMSCO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_PRZVAC = NVL(_Link_.TDPRZVAC,space(1))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_NUMSCO = NVL(_Link_.TDNUMSCO,0)
    else
      if i_cCtrl<>'Load'
        this.w_CAUDOC = space(5)
      endif
      this.w_PRZVAC = space(1)
      this.w_FLINTE = space(1)
      this.w_MVFLVEAC = space(1)
      this.w_NUMSCO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELRAGG
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_lTable = "PRE_ITER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2], .t., this.PRE_ITER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELRAGG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELRAGG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(this.w_SELRAGG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',this.w_SELRAGG)
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELRAGG = NVL(_Link_.ITCODICE,space(10))
      this.w_COMODO = NVL(_Link_.ITDESCRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SELRAGG = space(10)
      endif
      this.w_COMODO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])+'\'+cp_ToStr(_Link_.ITCODICE,1)
      cp_ShowWarn(i_cKey,this.PRE_ITER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELRAGG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_1_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUN
  func Link_1_79(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_APCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_APCODAZI;
                       ,'BUCODICE',this.w_CODBUN)
            select BUCODAZI,BUCODICE,BUFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUN = NVL(_Link_.BUCODICE,space(10))
      this.w_BUFLANAL = NVL(_Link_.BUFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUN = space(10)
      endif
      this.w_BUFLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNIMIN
  func Link_1_84(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNIMIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNIMIN)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOTIPCLI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNIMIN))
          select NOCODICE,NODESCRI,NOTIPCLI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNIMIN)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNIMIN)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOTIPCLI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNIMIN)+"%");

            select NOCODICE,NODESCRI,NOTIPCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNIMIN) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNIMIN_1_84'),i_cWhere,'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOTIPCLI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NOTIPCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNIMIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOTIPCLI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNIMIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNIMIN)
            select NOCODICE,NODESCRI,NOTIPCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNIMIN = NVL(_Link_.NOCODICE,space(15))
      this.w_NODESCRI = NVL(_Link_.NODESCRI,space(60))
      this.w_TIPCLIMIN = NVL(_Link_.NOTIPCLI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODNIMIN = space(15)
      endif
      this.w_NODESCRI = space(60)
      this.w_TIPCLIMIN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPCLIMIN<>'G' AND .w_TIPCLIMIN<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNIMIN = space(15)
        this.w_NODESCRI = space(60)
        this.w_TIPCLIMIN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNIMIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCENCOS
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ATCENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ATCENCOS))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oATCENCOS_2_1'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ATCENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ATCENCOS)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCENCOS = space(15)
      endif
      this.w_CCDESPIA = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
        endif
        this.w_ATCENCOS = space(15)
        this.w_CCDESPIA = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODPRA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZZ',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ATCODPRA))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_ATCODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oATCODPRA_2_2'),i_cWhere,'GSAR_BZZ',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ATCODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ATCODPRA)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(100))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODPRA = space(15)
      endif
      this.w_DESCAN = space(100)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice Commessa obsoleto")
        endif
        this.w_ATCODPRA = space(15)
        this.w_DESCAN = space(100)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAATTIVI
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAATTIVI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_DAATTIVI)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCODPRA);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_ATCODPRA;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_DAATTIVI))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAATTIVI)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAATTIVI) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oDAATTIVI_2_3'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODPRA<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCODPRA);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAATTIVI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_DAATTIVI);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCODPRA);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_ATCODPRA;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_DAATTIVI)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAATTIVI = NVL(_Link_.ATCODATT,space(15))
      this.w_ATDESCRI = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DAATTIVI = space(15)
      endif
      this.w_ATDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAATTIVI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCENRIC
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCENRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ATCENRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ATCENRIC))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCENRIC)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCENRIC) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oATCENRIC_2_4'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCENRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ATCENRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ATCENRIC)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCENRIC = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESRIC = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCENRIC = space(15)
      endif
      this.w_CCDESRIC = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
        endif
        this.w_ATCENRIC = space(15)
        this.w_CCDESRIC = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCENRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCOMRIC
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCOMRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZZ',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ATCOMRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ATCOMRIC))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCOMRIC)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_ATCOMRIC)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_ATCOMRIC)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCOMRIC) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oATCOMRIC_2_5'),i_cWhere,'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCOMRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ATCOMRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ATCOMRIC)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCOMRIC = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(100))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCOMRIC = space(15)
      endif
      this.w_DESCOM = space(100)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice Commessa obsoleto")
        endif
        this.w_ATCOMRIC = space(15)
        this.w_DESCOM = space(100)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCOMRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATATTRIC
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATATTRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATATTRIC)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCOMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_ATCOMRIC;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_ATATTRIC))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATATTRIC)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATATTRIC) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATATTRIC_2_6'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCOMRIC<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCOMRIC);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATATTRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATATTRIC);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCOMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_ATCOMRIC;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_ATATTRIC)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATATTRIC = NVL(_Link_.ATCODATT,space(15))
      this.w_ATDESRIC = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ATATTRIC = space(15)
      endif
      this.w_ATDESRIC = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATATTRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODNOM
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_DACODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOCODCLI,NOTIPCLI,NOTIPNOM,NONUMLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_DACODNOM))
          select NOCODICE,NODESCRI,NODTOBSO,NOCODCLI,NOTIPCLI,NOTIPNOM,NONUMLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_DACODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOCODCLI,NOTIPCLI,NOTIPNOM,NONUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_DACODNOM)+"%");

            select NOCODICE,NODESCRI,NODTOBSO,NOCODCLI,NOTIPCLI,NOTIPNOM,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oDACODNOM_2_7'),i_cWhere,'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOCODCLI,NOTIPCLI,NOTIPNOM,NONUMLIS";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODTOBSO,NOCODCLI,NOTIPCLI,NOTIPNOM,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODTOBSO,NOCODCLI,NOTIPCLI,NOTIPNOM,NONUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_DACODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_DACODNOM)
            select NOCODICE,NODESCRI,NODTOBSO,NOCODCLI,NOTIPCLI,NOTIPNOM,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_ATPERSON = NVL(_Link_.NODESCRI,space(60))
      this.w_DATOBSNOM = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_NOCODCLI = NVL(_Link_.NOCODCLI,space(15))
      this.w_NOTIPCLI = NVL(_Link_.NOTIPCLI,space(1))
      this.w_NOTIPNOM = NVL(_Link_.NOTIPNOM,space(1))
      this.w_NCODLIS = NVL(_Link_.NONUMLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DACODNOM = space(15)
      endif
      this.w_ATPERSON = space(60)
      this.w_DATOBSNOM = ctod("  /  /  ")
      this.w_NOCODCLI = space(15)
      this.w_NOTIPCLI = space(1)
      this.w_NOTIPNOM = space(1)
      this.w_NCODLIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DATOBSNOM>.w_OBTEST OR EMPTY(.w_DATOBSNOM)) AND .w_NOTIPNOM<>'G' AND .w_NOTIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DACODNOM = space(15)
        this.w_ATPERSON = space(60)
        this.w_DATOBSNOM = ctod("  /  /  ")
        this.w_NOCODCLI = space(15)
        this.w_NOTIPCLI = space(1)
        this.w_NOTIPNOM = space(1)
        this.w_NCODLIS = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODLIS
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ATCODLIS)+"%");
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);

          i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSVALLIS,LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSVALLIS',this.w_ATCODVAL;
                     ,'LSCODLIS',trim(this.w_ATCODLIS))
          select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSVALLIS,LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(oSource.parent,'oATCODLIS_2_10'),i_cWhere,'',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODVAL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valuta listino incongruente o listino gestito a sconti")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',oSource.xKey(1);
                       ,'LSCODLIS',oSource.xKey(2))
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ATCODLIS);
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',this.w_ATCODVAL;
                       ,'LSCODLIS',this.w_ATCODLIS)
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_SCOLIS = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_SCOLIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VALLIS=.w_ATCODVAL 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta listino incongruente o listino gestito a sconti")
        endif
        this.w_ATCODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_VALLIS = space(3)
        this.w_SCOLIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSVALLIS,1)+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATLISACQ
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATLISACQ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ATLISACQ)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ATLISACQ))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATLISACQ)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATLISACQ) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oATLISACQ_2_11'),i_cWhere,'',"Listini",'listdocu.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATLISACQ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ATLISACQ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ATLISACQ)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSVALLIS,LSDTINVA,LSDTOBSO,LSTIPLIS,LSFLGCIC,LSFLSTAT,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATLISACQ = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLISA = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVAACQ = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALACQ = NVL(_Link_.LSVALLIS,space(3))
      this.w_INIACQ = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINACQ = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_TIPACQ = NVL(_Link_.LSTIPLIS,space(1))
      this.w_FLGACQ = NVL(_Link_.LSFLGCIC,space(1))
      this.w_FLSACQ = NVL(cp_ToDate(_Link_.LSFLSTAT),ctod("  /  /  "))
      this.w_FLSCOAC = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATLISACQ = space(5)
      endif
      this.w_DESLISA = space(40)
      this.w_IVAACQ = space(1)
      this.w_VALACQ = space(3)
      this.w_INIACQ = ctod("  /  /  ")
      this.w_FINACQ = ctod("  /  /  ")
      this.w_TIPACQ = space(1)
      this.w_FLGACQ = space(1)
      this.w_FLSACQ = ctod("  /  /  ")
      this.w_FLSCOAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_Isahe and CHKLISD(.w_ATLISACQ, 'L', .w_VALACQ, .w_INIACQ, .w_FINACQ,.w_TIPACQ,'S',.w_FLGACQ, .w_FLSACQ,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'A','',.w_FLSCOAC)) or (Isahr() and .w_FLSCOAC <>'S' AND .w_IVAACQ<>'L' AND CHKLISD(.w_ATLISACQ,.w_IVAACQ,.w_VALACQ,.w_INIACQ,.w_FINACQ,'N', .w_ATCODVAL, .w_DATINI))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta listino incongruente o listino gestito a sconti")
        endif
        this.w_ATLISACQ = space(5)
        this.w_DESLISA = space(40)
        this.w_IVAACQ = space(1)
        this.w_VALACQ = space(3)
        this.w_INIACQ = ctod("  /  /  ")
        this.w_FINACQ = ctod("  /  /  ")
        this.w_TIPACQ = space(1)
        this.w_FLGACQ = space(1)
        this.w_FLSACQ = ctod("  /  /  ")
        this.w_FLSCOAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATLISACQ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCODCLI
  func Link_2_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_NOCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_NOTIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_NOTIPCLI;
                       ,'ANCODICE',this.w_NOCODCLI)
            select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_FLSCOR = NVL(_Link_.ANSCORPO,space(1))
      this.w_NUMLIS = NVL(_Link_.ANNUMLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODCLI = space(15)
      endif
      this.w_FLSCOR = space(1)
      this.w_NUMLIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODSED
  func Link_2_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_DACODSED);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_NOTIPCLI);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_NOCODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_NOTIPCLI;
                       ,'DDCODICE',this.w_NOCODCLI;
                       ,'DDCODDES',this.w_DACODSED)
            select DDTIPCON,DDCODICE,DDCODDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODSED = NVL(_Link_.DDCODDES,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DACODSED = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODRESP_1_10.value==this.w_CODRESP)
      this.oPgFrm.Page1.oPag.oCODRESP_1_10.value=this.w_CODRESP
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUPART_1_13.value==this.w_GRUPART)
      this.oPgFrm.Page1.oPag.oGRUPART_1_13.value=this.w_GRUPART
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_15.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_15.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_18.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_18.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSER_1_19.value==this.w_CODSER)
      this.oPgFrm.Page1.oPag.oCODSER_1_19.value=this.w_CODSER
    endif
    if not(this.oPgFrm.Page1.oPag.oDesSer_1_20.value==this.w_DesSer)
      this.oPgFrm.Page1.oPag.oDesSer_1_20.value=this.w_DesSer
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIZ_1_32.value==this.w_DESCRIZ)
      this.oPgFrm.Page1.oPag.oDESCRIZ_1_32.value=this.w_DESCRIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDESGRU_1_36.value==this.w_DPDESGRU)
      this.oPgFrm.Page1.oPag.oDPDESGRU_1_36.value=this.w_DPDESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oGENSINGATT_1_58.RadioValue()==this.w_GENSINGATT)
      this.oPgFrm.Page1.oPag.oGENSINGATT_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNODESCRI_1_81.value==this.w_NODESCRI)
      this.oPgFrm.Page1.oPag.oNODESCRI_1_81.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNIMIN_1_84.value==this.w_CODNIMIN)
      this.oPgFrm.Page1.oPag.oCODNIMIN_1_84.value=this.w_CODNIMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOrdinamento_1_85.RadioValue()==this.w_Ordinamento)
      this.oPgFrm.Page1.oPag.oOrdinamento_1_85.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATCENCOS_2_1.value==this.w_ATCENCOS)
      this.oPgFrm.Page2.oPag.oATCENCOS_2_1.value=this.w_ATCENCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oATCODPRA_2_2.value==this.w_ATCODPRA)
      this.oPgFrm.Page2.oPag.oATCODPRA_2_2.value=this.w_ATCODPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDAATTIVI_2_3.value==this.w_DAATTIVI)
      this.oPgFrm.Page2.oPag.oDAATTIVI_2_3.value=this.w_DAATTIVI
    endif
    if not(this.oPgFrm.Page2.oPag.oATCENRIC_2_4.value==this.w_ATCENRIC)
      this.oPgFrm.Page2.oPag.oATCENRIC_2_4.value=this.w_ATCENRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oATCOMRIC_2_5.value==this.w_ATCOMRIC)
      this.oPgFrm.Page2.oPag.oATCOMRIC_2_5.value=this.w_ATCOMRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oATATTRIC_2_6.value==this.w_ATATTRIC)
      this.oPgFrm.Page2.oPag.oATATTRIC_2_6.value=this.w_ATATTRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oDACODNOM_2_7.value==this.w_DACODNOM)
      this.oPgFrm.Page2.oPag.oDACODNOM_2_7.value=this.w_DACODNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDATLIS_2_9.value==this.w_DATLIS)
      this.oPgFrm.Page2.oPag.oDATLIS_2_9.value=this.w_DATLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oATCODLIS_2_10.value==this.w_ATCODLIS)
      this.oPgFrm.Page2.oPag.oATCODLIS_2_10.value=this.w_ATCODLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oATLISACQ_2_11.value==this.w_ATLISACQ)
      this.oPgFrm.Page2.oPag.oATLISACQ_2_11.value=this.w_ATLISACQ
    endif
    if not(this.oPgFrm.Page2.oPag.oATPERSON_2_12.value==this.w_ATPERSON)
      this.oPgFrm.Page2.oPag.oATPERSON_2_12.value=this.w_ATPERSON
    endif
    if not(this.oPgFrm.Page2.oPag.oATSTATUS_2_13.RadioValue()==this.w_ATSTATUS)
      this.oPgFrm.Page2.oPag.oATSTATUS_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAN_2_14.value==this.w_DESCAN)
      this.oPgFrm.Page2.oPag.oDESCAN_2_14.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDESPIA_2_16.value==this.w_CCDESPIA)
      this.oPgFrm.Page2.oPag.oCCDESPIA_2_16.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page2.oPag.oATDESCRI_2_19.value==this.w_ATDESCRI)
      this.oPgFrm.Page2.oPag.oATDESCRI_2_19.value=this.w_ATDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oORAINI_2_35.value==this.w_ORAINI)
      this.oPgFrm.Page2.oPag.oORAINI_2_35.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page2.oPag.oMININI_2_36.value==this.w_MININI)
      this.oPgFrm.Page2.oPag.oMININI_2_36.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page2.oPag.oORAFIN_2_37.value==this.w_ORAFIN)
      this.oPgFrm.Page2.oPag.oORAFIN_2_37.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oMINFIN_2_38.value==this.w_MINFIN)
      this.oPgFrm.Page2.oPag.oMINFIN_2_38.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLIS_2_43.value==this.w_DESLIS)
      this.oPgFrm.Page2.oPag.oDESLIS_2_43.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_51.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_51.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCCDESRIC_2_53.value==this.w_CCDESRIC)
      this.oPgFrm.Page2.oPag.oCCDESRIC_2_53.value=this.w_CCDESRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oATDESRIC_2_56.value==this.w_ATDESRIC)
      this.oPgFrm.Page2.oPag.oATDESRIC_2_56.value=this.w_ATDESRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLISA_2_62.value==this.w_DESLISA)
      this.oPgFrm.Page2.oPag.oDESLISA_2_62.value=this.w_DESLISA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CODRESP)) or not((EMPTY(.w_CODRESP) OR .w_TipoPersona=.w_DPTIPRIS) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODRESP_1_10.SetFocus()
            i_bnoObbl = !empty(.w_CODRESP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODRESP , .w_GRUPART))  and not(empty(.w_GRUPART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUPART_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo inesistente o non associato al partecipante o obsoleto")
          case   not(.w_TIPCLIMIN<>'G' AND .w_TIPCLIMIN<>'F')  and not(empty(.w_CODNIMIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODNIMIN_1_84.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO))  and not(empty(.w_ATCENCOS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCENCOS_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and (g_PERCAN='S' OR g_COMM='S')  and not(empty(.w_ATCODPRA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCODPRA_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice Commessa obsoleto")
          case   not(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO))  and not(empty(.w_ATCENRIC))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCENRIC_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and (g_PERCAN='S' OR g_COMM='S')  and not(empty(.w_ATCOMRIC))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCOMRIC_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice Commessa obsoleto")
          case   not((.w_DATOBSNOM>.w_OBTEST OR EMPTY(.w_DATOBSNOM)) AND .w_NOTIPNOM<>'G' AND .w_NOTIPNOM<>'F')  and not(empty(.w_DACODNOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDACODNOM_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VALLIS=.w_ATCODVAL )  and not(.w_ISAHE)  and not(empty(.w_ATCODLIS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCODLIS_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta listino incongruente o listino gestito a sconti")
          case   not((.w_Isahe and CHKLISD(.w_ATLISACQ, 'L', .w_VALACQ, .w_INIACQ, .w_FINACQ,.w_TIPACQ,'S',.w_FLGACQ, .w_FLSACQ,.w_FLSCOR, .w_ATCODVAL, .w_DATALIST,'A','',.w_FLSCOAC)) or (Isahr() and .w_FLSCOAC <>'S' AND .w_IVAACQ<>'L' AND CHKLISD(.w_ATLISACQ,.w_IVAACQ,.w_VALACQ,.w_INIACQ,.w_FINACQ,'N', .w_ATCODVAL, .w_DATINI)))  and not(empty(.w_ATLISACQ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATLISACQ_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta listino incongruente o listino gestito a sconti")
          case   ((empty(.w_ORAINI)) or not(VAL(.w_ORAINI) < 24))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oORAINI_2_35.SetFocus()
            i_bnoObbl = !empty(.w_ORAINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   ((empty(.w_MININI)) or not(VAL(.w_MININI) < 60))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMININI_2_36.SetFocus()
            i_bnoObbl = !empty(.w_MININI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   ((empty(.w_ORAFIN)) or not(VAL(.w_ORAFIN) < 24))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oORAFIN_2_37.SetFocus()
            i_bnoObbl = !empty(.w_ORAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   ((empty(.w_MINFIN)) or not(VAL(.w_MINFIN) < 60))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMINFIN_2_38.SetFocus()
            i_bnoObbl = !empty(.w_MINFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAG_MPP.CheckForm()
      if i_bres
        i_bres=  .GSAG_MPP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsag_kpp
      * --- Controlli Finali
      IF i_bRes=.t.
         .w_RESCHK=0
         .NotifyEvent('ChkFinali')
         WAIT CLEAR
         IF .w_RESCHK<>0
           i_bRes=.f.
         ENDIF
      ENDIF
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PARAMAGE = this.w_PARAMAGE
    this.o_EDITCODPRA = this.w_EDITCODPRA
    this.o_PACAUPRE = this.w_PACAUPRE
    this.o_CODRESP = this.w_CODRESP
    this.o_CENCOS = this.w_CENCOS
    this.o_GRUPART = this.w_GRUPART
    this.o_COGNOME = this.w_COGNOME
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    this.o_FLDTRP = this.w_FLDTRP
    this.o_NOME = this.w_NOME
    this.o_CodPratica = this.w_CodPratica
    this.o_ATDURORE = this.w_ATDURORE
    this.o_ATDURMIN = this.w_ATDURMIN
    this.o_CADTIN = this.w_CADTIN
    this.o_SELEZ = this.w_SELEZ
    this.o_ATCENCOS = this.w_ATCENCOS
    this.o_ATCODPRA = this.w_ATCODPRA
    this.o_DAATTIVI = this.w_DAATTIVI
    this.o_ATCENRIC = this.w_ATCENRIC
    this.o_ATCOMRIC = this.w_ATCOMRIC
    this.o_ATATTRIC = this.w_ATATTRIC
    this.o_DACODNOM = this.w_DACODNOM
    this.o_ATCODVAL = this.w_ATCODVAL
    this.o_DATLIS = this.w_DATLIS
    this.o_ORAINI = this.w_ORAINI
    this.o_MININI = this.w_MININI
    * --- GSAG_MPP : Depends On
    this.GSAG_MPP.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsag_kppPag1 as StdContainer
  Width  = 831
  height = 573
  stdWidth  = 831
  stdheight = 573
  resizeXpos=548
  resizeYpos=393
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODRESP_1_10 as StdField with uid="HIIAOTSMWB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODRESP", cQueryName = "CODRESP",nZero=5,;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 21205978,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=81, Top=8, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_CODRESP"

  func oCODRESP_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODRESP_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODRESP_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oCODRESP_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Partecipanti",'GSAG_KPR.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oCODRESP_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_CODRESP
     i_obj.ecpSave()
  endproc

  add object oGRUPART_1_13 as StdField with uid="AYUXRJLLLH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_GRUPART", cQueryName = "GRUPART",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo inesistente o non associato al partecipante o obsoleto",;
    HelpContextID = 226197350,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=487, Top=8, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_GRUPART"

  func oGRUPART_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPART_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPART_1_13.mZoom
      with this.Parent.oContained
        GSAR1BGP(this.Parent.oContained,  "", .w_CODRESP, "", "GRUPART" )
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDATINI_1_15 as StdField with uid="VWHWWQDGJC",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione",;
    HelpContextID = 180068810,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=81, Top=42

  add object oDATFIN_1_18 as StdField with uid="TRTMRAYYHF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione",;
    HelpContextID = 101622218,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=203, Top=42

  add object oCODSER_1_19 as AH_SEARCHFLD with uid="NQAZQYRMPT",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODSER", cQueryName = "CODSER",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 37917658,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=329, Top=42, InputMask=replicate('X',20)

  add object oDesSer_1_20 as AH_SEARCHFLD with uid="RAJBAVJSRR",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DesSer", cQueryName = "DesSer",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione prestazione o parte di essa",;
    HelpContextID = 4165066,;
   bGlobalFont=.t.,;
    Height=21, Width=200, Left=570, Top=42, InputMask=replicate('X',40)


  add object oLinkPC_1_24 as stdDynamicChildContainer with uid="RQSTKVFIVN",left=4, top=97, width=825, height=421, bOnScreen=.t.;


  func oLinkPC_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CODRESP) AND (.w_PAFLVISI='L' OR (.w_PAFLVISI<>'L' AND !EMPTY(.w_GRUPART))))
      endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="ERHOXVEEGI",left=672, top=527, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Aggiorna provvisorio prestazioni";
    , HelpContextID = 118900966;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        AggioProvv(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="TKCMXZXILU",left=726, top=527, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 126189638;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_27 as StdButton with uid="JEKYUELVIJ",left=780, top=527, width=48,height=45,;
    CpPicture="bmp\genera.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per procedere nell'inserimento definitivo delle prestazioni";
    , HelpContextID = 55385223;
    , caption='\<Completa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        AggioProvv(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_31 as StdButton with uid="TZRVCUISDS",left=5, top=526, width=48,height=45,;
    CpPicture="bmp\PARAMETRI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire il caricamento multiplo delle prestazioni";
    , HelpContextID = 216301862;
    , Caption='\<Ins. Mult.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      do GSAG_KPM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRIZ_1_32 as StdField with uid="KMZNHYKWWA",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCRIZ", cQueryName = "DESCRIZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 92164662,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=146, Top=8, InputMask=replicate('X',80)

  add object oDPDESGRU_1_36 as StdField with uid="LGRUONCFGR",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DPDESGRU", cQueryName = "DPDESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 208704117,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=554, Top=8, InputMask=replicate('X',40)


  add object oObj_1_39 as cp_setobjprop with uid="FSTGNBRDDX",left=255, top=614, width=168,height=19,;
    caption='Tooltip w_CODRESP',;
   bGlobalFont=.t.,;
    cProp="Tooltiptext",cObj="w_CODRESP",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 27469543


  add object oObj_1_49 as cp_runprogram with uid="GETBPRLFVL",left=255, top=632, width=168,height=19,;
    caption='GSAG_BM2(F)',;
   bGlobalFont=.t.,;
    prg="GSAG_BM2('F')",;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 11265768

  add object oGENSINGATT_1_58 as StdCheck with uid="EJECCBHQGI",rtseq=48,rtrep=.f.,left=520, top=544, caption="Genera singola attivit�",;
    ToolTipText = "Se attivo, in fase di inserimento definitivo delle prestazioni verr� generata una singola attivit� per ogni prestazione.",;
    HelpContextID = 167664615,;
    cFormVar="w_GENSINGATT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGENSINGATT_1_58.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGENSINGATT_1_58.GetRadio()
    this.Parent.oContained.w_GENSINGATT = this.RadioValue()
    return .t.
  endfunc

  func oGENSINGATT_1_58.SetRadio()
    this.Parent.oContained.w_GENSINGATT=trim(this.Parent.oContained.w_GENSINGATT)
    this.value = ;
      iif(this.Parent.oContained.w_GENSINGATT=='S',1,;
      0)
  endfunc


  add object oBtn_1_59 as StdButton with uid="LEVPPADLLT",left=780, top=41, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Aggiorna provvisorio prestazioni";
    , HelpContextID = 241076458;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_59.Click()
      with this.Parent.oContained
        gsag_bck(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_77 as cp_runprogram with uid="RVKDVOCXPN",left=255, top=650, width=168,height=21,;
    caption='GSAG_BPR',;
   bGlobalFont=.t.,;
    prg="GSAG_BPR",;
    cEvent = "Completa",;
    nPag=1;
    , HelpContextID = 11452232

  add object oNODESCRI_1_81 as StdField with uid="BGOCVKLGHV",rtseq=60,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 7377633,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=250, Top=66, InputMask=replicate('X',60)

  add object oCODNIMIN_1_84 as StdField with uid="TRTCEMDWKK",rtseq=63,rtrep=.f.,;
    cFormVar = "w_CODNIMIN", cQueryName = "CODNIMIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nominativo",;
    HelpContextID = 150498420,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=81, Top=66, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNIMIN"

  func oCODNIMIN_1_84.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_84('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNIMIN_1_84.ecpDrop(oSource)
    this.Parent.oContained.link_1_84('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNIMIN_1_84.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNIMIN_1_84'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNIMIN_1_84.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNIMIN
     i_obj.ecpSave()
  endproc


  add object oOrdinamento_1_85 as StdCombo with uid="CDEAFJNPSE",rtseq=64,rtrep=.f.,left=633,top=68,width=137,height=17;
    , ToolTipText = "Ordinamento dei dati in visualizzazione";
    , HelpContextID = 9635029;
    , cFormVar="w_Ordinamento",RowSource=""+"Data,"+"Commessa,"+"Cod. prestazione,"+"Des. prestazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOrdinamento_1_85.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'P',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    space(10))))))
  endfunc
  func oOrdinamento_1_85.GetRadio()
    this.Parent.oContained.w_Ordinamento = this.RadioValue()
    return .t.
  endfunc

  func oOrdinamento_1_85.SetRadio()
    this.Parent.oContained.w_Ordinamento=trim(this.Parent.oContained.w_Ordinamento)
    this.value = ;
      iif(this.Parent.oContained.w_Ordinamento=='N',1,;
      iif(this.Parent.oContained.w_Ordinamento=='P',2,;
      iif(this.Parent.oContained.w_Ordinamento=='C',3,;
      iif(this.Parent.oContained.w_Ordinamento=='D',4,;
      0))))
  endfunc

  add object oStr_1_9 as StdString with uid="GLFTJMROPX",Visible=.t., Left=-1, Top=9,;
    Alignment=1, Width=81, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="VXTZZXYPND",Visible=.t., Left=366, Top=10,;
    Alignment=1, Width=118, Height=18,;
    Caption="Gruppo partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="EVXJUIYDBW",Visible=.t., Left=17, Top=42,;
    Alignment=1, Width=63, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="SLXBLPBADH",Visible=.t., Left=160, Top=42,;
    Alignment=1, Width=42, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="RXWUWYGUBK",Visible=.t., Left=497, Top=42,;
    Alignment=1, Width=71, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="AUEJGKPLZP",Visible=.t., Left=279, Top=42,;
    Alignment=1, Width=48, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="EQTQBFOEXL",Visible=.t., Left=568, Top=68,;
    Alignment=1, Width=61, Height=18,;
    Caption="Ordina per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="TGATHROLXG",Visible=.t., Left=11, Top=66,;
    Alignment=1, Width=69, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_86 as StdBox with uid="ARKTETUIBR",left=9, top=34, width=819,height=2
enddefine
define class tgsag_kppPag2 as StdContainer
  Width  = 831
  height = 573
  stdWidth  = 831
  stdheight = 573
  resizeXpos=532
  resizeYpos=508
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATCENCOS_2_1 as StdField with uid="XMAJYYJCZO",rtseq=69,rtrep=.f.,;
    cFormVar = "w_ATCENCOS", cQueryName = "ATCENCOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di ricavo incongruente o obsoleto",;
    ToolTipText = "Codice centro di costo",;
    HelpContextID = 12623527,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=161, Top=37, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ATCENCOS"

  func oATCENCOS_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCENCOS_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCENCOS_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oATCENCOS_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oATCENCOS_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ATCENCOS
     i_obj.ecpSave()
  endproc

  add object oATCODPRA_2_2 as StdField with uid="ADTGYWXWOV",rtseq=70,rtrep=.f.,;
    cFormVar = "w_ATCODPRA", cQueryName = "ATCODPRA",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice Commessa obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 72785593,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=161, Top=64, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_BZZ", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ATCODPRA"

  func oATCODPRA_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCAN='S' OR g_COMM='S')
    endwith
   endif
  endfunc

  func oATCODPRA_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
      if .not. empty(.w_DAATTIVI)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oATCODPRA_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODPRA_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oATCODPRA_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZZ',"Commesse",'',this.parent.oContained
  endproc
  proc oATCODPRA_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_ATCODPRA
     i_obj.ecpSave()
  endproc

  add object oDAATTIVI_2_3 as StdField with uid="LTBYOWJTQX",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DAATTIVI", cQueryName = "DAATTIVI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 95301247,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=161, Top=91, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_ATCODPRA", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_DAATTIVI"

  func oDAATTIVI_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ATCODPRA) AND g_COMM='S')
    endwith
   endif
  endfunc

  func oDAATTIVI_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAATTIVI_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAATTIVI_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_ATCODPRA)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_ATCODPRA)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oDAATTIVI_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oDAATTIVI_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_ATCODPRA
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_DAATTIVI
     i_obj.ecpSave()
  endproc

  add object oATCENRIC_2_4 as StdField with uid="IQKRSZCPQZ",rtseq=72,rtrep=.f.,;
    cFormVar = "w_ATCENRIC", cQueryName = "ATCENRIC",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di ricavo incongruente o obsoleto",;
    ToolTipText = "Codice centro di ricavo",;
    HelpContextID = 239034697,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=161, Top=120, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ATCENRIC"

  func oATCENRIC_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCENRIC_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCENRIC_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oATCENRIC_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oATCENRIC_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ATCENRIC
     i_obj.ecpSave()
  endproc

  add object oATCOMRIC_2_5 as StdField with uid="RMJLMAELLW",rtseq=73,rtrep=.f.,;
    cFormVar = "w_ATCOMRIC", cQueryName = "ATCOMRIC",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice Commessa obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 238641481,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=161, Top=148, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_BZZ", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ATCOMRIC"

  func oATCOMRIC_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCAN='S' OR g_COMM='S')
    endwith
   endif
  endfunc

  func oATCOMRIC_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
      if .not. empty(.w_ATATTRIC)
        bRes2=.link_2_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oATCOMRIC_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCOMRIC_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oATCOMRIC_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'',this.parent.oContained
  endproc
  proc oATCOMRIC_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_ATCOMRIC
     i_obj.ecpSave()
  endproc

  add object oATATTRIC_2_6 as StdField with uid="GKHMGRNTUX",rtseq=74,rtrep=.f.,;
    cFormVar = "w_ATATTRIC", cQueryName = "ATATTRIC",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa ricavi",;
    HelpContextID = 246301001,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=161, Top=175, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_ATCOMRIC", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_ATATTRIC"

  func oATATTRIC_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ATCOMRIC) AND g_COMM='S')
    endwith
   endif
  endfunc

  func oATATTRIC_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oATATTRIC_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATATTRIC_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_ATCOMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_ATCOMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATATTRIC_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oATATTRIC_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_ATCOMRIC
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_ATATTRIC
     i_obj.ecpSave()
  endproc

  add object oDACODNOM_2_7 as StdField with uid="UTACBYLWZX",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DACODNOM", cQueryName = "DACODNOM",nZero=15,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 106344829,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=161, Top=202, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_DACODNOM"

  func oDACODNOM_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODNOM_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODNOM_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oDACODNOM_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oDACODNOM_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_DACODNOM
     i_obj.ecpSave()
  endproc

  add object oDATLIS_2_9 as StdField with uid="IFVVYJYTTP",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DATLIS", cQueryName = "DATLIS",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di determinazione listini validi da applicare",;
    HelpContextID = 17342922,;
   bGlobalFont=.t.,;
    Height=21, Width=85, Left=161, Top=229

  func oDATLIS_2_9.mHide()
    with this.Parent.oContained
      return (!.w_ISAHE)
    endwith
  endfunc

  add object oATCODLIS_2_10 as StdField with uid="VMCOWPZUQZ",rtseq=78,rtrep=.f.,;
    cFormVar = "w_ATCODLIS", cQueryName = "ATCODLIS",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta listino incongruente o listino gestito a sconti",;
    ToolTipText = "Codice listino",;
    HelpContextID = 128541017,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=161, Top=229, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSVALLIS", oKey_1_2="this.w_ATCODVAL", oKey_2_1="LSCODLIS", oKey_2_2="this.w_ATCODLIS"

  func oATCODLIS_2_10.mHide()
    with this.Parent.oContained
      return (.w_ISAHE)
    endwith
  endfunc

  func oATCODLIS_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODLIS_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODLIS_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LISTINI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStrODBC(this.Parent.oContained.w_ATCODVAL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStr(this.Parent.oContained.w_ATCODVAL)
    endif
    do cp_zoom with 'LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(this.parent,'oATCODLIS_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc

  add object oATLISACQ_2_11 as StdField with uid="HNFVPZTBWD",rtseq=79,rtrep=.f.,;
    cFormVar = "w_ATLISACQ", cQueryName = "ATLISACQ",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta listino incongruente o listino gestito a sconti",;
    ToolTipText = "Codice listino costi",;
    HelpContextID = 40636073,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=161, Top=256, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ATLISACQ"

  func oATLISACQ_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oATLISACQ_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATLISACQ_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oATLISACQ_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Listini",'listdocu.LISTINI_VZM',this.parent.oContained
  endproc

  add object oATPERSON_2_12 as StdField with uid="IGFNYJIELV",rtseq=80,rtrep=.f.,;
    cFormVar = "w_ATPERSON", cQueryName = "ATPERSON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione nominativo",;
    HelpContextID = 8375980,;
   bGlobalFont=.t.,;
    Height=21, Width=482, Left=306, Top=203, InputMask=replicate('X',60)


  add object oATSTATUS_2_13 as StdCombo with uid="MGRBZGPSKC",rtseq=81,rtrep=.f.,left=161,top=329,width=116,height=17;
    , ToolTipText = "Stato da assegnare all'attivit�";
    , HelpContextID = 260006233;
    , cFormVar="w_ATSTATUS",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa,"+"Completata", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATSTATUS_2_13.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oATSTATUS_2_13.GetRadio()
    this.Parent.oContained.w_ATSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oATSTATUS_2_13.SetRadio()
    this.Parent.oContained.w_ATSTATUS=trim(this.Parent.oContained.w_ATSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_ATSTATUS=='T',1,;
      iif(this.Parent.oContained.w_ATSTATUS=='D',2,;
      iif(this.Parent.oContained.w_ATSTATUS=='I',3,;
      iif(this.Parent.oContained.w_ATSTATUS=='F',4,;
      iif(this.Parent.oContained.w_ATSTATUS=='P',5,;
      0)))))
  endfunc

  add object oDESCAN_2_14 as StdField with uid="VBWZLHUZGU",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto",;
    HelpContextID = 110210506,;
   bGlobalFont=.t.,;
    Height=21, Width=482, Left=306, Top=64, InputMask=replicate('X',100)

  add object oCCDESPIA_2_16 as StdField with uid="QVKOSEQBBL",rtseq=83,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 210722919,;
   bGlobalFont=.t.,;
    Height=21, Width=482, Left=306, Top=37, InputMask=replicate('X',40)

  func oCCDESPIA_2_16.mHide()
    with this.Parent.oContained
      return (NOT (g_COAN='S'))
    endwith
  endfunc

  add object oATDESCRI_2_19 as StdField with uid="AEIZCFLSJU",rtseq=84,rtrep=.f.,;
    cFormVar = "w_ATDESCRI", cQueryName = "ATDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attivit�",;
    HelpContextID = 7376561,;
   bGlobalFont=.t.,;
    Height=21, Width=482, Left=306, Top=91, InputMask=replicate('X',30)

  add object oORAINI_2_35 as StdField with uid="IFZETBJSDN",rtseq=98,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .t. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora inizio attivit�",;
    HelpContextID = 180142106,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=161, Top=363, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_2_36 as StdField with uid="BMAZHAYBOV",rtseq=99,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .t. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti inizio attivit�",;
    HelpContextID = 180091194,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=196, Top=363, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oORAFIN_2_37 as StdField with uid="PBNKIXNQKG",rtseq=100,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",nZero=2,;
    bObbl = .t. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora fine attivit�",;
    HelpContextID = 101695514,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=161, Top=394, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAFIN_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_2_38 as StdField with uid="IFSBCOBBWT",rtseq=101,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .t. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti fine attivit�",;
    HelpContextID = 101644602,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=196, Top=394, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_2_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc

  add object oDESLIS_2_43 as StdField with uid="SEGSWEYBEL",rtseq=102,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione listino",;
    HelpContextID = 17345994,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=250, Top=229, InputMask=replicate('X',40)

  func oDESLIS_2_43.mHide()
    with this.Parent.oContained
      return (.w_ISAHE)
    endwith
  endfunc


  add object oBtn_2_46 as StdButton with uid="UGXTHDBITV",left=740, top=231, width=48,height=45,;
    CpPicture="bmp\applica.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per modificare i dati di testata con quelli di default";
    , HelpContextID = 260721561;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_46.Click()
      with this.Parent.oContained
        do GSAG_BGG with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_2_49 as cp_runprogram with uid="JPTFTQKXWI",left=28, top=609, width=416,height=19,;
    caption='GSAG_BM2(E)',;
   bGlobalFont=.t.,;
    prg="GSAG_BM2('E')",;
    cEvent = "w_DATLIS Changed,w_DACODNOM Changed,Blank",;
    nPag=2;
    , HelpContextID = 11266024

  add object oDESCOM_2_51 as StdField with uid="ITKRAOAGSB",rtseq=107,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione commessa",;
    HelpContextID = 112307658,;
   bGlobalFont=.t.,;
    Height=21, Width=482, Left=306, Top=148, InputMask=replicate('X',100)

  add object oCCDESRIC_2_53 as StdField with uid="RMUTZAKSDL",rtseq=108,rtrep=.f.,;
    cFormVar = "w_CCDESRIC", cQueryName = "CCDESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 244277353,;
   bGlobalFont=.t.,;
    Height=21, Width=482, Left=306, Top=120, InputMask=replicate('X',40)

  func oCCDESRIC_2_53.mHide()
    with this.Parent.oContained
      return (NOT (g_COAN='S') )
    endwith
  endfunc

  add object oATDESRIC_2_56 as StdField with uid="UULNCAOYNJ",rtseq=109,rtrep=.f.,;
    cFormVar = "w_ATDESRIC", cQueryName = "ATDESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attivit�",;
    HelpContextID = 244281673,;
   bGlobalFont=.t.,;
    Height=21, Width=482, Left=306, Top=175, InputMask=replicate('X',30)

  add object oDESLISA_2_62 as StdField with uid="HVNNCRZXPW",rtseq=114,rtrep=.f.,;
    cFormVar = "w_DESLISA", cQueryName = "DESLISA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione listino",;
    HelpContextID = 17345994,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=250, Top=256, InputMask=replicate('X',40)

  add object oStr_2_15 as StdString with uid="XCNTBWTQWS",Visible=.t., Left=34, Top=205,;
    Alignment=1, Width=121, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="QAWBBWUYUQ",Visible=.t., Left=57, Top=64,;
    Alignment=1, Width=98, Height=18,;
    Caption="Commessa costi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="THZJZYTFBJ",Visible=.t., Left=64, Top=93,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit� costi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="NDNECIPGFN",Visible=.t., Left=64, Top=39,;
    Alignment=1, Width=91, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="YYXYCLPDIS",Visible=.t., Left=53, Top=329,;
    Alignment=1, Width=105, Height=18,;
    Caption="Stato attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="DWXUKAMXAQ",Visible=.t., Left=190, Top=365,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="LRDPZVZQBN",Visible=.t., Left=66, Top=365,;
    Alignment=1, Width=92, Height=18,;
    Caption="Ora inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="WZFLEPMIEC",Visible=.t., Left=190, Top=396,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="YHXXGJBLXE",Visible=.t., Left=82, Top=396,;
    Alignment=1, Width=76, Height=18,;
    Caption="Ora fine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="LAJEFMNEAN",Visible=.t., Left=7, Top=231,;
    Alignment=1, Width=148, Height=18,;
    Caption="Data applicazione listini:"  ;
  , bGlobalFont=.t.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (!.w_ISAHE)
    endwith
  endfunc

  add object oStr_2_52 as StdString with uid="MGZCUQEBSW",Visible=.t., Left=52, Top=150,;
    Alignment=1, Width=103, Height=18,;
    Caption="Commessa ricavi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_54 as StdString with uid="LOGJONVSGJ",Visible=.t., Left=51, Top=122,;
    Alignment=1, Width=104, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="CAISYCOQIM",Visible=.t., Left=64, Top=177,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit� ricavi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_61 as StdString with uid="RGRABHKJLJ",Visible=.t., Left=86, Top=256,;
    Alignment=1, Width=69, Height=18,;
    Caption="Listino costi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_71 as StdString with uid="GVWVIJCRNA",Visible=.t., Left=81, Top=231,;
    Alignment=1, Width=74, Height=18,;
    Caption="Listino ricavi:"  ;
  , bGlobalFont=.t.

  func oStr_2_71.mHide()
    with this.Parent.oContained
      return (not isahr())
    endwith
  endfunc

  add object oStr_2_76 as StdString with uid="ZEFCOQUNXR",Visible=.t., Left=8, Top=303,;
    Alignment=0, Width=72, Height=19,;
    Caption="Dati attivit�"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_75 as StdBox with uid="IBSFJKRBEW",left=3, top=321, width=825,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kpp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_kpp
proc AggioProvv(obj,pAzione)
	Local l_object
Do Case 
  Case pAzione='B'
    * F10 in gsag_kpp
		ah_errormsg("Aggiornamento provvisorio prestazioni effettuato")
	Case pAzione $ 'Z-S'
    * Z refresh dati
    * S salva da bottone ok
     if OBJ.Gsag_mpp.Numrow()>0 or OBJ.Gsag_mpp.bUpdated
		   OBJ.Save_GSAG_MPP(.f.)
       OBJ.Gsag_mpp.loadrec()
       OBJ.Gsag_mpp.refresh()
		 endif
  Case pAzione ='C'
    OBJ.Notifyevent('Completa')
endcase

IF pAzione $ 'S-C'
   Local l_object
      if OBJ.w_RESCHK<>-1 
        OBJ.Gsag_mpp.addrow()
     endif
    if pAzione = 'S'
	    ah_errormsg("Aggiornamento provvisorio prestazioni effettuato")
       l_object=OBJ.Gsag_mpp.getbodyctrl("w_PR__DATA")
       l_object.Setfocus()
    endif
    OBJ.Gsag_mpp.refresh()
    l_object=null
endif
endproc
* --- Fine Area Manuale
