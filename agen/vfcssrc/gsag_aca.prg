* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_aca                                                        *
*              Contratti di assistenza                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-08-04                                                      *
* Last revis.: 2015-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_aca"))

* --- Class definition
define class tgsag_aca as StdForm
  Top    = 11
  Left   = 10

  * --- Standard Properties
  Width  = 789
  Height = 524+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-29"
  HelpContextID=158718103
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=73

  * --- Constant Properties
  CON_TRAS_IDX = 0
  CONTI_IDX = 0
  MODCLDAT_IDX = 0
  TIP_DOCU_IDX = 0
  DIPENDEN_IDX = 0
  CAUMATTI_IDX = 0
  PAG_AMEN_IDX = 0
  LISTINI_IDX = 0
  CENCOST_IDX = 0
  CAN_TIER_IDX = 0
  DES_DIVE_IDX = 0
  cFile = "CON_TRAS"
  cKeySelect = "COSERIAL"
  cKeyWhere  = "COSERIAL=this.w_COSERIAL"
  cKeyWhereODBC = '"COSERIAL="+cp_ToStrODBC(this.w_COSERIAL)';

  cKeyWhereODBCqualified = '"CON_TRAS.COSERIAL="+cp_ToStrODBC(this.w_COSERIAL)';

  cPrg = "gsag_aca"
  cComment = "Contratti di assistenza"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_COSERIAL = space(10)
  w_CODESCON = space(50)
  w_VALATT = space(20)
  w_TABKEY = space(8)
  w_DELIMM = .F.
  w_COTIPCON = space(1)
  w_COCODCON = space(15)
  w_COFLSCOR = space(1)
  w_CO__NOTE = space(0)
  w_PATIPRIS = space(1)
  w_CODESSUP = space(254)
  w_CODATSTI = ctod('  /  /  ')
  w_CODATINI = ctod('  /  /  ')
  o_CODATINI = ctod('  /  /  ')
  w_CODATFIN = ctod('  /  /  ')
  w_COCODLIS = space(5)
  w_COTIPATT = space(20)
  w_COPRIDAT = space(1)
  w_COFLSOSP = space(1)
  w_COGRUPAR = space(5)
  w_COTIPDOC = space(5)
  w_COPRIDOC = space(1)
  w_COCODPAG = space(5)
  w_COTIPSED = space(1)
  o_COTIPSED = space(1)
  w_COCODSED = space(5)
  w_COESCRIN = space(1)
  w_DESCON = space(60)
  w_COPERCON = space(3)
  w_MULTIPLO = 0
  w_CORINCON = space(1)
  w_ANNI = 0
  w_CONUMGIO = 0
  w_CODATFIS = space(1)
  w_COCODCEN = space(15)
  w_COCOCOMM = space(15)
  w_CODATLIM = ctod('  /  /  ')
  w_DAY = 0
  w_DESDOC = space(35)
  w_DESATT = space(254)
  w_CARAGGST = space(10)
  w_CATDOC = space(2)
  w_FLVEAC = space(1)
  w_TIPRIS = space(1)
  w_DESPAG = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_OB_TEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_FLRIPS = space(1)
  w_DESLIS = space(40)
  w_IVALIS = space(1)
  w_FLSCON = space(1)
  w_VALLIS = space(3)
  w_MDDESCRI = space(50)
  w_EDITCHECK = space(50)
  w_EDITCHECK2 = space(50)
  w_DTOBSO = ctod('  /  /  ')
  w_RESCHK = .F.
  w_AGGIORNAELEMENTI = .F.
  w_DPDESCRI = space(60)
  w_DESCOST = space(40)
  w_DESCOMM = space(100)
  w_CENOBSO = ctod('  /  /  ')
  w_COMOBSO = ctod('  /  /  ')
  w_ATTEXIST = .F.
  w_DOCEXIST = .F.
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_OKSTAMPA = .F.
  w_NOMDES = space(40)
  w_TIPRIF = space(2)
  w_UTDC = ctot('')
  w_UTCV = 0
  w_UTDV = ctot('')
  w_UTCC = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_COSERIAL = this.W_COSERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CON_TRAS','gsag_aca')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_acaPag1","gsag_aca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Contratti")
      .Pages(1).HelpContextID = 34681642
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOSERIAL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MODCLDAT'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='DIPENDEN'
    this.cWorkTables[5]='CAUMATTI'
    this.cWorkTables[6]='PAG_AMEN'
    this.cWorkTables[7]='LISTINI'
    this.cWorkTables[8]='CENCOST'
    this.cWorkTables[9]='CAN_TIER'
    this.cWorkTables[10]='DES_DIVE'
    this.cWorkTables[11]='CON_TRAS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(11))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CON_TRAS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CON_TRAS_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_COSERIAL = NVL(COSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_25_joined
    link_1_25_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    local link_1_28_joined
    link_1_28_joined=.f.
    local link_1_33_joined
    link_1_33_joined=.f.
    local link_1_39_joined
    link_1_39_joined=.f.
    local link_1_40_joined
    link_1_40_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CON_TRAS where COSERIAL=KeySet.COSERIAL
    *
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CON_TRAS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CON_TRAS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CON_TRAS '
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_25_joined=this.AddJoinedLink_1_25(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_28_joined=this.AddJoinedLink_1_28(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_33_joined=this.AddJoinedLink_1_33(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_39_joined=this.AddJoinedLink_1_39(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_40_joined=this.AddJoinedLink_1_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COSERIAL',this.w_COSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_DATSYS
        .w_VALATT = .w_COSERIAL
        .w_TABKEY = 'CON_TRAS'
        .w_DELIMM = .f.
        .w_PATIPRIS = 'G'
        .w_DESCON = space(60)
        .w_DESDOC = space(35)
        .w_DESATT = space(254)
        .w_CARAGGST = space(10)
        .w_CATDOC = space(2)
        .w_FLVEAC = space(1)
        .w_TIPRIS = space(1)
        .w_DESPAG = space(30)
        .w_OBTEST = I_DATSYS
        .w_DATOBSO = ctod("  /  /  ")
        .w_FLRIPS = space(1)
        .w_DESLIS = space(40)
        .w_IVALIS = space(1)
        .w_FLSCON = space(1)
        .w_VALLIS = space(3)
        .w_MDDESCRI = space(50)
        .w_EDITCHECK = space(50)
        .w_EDITCHECK2 = space(50)
        .w_DTOBSO = ctod("  /  /  ")
        .w_RESCHK = .T.
        .w_AGGIORNAELEMENTI = .f.
        .w_DPDESCRI = space(60)
        .w_DESCOST = space(40)
        .w_DESCOMM = space(100)
        .w_CENOBSO = ctod("  /  /  ")
        .w_COMOBSO = ctod("  /  /  ")
        .w_ATTEXIST = .f.
        .w_DOCEXIST = .f.
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_OKSTAMPA = .F.
        .w_NOMDES = space(40)
        .w_TIPRIF = space(2)
        .w_COSERIAL = NVL(COSERIAL,space(10))
        .op_COSERIAL = .w_COSERIAL
        .w_CODESCON = NVL(CODESCON,space(50))
        .w_COTIPCON = NVL(COTIPCON,space(1))
        .w_COCODCON = NVL(COCODCON,space(15))
          if link_1_8_joined
            this.w_COCODCON = NVL(ANCODICE108,NVL(this.w_COCODCON,space(15)))
            this.w_DESCON = NVL(ANDESCRI108,space(60))
            this.w_COFLSCOR = NVL(ANSCORPO108,space(1))
            this.w_DTOBSO = NVL(cp_ToDate(ANDTOBSO108),ctod("  /  /  "))
          else
          .link_1_8('Load')
          endif
        .w_COFLSCOR = NVL(COFLSCOR,space(1))
        .w_CO__NOTE = NVL(CO__NOTE,space(0))
        .w_CODESSUP = NVL(CODESSUP,space(254))
        .w_CODATSTI = NVL(cp_ToDate(CODATSTI),ctod("  /  /  "))
        .w_CODATINI = NVL(cp_ToDate(CODATINI),ctod("  /  /  "))
        .w_CODATFIN = NVL(cp_ToDate(CODATFIN),ctod("  /  /  "))
        .w_COCODLIS = NVL(COCODLIS,space(5))
          if link_1_20_joined
            this.w_COCODLIS = NVL(LSCODLIS120,NVL(this.w_COCODLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS120,space(40))
            this.w_IVALIS = NVL(LSIVALIS120,space(1))
            this.w_FLSCON = NVL(LSFLSCON120,space(1))
            this.w_VALLIS = NVL(LSVALLIS120,space(3))
          else
          .link_1_20('Load')
          endif
        .w_COTIPATT = NVL(COTIPATT,space(20))
          if link_1_22_joined
            this.w_COTIPATT = NVL(CACODICE122,NVL(this.w_COTIPATT,space(20)))
            this.w_DESATT = NVL(CADESCRI122,space(254))
            this.w_CARAGGST = NVL(CARAGGST122,space(10))
          else
          .link_1_22('Load')
          endif
        .w_COPRIDAT = NVL(COPRIDAT,space(1))
        .w_COFLSOSP = NVL(COFLSOSP,space(1))
        .w_COGRUPAR = NVL(COGRUPAR,space(5))
          if link_1_25_joined
            this.w_COGRUPAR = NVL(DPCODICE125,NVL(this.w_COGRUPAR,space(5)))
            this.w_TIPRIS = NVL(DPTIPRIS125,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(DPDTOBSO125),ctod("  /  /  "))
            this.w_DPDESCRI = NVL(DPDESCRI125,space(60))
          else
          .link_1_25('Load')
          endif
        .w_COTIPDOC = NVL(COTIPDOC,space(5))
          if link_1_26_joined
            this.w_COTIPDOC = NVL(TDTIPDOC126,NVL(this.w_COTIPDOC,space(5)))
            this.w_DESDOC = NVL(TDDESDOC126,space(35))
            this.w_CATDOC = NVL(TDCATDOC126,space(2))
            this.w_FLVEAC = NVL(TDFLVEAC126,space(1))
            this.w_FLRIPS = NVL(TDFLRIPS126,space(1))
          else
          .link_1_26('Load')
          endif
        .w_COPRIDOC = NVL(COPRIDOC,space(1))
        .w_COCODPAG = NVL(COCODPAG,space(5))
          if link_1_28_joined
            this.w_COCODPAG = NVL(PACODICE128,NVL(this.w_COCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI128,space(30))
          else
          .link_1_28('Load')
          endif
        .w_COTIPSED = NVL(COTIPSED,space(1))
        .w_COCODSED = NVL(COCODSED,space(5))
          .link_1_30('Load')
        .w_COESCRIN = NVL(COESCRIN,space(1))
        .w_COPERCON = NVL(COPERCON,space(3))
          if link_1_33_joined
            this.w_COPERCON = NVL(MDCODICE133,NVL(this.w_COPERCON,space(3)))
            this.w_MDDESCRI = NVL(MDDESCRI133,space(50))
          else
          .link_1_33('Load')
          endif
        .w_MULTIPLO = MONTH(.w_CODATFIN)-(.w_CONUMGIO/30)
        .w_CORINCON = NVL(CORINCON,space(1))
        .w_ANNI = INT(.w_MULTIPLO/12)
        .w_CONUMGIO = NVL(CONUMGIO,0)
        .w_CODATFIS = NVL(CODATFIS,space(1))
        .w_COCODCEN = NVL(COCODCEN,space(15))
          if link_1_39_joined
            this.w_COCODCEN = NVL(CC_CONTO139,NVL(this.w_COCODCEN,space(15)))
            this.w_DESCOST = NVL(CCDESPIA139,space(40))
            this.w_CENOBSO = NVL(cp_ToDate(CCDTOBSO139),ctod("  /  /  "))
          else
          .link_1_39('Load')
          endif
        .w_COCOCOMM = NVL(COCOCOMM,space(15))
          if link_1_40_joined
            this.w_COCOCOMM = NVL(CNCODCAN140,NVL(this.w_COCOCOMM,space(15)))
            this.w_DESCOMM = NVL(CNDESCAN140,space(100))
            this.w_COMOBSO = NVL(cp_ToDate(CNDTOBSO140),ctod("  /  /  "))
          else
          .link_1_40('Load')
          endif
        .w_CODATLIM = NVL(cp_ToDate(CODATLIM),ctod("  /  /  "))
        .w_DAY = IIF(MOD(.w_CONUMGIO,30)=0,30,DAY(.w_CODATFIN))
        .w_OB_TEST = IIF(EMPTY(.w_CODATINI), i_INIDAT, .w_CODATINI)
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_104.Calculate()
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCC = NVL(UTCC,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CON_TRAS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_100.enabled = this.oPgFrm.Page1.oPag.oBtn_1_100.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_101.enabled = this.oPgFrm.Page1.oPag.oBtn_1_101.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsag_aca
    * Utilizzato nella check form per controllare al salvataggio
    * se i campi sono stati modificati
    this.w_EDITCHECK = LEFT( this.w_COTIPDOC + SPACE( 5 ), 5 )
    this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COTIPATT + SPACE( 50 ),50 )
    this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COGRUPAR + SPACE( 5 ), 5 )
    this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COCODPAG + SPACE( 5 ), 5 )
    this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COESCRIN + "N" , 1 )
    this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COPERCON + SPACE( 5 ), 5 )
    this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_CORINCON + "N" , 1 )
    this.w_EDITCHECK = this.w_EDITCHECK + RIGHT( "000" + ALLTRIM( STR( this.w_CONUMGIO , 3 ) ) , 3 )
    this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COCODLIS + SPACE( 5 ), 5 )
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST = ctod("  /  /  ")
      .w_COSERIAL = space(10)
      .w_CODESCON = space(50)
      .w_VALATT = space(20)
      .w_TABKEY = space(8)
      .w_DELIMM = .f.
      .w_COTIPCON = space(1)
      .w_COCODCON = space(15)
      .w_COFLSCOR = space(1)
      .w_CO__NOTE = space(0)
      .w_PATIPRIS = space(1)
      .w_CODESSUP = space(254)
      .w_CODATSTI = ctod("  /  /  ")
      .w_CODATINI = ctod("  /  /  ")
      .w_CODATFIN = ctod("  /  /  ")
      .w_COCODLIS = space(5)
      .w_COTIPATT = space(20)
      .w_COPRIDAT = space(1)
      .w_COFLSOSP = space(1)
      .w_COGRUPAR = space(5)
      .w_COTIPDOC = space(5)
      .w_COPRIDOC = space(1)
      .w_COCODPAG = space(5)
      .w_COTIPSED = space(1)
      .w_COCODSED = space(5)
      .w_COESCRIN = space(1)
      .w_DESCON = space(60)
      .w_COPERCON = space(3)
      .w_MULTIPLO = 0
      .w_CORINCON = space(1)
      .w_ANNI = 0
      .w_CONUMGIO = 0
      .w_CODATFIS = space(1)
      .w_COCODCEN = space(15)
      .w_COCOCOMM = space(15)
      .w_CODATLIM = ctod("  /  /  ")
      .w_DAY = 0
      .w_DESDOC = space(35)
      .w_DESATT = space(254)
      .w_CARAGGST = space(10)
      .w_CATDOC = space(2)
      .w_FLVEAC = space(1)
      .w_TIPRIS = space(1)
      .w_DESPAG = space(30)
      .w_OBTEST = ctod("  /  /  ")
      .w_OB_TEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_FLRIPS = space(1)
      .w_DESLIS = space(40)
      .w_IVALIS = space(1)
      .w_FLSCON = space(1)
      .w_VALLIS = space(3)
      .w_MDDESCRI = space(50)
      .w_EDITCHECK = space(50)
      .w_EDITCHECK2 = space(50)
      .w_DTOBSO = ctod("  /  /  ")
      .w_RESCHK = .f.
      .w_AGGIORNAELEMENTI = .f.
      .w_DPDESCRI = space(60)
      .w_DESCOST = space(40)
      .w_DESCOMM = space(100)
      .w_CENOBSO = ctod("  /  /  ")
      .w_COMOBSO = ctod("  /  /  ")
      .w_ATTEXIST = .f.
      .w_DOCEXIST = .f.
      .w_DTBSO_CAUMATTI = ctod("  /  /  ")
      .w_OKSTAMPA = .f.
      .w_NOMDES = space(40)
      .w_TIPRIF = space(2)
      .w_UTDC = ctot("")
      .w_UTCV = 0
      .w_UTDV = ctot("")
      .w_UTCC = 0
      if .cFunction<>"Filter"
        .w_OBTEST = i_DATSYS
          .DoRTCalc(2,3,.f.)
        .w_VALATT = .w_COSERIAL
        .w_TABKEY = 'CON_TRAS'
        .w_DELIMM = .f.
        .w_COTIPCON = 'C'
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_COCODCON))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,10,.f.)
        .w_PATIPRIS = 'G'
          .DoRTCalc(12,12,.f.)
        .w_CODATSTI = i_datsys
        .w_CODATINI = i_datsys
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_COCODLIS))
          .link_1_20('Full')
          endif
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_COTIPATT))
          .link_1_22('Full')
          endif
        .w_COPRIDAT = 'M'
        .DoRTCalc(19,20,.f.)
          if not(empty(.w_COGRUPAR))
          .link_1_25('Full')
          endif
        .DoRTCalc(21,21,.f.)
          if not(empty(.w_COTIPDOC))
          .link_1_26('Full')
          endif
        .w_COPRIDOC = 'M'
        .DoRTCalc(23,23,.f.)
          if not(empty(.w_COCODPAG))
          .link_1_28('Full')
          endif
        .w_COTIPSED = 'I'
        .w_COCODSED = SPACE(5)
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_COCODSED))
          .link_1_30('Full')
          endif
        .w_COESCRIN = "M"
        .DoRTCalc(27,28,.f.)
          if not(empty(.w_COPERCON))
          .link_1_33('Full')
          endif
        .w_MULTIPLO = MONTH(.w_CODATFIN)-(.w_CONUMGIO/30)
        .w_CORINCON = "M"
        .w_ANNI = INT(.w_MULTIPLO/12)
          .DoRTCalc(32,32,.f.)
        .w_CODATFIS = "M"
        .DoRTCalc(34,34,.f.)
          if not(empty(.w_COCODCEN))
          .link_1_39('Full')
          endif
        .DoRTCalc(35,35,.f.)
          if not(empty(.w_COCOCOMM))
          .link_1_40('Full')
          endif
          .DoRTCalc(36,36,.f.)
        .w_DAY = IIF(MOD(.w_CONUMGIO,30)=0,30,DAY(.w_CODATFIN))
          .DoRTCalc(38,44,.f.)
        .w_OBTEST = I_DATSYS
        .w_OB_TEST = IIF(EMPTY(.w_CODATINI), i_INIDAT, .w_CODATINI)
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
          .DoRTCalc(47,56,.f.)
        .w_RESCHK = .T.
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_104.Calculate()
          .DoRTCalc(58,65,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_OKSTAMPA = .F.
      endif
    endwith
    cp_BlankRecExtFlds(this,'CON_TRAS')
    this.DoRTCalc(68,73,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_100.enabled = this.oPgFrm.Page1.oPag.oBtn_1_100.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_101.enabled = this.oPgFrm.Page1.oPag.oBtn_1_101.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SECONT","I_CODAZI,w_COSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_COSERIAL = .w_COSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCOSERIAL_1_2.enabled = !i_bVal
      .Page1.oPag.oCODESCON_1_3.enabled = i_bVal
      .Page1.oPag.oCOCODCON_1_8.enabled = i_bVal
      .Page1.oPag.oCO__NOTE_1_11.enabled = i_bVal
      .Page1.oPag.oCODESSUP_1_13.enabled = i_bVal
      .Page1.oPag.oCODATSTI_1_15.enabled = i_bVal
      .Page1.oPag.oCODATINI_1_17.enabled = i_bVal
      .Page1.oPag.oCODATFIN_1_19.enabled = i_bVal
      .Page1.oPag.oCOCODLIS_1_20.enabled = i_bVal
      .Page1.oPag.oCOTIPATT_1_22.enabled = i_bVal
      .Page1.oPag.oCOPRIDAT_1_23.enabled = i_bVal
      .Page1.oPag.oCOFLSOSP_1_24.enabled = i_bVal
      .Page1.oPag.oCOGRUPAR_1_25.enabled = i_bVal
      .Page1.oPag.oCOTIPDOC_1_26.enabled = i_bVal
      .Page1.oPag.oCOPRIDOC_1_27.enabled = i_bVal
      .Page1.oPag.oCOCODPAG_1_28.enabled = i_bVal
      .Page1.oPag.oCOTIPSED_1_29.enabled = i_bVal
      .Page1.oPag.oCOCODSED_1_30.enabled = i_bVal
      .Page1.oPag.oCOESCRIN_1_31.enabled = i_bVal
      .Page1.oPag.oCOPERCON_1_33.enabled = i_bVal
      .Page1.oPag.oCORINCON_1_35.enabled = i_bVal
      .Page1.oPag.oCONUMGIO_1_37.enabled = i_bVal
      .Page1.oPag.oCODATFIS_1_38.enabled = i_bVal
      .Page1.oPag.oCOCODCEN_1_39.enabled = i_bVal
      .Page1.oPag.oCOCOCOMM_1_40.enabled = i_bVal
      .Page1.oPag.oBtn_1_43.enabled = .Page1.oPag.oBtn_1_43.mCond()
      .Page1.oPag.oBtn_1_44.enabled = .Page1.oPag.oBtn_1_44.mCond()
      .Page1.oPag.oBtn_1_45.enabled = .Page1.oPag.oBtn_1_45.mCond()
      .Page1.oPag.oBtn_1_63.enabled = .Page1.oPag.oBtn_1_63.mCond()
      .Page1.oPag.oBtn_1_64.enabled = .Page1.oPag.oBtn_1_64.mCond()
      .Page1.oPag.oBtn_1_100.enabled = .Page1.oPag.oBtn_1_100.mCond()
      .Page1.oPag.oBtn_1_101.enabled = .Page1.oPag.oBtn_1_101.mCond()
      .Page1.oPag.oObj_1_77.enabled = i_bVal
      .Page1.oPag.oObj_1_83.enabled = i_bVal
      .Page1.oPag.oObj_1_104.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CON_TRAS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSERIAL,"COSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODESCON,"CODESCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPCON,"COTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODCON,"COCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLSCOR,"COFLSCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CO__NOTE,"CO__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODESSUP,"CODESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATSTI,"CODATSTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATINI,"CODATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATFIN,"CODATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODLIS,"COCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPATT,"COTIPATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPRIDAT,"COPRIDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLSOSP,"COFLSOSP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COGRUPAR,"COGRUPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPDOC,"COTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPRIDOC,"COPRIDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODPAG,"COCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPSED,"COTIPSED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODSED,"COCODSED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COESCRIN,"COESCRIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPERCON,"COPERCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CORINCON,"CORINCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CONUMGIO,"CONUMGIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATFIS,"CODATFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODCEN,"COCODCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOCOMM,"COCOCOMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATLIM,"CODATLIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    i_lTable = "CON_TRAS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CON_TRAS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAG_SCO with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CON_TRAS_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SECONT","I_CODAZI,w_COSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CON_TRAS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CON_TRAS')
        i_extval=cp_InsertValODBCExtFlds(this,'CON_TRAS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(COSERIAL,CODESCON,COTIPCON,COCODCON,COFLSCOR"+;
                  ",CO__NOTE,CODESSUP,CODATSTI,CODATINI,CODATFIN"+;
                  ",COCODLIS,COTIPATT,COPRIDAT,COFLSOSP,COGRUPAR"+;
                  ",COTIPDOC,COPRIDOC,COCODPAG,COTIPSED,COCODSED"+;
                  ",COESCRIN,COPERCON,CORINCON,CONUMGIO,CODATFIS"+;
                  ",COCODCEN,COCOCOMM,CODATLIM,UTDC,UTCV"+;
                  ",UTDV,UTCC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_COSERIAL)+;
                  ","+cp_ToStrODBC(this.w_CODESCON)+;
                  ","+cp_ToStrODBC(this.w_COTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_COCODCON)+;
                  ","+cp_ToStrODBC(this.w_COFLSCOR)+;
                  ","+cp_ToStrODBC(this.w_CO__NOTE)+;
                  ","+cp_ToStrODBC(this.w_CODESSUP)+;
                  ","+cp_ToStrODBC(this.w_CODATSTI)+;
                  ","+cp_ToStrODBC(this.w_CODATINI)+;
                  ","+cp_ToStrODBC(this.w_CODATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_COCODLIS)+;
                  ","+cp_ToStrODBCNull(this.w_COTIPATT)+;
                  ","+cp_ToStrODBC(this.w_COPRIDAT)+;
                  ","+cp_ToStrODBC(this.w_COFLSOSP)+;
                  ","+cp_ToStrODBCNull(this.w_COGRUPAR)+;
                  ","+cp_ToStrODBCNull(this.w_COTIPDOC)+;
                  ","+cp_ToStrODBC(this.w_COPRIDOC)+;
                  ","+cp_ToStrODBCNull(this.w_COCODPAG)+;
                  ","+cp_ToStrODBC(this.w_COTIPSED)+;
                  ","+cp_ToStrODBCNull(this.w_COCODSED)+;
                  ","+cp_ToStrODBC(this.w_COESCRIN)+;
                  ","+cp_ToStrODBCNull(this.w_COPERCON)+;
                  ","+cp_ToStrODBC(this.w_CORINCON)+;
                  ","+cp_ToStrODBC(this.w_CONUMGIO)+;
                  ","+cp_ToStrODBC(this.w_CODATFIS)+;
                  ","+cp_ToStrODBCNull(this.w_COCODCEN)+;
                  ","+cp_ToStrODBCNull(this.w_COCOCOMM)+;
                  ","+cp_ToStrODBC(this.w_CODATLIM)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CON_TRAS')
        i_extval=cp_InsertValVFPExtFlds(this,'CON_TRAS')
        cp_CheckDeletedKey(i_cTable,0,'COSERIAL',this.w_COSERIAL)
        INSERT INTO (i_cTable);
              (COSERIAL,CODESCON,COTIPCON,COCODCON,COFLSCOR,CO__NOTE,CODESSUP,CODATSTI,CODATINI,CODATFIN,COCODLIS,COTIPATT,COPRIDAT,COFLSOSP,COGRUPAR,COTIPDOC,COPRIDOC,COCODPAG,COTIPSED,COCODSED,COESCRIN,COPERCON,CORINCON,CONUMGIO,CODATFIS,COCODCEN,COCOCOMM,CODATLIM,UTDC,UTCV,UTDV,UTCC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_COSERIAL;
                  ,this.w_CODESCON;
                  ,this.w_COTIPCON;
                  ,this.w_COCODCON;
                  ,this.w_COFLSCOR;
                  ,this.w_CO__NOTE;
                  ,this.w_CODESSUP;
                  ,this.w_CODATSTI;
                  ,this.w_CODATINI;
                  ,this.w_CODATFIN;
                  ,this.w_COCODLIS;
                  ,this.w_COTIPATT;
                  ,this.w_COPRIDAT;
                  ,this.w_COFLSOSP;
                  ,this.w_COGRUPAR;
                  ,this.w_COTIPDOC;
                  ,this.w_COPRIDOC;
                  ,this.w_COCODPAG;
                  ,this.w_COTIPSED;
                  ,this.w_COCODSED;
                  ,this.w_COESCRIN;
                  ,this.w_COPERCON;
                  ,this.w_CORINCON;
                  ,this.w_CONUMGIO;
                  ,this.w_CODATFIS;
                  ,this.w_COCODCEN;
                  ,this.w_COCOCOMM;
                  ,this.w_CODATLIM;
                  ,this.w_UTDC;
                  ,this.w_UTCV;
                  ,this.w_UTDV;
                  ,this.w_UTCC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CON_TRAS_IDX,i_nConn)
      *
      * update CON_TRAS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CON_TRAS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CODESCON="+cp_ToStrODBC(this.w_CODESCON)+;
             ",COTIPCON="+cp_ToStrODBC(this.w_COTIPCON)+;
             ",COCODCON="+cp_ToStrODBCNull(this.w_COCODCON)+;
             ",COFLSCOR="+cp_ToStrODBC(this.w_COFLSCOR)+;
             ",CO__NOTE="+cp_ToStrODBC(this.w_CO__NOTE)+;
             ",CODESSUP="+cp_ToStrODBC(this.w_CODESSUP)+;
             ",CODATSTI="+cp_ToStrODBC(this.w_CODATSTI)+;
             ",CODATINI="+cp_ToStrODBC(this.w_CODATINI)+;
             ",CODATFIN="+cp_ToStrODBC(this.w_CODATFIN)+;
             ",COCODLIS="+cp_ToStrODBCNull(this.w_COCODLIS)+;
             ",COTIPATT="+cp_ToStrODBCNull(this.w_COTIPATT)+;
             ",COPRIDAT="+cp_ToStrODBC(this.w_COPRIDAT)+;
             ",COFLSOSP="+cp_ToStrODBC(this.w_COFLSOSP)+;
             ",COGRUPAR="+cp_ToStrODBCNull(this.w_COGRUPAR)+;
             ",COTIPDOC="+cp_ToStrODBCNull(this.w_COTIPDOC)+;
             ",COPRIDOC="+cp_ToStrODBC(this.w_COPRIDOC)+;
             ",COCODPAG="+cp_ToStrODBCNull(this.w_COCODPAG)+;
             ",COTIPSED="+cp_ToStrODBC(this.w_COTIPSED)+;
             ",COCODSED="+cp_ToStrODBCNull(this.w_COCODSED)+;
             ",COESCRIN="+cp_ToStrODBC(this.w_COESCRIN)+;
             ",COPERCON="+cp_ToStrODBCNull(this.w_COPERCON)+;
             ",CORINCON="+cp_ToStrODBC(this.w_CORINCON)+;
             ",CONUMGIO="+cp_ToStrODBC(this.w_CONUMGIO)+;
             ",CODATFIS="+cp_ToStrODBC(this.w_CODATFIS)+;
             ",COCODCEN="+cp_ToStrODBCNull(this.w_COCODCEN)+;
             ",COCOCOMM="+cp_ToStrODBCNull(this.w_COCOCOMM)+;
             ",CODATLIM="+cp_ToStrODBC(this.w_CODATLIM)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CON_TRAS')
        i_cWhere = cp_PKFox(i_cTable  ,'COSERIAL',this.w_COSERIAL  )
        UPDATE (i_cTable) SET;
              CODESCON=this.w_CODESCON;
             ,COTIPCON=this.w_COTIPCON;
             ,COCODCON=this.w_COCODCON;
             ,COFLSCOR=this.w_COFLSCOR;
             ,CO__NOTE=this.w_CO__NOTE;
             ,CODESSUP=this.w_CODESSUP;
             ,CODATSTI=this.w_CODATSTI;
             ,CODATINI=this.w_CODATINI;
             ,CODATFIN=this.w_CODATFIN;
             ,COCODLIS=this.w_COCODLIS;
             ,COTIPATT=this.w_COTIPATT;
             ,COPRIDAT=this.w_COPRIDAT;
             ,COFLSOSP=this.w_COFLSOSP;
             ,COGRUPAR=this.w_COGRUPAR;
             ,COTIPDOC=this.w_COTIPDOC;
             ,COPRIDOC=this.w_COPRIDOC;
             ,COCODPAG=this.w_COCODPAG;
             ,COTIPSED=this.w_COTIPSED;
             ,COCODSED=this.w_COCODSED;
             ,COESCRIN=this.w_COESCRIN;
             ,COPERCON=this.w_COPERCON;
             ,CORINCON=this.w_CORINCON;
             ,CONUMGIO=this.w_CONUMGIO;
             ,CODATFIS=this.w_CODATFIS;
             ,COCODCEN=this.w_COCODCEN;
             ,COCOCOMM=this.w_COCOCOMM;
             ,CODATLIM=this.w_CODATLIM;
             ,UTDC=this.w_UTDC;
             ,UTCV=this.w_UTCV;
             ,UTDV=this.w_UTDV;
             ,UTCC=this.w_UTCC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CON_TRAS_IDX,i_nConn)
      *
      * delete CON_TRAS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'COSERIAL',this.w_COSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,24,.t.)
        if .o_COTIPSED<>.w_COTIPSED
            .w_COCODSED = SPACE(5)
          .link_1_30('Full')
        endif
        .DoRTCalc(26,28,.t.)
            .w_MULTIPLO = MONTH(.w_CODATFIN)-(.w_CONUMGIO/30)
        .DoRTCalc(30,30,.t.)
            .w_ANNI = INT(.w_MULTIPLO/12)
        .DoRTCalc(32,36,.t.)
            .w_DAY = IIF(MOD(.w_CONUMGIO,30)=0,30,DAY(.w_CODATFIN))
        .DoRTCalc(38,45,.t.)
        if .o_CODATINI<>.w_CODATINI
            .w_OB_TEST = IIF(EMPTY(.w_CODATINI), i_INIDAT, .w_CODATINI)
        endif
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_104.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SECONT","I_CODAZI,w_COSERIAL")
          .op_COSERIAL = .w_COSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(47,73,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_104.Calculate()
    endwith
  return

  proc Calculate_HBWXDEHEWF()
    with this
          * --- Sbianca periodicit� - rinnovo tacito - giorni di preavviso
          .w_COPERCON = IIF( .w_COESCRIN <>"N", "   ", .w_COPERCON )
          .link_1_33('Full')
          .w_CORINCON = IIF( .w_COESCRIN = "S", "M", .w_CORINCON )
          .w_CONUMGIO = IIF( .w_COESCRIN = "S", 0, .w_CONUMGIO )
    endwith
  endproc
  proc Calculate_JDNBNZASFA()
    with this
          * --- Sbianca giorni di preavviso
          .w_CONUMGIO = IIF( .w_CORINCON <> "S", 0, .w_CONUMGIO )
    endwith
  endproc
  proc Calculate_CZGHMRBITS()
    with this
          * --- Cancellazione allegati collegati
          gsma_bae(this;
              ,'I';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCOPERCON_1_33.enabled = this.oPgFrm.Page1.oPag.oCOPERCON_1_33.mCond()
    this.oPgFrm.Page1.oPag.oCORINCON_1_35.enabled = this.oPgFrm.Page1.oPag.oCORINCON_1_35.mCond()
    this.oPgFrm.Page1.oPag.oCONUMGIO_1_37.enabled = this.oPgFrm.Page1.oPag.oCONUMGIO_1_37.mCond()
    this.oPgFrm.Page1.oPag.oCODATFIS_1_38.enabled = this.oPgFrm.Page1.oPag.oCODATFIS_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_100.enabled = this.oPgFrm.Page1.oPag.oBtn_1_100.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_101.enabled = this.oPgFrm.Page1.oPag.oBtn_1_101.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCOCODLIS_1_20.visible=!this.oPgFrm.Page1.oPag.oCOCODLIS_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCOCODSED_1_30.visible=!this.oPgFrm.Page1.oPag.oCOCODSED_1_30.mHide()
    this.oPgFrm.Page1.oPag.oCOCODCEN_1_39.visible=!this.oPgFrm.Page1.oPag.oCOCODCEN_1_39.mHide()
    this.oPgFrm.Page1.oPag.oCOCOCOMM_1_40.visible=!this.oPgFrm.Page1.oPag.oCOCOCOMM_1_40.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_43.visible=!this.oPgFrm.Page1.oPag.oBtn_1_43.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_44.visible=!this.oPgFrm.Page1.oPag.oBtn_1_44.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_45.visible=!this.oPgFrm.Page1.oPag.oBtn_1_45.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_63.visible=!this.oPgFrm.Page1.oPag.oBtn_1_63.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_64.visible=!this.oPgFrm.Page1.oPag.oBtn_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS_1_66.visible=!this.oPgFrm.Page1.oPag.oDESLIS_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_88.visible=!this.oPgFrm.Page1.oPag.oStr_1_88.mHide()
    this.oPgFrm.Page1.oPag.oDESCOST_1_92.visible=!this.oPgFrm.Page1.oPag.oDESCOST_1_92.mHide()
    this.oPgFrm.Page1.oPag.oDESCOMM_1_93.visible=!this.oPgFrm.Page1.oPag.oDESCOMM_1_93.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_100.visible=!this.oPgFrm.Page1.oPag.oBtn_1_100.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_101.visible=!this.oPgFrm.Page1.oPag.oBtn_1_101.mHide()
    this.oPgFrm.Page1.oPag.oNOMDES_1_108.visible=!this.oPgFrm.Page1.oPag.oNOMDES_1_108.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
        if lower(cEvent)==lower("w_COESCRIN Changed")
          .Calculate_HBWXDEHEWF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CORINCON Changed")
          .Calculate_JDNBNZASFA()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_83.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_104.Event(cEvent)
        if lower(cEvent)==lower("Delete start")
          .Calculate_CZGHMRBITS()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_aca
    if lower(cEvent)='aprivisualizzazioneelementicontratto'
       local L_OBJ_MASK
       L_OBJ_MASK=GSAG_KVC()
       SetValueLinked("MT", L_OBJ_MASK, "w_CODNOM", this.w_COCODCON, L_OBJ_MASK.w_TIPCON)
       SetValueLinked("MT", L_OBJ_MASK, "w_CODCONTR", this.w_COSERIAL)
       SetValueLinked("MT", L_OBJ_MASK, "w_CODCONTR2", this.w_COSERIAL)
       L_OBJ_MASK.NotifyEvent("Search")
       L_OBJ_MASK=.NULL.
    endif
    
    if cEvent='ApriGestioneElementiContratto'
       local L_OBJ_MASK, l_tipcon, l_contra,l_okapri
       l_okapri=.f.
        l_tipcon=this.w_COTIPCON
        l_contra=this.w_COSERIAL
       if this.cFunction <> "Query"
          if ah_YesNo("Si desidera salvare il contratto e procedere al caricamento di un nuovo elemento?")
         this.ecpSave()
           l_okapri=.t.
       endif
       else
         l_okapri=.t.
       endif
       if l_okapri
       L_OBJ_MASK=GSAG_AEL()
       L_OBJ_MASK.ecpLoad()
       SetValueLinked("MT", L_OBJ_MASK, "w_TIPCON", l_tipcon)
       SetValueLinked("MT", L_OBJ_MASK, "w_ELCONTRA", l_contra)
       L_OBJ_MASK=.NULL.
       endif
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODCON
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANSCORPO,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANSCORPO,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANSCORPO,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANSCORPO,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCODCON_1_8'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANSCORPO,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANSCORPO,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore incongruente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANSCORPO,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANSCORPO,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANSCORPO,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANSCORPO,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(60))
      this.w_COFLSCOR = NVL(_Link_.ANSCORPO,space(1))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCODCON = space(15)
      endif
      this.w_DESCON = space(60)
      this.w_COFLSCOR = space(1)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore incongruente o obsoleto")
        endif
        this.w_COCODCON = space(15)
        this.w_DESCON = space(60)
        this.w_COFLSCOR = space(1)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.ANCODICE as ANCODICE108"+ ",link_1_8.ANDESCRI as ANDESCRI108"+ ",link_1_8.ANSCORPO as ANSCORPO108"+ ",link_1_8.ANDTOBSO as ANDTOBSO108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on CON_TRAS.COCODCON=link_1_8.ANCODICE"+" and CON_TRAS.COTIPCON=link_1_8.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and CON_TRAS.COCODCON=link_1_8.ANCODICE(+)"'+'+" and CON_TRAS.COTIPCON=link_1_8.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCODLIS
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_COCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_COCODLIS))
          select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oCOCODLIS_1_20'),i_cWhere,'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_COCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_COCODLIS)
            select LSCODLIS,LSDESLIS,LSIVALIS,LSFLSCON,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_FLSCON = NVL(_Link_.LSFLSCON,space(1))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_COCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_IVALIS = space(1)
      this.w_FLSCON = space(1)
      this.w_VALLIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VALLIS=g_PERVAL 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta listino incongruente")
        endif
        this.w_COCODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_IVALIS = space(1)
        this.w_FLSCON = space(1)
        this.w_VALLIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.LSCODLIS as LSCODLIS120"+ ",link_1_20.LSDESLIS as LSDESLIS120"+ ",link_1_20.LSIVALIS as LSIVALIS120"+ ",link_1_20.LSFLSCON as LSFLSCON120"+ ",link_1_20.LSVALLIS as LSVALLIS120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on CON_TRAS.COCODLIS=link_1_20.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and CON_TRAS.COCODLIS=link_1_20.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COTIPATT
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COTIPATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_COTIPATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_COTIPATT))
          select CACODICE,CADESCRI,CARAGGST;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COTIPATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COTIPATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oCOTIPATT_1_22'),i_cWhere,'GSAG_MCA',"Tipi attivita",'GSAG_MCA.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COTIPATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_COTIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_COTIPATT)
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COTIPATT = NVL(_Link_.CACODICE,space(20))
      this.w_DESATT = NVL(_Link_.CADESCRI,space(254))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_COTIPATT = space(20)
      endif
      this.w_DESATT = space(254)
      this.w_CARAGGST = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CARAGGST<>'Z'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COTIPATT = space(20)
        this.w_DESATT = space(254)
        this.w_CARAGGST = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COTIPATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.CACODICE as CACODICE122"+ ",link_1_22.CADESCRI as CADESCRI122"+ ",link_1_22.CARAGGST as CARAGGST122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on CON_TRAS.COTIPATT=link_1_22.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and CON_TRAS.COTIPATT=link_1_22.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COGRUPAR
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COGRUPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_COGRUPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_COGRUPAR))
          select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COGRUPAR)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COGRUPAR) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oCOGRUPAR_1_25'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COGRUPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_COGRUPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_COGRUPAR)
            select DPCODICE,DPTIPRIS,DPDTOBSO,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COGRUPAR = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
      this.w_DPDESCRI = NVL(_Link_.DPDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_COGRUPAR = space(5)
      endif
      this.w_TIPRIS = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DPDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIS='G' AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore incongruente o obsoleto")
        endif
        this.w_COGRUPAR = space(5)
        this.w_TIPRIS = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DPDESCRI = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COGRUPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_25(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_25.DPCODICE as DPCODICE125"+ ",link_1_25.DPTIPRIS as DPTIPRIS125"+ ",link_1_25.DPDTOBSO as DPDTOBSO125"+ ",link_1_25.DPDESCRI as DPDESCRI125"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_25 on CON_TRAS.COGRUPAR=link_1_25.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_25"
          i_cKey=i_cKey+'+" and CON_TRAS.COGRUPAR=link_1_25.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COTIPDOC
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_COTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_COTIPDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCOTIPDOC_1_26'),i_cWhere,'GSVE_ATD',"Tipi documento",'GSAG_CAU.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_COTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_COTIPDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC,TDFLRIPS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLRIPS = NVL(_Link_.TDFLRIPS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COTIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_CATDOC = space(2)
      this.w_FLVEAC = space(1)
      this.w_FLRIPS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CATDOC<>'OR' AND .w_FLVEAC='V' and (! Isahe() or .w_FLRIPS='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo")
        endif
        this.w_COTIPDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_CATDOC = space(2)
        this.w_FLVEAC = space(1)
        this.w_FLRIPS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.TDTIPDOC as TDTIPDOC126"+ ",link_1_26.TDDESDOC as TDDESDOC126"+ ",link_1_26.TDCATDOC as TDCATDOC126"+ ",link_1_26.TDFLVEAC as TDFLVEAC126"+ ",link_1_26.TDFLRIPS as TDFLRIPS126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on CON_TRAS.COTIPDOC=link_1_26.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and CON_TRAS.COTIPDOC=link_1_26.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCODPAG
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_COCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_COCODPAG))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oCOCODPAG_1_28'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_COCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_COCODPAG)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(_Link_.PADESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_COCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_28(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_28.PACODICE as PACODICE128"+ ",link_1_28.PADESCRI as PADESCRI128"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_28 on CON_TRAS.COCODPAG=link_1_28.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_28"
          i_cKey=i_cKey+'+" and CON_TRAS.COCODPAG=link_1_28.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCODSED
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_COCODSED)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_COCODCON);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_COTIPCON;
                     ,'DDCODICE',this.w_COCODCON;
                     ,'DDCODDES',trim(this.w_COCODSED))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODSED)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODSED) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCOCODSED_1_30'),i_cWhere,'',"Sedi",'GSAG_SED.DES_DIVE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);
           .or. this.w_COCODCON<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_COCODCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_COCODSED);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_COCODCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_COTIPCON;
                       ,'DDCODICE',this.w_COCODCON;
                       ,'DDCODDES',this.w_COCODSED)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODSED = NVL(_Link_.DDCODDES,space(5))
      this.w_NOMDES = NVL(_Link_.DDNOMDES,space(40))
      this.w_TIPRIF = NVL(_Link_.DDTIPRIF,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_COCODSED = space(5)
      endif
      this.w_NOMDES = space(40)
      this.w_TIPRIF = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_COCODSED) OR .w_TIPRIF='CO')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        endif
        this.w_COCODSED = space(5)
        this.w_NOMDES = space(40)
        this.w_TIPRIF = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COPERCON
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COPERCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMD',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_COPERCON)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_COPERCON))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COPERCON)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStrODBC(trim(this.w_COPERCON)+"%");

            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStr(trim(this.w_COPERCON)+"%");

            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COPERCON) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oCOPERCON_1_33'),i_cWhere,'GSAR_AMD',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COPERCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_COPERCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_COPERCON)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COPERCON = NVL(_Link_.MDCODICE,space(3))
      this.w_MDDESCRI = NVL(_Link_.MDDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_COPERCON = space(3)
      endif
      this.w_MDDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COPERCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_33(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODCLDAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_33.MDCODICE as MDCODICE133"+ ",link_1_33.MDDESCRI as MDDESCRI133"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_33 on CON_TRAS.COPERCON=link_1_33.MDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_33"
          i_cKey=i_cKey+'+" and CON_TRAS.COPERCON=link_1_33.MDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCODCEN
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_COCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_COCODCEN))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_COCODCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_COCODCEN)+"%");

            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCOCODCEN_1_39'),i_cWhere,'GSCA_ACC',"Centri di ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_COCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_COCODCEN)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCOST = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCODCEN = space(15)
      endif
      this.w_DESCOST = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.), CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
        endif
        this.w_COCODCEN = space(15)
        this.w_DESCOST = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_39(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_39.CC_CONTO as CC_CONTO139"+ ",link_1_39.CCDESPIA as CCDESPIA139"+ ",link_1_39.CCDTOBSO as CCDTOBSO139"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_39 on CON_TRAS.COCODCEN=link_1_39.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_39"
          i_cKey=i_cKey+'+" and CON_TRAS.COCODCEN=link_1_39.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOCOMM
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOCOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COCOCOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COCOCOMM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOCOMM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_COCOCOMM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_COCOCOMM)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCOCOMM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOCOCOMM_1_40'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOCOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COCOCOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COCOCOMM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOCOMM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOMM = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCOCOMM = space(15)
      endif
      this.w_DESCOMM = space(100)
      this.w_COMOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.), CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_COCOCOMM = space(15)
        this.w_DESCOMM = space(100)
        this.w_COMOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOCOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_40.CNCODCAN as CNCODCAN140"+ ",link_1_40.CNDESCAN as CNDESCAN140"+ ",link_1_40.CNDTOBSO as CNDTOBSO140"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_40 on CON_TRAS.COCOCOMM=link_1_40.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_40"
          i_cKey=i_cKey+'+" and CON_TRAS.COCOCOMM=link_1_40.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOSERIAL_1_2.value==this.w_COSERIAL)
      this.oPgFrm.Page1.oPag.oCOSERIAL_1_2.value=this.w_COSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_1_3.value==this.w_CODESCON)
      this.oPgFrm.Page1.oPag.oCODESCON_1_3.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODCON_1_8.value==this.w_COCODCON)
      this.oPgFrm.Page1.oPag.oCOCODCON_1_8.value=this.w_COCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCO__NOTE_1_11.value==this.w_CO__NOTE)
      this.oPgFrm.Page1.oPag.oCO__NOTE_1_11.value=this.w_CO__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESSUP_1_13.value==this.w_CODESSUP)
      this.oPgFrm.Page1.oPag.oCODESSUP_1_13.value=this.w_CODESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATSTI_1_15.value==this.w_CODATSTI)
      this.oPgFrm.Page1.oPag.oCODATSTI_1_15.value=this.w_CODATSTI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATINI_1_17.value==this.w_CODATINI)
      this.oPgFrm.Page1.oPag.oCODATINI_1_17.value=this.w_CODATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATFIN_1_19.value==this.w_CODATFIN)
      this.oPgFrm.Page1.oPag.oCODATFIN_1_19.value=this.w_CODATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODLIS_1_20.value==this.w_COCODLIS)
      this.oPgFrm.Page1.oPag.oCOCODLIS_1_20.value=this.w_COCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCOTIPATT_1_22.value==this.w_COTIPATT)
      this.oPgFrm.Page1.oPag.oCOTIPATT_1_22.value=this.w_COTIPATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPRIDAT_1_23.RadioValue()==this.w_COPRIDAT)
      this.oPgFrm.Page1.oPag.oCOPRIDAT_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLSOSP_1_24.RadioValue()==this.w_COFLSOSP)
      this.oPgFrm.Page1.oPag.oCOFLSOSP_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOGRUPAR_1_25.value==this.w_COGRUPAR)
      this.oPgFrm.Page1.oPag.oCOGRUPAR_1_25.value=this.w_COGRUPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCOTIPDOC_1_26.value==this.w_COTIPDOC)
      this.oPgFrm.Page1.oPag.oCOTIPDOC_1_26.value=this.w_COTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPRIDOC_1_27.RadioValue()==this.w_COPRIDOC)
      this.oPgFrm.Page1.oPag.oCOPRIDOC_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODPAG_1_28.value==this.w_COCODPAG)
      this.oPgFrm.Page1.oPag.oCOCODPAG_1_28.value=this.w_COCODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCOTIPSED_1_29.RadioValue()==this.w_COTIPSED)
      this.oPgFrm.Page1.oPag.oCOTIPSED_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODSED_1_30.value==this.w_COCODSED)
      this.oPgFrm.Page1.oPag.oCOCODSED_1_30.value=this.w_COCODSED
    endif
    if not(this.oPgFrm.Page1.oPag.oCOESCRIN_1_31.RadioValue()==this.w_COESCRIN)
      this.oPgFrm.Page1.oPag.oCOESCRIN_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_32.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_32.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPERCON_1_33.value==this.w_COPERCON)
      this.oPgFrm.Page1.oPag.oCOPERCON_1_33.value=this.w_COPERCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCORINCON_1_35.RadioValue()==this.w_CORINCON)
      this.oPgFrm.Page1.oPag.oCORINCON_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONUMGIO_1_37.value==this.w_CONUMGIO)
      this.oPgFrm.Page1.oPag.oCONUMGIO_1_37.value=this.w_CONUMGIO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATFIS_1_38.RadioValue()==this.w_CODATFIS)
      this.oPgFrm.Page1.oPag.oCODATFIS_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODCEN_1_39.value==this.w_COCODCEN)
      this.oPgFrm.Page1.oPag.oCOCODCEN_1_39.value=this.w_COCODCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOCOMM_1_40.value==this.w_COCOCOMM)
      this.oPgFrm.Page1.oPag.oCOCOCOMM_1_40.value=this.w_COCOCOMM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_47.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_47.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_50.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_50.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_57.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_57.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_66.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_66.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMDDESCRI_1_70.value==this.w_MDDESCRI)
      this.oPgFrm.Page1.oPag.oMDDESCRI_1_70.value=this.w_MDDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDPDESCRI_1_91.value==this.w_DPDESCRI)
      this.oPgFrm.Page1.oPag.oDPDESCRI_1_91.value=this.w_DPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOST_1_92.value==this.w_DESCOST)
      this.oPgFrm.Page1.oPag.oDESCOST_1_92.value=this.w_DESCOST
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOMM_1_93.value==this.w_DESCOMM)
      this.oPgFrm.Page1.oPag.oDESCOMM_1_93.value=this.w_DESCOMM
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMDES_1_108.value==this.w_NOMDES)
      this.oPgFrm.Page1.oPag.oNOMDES_1_108.value=this.w_NOMDES
    endif
    cp_SetControlsValueExtFlds(this,'CON_TRAS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not((.w_COTIPSED='T' AND NOT EMPTY(NVL(.w_COCODSED,''))) OR .w_COTIPSED<>'T')
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione: occorre inserire una sede di tipo consegna!")
          case   ((empty(.w_COCODCON)) or not((EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCODCON_1_8.SetFocus()
            i_bnoObbl = !empty(.w_COCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore incongruente o obsoleto")
          case   (empty(.w_CODATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATINI_1_17.SetFocus()
            i_bnoObbl = !empty(.w_CODATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CODATFIN)) or not(NOT .w_CODATFIN<.w_CODATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATFIN_1_19.SetFocus()
            i_bnoObbl = !empty(.w_CODATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data fine contratto inferiore alla data di inizio")
          case   not(.w_VALLIS=g_PERVAL )  and not(Isahe())  and not(empty(.w_COCODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCODLIS_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta listino incongruente")
          case   not(.w_CARAGGST<>'Z')  and not(empty(.w_COTIPATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOTIPATT_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPRIS='G' AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST))  and not(empty(.w_COGRUPAR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOGRUPAR_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore incongruente o obsoleto")
          case   not(.w_CATDOC<>'OR' AND .w_FLVEAC='V' and (! Isahe() or .w_FLRIPS='S'))  and not(empty(.w_COTIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOTIPDOC_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo")
          case   not((EMPTY(.w_COCODSED) OR .w_TIPRIF='CO'))  and not(.w_COTIPSED<>'T')  and not(empty(.w_COCODSED))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCODSED_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
          case   (empty(.w_COPERCON))  and (.w_COESCRIN = "N" )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOPERCON_1_33.SetFocus()
            i_bnoObbl = !empty(.w_COPERCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.), CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.) ))  and not(g_PERCCR<>'S')  and not(empty(.w_COCODCEN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCODCEN_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.), CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.) ))  and not(NOT(g_PERCAN='S' OR g_COMM='S' ))  and not(empty(.w_COCOCOMM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOCOMM_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_aca
      * this.w_EDITCHECK � inizializzato nella load record end per controllare al salvataggio
      * se i campi sono stati modificati
      if i_bRes
         if this.cFunction = "Edit"
          this.w_EDITCHECK = LEFT( this.w_COTIPDOC + SPACE( 5 ), 5 )
          this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COTIPATT + SPACE( 50 ),50 )
          this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COGRUPAR + SPACE( 5 ), 5 )
          this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COCODPAG + SPACE( 5 ), 5 )
          this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COESCRIN + "N" , 1 )
          this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COPERCON + SPACE( 5 ), 5 )
          this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_CORINCON + "N" , 1 )
          this.w_EDITCHECK = this.w_EDITCHECK + RIGHT( "000" + ALLTRIM( STR( this.w_CONUMGIO , 3 ) ) , 3 )
          this.w_EDITCHECK = this.w_EDITCHECK + LEFT( this.w_COCODLIS + SPACE( 5 ), 5 )
         endif
      endif
      
      if i_bRes
         if this.cFunction = "Edit"
           .w_RESCHK = .t.
           this.notifyevent('Chkfinali')
           i_bRes = .w_RESCHK
           this.w_AGGIORNAELEMENTI = this.w_AGGIORNAELEMENTI AND AH_YESNO("Si desidera aggiornare i corrispondenti elementi contratto?")
         endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODATINI = this.w_CODATINI
    this.o_COTIPSED = this.w_COTIPSED
    return

  func CanDelete()
    local i_res
    i_res=GSMA_BAE(this,'G')
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione annullata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsag_acaPag1 as StdContainer
  Width  = 785
  height = 524
  stdWidth  = 785
  stdheight = 524
  resizeXpos=327
  resizeYpos=111
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOSERIAL_1_2 as StdField with uid="GALVRLXDOX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_COSERIAL", cQueryName = "COSERIAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 136291214,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=123, Top=15, InputMask=replicate('X',10)

  add object oCODESCON_1_3 as StdField with uid="DFVPQWYKVV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 235967372,;
   bGlobalFont=.t.,;
    Height=21, Width=425, Left=245, Top=14, InputMask=replicate('X',50)

  add object oCOCODCON_1_8 as StdField with uid="WJHDJCBBTJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_COCODCON", cQueryName = "COCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore incongruente o obsoleto",;
    ToolTipText = "Cliente",;
    HelpContextID = 251044748,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=123, Top=41, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCODCON"

  func oCOCODCON_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
      if .not. empty(.w_COCODSED)
        bRes2=.link_1_30('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCOCODCON_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODCON_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCODCON_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCOCODCON_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCODCON
     i_obj.ecpSave()
  endproc

  add object oCO__NOTE_1_11 as StdMemo with uid="DCDYYVMZCV",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CO__NOTE", cQueryName = "CO__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note aggiuntive",;
    HelpContextID = 230366315,;
   bGlobalFont=.t.,;
    Height=57, Width=547, Left=123, Top=68

  add object oCODESSUP_1_13 as StdField with uid="YTDJCNIRYN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODESSUP", cQueryName = "CODESSUP",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione parametrica",;
    HelpContextID = 32468086,;
   bGlobalFont=.t.,;
    Height=21, Width=547, Left=123, Top=133, InputMask=replicate('X',254)

  add object oCODATSTI_1_15 as StdField with uid="BGCMJVMJYW",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODATSTI", cQueryName = "CODATSTI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data stipulazione",;
    HelpContextID = 33254511,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=123, Top=161

  add object oCODATINI_1_17 as StdField with uid="GHRBJSCKNA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODATINI", cQueryName = "CODATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 134517649,;
   bGlobalFont=.t.,;
    Height=21, Width=78, Left=408, Top=161

  add object oCODATFIN_1_19 as StdField with uid="NZUWADBGKE",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODATFIN", cQueryName = "CODATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data fine contratto inferiore alla data di inizio",;
    ToolTipText = "Data fine validit�",;
    HelpContextID = 83586164,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=594, Top=161

  func oCODATFIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT .w_CODATFIN<.w_CODATINI)
    endwith
    return bRes
  endfunc

  add object oCOCODLIS_1_20 as StdField with uid="XGWRVDFFGC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_COCODLIS", cQueryName = "COCODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta listino incongruente",;
    ToolTipText = "Codice listino",;
    HelpContextID = 168385657,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=123, Top=188, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_COCODLIS"

  func oCOCODLIS_1_20.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oCOCODLIS_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODLIS_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODLIS_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oCOCODLIS_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oCOCODLIS_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_COCODLIS
     i_obj.ecpSave()
  endproc

  add object oCOTIPATT_1_22 as StdField with uid="OTUOJMHIFC",rtseq=17,rtrep=.f.,;
    cFormVar = "w_COTIPATT", cQueryName = "COTIPATT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 3904390,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=123, Top=224, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_COTIPATT"

  func oCOTIPATT_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOTIPATT_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOTIPATT_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oCOTIPATT_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",'GSAG_MCA.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oCOTIPATT_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_COTIPATT
     i_obj.ecpSave()
  endproc


  add object oCOPRIDAT_1_23 as StdCombo with uid="OUHRKAQPYZ",rtseq=18,rtrep=.f.,left=624,top=224,width=103,height=22;
    , ToolTipText = "In base alla valorizzazione di queste combo box dobbiamo ottenere i seguenti metodi di calcolo della prima data da proporre come prossima attivit� o prossimo documento";
    , HelpContextID = 228774790;
    , cFormVar="w_COPRIDAT",RowSource=""+"Inizio periodo,"+"Da periodicit�,"+"Fine periodo,"+"Da modello", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOPRIDAT_1_23.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'F',;
    iif(this.value =4,'M',;
    space(1))))))
  endfunc
  func oCOPRIDAT_1_23.GetRadio()
    this.Parent.oContained.w_COPRIDAT = this.RadioValue()
    return .t.
  endfunc

  func oCOPRIDAT_1_23.SetRadio()
    this.Parent.oContained.w_COPRIDAT=trim(this.Parent.oContained.w_COPRIDAT)
    this.value = ;
      iif(this.Parent.oContained.w_COPRIDAT=='I',1,;
      iif(this.Parent.oContained.w_COPRIDAT=='P',2,;
      iif(this.Parent.oContained.w_COPRIDAT=='F',3,;
      iif(this.Parent.oContained.w_COPRIDAT=='M',4,;
      0))))
  endfunc

  add object oCOFLSOSP_1_24 as StdCheck with uid="GMFUKYATSI",rtseq=19,rtrep=.f.,left=205, top=160, caption="Sospeso",;
    ToolTipText = "Flag sospeso",;
    HelpContextID = 34173834,;
    cFormVar="w_COFLSOSP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOFLSOSP_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCOFLSOSP_1_24.GetRadio()
    this.Parent.oContained.w_COFLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oCOFLSOSP_1_24.SetRadio()
    this.Parent.oContained.w_COFLSOSP=trim(this.Parent.oContained.w_COFLSOSP)
    this.value = ;
      iif(this.Parent.oContained.w_COFLSOSP=='S',1,;
      0)
  endfunc

  add object oCOGRUPAR_1_25 as StdField with uid="IDAESVUSZJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_COGRUPAR", cQueryName = "COGRUPAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valore incongruente o obsoleto",;
    HelpContextID = 14902152,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=123, Top=249, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_COGRUPAR"

  func oCOGRUPAR_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOGRUPAR_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOGRUPAR_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oCOGRUPAR_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oCOGRUPAR_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_COGRUPAR
     i_obj.ecpSave()
  endproc

  add object oCOTIPDOC_1_26 as StdField with uid="STQPYILYYQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_COTIPDOC", cQueryName = "COTIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente o di tipo ordine e/o del ciclo acquisti o senza ricalcolo prezzo",;
    HelpContextID = 222008215,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=123, Top=289, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_COTIPDOC"

  func oCOTIPDOC_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOTIPDOC_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOTIPDOC_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCOTIPDOC_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Tipi documento",'GSAG_CAU.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oCOTIPDOC_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_COTIPDOC
     i_obj.ecpSave()
  endproc


  add object oCOPRIDOC_1_27 as StdCombo with uid="VEFEHMYAAF",rtseq=22,rtrep=.f.,left=624,top=286,width=103,height=22;
    , ToolTipText = "In base alla valorizzazione di queste combo box dobbiamo ottenere i seguenti metodi di calcolo della prima data da proporre come prossima attivit� o prossimo documento";
    , HelpContextID = 228774807;
    , cFormVar="w_COPRIDOC",RowSource=""+"Inizio periodo,"+"Da periodicit�,"+"Fine periodo,"+"Da modello", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOPRIDOC_1_27.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'P',;
    iif(this.value =3,'F',;
    iif(this.value =4,'M',;
    space(1))))))
  endfunc
  func oCOPRIDOC_1_27.GetRadio()
    this.Parent.oContained.w_COPRIDOC = this.RadioValue()
    return .t.
  endfunc

  func oCOPRIDOC_1_27.SetRadio()
    this.Parent.oContained.w_COPRIDOC=trim(this.Parent.oContained.w_COPRIDOC)
    this.value = ;
      iif(this.Parent.oContained.w_COPRIDOC=='I',1,;
      iif(this.Parent.oContained.w_COPRIDOC=='P',2,;
      iif(this.Parent.oContained.w_COPRIDOC=='F',3,;
      iif(this.Parent.oContained.w_COPRIDOC=='M',4,;
      0))))
  endfunc

  add object oCOCODPAG_1_28 as StdField with uid="ASKOFPGSXQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_COCODPAG", cQueryName = "COCODPAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 32940947,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=123, Top=315, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_COCODPAG"

  func oCOCODPAG_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODPAG_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODPAG_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oCOCODPAG_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oCOCODPAG_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_COCODPAG
     i_obj.ecpSave()
  endproc


  add object oCOTIPSED_1_29 as StdCombo with uid="EUNDZXBVPO",rtseq=24,rtrep=.f.,left=123,top=341,width=102,height=21;
    , ToolTipText = "Tipo sede";
    , HelpContextID = 29650026;
    , cFormVar="w_COTIPSED",RowSource=""+"Da impianti,"+"Da cliente,"+"Da contratto,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOTIPSED_1_29.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'C',;
    iif(this.value =3,'T',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oCOTIPSED_1_29.GetRadio()
    this.Parent.oContained.w_COTIPSED = this.RadioValue()
    return .t.
  endfunc

  func oCOTIPSED_1_29.SetRadio()
    this.Parent.oContained.w_COTIPSED=trim(this.Parent.oContained.w_COTIPSED)
    this.value = ;
      iif(this.Parent.oContained.w_COTIPSED=='I',1,;
      iif(this.Parent.oContained.w_COTIPSED=='C',2,;
      iif(this.Parent.oContained.w_COTIPSED=='T',3,;
      iif(this.Parent.oContained.w_COTIPSED=='N',4,;
      0))))
  endfunc

  add object oCOCODSED_1_30 as StdField with uid="KFACJDWPNH",rtseq=25,rtrep=.f.,;
    cFormVar = "w_COCODSED", cQueryName = "COCODSED",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice destinazione inesistente o di tipo diverso da consegna",;
    HelpContextID = 17390698,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=229, Top=341, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="DDCODICE", oKey_2_2="this.w_COCODCON", oKey_3_1="DDCODDES", oKey_3_2="this.w_COCODSED"

  func oCOCODSED_1_30.mHide()
    with this.Parent.oContained
      return (.w_COTIPSED<>'T')
    endwith
  endfunc

  func oCOCODSED_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODSED_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODSED_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_COCODCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_COCODCON)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCOCODSED_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sedi",'GSAG_SED.DES_DIVE_VZM',this.parent.oContained
  endproc


  add object oCOESCRIN_1_31 as StdCombo with uid="SFRDVXIRRC",rtseq=26,rtrep=.f.,left=123,top=378,width=146,height=21;
    , ToolTipText = "Esclude rinnovi";
    , HelpContextID = 268270708;
    , cFormVar="w_COESCRIN",RowSource=""+"Da modello,"+"S�,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOESCRIN_1_31.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCOESCRIN_1_31.GetRadio()
    this.Parent.oContained.w_COESCRIN = this.RadioValue()
    return .t.
  endfunc

  func oCOESCRIN_1_31.SetRadio()
    this.Parent.oContained.w_COESCRIN=trim(this.Parent.oContained.w_COESCRIN)
    this.value = ;
      iif(this.Parent.oContained.w_COESCRIN=='M',1,;
      iif(this.Parent.oContained.w_COESCRIN=='S',2,;
      iif(this.Parent.oContained.w_COESCRIN=='N',3,;
      0)))
  endfunc

  add object oDESCON_1_32 as StdField with uid="PIRQGBNRNV",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 55684554,;
   bGlobalFont=.t.,;
    Height=21, Width=424, Left=246, Top=41, InputMask=replicate('X',60)

  add object oCOPERCON_1_33 as StdField with uid="PCPYKTKNAF",rtseq=28,rtrep=.f.,;
    cFormVar = "w_COPERCON", cQueryName = "COPERCON",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Periodicit� rinnovo contratti",;
    HelpContextID = 236966796,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=123, Top=402, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", cZoomOnZoom="GSAR_AMD", oKey_1_1="MDCODICE", oKey_1_2="this.w_COPERCON"

  func oCOPERCON_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COESCRIN = "N" )
    endwith
   endif
  endfunc

  func oCOPERCON_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOPERCON_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOPERCON_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oCOPERCON_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMD',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oCOPERCON_1_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MDCODICE=this.parent.oContained.w_COPERCON
     i_obj.ecpSave()
  endproc


  add object oCORINCON_1_35 as StdCombo with uid="ASMOIOOXOX",rtseq=30,rtrep=.f.,left=123,top=429,width=146,height=21;
    , ToolTipText = "Rinnovo tacito";
    , HelpContextID = 240890764;
    , cFormVar="w_CORINCON",RowSource=""+"Da modello,"+"S�,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCORINCON_1_35.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCORINCON_1_35.GetRadio()
    this.Parent.oContained.w_CORINCON = this.RadioValue()
    return .t.
  endfunc

  func oCORINCON_1_35.SetRadio()
    this.Parent.oContained.w_CORINCON=trim(this.Parent.oContained.w_CORINCON)
    this.value = ;
      iif(this.Parent.oContained.w_CORINCON=='M',1,;
      iif(this.Parent.oContained.w_CORINCON=='S',2,;
      iif(this.Parent.oContained.w_CORINCON=='N',3,;
      0)))
  endfunc

  func oCORINCON_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COESCRIN ='N')
    endwith
   endif
  endfunc

  add object oCONUMGIO_1_37 as StdField with uid="GGPAOVIDJS",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CONUMGIO", cQueryName = "CONUMGIO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni entro i quali comunicare la disdetta",;
    HelpContextID = 94375029,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=420, Top=429

  func oCONUMGIO_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COESCRIN ='N'  AND .w_CORINCON = "S")
    endwith
   endif
  endfunc


  add object oCODATFIS_1_38 as StdCombo with uid="VHYPNYADRF",rtseq=33,rtrep=.f.,left=624,top=429,width=103,height=22;
    , ToolTipText = "Data iniziale fissa";
    , HelpContextID = 83586169;
    , cFormVar="w_CODATFIS",RowSource=""+"Da modello,"+"S�,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCODATFIS_1_38.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCODATFIS_1_38.GetRadio()
    this.Parent.oContained.w_CODATFIS = this.RadioValue()
    return .t.
  endfunc

  func oCODATFIS_1_38.SetRadio()
    this.Parent.oContained.w_CODATFIS=trim(this.Parent.oContained.w_CODATFIS)
    this.value = ;
      iif(this.Parent.oContained.w_CODATFIS=='M',1,;
      iif(this.Parent.oContained.w_CODATFIS=='S',2,;
      iif(this.Parent.oContained.w_CODATFIS=='N',3,;
      0)))
  endfunc

  func oCODATFIS_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COESCRIN ='N')
    endwith
   endif
  endfunc

  add object oCOCODCEN_1_39 as StdField with uid="IQQSCDMNSS",rtseq=34,rtrep=.f.,;
    cFormVar = "w_COCODCEN", cQueryName = "COCODCEN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di ricavo incongruente o obsoleto",;
    HelpContextID = 17390708,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=123, Top=469, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_COCODCEN"

  func oCOCODCEN_1_39.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  func oCOCODCEN_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODCEN_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODCEN_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCOCODCEN_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di ricavo",'',this.parent.oContained
  endproc
  proc oCOCODCEN_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_COCODCEN
     i_obj.ecpSave()
  endproc

  add object oCOCOCOMM_1_40 as StdField with uid="FMAOEAGLMX",rtseq=35,rtrep=.f.,;
    cFormVar = "w_COCOCOMM", cQueryName = "COCOCOMM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    HelpContextID = 50766733,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=123, Top=494, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COCOCOMM"

  func oCOCOCOMM_1_40.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc

  func oCOCOCOMM_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOCOMM_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOCOMM_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOCOCOMM_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCOCOCOMM_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_COCOCOMM
     i_obj.ecpSave()
  endproc


  add object oBtn_1_43 as StdButton with uid="OJYMBPOMWQ",left=680, top=15, width=48,height=45,;
    CpPicture="bmp\cattura.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da unit� disco";
    , HelpContextID = 54623782;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'CATTURA', disabledpicture = 'bmp\catturad.bmp',Caption='\<Cattura';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"CON_TRAS",.w_COSERIAL,"GSAG_ACA","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_COSERIAL) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_43.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_44 as StdButton with uid="YOPFSNISFV",left=732, top=15, width=48,height=45,;
    CpPicture="BMP\visualicat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza allegati";
    , HelpContextID = 201515120;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"CON_TRAS",.w_COSERIAL,"GSAG_ACA","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_44.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_COSERIAL) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_44.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_45 as StdButton with uid="BJRFWTRDLX",left=680, top=64, width=48,height=45,;
    CpPicture="bmp\scanner.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da periferica di acquisizione immagini";
    , HelpContextID = 97145126;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'SCANNER', disabledpicture = 'bmp\scannerd.bmp', Caption='\<Scanner';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"CON_TRAS",.w_COSERIAL,"GSAG_ACA","S",MROW(),MCOL(),.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_45.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_COSERIAL) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_45.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DOCM<>'S' OR g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc

  add object oDESDOC_1_47 as StdField with uid="LQBJZJZROB",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 240168394,;
   bGlobalFont=.t.,;
    Height=21, Width=358, Left=197, Top=289, InputMask=replicate('X',35)

  add object oDESATT_1_50 as StdField with uid="ECGJLGLYZZ",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 218344906,;
   bGlobalFont=.t.,;
    Height=21, Width=276, Left=279, Top=224, InputMask=replicate('X',254)

  add object oDESPAG_1_57 as StdField with uid="CEKXLHALVI",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 186953162,;
   bGlobalFont=.t.,;
    Height=21, Width=461, Left=197, Top=315, InputMask=replicate('X',30)


  add object oBtn_1_63 as StdButton with uid="WDJPKKZCQX",left=732, top=64, width=48,height=45,;
    CpPicture="BMP\visuali.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la visualizzazione elementi contratto";
    , HelpContextID = 166096134;
    , tabstop=.f., Caption='\<Apri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_63.Click()
      this.parent.oContained.NotifyEvent("ApriVisualizzazioneElementiContratto")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_63.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_COSERIAL) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_63.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_64 as StdButton with uid="BWMIQQQPFJ",left=732, top=114, width=48,height=45,;
    CpPicture="BMP\NEWOFFE.ICO", caption="", nPag=1;
    , ToolTipText = "Premere per procedere al caricamento di un nuovo elemento contratto";
    , HelpContextID = 253540830;
    , tabstop=.f., Caption='\<Nuovo el.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_64.Click()
      this.parent.oContained.NotifyEvent("ApriGestioneElementiContratto")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_64.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_COSERIAL) AND .not. empty(.w_CODATFIN))
      endwith
    endif
  endfunc

  func oBtn_1_64.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (isalt())
     endwith
    endif
  endfunc

  add object oDESLIS_1_66 as StdField with uid="SWKCQHAJKZ",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 245935562,;
   bGlobalFont=.t.,;
    Height=21, Width=360, Left=195, Top=188, InputMask=replicate('X',40)

  func oDESLIS_1_66.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oMDDESCRI_1_70 as StdField with uid="SYOXWKZIIL",rtseq=53,rtrep=.f.,;
    cFormVar = "w_MDDESCRI", cQueryName = "MDDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 235970033,;
   bGlobalFont=.t.,;
    Height=21, Width=474, Left=184, Top=402, InputMask=replicate('X',50)


  add object oObj_1_77 as cp_runprogram with uid="CFEIFPTTEI",left=-6, top=666, width=165,height=21,;
    caption='GSAG_BC1',;
   bGlobalFont=.t.,;
    prg="GSAG_BC1",;
    cEvent = "Chkfinali",;
    nPag=1;
    , HelpContextID = 240041833


  add object oObj_1_83 as cp_runprogram with uid="UDUWXBMNDV",left=481, top=670, width=220,height=19,;
    caption='GSAG_BC2',;
   bGlobalFont=.t.,;
    prg="GSAG_BC2",;
    cEvent = "Update end,Insert end",;
    nPag=1;
    , ToolTipText = "Aggiorna gli elementi contratto";
    , HelpContextID = 240041832

  add object oDPDESCRI_1_91 as StdField with uid="BQXYASYUCZ",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DPDESCRI", cQueryName = "DPDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 235967105,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=203, Top=249, InputMask=replicate('X',60)

  add object oDESCOST_1_92 as StdField with uid="VPYNQCVTOW",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESCOST", cQueryName = "DESCOST",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 28201526,;
   bGlobalFont=.t.,;
    Height=21, Width=413, Left=245, Top=469, InputMask=replicate('X',40)

  func oDESCOST_1_92.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oDESCOMM_1_93 as StdField with uid="PXOISEMVWC",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESCOMM", cQueryName = "DESCOMM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 72461770,;
   bGlobalFont=.t.,;
    Height=21, Width=412, Left=246, Top=494, InputMask=replicate('X',100)

  func oDESCOMM_1_93.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc


  add object oBtn_1_100 as StdButton with uid="KSUWNHATXF",left=732, top=224, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla visualizzazione delle attivit� generate";
    , HelpContextID = 201515120;
    , Caption='\<Vis. Att.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_100.Click()
      with this.Parent.oContained
        GSAG2BZM(this.Parent.oContained,"AO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_100.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' and not empty(.w_COSERIAL) )
      endwith
    endif
  endfunc

  func oBtn_1_100.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_ATTEXIST)
     endwith
    endif
  endfunc


  add object oBtn_1_101 as StdButton with uid="DCBPALJITA",left=732, top=289, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla visualizzazione dei documenti generati";
    , HelpContextID = 201515120;
    , Caption='\<Vis. Doc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_101.Click()
      with this.Parent.oContained
        GSAG2BZM(this.Parent.oContained,"DO") 
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_101.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query'  and not empty(.w_COSERIAL) )
      endwith
    endif
  endfunc

  func oBtn_1_101.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_DOCEXIST)
     endwith
    endif
  endfunc


  add object oObj_1_104 as cp_runprogram with uid="WMCMDQULXA",left=481, top=695, width=220,height=23,;
    caption='GSAG2BZM(XR)',;
   bGlobalFont=.t.,;
    prg="GSAG2BZM('XR')",;
    cEvent = "Load",;
    nPag=1;
    , ToolTipText = "Aggiorna gli elementi contratto";
    , HelpContextID = 252689203

  add object oNOMDES_1_108 as StdField with uid="DJQKVZQVFM",rtseq=68,rtrep=.f.,;
    cFormVar = "w_NOMDES", cQueryName = "NOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 250676010,;
   bGlobalFont=.t.,;
    Height=21, Width=354, Left=304, Top=341, InputMask=replicate('X',40)

  func oNOMDES_1_108.mHide()
    with this.Parent.oContained
      return (.w_COTIPSED<>'T')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="RMCMDBEFDS",Visible=.t., Left=37, Top=41,;
    Alignment=1, Width=84, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="FPEVOMGJJI",Visible=.t., Left=37, Top=68,;
    Alignment=1, Width=84, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="AVEBAKSBBA",Visible=.t., Left=9, Top=162,;
    Alignment=1, Width=112, Height=18,;
    Caption="Stipulato il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="BCLSPQHBOP",Visible=.t., Left=311, Top=162,;
    Alignment=1, Width=96, Height=18,;
    Caption="Inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ACGUORVPFU",Visible=.t., Left=490, Top=162,;
    Alignment=1, Width=101, Height=18,;
    Caption="Fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="INOTDFWVVX",Visible=.t., Left=37, Top=15,;
    Alignment=1, Width=84, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="SGVGADDMHM",Visible=.t., Left=6, Top=288,;
    Alignment=1, Width=115, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="FZQKYGREYD",Visible=.t., Left=60, Top=249,;
    Alignment=1, Width=62, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="FHHIMOYASQ",Visible=.t., Left=37, Top=224,;
    Alignment=1, Width=84, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="YBRCUUTOWA",Visible=.t., Left=37, Top=315,;
    Alignment=1, Width=84, Height=18,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="FJWAIDMKOO",Visible=.t., Left=37, Top=134,;
    Alignment=1, Width=84, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="KNTMQONIRV",Visible=.t., Left=40, Top=190,;
    Alignment=1, Width=81, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_1_71 as StdString with uid="JOMWBBMPTU",Visible=.t., Left=1, Top=404,;
    Alignment=1, Width=120, Height=18,;
    Caption="Periodicit� rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="TSFVLERUOH",Visible=.t., Left=271, Top=429,;
    Alignment=1, Width=147, Height=18,;
    Caption="Giorni preavviso disdetta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="DSMQHRMATA",Visible=.t., Left=11, Top=378,;
    Alignment=1, Width=110, Height=18,;
    Caption="Escludi rinnovo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="UYGZNYJMTF",Visible=.t., Left=14, Top=429,;
    Alignment=1, Width=107, Height=18,;
    Caption="Rinnovo tacito:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="QBJSUUYVUO",Visible=.t., Left=59, Top=341,;
    Alignment=1, Width=62, Height=18,;
    Caption="Tipo sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="YPYTQEMOYQ",Visible=.t., Left=6, Top=449,;
    Alignment=0, Width=100, Height=18,;
    Caption="Analitica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_87 as StdString with uid="UHHHGWTMRT",Visible=.t., Left=9, Top=469,;
    Alignment=1, Width=112, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="DFUFULWPYZ",Visible=.t., Left=9, Top=494,;
    Alignment=1, Width=112, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_88.mHide()
    with this.Parent.oContained
      return (NOT(g_PERCAN='S' OR g_COMM='S' ))
    endwith
  endfunc

  add object oStr_1_89 as StdString with uid="ZCNLXLYIPQ",Visible=.t., Left=6, Top=357,;
    Alignment=0, Width=100, Height=18,;
    Caption="Rinnovi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_97 as StdString with uid="YZVVOCOKFH",Visible=.t., Left=6, Top=203,;
    Alignment=0, Width=64, Height=19,;
    Caption="Attivit�"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_99 as StdString with uid="HTISCBXRRV",Visible=.t., Left=6, Top=268,;
    Alignment=0, Width=115, Height=19,;
    Caption="Documento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_110 as StdString with uid="ZTFIEWGENC",Visible=.t., Left=559, Top=224,;
    Alignment=1, Width=63, Height=18,;
    Caption="Prima data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_111 as StdString with uid="FGVUDSHIRP",Visible=.t., Left=559, Top=288,;
    Alignment=1, Width=63, Height=18,;
    Caption="Prima data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_116 as StdString with uid="MTQDQFXHKA",Visible=.t., Left=512, Top=429,;
    Alignment=1, Width=110, Height=18,;
    Caption="Data iniziale fissa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_86 as StdBox with uid="COHYAOXIQL",left=9, top=464, width=773,height=1

  add object oBox_1_90 as StdBox with uid="ZFGYPLIRBH",left=9, top=372, width=773,height=1

  add object oBox_1_96 as StdBox with uid="LIPBORAQCJ",left=9, top=219, width=773,height=2

  add object oBox_1_98 as StdBox with uid="TNGEULSYRY",left=9, top=283, width=773,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_aca','CON_TRAS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COSERIAL=CON_TRAS.COSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
