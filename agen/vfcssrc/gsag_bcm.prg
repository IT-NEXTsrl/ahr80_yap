* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bcm                                                        *
*              Carica attivit� per pi� pratiche                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-11-05                                                      *
* Last revis.: 2016-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bcm",oParentObject)
return(i_retval)

define class tgsag_bcm as StdBatch
  * --- Local variables
  w_RESOCON1 = space(0)
  w_ANNULLA = .f.
  w_MESBLOK = space(0)
  w_OBJECT = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                     Carica le attivit� per pi� pratiche
    SELECT * FROM ( this.oParentObject.w_AGKCM_SZOOM.cCursor ) WHERE XCHK = 1 INTO CURSOR PRATICHE
    if RECCOUNT("PRATICHE") = 0
      ah_ErrorMsg("Attenzione: selezionare almeno una pratica",,"")
      * --- Chiusura cursore
      if USED("Pratiche")
        SELECT PRATICHE
        USE
      endif
      i_retcode = 'stop'
      return
    else
      g_SCHEDULER="S"
      g_MSG=""
      this.w_RESOCON1 = ""
      this.w_MESBLOK = ""
      SELECT PRATICHE
      GO TOP
      SCAN
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if NOT EMPTY(g_MSG)
        this.w_RESOCON1 = this.w_RESOCON1+ah_msgformat("Pratica ")+alltrim(PRATICHE.CNCODCAN)+":"+chr(13)+chr(10)+g_MSG+chr(13)+chr(10)
      endif
      g_MSG=""
      ENDSCAN
      g_SCHEDULER="N"
      if Not empty(this.w_RESOCON1) and Ah_yesno("Si desidera vedere log elaborazione?")
        do GSUT_KLG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        ah_ErrorMsg("Generazione attivit� ultimata")
      endif
    endif
    * --- Chiusura cursore
    if USED("Pratiche")
      SELECT PRATICHE
      USE
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                          Carica singola attivit� per una pratica
    this.w_OBJECT = GSAG_AAT()
    * --- Controllo se ha passato il test di accesso
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    this.w_OBJECT.ECPLOAD()     
    this.w_OBJECT.w_ATCAUATT = this.oParentObject.w_TIPOATT
    * --- La GSUT_BVP notifica l'evento 'w_ATCAUATT Changed'
    GSUT_BVP(this,this.w_OBJECT, "w_ATCAUATT", "["+this.w_OBJECT.w_ATCAUATT+"]")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_OBJECT.w_Noautom = .F.
    SetValueLinked("M", this.w_OBJECT,"w_ATCODPRA", PRATICHE.CNCODCAN)
    this.w_OBJECT.NotifyEvent("w_ATCODPRA Changed")     
    if NOT EMPTY(this.oParentObject.w_CODNOMIN)
      SetValueLinked("M", this.w_OBJECT,"w_ATCODNOM", this.oParentObject.w_CODNOMIN)
      this.w_OBJECT.NotifyEvent("w_ATCODNOM Changed")     
    endif
    * --- La data inizio attivit� viene ricalcolata al salvataggio
    this.w_OBJECT.w_ATDATINI = cp_CharToDatetime(DTOC(this.oParentObject.w_DATINI)+" "+this.oParentObject.w_OREINI+":"+this.oParentObject.w_MININI+":00")
    this.w_OBJECT.w_DATINI = this.oParentObject.w_DATINI
    this.w_OBJECT.w_ORAINI = this.oParentObject.w_OREINI
    this.w_OBJECT.w_MININI = this.oParentObject.w_MININI
    * --- Forza la data di inizio attivit�
    this.w_OBJECT.w_FORZADTI = .T.
    * --- La data fine attivit� viene ricalcolata al salvataggio
    this.w_OBJECT.w_ATDATFIN = cp_CharToDatetime(DTOC(this.oParentObject.w_DATFIN)+" "+this.oParentObject.w_OREFIN+":"+this.oParentObject.w_MINFIN+":00")
    this.w_OBJECT.w_DATFIN = this.oParentObject.w_DATFIN
    this.w_OBJECT.w_ORAFIN = this.oParentObject.w_OREFIN
    this.w_OBJECT.w_MINFIN = this.oParentObject.w_MINFIN
    * --- Forza la data di fine attivit�
    this.w_OBJECT.w_FORZADTF = .T.
    this.w_OBJECT.w_ATSTATUS = this.oParentObject.w_STATO_ATT
    this.w_OBJECT.w_ATOGGETT = this.oParentObject.w_OGGE_ATT
    this.w_OBJECT.w_ATNOTPIA = this.oParentObject.w_NOTE_ATT
    this.w_OBJECT.w_tipomask = "C"
    * --- Grazie a queste 2 variabili l'anagrafica si chiude
    this.w_OBJECT.w_MaskCalend = this.oParentObject
    this.w_OBJECT.w_AggiornaCalend = .T.
    this.w_OBJECT.EcpSave()     
    this.w_OBJECT = .NULL.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
