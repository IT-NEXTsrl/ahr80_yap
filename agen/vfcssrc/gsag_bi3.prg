* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bi3                                                        *
*              Applica il gruppo di default                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-22                                                      *
* Last revis.: 2009-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bi3",oParentObject)
return(i_retval)

define class tgsag_bi3 as StdBatch
  * --- Local variables
  w_CODICE = space(10)
  w_NUMRIG = 0
  w_ROWORD = 0
  w_DESCON = space(50)
  w_MODATTR = space(10)
  w_LEGAME = 0
  w_PADRE = .NULL.
  w_NUMREC = 0
  w_GRUPPO = space(15)
  w_RIGA = 0
  w_GEST = space(10)
  w_NEWLEG = 0
  w_NEWROW = 0
  w_SETLINKED = .f.
  * --- WorkFile variables
  MODDATTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dalla maschera GSAG_KIM (IMPORTA COMPONENTI)
    this.w_PADRE = this.oParentObject.oparentobject
    this.w_PADRE.MarkPos()     
     
 SELECT GRUPPI 
 GO TOP 
 SCAN
    this.w_CODICE = GRUPPI.IMCODICE
    this.w_NUMRIG = GRUPPI.NUMRIG
    this.w_ROWORD = GRUPPI.ROWORD
    this.w_DESCON = GRUPPI.IMDESCON
    this.w_MODATTR = GRUPPI.IMMODATR
    this.w_LEGAME = GRUPPI.IMLEGAME
    this.w_GRUPPO = GRUPPI.IMCODICE
    this.w_RIGA = GRUPPI.NUMRIG
    this.w_NEWLEG = 0
    this.w_NEWROW = 0
    if this.w_LEGAME<>0
      this.w_NUMREC = this.w_PADRE.Search("NVL(t_GRUPPO, SPACE(15))="+CP_TOSTR(this.w_CODICE) +" AND t_RIGA="+ CP_TOSTR(this.w_LEGAME) +" And Not Deleted() ")
      if this.w_NUMREC<>-1
        this.w_PADRE.SetRow(this.w_NUMREC)     
        this.w_NEWLEG = this.w_PADRE.get("CPROWNUM")
        this.w_NEWROW = this.w_PADRE.get("t_CPROWORD")
      endif
    endif
    * --- valorizzare con la set
    this.w_PADRE.AddRow()     
    this.w_PADRE.w_IMDESCON = this.w_DESCON
    this.w_PADRE.w_IMMODATR = this.w_MODATTR
    this.w_PADRE.w_GEST = NVL(this.w_MODATTR,"")
    * --- Read from MODDATTR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.MODDATTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODDATTR_idx,2],.t.,this.MODDATTR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MAGRUDEF,MACODATT"+;
        " from "+i_cTable+" MODDATTR where ";
            +"MACODICE = "+cp_ToStrODBC(this.w_MODATTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MAGRUDEF,MACODATT;
        from (i_cTable) where;
            MACODICE = this.w_MODATTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PADRE.w_GRUDEF = NVL(cp_ToDate(_read_.MAGRUDEF),cp_NullValue(_read_.MAGRUDEF))
      this.w_PADRE.w_CODATT = NVL(cp_ToDate(_read_.MACODATT),cp_NullValue(_read_.MACODATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PADRE.w_IMLEGAME = NVL(this.w_NEWLEG,0)
    this.w_PADRE.w_LIVELLO = NVL(this.w_NEWROW,0)
    this.w_PADRE.w_RIGA = this.w_RIGA
    this.w_PADRE.w_GRUPPO = this.w_CODICE
    this.w_PADRE.SaveRow()     
    SELECT GRUPPI
    ENDSCAN
    this.w_PADRE.RePos()     
    this.w_PADRE.Firstrow()     
    this.w_PADRE.Setrow(1)     
    i_stopFK=.F.
    USE IN SELECT ("GRUPPI")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MODDATTR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
