* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bcf                                                        *
*              Controlli cancellazione piano                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-15                                                      *
* Last revis.: 2011-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pDELETE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bcf",oParentObject,m.pDELETE)
return(i_retval)

define class tgsag_bcf as StdBatch
  * --- Local variables
  pDELETE = space(1)
  w_OLDATA = ctod("  /  /  ")
  w_OKATT = .f.
  * --- WorkFile variables
  DET_GEN_idx=0
  ELE_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli in cancellazione del piano generazione attivit�
    if this.pDELETE="A"
      this.w_OKATT = .F. 
      * --- Select from GSAG_BCK.VQR
      do vq_exec with 'GSAG_BCK.VQR',this,'_Curs_GSAG_BCK_d_VQR','',.f.,.t.
      if used('_Curs_GSAG_BCK_d_VQR')
        select _Curs_GSAG_BCK_d_VQR
        locate for 1=1
        do while not(eof())
        this.w_OKATT = .T. 
        exit
          select _Curs_GSAG_BCK_d_VQR
          continue
        enddo
        use
      endif
      i_retcode = 'stop'
      i_retval = this.w_OKATT
      return
    else
      * --- Write into ELE_CONT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ELE_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.DET_GEN_idx,2])
        i_cTempTable=cp_GetTempTableName(i_nConn)
        i_aIndex[1]='ELCONTRA,ELCODMOD,ELCODIMP,ELCODCOM,ELRINNOV'
        cp_CreateTempTable(i_nConn,i_cTempTable,"MDCONTRA AS ELCONTRA,MDCODMOD AS ELCODMOD,MDCODIMP AS ELCODIMP,MDCOMPON AS ELCODCOM, MDRINNOV AS ELRINNOV, MDOLDDAT, MDDATGAT,MDINICOM,MDFINCOM "," from "+i_cQueryTable+" where MDSERIAL="+cp_ToStrODBC(this.oParentObject.w_ATSERIAL)+"",.f.,@i_aIndex)
        i_cQueryTable=i_cTempTable
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
                +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
                +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
                +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
                +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ELDATATT = _t2.MDOLDDAT";
            +",ELINICOA = _t2.MDINICOM";
            +",ELFINCOA = _t2.MDFINCOM";
            +i_ccchkf;
            +" from "+i_cTable+" ELE_CONT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
                +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
                +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
                +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
                +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT, "+i_cQueryTable+" _t2 set ";
            +"ELE_CONT.ELDATATT = _t2.MDOLDDAT";
            +",ELE_CONT.ELINICOA = _t2.MDINICOM";
            +",ELE_CONT.ELFINCOA = _t2.MDFINCOM";
            +Iif(Empty(i_ccchkf),"",",ELE_CONT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="PostgreSQL"
          i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
                +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
                +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
                +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
                +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT set ";
            +"ELDATATT = _t2.MDOLDDAT";
            +",ELINICOA = _t2.MDINICOM";
            +",ELFINCOA = _t2.MDFINCOM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".ELCONTRA = "+i_cQueryTable+".ELCONTRA";
                +" and "+i_cTable+".ELCODMOD = "+i_cQueryTable+".ELCODMOD";
                +" and "+i_cTable+".ELCODIMP = "+i_cQueryTable+".ELCODIMP";
                +" and "+i_cTable+".ELCODCOM = "+i_cQueryTable+".ELCODCOM";
                +" and "+i_cTable+".ELRINNOV = "+i_cQueryTable+".ELRINNOV";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ELDATATT = (select MDOLDDAT from "+i_cQueryTable+" where "+i_cWhere+")";
            +",ELINICOA = (select MDINICOM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",ELFINCOA = (select MDFINCOM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Select from DET_GEN
      i_nConn=i_TableProp[this.DET_GEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DET_GEN_idx,2],.t.,this.DET_GEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select DISTINCT MDSERATT  from "+i_cTable+" DET_GEN ";
            +" where MDSERIAL="+cp_ToStrODBC(this.oParentObject.w_ATSERIAL)+"";
             ,"_Curs_DET_GEN")
      else
        select DISTINCT MDSERATT from (i_cTable);
         where MDSERIAL=this.oParentObject.w_ATSERIAL;
          into cursor _Curs_DET_GEN
      endif
      if used('_Curs_DET_GEN')
        select _Curs_DET_GEN
        locate for 1=1
        do while not(eof())
        GSAG_BDA(this,_Curs_DET_GEN.MDSERATT)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_DET_GEN
          continue
        enddo
        use
      endif
    endif
  endproc


  proc Init(oParentObject,pDELETE)
    this.pDELETE=pDELETE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DET_GEN'
    this.cWorkTables[2]='ELE_CONT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSAG_BCK_d_VQR')
      use in _Curs_GSAG_BCK_d_VQR
    endif
    if used('_Curs_DET_GEN')
      use in _Curs_DET_GEN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pDELETE"
endproc
