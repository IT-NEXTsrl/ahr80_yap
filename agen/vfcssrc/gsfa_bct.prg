* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bct                                                        *
*              Gestione reggruppamenti attivit� per eventi                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-19                                                      *
* Last revis.: 2010-05-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bct",oParentObject,m.pParam)
return(i_retval)

define class tgsfa_bct as StdBatch
  * --- Local variables
  pParam = space(3)
  w_CPROWNUM = 0
  w_CONTARIGHE = 0
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pParam = "PRE"
        * --- Disattivazione check Preferenziale
        * --- Se attivo il check Preferenziale sulla riga corrente
        if this.oParentObject.w_REFLPREF = "S"
          this.w_PADRE = this.oParentObject
          this.w_PADRE.MarkPos()     
          SELECT (this.w_PADRE.cTrsName)
          * --- Memorizza il cprownum della riga corrente
          this.w_CPROWNUM = CPROWNUM
          Go Top
          SCAN
          Replace t_REFLPREF with 0
          if CPROWNUM = this.w_CPROWNUM
            Replace t_REFLPREF with 1
          endif
          ENDSCAN
          * --- Riposizionamento sul Transitorio effettuando la SaveDependsOn()
          this.w_PADRE.RePos(.F.)     
        endif
      case this.pParam = "CHK"
        this.w_PADRE = this.oParentObject.GSFA_MRA
        this.w_PADRE.MarkPos()     
        * --- Se vi � almeno una riga piena non cancellata
        if this.w_PADRE.NumRow() > 0
          SELECT (this.w_PADRE.cTrsName)
          COUNT FOR NOT DELETED() AND t_REFLPREF=1 AND NOT EMPTY(t_RECODRAG) TO this.w_CONTARIGHE
          if this.w_CONTARIGHE = 0
            AH_ErrorMsg("E' necessario selezionare un raggruppamento come preferenziale")
            this.oParentObject.w_RESCHK = -1
          endif
        endif
        this.w_PADRE.RePos()     
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
