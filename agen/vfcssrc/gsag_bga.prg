* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bga                                                        *
*              Creazione movimanto di analitica                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-18                                                      *
* Last revis.: 2012-11-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERATT,pSERMOV,pTIPOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bga",oParentObject,m.pSERATT,m.pSERMOV,m.pTIPOPE)
return(i_retval)

define class tgsag_bga as StdBatch
  * --- Local variables
  pSERATT = space(20)
  pSERMOV = space(10)
  pTIPOPE = space(1)
  w_RETVAL = space(254)
  w_ATRIFMOV = space(10)
  w_ATSERIAL = space(20)
  w_ATDATFIN = ctod("  /  /  ")
  w_OKANA = .f.
  w_bUnderTran = .f.
  w_CMCOMPET = 0
  w_CMCODICE = space(10)
  w_CMNUMREG = 0
  w_DATA = ctod("  /  /  ")
  w_DESAGG = space(40)
  * --- WorkFile variables
  MOVICOST_idx=0
  CDC_MANU_idx=0
  OFF_ATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- D delete
    *     C creazione
    *     E entrambe
    this.w_ATRIFMOV = this.pSERMOV
    this.w_ATSERIAL = this.pSERATT
    * --- Read from OFF_ATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATDATFIN"+;
        " from "+i_cTable+" OFF_ATTI where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.pSERATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATDATFIN;
        from (i_cTable) where;
            ATSERIAL = this.pSERATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ATDATFIN = NVL(cp_ToDate(_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Verifico presenza di righe significative per generare movimento di analitica
    this.w_OKANA = .f.
    if this.pTIPOPE $ "C-E"
      * --- Select from GSZM_BAT
      do vq_exec with 'GSZM_BAT',this,'_Curs_GSZM_BAT','',.f.,.t.
      if used('_Curs_GSZM_BAT')
        select _Curs_GSZM_BAT
        locate for 1=1
        do while not(eof())
        this.w_OKANA = .t.
        exit
          select _Curs_GSZM_BAT
          continue
        enddo
        use
      endif
    endif
    this.w_bUnderTran = vartype(nTrsConnCnt)="U" or (nTrsConnCnt=0 or (Type("NTRSCONN[2]") ="N" and nTrsConn[2]=0))
    if this.w_bUnderTran
      * --- begin transaction
      cp_BeginTrs()
    endif
    if Not Empty(this.w_ATRIFMOV) and this.pTIPOPE $ "D-E"
      * --- Ho confermato movimento che prevedeva gi� movimento di analitica 
      *     eseguo cancellazione
      * --- Try
      local bErr_04D4E768
      bErr_04D4E768=bTrsErr
      this.Try_04D4E768()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_RETVAL = ah_MsgFormat("Errore cancellazione righe movimento di analitica.%0%1", MESSAGE())
      endif
      bTrsErr=bTrsErr or bErr_04D4E768
      * --- End
      * --- Try
      local bErr_04D4ED98
      bErr_04D4ED98=bTrsErr
      this.Try_04D4ED98()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_RETVAL = ah_MsgFormat("Errore cancellazione testata movimento di analitica.%0%1", MESSAGE())
      endif
      bTrsErr=bTrsErr or bErr_04D4ED98
      * --- End
    else
      this.w_CMCODICE = space(10)
    endif
    if this.w_OKANA
      if Empty(this.w_RETVAL)
        this.w_DESAGG = Ah_msgformat("Movimento generato da attivit� agenda")
        this.w_CMCOMPET = g_CODESE
        if EMPTY(this.w_ATRIFMOV)
          this.w_CMCODICE = SPACE(10)
          this.w_DATA = Cp_CharTodate("  -  -    ")
          i_Conn=i_TableProp[this.CDC_MANU_IDX, 3]
          cp_NextTableProg(this, i_Conn, "SECDC", "i_codazi,w_CMCODICE")
          cp_NextTableProg(this, i_Conn, "PRCDC", "i_codazi,w_CMCOMPET,w_CMNUMREG" )
          this.w_ATRIFMOV = this.w_CMCODICE
        endif
        * --- Try
        local bErr_04D4ED08
        bErr_04D4ED08=bTrsErr
        this.Try_04D4ED08()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_RETVAL = ah_MsgFormat("Errore inserimento testata movimento di analitica.%0%1", MESSAGE())
        endif
        bTrsErr=bTrsErr or bErr_04D4ED08
        * --- End
      endif
      if Empty(this.w_RETVAL)
        if NOT EMPTY(this.w_CMCODICE) 
          * --- Creo righe movimento di analitica
          * --- Try
          local bErr_04D4F1E8
          bErr_04D4F1E8=bTrsErr
          this.Try_04D4F1E8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_RETVAL = ah_MsgFormat("Errore inserimento righe movimento di analitica.%0%1", MESSAGE())
          endif
          bTrsErr=bTrsErr or bErr_04D4F1E8
          * --- End
        endif
      endif
    else
      this.w_ATRIFMOV = SPACE(10)
    endif
    if Empty(this.w_RETVAL)
      * --- Try
      local bErr_04D4F248
      bErr_04D4F248=bTrsErr
      this.Try_04D4F248()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_RETVAL = ah_MsgFormat("Errore nella scrittura  riferimento movimento di analitica.%0%1", MESSAGE())
      endif
      bTrsErr=bTrsErr or bErr_04D4F248
      * --- End
    endif
    if this.w_bUnderTran
      if EMPTY(this.w_RETVAL)
        * --- commit
        cp_EndTrs(.t.)
      else
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc
  proc Try_04D4E768()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_ATRIFMOV);
            +" and MRROWORD = "+cp_ToStrODBC(-1);
             )
    else
      delete from (i_cTable) where;
            MRSERIAL = this.w_ATRIFMOV;
            and MRROWORD = -1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_CMCODICE = this.w_ATRIFMOV
    return
  proc Try_04D4ED98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from CDC_MANU
    i_nConn=i_TableProp[this.CDC_MANU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CDC_MANU_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_ATRIFMOV);
             )
    else
      delete from (i_cTable) where;
            CMCODICE = this.w_ATRIFMOV;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04D4ED08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if ! Isahe()
      * --- Insert into CDC_MANU
      i_nConn=i_TableProp[this.CDC_MANU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CDC_MANU_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CDC_MANU_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CMCODICE"+",CMCODSEC"+",CMFLGMOV"+",CMDESAGG"+",CMNUMREG"+",CMDATREG"+",CMVALNAZ"+",CMNUMDOC"+",CMALFDOC"+",CMDATDOC"+",CMCOMPET"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CMCODICE),'CDC_MANU','CMCODICE');
        +","+cp_NullLink(cp_ToStrODBC(-1),'CDC_MANU','CMCODSEC');
        +","+cp_NullLink(cp_ToStrODBC("E"),'CDC_MANU','CMFLGMOV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESAGG),'CDC_MANU','CMDESAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CMNUMREG),'CDC_MANU','CMNUMREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ATDATFIN),'CDC_MANU','CMDATREG');
        +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'CDC_MANU','CMVALNAZ');
        +","+cp_NullLink(cp_ToStrODBC(0),'CDC_MANU','CMNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(""),'CDC_MANU','CMALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ATDATFIN),'CDC_MANU','CMDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CMCOMPET),'CDC_MANU','CMCOMPET');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CMCODICE',this.w_CMCODICE,'CMCODSEC',-1,'CMFLGMOV',"E",'CMDESAGG',this.w_DESAGG,'CMNUMREG',this.w_CMNUMREG,'CMDATREG',this.w_ATDATFIN,'CMVALNAZ',g_PERVAL,'CMNUMDOC',0,'CMALFDOC',"",'CMDATDOC',this.w_ATDATFIN,'CMCOMPET',this.w_CMCOMPET)
        insert into (i_cTable) (CMCODICE,CMCODSEC,CMFLGMOV,CMDESAGG,CMNUMREG,CMDATREG,CMVALNAZ,CMNUMDOC,CMALFDOC,CMDATDOC,CMCOMPET &i_ccchkf. );
           values (;
             this.w_CMCODICE;
             ,-1;
             ,"E";
             ,this.w_DESAGG;
             ,this.w_CMNUMREG;
             ,this.w_ATDATFIN;
             ,g_PERVAL;
             ,0;
             ,"";
             ,this.w_ATDATFIN;
             ,this.w_CMCOMPET;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into CDC_MANU
      i_nConn=i_TableProp[this.CDC_MANU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CDC_MANU_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CDC_MANU_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CMCODICE"+",CMCODSEC"+",CMFLGMOV"+",CMDESAGG"+",CMNUMREG"+",CMDATREG"+",CMVALNAZ"+",CMNUMDOC"+",CMALFDOC"+",CMDATDOC"+",CMCOMPET"+",CMFLCOIM"+",CMCODBUN"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CMCODICE),'CDC_MANU','CMCODICE');
        +","+cp_NullLink(cp_ToStrODBC(-1),'CDC_MANU','CMCODSEC');
        +","+cp_NullLink(cp_ToStrODBC("E"),'CDC_MANU','CMFLGMOV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESAGG),'CDC_MANU','CMDESAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CMNUMREG),'CDC_MANU','CMNUMREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ATDATFIN),'CDC_MANU','CMDATREG');
        +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'CDC_MANU','CMVALNAZ');
        +","+cp_NullLink(cp_ToStrODBC(0),'CDC_MANU','CMNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(""),'CDC_MANU','CMALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ATDATFIN),'CDC_MANU','CMDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CMCOMPET),'CDC_MANU','CMCOMPET');
        +","+cp_NullLink(cp_ToStrODBC("S"),'CDC_MANU','CMFLCOIM');
        +","+cp_NullLink(cp_ToStrODBC(g_CODBUN),'CDC_MANU','CMCODBUN');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CMCODICE',this.w_CMCODICE,'CMCODSEC',-1,'CMFLGMOV',"E",'CMDESAGG',this.w_DESAGG,'CMNUMREG',this.w_CMNUMREG,'CMDATREG',this.w_ATDATFIN,'CMVALNAZ',g_PERVAL,'CMNUMDOC',0,'CMALFDOC',"",'CMDATDOC',this.w_ATDATFIN,'CMCOMPET',this.w_CMCOMPET,'CMFLCOIM',"S")
        insert into (i_cTable) (CMCODICE,CMCODSEC,CMFLGMOV,CMDESAGG,CMNUMREG,CMDATREG,CMVALNAZ,CMNUMDOC,CMALFDOC,CMDATDOC,CMCOMPET,CMFLCOIM,CMCODBUN &i_ccchkf. );
           values (;
             this.w_CMCODICE;
             ,-1;
             ,"E";
             ,this.w_DESAGG;
             ,this.w_CMNUMREG;
             ,this.w_ATDATFIN;
             ,g_PERVAL;
             ,0;
             ,"";
             ,this.w_ATDATFIN;
             ,this.w_CMCOMPET;
             ,"S";
             ,g_CODBUN;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_04D4F1E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\AGEN\EXE\QUERY\GSAG_BGA",this.MOVICOST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04D4F248()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATRIFMOV ="+cp_NullLink(cp_ToStrODBC(this.w_ATRIFMOV),'OFF_ATTI','ATRIFMOV');
          +i_ccchkf ;
      +" where ";
          +"ATSERIAL = "+cp_ToStrODBC(this.pSERATT);
             )
    else
      update (i_cTable) set;
          ATRIFMOV = this.w_ATRIFMOV;
          &i_ccchkf. ;
       where;
          ATSERIAL = this.pSERATT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pSERATT,pSERMOV,pTIPOPE)
    this.pSERATT=pSERATT
    this.pSERMOV=pSERMOV
    this.pTIPOPE=pTIPOPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MOVICOST'
    this.cWorkTables[2]='CDC_MANU'
    this.cWorkTables[3]='OFF_ATTI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSZM_BAT')
      use in _Curs_GSZM_BAT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERATT,pSERMOV,pTIPOPE"
endproc
