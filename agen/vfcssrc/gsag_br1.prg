* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_br1                                                        *
*              Check selezione zoom elenco attivit�                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-04-20                                                      *
* Last revis.: 2009-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_br1",oParentObject)
return(i_retval)

define class tgsag_br1 as StdBatch
  * --- Local variables
  w_EVENTO = space(10)
  w_SERIALE = space(10)
  w_ATFLATRI = space(1)
  w_IDX = 0
  * --- WorkFile variables
  OFF_ATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se l'attivit� selezionata dagli zoom di attivit� � riservata o meno...
    *     In base all'evento scatentato decido da quale zoom leggere l'informazione
    *     del seriale per risalire all'attivit�...
    this.w_EVENTO = Lower( this.oParentObject.currentEvent )
    * --- Non serve mantenere la vecchia area (ripristina tutto Code Painter)
    *     Si potrebbe utilizzare una var. oggetto che punti allo zomo selezionato
    *     per snellire il codice (non fatto per evitare problemi di puntatori appesi)
    if this.w_EVENTO= "w_agkra_zoom row checked"
      Select ( this.oParentObject.w_AGKRA_ZOOM.cCursor )
    else
      Select ( this.oParentObject.w_AGKRA_ZOOM3.cCursor )
    endif
    this.w_SERIALE = ATSERIAL
    * --- Read from OFF_ATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATFLATRI"+;
        " from "+i_cTable+" OFF_ATTI where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATFLATRI;
        from (i_cTable) where;
            ATSERIAL = this.w_SERIALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ATFLATRI = NVL(cp_ToDate(_read_.ATFLATRI),cp_NullValue(_read_.ATFLATRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not IsRisAtt( this.w_SERIALE , this.w_ATFLATRI ) 
      if this.w_EVENTO= "w_agkra_zoom row checked"
        this.w_IDX = this.oParentObject.w_AGKRA_ZOOM.grd.ColumnCount
        this.oParentObject.w_AGKRA_ZOOM.grd.Columns[ this.w_IDX ].chk.Value = 0
      else
        this.w_IDX = this.oParentObject.w_AGKRA_ZOOM3.grd.ColumnCount
        this.oParentObject.w_AGKRA_ZOOM3.grd.Columns[ this.w_IDX ].chk.Value = 0
      endif
      ah_ErrorMsg("Operazione non consentita, attivit� riservata")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_ATTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
