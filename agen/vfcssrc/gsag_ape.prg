* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ape                                                        *
*              Profili esportazione/importazione                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-07                                                      *
* Last revis.: 2014-02-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_ape"))

* --- Class definition
define class tgsag_ape as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 710
  Height = 496+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-03"
  HelpContextID=160049001
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=42

  * --- Constant Properties
  PRO_EXPO_IDX = 0
  cpusers_IDX = 0
  OFF_NOMI_IDX = 0
  CAUMATTI_IDX = 0
  OFFTIPAT_IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  cFile = "PRO_EXPO"
  cKeySelect = "PRSERIAL"
  cKeyWhere  = "PRSERIAL=this.w_PRSERIAL"
  cKeyWhereODBC = '"PRSERIAL="+cp_ToStrODBC(this.w_PRSERIAL)';

  cKeyWhereODBCqualified = '"PRO_EXPO.PRSERIAL="+cp_ToStrODBC(this.w_PRSERIAL)';

  cPrg = "gsag_ape"
  cComment = "Profili esportazione/importazione"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PRSERIAL = space(10)
  w_PRDESCRI = space(50)
  w_PRCODUTE = space(5)
  w_PR__TIPO = space(1)
  w_PRCAUATT = space(20)
  w_PRTIPDAT = space(1)
  o_PRTIPDAT = space(1)
  w_PR_STATO = space(1)
  w_PRNOFLAT = space(1)
  o_PRNOFLAT = space(1)
  w_PRDATINI = ctod('  /  /  ')
  w_PROGGETT = space(254)
  w_PRNOMINA = space(15)
  w_PRRIFPER = space(60)
  w_PRDISPON = 0
  w_PRLOCALI = space(30)
  w_PRTIPOLO = space(5)
  w_PR__NOTE = space(0)
  w_NODES = space(60)
  w_DESCAU = space(254)
  w_DESTIP = space(50)
  w_PRAPPUNT = space(1)
  w_PRDAFARE = space(1)
  w_PRSESTEL = space(1)
  w_PRCATNOT = space(1)
  w_PRATTGEN = space(1)
  w_PRASSENZ = space(1)
  w_PRUDIENZ = space(1)
  w_PRINSPRE = space(1)
  w_DESUTE = space(100)
  w_NOME = space(50)
  w_COGNOM = space(50)
  w_PRTIPANA = space(1)
  w_PRCODCOM = space(15)
  w_DESCAN = space(100)
  w_PRDATFIN = ctod('  /  /  ')
  w_PRDATATP = 0
  w_PRDATATS = 0
  w_PRPATHEX = space(254)
  w_PRPATHIM = space(254)
  w_RESCHK = 0
  w_TIPESP = space(1)
  w_TIPIMP = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_PRSERIAL = this.W_PRSERIAL

  * --- Children pointers
  GSAG_MPE = .NULL.
  GSAG_MPI = .NULL.
  gsag_mpc = .NULL.
  gsag_mpk = .NULL.
  w_BTNQRY = .NULL.
  w_BTNQRY = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PRO_EXPO','gsag_ape')
    stdPageFrame::Init()
    *set procedure to gsag_mpc additive
    with this
      .Pages(1).addobject("oPag","tgsag_apePag1","gsag_ape",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Esportazione")
      .Pages(1).HelpContextID = 41461328
      .Pages(2).addobject("oPag","tgsag_apePag2","gsag_ape",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Importazione")
      .Pages(2).HelpContextID = 41459856
      .Pages(3).addobject("oPag","tgsag_apePag3","gsag_ape",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dettaglio attivit�")
      .Pages(3).HelpContextID = 188964552
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPRSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure gsag_mpc
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_BTNQRY = this.oPgFrm.Pages(1).oPag.BTNQRY
      this.w_BTNQRY = this.oPgFrm.Pages(2).oPag.BTNQRY
      DoDefault()
    proc Destroy()
      this.w_BTNQRY = .NULL.
      this.w_BTNQRY = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='CAUMATTI'
    this.cWorkTables[4]='OFFTIPAT'
    this.cWorkTables[5]='DIPENDEN'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='PRO_EXPO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRO_EXPO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRO_EXPO_IDX,3]
  return

  function CreateChildren()
    this.GSAG_MPE = CREATEOBJECT('stdDynamicChild',this,'GSAG_MPE',this.oPgFrm.Page3.oPag.oLinkPC_3_1)
    this.GSAG_MPI = CREATEOBJECT('stdDynamicChild',this,'GSAG_MPI',this.oPgFrm.Page2.oPag.oLinkPC_2_5)
    this.gsag_mpc = CREATEOBJECT('stdLazyChild',this,'gsag_mpc')
    this.gsag_mpk = CREATEOBJECT('stdLazyChild',this,'gsag_mpk')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAG_MPE)
      this.GSAG_MPE.DestroyChildrenChain()
      this.GSAG_MPE=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_1')
    if !ISNULL(this.GSAG_MPI)
      this.GSAG_MPI.DestroyChildrenChain()
      this.GSAG_MPI=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_5')
    if !ISNULL(this.gsag_mpc)
      this.gsag_mpc.DestroyChildrenChain()
      this.gsag_mpc=.NULL.
    endif
    if !ISNULL(this.gsag_mpk)
      this.gsag_mpk.DestroyChildrenChain()
      this.gsag_mpk=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAG_MPE.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAG_MPI.IsAChildUpdated()
      i_bRes = i_bRes .or. this.gsag_mpc.IsAChildUpdated()
      i_bRes = i_bRes .or. this.gsag_mpk.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAG_MPE.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAG_MPI.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.gsag_mpc.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.gsag_mpk.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAG_MPE.NewDocument()
    this.GSAG_MPI.NewDocument()
    this.gsag_mpc.NewDocument()
    this.gsag_mpk.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAG_MPE.SetKey(;
            .w_PRSERIAL,"COSERPRO";
            )
      this.GSAG_MPI.SetKey(;
            .w_PRSERIAL,"PICODPRO";
            )
      this.gsag_mpc.SetKey(;
            .w_PRSERIAL,"PCSERPRO";
            ,.w_TIPESP,"PCTIPTRA";
            )
      this.gsag_mpk.SetKey(;
            .w_PRSERIAL,"PCSERPRO";
            ,.w_TIPIMP,"PCTIPTRA";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAG_MPE.ChangeRow(this.cRowID+'      1',1;
             ,.w_PRSERIAL,"COSERPRO";
             )
      .GSAG_MPI.ChangeRow(this.cRowID+'      1',1;
             ,.w_PRSERIAL,"PICODPRO";
             )
      .gsag_mpc.ChangeRow(this.cRowID+'      1',1;
             ,.w_PRSERIAL,"PCSERPRO";
             ,.w_TIPESP,"PCTIPTRA";
             )
      .gsag_mpk.ChangeRow(this.cRowID+'      1',1;
             ,.w_PRSERIAL,"PCSERPRO";
             ,.w_TIPIMP,"PCTIPTRA";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAG_MPE)
        i_f=.GSAG_MPE.BuildFilter()
        if !(i_f==.GSAG_MPE.cQueryFilter)
          i_fnidx=.GSAG_MPE.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAG_MPE.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAG_MPE.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAG_MPE.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAG_MPE.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAG_MPI)
        i_f=.GSAG_MPI.BuildFilter()
        if !(i_f==.GSAG_MPI.cQueryFilter)
          i_fnidx=.GSAG_MPI.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAG_MPI.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAG_MPI.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAG_MPI.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAG_MPI.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PRSERIAL = NVL(PRSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_45_joined
    link_1_45_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PRO_EXPO where PRSERIAL=KeySet.PRSERIAL
    *
    i_nConn = i_TableProp[this.PRO_EXPO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRO_EXPO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRO_EXPO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PRO_EXPO '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_45_joined=this.AddJoinedLink_1_45(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRSERIAL',this.w_PRSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_NODES = space(60)
        .w_DESCAU = space(254)
        .w_DESTIP = space(50)
        .w_NOME = space(50)
        .w_COGNOM = space(50)
        .w_DESCAN = space(100)
        .w_RESCHK = 0
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_PRSERIAL = NVL(PRSERIAL,space(10))
        .op_PRSERIAL = .w_PRSERIAL
        .w_PRDESCRI = NVL(PRDESCRI,space(50))
        .w_PRCODUTE = NVL(PRCODUTE,space(5))
          if link_1_3_joined
            this.w_PRCODUTE = NVL(DPCODICE103,NVL(this.w_PRCODUTE,space(5)))
            this.w_NOME = NVL(DPNOME103,space(50))
            this.w_COGNOM = NVL(DPCOGNOM103,space(50))
          else
          .link_1_3('Load')
          endif
        .w_PR__TIPO = NVL(PR__TIPO,space(1))
        .w_PRCAUATT = NVL(PRCAUATT,space(20))
          if link_1_6_joined
            this.w_PRCAUATT = NVL(CACODICE106,NVL(this.w_PRCAUATT,space(20)))
            this.w_DESCAU = NVL(CADESCRI106,space(254))
          else
          .link_1_6('Load')
          endif
        .w_PRTIPDAT = NVL(PRTIPDAT,space(1))
        .w_PR_STATO = NVL(PR_STATO,space(1))
        .w_PRNOFLAT = NVL(PRNOFLAT,space(1))
        .w_PRDATINI = NVL(cp_ToDate(PRDATINI),ctod("  /  /  "))
        .w_PROGGETT = NVL(PROGGETT,space(254))
        .w_PRNOMINA = NVL(PRNOMINA,space(15))
          if link_1_16_joined
            this.w_PRNOMINA = NVL(NOCODICE116,NVL(this.w_PRNOMINA,space(15)))
            this.w_NODES = NVL(NODESCRI116,space(60))
          else
          .link_1_16('Load')
          endif
        .w_PRRIFPER = NVL(PRRIFPER,space(60))
        .w_PRDISPON = NVL(PRDISPON,0)
        .w_PRLOCALI = NVL(PRLOCALI,space(30))
        .w_PRTIPOLO = NVL(PRTIPOLO,space(5))
          if link_1_23_joined
            this.w_PRTIPOLO = NVL(TACODTIP123,NVL(this.w_PRTIPOLO,space(5)))
            this.w_DESTIP = NVL(TADESCRI123,space(50))
          else
          .link_1_23('Load')
          endif
        .w_PR__NOTE = NVL(PR__NOTE,space(0))
        .w_PRAPPUNT = NVL(PRAPPUNT,space(1))
        .w_PRDAFARE = NVL(PRDAFARE,space(1))
        .w_PRSESTEL = NVL(PRSESTEL,space(1))
        .w_PRCATNOT = NVL(PRCATNOT,space(1))
        .w_PRATTGEN = NVL(PRATTGEN,space(1))
        .w_PRASSENZ = NVL(PRASSENZ,space(1))
        .w_PRUDIENZ = NVL(PRUDIENZ,space(1))
        .w_PRINSPRE = NVL(PRINSPRE,space(1))
        .w_DESUTE = Alltrim(.w_NOME)+' ' + Alltrim(.w_COGNOM)
        .w_PRTIPANA = NVL(PRTIPANA,space(1))
        .w_PRCODCOM = NVL(PRCODCOM,space(15))
          if link_1_45_joined
            this.w_PRCODCOM = NVL(CNCODCAN145,NVL(this.w_PRCODCOM,space(15)))
            this.w_DESCAN = NVL(CNDESCAN145,space(100))
          else
          .link_1_45('Load')
          endif
        .w_PRDATFIN = NVL(cp_ToDate(PRDATFIN),ctod("  /  /  "))
        .w_PRDATATP = NVL(PRDATATP,0)
        .w_PRDATATS = NVL(PRDATATS,0)
        .w_PRPATHEX = NVL(PRPATHEX,space(254))
        .w_PRPATHIM = NVL(PRPATHIM,space(254))
        .w_TIPESP = 'E'
        .w_TIPIMP = 'I'
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page2.oPag.BTNQRY.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'PRO_EXPO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_65.enabled = this.oPgFrm.Page1.oPag.oBtn_1_65.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_7.enabled = this.oPgFrm.Page2.oPag.oBtn_2_7.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PRSERIAL = space(10)
      .w_PRDESCRI = space(50)
      .w_PRCODUTE = space(5)
      .w_PR__TIPO = space(1)
      .w_PRCAUATT = space(20)
      .w_PRTIPDAT = space(1)
      .w_PR_STATO = space(1)
      .w_PRNOFLAT = space(1)
      .w_PRDATINI = ctod("  /  /  ")
      .w_PROGGETT = space(254)
      .w_PRNOMINA = space(15)
      .w_PRRIFPER = space(60)
      .w_PRDISPON = 0
      .w_PRLOCALI = space(30)
      .w_PRTIPOLO = space(5)
      .w_PR__NOTE = space(0)
      .w_NODES = space(60)
      .w_DESCAU = space(254)
      .w_DESTIP = space(50)
      .w_PRAPPUNT = space(1)
      .w_PRDAFARE = space(1)
      .w_PRSESTEL = space(1)
      .w_PRCATNOT = space(1)
      .w_PRATTGEN = space(1)
      .w_PRASSENZ = space(1)
      .w_PRUDIENZ = space(1)
      .w_PRINSPRE = space(1)
      .w_DESUTE = space(100)
      .w_NOME = space(50)
      .w_COGNOM = space(50)
      .w_PRTIPANA = space(1)
      .w_PRCODCOM = space(15)
      .w_DESCAN = space(100)
      .w_PRDATFIN = ctod("  /  /  ")
      .w_PRDATATP = 0
      .w_PRDATATS = 0
      .w_PRPATHEX = space(254)
      .w_PRPATHIM = space(254)
      .w_RESCHK = 0
      .w_TIPESP = space(1)
      .w_TIPIMP = space(1)
      .w_DTBSO_CAUMATTI = ctod("  /  /  ")
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_PRCODUTE = readdipend(i_CodUte, 'D')
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_PRCODUTE))
          .link_1_3('Full')
          endif
        .w_PR__TIPO = 'I'
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_PRCAUATT))
          .link_1_6('Full')
          endif
        .w_PRTIPDAT = 'D'
        .w_PR_STATO = 'N'
        .w_PRNOFLAT = 'F'
        .w_PRDATINI = Cp_chartodate('  -  -    ')
        .DoRTCalc(10,11,.f.)
          if not(empty(.w_PRNOMINA))
          .link_1_16('Full')
          endif
        .DoRTCalc(12,15,.f.)
          if not(empty(.w_PRTIPOLO))
          .link_1_23('Full')
          endif
          .DoRTCalc(16,19,.f.)
        .w_PRAPPUNT = 'S'
          .DoRTCalc(21,25,.f.)
        .w_PRUDIENZ = 'U'
          .DoRTCalc(27,27,.f.)
        .w_DESUTE = Alltrim(.w_NOME)+' ' + Alltrim(.w_COGNOM)
          .DoRTCalc(29,30,.f.)
        .w_PRTIPANA = 'C'
        .DoRTCalc(32,32,.f.)
          if not(empty(.w_PRCODCOM))
          .link_1_45('Full')
          endif
          .DoRTCalc(33,33,.f.)
        .w_PRDATFIN = Cp_chartodate('  -  -    ')
        .w_PRDATATP = IIF(.w_PRNOFLAT$'ENI', 0, .w_PRDATATP)
        .w_PRDATATS = IIF(.w_PRNOFLAT$ 'ENF', 0, .w_PRDATATS)
          .DoRTCalc(37,38,.f.)
        .w_RESCHK = 0
        .w_TIPESP = 'E'
        .w_TIPIMP = 'I'
        .w_DTBSO_CAUMATTI = i_DatSys
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page2.oPag.BTNQRY.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRO_EXPO')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_65.enabled = this.oPgFrm.Page1.oPag.oBtn_1_65.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_7.enabled = this.oPgFrm.Page2.oPag.oBtn_2_7.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRO_EXPO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"PROFI","i_CODAZI,w_PRSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_PRSERIAL = .w_PRSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPRSERIAL_1_1.enabled = !i_bVal
      .Page1.oPag.oPRDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oPRCODUTE_1_3.enabled = i_bVal
      .Page1.oPag.oPRCAUATT_1_6.enabled = i_bVal
      .Page1.oPag.oPRTIPDAT_1_7.enabled = i_bVal
      .Page1.oPag.oPR_STATO_1_8.enabled = i_bVal
      .Page1.oPag.oPRNOFLAT_1_9.enabled = i_bVal
      .Page1.oPag.oPRDATINI_1_11.enabled = i_bVal
      .Page1.oPag.oPROGGETT_1_14.enabled = i_bVal
      .Page1.oPag.oPRNOMINA_1_16.enabled = i_bVal
      .Page1.oPag.oPRRIFPER_1_17.enabled = i_bVal
      .Page1.oPag.oPRDISPON_1_19.enabled = i_bVal
      .Page1.oPag.oPRLOCALI_1_20.enabled = i_bVal
      .Page1.oPag.oPRTIPOLO_1_23.enabled = i_bVal
      .Page1.oPag.oPR__NOTE_1_25.enabled = i_bVal
      .Page1.oPag.oPRAPPUNT_1_31.enabled = i_bVal
      .Page1.oPag.oPRDAFARE_1_32.enabled = i_bVal
      .Page1.oPag.oPRSESTEL_1_33.enabled = i_bVal
      .Page1.oPag.oPRCATNOT_1_34.enabled = i_bVal
      .Page1.oPag.oPRATTGEN_1_35.enabled = i_bVal
      .Page1.oPag.oPRASSENZ_1_36.enabled = i_bVal
      .Page1.oPag.oPRUDIENZ_1_37.enabled = i_bVal
      .Page1.oPag.oPRTIPANA_1_43.enabled = i_bVal
      .Page1.oPag.oPRCODCOM_1_45.enabled = i_bVal
      .Page1.oPag.oPRDATFIN_1_49.enabled = i_bVal
      .Page1.oPag.oPRDATATP_1_51.enabled = i_bVal
      .Page1.oPag.oPRDATATS_1_52.enabled = i_bVal
      .Page1.oPag.oPRPATHEX_1_59.enabled = i_bVal
      .Page2.oPag.oPRPATHIM_2_3.enabled = i_bVal
      .Page1.oPag.oBtn_1_65.enabled = .Page1.oPag.oBtn_1_65.mCond()
      .Page2.oPag.oBtn_2_7.enabled = .Page2.oPag.oBtn_2_7.mCond()
      .Page1.oPag.BTNQRY.enabled = i_bVal
      .Page2.oPag.BTNQRY.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oPRCODUTE_1_3.enabled = .t.
      endif
    endwith
    this.GSAG_MPE.SetStatus(i_cOp)
    this.GSAG_MPI.SetStatus(i_cOp)
    this.gsag_mpc.SetStatus(i_cOp)
    this.gsag_mpk.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PRO_EXPO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAG_MPE.SetChildrenStatus(i_cOp)
  *  this.GSAG_MPI.SetChildrenStatus(i_cOp)
  *  this.gsag_mpc.SetChildrenStatus(i_cOp)
  *  this.gsag_mpk.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRO_EXPO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRSERIAL,"PRSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDESCRI,"PRDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODUTE,"PRCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PR__TIPO,"PR__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCAUATT,"PRCAUATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPDAT,"PRTIPDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PR_STATO,"PR_STATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRNOFLAT,"PRNOFLAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATINI,"PRDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PROGGETT,"PROGGETT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRNOMINA,"PRNOMINA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRRIFPER,"PRRIFPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDISPON,"PRDISPON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRLOCALI,"PRLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPOLO,"PRTIPOLO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PR__NOTE,"PR__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRAPPUNT,"PRAPPUNT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDAFARE,"PRDAFARE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRSESTEL,"PRSESTEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCATNOT,"PRCATNOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRATTGEN,"PRATTGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRASSENZ,"PRASSENZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRUDIENZ,"PRUDIENZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRINSPRE,"PRINSPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPANA,"PRTIPANA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODCOM,"PRCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATFIN,"PRDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATATP,"PRDATATP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRDATATS,"PRDATATS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPATHEX,"PRPATHEX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRPATHIM,"PRPATHIM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRO_EXPO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])
    i_lTable = "PRO_EXPO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PRO_EXPO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRO_EXPO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PRO_EXPO_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"PROFI","i_CODAZI,w_PRSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PRO_EXPO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRO_EXPO')
        i_extval=cp_InsertValODBCExtFlds(this,'PRO_EXPO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PRSERIAL,PRDESCRI,PRCODUTE,PR__TIPO,PRCAUATT"+;
                  ",PRTIPDAT,PR_STATO,PRNOFLAT,PRDATINI,PROGGETT"+;
                  ",PRNOMINA,PRRIFPER,PRDISPON,PRLOCALI,PRTIPOLO"+;
                  ",PR__NOTE,PRAPPUNT,PRDAFARE,PRSESTEL,PRCATNOT"+;
                  ",PRATTGEN,PRASSENZ,PRUDIENZ,PRINSPRE,PRTIPANA"+;
                  ",PRCODCOM,PRDATFIN,PRDATATP,PRDATATS,PRPATHEX"+;
                  ",PRPATHIM "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PRSERIAL)+;
                  ","+cp_ToStrODBC(this.w_PRDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_PRCODUTE)+;
                  ","+cp_ToStrODBC(this.w_PR__TIPO)+;
                  ","+cp_ToStrODBCNull(this.w_PRCAUATT)+;
                  ","+cp_ToStrODBC(this.w_PRTIPDAT)+;
                  ","+cp_ToStrODBC(this.w_PR_STATO)+;
                  ","+cp_ToStrODBC(this.w_PRNOFLAT)+;
                  ","+cp_ToStrODBC(this.w_PRDATINI)+;
                  ","+cp_ToStrODBC(this.w_PROGGETT)+;
                  ","+cp_ToStrODBCNull(this.w_PRNOMINA)+;
                  ","+cp_ToStrODBC(this.w_PRRIFPER)+;
                  ","+cp_ToStrODBC(this.w_PRDISPON)+;
                  ","+cp_ToStrODBC(this.w_PRLOCALI)+;
                  ","+cp_ToStrODBCNull(this.w_PRTIPOLO)+;
                  ","+cp_ToStrODBC(this.w_PR__NOTE)+;
                  ","+cp_ToStrODBC(this.w_PRAPPUNT)+;
                  ","+cp_ToStrODBC(this.w_PRDAFARE)+;
                  ","+cp_ToStrODBC(this.w_PRSESTEL)+;
                  ","+cp_ToStrODBC(this.w_PRCATNOT)+;
                  ","+cp_ToStrODBC(this.w_PRATTGEN)+;
                  ","+cp_ToStrODBC(this.w_PRASSENZ)+;
                  ","+cp_ToStrODBC(this.w_PRUDIENZ)+;
                  ","+cp_ToStrODBC(this.w_PRINSPRE)+;
                  ","+cp_ToStrODBC(this.w_PRTIPANA)+;
                  ","+cp_ToStrODBCNull(this.w_PRCODCOM)+;
                  ","+cp_ToStrODBC(this.w_PRDATFIN)+;
                  ","+cp_ToStrODBC(this.w_PRDATATP)+;
                  ","+cp_ToStrODBC(this.w_PRDATATS)+;
                  ","+cp_ToStrODBC(this.w_PRPATHEX)+;
                  ","+cp_ToStrODBC(this.w_PRPATHIM)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRO_EXPO')
        i_extval=cp_InsertValVFPExtFlds(this,'PRO_EXPO')
        cp_CheckDeletedKey(i_cTable,0,'PRSERIAL',this.w_PRSERIAL)
        INSERT INTO (i_cTable);
              (PRSERIAL,PRDESCRI,PRCODUTE,PR__TIPO,PRCAUATT,PRTIPDAT,PR_STATO,PRNOFLAT,PRDATINI,PROGGETT,PRNOMINA,PRRIFPER,PRDISPON,PRLOCALI,PRTIPOLO,PR__NOTE,PRAPPUNT,PRDAFARE,PRSESTEL,PRCATNOT,PRATTGEN,PRASSENZ,PRUDIENZ,PRINSPRE,PRTIPANA,PRCODCOM,PRDATFIN,PRDATATP,PRDATATS,PRPATHEX,PRPATHIM  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PRSERIAL;
                  ,this.w_PRDESCRI;
                  ,this.w_PRCODUTE;
                  ,this.w_PR__TIPO;
                  ,this.w_PRCAUATT;
                  ,this.w_PRTIPDAT;
                  ,this.w_PR_STATO;
                  ,this.w_PRNOFLAT;
                  ,this.w_PRDATINI;
                  ,this.w_PROGGETT;
                  ,this.w_PRNOMINA;
                  ,this.w_PRRIFPER;
                  ,this.w_PRDISPON;
                  ,this.w_PRLOCALI;
                  ,this.w_PRTIPOLO;
                  ,this.w_PR__NOTE;
                  ,this.w_PRAPPUNT;
                  ,this.w_PRDAFARE;
                  ,this.w_PRSESTEL;
                  ,this.w_PRCATNOT;
                  ,this.w_PRATTGEN;
                  ,this.w_PRASSENZ;
                  ,this.w_PRUDIENZ;
                  ,this.w_PRINSPRE;
                  ,this.w_PRTIPANA;
                  ,this.w_PRCODCOM;
                  ,this.w_PRDATFIN;
                  ,this.w_PRDATATP;
                  ,this.w_PRDATATS;
                  ,this.w_PRPATHEX;
                  ,this.w_PRPATHIM;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PRO_EXPO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PRO_EXPO_IDX,i_nConn)
      *
      * update PRO_EXPO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_EXPO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PRDESCRI="+cp_ToStrODBC(this.w_PRDESCRI)+;
             ",PRCODUTE="+cp_ToStrODBCNull(this.w_PRCODUTE)+;
             ",PR__TIPO="+cp_ToStrODBC(this.w_PR__TIPO)+;
             ",PRCAUATT="+cp_ToStrODBCNull(this.w_PRCAUATT)+;
             ",PRTIPDAT="+cp_ToStrODBC(this.w_PRTIPDAT)+;
             ",PR_STATO="+cp_ToStrODBC(this.w_PR_STATO)+;
             ",PRNOFLAT="+cp_ToStrODBC(this.w_PRNOFLAT)+;
             ",PRDATINI="+cp_ToStrODBC(this.w_PRDATINI)+;
             ",PROGGETT="+cp_ToStrODBC(this.w_PROGGETT)+;
             ",PRNOMINA="+cp_ToStrODBCNull(this.w_PRNOMINA)+;
             ",PRRIFPER="+cp_ToStrODBC(this.w_PRRIFPER)+;
             ",PRDISPON="+cp_ToStrODBC(this.w_PRDISPON)+;
             ",PRLOCALI="+cp_ToStrODBC(this.w_PRLOCALI)+;
             ",PRTIPOLO="+cp_ToStrODBCNull(this.w_PRTIPOLO)+;
             ",PR__NOTE="+cp_ToStrODBC(this.w_PR__NOTE)+;
             ",PRAPPUNT="+cp_ToStrODBC(this.w_PRAPPUNT)+;
             ",PRDAFARE="+cp_ToStrODBC(this.w_PRDAFARE)+;
             ",PRSESTEL="+cp_ToStrODBC(this.w_PRSESTEL)+;
             ",PRCATNOT="+cp_ToStrODBC(this.w_PRCATNOT)+;
             ",PRATTGEN="+cp_ToStrODBC(this.w_PRATTGEN)+;
             ",PRASSENZ="+cp_ToStrODBC(this.w_PRASSENZ)+;
             ",PRUDIENZ="+cp_ToStrODBC(this.w_PRUDIENZ)+;
             ",PRINSPRE="+cp_ToStrODBC(this.w_PRINSPRE)+;
             ",PRTIPANA="+cp_ToStrODBC(this.w_PRTIPANA)+;
             ",PRCODCOM="+cp_ToStrODBCNull(this.w_PRCODCOM)+;
             ",PRDATFIN="+cp_ToStrODBC(this.w_PRDATFIN)+;
             ",PRDATATP="+cp_ToStrODBC(this.w_PRDATATP)+;
             ",PRDATATS="+cp_ToStrODBC(this.w_PRDATATS)+;
             ",PRPATHEX="+cp_ToStrODBC(this.w_PRPATHEX)+;
             ",PRPATHIM="+cp_ToStrODBC(this.w_PRPATHIM)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_EXPO')
        i_cWhere = cp_PKFox(i_cTable  ,'PRSERIAL',this.w_PRSERIAL  )
        UPDATE (i_cTable) SET;
              PRDESCRI=this.w_PRDESCRI;
             ,PRCODUTE=this.w_PRCODUTE;
             ,PR__TIPO=this.w_PR__TIPO;
             ,PRCAUATT=this.w_PRCAUATT;
             ,PRTIPDAT=this.w_PRTIPDAT;
             ,PR_STATO=this.w_PR_STATO;
             ,PRNOFLAT=this.w_PRNOFLAT;
             ,PRDATINI=this.w_PRDATINI;
             ,PROGGETT=this.w_PROGGETT;
             ,PRNOMINA=this.w_PRNOMINA;
             ,PRRIFPER=this.w_PRRIFPER;
             ,PRDISPON=this.w_PRDISPON;
             ,PRLOCALI=this.w_PRLOCALI;
             ,PRTIPOLO=this.w_PRTIPOLO;
             ,PR__NOTE=this.w_PR__NOTE;
             ,PRAPPUNT=this.w_PRAPPUNT;
             ,PRDAFARE=this.w_PRDAFARE;
             ,PRSESTEL=this.w_PRSESTEL;
             ,PRCATNOT=this.w_PRCATNOT;
             ,PRATTGEN=this.w_PRATTGEN;
             ,PRASSENZ=this.w_PRASSENZ;
             ,PRUDIENZ=this.w_PRUDIENZ;
             ,PRINSPRE=this.w_PRINSPRE;
             ,PRTIPANA=this.w_PRTIPANA;
             ,PRCODCOM=this.w_PRCODCOM;
             ,PRDATFIN=this.w_PRDATFIN;
             ,PRDATATP=this.w_PRDATATP;
             ,PRDATATS=this.w_PRDATATS;
             ,PRPATHEX=this.w_PRPATHEX;
             ,PRPATHIM=this.w_PRPATHIM;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAG_MPE : Saving
      this.GSAG_MPE.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PRSERIAL,"COSERPRO";
             )
      this.GSAG_MPE.mReplace()
      * --- GSAG_MPI : Saving
      this.GSAG_MPI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PRSERIAL,"PICODPRO";
             )
      this.GSAG_MPI.mReplace()
      * --- gsag_mpc : Saving
      this.gsag_mpc.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PRSERIAL,"PCSERPRO";
             ,this.w_TIPESP,"PCTIPTRA";
             )
      this.gsag_mpc.mReplace()
      * --- gsag_mpk : Saving
      this.gsag_mpk.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PRSERIAL,"PCSERPRO";
             ,this.w_TIPIMP,"PCTIPTRA";
             )
      this.gsag_mpk.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsag_ape
    * --- Controlli Finali
     Ah_Msg('Controlli finali...',.T.)
     This.NotifyEvent('ControlliFinali')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAG_MPE : Deleting
    this.GSAG_MPE.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PRSERIAL,"COSERPRO";
           )
    this.GSAG_MPE.mDelete()
    * --- GSAG_MPI : Deleting
    this.GSAG_MPI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PRSERIAL,"PICODPRO";
           )
    this.GSAG_MPI.mDelete()
    * --- gsag_mpc : Deleting
    this.gsag_mpc.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PRSERIAL,"PCSERPRO";
           ,this.w_TIPESP,"PCTIPTRA";
           )
    this.gsag_mpc.mDelete()
    * --- gsag_mpk : Deleting
    this.gsag_mpk.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PRSERIAL,"PCSERPRO";
           ,this.w_TIPIMP,"PCTIPTRA";
           )
    this.gsag_mpk.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PRO_EXPO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PRO_EXPO_IDX,i_nConn)
      *
      * delete PRO_EXPO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PRSERIAL',this.w_PRSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsag_ape
    * --- Controlli Finali
     Ah_Msg('Controlli finali...',.T.)
     This.NotifyEvent('ControlliFinali')
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRO_EXPO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
        if .o_PRTIPDAT<>.w_PRTIPDAT
            .w_PRDATINI = Cp_chartodate('  -  -    ')
        endif
        .DoRTCalc(10,27,.t.)
            .w_DESUTE = Alltrim(.w_NOME)+' ' + Alltrim(.w_COGNOM)
        .DoRTCalc(29,33,.t.)
        if .o_PRTIPDAT<>.w_PRTIPDAT
            .w_PRDATFIN = Cp_chartodate('  -  -    ')
        endif
        if .o_PRNOFLAT<>.w_PRNOFLAT
            .w_PRDATATP = IIF(.w_PRNOFLAT$'ENI', 0, .w_PRDATATP)
        endif
        if .o_PRNOFLAT<>.w_PRNOFLAT
            .w_PRDATATS = IIF(.w_PRNOFLAT$ 'ENF', 0, .w_PRDATATS)
        endif
        .DoRTCalc(37,39,.t.)
            .w_TIPESP = 'E'
            .w_TIPIMP = 'I'
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page2.oPag.BTNQRY.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"PROFI","i_CODAZI,w_PRSERIAL")
          .op_PRSERIAL = .w_PRSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(42,42,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page2.oPag.BTNQRY.Calculate()
    endwith
  return

  proc Calculate_WMFTZGPBJG()
    with this
          * --- Controlli a checkform
          gsag_bch(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_TLQOYKBSDU()
    with this
          * --- Controlli Replace end
          gsag_bch(this;
              ,'R';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPRCODUTE_1_3.enabled = this.oPgFrm.Page1.oPag.oPRCODUTE_1_3.mCond()
    this.oPgFrm.Page1.oPag.oPRDATATP_1_51.enabled = this.oPgFrm.Page1.oPag.oPRDATATP_1_51.mCond()
    this.oPgFrm.Page1.oPag.oPRDATATS_1_52.enabled = this.oPgFrm.Page1.oPag.oPRDATATS_1_52.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPRNOFLAT_1_9.visible=!this.oPgFrm.Page1.oPag.oPRNOFLAT_1_9.mHide()
    this.oPgFrm.Page1.oPag.oPRDATINI_1_11.visible=!this.oPgFrm.Page1.oPag.oPRDATINI_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oPRUDIENZ_1_37.visible=!this.oPgFrm.Page1.oPag.oPRUDIENZ_1_37.mHide()
    this.oPgFrm.Page1.oPag.oPRTIPANA_1_43.visible=!this.oPgFrm.Page1.oPag.oPRTIPANA_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oPRDATFIN_1_49.visible=!this.oPgFrm.Page1.oPag.oPRDATFIN_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oPRDATATP_1_51.visible=!this.oPgFrm.Page1.oPag.oPRDATATP_1_51.mHide()
    this.oPgFrm.Page1.oPag.oPRDATATS_1_52.visible=!this.oPgFrm.Page1.oPag.oPRDATATS_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_65.visible=!this.oPgFrm.Page1.oPag.oBtn_1_65.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_7.visible=!this.oPgFrm.Page2.oPag.oBtn_2_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_WMFTZGPBJG()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.BTNQRY.Event(cEvent)
      .oPgFrm.Page2.oPag.BTNQRY.Event(cEvent)
        if lower(cEvent)==lower("ControlliFinali")
          .Calculate_TLQOYKBSDU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PRCODUTE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_PRCODUTE)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPNOME,DPCOGNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_PRCODUTE))
          select DPCODICE,DPNOME,DPCOGNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODUTE)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCODUTE) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oPRCODUTE_1_3'),i_cWhere,'GSAR_BDZ',"Persone",'GSARTADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPNOME,DPCOGNOM";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPNOME,DPCOGNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPNOME,DPCOGNOM";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_PRCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_PRCODUTE)
            select DPCODICE,DPNOME,DPCOGNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODUTE = NVL(_Link_.DPCODICE,space(5))
      this.w_NOME = NVL(_Link_.DPNOME,space(50))
      this.w_COGNOM = NVL(_Link_.DPCOGNOM,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODUTE = space(5)
      endif
      this.w_NOME = space(50)
      this.w_COGNOM = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.DPCODICE as DPCODICE103"+ ",link_1_3.DPNOME as DPNOME103"+ ",link_1_3.DPCOGNOM as DPCOGNOM103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on PRO_EXPO.PRCODUTE=link_1_3.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and PRO_EXPO.PRCODUTE=link_1_3.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCAUATT
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCAUATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PRCAUATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PRCAUATT))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCAUATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_PRCAUATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_PRCAUATT)+"%");

            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRCAUATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oPRCAUATT_1_6'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCAUATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PRCAUATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PRCAUATT)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCAUATT = NVL(_Link_.CACODICE,space(20))
      this.w_DESCAU = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_PRCAUATT = space(20)
      endif
      this.w_DESCAU = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCAUATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.CACODICE as CACODICE106"+ ",link_1_6.CADESCRI as CADESCRI106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on PRO_EXPO.PRCAUATT=link_1_6.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and PRO_EXPO.PRCAUATT=link_1_6.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRNOMINA
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRNOMINA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_PRNOMINA)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_PRNOMINA))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRNOMINA)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_PRNOMINA)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_PRNOMINA)+"%");

            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRNOMINA) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oPRNOMINA_1_16'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRNOMINA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_PRNOMINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_PRNOMINA)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRNOMINA = NVL(_Link_.NOCODICE,space(15))
      this.w_NODES = NVL(_Link_.NODESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_PRNOMINA = space(15)
      endif
      this.w_NODES = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRNOMINA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFF_NOMI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.NOCODICE as NOCODICE116"+ ",link_1_16.NODESCRI as NODESCRI116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on PRO_EXPO.PRNOMINA=link_1_16.NOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and PRO_EXPO.PRNOMINA=link_1_16.NOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRTIPOLO
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFFTIPAT_IDX,3]
    i_lTable = "OFFTIPAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2], .t., this.OFFTIPAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRTIPOLO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFFTIPAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODTIP like "+cp_ToStrODBC(trim(this.w_PRTIPOLO)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODTIP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODTIP',trim(this.w_PRTIPOLO))
          select TACODTIP,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODTIP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRTIPOLO)==trim(_Link_.TACODTIP) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStrODBC(trim(this.w_PRTIPOLO)+"%");

            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStr(trim(this.w_PRTIPOLO)+"%");

            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRTIPOLO) and !this.bDontReportError
            deferred_cp_zoom('OFFTIPAT','*','TACODTIP',cp_AbsName(oSource.parent,'oPRTIPOLO_1_23'),i_cWhere,'',"Tipologie attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',oSource.xKey(1))
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRTIPOLO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(this.w_PRTIPOLO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',this.w_PRTIPOLO)
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRTIPOLO = NVL(_Link_.TACODTIP,space(5))
      this.w_DESTIP = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_PRTIPOLO = space(5)
      endif
      this.w_DESTIP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])+'\'+cp_ToStr(_Link_.TACODTIP,1)
      cp_ShowWarn(i_cKey,this.OFFTIPAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRTIPOLO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFFTIPAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.TACODTIP as TACODTIP123"+ ",link_1_23.TADESCRI as TADESCRI123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on PRO_EXPO.PRTIPOLO=link_1_23.TACODTIP"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and PRO_EXPO.PRTIPOLO=link_1_23.TACODTIP(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCODCOM
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_PRCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_PRCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_PRCODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_PRCODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oPRCODCOM_1_45'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_PRCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_PRCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODCOM = space(15)
      endif
      this.w_DESCAN = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_45(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_45.CNCODCAN as CNCODCAN145"+ ",link_1_45.CNDESCAN as CNDESCAN145"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_45 on PRO_EXPO.PRCODCOM=link_1_45.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_45"
          i_cKey=i_cKey+'+" and PRO_EXPO.PRCODCOM=link_1_45.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPRSERIAL_1_1.value==this.w_PRSERIAL)
      this.oPgFrm.Page1.oPag.oPRSERIAL_1_1.value=this.w_PRSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDESCRI_1_2.value==this.w_PRDESCRI)
      this.oPgFrm.Page1.oPag.oPRDESCRI_1_2.value=this.w_PRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODUTE_1_3.value==this.w_PRCODUTE)
      this.oPgFrm.Page1.oPag.oPRCODUTE_1_3.value=this.w_PRCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPR__TIPO_1_4.RadioValue()==this.w_PR__TIPO)
      this.oPgFrm.Page1.oPag.oPR__TIPO_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCAUATT_1_6.value==this.w_PRCAUATT)
      this.oPgFrm.Page1.oPag.oPRCAUATT_1_6.value=this.w_PRCAUATT
    endif
    if not(this.oPgFrm.Page1.oPag.oPRTIPDAT_1_7.RadioValue()==this.w_PRTIPDAT)
      this.oPgFrm.Page1.oPag.oPRTIPDAT_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPR_STATO_1_8.RadioValue()==this.w_PR_STATO)
      this.oPgFrm.Page1.oPag.oPR_STATO_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRNOFLAT_1_9.RadioValue()==this.w_PRNOFLAT)
      this.oPgFrm.Page1.oPag.oPRNOFLAT_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATINI_1_11.value==this.w_PRDATINI)
      this.oPgFrm.Page1.oPag.oPRDATINI_1_11.value=this.w_PRDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGGETT_1_14.value==this.w_PROGGETT)
      this.oPgFrm.Page1.oPag.oPROGGETT_1_14.value=this.w_PROGGETT
    endif
    if not(this.oPgFrm.Page1.oPag.oPRNOMINA_1_16.value==this.w_PRNOMINA)
      this.oPgFrm.Page1.oPag.oPRNOMINA_1_16.value=this.w_PRNOMINA
    endif
    if not(this.oPgFrm.Page1.oPag.oPRRIFPER_1_17.value==this.w_PRRIFPER)
      this.oPgFrm.Page1.oPag.oPRRIFPER_1_17.value=this.w_PRRIFPER
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDISPON_1_19.RadioValue()==this.w_PRDISPON)
      this.oPgFrm.Page1.oPag.oPRDISPON_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRLOCALI_1_20.value==this.w_PRLOCALI)
      this.oPgFrm.Page1.oPag.oPRLOCALI_1_20.value=this.w_PRLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oPRTIPOLO_1_23.value==this.w_PRTIPOLO)
      this.oPgFrm.Page1.oPag.oPRTIPOLO_1_23.value=this.w_PRTIPOLO
    endif
    if not(this.oPgFrm.Page1.oPag.oPR__NOTE_1_25.value==this.w_PR__NOTE)
      this.oPgFrm.Page1.oPag.oPR__NOTE_1_25.value=this.w_PR__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oNODES_1_28.value==this.w_NODES)
      this.oPgFrm.Page1.oPag.oNODES_1_28.value=this.w_NODES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_29.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_29.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIP_1_30.value==this.w_DESTIP)
      this.oPgFrm.Page1.oPag.oDESTIP_1_30.value=this.w_DESTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oPRAPPUNT_1_31.RadioValue()==this.w_PRAPPUNT)
      this.oPgFrm.Page1.oPag.oPRAPPUNT_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDAFARE_1_32.RadioValue()==this.w_PRDAFARE)
      this.oPgFrm.Page1.oPag.oPRDAFARE_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRSESTEL_1_33.RadioValue()==this.w_PRSESTEL)
      this.oPgFrm.Page1.oPag.oPRSESTEL_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCATNOT_1_34.RadioValue()==this.w_PRCATNOT)
      this.oPgFrm.Page1.oPag.oPRCATNOT_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRATTGEN_1_35.RadioValue()==this.w_PRATTGEN)
      this.oPgFrm.Page1.oPag.oPRATTGEN_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRASSENZ_1_36.RadioValue()==this.w_PRASSENZ)
      this.oPgFrm.Page1.oPag.oPRASSENZ_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRUDIENZ_1_37.RadioValue()==this.w_PRUDIENZ)
      this.oPgFrm.Page1.oPag.oPRUDIENZ_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_39.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_39.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPRTIPANA_1_43.RadioValue()==this.w_PRTIPANA)
      this.oPgFrm.Page1.oPag.oPRTIPANA_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODCOM_1_45.value==this.w_PRCODCOM)
      this.oPgFrm.Page1.oPag.oPRCODCOM_1_45.value=this.w_PRCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_46.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_46.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATFIN_1_49.value==this.w_PRDATFIN)
      this.oPgFrm.Page1.oPag.oPRDATFIN_1_49.value=this.w_PRDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATATP_1_51.value==this.w_PRDATATP)
      this.oPgFrm.Page1.oPag.oPRDATATP_1_51.value=this.w_PRDATATP
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATATS_1_52.value==this.w_PRDATATS)
      this.oPgFrm.Page1.oPag.oPRDATATS_1_52.value=this.w_PRDATATS
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPATHEX_1_59.value==this.w_PRPATHEX)
      this.oPgFrm.Page1.oPag.oPRPATHEX_1_59.value=this.w_PRPATHEX
    endif
    if not(this.oPgFrm.Page2.oPag.oPRPATHIM_2_3.value==this.w_PRPATHIM)
      this.oPgFrm.Page2.oPag.oPRPATHIM_2_3.value=this.w_PRPATHIM
    endif
    cp_SetControlsValueExtFlds(this,'PRO_EXPO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PRDESCRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRDESCRI_1_2.SetFocus()
            i_bnoObbl = !empty(.w_PRDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PRCODUTE))  and (.Cfunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRCODUTE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_PRCODUTE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PRPATHEX)) or not(DIRECTORY(JUSTPATH(.w_PRPATHEX))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPRPATHEX_1_59.SetFocus()
            i_bnoObbl = !empty(.w_PRPATHEX)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, percorso file inesistente")
          case   not(DIRECTORY(JUSTPATH(.w_PRPATHIM)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPRPATHIM_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, percorso file inesistente")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAG_MPE.CheckForm()
      if i_bres
        i_bres=  .GSAG_MPE.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSAG_MPI.CheckForm()
      if i_bres
        i_bres=  .GSAG_MPI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .gsag_mpc.CheckForm()
      if i_bres
        i_bres=  .gsag_mpc.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .gsag_mpk.CheckForm()
      if i_bres
        i_bres=  .gsag_mpk.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsag_ape
      * --- Chk Finali
      IF i_bRes
         .w_RESCHK=0
         Ah_Msg('Check finali...',.T.)
         .NotifyEvent('ChkFinali')
         WAIT CLEAR
         IF .w_RESCHK<>0
           i_bRes=.f.
         ENDIF
      ENDIF
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PRTIPDAT = this.w_PRTIPDAT
    this.o_PRNOFLAT = this.w_PRNOFLAT
    * --- GSAG_MPE : Depends On
    this.GSAG_MPE.SaveDependsOn()
    * --- GSAG_MPI : Depends On
    this.GSAG_MPI.SaveDependsOn()
    * --- gsag_mpc : Depends On
    this.gsag_mpc.SaveDependsOn()
    * --- gsag_mpk : Depends On
    this.gsag_mpk.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsag_apePag1 as StdContainer
  Width  = 706
  height = 497
  stdWidth  = 706
  stdheight = 497
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRSERIAL_1_1 as StdField with uid="UNXTXKWATX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PRSERIAL", cQueryName = "PRSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 81813570,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=129, Top=13, InputMask=replicate('X',10)

  add object oPRDESCRI_1_2 as StdField with uid="DDYURMOEMW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PRDESCRI", cQueryName = "PRDESCRI",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione profilo",;
    HelpContextID = 250572863,;
   bGlobalFont=.t.,;
    Height=21, Width=462, Left=220, Top=13, InputMask=replicate('X',50)

  add object oPRCODUTE_1_3 as StdField with uid="IVOGNCRSCT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PRCODUTE", cQueryName = "PRCODUTE",nZero=5,;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona",;
    HelpContextID = 614459,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=129, Top=39, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_PRCODUTE"

  func oPRCODUTE_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.Cfunction='Load')
    endwith
   endif
  endfunc

  func oPRCODUTE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODUTE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCODUTE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oPRCODUTE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone",'GSARTADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oPRCODUTE_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_PRCODUTE
     i_obj.ecpSave()
  endproc


  add object oPR__TIPO_1_4 as StdCombo with uid="RMZPLIXPJK",rtseq=4,rtrep=.f.,left=129,top=65,width=151,height=21, enabled=.f.;
    , HelpContextID = 85663813;
    , cFormVar="w_PR__TIPO",RowSource=""+"ICS,"+"VCS", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPR__TIPO_1_4.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'V',;
    space(1))))
  endfunc
  func oPR__TIPO_1_4.GetRadio()
    this.Parent.oContained.w_PR__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oPR__TIPO_1_4.SetRadio()
    this.Parent.oContained.w_PR__TIPO=trim(this.Parent.oContained.w_PR__TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_PR__TIPO=='I',1,;
      iif(this.Parent.oContained.w_PR__TIPO=='V',2,;
      0))
  endfunc

  add object oPRCAUATT_1_6 as StdField with uid="NBQLHBTOOQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PRCAUATT", cQueryName = "PRCAUATT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo Attivit�",;
    HelpContextID = 218849354,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=129, Top=102, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_PRCAUATT"

  func oPRCAUATT_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCAUATT_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCAUATT_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oPRCAUATT_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oPRCAUATT_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PRCAUATT
     i_obj.ecpSave()
  endproc


  add object oPRTIPDAT_1_7 as StdCombo with uid="FAVVSGXWYK",rtseq=6,rtrep=.f.,left=129,top=128,width=150,height=21;
    , ToolTipText = "Date prefissate\dinamiche";
    , HelpContextID = 264532042;
    , cFormVar="w_PRTIPDAT",RowSource=""+"Prefissate,"+"Dinamiche", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRTIPDAT_1_7.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oPRTIPDAT_1_7.GetRadio()
    this.Parent.oContained.w_PRTIPDAT = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPDAT_1_7.SetRadio()
    this.Parent.oContained.w_PRTIPDAT=trim(this.Parent.oContained.w_PRTIPDAT)
    this.value = ;
      iif(this.Parent.oContained.w_PRTIPDAT=='P',1,;
      iif(this.Parent.oContained.w_PRTIPDAT=='D',2,;
      0))
  endfunc


  add object oPR_STATO_1_8 as StdCombo with uid="KYNYOBXYHT",rtseq=7,rtrep=.f.,left=514,top=128,width=151,height=21;
    , ToolTipText = "Stato";
    , HelpContextID = 219095109;
    , cFormVar="w_PR_STATO",RowSource=""+"Non evasa,"+"Da svolgere,"+"In corso,"+"Provvisoria,"+"Evasa,"+"Completata,"+"Evasa o completata,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPR_STATO_1_8.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'T',;
    iif(this.value =5,'F',;
    iif(this.value =6,'P',;
    iif(this.value =7,'B',;
    iif(this.value =8,'K',;
    space(1))))))))))
  endfunc
  func oPR_STATO_1_8.GetRadio()
    this.Parent.oContained.w_PR_STATO = this.RadioValue()
    return .t.
  endfunc

  func oPR_STATO_1_8.SetRadio()
    this.Parent.oContained.w_PR_STATO=trim(this.Parent.oContained.w_PR_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_PR_STATO=='N',1,;
      iif(this.Parent.oContained.w_PR_STATO=='D',2,;
      iif(this.Parent.oContained.w_PR_STATO=='I',3,;
      iif(this.Parent.oContained.w_PR_STATO=='T',4,;
      iif(this.Parent.oContained.w_PR_STATO=='F',5,;
      iif(this.Parent.oContained.w_PR_STATO=='P',6,;
      iif(this.Parent.oContained.w_PR_STATO=='B',7,;
      iif(this.Parent.oContained.w_PR_STATO=='K',8,;
      0))))))))
  endfunc


  add object oPRNOFLAT_1_9 as StdCombo with uid="PYKMLGYMOI",rtseq=8,rtrep=.f.,left=129,top=151,width=185,height=21;
    , ToolTipText = "Filtri data";
    , HelpContextID = 120197194;
    , cFormVar="w_PRNOFLAT",RowSource=""+"Intervallo,"+"Data odierna,"+"Prima data vuota,"+"Seconda data vuota,"+"Entrambe date vuote,"+"Settimana corrente,"+"Settimana corrente+successiva,"+"Settimana corrente+precedente,"+"Settimana corrente+prec.+succ.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRNOFLAT_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    iif(this.value =5,'E',;
    iif(this.value =6,'A',;
    iif(this.value =7,'B',;
    iif(this.value =8,'C',;
    iif(this.value =9,'D',;
    space(1)))))))))))
  endfunc
  func oPRNOFLAT_1_9.GetRadio()
    this.Parent.oContained.w_PRNOFLAT = this.RadioValue()
    return .t.
  endfunc

  func oPRNOFLAT_1_9.SetRadio()
    this.Parent.oContained.w_PRNOFLAT=trim(this.Parent.oContained.w_PRNOFLAT)
    this.value = ;
      iif(this.Parent.oContained.w_PRNOFLAT=='S',1,;
      iif(this.Parent.oContained.w_PRNOFLAT=='N',2,;
      iif(this.Parent.oContained.w_PRNOFLAT=='I',3,;
      iif(this.Parent.oContained.w_PRNOFLAT=='F',4,;
      iif(this.Parent.oContained.w_PRNOFLAT=='E',5,;
      iif(this.Parent.oContained.w_PRNOFLAT=='A',6,;
      iif(this.Parent.oContained.w_PRNOFLAT=='B',7,;
      iif(this.Parent.oContained.w_PRNOFLAT=='C',8,;
      iif(this.Parent.oContained.w_PRNOFLAT=='D',9,;
      0)))))))))
  endfunc

  func oPRNOFLAT_1_9.mHide()
    with this.Parent.oContained
      return (.w_PRTIPDAT='P')
    endwith
  endfunc

  add object oPRDATINI_1_11 as StdField with uid="VODSSEKIEM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PRDATINI", cQueryName = "PRDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale",;
    HelpContextID = 184848321,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=129, Top=150

  func oPRDATINI_1_11.mHide()
    with this.Parent.oContained
      return (.w_PRTIPDAT='D')
    endwith
  endfunc

  add object oPROGGETT_1_14 as AH_SEARCHFLD with uid="JUXTNZUUCC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PROGGETT", cQueryName = "PROGGETT",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 3285066,;
   bGlobalFont=.t.,;
    Height=21, Width=554, Left=129, Top=176, InputMask=replicate('X',254)

  add object oPRNOMINA_1_16 as StdField with uid="KGBFFVLFMG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PRNOMINA", cQueryName = "PRNOMINA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nominativo",;
    HelpContextID = 191229897,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=129, Top=200, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", oKey_1_1="NOCODICE", oKey_1_2="this.w_PRNOMINA"

  func oPRNOMINA_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRNOMINA_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRNOMINA_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oPRNOMINA_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oPRRIFPER_1_17 as AH_SEARCHFLD with uid="ZFYCQALTJC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PRRIFPER", cQueryName = "PRRIFPER",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento persona",;
    HelpContextID = 186929224,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=129, Top=225, InputMask=replicate('X',60)


  add object oPRDISPON_1_19 as StdCombo with uid="JBHZXFOOHY",value=5,rtseq=13,rtrep=.f.,left=129,top=250,width=140,height=21;
    , ToolTipText = "Disponibilit�";
    , HelpContextID = 200503364;
    , cFormVar="w_PRDISPON",RowSource=""+"Occupato,"+"Per urgenze,"+"Libero,"+"Fuori sede,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRDISPON_1_19.RadioValue()
    return(iif(this.value =1,2,;
    iif(this.value =2,3,;
    iif(this.value =3,1,;
    iif(this.value =4,4,;
    iif(this.value =5,0,;
    0))))))
  endfunc
  func oPRDISPON_1_19.GetRadio()
    this.Parent.oContained.w_PRDISPON = this.RadioValue()
    return .t.
  endfunc

  func oPRDISPON_1_19.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PRDISPON==2,1,;
      iif(this.Parent.oContained.w_PRDISPON==3,2,;
      iif(this.Parent.oContained.w_PRDISPON==1,3,;
      iif(this.Parent.oContained.w_PRDISPON==4,4,;
      iif(this.Parent.oContained.w_PRDISPON==0,5,;
      0)))))
  endfunc

  add object oPRLOCALI_1_20 as AH_SEARCHFLD with uid="GGNUYZMXVQ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PRLOCALI", cQueryName = "PRLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit�",;
    HelpContextID = 67506113,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=338, Top=250, InputMask=replicate('X',30)

  add object oPRTIPOLO_1_23 as StdField with uid="PZBXWEDXJE",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PRTIPOLO", cQueryName = "PRTIPOLO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia",;
    HelpContextID = 87789499,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=129, Top=276, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="OFFTIPAT", oKey_1_1="TACODTIP", oKey_1_2="this.w_PRTIPOLO"

  func oPRTIPOLO_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRTIPOLO_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRTIPOLO_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFFTIPAT','*','TACODTIP',cp_AbsName(this.parent,'oPRTIPOLO_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipologie attivit�",'',this.parent.oContained
  endproc

  add object oPR__NOTE_1_25 as AH_SEARCHFLD with uid="AHQUCYIFWU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PR__NOTE", cQueryName = "PR__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 180035643,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=129, Top=301

  add object oNODES_1_28 as StdField with uid="QOLHBRTVCB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_NODES", cQueryName = "NODES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 68195114,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=249, Top=200, InputMask=replicate('X',60)

  add object oDESCAU_1_29 as StdField with uid="YYCWBCAXNC",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 265179702,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=284, Top=102, InputMask=replicate('X',254)

  add object oDESTIP_1_30 as StdField with uid="YABTSEIHHF",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESTIP", cQueryName = "DESTIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 190796342,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=183, Top=276, InputMask=replicate('X',50)

  add object oPRAPPUNT_1_31 as StdCheck with uid="UZYPORAADI",rtseq=20,rtrep=.f.,left=129, top=369, caption="Appuntamenti",;
    ToolTipText = "Appuntamenti",;
    HelpContextID = 13254730,;
    cFormVar="w_PRAPPUNT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPRAPPUNT_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPRAPPUNT_1_31.GetRadio()
    this.Parent.oContained.w_PRAPPUNT = this.RadioValue()
    return .t.
  endfunc

  func oPRAPPUNT_1_31.SetRadio()
    this.Parent.oContained.w_PRAPPUNT=trim(this.Parent.oContained.w_PRAPPUNT)
    this.value = ;
      iif(this.Parent.oContained.w_PRAPPUNT=='S',1,;
      0)
  endfunc

  add object oPRDAFARE_1_32 as StdCheck with uid="BLUIJQPUXI",rtseq=21,rtrep=.f.,left=250, top=369, caption="Cose da fare",;
    ToolTipText = "Cose da fare",;
    HelpContextID = 203124795,;
    cFormVar="w_PRDAFARE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPRDAFARE_1_32.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oPRDAFARE_1_32.GetRadio()
    this.Parent.oContained.w_PRDAFARE = this.RadioValue()
    return .t.
  endfunc

  func oPRDAFARE_1_32.SetRadio()
    this.Parent.oContained.w_PRDAFARE=trim(this.Parent.oContained.w_PRDAFARE)
    this.value = ;
      iif(this.Parent.oContained.w_PRDAFARE=='D',1,;
      0)
  endfunc

  add object oPRSESTEL_1_33 as StdCheck with uid="AVZGKWQUGN",rtseq=22,rtrep=.f.,left=356, top=369, caption="Sessioni telefoniche",;
    HelpContextID = 267411522,;
    cFormVar="w_PRSESTEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPRSESTEL_1_33.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oPRSESTEL_1_33.GetRadio()
    this.Parent.oContained.w_PRSESTEL = this.RadioValue()
    return .t.
  endfunc

  func oPRSESTEL_1_33.SetRadio()
    this.Parent.oContained.w_PRSESTEL=trim(this.Parent.oContained.w_PRSESTEL)
    this.value = ;
      iif(this.Parent.oContained.w_PRSESTEL=='T',1,;
      0)
  endfunc

  add object oPRCATNOT_1_34 as StdCheck with uid="KDEFUIVSLF",rtseq=23,rtrep=.f.,left=499, top=369, caption="Note",;
    ToolTipText = "Note",;
    HelpContextID = 167469130,;
    cFormVar="w_PRCATNOT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPRCATNOT_1_34.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oPRCATNOT_1_34.GetRadio()
    this.Parent.oContained.w_PRCATNOT = this.RadioValue()
    return .t.
  endfunc

  func oPRCATNOT_1_34.SetRadio()
    this.Parent.oContained.w_PRCATNOT=trim(this.Parent.oContained.w_PRCATNOT)
    this.value = ;
      iif(this.Parent.oContained.w_PRCATNOT=='M',1,;
      0)
  endfunc

  add object oPRATTGEN_1_35 as StdCheck with uid="SCSNJFJQEF",rtseq=24,rtrep=.f.,left=129, top=388, caption="Attivit� generiche",;
    ToolTipText = "Attivit� generiche",;
    HelpContextID = 51265604,;
    cFormVar="w_PRATTGEN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPRATTGEN_1_35.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oPRATTGEN_1_35.GetRadio()
    this.Parent.oContained.w_PRATTGEN = this.RadioValue()
    return .t.
  endfunc

  func oPRATTGEN_1_35.SetRadio()
    this.Parent.oContained.w_PRATTGEN=trim(this.Parent.oContained.w_PRATTGEN)
    this.value = ;
      iif(this.Parent.oContained.w_PRATTGEN=='G',1,;
      0)
  endfunc

  add object oPRASSENZ_1_36 as StdCheck with uid="SSQFXYHEPP",rtseq=25,rtrep=.f.,left=250, top=388, caption="Assenze",;
    ToolTipText = "Assenze",;
    HelpContextID = 251838384,;
    cFormVar="w_PRASSENZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPRASSENZ_1_36.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oPRASSENZ_1_36.GetRadio()
    this.Parent.oContained.w_PRASSENZ = this.RadioValue()
    return .t.
  endfunc

  func oPRASSENZ_1_36.SetRadio()
    this.Parent.oContained.w_PRASSENZ=trim(this.Parent.oContained.w_PRASSENZ)
    this.value = ;
      iif(this.Parent.oContained.w_PRASSENZ=='A',1,;
      0)
  endfunc

  add object oPRUDIENZ_1_37 as StdCheck with uid="EOUSAQVEAT",rtseq=26,rtrep=.f.,left=356, top=388, caption="Udienze",;
    ToolTipText = "Udienze",;
    HelpContextID = 263225264,;
    cFormVar="w_PRUDIENZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPRUDIENZ_1_37.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oPRUDIENZ_1_37.GetRadio()
    this.Parent.oContained.w_PRUDIENZ = this.RadioValue()
    return .t.
  endfunc

  func oPRUDIENZ_1_37.SetRadio()
    this.Parent.oContained.w_PRUDIENZ=trim(this.Parent.oContained.w_PRUDIENZ)
    this.value = ;
      iif(this.Parent.oContained.w_PRUDIENZ=='U',1,;
      0)
  endfunc

  func oPRUDIENZ_1_37.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oDESUTE_1_39 as StdField with uid="LMXZQYPHAH",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 17846838,;
   bGlobalFont=.t.,;
    Height=21, Width=482, Left=199, Top=39, InputMask=replicate('X',100)


  add object oPRTIPANA_1_43 as StdCombo with uid="WGJUGXDFTC",rtseq=31,rtrep=.f.,left=129,top=324,width=110,height=21;
    , ToolTipText = "Natura analitica";
    , HelpContextID = 54235081;
    , cFormVar="w_PRTIPANA",RowSource=""+"Costo,"+"Ricavo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRTIPANA_1_43.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oPRTIPANA_1_43.GetRadio()
    this.Parent.oContained.w_PRTIPANA = this.RadioValue()
    return .t.
  endfunc

  func oPRTIPANA_1_43.SetRadio()
    this.Parent.oContained.w_PRTIPANA=trim(this.Parent.oContained.w_PRTIPANA)
    this.value = ;
      iif(this.Parent.oContained.w_PRTIPANA=='C',1,;
      iif(this.Parent.oContained.w_PRTIPANA=='R',2,;
      0))
  endfunc

  func oPRTIPANA_1_43.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oPRCODCOM_1_45 as StdField with uid="QUALFHPQKB",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PRCODCOM", cQueryName = "PRCODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 32939965,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=129, Top=345, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_PRCODCOM"

  func oPRCODCOM_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODCOM_1_45.ecpDrop(oSource)
    this.Parent.oContained.link_1_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPRCODCOM_1_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oPRCODCOM_1_45'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESCAN_1_46 as StdField with uid="ZIDDXSQPSG",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 147739190,;
   bGlobalFont=.t.,;
    Height=21, Width=432, Left=249, Top=345, InputMask=replicate('X',100)

  add object oPRDATFIN_1_49 as StdField with uid="QYFCMKHVAV",rtseq=34,rtrep=.f.,;
    cFormVar = "w_PRDATFIN", cQueryName = "PRDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale",;
    HelpContextID = 235179964,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=284, Top=150

  func oPRDATFIN_1_49.mHide()
    with this.Parent.oContained
      return (.w_PRTIPDAT='D')
    endwith
  endfunc

  add object oPRDATATP_1_51 as StdField with uid="VBYREBKRJF",rtseq=35,rtrep=.f.,;
    cFormVar = "w_PRDATATP", cQueryName = "PRDATATP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sottrarre alla data di sistema",;
    HelpContextID = 217804870,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=514, Top=150

  func oPRDATATP_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRNOFLAT$ 'SF')
    endwith
   endif
  endfunc

  func oPRDATATP_1_51.mHide()
    with this.Parent.oContained
      return (.w_PRNOFLAT$ 'IEABCD' OR .w_PRTIPDAT='P')
    endwith
  endfunc

  add object oPRDATATS_1_52 as StdField with uid="DXZRHDMIEW",rtseq=36,rtrep=.f.,;
    cFormVar = "w_PRDATATS", cQueryName = "PRDATATS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni filtro da sommare alla data di sistema",;
    HelpContextID = 217804873,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=650, Top=150

  func oPRDATATS_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PRNOFLAT$ 'SI')
    endwith
   endif
  endfunc

  func oPRDATATS_1_52.mHide()
    with this.Parent.oContained
      return (.w_PRNOFLAT$ 'EFABCD' OR .w_PRTIPDAT='P')
    endwith
  endfunc

  add object oPRPATHEX_1_59 as StdField with uid="QNUXWTUMFV",rtseq=37,rtrep=.f.,;
    cFormVar = "w_PRPATHEX", cQueryName = "PRPATHEX",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, percorso file inesistente",;
    ToolTipText = "File di export",;
    HelpContextID = 66859086,;
   bGlobalFont=.t.,;
    Height=21, Width=468, Left=129, Top=418, InputMask=replicate('X',254)

  func oPRPATHEX_1_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (DIRECTORY(JUSTPATH(.w_PRPATHEX)))
    endwith
    return bRes
  endfunc


  add object oLinkPC_1_64 as StdButton with uid="IBYAVIVRSL",left=580, top=452, width=48,height=45,;
    CpPicture="bmp\trascodifica.ico", caption="", nPag=1;
    , ToolTipText = "Mappatura categorie per export";
    , HelpContextID = 206734559;
    , Caption='\<Categorie';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_64.Click()
      this.Parent.oContained.gsag_mpc.LinkPCClick()
    endproc


  add object oBtn_1_65 as StdButton with uid="XUIMGVFZJF",left=631, top=452, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per esportare attivit�";
    , HelpContextID = 34361414;
    , Caption='\<Esporta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_65.Click()
      with this.Parent.oContained
        do gsag_kis with  .w_PRSERIAL
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_65.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_PRSERIAL) or .Cfunction<>'Query')
     endwith
    endif
  endfunc


  add object BTNQRY as cp_askfile with uid="SVNANQOYOT",left=599, top=419, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_PRPATHEX",cext="ICS,VCS",;
    nPag=1;
    , ToolTipText = "Premere per selezionare il file di export";
    , HelpContextID = 159847978

  add object oStr_1_5 as StdString with uid="OBKUENKFAK",Visible=.t., Left=11, Top=42,;
    Alignment=1, Width=116, Height=18,;
    Caption="Destinatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="UJDKAVUIHH",Visible=.t., Left=63, Top=106,;
    Alignment=1, Width=64, Height=18,;
    Caption="Tipo Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="LXFLALDCHQ",Visible=.t., Left=57, Top=154,;
    Alignment=1, Width=70, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_PRTIPDAT='D')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="WDSANYXPRQ",Visible=.t., Left=482, Top=130,;
    Alignment=1, Width=31, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="SCKJLYPZPH",Visible=.t., Left=81, Top=180,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="WYARXJOGFW",Visible=.t., Left=62, Top=204,;
    Alignment=1, Width=65, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="FMGZCCCROY",Visible=.t., Left=55, Top=252,;
    Alignment=1, Width=72, Height=18,;
    Caption="Disponibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="QGLWLKGMUR",Visible=.t., Left=11, Top=229,;
    Alignment=1, Width=116, Height=18,;
    Caption="Riferimento persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="OKYHAZVVDM",Visible=.t., Left=73, Top=280,;
    Alignment=1, Width=54, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="JGXCBOIXFI",Visible=.t., Left=98, Top=304,;
    Alignment=1, Width=29, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="FAZROBRZWM",Visible=.t., Left=289, Top=252,;
    Alignment=1, Width=46, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="MOOTKODNVM",Visible=.t., Left=63, Top=65,;
    Alignment=1, Width=64, Height=18,;
    Caption="Tipo file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="USIZHHZFMU",Visible=.t., Left=87, Top=324,;
    Alignment=1, Width=40, Height=18,;
    Caption="Natura:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="RSCXCGTJXQ",Visible=.t., Left=57, Top=346,;
    Alignment=1, Width=69, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="MXGQCJJOEI",Visible=.t., Left=58, Top=346,;
    Alignment=1, Width=69, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="QETFJMOJAI",Visible=.t., Left=211, Top=152,;
    Alignment=1, Width=70, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (.w_PRTIPDAT='D')
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="BSOGHKRNWR",Visible=.t., Left=28, Top=154,;
    Alignment=1, Width=99, Height=18,;
    Caption="Filtri data:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_PRTIPDAT='P')
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="HUZENCKCWV",Visible=.t., Left=413, Top=151,;
    Alignment=1, Width=100, Height=18,;
    Caption="Giorni indietro:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.w_PRNOFLAT$ 'IEABCD' or .w_PRTIPDAT='P')
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="CJAGLJRGTY",Visible=.t., Left=553, Top=151,;
    Alignment=1, Width=96, Height=18,;
    Caption="Giorni in avanti:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (.w_PRNOFLAT$ 'EFABCD' OR .w_PRTIPDAT='P')
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="ESTAYJIOHG",Visible=.t., Left=63, Top=130,;
    Alignment=1, Width=64, Height=18,;
    Caption="Criteri date:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="LETCACCPSC",Visible=.t., Left=7, Top=80,;
    Alignment=0, Width=73, Height=18,;
    Caption="Export"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_60 as StdString with uid="YJVMZEQDZU",Visible=.t., Left=25, Top=421,;
    Alignment=1, Width=102, Height=18,;
    Caption="File:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="CWHDQXQRLB",Visible=.t., Left=44, Top=15,;
    Alignment=1, Width=83, Height=18,;
    Caption="Profilo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_58 as StdBox with uid="FDXGWNCZVF",left=5, top=96, width=675,height=2
enddefine
define class tgsag_apePag2 as StdContainer
  Width  = 706
  height = 497
  stdWidth  = 706
  stdheight = 497
  resizeXpos=481
  resizeYpos=239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPRPATHIM_2_3 as StdField with uid="PIGDIYYTRR",rtseq=38,rtrep=.f.,;
    cFormVar = "w_PRPATHIM", cQueryName = "PRPATHIM",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, percorso file inesistente",;
    ToolTipText = "Percorso di import",;
    HelpContextID = 201576381,;
   bGlobalFont=.t.,;
    Height=21, Width=487, Left=46, Top=52, InputMask=replicate('X',254)

  func oPRPATHIM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (DIRECTORY(JUSTPATH(.w_PRPATHIM)))
    endwith
    return bRes
  endfunc


  add object oLinkPC_2_5 as stdDynamicChildContainer with uid="NEAUAOTJLH",left=6, top=133, width=641, height=326, bOnScreen=.t.;



  add object oLinkPC_2_6 as StdButton with uid="MKWEVJYBRZ",left=550, top=85, width=48,height=45,;
    CpPicture="bmp\trascodifica.ico", caption="", nPag=2;
    , ToolTipText = "Mappatura categorie per import";
    , HelpContextID = 206734559;
    , Caption='\<Categorie';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_6.Click()
      this.Parent.oContained.gsag_mpk.LinkPCClick()
    endproc


  add object oBtn_2_7 as StdButton with uid="XKRYZTPXZE",left=601, top=85, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per importare attivit�";
    , HelpContextID = 34359942;
    , Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_7.Click()
      with this.Parent.oContained
        do gsag_kiv with  .w_PRSERIAL
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_PRSERIAL) or .Cfunction<>'Query')
     endwith
    endif
  endfunc


  add object BTNQRY as cp_askfile with uid="UTFZBTBKAM",left=536, top=53, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_PRPATHIM",cext="ICS,VCS",;
    nPag=2;
    , ToolTipText = "Premere per selezionare il file di import";
    , HelpContextID = 159847978

  add object oStr_2_1 as StdString with uid="FADOMCLTHJ",Visible=.t., Left=7, Top=16,;
    Alignment=0, Width=73, Height=18,;
    Caption="Import"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_4 as StdString with uid="FDVKSJRHDO",Visible=.t., Left=6, Top=55,;
    Alignment=1, Width=38, Height=18,;
    Caption="File:"  ;
  , bGlobalFont=.t.

  add object oBox_2_2 as StdBox with uid="LPXXTEQMSR",left=5, top=32, width=646,height=2
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsag_mpi",lower(this.oContained.GSAG_MPI.class))=0
        this.oContained.GSAG_MPI.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsag_apePag3 as StdContainer
  Width  = 706
  height = 497
  stdWidth  = 706
  stdheight = 497
  resizeXpos=505
  resizeYpos=147
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_1 as stdDynamicChildContainer with uid="ONRDELULPV",left=15, top=18, width=619, height=397, bOnScreen=.t.;

  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsag_mpe",lower(this.oContained.GSAG_MPE.class))=0
        this.oContained.GSAG_MPE.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_ape','PRO_EXPO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRSERIAL=PRO_EXPO.PRSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
