* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_sev                                                        *
*              Stampa eventi                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-08-01                                                      *
* Last revis.: 2014-02-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsfa_sev",oParentObject))

* --- Class definition
define class tgsfa_sev as StdForm
  Top    = 20
  Left   = 31

  * --- Standard Properties
  Width  = 543
  Height = 355
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-18"
  HelpContextID=57311849
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=33

  * --- Constant Properties
  _IDX = 0
  TIPEVENT_IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  OFF_NOMI_IDX = 0
  ANEVENTI_IDX = 0
  cPrg = "gsfa_sev"
  cComment = "Stampa eventi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPEVE = space(1)
  w_TIPDIR = space(1)
  w_DATINI = ctot('')
  w_DATFIN = ctot('')
  w_TIPOEVE = space(10)
  w_DESCRI = space(50)
  w_EV__ANNO = space(4)
  w_PERSON = space(5)
  o_PERSON = space(5)
  w_TIPPER = space(10)
  w_TIPGRU = space(10)
  w_COGNOM = space(50)
  w_NOME = space(50)
  w_GRUPPO = space(5)
  w_DESGRU = space(60)
  w_STATO = space(1)
  w_DETTATT = space(1)
  w_CODNOM = space(15)
  o_CODNOM = space(15)
  w_ATCODNOM = space(15)
  o_ATCODNOM = space(15)
  w_CODPRA = space(15)
  w_IDRICH = space(15)
  o_IDRICH = space(15)
  w_DESCAN = space(100)
  w_OBTEST = ctod('  /  /  ')
  w_COMODO = space(30)
  w_DESNOM = space(40)
  w_TIPNOM = space(1)
  w_OFDATDOC = ctod('  /  /  ')
  w_ATEVANNO = space(4)
  w_DESPER = space(100)
  w_ATSEREVE = space(15)
  w_STARIC = space(1)
  w_EVIDRICH = space(10)
  w_EVSTARIC = space(1)
  w_TIPRIC = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsfa_sevPag1","gsfa_sev",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPEVE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='TIPEVENT'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='OFF_NOMI'
    this.cWorkTables[5]='ANEVENTI'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPEVE=space(1)
      .w_TIPDIR=space(1)
      .w_DATINI=ctot("")
      .w_DATFIN=ctot("")
      .w_TIPOEVE=space(10)
      .w_DESCRI=space(50)
      .w_EV__ANNO=space(4)
      .w_PERSON=space(5)
      .w_TIPPER=space(10)
      .w_TIPGRU=space(10)
      .w_COGNOM=space(50)
      .w_NOME=space(50)
      .w_GRUPPO=space(5)
      .w_DESGRU=space(60)
      .w_STATO=space(1)
      .w_DETTATT=space(1)
      .w_CODNOM=space(15)
      .w_ATCODNOM=space(15)
      .w_CODPRA=space(15)
      .w_IDRICH=space(15)
      .w_DESCAN=space(100)
      .w_OBTEST=ctod("  /  /  ")
      .w_COMODO=space(30)
      .w_DESNOM=space(40)
      .w_TIPNOM=space(1)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_ATEVANNO=space(4)
      .w_DESPER=space(100)
      .w_ATSEREVE=space(15)
      .w_STARIC=space(1)
      .w_EVIDRICH=space(10)
      .w_EVSTARIC=space(1)
      .w_TIPRIC=space(1)
        .w_TIPEVE = ' '
        .w_TIPDIR = ' '
        .DoRTCalc(3,5,.f.)
        if not(empty(.w_TIPOEVE))
          .link_1_10('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_EV__ANNO = CALCESER(i_DATSYS)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_PERSON))
          .link_1_14('Full')
        endif
        .w_TIPPER = 'P'
        .w_TIPGRU = 'G'
        .DoRTCalc(11,13,.f.)
        if not(empty(.w_GRUPPO))
          .link_1_20('Full')
        endif
          .DoRTCalc(14,14,.f.)
        .w_STATO = ' '
          .DoRTCalc(16,16,.f.)
        .w_CODNOM = .w_ATCODNOM
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODNOM))
          .link_1_26('Full')
        endif
        .w_ATCODNOM = .w_CODNOM
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODPRA))
          .link_1_28('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
          .DoRTCalc(20,21,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(23,25,.f.)
        .w_OFDATDOC = i_datsys
        .w_ATEVANNO = .w_EV__ANNO
        .w_DESPER = Alltrim(.w_NOME)+'  ' + Alltrim(.w_COGNOM) 
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_ATSEREVE))
          .link_1_43('Full')
        endif
          .DoRTCalc(30,32,.f.)
        .w_TIPRIC = 'T'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
            .w_EV__ANNO = CALCESER(i_DATSYS)
        .DoRTCalc(8,8,.t.)
            .w_TIPPER = 'P'
            .w_TIPGRU = 'G'
        .DoRTCalc(11,16,.t.)
        if .o_ATCODNOM<>.w_ATCODNOM
            .w_CODNOM = .w_ATCODNOM
          .link_1_26('Full')
        endif
        if .o_CODNOM<>.w_CODNOM
            .w_ATCODNOM = .w_CODNOM
        endif
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .DoRTCalc(19,27,.t.)
        if .o_PERSON<>.w_PERSON
            .w_DESPER = Alltrim(.w_NOME)+'  ' + Alltrim(.w_COGNOM) 
        endif
        .DoRTCalc(29,32,.t.)
        if .o_IDRICH<>.w_IDRICH
            .w_TIPRIC = 'T'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
    endwith
  return

  proc Calculate_XUGSXMGFPK()
    with this
          * --- Azzero ID al cambiare del nominativi
          .w_IDRICH = Space(10)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTIPRIC_1_51.enabled = this.oPgFrm.Page1.oPag.oTIPRIC_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODPRA_1_28.visible=!this.oPgFrm.Page1.oPag.oCODPRA_1_28.mHide()
    this.oPgFrm.Page1.oPag.oIDRICH_1_29.visible=!this.oPgFrm.Page1.oPag.oIDRICH_1_29.mHide()
    this.oPgFrm.Page1.oPag.oDESCAN_1_30.visible=!this.oPgFrm.Page1.oPag.oDESCAN_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oATSEREVE_1_43.visible=!this.oPgFrm.Page1.oPag.oATSEREVE_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_45.visible=!this.oPgFrm.Page1.oPag.oBtn_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oTIPRIC_1_51.visible=!this.oPgFrm.Page1.oPag.oTIPRIC_1_51.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
        if lower(cEvent)==lower("w_CODNOM Changed")
          .Calculate_XUGSXMGFPK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPOEVE
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOEVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSFA_ATE',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_TIPOEVE)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_TIPOEVE))
          select TETIPEVE,TEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOEVE)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStrODBC(trim(this.w_TIPOEVE)+"%");

            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStr(trim(this.w_TIPOEVE)+"%");

            select TETIPEVE,TEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TIPOEVE) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oTIPOEVE_1_10'),i_cWhere,'GSFA_ATE',"Tipi eventi",'GSFARKCE.TIPEVENT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOEVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_TIPOEVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_TIPOEVE)
            select TETIPEVE,TEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOEVE = NVL(_Link_.TETIPEVE,space(10))
      this.w_DESCRI = NVL(_Link_.TEDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOEVE = space(10)
      endif
      this.w_DESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOEVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERSON
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERSON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_PERSON)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TIPPER;
                     ,'DPCODICE',trim(this.w_PERSON))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERSON)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PERSON) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oPERSON_1_14'),i_cWhere,'',"Persone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPPER<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERSON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_PERSON);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TIPPER;
                       ,'DPCODICE',this.w_PERSON)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERSON = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOM = NVL(_Link_.DPCOGNOM,space(50))
      this.w_NOME = NVL(_Link_.DPNOME,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_PERSON = space(5)
      endif
      this.w_COGNOM = space(50)
      this.w_NOME = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERSON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPPO
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPPO)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPGRU);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TIPGRU;
                     ,'DPCODICE',trim(this.w_GRUPPO))
          select DPTIPRIS,DPCODICE,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPPO)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_GRUPPO)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPGRU);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_GRUPPO)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TIPGRU);

            select DPTIPRIS,DPCODICE,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GRUPPO) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oGRUPPO_1_20'),i_cWhere,'',"Gruppi",'GSARGADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPGRU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPGRU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPPO);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TIPGRU;
                       ,'DPCODICE',this.w_GRUPPO)
            select DPTIPRIS,DPCODICE,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPPO = NVL(_Link_.DPCODICE,space(5))
      this.w_DESGRU = NVL(_Link_.DPDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPPO = space(5)
      endif
      this.w_DESGRU = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_PERSON , .w_GRUPPO) OR Empty(.w_PERSON))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
        endif
        this.w_GRUPPO = space(5)
        this.w_DESGRU = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_1_26'),i_cWhere,'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(30))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_DESNOM = space(40)
      this.w_COMODO = space(30)
      this.w_TIPNOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM = space(15)
        this.w_DESNOM = space(40)
        this.w_COMODO = space(30)
        this.w_TIPNOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_bzz',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRA))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRA_1_28'),i_cWhere,'gsar_bzz',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESCAN = space(100)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATSEREVE
  func Link_1_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_lTable = "ANEVENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2], .t., this.ANEVENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATSEREVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ANEVENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EVSERIAL like "+cp_ToStrODBC(trim(this.w_ATSEREVE)+"%");
                   +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);

          i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EV__ANNO,EVSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EV__ANNO',this.w_ATEVANNO;
                     ,'EVSERIAL',trim(this.w_ATSEREVE))
          select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EV__ANNO,EVSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATSEREVE)==trim(_Link_.EVSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATSEREVE) and !this.bDontReportError
            deferred_cp_zoom('ANEVENTI','*','EV__ANNO,EVSERIAL',cp_AbsName(oSource.parent,'oATSEREVE_1_43'),i_cWhere,'',"Elenco ID richieste",'GSFA_ZID.ANEVENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATEVANNO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC";
                     +" from "+i_cTable+" "+i_lTable+" where EVSERIAL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EV__ANNO',oSource.xKey(1);
                       ,'EVSERIAL',oSource.xKey(2))
            select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATSEREVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC";
                   +" from "+i_cTable+" "+i_lTable+" where EVSERIAL="+cp_ToStrODBC(this.w_ATSEREVE);
                   +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EV__ANNO',this.w_ATEVANNO;
                       ,'EVSERIAL',this.w_ATSEREVE)
            select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATSEREVE = NVL(_Link_.EVSERIAL,space(15))
      this.w_IDRICH = NVL(_Link_.EVIDRICH,space(15))
      this.w_STARIC = NVL(_Link_.EVSTARIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATSEREVE = space(15)
      endif
      this.w_IDRICH = space(15)
      this.w_STARIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])+'\'+cp_ToStr(_Link_.EV__ANNO,1)+'\'+cp_ToStr(_Link_.EVSERIAL,1)
      cp_ShowWarn(i_cKey,this.ANEVENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATSEREVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPEVE_1_2.RadioValue()==this.w_TIPEVE)
      this.oPgFrm.Page1.oPag.oTIPEVE_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDIR_1_3.RadioValue()==this.w_TIPDIR)
      this.oPgFrm.Page1.oPag.oTIPDIR_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_4.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_4.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_5.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_5.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOEVE_1_10.value==this.w_TIPOEVE)
      this.oPgFrm.Page1.oPag.oTIPOEVE_1_10.value=this.w_TIPOEVE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_11.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_11.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPERSON_1_14.value==this.w_PERSON)
      this.oPgFrm.Page1.oPag.oPERSON_1_14.value=this.w_PERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUPPO_1_20.value==this.w_GRUPPO)
      this.oPgFrm.Page1.oPag.oGRUPPO_1_20.value=this.w_GRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_22.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_22.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_23.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDETTATT_1_24.RadioValue()==this.w_DETTATT)
      this.oPgFrm.Page1.oPag.oDETTATT_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNOM_1_26.value==this.w_CODNOM)
      this.oPgFrm.Page1.oPag.oCODNOM_1_26.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRA_1_28.value==this.w_CODPRA)
      this.oPgFrm.Page1.oPag.oCODPRA_1_28.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oIDRICH_1_29.value==this.w_IDRICH)
      this.oPgFrm.Page1.oPag.oIDRICH_1_29.value=this.w_IDRICH
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_30.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_30.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_38.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_38.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPER_1_42.value==this.w_DESPER)
      this.oPgFrm.Page1.oPag.oDESPER_1_42.value=this.w_DESPER
    endif
    if not(this.oPgFrm.Page1.oPag.oATSEREVE_1_43.value==this.w_ATSEREVE)
      this.oPgFrm.Page1.oPag.oATSEREVE_1_43.value=this.w_ATSEREVE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPRIC_1_51.RadioValue()==this.w_TIPRIC)
      this.oPgFrm.Page1.oPag.oTIPRIC_1_51.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_PERSON , .w_GRUPPO) OR Empty(.w_PERSON)))  and not(empty(.w_GRUPPO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUPPO_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
          case   not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and not(empty(.w_CODNOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODNOM_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PERSON = this.w_PERSON
    this.o_CODNOM = this.w_CODNOM
    this.o_ATCODNOM = this.w_ATCODNOM
    this.o_IDRICH = this.w_IDRICH
    return

enddefine

* --- Define pages as container
define class tgsfa_sevPag1 as StdContainer
  Width  = 539
  height = 355
  stdWidth  = 539
  stdheight = 355
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPEVE_1_2 as StdCombo with uid="PJHRCAEYRX",value=6,rtseq=1,rtrep=.f.,left=80,top=18,width=125,height=21;
    , ToolTipText = "Selezionare la tipologia";
    , HelpContextID = 121621558;
    , cFormVar="w_TIPEVE",RowSource=""+"Mail,"+"Telefonata,"+"Fax,"+"Documento,"+"Altro,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPEVE_1_2.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    iif(this.value =3,'F',;
    iif(this.value =4,'D',;
    iif(this.value =5,'A',;
    iif(this.value =6,' ',;
    space(1))))))))
  endfunc
  func oTIPEVE_1_2.GetRadio()
    this.Parent.oContained.w_TIPEVE = this.RadioValue()
    return .t.
  endfunc

  func oTIPEVE_1_2.SetRadio()
    this.Parent.oContained.w_TIPEVE=trim(this.Parent.oContained.w_TIPEVE)
    this.value = ;
      iif(this.Parent.oContained.w_TIPEVE=='M',1,;
      iif(this.Parent.oContained.w_TIPEVE=='T',2,;
      iif(this.Parent.oContained.w_TIPEVE=='F',3,;
      iif(this.Parent.oContained.w_TIPEVE=='D',4,;
      iif(this.Parent.oContained.w_TIPEVE=='A',5,;
      iif(this.Parent.oContained.w_TIPEVE=='',6,;
      0))))))
  endfunc


  add object oTIPDIR_1_3 as StdCombo with uid="TIMOMOMHKI",value=3,rtseq=2,rtrep=.f.,left=318,top=18,width=125,height=21;
    , ToolTipText = "Direzione";
    , HelpContextID = 57592886;
    , cFormVar="w_TIPDIR",RowSource=""+"Entrata,"+"Uscita,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDIR_1_3.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'U',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oTIPDIR_1_3.GetRadio()
    this.Parent.oContained.w_TIPDIR = this.RadioValue()
    return .t.
  endfunc

  func oTIPDIR_1_3.SetRadio()
    this.Parent.oContained.w_TIPDIR=trim(this.Parent.oContained.w_TIPDIR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDIR=='E',1,;
      iif(this.Parent.oContained.w_TIPDIR=='U',2,;
      iif(this.Parent.oContained.w_TIPDIR=='',3,;
      0)))
  endfunc

  add object oDATINI_1_4 as StdField with uid="BHGMAMATJT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 180618038,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=80, Top=42

  add object oDATFIN_1_5 as StdField with uid="LGLFYXSTYB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 259064630,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=318, Top=42

  add object oTIPOEVE_1_10 as StdField with uid="BTJFSOBDRR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TIPOEVE", cQueryName = "TIPOEVE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipo evento",;
    HelpContextID = 121228342,;
   bGlobalFont=.t.,;
    Height=21, Width=89, Left=80, Top=68, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", cZoomOnZoom="GSFA_ATE", oKey_1_1="TETIPEVE", oKey_1_2="this.w_TIPOEVE"

  func oTIPOEVE_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPOEVE_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOEVE_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oTIPOEVE_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSFA_ATE',"Tipi eventi",'GSFARKCE.TIPEVENT_VZM',this.parent.oContained
  endproc
  proc oTIPOEVE_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSFA_ATE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TETIPEVE=this.parent.oContained.w_TIPOEVE
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_11 as StdField with uid="XPGCZTQBXN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 184416054,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=175, Top=68, InputMask=replicate('X',50)

  add object oPERSON_1_14 as StdField with uid="HINYRZARGF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PERSON", cQueryName = "PERSON",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona",;
    HelpContextID = 266201078,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=80, Top=95, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TIPPER", oKey_2_1="DPCODICE", oKey_2_2="this.w_PERSON"

  func oPERSON_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERSON_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERSON_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TIPPER)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TIPPER)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oPERSON_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Persone",'',this.parent.oContained
  endproc

  add object oGRUPPO_1_20 as StdField with uid="LVTOQYBRVV",rtseq=13,rtrep=.f.,;
    cFormVar = "w_GRUPPO", cQueryName = "GRUPPO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, persona non appartenente al gruppo selezionato",;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 15410278,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=80, Top=123, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TIPGRU", oKey_2_1="DPCODICE", oKey_2_2="this.w_GRUPPO"

  func oGRUPPO_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPPO_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPPO_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TIPGRU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TIPGRU)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oGRUPPO_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi",'GSARGADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oDESGRU_1_22 as StdField with uid="MADNUSHMHU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 117569334,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=142, Top=122, InputMask=replicate('X',60)


  add object oSTATO_1_23 as StdCombo with uid="CVQETAOZUY",value=3,rtseq=15,rtrep=.f.,left=80,top=152,width=131,height=21;
    , ToolTipText = "Stato evento";
    , HelpContextID = 237115610;
    , cFormVar="w_STATO",RowSource=""+"Processati,"+"Da processare,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_23.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oSTATO_1_23.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_23.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='P',1,;
      iif(this.Parent.oContained.w_STATO=='D',2,;
      iif(this.Parent.oContained.w_STATO=='',3,;
      0)))
  endfunc

  add object oDETTATT_1_24 as StdCheck with uid="MBLVZOOETX",rtseq=16,rtrep=.f.,left=230, top=152, caption="Dettaglio attivit�",;
    ToolTipText = "Se attivo, viene riportato in stampa il dettaglio attivit�",;
    HelpContextID = 83822390,;
    cFormVar="w_DETTATT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDETTATT_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDETTATT_1_24.GetRadio()
    this.Parent.oContained.w_DETTATT = this.RadioValue()
    return .t.
  endfunc

  func oDETTATT_1_24.SetRadio()
    this.Parent.oContained.w_DETTATT=trim(this.Parent.oContained.w_DETTATT)
    this.value = ;
      iif(this.Parent.oContained.w_DETTATT=='S',1,;
      0)
  endfunc

  add object oCODNOM_1_26 as StdField with uid="LDORSNJAGM",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 249041190,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=80, Top=181, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc

  add object oCODPRA_1_28 as StdField with uid="VWKQXIMWIB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice pratica",;
    HelpContextID = 50991398,;
   bGlobalFont=.t.,;
    Height=21, Width=131, Left=80, Top=210, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="gsar_bzz", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_1_28.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc

  func oCODPRA_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRA_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRA_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRA_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_bzz',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODPRA_1_28.mZoomOnZoom
    local i_obj
    i_obj=gsar_bzz()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRA
     i_obj.ecpSave()
  endproc

  add object oIDRICH_1_29 as StdField with uid="LHDEPJGQVJ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_IDRICH", cQueryName = "IDRICH",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "ID richiesta",;
    HelpContextID = 152299142,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=80, Top=211, InputMask=replicate('X',15)

  func oIDRICH_1_29.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  proc oIDRICH_1_29.mAfter
      with this.Parent.oContained
        gsag_ble(this.Parent.oContained,.w_IDRICH,".w_ATSEREVE",This.Parent.oContained,".w_CODNOM")
      endwith
  endproc

  add object oDESCAN_1_30 as StdField with uid="FMMTXBMYYQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 250476342,;
   bGlobalFont=.t.,;
    Height=21, Width=311, Left=216, Top=210, InputMask=replicate('X',100)

  func oDESCAN_1_30.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc


  add object oObj_1_32 as cp_outputCombo with uid="ZLERXIVPWT",left=134, top=267, width=393,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 120685798


  add object oBtn_1_33 as StdButton with uid="FFZUPFQMOE",left=428, top=301, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 84477734;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="DIOMPKAHZX",left=479, top=301, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 49994426;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESNOM_1_38 as StdField with uid="YBKTMCLSXL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 249100086,;
   bGlobalFont=.t.,;
    Height=21, Width=311, Left=216, Top=181, InputMask=replicate('X',40)

  add object oDESPER_1_42 as StdField with uid="PUXSCXRYTX",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESPER", cQueryName = "DESPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 54196022,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=142, Top=95, InputMask=replicate('X',100)

  add object oATSEREVE_1_43 as StdField with uid="UCMJRBTPAW",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ATSEREVE", cQueryName = "ATSEREVE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 117442123,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=36, Top=320, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ANEVENTI", oKey_1_1="EV__ANNO", oKey_1_2="this.w_ATEVANNO", oKey_2_1="EVSERIAL", oKey_2_2="this.w_ATSEREVE"

  func oATSEREVE_1_43.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  func oATSEREVE_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oATSEREVE_1_43.ecpDrop(oSource)
    this.Parent.oContained.link_1_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATSEREVE_1_43.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ANEVENTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"EV__ANNO="+cp_ToStrODBC(this.Parent.oContained.w_ATEVANNO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"EV__ANNO="+cp_ToStr(this.Parent.oContained.w_ATEVANNO)
    endif
    do cp_zoom with 'ANEVENTI','*','EV__ANNO,EVSERIAL',cp_AbsName(this.parent,'oATSEREVE_1_43'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco ID richieste",'GSFA_ZID.ANEVENTI_VZM',this.parent.oContained
  endproc


  add object oBtn_1_45 as StdButton with uid="JMPPTWBRZQ",left=199, top=211, width=20,height=22,;
    caption="?", nPag=1;
    , ToolTipText = "Premere per selezionare ID richiesta";
    , HelpContextID = 57310746;
  , bGlobalFont=.t.

    proc oBtn_1_45.Click()
      do GSFA_KID with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_45.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Isalt())
     endwith
    endif
  endfunc


  add object oTIPRIC_1_51 as StdCombo with uid="VQXDZGGOTS",rtseq=33,rtrep=.f.,left=298,top=211,width=93,height=21;
    , HelpContextID = 75287606;
    , cFormVar="w_TIPRIC",RowSource=""+"Da chiudere,"+"Aperta,"+"In corso,"+"In attesa,"+"Chiusa,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPRIC_1_51.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'A',;
    iif(this.value =3,'I',;
    iif(this.value =4,'S',;
    iif(this.value =5,'C',;
    iif(this.value =6,'T',;
    space(1))))))))
  endfunc
  func oTIPRIC_1_51.GetRadio()
    this.Parent.oContained.w_TIPRIC = this.RadioValue()
    return .t.
  endfunc

  func oTIPRIC_1_51.SetRadio()
    this.Parent.oContained.w_TIPRIC=trim(this.Parent.oContained.w_TIPRIC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPRIC=='D',1,;
      iif(this.Parent.oContained.w_TIPRIC=='A',2,;
      iif(this.Parent.oContained.w_TIPRIC=='I',3,;
      iif(this.Parent.oContained.w_TIPRIC=='S',4,;
      iif(this.Parent.oContained.w_TIPRIC=='C',5,;
      iif(this.Parent.oContained.w_TIPRIC=='T',6,;
      0))))))
  endfunc

  func oTIPRIC_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_IDRICH))
    endwith
   endif
  endfunc

  func oTIPRIC_1_51.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_1 as StdString with uid="HVQDALZAWB",Visible=.t., Left=31, Top=268,;
    Alignment=1, Width=99, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="FEMZHDXVBH",Visible=.t., Left=29, Top=44,;
    Alignment=1, Width=46, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="TYGTAHTLMH",Visible=.t., Left=268, Top=44,;
    Alignment=1, Width=46, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="YMFIMMNSEN",Visible=.t., Left=21, Top=17,;
    Alignment=1, Width=54, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ROPFPIJMPZ",Visible=.t., Left=259, Top=17,;
    Alignment=1, Width=55, Height=18,;
    Caption="Direzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="IDRHUYXQWK",Visible=.t., Left=21, Top=69,;
    Alignment=1, Width=54, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="ZSHLOBBLVJ",Visible=.t., Left=25, Top=96,;
    Alignment=1, Width=50, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="YUSWUTADFD",Visible=.t., Left=25, Top=124,;
    Alignment=1, Width=50, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="NZZBBATOGP",Visible=.t., Left=25, Top=152,;
    Alignment=1, Width=50, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="CKNKHWDGGA",Visible=.t., Left=34, Top=212,;
    Alignment=1, Width=41, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="FAOUGAALLE",Visible=.t., Left=10, Top=183,;
    Alignment=1, Width=65, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="CZXHGRLBZS",Visible=.t., Left=3, Top=212,;
    Alignment=1, Width=72, Height=18,;
    Caption="ID richiesta:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="ROKAGEDAUD",Visible=.t., Left=224, Top=213,;
    Alignment=1, Width=73, Height=18,;
    Caption="Stato  rich.:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_sev','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
