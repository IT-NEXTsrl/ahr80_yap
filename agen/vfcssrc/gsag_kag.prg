* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kag                                                        *
*              La mia agenda                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-02-14                                                      *
* Last revis.: 2015-03-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsag_kag
* Controllo presenza delle ocx
IF ProvaOcx('cp_ctMDay')
   Ah_ErrorMsg("Errore nella gestione di ctMDay.OCX")
   RETURN
ENDIF
IF ProvaOcx('cp_ctCalendar')
   Ah_ErrorMsg("Errore nella gestione di ctCalendar.OCX")
   RETURN
ENDIF
IF ProvaOcx('cp_ctDate')
   Ah_ErrorMsg("Errore nella gestione di ctDate.OCX")
   RETURN
ENDIF
IF ProvaOcx('cp_ctDropDate')
   Ah_ErrorMsg("Errore nella gestione di ctDropDate.OCX")
   RETURN
ENDIF
IF ProvaOcx('cp_ctList')
   Ah_ErrorMsg("Errore nella gestione di ctList.OCX")
   RETURN
ENDIF
IF ProvaOcx('cp_ctMonth')
   Ah_ErrorMsg("Errore nella gestione di ctMonth.OCX")
   RETURN
ENDIF
IF ProvaOcx('cp_ctYear')
   Ah_ErrorMsg("Errore nella gestione di ctYear.OCX")
   RETURN
ENDIF
* --- Fine Area Manuale
return(createobject("tgsag_kag",oParentObject))

* --- Class definition
define class tgsag_kag as StdForm
  Top    = 1
  Left   = 0

  * --- Standard Properties
  Width  = 813
  Height = 612+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-18"
  HelpContextID=132786025
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=136

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  PRA_ENTI_IDX = 0
  PRA_UFFI_IDX = 0
  CAN_TIER_IDX = 0
  CPUSERS_IDX = 0
  OFF_NOMI_IDX = 0
  CAT_SOGG_IDX = 0
  OFFTIPAT_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  CENCOST_IDX = 0
  DES_DIVE_IDX = 0
  TIP_RISE_IDX = 0
  PAR_AGEN_IDX = 0
  CAUMATTI_IDX = 0
  cPrg = "gsag_kag"
  cComment = "La mia agenda"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_Parametro = space(1)
  w_Serial = space(20)
  w_datasel = ctod('  /  /  ')
  w_OraStart = 0
  w_OraEnd = 0
  w_TxtSel = space(154)
  w_DataApp = ctod('  /  /  ')
  w_ClnBMP = 0
  w_ClnCheck = 0
  w_ClnData = 0
  w_ClnOgg = 0
  w_DatiAttivita = space(0)
  w_DatiPratica = space(0)
  w_DatiAttiPratica = space(0)
  w_ClnOgg_Gio = 0
  w_ClnDataIni_Gio = 0
  w_ClnDataFin_Gio = 0
  w_ItemSel = 0
  w_DATA_INI = ctot('')
  w_DATA_FIN = ctot('')
  w_ClnCheck_Gio = 0
  w_ClnSel = 0
  w_ClnGiorno = 0
  w_ClnComp = 0
  w_ClnComp_Gio = 0
  w_ClnGiornoDa_Gio = 0
  w_ClnGiornoA_Gio = 0
  w_CauDefault = space(20)
  w_AskCau = 0
  w_VARLOG = .F.
  w_calAttivo = 0
  w_PATIPRIS = space(1)
  o_PATIPRIS = space(1)
  w_CODPART = space(5)
  o_CODPART = space(5)
  w_STATO = space(1)
  w_RISERVE = space(1)
  w_DENOM_PART = space(48)
  w_FLESGR = space(1)
  w_GRUPART = space(5)
  w_PRIORITA = 0
  w_TIPORIS = space(1)
  w_CACODICE = space(20)
  w_CADESCRI = space(254)
  w_ATTSTU = space(1)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_CODNOM = space(15)
  o_CODNOM = space(15)
  w_CODCAT = space(10)
  w_ATTGENER = space(1)
  w_UDIENZE = space(1)
  w_SESSTEL = space(1)
  w_ASSENZE = space(1)
  w_DAINSPRE = space(1)
  w_MEMO = space(1)
  w_DAFARE = space(1)
  w_FiltroWeb = space(1)
  o_FiltroWeb = space(1)
  w_DISPONIB = 0
  w_FiltPerson = space(60)
  w_CODTIPOL = space(5)
  w_NOTE = space(0)
  w_GIUDICE = space(15)
  w_ATLOCALI = space(30)
  w_ENTE = space(10)
  w_IMPIANTO = space(10)
  o_IMPIANTO = space(10)
  w_DesEnte = space(60)
  w_COMPIMP = space(50)
  o_COMPIMP = space(50)
  w_UFFICIO = space(10)
  w_CENCOS = space(15)
  w_CODPRA = space(15)
  w_CodUte = 0
  w_DESPRA = space(75)
  w_DesUffi = space(60)
  w_DESUTE = space(20)
  w_DATINI_Cose = ctod('  /  /  ')
  o_DATINI_Cose = ctod('  /  /  ')
  w_DATFIN_Cose = ctod('  /  /  ')
  o_DATFIN_Cose = ctod('  /  /  ')
  w_VisNote = space(1)
  o_VisNote = space(1)
  w_StrPratica = space(1)
  w_DESNOM = space(40)
  w_DESTIPOL = space(50)
  w_DPDESGRU = space(40)
  w_DESPIA = space(40)
  w_CODSED = space(5)
  o_CODSED = space(5)
  w_DESCRI = space(40)
  w_TIPANA = space(1)
  w_DESNOMGIU = space(60)
  w_FiltPubWeb = space(1)
  w_COMODOGIU = space(60)
  w_INDNOM = space(35)
  w_CAPNOM = space(9)
  w_DATOBSONOM = ctod('  /  /  ')
  w_TGIUDICE = space(15)
  w_FLVISI = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_COMPIMPKEYREAD = 0
  w_COMPIMPKEY = 0
  o_COMPIMPKEY = 0
  w_ATCODSED = space(5)
  w_CODCLI = space(15)
  w_TIPOGRUP = space(1)
  w_DESCRPART = space(40)
  w_OFDATDOC = ctod('  /  /  ')
  w_TIPCAT = space(1)
  w_DESCAT = space(30)
  w_TIPNOM = space(1)
  w_COMODO = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_CACODIMP = space(10)
  w_OLDCOMP = space(10)
  w_ATTFUOSTU = space(1)
  w_ATCODNOM = space(15)
  w_NOMEPART = space(40)
  w_COGNPART = space(40)
  w_COD_UTE = space(5)
  w_FLTPART = space(5)
  o_FLTPART = space(5)
  w_READAZI = space(5)
  o_READAZI = space(5)
  w_EDITCODPRA = .F.
  w_DPTIPRIS = space(1)
  w_PATIPRIS = space(1)
  w_CODPART = space(5)
  w_DENOM_PART = space(48)
  w_GRUPART = space(5)
  w_FLESGR = space(1)
  w_DPDESGRU = space(40)
  w_CURPART = space(10)
  w_ATAPPOVR = space(10)
  w_OREFCALE = .F.
  w_FLCALEXP = .F.
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_OREINI_Cose = space(2)
  o_OREINI_Cose = space(2)
  w_MININI_Cose = space(2)
  o_MININI_Cose = space(2)
  w_OREFIN_Cose = space(2)
  o_OREFIN_Cose = space(2)
  w_MINFIN_Cose = space(2)
  o_MINFIN_Cose = space(2)
  w_DATA_INI_Cose = ctot('')
  w_DATA_FIN_Cose = ctot('')
  w_IMDESCRI = space(50)
  w_DATINIZ = ctod('  /  /  ')
  o_DATINIZ = ctod('  /  /  ')
  w_obsodat1 = space(1)
  o_obsodat1 = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_ctMonth = .NULL.
  w_ctYear = .NULL.
  w_ctCalendar = .NULL.
  w_ctDate = .NULL.
  w_ct5Day = .NULL.
  w_ct1Day = .NULL.
  w_ctList_Memo = .NULL.
  w_ctList_Giorno = .NULL.
  w_ctDropDate = .NULL.
  w_PART_ZOOM = .NULL.
  w_oTmrBalloon = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_kag
  w_BlankEseguita=.F.
  w_ImgNote=0
  w_ImgDaFare=0
  w_ImgPreav=0
  
  proc ecpDelete()
     this.NotifyEvent("CancellaApp")
  ENDPROC
  
  proc ecpQuit()
     UNBINDEVENT(this.opgfrm.page1.opAG.ct1DAY.cal  ,"MouseMove", this.parent, "ct1DayMouseMove")
     UNBINDEVENT(this.opgfrm.page1.opAG.ct5DAY.cal  ,"MouseMove", this.parent, "ct5DayMouseMove")
     DoDefault()
  endproc
  
  proc Deactivate
    DoDefault()
    this.oBALLOON.CTLVISIBLE = .F.
    Raiseevent(This.oBALLOON, "ctlHide", 0)
  endproc
  
  PROCEDURE GenericCalendarMouseMove
    LPARAMETERS oCalendar, nXCoord, nYCoord
    LOCAL L_AppointNumber
    L_AppointNumber = oCalendar.AppointmentAt(nXCoord , nYCoord)
    IF L_AppointNumber > 0
      IF this.w_ATAPPOVR <> m.L_AppointNumber
        this.w_ATAPPOVR = m.L_AppointNumber
        this.w_OREFCALE = oCalendar
        this.w_oTmrBalloon.Tag=ALLTRIM(STR(nXCoord))+"#"+ALLTRIM(STR(nYCoord))
        this.w_oTmrBalloon.Interval=750
        this.w_oTmrBalloon.Enabled=.T.
        this.oBALLOON.CTLVISIBLE = .F.
      ENDIF
    ELSE
      this.w_oTmrBalloon.Enabled=.F.
      this.w_oTmrBalloon.Tag=''
      this.w_ATAPPOVR = -1
      this.w_OREFCALE = .NULL.
      this.oBALLOON.CTLVISIBLE = .F.
      Raiseevent(This.oBALLOON, "ctlHide", 0)
    ENDIF
  ENDPROC
  
  PROCEDURE ct1DayMouseMove
  	LPARAMETERS nButton, nShift, nXCoord, nYCoord
  	IF nButton=0 AND nShift=0
      this.GenericCalendarMouseMove(this.w_ct1Day.CAL, nXCoord , nYCoord)
  	ENDIF
  ENDPROC
  
  PROCEDURE ct5DayMouseMove
  	LPARAMETERS nButton, nShift, nXCoord, nYCoord
  	IF nButton=0 AND nShift=0
      this.GenericCalendarMouseMove(this.w_ct5Day.CAL, nXCoord , nYCoord)
  	ENDIF
  ENDPROC
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kagPag1","gsag_kag",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Agenda")
      .Pages(2).addobject("oPag","tgsag_kagPag2","gsag_kag",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(3).addobject("oPag","tgsag_kagPag3","gsag_kag",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Elenco partecipanti")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDatiAttivita_1_30
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsag_kag
    BINDEVENT(this.page1.opAG.ct1DAY.cal  ,"MouseMove", this.parent, "ct1DayMouseMove")
    BINDEVENT(this.page1.opAG.ct5DAY.cal  ,"MouseMove", this.parent, "ct5DayMouseMove")
    
    if not(vartype(i_cWarnType)='C' and i_cWarnType='B')
       this.Parent.AddObject("oBalloon","cp_balloontip")
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ctMonth = this.oPgFrm.Pages(1).oPag.ctMonth
    this.w_ctYear = this.oPgFrm.Pages(1).oPag.ctYear
    this.w_ctCalendar = this.oPgFrm.Pages(1).oPag.ctCalendar
    this.w_ctDate = this.oPgFrm.Pages(1).oPag.ctDate
    this.w_ct5Day = this.oPgFrm.Pages(1).oPag.ct5Day
    this.w_ct1Day = this.oPgFrm.Pages(1).oPag.ct1Day
    this.w_ctList_Memo = this.oPgFrm.Pages(1).oPag.ctList_Memo
    this.w_ctList_Giorno = this.oPgFrm.Pages(1).oPag.ctList_Giorno
    this.w_ctDropDate = this.oPgFrm.Pages(2).oPag.ctDropDate
    this.w_PART_ZOOM = this.oPgFrm.Pages(3).oPag.PART_ZOOM
    this.w_oTmrBalloon = this.oPgFrm.Pages(1).oPag.oTmrBalloon
    DoDefault()
    proc Destroy()
      this.w_ctMonth = .NULL.
      this.w_ctYear = .NULL.
      this.w_ctCalendar = .NULL.
      this.w_ctDate = .NULL.
      this.w_ct5Day = .NULL.
      this.w_ct1Day = .NULL.
      this.w_ctList_Memo = .NULL.
      this.w_ctList_Giorno = .NULL.
      this.w_ctDropDate = .NULL.
      this.w_PART_ZOOM = .NULL.
      this.w_oTmrBalloon = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[15]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='PRA_ENTI'
    this.cWorkTables[3]='PRA_UFFI'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='CPUSERS'
    this.cWorkTables[6]='OFF_NOMI'
    this.cWorkTables[7]='CAT_SOGG'
    this.cWorkTables[8]='OFFTIPAT'
    this.cWorkTables[9]='IMP_MAST'
    this.cWorkTables[10]='IMP_DETT'
    this.cWorkTables[11]='CENCOST'
    this.cWorkTables[12]='DES_DIVE'
    this.cWorkTables[13]='TIP_RISE'
    this.cWorkTables[14]='PAR_AGEN'
    this.cWorkTables[15]='CAUMATTI'
    return(this.OpenAllTables(15))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_Parametro=space(1)
      .w_Serial=space(20)
      .w_datasel=ctod("  /  /  ")
      .w_OraStart=0
      .w_OraEnd=0
      .w_TxtSel=space(154)
      .w_DataApp=ctod("  /  /  ")
      .w_ClnBMP=0
      .w_ClnCheck=0
      .w_ClnData=0
      .w_ClnOgg=0
      .w_DatiAttivita=space(0)
      .w_DatiPratica=space(0)
      .w_DatiAttiPratica=space(0)
      .w_ClnOgg_Gio=0
      .w_ClnDataIni_Gio=0
      .w_ClnDataFin_Gio=0
      .w_ItemSel=0
      .w_DATA_INI=ctot("")
      .w_DATA_FIN=ctot("")
      .w_ClnCheck_Gio=0
      .w_ClnSel=0
      .w_ClnGiorno=0
      .w_ClnComp=0
      .w_ClnComp_Gio=0
      .w_ClnGiornoDa_Gio=0
      .w_ClnGiornoA_Gio=0
      .w_CauDefault=space(20)
      .w_AskCau=0
      .w_VARLOG=.f.
      .w_calAttivo=0
      .w_PATIPRIS=space(1)
      .w_CODPART=space(5)
      .w_STATO=space(1)
      .w_RISERVE=space(1)
      .w_DENOM_PART=space(48)
      .w_FLESGR=space(1)
      .w_GRUPART=space(5)
      .w_PRIORITA=0
      .w_TIPORIS=space(1)
      .w_CACODICE=space(20)
      .w_CADESCRI=space(254)
      .w_ATTSTU=space(1)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_CODNOM=space(15)
      .w_CODCAT=space(10)
      .w_ATTGENER=space(1)
      .w_UDIENZE=space(1)
      .w_SESSTEL=space(1)
      .w_ASSENZE=space(1)
      .w_DAINSPRE=space(1)
      .w_MEMO=space(1)
      .w_DAFARE=space(1)
      .w_FiltroWeb=space(1)
      .w_DISPONIB=0
      .w_FiltPerson=space(60)
      .w_CODTIPOL=space(5)
      .w_NOTE=space(0)
      .w_GIUDICE=space(15)
      .w_ATLOCALI=space(30)
      .w_ENTE=space(10)
      .w_IMPIANTO=space(10)
      .w_DesEnte=space(60)
      .w_COMPIMP=space(50)
      .w_UFFICIO=space(10)
      .w_CENCOS=space(15)
      .w_CODPRA=space(15)
      .w_CodUte=0
      .w_DESPRA=space(75)
      .w_DesUffi=space(60)
      .w_DESUTE=space(20)
      .w_DATINI_Cose=ctod("  /  /  ")
      .w_DATFIN_Cose=ctod("  /  /  ")
      .w_VisNote=space(1)
      .w_StrPratica=space(1)
      .w_DESNOM=space(40)
      .w_DESTIPOL=space(50)
      .w_DPDESGRU=space(40)
      .w_DESPIA=space(40)
      .w_CODSED=space(5)
      .w_DESCRI=space(40)
      .w_TIPANA=space(1)
      .w_DESNOMGIU=space(60)
      .w_FiltPubWeb=space(1)
      .w_COMODOGIU=space(60)
      .w_INDNOM=space(35)
      .w_CAPNOM=space(9)
      .w_DATOBSONOM=ctod("  /  /  ")
      .w_TGIUDICE=space(15)
      .w_FLVISI=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_COMPIMPKEYREAD=0
      .w_COMPIMPKEY=0
      .w_ATCODSED=space(5)
      .w_CODCLI=space(15)
      .w_TIPOGRUP=space(1)
      .w_DESCRPART=space(40)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_TIPCAT=space(1)
      .w_DESCAT=space(30)
      .w_TIPNOM=space(1)
      .w_COMODO=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_CACODIMP=space(10)
      .w_OLDCOMP=space(10)
      .w_ATTFUOSTU=space(1)
      .w_ATCODNOM=space(15)
      .w_NOMEPART=space(40)
      .w_COGNPART=space(40)
      .w_COD_UTE=space(5)
      .w_FLTPART=space(5)
      .w_READAZI=space(5)
      .w_EDITCODPRA=.f.
      .w_DPTIPRIS=space(1)
      .w_PATIPRIS=space(1)
      .w_CODPART=space(5)
      .w_DENOM_PART=space(48)
      .w_GRUPART=space(5)
      .w_FLESGR=space(1)
      .w_DPDESGRU=space(40)
      .w_CURPART=space(10)
      .w_ATAPPOVR=space(10)
      .w_OREFCALE=.f.
      .w_FLCALEXP=.f.
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      .w_OREINI_Cose=space(2)
      .w_MININI_Cose=space(2)
      .w_OREFIN_Cose=space(2)
      .w_MINFIN_Cose=space(2)
      .w_DATA_INI_Cose=ctot("")
      .w_DATA_FIN_Cose=ctot("")
      .w_IMDESCRI=space(50)
      .w_DATINIZ=ctod("  /  /  ")
      .w_obsodat1=space(1)
      .w_DATOBSO=ctod("  /  /  ")
        .w_Parametro = IIF(VARTYPE(this.oParentObject)='C',this.oParentObject,'G')
          .DoRTCalc(2,2,.f.)
        .w_datasel = i_datsys
      .oPgFrm.Page1.oPag.ctMonth.Calculate()
      .oPgFrm.Page1.oPag.ctYear.Calculate()
      .oPgFrm.Page1.oPag.ctCalendar.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate(GSAG_BAG(this, 'DICITURA'))
      .oPgFrm.Page1.oPag.ctDate.Calculate()
      .oPgFrm.Page1.oPag.ct5Day.Calculate()
      .oPgFrm.Page1.oPag.ct1Day.Calculate()
      .oPgFrm.Page1.oPag.ctList_Memo.Calculate()
      .oPgFrm.Page1.oPag.ctList_Giorno.Calculate()
          .DoRTCalc(4,30,.f.)
        .w_calAttivo = g_AgeVis
        .w_PATIPRIS = IIF(.w_Parametro='P','P','')
        .w_CODPART = IIF(.w_Parametro='P',readdipend(i_CodUte, 'C'),'')
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_CODPART))
          .link_2_3('Full')
        endif
        .w_STATO = IIF(NOT ISALT(), 'N', InitStato())
        .w_RISERVE = 'L'
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        .w_FLESGR = 'N'
        .w_GRUPART = .w_GRUPART
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_GRUPART))
          .link_2_11('Full')
        endif
        .w_PRIORITA = 0
        .w_TIPORIS = SPACE(1)
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_TIPORIS))
          .link_2_13('Full')
        endif
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_CACODICE))
          .link_2_14('Full')
        endif
        .DoRTCalc(42,46,.f.)
        if not(empty(.w_CODNOM))
          .link_2_19('Full')
        endif
        .w_CODCAT = ''
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_CODCAT))
          .link_2_20('Full')
        endif
          .DoRTCalc(48,54,.f.)
        .w_FiltroWeb = 'T'
        .w_DISPONIB = 0
        .DoRTCalc(57,58,.f.)
        if not(empty(.w_CODTIPOL))
          .link_2_31('Full')
        endif
        .DoRTCalc(59,60,.f.)
        if not(empty(.w_GIUDICE))
          .link_2_33('Full')
        endif
        .DoRTCalc(61,62,.f.)
        if not(empty(.w_ENTE))
          .link_2_35('Full')
        endif
        .w_IMPIANTO = IIF(EMPTY(.w_CODSED), space(10), .w_IMPIANTO)
        .DoRTCalc(63,63,.f.)
        if not(empty(.w_IMPIANTO))
          .link_2_36('Full')
        endif
        .DoRTCalc(64,66,.f.)
        if not(empty(.w_UFFICIO))
          .link_2_39('Full')
        endif
        .DoRTCalc(67,67,.f.)
        if not(empty(.w_CENCOS))
          .link_2_41('Full')
        endif
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_CODPRA))
          .link_2_42('Full')
        endif
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_CodUte))
          .link_2_43('Full')
        endif
          .DoRTCalc(70,74,.f.)
        .w_VisNote = 'N'
        .w_StrPratica = 'N'
        .DoRTCalc(77,81,.f.)
        if not(empty(.w_CODSED))
          .link_2_70('Full')
        endif
      .oPgFrm.Page2.oPag.oObj_2_75.Calculate()
          .DoRTCalc(82,82,.f.)
        .w_TIPANA = 'C'
      .oPgFrm.Page2.oPag.oObj_2_81.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_69.Calculate()
          .DoRTCalc(84,84,.f.)
        .w_FiltPubWeb = IIF(.w_FiltroWeb='T','',.w_FiltroWeb)
          .DoRTCalc(86,91,.f.)
        .w_OB_TEST = i_INIDAT
        .w_COMPIMPKEYREAD = .w_COMPIMPKEY
        .DoRTCalc(93,93,.f.)
        if not(empty(.w_COMPIMPKEYREAD))
          .link_2_91('Full')
        endif
        .w_COMPIMPKEY = 0
        .w_ATCODSED = .w_CODSED
          .DoRTCalc(96,98,.f.)
        .w_OFDATDOC = i_datsys
      .oPgFrm.Page2.oPag.ctDropDate.Calculate()
          .DoRTCalc(100,103,.f.)
        .w_OBTEST = i_INIDAT
        .w_CACODIMP = .w_IMPIANTO
          .DoRTCalc(106,106,.f.)
        .w_ATTFUOSTU = ' '
        .w_ATCODNOM = .w_CODNOM
          .DoRTCalc(109,110,.f.)
        .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
        .w_FLTPART = readdipend( i_CodUte, 'C')
        .w_READAZI = i_CODAZI
        .DoRTCalc(113,113,.f.)
        if not(empty(.w_READAZI))
          .link_2_112('Full')
        endif
        .w_EDITCODPRA = .T.
          .DoRTCalc(115,115,.f.)
        .w_PATIPRIS = IIF(.w_Parametro='P','P','')
        .w_CODPART = IIF(.w_Parametro='P',readdipend(i_CodUte, 'C'),'')
        .DoRTCalc(117,117,.f.)
        if not(empty(.w_CODPART))
          .link_3_2('Full')
        endif
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        .w_GRUPART = .w_GRUPART
        .DoRTCalc(119,119,.f.)
        if not(empty(.w_GRUPART))
          .link_3_5('Full')
        endif
        .w_FLESGR = 'N'
      .oPgFrm.Page3.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
          .DoRTCalc(121,121,.f.)
        .w_CURPART = .w_PART_ZOOM.cCursor
      .oPgFrm.Page1.oPag.oTmrBalloon.Calculate()
        .w_ATAPPOVR = -1
          .DoRTCalc(124,124,.f.)
        .w_FLCALEXP = .F.
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_OREINI_Cose = RIGHT('00'+IIF(EMPTY(.w_OREINI_Cose),'00',.w_OREINI_Cose),2)
        .w_MININI_Cose = RIGHT('00'+IIF(empty(.w_MININI_Cose),'00',.w_MININI_Cose),2)
        .w_OREFIN_Cose = RIGHT('00'+IIF(empty(.w_OREFIN_Cose) OR .w_OREFIN_Cose="00", '23', .w_OREFIN_Cose),2)
        .w_MINFIN_Cose = RIGHT('00'+IIF(empty(.w_MINFIN_Cose) OR (empty(.w_DATFIN_Cose) AND .w_OREFIN_Cose="23" AND .w_MINFIN_Cose="00"), '59', .w_MINFIN_Cose),2)
        .w_DATA_INI_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI_Cose),  ALLTR(STR(DAY(.w_DATINI_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATINI_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATINI_Cose)))+' '+.w_OREINI_Cose+':'+.w_MININI_Cose+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN_Cose),  ALLTR(STR(DAY(.w_DATFIN_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATFIN_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATFIN_Cose)))+' '+.w_OREFIN_Cose+':'+.w_MINFIN_Cose+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
          .DoRTCalc(133,133,.f.)
        .w_DATINIZ = i_datsys
        .w_obsodat1 = 'N'
    endwith
    this.DoRTCalc(136,136,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_65.enabled = this.oPgFrm.Page1.oPag.oBtn_1_65.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_56.enabled = this.oPgFrm.Page2.oPag.oBtn_2_56.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_79.enabled = this.oPgFrm.Page2.oPag.oBtn_2_79.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_71.enabled = this.oPgFrm.Page1.oPag.oBtn_1_71.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_72.enabled = this.oPgFrm.Page1.oPag.oBtn_1_72.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_73.enabled = this.oPgFrm.Page1.oPag.oBtn_1_73.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_9.enabled = this.oPgFrm.Page3.oPag.oBtn_3_9.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_10.enabled = this.oPgFrm.Page3.oPag.oBtn_3_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ctMonth.Calculate()
        .oPgFrm.Page1.oPag.ctYear.Calculate()
        .oPgFrm.Page1.oPag.ctCalendar.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(GSAG_BAG(this, 'DICITURA'))
        .oPgFrm.Page1.oPag.ctDate.Calculate()
        .oPgFrm.Page1.oPag.ct5Day.Calculate()
        .oPgFrm.Page1.oPag.ct1Day.Calculate()
        .oPgFrm.Page1.oPag.ctList_Memo.Calculate()
        if .o_CODPART<>.w_CODPART
          .Calculate_EZJXAIGEFA()
        endif
        .oPgFrm.Page1.oPag.ctList_Giorno.Calculate()
        if .o_VisNote<>.w_VisNote
          .Calculate_SNIKZSBUXZ()
        endif
        .DoRTCalc(1,32,.t.)
        if .o_CodPart<>.w_CodPart.or. .o_PATIPRIS<>.w_PATIPRIS
          .link_2_3('Full')
        endif
        .DoRTCalc(34,35,.t.)
        if .o_CODPART<>.w_CODPART
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        if .o_CODPART<>.w_CODPART
            .w_FLESGR = 'N'
        endif
        if .o_CODPART<>.w_CODPART
            .w_GRUPART = .w_GRUPART
          .link_2_11('Full')
        endif
        .DoRTCalc(39,46,.t.)
        if .o_CODNOM<>.w_CODNOM
            .w_CODCAT = ''
          .link_2_20('Full')
        endif
        .DoRTCalc(48,62,.t.)
        if .o_CODSED<>.w_CODSED
            .w_IMPIANTO = IIF(EMPTY(.w_CODSED), space(10), .w_IMPIANTO)
          .link_2_36('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_2_75.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_69.Calculate()
        if .o_CodPart<>.w_CodPart
          .Calculate_TKGKPESWZF()
        endif
        .DoRTCalc(64,84,.t.)
        if .o_FiltroWeb<>.w_FiltroWeb
            .w_FiltPubWeb = IIF(.w_FiltroWeb='T','',.w_FiltroWeb)
        endif
        .DoRTCalc(86,92,.t.)
        if .o_IMPIANTO<>.w_IMPIANTO.or. .o_COMPIMPKEY<>.w_COMPIMPKEY
            .w_COMPIMPKEYREAD = .w_COMPIMPKEY
          .link_2_91('Full')
        endif
        if .o_COMPIMP<>.w_COMPIMP.or. .o_IMPIANTO<>.w_IMPIANTO
            .w_COMPIMPKEY = 0
        endif
            .w_ATCODSED = .w_CODSED
        .oPgFrm.Page2.oPag.ctDropDate.Calculate()
        .DoRTCalc(96,104,.t.)
            .w_CACODIMP = .w_IMPIANTO
        .DoRTCalc(106,107,.t.)
            .w_ATCODNOM = .w_CODNOM
        .DoRTCalc(109,110,.t.)
        if .o_CODPART<>.w_CODPART
            .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
        endif
        if .o_FLTPART<>.w_FLTPART
            .w_FLTPART = readdipend( i_CodUte, 'C')
        endif
        if .o_READAZI<>.w_READAZI
            .w_READAZI = i_CODAZI
          .link_2_112('Full')
        endif
        .DoRTCalc(114,116,.t.)
        if .o_CodPart<>.w_CodPart.or. .o_PATIPRIS<>.w_PATIPRIS
          .link_3_2('Full')
        endif
        if .o_CODPART<>.w_CODPART
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        if .o_CODPART<>.w_CODPART
            .w_GRUPART = .w_GRUPART
          .link_3_5('Full')
        endif
        if .o_CODPART<>.w_CODPART
            .w_FLESGR = 'N'
        endif
        .oPgFrm.Page3.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
        if .o_DATINIZ<>.w_DATINIZ.or. .o_obsodat1<>.w_obsodat1
          .Calculate_JQCKGKZTVV()
        endif
        if .o_PATIPRIS<>.w_PATIPRIS.or. .o_DATINIZ<>.w_DATINIZ.or. .o_obsodat1<>.w_obsodat1
          .Calculate_BLTFQIRAHO()
        endif
        if .o_CODPART<>.w_CODPART
          .Calculate_FWWDAFBQBE()
        endif
        .oPgFrm.Page1.oPag.oTmrBalloon.Calculate()
        .DoRTCalc(121,126,.t.)
        if .o_OREINI_Cose<>.w_OREINI_Cose
            .w_OREINI_Cose = RIGHT('00'+IIF(EMPTY(.w_OREINI_Cose),'00',.w_OREINI_Cose),2)
        endif
        if .o_MININI_Cose<>.w_MININI_Cose
            .w_MININI_Cose = RIGHT('00'+IIF(empty(.w_MININI_Cose),'00',.w_MININI_Cose),2)
        endif
        if .o_OREFIN_Cose<>.w_OREFIN_Cose.or. .o_DATFIN_Cose<>.w_DATFIN_Cose
            .w_OREFIN_Cose = RIGHT('00'+IIF(empty(.w_OREFIN_Cose) OR .w_OREFIN_Cose="00", '23', .w_OREFIN_Cose),2)
        endif
        if .o_MINFIN_Cose<>.w_MINFIN_Cose.or. .o_DATFIN_Cose<>.w_DATFIN_Cose
            .w_MINFIN_Cose = RIGHT('00'+IIF(empty(.w_MINFIN_Cose) OR (empty(.w_DATFIN_Cose) AND .w_OREFIN_Cose="23" AND .w_MINFIN_Cose="00"), '59', .w_MINFIN_Cose),2)
        endif
        if .o_DATINI_Cose<>.w_DATINI_Cose
          .Calculate_HWTLQYNXHT()
        endif
        if .o_DATFIN_Cose<>.w_DATFIN_Cose
          .Calculate_JBMQEMONXV()
        endif
        if .o_DATINI_Cose<>.w_DATINI_Cose.or. .o_OREINI_Cose<>.w_OREINI_Cose.or. .o_MININI_Cose<>.w_MININI_Cose
            .w_DATA_INI_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI_Cose),  ALLTR(STR(DAY(.w_DATINI_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATINI_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATINI_Cose)))+' '+.w_OREINI_Cose+':'+.w_MININI_Cose+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN_Cose<>.w_DATFIN_Cose.or. .o_OREFIN_Cose<>.w_OREFIN_Cose.or. .o_MINFIN_Cose<>.w_MINFIN_Cose
            .w_DATA_FIN_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN_Cose),  ALLTR(STR(DAY(.w_DATFIN_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATFIN_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATFIN_Cose)))+' '+.w_OREFIN_Cose+':'+.w_MINFIN_Cose+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(133,136,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ctMonth.Calculate()
        .oPgFrm.Page1.oPag.ctYear.Calculate()
        .oPgFrm.Page1.oPag.ctCalendar.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate(GSAG_BAG(this, 'DICITURA'))
        .oPgFrm.Page1.oPag.ctDate.Calculate()
        .oPgFrm.Page1.oPag.ct5Day.Calculate()
        .oPgFrm.Page1.oPag.ct1Day.Calculate()
        .oPgFrm.Page1.oPag.ctList_Memo.Calculate()
        .oPgFrm.Page1.oPag.ctList_Giorno.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_75.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_69.Calculate()
        .oPgFrm.Page2.oPag.ctDropDate.Calculate()
        .oPgFrm.Page3.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
        .oPgFrm.Page1.oPag.oTmrBalloon.Calculate()
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Controllo utente
          .w_Parametro = IIF(.w_Parametro='P' AND EMPTY(.w_CodPart),'G',.w_Parametro)
    endwith
  endproc
  proc Calculate_UDTGOEMFEC()
    with this
          * --- Inizializza memo
          gsag_bag(this;
              ,'INIT';
             )
    endwith
  endproc
  proc Calculate_QKCKJJLEOF()
    with this
          * --- Cambia data e aggiorna
          gsag_bag(this;
              ,'CHANGEDATE';
             )
    endwith
  endproc
  proc Calculate_FZORDMLBAP()
    with this
          * --- Appuntamento selezionato
          gsag_bag(this;
              ,'SELEZIONATO';
             )
    endwith
  endproc
  proc Calculate_MCIUXCRSYF()
    with this
          * --- Carica un nuovo appuntamento
          gsag_bag(this;
              ,'CARICA';
             )
    endwith
  endproc
  proc Calculate_WXWXRTBVWT()
    with this
          * --- Modifica un appuntamento
          gsag_bag(this;
              ,'MODIFICA';
             )
    endwith
  endproc
  proc Calculate_LVXNAWAKTZ()
    with this
          * --- Modifica testo di un appuntamento
          gsag_bag(this;
              ,'MODIFICATXT';
             )
    endwith
  endproc
  proc Calculate_DRMRDFQZXG()
    with this
          * --- Appuntamento deselezionato
          gsag_bag(this;
              ,'DESELEZIONATO';
             )
    endwith
  endproc
  proc Calculate_NKDTEIEXMX()
    with this
          * --- Aggiunge un appuntamento da1-5 gg
          gsag_bag(this;
              ,'ADDAPP';
             )
    endwith
  endproc
  proc Calculate_OCIBWDMCMG()
    with this
          * --- Modifica l'ora di un appuntamento
          gsag_bag(this;
              ,'MODIFICAORA';
             )
    endwith
  endproc
  proc Calculate_WLCGBULOPD()
    with this
          * --- Giorno selezionato
          gsag_bag(this;
              ,'GIORNOSEL';
             )
    endwith
  endproc
  proc Calculate_EZJXAIGEFA()
    with this
          * --- Aggiorna memo
          gsag_bag(this;
              ,'MEMO';
             )
    endwith
  endproc
  proc Calculate_XQWWLUSOKJ()
    with this
          * --- Attivavisualizzazione1giorno
          gsag_bag(this;
              ,'ATTIVA1';
             )
    endwith
  endproc
  proc Calculate_ABLCCHFOLY()
    with this
          * --- Gestione check attivo
          gsag_bag(this;
              ,'CHECKATTIVO';
             )
    endwith
  endproc
  proc Calculate_CWLWOSBKVB()
    with this
          * --- Gestione check disattivo
          gsag_bag(this;
              ,'CHECKDISATTIVO';
             )
    endwith
  endproc
  proc Calculate_RBQUBBJMWW()
    with this
          * --- Cambia data - non aggiorna
          gsag_bag(this;
              ,'CHANGENOUPDATE';
             )
    endwith
  endproc
  proc Calculate_SNIKZSBUXZ()
    with this
          * --- Visualizza le note dei memo
          gsag_bag(this;
              ,'VISNOTEMEMO';
             )
    endwith
  endproc
  proc Calculate_GEHTVBATEI()
    with this
          * --- F5 su appuntamento
          gsag_bag(this;
              ,'CANCELLAAPP';
             )
    endwith
  endproc
  proc Calculate_WNNFNWVMSW()
    with this
          * --- Resetto la sede
          .w_CODSED = ' '
          .w_DESCRI = ' '
    endwith
  endproc
  proc Calculate_YBYZXGTXOU()
    with this
          * --- Resetto l'impianto
          .w_IMPIANTO = ' '
          .w_COMPIMP = ' '
    endwith
  endproc
  proc Calculate_TKGKPESWZF()
    with this
          * --- Cambia caption form
          .cComment = IIF(.w_Parametro='G', IIF(EMPTY(.w_CodPart), IIF(IsAlt(), AH_MSGFORMAT("Agenda di studio"), AH_MSGFORMAT("Agenda globale")), AH_MSGFORMAT("Agenda di")+space(1)+alltrim(.w_DENOM_PART)), AH_MSGFORMAT("La mia agenda"))
          .Caption = .cComment
    endwith
  endproc
  proc Calculate_CXNVLISJIH()
    with this
          * --- CtMDay GotFocus
          gsag_bag(this;
              ,'GOTFOCUS';
             )
    endwith
  endproc
  proc Calculate_WKVPTHREOF()
    with this
          * --- CtMDay LostFocus
          gsag_bag(this;
              ,'LOSTFOCUS';
             )
    endwith
  endproc
  proc Calculate_JQCKGKZTVV()
    with this
          * --- Azzera w_CODPART
          .w_CODPART = space(5)
          .link_2_3('Full')
          .link_3_2('Full')
          .w_DENOM_PART = space(48)
    endwith
  endproc
  proc Calculate_BLTFQIRAHO()
    with this
          * --- Attiva zoom selezionando le righe
          GSAG_BPP(this;
              ,'ATTIVAZOOM';
              ,.w_PART_ZOOM;
              ,.w_CODPART;
              ,this;
             )
    endwith
  endproc
  proc Calculate_FWWDAFBQBE()
    with this
          * --- Seleziona righe
          GSAG_BPP(this;
              ,'SELRIGA';
              ,.w_PART_ZOOM;
              ,.w_CODPART;
              ,this;
             )
    endwith
  endproc
  proc Calculate_JUBNYQLWYE()
    with this
          * --- Visualizza informazioni appuntamento
          gsag_bag(this;
              ,"TMRBALLOON";
             )
    endwith
  endproc
  proc Calculate_HWTLQYNXHT()
    with this
          * --- Azzera ore- minuti inizio
          .w_OREINI_Cose = IIF(EMPTY(.w_DATINI_Cose),'00',.w_OREINI_Cose)
          .w_MININI_Cose = IIF(EMPTY(.w_DATINI_Cose),'00',.w_MININI_Cose)
    endwith
  endproc
  proc Calculate_JBMQEMONXV()
    with this
          * --- Azzera ore- minuti fine
          .w_OREFIN_Cose = IIF(EMPTY(.w_DATFIN_Cose),'00',.w_OREFIN_Cose)
          .w_MINFIN_Cose = IIF(EMPTY(.w_DATFIN_Cose),'00',.w_MINFIN_Cose)
    endwith
  endproc
  proc Calculate_NRTSRHMIVM()
    with this
          * --- Inizializza date
          gsag_ba3(this;
              ,'ct_List';
             )
    endwith
  endproc
  proc Calculate_HFDBZOGJCM()
    with this
          * --- Lancia messaggio
          .w_VARLOG = iif(.w_Parametro='P' AND EMPTY(.w_CodPart), ah_ErrorMsg("Attenzione: l'utente "+alltrim(g_desute)+" non � identificato fra le persone"), .T.)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oPATIPRIS_2_1.enabled = this.oPgFrm.Page2.oPag.oPATIPRIS_2_1.mCond()
    this.oPgFrm.Page2.oPag.oCODPART_2_3.enabled = this.oPgFrm.Page2.oPag.oCODPART_2_3.mCond()
    this.oPgFrm.Page2.oPag.oGRUPART_2_11.enabled = this.oPgFrm.Page2.oPag.oGRUPART_2_11.mCond()
    this.oPgFrm.Page2.oPag.oCODNOM_2_19.enabled = this.oPgFrm.Page2.oPag.oCODNOM_2_19.mCond()
    this.oPgFrm.Page2.oPag.oCODCAT_2_20.enabled = this.oPgFrm.Page2.oPag.oCODCAT_2_20.mCond()
    this.oPgFrm.Page2.oPag.oGIUDICE_2_33.enabled = this.oPgFrm.Page2.oPag.oGIUDICE_2_33.mCond()
    this.oPgFrm.Page2.oPag.oCOMPIMP_2_38.enabled = this.oPgFrm.Page2.oPag.oCOMPIMP_2_38.mCond()
    this.oPgFrm.Page2.oPag.oCODPRA_2_42.enabled = this.oPgFrm.Page2.oPag.oCODPRA_2_42.mCond()
    this.oPgFrm.Page2.oPag.oCODSED_2_70.enabled = this.oPgFrm.Page2.oPag.oCODSED_2_70.mCond()
    this.oPgFrm.Page3.oPag.oPATIPRIS_3_1.enabled = this.oPgFrm.Page3.oPag.oPATIPRIS_3_1.mCond()
    this.oPgFrm.Page3.oPag.oCODPART_3_2.enabled = this.oPgFrm.Page3.oPag.oCODPART_3_2.mCond()
    this.oPgFrm.Page3.oPag.oGRUPART_3_5.enabled = this.oPgFrm.Page3.oPag.oGRUPART_3_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show2
    i_show2=not(this.w_Parametro<>'G')
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Elenco partecipanti"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oDatiAttivita_1_30.visible=!this.oPgFrm.Page1.oPag.oDatiAttivita_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDatiPratica_1_31.visible=!this.oPgFrm.Page1.oPag.oDatiPratica_1_31.mHide()
    this.oPgFrm.Page1.oPag.oDatiAttiPratica_1_40.visible=!this.oPgFrm.Page1.oPag.oDatiAttiPratica_1_40.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_2.visible=!this.oPgFrm.Page2.oPag.oStr_2_2.mHide()
    this.oPgFrm.Page2.oPag.oSTATO_2_4.visible=!this.oPgFrm.Page2.oPag.oSTATO_2_4.mHide()
    this.oPgFrm.Page2.oPag.oRISERVE_2_5.visible=!this.oPgFrm.Page2.oPag.oRISERVE_2_5.mHide()
    this.oPgFrm.Page2.oPag.oFLESGR_2_10.visible=!this.oPgFrm.Page2.oPag.oFLESGR_2_10.mHide()
    this.oPgFrm.Page2.oPag.oGRUPART_2_11.visible=!this.oPgFrm.Page2.oPag.oGRUPART_2_11.mHide()
    this.oPgFrm.Page2.oPag.oTIPORIS_2_13.visible=!this.oPgFrm.Page2.oPag.oTIPORIS_2_13.mHide()
    this.oPgFrm.Page2.oPag.oNUMDOC_2_17.visible=!this.oPgFrm.Page2.oPag.oNUMDOC_2_17.mHide()
    this.oPgFrm.Page2.oPag.oALFDOC_2_18.visible=!this.oPgFrm.Page2.oPag.oALFDOC_2_18.mHide()
    this.oPgFrm.Page2.oPag.oCODCAT_2_20.visible=!this.oPgFrm.Page2.oPag.oCODCAT_2_20.mHide()
    this.oPgFrm.Page2.oPag.oUDIENZE_2_22.visible=!this.oPgFrm.Page2.oPag.oUDIENZE_2_22.mHide()
    this.oPgFrm.Page2.oPag.oFiltroWeb_2_28.visible=!this.oPgFrm.Page2.oPag.oFiltroWeb_2_28.mHide()
    this.oPgFrm.Page2.oPag.oGIUDICE_2_33.visible=!this.oPgFrm.Page2.oPag.oGIUDICE_2_33.mHide()
    this.oPgFrm.Page2.oPag.oENTE_2_35.visible=!this.oPgFrm.Page2.oPag.oENTE_2_35.mHide()
    this.oPgFrm.Page2.oPag.oIMPIANTO_2_36.visible=!this.oPgFrm.Page2.oPag.oIMPIANTO_2_36.mHide()
    this.oPgFrm.Page2.oPag.oDesEnte_2_37.visible=!this.oPgFrm.Page2.oPag.oDesEnte_2_37.mHide()
    this.oPgFrm.Page2.oPag.oCOMPIMP_2_38.visible=!this.oPgFrm.Page2.oPag.oCOMPIMP_2_38.mHide()
    this.oPgFrm.Page2.oPag.oUFFICIO_2_39.visible=!this.oPgFrm.Page2.oPag.oUFFICIO_2_39.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_40.visible=!this.oPgFrm.Page2.oPag.oStr_2_40.mHide()
    this.oPgFrm.Page2.oPag.oCENCOS_2_41.visible=!this.oPgFrm.Page2.oPag.oCENCOS_2_41.mHide()
    this.oPgFrm.Page2.oPag.oDesUffi_2_46.visible=!this.oPgFrm.Page2.oPag.oDesUffi_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_47.visible=!this.oPgFrm.Page2.oPag.oStr_2_47.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_51.visible=!this.oPgFrm.Page2.oPag.oStr_2_51.mHide()
    this.oPgFrm.Page2.oPag.oStrPratica_2_55.visible=!this.oPgFrm.Page2.oPag.oStrPratica_2_55.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_63.visible=!this.oPgFrm.Page2.oPag.oStr_2_63.mHide()
    this.oPgFrm.Page2.oPag.oDPDESGRU_2_64.visible=!this.oPgFrm.Page2.oPag.oDPDESGRU_2_64.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_65.visible=!this.oPgFrm.Page2.oPag.oStr_2_65.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_66.visible=!this.oPgFrm.Page2.oPag.oStr_2_66.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_67.visible=!this.oPgFrm.Page2.oPag.oStr_2_67.mHide()
    this.oPgFrm.Page2.oPag.oDESPIA_2_68.visible=!this.oPgFrm.Page2.oPag.oDESPIA_2_68.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_69.visible=!this.oPgFrm.Page2.oPag.oStr_2_69.mHide()
    this.oPgFrm.Page2.oPag.oCODSED_2_70.visible=!this.oPgFrm.Page2.oPag.oCODSED_2_70.mHide()
    this.oPgFrm.Page2.oPag.oDESCRI_2_71.visible=!this.oPgFrm.Page2.oPag.oDESCRI_2_71.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_72.visible=!this.oPgFrm.Page2.oPag.oStr_2_72.mHide()
    this.oPgFrm.Page2.oPag.oTIPANA_2_76.visible=!this.oPgFrm.Page2.oPag.oTIPANA_2_76.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_77.visible=!this.oPgFrm.Page2.oPag.oStr_2_77.mHide()
    this.oPgFrm.Page2.oPag.oDESNOMGIU_2_78.visible=!this.oPgFrm.Page2.oPag.oDESNOMGIU_2_78.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_79.visible=!this.oPgFrm.Page2.oPag.oBtn_2_79.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_80.visible=!this.oPgFrm.Page2.oPag.oStr_2_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_92.visible=!this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_92.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_71.visible=!this.oPgFrm.Page1.oPag.oBtn_1_71.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_72.visible=!this.oPgFrm.Page1.oPag.oBtn_1_72.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_118.visible=!this.oPgFrm.Page2.oPag.oStr_2_118.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_73.visible=!this.oPgFrm.Page1.oPag.oBtn_1_73.mHide()
    this.oPgFrm.Page3.oPag.oGRUPART_3_5.visible=!this.oPgFrm.Page3.oPag.oGRUPART_3_5.mHide()
    this.oPgFrm.Page3.oPag.oFLESGR_3_6.visible=!this.oPgFrm.Page3.oPag.oFLESGR_3_6.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_7.visible=!this.oPgFrm.Page3.oPag.oStr_3_7.mHide()
    this.oPgFrm.Page3.oPag.oDPDESGRU_3_8.visible=!this.oPgFrm.Page3.oPag.oDPDESGRU_3_8.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_10.visible=!this.oPgFrm.Page3.oPag.oBtn_3_10.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_131.visible=!this.oPgFrm.Page2.oPag.oBtn_2_131.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_133.visible=!this.oPgFrm.Page2.oPag.oStr_2_133.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_134.visible=!this.oPgFrm.Page2.oPag.oStr_2_134.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_UDTGOEMFEC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("CFGLoaded") or lower(cEvent)==lower("ChangeDate")
          .Calculate_QKCKJJLEOF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Selezionato")
          .Calculate_FZORDMLBAP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Carica")
          .Calculate_MCIUXCRSYF()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ctMonth.Event(cEvent)
      .oPgFrm.Page1.oPag.ctYear.Event(cEvent)
      .oPgFrm.Page1.oPag.ctCalendar.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.ctDate.Event(cEvent)
        if lower(cEvent)==lower("Modifica")
          .Calculate_WXWXRTBVWT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("TestoModificato")
          .Calculate_LVXNAWAKTZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Deselezionato")
          .Calculate_DRMRDFQZXG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AggiungiApp")
          .Calculate_NKDTEIEXMX()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ct5Day.Event(cEvent)
      .oPgFrm.Page1.oPag.ct1Day.Event(cEvent)
        if lower(cEvent)==lower("OraModificata")
          .Calculate_OCIBWDMCMG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GiornoSelezionato")
          .Calculate_WLCGBULOPD()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ctList_Memo.Event(cEvent)
        if lower(cEvent)==lower("CFGLoaded")
          .Calculate_EZJXAIGEFA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AttivaGiornaliero")
          .Calculate_XQWWLUSOKJ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ctList_Giorno.Event(cEvent)
        if lower(cEvent)==lower("CheckAttivo")
          .Calculate_ABLCCHFOLY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CheckDisattivo")
          .Calculate_CWLWOSBKVB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ChangeNoUpdate") or lower(cEvent)==lower("Blank")
          .Calculate_RBQUBBJMWW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CancellaApp")
          .Calculate_GEHTVBATEI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CODNOM Changed")
          .Calculate_WNNFNWVMSW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CODSED Changed")
          .Calculate_YBYZXGTXOU()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_75.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_81.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_69.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_TKGKPESWZF()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.ctDropDate.Event(cEvent)
        if lower(cEvent)==lower("CtMDay GotFocus")
          .Calculate_CXNVLISJIH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CtMDay LostFocus")
          .Calculate_WKVPTHREOF()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.PART_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_PART_ZOOM after query")
          .Calculate_BLTFQIRAHO()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oTmrBalloon.Event(cEvent)
        if lower(cEvent)==lower("tmrBalloon")
          .Calculate_JUBNYQLWYE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_NRTSRHMIVM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_HFDBZOGJCM()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODPART
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_CODPART))
          select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oCODPART_2_3'),i_cWhere,'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODPART)
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPART = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_GRUPART = NVL(_Link_.DPGRUPRE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPART = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
      this.w_DPTIPRIS = space(1)
      this.w_GRUPART = space(5)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS) AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1<>'S') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODPART = space(5)
        this.w_COGNPART = space(40)
        this.w_NOMEPART = space(40)
        this.w_DESCRPART = space(40)
        this.w_DPTIPRIS = space(1)
        this.w_GRUPART = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPART
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_GRUPART))
          select DPCODICE,DPTIPRIS,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oGRUPART_2_11'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPART)
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPART = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPOGRUP = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DPDESGRU = NVL(_Link_.DPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPART = space(5)
      endif
      this.w_TIPOGRUP = space(1)
      this.w_DPDESGRU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
        endif
        this.w_GRUPART = space(5)
        this.w_TIPOGRUP = space(1)
        this.w_DPDESGRU = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPORIS
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_RISE_IDX,3]
    i_lTable = "TIP_RISE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2], .t., this.TIP_RISE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPORIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ATR',True,'TIP_RISE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_TIPORIS)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_TIPORIS))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPORIS)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPORIS) and !this.bDontReportError
            deferred_cp_zoom('TIP_RISE','*','TRCODICE',cp_AbsName(oSource.parent,'oTIPORIS_2_13'),i_cWhere,'GSAG_ATR',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPORIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_TIPORIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_TIPORIS)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPORIS = NVL(_Link_.TRCODICE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPORIS = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_RISE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPORIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODICE
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CACODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CACODICE))
          select CACODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODICE) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oCACODICE_2_14'),i_cWhere,'',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODICE)
            select CACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODICE = NVL(_Link_.CACODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CACODICE = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_2_19'),i_cWhere,'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(30))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
      this.w_CODCLI = NVL(_Link_.NOCODCLI,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_DESNOM = space(40)
      this.w_COMODO = space(30)
      this.w_TIPNOM = space(1)
      this.w_CODCLI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM = space(15)
        this.w_DESNOM = space(40)
        this.w_COMODO = space(30)
        this.w_TIPNOM = space(1)
        this.w_CODCLI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SOGG_IDX,3]
    i_lTable = "CAT_SOGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2], .t., this.CAT_SOGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACS',True,'CAT_SOGG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCAT like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODCAT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODCAT',trim(this.w_CODCAT))
          select CSCODCAT,CSDESCAT,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODCAT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.CSCODCAT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStr(trim(this.w_CODCAT)+"%");

            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CAT_SOGG','*','CSCODCAT',cp_AbsName(oSource.parent,'oCODCAT_2_20'),i_cWhere,'GSPR_ACS',"Categorie soggetti esterni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',oSource.xKey(1))
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',this.w_CODCAT)
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.CSCODCAT,space(10))
      this.w_DESCAT = NVL(_Link_.CSDESCAT,space(30))
      this.w_TIPCAT = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(10)
      endif
      this.w_DESCAT = space(30)
      this.w_TIPCAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCAT,1)
      cp_ShowWarn(i_cKey,this.CAT_SOGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODTIPOL
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFFTIPAT_IDX,3]
    i_lTable = "OFFTIPAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2], .t., this.OFFTIPAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODTIPOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFFTIPAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODTIP like "+cp_ToStrODBC(trim(this.w_CODTIPOL)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODTIP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODTIP',trim(this.w_CODTIPOL))
          select TACODTIP,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODTIP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODTIPOL)==trim(_Link_.TACODTIP) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStrODBC(trim(this.w_CODTIPOL)+"%");

            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStr(trim(this.w_CODTIPOL)+"%");

            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODTIPOL) and !this.bDontReportError
            deferred_cp_zoom('OFFTIPAT','*','TACODTIP',cp_AbsName(oSource.parent,'oCODTIPOL_2_31'),i_cWhere,'',"Tipologie attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',oSource.xKey(1))
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODTIPOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(this.w_CODTIPOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',this.w_CODTIPOL)
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODTIPOL = NVL(_Link_.TACODTIP,space(5))
      this.w_DESTIPOL = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODTIPOL = space(5)
      endif
      this.w_DESTIPOL = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])+'\'+cp_ToStr(_Link_.TACODTIP,1)
      cp_ShowWarn(i_cKey,this.OFFTIPAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODTIPOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GIUDICE
  func Link_2_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GIUDICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_GIUDICE))
          select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GIUDICE)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_GIUDICE)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_GIUDICE)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NOINDIRI like "+cp_ToStrODBC(trim(this.w_GIUDICE)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NOINDIRI like "+cp_ToStr(trim(this.w_GIUDICE)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GIUDICE) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oGIUDICE_2_33'),i_cWhere,'GSAR_ANO',"Giudici",'GSPR_ZGI.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GIUDICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_GIUDICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_GIUDICE)
            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GIUDICE = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOMGIU = NVL(_Link_.NODESCRI,space(60))
      this.w_COMODOGIU = NVL(_Link_.NODESCR2,space(60))
      this.w_INDNOM = NVL(_Link_.NOINDIRI,space(35))
      this.w_CAPNOM = NVL(_Link_.NO___CAP,space(9))
      this.w_DATOBSONOM = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GIUDICE = space(15)
      endif
      this.w_DESNOMGIU = space(60)
      this.w_COMODOGIU = space(60)
      this.w_INDNOM = space(35)
      this.w_CAPNOM = space(9)
      this.w_DATOBSONOM = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!IsAlt() OR ( (.w_DATOBSONOM>i_datsys OR EMPTY(.w_DATOBSONOM) ) And CHKGIUDI( .w_GIUDICE ))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Nominativo inesistente o obsoleto non associato come giudice ad alcuna pratica")
        endif
        this.w_GIUDICE = space(15)
        this.w_DESNOMGIU = space(60)
        this.w_COMODOGIU = space(60)
        this.w_INDNOM = space(35)
        this.w_CAPNOM = space(9)
        this.w_DATOBSONOM = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GIUDICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ENTE
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRA_ENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EPCODICE like "+cp_ToStrODBC(trim(this.w_ENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EPCODICE',trim(this.w_ENTE))
          select EPCODICE,EPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ENTE)==trim(_Link_.EPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" EPDESCRI like "+cp_ToStrODBC(trim(this.w_ENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" EPDESCRI like "+cp_ToStr(trim(this.w_ENTE)+"%");

            select EPCODICE,EPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ENTE) and !this.bDontReportError
            deferred_cp_zoom('PRA_ENTI','*','EPCODICE',cp_AbsName(oSource.parent,'oENTE_2_35'),i_cWhere,'',"Enti pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',oSource.xKey(1))
            select EPCODICE,EPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_ENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_ENTE)
            select EPCODICE,EPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ENTE = NVL(_Link_.EPCODICE,space(10))
      this.w_DesEnte = NVL(_Link_.EPDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ENTE = space(10)
      endif
      this.w_DesEnte = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMPIANTO
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMPIANTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIM',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_IMPIANTO)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_IMPIANTO))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMPIANTO)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IMPIANTO) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oIMPIANTO_2_36'),i_cWhere,'GSAG_MIM',"Impianto",'GSAG_AAT.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMPIANTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMPIANTO = NVL(_Link_.IMCODICE,space(10))
      this.w_IMDESCRI = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IMPIANTO = space(10)
      endif
      this.w_IMDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMPIANTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UFFICIO
  func Link_2_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_UFFI_IDX,3]
    i_lTable = "PRA_UFFI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2], .t., this.PRA_UFFI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UFFICIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PRA_UFFI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UFCODICE like "+cp_ToStrODBC(trim(this.w_UFFICIO)+"%");

          i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UFCODICE',trim(this.w_UFFICIO))
          select UFCODICE,UFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UFFICIO)==trim(_Link_.UFCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" UFDESCRI like "+cp_ToStrODBC(trim(this.w_UFFICIO)+"%");

            i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" UFDESCRI like "+cp_ToStr(trim(this.w_UFFICIO)+"%");

            select UFCODICE,UFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_UFFICIO) and !this.bDontReportError
            deferred_cp_zoom('PRA_UFFI','*','UFCODICE',cp_AbsName(oSource.parent,'oUFFICIO_2_39'),i_cWhere,'',"Uffici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',oSource.xKey(1))
            select UFCODICE,UFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UFFICIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE,UFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(this.w_UFFICIO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',this.w_UFFICIO)
            select UFCODICE,UFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UFFICIO = NVL(_Link_.UFCODICE,space(10))
      this.w_DESUFFI = NVL(_Link_.UFDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_UFFICIO = space(10)
      endif
      this.w_DESUFFI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2])+'\'+cp_ToStr(_Link_.UFCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_UFFI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UFFICIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENCOS
  func Link_2_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CENCOS))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCENCOS_2_41'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENCOS)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CENCOS = space(15)
      endif
      this.w_DESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_2_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_bzz',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRA))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRA_2_42'),i_cWhere,'gsar_bzz',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(75))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESPRA = space(75)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodUte
  func Link_2_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodUte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CodUte);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CodUte)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CodUte) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCodUte_2_43'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodUte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CodUte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CodUte)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodUte = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CodUte = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodUte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSED
  func Link_2_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MDD',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_CODSED)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPNOM);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_TIPNOM;
                     ,'DDCODICE',this.w_CODCLI;
                     ,'DDCODDES',trim(this.w_CODSED))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSED)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSED) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCODSED_2_70'),i_cWhere,'GSAR_MDD',"Sedi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPNOM<>oSource.xKey(1);
           .or. this.w_CODCLI<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPNOM);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_CODSED);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPNOM);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_TIPNOM;
                       ,'DDCODICE',this.w_CODCLI;
                       ,'DDCODDES',this.w_CODSED)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSED = NVL(_Link_.DDCODDES,space(5))
      this.w_DESCRI = NVL(_Link_.DDNOMDES,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODSED = space(5)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPIMPKEYREAD
  func Link_2_91(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPIMPKEYREAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPIMPKEYREAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_COMPIMPKEYREAD);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO;
                       ,'CPROWNUM',this.w_COMPIMPKEYREAD)
            select IMCODICE,CPROWNUM,IMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPIMPKEYREAD = NVL(_Link_.CPROWNUM,0)
      this.w_COMPIMP = NVL(_Link_.IMDESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_COMPIMPKEYREAD = 0
      endif
      this.w_COMPIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPIMPKEYREAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_2_112(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLVISI,PAFLAPPU,PAFLATGE,PAFLUDIE,PAFLSEST,PAFLASSE,PAFLINPR,PAFLNOTE,PAFLDAFA";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PAFLVISI,PAFLAPPU,PAFLATGE,PAFLUDIE,PAFLSEST,PAFLASSE,PAFLINPR,PAFLNOTE,PAFLDAFA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_FLVISI = NVL(_Link_.PAFLVISI,space(1))
      this.w_ATTSTU = NVL(_Link_.PAFLAPPU,space(1))
      this.w_ATTGENER = NVL(_Link_.PAFLATGE,space(1))
      this.w_UDIENZE = NVL(_Link_.PAFLUDIE,space(1))
      this.w_SESSTEL = NVL(_Link_.PAFLSEST,space(1))
      this.w_ASSENZE = NVL(_Link_.PAFLASSE,space(1))
      this.w_DAINSPRE = NVL(_Link_.PAFLINPR,space(1))
      this.w_MEMO = NVL(_Link_.PAFLNOTE,space(1))
      this.w_DAFARE = NVL(_Link_.PAFLDAFA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_FLVISI = space(1)
      this.w_ATTSTU = space(1)
      this.w_ATTGENER = space(1)
      this.w_UDIENZE = space(1)
      this.w_SESSTEL = space(1)
      this.w_ASSENZE = space(1)
      this.w_DAINSPRE = space(1)
      this.w_MEMO = space(1)
      this.w_DAFARE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPART
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_CODPART))
          select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_CODPART)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_CODPART)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oCODPART_3_2'),i_cWhere,'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODPART)
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPART = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
      this.w_GRUPART = NVL(_Link_.DPGRUPRE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPART = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
      this.w_DPTIPRIS = space(1)
      this.w_GRUPART = space(5)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS) AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1<>'S') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODPART = space(5)
        this.w_COGNPART = space(40)
        this.w_NOMEPART = space(40)
        this.w_DESCRPART = space(40)
        this.w_DPTIPRIS = space(1)
        this.w_GRUPART = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPART
  func Link_3_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPART)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_GRUPART))
          select DPCODICE,DPTIPRIS,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPART)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUPART) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oGRUPART_3_5'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPART)
            select DPCODICE,DPTIPRIS,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPART = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPOGRUP = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DPDESGRU = NVL(_Link_.DPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPART = space(5)
      endif
      this.w_TIPOGRUP = space(1)
      this.w_DPDESGRU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
        endif
        this.w_GRUPART = space(5)
        this.w_TIPOGRUP = space(1)
        this.w_DPDESGRU = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDatiAttivita_1_30.value==this.w_DatiAttivita)
      this.oPgFrm.Page1.oPag.oDatiAttivita_1_30.value=this.w_DatiAttivita
    endif
    if not(this.oPgFrm.Page1.oPag.oDatiPratica_1_31.value==this.w_DatiPratica)
      this.oPgFrm.Page1.oPag.oDatiPratica_1_31.value=this.w_DatiPratica
    endif
    if not(this.oPgFrm.Page1.oPag.oDatiAttiPratica_1_40.value==this.w_DatiAttiPratica)
      this.oPgFrm.Page1.oPag.oDatiAttiPratica_1_40.value=this.w_DatiAttiPratica
    endif
    if not(this.oPgFrm.Page2.oPag.oPATIPRIS_2_1.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page2.oPag.oPATIPRIS_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODPART_2_3.value==this.w_CODPART)
      this.oPgFrm.Page2.oPag.oCODPART_2_3.value=this.w_CODPART
    endif
    if not(this.oPgFrm.Page2.oPag.oSTATO_2_4.RadioValue()==this.w_STATO)
      this.oPgFrm.Page2.oPag.oSTATO_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRISERVE_2_5.RadioValue()==this.w_RISERVE)
      this.oPgFrm.Page2.oPag.oRISERVE_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDENOM_PART_2_8.value==this.w_DENOM_PART)
      this.oPgFrm.Page2.oPag.oDENOM_PART_2_8.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page2.oPag.oFLESGR_2_10.RadioValue()==this.w_FLESGR)
      this.oPgFrm.Page2.oPag.oFLESGR_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUPART_2_11.value==this.w_GRUPART)
      this.oPgFrm.Page2.oPag.oGRUPART_2_11.value=this.w_GRUPART
    endif
    if not(this.oPgFrm.Page2.oPag.oPRIORITA_2_12.RadioValue()==this.w_PRIORITA)
      this.oPgFrm.Page2.oPag.oPRIORITA_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPORIS_2_13.RadioValue()==this.w_TIPORIS)
      this.oPgFrm.Page2.oPag.oTIPORIS_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCACODICE_2_14.value==this.w_CACODICE)
      this.oPgFrm.Page2.oPag.oCACODICE_2_14.value=this.w_CACODICE
    endif
    if not(this.oPgFrm.Page2.oPag.oCADESCRI_2_15.value==this.w_CADESCRI)
      this.oPgFrm.Page2.oPag.oCADESCRI_2_15.value=this.w_CADESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oATTSTU_2_16.RadioValue()==this.w_ATTSTU)
      this.oPgFrm.Page2.oPag.oATTSTU_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDOC_2_17.value==this.w_NUMDOC)
      this.oPgFrm.Page2.oPag.oNUMDOC_2_17.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oALFDOC_2_18.value==this.w_ALFDOC)
      this.oPgFrm.Page2.oPag.oALFDOC_2_18.value=this.w_ALFDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCODNOM_2_19.value==this.w_CODNOM)
      this.oPgFrm.Page2.oPag.oCODNOM_2_19.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCAT_2_20.RadioValue()==this.w_CODCAT)
      this.oPgFrm.Page2.oPag.oCODCAT_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATTGENER_2_21.RadioValue()==this.w_ATTGENER)
      this.oPgFrm.Page2.oPag.oATTGENER_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oUDIENZE_2_22.RadioValue()==this.w_UDIENZE)
      this.oPgFrm.Page2.oPag.oUDIENZE_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSESSTEL_2_23.RadioValue()==this.w_SESSTEL)
      this.oPgFrm.Page2.oPag.oSESSTEL_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oASSENZE_2_24.RadioValue()==this.w_ASSENZE)
      this.oPgFrm.Page2.oPag.oASSENZE_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDAINSPRE_2_25.RadioValue()==this.w_DAINSPRE)
      this.oPgFrm.Page2.oPag.oDAINSPRE_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMEMO_2_26.RadioValue()==this.w_MEMO)
      this.oPgFrm.Page2.oPag.oMEMO_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDAFARE_2_27.RadioValue()==this.w_DAFARE)
      this.oPgFrm.Page2.oPag.oDAFARE_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFiltroWeb_2_28.RadioValue()==this.w_FiltroWeb)
      this.oPgFrm.Page2.oPag.oFiltroWeb_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDISPONIB_2_29.RadioValue()==this.w_DISPONIB)
      this.oPgFrm.Page2.oPag.oDISPONIB_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFiltPerson_2_30.value==this.w_FiltPerson)
      this.oPgFrm.Page2.oPag.oFiltPerson_2_30.value=this.w_FiltPerson
    endif
    if not(this.oPgFrm.Page2.oPag.oCODTIPOL_2_31.value==this.w_CODTIPOL)
      this.oPgFrm.Page2.oPag.oCODTIPOL_2_31.value=this.w_CODTIPOL
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTE_2_32.value==this.w_NOTE)
      this.oPgFrm.Page2.oPag.oNOTE_2_32.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oGIUDICE_2_33.value==this.w_GIUDICE)
      this.oPgFrm.Page2.oPag.oGIUDICE_2_33.value=this.w_GIUDICE
    endif
    if not(this.oPgFrm.Page2.oPag.oATLOCALI_2_34.value==this.w_ATLOCALI)
      this.oPgFrm.Page2.oPag.oATLOCALI_2_34.value=this.w_ATLOCALI
    endif
    if not(this.oPgFrm.Page2.oPag.oENTE_2_35.value==this.w_ENTE)
      this.oPgFrm.Page2.oPag.oENTE_2_35.value=this.w_ENTE
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPIANTO_2_36.value==this.w_IMPIANTO)
      this.oPgFrm.Page2.oPag.oIMPIANTO_2_36.value=this.w_IMPIANTO
    endif
    if not(this.oPgFrm.Page2.oPag.oDesEnte_2_37.value==this.w_DesEnte)
      this.oPgFrm.Page2.oPag.oDesEnte_2_37.value=this.w_DesEnte
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMPIMP_2_38.value==this.w_COMPIMP)
      this.oPgFrm.Page2.oPag.oCOMPIMP_2_38.value=this.w_COMPIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oUFFICIO_2_39.value==this.w_UFFICIO)
      this.oPgFrm.Page2.oPag.oUFFICIO_2_39.value=this.w_UFFICIO
    endif
    if not(this.oPgFrm.Page2.oPag.oCENCOS_2_41.value==this.w_CENCOS)
      this.oPgFrm.Page2.oPag.oCENCOS_2_41.value=this.w_CENCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oCODPRA_2_42.value==this.w_CODPRA)
      this.oPgFrm.Page2.oPag.oCODPRA_2_42.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oCodUte_2_43.value==this.w_CodUte)
      this.oPgFrm.Page2.oPag.oCodUte_2_43.value=this.w_CodUte
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRA_2_44.value==this.w_DESPRA)
      this.oPgFrm.Page2.oPag.oDESPRA_2_44.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDesUffi_2_46.value==this.w_DesUffi)
      this.oPgFrm.Page2.oPag.oDesUffi_2_46.value=this.w_DesUffi
    endif
    if not(this.oPgFrm.Page2.oPag.oDESUTE_2_48.value==this.w_DESUTE)
      this.oPgFrm.Page2.oPag.oDESUTE_2_48.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINI_Cose_2_52.value==this.w_DATINI_Cose)
      this.oPgFrm.Page2.oPag.oDATINI_Cose_2_52.value=this.w_DATINI_Cose
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFIN_Cose_2_53.value==this.w_DATFIN_Cose)
      this.oPgFrm.Page2.oPag.oDATFIN_Cose_2_53.value=this.w_DATFIN_Cose
    endif
    if not(this.oPgFrm.Page2.oPag.oVisNote_2_54.RadioValue()==this.w_VisNote)
      this.oPgFrm.Page2.oPag.oVisNote_2_54.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oStrPratica_2_55.RadioValue()==this.w_StrPratica)
      this.oPgFrm.Page2.oPag.oStrPratica_2_55.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOM_2_58.value==this.w_DESNOM)
      this.oPgFrm.Page2.oPag.oDESNOM_2_58.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESTIPOL_2_61.value==this.w_DESTIPOL)
      this.oPgFrm.Page2.oPag.oDESTIPOL_2_61.value=this.w_DESTIPOL
    endif
    if not(this.oPgFrm.Page2.oPag.oDPDESGRU_2_64.value==this.w_DPDESGRU)
      this.oPgFrm.Page2.oPag.oDPDESGRU_2_64.value=this.w_DPDESGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPIA_2_68.value==this.w_DESPIA)
      this.oPgFrm.Page2.oPag.oDESPIA_2_68.value=this.w_DESPIA
    endif
    if not(this.oPgFrm.Page2.oPag.oCODSED_2_70.value==this.w_CODSED)
      this.oPgFrm.Page2.oPag.oCODSED_2_70.value=this.w_CODSED
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCRI_2_71.value==this.w_DESCRI)
      this.oPgFrm.Page2.oPag.oDESCRI_2_71.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPANA_2_76.RadioValue()==this.w_TIPANA)
      this.oPgFrm.Page2.oPag.oTIPANA_2_76.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOMGIU_2_78.value==this.w_DESNOMGIU)
      this.oPgFrm.Page2.oPag.oDESNOMGIU_2_78.value=this.w_DESNOMGIU
    endif
    if not(this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_92.value==this.w_COMPIMPKEY)
      this.oPgFrm.Page2.oPag.oCOMPIMPKEY_2_92.value=this.w_COMPIMPKEY
    endif
    if not(this.oPgFrm.Page3.oPag.oPATIPRIS_3_1.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page3.oPag.oPATIPRIS_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCODPART_3_2.value==this.w_CODPART)
      this.oPgFrm.Page3.oPag.oCODPART_3_2.value=this.w_CODPART
    endif
    if not(this.oPgFrm.Page3.oPag.oDENOM_PART_3_4.value==this.w_DENOM_PART)
      this.oPgFrm.Page3.oPag.oDENOM_PART_3_4.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page3.oPag.oGRUPART_3_5.value==this.w_GRUPART)
      this.oPgFrm.Page3.oPag.oGRUPART_3_5.value=this.w_GRUPART
    endif
    if not(this.oPgFrm.Page3.oPag.oFLESGR_3_6.RadioValue()==this.w_FLESGR)
      this.oPgFrm.Page3.oPag.oFLESGR_3_6.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDPDESGRU_3_8.value==this.w_DPDESGRU)
      this.oPgFrm.Page3.oPag.oDPDESGRU_3_8.value=this.w_DPDESGRU
    endif
    if not(this.oPgFrm.Page3.oPag.oDATINIZ_3_17.value==this.w_DATINIZ)
      this.oPgFrm.Page3.oPag.oDATINIZ_3_17.value=this.w_DATINIZ
    endif
    if not(this.oPgFrm.Page3.oPag.oobsodat1_3_18.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page3.oPag.oobsodat1_3_18.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS) AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1<>'S') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S')))  and (.w_Parametro='G')  and not(empty(.w_CODPART))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODPART_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART))  and not(.w_DPTIPRIS <> 'P' )  and (.w_PATIPRIS='P' OR .w_PATIPRIS='T')  and not(empty(.w_GRUPART))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUPART_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
          case   not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and (Empty( .w_GIUDICE ))  and not(empty(.w_CODNOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODNOM_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!IsAlt() OR ( (.w_DATOBSONOM>i_datsys OR EMPTY(.w_DATOBSONOM) ) And CHKGIUDI( .w_GIUDICE )))  and not(not isalt())  and (Empty( .w_CODNOM ))  and not(empty(.w_GIUDICE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGIUDICE_2_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Nominativo inesistente o obsoleto non associato come giudice ad alcuna pratica")
          case   not(.w_DATINI_Cose<=.w_DATFIN_Cose OR EMPTY(.w_DATFIN_Cose))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATFIN_Cose_2_53.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not((EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS) AND (((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_DATINIZ) AND .w_obsodat1<>'S') OR (NOT EMPTY(.w_DATOBSO) AND .w_DATOBSO<=.w_DATINIZ AND .w_obsodat1='S')))  and (.w_Parametro='G')  and not(empty(.w_CODPART))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCODPART_3_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART))  and not(.w_DPTIPRIS <> 'P' )  and (.w_PATIPRIS='P' OR .w_PATIPRIS='T')  and not(empty(.w_GRUPART))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oGRUPART_3_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
          case   (empty(.w_DATINIZ))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oDATINIZ_3_17.SetFocus()
            i_bnoObbl = !empty(.w_DATINIZ)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PATIPRIS = this.w_PATIPRIS
    this.o_CODPART = this.w_CODPART
    this.o_CODNOM = this.w_CODNOM
    this.o_FiltroWeb = this.w_FiltroWeb
    this.o_IMPIANTO = this.w_IMPIANTO
    this.o_COMPIMP = this.w_COMPIMP
    this.o_DATINI_Cose = this.w_DATINI_Cose
    this.o_DATFIN_Cose = this.w_DATFIN_Cose
    this.o_VisNote = this.w_VisNote
    this.o_CODSED = this.w_CODSED
    this.o_COMPIMPKEY = this.w_COMPIMPKEY
    this.o_FLTPART = this.w_FLTPART
    this.o_READAZI = this.w_READAZI
    this.o_OREINI_Cose = this.w_OREINI_Cose
    this.o_MININI_Cose = this.w_MININI_Cose
    this.o_OREFIN_Cose = this.w_OREFIN_Cose
    this.o_MINFIN_Cose = this.w_MINFIN_Cose
    this.o_DATINIZ = this.w_DATINIZ
    this.o_obsodat1 = this.w_obsodat1
    return

enddefine

* --- Define pages as container
define class tgsag_kagPag1 as StdContainer
  Width  = 812
  height = 612
  stdWidth  = 812
  stdheight = 612
  resizeXpos=412
  resizeYpos=398
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_17 as StdButton with uid="LXCNFYKIMD",left=550, top=10, width=50,height=25,;
    caption="Giorno", nPag=1;
    , ToolTipText = "Attiva la visualizzazione giornaliera";
    , HelpContextID = 242169446;
    , Forecolor=15181930;
    , FontName = "MS Sans Serif", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSAG_BAG(this.Parent.oContained,"ATTIVA1")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="GCQLGYJLPY",left=601, top=10, width=54,height=25,;
    caption="5 Giorni", nPag=1;
    , ToolTipText = "Attiva la visualizzazione lavorativa";
    , HelpContextID = 24341679;
    , Forecolor=15181930;
    , FontName = "MS Sans Serif", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSAG_BAG(this.Parent.oContained,"ATTIVA5")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="KARGNLYPFN",left=656, top=10, width=54,height=25,;
    caption="Settim.", nPag=1;
    , ToolTipText = "Attiva la visualizzazione settimanale";
    , HelpContextID = 203522854;
    , Forecolor=15181930;
    , FontName = "MS Sans Serif", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSAG_BAG(this.Parent.oContained,"ATTIVA7")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="XANKMBWBSY",left=711, top=10, width=46,height=25,;
    caption="Mese", nPag=1;
    , ToolTipText = "Attiva la visualizzazione mensile";
    , HelpContextID = 125668666;
    , Forecolor=15181930;
    , FontName = "MS Sans Serif", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSAG_BAG(this.Parent.oContained,"ATTIVA31")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_21 as StdButton with uid="PXHFPNNKQB",left=758, top=10, width=47,height=25,;
    caption="Anno", nPag=1;
    , ToolTipText = "Attiva la visualizzazione annuale";
    , HelpContextID = 125031674;
    , Forecolor=15181930;
    , FontName = "MS Sans Serif", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSAG_BAG(this.Parent.oContained,"ATTIVA365")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ctMonth as cp_ctMonth with uid="NKVCTKODLL",left=0, top=43, width=439,height=535,;
    caption='ctMonth',;
   bGlobalFont=.t.,;
    DateAlign=1,DateBorder=0,DatePosition=0,FirstDay=1,FillDates=.t.,HeaderBorder=0,var="w_datasel",DayHeader="",Visible=.f.,MonthNames="",FontSize=8,FontName="MS Sans Serif",VarDate="w_DataApp",;
    nPag=1;
    , HelpContextID = 211147994


  add object ctYear as cp_ctYear with uid="JNYXONDSYU",left=0, top=43, width=439,height=535,;
    caption='ctYear',;
   bGlobalFont=.t.,;
    MonthNames="",DisplayType=0,DayHeader="",var="w_datasel",Visible=.f.,FontName="MS Sans Serif",FontSize=8,ReverseDisplay=.t.,VarDate="w_DataApp",;
    nPag=1;
    , HelpContextID = 9495334


  add object ctCalendar as cp_ctCalendar with uid="RQQJFDZTSR",left=0, top=44, width=586,height=479,;
    caption='ctCalendar',;
   bGlobalFont=.t.,;
    Visible=.f.,var="w_DataSel",DayNames="",MonthNames="",FontName="MS Sans Serif",FontSize=8,VarDate="w_DataApp",;
    nPag=1;
    , HelpContextID = 71039898


  add object oBtn_1_25 as StdButton with uid="QRRMNBJWBE",left=24, top=10, width=23,height=25,;
    CpPicture="tzoom.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 132785930;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        gsag_bag(this.Parent.oContained,"RICERCA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="BTDQRPJAMB",left=70, top=10, width=76,height=25,;
    caption="Oggi", nPag=1;
    , ToolTipText = "Vai alla data di oggi";
    , HelpContextID = 125455130;
    , Forecolor=15181930;
    , FontName = "MS Sans Serif", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSAG_BAG(this.Parent.oContained,"OGGI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_27 as cp_calclbl with uid="SBYVTOWUAF",left=195, top=14, width=257,height=27,;
    caption='Dicitura',;
   bGlobalFont=.t.,;
    caption="",fontname="ms sans serif",fontsize=12,fontBold=.t.,fontItalic=.f.,fontUnderline=.f.,;
    nPag=1;
    , HelpContextID = 80049815


  add object oBtn_1_28 as StdButton with uid="UPYLWPLCRS",left=169, top=10, width=23,height=25,;
    CpPicture="bmp\frecciadx_small.bmp", caption="", nPag=1;
    , ToolTipText = "Incrementa";
    , HelpContextID = 132785930;
    , ImgSize = 16;
    , FontName = "Tahoma", FontSize = 12, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        GSAG_BAG(this.Parent.oContained,"+")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="JHUMCEAPHZ",left=146, top=10, width=23,height=25,;
    CpPicture="bmp\frecciasn_small.bmp", caption="", nPag=1;
    , ToolTipText = "Decrementa";
    , HelpContextID = 132785930;
    , ImgSize = 16;
    , FontName = "MS Sans Serif", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSAG_BAG(this.Parent.oContained,"-")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDatiAttivita_1_30 as StdMemo with uid="FLUEGQTEXG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DatiAttivita", cQueryName = "DatiAttivita",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 251711745,;
    FontName = "MS Sans Serif", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=58, Width=404, Left=2, Top=523, ReadOnly=.T.

  func oDatiAttivita_1_30.mHide()
    with this.Parent.oContained
      return (.w_CalAttivo#1 AND .w_CalAttivo#2)
    endwith
  endfunc

  add object oDatiPratica_1_31 as StdMemo with uid="YKJRFMCTDJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DatiPratica", cQueryName = "DatiPratica",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 260896826,;
    FontName = "MS Sans Serif", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=58, Width=404, Left=408, Top=523, ReadOnly=.T.

  func oDatiPratica_1_31.mHide()
    with this.Parent.oContained
      return (.w_CalAttivo#1 AND .w_CalAttivo#2)
    endwith
  endfunc


  add object ctDate as cp_ctDate with uid="HPZZEUUBFO",left=585, top=44, width=223,height=133,;
    caption='ctDate',;
   bGlobalFont=.t.,;
    FontName="MS Sans Serif",FontSize=8,var="w_DataSel",DayHeader="",TodayText="Oggi",MonthNames="",DualMonths=.f.,;
    nPag=1;
    , HelpContextID = 79401766


  add object ct5Day as cp_ctMDay with uid="EFGLHMINVP",left=0, top=44, width=585,height=477,;
    caption='ct5Day',;
   bGlobalFont=.t.,;
    serial="w_Serial",Visible=.f.,VirtualColumns=0,FontName="MS Sans Serif",VarDate="w_DataApp",VarStart="w_OraStart",IncludeAmPm=.f.,AllDayHeight=30,DisplayColumns=5,MonthNames="Gen,Feb,Mar,Apr,Mag,Giu,Lug,Ago,Set,Ott,Nov,Dic",DayNames="Dom,Lun,Mar,Mer,Gio,Ven,Sab",DateFormat="d n M y",FontSize=8,VarEnd="w_OraEnd",var="w_DataSel",varTxt="w_TxtSel",HeaderLineBreak=.f.,;
    nPag=1;
    , HelpContextID = 124625702


  add object ct1Day as cp_ctMDay with uid="OAQBVTOHJE",left=0, top=44, width=585,height=477,;
    caption='ct1Day',;
   bGlobalFont=.t.,;
    serial="w_Serial",Visible=.f.,VirtualColumns=0,FontName="MS Sans Serif",VarDate="w_DataApp",VarStart="w_OraStart",IncludeAmPm=.f.,AllDayHeight=30,DisplayColumns=1,MonthNames="",DayNames="",DateFormat="d n M Y",FontSize=8,VarEnd="w_OraEnd",var="w_DataSel",varTxt="w_TxtSel",HeaderLineBreak=.f.,;
    nPag=1;
    , HelpContextID = 124609318

  add object oDatiAttiPratica_1_40 as StdMemo with uid="LMJKSOJBCD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DatiAttiPratica", cQueryName = "DatiAttiPratica",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 178324895,;
    FontName = "MS Sans Serif", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=58, Width=808, Left=2, Top=523, ReadOnly=.T.

  func oDatiAttiPratica_1_40.mHide()
    with this.Parent.oContained
      return (.w_CalAttivo#3)
    endwith
  endfunc


  add object ctList_Memo as cp_ctList with uid="UYKQQCODQG",left=585, top=177, width=223,height=345,;
    caption='ctList_Memo',;
   bGlobalFont=.t.,;
    FontSize=8,serial="w_Serial",FontName="MS Sans Serif",HeadrText="",ItemSel="w_ItemSel",ColumnSel="w_ClnSel",HeaderFillType=0,HeaderBorder=0,VertGridLines=.t.,;
    nPag=1;
    , HelpContextID = 62617283


  add object ctList_Giorno as cp_ctList with uid="YDSGZQVXNO",left=439, top=43, width=369,height=536,;
    caption='ctList_Giorno',;
   bGlobalFont=.t.,;
    FontSize=8,serial="w_Serial",FontName="MS Sans Serif",HeadrText="",ItemSel="w_ItemSel",ColumnSel="w_ClnSel",HeaderFillType=0,HeaderBorder=0,VertGridLines=.t.,;
    nPag=1;
    , HelpContextID = 186231037


  add object oBtn_1_65 as StdButton with uid="ODQLJUAFBO",left=1, top=10, width=23,height=25,;
    CpPicture="bitmap.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza la legenda dei colori";
    , HelpContextID = 132785930;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_65.Click()
      do gsag_kle with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_69 as cp_runprogram with uid="YFYWYIYODF",left=834, top=487, width=212,height=25,;
    caption='RGIHTCLICK',;
   bGlobalFont=.t.,;
    prg="GSAG_BAG('RIGHTCLICK')",;
    cEvent = "RightClick_CtMDay",;
    nPag=1;
    , HelpContextID = 257751921


  add object oBtn_1_71 as StdButton with uid="JNGXETNHZS",left=501, top=10, width=23,height=25,;
    CpPicture="disptemp.ico", caption="", nPag=1;
    , ToolTipText = "Visualizzazione delle attivit� in scadenza termine";
    , HelpContextID = 56779510;
    , ImgSize = 16,Forecolor=15181930;
    , FontName = "MS Sans Serif", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_71.Click()
      with this.Parent.oContained
        GSZM_BAT(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_71.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_72 as StdButton with uid="FDFUARMSLQ",left=525, top=10, width=23,height=25,;
    CpPicture="esplodi.ico", caption="", nPag=1;
    , ToolTipText = "Visualizzazione delle attivit� scadute";
    , HelpContextID = 77573850;
    , ImgSize = 16,Forecolor=15181930;
    , FontName = "MS Sans Serif", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_72.Click()
      with this.Parent.oContained
        GSZM_BAT(this.Parent.oContained,"O")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_72.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_73 as StdButton with uid="NSCPYCIXUW",left=47, top=10, width=23,height=25,;
    CpPicture="print.ico", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa della visualizzazione corrente";
    , HelpContextID = 77573850;
    , ImgSize = 16,Forecolor=15181930;
    , FontName = "MS Sans Serif", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_73.Click()
      with this.Parent.oContained
        gsag_bag(this.Parent.oContained,"STAMPAAG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_73.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CalAttivo=4)
     endwith
    endif
  endfunc


  add object oTmrBalloon as cp_Timer with uid="TRBAAIZYFF",left=506, top=690, width=172,height=19,;
    caption='oTmrBalloon',;
   bGlobalFont=.t.,;
    nSeconds=0,cStartAt="",cStopAt="",nTimes=0,;
    cEvent = "tmrBalloon",;
    nPag=1;
    , HelpContextID = 38380990

  add object oStr_1_70 as StdString with uid="INKLGBUQMF",Visible=.t., Left=385, Top=600,;
    Alignment=0, Width=410, Height=18,;
    Caption="ImgNote, ImgDaFare e ImgPreav sono dichiarate in A.M. Declare Variabiles"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc
enddefine
define class tgsag_kagPag2 as StdContainer
  Width  = 812
  height = 612
  stdWidth  = 812
  stdheight = 612
  resizeXpos=430
  resizeYpos=242
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPATIPRIS_2_1 as StdCombo with uid="FEFTOWFWCM",value=4,rtseq=32,rtrep=.f.,left=106,top=14,width=76,height=21;
    , HelpContextID = 10199223;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPATIPRIS_2_1.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    iif(this.value =3,'R',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oPATIPRIS_2_1.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_2_1.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',3,;
      iif(this.Parent.oContained.w_PATIPRIS=='',4,;
      0))))
  endfunc

  func oPATIPRIS_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Parametro='G')
    endwith
   endif
  endfunc

  add object oCODPART_2_3 as StdField with uid="QJYWRUDDRU",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CODPART", cQueryName = "CODPART",nZero=5,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice partecipante",;
    HelpContextID = 242904102,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=193, Top=14, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_CODPART"

  func oCODPART_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Parametro='G')
    endwith
   endif
  endfunc

  func oCODPART_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPART_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPART_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oCODPART_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this.parent.oContained
  endproc


  add object oSTATO_2_4 as StdCombo with uid="NMMVFNUTTX",rtseq=34,rtrep=.f.,left=106,top=74,width=125,height=21;
    , ToolTipText = "Stato";
    , HelpContextID = 44154330;
    , cFormVar="w_STATO",RowSource=""+"Non evasa,"+"Da svolgere,"+"In corso,"+"Provvisoria,"+"Evasa,"+"Completata,"+"Evasa o completata,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oSTATO_2_4.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'T',;
    iif(this.value =5,'F',;
    iif(this.value =6,'P',;
    iif(this.value =7,'B',;
    iif(this.value =8,'K',;
    space(1))))))))))
  endfunc
  func oSTATO_2_4.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_2_4.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='N',1,;
      iif(this.Parent.oContained.w_STATO=='D',2,;
      iif(this.Parent.oContained.w_STATO=='I',3,;
      iif(this.Parent.oContained.w_STATO=='T',4,;
      iif(this.Parent.oContained.w_STATO=='F',5,;
      iif(this.Parent.oContained.w_STATO=='P',6,;
      iif(this.Parent.oContained.w_STATO=='B',7,;
      iif(this.Parent.oContained.w_STATO=='K',8,;
      0))))))))
  endfunc

  func oSTATO_2_4.mHide()
    with this.Parent.oContained
      return (.w_RISERVE='S')
    endwith
  endfunc


  add object oRISERVE_2_5 as StdCombo with uid="MIFARZNGXZ",rtseq=35,rtrep=.f.,left=235,top=74,width=106,height=21;
    , ToolTipText = "Considera o meno tutte le attivit� in riserva a prescindere dai filtri sullo stato e sulle date";
    , HelpContextID = 209692906;
    , cFormVar="w_RISERVE",RowSource=""+"Escludi riserve,"+"Aggiungi riserve,"+"No filtro riserve,"+"Solo riserve", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oRISERVE_2_5.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'A',;
    iif(this.value =3,'L',;
    iif(this.value =4,'S',;
    space(1))))))
  endfunc
  func oRISERVE_2_5.GetRadio()
    this.Parent.oContained.w_RISERVE = this.RadioValue()
    return .t.
  endfunc

  func oRISERVE_2_5.SetRadio()
    this.Parent.oContained.w_RISERVE=trim(this.Parent.oContained.w_RISERVE)
    this.value = ;
      iif(this.Parent.oContained.w_RISERVE=='E',1,;
      iif(this.Parent.oContained.w_RISERVE=='A',2,;
      iif(this.Parent.oContained.w_RISERVE=='L',3,;
      iif(this.Parent.oContained.w_RISERVE=='S',4,;
      0))))
  endfunc

  func oRISERVE_2_5.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDENOM_PART_2_8 as StdField with uid="SKXCKTCAOH",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(48), bMultilanguage =  .f.,;
    HelpContextID = 205151127,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=263, Top=14, InputMask=replicate('X',48)

  add object oFLESGR_2_10 as StdCheck with uid="AYKWNFTPEF",rtseq=37,rtrep=.f.,left=576, top=14, caption="Esplodi gruppo",;
    ToolTipText = "Se attivo visualizza anche appuntamenti delle persone del gruppo",;
    HelpContextID = 249395542,;
    cFormVar="w_FLESGR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLESGR_2_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLESGR_2_10.GetRadio()
    this.Parent.oContained.w_FLESGR = this.RadioValue()
    return .t.
  endfunc

  func oFLESGR_2_10.SetRadio()
    this.Parent.oContained.w_FLESGR=trim(this.Parent.oContained.w_FLESGR)
    this.value = ;
      iif(this.Parent.oContained.w_FLESGR=='S',1,;
      0)
  endfunc

  func oFLESGR_2_10.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'G' or Empty(.w_CODPART))
    endwith
  endfunc

  add object oGRUPART_2_11 as StdField with uid="AYUXRJLLLH",rtseq=38,rtrep=.f.,;
    cFormVar = "w_GRUPART", cQueryName = "GRUPART",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo inesistente o non associato al partecipante",;
    HelpContextID = 242974566,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=193, Top=44, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_GRUPART"

  func oGRUPART_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PATIPRIS='P' OR .w_PATIPRIS='T')
    endwith
   endif
  endfunc

  func oGRUPART_2_11.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  func oGRUPART_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPART_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPART_2_11.mZoom
      with this.Parent.oContained
        GSAR1BGP(this.Parent.oContained,  "", .w_CODPART, "", "GRUPART" )
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oPRIORITA_2_12 as StdCombo with uid="TPBXXBSCEW",value=4,rtseq=39,rtrep=.f.,left=389,top=74,width=118,height=21;
    , ToolTipText = "Priorit�";
    , HelpContextID = 109690935;
    , cFormVar="w_PRIORITA",RowSource=""+"Normale,"+"Urgente,"+"Scadenza termine,"+"Tutte", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPRIORITA_2_12.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,3,;
    iif(this.value =3,4,;
    iif(this.value =4,0,;
    0)))))
  endfunc
  func oPRIORITA_2_12.GetRadio()
    this.Parent.oContained.w_PRIORITA = this.RadioValue()
    return .t.
  endfunc

  func oPRIORITA_2_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PRIORITA==1,1,;
      iif(this.Parent.oContained.w_PRIORITA==3,2,;
      iif(this.Parent.oContained.w_PRIORITA==4,3,;
      iif(this.Parent.oContained.w_PRIORITA==0,4,;
      0))))
  endfunc


  add object oTIPORIS_2_13 as StdTableCombo with uid="XMEKXWDJPL",rtseq=40,rtrep=.f.,left=628,top=74,width=118,height=21;
    , HelpContextID = 109717302;
    , cFormVar="w_TIPORIS",tablefilter="", bObbl = .f. , nPag = 2;
    , cLinkFile="TIP_RISE";
    , cTable='TIP_RISE',cKey='TRCODICE',cValue='TRDESTIP',cOrderBy='TRDESTIP',xDefault=space(1);
  , bGlobalFont=.t.


  func oTIPORIS_2_13.mHide()
    with this.Parent.oContained
      return (.w_RISERVE<> 'S' or not IsAlt())
    endwith
  endfunc

  func oTIPORIS_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPORIS_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oCACODICE_2_14 as StdField with uid="MBGICDMGLD",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CACODICE", cQueryName = "CACODICE",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 94981739,;
   bGlobalFont=.t.,;
    Height=21, Width=171, Left=106, Top=100, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CACODICE"

  func oCACODICE_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODICE_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODICE_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oCACODICE_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc

  add object oCADESCRI_2_15 as AH_SEARCHFLD with uid="ZSNGVSJHNR",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CADESCRI", cQueryName = "CADESCRI",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione oggetto o parte di essa",;
    HelpContextID = 9395823,;
   bGlobalFont=.t.,;
    Height=21, Width=357, Left=389, Top=101, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oCADESCRI_2_15.mZoom
    vx_exec("query\GSAG1KRA.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oATTSTU_2_16 as StdCheck with uid="FUHPBAIQSC",rtseq=43,rtrep=.f.,left=108, top=189, caption="Appuntamenti",;
    ToolTipText = "Se attivo: visualizza gli appuntamenti",;
    HelpContextID = 44986630,;
    cFormVar="w_ATTSTU", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATTSTU_2_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oATTSTU_2_16.GetRadio()
    this.Parent.oContained.w_ATTSTU = this.RadioValue()
    return .t.
  endfunc

  func oATTSTU_2_16.SetRadio()
    this.Parent.oContained.w_ATTSTU=trim(this.Parent.oContained.w_ATTSTU)
    this.value = ;
      iif(this.Parent.oContained.w_ATTSTU=='S',1,;
      0)
  endfunc

  add object oNUMDOC_2_17 as StdField with uid="ALSVTWVYIC",rtseq=44,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'attivit�",;
    HelpContextID = 5178070,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=106, Top=161, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMDOC_2_17.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oALFDOC_2_18 as StdField with uid="TNHYATACTB",rtseq=45,rtrep=.f.,;
    cFormVar = "w_ALFDOC", cQueryName = "ALFDOC",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie dell'attivit�",;
    HelpContextID = 5146886,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=246, Top=161, InputMask=replicate('X',10)

  func oALFDOC_2_18.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oCODNOM_2_19 as StdField with uid="LDORSNJAGM",rtseq=46,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo o soggetto esterno della pratica",;
    HelpContextID = 173567014,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=106, Top=131, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_GIUDICE ))
    endwith
   endif
  endfunc

  func oCODNOM_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc


  add object oCODCAT_2_20 as StdTableCombo with uid="AXUHUESMBU",rtseq=47,rtrep=.f.,left=648,top=131,width=134,height=21;
    , ToolTipText = "Ruolo del nominativo (soggetti esterni)";
    , HelpContextID = 7171110;
    , cFormVar="w_CODCAT",tablefilter="", bObbl = .f. , nPag = 2;
    , sErrorMsg = "Codice categoria soggetti inesistente o di tipo cliente";
    , cLinkFile="CAT_SOGG";
    , cTable='CAT_SOGG',cKey='CSCODCAT',cValue='CSDESCAT',cOrderBy='CSDESCAT',xDefault=space(10);
  , bGlobalFont=.t.


  func oCODCAT_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODNOM))
    endwith
   endif
  endfunc

  func oCODCAT_2_20.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  func oCODCAT_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oATTGENER_2_21 as StdCheck with uid="KLBNRLNCJC",rtseq=48,rtrep=.f.,left=108, top=209, caption="Attivit� generiche",;
    ToolTipText = "Se attivo: visualizza le attivit� generiche",;
    HelpContextID = 179466584,;
    cFormVar="w_ATTGENER", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATTGENER_2_21.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oATTGENER_2_21.GetRadio()
    this.Parent.oContained.w_ATTGENER = this.RadioValue()
    return .t.
  endfunc

  func oATTGENER_2_21.SetRadio()
    this.Parent.oContained.w_ATTGENER=trim(this.Parent.oContained.w_ATTGENER)
    this.value = ;
      iif(this.Parent.oContained.w_ATTGENER=='G',1,;
      0)
  endfunc

  add object oUDIENZE_2_22 as StdCheck with uid="HBQKVBXHMX",rtseq=49,rtrep=.f.,left=108, top=229, caption="Udienze",;
    ToolTipText = "Se attivo: visualizza le udienze",;
    HelpContextID = 146820538,;
    cFormVar="w_UDIENZE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oUDIENZE_2_22.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oUDIENZE_2_22.GetRadio()
    this.Parent.oContained.w_UDIENZE = this.RadioValue()
    return .t.
  endfunc

  func oUDIENZE_2_22.SetRadio()
    this.Parent.oContained.w_UDIENZE=trim(this.Parent.oContained.w_UDIENZE)
    this.value = ;
      iif(this.Parent.oContained.w_UDIENZE=='U',1,;
      0)
  endfunc

  func oUDIENZE_2_22.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oSESSTEL_2_23 as StdCheck with uid="KYMNVXZYIW",rtseq=50,rtrep=.f.,left=294, top=189, caption="Sessioni telefoniche",;
    ToolTipText = "Se attivo: visualizza le sessioni telefoniche",;
    HelpContextID = 223456474,;
    cFormVar="w_SESSTEL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSESSTEL_2_23.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oSESSTEL_2_23.GetRadio()
    this.Parent.oContained.w_SESSTEL = this.RadioValue()
    return .t.
  endfunc

  func oSESSTEL_2_23.SetRadio()
    this.Parent.oContained.w_SESSTEL=trim(this.Parent.oContained.w_SESSTEL)
    this.value = ;
      iif(this.Parent.oContained.w_SESSTEL=='T',1,;
      0)
  endfunc

  add object oASSENZE_2_24 as StdCheck with uid="RPXCBGRCCL",rtseq=51,rtrep=.f.,left=294, top=209, caption="Assenze",;
    ToolTipText = "Se attivo: visualizza le assenze",;
    HelpContextID = 146776058,;
    cFormVar="w_ASSENZE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oASSENZE_2_24.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oASSENZE_2_24.GetRadio()
    this.Parent.oContained.w_ASSENZE = this.RadioValue()
    return .t.
  endfunc

  func oASSENZE_2_24.SetRadio()
    this.Parent.oContained.w_ASSENZE=trim(this.Parent.oContained.w_ASSENZE)
    this.value = ;
      iif(this.Parent.oContained.w_ASSENZE=='A',1,;
      0)
  endfunc

  add object oDAINSPRE_2_25 as StdCheck with uid="AMMYJSOHVW",rtseq=52,rtrep=.f.,left=294, top=229, caption="Da inserimento prestazioni",;
    ToolTipText = "Se attivo: visualizza le attivit� generate dall'inserimento delle prestazioni",;
    HelpContextID = 228109947,;
    cFormVar="w_DAINSPRE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDAINSPRE_2_25.RadioValue()
    return(iif(this.value =1,'Z',;
    ' '))
  endfunc
  func oDAINSPRE_2_25.GetRadio()
    this.Parent.oContained.w_DAINSPRE = this.RadioValue()
    return .t.
  endfunc

  func oDAINSPRE_2_25.SetRadio()
    this.Parent.oContained.w_DAINSPRE=trim(this.Parent.oContained.w_DAINSPRE)
    this.value = ;
      iif(this.Parent.oContained.w_DAINSPRE=='Z',1,;
      0)
  endfunc

  add object oMEMO_2_26 as StdCheck with uid="ELRSHFYRWO",rtseq=53,rtrep=.f.,left=491, top=189, caption="Note",;
    ToolTipText = "Se attivo: visualizza le note",;
    HelpContextID = 127274298,;
    cFormVar="w_MEMO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMEMO_2_26.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oMEMO_2_26.GetRadio()
    this.Parent.oContained.w_MEMO = this.RadioValue()
    return .t.
  endfunc

  func oMEMO_2_26.SetRadio()
    this.Parent.oContained.w_MEMO=trim(this.Parent.oContained.w_MEMO)
    this.value = ;
      iif(this.Parent.oContained.w_MEMO=='M',1,;
      0)
  endfunc

  add object oDAFARE_2_27 as StdCheck with uid="JYTLCSHGGN",rtseq=54,rtrep=.f.,left=491, top=209, caption="Cose da fare",;
    ToolTipText = "Se attivo: visualizza le cose da fare",;
    HelpContextID = 41647670,;
    cFormVar="w_DAFARE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDAFARE_2_27.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oDAFARE_2_27.GetRadio()
    this.Parent.oContained.w_DAFARE = this.RadioValue()
    return .t.
  endfunc

  func oDAFARE_2_27.SetRadio()
    this.Parent.oContained.w_DAFARE=trim(this.Parent.oContained.w_DAFARE)
    this.value = ;
      iif(this.Parent.oContained.w_DAFARE=='D',1,;
      0)
  endfunc

  add object oFiltroWeb_2_28 as StdRadio with uid="NOISYYKDWQ",rtseq=55,rtrep=.f.,left=632, top=189, width=156,height=49;
    , ToolTipText = "Filtro su flag di pubblicazione su Web";
    , cFormVar="w_FiltroWeb", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oFiltroWeb_2_28.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo pubblicabili"
      this.Buttons(1).HelpContextID = 21951269
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo non pubblicabili"
      this.Buttons(2).HelpContextID = 21951269
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 21951269
      this.Buttons(3).Top=30
      this.SetAll("Width",154)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Filtro su flag di pubblicazione su Web")
      StdRadio::init()
    endproc

  func oFiltroWeb_2_28.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFiltroWeb_2_28.GetRadio()
    this.Parent.oContained.w_FiltroWeb = this.RadioValue()
    return .t.
  endfunc

  func oFiltroWeb_2_28.SetRadio()
    this.Parent.oContained.w_FiltroWeb=trim(this.Parent.oContained.w_FiltroWeb)
    this.value = ;
      iif(this.Parent.oContained.w_FiltroWeb=='S',1,;
      iif(this.Parent.oContained.w_FiltroWeb=='N',2,;
      iif(this.Parent.oContained.w_FiltroWeb=='T',3,;
      0)))
  endfunc

  func oFiltroWeb_2_28.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oDISPONIB_2_29 as StdCombo with uid="MRAFDOVVBK",value=5,rtseq=56,rtrep=.f.,left=106,top=254,width=118,height=21;
    , ToolTipText = "Disponibilit�";
    , HelpContextID = 77900168;
    , cFormVar="w_DISPONIB",RowSource=""+"Occupato,"+"Per urgenze,"+"Libero,"+"Fuori sede,"+"Tutte", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDISPONIB_2_29.RadioValue()
    return(iif(this.value =1,2,;
    iif(this.value =2,3,;
    iif(this.value =3,1,;
    iif(this.value =4,4,;
    iif(this.value =5,0,;
    0))))))
  endfunc
  func oDISPONIB_2_29.GetRadio()
    this.Parent.oContained.w_DISPONIB = this.RadioValue()
    return .t.
  endfunc

  func oDISPONIB_2_29.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DISPONIB==2,1,;
      iif(this.Parent.oContained.w_DISPONIB==3,2,;
      iif(this.Parent.oContained.w_DISPONIB==1,3,;
      iif(this.Parent.oContained.w_DISPONIB==4,4,;
      iif(this.Parent.oContained.w_DISPONIB==0,5,;
      0)))))
  endfunc

  add object oFiltPerson_2_30 as AH_SEARCHFLD with uid="MYRJGKWLFV",rtseq=57,rtrep=.f.,;
    cFormVar = "w_FiltPerson", cQueryName = "FiltPerson",;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Persona o parte di essa",;
    HelpContextID = 43088825,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=349, Top=254, InputMask=replicate('X',60)

  add object oCODTIPOL_2_31 as StdField with uid="SKMPHTYBJO",rtseq=58,rtrep=.f.,;
    cFormVar = "w_CODTIPOL", cQueryName = "CODTIPOL",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia",;
    HelpContextID = 218000498,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=283, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="OFFTIPAT", oKey_1_1="TACODTIP", oKey_1_2="this.w_CODTIPOL"

  func oCODTIPOL_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODTIPOL_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODTIPOL_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFFTIPAT','*','TACODTIP',cp_AbsName(this.parent,'oCODTIPOL_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipologie attivit�",'',this.parent.oContained
  endproc

  add object oNOTE_2_32 as AH_SEARCHMEMO with uid="YFIDICHIXZ",rtseq=59,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Annotazioni o parte di esse",;
    HelpContextID = 127898410,;
   bGlobalFont=.t.,;
    Height=21, Width=676, Left=106, Top=315

  add object oGIUDICE_2_33 as StdField with uid="FJVJFDFGGP",rtseq=60,rtrep=.f.,;
    cFormVar = "w_GIUDICE", cQueryName = "GIUDICE",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Nominativo inesistente o obsoleto non associato come giudice ad alcuna pratica",;
    ToolTipText = "Se specificato filtra le attivit� associate a pratiche con data fine compresa all'interno dell'intervallo di validiit� del giudice",;
    HelpContextID = 267351654,;
   bGlobalFont=.t.,;
    Height=21, Width=138, Left=107, Top=346, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_GIUDICE"

  func oGIUDICE_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_CODNOM ))
    endwith
   endif
  endfunc

  func oGIUDICE_2_33.mHide()
    with this.Parent.oContained
      return (not isalt())
    endwith
  endfunc

  func oGIUDICE_2_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oGIUDICE_2_33.ecpDrop(oSource)
    this.Parent.oContained.link_2_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGIUDICE_2_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oGIUDICE_2_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Giudici",'GSPR_ZGI.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oGIUDICE_2_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_GIUDICE
     i_obj.ecpSave()
  endproc

  add object oATLOCALI_2_34 as AH_SEARCHFLD with uid="KNSKLUNXGJ",rtseq=61,rtrep=.f.,;
    cFormVar = "w_ATLOCALI", cQueryName = "ATLOCALI",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� o parte di essa",;
    HelpContextID = 40242865,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=106, Top=377, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oATLOCALI_2_34.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_ATLOCALI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oENTE_2_35 as StdField with uid="NHZLOLJQPN",rtseq=62,rtrep=.f.,;
    cFormVar = "w_ENTE", cQueryName = "ENTE",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente/ufficio giudiziario",;
    HelpContextID = 127898810,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=106, Top=408, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRA_ENTI", oKey_1_1="EPCODICE", oKey_1_2="this.w_ENTE"

  func oENTE_2_35.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  func oENTE_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oENTE_2_35.ecpDrop(oSource)
    this.Parent.oContained.link_2_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oENTE_2_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRA_ENTI','*','EPCODICE',cp_AbsName(this.parent,'oENTE_2_35'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Enti pratica",'',this.parent.oContained
  endproc

  add object oIMPIANTO_2_36 as StdField with uid="WYZQFLHLBI",rtseq=63,rtrep=.f.,;
    cFormVar = "w_IMPIANTO", cQueryName = "IMPIANTO",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice impianto",;
    HelpContextID = 175385301,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=106, Top=408, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", cZoomOnZoom="GSAG_MIM", oKey_1_1="IMCODICE", oKey_1_2="this.w_IMPIANTO"

  func oIMPIANTO_2_36.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  func oIMPIANTO_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_36('Part',this)
      if .not. empty(.w_COMPIMPKEYREAD)
        bRes2=.link_2_91('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIMPIANTO_2_36.ecpDrop(oSource)
    this.Parent.oContained.link_2_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMPIANTO_2_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oIMPIANTO_2_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIM',"Impianto",'GSAG_AAT.IMP_MAST_VZM',this.parent.oContained
  endproc
  proc oIMPIANTO_2_36.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_IMPIANTO
     i_obj.ecpSave()
  endproc

  add object oDesEnte_2_37 as StdField with uid="LTVJZFGKNZ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DesEnte", cQueryName = "DesEnte",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 213749194,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=217, Top=408, InputMask=replicate('X',60)

  func oDesEnte_2_37.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oCOMPIMP_2_38 as StdField with uid="GOVKVGTSZB",rtseq=65,rtrep=.f.,;
    cFormVar = "w_COMPIMP", cQueryName = "COMPIMP",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Componente dell'impianto",;
    HelpContextID = 167443494,;
   bGlobalFont=.t.,;
    Height=21, Width=465, Left=318, Top=408, InputMask=replicate('X',50), bHasZoom = .t. 

  func oCOMPIMP_2_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_IMPIANTO))
    endwith
   endif
  endfunc

  func oCOMPIMP_2_38.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  proc oCOMPIMP_2_38.mBefore
    with this.Parent.oContained
      .w_OLDCOMP=.w_COMPIMP
    endwith
  endproc

  proc oCOMPIMP_2_38.mAfter
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M")
      endwith
  endproc

  proc oCOMPIMP_2_38.mZoom
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M",.T.)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oUFFICIO_2_39 as StdField with uid="NQCZTZGKLZ",rtseq=66,rtrep=.f.,;
    cFormVar = "w_UFFICIO", cQueryName = "UFFICIO",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice ufficio/sezione",;
    HelpContextID = 93553734,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=106, Top=439, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRA_UFFI", oKey_1_1="UFCODICE", oKey_1_2="this.w_UFFICIO"

  func oUFFICIO_2_39.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  func oUFFICIO_2_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oUFFICIO_2_39.ecpDrop(oSource)
    this.Parent.oContained.link_2_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUFFICIO_2_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRA_UFFI','*','UFCODICE',cp_AbsName(this.parent,'oUFFICIO_2_39'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Uffici",'',this.parent.oContained
  endproc

  add object oCENCOS_2_41 as StdField with uid="LTYDLVFPON",rtseq=67,rtrep=.f.,;
    cFormVar = "w_CENCOS", cQueryName = "CENCOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "filtro relativo al centro di costo/ricavo inserito nei dati di testata",;
    HelpContextID = 5112358,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=106, Top=470, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENCOS"

  func oCENCOS_2_41.mHide()
    with this.Parent.oContained
      return (g_COAN <> 'S')
    endwith
  endfunc

  func oCENCOS_2_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oCENCOS_2_41.ecpDrop(oSource)
    this.Parent.oContained.link_2_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCENCOS_2_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCENCOS_2_41'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCENCOS_2_41.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CENCOS
     i_obj.ecpSave()
  endproc

  add object oCODPRA_2_42 as StdField with uid="LNZCMMZIKS",rtseq=68,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 243952678,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=106, Top=501, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="gsar_bzz", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_2_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITCODPRA)
    endwith
   endif
  endfunc

  func oCODPRA_2_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRA_2_42.ecpDrop(oSource)
    this.Parent.oContained.link_2_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRA_2_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRA_2_42'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_bzz',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODPRA_2_42.mZoomOnZoom
    local i_obj
    i_obj=gsar_bzz()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRA
     i_obj.ecpSave()
  endproc

  add object oCodUte_2_43 as StdField with uid="PIUVUYSBHD",rtseq=69,rtrep=.f.,;
    cFormVar = "w_CodUte", cQueryName = "CodUte",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente ultima modifica",;
    HelpContextID = 78744614,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=106, Top=532, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CodUte"

  func oCodUte_2_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodUte_2_43.ecpDrop(oSource)
    this.Parent.oContained.link_2_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodUte_2_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCodUte_2_43'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDESPRA_2_44 as StdField with uid="DHZPCSQKFH",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(75), bMultilanguage =  .f.,;
    HelpContextID = 244011574,;
   bGlobalFont=.t.,;
    Height=21, Width=531, Left=251, Top=501, InputMask=replicate('X',75)

  add object oDesUffi_2_46 as StdField with uid="PFQCMQCJNZ",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DesUffi", cQueryName = "DesUffi",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 187534794,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=216, Top=439, InputMask=replicate('X',60)

  func oDesUffi_2_46.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oDESUTE_2_48 as StdField with uid="JXGTZBFHCK",rtseq=72,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 45109814,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=181, Top=532, InputMask=replicate('X',20)

  add object oDATINI_Cose_2_52 as StdField with uid="QWTXKWCFBP",rtseq=73,rtrep=.f.,;
    cFormVar = "w_DATINI_Cose", cQueryName = "DATINI_Cose",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione per note e cose da fare",;
    HelpContextID = 105588841,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=106, Top=562

  add object oDATFIN_Cose_2_53 as StdField with uid="DETMKSVCXW",rtseq=74,rtrep=.f.,;
    cFormVar = "w_DATFIN_Cose", cQueryName = "DATFIN_Cose",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione per note e cose da fare",;
    HelpContextID = 184035433,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=212, Top=562

  func oDATFIN_Cose_2_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI_Cose<=.w_DATFIN_Cose OR EMPTY(.w_DATFIN_Cose))
    endwith
    return bRes
  endfunc

  add object oVisNote_2_54 as StdCheck with uid="FFPQECHGFO",rtseq=75,rtrep=.f.,left=106, top=588, caption="Espandi la visualizzazione di tutte le attivit�",;
    ToolTipText = "Se attivo: espande la visualizzazione delle attivit� del giorno (in visualizzazione mensile e annuale)",;
    HelpContextID = 212109482,;
    cFormVar="w_VisNote", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVisNote_2_54.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVisNote_2_54.GetRadio()
    this.Parent.oContained.w_VisNote = this.RadioValue()
    return .t.
  endfunc

  func oVisNote_2_54.SetRadio()
    this.Parent.oContained.w_VisNote=trim(this.Parent.oContained.w_VisNote)
    this.value = ;
      iif(this.Parent.oContained.w_VisNote=='S',1,;
      0)
  endfunc

  add object oStrPratica_2_55 as StdCheck with uid="EIZUKGJYSZ",rtseq=76,rtrep=.f.,left=491, top=588, caption="Abbrevia la dicitura Pratica in Pr",;
    ToolTipText = "Se attivo: abbrevia la dicitura Pratica in Pr in visualizzazione 1 giorno o 5 giorni",;
    HelpContextID = 259139137,;
    cFormVar="w_StrPratica", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oStrPratica_2_55.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oStrPratica_2_55.GetRadio()
    this.Parent.oContained.w_StrPratica = this.RadioValue()
    return .t.
  endfunc

  func oStrPratica_2_55.SetRadio()
    this.Parent.oContained.w_StrPratica=trim(this.Parent.oContained.w_StrPratica)
    this.value = ;
      iif(this.Parent.oContained.w_StrPratica=='S',1,;
      0)
  endfunc

  func oStrPratica_2_55.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc


  add object oBtn_2_56 as StdButton with uid="PHEWMSSLEL",left=759, top=14, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=2;
    , ToolTipText = "Ricerca";
    , HelpContextID = 44136214;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_56.Click()
      with this.Parent.oContained
        gsag_bag(this.Parent.oContained,"RICERCA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESNOM_2_58 as StdField with uid="YBKTMCLSXL",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 173625910,;
   bGlobalFont=.t.,;
    Height=21, Width=358, Left=246, Top=131, InputMask=replicate('X',40)

  add object oDESTIPOL_2_61 as StdField with uid="XDLBTHYTYA",rtseq=78,rtrep=.f.,;
    cFormVar = "w_DESTIPOL", cQueryName = "DESTIPOL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 218059394,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=181, Top=283, InputMask=replicate('X',50)

  add object oDPDESGRU_2_64 as StdField with uid="LGRUONCFGR",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DPDESGRU", cQueryName = "DPDESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 76508555,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=263, Top=44, InputMask=replicate('X',40)

  func oDPDESGRU_2_64.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  add object oDESPIA_2_68 as StdField with uid="RDERHTGXNR",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DESPIA", cQueryName = "DESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 234574390,;
   bGlobalFont=.t.,;
    Height=21, Width=531, Left=251, Top=470, InputMask=replicate('X',40)

  func oDESPIA_2_68.mHide()
    with this.Parent.oContained
      return (g_COAN <> 'S')
    endwith
  endfunc

  add object oCODSED_2_70 as StdField with uid="DGJQCXNDEJ",rtseq=81,rtrep=.f.,;
    cFormVar = "w_CODSED", cQueryName = "CODSED",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Sede",;
    HelpContextID = 12413990,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=106, Top=347, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", cZoomOnZoom="GSAR_MDD", oKey_1_1="DDTIPCON", oKey_1_2="this.w_TIPNOM", oKey_2_1="DDCODICE", oKey_2_2="this.w_CODCLI", oKey_3_1="DDCODDES", oKey_3_2="this.w_CODSED"

  func oCODSED_2_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPNOM='C')
    endwith
   endif
  endfunc

  func oCODSED_2_70.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  func oCODSED_2_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_70('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSED_2_70.ecpDrop(oSource)
    this.Parent.oContained.link_2_70('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSED_2_70.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPNOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPNOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CODCLI)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCODSED_2_70'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MDD',"Sedi",'',this.parent.oContained
  endproc
  proc oCODSED_2_70.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MDD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DDTIPCON=w_TIPNOM
    i_obj.DDCODICE=w_CODCLI
     i_obj.w_DDCODDES=this.parent.oContained.w_CODSED
     i_obj.ecpSave()
  endproc

  add object oDESCRI_2_71 as StdField with uid="FFAMWOLEDT",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 108941878,;
   bGlobalFont=.t.,;
    Height=21, Width=530, Left=253, Top=347, InputMask=replicate('X',40)

  func oDESCRI_2_71.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc


  add object oObj_2_75 as cp_runprogram with uid="VZHVOFMYMD",left=15, top=652, width=182,height=25,;
    caption='gsag_bag(RICERCA)',;
   bGlobalFont=.t.,;
    prg="gsag_bag('RICERCA')",;
    cEvent = "Ricalcola",;
    nPag=2;
    , HelpContextID = 150255074


  add object oTIPANA_2_76 as StdCombo with uid="OOYOOVQSKI",rtseq=83,rtrep=.f.,left=106,top=439,width=146,height=21;
    , ToolTipText = "Natura";
    , HelpContextID = 238823222;
    , cFormVar="w_TIPANA",RowSource=""+"Costo,"+"Ricavo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPANA_2_76.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPANA_2_76.GetRadio()
    this.Parent.oContained.w_TIPANA = this.RadioValue()
    return .t.
  endfunc

  func oTIPANA_2_76.SetRadio()
    this.Parent.oContained.w_TIPANA=trim(this.Parent.oContained.w_TIPANA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPANA=='C',1,;
      iif(this.Parent.oContained.w_TIPANA=='R',2,;
      0))
  endfunc

  func oTIPANA_2_76.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oDESNOMGIU_2_78 as StdField with uid="OVSVURVTXJ",rtseq=84,rtrep=.f.,;
    cFormVar = "w_DESNOMGIU", cQueryName = "DESNOMGIU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 94808113,;
   bGlobalFont=.t.,;
    Height=21, Width=531, Left=251, Top=346, InputMask=replicate('X',60)

  func oDESNOMGIU_2_78.mHide()
    with this.Parent.oContained
      return (Not isAlt())
    endwith
  endfunc


  add object oBtn_2_79 as StdButton with uid="MBXAIGGBZE",left=758, top=62, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per effettuare la stampa della visualizzazione corrente";
    , HelpContextID = 9003558;
    , caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_79.Click()
      with this.Parent.oContained
        gsag_bag(this.Parent.oContained,"STAMPAAG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_79.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CalAttivo=4)
     endwith
    endif
  endfunc


  add object oObj_2_81 as cp_runprogram with uid="WQOFRYVNRB",left=205, top=652, width=209,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsag_bsa",;
    cEvent = "StampaGraf",;
    nPag=2;
    , HelpContextID = 45211622

  add object oCOMPIMPKEY_2_92 as StdField with uid="SBSCAGJTGI",rtseq=94,rtrep=.f.,;
    cFormVar = "w_COMPIMPKEY", cQueryName = "COMPIMPKEY",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 167467457,;
   bGlobalFont=.t.,;
    Height=22, Width=94, Left=-9, Top=200

  func oCOMPIMPKEY_2_92.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc


  add object ctDropDate as cp_ctDropDate with uid="VJEBHTZIVS",left=828, top=273, width=90,height=20,;
    caption='ctDropDate',;
   bGlobalFont=.t.,;
    StartMonth=0,StartYear=0,CalForeColor=0,SelectBackColor=0,CalHeaderColor=0,CalTitleColor=0,var="w_DataSel",CalTodayColor=0,CalLevelColor=0,CalBackColor=0,CalSelectBackColor=0,BackColor=0,DayNames="",FontName="MS Sans Serif",FontSize=8,TodayText="",MonthNames="",;
    nPag=2;
    , HelpContextID = 8585273


  add object oBtn_2_131 as StdButton with uid="MNVZAMDXTB",left=206, top=408, width=22,height=21,;
    CpPicture="bmp\ZOOM.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per ricercare per attributo";
    , HelpContextID = 132585002;
    , IMGSIZE=16;
  , bGlobalFont=.t.

    proc oBtn_2_131.Click()
      do gsag_kir with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_131.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsALT())
     endwith
    endif
  endfunc

  add object oStr_2_2 as StdString with uid="JGUELPQNWG",Visible=.t., Left=3, Top=75,;
    Alignment=1, Width=100, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  func oStr_2_2.mHide()
    with this.Parent.oContained
      return (.w_RISERVE='S')
    endwith
  endfunc

  add object oStr_2_6 as StdString with uid="UQSAZJMDFM",Visible=.t., Left=339, Top=75,;
    Alignment=1, Width=45, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="ZGENJNNRPH",Visible=.t., Left=3, Top=15,;
    Alignment=1, Width=100, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="HZALIZNENK",Visible=.t., Left=3, Top=501,;
    Alignment=1, Width=100, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_2_40.mHide()
    with this.Parent.oContained
      return (! isalt())
    endwith
  endfunc

  add object oStr_2_45 as StdString with uid="BSGKMHWZHM",Visible=.t., Left=3, Top=315,;
    Alignment=1, Width=100, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="QSNQCLUHKK",Visible=.t., Left=524, Top=74,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo riserva:"  ;
  , bGlobalFont=.t.

  func oStr_2_47.mHide()
    with this.Parent.oContained
      return (.w_RISERVE<> 'S' or not IsAlt())
    endwith
  endfunc

  add object oStr_2_49 as StdString with uid="EIENBMKRMG",Visible=.t., Left=3, Top=532,;
    Alignment=1, Width=100, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="SAKVGFUNQI",Visible=.t., Left=3, Top=408,;
    Alignment=1, Width=100, Height=18,;
    Caption="Ente/uff. giud.:"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="XAAHFWDYMF",Visible=.t., Left=3, Top=439,;
    Alignment=1, Width=100, Height=18,;
    Caption="Ufficio/sezione:"  ;
  , bGlobalFont=.t.

  func oStr_2_51.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oStr_2_57 as StdString with uid="RCYDFBJYOO",Visible=.t., Left=3, Top=131,;
    Alignment=1, Width=100, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_59 as StdString with uid="LBTIYCWQWU",Visible=.t., Left=3, Top=255,;
    Alignment=1, Width=100, Height=18,;
    Caption="Disponibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_60 as StdString with uid="YJIZXRPWAZ",Visible=.t., Left=3, Top=283,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_62 as StdString with uid="CTBVQCRJMM",Visible=.t., Left=30, Top=379,;
    Alignment=1, Width=73, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_63 as StdString with uid="VRCOAPPYPP",Visible=.t., Left=8, Top=45,;
    Alignment=1, Width=177, Height=18,;
    Caption="Gruppo partecipante:"  ;
  , bGlobalFont=.t.

  func oStr_2_63.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  add object oStr_2_65 as StdString with uid="XTHDJBEYME",Visible=.t., Left=26, Top=501,;
    Alignment=1, Width=77, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_65.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_2_66 as StdString with uid="QWICFPOKNJ",Visible=.t., Left=3, Top=408,;
    Alignment=1, Width=100, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  func oStr_2_66.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  add object oStr_2_67 as StdString with uid="PYXGIHNCPT",Visible=.t., Left=241, Top=408,;
    Alignment=1, Width=75, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  func oStr_2_67.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  add object oStr_2_69 as StdString with uid="ISPYDIAKEG",Visible=.t., Left=53, Top=470,;
    Alignment=1, Width=50, Height=18,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  func oStr_2_69.mHide()
    with this.Parent.oContained
      return (g_COAN <> 'S')
    endwith
  endfunc

  add object oStr_2_72 as StdString with uid="CKQRVEYIHP",Visible=.t., Left=51, Top=346,;
    Alignment=1, Width=52, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  func oStr_2_72.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_2_77 as StdString with uid="KSBGPQZUEI",Visible=.t., Left=53, Top=439,;
    Alignment=1, Width=50, Height=18,;
    Caption="Natura:"  ;
  , bGlobalFont=.t.

  func oStr_2_77.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_2_80 as StdString with uid="YAKKYAVGYE",Visible=.t., Left=58, Top=346,;
    Alignment=1, Width=45, Height=18,;
    Caption="Giudice:"  ;
  , bGlobalFont=.t.

  func oStr_2_80.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oStr_2_115 as StdString with uid="KEGEVBAVOI",Visible=.t., Left=292, Top=102,;
    Alignment=1, Width=92, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_116 as StdString with uid="QVTFQRDCTZ",Visible=.t., Left=20, Top=102,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_117 as StdString with uid="DNFJJECVYE",Visible=.t., Left=221, Top=255,;
    Alignment=1, Width=125, Height=18,;
    Caption="Riferimento persona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_118 as StdString with uid="GOADFKEWFS",Visible=.t., Left=600, Top=131,;
    Alignment=1, Width=45, Height=18,;
    Caption="Ruolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_118.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_2_120 as StdString with uid="DACIAVJCND",Visible=.t., Left=7, Top=562,;
    Alignment=1, Width=96, Height=18,;
    Caption="Cose da fare da:"  ;
  , bGlobalFont=.t.

  add object oStr_2_121 as StdString with uid="KMXRIRFDUM",Visible=.t., Left=186, Top=562,;
    Alignment=1, Width=23, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_2_133 as StdString with uid="LIJLPIXOSP",Visible=.t., Left=63, Top=161,;
    Alignment=1, Width=40, Height=18,;
    Caption="Num.:"  ;
  , bGlobalFont=.t.

  func oStr_2_133.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_2_134 as StdString with uid="JMLLMGFFZD",Visible=.t., Left=230, Top=163,;
    Alignment=1, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_134.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oBox_2_9 as StdBox with uid="LOIZABPOHX",left=190, top=11, width=68,height=27
enddefine
define class tgsag_kagPag3 as StdContainer
  Width  = 812
  height = 612
  stdWidth  = 812
  stdheight = 612
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPATIPRIS_3_1 as StdCombo with uid="WNQCBCPXSW",value=4,rtseq=116,rtrep=.f.,left=126,top=34,width=76,height=21;
    , HelpContextID = 10199223;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPATIPRIS_3_1.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    iif(this.value =3,'R',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oPATIPRIS_3_1.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_3_1.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',3,;
      iif(this.Parent.oContained.w_PATIPRIS=='',4,;
      0))))
  endfunc

  func oPATIPRIS_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Parametro='G')
    endwith
   endif
  endfunc

  add object oCODPART_3_2 as StdField with uid="YOZKMASQTM",rtseq=117,rtrep=.f.,;
    cFormVar = "w_CODPART", cQueryName = "CODPART",nZero=5,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice partecipante",;
    HelpContextID = 242904102,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=212, Top=34, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_CODPART"

  func oCODPART_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Parametro='G')
    endwith
   endif
  endfunc

  func oCODPART_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPART_3_2.ecpDrop(oSource)
    this.Parent.oContained.link_3_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPART_3_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oCODPART_3_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSAG2MPA.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oDENOM_PART_3_4 as StdField with uid="PTZFIJREQD",rtseq=118,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(48), bMultilanguage =  .f.,;
    HelpContextID = 205151127,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=282, Top=34, InputMask=replicate('X',48)

  add object oGRUPART_3_5 as StdField with uid="ODMBUMVOKY",rtseq=119,rtrep=.f.,;
    cFormVar = "w_GRUPART", cQueryName = "GRUPART",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo inesistente o non associato al partecipante",;
    HelpContextID = 242974566,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=212, Top=64, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_GRUPART"

  func oGRUPART_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PATIPRIS='P' OR .w_PATIPRIS='T')
    endwith
   endif
  endfunc

  func oGRUPART_3_5.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  func oGRUPART_3_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPART_3_5.ecpDrop(oSource)
    this.Parent.oContained.link_3_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPART_3_5.mZoom
      with this.Parent.oContained
        GSAR1BGP(this.Parent.oContained,  "", .w_CODPART, "", "GRUPART" )
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFLESGR_3_6 as StdCheck with uid="JLHNXFTTGU",rtseq=120,rtrep=.f.,left=212, top=64, caption="Esplodi gruppo",;
    ToolTipText = "Se attivo visualizza anche appuntamenti delle persone del gruppo",;
    HelpContextID = 249395542,;
    cFormVar="w_FLESGR", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFLESGR_3_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLESGR_3_6.GetRadio()
    this.Parent.oContained.w_FLESGR = this.RadioValue()
    return .t.
  endfunc

  func oFLESGR_3_6.SetRadio()
    this.Parent.oContained.w_FLESGR=trim(this.Parent.oContained.w_FLESGR)
    this.value = ;
      iif(this.Parent.oContained.w_FLESGR=='S',1,;
      0)
  endfunc

  func oFLESGR_3_6.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'G' or Empty(.w_CODPART))
    endwith
  endfunc

  add object oDPDESGRU_3_8 as StdField with uid="AWMVCMIRWZ",rtseq=121,rtrep=.f.,;
    cFormVar = "w_DPDESGRU", cQueryName = "DPDESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 76508555,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=282, Top=64, InputMask=replicate('X',40)

  func oDPDESGRU_3_8.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc


  add object oBtn_3_9 as StdButton with uid="HNYBMUCCIQ",left=711, top=34, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=3;
    , ToolTipText = "Ricerca";
    , HelpContextID = 44136214;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_9.Click()
      with this.Parent.oContained
        gsag_bag(this.Parent.oContained,"RICERCA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_10 as StdButton with uid="ESQTRXQXSH",left=711, top=81, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per effettuare la stampa della visualizzazione corrente";
    , HelpContextID = 9003558;
    , caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_10.Click()
      with this.Parent.oContained
        gsag_bag(this.Parent.oContained,"STAMPAAG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CalAttivo=4)
     endwith
    endif
  endfunc


  add object PART_ZOOM as cp_szoombox with uid="WODZOSNAJX",left=41, top=132, width=731,height=437,;
    caption='PART_ZOOM',;
   bGlobalFont=.t.,;
    cTable="DIPENDEN",cZoomFile="GSAG2MPP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Init,w_DATINIZ Changed,w_obsodat1 Changed",;
    nPag=3;
    , HelpContextID = 140461077

  add object oDATINIZ_3_17 as StdField with uid="KXZLKOTRZF",rtseq=134,rtrep=.f.,;
    cFormVar = "w_DATINIZ", cQueryName = "DATINIZ",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 163291594,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=380, Top=95

  add object oobsodat1_3_18 as StdCheck with uid="TANZHKUOWP",rtseq=135,rtrep=.f.,left=492, top=95, caption="Solo obsoleti",;
    ToolTipText = "Se attivo, visualizza solo i partecipanti obsoleti",;
    HelpContextID = 265056791,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oobsodat1_3_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_3_18.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_3_18.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc

  add object oStr_3_3 as StdString with uid="PQMTGLTTUR",Visible=.t., Left=22, Top=35,;
    Alignment=1, Width=100, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_3_7 as StdString with uid="ZZMMLQYKNY",Visible=.t., Left=27, Top=65,;
    Alignment=1, Width=177, Height=18,;
    Caption="Gruppo partecipante:"  ;
  , bGlobalFont=.t.

  func oStr_3_7.mHide()
    with this.Parent.oContained
      return (.w_DPTIPRIS <> 'P' )
    endwith
  endfunc

  add object oStr_3_16 as StdString with uid="XVUBYBVEQE",Visible=.t., Left=283, Top=98,;
    Alignment=1, Width=92, Height=18,;
    Caption="Data di controllo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kag','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
