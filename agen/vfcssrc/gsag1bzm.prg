* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag1bzm                                                        *
*              Lancia elenco attivit� dal piano di generazione                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-10-06                                                      *
* Last revis.: 2011-09-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag1bzm",oParentObject)
return(i_retval)

define class tgsag1bzm as StdBatch
  * --- Local variables
  w_PROG = .NULL.
  w_OBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Apre masch.VISUALIZZA DOCUMENTI DI VENDITA
    this.w_PROG = GSAG_KRA()
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      * --- Messaggio di errore
      Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
      i_retcode = 'stop'
      return
    endif
    this.w_PROG.w_SERPIAN = this.oParentObject.w_ATSERIAL
    this.w_PROG.w_DESCRI = this.oParentObject.w_ATDESCRI
    this.w_PROG.w_STATO = "K"
    this.w_PROG.w_PRIORITA = 0
    this.w_PROG.w_DATINI = this.oParentObject.w_ATDATINI
    this.w_PROG.w_DATFIN = this.oParentObject.w_ATDATFIN
    this.w_PROG.w_PATIPRIS = ""
    this.w_PROG.w_PRIORITA = 0
    this.w_PROG.mcalc(.t.)     
    this.w_PROG.NotifyEvent("Ricerca")     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
