* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_btw                                                        *
*              Lancia la treewiew dagli impianti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-03                                                      *
* Last revis.: 2011-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_btw",oParentObject)
return(i_retval)

define class tgsag_btw as StdBatch
  * --- Local variables
  w_OGG = .NULL.
  w_CODICE = space(15)
  w_IMPIANTO = space(10)
  w_DESCRI = space(60)
  w_DESIMP = space(50)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- lancia TREEVIEW ATTRIBUTI (Da GSAG_MIM)
    * --- apro la maschera della tracciabilita e riempio i campi
    this.w_OGG = GSAG_KTW()
    * --- Assegna il tipo conto
    setvaluelinked("M", this.w_OGG, "w_TIPCON", this.oParentObject.w_IMTIPCON)
    * --- Assegna il codice conto
    setvaluelinked("M", this.w_OGG, "w_CODICE", this.oParentObject.w_IMCODCON,this.w_OGG.w_TIPCON)
    * --- Assegna l'impianto
    setvaluelinked("M", this.w_OGG, "w_IMPIANTO", this.oParentObject.w_IMCODICE)
    this.w_OGG.w_IMMODATR = this.oParentObject.w_IMMODATR
    this.w_OGG.SetControlsValue()     
    this.w_OGG.mEnableControls()     
    this.w_OGG.NotifyEvent("Reload")     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
