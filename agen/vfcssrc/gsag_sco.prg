* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_sco                                                        *
*              Stampa contratti                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-16                                                      *
* Last revis.: 2013-06-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_sco",oParentObject))

* --- Class definition
define class tgsag_sco as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 601
  Height = 383
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-06-11"
  HelpContextID=177592471
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Constant Properties
  _IDX = 0
  MAGAZZIN_IDX = 0
  CON_TRAS_IDX = 0
  CONTI_IDX = 0
  IMP_MAST_IDX = 0
  MOD_ELEM_IDX = 0
  IMP_DETT_IDX = 0
  cPrg = "gsag_sco"
  cComment = "Stampa contratti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  w_ELCODCLI = space(15)
  o_ELCODCLI = space(15)
  w_CODICE = space(15)
  w_CONTINI = space(10)
  o_CONTINI = space(10)
  w_DESCRI = space(50)
  w_CONTFIN = space(10)
  o_CONTFIN = space(10)
  w_DESCRI1 = space(50)
  w_DESCLI = space(60)
  w_OLDCOMP = space(10)
  w_IMPIANTO = space(10)
  o_IMPIANTO = space(10)
  w_COMPIMP = space(50)
  o_COMPIMP = space(50)
  w_COMPIMPKEY = 0
  o_COMPIMPKEY = 0
  w_COMPIMPKEYREAD = 0
  w_DESIMP = space(60)
  w_FLTIPCON = space(1)
  w_MODELLO = space(10)
  w_DESMOD = space(60)
  w_DATSTINI = ctod('  /  /  ')
  w_DATSTIFIN = ctod('  /  /  ')
  w_DATINIVAL = ctod('  /  /  ')
  w_DATFINVAL = ctod('  /  /  ')
  w_DATINIDOC = ctod('  /  /  ')
  w_DATFINDOC = ctod('  /  /  ')
  w_DATINIATT = ctod('  /  /  ')
  w_DATFINATT = ctod('  /  /  ')
  w_FILTRO = 0
  o_FILTRO = 0
  w_CODSED = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_ELEMENTI = space(1)
  w_MOTIPCON = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_scoPag1","gsag_sco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oELCODCLI_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='CON_TRAS'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='IMP_MAST'
    this.cWorkTables[5]='MOD_ELEM'
    this.cWorkTables[6]='IMP_DETT'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_ELCODCLI=space(15)
      .w_CODICE=space(15)
      .w_CONTINI=space(10)
      .w_DESCRI=space(50)
      .w_CONTFIN=space(10)
      .w_DESCRI1=space(50)
      .w_DESCLI=space(60)
      .w_OLDCOMP=space(10)
      .w_IMPIANTO=space(10)
      .w_COMPIMP=space(50)
      .w_COMPIMPKEY=0
      .w_COMPIMPKEYREAD=0
      .w_DESIMP=space(60)
      .w_FLTIPCON=space(1)
      .w_MODELLO=space(10)
      .w_DESMOD=space(60)
      .w_DATSTINI=ctod("  /  /  ")
      .w_DATSTIFIN=ctod("  /  /  ")
      .w_DATINIVAL=ctod("  /  /  ")
      .w_DATFINVAL=ctod("  /  /  ")
      .w_DATINIDOC=ctod("  /  /  ")
      .w_DATFINDOC=ctod("  /  /  ")
      .w_DATINIATT=ctod("  /  /  ")
      .w_DATFINATT=ctod("  /  /  ")
      .w_FILTRO=0
      .w_CODSED=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_ELEMENTI=space(1)
      .w_MOTIPCON=space(1)
        .w_TIPCON = 'C'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ELCODCLI))
          .link_1_4('Full')
        endif
        .w_CODICE = .w_ELCODCLI
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CONTINI))
          .link_1_6('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_CONTFIN = .w_CONTINI
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CONTFIN))
          .link_1_8('Full')
        endif
        .DoRTCalc(7,10,.f.)
        if not(empty(.w_IMPIANTO))
          .link_1_15('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_COMPIMPKEY = 0
        .w_COMPIMPKEYREAD = .w_COMPIMPKEY
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_COMPIMPKEYREAD))
          .link_1_18('Full')
        endif
          .DoRTCalc(14,14,.f.)
        .w_FLTIPCON = 'T'
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_MODELLO))
          .link_1_22('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
          .DoRTCalc(17,25,.f.)
        .w_FILTRO = iif(empty(.w_COMPIMP),0,.w_COMPIMPKEY)
          .DoRTCalc(27,27,.f.)
        .w_OBTEST = I_DATSYS
        .w_ELEMENTI = "A"
    endwith
    this.DoRTCalc(30,30,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPCON = 'C'
        .DoRTCalc(2,2,.t.)
            .w_CODICE = .w_ELCODCLI
        .DoRTCalc(4,5,.t.)
        if .o_CONTINI<>.w_CONTINI
            .w_CONTFIN = .w_CONTINI
          .link_1_8('Full')
        endif
        .DoRTCalc(7,12,.t.)
        if .o_IMPIANTO<>.w_IMPIANTO.or. .o_COMPIMPKEY<>.w_COMPIMPKEY
            .w_COMPIMPKEYREAD = .w_COMPIMPKEY
          .link_1_18('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .DoRTCalc(14,25,.t.)
        if .o_COMPIMP<>.w_COMPIMP.or. .o_IMPIANTO<>.w_IMPIANTO.or. .o_CONTINI<>.w_CONTINI.or. .o_CONTFIN<>.w_CONTFIN.or. .o_ELCODCLI<>.w_ELCODCLI
            .w_FILTRO = iif(empty(.w_COMPIMP),0,.w_COMPIMPKEY)
        endif
        .DoRTCalc(27,27,.t.)
            .w_OBTEST = I_DATSYS
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(29,30,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
    endwith
  return

  proc Calculate_PLWQRIQXJA()
    with this
          * --- Ricalcolo al cambio del cliente
          .w_CONTINI = ''
          .w_CONTFIN = ''
          .w_IMPIANTO = ''
          .w_COMPIMP = ''
          .w_DESCRI = ''
          .w_DESCRI1 = ''
          .w_DESIMP = ''
    endwith
  endproc
  proc Calculate_KBRYQFTJFJ()
    with this
          * --- Azzero al variare dell'impianto
          .w_COMPIMPKEY = 0
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCOMPIMP_1_16.enabled = this.oPgFrm.Page1.oPag.oCOMPIMP_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCOMPIMP_1_16.visible=!this.oPgFrm.Page1.oPag.oCOMPIMP_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_17.visible=!this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_ELCODCLI Changed")
          .Calculate_PLWQRIQXJA()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
        if lower(cEvent)==lower("w_CONTFIN Changed") or lower(cEvent)==lower("w_CONTINI Changed") or lower(cEvent)==lower("w_ELCODCLI Changed") or lower(cEvent)==lower("w_IMPIANTO Changed")
          .Calculate_KBRYQFTJFJ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ELCODCLI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ELCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_ELCODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oELCODCLI_1_4'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ELCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_ELCODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODCLI = space(15)
      endif
      this.w_DESCLI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTINI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACA',True,'CON_TRAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COSERIAL like "+cp_ToStrODBC(trim(this.w_CONTINI)+"%");

          i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COSERIAL',trim(this.w_CONTINI))
          select COSERIAL,CODESCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONTINI)==trim(_Link_.COSERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStrODBC(trim(this.w_CONTINI)+"%");

            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStr(trim(this.w_CONTINI)+"%");

            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONTINI) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAS','*','COSERIAL',cp_AbsName(oSource.parent,'oCONTINI_1_6'),i_cWhere,'GSAG_ACA',"Contratti",'GSAG_AEL.CON_TRAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                     +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',oSource.xKey(1))
            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_CONTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_CONTINI)
            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTINI = NVL(_Link_.COSERIAL,space(10))
      this.w_DESCRI = NVL(_Link_.CODESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CONTINI = space(10)
      endif
      this.w_DESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_CONTFIN)) OR  (.w_CONTINI<=.w_CONTFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CONTINI = space(10)
        this.w_DESCRI = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTFIN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACA',True,'CON_TRAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COSERIAL like "+cp_ToStrODBC(trim(this.w_CONTFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COSERIAL',trim(this.w_CONTFIN))
          select COSERIAL,CODESCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONTFIN)==trim(_Link_.COSERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStrODBC(trim(this.w_CONTFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStr(trim(this.w_CONTFIN)+"%");

            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONTFIN) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAS','*','COSERIAL',cp_AbsName(oSource.parent,'oCONTFIN_1_8'),i_cWhere,'GSAG_ACA',"Contratti",'GSAG_AEL.CON_TRAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                     +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',oSource.xKey(1))
            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_CONTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_CONTFIN)
            select COSERIAL,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTFIN = NVL(_Link_.COSERIAL,space(10))
      this.w_DESCRI1 = NVL(_Link_.CODESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CONTFIN = space(10)
      endif
      this.w_DESCRI1 = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_CONTFIN>=.w_CONTINI) or (empty(.w_CONTINI)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_CONTFIN = space(10)
        this.w_DESCRI1 = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMPIANTO
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMPIANTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsag_mim',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_IMPIANTO)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_IMPIANTO))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMPIANTO)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IMPIANTO) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oIMPIANTO_1_15'),i_cWhere,'gsag_mim',"Impianti",'GSAG_IMP.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMPIANTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMPIANTO = NVL(_Link_.IMCODICE,space(10))
      this.w_DESIMP = NVL(_Link_.IMDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_IMPIANTO = space(10)
      endif
      this.w_DESIMP = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMPIANTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPIMPKEYREAD
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPIMPKEYREAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPIMPKEYREAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_COMPIMPKEYREAD);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO;
                       ,'CPROWNUM',this.w_COMPIMPKEYREAD)
            select IMCODICE,CPROWNUM,IMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPIMPKEYREAD = NVL(_Link_.CPROWNUM,0)
      this.w_COMPIMP = NVL(_Link_.IMDESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_COMPIMPKEYREAD = 0
      endif
      this.w_COMPIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPIMPKEYREAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MODELLO
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MODELLO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_AME',True,'MOD_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_MODELLO)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_MODELLO))
          select MOCODICE,MODESCRI,MOTIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MODELLO)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MODELLO) and !this.bDontReportError
            deferred_cp_zoom('MOD_ELEM','*','MOCODICE',cp_AbsName(oSource.parent,'oMODELLO_1_22'),i_cWhere,'GSAG_AME',"Modelli",'GSAG_KZM.MOD_ELEM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI,MOTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MODELLO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_MODELLO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_MODELLO)
            select MOCODICE,MODESCRI,MOTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MODELLO = NVL(_Link_.MOCODICE,space(10))
      this.w_DESMOD = NVL(_Link_.MODESCRI,space(60))
      this.w_MOTIPCON = NVL(_Link_.MOTIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MODELLO = space(10)
      endif
      this.w_DESMOD = space(60)
      this.w_MOTIPCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLTIPCON='T' OR .w_MOTIPCON=.w_FLTIPCON
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Modello inesistente o con tipologia errata")
        endif
        this.w_MODELLO = space(10)
        this.w_DESMOD = space(60)
        this.w_MOTIPCON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MODELLO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oELCODCLI_1_4.value==this.w_ELCODCLI)
      this.oPgFrm.Page1.oPag.oELCODCLI_1_4.value=this.w_ELCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTINI_1_6.value==this.w_CONTINI)
      this.oPgFrm.Page1.oPag.oCONTINI_1_6.value=this.w_CONTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_7.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_7.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTFIN_1_8.value==this.w_CONTFIN)
      this.oPgFrm.Page1.oPag.oCONTFIN_1_8.value=this.w_CONTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_9.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_9.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_12.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_12.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPIANTO_1_15.value==this.w_IMPIANTO)
      this.oPgFrm.Page1.oPag.oIMPIANTO_1_15.value=this.w_IMPIANTO
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPIMP_1_16.value==this.w_COMPIMP)
      this.oPgFrm.Page1.oPag.oCOMPIMP_1_16.value=this.w_COMPIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_17.value==this.w_COMPIMPKEY)
      this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_17.value=this.w_COMPIMPKEY
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_1_20.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_1_20.value=this.w_DESIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oMODELLO_1_22.value==this.w_MODELLO)
      this.oPgFrm.Page1.oPag.oMODELLO_1_22.value=this.w_MODELLO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMOD_1_23.value==this.w_DESMOD)
      this.oPgFrm.Page1.oPag.oDESMOD_1_23.value=this.w_DESMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTINI_1_24.value==this.w_DATSTINI)
      this.oPgFrm.Page1.oPag.oDATSTINI_1_24.value=this.w_DATSTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTIFIN_1_27.value==this.w_DATSTIFIN)
      this.oPgFrm.Page1.oPag.oDATSTIFIN_1_27.value=this.w_DATSTIFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINIVAL_1_30.value==this.w_DATINIVAL)
      this.oPgFrm.Page1.oPag.oDATINIVAL_1_30.value=this.w_DATINIVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFINVAL_1_31.value==this.w_DATFINVAL)
      this.oPgFrm.Page1.oPag.oDATFINVAL_1_31.value=this.w_DATFINVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINIDOC_1_33.value==this.w_DATINIDOC)
      this.oPgFrm.Page1.oPag.oDATINIDOC_1_33.value=this.w_DATINIDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFINDOC_1_34.value==this.w_DATFINDOC)
      this.oPgFrm.Page1.oPag.oDATFINDOC_1_34.value=this.w_DATFINDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINIATT_1_37.value==this.w_DATINIATT)
      this.oPgFrm.Page1.oPag.oDATINIATT_1_37.value=this.w_DATINIATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFINATT_1_38.value==this.w_DATFINATT)
      this.oPgFrm.Page1.oPag.oDATFINATT_1_38.value=this.w_DATFINATT
    endif
    if not(this.oPgFrm.Page1.oPag.oELEMENTI_1_49.RadioValue()==this.w_ELEMENTI)
      this.oPgFrm.Page1.oPag.oELEMENTI_1_49.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_CONTFIN)) OR  (.w_CONTINI<=.w_CONTFIN)))  and not(empty(.w_CONTINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONTINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not(((.w_CONTFIN>=.w_CONTINI) or (empty(.w_CONTINI))))  and not(empty(.w_CONTFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONTFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not(.w_FLTIPCON='T' OR .w_MOTIPCON=.w_FLTIPCON)  and not(empty(.w_MODELLO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMODELLO_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Modello inesistente o con tipologia errata")
          case   not((.w_DATSTINI<=.w_DATSTIFIN OR (EMPTY(.w_DATSTIFIN))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTINI_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((.w_DATSTINI<=.w_DATSTIFIN OR EMPTY(.w_DATSTINI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSTIFIN_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((.w_DATINIVAL<=.w_DATFINVAL OR (EMPTY(.w_DATFINVAL))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINIVAL_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((.w_DATINIVAL<=.w_DATFINVAL OR EMPTY(.w_DATINIVAL)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFINVAL_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((.w_DATINIDOC<=.w_DATFINDOC OR (EMPTY(.w_DATFINDOC))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINIDOC_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((.w_DATINIDOC<=.w_DATFINDOC OR EMPTY(.w_DATINIDOC)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFINDOC_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((.w_DATINIATT<=.w_DATFINATT OR (EMPTY(.w_DATFINATT))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINIATT_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not((.w_DATINIATT<=.w_DATFINATT OR EMPTY(.w_DATINIATT)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFINATT_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ELCODCLI = this.w_ELCODCLI
    this.o_CONTINI = this.w_CONTINI
    this.o_CONTFIN = this.w_CONTFIN
    this.o_IMPIANTO = this.w_IMPIANTO
    this.o_COMPIMP = this.w_COMPIMP
    this.o_COMPIMPKEY = this.w_COMPIMPKEY
    this.o_FILTRO = this.w_FILTRO
    return

enddefine

* --- Define pages as container
define class tgsag_scoPag1 as StdContainer
  Width  = 699
  height = 383
  stdWidth  = 699
  stdheight = 383
  resizeXpos=308
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELCODCLI_1_4 as StdField with uid="IJERBZBQVI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ELCODCLI", cQueryName = "ELCODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente selezionato",;
    HelpContextID = 36264335,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=126, Top=11, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_ELCODCLI"

  func oELCODCLI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODCLI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODCLI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oELCODCLI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oELCODCLI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_ELCODCLI
     i_obj.ecpSave()
  endproc

  add object oCONTINI_1_6 as StdField with uid="SHKDUWACNA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CONTINI", cQueryName = "CONTINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Contratto di inizio selezione",;
    HelpContextID = 226429990,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=126, Top=35, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CON_TRAS", cZoomOnZoom="GSAG_ACA", oKey_1_1="COSERIAL", oKey_1_2="this.w_CONTINI"

  func oCONTINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONTINI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONTINI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CON_TRAS','*','COSERIAL',cp_AbsName(this.parent,'oCONTINI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACA',"Contratti",'GSAG_AEL.CON_TRAS_VZM',this.parent.oContained
  endproc
  proc oCONTINI_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_COSERIAL=this.parent.oContained.w_CONTINI
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_7 as StdField with uid="YYWNVZAKCO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 117550538,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=218, Top=35, InputMask=replicate('X',50)

  add object oCONTFIN_1_8 as StdField with uid="BZTVPZNHMD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CONTFIN", cQueryName = "CONTFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Contratto di fine selezione",;
    HelpContextID = 129037274,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=126, Top=59, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CON_TRAS", cZoomOnZoom="GSAG_ACA", oKey_1_1="COSERIAL", oKey_1_2="this.w_CONTFIN"

  func oCONTFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONTFIN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONTFIN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CON_TRAS','*','COSERIAL',cp_AbsName(this.parent,'oCONTFIN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACA',"Contratti",'GSAG_AEL.CON_TRAS_VZM',this.parent.oContained
  endproc
  proc oCONTFIN_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_COSERIAL=this.parent.oContained.w_CONTFIN
     i_obj.ecpSave()
  endproc

  add object oDESCRI1_1_9 as StdField with uid="LCJZIDXXBX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 117550538,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=218, Top=59, InputMask=replicate('X',50)

  add object oDESCLI_1_12 as StdField with uid="SNLVKXYMMU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 123841994,;
   bGlobalFont=.t.,;
    Height=21, Width=336, Left=245, Top=11, InputMask=replicate('X',60)

  add object oIMPIANTO_1_15 as StdField with uid="AUISPJHOOI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_IMPIANTO", cQueryName = "IMPIANTO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice impianto selezionato",;
    HelpContextID = 51107115,;
   bGlobalFont=.t.,;
    Height=21, Width=99, Left=126, Top=84, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", cZoomOnZoom="gsag_mim", oKey_1_1="IMCODICE", oKey_1_2="this.w_IMPIANTO"

  func oIMPIANTO_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
      if .not. empty(.w_COMPIMPKEYREAD)
        bRes2=.link_1_18('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIMPIANTO_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMPIANTO_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oIMPIANTO_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'gsag_mim',"Impianti",'GSAG_IMP.IMP_MAST_VZM',this.parent.oContained
  endproc
  proc oIMPIANTO_1_15.mZoomOnZoom
    local i_obj
    i_obj=gsag_mim()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_IMPIANTO
     i_obj.ecpSave()
  endproc

  add object oCOMPIMP_1_16 as StdField with uid="GOVKVGTSZB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_COMPIMP", cQueryName = "COMPIMP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Componente impianto selezionato",;
    HelpContextID = 59048922,;
   bGlobalFont=.t.,;
    Height=21, Width=456, Left=126, Top=108, InputMask=replicate('X',50), bHasZoom = .t. 

  func oCOMPIMP_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_IMPIANTO))
    endwith
   endif
  endfunc

  func oCOMPIMP_1_16.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  proc oCOMPIMP_1_16.mBefore
    with this.Parent.oContained
      .w_OLDCOMP=.w_COMPIMP
    endwith
  endproc

  proc oCOMPIMP_1_16.mAfter
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M")
      endwith
  endproc

  proc oCOMPIMP_1_16.mZoom
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M",.T.)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCOMPIMPKEY_1_17 as StdField with uid="SBSCAGJTGI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COMPIMPKEY", cQueryName = "COMPIMPKEY",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 59024959,;
   bGlobalFont=.t.,;
    Height=22, Width=83, Left=616, Top=9

  func oCOMPIMPKEY_1_17.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oDESIMP_1_20 as StdField with uid="MZTTQOQOOZ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 4959690,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=228, Top=85, InputMask=replicate('X',60)

  add object oMODELLO_1_22 as StdField with uid="MRCOWLOFIP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MODELLO", cQueryName = "MODELLO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Modello inesistente o con tipologia errata",;
    ToolTipText = "Modello elemento selezionato",;
    HelpContextID = 73438010,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=126, Top=132, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_ELEM", cZoomOnZoom="GSAG_AME", oKey_1_1="MOCODICE", oKey_1_2="this.w_MODELLO"

  func oMODELLO_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oMODELLO_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMODELLO_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_ELEM','*','MOCODICE',cp_AbsName(this.parent,'oMODELLO_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_AME',"Modelli",'GSAG_KZM.MOD_ELEM_VZM',this.parent.oContained
  endproc
  proc oMODELLO_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAG_AME()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_MODELLO
     i_obj.ecpSave()
  endproc

  add object oDESMOD_1_23 as StdField with uid="OUWJKTVSEI",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESMOD", cQueryName = "DESMOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 203926986,;
   bGlobalFont=.t.,;
    Height=21, Width=371, Left=211, Top=132, InputMask=replicate('X',60)

  add object oDATSTINI_1_24 as StdField with uid="YFVNXNZOOH",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATSTINI", cQueryName = "DATSTINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data stipulazione iniziale",;
    HelpContextID = 114401665,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=212, Top=188

  func oDATSTINI_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATSTINI<=.w_DATSTIFIN OR (EMPTY(.w_DATSTIFIN))))
    endwith
    return bRes
  endfunc

  add object oDATSTIFIN_1_27 as StdField with uid="BSTCOEOHKG",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DATSTIFIN", cQueryName = "DATSTIFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data stipulazione finale",;
    HelpContextID = 154035039,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=212, Top=213

  func oDATSTIFIN_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATSTINI<=.w_DATSTIFIN OR EMPTY(.w_DATSTINI)))
    endwith
    return bRes
  endfunc

  add object oDATINIVAL_1_30 as StdField with uid="XWDWMJQRBQ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DATINIVAL", cQueryName = "DATINIVAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 147088183,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=464, Top=188

  func oDATINIVAL_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINIVAL<=.w_DATFINVAL OR (EMPTY(.w_DATFINVAL))))
    endwith
    return bRes
  endfunc

  add object oDATFINVAL_1_31 as StdField with uid="EALHFNJBQU",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DATFINVAL", cQueryName = "DATFINVAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data fine validit�",;
    HelpContextID = 225534775,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=464, Top=213

  func oDATFINVAL_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINIVAL<=.w_DATFINVAL OR EMPTY(.w_DATINIVAL)))
    endwith
    return bRes
  endfunc

  add object oDATINIDOC_1_33 as StdField with uid="UMZMFEOKLF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DATINIDOC", cQueryName = "DATINIDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data iniziale prossimo documento",;
    HelpContextID = 121347403,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=212, Top=246

  func oDATINIDOC_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINIDOC<=.w_DATFINDOC OR (EMPTY(.w_DATFINDOC))))
    endwith
    return bRes
  endfunc

  add object oDATFINDOC_1_34 as StdField with uid="NASCINLJGB",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DATFINDOC", cQueryName = "DATFINDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data finale prossimo documento",;
    HelpContextID = 42900811,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=212, Top=273

  func oDATFINDOC_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINIDOC<=.w_DATFINDOC OR EMPTY(.w_DATINIDOC)))
    endwith
    return bRes
  endfunc

  add object oDATINIATT_1_37 as StdField with uid="ZVJKJIPSMP",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DATINIATT", cQueryName = "DATINIATT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data iniziale prossima attivit�",;
    HelpContextID = 121347126,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=464, Top=247

  func oDATINIATT_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINIATT<=.w_DATFINATT OR (EMPTY(.w_DATFINATT))))
    endwith
    return bRes
  endfunc

  add object oDATFINATT_1_38 as StdField with uid="XLJNZLCYRN",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DATFINATT", cQueryName = "DATFINATT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data finale prossima attivit�",;
    HelpContextID = 42900534,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=464, Top=274

  func oDATFINATT_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINIATT<=.w_DATFINATT OR EMPTY(.w_DATINIATT)))
    endwith
    return bRes
  endfunc


  add object oObj_1_41 as cp_outputCombo with uid="ZLERXIVPWT",left=126, top=304, width=456,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181280794


  add object oBtn_1_43 as StdButton with uid="PYYBFJPVEW",left=483, top=330, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 217488858;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_45 as StdButton with uid="XQXXJWXSFU",left=534, top=330, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 184909894;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_45.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oELEMENTI_1_49 as StdCombo with uid="ERMAAWZBFT",rtseq=29,rtrep=.f.,left=126,top=157,width=146,height=21;
    , ToolTipText = "Permette la visualizzazione dei soli elementi contratto attivi, non attivi (con data prossimo documento e data prossima attivit� vuote) oppure tutti";
    , HelpContextID = 46696049;
    , cFormVar="w_ELEMENTI",RowSource=""+"Attivi,"+"Non attivi,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oELEMENTI_1_49.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'N',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oELEMENTI_1_49.GetRadio()
    this.Parent.oContained.w_ELEMENTI = this.RadioValue()
    return .t.
  endfunc

  func oELEMENTI_1_49.SetRadio()
    this.Parent.oContained.w_ELEMENTI=trim(this.Parent.oContained.w_ELEMENTI)
    this.value = ;
      iif(this.Parent.oContained.w_ELEMENTI=='A',1,;
      iif(this.Parent.oContained.w_ELEMENTI=='N',2,;
      iif(this.Parent.oContained.w_ELEMENTI=='E',3,;
      0)))
  endfunc

  add object oStr_1_2 as StdString with uid="IFWIPBAPNS",Visible=.t., Left=47, Top=35,;
    Alignment=1, Width=76, Height=18,;
    Caption="Da contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="BBYALKDJZJ",Visible=.t., Left=57, Top=59,;
    Alignment=1, Width=66, Height=18,;
    Caption="A contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="HRTFFGQQPR",Visible=.t., Left=35, Top=304,;
    Alignment=1, Width=88, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="WAHXGQIVLD",Visible=.t., Left=81, Top=11,;
    Alignment=1, Width=42, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="SEIYNYNWMD",Visible=.t., Left=72, Top=84,;
    Alignment=1, Width=51, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="PYXGIHNCPT",Visible=.t., Left=48, Top=108,;
    Alignment=1, Width=75, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="TSRSMOYULA",Visible=.t., Left=123, Top=188,;
    Alignment=1, Width=86, Height=18,;
    Caption="Da data stipula:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="IAHMENFMHB",Visible=.t., Left=132, Top=213,;
    Alignment=1, Width=77, Height=18,;
    Caption="A data stipula:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="QYPYIMXQOI",Visible=.t., Left=343, Top=188,;
    Alignment=1, Width=118, Height=18,;
    Caption="Da data inizio validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="LCCUSXNVPW",Visible=.t., Left=360, Top=213,;
    Alignment=1, Width=101, Height=18,;
    Caption="A data fine validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="FHCTEQYDFC",Visible=.t., Left=81, Top=246,;
    Alignment=1, Width=128, Height=18,;
    Caption="Da data prossimo doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="WMOWZGNFXU",Visible=.t., Left=90, Top=273,;
    Alignment=1, Width=119, Height=18,;
    Caption="A data prossimo doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="OQOSQWGDCH",Visible=.t., Left=340, Top=247,;
    Alignment=1, Width=121, Height=18,;
    Caption="Da data prossima att.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="NEJTOJXXOX",Visible=.t., Left=349, Top=274,;
    Alignment=1, Width=112, Height=18,;
    Caption="A data prossima att.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="ISUTIXPXUY",Visible=.t., Left=71, Top=159,;
    Alignment=1, Width=52, Height=18,;
    Caption="Elementi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="NVINUVKSBJ",Visible=.t., Left=77, Top=132,;
    Alignment=1, Width=46, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_sco','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
