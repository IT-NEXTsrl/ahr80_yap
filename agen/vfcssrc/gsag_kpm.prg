* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kpm                                                        *
*              Scelta multipla delle prestazioni                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-25                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kpm",oParentObject))

* --- Class definition
define class tgsag_kpm as StdForm
  Top    = 7
  Left   = 16

  * --- Standard Properties
  Width  = 854
  Height = 556
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-17"
  HelpContextID=118872215
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=49

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  VALUTE_IDX = 0
  LISTINI_IDX = 0
  PAR_AGEN_IDX = 0
  PRA_ENTI_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  PRE_ITER_IDX = 0
  PAR_ALTE_IDX = 0
  cPrg = "gsag_kpm"
  cComment = "Scelta multipla delle prestazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PROG_PARENT = space(20)
  w_CODPRA = space(15)
  w_LetturaParAlte = space(5)
  o_LetturaParAlte = space(5)
  w_CACODINI = space(20)
  w_CACODFIN = space(20)
  w_DESCR = space(40)
  w_DESCRSUPP = space(0)
  w_NUMPRA = space(15)
  o_NUMPRA = space(15)
  w_TIPOLOGIA = space(1)
  w_SELRAGG = space(10)
  o_SELRAGG = space(10)
  w_DESPRA = space(100)
  w_ENTEPRA1 = space(10)
  o_ENTEPRA1 = space(10)
  w_ENTEPRA2 = space(10)
  o_ENTEPRA2 = space(10)
  w_ENTEPRA = space(10)
  w_TIPOENTE = space(1)
  w_TipoPersona = space(1)
  w_CODRESP = space(5)
  o_CODRESP = space(5)
  w_COGNRESP = space(10)
  w_NOMRESP = space(10)
  w_DENOMRES = space(50)
  w_APPOAZI = space(5)
  w_CODLIS = space(5)
  w_DATAPRE = ctod('  /  /  ')
  o_DATAPRE = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CODVAL = space(3)
  o_CODVAL = space(3)
  w_DESVAL = space(35)
  w_CODLISPRE = space(5)
  w_DESLIS = space(40)
  w_VALLIS = space(3)
  w_DATOBSO = ctod('  /  /  ')
  w_CADESINI = space(40)
  w_CADESFIN = space(40)
  w_CAARTINI = space(20)
  w_CADOBINI = ctod('  /  /  ')
  w_CAARTFIN = space(20)
  w_CADOBFIN = ctod('  /  /  ')
  w_SCOLIS = space(1)
  w_CICLO = space(1)
  w_COMODO = space(10)
  w_OB_TEST = ctod('  /  /  ')
  w_RESCHK = 0
  w_SELEZI = space(1)
  w_FLASPAN = space(1)
  w_FLSELE = 0
  w_FLGMSK = space(1)
  w_CNDATFIN = ctod('  /  /  ')
  w_PAFLDESP = space(1)
  w_FLDTRP = space(1)
  w_DESCRI = space(40)
  w_AGKPM_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kpmPag1","gsag_kpm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCACODINI_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsag_kpm
    if VARTYPE(g_SCHEDULER)='C' AND g_SCHEDULER='S'
       this.parent.left=_screen.width+1
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGKPM_ZOOM = this.oPgFrm.Pages(1).oPag.AGKPM_ZOOM
    DoDefault()
    proc Destroy()
      this.w_AGKPM_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='PAR_AGEN'
    this.cWorkTables[6]='PRA_ENTI'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='ART_ICOL'
    this.cWorkTables[9]='PRE_ITER'
    this.cWorkTables[10]='PAR_ALTE'
    return(this.OpenAllTables(10))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PROG_PARENT=space(20)
      .w_CODPRA=space(15)
      .w_LetturaParAlte=space(5)
      .w_CACODINI=space(20)
      .w_CACODFIN=space(20)
      .w_DESCR=space(40)
      .w_DESCRSUPP=space(0)
      .w_NUMPRA=space(15)
      .w_TIPOLOGIA=space(1)
      .w_SELRAGG=space(10)
      .w_DESPRA=space(100)
      .w_ENTEPRA1=space(10)
      .w_ENTEPRA2=space(10)
      .w_ENTEPRA=space(10)
      .w_TIPOENTE=space(1)
      .w_TipoPersona=space(1)
      .w_CODRESP=space(5)
      .w_COGNRESP=space(10)
      .w_NOMRESP=space(10)
      .w_DENOMRES=space(50)
      .w_APPOAZI=space(5)
      .w_CODLIS=space(5)
      .w_DATAPRE=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_CODVAL=space(3)
      .w_DESVAL=space(35)
      .w_CODLISPRE=space(5)
      .w_DESLIS=space(40)
      .w_VALLIS=space(3)
      .w_DATOBSO=ctod("  /  /  ")
      .w_CADESINI=space(40)
      .w_CADESFIN=space(40)
      .w_CAARTINI=space(20)
      .w_CADOBINI=ctod("  /  /  ")
      .w_CAARTFIN=space(20)
      .w_CADOBFIN=ctod("  /  /  ")
      .w_SCOLIS=space(1)
      .w_CICLO=space(1)
      .w_COMODO=space(10)
      .w_OB_TEST=ctod("  /  /  ")
      .w_RESCHK=0
      .w_SELEZI=space(1)
      .w_FLASPAN=space(1)
      .w_FLSELE=0
      .w_FLGMSK=space(1)
      .w_CNDATFIN=ctod("  /  /  ")
      .w_PAFLDESP=space(1)
      .w_FLDTRP=space(1)
      .w_DESCRI=space(40)
      .w_SELRAGG=oParentObject.w_SELRAGG
      .w_COMODO=oParentObject.w_COMODO
        .w_PROG_PARENT = upper(oParentObject.cPrg)
        .w_CODPRA = ICASE(.w_PROG_PARENT = 'GSAG_AAT','',.w_PROG_PARENT = 'GSAL_KCI',this.oParentObject.w_CodPra,this.oParentObject.w_CodPratica)
        .w_LetturaParAlte = i_CodAzi
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_LetturaParAlte))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CACODINI))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CACODFIN))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,7,.f.)
        .w_NUMPRA = .w_CODPRA
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_NUMPRA))
          .link_1_8('Full')
        endif
        .w_TIPOLOGIA = 'K'
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_SELRAGG))
          .link_1_10('Full')
        endif
          .DoRTCalc(11,12,.f.)
        .w_ENTEPRA2 = iif(.w_PROG_PARENT = 'GSAG_AAT', oParentObject.w_AT__ENTE, space(10))
        .w_ENTEPRA = iif(.w_PROG_PARENT = 'GSAG_AAT',.w_ENTEPRA2,.w_ENTEPRA1)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_ENTEPRA))
          .link_1_14('Full')
        endif
          .DoRTCalc(15,15,.f.)
        .w_TipoPersona = 'P'
        .w_CODRESP = iif(upper(this.oParentObject.cPrg)='GSAG_AAT', InitResp( this ), space(5))
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODRESP))
          .link_1_19('Full')
        endif
          .DoRTCalc(18,19,.f.)
        .w_DENOMRES = alltrim(.w_COGNRESP)+' '+alltrim(.w_NOMRESP)
        .w_APPOAZI = i_codazi
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_APPOAZI))
          .link_1_23('Full')
        endif
        .w_CODLIS = This.oparentobject .w_ATCODLIS
        .w_DATAPRE = IIF(.w_PROG_PARENT = 'GSAG_KPR'  AND NOT EMPTY(.w_SELRAGG) AND NOT EMPTY(this.oparentobject.w_DATA_PRE), this.oparentobject.w_DATA_PRE, i_datsys)
        .w_OBTEST = i_datsys
        .w_CODVAL = g_PERVAL
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_CODVAL))
          .link_1_34('Full')
        endif
          .DoRTCalc(26,26,.f.)
        .w_CODLISPRE = IIF(EMPTY(.w_CODLISPRE), .w_CODLIS, .w_CODLISPRE)
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CODLISPRE))
          .link_1_37('Full')
        endif
      .oPgFrm.Page1.oPag.AGKPM_ZOOM.Calculate(.w_TIPOENTE)
          .DoRTCalc(28,37,.f.)
        .w_CICLO = 'E'
          .DoRTCalc(39,39,.f.)
        .w_OB_TEST = IIF(.w_PROG_PARENT = 'GSAG_AAT', this.oParentObject.w_DATINI, ctod("  -  -  "))
        .w_RESCHK = 0
        .w_SELEZI = 'D'
        .w_FLASPAN = 'S'
        .w_FLSELE = 0
      .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
    endwith
    this.DoRTCalc(45,49,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsag_kpm
    If IsAlt()
      this.NotifyEvent('Ricerca')
      if Not Empty(This.w_SELRAGG)
       this.w_SELEZI='S'
       this.NotifyEvent('w_SELEZI Changed')
      endif
    endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SELRAGG=.w_SELRAGG
      .oParentObject.w_COMODO=.w_COMODO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_LetturaParalte<>.w_LetturaParalte
            .w_LetturaParAlte = i_CodAzi
          .link_1_3('Full')
        endif
        .DoRTCalc(4,13,.t.)
        if .o_ENTEPRA1<>.w_ENTEPRA1.or. .o_ENTEPRA2<>.w_ENTEPRA2
            .w_ENTEPRA = iif(.w_PROG_PARENT = 'GSAG_AAT',.w_ENTEPRA2,.w_ENTEPRA1)
          .link_1_14('Full')
        endif
        .DoRTCalc(15,19,.t.)
        if .o_CODRESP<>.w_CODRESP
            .w_DENOMRES = alltrim(.w_COGNRESP)+' '+alltrim(.w_NOMRESP)
        endif
          .link_1_23('Full')
            .w_CODLIS = This.oparentobject .w_ATCODLIS
        .DoRTCalc(23,24,.t.)
          .link_1_34('Full')
        .DoRTCalc(26,26,.t.)
        if .o_NUMPRA<>.w_NUMPRA.or. .o_CODVAL<>.w_CODVAL
            .w_CODLISPRE = IIF(EMPTY(.w_CODLISPRE), .w_CODLIS, .w_CODLISPRE)
          .link_1_37('Full')
        endif
        .oPgFrm.Page1.oPag.AGKPM_ZOOM.Calculate(.w_TIPOENTE)
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        if .o_SELRAGG<>.w_SELRAGG.or. .o_DATAPRE<>.w_DATAPRE
          .Calculate_TVQNJCWYBW()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,49,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AGKPM_ZOOM.Calculate(.w_TIPOENTE)
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
    endwith
  return

  proc Calculate_QTYFFRAAXR()
    with this
          * --- Inserimento prestazioni multiple
          GSAG_BIP(this;
             )
    endwith
  endproc
  proc Calculate_UXZYYWFHYA()
    with this
          * --- Modifica zoom per prodotto
          gsag_bpm(this;
              ,'ZAQ';
             )
    endwith
  endproc
  proc Calculate_BFGLLSLECP()
    with this
          * --- Inserimento prestazioni in atto di precetto
          GSAL_BCI(this;
              ,'INSER_MULTIPLO' ;
             )
    endwith
  endproc
  proc Calculate_TVQNJCWYBW()
    with this
          * --- Calcola data dopo selezione di raggruppamento
          gsag_bpm(this;
              ,'RAGG';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCACODINI_1_4.enabled = this.oPgFrm.Page1.oPag.oCACODINI_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCACODFIN_1_5.enabled = this.oPgFrm.Page1.oPag.oCACODFIN_1_5.mCond()
    this.oPgFrm.Page1.oPag.oDESCR_1_6.enabled = this.oPgFrm.Page1.oPag.oDESCR_1_6.mCond()
    this.oPgFrm.Page1.oPag.oDESCRSUPP_1_7.enabled = this.oPgFrm.Page1.oPag.oDESCRSUPP_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCACODINI_1_4.visible=!this.oPgFrm.Page1.oPag.oCACODINI_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCACODFIN_1_5.visible=!this.oPgFrm.Page1.oPag.oCACODFIN_1_5.mHide()
    this.oPgFrm.Page1.oPag.oDESCR_1_6.visible=!this.oPgFrm.Page1.oPag.oDESCR_1_6.mHide()
    this.oPgFrm.Page1.oPag.oDESCRSUPP_1_7.visible=!this.oPgFrm.Page1.oPag.oDESCRSUPP_1_7.mHide()
    this.oPgFrm.Page1.oPag.oNUMPRA_1_8.visible=!this.oPgFrm.Page1.oPag.oNUMPRA_1_8.mHide()
    this.oPgFrm.Page1.oPag.oTIPOLOGIA_1_9.visible=!this.oPgFrm.Page1.oPag.oTIPOLOGIA_1_9.mHide()
    this.oPgFrm.Page1.oPag.oDESPRA_1_11.visible=!this.oPgFrm.Page1.oPag.oDESPRA_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCODRESP_1_19.visible=!this.oPgFrm.Page1.oPag.oCODRESP_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDENOMRES_1_22.visible=!this.oPgFrm.Page1.oPag.oDENOMRES_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDATAPRE_1_25.visible=!this.oPgFrm.Page1.oPag.oDATAPRE_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oCODVAL_1_34.visible=!this.oPgFrm.Page1.oPag.oCODVAL_1_34.mHide()
    this.oPgFrm.Page1.oPag.oDESVAL_1_35.visible=!this.oPgFrm.Page1.oPag.oDESVAL_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oCODLISPRE_1_37.visible=!this.oPgFrm.Page1.oPag.oCODLISPRE_1_37.mHide()
    this.oPgFrm.Page1.oPag.oDESLIS_1_38.visible=!this.oPgFrm.Page1.oPag.oDESLIS_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oCADESINI_1_45.visible=!this.oPgFrm.Page1.oPag.oCADESINI_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oCADESFIN_1_47.visible=!this.oPgFrm.Page1.oPag.oCADESFIN_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oFLASPAN_1_61.visible=!this.oPgFrm.Page1.oPag.oFLASPAN_1_61.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("InsPrestaz")
          .Calculate_QTYFFRAAXR()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.AGKPM_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("AGKPM_ZOOM after query")
          .Calculate_UXZYYWFHYA()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
        if lower(cEvent)==lower("InsAttoPrecetto")
          .Calculate_BFGLLSLECP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_AGKPM_ZOOM after query")
          .Calculate_TVQNJCWYBW()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kpm
    If cevent ='w_NUMPRA SelectedFromZoom' and Not Empty(This.w_SELRAGG)
       UPDATE (This.w_AGKPM_ZOOM.cCursor) SET XCHK = 1 
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LetturaParAlte
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LetturaParAlte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LetturaParAlte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLGMSK,PAFLDESP";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LetturaParAlte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LetturaParAlte)
            select PACODAZI,PAFLGMSK,PAFLDESP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LetturaParAlte = NVL(_Link_.PACODAZI,space(5))
      this.w_FLGMSK = NVL(_Link_.PAFLGMSK,space(1))
      this.w_PAFLDESP = NVL(_Link_.PAFLDESP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LetturaParAlte = space(5)
      endif
      this.w_FLGMSK = space(1)
      this.w_PAFLDESP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LetturaParAlte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODINI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CACODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CACODINI))
          select CACODICE,CADESART,CACODART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCACODINI_1_4'),i_cWhere,'GSMA_ACA',"Servizi",'GSAGZKPM.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODINI)
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODINI = NVL(_Link_.CACODICE,space(20))
      this.w_CADESINI = NVL(_Link_.CADESART,space(40))
      this.w_CAARTINI = NVL(_Link_.CACODART,space(20))
      this.w_CADOBINI = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CACODINI = space(20)
      endif
      this.w_CADESINI = space(40)
      this.w_CAARTINI = space(20)
      this.w_CADOBINI = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_CAARTINI, 'FM-FO-DE') AND (EMPTY(.w_CADOBINI) OR .w_CADOBINI>=i_DatSys) AND (EMPTY(.w_CACODFIN) OR .w_CACODINI<=.w_CACODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di ricerca non valido, obsoleto oppure codice iniziale maggiore del codice finale")
        endif
        this.w_CACODINI = space(20)
        this.w_CADESINI = space(40)
        this.w_CAARTINI = space(20)
        this.w_CADOBINI = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODFIN
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CACODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CACODFIN))
          select CACODICE,CADESART,CACODART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCACODFIN_1_5'),i_cWhere,'GSMA_ACA',"Servizi",'GSAGZKPM.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODFIN)
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_CADESFIN = NVL(_Link_.CADESART,space(40))
      this.w_CAARTFIN = NVL(_Link_.CACODART,space(20))
      this.w_CADOBFIN = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CACODFIN = space(20)
      endif
      this.w_CADESFIN = space(40)
      this.w_CAARTFIN = space(20)
      this.w_CADOBFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_CAARTFIN, 'FM-FO-DE') AND (EMPTY(.w_CADOBFIN) OR .w_CADOBFIN>=i_DatSys) AND (EMPTY(.w_CACODINI) OR .w_CACODINI<=.w_CACODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di ricerca non valido, obsoleto oppure codice finale minore del codice iniziale")
        endif
        this.w_CACODFIN = space(20)
        this.w_CADESFIN = space(40)
        this.w_CAARTFIN = space(20)
        this.w_CADOBFIN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMPRA
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_NUMPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL,CNCODLIS,CNDTOBSO,CN__ENTE,CNDATFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_NUMPRA))
          select CNCODCAN,CNDESCAN,CNCODVAL,CNCODLIS,CNDTOBSO,CN__ENTE,CNDATFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_NUMPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL,CNCODLIS,CNDTOBSO,CN__ENTE,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_NUMPRA)+"%");

            select CNCODCAN,CNDESCAN,CNCODVAL,CNCODLIS,CNDTOBSO,CN__ENTE,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NUMPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oNUMPRA_1_8'),i_cWhere,'GSPR_ACN',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL,CNCODLIS,CNDTOBSO,CN__ENTE,CNDATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNCODVAL,CNCODLIS,CNDTOBSO,CN__ENTE,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNCODVAL,CNCODLIS,CNDTOBSO,CN__ENTE,CNDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_NUMPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_NUMPRA)
            select CNCODCAN,CNDESCAN,CNCODVAL,CNCODLIS,CNDTOBSO,CN__ENTE,CNDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(100))
      this.w_CODVAL = NVL(_Link_.CNCODVAL,space(3))
      this.w_CODLISPRE = NVL(_Link_.CNCODLIS,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_ENTEPRA1 = NVL(_Link_.CN__ENTE,space(10))
      this.w_CNDATFIN = NVL(cp_ToDate(_Link_.CNDATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NUMPRA = space(15)
      endif
      this.w_DESPRA = space(100)
      this.w_CODVAL = space(3)
      this.w_CODLISPRE = space(5)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_ENTEPRA1 = space(10)
      this.w_CNDATFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NUMPRA = space(15)
        this.w_DESPRA = space(100)
        this.w_CODVAL = space(3)
        this.w_CODLISPRE = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_ENTEPRA1 = space(10)
        this.w_CNDATFIN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_bRes and i_cCtrl='Drop'
      with this
        if i_bRes and !((.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) OR !Isalt())
          i_bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
          if not(i_bRes)
            this.w_NUMPRA = space(15)
            this.w_DESPRA = space(100)
            this.w_CODVAL = space(3)
            this.w_CODLISPRE = space(5)
            this.w_DATOBSO = ctod("  /  /  ")
            this.w_ENTEPRA1 = space(10)
            this.w_CNDATFIN = ctod("  /  /  ")
          endif
        endif
      endwith
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELRAGG
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_lTable = "PRE_ITER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2], .t., this.PRE_ITER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELRAGG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIT',True,'PRE_ITER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ITCODICE like "+cp_ToStrODBC(trim(this.w_SELRAGG)+"%");

          i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ITCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ITCODICE',trim(this.w_SELRAGG))
          select ITCODICE,ITDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ITCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SELRAGG)==trim(_Link_.ITCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStrODBC(trim(this.w_SELRAGG)+"%");

            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStr(trim(this.w_SELRAGG)+"%");

            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SELRAGG) and !this.bDontReportError
            deferred_cp_zoom('PRE_ITER','*','ITCODICE',cp_AbsName(oSource.parent,'oSELRAGG_1_10'),i_cWhere,'GSAG_MIT',"Raggruppamenti prestazioni",'GSAG_MIT.PRE_ITER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',oSource.xKey(1))
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELRAGG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(this.w_SELRAGG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',this.w_SELRAGG)
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELRAGG = NVL(_Link_.ITCODICE,space(10))
      this.w_COMODO = NVL(_Link_.ITDESCRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SELRAGG = space(10)
      endif
      this.w_COMODO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])+'\'+cp_ToStr(_Link_.ITCODICE,1)
      cp_ShowWarn(i_cKey,this.PRE_ITER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELRAGG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ENTEPRA
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ENTEPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ENTEPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EP__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_ENTEPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_ENTEPRA)
            select EPCODICE,EP__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ENTEPRA = NVL(_Link_.EPCODICE,space(10))
      this.w_TIPOENTE = NVL(_Link_.EP__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ENTEPRA = space(10)
      endif
      this.w_TIPOENTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ENTEPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRESP
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRESP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODRESP)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoPersona;
                     ,'DPCODICE',trim(this.w_CODRESP))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODRESP)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODRESP)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODRESP)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoPersona);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODRESP)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODRESP)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoPersona);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODRESP) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCODRESP_1_19'),i_cWhere,'',"Responsabili",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoPersona<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRESP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODRESP);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoPersona;
                       ,'DPCODICE',this.w_CODRESP)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRESP = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNRESP = NVL(_Link_.DPCOGNOM,space(10))
      this.w_NOMRESP = NVL(_Link_.DPNOME,space(10))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODRESP = space(5)
      endif
      this.w_COGNRESP = space(10)
      this.w_NOMRESP = space(10)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore obsoleto")
        endif
        this.w_CODRESP = space(5)
        this.w_COGNRESP = space(10)
        this.w_NOMRESP = space(10)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRESP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=APPOAZI
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_APPOAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_APPOAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLDTRP";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_APPOAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_APPOAZI)
            select PACODAZI,PAFLDTRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_APPOAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_FLDTRP = NVL(_Link_.PAFLDTRP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_APPOAZI = space(5)
      endif
      this.w_FLDTRP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_APPOAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLISPRE
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLISPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_CODLISPRE)+"%");
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_CODVAL);

          i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSVALLIS,LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSVALLIS',this.w_CODVAL;
                     ,'LSCODLIS',trim(this.w_CODLISPRE))
          select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSVALLIS,LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLISPRE)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODLISPRE) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(oSource.parent,'oCODLISPRE_1_37'),i_cWhere,'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODVAL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valuta listino incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LSVALLIS="+cp_ToStrODBC(this.w_CODVAL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',oSource.xKey(1);
                       ,'LSCODLIS',oSource.xKey(2))
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLISPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CODLISPRE);
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',this.w_CODVAL;
                       ,'LSCODLIS',this.w_CODLISPRE)
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLISPRE = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_SCOLIS = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODLISPRE = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_SCOLIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VALLIS=.w_CODVAL
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta listino incongruente")
        endif
        this.w_CODLISPRE = space(5)
        this.w_DESLIS = space(40)
        this.w_VALLIS = space(3)
        this.w_SCOLIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSVALLIS,1)+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLISPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCACODINI_1_4.value==this.w_CACODINI)
      this.oPgFrm.Page1.oPag.oCACODINI_1_4.value=this.w_CACODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODFIN_1_5.value==this.w_CACODFIN)
      this.oPgFrm.Page1.oPag.oCACODFIN_1_5.value=this.w_CACODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR_1_6.value==this.w_DESCR)
      this.oPgFrm.Page1.oPag.oDESCR_1_6.value=this.w_DESCR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRSUPP_1_7.value==this.w_DESCRSUPP)
      this.oPgFrm.Page1.oPag.oDESCRSUPP_1_7.value=this.w_DESCRSUPP
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPRA_1_8.value==this.w_NUMPRA)
      this.oPgFrm.Page1.oPag.oNUMPRA_1_8.value=this.w_NUMPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOLOGIA_1_9.RadioValue()==this.w_TIPOLOGIA)
      this.oPgFrm.Page1.oPag.oTIPOLOGIA_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELRAGG_1_10.value==this.w_SELRAGG)
      this.oPgFrm.Page1.oPag.oSELRAGG_1_10.value=this.w_SELRAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRA_1_11.value==this.w_DESPRA)
      this.oPgFrm.Page1.oPag.oDESPRA_1_11.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODRESP_1_19.value==this.w_CODRESP)
      this.oPgFrm.Page1.oPag.oCODRESP_1_19.value=this.w_CODRESP
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOMRES_1_22.value==this.w_DENOMRES)
      this.oPgFrm.Page1.oPag.oDENOMRES_1_22.value=this.w_DENOMRES
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAPRE_1_25.value==this.w_DATAPRE)
      this.oPgFrm.Page1.oPag.oDATAPRE_1_25.value=this.w_DATAPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_34.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_34.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_35.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_35.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLISPRE_1_37.value==this.w_CODLISPRE)
      this.oPgFrm.Page1.oPag.oCODLISPRE_1_37.value=this.w_CODLISPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESLIS_1_38.value==this.w_DESLIS)
      this.oPgFrm.Page1.oPag.oDESLIS_1_38.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESINI_1_45.value==this.w_CADESINI)
      this.oPgFrm.Page1.oPag.oCADESINI_1_45.value=this.w_CADESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESFIN_1_47.value==this.w_CADESFIN)
      this.oPgFrm.Page1.oPag.oCADESFIN_1_47.value=this.w_CADESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_60.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLASPAN_1_61.RadioValue()==this.w_FLASPAN)
      this.oPgFrm.Page1.oPag.oFLASPAN_1_61.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(ChkTipArt(.w_CAARTINI, 'FM-FO-DE') AND (EMPTY(.w_CADOBINI) OR .w_CADOBINI>=i_DatSys) AND (EMPTY(.w_CACODFIN) OR .w_CACODINI<=.w_CACODFIN))  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_CACODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODINI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di ricerca non valido, obsoleto oppure codice iniziale maggiore del codice finale")
          case   not(ChkTipArt(.w_CAARTFIN, 'FM-FO-DE') AND (EMPTY(.w_CADOBFIN) OR .w_CADOBFIN>=i_DatSys) AND (EMPTY(.w_CACODINI) OR .w_CACODINI<=.w_CACODFIN))  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_CACODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di ricerca non valido, obsoleto oppure codice finale minore del codice iniziale")
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(.w_PROG_PARENT = 'GSAG_AAT' OR !IsAlt())  and not(empty(.w_NUMPRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMPRA_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)  and not(.w_PROG_PARENT = 'GSAG_KPR' or .w_PROG_PARENT = 'GSAG_KPP')  and not(empty(.w_CODRESP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODRESP_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore obsoleto")
          case   (empty(.w_DATAPRE))  and not(.w_PROG_PARENT = 'GSAG_AAT' or .w_PROG_PARENT = 'GSAL_KCI')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAPRE_1_25.SetFocus()
            i_bnoObbl = !empty(.w_DATAPRE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODVAL))  and not(.w_PROG_PARENT = 'GSAG_AAT')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODVAL_1_34.SetFocus()
            i_bnoObbl = !empty(.w_CODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VALLIS=.w_CODVAL)  and not(.w_PROG_PARENT = 'GSAG_AAT' or Isahe())  and not(empty(.w_CODLISPRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODLISPRE_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta listino incongruente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_kpm
      IF i_bRes=.t.
          IF NOT(.w_PROG_PARENT='GSAG_AAT') AND EMPTY(.w_NUMPRA) AND IsAlt() AND NOT(.w_PROG_PARENT='GSAL_KCI')
            i_bRes = NOT(ah_YESNO("Le prestazioni selezionate risultano prive del campo pratica. Vuoi inserire la pratica?"))
          ENDIF
          IF i_bRes
            .w_RESCHK=0
            IF IsAlt() AND UPPER(THIS.oParentObject.cPrg)='GSAL_KCI'
               .NotifyEvent('InsAttoPrecetto')
            ELSE
            .NotifyEvent('InsPrestaz')
            ENDIF 
            IF .w_RESCHK<>0
              i_bRes=.f.
            ENDIF
            * tolgo il controllo sulla maschera padre, altrimenti si disabilita la toolbar
            .oPARENTOBJECT.Deactivate()
          ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LetturaParAlte = this.w_LetturaParAlte
    this.o_NUMPRA = this.w_NUMPRA
    this.o_SELRAGG = this.w_SELRAGG
    this.o_ENTEPRA1 = this.w_ENTEPRA1
    this.o_ENTEPRA2 = this.w_ENTEPRA2
    this.o_CODRESP = this.w_CODRESP
    this.o_DATAPRE = this.w_DATAPRE
    this.o_CODVAL = this.w_CODVAL
    return

enddefine

* --- Define pages as container
define class tgsag_kpmPag1 as StdContainer
  Width  = 850
  height = 556
  stdWidth  = 850
  stdheight = 556
  resizeXpos=544
  resizeYpos=239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODINI_1_4 as StdField with uid="FFIMPKLFUQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CACODINI", cQueryName = "CACODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di ricerca non valido, obsoleto oppure codice iniziale maggiore del codice finale",;
    HelpContextID = 78204527,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=134, Top=7, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_CACODINI"

  func oCACODINI_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oCACODINI_1_4.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCACODINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODINI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODINI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCACODINI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Servizi",'GSAGZKPM.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCACODINI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CACODINI
     i_obj.ecpSave()
  endproc

  add object oCACODFIN_1_5 as StdField with uid="AOIHXNVPYJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CACODFIN", cQueryName = "CACODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di ricerca non valido, obsoleto oppure codice finale minore del codice iniziale",;
    HelpContextID = 27872884,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=134, Top=33, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_CACODFIN"

  func oCACODFIN_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oCACODFIN_1_5.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCACODFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODFIN_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODFIN_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCACODFIN_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Servizi",'GSAGZKPM.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCACODFIN_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CACODFIN
     i_obj.ecpSave()
  endproc

  add object oDESCR_1_6 as AH_SEARCHFLD with uid="KVDDVWNUZP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCR", cQueryName = "DESCR",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione prestazione o parte di essa",;
    HelpContextID = 209605174,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=134, Top=7, InputMask=replicate('X',40)

  func oDESCR_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oDESCR_1_6.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDESCRSUPP_1_7 as AH_SEARCHMEMO with uid="HIKHGEICJP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCRSUPP", cQueryName = "DESCRSUPP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva prestazione o parte di essa",;
    HelpContextID = 8497274,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=134, Top=33

  func oDESCRSUPP_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oDESCRSUPP_1_7.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oNUMPRA_1_8 as StdField with uid="OYFAPEEGUV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NUMPRA", cQueryName = "NUMPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il codice della pratica da attribuire alle prestazioni",;
    HelpContextID = 41221418,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=134, Top=59, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSPR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_NUMPRA"

  func oNUMPRA_1_8.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_AAT' OR !IsAlt())
    endwith
  endfunc

  func oNUMPRA_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
      if bRes and !((.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) OR !Isalt())
         bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oNUMPRA_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMPRA_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oNUMPRA_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPR_ACN',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oNUMPRA_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSPR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_NUMPRA
     i_obj.ecpSave()
  endproc


  add object oTIPOLOGIA_1_9 as StdCombo with uid="BBZBIAJDXM",rtseq=9,rtrep=.f.,left=693,top=7,width=150,height=21;
    , ToolTipText = "Classificazione prestazione";
    , HelpContextID = 187313039;
    , cFormVar="w_TIPOLOGIA",RowSource=""+"Onorario civile,"+"Onorario penale,"+"Onorario stragiudiziale,"+"Diritto stragiudiziale,"+"Diritto civile,"+"Prestazione generica,"+"Prestazione a tempo,"+"Spesa,"+"Anticipazione,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOLOGIA_1_9.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    iif(this.value =3,'T',;
    iif(this.value =4,'R',;
    iif(this.value =5,'C',;
    iif(this.value =6,'G',;
    iif(this.value =7,'P',;
    iif(this.value =8,'S',;
    iif(this.value =9,'A',;
    iif(this.value =10,'K',;
    space(1))))))))))))
  endfunc
  func oTIPOLOGIA_1_9.GetRadio()
    this.Parent.oContained.w_TIPOLOGIA = this.RadioValue()
    return .t.
  endfunc

  func oTIPOLOGIA_1_9.SetRadio()
    this.Parent.oContained.w_TIPOLOGIA=trim(this.Parent.oContained.w_TIPOLOGIA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOLOGIA=='I',1,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='E',2,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='T',3,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='R',4,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='C',5,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='G',6,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='P',7,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='S',8,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='A',9,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='K',10,;
      0))))))))))
  endfunc

  func oTIPOLOGIA_1_9.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oSELRAGG_1_10 as StdField with uid="KEJKCSVKFW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SELRAGG", cQueryName = "SELRAGG",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Raggruppamento prestazioni",;
    HelpContextID = 41739046,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=693, Top=33, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRE_ITER", cZoomOnZoom="GSAG_MIT", oKey_1_1="ITCODICE", oKey_1_2="this.w_SELRAGG"

  func oSELRAGG_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELRAGG_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELRAGG_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRE_ITER','*','ITCODICE',cp_AbsName(this.parent,'oSELRAGG_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIT',"Raggruppamenti prestazioni",'GSAG_MIT.PRE_ITER_VZM',this.parent.oContained
  endproc
  proc oSELRAGG_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ITCODICE=this.parent.oContained.w_SELRAGG
     i_obj.ecpSave()
  endproc

  add object oDESPRA_1_11 as StdField with uid="BXHUYBQHHC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 41201098,;
   bGlobalFont=.t.,;
    Height=21, Width=399, Left=270, Top=59, InputMask=replicate('X',100)

  func oDESPRA_1_11.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_AAT' OR !IsAlt())
    endwith
  endfunc


  add object oBtn_1_16 as StdButton with uid="PHEWMSSLEL",left=795, top=36, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 241076458;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODRESP_1_19 as StdField with uid="XLTBQSGNBS",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODRESP", cQueryName = "CODRESP",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valore obsoleto",;
    ToolTipText = "Inserire il codice del responsabile da attribuire alle prestazioni",;
    HelpContextID = 21205978,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=112, Top=497, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoPersona", oKey_2_1="DPCODICE", oKey_2_2="this.w_CODRESP"

  func oCODRESP_1_19.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_KPR' or .w_PROG_PARENT = 'GSAG_KPP')
    endwith
  endfunc

  func oCODRESP_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODRESP_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODRESP_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoPersona)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoPersona)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCODRESP_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Responsabili",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oDENOMRES_1_22 as StdField with uid="EXHSESXJWG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DENOMRES", cQueryName = "DENOMRES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 29752695,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=178, Top=497, InputMask=replicate('X',50)

  func oDENOMRES_1_22.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_KPR' or .w_PROG_PARENT = 'GSAG_KPP')
    endwith
  endfunc

  add object oDATAPRE_1_25 as StdField with uid="TVOUIOKTWW",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DATAPRE", cQueryName = "DATAPRE",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Inserire la data da attribuire alle prestazioni",;
    HelpContextID = 27501002,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=66, Top=497

  func oDATAPRE_1_25.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_AAT' or .w_PROG_PARENT = 'GSAL_KCI')
    endwith
  endfunc

  add object oCODVAL_1_34 as StdField with uid="PYAJAVGDGI",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il codice valuta da attribuire alle prestazioni",;
    HelpContextID = 142578650,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=66, Top=521, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_34.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_AAT')
    endwith
  endfunc

  func oCODVAL_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODLISPRE)
        bRes2=.link_1_37('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDESVAL_1_35 as StdField with uid="CWPENMEBKE",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 142519754,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=123, Top=521, InputMask=replicate('X',35)

  func oDESVAL_1_35.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_AAT')
    endwith
  endfunc

  add object oCODLISPRE_1_37 as StdField with uid="NJSOCMNZLD",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CODLISPRE", cQueryName = "CODLISPRE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta listino incongruente",;
    ToolTipText = "Inserire il codice listino da attribuire alle prestazioni",;
    HelpContextID = 17403704,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=425, Top=521, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSVALLIS", oKey_1_2="this.w_CODVAL", oKey_2_1="LSCODLIS", oKey_2_2="this.w_CODLISPRE"

  func oCODLISPRE_1_37.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_AAT' or Isahe())
    endwith
  endfunc

  func oCODLISPRE_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLISPRE_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLISPRE_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LISTINI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStrODBC(this.Parent.oContained.w_CODVAL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStr(this.Parent.oContained.w_CODVAL)
    endif
    do cp_zoom with 'LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(this.parent,'oCODLISPRE_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oCODLISPRE_1_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LSVALLIS=w_CODVAL
     i_obj.w_LSCODLIS=this.parent.oContained.w_CODLISPRE
     i_obj.ecpSave()
  endproc

  add object oDESLIS_1_38 as StdField with uid="SVIDHCGAIK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 17345994,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=492, Top=521, InputMask=replicate('X',40)

  func oDESLIS_1_38.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_AAT' or Isahe())
    endwith
  endfunc


  add object oBtn_1_40 as StdButton with uid="ERHOXVEEGI",left=743, top=502, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 118900966;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_41 as StdButton with uid="TKCMXZXILU",left=795, top=502, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 126189638;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AGKPM_ZOOM as cp_szoombox with uid="CKWGSYWNMA",left=4, top=84, width=839,height=368,;
    caption='AGKPM_ZOOM',;
   bGlobalFont=.t.,;
    cTable="KEY_ARTI",cZoomFile="GSAG_KPM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Ricerca,w_SELRAGG Changed",;
    nPag=1;
    , HelpContextID = 188425797

  add object oCADESINI_1_45 as StdField with uid="VHYSFMYIRX",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CADESINI", cQueryName = "CADESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 93281903,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=305, Top=7, InputMask=replicate('X',40)

  func oCADESINI_1_45.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCADESFIN_1_47 as StdField with uid="VJSBLGKGMF",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CADESFIN", cQueryName = "CADESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42950260,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=305, Top=33, InputMask=replicate('X',40)

  func oCADESFIN_1_47.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oSELEZI_1_60 as StdRadio with uid="VNICEXLLTR",rtseq=42,rtrep=.f.,left=15, top=459, width=127,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_60.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 167779546
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 167779546
      this.Buttons(2).Top=15
      this.SetAll("Width",125)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_60.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_60.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  add object oFLASPAN_1_61 as StdCheck with uid="VYWYXNINHR",rtseq=43,rtrep=.f.,left=178, top=463, caption="Collega spesa e anticipazione",;
    ToolTipText = "Se attivo, collega la spesa e l'anticipazione alla prestazione selezionata",;
    HelpContextID = 225261910,;
    cFormVar="w_FLASPAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLASPAN_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLASPAN_1_61.GetRadio()
    this.Parent.oContained.w_FLASPAN = this.RadioValue()
    return .t.
  endfunc

  func oFLASPAN_1_61.SetRadio()
    this.Parent.oContained.w_FLASPAN=trim(this.Parent.oContained.w_FLASPAN)
    this.value = ;
      iif(this.Parent.oContained.w_FLASPAN=='S',1,;
      0)
  endfunc

  func oFLASPAN_1_61.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc


  add object oObj_1_63 as cp_runprogram with uid="JEGFKSDCLA",left=7, top=601, width=257,height=20,;
    caption='GSAG_BSL',;
   bGlobalFont=.t.,;
    prg="GSAG_BSL",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 11452238

  add object oStr_1_17 as StdString with uid="ANIXBUIFAP",Visible=.t., Left=19, Top=500,;
    Alignment=1, Width=85, Height=18,;
    Caption="Responsabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (not isalt() or .w_PROG_PARENT = 'GSAG_KPR' or .w_PROG_PARENT = 'GSAG_KPP')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="YKTEKQIXND",Visible=.t., Left=4, Top=9,;
    Alignment=1, Width=127, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="EKPXIOSQAT",Visible=.t., Left=4, Top=35,;
    Alignment=1, Width=127, Height=18,;
    Caption="Descrizione suppl.:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="LTSCZWRIFR",Visible=.t., Left=596, Top=9,;
    Alignment=1, Width=95, Height=18,;
    Caption="Classificazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="BSZHFPQCBJ",Visible=.t., Left=74, Top=62,;
    Alignment=1, Width=57, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_AAT' OR !IsAlt())
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="MXSCDWXXMU",Visible=.t., Left=18, Top=500,;
    Alignment=1, Width=42, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_AAT' or .w_PROG_PARENT = 'GSAL_KCI')
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="JANUXCYWDU",Visible=.t., Left=11, Top=522,;
    Alignment=1, Width=49, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_AAT')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="SLNFCCPZLY",Visible=.t., Left=364, Top=522,;
    Alignment=1, Width=57, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_PROG_PARENT = 'GSAG_AAT' or Isahe())
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="UYQPQOKTUT",Visible=.t., Left=49, Top=9,;
    Alignment=1, Width=82, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="TXQNIIJISJ",Visible=.t., Left=20, Top=35,;
    Alignment=1, Width=111, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="KWLOTBLPUX",Visible=.t., Left=18, Top=500,;
    Alignment=1, Width=86, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (isalt() or .w_PROG_PARENT = 'GSAG_KPR' or .w_PROG_PARENT = 'GSAL_KCI' or .w_PROG_PARENT = 'GSAG_KPP')
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="GJFVMANWYG",Visible=.t., Left=586, Top=35,;
    Alignment=1, Width=105, Height=18,;
    Caption="Raggruppamento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kpm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
