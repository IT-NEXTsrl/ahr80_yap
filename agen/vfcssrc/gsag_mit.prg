* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mit                                                        *
*              Raggruppamenti di prestazioni                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-26                                                      *
* Last revis.: 2012-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_mit"))

* --- Class definition
define class tgsag_mit as StdTrsForm
  Top    = 10
  Left   = 30

  * --- Standard Properties
  Width  = 656
  Height = 312+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-30"
  HelpContextID=264906601
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PRE_ITER_IDX = 0
  KEY_ARTI_IDX = 0
  PAR_ALTE_IDX = 0
  cFile = "PRE_ITER"
  cKeySelect = "ITCODICE"
  cKeyWhere  = "ITCODICE=this.w_ITCODICE"
  cKeyDetail  = "ITCODICE=this.w_ITCODICE"
  cKeyWhereODBC = '"ITCODICE="+cp_ToStrODBC(this.w_ITCODICE)';

  cKeyDetailWhereODBC = '"ITCODICE="+cp_ToStrODBC(this.w_ITCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PRE_ITER.ITCODICE="+cp_ToStrODBC(this.w_ITCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PRE_ITER.CPROWORD '
  cPrg = "gsag_mit"
  cComment = "Raggruppamenti di prestazioni"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  cAutoZoom = 'GSAG_MIT'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ISALT = .F.
  w_COD_AZI = space(5)
  o_COD_AZI = space(5)
  w_ITCODICE = space(10)
  w_ITDESCRI = space(60)
  w_CPROWORD = 0
  w_ITCOD_PR = space(41)
  w_ITCODPRE = space(20)
  o_ITCODPRE = space(20)
  w_RCACODART = space(20)
  w_RCADTOBSO = ctod('  /  /  ')
  w_ECACODART = space(20)
  w_ECADTOBSO = ctod('  /  /  ')
  w_DESPRE2 = space(40)
  w_DESPRE = space(40)
  w_ITDESPRE = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_TIPOENTE = space(1)
  w_SELRAGG = space(10)
  w_COMODO = space(10)
  w_PAFLDESP = space(1)
  w_ITNUMGIO = 0
  w_ITRIFPRO = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_mit
  w_OBJMSK = .null.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PRE_ITER','gsag_mit')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_mitPag1","gsag_mit",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Raggruppamento")
      .Pages(1).HelpContextID = 210210154
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oITCODICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='PAR_ALTE'
    this.cWorkTables[3]='PRE_ITER'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRE_ITER_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRE_ITER_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_ITCODICE = NVL(ITCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PRE_ITER where ITCODICE=KeySet.ITCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2],this.bLoadRecFilter,this.PRE_ITER_IDX,"gsag_mit")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRE_ITER')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRE_ITER.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PRE_ITER '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ITCODICE',this.w_ITCODICE  )
      select * from (i_cTable) PRE_ITER where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ISALT = isalt()
        .w_SELRAGG = space(10)
        .w_COMODO = space(10)
        .w_PAFLDESP = space(1)
        .w_COD_AZI = i_codazi
          .link_1_2('Load')
        .w_ITCODICE = NVL(ITCODICE,space(10))
        .w_ITDESCRI = NVL(ITDESCRI,space(60))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PRE_ITER')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_RCACODART = space(20)
          .w_RCADTOBSO = ctod("  /  /  ")
          .w_ECACODART = space(20)
          .w_ECADTOBSO = ctod("  /  /  ")
          .w_DESPRE2 = space(40)
          .w_DESPRE = space(40)
        .w_OBTEST = i_datsys
          .w_TIPOENTE = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_ITCOD_PR = NVL(ITCOD_PR,space(41))
          if link_2_2_joined
            this.w_ITCOD_PR = NVL(CACODICE202,NVL(this.w_ITCOD_PR,space(41)))
            this.w_DESPRE2 = NVL(CADESART202,space(40))
            this.w_ECACODART = NVL(CACODART202,space(20))
            this.w_ECADTOBSO = NVL(cp_ToDate(CADTOBSO202),ctod("  /  /  "))
          else
          .link_2_2('Load')
          endif
          .w_ITCODPRE = NVL(ITCODPRE,space(20))
          if link_2_3_joined
            this.w_ITCODPRE = NVL(CACODICE203,NVL(this.w_ITCODPRE,space(20)))
            this.w_DESPRE = NVL(CADESART203,space(40))
            this.w_RCACODART = NVL(CACODART203,space(20))
            this.w_RCADTOBSO = NVL(cp_ToDate(CADTOBSO203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
          .w_ITDESPRE = NVL(ITDESPRE,space(40))
          .w_ITNUMGIO = NVL(ITNUMGIO,0)
          .w_ITRIFPRO = NVL(ITRIFPRO,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_COD_AZI = i_codazi
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_6.enabled = .oPgFrm.Page1.oPag.oBtn_1_6.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_7.enabled = .oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_ISALT=.f.
      .w_COD_AZI=space(5)
      .w_ITCODICE=space(10)
      .w_ITDESCRI=space(60)
      .w_CPROWORD=10
      .w_ITCOD_PR=space(41)
      .w_ITCODPRE=space(20)
      .w_RCACODART=space(20)
      .w_RCADTOBSO=ctod("  /  /  ")
      .w_ECACODART=space(20)
      .w_ECADTOBSO=ctod("  /  /  ")
      .w_DESPRE2=space(40)
      .w_DESPRE=space(40)
      .w_ITDESPRE=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPOENTE=space(1)
      .w_SELRAGG=space(10)
      .w_COMODO=space(10)
      .w_PAFLDESP=space(1)
      .w_ITNUMGIO=0
      .w_ITRIFPRO=0
      if .cFunction<>"Filter"
        .w_ISALT = isalt()
        .w_COD_AZI = i_codazi
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_COD_AZI))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,6,.f.)
        if not(empty(.w_ITCOD_PR))
         .link_2_2('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_ITCODPRE))
         .link_2_3('Full')
        endif
        .DoRTCalc(8,13,.f.)
        .w_ITDESPRE = .w_DESPRE
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRE_ITER')
    this.DoRTCalc(16,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsag_mit
    this.w_OBJMSK = this
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oITCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oITDESCRI_1_5.enabled = i_bVal
      .Page1.oPag.oBtn_1_6.enabled = .Page1.oPag.oBtn_1_6.mCond()
      .Page1.oPag.oBtn_1_7.enabled = .Page1.oPag.oBtn_1_7.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oITCODICE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oITCODICE_1_3.enabled = .t.
        .Page1.oPag.oITDESCRI_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PRE_ITER',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ITCODICE,"ITCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ITDESCRI,"ITDESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
    i_lTable = "PRE_ITER"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PRE_ITER_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do gsag_srp with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_ITCOD_PR C(41);
      ,t_ITCODPRE C(20);
      ,t_DESPRE2 C(40);
      ,t_DESPRE C(40);
      ,t_ITDESPRE C(40);
      ,t_ITNUMGIO N(5);
      ,t_ITRIFPRO N(5);
      ,CPROWNUM N(10);
      ,t_RCACODART C(20);
      ,t_RCADTOBSO D(8);
      ,t_ECACODART C(20);
      ,t_ECADTOBSO D(8);
      ,t_OBTEST D(8);
      ,t_TIPOENTE C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mitbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oITCOD_PR_2_2.controlsource=this.cTrsName+'.t_ITCOD_PR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oITCODPRE_2_3.controlsource=this.cTrsName+'.t_ITCODPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE2_2_8.controlsource=this.cTrsName+'.t_DESPRE2'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE_2_9.controlsource=this.cTrsName+'.t_DESPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oITDESPRE_2_10.controlsource=this.cTrsName+'.t_ITDESPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oITNUMGIO_2_13.controlsource=this.cTrsName+'.t_ITNUMGIO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oITRIFPRO_2_14.controlsource=this.cTrsName+'.t_ITRIFPRO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(70)
    this.AddVLine(228)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
      *
      * insert into PRE_ITER
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRE_ITER')
        i_extval=cp_InsertValODBCExtFlds(this,'PRE_ITER')
        i_cFldBody=" "+;
                  "(ITCODICE,ITDESCRI,CPROWORD,ITCOD_PR,ITCODPRE"+;
                  ",ITDESPRE,ITNUMGIO,ITRIFPRO,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ITCODICE)+","+cp_ToStrODBC(this.w_ITDESCRI)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_ITCOD_PR)+","+cp_ToStrODBCNull(this.w_ITCODPRE)+;
             ","+cp_ToStrODBC(this.w_ITDESPRE)+","+cp_ToStrODBC(this.w_ITNUMGIO)+","+cp_ToStrODBC(this.w_ITRIFPRO)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRE_ITER')
        i_extval=cp_InsertValVFPExtFlds(this,'PRE_ITER')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'ITCODICE',this.w_ITCODICE)
        INSERT INTO (i_cTable) (;
                   ITCODICE;
                  ,ITDESCRI;
                  ,CPROWORD;
                  ,ITCOD_PR;
                  ,ITCODPRE;
                  ,ITDESPRE;
                  ,ITNUMGIO;
                  ,ITRIFPRO;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_ITCODICE;
                  ,this.w_ITDESCRI;
                  ,this.w_CPROWORD;
                  ,this.w_ITCOD_PR;
                  ,this.w_ITCODPRE;
                  ,this.w_ITDESPRE;
                  ,this.w_ITNUMGIO;
                  ,this.w_ITRIFPRO;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_ITCODPRE+t_ITCOD_PR))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PRE_ITER')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " ITDESCRI="+cp_ToStrODBC(this.w_ITDESCRI)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PRE_ITER')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  ITDESCRI=this.w_ITDESCRI;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_ITCODPRE+t_ITCOD_PR))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PRE_ITER
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PRE_ITER')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ITDESCRI="+cp_ToStrODBC(this.w_ITDESCRI)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",ITCOD_PR="+cp_ToStrODBCNull(this.w_ITCOD_PR)+;
                     ",ITCODPRE="+cp_ToStrODBCNull(this.w_ITCODPRE)+;
                     ",ITDESPRE="+cp_ToStrODBC(this.w_ITDESPRE)+;
                     ",ITNUMGIO="+cp_ToStrODBC(this.w_ITNUMGIO)+;
                     ",ITRIFPRO="+cp_ToStrODBC(this.w_ITRIFPRO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PRE_ITER')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ITDESCRI=this.w_ITDESCRI;
                     ,CPROWORD=this.w_CPROWORD;
                     ,ITCOD_PR=this.w_ITCOD_PR;
                     ,ITCODPRE=this.w_ITCODPRE;
                     ,ITDESPRE=this.w_ITDESPRE;
                     ,ITNUMGIO=this.w_ITNUMGIO;
                     ,ITRIFPRO=this.w_ITRIFPRO;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_ITCODPRE+t_ITCOD_PR))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PRE_ITER
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) AND not(Empty(t_ITCODPRE+t_ITCOD_PR))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_COD_AZI<>.w_COD_AZI
          .w_COD_AZI = i_codazi
          .link_1_2('Full')
        endif
        .DoRTCalc(3,13,.t.)
        if .o_ITCODPRE<>.w_ITCODPRE
          .w_ITDESPRE = .w_DESPRE
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_RCACODART with this.w_RCACODART
      replace t_RCADTOBSO with this.w_RCADTOBSO
      replace t_ECACODART with this.w_ECACODART
      replace t_ECADTOBSO with this.w_ECADTOBSO
      replace t_OBTEST with this.w_OBTEST
      replace t_TIPOENTE with this.w_TIPOENTE
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oITNUMGIO_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oITNUMGIO_2_13.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oITRIFPRO_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oITRIFPRO_2_14.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.visible=!this.oPgFrm.Page1.oPag.oBtn_1_6.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCOD_PR_2_2.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCOD_PR_2_2.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCODPRE_2_3.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCODPRE_2_3.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE2_2_8.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE2_2_8.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE_2_9.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE_2_9.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITDESPRE_2_10.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITDESPRE_2_10.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITNUMGIO_2_13.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITNUMGIO_2_13.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITRIFPRO_2_14.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITRIFPRO_2_14.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COD_AZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_AZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_AZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLDESP";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_COD_AZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_COD_AZI)
            select PACODAZI,PAFLDESP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_AZI = NVL(_Link_.PACODAZI,space(5))
      this.w_PAFLDESP = NVL(_Link_.PAFLDESP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COD_AZI = space(5)
      endif
      this.w_PAFLDESP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_AZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ITCOD_PR
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ITCOD_PR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ITCOD_PR)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ITCOD_PR))
          select CACODICE,CADESART,CACODART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ITCOD_PR)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_ITCOD_PR)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_ITCOD_PR)+"%");

            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ITCOD_PR) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oITCOD_PR_2_2'),i_cWhere,'GSMA_BZA',"Prestazioni",'GSAGZKPM.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ITCOD_PR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ITCOD_PR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ITCOD_PR)
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ITCOD_PR = NVL(_Link_.CACODICE,space(41))
      this.w_DESPRE2 = NVL(_Link_.CADESART,space(40))
      this.w_ECACODART = NVL(_Link_.CACODART,space(20))
      this.w_ECADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ITCOD_PR = space(41)
      endif
      this.w_DESPRE2 = space(40)
      this.w_ECACODART = space(20)
      this.w_ECADTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_ECACODART, "FM-FO-DE") AND (EMPTY(.w_ECADTOBSO) OR .w_ECADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_ITCOD_PR = space(41)
        this.w_DESPRE2 = space(40)
        this.w_ECACODART = space(20)
        this.w_ECADTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ITCOD_PR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CACODART as CACODART202"+ ",link_2_2.CADTOBSO as CADTOBSO202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on PRE_ITER.ITCOD_PR=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and PRE_ITER.ITCOD_PR=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ITCODPRE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ITCODPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ITCODPRE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ITCODPRE))
          select CACODICE,CADESART,CACODART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ITCODPRE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_ITCODPRE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_ITCODPRE)+"%");

            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ITCODPRE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oITCODPRE_2_3'),i_cWhere,'GSMA_BZA',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ITCODPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ITCODPRE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ITCODPRE)
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ITCODPRE = NVL(_Link_.CACODICE,space(20))
      this.w_DESPRE = NVL(_Link_.CADESART,space(40))
      this.w_RCACODART = NVL(_Link_.CACODART,space(20))
      this.w_RCADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ITCODPRE = space(20)
      endif
      this.w_DESPRE = space(40)
      this.w_RCACODART = space(20)
      this.w_RCADTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_ITCODPRE = space(20)
        this.w_DESPRE = space(40)
        this.w_RCACODART = space(20)
        this.w_RCADTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ITCODPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CACODICE as CACODICE203"+ ",link_2_3.CADESART as CADESART203"+ ",link_2_3.CACODART as CACODART203"+ ",link_2_3.CADTOBSO as CADTOBSO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on PRE_ITER.ITCODPRE=link_2_3.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and PRE_ITER.ITCODPRE=link_2_3.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oITCODICE_1_3.value==this.w_ITCODICE)
      this.oPgFrm.Page1.oPag.oITCODICE_1_3.value=this.w_ITCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oITDESCRI_1_5.value==this.w_ITDESCRI)
      this.oPgFrm.Page1.oPag.oITDESCRI_1_5.value=this.w_ITDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCOD_PR_2_2.value==this.w_ITCOD_PR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCOD_PR_2_2.value=this.w_ITCOD_PR
      replace t_ITCOD_PR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCOD_PR_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCODPRE_2_3.value==this.w_ITCODPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCODPRE_2_3.value=this.w_ITCODPRE
      replace t_ITCODPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCODPRE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE2_2_8.value==this.w_DESPRE2)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE2_2_8.value=this.w_DESPRE2
      replace t_DESPRE2 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE2_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE_2_9.value==this.w_DESPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE_2_9.value=this.w_DESPRE
      replace t_DESPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPRE_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITDESPRE_2_10.value==this.w_ITDESPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITDESPRE_2_10.value=this.w_ITDESPRE
      replace t_ITDESPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITDESPRE_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITNUMGIO_2_13.value==this.w_ITNUMGIO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITNUMGIO_2_13.value=this.w_ITNUMGIO
      replace t_ITNUMGIO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITNUMGIO_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITRIFPRO_2_14.value==this.w_ITRIFPRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITRIFPRO_2_14.value=this.w_ITRIFPRO
      replace t_ITRIFPRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITRIFPRO_2_14.value
    endif
    cp_SetControlsValueExtFlds(this,'PRE_ITER')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ITCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oITCODICE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_ITCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_CPROWORD)) AND not(Empty(t_ITCODPRE+t_ITCOD_PR)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(ChkTipArt(.w_ECACODART, "FM-FO-DE") AND (EMPTY(.w_ECADTOBSO) OR .w_ECADTOBSO>=i_datsys)) and not(empty(.w_ITCOD_PR)) and (not(Empty(.w_CPROWORD)) AND not(Empty(.w_ITCODPRE+.w_ITCOD_PR)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCOD_PR_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        case   not(ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys)) and not(empty(.w_ITCODPRE)) and (not(Empty(.w_CPROWORD)) AND not(Empty(.w_ITCODPRE+.w_ITCOD_PR)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITCODPRE_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        case   not(.w_ITRIFPRO < .w_CPROWORD AND chkriga(.w_OBJMSK, .w_ITRIFPRO,.F.)) and (IsAlt() AND .w_ITNUMGIO # 0) and (not(Empty(.w_CPROWORD)) AND not(Empty(.w_ITCODPRE+.w_ITCOD_PR)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oITRIFPRO_2_14
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_CPROWORD)) AND not(Empty(.w_ITCODPRE+.w_ITCOD_PR))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COD_AZI = this.w_COD_AZI
    this.o_ITCODPRE = this.w_ITCODPRE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) AND not(Empty(t_ITCODPRE+t_ITCOD_PR)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_ITCOD_PR=space(41)
      .w_ITCODPRE=space(20)
      .w_RCACODART=space(20)
      .w_RCADTOBSO=ctod("  /  /  ")
      .w_ECACODART=space(20)
      .w_ECADTOBSO=ctod("  /  /  ")
      .w_DESPRE2=space(40)
      .w_DESPRE=space(40)
      .w_ITDESPRE=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPOENTE=space(1)
      .w_ITNUMGIO=0
      .w_ITRIFPRO=0
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_ITCOD_PR))
        .link_2_2('Full')
      endif
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_ITCODPRE))
        .link_2_3('Full')
      endif
      .DoRTCalc(8,13,.f.)
        .w_ITDESPRE = .w_DESPRE
        .w_OBTEST = i_datsys
    endwith
    this.DoRTCalc(16,21,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_ITCOD_PR = t_ITCOD_PR
    this.w_ITCODPRE = t_ITCODPRE
    this.w_RCACODART = t_RCACODART
    this.w_RCADTOBSO = t_RCADTOBSO
    this.w_ECACODART = t_ECACODART
    this.w_ECADTOBSO = t_ECADTOBSO
    this.w_DESPRE2 = t_DESPRE2
    this.w_DESPRE = t_DESPRE
    this.w_ITDESPRE = t_ITDESPRE
    this.w_OBTEST = t_OBTEST
    this.w_TIPOENTE = t_TIPOENTE
    this.w_ITNUMGIO = t_ITNUMGIO
    this.w_ITRIFPRO = t_ITRIFPRO
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_ITCOD_PR with this.w_ITCOD_PR
    replace t_ITCODPRE with this.w_ITCODPRE
    replace t_RCACODART with this.w_RCACODART
    replace t_RCADTOBSO with this.w_RCADTOBSO
    replace t_ECACODART with this.w_ECACODART
    replace t_ECADTOBSO with this.w_ECADTOBSO
    replace t_DESPRE2 with this.w_DESPRE2
    replace t_DESPRE with this.w_DESPRE
    replace t_ITDESPRE with this.w_ITDESPRE
    replace t_OBTEST with this.w_OBTEST
    replace t_TIPOENTE with this.w_TIPOENTE
    replace t_ITNUMGIO with this.w_ITNUMGIO
    replace t_ITRIFPRO with this.w_ITRIFPRO
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mitPag1 as StdContainer
  Width  = 652
  height = 312
  stdWidth  = 652
  stdheight = 312
  resizeXpos=431
  resizeYpos=225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oITCODICE_1_3 as StdField with uid="KKVHUALWQH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ITCODICE", cQueryName = "ITCODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice raggruppamento",;
    HelpContextID = 231301579,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=71, Top=21, InputMask=replicate('X',10)

  add object oITDESCRI_1_5 as StdField with uid="KDVTVDAGDG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ITDESCRI", cQueryName = "ITDESCRI",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 145715663,;
   bGlobalFont=.t.,;
    Height=21, Width=482, Left=160, Top=21, InputMask=replicate('X',60)


  add object oBtn_1_6 as StdButton with uid="NQXZKOURAE",left=542, top=45, width=48,height=45,;
    CpPicture="bmp\PARAMETRI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire il caricamento multiplo delle prestazioni";
    , HelpContextID = 63209766;
    , Caption='\<Ins. Mult.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      do GSAG_KIP with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    with this.Parent.oContained
      return (inlist(.cfunction,"Edit","Load"))
    endwith
  endfunc

  func oBtn_1_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! inlist(.cfunction,"Edit","Load"))
    endwith
   endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="XVFSKZRQBE",left=594, top=45, width=48,height=45,;
    CpPicture="copy.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per duplicare il raggruppamento prestazioni";
    , HelpContextID = 171528758;
    , tabstop=.f., Caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      do GSAG_KDW with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction#"Query" OR (.cFunction="Query" AND EMPTY(.w_ITCODICE)))
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=20, top=98, width=627,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Progr.",Field2="ITCOD_PR",Label2="Codice",Field3="ITCODPRE",Label3="",Field4="ITNUMGIO",Label4="iif(w_ISALT, ah_MsgFormat('N. gg.'),'')",Field5="ITRIFPRO",Label5="iif(w_ISALT, ah_MsgFormat('Rif.prog.'),'')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 51184250

  add object oStr_1_4 as StdString with uid="ANBPUWFXDQ",Visible=.t., Left=21, Top=22,;
    Alignment=1, Width=46, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=10,top=117,;
    width=622+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=11,top=118,width=621+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oITCOD_PR_2_2
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oITCODPRE_2_3
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsag_mitBodyRow as CPBodyRowCnt
  Width=612
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="UQVCOQAWDN",rtseq=5,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Numero riga",;
    HelpContextID = 83512682,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oITCOD_PR_2_2 as StdTrsField with uid="RDFWUWBSAP",rtseq=6,rtrep=.t.,;
    cFormVar="w_ITCOD_PR",value=space(41),;
    ToolTipText = "Codice prestazione",;
    HelpContextID = 63529432,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=51, Top=0, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_ITCOD_PR"

  func oITCOD_PR_2_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!Isahe())
    endwith
    endif
  endfunc

  func oITCOD_PR_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oITCOD_PR_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oITCOD_PR_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oITCOD_PR_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",'GSAGZKPM.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oITCOD_PR_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ITCOD_PR
    i_obj.ecpSave()
  endproc

  add object oITCODPRE_2_3 as StdTrsField with uid="IZAFMWAFLE",rtseq=7,rtrep=.t.,;
    cFormVar="w_ITCODPRE",value=space(20),;
    ToolTipText = "Codice prestazione",;
    HelpContextID = 80306635,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=51, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_ITCODPRE"

  func oITCODPRE_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Isahe())
    endwith
    endif
  endfunc

  func oITCODPRE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oITCODPRE_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oITCODPRE_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oITCODPRE_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",''+iif(NOT IsAlt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oITCODPRE_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ITCODPRE
    i_obj.ecpSave()
  endproc

  add object oDESPRE2_2_8 as StdTrsField with uid="QCRNFEQDUV",rtseq=12,rtrep=.t.,;
    cFormVar="w_DESPRE2",value=space(40),enabled=.f.,;
    HelpContextID = 178999862,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=398, Left=209, Top=0, InputMask=replicate('X',40)

  func oDESPRE2_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!Isahe())
    endwith
    endif
  endfunc

  add object oDESPRE_2_9 as StdTrsField with uid="OBHAZKCQQX",rtseq=13,rtrep=.t.,;
    cFormVar="w_DESPRE",value=space(40),enabled=.f.,;
    HelpContextID = 89435594,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=398, Left=209, Top=0, InputMask=replicate('X',40)

  func oDESPRE_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Isahe() OR .w_PAFLDESP='S')
    endwith
    endif
  endfunc

  add object oITDESPRE_2_10 as StdTrsField with uid="ZLICWZBWWD",rtseq=14,rtrep=.t.,;
    cFormVar="w_ITDESPRE",value=space(40),;
    HelpContextID = 95384011,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=209, Top=0, InputMask=replicate('X',40)

  func oITDESPRE_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PAFLDESP='N' OR NOT ISALT())
    endwith
    endif
  endfunc

  add object oITNUMGIO_2_13 as StdTrsField with uid="GQJCSNIAPW",rtseq=20,rtrep=.t.,;
    cFormVar="w_ITNUMGIO",value=0,;
    ToolTipText = "Giorni di decorrenza",;
    HelpContextID = 60812843,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=507, Top=0, cSayPict=["@Z 99999"], cGetPict=["@Z 99999"]

  func oITNUMGIO_2_13.mCond()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oITNUMGIO_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
    endif
  endfunc

  add object oITRIFPRO_2_14 as StdTrsField with uid="TZVLNXFVTM",rtseq=21,rtrep=.t.,;
    cFormVar="w_ITRIFPRO",value=0,;
    HelpContextID = 82072021,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=559, Top=0, cSayPict=["@Z 99999"], cGetPict=["@Z 99999"]

  func oITRIFPRO_2_14.mCond()
    with this.Parent.oContained
      return (IsAlt() AND .w_ITNUMGIO # 0)
    endwith
  endfunc

  func oITRIFPRO_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
    endif
  endfunc

  func oITRIFPRO_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ITRIFPRO < .w_CPROWORD AND chkriga(.w_OBJMSK, .w_ITRIFPRO,.F.))
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mit','PRE_ITER','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ITCODICE=PRE_ITER.ITCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
