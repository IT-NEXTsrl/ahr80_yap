* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kut                                                        *
*              Associazione utenti per tipo attivit� in modalit� wizard        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-15                                                      *
* Last revis.: 2011-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kut",oParentObject))

* --- Class definition
define class tgsag_kut as StdForm
  Top    = 7
  Left   = 9

  * --- Standard Properties
  Width  = 622
  Height = 451
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-12-30"
  HelpContextID=65677161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_kut"
  cComment = "Associazione utenti per tipo attivit� in modalit� wizard"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FL_DELETE = space(1)
  w_OB_TEST = space(10)
  w_PATIPRIS = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_ZOOM_UTE = .NULL.
  w_ZOOM_CAU = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kutPag1","gsag_kut",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFL_DELETE_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM_UTE = this.oPgFrm.Pages(1).oPag.ZOOM_UTE
    this.w_ZOOM_CAU = this.oPgFrm.Pages(1).oPag.ZOOM_CAU
    DoDefault()
    proc Destroy()
      this.w_ZOOM_UTE = .NULL.
      this.w_ZOOM_CAU = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FL_DELETE=space(1)
      .w_OB_TEST=space(10)
      .w_PATIPRIS=space(1)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      .oPgFrm.Page1.oPag.ZOOM_UTE.Calculate()
        .w_FL_DELETE = 'N'
      .oPgFrm.Page1.oPag.ZOOM_CAU.Calculate()
          .DoRTCalc(2,2,.f.)
        .w_PATIPRIS = 'P'
        .w_DTBSO_CAUMATTI = i_DatSys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOM_UTE.Calculate()
        .oPgFrm.Page1.oPag.ZOOM_CAU.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM_UTE.Calculate()
        .oPgFrm.Page1.oPag.ZOOM_CAU.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM_UTE.Event(cEvent)
      .oPgFrm.Page1.oPag.ZOOM_CAU.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFL_DELETE_1_4.RadioValue()==this.w_FL_DELETE)
      this.oPgFrm.Page1.oPag.oFL_DELETE_1_4.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_kutPag1 as StdContainer
  Width  = 618
  height = 451
  stdWidth  = 618
  stdheight = 451
  resizeXpos=192
  resizeYpos=283
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOM_UTE as cp_szoombox with uid="WODZOSNAJX",left=4, top=20, width=607,height=136,;
    caption='ZOOM_UTE',;
   bGlobalFont=.t.,;
    cTable="DIPENDEN",cZoomFile="GSAG_MPP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 123215323

  add object oFL_DELETE_1_4 as StdCheck with uid="DVRJNQLCQX",rtseq=1,rtrep=.f.,left=4, top=374, caption="Cancella i tipi attivit� precedentemente attivati per le persone selezionate",;
    ToolTipText = "Cancella i tipi attivit� precedentemente attivati per le persone selezionate",;
    HelpContextID = 212868602,;
    cFormVar="w_FL_DELETE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFL_DELETE_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFL_DELETE_1_4.GetRadio()
    this.Parent.oContained.w_FL_DELETE = this.RadioValue()
    return .t.
  endfunc

  func oFL_DELETE_1_4.SetRadio()
    this.Parent.oContained.w_FL_DELETE=trim(this.Parent.oContained.w_FL_DELETE)
    this.value = ;
      iif(this.Parent.oContained.w_FL_DELETE=='S',1,;
      0)
  endfunc


  add object ZOOM_CAU as cp_szoombox with uid="HJCERMYNBW",left=4, top=172, width=607,height=199,;
    caption='ZOOM_CAU',;
   bGlobalFont=.t.,;
    cTable="CAUMATTI",cZoomFile="GSAG1AAT",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 89660907


  add object oBtn_1_8 as StdButton with uid="NLRIOAGGKF",left=508, top=400, width=48,height=45,;
    CpPicture="..\EXE\BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Associa le persone selezionate ai tipi attivit�";
    , HelpContextID = 65656602;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSAG_BWA(this.Parent.oContained,"CAU_UTE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="ABKYYMEJLO",left=561, top=400, width=48,height=45,;
    CpPicture="..\EXE\BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 257043206;
    , Caption='\<Annulla';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="IIPQFPVHAG",Visible=.t., Left=5, Top=4,;
    Alignment=0, Width=77, Height=18,;
    Caption="Utenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="GFIMBMRTTJ",Visible=.t., Left=7, Top=155,;
    Alignment=0, Width=77, Height=18,;
    Caption="Tipi attivit�"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kut','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
