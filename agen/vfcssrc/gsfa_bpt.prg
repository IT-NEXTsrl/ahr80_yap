* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bpt                                                        *
*              Prepara dati treeview strutture                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-21                                                      *
* Last revis.: 2010-05-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCURSORE,pROOT,w_EVGRUPPO,w_EVPERSON
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bpt",oParentObject,m.pCURSORE,m.pROOT,m.w_EVGRUPPO,m.w_EVPERSON)
return(i_retval)

define class tgsfa_bpt as StdBatch
  * --- Local variables
  pCURSORE = space(10)
  pROOT = space(20)
  w_EVGRUPPO = space(5)
  w_EVPERSON = space(5)
  w_COUTCURS = space(100)
  w_INPCURS = space(100)
  w_CRIFTABLE = space(100)
  w_RIFKEY = space(100)
  w_CEXPTABLE = space(100)
  w_EXPKEY = space(100)
  w_REPKEY = space(100)
  w_EXPFIELD = space(100)
  w_OTHERFLD = space(100)
  w_CODSTR = space(10)
  w_ROOT = space(20)
  w_RESULT = 0
  * --- WorkFile variables
  TMP_STR_idx=0
  TIPEVENT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.pCURSORE = IIF(VARTYPE(this.pCURSORE)="C", this.pCURSORE, "")
    this.pROOT = IIF(VARTYPE(this.pROOT)="C", this.pROOT, "")
    if EMPTY(this.pCURSORE)
      i_retcode = 'stop'
      i_retval = -1
      return
    endif
    * --- Create temporary table TMP_STR
    i_nIdx=cp_AddTableDef('TMP_STR') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\agen\exe\query\gsfa1bpt',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_STR_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Definizione parametri da passare alla CP_EXPDB
    * --- Cursore utilizzato dalla Tree View
    this.w_COUTCURS = this.pCURSORE
    * --- Cursore di partenza
    this.w_INPCURS = "QUERY"
    * --- Tabella di riferimento
    this.w_CRIFTABLE = "TMP_STR"
    * --- Chiave anagrafica componenti
    this.w_RIFKEY = "TETIPEVE"
    * --- Tabella di esplosione
    this.w_CEXPTABLE = "TIPEVENT"
    * --- Chiave della movimentazione contenente la struttura
    this.w_EXPKEY = "TEPADEVE,TETIPEVE"
    * --- Chiave ripetuta nella movimentazione contenente la struttura
    this.w_REPKEY = "TETIPEVE"
    * --- Campi per espolosione
    this.w_EXPFIELD = ""
    * --- Altri campi (ordinamento)
    this.w_OTHERFLD = ""
    this.w_ROOT = this.pROOT
    VQ_EXEC("..\AGEN\EXE\QUERY\GSFA_BPT.VQR",this,"query")
    SELECT "QUERY"
    if RECCOUNT()>0
      * --- Costruisco il cursore dei dati della Tree View
      CP_EXPDB (this.w_COUTCURS,this.w_INPCURS,this.w_CRIFTABLE,this.w_RIFKEY,this.w_CEXPTABLE,this.w_EXPKEY,this.w_REPKEY,this.w_EXPFIELD,this.w_OTHERFLD,this.w_OTHERFLD)
      this.w_RESULT = 1
    else
      this.w_RESULT = -1
    endif
    if USED("QUERY")
      USE IN "QUERY"
    endif
    i_retcode = 'stop'
    i_retval = this.w_RESULT
    return
  endproc


  proc Init(oParentObject,pCURSORE,pROOT,w_EVGRUPPO,w_EVPERSON)
    this.pCURSORE=pCURSORE
    this.pROOT=pROOT
    this.w_EVGRUPPO=w_EVGRUPPO
    this.w_EVPERSON=w_EVPERSON
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMP_STR'
    this.cWorkTables[2]='TIPEVENT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCURSORE,pROOT,w_EVGRUPPO,w_EVPERSON"
endproc
