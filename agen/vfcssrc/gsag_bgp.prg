* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bgp                                                        *
*              Generazione prestazioni                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-12                                                      *
* Last revis.: 2014-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPADRE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bgp",oParentObject,m.pPADRE)
return(i_retval)

define class tgsag_bgp as StdBatch
  * --- Local variables
  pPADRE = .NULL.
  w_PRCODATT = space(20)
  w_PRSERAGG = space(10)
  w_PRDESPRE = space(40)
  w_PRCODRES = space(5)
  w_PRNUMPRA = space(15)
  w_PRDESAGG = space(0)
  w_PRCODOPE = 0
  w_PRDATMOD = ctod("  /  /  ")
  w_PRUNIMIS = space(3)
  w_PRQTAMOV = 0
  w_PRCENCOS = space(15)
  w_PRPREZZO = 0
  w_PRVALRIG = 0
  w_PROREEFF = 0
  w_PRMINEFF = 0
  w_PRCOSINT = 0
  w_PRCOSUNI = 0
  w_PRFLDEFF = space(1)
  w_PRPREMIN = 0
  w_PRPREMAX = 0
  w_PRGAZUFF = space(6)
  w_PRVOCCOS = space(15)
  w_PR_SEGNO = space(1)
  w_PRATTIVI = space(15)
  w_PRINICOM = ctod("  /  /  ")
  w_PRFINCOM = ctod("  /  /  ")
  w_PRQTAUM1 = 0
  w_PRTIPRIG = space(1)
  w_PRTCOINI = ctod("  /  /  ")
  w_PRTCOFIN = ctod("  /  /  ")
  w_PRCODNOM = space(15)
  w_PRTIPRI2 = space(1)
  w_PRRIFPRE = 0
  w_PRRIGPRE = space(1)
  w_PRRIFNOT = space(10)
  w_PRCODANT = space(20)
  w_PRCODSPE = space(20)
  w_PRIMPANT = 0
  w_PRIMPSPE = 0
  w_PRSERATT = space(20)
  w_PRROWATT = 0
  w_PRCODVAL = space(3)
  w_PRCODLIS = space(5)
  w_PR__DATA = ctod("  /  /  ")
  w_PRTIPDOC = space(5)
  w_LPRRIGPRE = space(1)
  w_UTCC = 0
  w_UTDC = ctod("  /  /  ")
  w_UTDV = ctod("  /  /  ")
  w_PRSERIAL = space(10)
  w_PRNUMPRE = 0
  w_PRROWBOZ = 0
  w_PRROWFAA = 0
  w_PRROWFAT = 0
  w_PRROWNOT = 0
  w_PRROWPRA = 0
  w_PRROWPRO = 0
  w_PROPINS = 0
  w_PROPATT = 0
  w_NUMPROP = 0
  w_RETVAL = space(254)
  w_PADRE = .NULL.
  w_bUnderTran = .f.
  w_PRRIFPRO = space(10)
  w_PRRIFFAT = space(10)
  w_PRRIFNOT = space(10)
  w_PRRIFBOZ = space(10)
  w_PRRIFFAA = space(10)
  w_PRRIFPRA = space(10)
  w_LPRSERIAL = space(10)
  w_TIPDOC = space(5)
  w_CODSPE = space(5)
  w_CODANT = space(5)
  w_PACAUDOC = space(5)
  w_SER_RETURN = space(10)
  w_CODSES = space(15)
  w_FLRESTO = .f.
  w_PARAME = 0
  w_TOTCALC = 0
  w_TOTPARAME = 0
  w_PRPREZZO = 0
  w_TOTALE = 0
  w_SERTOT = space(10)
  w_ROWNUM = 0
  w_PPRSERIAL = space(10)
  w_CALRIF = .f.
  w_OLD_PRROWATT = 0
  w_COST_ORA = 0
  w_CN__ENTE = space(10)
  w_CNFLAPON = space(1)
  w_CNFLVALO = space(1)
  w_CNFLVALO1 = space(1)
  w_CNIMPORT = 0
  w_CNCALDIR = space(1)
  w_CNCOECAL = 0
  w_CNUFFICI = space(10)
  w_CNPARASS = 0
  w_CNFLAMPA = space(1)
  w_CNTARTEM = space(1)
  w_CNTARCON = 0
  w_CONTRACN = space(5)
  w_TIPCONCO = space(1)
  w_LISCOLCO = space(5)
  w_STATUSCO = space(1)
  w_PRCODART = space(20)
  w_UNMIS3 = space(20)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_FLUSEP = space(1)
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(1)
  w_DECUNI = 0
  w_DETIPPRA = space(10)
  w_CNMATOBB = space(1)
  w_CNASSCTP = space(1)
  w_CNCOMPLX = space(1)
  w_CNPROFMT = space(1)
  w_CNESIPOS = space(1)
  w_CNPERPLX = 0
  w_CNPERPOS = 0
  w_UNMIS1 = space(3)
  w_CALCONC = 0
  * --- WorkFile variables
  PRE_STAZ_idx=0
  RIS_ESTR_idx=0
  DET_PART_idx=0
  DIPENDEN_idx=0
  OFFDATTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_RETVAL = ""
    this.w_PADRE = this.pPADRE
    this.w_PADRE.w_UTCC = i_CODUTE
    this.w_PADRE.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_PADRE.w_UTDV = cp_CharToDate("  -  -    ")
    this.w_bUnderTran = vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    if this.w_bUnderTran
      * --- begin transaction
      cp_BeginTrs()
    endif
     
 DIMENSION ARRPART(1,2)
    if EMPTY(this.w_RETVAL)
      this.w_PRCODANT = this.w_PADRE.cCodant
      this.w_PRCODSPE = this.w_PADRE.cCodspe
      this.w_PRIMPANT = this.w_PADRE.nImpant
      this.w_PRIMPSPE = this.w_PADRE.nImpspe
      this.w_PADRE.SetLocalValues(This)     
      this.w_TIPDOC = this.w_PRTIPDOC
      * --- Leggo causale documento da applicare
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "PACODAZI"
      ArrWhere(1,2) = i_CODAZI
      this.w_TIPDOC = this.w_PADRE.ReadTable( "PAR_ALTE" , "PACAUPRE" , @ArrWhere )
      if !(Not Empty(this.w_PRCODANT) and Not Empty(this.w_PRCODSPE) and Not Empty(this.w_PRCODSPE) )
        this.w_CODSPE = this.w_PADRE.ReadTable( "PAR_ALTE" , "PAPRESPE" , @ArrWhere )
        this.w_CODANT = this.w_PADRE.ReadTable( "PAR_ALTE" , "PAPREANT" , @ArrWhere )
      endif
      if this.w_PRIMPANT>0 AND Empty(Nvl(this.w_PRCODANT," "))
        this.w_PRCODANT = this.w_CODANT
      endif
      if this.w_PRIMPSPE>0 AND Empty(Nvl(this.w_PRCODSPE," "))
        this.w_PRCODSPE = this.w_CODSPE
      endif
      Release ArrWhere
      if EMPTY(this.w_PRCODLIS)
        DIMENSION ArrWhere(1,2)
        ArrWhere(1,1) = "PACODAZI"
        ArrWhere(1,2) = i_CODAZI
        this.w_PRCODLIS = this.w_PADRE.ReadTable( "PAR_AGEN" , "PACODLIS" , @ArrWhere )
        Release ArrWhere
      endif
      if this.w_PADRE.bParass
        * --- Solo se ho le parti
        * --- PACAUDOC
        DIMENSION ArrWhere(1,2)
        ArrWhere(1,1) = "PACODAZI"
        ArrWhere(1,2) = i_CODAZI
        this.w_PACAUDOC = this.w_PADRE.ReadTable( "PAR_PRAT" , "PACAUDOC" , @ArrWhere )
        Release ArrWhere
      endif
      this.w_PRTIPDOC = this.w_TIPDOC
      if Empty(this.w_PRCODRES)
        this.w_PRCODRES = ReadResp(i_codute, "C", this.w_PRNUMPRA)
      endif
      this.w_LPRRIGPRE = this.w_PRRIGPRE
      if this.w_PADRE.bParass
        this.w_PRRIFPRO = "@@@@@@@@@@"
        this.w_PRRIFFAT = "@@@@@@@@@@"
        this.w_PRRIFBOZ = "@@@@@@@@@@"
        this.w_PRRIFFAA = "@@@@@@@@@@"
        this.w_PRRIFPRA = "@@@@@@@@@@"
        this.w_PRRIFNOT = Space(10)
      endif
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_SER_RETURN = this.w_PADRE.w_PRSERIAL
      if Empty(this.w_RETVAL) and this.w_PADRE.bParass
        this.w_TOTPARAME = 0
        this.w_PADRE.SetLocalValues(This)     
        this.w_TOTALE = this.w_PRPREZZO
        this.w_TOTCALC = this.w_PRPREZZO
        this.w_PPRSERIAL = this.w_PADRE.w_PRSERIAL
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_PRRIGPRE="S"
        this.w_LPRRIGPRE = "N"
        if Not Empty(Nvl(this.w_PRSERATT," "))
          this.w_OLD_PRROWATT = this.w_PRROWATT
          * --- Select from OFFDATTI
          i_nConn=i_TableProp[this.OFFDATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2],.t.,this.OFFDATTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select DAVALRIG,DASERIAL,DARIFPRE,DACODATT,DATIPRIG,DADESATT,DADESAGG,CPROWNUM  from "+i_cTable+" OFFDATTI ";
                +" where DASERIAL="+cp_ToStrODBC(this.w_PRSERATT)+" AND DARIFPRE="+cp_ToStrODBC(this.w_PRROWATT)+"";
                 ,"_Curs_OFFDATTI")
          else
            select DAVALRIG,DASERIAL,DARIFPRE,DACODATT,DATIPRIG,DADESATT,DADESAGG,CPROWNUM from (i_cTable);
             where DASERIAL=this.w_PRSERATT AND DARIFPRE=this.w_PRROWATT;
              into cursor _Curs_OFFDATTI
          endif
          if used('_Curs_OFFDATTI')
            select _Curs_OFFDATTI
            locate for 1=1
            do while not(eof())
            this.w_PRCODANT = Nvl(_Curs_OFFDATTI.DACODATT," ")
            this.w_PRIMPANT = Nvl(_Curs_OFFDATTI.DAVALRIG,0)
            this.w_PRDESPRE = _Curs_OFFDATTI.DADESATT
            this.w_PRDESAGG = _Curs_OFFDATTI.DADESAGG
            if Nvl(_Curs_OFFDATTI.DATIPRIG," ")="D"
              this.Page_5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if Empty(this.w_RETVAL) and this.w_PADRE.bParass
                this.w_TOTALE = this.w_PRIMPANT
                this.w_TOTCALC = this.w_PRIMPANT
                this.w_ROWNUM = 0
                this.w_CALRIF = .T.
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
              select _Curs_OFFDATTI
              continue
            enddo
            use
          endif
          this.w_PRROWATT = this.w_OLD_PRROWATT
        else
          if Not Empty(this.w_PRCODANT) AND this.w_PRIMPANT<>0
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if Empty(this.w_RETVAL) and this.w_PADRE.bParass
              this.w_TOTALE = this.w_PRIMPANT
              this.w_TOTCALC = this.w_PRIMPANT
              this.w_ROWNUM = 0
              this.w_CALRIF = .T.
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          if Not Empty(this.w_PRCODSPE) AND this.w_PRIMPSPE<>0
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_CALRIF = .F.
            if Empty(this.w_RETVAL) and this.w_PADRE.bParass
              this.w_TOTALE = this.w_PRIMPSPE
              this.w_TOTCALC = this.w_PRIMPSPE
              this.w_ROWNUM = 0
              this.w_CALRIF = .T.
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
      endif
    endif
    this.w_PADRE.w_PRSERIAL = this.w_SER_RETURN
    if this.w_bUnderTran
      if EMPTY(this.w_RETVAL)
        * --- commit
        cp_EndTrs(.t.)
      else
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE.SetObjectValues(This)     
    this.w_PADRE.w_PRSERIAL = Space(10)
    i_Conn=i_TableProp[this.PRE_STAZ_IDX, 3]
    cp_NextTableProg(this.w_PADRE, i_Conn, "SEPRE", "i_codazi,w_PRSERIAL")
    if Type("g_NOGENPRO")="U" or g_NOGENPRO="N"
      cp_AskTableProg(this.w_PADRE, i_Conn, "NUPRE", "i_codazi,w_PRNUMPRA,w_PRNUMPRE")
      if this.w_PADRE.w_PRNUMPRE>1
        this.w_PADRE.w_PRNUMPRE = this.w_PADRE.w_PRNUMPRE+iif(this.w_PADRE.w_PRNUMPRE=2,8,9)
      endif
      cp_NextTableProg(this.w_PADRE, i_Conn, "NUPRE", "i_codazi,w_PRNUMPRA,w_PRNUMPRE")
    endif
    if this.w_PRRIGPRE="S"
      this.w_LPRSERIAL = this.w_PADRE.w_PRSERIAL
    endif
    if this.w_PADRE.bCalcPrezzo or this.w_PADRE.bRicTar
      * --- Calcola il prezzo della prestazione
      if this.w_PADRE.bCalcPrezzo 
        this.w_PRPREMIN = 0
        this.w_PRPREMAX = 0
        this.w_PRPREZZO = 0
        this.w_PRVALRIG = 0
        this.w_PRGAZUFF = 0
        this.w_PRQTAUM1 = 0
      endif
      * --- Pratica
      this.w_COST_ORA = 0
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "CNCODCAN"
      ArrWhere(1,2) = this.w_PRNUMPRA
      if Not Empty(Nvl(this.w_PRNUMPRA," "))
        this.w_CN__ENTE = this.w_PADRE.ReadTable( "CAN_TIER" , "CN__ENTE" , @ArrWhere )
        this.w_CNFLAPON = this.w_PADRE.ReadTable( "CAN_TIER" , "CNFLAPON" , @ArrWhere )
        this.w_CNIMPORT = this.w_PADRE.ReadTable( "CAN_TIER" , "CNIMPORT" , @ArrWhere )
        this.w_CNUFFICI = this.w_PADRE.ReadTable( "CAN_TIER" , "CNUFFICI" , @ArrWhere )
        this.w_CNFLVALO = this.w_PADRE.ReadTable( "CAN_TIER" , "CNFLVALO" , @ArrWhere )
        this.w_CNFLVALO1 = this.w_PADRE.ReadTable( "CAN_TIER" , "CNFLDIND" , @ArrWhere )
        this.w_CNCALDIR = this.w_PADRE.ReadTable( "CAN_TIER" , "CNCALDIR" , @ArrWhere )
        this.w_CNCOECAL = this.w_PADRE.ReadTable( "CAN_TIER" , "CNCOECAL" , @ArrWhere )
        this.w_CNPARASS = this.w_PADRE.ReadTable( "CAN_TIER" , "CNPARASS" , @ArrWhere )
        this.w_CNFLAMPA = this.w_PADRE.ReadTable( "CAN_TIER" , "CNFLAMPA" , @ArrWhere )
        this.w_CNTARTEM = this.w_PADRE.ReadTable( "CAN_TIER" , "CNTARTEM" , @ArrWhere )
        this.w_CNTARCON = this.w_PADRE.ReadTable( "CAN_TIER" , "CNTARCON" , @ArrWhere )
        this.w_CONTRACN = this.w_PADRE.ReadTable( "CAN_TIER" , "CNCONTRA" , @ArrWhere )
        this.w_DETIPPRA = this.w_PADRE.ReadTable( "CAN_TIER" , "CNTIPPRA" , @ArrWhere )
        this.w_CNMATOBB = this.w_PADRE.ReadTable( "CAN_TIER" , "CNMATOBB" , @ArrWhere )
        this.w_CNASSCTP = this.w_PADRE.ReadTable( "CAN_TIER" , "CNASSCTP" , @ArrWhere )
        this.w_CNCOMPLX = this.w_PADRE.ReadTable( "CAN_TIER" , "CNCOMPLX" , @ArrWhere )
        this.w_CNPROFMT = this.w_PADRE.ReadTable( "CAN_TIER" , "CNPROFMT" , @ArrWhere )
        this.w_CNESIPOS = this.w_PADRE.ReadTable( "CAN_TIER" , "CNESIPOS" , @ArrWhere )
        this.w_CNPERPLX = this.w_PADRE.ReadTable( "CAN_TIER" , "CNPERPLX" , @ArrWhere )
        this.w_CNPERPOS = this.w_PADRE.ReadTable( "CAN_TIER" , "CNPERPOS" , @ArrWhere )
        this.w_CNFLVMLQ = this.w_PADRE.ReadTable( "CAN_TIER" , "CNFLVMLQ" , @ArrWhere )
      endif
      Release ArrWhere
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "CPCODCON"
      ArrWhere(1,2) = this.w_CONTRACN
      this.w_TIPCONCO = this.w_PADRE.ReadTable( "PRA_CONT" , "CPTIPCON" , @ArrWhere )
      this.w_LISCOLCO = this.w_PADRE.ReadTable( "PRA_CONT" , "CPLISCOL" , @ArrWhere )
      this.w_STATUSCO = this.w_PADRE.ReadTable( "PRA_CONT" , "CPSTATUS" , @ArrWhere )
      Release ArrWhere
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "CACODICE"
      ArrWhere(1,2) = this.w_PRCODATT
      this.w_PRCODART = this.w_PADRE.ReadTable( "KEY_ARTI" , "CACODART" , @ArrWhere )
      this.w_UNMIS3 = this.w_PADRE.ReadTable( "KEY_ARTI" , "CAUNIMIS" , @ArrWhere )
      this.w_OPERA3 = this.w_PADRE.ReadTable( "KEY_ARTI" , "CAOPERAT" , @ArrWhere )
      this.w_MOLTI3 = this.w_PADRE.ReadTable( "KEY_ARTI" , "CAMOLTIP" , @ArrWhere )
      Release ArrWhere
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "ARCODART"
      ArrWhere(1,2) = this.w_PRCODART
      this.w_UNMIS1 = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUNMIS1" , @ArrWhere )
      this.w_UNMIS2 = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUNMIS2" , @ArrWhere )
      this.w_OPERAT = this.w_PADRE.ReadTable( "ART_ICOL" , "AROPERAT" , @ArrWhere )
      this.w_MOLTIP = this.w_PADRE.ReadTable( "ART_ICOL" , "ARMOLTIP" , @ArrWhere )
      this.w_FLUSEP = this.w_PADRE.ReadTable( "ART_ICOL" , "ARFLUSEP" , @ArrWhere )
      Release ArrWhere
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "UMCODICE"
      ArrWhere(1,2) = this.w_PRUNIMIS
      this.w_FLFRAZ1 = this.w_PADRE.ReadTable( "UNIMIS" , "UMFLFRAZ" , @ArrWhere )
      this.w_MODUM2 = this.w_PADRE.ReadTable( "UNIMIS" , "UMMODUM2" , @ArrWhere )
      Release ArrWhere
      this.w_PRCODVAL = g_PERVAL
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "VACODVAL"
      ArrWhere(1,2) = this.w_PRCODVAL
      this.w_DECUNI = this.w_PADRE.ReadTable( "VALUTE" , "VADECUNI" , @ArrWhere )
      Release ArrWhere
      * --- Se si proviene dal Timer, se sulla pratica � definita Tariffa concordata e se siamo in presenza di una tariffa a tempo
      if this.w_PADRE.bTarCon AND this.w_CNTARTEM="C" AND w_TIPO_PRE="P"
        * --- Dichiarazione array tariffe a tempo concordate
        DECLARE ARRTARCON (3,1)
        * --- Azzero l'array che verr� riempito dalla funzione Caltarcon()
        ARRTARCON(1)=0 
 ARRTARCON(2)= " " 
 ARRTARCON(3)=0
        this.w_CALCONC = CALTARCON(this.w_PRNUMPRA, this.w_PRCODRES, this.w_PRCODATT, @ARRTARCON)
        this.w_CNTARCON = ARRTARCON(1)
      endif
      if this.w_PRPREZZO=0 or this.w_PADRE.bCalcPrezzo 
        GSAG_BCD(this,"1")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if !this.w_PADRE.bRicTar
        this.w_PADRE.w_PRPREZZO = this.w_PRPREZZO
        this.w_PADRE.w_PRVALRIG = cp_round(this.w_PRQTAMOV*this.w_PRPREZZO,g_PERPVL)
        this.w_PADRE.w_PRQTAUM1 = this.w_PRQTAUM1
      else
        this.w_PRUNIMIS = this.w_UNMIS1
      endif
      this.w_PADRE.w_PRPREMIN = this.w_PRPREMIN
      this.w_PADRE.w_PRPREMAX = this.w_PRPREMAX
      this.w_PADRE.w_PRUNIMIS = this.w_PRUNIMIS
    endif
    if this.w_PADRE.bCalcCosto AND !EMPTY(this.w_PRCODRES)
      * --- Calcola il costo interno
      this.w_PRCOSINT = 0
      this.w_PRCOSUNI = 0
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "DPCODICE"
      ArrWhere(1,2) = this.w_PRCODRES
      this.w_PRCOSUNI = this.w_PADRE.ReadTable( "DIPENDEN", "DPCOSORA" , @ArrWhere )
      Release ArrWhere
      this.w_PRCOSINT = (this.w_PROREEFF+(this.w_PRMINEFF/60))*this.w_PRCOSUNI
      this.w_PADRE.w_PRCOSUNI = this.w_PRCOSUNI
      this.w_PADRE.w_PRCOSINT = this.w_PRCOSINT
    endif
    Local cMacro
    this.w_PROPINS = 0
    this.w_PROPATT = 1
    this.w_NUMPROP = ALEN(this.w_PADRE.PropList, 1)
    do while this.w_PROPATT<=this.w_NUMPROP
      if this.w_PADRE.PropList(this.w_PROPATT,3)="F"
        this.w_PROPINS = this.w_PROPINS + 1
        DIMENSION ArrInsert( this.w_PROPINS ,2)
        cMacro = UPPER( ALLTRIM( this.w_PADRE.PropList(this.w_PROPATT,1) ) )
        ArrInsert( this.w_PROPINS ,1) = STRTRAN(m.cMacro, "W_", "")
        cMacro = "this.w_PADRE."+m.cMacro
        ArrInsert( this.w_PROPINS ,2) = &cMacro
      endif
      this.w_PROPATT = this.w_PROPATT + 1
    enddo
    release cMacro
    * --- Aggiungo un elemento all'array per ospitare il cpcchk
    DIMENSION ArrInsert( ALEN(ArrInsert,1)+1 ,2)
    ArrInsert( ALEN(ArrInsert,1) , 1) = "cpccchk"
    ArrInsert( ALEN(ArrInsert,1) , 2) = cp_NewCCChk()
    * --- Try
    local bErr_00E1F788
    bErr_00E1F788=bTrsErr
    this.Try_00E1F788()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_RETVAL = ah_MsgFormat("Errore inserimento prestazione.%0%1", MESSAGE())
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_RETVAL
    endif
    bTrsErr=bTrsErr or bErr_00E1F788
    * --- End
    Release ArrInsert
  endproc
  proc Try_00E1F788()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_RETVAL = InsertTable("PRE_STAZ", @ArrInsert )
    if !EMPTY(this.w_RETVAL)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_Msgformat("Errore inserimento attivit�.%0%1", this.w_RETVAL)
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PRTIPDOC = this.w_PACAUDOC
    * --- Select from RIS_ESTR
    i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SRCODPRA,SRCODSES,SRPARAME  from "+i_cTable+" RIS_ESTR ";
          +" where SRCODPRA="+cp_ToStrODBC(this.w_PRNUMPRA)+" and SRPARAME>0";
          +" order by SRPARAME Desc";
           ,"_Curs_RIS_ESTR")
    else
      select SRCODPRA,SRCODSES,SRPARAME from (i_cTable);
       where SRCODPRA=this.w_PRNUMPRA and SRPARAME>0;
       order by SRPARAME Desc;
        into cursor _Curs_RIS_ESTR
    endif
    if used('_Curs_RIS_ESTR')
      select _Curs_RIS_ESTR
      locate for 1=1
      do while not(eof())
      this.w_PARAME = Nvl(_Curs_RIS_ESTR.SRPARAME,0)
      this.w_TOTPARAME = this.w_TOTPARAME + this.w_PARAME
      this.w_PRPREZZO = (NVL(this.w_TOTCALC, 0)*this.w_PARAME)/100
      this.w_PRVALRIG = cp_round(this.w_PRQTAMOV*this.w_PRPREZZO,g_PERPVL)
      this.w_PRCODNOM = Nvl(_Curs_RIS_ESTR.SRCODSES," ")
      * --- Nel caso di gestione pi� parti inibisco visiaulizzazione della prestazione nella nota spese
      this.w_PRRIFPRO = Space(10)
      this.w_PRRIFFAT = Space(10)
      this.w_PRRIFBOZ = Space(10)
      this.w_PRRIFFAA = Space(10)
      this.w_PRRIFPRA = Space(10)
      this.w_PRRIFNOT = "@@@@@@@@@@"
      if this.w_TOTPARAME>=100
        this.w_PADRE.w_PRPREZZO = this.w_TOTALE
      endif
      this.w_PADRE.SetObjectValues(This)     
      this.w_PADRE.w_PRSERIAL = Space(10)
      this.w_TOTALE = this.w_TOTALE-this.w_PRPREZZO
      this.w_ROWNUM = this.w_ROWNUM + 1
      if this.w_CALRIF
        this.w_PRSERAGG = ARRPART(INT(ASCAN(ARRPART,this.w_ROWNUM,1)/2),1)
      endif
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if !this.w_CALRIF
        DIMENSION ARRPART(this.w_ROWNUM,2) 
 ARRPART(this.w_ROWNUM,1)=this.w_PADRE.w_PRSERIAL 
 ARRPART(this.w_ROWNUM,2)=this.w_ROWNUM
      endif
      * --- Insert into DET_PART
      i_nConn=i_TableProp[this.DET_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DET_PART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_PART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DPSERIAL"+",CPROWNUM"+",DPSERPRE"+",CPROWORD"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PPRSERIAL),'DET_PART','DPSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'DET_PART','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PADRE.w_PRSERIAL),'DET_PART','DPSERPRE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM*10),'DET_PART','CPROWORD');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DPSERIAL',this.w_PPRSERIAL,'CPROWNUM',this.w_ROWNUM,'DPSERPRE',this.w_PADRE.w_PRSERIAL,'CPROWORD',this.w_ROWNUM*10)
        insert into (i_cTable) (DPSERIAL,CPROWNUM,DPSERPRE,CPROWORD &i_ccchkf. );
           values (;
             this.w_PPRSERIAL;
             ,this.w_ROWNUM;
             ,this.w_PADRE.w_PRSERIAL;
             ,this.w_ROWNUM*10;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_RIS_ESTR
        continue
      enddo
      use
    endif
    * --- Ripristino causale per altre prestazioni
    this.w_PRTIPDOC = this.w_TIPDOC
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_PADRE.bParass
      this.w_PRRIFPRO = "@@@@@@@@@@"
      this.w_PRRIFFAT = "@@@@@@@@@@"
      this.w_PRRIFBOZ = "@@@@@@@@@@"
      this.w_PRRIFFAA = "@@@@@@@@@@"
      this.w_PRRIFPRA = "@@@@@@@@@@"
      this.w_PRRIFNOT = Space(10)
    endif
    this.w_PRCODATT = this.w_PRCODSPE
    if Empty(this.w_PADRE.cDesspe)
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "ARCODART"
      ArrWhere(1,2) = this.w_PRCODATT
      this.w_PRDESPRE = this.w_PADRE.ReadTable( "ART_ICOL" , "ARDESART" , @ArrWhere )
      this.w_PRDESAGG = this.w_PADRE.ReadTable( "ART_ICOL" , "ARDESSUP" , @ArrWhere )
      this.w_PRUNIMIS = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUNMIS1" , @ArrWhere )
      Release ArrWhere
    else
      if Len(this.w_PADRE.cDesspe)>40
        * --- Se la descrizione passata supera i 40 caratteri metto il resto nella supplementare
        this.w_PRDESPRE = Substr(this.w_PADRE.cDesspe,1,40)
        this.w_PRDESAGG = Substr(this.w_PADRE.cDesspe,41)
      else
        this.w_PRDESPRE = this.w_PADRE.cDesspe
      endif
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "ARCODART"
      ArrWhere(1,2) = this.w_PRCODATT
      this.w_PRUNIMIS = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUNMIS1" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_PRPREZZO = this.w_PRIMPSPE
    this.w_PRPREMAX = this.w_PRIMPSPE
    this.w_PRPREMIN = this.w_PRIMPSPE
    this.w_PRQTAMOV = 1
    this.w_PRQTAUM1 = 1
    this.w_PRVALRIG = cp_round(this.w_PRQTAMOV*this.w_PRPREZZO,g_PERPVL)
    this.w_PRCOSINT = 0
    this.w_PRCOSUNI = 0
    this.w_PRSERAGG = this.w_SER_RETURN
    this.w_PROREEFF = 0
    this.w_PRMINEFF = 0
    this.w_PRRIGPRE = "N"
    this.w_PRCENCOS = Space(15)
    this.w_PRVOCCOS = Space(15)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_PPRSERIAL = this.w_PADRE.w_PRSERIAL
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_PADRE.bParass
      this.w_PRRIFPRO = "@@@@@@@@@@"
      this.w_PRRIFFAT = "@@@@@@@@@@"
      this.w_PRRIFBOZ = "@@@@@@@@@@"
      this.w_PRRIFFAA = "@@@@@@@@@@"
      this.w_PRRIFPRA = "@@@@@@@@@@"
      this.w_PRRIFNOT = Space(10)
    endif
    this.w_PRCODATT = this.w_PRCODANT
    if Empty(this.w_PADRE.cDesant)
      if Empty(Nvl(this.w_PRDESPRE," ")) or Not Empty(Nvl(this.w_PADRE.cCodant," "))
        * --- Rileggo descrizione da anagrafica se vuota o se anticipazione collegata da inserimento provvisorio
        *     (sulla stessa riga del padre)
        DIMENSION ArrWhere(1,2)
        ArrWhere(1,1) = "ARCODART"
        ArrWhere(1,2) = this.w_PRCODATT
        this.w_PRDESPRE = this.w_PADRE.ReadTable( "ART_ICOL" , "ARDESART" , @ArrWhere )
        this.w_PRDESAGG = this.w_PADRE.ReadTable( "ART_ICOL" , "ARDESSUP" , @ArrWhere )
        this.w_PRUNIMIS = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUNMIS1" , @ArrWhere )
        Release ArrWhere
      endif
    else
      if Len(this.w_PADRE.cDesant)>40
        * --- Se la descrizione passata supera i 40 caratteri metto il resto nella supplementare
        this.w_PRDESPRE = Substr(this.w_PADRE.cDesant,1,40)
        this.w_PRDESAGG = Substr(this.w_PADRE.cDesant,41)
      else
        this.w_PRDESPRE = this.w_PADRE.cDesant
      endif
      DIMENSION ArrWhere(1,2)
      ArrWhere(1,1) = "ARCODART"
      ArrWhere(1,2) = this.w_PRCODATT
      this.w_PRUNIMIS = this.w_PADRE.ReadTable( "ART_ICOL" , "ARUNMIS1" , @ArrWhere )
      Release ArrWhere
    endif
    this.w_PRPREZZO = this.w_PRIMPANT
    this.w_PRPREMAX = this.w_PRIMPANT
    this.w_PRPREMIN = this.w_PRIMPANT
    this.w_PRQTAMOV = 1
    this.w_PRQTAUM1 = 1
    this.w_PRVALRIG = cp_round(this.w_PRQTAMOV*this.w_PRPREZZO,g_PERPVL)
    this.w_PRCOSINT = 0
    this.w_PRCOSUNI = 0
    this.w_PROREEFF = 0
    this.w_PRMINEFF = 0
    this.w_PRSERAGG = this.w_SER_RETURN
    this.w_PRRIGPRE = "N"
    this.w_PRCENCOS = Space(15)
    this.w_PRVOCCOS = Space(15)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_PPRSERIAL = this.w_PADRE.w_PRSERIAL
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPADRE)
    this.pPADRE=pPADRE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='PRE_STAZ'
    this.cWorkTables[2]='RIS_ESTR'
    this.cWorkTables[3]='DET_PART'
    this.cWorkTables[4]='DIPENDEN'
    this.cWorkTables[5]='OFFDATTI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_OFFDATTI')
      use in _Curs_OFFDATTI
    endif
    if used('_Curs_RIS_ESTR')
      use in _Curs_RIS_ESTR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPADRE"
endproc
