* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ba7                                                        *
*              Aggiorna date di competenza documento e attivit�                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-10-21                                                      *
* Last revis.: 2011-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pELCONTRA,pELCODMOD,pELCODIMP,pELCODCOM,pELRINNOV,pMODALITA,pMOD_CALCOLO,pFORZA_CALCOLO_DATE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_ba7",oParentObject,m.pELCONTRA,m.pELCODMOD,m.pELCODIMP,m.pELCODCOM,m.pELRINNOV,m.pMODALITA,m.pMOD_CALCOLO,m.pFORZA_CALCOLO_DATE)
return(i_retval)

define class tgsag_ba7 as StdBatch
  * --- Local variables
  pELCONTRA = space(10)
  pELCODMOD = space(10)
  pELCODIMP = space(10)
  pELCODCOM = 0
  pELRINNOV = 0
  pMODALITA = space(1)
  pMOD_CALCOLO = space(1)
  pFORZA_CALCOLO_DATE = space(1)
  w_ELDATATT = ctod("  /  /  ")
  w_ELDATFAT = ctod("  /  /  ")
  w_ELDATINI = ctod("  /  /  ")
  w_ELINICOA = ctod("  /  /  ")
  w_ELFINCOA = ctod("  /  /  ")
  w_ELINICOD = ctod("  /  /  ")
  w_ELFINCOD = ctod("  /  /  ")
  w_ELTIPATT = space(20)
  w_ELCAUDOC = space(5)
  w_ELPERATT = space(3)
  w_ELPERFAT = space(3)
  w_ELDATDOC = ctod("  /  /  ")
  w_CAFLNSAP = space(1)
  w_ELPERATT_PREC = space(3)
  w_ELDATGAT_PREC = ctod("  /  /  ")
  w_ELDATATT_PREC = ctod("  /  /  ")
  w_ELPERFAT_PREC = space(3)
  w_ELDATDOC_PREC = ctod("  /  /  ")
  w_ELDATFAT_PREC = ctod("  /  /  ")
  * --- WorkFile variables
  ELE_CONT_idx=0
  DET_GEN_idx=0
  DOC_GENE_idx=0
  CAUMATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Allineamento delle date di competanza documento e attivit� dell'elemento contratto
    * --- pMODALITA
    *     'A' attivit�
    *     'D' documenti
    * --- pMOD_CALCOLO
    *     'P' posticipata
    *     'A' anticipata
    * --- pFORZA_CALCOLO_DATE
    *     'S' sovrascrive le date di competenza anche se esisitenti
    * --- Legge i dati dall'elemento da aggiornare, in modo da mantenere il valore corrente se non specificato nel contratto o nel modello
    * --- Date competenza attivit�
    * --- Date competenza documento
    * --- Tipo attivit�
    * --- Causale documento
    * --- Periodicit� attivit�
    * --- Periodicit� documento
    * --- --
    * --- Read from ELE_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2],.t.,this.ELE_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ELDATATT,ELDATFAT,ELDATINI,ELINICOA,ELFINCOA,ELINICOD,ELFINCOD,ELTIPATT,ELCAUDOC,ELPERATT,ELPERFAT,ELDATGAT,ELDATDOC"+;
        " from "+i_cTable+" ELE_CONT where ";
            +"ELCONTRA = "+cp_ToStrODBC(this.pELCONTRA);
            +" and ELCODMOD = "+cp_ToStrODBC(this.pELCODMOD);
            +" and ELCODIMP = "+cp_ToStrODBC(this.pELCODIMP);
            +" and ELCODCOM = "+cp_ToStrODBC(this.pELCODCOM);
            +" and ELRINNOV = "+cp_ToStrODBC(this.pELRINNOV);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ELDATATT,ELDATFAT,ELDATINI,ELINICOA,ELFINCOA,ELINICOD,ELFINCOD,ELTIPATT,ELCAUDOC,ELPERATT,ELPERFAT,ELDATGAT,ELDATDOC;
        from (i_cTable) where;
            ELCONTRA = this.pELCONTRA;
            and ELCODMOD = this.pELCODMOD;
            and ELCODIMP = this.pELCODIMP;
            and ELCODCOM = this.pELCODCOM;
            and ELRINNOV = this.pELRINNOV;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ELDATATT = NVL(cp_ToDate(_read_.ELDATATT),cp_NullValue(_read_.ELDATATT))
      this.w_ELDATFAT = NVL(cp_ToDate(_read_.ELDATFAT),cp_NullValue(_read_.ELDATFAT))
      this.w_ELDATINI = NVL(cp_ToDate(_read_.ELDATINI),cp_NullValue(_read_.ELDATINI))
      this.w_ELINICOA = NVL(cp_ToDate(_read_.ELINICOA),cp_NullValue(_read_.ELINICOA))
      this.w_ELFINCOA = NVL(cp_ToDate(_read_.ELFINCOA),cp_NullValue(_read_.ELFINCOA))
      this.w_ELINICOD = NVL(cp_ToDate(_read_.ELINICOD),cp_NullValue(_read_.ELINICOD))
      this.w_ELFINCOD = NVL(cp_ToDate(_read_.ELFINCOD),cp_NullValue(_read_.ELFINCOD))
      this.w_ELTIPATT = NVL(cp_ToDate(_read_.ELTIPATT),cp_NullValue(_read_.ELTIPATT))
      this.w_ELCAUDOC = NVL(cp_ToDate(_read_.ELCAUDOC),cp_NullValue(_read_.ELCAUDOC))
      this.w_ELPERATT = NVL(cp_ToDate(_read_.ELPERATT),cp_NullValue(_read_.ELPERATT))
      this.w_ELPERFAT = NVL(cp_ToDate(_read_.ELPERFAT),cp_NullValue(_read_.ELPERFAT))
      w_ELDATGAT = NVL(cp_ToDate(_read_.ELDATGAT),cp_NullValue(_read_.ELDATGAT))
      this.w_ELDATDOC = NVL(cp_ToDate(_read_.ELDATDOC),cp_NullValue(_read_.ELDATDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ELINICOA = NVL( this.w_ELINICOA, CP_CHARTODATE("  -  -  ") )
    this.w_ELFINCOA = NVL( this.w_ELINICOA, CP_CHARTODATE("  -  -  ") )
    this.w_ELINICOD = NVL( this.w_ELINICOD, CP_CHARTODATE("  -  -  ") )
    this.w_ELFINCOD = NVL( this.w_ELFINCOD, CP_CHARTODATE("  -  -  ") )
    this.w_ELTIPATT = NVL( this.w_ELTIPATT, SPACE( 20 ) )
    * --- Legge il flag
    if NOT EMPTY( this.w_ELTIPATT )
      * --- Read from CAUMATTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAUMATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CAFLNSAP"+;
          " from "+i_cTable+" CAUMATTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_ELTIPATT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CAFLNSAP;
          from (i_cTable) where;
              CACODICE = this.w_ELTIPATT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAFLNSAP = NVL(cp_ToDate(_read_.CAFLNSAP),cp_NullValue(_read_.CAFLNSAP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Bisogna aggiornare le date di competenza attivit� solo se il tipo attivit� � soggetto a prestazioni
      this.w_CAFLNSAP = NVL( this.w_CAFLNSAP, " " )
    endif
    this.w_ELCAUDOC = NVL( this.w_ELCAUDOC, SPACE( 5 ) )
    do case
      case this.pMODALITA = "A"
        * --- Bisogna aggiornare le date di competenza attivit� solo se il tipo attivit� � soggetto a prestazioni
        if this.w_CAFLNSAP <> "S" AND ( EMPTY( this.w_ELINICOA ) OR EMPTY( this.w_ELFINCOA ) OR this.pFORZA_CALCOLO_DATE="S" )
          if EMPTY( this.w_ELINICOA ) OR this.pFORZA_CALCOLO_DATE="S"
            do case
              case this.pMOD_CALCOLO = "P"
                * --- Registrazione posticipata
                * --- Valorizza la data di inizio competenza attivit� con la data dell'ultima attivit� generata (+1)
                this.w_ELINICOA = CP_CHARTODATE( "  -  -  " )
                * --- Select from DET_GEN
                i_nConn=i_TableProp[this.DET_GEN_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DET_GEN_idx,2],.t.,this.DET_GEN_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select MAX( MDOLDDAT ) AS MDOLDDAT  from "+i_cTable+" DET_GEN ";
                      +" where MDCODMOD = "+cp_ToStrODBC(this.pELCODMOD)+" and MDCONTRA = "+cp_ToStrODBC(this.pELCONTRA)+" and MDCODIMP = "+cp_ToStrODBC(this.pELCODIMP)+" and MDCOMPON = "+cp_ToStrODBC(this.pELCODCOM)+" and MDRINNOV = "+cp_ToStrODBC(this.pELRINNOV)+"";
                       ,"_Curs_DET_GEN")
                else
                  select MAX( MDOLDDAT ) AS MDOLDDAT from (i_cTable);
                   where MDCODMOD = this.pELCODMOD and MDCONTRA = this.pELCONTRA and MDCODIMP = this.pELCODIMP and MDCOMPON = this.pELCODCOM and MDRINNOV = this.pELRINNOV;
                    into cursor _Curs_DET_GEN
                endif
                if used('_Curs_DET_GEN')
                  select _Curs_DET_GEN
                  locate for 1=1
                  do while not(eof())
                  this.w_ELINICOA = CP_TODATE( _Curs_DET_GEN.MDOLDDAT ) +1
                    select _Curs_DET_GEN
                    continue
                  enddo
                  use
                endif
                if EMPTY( NVL( this.w_ELINICOA, CP_CHARTODATE("  -  -  ") ) ) AND this.pELRINNOV > 0
                  * --- Read from ELE_CONT
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ELE_CONT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2],.t.,this.ELE_CONT_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ELPERATT,ELDATGAT,ELDATATT"+;
                      " from "+i_cTable+" ELE_CONT where ";
                          +"ELCONTRA = "+cp_ToStrODBC(this.pELCONTRA);
                          +" and ELCODMOD = "+cp_ToStrODBC(this.pELCODMOD);
                          +" and ELCODIMP = "+cp_ToStrODBC(this.pELCODIMP);
                          +" and ELCODCOM = "+cp_ToStrODBC(this.pELCODCOM);
                          +" and ELRINNOV = "+cp_ToStrODBC(this.pELRINNOV - 1);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ELPERATT,ELDATGAT,ELDATATT;
                      from (i_cTable) where;
                          ELCONTRA = this.pELCONTRA;
                          and ELCODMOD = this.pELCODMOD;
                          and ELCODIMP = this.pELCODIMP;
                          and ELCODCOM = this.pELCODCOM;
                          and ELRINNOV = this.pELRINNOV - 1;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_ELPERATT_PREC = NVL(cp_ToDate(_read_.ELPERATT),cp_NullValue(_read_.ELPERATT))
                    this.w_ELDATGAT_PREC = NVL(cp_ToDate(_read_.ELDATGAT),cp_NullValue(_read_.ELDATGAT))
                    this.w_ELDATATT_PREC = NVL(cp_ToDate(_read_.ELDATATT),cp_NullValue(_read_.ELDATATT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if EMPTY( NVL( this.w_ELDATATT_PREC, CP_CHARTODATE("  -  -  ") ) )
                    * --- Select from DET_GEN
                    i_nConn=i_TableProp[this.DET_GEN_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.DET_GEN_idx,2],.t.,this.DET_GEN_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select MAX( MDOLDDAT ) AS MDOLDDAT  from "+i_cTable+" DET_GEN ";
                          +" where MDCODMOD = "+cp_ToStrODBC(this.pELCODMOD)+" and MDCONTRA = "+cp_ToStrODBC(this.pELCONTRA)+" and MDCODIMP = "+cp_ToStrODBC(this.pELCODIMP)+" and MDCOMPON = "+cp_ToStrODBC(this.pELCODCOM)+" and MDRINNOV = "+cp_ToStrODBC(this.pELRINNOV)+"-1";
                           ,"_Curs_DET_GEN")
                    else
                      select MAX( MDOLDDAT ) AS MDOLDDAT from (i_cTable);
                       where MDCODMOD = this.pELCODMOD and MDCONTRA = this.pELCONTRA and MDCODIMP = this.pELCODIMP and MDCOMPON = this.pELCODCOM and MDRINNOV = this.pELRINNOV-1;
                        into cursor _Curs_DET_GEN
                    endif
                    if used('_Curs_DET_GEN')
                      select _Curs_DET_GEN
                      locate for 1=1
                      do while not(eof())
                      this.w_ELINICOA = CP_TODATE( _Curs_DET_GEN.MDOLDDAT ) +1
                        select _Curs_DET_GEN
                        continue
                      enddo
                      use
                    endif
                  else
                    * --- Altrimenti bisogna calcolare le date successive a w_ELDATATT_PREC utilizzando la periodicit� dell'elemento precedente w_ELPERATT_PREC
                    *     finch� non si supera la data w_ELDATGAT_PREC dell'el. contratto precedente (w_ELDATGAT_PREC),
                    *     tenendo buona l'ultima data non superiore a w_ELDATGAT_PREC
                    this.w_ELINICOA = this.w_ELDATATT_PREC
                    do while NEXTTIME( this.w_ELPERATT_PREC, this.w_ELINICOA ) <= this.w_ELDATGAT_PREC
                      this.w_ELINICOA = NEXTTIME( this.w_ELPERATT_PREC, this.w_ELINICOA )
                    enddo
                    this.w_ELINICOA = this.w_ELINICOA + 1
                  endif
                endif
                * --- Se ancora non ha trovato niente utilizza la data di inzio validit� dell'elemento contratto
                if EMPTY( NVL( this.w_ELINICOA, CP_CHARTODATE("  -  -  ") ) )
                  this.w_ELINICOA = this.w_ELDATINI
                endif
              case this.pMOD_CALCOLO = "A"
                * --- Registrazione anticipata
                * --- La data di inizio competenza attivit� � la data della prossima attivit�
                this.w_ELINICOA = this.w_ELDATATT
            endcase
          endif
          if EMPTY( this.w_ELFINCOA ) OR this.pFORZA_CALCOLO_DATE="S"
            do case
              case this.pMOD_CALCOLO = "P"
                * --- Registrazione posticipata
                * --- La data fine competenza attivit� � la data della prossima attivit�
                this.w_ELFINCOA = this.w_ELDATATT
              case this.pMOD_CALCOLO = "A"
                * --- Registrazione anticipata
                * --- La data di fine competenza dell'attivit� si calcola applicando la periodicit� alla data inizio competenza meno un giorno
                this.w_ELFINCOA = NEXTTIME( this.w_ELPERATT, this.w_ELINICOA - 1 )
            endcase
          endif
          * --- Aggiorna le date sull'elemento contratto
          * --- Write into ELE_CONT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ELE_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ELINICOA ="+cp_NullLink(cp_ToStrODBC(this.w_ELINICOA),'ELE_CONT','ELINICOA');
            +",ELFINCOA ="+cp_NullLink(cp_ToStrODBC(this.w_ELFINCOA),'ELE_CONT','ELFINCOA');
                +i_ccchkf ;
            +" where ";
                +"ELCONTRA = "+cp_ToStrODBC(this.pELCONTRA);
                +" and ELCODMOD = "+cp_ToStrODBC(this.pELCODMOD);
                +" and ELCODIMP = "+cp_ToStrODBC(this.pELCODIMP);
                +" and ELCODCOM = "+cp_ToStrODBC(this.pELCODCOM);
                +" and ELRINNOV = "+cp_ToStrODBC(this.pELRINNOV);
                   )
          else
            update (i_cTable) set;
                ELINICOA = this.w_ELINICOA;
                ,ELFINCOA = this.w_ELFINCOA;
                &i_ccchkf. ;
             where;
                ELCONTRA = this.pELCONTRA;
                and ELCODMOD = this.pELCODMOD;
                and ELCODIMP = this.pELCODIMP;
                and ELCODCOM = this.pELCODCOM;
                and ELRINNOV = this.pELRINNOV;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pMODALITA = "D"
        * --- Bisogna aggiornare le date di competenza documento solo se � presente la causale del documento
        if NOT EMPTY( this.w_ELCAUDOC ) AND ( EMPTY( this.w_ELINICOD ) OR EMPTY( this.w_ELFINCOD ) OR this.pFORZA_CALCOLO_DATE="S" )
          if EMPTY( this.w_ELINICOD ) OR this.pFORZA_CALCOLO_DATE="S"
            do case
              case this.pMOD_CALCOLO = "P"
                * --- Registrazione posticipata
                * --- Valorizza la data di inizio competenza documento con la data dell'ultimo documento generato (+1)
                this.w_ELINICOD = CP_CHARTODATE( "  -  -  " )
                * --- Select from DOC_GENE
                i_nConn=i_TableProp[this.DOC_GENE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_GENE_idx,2],.t.,this.DOC_GENE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select MAX( DDOLDDAT ) AS DDOLDDAT  from "+i_cTable+" DOC_GENE ";
                      +" where DDCODMOD = "+cp_ToStrODBC(this.pELCODMOD)+" and DDCONTRA = "+cp_ToStrODBC(this.pELCONTRA)+" and DDCODIMP = "+cp_ToStrODBC(this.pELCODIMP)+" and DDCOMPON = "+cp_ToStrODBC(this.pELCODCOM)+" and DDRINNOV = "+cp_ToStrODBC(this.pELRINNOV)+"";
                       ,"_Curs_DOC_GENE")
                else
                  select MAX( DDOLDDAT ) AS DDOLDDAT from (i_cTable);
                   where DDCODMOD = this.pELCODMOD and DDCONTRA = this.pELCONTRA and DDCODIMP = this.pELCODIMP and DDCOMPON = this.pELCODCOM and DDRINNOV = this.pELRINNOV;
                    into cursor _Curs_DOC_GENE
                endif
                if used('_Curs_DOC_GENE')
                  select _Curs_DOC_GENE
                  locate for 1=1
                  do while not(eof())
                  this.w_ELINICOD = CP_TODATE( _Curs_DOC_GENE.DDOLDDAT ) + 1
                    select _Curs_DOC_GENE
                    continue
                  enddo
                  use
                endif
                if EMPTY( NVL( this.w_ELINICOD, CP_CHARTODATE("  -  -  ") ) ) AND this.pELRINNOV > 0
                  * --- Read from ELE_CONT
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ELE_CONT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2],.t.,this.ELE_CONT_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ELPERFAT,ELDATDOC,ELDATFAT"+;
                      " from "+i_cTable+" ELE_CONT where ";
                          +"ELCONTRA = "+cp_ToStrODBC(this.pELCONTRA);
                          +" and ELCODMOD = "+cp_ToStrODBC(this.pELCODMOD);
                          +" and ELCODIMP = "+cp_ToStrODBC(this.pELCODIMP);
                          +" and ELCODCOM = "+cp_ToStrODBC(this.pELCODCOM);
                          +" and ELRINNOV = "+cp_ToStrODBC(this.pELRINNOV - 1);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ELPERFAT,ELDATDOC,ELDATFAT;
                      from (i_cTable) where;
                          ELCONTRA = this.pELCONTRA;
                          and ELCODMOD = this.pELCODMOD;
                          and ELCODIMP = this.pELCODIMP;
                          and ELCODCOM = this.pELCODCOM;
                          and ELRINNOV = this.pELRINNOV - 1;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_ELPERFAT_PREC = NVL(cp_ToDate(_read_.ELPERFAT),cp_NullValue(_read_.ELPERFAT))
                    this.w_ELDATDOC_PREC = NVL(cp_ToDate(_read_.ELDATDOC),cp_NullValue(_read_.ELDATDOC))
                    this.w_ELDATFAT_PREC = NVL(cp_ToDate(_read_.ELDATFAT),cp_NullValue(_read_.ELDATFAT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if EMPTY( NVL( this.w_ELDATFAT_PREC, CP_CHARTODATE("  -  -  ") ) )
                    * --- Se la data di prossimo documento w_ELDATFAT_PREC � vuota, la data da utilizzare � w_ELINICOD letta da DOC_GENE
                    * --- Select from DOC_GENE
                    i_nConn=i_TableProp[this.DOC_GENE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.DOC_GENE_idx,2],.t.,this.DOC_GENE_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select MAX( DDOLDDAT ) AS DDOLDDAT  from "+i_cTable+" DOC_GENE ";
                          +" where DDCODMOD = "+cp_ToStrODBC(this.pELCODMOD)+" and DDCONTRA = "+cp_ToStrODBC(this.pELCONTRA)+" and DDCODIMP = "+cp_ToStrODBC(this.pELCODIMP)+" and DDCOMPON = "+cp_ToStrODBC(this.pELCODCOM)+" and DDRINNOV = "+cp_ToStrODBC(this.pELRINNOV)+"-1";
                           ,"_Curs_DOC_GENE")
                    else
                      select MAX( DDOLDDAT ) AS DDOLDDAT from (i_cTable);
                       where DDCODMOD = this.pELCODMOD and DDCONTRA = this.pELCONTRA and DDCODIMP = this.pELCODIMP and DDCOMPON = this.pELCODCOM and DDRINNOV = this.pELRINNOV-1;
                        into cursor _Curs_DOC_GENE
                    endif
                    if used('_Curs_DOC_GENE')
                      select _Curs_DOC_GENE
                      locate for 1=1
                      do while not(eof())
                      this.w_ELINICOD = CP_TODATE( _Curs_DOC_GENE.DDOLDDAT ) + 1
                        select _Curs_DOC_GENE
                        continue
                      enddo
                      use
                    endif
                  else
                    * --- Altrimenti bisogna calcolare le date successive a w_ELDATFAT_PREC utilizzando la periodicit� dell'elemento precedente w_ELPERFAT_PREC
                    *     finch� non si supera la data w_ELDATDOC dell'elemento precedente (w_ELDATDOC_PREC),
                    *     tenendo buona l'ultima data non superiore a w_ELDATDOC_PREC
                    this.w_ELINICOD = this.w_ELDATFAT_PREC
                    do while NEXTTIME( this.w_ELPERFAT_PREC, this.w_ELINICOD ) <= this.w_ELDATDOC_PREC
                      this.w_ELINICOD = NEXTTIME( this.w_ELPERFAT_PREC, this.w_ELINICOD )
                    enddo
                    this.w_ELINICOD = this.w_ELINICOD + 1
                  endif
                endif
                * --- Se ancora non ha trovato niente utilizza la data di inzio validit� dell'elemento contratto
                if EMPTY( NVL( this.w_ELINICOD, CP_CHARTODATE("  -  -  ") ) )
                  this.w_ELINICOD = this.w_ELDATINI
                endif
              case this.pMOD_CALCOLO = "A"
                * --- Registrazione anticipata
                * --- La data inizio competenza documento � la data del prossimo documento
                this.w_ELINICOD = this.w_ELDATFAT
            endcase
          endif
          if EMPTY( this.w_ELFINCOD ) OR this.pFORZA_CALCOLO_DATE="S"
            do case
              case this.pMOD_CALCOLO = "P"
                * --- Registrazione posticipata
                * --- La data fine competenza documento � la data del prossimo documento
                this.w_ELFINCOD = this.w_ELDATFAT
              case this.pMOD_CALCOLO = "A"
                * --- Registrazione anticipata
                * --- La data di fine competenza del documento si calcola applicando la periodicit� alla data inizio competenza meno un giorno
                this.w_ELFINCOD = NEXTTIME( this.w_ELPERFAT, this.w_ELINICOD - 1 )
            endcase
          endif
          * --- Aggiorna le date sull'elemento contratto
          * --- Write into ELE_CONT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ELE_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ELINICOD ="+cp_NullLink(cp_ToStrODBC(this.w_ELINICOD),'ELE_CONT','ELINICOD');
            +",ELFINCOD ="+cp_NullLink(cp_ToStrODBC(this.w_ELFINCOD),'ELE_CONT','ELFINCOD');
                +i_ccchkf ;
            +" where ";
                +"ELCONTRA = "+cp_ToStrODBC(this.pELCONTRA);
                +" and ELCODMOD = "+cp_ToStrODBC(this.pELCODMOD);
                +" and ELCODIMP = "+cp_ToStrODBC(this.pELCODIMP);
                +" and ELCODCOM = "+cp_ToStrODBC(this.pELCODCOM);
                +" and ELRINNOV = "+cp_ToStrODBC(this.pELRINNOV);
                   )
          else
            update (i_cTable) set;
                ELINICOD = this.w_ELINICOD;
                ,ELFINCOD = this.w_ELFINCOD;
                &i_ccchkf. ;
             where;
                ELCONTRA = this.pELCONTRA;
                and ELCODMOD = this.pELCODMOD;
                and ELCODIMP = this.pELCODIMP;
                and ELCODCOM = this.pELCODCOM;
                and ELRINNOV = this.pELRINNOV;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pELCONTRA,pELCODMOD,pELCODIMP,pELCODCOM,pELRINNOV,pMODALITA,pMOD_CALCOLO,pFORZA_CALCOLO_DATE)
    this.pELCONTRA=pELCONTRA
    this.pELCODMOD=pELCODMOD
    this.pELCODIMP=pELCODIMP
    this.pELCODCOM=pELCODCOM
    this.pELRINNOV=pELRINNOV
    this.pMODALITA=pMODALITA
    this.pMOD_CALCOLO=pMOD_CALCOLO
    this.pFORZA_CALCOLO_DATE=pFORZA_CALCOLO_DATE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ELE_CONT'
    this.cWorkTables[2]='DET_GEN'
    this.cWorkTables[3]='DOC_GENE'
    this.cWorkTables[4]='CAUMATTI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_DET_GEN')
      use in _Curs_DET_GEN
    endif
    if used('_Curs_DET_GEN')
      use in _Curs_DET_GEN
    endif
    if used('_Curs_DOC_GENE')
      use in _Curs_DOC_GENE
    endif
    if used('_Curs_DOC_GENE')
      use in _Curs_DOC_GENE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pELCONTRA,pELCODMOD,pELCODIMP,pELCODCOM,pELRINNOV,pMODALITA,pMOD_CALCOLO,pFORZA_CALCOLO_DATE"
endproc
