* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_krc                                                        *
*              Ricerca contatti                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-22                                                      *
* Last revis.: 2013-09-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsfa_krc",oParentObject))

* --- Class definition
define class tgsfa_krc as StdForm
  Top    = 29
  Left   = 77

  * --- Standard Properties
  Width  = 709
  Height = 487+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-09-04"
  HelpContextID=116032105
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=60

  * --- Constant Properties
  _IDX = 0
  ANEVENTI_IDX = 0
  OFF_NOMI_IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  PAR_PRAT_IDX = 0
  TIPEVENT_IDX = 0
  cPrg = "gsfa_krc"
  cComment = "Ricerca contatti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_OREFIN = space(2)
  o_OREFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_DATA_INI = ctot('')
  w_DATA_FIN = ctot('')
  w_EV__ANNO = space(4)
  w_EVSERIAL = space(10)
  w_TIPPER = space(1)
  w_TIPDIR1 = space(1)
  w_TIPGRU = space(1)
  w_TIPRIC1 = space(1)
  w_TipoDrv = space(1)
  o_TipoDrv = space(1)
  w_FlgStato = space(1)
  o_FlgStato = space(1)
  w_TIPDIR = space(1)
  o_TIPDIR = space(1)
  w_Riferimento = space(250)
  w_PERSON = space(5)
  o_PERSON = space(5)
  w_GRUPPO = space(5)
  w_TipoDrv1 = space(1)
  w_CODNOM = space(15)
  w_EDITCODPRA = .F.
  w_CODPRA = space(15)
  w_NOTE = space(0)
  w_TipEve = space(10)
  w_Oggetto = space(100)
  w_RIC_MAIL = space(100)
  w_DESPRA = space(75)
  w_COMODO = space(30)
  w_DESNOM = space(40)
  w_RIC_TLF = space(18)
  w_RIC_CEL = space(18)
  w_RIC_INT = space(18)
  w_TIPRIC = space(1)
  o_TIPRIC = space(1)
  w_EVNOMINA = space(15)
  w_EV_PHONE = space(18)
  w_EV_EMAIL = space(254)
  w_EVNUMCEL = space(20)
  w_EV__NOTE = space(0)
  w_CNCODCAN = space(15)
  w_COD_AZI = space(5)
  o_COD_AZI = space(5)
  w_VISELA = space(1)
  w_CNDESCAN = space(100)
  w_NODESCRI = space(60)
  w_TEDESCRI = space(50)
  w_TEDRIVER = space(10)
  w_DPTIPRIS = space(1)
  w_DENOM_PART = space(48)
  w_COGNPART = space(40)
  w_NOMEPART = space(40)
  w_DESCRPART = space(40)
  w_PATIPRIS = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_DPTIPRIS = space(1)
  w_TIPGRUPPO = space(1)
  w_DESGRUPPO = space(60)
  w_OFDATDOC = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_FilStato = space(1)
  w_FAKRC_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsfa_krcPag1","gsfa_krc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Ricerca contatti")
      .Pages(2).addobject("oPag","tgsfa_krcPag2","gsfa_krc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettagli")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_FAKRC_ZOOM = this.oPgFrm.Pages(1).oPag.FAKRC_ZOOM
    DoDefault()
    proc Destroy()
      this.w_FAKRC_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='ANEVENTI'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='DIPENDEN'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='PAR_PRAT'
    this.cWorkTables[6]='TIPEVENT'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATINI=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_DATFIN=ctod("  /  /  ")
      .w_OREFIN=space(2)
      .w_MINFIN=space(2)
      .w_DATA_INI=ctot("")
      .w_DATA_FIN=ctot("")
      .w_EV__ANNO=space(4)
      .w_EVSERIAL=space(10)
      .w_TIPPER=space(1)
      .w_TIPDIR1=space(1)
      .w_TIPGRU=space(1)
      .w_TIPRIC1=space(1)
      .w_TipoDrv=space(1)
      .w_FlgStato=space(1)
      .w_TIPDIR=space(1)
      .w_Riferimento=space(250)
      .w_PERSON=space(5)
      .w_GRUPPO=space(5)
      .w_TipoDrv1=space(1)
      .w_CODNOM=space(15)
      .w_EDITCODPRA=.f.
      .w_CODPRA=space(15)
      .w_NOTE=space(0)
      .w_TipEve=space(10)
      .w_Oggetto=space(100)
      .w_RIC_MAIL=space(100)
      .w_DESPRA=space(75)
      .w_COMODO=space(30)
      .w_DESNOM=space(40)
      .w_RIC_TLF=space(18)
      .w_RIC_CEL=space(18)
      .w_RIC_INT=space(18)
      .w_TIPRIC=space(1)
      .w_EVNOMINA=space(15)
      .w_EV_PHONE=space(18)
      .w_EV_EMAIL=space(254)
      .w_EVNUMCEL=space(20)
      .w_EV__NOTE=space(0)
      .w_CNCODCAN=space(15)
      .w_COD_AZI=space(5)
      .w_VISELA=space(1)
      .w_CNDESCAN=space(100)
      .w_NODESCRI=space(60)
      .w_TEDESCRI=space(50)
      .w_TEDRIVER=space(10)
      .w_DPTIPRIS=space(1)
      .w_DENOM_PART=space(48)
      .w_COGNPART=space(40)
      .w_NOMEPART=space(40)
      .w_DESCRPART=space(40)
      .w_PATIPRIS=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_DPTIPRIS=space(1)
      .w_TIPGRUPPO=space(1)
      .w_DESGRUPPO=space(60)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_FilStato=space(1)
        .w_DATINI = i_DatSys
        .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        .w_DATFIN = i_DatSys
        .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (empty(.w_DATFIN) AND .w_OREFIN="23" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
      .oPgFrm.Page1.oPag.FAKRC_ZOOM.Calculate()
        .w_EV__ANNO = .w_FAKRC_ZOOM.GetVar('EV__ANNO')
        .w_EVSERIAL = .w_FAKRC_ZOOM.GetVar('EVSERIAL')
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_EVSERIAL))
          .link_1_12('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .w_TIPPER = 'P'
        .w_TIPDIR1 = IIF(.w_TIPDIR='T','',.w_TIPDIR)
        .w_TIPGRU = 'G'
        .w_TIPRIC1 = IIF(.w_TIPRIC='T','',.w_TIPRIC)
        .w_TipoDrv = 'X'
        .w_FlgStato = 'X'
        .w_TIPDIR = 'T'
        .DoRTCalc(18,19,.f.)
        if not(empty(.w_PERSON))
          .link_2_13('Full')
        endif
        .w_GRUPPO = Space(5)
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_GRUPPO))
          .link_2_14('Full')
        endif
        .w_TipoDrv1 = IIF(.w_TipoDrv='X','',.w_TipoDrv)
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CODNOM))
          .link_2_16('Full')
        endif
        .w_EDITCODPRA = .T.
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_CODPRA))
          .link_2_18('Full')
        endif
        .DoRTCalc(25,26,.f.)
        if not(empty(.w_TipEve))
          .link_2_21('Full')
        endif
          .DoRTCalc(27,34,.f.)
        .w_TIPRIC = 'T'
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_EVNOMINA))
          .link_1_15('Full')
        endif
        .DoRTCalc(37,41,.f.)
        if not(empty(.w_CNCODCAN))
          .link_1_20('Full')
        endif
        .w_COD_AZI = i_codazi
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_COD_AZI))
          .link_1_21('Full')
        endif
          .DoRTCalc(43,48,.f.)
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
          .DoRTCalc(50,52,.f.)
        .w_PATIPRIS = 'P'
        .w_OB_TEST = i_INIDAT
          .DoRTCalc(55,57,.f.)
        .w_OFDATDOC = i_datsys
        .w_OBTEST = i_INIDAT
        .w_FilStato = IIF(.w_FlgStato='X',' ',.w_FlgStato)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_57.enabled = this.oPgFrm.Page2.oPag.oBtn_2_57.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_OREINI<>.w_OREINI
            .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        endif
        if .o_MININI<>.w_MININI
            .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        endif
        .DoRTCalc(4,4,.t.)
        if .o_OREFIN<>.w_OREFIN.or. .o_DATFIN<>.w_DATFIN
            .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        endif
        if .o_MINFIN<>.w_MINFIN.or. .o_DATFIN<>.w_DATFIN
            .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (empty(.w_DATFIN) AND .w_OREFIN="23" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        endif
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN<>.w_DATFIN.or. .o_OREFIN<>.w_OREFIN.or. .o_MINFIN<>.w_MINFIN
            .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        .oPgFrm.Page1.oPag.FAKRC_ZOOM.Calculate()
            .w_EV__ANNO = .w_FAKRC_ZOOM.GetVar('EV__ANNO')
            .w_EVSERIAL = .w_FAKRC_ZOOM.GetVar('EVSERIAL')
          .link_1_12('Full')
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .DoRTCalc(11,11,.t.)
        if .o_TIPDIR<>.w_TIPDIR
            .w_TIPDIR1 = IIF(.w_TIPDIR='T','',.w_TIPDIR)
        endif
        .DoRTCalc(13,13,.t.)
        if .o_TIPRIC<>.w_TIPRIC
            .w_TIPRIC1 = IIF(.w_TIPRIC='T','',.w_TIPRIC)
        endif
        .DoRTCalc(15,19,.t.)
        if .o_PERSON<>.w_PERSON
            .w_GRUPPO = Space(5)
          .link_2_14('Full')
        endif
        if .o_TipoDrv<>.w_TipoDrv
            .w_TipoDrv1 = IIF(.w_TipoDrv='X','',.w_TipoDrv)
        endif
        .DoRTCalc(22,35,.t.)
          .link_1_15('Full')
        .DoRTCalc(37,40,.t.)
          .link_1_20('Full')
        if .o_COD_AZI<>.w_COD_AZI
            .w_COD_AZI = i_codazi
          .link_1_21('Full')
        endif
        .DoRTCalc(43,48,.t.)
        if .o_PERSON<>.w_PERSON
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        .DoRTCalc(50,59,.t.)
        if .o_FlgStato<>.w_FlgStato
            .w_FilStato = IIF(.w_FlgStato='X',' ',.w_FlgStato)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.FAKRC_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
    endwith
  return

  proc Calculate_LYBISUKBKM()
    with this
          * --- Imposta data odierna
          .w_DATINI = i_DatSys
          .w_DATFIN = i_DatSys
          .w_OREINI = '00'
          .w_MININI = '00'
          .w_OREFIN = '23'
          .w_MINFIN = '59'
          .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'))
          .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'))
    endwith
  endproc
  proc Calculate_DOBRBAQOOH()
    with this
          * --- Attiva la prima pagina
          .oPgFrm.ActivePage = 1
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOREINI_1_2.enabled = this.oPgFrm.Page1.oPag.oOREINI_1_2.mCond()
    this.oPgFrm.Page1.oPag.oMININI_1_3.enabled = this.oPgFrm.Page1.oPag.oMININI_1_3.mCond()
    this.oPgFrm.Page1.oPag.oOREFIN_1_5.enabled = this.oPgFrm.Page1.oPag.oOREFIN_1_5.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_6.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_6.mCond()
    this.oPgFrm.Page2.oPag.oCODPRA_2_18.enabled = this.oPgFrm.Page2.oPag.oCODPRA_2_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oStr_2_1.visible=!this.oPgFrm.Page2.oPag.oStr_2_1.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_20.visible=!this.oPgFrm.Page2.oPag.oStr_2_20.mHide()
    this.oPgFrm.Page2.oPag.oDESPRA_2_24.visible=!this.oPgFrm.Page2.oPag.oDESPRA_2_24.mHide()
    this.oPgFrm.Page2.oPag.oTIPRIC_2_34.visible=!this.oPgFrm.Page2.oPag.oTIPRIC_2_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsfa_krc
    IF UPPER(cevent)='W_FAKRC_ZOOM SELECTED' AND !EMPTY(this.w_EVSERIAL)
       OpenGest('A','gsfa_aev','EVSERIAL',this.w_EVSERIAL,'EV__ANNO',this.w_EV__ANNO)
    Endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("ImpostaDataOdierna")
          .Calculate_LYBISUKBKM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.FAKRC_ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
        if lower(cEvent)==lower("Ricerca")
          .Calculate_DOBRBAQOOH()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=EVSERIAL
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_lTable = "ANEVENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2], .t., this.ANEVENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVNOMINA,EV_PHONE,EV_EMAIL,EVNUMCEL,EV__NOTE,EVCODPRA";
                   +" from "+i_cTable+" "+i_lTable+" where EVSERIAL="+cp_ToStrODBC(this.w_EVSERIAL);
                   +" and EV__ANNO="+cp_ToStrODBC(this.w_EV__ANNO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EV__ANNO',this.w_EV__ANNO;
                       ,'EVSERIAL',this.w_EVSERIAL)
            select EV__ANNO,EVSERIAL,EVNOMINA,EV_PHONE,EV_EMAIL,EVNUMCEL,EV__NOTE,EVCODPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVSERIAL = NVL(_Link_.EVSERIAL,space(10))
      this.w_EVNOMINA = NVL(_Link_.EVNOMINA,space(15))
      this.w_EV_PHONE = NVL(_Link_.EV_PHONE,space(18))
      this.w_EV_EMAIL = NVL(_Link_.EV_EMAIL,space(254))
      this.w_EVNUMCEL = NVL(_Link_.EVNUMCEL,space(20))
      this.w_EV__NOTE = NVL(_Link_.EV__NOTE,space(0))
      this.w_CNCODCAN = NVL(_Link_.EVCODPRA,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_EVSERIAL = space(10)
      endif
      this.w_EVNOMINA = space(15)
      this.w_EV_PHONE = space(18)
      this.w_EV_EMAIL = space(254)
      this.w_EVNUMCEL = space(20)
      this.w_EV__NOTE = space(0)
      this.w_CNCODCAN = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])+'\'+cp_ToStr(_Link_.EV__ANNO,1)+'\'+cp_ToStr(_Link_.EVSERIAL,1)
      cp_ShowWarn(i_cKey,this.ANEVENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERSON
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERSON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_PERSON)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TIPPER;
                     ,'DPCODICE',trim(this.w_PERSON))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERSON)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PERSON) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oPERSON_2_13'),i_cWhere,'GSAR_BDZ',"Persone",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPPER<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERSON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_PERSON);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TIPPER;
                       ,'DPCODICE',this.w_PERSON)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERSON = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERSON = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
      this.w_DPTIPRIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_PERSON) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_TIPPER
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PERSON = space(5)
        this.w_COGNPART = space(40)
        this.w_NOMEPART = space(40)
        this.w_DESCRPART = space(40)
        this.w_DPTIPRIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERSON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPPO
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_GRUPPO)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_GRUPPO))
          select DPCODICE,DPDESCRI,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUPPO)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_GRUPPO)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_GRUPPO)+"%");

            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GRUPPO) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oGRUPPO_2_14'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSARGADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPPO)
            select DPCODICE,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPPO = NVL(_Link_.DPCODICE,space(5))
      this.w_DESGRUPPO = NVL(_Link_.DPDESCRI,space(60))
      this.w_TIPGRUPPO = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPPO = space(5)
      endif
      this.w_DESGRUPPO = space(60)
      this.w_TIPGRUPPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPGRUPPO='G' AND (GSAR1BGP( '' , 'CHK', .w_PERSON , .w_GRUPPO) OR Empty(.w_PERSON))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo inesistente o persona non appartenente al gruppo selezionato")
        endif
        this.w_GRUPPO = space(5)
        this.w_DESGRUPPO = space(60)
        this.w_TIPGRUPPO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NODESCR2;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_2_16'),i_cWhere,'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_DESNOM = space(40)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_bzz',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRA))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRA_2_18'),i_cWhere,'gsar_bzz',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(75))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESPRA = space(75)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TipEve
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TipEve) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_TipEve)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_TipEve))
          select TETIPEVE,TEDESCRI,TEDRIVER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TipEve)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStrODBC(trim(this.w_TipEve)+"%");

            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TEDESCRI like "+cp_ToStr(trim(this.w_TipEve)+"%");

            select TETIPEVE,TEDESCRI,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TipEve) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oTipEve_2_21'),i_cWhere,'',"Tipi eventi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TipEve)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_TipEve);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_TipEve)
            select TETIPEVE,TEDESCRI,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TipEve = NVL(_Link_.TETIPEVE,space(10))
      this.w_TEDESCRI = NVL(_Link_.TEDESCRI,space(50))
      this.w_TEDRIVER = NVL(_Link_.TEDRIVER,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_TipEve = space(10)
      endif
      this.w_TEDESCRI = space(50)
      this.w_TEDRIVER = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TipEve Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EVNOMINA
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVNOMINA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVNOMINA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_EVNOMINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_EVNOMINA)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVNOMINA = NVL(_Link_.NOCODICE,space(15))
      this.w_NODESCRI = NVL(_Link_.NODESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_EVNOMINA = space(15)
      endif
      this.w_NODESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVNOMINA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNCODCAN
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNCODCAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNCODCAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CNCODCAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CNCODCAN)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNCODCAN = NVL(_Link_.CNCODCAN,space(15))
      this.w_CNDESCAN = NVL(_Link_.CNDESCAN,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_CNCODCAN = space(15)
      endif
      this.w_CNDESCAN = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNCODCAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COD_AZI
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_PRAT_IDX,3]
    i_lTable = "PAR_PRAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_PRAT_IDX,2], .t., this.PAR_PRAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_PRAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_AZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_AZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAVISELA";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_COD_AZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_COD_AZI)
            select PACODAZI,PAVISELA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_AZI = NVL(_Link_.PACODAZI,space(5))
      this.w_VISELA = NVL(_Link_.PAVISELA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COD_AZI = space(5)
      endif
      this.w_VISELA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_PRAT_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_PRAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_AZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_1.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINI_1_2.value==this.w_OREINI)
      this.oPgFrm.Page1.oPag.oOREINI_1_2.value=this.w_OREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_3.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_3.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_4.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_4.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOREFIN_1_5.value==this.w_OREFIN)
      this.oPgFrm.Page1.oPag.oOREFIN_1_5.value=this.w_OREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_6.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_6.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oTipoDrv_2_9.RadioValue()==this.w_TipoDrv)
      this.oPgFrm.Page2.oPag.oTipoDrv_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFlgStato_2_10.RadioValue()==this.w_FlgStato)
      this.oPgFrm.Page2.oPag.oFlgStato_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDIR_2_11.RadioValue()==this.w_TIPDIR)
      this.oPgFrm.Page2.oPag.oTIPDIR_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRiferimento_2_12.value==this.w_Riferimento)
      this.oPgFrm.Page2.oPag.oRiferimento_2_12.value=this.w_Riferimento
    endif
    if not(this.oPgFrm.Page2.oPag.oPERSON_2_13.value==this.w_PERSON)
      this.oPgFrm.Page2.oPag.oPERSON_2_13.value=this.w_PERSON
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUPPO_2_14.value==this.w_GRUPPO)
      this.oPgFrm.Page2.oPag.oGRUPPO_2_14.value=this.w_GRUPPO
    endif
    if not(this.oPgFrm.Page2.oPag.oCODNOM_2_16.value==this.w_CODNOM)
      this.oPgFrm.Page2.oPag.oCODNOM_2_16.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODPRA_2_18.value==this.w_CODPRA)
      this.oPgFrm.Page2.oPag.oCODPRA_2_18.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTE_2_19.value==this.w_NOTE)
      this.oPgFrm.Page2.oPag.oNOTE_2_19.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oTipEve_2_21.value==this.w_TipEve)
      this.oPgFrm.Page2.oPag.oTipEve_2_21.value=this.w_TipEve
    endif
    if not(this.oPgFrm.Page2.oPag.oOggetto_2_22.value==this.w_Oggetto)
      this.oPgFrm.Page2.oPag.oOggetto_2_22.value=this.w_Oggetto
    endif
    if not(this.oPgFrm.Page2.oPag.oRIC_MAIL_2_23.value==this.w_RIC_MAIL)
      this.oPgFrm.Page2.oPag.oRIC_MAIL_2_23.value=this.w_RIC_MAIL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRA_2_24.value==this.w_DESPRA)
      this.oPgFrm.Page2.oPag.oDESPRA_2_24.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOM_2_27.value==this.w_DESNOM)
      this.oPgFrm.Page2.oPag.oDESNOM_2_27.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oRIC_TLF_2_28.value==this.w_RIC_TLF)
      this.oPgFrm.Page2.oPag.oRIC_TLF_2_28.value=this.w_RIC_TLF
    endif
    if not(this.oPgFrm.Page2.oPag.oRIC_CEL_2_30.value==this.w_RIC_CEL)
      this.oPgFrm.Page2.oPag.oRIC_CEL_2_30.value=this.w_RIC_CEL
    endif
    if not(this.oPgFrm.Page2.oPag.oRIC_INT_2_32.value==this.w_RIC_INT)
      this.oPgFrm.Page2.oPag.oRIC_INT_2_32.value=this.w_RIC_INT
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPRIC_2_34.RadioValue()==this.w_TIPRIC)
      this.oPgFrm.Page2.oPag.oTIPRIC_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEVNOMINA_1_15.value==this.w_EVNOMINA)
      this.oPgFrm.Page1.oPag.oEVNOMINA_1_15.value=this.w_EVNOMINA
    endif
    if not(this.oPgFrm.Page1.oPag.oEV_PHONE_1_16.value==this.w_EV_PHONE)
      this.oPgFrm.Page1.oPag.oEV_PHONE_1_16.value=this.w_EV_PHONE
    endif
    if not(this.oPgFrm.Page1.oPag.oEV_EMAIL_1_17.value==this.w_EV_EMAIL)
      this.oPgFrm.Page1.oPag.oEV_EMAIL_1_17.value=this.w_EV_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oEVNUMCEL_1_18.value==this.w_EVNUMCEL)
      this.oPgFrm.Page1.oPag.oEVNUMCEL_1_18.value=this.w_EVNUMCEL
    endif
    if not(this.oPgFrm.Page1.oPag.oEV__NOTE_1_19.value==this.w_EV__NOTE)
      this.oPgFrm.Page1.oPag.oEV__NOTE_1_19.value=this.w_EV__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCNCODCAN_1_20.value==this.w_CNCODCAN)
      this.oPgFrm.Page1.oPag.oCNCODCAN_1_20.value=this.w_CNCODCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCNDESCAN_1_29.value==this.w_CNDESCAN)
      this.oPgFrm.Page1.oPag.oCNDESCAN_1_29.value=this.w_CNDESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oNODESCRI_1_30.value==this.w_NODESCRI)
      this.oPgFrm.Page1.oPag.oNODESCRI_1_30.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oTEDESCRI_2_39.value==this.w_TEDESCRI)
      this.oPgFrm.Page2.oPag.oTEDESCRI_2_39.value=this.w_TEDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDENOM_PART_2_44.value==this.w_DENOM_PART)
      this.oPgFrm.Page2.oPag.oDENOM_PART_2_44.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRUPPO_2_52.value==this.w_DESGRUPPO)
      this.oPgFrm.Page2.oPag.oDESGRUPPO_2_52.value=this.w_DESGRUPPO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(VAL(.w_OREINI) < 24)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(VAL(.w_OREFIN) < 24)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(EMPTY(.w_PERSON) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_TIPPER)  and not(empty(.w_PERSON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPERSON_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPGRUPPO='G' AND (GSAR1BGP( '' , 'CHK', .w_PERSON , .w_GRUPPO) OR Empty(.w_PERSON)))  and not(empty(.w_GRUPPO))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUPPO_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo inesistente o persona non appartenente al gruppo selezionato")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATINI = this.w_DATINI
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_DATFIN = this.w_DATFIN
    this.o_OREFIN = this.w_OREFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_TipoDrv = this.w_TipoDrv
    this.o_FlgStato = this.w_FlgStato
    this.o_TIPDIR = this.w_TIPDIR
    this.o_PERSON = this.w_PERSON
    this.o_TIPRIC = this.w_TIPRIC
    this.o_COD_AZI = this.w_COD_AZI
    return

enddefine

* --- Define pages as container
define class tgsfa_krcPag1 as StdContainer
  Width  = 705
  height = 487
  stdWidth  = 705
  stdheight = 487
  resizeXpos=299
  resizeYpos=228
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_1 as StdField with uid="VJALXHXOBD",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale",;
    HelpContextID = 146537674,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=99, Top=8

  add object oOREINI_1_2 as StdField with uid="WOKDXXPUIP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_OREINI", cQueryName = "OREINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di inizio selezione",;
    HelpContextID = 146594586,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=230, Top=8, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREINI_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oOREINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_3 as StdField with uid="QMRKNBACQP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio selezione",;
    HelpContextID = 146560058,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=264, Top=8, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oMININI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_4 as StdField with uid="IHLXVHHGUF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale",;
    HelpContextID = 68091082,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=345, Top=8

  add object oOREFIN_1_5 as StdField with uid="VKJCJSQZFK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_OREFIN", cQueryName = "OREFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di fine selezione",;
    HelpContextID = 68147994,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=475, Top=8, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREFIN_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oOREFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_6 as StdField with uid="GIWPXXGMLC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di fine selezione",;
    HelpContextID = 68113466,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=509, Top=8, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oMINFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc


  add object FAKRC_ZOOM as cp_zoombox with uid="TCWQBJQGVA",left=6, top=49, width=684,height=309,;
    caption='FAKRC_ZOOM',;
   bGlobalFont=.t.,;
    cZoomOnZoom="",cZoomFile="GSFA_KRC",bOptions=.f.,bAdvOptions=.f.,cTable="ANEVENTI",bQueryOnLoad=.t.,bReadOnly=.f.,cMenuFile="",bRetriveAllRows=.f.,bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Ricerca,Blank",;
    nPag=1;
    , HelpContextID = 56834667


  add object oObj_1_13 as cp_runprogram with uid="PXXZWEIRWQ",left=5, top=537, width=173,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsag_ble(w_IDRICH,'w_ATSEREVE',This.Parent.oContained.oparentobject,w_NOMNOM)",;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 61965542


  add object oBtn_1_14 as StdButton with uid="SVBJVATCCD",left=649, top=3, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare l'elenco contatti";
    , HelpContextID = 60890134;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oEVNOMINA_1_15 as StdField with uid="SOZFEIAOTY",rtseq=36,rtrep=.f.,;
    cFormVar = "w_EVNOMINA", cQueryName = "EVNOMINA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 147212153,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=99, Top=362, InputMask=replicate('X',15), cLinkFile="OFF_NOMI", oKey_1_1="NOCODICE", oKey_1_2="this.w_EVNOMINA"

  func oEVNOMINA_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oEV_PHONE_1_16 as StdField with uid="TOWRYZJOAT",rtseq=37,rtrep=.f.,;
    cFormVar = "w_EV_PHONE", cQueryName = "EV_PHONE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    HelpContextID = 51656565,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=99, Top=412, InputMask=replicate('X',18)

  add object oEV_EMAIL_1_17 as StdField with uid="VSYJUOSVEL",rtseq=38,rtrep=.f.,;
    cFormVar = "w_EV_EMAIL", cQueryName = "EV_EMAIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 254855314,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=416, Top=412, InputMask=replicate('X',254)

  add object oEVNUMCEL_1_18 as StdField with uid="REDEQXFPMK",rtseq=39,rtrep=.f.,;
    cFormVar = "w_EVNUMCEL", cQueryName = "EVNUMCEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 20953234,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=255, Top=412, InputMask=replicate('X',20)

  add object oEV__NOTE_1_19 as StdMemo with uid="KFCEUSXRRN",rtseq=40,rtrep=.f.,;
    cFormVar = "w_EV__NOTE", cQueryName = "EV__NOTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 224053387,;
   bGlobalFont=.t.,;
    Height=45, Width=431, Left=99, Top=437

  add object oCNCODCAN_1_20 as StdField with uid="VFEHOBSFQK",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CNCODCAN", cQueryName = "CNCODCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 11075700,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=99, Top=387, InputMask=replicate('X',15), cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CNCODCAN"

  func oCNCODCAN_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCNDESCAN_1_29 as StdField with uid="PBHTBXHSEJ",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CNDESCAN", cQueryName = "CNDESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 26153076,;
   bGlobalFont=.t.,;
    Height=21, Width=468, Left=220, Top=387, InputMask=replicate('X',100)

  add object oNODESCRI_1_30 as StdField with uid="MBDAXPIEJS",rtseq=45,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 26153503,;
   bGlobalFont=.t.,;
    Height=21, Width=468, Left=220, Top=362, InputMask=replicate('X',60)


  add object oBtn_1_35 as StdButton with uid="FKYRREBYVR",left=545, top=437, width=48,height=45,;
    CpPicture="bmp\visuali.ico", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare l'evento";
    , HelpContextID = 264915014;
    , Caption='\<Evento';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      with this.Parent.oContained
        OpenGest('A','gsfa_aev','EVSERIAL',.w_EVSERIAL,'EV__ANNO',.w_EV__ANNO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_EVSERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="MAQKJGXEIK",left=597, top=437, width=48,height=45,;
    CpPicture="bmp\DM_agenda.bmp", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Visualizza attivit� ";
    , HelpContextID = 166082278;
    , Caption='\<Attivit�';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSPR_BAT(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_37 as StdButton with uid="UNOCCKYENI",left=649, top=437, width=48,height=45,;
    CpPicture="BMP\carica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per caricare un nuovo evento/prestazione.";
    , HelpContextID = 259856426;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSFA_BVS(this.Parent.oContained,"NewEvt")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_23 as StdString with uid="JQWHNGECVG",Visible=.t., Left=9, Top=412,;
    Alignment=1, Width=85, Height=18,;
    Caption="Telefono/FAX:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="JDYKCNQBJE",Visible=.t., Left=221, Top=412,;
    Alignment=1, Width=31, Height=18,;
    Caption="Cell.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="ZRMSHKGRWY",Visible=.t., Left=373, Top=412,;
    Alignment=1, Width=41, Height=18,;
    Caption="e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="GNMQDSQBMH",Visible=.t., Left=9, Top=362,;
    Alignment=1, Width=85, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="GBNXQDCZVX",Visible=.t., Left=50, Top=437,;
    Alignment=1, Width=44, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="PSVSDIHZER",Visible=.t., Left=9, Top=387,;
    Alignment=1, Width=85, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="DBGPZMROUJ",Visible=.t., Left=48, Top=8,;
    Alignment=1, Width=46, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="CBRLJUQJGD",Visible=.t., Left=296, Top=8,;
    Alignment=1, Width=46, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="EWZSPAOTVW",Visible=.t., Left=177, Top=8,;
    Alignment=1, Width=49, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="CTAHWBKCXZ",Visible=.t., Left=422, Top=8,;
    Alignment=1, Width=49, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsfa_krcPag2 as StdContainer
  Width  = 705
  height = 487
  stdWidth  = 705
  stdheight = 487
  resizeXpos=367
  resizeYpos=263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTipoDrv_2_9 as StdCombo with uid="SQBXXQKSHH",rtseq=15,rtrep=.f.,left=123,top=15,width=146,height=22;
    , ToolTipText = "Tipo driver";
    , HelpContextID = 265022518;
    , cFormVar="w_TipoDrv",RowSource=""+"Mail,"+"Telefonata,"+"Fax,"+"Documento,"+"Altro,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTipoDrv_2_9.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    iif(this.value =3,'F',;
    iif(this.value =4,'D',;
    iif(this.value =5,'A',;
    iif(this.value =6,'X',;
    space(1))))))))
  endfunc
  func oTipoDrv_2_9.GetRadio()
    this.Parent.oContained.w_TipoDrv = this.RadioValue()
    return .t.
  endfunc

  func oTipoDrv_2_9.SetRadio()
    this.Parent.oContained.w_TipoDrv=trim(this.Parent.oContained.w_TipoDrv)
    this.value = ;
      iif(this.Parent.oContained.w_TipoDrv=='M',1,;
      iif(this.Parent.oContained.w_TipoDrv=='T',2,;
      iif(this.Parent.oContained.w_TipoDrv=='F',3,;
      iif(this.Parent.oContained.w_TipoDrv=='D',4,;
      iif(this.Parent.oContained.w_TipoDrv=='A',5,;
      iif(this.Parent.oContained.w_TipoDrv=='X',6,;
      0))))))
  endfunc


  add object oFlgStato_2_10 as StdCombo with uid="BVWPUIRVQJ",rtseq=16,rtrep=.f.,left=123,top=44,width=146,height=22;
    , ToolTipText = "Tipo driver";
    , HelpContextID = 28270277;
    , cFormVar="w_FlgStato",RowSource=""+"Processato,"+"Da processare,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFlgStato_2_10.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oFlgStato_2_10.GetRadio()
    this.Parent.oContained.w_FlgStato = this.RadioValue()
    return .t.
  endfunc

  func oFlgStato_2_10.SetRadio()
    this.Parent.oContained.w_FlgStato=trim(this.Parent.oContained.w_FlgStato)
    this.value = ;
      iif(this.Parent.oContained.w_FlgStato=='P',1,;
      iif(this.Parent.oContained.w_FlgStato=='D',2,;
      iif(this.Parent.oContained.w_FlgStato=='X',3,;
      0)))
  endfunc


  add object oTIPDIR_2_11 as StdCombo with uid="UOLGNCUVIK",rtseq=17,rtrep=.f.,left=123,top=73,width=146,height=22;
    , ToolTipText = "Direzione evento";
    , HelpContextID = 1127370;
    , cFormVar="w_TIPDIR",RowSource=""+"Entrata,"+"Uscita,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPDIR_2_11.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'U',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPDIR_2_11.GetRadio()
    this.Parent.oContained.w_TIPDIR = this.RadioValue()
    return .t.
  endfunc

  func oTIPDIR_2_11.SetRadio()
    this.Parent.oContained.w_TIPDIR=trim(this.Parent.oContained.w_TIPDIR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDIR=='E',1,;
      iif(this.Parent.oContained.w_TIPDIR=='U',2,;
      iif(this.Parent.oContained.w_TIPDIR=='T',3,;
      0)))
  endfunc

  add object oRiferimento_2_12 as AH_SEARCHFLD with uid="FVQCKJUMLY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_Riferimento", cQueryName = "Riferimento",;
    bObbl = .f. , nPag = 2, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento della persona o parte della ragione sociale",;
    HelpContextID = 106383525,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=123, Top=102, InputMask=replicate('X',250)

  add object oPERSON_2_13 as StdField with uid="ZTRRYFXJMX",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PERSON", cQueryName = "PERSON",nZero=5,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Persona di studio",;
    HelpContextID = 60954634,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=123, Top=131, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TIPPER", oKey_2_1="DPCODICE", oKey_2_2="this.w_PERSON"

  func oPERSON_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERSON_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERSON_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TIPPER)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TIPPER)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oPERSON_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oPERSON_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TIPPER
     i_obj.w_DPCODICE=this.parent.oContained.w_PERSON
     i_obj.ecpSave()
  endproc

  add object oGRUPPO_2_14 as StdField with uid="ZEDITMWRPB",rtseq=20,rtrep=.f.,;
    cFormVar = "w_GRUPPO", cQueryName = "GRUPPO",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo inesistente o persona non appartenente al gruppo selezionato",;
    ToolTipText = "Gruppo",;
    HelpContextID = 43309978,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=123, Top=160, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_GRUPPO"

  func oGRUPPO_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUPPO_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUPPO_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oGRUPPO_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSARGADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oGRUPPO_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_GRUPPO
     i_obj.ecpSave()
  endproc

  add object oCODNOM_2_16 as StdField with uid="LDORSNJAGM",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo o soggetto esterno della pratica",;
    HelpContextID = 78114522,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=123, Top=189, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc

  add object oCODPRA_2_18 as StdField with uid="LNZCMMZIKS",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice pratica",;
    HelpContextID = 7728858,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=123, Top=218, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="gsar_bzz", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITCODPRA)
    endwith
   endif
  endfunc

  func oCODPRA_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRA_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRA_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRA_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_bzz',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODPRA_2_18.mZoomOnZoom
    local i_obj
    i_obj=gsar_bzz()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRA
     i_obj.ecpSave()
  endproc

  add object oNOTE_2_19 as AH_SEARCHMEMO with uid="YFIDICHIXZ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Annotazioni o parte di esse",;
    HelpContextID = 111144490,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=123, Top=247

  add object oTipEve_2_21 as StdField with uid="KNOVHIRVUD",rtseq=26,rtrep=.f.,;
    cFormVar = "w_TipEve", cQueryName = "TipEve",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Tipo evento",;
    HelpContextID = 96594998,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=123, Top=276, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", oKey_1_1="TETIPEVE", oKey_1_2="this.w_TipEve"

  func oTipEve_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oTipEve_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTipEve_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oTipEve_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipi eventi",'',this.parent.oContained
  endproc

  add object oOggetto_2_22 as AH_SEARCHFLD with uid="AVSKMMIFVK",rtseq=27,rtrep=.f.,;
    cFormVar = "w_Oggetto", cQueryName = "Oggetto",;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione oggetto o parte di essa",;
    HelpContextID = 79780326,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=123, Top=305, InputMask=replicate('X',100)

  add object oRIC_MAIL_2_23 as AH_SEARCHFLD with uid="FGPYBELTXO",rtseq=28,rtrep=.f.,;
    cFormVar = "w_RIC_MAIL", cQueryName = "RIC_MAIL",;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di e-mail o parte di esso",;
    HelpContextID = 256441442,;
   bGlobalFont=.t.,;
    Height=21, Width=566, Left=123, Top=334, InputMask=replicate('X',100)

  add object oDESPRA_2_24 as StdField with uid="DHZPCSQKFH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(75), bMultilanguage =  .f.,;
    HelpContextID = 7669962,;
   bGlobalFont=.t.,;
    Height=21, Width=412, Left=277, Top=218, InputMask=replicate('X',75)

  func oDESPRA_2_24.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDESNOM_2_27 as StdField with uid="YBKTMCLSXL",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78055626,;
   bGlobalFont=.t.,;
    Height=21, Width=412, Left=277, Top=189, InputMask=replicate('X',40)

  add object oRIC_TLF_2_28 as AH_SEARCHFLD with uid="OBXUONETPR",rtseq=32,rtrep=.f.,;
    cFormVar = "w_RIC_TLF", cQueryName = "RIC_TLF",;
    bObbl = .f. , nPag = 2, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di telefono/fax o parte di esso",;
    HelpContextID = 179895318,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=123, Top=363, InputMask=replicate('X',18)

  add object oRIC_CEL_2_30 as AH_SEARCHFLD with uid="WDZNOMZIRN",rtseq=33,rtrep=.f.,;
    cFormVar = "w_RIC_CEL", cQueryName = "RIC_CEL",;
    bObbl = .f. , nPag = 2, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di cellulare o parte di esso",;
    HelpContextID = 223806442,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=123, Top=392, InputMask=replicate('X',18)

  add object oRIC_INT_2_32 as AH_SEARCHFLD with uid="ZFVNTGQSWX",rtseq=34,rtrep=.f.,;
    cFormVar = "w_RIC_INT", cQueryName = "RIC_INT",;
    bObbl = .f. , nPag = 2, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di telefono interno o parte di esso",;
    HelpContextID = 201915414,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=123, Top=421, InputMask=replicate('X',18)


  add object oTIPRIC_2_34 as StdCombo with uid="NNSKIDFEAC",rtseq=35,rtrep=.f.,left=543,top=73,width=146,height=22;
    , ToolTipText = "Stato della richiesta - aperto, in corso, in attesa, chiuso";
    , HelpContextID = 251868106;
    , cFormVar="w_TIPRIC",RowSource=""+"Da chiudere,"+"Aperta,"+"In corso,"+"In attesa,"+"Chiusa,"+"Tutte", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPRIC_2_34.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'A',;
    iif(this.value =3,'I',;
    iif(this.value =4,'S',;
    iif(this.value =5,'C',;
    iif(this.value =6,'T',;
    space(1))))))))
  endfunc
  func oTIPRIC_2_34.GetRadio()
    this.Parent.oContained.w_TIPRIC = this.RadioValue()
    return .t.
  endfunc

  func oTIPRIC_2_34.SetRadio()
    this.Parent.oContained.w_TIPRIC=trim(this.Parent.oContained.w_TIPRIC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPRIC=='D',1,;
      iif(this.Parent.oContained.w_TIPRIC=='A',2,;
      iif(this.Parent.oContained.w_TIPRIC=='I',3,;
      iif(this.Parent.oContained.w_TIPRIC=='S',4,;
      iif(this.Parent.oContained.w_TIPRIC=='C',5,;
      iif(this.Parent.oContained.w_TIPRIC=='T',6,;
      0))))))
  endfunc

  func oTIPRIC_2_34.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oTEDESCRI_2_39 as StdField with uid="PCMCQLJYWO",rtseq=46,rtrep=.f.,;
    cFormVar = "w_TEDESCRI", cQueryName = "TEDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 26151039,;
   bGlobalFont=.t.,;
    Height=21, Width=447, Left=242, Top=276, InputMask=replicate('X',50)

  add object oDENOM_PART_2_44 as StdField with uid="VCFNHXFDWG",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(48), bMultilanguage =  .f.,;
    HelpContextID = 221905047,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=193, Top=131, InputMask=replicate('X',48)

  add object oDESGRUPPO_2_52 as StdField with uid="KGSIUCUANR",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESGRUPPO", cQueryName = "DESGRUPPO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 58850422,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=193, Top=160, InputMask=replicate('X',60)


  add object oBtn_2_57 as StdButton with uid="MIFDUJZEEP",left=649, top=3, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per aggiornare l'elenco contatti";
    , HelpContextID = 60890134;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_57.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_1 as StdString with uid="PKLUAHGBDW",Visible=.t., Left=427, Top=73,;
    Alignment=1, Width=108, Height=18,;
    Caption="Stato  richieste:"  ;
  , bGlobalFont=.t.

  func oStr_2_1.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_3 as StdString with uid="KYJNGQLJSJ",Visible=.t., Left=8, Top=131,;
    Alignment=1, Width=108, Height=18,;
    Caption="Persona di studio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="CHKPISWTGJ",Visible=.t., Left=8, Top=160,;
    Alignment=1, Width=108, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="LMSPRXSGTO",Visible=.t., Left=8, Top=15,;
    Alignment=1, Width=108, Height=18,;
    Caption="Tipo driver:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="HZALIZNENK",Visible=.t., Left=8, Top=218,;
    Alignment=1, Width=108, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_2_20.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_2_26 as StdString with uid="RCYDFBJYOO",Visible=.t., Left=8, Top=189,;
    Alignment=1, Width=108, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="LTLCHJLFHB",Visible=.t., Left=8, Top=363,;
    Alignment=1, Width=108, Height=18,;
    Caption="Telefono/fax:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="WIIJQWRENE",Visible=.t., Left=8, Top=392,;
    Alignment=1, Width=108, Height=18,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="BENCEAGZGZ",Visible=.t., Left=8, Top=421,;
    Alignment=1, Width=108, Height=18,;
    Caption="Telefono interno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="GVAGGVLXVN",Visible=.t., Left=8, Top=334,;
    Alignment=1, Width=108, Height=18,;
    Caption="Mail:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="BSGKMHWZHM",Visible=.t., Left=8, Top=247,;
    Alignment=1, Width=108, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="GXOWXVAARV",Visible=.t., Left=8, Top=305,;
    Alignment=1, Width=108, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="IKRHYKNAON",Visible=.t., Left=8, Top=102,;
    Alignment=1, Width=108, Height=18,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="RULIUYLWKZ",Visible=.t., Left=8, Top=276,;
    Alignment=1, Width=108, Height=18,;
    Caption="Tipo evento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="AHZQYXDKXK",Visible=.t., Left=8, Top=73,;
    Alignment=1, Width=108, Height=18,;
    Caption="Direzione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="JFCNGYIXYG",Visible=.t., Left=8, Top=44,;
    Alignment=1, Width=108, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_krc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
