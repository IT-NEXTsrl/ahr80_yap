* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bdl                                                        *
*              Aggiorna temporaneo listini                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-03-05                                                      *
* Last revis.: 2010-09-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pKEYLISTB,pLog,pCODLIS
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bdl",oParentObject,m.pOPER,m.pKEYLISTB,m.pLog,m.pCODLIS)
return(i_retval)

define class tgsag_bdl as StdBatch
  * --- Local variables
  pOPER = space(1)
  pKEYLISTB = space(10)
  pLog = .NULL.
  pCODLIS = space(5)
  * --- WorkFile variables
  LIST_TMP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Azzera temporaneo lisitni
    * --- D:  azzere temporaneo listini
    *     E:  elimina solo riga listino acquisti
    *     I:   inserisce solo riga listino acquisti
    if Not EMpty(this.pKEYLISTB)
      * --- Try
      local bErr_03824EA0
      bErr_03824EA0=bTrsErr
      this.Try_03824EA0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        if Vartype(this.pLog)="O"
          this.plog.AddMsgLogPartNoTrans(space(4), "Impossibile aggiornare temporaneo listini")     
        else
          AH_ERRORMSG("Impossibile aggiornare temporaneo listini",48,"")
        endif
      endif
      bTrsErr=bTrsErr or bErr_03824EA0
      * --- End
    endif
  endproc
  proc Try_03824EA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.pOPER="D"
        this.bUpdateParentObject=.F.
        * --- Delete from LIST_TMP
        i_nConn=i_TableProp[this.LIST_TMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIST_TMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CHIAVEP = "+cp_ToStrODBC(this.pKEYLISTB);
                 )
        else
          delete from (i_cTable) where;
                CHIAVEP = this.pKEYLISTB;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.pOPER="I"
        if Not Empty(this.pCODLIS)
          * --- Read from LIST_TMP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.LIST_TMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIST_TMP_idx,2],.t.,this.LIST_TMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CODICE"+;
              " from "+i_cTable+" LIST_TMP where ";
                  +"CHIAVEP = "+cp_ToStrODBC(this.pKEYLISTB);
                  +" and CODICE = "+cp_ToStrODBC(this.pCODLIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CODICE;
              from (i_cTable) where;
                  CHIAVEP = this.pKEYLISTB;
                  and CODICE = this.pCODLIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_CODLIS = NVL(cp_ToDate(_read_.CODICE),cp_NullValue(_read_.CODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if Empty(w_CODLIS)
          * --- Insert into LIST_TMP
          i_nConn=i_TableProp[this.LIST_TMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIST_TMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsag_bdl",this.LIST_TMP_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      case this.pOPER="E"
        if Not Empty(this.pCODLIS)
          * --- Delete from LIST_TMP
          i_nConn=i_TableProp[this.LIST_TMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIST_TMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"CHIAVEP = "+cp_ToStrODBC(this.pKEYLISTB);
                  +" and CODICE = "+cp_ToStrODBC(this.pCODLIS);
                   )
          else
            delete from (i_cTable) where;
                  CHIAVEP = this.pKEYLISTB;
                  and CODICE = this.pCODLIS;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
    endcase
    return


  proc Init(oParentObject,pOPER,pKEYLISTB,pLog,pCODLIS)
    this.pOPER=pOPER
    this.pKEYLISTB=pKEYLISTB
    this.pLog=pLog
    this.pCODLIS=pCODLIS
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LIST_TMP'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pKEYLISTB,pLog,pCODLIS"
endproc
