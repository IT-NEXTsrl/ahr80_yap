* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ksc                                                        *
*              Ricostruzione saldi contratti a pacchetto                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-09-30                                                      *
* Last revis.: 2013-06-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_ksc",oParentObject))

* --- Class definition
define class tgsag_ksc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 762
  Height = 169
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-06-07"
  HelpContextID=99231593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  CON_TRAS_IDX = 0
  MOD_ELEM_IDX = 0
  cPrg = "gsag_ksc"
  cComment = "Ricostruzione saldi contratti a pacchetto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ELTIPCLI = space(1)
  w_ELCODCLI = space(15)
  w_CODICE = space(15)
  w_ANDESCRI = space(60)
  w_ELCONTRA = space(10)
  o_ELCONTRA = space(10)
  w_CODESCON = space(50)
  w_ELCODMOD = space(10)
  w_MOTIPCON = space(1)
  w_MODESCRI = space(60)
  w_ELCODIMP = space(10)
  w_ELCODCOM = 0
  w_ELRINNOV = 0
  w_FLDATINI = ctod('  /  /  ')
  w_FLDATFIN = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kscPag1","gsag_ksc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oELCODCLI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CON_TRAS'
    this.cWorkTables[3]='MOD_ELEM'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSAG_BKC(this,"OK")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ELTIPCLI=space(1)
      .w_ELCODCLI=space(15)
      .w_CODICE=space(15)
      .w_ANDESCRI=space(60)
      .w_ELCONTRA=space(10)
      .w_CODESCON=space(50)
      .w_ELCODMOD=space(10)
      .w_MOTIPCON=space(1)
      .w_MODESCRI=space(60)
      .w_ELCODIMP=space(10)
      .w_ELCODCOM=0
      .w_ELRINNOV=0
      .w_FLDATINI=ctod("  /  /  ")
      .w_FLDATFIN=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
        .w_ELTIPCLI = 'C'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ELCODCLI))
          .link_1_3('Full')
        endif
        .w_CODICE = .w_ELCODCLI
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_ELCONTRA))
          .link_1_7('Full')
        endif
        .DoRTCalc(6,7,.f.)
        if not(empty(.w_ELCODMOD))
          .link_1_11('Full')
        endif
          .DoRTCalc(8,14,.f.)
        .w_OBTEST = i_DatSys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_CODICE = .w_ELCODCLI
        if .o_ELCONTRA<>.w_ELCONTRA
          .Calculate_KLTFAYDMHU()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_KLTFAYDMHU()
    with this
          * --- Aggiorna cliente su cambio contratto
          .w_ELCODCLI = .w_ELCODCLI
          .link_1_3('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ELCODCLI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ELCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ELTIPCLI);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ELTIPCLI;
                     ,'ANCODICE',trim(this.w_ELCODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_ELCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ELTIPCLI);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_ELCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_ELTIPCLI);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oELCODCLI_1_3'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ELTIPCLI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ELTIPCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ELCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ELTIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ELTIPCLI;
                       ,'ANCODICE',this.w_ELCODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODCLI = space(15)
      endif
      this.w_ANDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCONTRA
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACA',True,'CON_TRAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COSERIAL like "+cp_ToStrODBC(trim(this.w_ELCONTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON,COCODCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COSERIAL',trim(this.w_ELCONTRA))
          select COSERIAL,CODESCON,COCODCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCONTRA)==trim(_Link_.COSERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStrODBC(trim(this.w_ELCONTRA)+"%");

            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON,COCODCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CODESCON like "+cp_ToStr(trim(this.w_ELCONTRA)+"%");

            select COSERIAL,CODESCON,COCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCONTRA) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAS','*','COSERIAL',cp_AbsName(oSource.parent,'oELCONTRA_1_7'),i_cWhere,'GSAG_ACA',"Contratti",'GSAG_AEL.CON_TRAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON,COCODCON";
                     +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',oSource.xKey(1))
            select COSERIAL,CODESCON,COCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON,COCODCON";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_ELCONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_ELCONTRA)
            select COSERIAL,CODESCON,COCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCONTRA = NVL(_Link_.COSERIAL,space(10))
      this.w_CODESCON = NVL(_Link_.CODESCON,space(50))
      this.w_ELCODCLI = NVL(_Link_.COCODCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ELCONTRA = space(10)
      endif
      this.w_CODESCON = space(50)
      this.w_ELCODCLI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODMOD
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_AME',True,'MOD_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_ELCODMOD)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_ELCODMOD))
          select MOCODICE,MODESCRI,MOTIPCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODMOD)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MODESCRI like "+cp_ToStrODBC(trim(this.w_ELCODMOD)+"%");

            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MODESCRI like "+cp_ToStr(trim(this.w_ELCODMOD)+"%");

            select MOCODICE,MODESCRI,MOTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODMOD) and !this.bDontReportError
            deferred_cp_zoom('MOD_ELEM','*','MOCODICE',cp_AbsName(oSource.parent,'oELCODMOD_1_11'),i_cWhere,'GSAG_AME',"Modelli",'GSAG_KSC.MOD_ELEM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI,MOTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI,MOTIPCON";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_ELCODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_ELCODMOD)
            select MOCODICE,MODESCRI,MOTIPCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODMOD = NVL(_Link_.MOCODICE,space(10))
      this.w_MODESCRI = NVL(_Link_.MODESCRI,space(60))
      this.w_MOTIPCON = NVL(_Link_.MOTIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODMOD = space(10)
      endif
      this.w_MODESCRI = space(60)
      this.w_MOTIPCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MOTIPCON='P'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Modello elemento inesistente o non di tipo a pacchetto")
        endif
        this.w_ELCODMOD = space(10)
        this.w_MODESCRI = space(60)
        this.w_MOTIPCON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oELCODCLI_1_3.value==this.w_ELCODCLI)
      this.oPgFrm.Page1.oPag.oELCODCLI_1_3.value=this.w_ELCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_5.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_5.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oELCONTRA_1_7.value==this.w_ELCONTRA)
      this.oPgFrm.Page1.oPag.oELCONTRA_1_7.value=this.w_ELCONTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_1_8.value==this.w_CODESCON)
      this.oPgFrm.Page1.oPag.oCODESCON_1_8.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODMOD_1_11.value==this.w_ELCODMOD)
      this.oPgFrm.Page1.oPag.oELCODMOD_1_11.value=this.w_ELCODMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESCRI_1_13.value==this.w_MODESCRI)
      this.oPgFrm.Page1.oPag.oMODESCRI_1_13.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATINI_1_18.value==this.w_FLDATINI)
      this.oPgFrm.Page1.oPag.oFLDATINI_1_18.value=this.w_FLDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATFIN_1_20.value==this.w_FLDATFIN)
      this.oPgFrm.Page1.oPag.oFLDATFIN_1_20.value=this.w_FLDATFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_MOTIPCON='P')  and not(empty(.w_ELCODMOD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELCODMOD_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Modello elemento inesistente o non di tipo a pacchetto")
          case   not(EMPTY(.w_FLDATFIN) OR .w_FLDATINI<=.w_FLDATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFLDATINI_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data d'inizio selezione attivit� deve essere minore o uguale della data di fine selezione attivit�")
          case   not(EMPTY(.w_FLDATINI) OR .w_FLDATINI<=.w_FLDATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFLDATFIN_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di fine selezione attivit� deve essere maggiore o uguale della data d'inizio selezione attivit�")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ELCONTRA = this.w_ELCONTRA
    return

enddefine

* --- Define pages as container
define class tgsag_kscPag1 as StdContainer
  Width  = 758
  height = 169
  stdWidth  = 758
  stdheight = 169
  resizeXpos=563
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELCODCLI_1_3 as StdField with uid="ZTGNMVFVCG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ELCODCLI", cQueryName = "ELCODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente",;
    HelpContextID = 240559729,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=165, Top=19, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ELTIPCLI", oKey_2_1="ANCODICE", oKey_2_2="this.w_ELCODCLI"

  func oELCODCLI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODCLI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODCLI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ELTIPCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_ELTIPCLI)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oELCODCLI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oELCODCLI_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_ELTIPCLI
     i_obj.w_ANCODICE=this.parent.oContained.w_ELCODCLI
     i_obj.ecpSave()
  endproc

  add object oANDESCRI_1_5 as StdField with uid="RDFMEBVSTG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 42953551,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=314, Top=19, InputMask=replicate('X',60)

  add object oELCONTRA_1_7 as StdField with uid="OYQLEYVDQZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ELCONTRA", cQueryName = "ELCONTRA",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice contratto",;
    HelpContextID = 55138695,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=165, Top=48, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CON_TRAS", cZoomOnZoom="GSAG_ACA", oKey_1_1="COSERIAL", oKey_1_2="this.w_ELCONTRA"

  func oELCONTRA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCONTRA_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCONTRA_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CON_TRAS','*','COSERIAL',cp_AbsName(this.parent,'oELCONTRA_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACA',"Contratti",'GSAG_AEL.CON_TRAS_VZM',this.parent.oContained
  endproc
  proc oELCONTRA_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_COSERIAL=this.parent.oContained.w_ELCONTRA
     i_obj.ecpSave()
  endproc

  add object oCODESCON_1_8 as StdField with uid="OWSBBUPJPS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 225481612,;
   bGlobalFont=.t.,;
    Height=21, Width=466, Left=281, Top=48, InputMask=replicate('X',50)

  add object oELCODMOD_1_11 as StdField with uid="JXVUKMAYMU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ELCODMOD", cQueryName = "ELCODMOD",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Modello elemento inesistente o non di tipo a pacchetto",;
    ToolTipText = "Codice modello elementi",;
    HelpContextID = 72787574,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=165, Top=77, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_ELEM", cZoomOnZoom="GSAG_AME", oKey_1_1="MOCODICE", oKey_1_2="this.w_ELCODMOD"

  func oELCODMOD_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODMOD_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODMOD_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_ELEM','*','MOCODICE',cp_AbsName(this.parent,'oELCODMOD_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_AME',"Modelli",'GSAG_KSC.MOD_ELEM_VZM',this.parent.oContained
  endproc
  proc oELCODMOD_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAG_AME()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_ELCODMOD
     i_obj.ecpSave()
  endproc

  add object oMODESCRI_1_13 as StdField with uid="XYDCYQUHYN",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 42953999,;
   bGlobalFont=.t.,;
    Height=21, Width=466, Left=281, Top=77, InputMask=replicate('X',60)

  add object oFLDATINI_1_18 as StdField with uid="BVLZWMKFOT",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FLDATINI", cQueryName = "FLDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data d'inizio selezione attivit� deve essere minore o uguale della data di fine selezione attivit�",;
    ToolTipText = "Data inizio selezione attivit�",;
    HelpContextID = 124032609,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=165, Top=106

  func oFLDATINI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_FLDATFIN) OR .w_FLDATINI<=.w_FLDATFIN)
    endwith
    return bRes
  endfunc

  add object oFLDATFIN_1_20 as StdField with uid="JMSWAFJHYU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FLDATFIN", cQueryName = "FLDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di fine selezione attivit� deve essere maggiore o uguale della data d'inizio selezione attivit�",;
    ToolTipText = "Data fine selezione attivit�",;
    HelpContextID = 174364252,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=165, Top=135

  func oFLDATFIN_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_FLDATINI) OR .w_FLDATINI<=.w_FLDATFIN)
    endwith
    return bRes
  endfunc


  add object oBtn_1_21 as StdButton with uid="PGFMVLGCKD",left=648, top=112, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per ricostruire i saldi dei contratti a pacchetto ore selezionati";
    , HelpContextID = 99202842;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSAG_BKC(this.Parent.oContained,"OK")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="KOTWJAFPMC",left=699, top=112, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91914170;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="ZNSWKMDWGC",Visible=.t., Left=17, Top=21,;
    Alignment=1, Width=145, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="GOFVYYEOFF",Visible=.t., Left=17, Top=51,;
    Alignment=1, Width=145, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="WIYVNLRFVC",Visible=.t., Left=17, Top=80,;
    Alignment=1, Width=145, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="FKBLDCRHZU",Visible=.t., Left=17, Top=108,;
    Alignment=1, Width=145, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="LSTOKBHFXJ",Visible=.t., Left=17, Top=136,;
    Alignment=1, Width=145, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_ksc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
