* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ba2                                                        *
*              Azzera data e ora                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-09                                                      *
* Last revis.: 2013-06-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,PARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_ba2",oParentObject,m.PARAM)
return(i_retval)

define class tgsag_ba2 as StdBatch
  * --- Local variables
  PARAM = space(0)
  TipoFiltro = space(1)
  N_GG_IN = 0
  N_GG_AV = 0
  * --- WorkFile variables
  PAR_AGEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Azzeramento ore e minuti della data iniziale e di quella finale in Ricerca Attivit�
    * --- PARAM=1 : data iniziale
    *     PARAM=2 : data finale
    do case
      case this.PARAM="1"
        if empty(this.oParentObject.w_DATINI)
          this.oParentObject.w_OREINI = "00"
          this.oParentObject.w_MININI = "00"
        endif
        this.oParentObject.w_DATFIN = Trasdatfin(this.oParentObject.w_DATINI,this.oParentObject.o_DATINI,this.oParentObject.w_DATFIN)
      case this.PARAM="2"
        if empty(this.oParentObject.w_DATFIN)
          this.oParentObject.w_OREFIN = "00"
          this.oParentObject.w_MINFIN = "00"
        endif
      case this.PARAM="3"
        * --- Aggiornamento date nella maschera di Stampa pratiche (filtro prestazioni)
        this.TipoFiltro = space(1)
        this.N_GG_IN = 0
        this.N_GG_AV = 0
        * --- Read from PAR_AGEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PANOFLPR,PADATPRP,PADATPRS"+;
            " from "+i_cTable+" PAR_AGEN where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PANOFLPR,PADATPRP,PADATPRS;
            from (i_cTable) where;
                PACODAZI = i_codazi;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.TipoFiltro = NVL(cp_ToDate(_read_.PANOFLPR),cp_NullValue(_read_.PANOFLPR))
          this.N_GG_IN = NVL(cp_ToDate(_read_.PADATPRP),cp_NullValue(_read_.PADATPRP))
          this.N_GG_AV = NVL(cp_ToDate(_read_.PADATPRS),cp_NullValue(_read_.PADATPRS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !empty(this.TipoFiltro)
          do case
            case this.TipoFiltro="I"
              * --- Prima data vuota
              this.oParentObject.w_DATINIPRE = ctod("  -  -  ")
              this.oParentObject.w_DATFINPRE = i_datsys + this.N_GG_AV
            case this.TipoFiltro="F"
              * --- Seconda data vuota
              this.oParentObject.w_DATINIPRE = i_datsys - this.N_GG_IN
              this.oParentObject.w_DATFINPRE = ctod("  -  -  ")
            case this.TipoFiltro="E"
              * --- Entrambe date vuote
              this.oParentObject.w_DATINIPRE = ctod("  -  -  ")
              this.oParentObject.w_DATFINPRE = ctod("  -  -  ")
            otherwise
              * --- Intervallo / Data odierna
              this.oParentObject.w_DATINIPRE = i_datsys - this.N_GG_IN
              this.oParentObject.w_DATFINPRE = i_datsys + this.N_GG_AV
          endcase
        endif
    endcase
  endproc


  proc Init(oParentObject,PARAM)
    this.PARAM=PARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_AGEN'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="PARAM"
endproc
