* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bed                                                        *
*              Eliminazione documento da attivita                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-28                                                      *
* Last revis.: 2015-06-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bed",oParentObject,m.pParame)
return(i_retval)

define class tgsag_bed as StdBatch
  * --- Local variables
  pParame = space(1)
  w_MVSERIAL = space(10)
  w_MVALFDOC = space(10)
  w_MVALFEST = space(10)
  w_MVCAOVAL = 0
  w_MVCAUCON = space(5)
  w_MVCLADOC = space(2)
  w_MVCODAG2 = space(5)
  w_MVCODAGE = space(5)
  w_MVCODBA2 = space(15)
  w_MVCODBAN = space(10)
  w_MVCODCON = space(15)
  w_MVCODDES = space(5)
  w_MVCODESE = space(4)
  w_MVCODPAG = space(5)
  w_MVCODUTE = 0
  w_MVCODVAL = space(3)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATEST = ctod("  /  /  ")
  w_MVDATREG = ctod("  /  /  ")
  w_MVFLACCO = space(1)
  w_MVFLINTE = space(1)
  w_MVFLPROV = space(1)
  w_MVFLVEAC = space(1)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVNUMREG = 0
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVTCAMAG = space(5)
  w_MVTCOLIS = space(5)
  w_MVTCONTR = space(15)
  w_MVTFRAGG = space(1)
  w_MVTIPCON = space(1)
  w_MVTIPDOC = space(5)
  w_MVVALNAZ = space(3)
  w_MVANNDOC = space(4)
  w_MVANNPRO = space(4)
  w_MVSCOPAG = 0
  w_MVCODIVE = space(5)
  w_MVFLGIOM = space(1)
  w_MVFLCONT = space(1)
  w_MVSCONTI = 0
  w_MVFLFOSC = space(1)
  w_MVSPEINC = 0
  w_MVIVAINC = space(5)
  w_MVFLRINC = space(1)
  w_MVSPETRA = 0
  w_MVIVATRA = space(5)
  w_MVFLRTRA = space(1)
  w_MVSPEIMB = 0
  w_MVIVAIMB = space(5)
  w_MVFLRIMB = space(1)
  w_MVSPEBOL = 0
  w_MVIVABOL = space(5)
  w_MVIMPARR = 0
  w_MVACCONT = 0
  w_MVCODSPE = space(3)
  w_MVCODVET = space(5)
  w_MVCODVE2 = space(5)
  w_MVCODVE3 = space(5)
  w_MVCODPOR = space(1)
  w_MVCONCON = space(1)
  w_MVASPEST = space(30)
  w_MVNOTAGG = space(40)
  w_MVQTACOL = 0
  w_MVQTAPES = 0
  w_MVQTALOR = 0
  w_MVTIPORN = space(1)
  w_MVCODORN = space(15)
  w_MVORATRA = space(2)
  w_MVDATTRA = ctod("  /  /  ")
  w_MVMINTRA = space(2)
  w_MVGENEFF = space(1)
  w_MVGENPRO = space(1)
  w_MVACIVA1 = space(5)
  w_MVACIVA2 = space(5)
  w_MVACIVA3 = space(5)
  w_MVACIVA4 = space(5)
  w_MVACIVA5 = space(5)
  w_MVACIVA6 = space(5)
  w_MVAIMPN1 = 0
  w_MVAIMPN2 = 0
  w_MVAIMPN3 = 0
  w_MVAIMPN4 = 0
  w_MVAIMPN5 = 0
  w_MVAIMPN6 = 0
  w_MVAIMPS1 = 0
  w_MVAIMPS2 = 0
  w_MVAIMPS3 = 0
  w_MVAIMPS4 = 0
  w_MVAIMPS5 = 0
  w_MVAIMPS6 = 0
  w_MVAFLOM1 = space(1)
  w_MVAFLOM2 = space(1)
  w_MVAFLOM3 = space(1)
  w_MVAFLOM4 = space(1)
  w_MVAFLOM5 = space(1)
  w_MVAFLOM6 = space(1)
  w_MVPRD = space(2)
  w_MVRIFCON = space(10)
  w_UTCV = 0
  w_UTDV = ctod("  /  /  ")
  w_UTCC = 0
  w_UTDC = ctod("  /  /  ")
  w_MVPRP = space(2)
  w_MVTINCOM = ctod("  /  /  ")
  w_MVTFICOM = ctod("  /  /  ")
  w_MVRIFDIC = space(15)
  w_MVFLSCOR = space(1)
  w_MVDESDOC = space(50)
  w_MVTIPOPE = space(10)
  w_MVCATOPE = space(2)
  w_MVNUMCOR = space(25)
  w_MVIVAARR = space(5)
  w_MVACCPRE = 0
  w_MVMAXACC = 0
  w_MVACCSUC = 0
  w_MVTOTRIT = 0
  w_MVTOTENA = 0
  w_MVRIFPIA = space(10)
  w_MVRIFACC = space(10)
  w_MVRIFFAD = space(10)
  w_MVPERRET = 0
  w_MVANNRET = space(4)
  w_MVFLINTR = space(1)
  w_MVTRAINT = 0
  w_MV__NOTE = space(0)
  w_MVFLSALD = space(1)
  w_MVFLCAPA = space(1)
  w_MVFLSCAF = space(1)
  w_MVCODASP = space(3)
  w_MVFLVABD = space(1)
  w_MVFLSCOM = space(1)
  w_MVDATPLA = ctod("  /  /  ")
  w_MVTIPPER = space(1)
  w_MVRIFESP = space(10)
  w_MVMOVCOM = space(10)
  w_MVRIFODL = space(10)
  w_MVRIFDCO = space(10)
  w_MVFLBLOC = space(1)
  w_MVSERDDT = space(10)
  w_MVROWDDT = 0
  w_MVFLOFFE = space(1)
  w_MVCODSED = space(5)
  w_MVACCOLD = 0
  w_MVSERWEB = space(50)
  w_MVRITPRE = 0
  w_MVPERFIN = 0
  w_MVIMPFIN = 0
  w_MVFLSFIN = space(1)
  w_MVCAUIMB = 0
  w_MVIVACAU = space(5)
  w_MVRITATT = 0
  w_MVFLFOCA = space(1)
  w_MVTIPIMB = space(1)
  w_MVSEREST = space(30)
  w_MVFLSEND = space(1)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MVNUMRIF = 0
  w_MVCODICE = space(20)
  w_MVTIPRIG = space(1)
  w_MVDESART = space(40)
  w_MVCODART = space(20)
  w_MVUNIMIS = space(3)
  w_MVCATCON = space(5)
  w_MVDESSUP = space(0)
  w_MVCONIND = space(15)
  w_MVCAUMAG = space(5)
  w_MVCONTRO = space(15)
  w_MVCAUCOL = space(5)
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVCODCLA = space(3)
  w_MVCODLIS = space(5)
  w_MVQTAMOV = 0
  w_MVQTAUM1 = 0
  w_MVPREZZO = 0
  w_MVSCONT1 = 0
  w_MVSCONT2 = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_MVFLOMAG = space(1)
  w_MVCODIVA = space(5)
  w_MVVALRIG = 0
  w_MVIMPACC = 0
  w_MVCONTRA = space(15)
  w_MVVALMAG = 0
  w_MVIMPNAZ = 0
  w_MVIMPSCO = 0
  w_MVFLCASC = space(1)
  w_MVF2CASC = space(1)
  w_MVKEYSAL = space(20)
  w_MVF2ORDI = space(1)
  w_MVFLORDI = space(1)
  w_MVF2IMPE = space(1)
  w_MVFLIMPE = space(0)
  w_MVF2RISE = space(1)
  w_MVFLRISE = space(0)
  w_MVPERPRO = 0
  w_MVIMPPRO = 0
  w_MVPESNET = 0
  w_MVFLTRAS = space(1)
  w_MVNOMENC = space(8)
  w_MVUMSUPP = space(3)
  w_MVNAZPRO = space(3)
  w_MVMOLSUP = 0
  w_MVPROORD = space(2)
  w_MVNUMCOL = 0
  w_MVTIPCOL = space(20)
  w_MVCODCOM = space(15)
  w_MVQTAEVA = 0
  w_MVQTAEV1 = 0
  w_MVFLRAGG = space(1)
  w_MVIMPEVA = 0
  w_MVQTASAL = 0
  w_MVEFFEVA = ctod("  /  /  ")
  w_MVDATEVA = ctod("  /  /  ")
  w_MVFLELGM = space(1)
  w_MVFLEVAS = space(1)
  w_MVROWRIF = 0
  w_MVSERRIF = space(10)
  w_MVQTANOC = 0
  w_MVFLARIF = space(1)
  w_MVFLRESC = space(1)
  w_MVFLERIF = space(1)
  w_MVTIPATT = space(1)
  w_MVQTARES = 0
  w_MVCODCOS = space(5)
  w_MVFLRIAP = space(1)
  w_MVPRECON = 0
  w_MVFLORCO = space(1)
  w_MVFLCOCO = space(1)
  w_MVFLULPV = space(1)
  w_MVFLULCA = space(1)
  w_MVIMPCOM = 0
  w_MVVALULT = 0
  w_MVINICOM = ctod("  /  /  ")
  w_MVDATGEN = ctod("  /  /  ")
  w_MVFINCOM = ctod("  /  /  ")
  w_MVRIFORD = space(10)
  w_MVCODATT = space(15)
  w_MVCODCEN = space(15)
  w_MVCODODL = space(15)
  w_MVVOCCEN = space(15)
  w_MVCODCOL = space(5)
  w_MVFLRIPA = space(1)
  w_MVIMPRBA = 0
  w_MVFLELAN = space(1)
  w_MVRIFESC = space(10)
  w_MVRIGMAT = 0
  w_MVF2LOTT = space(1)
  w_MVFLLOTT = space(1)
  w_MVCODLOT = space(20)
  w_MVCODUB2 = space(20)
  w_MV_FLAGG = space(1)
  w_MVFLRVCL = space(1)
  w_MVCODUBI = space(20)
  w_MVQTAIM1 = 0
  w_MVTIPPRO = space(2)
  w_MV_SEGNO = space(1)
  w_MVCODCES = space(20)
  w_MVCESSER = space(10)
  w_MVQTAIMP = 0
  w_MVIMPAC2 = 0
  w_MVUNILOG = space(18)
  w_MVPROCAP = 0
  w_MVIMPCAP = 0
  w_MVTIPPR2 = space(2)
  w_MVFLNOAN = space(1)
  w_MVMC_PER = space(3)
  w_MVDATOAI = ctod("  /  /  ")
  w_MVDATOAF = ctod("  /  /  ")
  w_MVPAEFOR = space(3)
  w_MVAIRPOR = space(10)
  w_MVRIFEDI = space(14)
  w_MVSERIAL = space(10)
  w_TDFLINTE = space(1)
  w_CODEVE = space(10)
  w_RETVAL = space(254)
  w_GSAG_MDD = .NULL.
  w_SERDOC = space(10)
  w_TIPDOC = 0
  w_RETVAL = space(254)
  w_SERDOC = space(10)
  w_GENPRE = space(1)
  w_NUMDOC = 0
  w_NUMRIS = 0
  w_CAUNOT = space(5)
  w_CODSES = space(15)
  w_FLRESTO = .f.
  w_PARDOC = space(1)
  w_RETVAL = space(254)
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  OFF_ATTI_idx=0
  DOC_RATE_idx=0
  TIP_DOCU_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  CDC_MANU_idx=0
  MOVICOST_idx=0
  OFF_NOMI_idx=0
  ATT_DCOL_idx=0
  RIS_ESTR_idx=0
  PAR_PRAT_idx=0
  ASS_ATEV_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione Automatica Documento Interno da Attivit� (GSAG_AAT)
    *     pParame='1' - Record Deleted
    *     pParame='2' - ATSTATUS Change
    *     pParame='3' - Genera
    * --- Definizione Variabili
    * --- Gestite nella routine GSIM_BMM in maniera diversa tra AHE e AHR
    * --- Non gestite nella routine GSIM_BMM
    do case
      case this.pParame="1" and (lower(this.oParentObject.cFunction)<>"load" or this.oParentObject.w_ATSTATUS<>"P")
        if g_AGFA="S" and Lower(this.oParentObject.cFunction)= "query"
          * --- Read from ASS_ATEV
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ASS_ATEV_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATEV_idx,2],.t.,this.ASS_ATEV_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AECODEVE"+;
              " from "+i_cTable+" ASS_ATEV where ";
                  +"AECODATT = "+cp_ToStrODBC(this.oParentObject.w_ATSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AECODEVE;
              from (i_cTable) where;
                  AECODATT = this.oParentObject.w_ATSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODEVE = NVL(cp_ToDate(_read_.AECODEVE),cp_NullValue(_read_.AECODEVE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows <>0
            if !Ah_yesno("Attenzione! Attivit� associata all'evento %1. Proseguire con la cancellazione?","",Alltrim(this.w_CODEVE))
              this.w_RETVAL = Ah_msgformat("Transazione abbandonata")
            endif
          endif
        endif
        if this.oParentObject.w_OKGEN and empty(this.w_RETVAL)
          this.w_GSAG_MDD = this.oParentObject.GSAG_MDD
          this.w_GSAG_MDD.LinkPCClick(.t.)     
          * --- Nuovo Oggetto
          this.w_GSAG_MDD = this.oParentObject.GSAG_MDD.CNT
          this.w_GSAG_MDD.Firstrow()     
          * --- Eseguo simulazione cancellazione documenti
          do while Not this.w_GSAG_MDD.Eof_Trs()
            this.w_GSAG_MDD.SetRow()     
            if this.w_GSAG_MDD.FullRow()
              this.w_SERDOC = NVL( this.w_GSAG_MDD. Get( "ADSERDOC" ) ," ")
              this.w_TIPDOC = NVL( this.w_GSAG_MDD. Get( "t_ADTIPDOC" ) ," ")
              if this.w_TIPDOC=1 or this.w_TIPDOC=2
                this.w_RETVAL = gsar_bdk(.null.,this.w_SERDOC,.t.)
                if !empty(this.w_RETVAL)
                  exit
                endif
              endif
            endif
            this.w_GSAG_MDD.NextRow()     
          enddo
          if Empty(this.w_RETVAL) and Isalt()
            * --- Select from gsag1bdm
            do vq_exec with 'gsag1bdm',this,'_Curs_gsag1bdm','',.f.,.t.
            if used('_Curs_gsag1bdm')
              select _Curs_gsag1bdm
              locate for 1=1
              do while not(eof())
              this.w_RETVAL = Ah_msgformat("Esistono documenti associati alle prestazioni")
                select _Curs_gsag1bdm
                continue
              enddo
              use
            endif
          endif
        endif
        if Empty(this.w_RETVAL) and Isalt()
          gsal_baw(this,this.oParentObject.w_ATSERIAL)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Not empty(this.w_RETVAL)
          this.oParentObject.w_OKGEN = .F.
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_RETVAL
        endif
      case this.pParame="2"
        if this.oParentObject.w_ATSTATUS=="P"
          this.w_TDFLINTE = SPACE(1)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLINTE"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLINTE;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_CAUDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TDFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NVL(this.w_TDFLINTE, "N")<>"N" AND EMPTY(NVL(this.oParentObject.w_ATCODNOM, " "))
            ah_ErrorMsg("Impossibile impostare lo stato attivit� a completata senza specificare il codice nominativo associato")
            this.oParentObject.w_ATSTATUS = this.oParentObject.o_ATSTATUS
            i_retcode = 'stop'
            return
          endif
        endif
      case this.pParame="3"
        if this.oParentObject.w_OKGEN
          * --- Elimino righe documenti generati
          if lower(this.oParentObject.cFunction) <> "query"
            * --- Verifico esistenza dettaglio ripartizione
            this.w_GENPRE = this.oParentObject.gsag_mda.w_GENPRE
            if this.w_GENPRE="S"
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if Empty(this.w_RETVAL)
                this.w_RETVAL = GSAG_BGE(.null.,this.oParentObject.w_ATSERIAL,this.w_SERDOC," ",0,.F.,"D", .T.)
              endif
            else
              if Isalt()
                * --- Read from RIS_ESTR
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "SRCODSES"+;
                    " from "+i_cTable+" RIS_ESTR where ";
                        +"SRCODPRA = "+cp_ToStrODBC(this.oParentObject.w_ATCODPRA);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    SRCODSES;
                    from (i_cTable) where;
                        SRCODPRA = this.oParentObject.w_ATCODPRA;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CODSES = NVL(cp_ToDate(_read_.SRCODSES),cp_NullValue(_read_.SRCODSES))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_NUMRIS = iif(i_rows=0,1,i_rows)
                * --- Select from gsag_bed
                do vq_exec with 'gsag_bed',this,'_Curs_gsag_bed','',.f.,.t.
                if used('_Curs_gsag_bed')
                  select _Curs_gsag_bed
                  locate for 1=1
                  do while not(eof())
                  this.w_NUMDOC = _Curs_gsag_bed.NumDoc
                    select _Curs_gsag_bed
                    continue
                  enddo
                  use
                endif
              endif
              if (this.w_NUMDOC>0 and this.w_NUMDOC<>this.w_NUMRIS) or (!Isalt() and this.oParentObject.w_ATSTATUS="P")
                * --- il numero delle parti � diverso dal numero dei documenti
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.w_NUMDOC = 0
              endif
              do case
                case this.w_NUMDOC=0 and Empty(this.w_CODSES) and this.oParentObject.w_ATSTATUS="P"
                  * --- Non ho documenti da cancellare devo direttamente generare i nuovi documenti
                  this.w_SERDOC = Space(10)
                  this.Page_2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.w_RETVAL = GSAG_BGE(.null.,this.oParentObject.w_ATSERIAL,this.w_SERDOC," ",0,.F.,"D", .F.)
                  if Isalt()
                    * --- Read from PAR_PRAT
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.PAR_PRAT_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PRAT_idx,2],.t.,this.PAR_PRAT_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "PACAUNOT"+;
                        " from "+i_cTable+" PAR_PRAT where ";
                            +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        PACAUNOT;
                        from (i_cTable) where;
                            PACODAZI = i_codazi;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_CAUNOT = NVL(cp_ToDate(_read_.PACAUNOT),cp_NullValue(_read_.PACAUNOT))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    if Not Empty(this.w_CAUNOT)
                      this.Page_3()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    endif
                  endif
                case this.oParentObject.w_ATSTATUS <>"P" 
                  * --- Sto cambiando status da confermato devo eliminare i documenti
                  this.Page_2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                case Isalt() and this.oParentObject.w_ATSTATUS="P" and Not Empty(this.oParentObject.w_ATCODPRA) and Not Empty(this.w_CODSES) 
                  * --- Ho ripartizione 
                  * --- Devo eliminare i documenti per rigenerarli con i dati della ripartizione
                  if this.w_NUMDOC>0
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  if Empty(this.w_RETVAL) 
                    * --- Try
                    local bErr_02EAB780
                    bErr_02EAB780=bTrsErr
                    this.Try_02EAB780()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      * --- rollback
                      bTrsErr=.t.
                      cp_EndTrs(.t.)
                    endif
                    bTrsErr=bTrsErr or bErr_02EAB780
                    * --- End
                  endif
                otherwise
                  * --- Verifico esistenza documenti gi� generati nel dettaglio documenti collegati
                  if this.oParentObject.w_ATSTATUS="P"
                    * --- Select from GSAG1BED
                    do vq_exec with 'GSAG1BED',this,'_Curs_GSAG1BED','',.f.,.t.
                    if used('_Curs_GSAG1BED')
                      select _Curs_GSAG1BED
                      locate for 1=1
                      do while not(eof())
                      this.w_SERDOC = Nvl(_Curs_GSAG1BED.ADSERDOC," ")
                      this.w_PARDOC = IIF(Nvl(_Curs_GSAG1BED.PRTIPCAU," ") $ "L-N","N","D")
                      if _Curs_GSAG1BED.ADTIPDOC="V"
                        * --- Genero contemporaneamente documento vendite \acquisti
                        this.w_RETVAL = GSAG_BGE(.null.,this.oParentObject.w_ATSERIAL,this.w_SERDOC," ",0,.F.,this.w_PARDOC, .F.)
                      else
                        * --- Elimino documento acquisti
                        * --- Delete from ATT_DCOL
                        i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2])
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        if i_nConn<>0
                          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                                +"ADSERIAL = "+cp_ToStrODBC(this.oParentObject.w_ATSERIAL);
                                +" and ADSERDOC = "+cp_ToStrODBC(this.w_SERDOC);
                                 )
                        else
                          delete from (i_cTable) where;
                                ADSERIAL = this.oParentObject.w_ATSERIAL;
                                and ADSERDOC = this.w_SERDOC;

                          i_Rows=_tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          * --- Error: delete not accepted
                          i_Error=MSG_DELETE_ERROR
                          return
                        endif
                        this.w_RETVAL = gsar_bdk(.null.,this.w_SERDOC)
                      endif
                        select _Curs_GSAG1BED
                        continue
                      enddo
                      use
                    endif
                  endif
              endcase
            endif
            if Not Empty(this.w_RETVAL)
              if Isalt()
                ah_ErrorMsg("Impossibile completare l'attivit�: %0%1. ","","",Alltrim(this.w_RETVAL))
              else
                ah_ErrorMsg("Attenzione � stato rilevato un errore in fase di generazione movimenti %1%0L'attivit� non pu� essere completata.","","",Alltrim(this.w_RETVAL))
              endif
              if EMPTY(this.oParentObject.w_OLDSTATUS) OR this.oParentObject.w_OLDSTATUS="P"
                this.oParentObject.w_OLDSTATUS = "D"
              endif
              * --- Write into OFF_ATTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ATSTATUS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLDSTATUS),'OFF_ATTI','ATSTATUS');
                    +i_ccchkf ;
                +" where ";
                    +"ATSERIAL = "+cp_ToStrODBC(this.oParentObject.w_ATSERIAL);
                       )
              else
                update (i_cTable) set;
                    ATSTATUS = this.oParentObject.w_OLDSTATUS;
                    &i_ccchkf. ;
                 where;
                    ATSERIAL = this.oParentObject.w_ATSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          else
            * --- Eseguo cancellazione effettiva
            this.w_GSAG_MDD = this.oParentObject.GSAG_MDD
            this.w_GSAG_MDD.LinkPCClick(.t.)     
            * --- Nuovo Oggetto
            this.w_GSAG_MDD = this.oParentObject.GSAG_MDD.CNT
            this.w_GSAG_MDD.Firstrow()     
            do while Not this.w_GSAG_MDD.Eof_Trs()
              this.w_GSAG_MDD.SetRow()     
              if this.w_GSAG_MDD.FullRow()
                this.w_SERDOC = NVL( this.w_GSAG_MDD. Get( "ADSERDOC" ) ," ")
                this.w_TIPDOC = NVL( this.w_GSAG_MDD. Get( "t_ADTIPDOC" ) ," ")
                if this.w_TIPDOC=1 or this.w_TIPDOC=2
                  this.w_RETVAL = gsar_bdk(.null.,this.w_SERDOC)
                  if !empty(this.w_RETVAL)
                    exit
                  endif
                endif
              endif
              this.w_GSAG_MDD.NextRow()     
            enddo
            if Isalt() and empty(this.w_RETVAL)
              * --- elimino prestazioni
              this.w_RETVAL = gsag_bdm(This,this.oParentObject.w_ATSERIAL)
            endif
          endif
        endif
    endcase
  endproc
  proc Try_02EAB780()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_FLRESTO = .t.
    * --- Select from RIS_ESTR
    i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SRCODPRA,SRCODSES,SRPARAME  from "+i_cTable+" RIS_ESTR ";
          +" where SRCODPRA="+cp_ToStrODBC(this.oParentObject.w_ATCODPRA)+"";
          +" order by SRPARAME Desc";
           ,"_Curs_RIS_ESTR")
    else
      select SRCODPRA,SRCODSES,SRPARAME from (i_cTable);
       where SRCODPRA=this.oParentObject.w_ATCODPRA;
       order by SRPARAME Desc;
        into cursor _Curs_RIS_ESTR
    endif
    if used('_Curs_RIS_ESTR')
      select _Curs_RIS_ESTR
      locate for 1=1
      do while not(eof())
      this.w_RETVAL = GSAG_BGE(.null.,this.oParentObject.w_ATSERIAL,space(10),_Curs_RIS_ESTR.SRCODSES,_Curs_RIS_ESTR.SRPARAME,this.w_FLRESTO,"R", .F.)
      this.w_FLRESTO = .f.
      if Not Empty(this.w_RETVAL)
        exit
      endif
        select _Curs_RIS_ESTR
        continue
      enddo
      use
    endif
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Not Empty(this.w_RETVAL)
      * --- Raise
      i_Error="Errore"
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_04373E58
    bErr_04373E58=bTrsErr
    this.Try_04373E58()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_04373E58
    * --- End
  endproc
  proc Try_04373E58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from ATT_DCOL
    i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2],.t.,this.ATT_DCOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ADSERDOC,ADTIPDOC,ADSERIAL  from "+i_cTable+" ATT_DCOL ";
          +" where ADSERIAL="+cp_ToStrODBC(this.oParentObject.w_ATSERIAL)+" and ADTIPDOC<>'D'";
           ,"_Curs_ATT_DCOL")
    else
      select ADSERDOC,ADTIPDOC,ADSERIAL from (i_cTable);
       where ADSERIAL=this.oParentObject.w_ATSERIAL and ADTIPDOC<>"D";
        into cursor _Curs_ATT_DCOL
    endif
    if used('_Curs_ATT_DCOL')
      select _Curs_ATT_DCOL
      locate for 1=1
      do while not(eof())
      this.w_SERDOC = Nvl(_Curs_ATT_DCOL.ADSERDOC," ")
      * --- Elimina Documento
      this.w_RETVAL = gsar_bdk(.null.,this.w_SERDOC)
      if Empty(this.w_RETVAL)
        * --- Cancello riga relativa nella tabella documenti associati
        * --- Delete from ATT_DCOL
        i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"ADSERIAL = "+cp_ToStrODBC(this.oParentObject.w_ATSERIAL);
                +" and ADSERDOC = "+cp_ToStrODBC(this.w_SERDOC);
                 )
        else
          delete from (i_cTable) where;
                ADSERIAL = this.oParentObject.w_ATSERIAL;
                and ADSERDOC = this.w_SERDOC;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        * --- Raise
        i_Error=this.w_RETVAL
        return
      endif
        select _Curs_ATT_DCOL
        continue
      enddo
      use
    endif
    if Empty(this.w_RETVAL) and Not Empty(this.oParentObject.w_ATRIFMOV)
      this.w_RETVAL = gsag_bga(This,this.oParentObject.w_ATSERIAL,this.oParentObject.w_ATRIFMOV,"D")
      if Not Empty(this.w_RETVAL)
        * --- Raise
        i_Error=this.w_RETVAL
        return
      else
        this.oParentObject.w_ATRIFMOV = Space(10)
      endif
    endif
    if Isalt()
      * --- elimino prestazioni
      this.w_RETVAL = gsag_bdm(This,this.oParentObject.w_ATSERIAL)
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genero documento pre nota spese
    if Empty(this.w_RETVAL)
      this.w_SERDOC = Space(10)
      this.w_RETVAL = GSAG_BGE(.null.,this.oParentObject.w_ATSERIAL,this.w_SERDOC," ",0,.F.,"N", .F.)
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,14)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='OFF_ATTI'
    this.cWorkTables[4]='DOC_RATE'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='CDC_MANU'
    this.cWorkTables[9]='MOVICOST'
    this.cWorkTables[10]='OFF_NOMI'
    this.cWorkTables[11]='ATT_DCOL'
    this.cWorkTables[12]='RIS_ESTR'
    this.cWorkTables[13]='PAR_PRAT'
    this.cWorkTables[14]='ASS_ATEV'
    return(this.OpenAllTables(14))

  proc CloseCursors()
    if used('_Curs_gsag1bdm')
      use in _Curs_gsag1bdm
    endif
    if used('_Curs_gsag_bed')
      use in _Curs_gsag_bed
    endif
    if used('_Curs_RIS_ESTR')
      use in _Curs_RIS_ESTR
    endif
    if used('_Curs_GSAG1BED')
      use in _Curs_GSAG1BED
    endif
    if used('_Curs_ATT_DCOL')
      use in _Curs_ATT_DCOL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
