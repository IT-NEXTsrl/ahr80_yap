* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_kte                                                        *
*              Inserimento telefonate e prestazioni                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-27                                                      *
* Last revis.: 2014-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsfa_kte",oParentObject))

* --- Class definition
define class tgsfa_kte as StdForm
  Top    = 4
  Left   = 4

  * --- Standard Properties
  Width  = 791
  Height = 545
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-29"
  HelpContextID=82477673
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=176

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  TIPEVENT_IDX = 0
  DRVEVENT_IDX = 0
  OFF_NOMI_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  CAN_TIER_IDX = 0
  KEY_ARTI_IDX = 0
  TIP_DOCU_IDX = 0
  CAUMATTI_IDX = 0
  PAR_AGEN_IDX = 0
  CONTI_IDX = 0
  BUSIUNIT_IDX = 0
  cPrg = "gsfa_kte"
  cComment = "Inserimento telefonate e prestazioni "
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READAZI = space(5)
  w_ZoomName = space(20)
  w_LeggiCausale = space(10)
  w_TETIPEVE = space(10)
  o_TETIPEVE = space(10)
  w_DATA_INI = ctot('')
  w_TIPEVE1 = space(10)
  w_DATA_FIN = ctot('')
  w_CODNOM = space(15)
  w_ATTIPCLI = space(1)
  w_TIPDIR1 = space(10)
  w_EVPERSON = space(5)
  w_EVGRUPPO = space(5)
  w_PR__DATA = ctod('  /  /  ')
  w_DE__TIPO = space(1)
  w_TEDESCRI = space(50)
  w_TIPPER = space(1)
  w_TIPGRU = space(1)
  w_EVSERIAL = space(10)
  o_EVSERIAL = space(10)
  w_EV__ANNO = space(4)
  w_ZCODPRA = space(15)
  o_ZCODPRA = space(15)
  w_SELEZIONE = 0
  o_SELEZIONE = 0
  w_STATO = space(1)
  w_NOMINA = space(15)
  w_RIFPER = space(254)
  w_CODPRA = space(15)
  o_CODPRA = space(15)
  w_EDATINI = ctot('')
  o_EDATINI = ctot('')
  w_EDATFIN = ctot('')
  o_EDATFIN = ctot('')
  w_MINEFF = space(2)
  w_ORAEFF = ctot('')
  w_CodPart = space(5)
  o_CodPart = space(5)
  w_CAUACQ = space(5)
  w_SELTIME = space(10)
  o_SELTIME = space(10)
  w_SerUtente = space(41)
  o_SerUtente = space(41)
  w_CodPratica = space(15)
  w_CODSER = space(20)
  o_CODSER = space(20)
  w_CACODART = space(20)
  o_CACODART = space(20)
  w_DAVOCCOS = space(15)
  w_DAVOCRIC = space(15)
  w_EVNOMINA = space(15)
  w_DesAgg = space(0)
  w_RTIPRIG = space(1)
  o_RTIPRIG = space(1)
  w_ETIPRIG = space(1)
  o_ETIPRIG = space(1)
  w_ATIPRIG = space(1)
  o_ATIPRIG = space(1)
  w_ATIPRI2 = space(1)
  o_ATIPRI2 = space(1)
  w_ETIPRI2 = space(1)
  o_ETIPRI2 = space(1)
  w_RTIPRI2 = space(1)
  o_RTIPRI2 = space(1)
  w_DesPratica = space(100)
  w_DurataOre = 0
  o_DurataOre = 0
  w_DurataMin = 0
  o_DurataMin = 0
  w_DataIniTimer = ctot('')
  w_ENTE = space(10)
  w_COMODO = space(30)
  w_PrimaUniMis = space(10)
  w_UniMisTempo = space(10)
  o_UniMisTempo = space(10)
  w_ConversioneOre = 0
  w_QtaTimerOre = 0
  w_QtaTimerMinuti = 0
  w_QtaTimer = 0
  w_OBTEST = ctod('  /  /  ')
  w_TIPOENTE = space(1)
  w_NOEDES = space(30)
  w_DataFinTimer = ctot('')
  w_DATIPRIG = space(10)
  w_DACENRIC = space(15)
  w_DACOMRIC = space(15)
  w_DAATTRIC = space(15)
  w_DATIPRI2 = space(10)
  w_DACENCOS = space(15)
  w_DACODCOM = space(15)
  o_DACODCOM = space(15)
  w_DAATTIVI = space(15)
  w_DA_SEGNO = space(1)
  w_DAINICOM = ctod('  /  /  ')
  w_DAFINCOM = ctod('  /  /  ')
  w_FLGCOM = space(1)
  w_FLDANA = space(1)
  w_CAUDOC = space(5)
  o_CAUDOC = space(5)
  w_FLANAL = space(1)
  w_VOCECR = space(1)
  w_DASCONT1 = 0
  w_DASCONT2 = 0
  w_DASCONT3 = 0
  w_DASCONT4 = 0
  w_DATALIST = ctod('  /  /  ')
  w_DASCOLIS = space(5)
  w_DALISACQ = space(5)
  w_DAPROLIS = space(5)
  w_DAPROSCO = space(5)
  w_DACODNOM = space(15)
  w_PR__DATA = ctod('  /  /  ')
  w_COD1ATT = space(20)
  w_BUFLANAL = space(1)
  w_FLACOMAQ = space(1)
  w_LISACQ = space(5)
  w_DACODLIS = space(5)
  w_DATCOINI = ctod('  /  /  ')
  w_DATCOFIN = ctod('  /  /  ')
  w_DATRIINI = ctod('  /  /  ')
  w_FLACQ = space(1)
  w_DATRIFIN = ctod('  /  /  ')
  w_FLSCOR = space(1)
  w_NOCODCLI = space(15)
  w_NOTIPCLI = space(1)
  w_CATCOM = space(5)
  w_DACONCOD = space(10)
  w_EVTIPEVE = space(10)
  w_CauDefault = space(10)
  w_TxtSel = space(10)
  w_FLNSAP = space(10)
  w_TIPCAU = space(1)
  w_DESNOM = space(60)
  w_CODDRI = space(10)
  w_TIPODRV = space(1)
  w_mEnableCtrl = .F.
  w_DACONTRA = space(10)
  w_DACODMOD = space(10)
  w_DACODIMP = space(10)
  w_CODCLI = space(15)
  w_DARINNOV = 0
  w_DACOCOMP = 0
  w_DASERIAL = space(20)
  w_OB_TEST = ctod('  /  /  ')
  w_DACODATT = space(20)
  o_DACODATT = space(20)
  w_DAUNIMIS = space(3)
  w_DAPREZZO = 0
  o_DAPREZZO = 0
  w_DACODICE = space(20)
  o_DACODICE = space(20)
  w_CAUATT = space(10)
  w_MVCODVAL = space(5)
  w_MVFLVEAC = space(1)
  w_FLGLIS = space(1)
  w_TIPSER = space(2)
  w_ATCENCOS = space(15)
  w_ATCENRIC = space(15)
  w_ATCODPRA = space(15)
  w_ATCOMRIC = space(15)
  w_ATATTCOS = space(15)
  w_ATATTRIC = space(15)
  w_ATCODNOM = space(15)
  w_RCACODART = space(20)
  w_ECOD1ATT = space(20)
  w_RCOD1ATT = space(20)
  w_ECACODART = space(20)
  w_CODICE = space(41)
  o_CODICE = space(41)
  w_EMOLTI3 = 0
  w_RMOLTI3 = 0
  w_EOPERA3 = space(1)
  w_ROPERA3 = space(1)
  w_EDESAGG = space(0)
  w_RDESAGG = space(0)
  w_EDESATT = space(40)
  w_RDESATT = space(40)
  w_RUNMIS3 = space(3)
  w_ECADTOBSO = ctod('  /  /  ')
  w_RCADTOBSO = ctod('  /  /  ')
  w_ETIPSER = space(1)
  w_RTIPSER = space(1)
  w_EUNMIS3 = space(3)
  w_ATCODBUN = space(3)
  w_ATCODLIS = space(5)
  w_DADESATT = space(40)
  w_DesSer = space(40)
  w_DESCAN = space(100)
  w_FLPRES = space(1)
  w_CHKDATAPRE = space(1)
  w_GG_PRE = 0
  w_GG_SUC = 0
  w_DATAPREC = ctod('  /  /  ')
  w_DATASUCC = ctod('  /  /  ')
  w_TipoRisorsa = space(1)
  w_DTIPRIG = space(1)
  w_CODPERS = space(5)
  w_DPCOGNOM = space(50)
  w_DPNOME = space(50)
  w_PARASS = space(3)
  w_FLGZER = space(1)
  w_TIPART = space(2)
  w_ARTIPRIG = space(1)
  w_EVENT = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsfa_ktePag1","gsfa_kte",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTETIPEVE_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsfa_kte
    if !isalt()
      local oEVENT
      oEVENT=this.Parent.GetCtrl("EVENT")
      oEVENT.cZoomfile="GSFARKTE"
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_EVENT = this.oPgFrm.Pages(1).oPag.EVENT
    DoDefault()
    proc Destroy()
      this.w_EVENT = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[13]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='TIPEVENT'
    this.cWorkTables[3]='DRVEVENT'
    this.cWorkTables[4]='OFF_NOMI'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='UNIMIS'
    this.cWorkTables[7]='CAN_TIER'
    this.cWorkTables[8]='KEY_ARTI'
    this.cWorkTables[9]='TIP_DOCU'
    this.cWorkTables[10]='CAUMATTI'
    this.cWorkTables[11]='PAR_AGEN'
    this.cWorkTables[12]='CONTI'
    this.cWorkTables[13]='BUSIUNIT'
    return(this.OpenAllTables(13))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READAZI=space(5)
      .w_ZoomName=space(20)
      .w_LeggiCausale=space(10)
      .w_TETIPEVE=space(10)
      .w_DATA_INI=ctot("")
      .w_TIPEVE1=space(10)
      .w_DATA_FIN=ctot("")
      .w_CODNOM=space(15)
      .w_ATTIPCLI=space(1)
      .w_TIPDIR1=space(10)
      .w_EVPERSON=space(5)
      .w_EVGRUPPO=space(5)
      .w_PR__DATA=ctod("  /  /  ")
      .w_DE__TIPO=space(1)
      .w_TEDESCRI=space(50)
      .w_TIPPER=space(1)
      .w_TIPGRU=space(1)
      .w_EVSERIAL=space(10)
      .w_EV__ANNO=space(4)
      .w_ZCODPRA=space(15)
      .w_SELEZIONE=0
      .w_STATO=space(1)
      .w_NOMINA=space(15)
      .w_RIFPER=space(254)
      .w_CODPRA=space(15)
      .w_EDATINI=ctot("")
      .w_EDATFIN=ctot("")
      .w_MINEFF=space(2)
      .w_ORAEFF=ctot("")
      .w_CodPart=space(5)
      .w_CAUACQ=space(5)
      .w_SELTIME=space(10)
      .w_SerUtente=space(41)
      .w_CodPratica=space(15)
      .w_CODSER=space(20)
      .w_CACODART=space(20)
      .w_DAVOCCOS=space(15)
      .w_DAVOCRIC=space(15)
      .w_EVNOMINA=space(15)
      .w_DesAgg=space(0)
      .w_RTIPRIG=space(1)
      .w_ETIPRIG=space(1)
      .w_ATIPRIG=space(1)
      .w_ATIPRI2=space(1)
      .w_ETIPRI2=space(1)
      .w_RTIPRI2=space(1)
      .w_DesPratica=space(100)
      .w_DurataOre=0
      .w_DurataMin=0
      .w_DataIniTimer=ctot("")
      .w_ENTE=space(10)
      .w_COMODO=space(30)
      .w_PrimaUniMis=space(10)
      .w_UniMisTempo=space(10)
      .w_ConversioneOre=0
      .w_QtaTimerOre=0
      .w_QtaTimerMinuti=0
      .w_QtaTimer=0
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPOENTE=space(1)
      .w_NOEDES=space(30)
      .w_DataFinTimer=ctot("")
      .w_DATIPRIG=space(10)
      .w_DACENRIC=space(15)
      .w_DACOMRIC=space(15)
      .w_DAATTRIC=space(15)
      .w_DATIPRI2=space(10)
      .w_DACENCOS=space(15)
      .w_DACODCOM=space(15)
      .w_DAATTIVI=space(15)
      .w_DA_SEGNO=space(1)
      .w_DAINICOM=ctod("  /  /  ")
      .w_DAFINCOM=ctod("  /  /  ")
      .w_FLGCOM=space(1)
      .w_FLDANA=space(1)
      .w_CAUDOC=space(5)
      .w_FLANAL=space(1)
      .w_VOCECR=space(1)
      .w_DASCONT1=0
      .w_DASCONT2=0
      .w_DASCONT3=0
      .w_DASCONT4=0
      .w_DATALIST=ctod("  /  /  ")
      .w_DASCOLIS=space(5)
      .w_DALISACQ=space(5)
      .w_DAPROLIS=space(5)
      .w_DAPROSCO=space(5)
      .w_DACODNOM=space(15)
      .w_PR__DATA=ctod("  /  /  ")
      .w_COD1ATT=space(20)
      .w_BUFLANAL=space(1)
      .w_FLACOMAQ=space(1)
      .w_LISACQ=space(5)
      .w_DACODLIS=space(5)
      .w_DATCOINI=ctod("  /  /  ")
      .w_DATCOFIN=ctod("  /  /  ")
      .w_DATRIINI=ctod("  /  /  ")
      .w_FLACQ=space(1)
      .w_DATRIFIN=ctod("  /  /  ")
      .w_FLSCOR=space(1)
      .w_NOCODCLI=space(15)
      .w_NOTIPCLI=space(1)
      .w_CATCOM=space(5)
      .w_DACONCOD=space(10)
      .w_EVTIPEVE=space(10)
      .w_CauDefault=space(10)
      .w_TxtSel=space(10)
      .w_FLNSAP=space(10)
      .w_TIPCAU=space(1)
      .w_DESNOM=space(60)
      .w_CODDRI=space(10)
      .w_TIPODRV=space(1)
      .w_mEnableCtrl=.f.
      .w_DACONTRA=space(10)
      .w_DACODMOD=space(10)
      .w_DACODIMP=space(10)
      .w_CODCLI=space(15)
      .w_DARINNOV=0
      .w_DACOCOMP=0
      .w_DASERIAL=space(20)
      .w_OB_TEST=ctod("  /  /  ")
      .w_DACODATT=space(20)
      .w_DAUNIMIS=space(3)
      .w_DAPREZZO=0
      .w_DACODICE=space(20)
      .w_CAUATT=space(10)
      .w_MVCODVAL=space(5)
      .w_MVFLVEAC=space(1)
      .w_FLGLIS=space(1)
      .w_TIPSER=space(2)
      .w_ATCENCOS=space(15)
      .w_ATCENRIC=space(15)
      .w_ATCODPRA=space(15)
      .w_ATCOMRIC=space(15)
      .w_ATATTCOS=space(15)
      .w_ATATTRIC=space(15)
      .w_ATCODNOM=space(15)
      .w_RCACODART=space(20)
      .w_ECOD1ATT=space(20)
      .w_RCOD1ATT=space(20)
      .w_ECACODART=space(20)
      .w_CODICE=space(41)
      .w_EMOLTI3=0
      .w_RMOLTI3=0
      .w_EOPERA3=space(1)
      .w_ROPERA3=space(1)
      .w_EDESAGG=space(0)
      .w_RDESAGG=space(0)
      .w_EDESATT=space(40)
      .w_RDESATT=space(40)
      .w_RUNMIS3=space(3)
      .w_ECADTOBSO=ctod("  /  /  ")
      .w_RCADTOBSO=ctod("  /  /  ")
      .w_ETIPSER=space(1)
      .w_RTIPSER=space(1)
      .w_EUNMIS3=space(3)
      .w_ATCODBUN=space(3)
      .w_ATCODLIS=space(5)
      .w_DADESATT=space(40)
      .w_DesSer=space(40)
      .w_DESCAN=space(100)
      .w_FLPRES=space(1)
      .w_CHKDATAPRE=space(1)
      .w_GG_PRE=0
      .w_GG_SUC=0
      .w_DATAPREC=ctod("  /  /  ")
      .w_DATASUCC=ctod("  /  /  ")
      .w_TipoRisorsa=space(1)
      .w_DTIPRIG=space(1)
      .w_CODPERS=space(5)
      .w_DPCOGNOM=space(50)
      .w_DPNOME=space(50)
      .w_PARASS=space(3)
      .w_FLGZER=space(1)
      .w_TIPART=space(2)
      .w_ARTIPRIG=space(1)
        .w_READAZI = i_CODAZI
        .w_ZoomName = iif(IsAlt(),'TarTempo','GSFA_KTE')
        .w_LeggiCausale = i_CodAzi
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_LeggiCausale))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_TETIPEVE))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_TIPEVE1 = 'D'
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_CODNOM))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_TIPDIR1 = ""
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_EVPERSON))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_EVGRUPPO))
          .link_1_12('Full')
        endif
      .oPgFrm.Page1.oPag.EVENT.Calculate()
        .w_PR__DATA = .w_event.GETVAR('EVDATFIN')
        .w_DE__TIPO = 'T'
          .DoRTCalc(15,15,.f.)
        .w_TIPPER = 'P'
        .w_TIPGRU = 'G'
        .w_EVSERIAL = .w_event.GETVAR('EVSERIAL')
        .w_EV__ANNO = .w_event.GETVAR('EV__ANNO')
        .w_ZCODPRA = .w_event.GETVAR('EVCODPRA')
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_ZCODPRA))
          .link_1_29('Full')
        endif
        .w_SELEZIONE = .w_event.GETVAR('XCHK')
        .w_STATO = .w_event.GETVAR('EV_STATO')
        .w_NOMINA = IIF(.w_SELEZIONE=1,.w_event.GETVAR('EVNOMINA'),'')
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_NOMINA))
          .link_1_32('Full')
        endif
        .w_RIFPER = .w_event.GETVAR('EVRIFPER')
        .w_CODPRA = .w_event.GETVAR('EVCODPRA')
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_CODPRA))
          .link_1_34('Full')
        endif
        .w_EDATINI = .w_event.GETVAR('EVDATINI')
        .w_EDATFIN = .w_event.GETVAR('EVDATFIN')
        .w_MINEFF = .w_event.GETVAR('EVMINEFF')
        .w_ORAEFF = .w_event.GETVAR('EVORAEFF')
        .w_CodPart = readdipend(i_CodUte, 'C')
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_CodPart))
          .link_1_40('Full')
        endif
        .DoRTCalc(31,34,.f.)
        if not(empty(.w_CodPratica))
          .link_1_44('Full')
        endif
        .w_CODSER = IIF(!ISAHE(), IIF(EMPTY(.w_CODSER), .w_SerUtente, .w_CODSER), SPACE(20))
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_CODSER))
          .link_1_45('Full')
        endif
        .w_CACODART = IIF(ISAHE(),.w_ECACODART,.w_RCACODART)
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_CACODART))
          .link_1_46('Full')
        endif
        .w_DAVOCCOS = SearchVoc(this,'C',.w_COD1ATT,.w_ATTIPCLI,.w_CODCLI,.w_CAUDOC,.w_ATCODNOM)
        .w_DAVOCRIC = SearchVoc(this,'R',.w_COD1ATT,.w_ATTIPCLI,.w_CODCLI,.w_CAUDOC,.w_ATCODNOM)
          .DoRTCalc(39,39,.f.)
        .w_DesAgg = IIF(ISAHE(), .w_EDESAGG, .w_RDESAGG)
        .w_RTIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_ETIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_ATIPRIG = ICASE(ISAHE(),.w_ETIPRIG,Isahr(),.w_RTIPRIG,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N', Not empty(.w_DATIPRIG),.w_DATIPRIG,.w_ARTIPRIG))
        .w_ATIPRI2 = 'D'
        .w_ETIPRI2 = 'E'
        .w_RTIPRI2 = 'D'
        .DoRTCalc(47,53,.f.)
        if not(empty(.w_PrimaUniMis))
          .link_1_74('Full')
        endif
          .DoRTCalc(54,55,.f.)
        .w_QtaTimerOre = IIF(.w_UniMisTempo='S',(.w_DurataOre+cp_ROUND(.w_DurataMin/60,2)),1)
        .w_QtaTimerMinuti = IIF(.w_UniMisTempo='S',.w_DurataOre*60+.w_DurataMin,1)
        .w_QtaTimer = IIF(.w_UniMisTempo='S',IIF( .w_ConversioneOre= 0.01667,.w_QtaTimerMinuti,.w_QtaTimerOre*.w_ConversioneOre),1)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(60,62,.f.)
        .w_DATIPRIG = ICASE(Isahe(),.w_ETIPRIG,Isahr(),.w_RTIPRIG,.w_ATIPRIG)
          .DoRTCalc(64,64,.f.)
        .w_DACOMRIC = .w_DACODCOM
          .DoRTCalc(66,66,.f.)
        .w_DATIPRI2 = ICASE(Isahe(),.w_ETIPRI2,Isahr(),.w_RTIPRI2,.w_ATIPRI2)
          .DoRTCalc(68,68,.f.)
        .w_DACODCOM = iif(((g_PERCAN='S' AND (.w_FLANAL='S' or .w_FLDANA='S' )) OR (g_COMM='S' AND .w_FLGCOM='S')),.w_ZCODPRA,'')
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_DACODCOM))
          .link_1_92('Full')
        endif
          .DoRTCalc(70,70,.f.)
        .w_DA_SEGNO = 'D'
          .DoRTCalc(72,73,.f.)
        .w_FLGCOM = Docgesana(.w_CAUDOC,'C')
        .w_FLDANA = docgesana(.w_CAUDOC,'A')
        .DoRTCalc(76,76,.f.)
        if not(empty(.w_CAUDOC))
          .link_1_99('Full')
        endif
          .DoRTCalc(77,77,.f.)
        .w_VOCECR = Docgesana(.w_CAUDOC,'V')
          .DoRTCalc(79,89,.f.)
        .w_COD1ATT = IIF(ISAHE(),.w_ECOD1ATT,.w_RCOD1ATT)
          .DoRTCalc(91,101,.f.)
        .w_NOTIPCLI = .w_ATTIPCLI
        .DoRTCalc(103,106,.f.)
        if not(empty(.w_CauDefault))
          .link_1_129('Full')
        endif
          .DoRTCalc(107,108,.f.)
        .w_TIPCAU = 'T'
        .DoRTCalc(110,111,.f.)
        if not(empty(.w_CODDRI))
          .link_1_136('Full')
        endif
        .DoRTCalc(112,117,.f.)
        if not(empty(.w_CODCLI))
          .link_1_144('Full')
        endif
          .DoRTCalc(118,120,.f.)
        .w_OB_TEST = i_INIDAT
          .DoRTCalc(122,126,.f.)
        .w_MVCODVAL = g_PERVAL
          .DoRTCalc(128,132,.f.)
        .w_ATCODPRA = IIF(ISALT(),.w_CODPRA,.w_DACODCOM)
        .w_ATCOMRIC = .w_DACODCOM
        .w_ATATTCOS = .w_DAATTIVI
        .w_ATATTRIC = .w_DAATTRIC
          .DoRTCalc(137,141,.f.)
        .w_CODICE = IIF(ISAHE(),IIF(EMPTY(.w_CODICE), .w_SerUtente, .w_CODICE), SPACE(41))
        .DoRTCalc(142,142,.f.)
        if not(empty(.w_CODICE))
          .link_1_170('Full')
        endif
          .DoRTCalc(143,156,.f.)
        .w_ATCODBUN = g_CODBUN
        .DoRTCalc(157,157,.f.)
        if not(empty(.w_ATCODBUN))
          .link_1_185('Full')
        endif
          .DoRTCalc(158,158,.f.)
        .w_DADESATT = IIF(ISAHE(),.w_EDESATT,.w_RDESATT)
        .w_DesSer = IIF(ISAHE(),.w_EDESATT,.w_RDESATT)
          .DoRTCalc(161,165,.f.)
        .w_DATAPREC = Date() - .w_GG_PRE
        .w_DATASUCC = Date() + .w_GG_SUC
        .w_TipoRisorsa = 'P'
          .DoRTCalc(169,169,.f.)
        .w_CODPERS = ReadResp(i_codute, "C", .w_CODPRATICA)
        .DoRTCalc(170,170,.f.)
        if not(empty(.w_CODPERS))
          .link_1_198('Full')
        endif
    endwith
    this.DoRTCalc(171,176,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_3('Full')
        .oPgFrm.Page1.oPag.EVENT.Calculate()
        .DoRTCalc(4,12,.t.)
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_PR__DATA = .w_event.GETVAR('EVDATFIN')
        endif
        .DoRTCalc(14,17,.t.)
            .w_EVSERIAL = .w_event.GETVAR('EVSERIAL')
            .w_EV__ANNO = .w_event.GETVAR('EV__ANNO')
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_ZCODPRA = .w_event.GETVAR('EVCODPRA')
          .link_1_29('Full')
        endif
            .w_SELEZIONE = .w_event.GETVAR('XCHK')
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_STATO = .w_event.GETVAR('EV_STATO')
        endif
        if .o_SELEZIONE<>.w_SELEZIONE.or. .o_EVSERIAL<>.w_EVSERIAL
            .w_NOMINA = IIF(.w_SELEZIONE=1,.w_event.GETVAR('EVNOMINA'),'')
          .link_1_32('Full')
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_RIFPER = .w_event.GETVAR('EVRIFPER')
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_CODPRA = .w_event.GETVAR('EVCODPRA')
          .link_1_34('Full')
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_EDATINI = .w_event.GETVAR('EVDATINI')
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_EDATFIN = .w_event.GETVAR('EVDATFIN')
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_MINEFF = .w_event.GETVAR('EVMINEFF')
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
            .w_ORAEFF = .w_event.GETVAR('EVORAEFF')
        endif
          .link_1_40('Full')
        .DoRTCalc(31,33,.t.)
        if .o_ZCODPRA<>.w_ZCODPRA
          .link_1_44('Full')
        endif
        if .o_SerUtente<>.w_SerUtente
            .w_CODSER = IIF(!ISAHE(), IIF(EMPTY(.w_CODSER), .w_SerUtente, .w_CODSER), SPACE(20))
          .link_1_45('Full')
        endif
        if .o_CODSER<>.w_CODSER.or. .o_CACODART<>.w_CACODART
            .w_CACODART = IIF(ISAHE(),.w_ECACODART,.w_RCACODART)
          .link_1_46('Full')
        endif
            .w_DAVOCCOS = SearchVoc(this,'C',.w_COD1ATT,.w_ATTIPCLI,.w_CODCLI,.w_CAUDOC,.w_ATCODNOM)
            .w_DAVOCRIC = SearchVoc(this,'R',.w_COD1ATT,.w_ATTIPCLI,.w_CODCLI,.w_CAUDOC,.w_ATCODNOM)
        .DoRTCalc(39,39,.t.)
        if .o_CODICE<>.w_CODICE.or. .o_CODSER<>.w_CODSER
            .w_DesAgg = IIF(ISAHE(), .w_EDESAGG, .w_RDESAGG)
        endif
        .DoRTCalc(41,52,.t.)
        if .o_CODSER<>.w_CODSER
          .link_1_74('Full')
        endif
        .DoRTCalc(54,55,.t.)
        if .o_DurataOre<>.w_DurataOre.or. .o_DurataMin<>.w_DurataMin.or. .o_UniMisTempo<>.w_UniMisTempo.or. .o_CODSER<>.w_CODSER
            .w_QtaTimerOre = IIF(.w_UniMisTempo='S',(.w_DurataOre+cp_ROUND(.w_DurataMin/60,2)),1)
        endif
        if .o_DurataOre<>.w_DurataOre.or. .o_DurataMin<>.w_DurataMin.or. .o_UniMisTempo<>.w_UniMisTempo.or. .o_CODSER<>.w_CODSER
            .w_QtaTimerMinuti = IIF(.w_UniMisTempo='S',.w_DurataOre*60+.w_DurataMin,1)
        endif
        if .o_DurataOre<>.w_DurataOre.or. .o_DurataMin<>.w_DurataMin.or. .o_UniMisTempo<>.w_UniMisTempo.or. .o_CODSER<>.w_CODSER
            .w_QtaTimer = IIF(.w_UniMisTempo='S',IIF( .w_ConversioneOre= 0.01667,.w_QtaTimerMinuti,.w_QtaTimerOre*.w_ConversioneOre),1)
        endif
        .DoRTCalc(59,62,.t.)
        if .o_ATIPRIG<>.w_ATIPRIG.or. .o_RTIPRIG<>.w_RTIPRIG.or. .o_ETIPRIG<>.w_ETIPRIG
            .w_DATIPRIG = ICASE(Isahe(),.w_ETIPRIG,Isahr(),.w_RTIPRIG,.w_ATIPRIG)
        endif
        .DoRTCalc(64,64,.t.)
        if .o_DACODCOM<>.w_DACODCOM
            .w_DACOMRIC = .w_DACODCOM
        endif
        .DoRTCalc(66,66,.t.)
        if .o_ATIPRI2<>.w_ATIPRI2.or. .o_RTIPRI2<>.w_RTIPRI2.or. .o_ETIPRI2<>.w_ETIPRI2
            .w_DATIPRI2 = ICASE(Isahe(),.w_ETIPRI2,Isahr(),.w_RTIPRI2,.w_ATIPRI2)
        endif
        .DoRTCalc(68,68,.t.)
        if .o_ZCODPRA<>.w_ZCODPRA
            .w_DACODCOM = iif(((g_PERCAN='S' AND (.w_FLANAL='S' or .w_FLDANA='S' )) OR (g_COMM='S' AND .w_FLGCOM='S')),.w_ZCODPRA,'')
          .link_1_92('Full')
        endif
        .DoRTCalc(70,73,.t.)
            .w_FLGCOM = Docgesana(.w_CAUDOC,'C')
        if .o_CAUDOC<>.w_CAUDOC
            .w_FLDANA = docgesana(.w_CAUDOC,'A')
        endif
          .link_1_99('Full')
        .DoRTCalc(77,77,.t.)
            .w_VOCECR = Docgesana(.w_CAUDOC,'V')
        .DoRTCalc(79,89,.t.)
            .w_COD1ATT = IIF(ISAHE(),.w_ECOD1ATT,.w_RCOD1ATT)
        .DoRTCalc(91,101,.t.)
            .w_NOTIPCLI = .w_ATTIPCLI
        .DoRTCalc(103,105,.t.)
          .link_1_129('Full')
        .DoRTCalc(107,108,.t.)
            .w_TIPCAU = 'T'
        .DoRTCalc(110,110,.t.)
          .link_1_136('Full')
        if .o_TETIPEVE<>.w_TETIPEVE
          .Calculate_KOHUIRSVTQ()
        endif
        .DoRTCalc(112,116,.t.)
          .link_1_144('Full')
        .DoRTCalc(118,126,.t.)
            .w_MVCODVAL = g_PERVAL
        .DoRTCalc(128,132,.t.)
        if .o_CODPRA<>.w_CODPRA.or. .o_DACODCOM<>.w_DACODCOM
            .w_ATCODPRA = IIF(ISALT(),.w_CODPRA,.w_DACODCOM)
        endif
        if .o_DACODCOM<>.w_DACODCOM
            .w_ATCOMRIC = .w_DACODCOM
        endif
            .w_ATATTCOS = .w_DAATTIVI
            .w_ATATTRIC = .w_DAATTRIC
        .DoRTCalc(137,141,.t.)
        if .o_SerUtente<>.w_SerUtente
            .w_CODICE = IIF(ISAHE(),IIF(EMPTY(.w_CODICE), .w_SerUtente, .w_CODICE), SPACE(41))
          .link_1_170('Full')
        endif
        .DoRTCalc(143,156,.t.)
          .link_1_185('Full')
        .DoRTCalc(158,158,.t.)
            .w_DADESATT = IIF(ISAHE(),.w_EDESATT,.w_RDESATT)
            .w_DesSer = IIF(ISAHE(),.w_EDESATT,.w_RDESATT)
        .DoRTCalc(161,165,.t.)
        if .o_CodPart<>.w_CodPart
            .w_DATAPREC = Date() - .w_GG_PRE
        endif
        if .o_CodPart<>.w_CodPart
            .w_DATASUCC = Date() + .w_GG_SUC
        endif
        .DoRTCalc(168,169,.t.)
          .link_1_198('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(171,176,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.EVENT.Calculate()
    endwith
  return

  proc Calculate_VTZGIBKZTO()
    with this
          * --- Ricalcolo tempo totale selezionato
          .w_SELTIME = CalcTotTime(.w_EVENT, This)
          .w_DurataOre = VAL(Left(.w_SELTIME, 2))
          .w_DurataMin = VAL(Right(.w_SELTIME, 2))
          .w_QtaTimerOre = IIF(.w_UniMisTempo='S',(.w_DurataOre+cp_ROUND(.w_DurataMin/60,2)),1)
          .w_QtaTimerOre = IIF(.w_UniMisTempo='S',(.w_DurataOre+cp_ROUND(.w_DurataMin/60,2)),1)
          .w_QtaTimerMinuti = IIF(.w_UniMisTempo='S',.w_DurataOre*60+.w_DurataMin,1)
          .w_QtaTimer = IIF(.w_UniMisTempo='S',IIF( .w_ConversioneOre= 0.01667,.w_QtaTimerMinuti,.w_QtaTimerOre*.w_ConversioneOre),1)
          .w_mEnableCtrl = .mEnableControls()
    endwith
  endproc
  proc Calculate_PCCPPBWXPE()
    with this
          * --- Apre anagrafica eventi
          .a = opengest('A','GSFA_AEV','EVSERIAL',.w_EVSERIAL)
    endwith
  endproc
  proc Calculate_KOHUIRSVTQ()
    with this
          * --- Controllo se diver ingresso di tipo telefono
          ChkEVENTO(this;
             )
    endwith
  endproc
  proc Calculate_YPGXONLEYL()
    with this
          * --- Sbianco i dati
          .w_EVNOMINA = ''
          .w_NOMINA = ''
          .w_DACODCOM = ''
          .w_CODICE = ''
          .w_DesAgg = ''
          .w_DESCAN = ''
          .w_RDESATT = ''
          .w_EDESATT = ''
          .w_CODSER = ''
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRTIPRIG_1_51.enabled = this.oPgFrm.Page1.oPag.oRTIPRIG_1_51.mCond()
    this.oPgFrm.Page1.oPag.oETIPRIG_1_52.enabled = this.oPgFrm.Page1.oPag.oETIPRIG_1_52.mCond()
    this.oPgFrm.Page1.oPag.oATIPRIG_1_53.enabled = this.oPgFrm.Page1.oPag.oATIPRIG_1_53.mCond()
    this.oPgFrm.Page1.oPag.oATIPRI2_1_54.enabled = this.oPgFrm.Page1.oPag.oATIPRI2_1_54.mCond()
    this.oPgFrm.Page1.oPag.oRDESATT_1_178.enabled = this.oPgFrm.Page1.oPag.oRDESATT_1_178.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_139.enabled = this.oPgFrm.Page1.oPag.oBtn_1_139.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCodPratica_1_44.visible=!this.oPgFrm.Page1.oPag.oCodPratica_1_44.mHide()
    this.oPgFrm.Page1.oPag.oCODSER_1_45.visible=!this.oPgFrm.Page1.oPag.oCODSER_1_45.mHide()
    this.oPgFrm.Page1.oPag.oRTIPRIG_1_51.visible=!this.oPgFrm.Page1.oPag.oRTIPRIG_1_51.mHide()
    this.oPgFrm.Page1.oPag.oETIPRIG_1_52.visible=!this.oPgFrm.Page1.oPag.oETIPRIG_1_52.mHide()
    this.oPgFrm.Page1.oPag.oATIPRIG_1_53.visible=!this.oPgFrm.Page1.oPag.oATIPRIG_1_53.mHide()
    this.oPgFrm.Page1.oPag.oATIPRI2_1_54.visible=!this.oPgFrm.Page1.oPag.oATIPRI2_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oDesPratica_1_59.visible=!this.oPgFrm.Page1.oPag.oDesPratica_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oDACODCOM_1_92.visible=!this.oPgFrm.Page1.oPag.oDACODCOM_1_92.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_139.visible=!this.oPgFrm.Page1.oPag.oBtn_1_139.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_154.visible=!this.oPgFrm.Page1.oPag.oStr_1_154.mHide()
    this.oPgFrm.Page1.oPag.oCODICE_1_170.visible=!this.oPgFrm.Page1.oPag.oCODICE_1_170.mHide()
    this.oPgFrm.Page1.oPag.oEDESATT_1_177.visible=!this.oPgFrm.Page1.oPag.oEDESATT_1_177.mHide()
    this.oPgFrm.Page1.oPag.oRDESATT_1_178.visible=!this.oPgFrm.Page1.oPag.oRDESATT_1_178.mHide()
    this.oPgFrm.Page1.oPag.oDESCAN_1_189.visible=!this.oPgFrm.Page1.oPag.oDESCAN_1_189.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsfa_kte
    If Cevent='Attivit�'
    Select(This.w_EVENT.ccursor)
       Go top
       Select min(evnomina) as minnom,max(evnomina) as maxnom from (This.w_EVENT.ccursor) where xChk=1 into cursor Nomina
       Select Nomina
       if minnom<>maxnom
         ah_errormsg("Attenzione! Occorre selezionare eventi intestati allo stesso nominativo!")
         this.w_DataIniTimer=cp_chartodate('  -  -  ')
         this.w_DataFinTimer=cp_chartodate('  -  -  ')
         this.w_Dacodcom=''
         this.w_Codice=''
         this.w_Desagg=''
         this.w_Descan=''
         this.w_RDESATT=''
       Else
         Do Gsag_btm with This,'S'
       endif
       Select Nomina
       Use in Nomina
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.EVENT.Event(cEvent)
        if lower(cEvent)==lower("Event rowcheckall") or lower(cEvent)==lower("Event rowcheckfrom") or lower(cEvent)==lower("Event rowcheckto") or lower(cEvent)==lower("Event rowinvertselection") or lower(cEvent)==lower("Event rowuncheckall") or lower(cEvent)==lower("Ricerca") or lower(cEvent)==lower("w_EVENT row checked") or lower(cEvent)==lower("w_EVENT row unchecked")
          .Calculate_VTZGIBKZTO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_EVENT selected")
          .Calculate_PCCPPBWXPE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_EVENT row unchecked")
          .Calculate_YPGXONLEYL()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LeggiCausale
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LeggiCausale) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LeggiCausale)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACAUPRE,PALISACQ,PAFLPRES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LeggiCausale);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LeggiCausale)
            select PACODAZI,PACAUPRE,PALISACQ,PAFLPRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LeggiCausale = NVL(_Link_.PACODAZI,space(10))
      this.w_CauDefault = NVL(_Link_.PACAUPRE,space(10))
      this.w_LISACQ = NVL(_Link_.PALISACQ,space(5))
      this.w_FLPRES = NVL(_Link_.PAFLPRES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LeggiCausale = space(10)
      endif
      this.w_CauDefault = space(10)
      this.w_LISACQ = space(5)
      this.w_FLPRES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LeggiCausale Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TETIPEVE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TETIPEVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSFA_ATE',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_TETIPEVE)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_TETIPEVE))
          select TETIPEVE,TEDESCRI,TEDRIVER;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TETIPEVE)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TETIPEVE) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oTETIPEVE_1_4'),i_cWhere,'GSFA_ATE',"Tipi eventi",'GSFA_KTE.TIPEVENT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TEDESCRI,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TETIPEVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDESCRI,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_TETIPEVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_TETIPEVE)
            select TETIPEVE,TEDESCRI,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TETIPEVE = NVL(_Link_.TETIPEVE,space(10))
      this.w_TEDESCRI = NVL(_Link_.TEDESCRI,space(50))
      this.w_CODDRI = NVL(_Link_.TEDRIVER,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_TETIPEVE = space(10)
      endif
      this.w_TEDESCRI = space(50)
      this.w_CODDRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TETIPEVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOTIPCLI,NOCODCLI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NOTIPCLI,NOCODCLI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOTIPCLI,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NOTIPCLI,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_1_8'),i_cWhere,'',"Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOTIPCLI,NOCODCLI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NOTIPCLI,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOTIPCLI,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NOTIPCLI,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(60))
      this.w_ATTIPCLI = NVL(_Link_.NOTIPCLI,space(1))
      this.w_NOCODCLI = NVL(_Link_.NOCODCLI,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_DESNOM = space(60)
      this.w_ATTIPCLI = space(1)
      this.w_NOCODCLI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ATTIPCLI<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM = space(15)
        this.w_DESNOM = space(60)
        this.w_ATTIPCLI = space(1)
        this.w_NOCODCLI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EVPERSON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVPERSON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_EVPERSON)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TIPPER;
                     ,'DPCODICE',trim(this.w_EVPERSON))
          select DPTIPRIS,DPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVPERSON)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EVPERSON) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oEVPERSON_1_11'),i_cWhere,'GSAR_BDZ',"Persone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPPER<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVPERSON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_EVPERSON);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPPER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TIPPER;
                       ,'DPCODICE',this.w_EVPERSON)
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVPERSON = NVL(_Link_.DPCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EVPERSON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVPERSON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EVGRUPPO
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVGRUPPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_EVGRUPPO)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPGRU);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TIPGRU;
                     ,'DPCODICE',trim(this.w_EVGRUPPO))
          select DPTIPRIS,DPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVGRUPPO)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EVGRUPPO) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oEVGRUPPO_1_12'),i_cWhere,'GSAR_BDZ',"Gruppi",'GSARGADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPGRU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPGRU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVGRUPPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_EVGRUPPO);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TIPGRU;
                       ,'DPCODICE',this.w_EVGRUPPO)
            select DPTIPRIS,DPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVGRUPPO = NVL(_Link_.DPCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_EVGRUPPO = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_EVPERSON , .w_EVGRUPPO) OR Empty(.w_EVPERSON))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
        endif
        this.w_EVGRUPPO = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVGRUPPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZCODPRA
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZCODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZCODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_ZCODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_ZCODPRA)
            select NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZCODPRA = NVL(_Link_.NOCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ZCODPRA = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZCODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOMINA
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOMINA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOMINA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_NOMINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_NOMINA)
            select NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOMINA = NVL(_Link_.NOCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_NOMINA = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOMINA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodPart
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodPart) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodPart)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPSERPRE,DPCODCEN,DPCTRPRE,DPGG_PRE,DPGG_SUC,DPTIPRIG";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CodPart);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CodPart)
            select DPCODICE,DPSERPRE,DPCODCEN,DPCTRPRE,DPGG_PRE,DPGG_SUC,DPTIPRIG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodPart = NVL(_Link_.DPCODICE,space(5))
      this.w_SerUtente = NVL(_Link_.DPSERPRE,space(41))
      this.w_DACENCOS = NVL(_Link_.DPCODCEN,space(15))
      this.w_DACENRIC = NVL(_Link_.DPCODCEN,space(15))
      this.w_CHKDATAPRE = NVL(_Link_.DPCTRPRE,space(1))
      this.w_GG_PRE = NVL(_Link_.DPGG_PRE,0)
      this.w_GG_SUC = NVL(_Link_.DPGG_SUC,0)
      this.w_DTIPRIG = NVL(_Link_.DPTIPRIG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CodPart = space(5)
      endif
      this.w_SerUtente = space(41)
      this.w_DACENCOS = space(15)
      this.w_DACENRIC = space(15)
      this.w_CHKDATAPRE = space(1)
      this.w_GG_PRE = 0
      this.w_GG_SUC = 0
      this.w_DTIPRIG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodPart Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodPratica
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodPratica) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CodPratica)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CodPratica))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CodPratica)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CodPratica)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CodPratica)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CodPratica)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CodPratica)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CodPratica)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CodPratica)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CodPratica) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCodPratica_1_44'),i_cWhere,'',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSFAZAEV.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodPratica)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CodPratica);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CodPratica)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNCODLIS,CNPARASS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodPratica = NVL(_Link_.CNCODCAN,space(15))
      this.w_DesPratica = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
      this.w_ENTE = NVL(_Link_.CN__ENTE,space(10))
      this.w_ATCODLIS = NVL(_Link_.CNCODLIS,space(5))
      this.w_PARASS = NVL(_Link_.CNPARASS,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CodPratica = space(15)
      endif
      this.w_DesPratica = space(100)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
      this.w_ENTE = space(10)
      this.w_ATCODLIS = space(5)
      this.w_PARASS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodPratica Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSER
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODSER))
          select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSER)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODSER) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODSER_1_45'),i_cWhere,'GSMA_BZA',""+iif(not isalt (), "Codici articoli", "Prestazioni") +"",''+iif(NOT isalt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODSER)
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSER = NVL(_Link_.CACODICE,space(20))
      this.w_RDESATT = NVL(_Link_.CADESART,space(40))
      this.w_RDESAGG = NVL(_Link_.CADESSUP,space(0))
      this.w_RCOD1ATT = NVL(_Link_.CACODART,space(20))
      this.w_RUNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_RCACODART = NVL(_Link_.CACODART,space(20))
      this.w_RCADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_RTIPSER = NVL(_Link_.CA__TIPO,space(1))
      this.w_ROPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_RMOLTI3 = NVL(_Link_.CAMOLTIP,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODSER = space(20)
      endif
      this.w_RDESATT = space(40)
      this.w_RDESAGG = space(0)
      this.w_RCOD1ATT = space(20)
      this.w_RUNMIS3 = space(3)
      this.w_RCACODART = space(20)
      this.w_RCADTOBSO = ctod("  /  /  ")
      this.w_RTIPSER = space(1)
      this.w_ROPERA3 = space(1)
      this.w_RMOLTI3 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ISAHE() or ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_CODSER = space(20)
        this.w_RDESATT = space(40)
        this.w_RDESAGG = space(0)
        this.w_RCOD1ATT = space(20)
        this.w_RUNMIS3 = space(3)
        this.w_RCACODART = space(20)
        this.w_RCADTOBSO = ctod("  /  /  ")
        this.w_RTIPSER = space(1)
        this.w_ROPERA3 = space(1)
        this.w_RMOLTI3 = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODART
  func Link_1_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARTIPART,ARTIPRIG";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CACODART)
            select ARCODART,ARUNMIS1,ARTIPART,ARTIPRIG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODART = NVL(_Link_.ARCODART,space(20))
      this.w_PrimaUniMis = NVL(_Link_.ARUNMIS1,space(10))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_ARTIPRIG = NVL(_Link_.ARTIPRIG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACODART = space(20)
      endif
      this.w_PrimaUniMis = space(10)
      this.w_TIPART = space(2)
      this.w_ARTIPRIG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PrimaUniMis
  func Link_1_74(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PrimaUniMis) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PrimaUniMis)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLTEMP,UMDURORE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_PrimaUniMis);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_PrimaUniMis)
            select UMCODICE,UMFLTEMP,UMDURORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PrimaUniMis = NVL(_Link_.UMCODICE,space(10))
      this.w_UniMisTempo = NVL(_Link_.UMFLTEMP,space(10))
      this.w_ConversioneOre = NVL(_Link_.UMDURORE,0)
    else
      if i_cCtrl<>'Load'
        this.w_PrimaUniMis = space(10)
      endif
      this.w_UniMisTempo = space(10)
      this.w_ConversioneOre = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PrimaUniMis Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODCOM
  func Link_1_92(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_DACODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_DACODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oDACODCOM_1_92'),i_cWhere,'GSAR_ACN',"",'GSFAZAEV.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_DACODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_DACODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_DACODCOM = space(15)
      endif
      this.w_DESCAN = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUDOC
  func Link_1_99(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDFLVEAC,TDFLGLIS";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUDOC)
            select TDTIPDOC,TDFLVEAC,TDFLGLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_MVFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLGLIS = NVL(_Link_.TDFLGLIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUDOC = space(5)
      endif
      this.w_MVFLVEAC = space(1)
      this.w_FLGLIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CauDefault
  func Link_1_129(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CauDefault) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CauDefault)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CAFLNSAP,CADESCRI,CACAUDOC,CAFLANAL,CACAUACQ";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CauDefault);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CauDefault)
            select CACODICE,CAFLNSAP,CADESCRI,CACAUDOC,CAFLANAL,CACAUACQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CauDefault = NVL(_Link_.CACODICE,space(10))
      this.w_FLNSAP = NVL(_Link_.CAFLNSAP,space(10))
      this.w_TxtSel = NVL(_Link_.CADESCRI,space(10))
      this.w_CAUDOC = NVL(_Link_.CACAUDOC,space(5))
      this.w_FLANAL = NVL(_Link_.CAFLANAL,space(1))
      this.w_CAUACQ = NVL(_Link_.CACAUACQ,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CauDefault = space(10)
      endif
      this.w_FLNSAP = space(10)
      this.w_TxtSel = space(10)
      this.w_CAUDOC = space(5)
      this.w_FLANAL = space(1)
      this.w_CAUACQ = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CauDefault Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODDRI
  func Link_1_136(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_lTable = "DRVEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2], .t., this.DRVEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODDRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODDRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DEDRIVER,DE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where DEDRIVER="+cp_ToStrODBC(this.w_CODDRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DEDRIVER',this.w_CODDRI)
            select DEDRIVER,DE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODDRI = NVL(_Link_.DEDRIVER,space(10))
      this.w_TIPODRV = NVL(_Link_.DE__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODDRI = space(10)
      endif
      this.w_TIPODRV = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])+'\'+cp_ToStr(_Link_.DEDRIVER,1)
      cp_ShowWarn(i_cKey,this.DRVEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODDRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLI
  func Link_1_144(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANSCORPO,ANCATCON";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ATTIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ATTIPCLI;
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANSCORPO,ANCATCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_FLSCOR = NVL(_Link_.ANSCORPO,space(1))
      this.w_CATCOM = NVL(_Link_.ANCATCON,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_FLSCOR = space(1)
      this.w_CATCOM = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODICE
  func Link_1_170(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODICE))
          select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODICE)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_CODICE)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODICE_1_170'),i_cWhere,'GSMA_BZA',"",'GSAGZKPM.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODICE)
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.CACODICE,space(41))
      this.w_EDESATT = NVL(_Link_.CADESART,space(40))
      this.w_EDESAGG = NVL(_Link_.CADESSUP,space(0))
      this.w_ECOD1ATT = NVL(_Link_.CACODART,space(20))
      this.w_EUNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_ECACODART = NVL(_Link_.CACODART,space(20))
      this.w_ECADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_ETIPSER = NVL(_Link_.CA__TIPO,space(1))
      this.w_EOPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_EMOLTI3 = NVL(_Link_.CAMOLTIP,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(41)
      endif
      this.w_EDESATT = space(40)
      this.w_EDESAGG = space(0)
      this.w_ECOD1ATT = space(20)
      this.w_EUNMIS3 = space(3)
      this.w_ECACODART = space(20)
      this.w_ECADTOBSO = ctod("  /  /  ")
      this.w_ETIPSER = space(1)
      this.w_EOPERA3 = space(1)
      this.w_EMOLTI3 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!ISAHE() or ChkTipArt(.w_ECACODART, "FM-FO-DE") AND (EMPTY(.w_ECADTOBSO) OR .w_ECADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_CODICE = space(41)
        this.w_EDESATT = space(40)
        this.w_EDESAGG = space(0)
        this.w_ECOD1ATT = space(20)
        this.w_EUNMIS3 = space(3)
        this.w_ECACODART = space(20)
        this.w_ECADTOBSO = ctod("  /  /  ")
        this.w_ETIPSER = space(1)
        this.w_EOPERA3 = space(1)
        this.w_EMOLTI3 = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODBUN
  func Link_1_185(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_ATCODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_READAZI;
                       ,'BUCODICE',this.w_ATCODBUN)
            select BUCODAZI,BUCODICE,BUFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_BUFLANAL = NVL(_Link_.BUFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODBUN = space(3)
      endif
      this.w_BUFLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPERS
  func Link_1_198(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPERS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPERS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPERS);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_CODPERS)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPERS = NVL(_Link_.DPCODICE,space(5))
      this.w_DPCOGNOM = NVL(_Link_.DPCOGNOM,space(50))
      this.w_DPNOME = NVL(_Link_.DPNOME,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODPERS = space(5)
      endif
      this.w_DPCOGNOM = space(50)
      this.w_DPNOME = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPERS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTETIPEVE_1_4.value==this.w_TETIPEVE)
      this.oPgFrm.Page1.oPag.oTETIPEVE_1_4.value=this.w_TETIPEVE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA_INI_1_5.value==this.w_DATA_INI)
      this.oPgFrm.Page1.oPag.oDATA_INI_1_5.value=this.w_DATA_INI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPEVE1_1_6.RadioValue()==this.w_TIPEVE1)
      this.oPgFrm.Page1.oPag.oTIPEVE1_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA_FIN_1_7.value==this.w_DATA_FIN)
      this.oPgFrm.Page1.oPag.oDATA_FIN_1_7.value=this.w_DATA_FIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNOM_1_8.value==this.w_CODNOM)
      this.oPgFrm.Page1.oPag.oCODNOM_1_8.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDIR1_1_10.RadioValue()==this.w_TIPDIR1)
      this.oPgFrm.Page1.oPag.oTIPDIR1_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEVPERSON_1_11.value==this.w_EVPERSON)
      this.oPgFrm.Page1.oPag.oEVPERSON_1_11.value=this.w_EVPERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oEVGRUPPO_1_12.value==this.w_EVGRUPPO)
      this.oPgFrm.Page1.oPag.oEVGRUPPO_1_12.value=this.w_EVGRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oTEDESCRI_1_23.value==this.w_TEDESCRI)
      this.oPgFrm.Page1.oPag.oTEDESCRI_1_23.value=this.w_TEDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSELTIME_1_42.value==this.w_SELTIME)
      this.oPgFrm.Page1.oPag.oSELTIME_1_42.value=this.w_SELTIME
    endif
    if not(this.oPgFrm.Page1.oPag.oCodPratica_1_44.value==this.w_CodPratica)
      this.oPgFrm.Page1.oPag.oCodPratica_1_44.value=this.w_CodPratica
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSER_1_45.value==this.w_CODSER)
      this.oPgFrm.Page1.oPag.oCODSER_1_45.value=this.w_CODSER
    endif
    if not(this.oPgFrm.Page1.oPag.oDesAgg_1_50.value==this.w_DesAgg)
      this.oPgFrm.Page1.oPag.oDesAgg_1_50.value=this.w_DesAgg
    endif
    if not(this.oPgFrm.Page1.oPag.oRTIPRIG_1_51.RadioValue()==this.w_RTIPRIG)
      this.oPgFrm.Page1.oPag.oRTIPRIG_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oETIPRIG_1_52.RadioValue()==this.w_ETIPRIG)
      this.oPgFrm.Page1.oPag.oETIPRIG_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATIPRIG_1_53.RadioValue()==this.w_ATIPRIG)
      this.oPgFrm.Page1.oPag.oATIPRIG_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATIPRI2_1_54.RadioValue()==this.w_ATIPRI2)
      this.oPgFrm.Page1.oPag.oATIPRI2_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDesPratica_1_59.value==this.w_DesPratica)
      this.oPgFrm.Page1.oPag.oDesPratica_1_59.value=this.w_DesPratica
    endif
    if not(this.oPgFrm.Page1.oPag.oDurataOre_1_65.value==this.w_DurataOre)
      this.oPgFrm.Page1.oPag.oDurataOre_1_65.value=this.w_DurataOre
    endif
    if not(this.oPgFrm.Page1.oPag.oDurataMin_1_66.value==this.w_DurataMin)
      this.oPgFrm.Page1.oPag.oDurataMin_1_66.value=this.w_DurataMin
    endif
    if not(this.oPgFrm.Page1.oPag.oDataIniTimer_1_69.value==this.w_DataIniTimer)
      this.oPgFrm.Page1.oPag.oDataIniTimer_1_69.value=this.w_DataIniTimer
    endif
    if not(this.oPgFrm.Page1.oPag.oDataFinTimer_1_83.value==this.w_DataFinTimer)
      this.oPgFrm.Page1.oPag.oDataFinTimer_1_83.value=this.w_DataFinTimer
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODCOM_1_92.value==this.w_DACODCOM)
      this.oPgFrm.Page1.oPag.oDACODCOM_1_92.value=this.w_DACODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_134.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_134.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_170.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_170.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oEDESATT_1_177.value==this.w_EDESATT)
      this.oPgFrm.Page1.oPag.oEDESATT_1_177.value=this.w_EDESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oRDESATT_1_178.value==this.w_RDESATT)
      this.oPgFrm.Page1.oPag.oRDESATT_1_178.value=this.w_RDESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_189.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_189.value=this.w_DESCAN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_ATTIPCLI<>'F')  and not(empty(.w_CODNOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODNOM_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPGRU='G' AND (GSAR1BGP( '' , 'CHK', .w_EVPERSON , .w_EVGRUPPO) OR Empty(.w_EVPERSON)))  and not(empty(.w_EVGRUPPO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVGRUPPO_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, persona non appartenente al gruppo selezionato")
          case   not(ISAHE() or ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys))  and not(ISAHE())  and not(empty(.w_CODSER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODSER_1_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
          case   not(.w_DurataMin<60)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDurataMin_1_66.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!ISAHE() or ChkTipArt(.w_ECACODART, "FM-FO-DE") AND (EMPTY(.w_ECADTOBSO) OR .w_ECADTOBSO>=i_datsys))  and not(!ISAHE())  and not(empty(.w_CODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODICE_1_170.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TETIPEVE = this.w_TETIPEVE
    this.o_EVSERIAL = this.w_EVSERIAL
    this.o_ZCODPRA = this.w_ZCODPRA
    this.o_SELEZIONE = this.w_SELEZIONE
    this.o_CODPRA = this.w_CODPRA
    this.o_EDATINI = this.w_EDATINI
    this.o_EDATFIN = this.w_EDATFIN
    this.o_CodPart = this.w_CodPart
    this.o_SELTIME = this.w_SELTIME
    this.o_SerUtente = this.w_SerUtente
    this.o_CODSER = this.w_CODSER
    this.o_CACODART = this.w_CACODART
    this.o_RTIPRIG = this.w_RTIPRIG
    this.o_ETIPRIG = this.w_ETIPRIG
    this.o_ATIPRIG = this.w_ATIPRIG
    this.o_ATIPRI2 = this.w_ATIPRI2
    this.o_ETIPRI2 = this.w_ETIPRI2
    this.o_RTIPRI2 = this.w_RTIPRI2
    this.o_DurataOre = this.w_DurataOre
    this.o_DurataMin = this.w_DurataMin
    this.o_UniMisTempo = this.w_UniMisTempo
    this.o_DACODCOM = this.w_DACODCOM
    this.o_CAUDOC = this.w_CAUDOC
    this.o_DACODATT = this.w_DACODATT
    this.o_DAPREZZO = this.w_DAPREZZO
    this.o_DACODICE = this.w_DACODICE
    this.o_CODICE = this.w_CODICE
    return

enddefine

* --- Define pages as container
define class tgsfa_ktePag1 as StdContainer
  Width  = 787
  height = 545
  stdWidth  = 787
  stdheight = 545
  resizeXpos=509
  resizeYpos=193
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTETIPEVE_1_4 as StdField with uid="MGURRLFJJI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TETIPEVE", cQueryName = "TETIPEVE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Tipo evento",;
    HelpContextID = 90441851,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=70, Top=9, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPEVENT", cZoomOnZoom="GSFA_ATE", oKey_1_1="TETIPEVE", oKey_1_2="this.w_TETIPEVE"

  func oTETIPEVE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTETIPEVE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTETIPEVE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPEVENT','*','TETIPEVE',cp_AbsName(this.parent,'oTETIPEVE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSFA_ATE',"Tipi eventi",'GSFA_KTE.TIPEVENT_VZM',this.parent.oContained
  endproc
  proc oTETIPEVE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSFA_ATE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TETIPEVE=this.parent.oContained.w_TETIPEVE
     i_obj.ecpSave()
  endproc

  add object oDATA_INI_1_5 as StdField with uid="FPDIQHIOWC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATA_INI", cQueryName = "DATA_INI",;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 95681665,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=611, Top=9


  add object oTIPEVE1_1_6 as StdCombo with uid="BLKAPLOGCU",value=3,rtseq=6,rtrep=.f.,left=70,top=36,width=110,height=21;
    , ToolTipText = "Stato evento";
    , HelpContextID = 96455734;
    , cFormVar="w_TIPEVE1",RowSource=""+"Processato,"+"Da processare,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPEVE1_1_6.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'',;
    space(10)))))
  endfunc
  func oTIPEVE1_1_6.GetRadio()
    this.Parent.oContained.w_TIPEVE1 = this.RadioValue()
    return .t.
  endfunc

  func oTIPEVE1_1_6.SetRadio()
    this.Parent.oContained.w_TIPEVE1=trim(this.Parent.oContained.w_TIPEVE1)
    this.value = ;
      iif(this.Parent.oContained.w_TIPEVE1=='P',1,;
      iif(this.Parent.oContained.w_TIPEVE1=='D',2,;
      iif(this.Parent.oContained.w_TIPEVE1=='',3,;
      0)))
  endfunc

  add object oDATA_FIN_1_7 as StdField with uid="OQMNOWMBDB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATA_FIN", cQueryName = "DATA_FIN",;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 146013308,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=611, Top=36

  add object oCODNOM_1_8 as StdField with uid="WTLAAXZRJJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nominativo",;
    HelpContextID = 223875366,;
   bGlobalFont=.t.,;
    Height=21, Width=131, Left=70, Top=62, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Nominativi",'',this.parent.oContained
  endproc


  add object oTIPDIR1_1_10 as StdCombo with uid="YZAGMTAQCK",value=3,rtseq=10,rtrep=.f.,left=219,top=36,width=107,height=21;
    , ToolTipText = "Direzione evento";
    , HelpContextID = 32427062;
    , cFormVar="w_TIPDIR1",RowSource=""+"Entrata,"+"Uscita,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDIR1_1_10.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'U',;
    iif(this.value =3,'',;
    space(10)))))
  endfunc
  func oTIPDIR1_1_10.GetRadio()
    this.Parent.oContained.w_TIPDIR1 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDIR1_1_10.SetRadio()
    this.Parent.oContained.w_TIPDIR1=trim(this.Parent.oContained.w_TIPDIR1)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDIR1=='E',1,;
      iif(this.Parent.oContained.w_TIPDIR1=='U',2,;
      iif(this.Parent.oContained.w_TIPDIR1=='',3,;
      0)))
  endfunc

  add object oEVPERSON_1_11 as StdField with uid="AOKPBXBJKJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_EVPERSON", cQueryName = "EVPERSON",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Persona",;
    HelpContextID = 58710164,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=389, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TIPPER", oKey_2_1="DPCODICE", oKey_2_2="this.w_EVPERSON"

  func oEVPERSON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVPERSON_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVPERSON_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TIPPER)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TIPPER)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oEVPERSON_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone",'',this.parent.oContained
  endproc
  proc oEVPERSON_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TIPPER
     i_obj.w_DPCODICE=this.parent.oContained.w_EVPERSON
     i_obj.ecpSave()
  endproc

  add object oEVGRUPPO_1_12 as StdField with uid="WQXVDIIIVM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_EVGRUPPO", cQueryName = "EVGRUPPO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, persona non appartenente al gruppo selezionato",;
    ToolTipText = "Gruppo",;
    HelpContextID = 12339349,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=500, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TIPGRU", oKey_2_1="DPCODICE", oKey_2_2="this.w_EVGRUPPO"

  func oEVGRUPPO_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVGRUPPO_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVGRUPPO_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TIPGRU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TIPGRU)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oEVGRUPPO_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Gruppi",'GSARGADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oEVGRUPPO_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TIPGRU
     i_obj.w_DPCODICE=this.parent.oContained.w_EVGRUPPO
     i_obj.ecpSave()
  endproc


  add object EVENT as cp_szoombox with uid="NQHSFSVQZT",left=9, top=86, width=766,height=251,;
    caption='',;
   bGlobalFont=.t.,;
    cTable="ANEVENTI",cZoomFile="GSFA_KTE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="gsfa_aev",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 82477578

  add object oTEDESCRI_1_23 as StdField with uid="UGMVGEFMLI",rtseq=15,rtrep=.f.,;
    cFormVar = "w_TEDESCRI", cQueryName = "TEDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Tipo evento",;
    HelpContextID = 59705471,;
   bGlobalFont=.t.,;
    Height=21, Width=400, Left=157, Top=9, InputMask=replicate('X',50)


  add object oBtn_1_26 as StdButton with uid="PPWFAOCXOY",left=726, top=11, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 94444566;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        .NotifyEvent("Ricerca")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELTIME_1_42 as StdField with uid="OXWMRCYAVM",rtseq=32,rtrep=.f.,;
    cFormVar = "w_SELTIME", cQueryName = "SELTIME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 218007590,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=483, Top=354, InputMask=replicate('X',10)

  add object oCodPratica_1_44 as StdField with uid="NHEKFOTWVB",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CodPratica", cQueryName = "CodPratica",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 59545791,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=105, Top=382, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CodPratica"

  func oCodPratica_1_44.mHide()
    with this.Parent.oContained
      return (!ISALT())
    endwith
  endfunc

  func oCodPratica_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodPratica_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodPratica_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCodPratica_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSFAZAEV.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oCODSER_1_45 as StdField with uid="NQAZQYRMPT",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CODSER", cQueryName = "CODSER",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 29167910,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=105, Top=409, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_CODSER"

  func oCODSER_1_45.mHide()
    with this.Parent.oContained
      return (ISAHE())
    endwith
  endfunc

  func oCODSER_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSER_1_45.ecpDrop(oSource)
    this.Parent.oContained.link_1_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSER_1_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODSER_1_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',""+iif(not isalt (), "Codici articoli", "Prestazioni") +"",''+iif(NOT isalt(),"GSAGZKPM", "Inspre")+'.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCODSER_1_45.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODSER
     i_obj.ecpSave()
  endproc

  add object oDesAgg_1_50 as StdMemo with uid="XENUFWOWED",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DesAgg", cQueryName = "DesAgg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva della prestazione",;
    HelpContextID = 147724086,;
   bGlobalFont=.t.,;
    Height=66, Width=491, Left=105, Top=441


  add object oRTIPRIG_1_51 as StdCombo with uid="MPUBOVVPOO",rtseq=41,rtrep=.f.,left=105,top=513,width=126,height=21;
    , HelpContextID = 108370154;
    , cFormVar="w_RTIPRIG",RowSource=""+"Nessuno,"+"Doc. vendite,"+"Doc. acquisti,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRTIPRIG_1_51.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'A',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oRTIPRIG_1_51.GetRadio()
    this.Parent.oContained.w_RTIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oRTIPRIG_1_51.SetRadio()
    this.Parent.oContained.w_RTIPRIG=trim(this.Parent.oContained.w_RTIPRIG)
    this.value = ;
      iif(this.Parent.oContained.w_RTIPRIG=='N',1,;
      iif(this.Parent.oContained.w_RTIPRIG=='D',2,;
      iif(this.Parent.oContained.w_RTIPRIG=='A',3,;
      iif(this.Parent.oContained.w_RTIPRIG=='E',4,;
      0))))
  endfunc

  func oRTIPRIG_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAhr())
    endwith
   endif
  endfunc

  func oRTIPRIG_1_51.mHide()
    with this.Parent.oContained
      return (!IsAhr())
    endwith
  endfunc


  add object oETIPRIG_1_52 as StdCombo with uid="GBKXYQPUKG",rtseq=42,rtrep=.f.,left=105,top=513,width=126,height=21;
    , HelpContextID = 108370362;
    , cFormVar="w_ETIPRIG",RowSource=""+"Nessuno,"+"Doc. vendite,"+"Doc. acquisti,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oETIPRIG_1_52.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'A',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oETIPRIG_1_52.GetRadio()
    this.Parent.oContained.w_ETIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oETIPRIG_1_52.SetRadio()
    this.Parent.oContained.w_ETIPRIG=trim(this.Parent.oContained.w_ETIPRIG)
    this.value = ;
      iif(this.Parent.oContained.w_ETIPRIG=='N',1,;
      iif(this.Parent.oContained.w_ETIPRIG=='D',2,;
      iif(this.Parent.oContained.w_ETIPRIG=='A',3,;
      iif(this.Parent.oContained.w_ETIPRIG=='E',4,;
      0))))
  endfunc

  func oETIPRIG_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAhe())
    endwith
   endif
  endfunc

  func oETIPRIG_1_52.mHide()
    with this.Parent.oContained
      return (!IsAhe())
    endwith
  endfunc


  add object oATIPRIG_1_53 as StdCombo with uid="SVQWWRTAHC",rtseq=43,rtrep=.f.,left=105,top=513,width=126,height=21;
    , HelpContextID = 108370426;
    , cFormVar="w_ATIPRIG",RowSource=""+"Non fatturabile,"+"Fatturabile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATIPRIG_1_53.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oATIPRIG_1_53.GetRadio()
    this.Parent.oContained.w_ATIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oATIPRIG_1_53.SetRadio()
    this.Parent.oContained.w_ATIPRIG=trim(this.Parent.oContained.w_ATIPRIG)
    this.value = ;
      iif(this.Parent.oContained.w_ATIPRIG=='N',1,;
      iif(this.Parent.oContained.w_ATIPRIG=='D',2,;
      0))
  endfunc

  func oATIPRIG_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oATIPRIG_1_53.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oATIPRI2_1_54 as StdCombo with uid="WIPSMOWBSJ",rtseq=44,rtrep=.f.,left=339,top=514,width=126,height=21;
    , HelpContextID = 160065030;
    , cFormVar="w_ATIPRI2",RowSource=""+"Senza nota spese,"+"Con nota spese", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATIPRI2_1_54.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oATIPRI2_1_54.GetRadio()
    this.Parent.oContained.w_ATIPRI2 = this.RadioValue()
    return .t.
  endfunc

  func oATIPRI2_1_54.SetRadio()
    this.Parent.oContained.w_ATIPRI2=trim(this.Parent.oContained.w_ATIPRI2)
    this.value = ;
      iif(this.Parent.oContained.w_ATIPRI2=='N',1,;
      iif(this.Parent.oContained.w_ATIPRI2=='D',2,;
      0))
  endfunc

  func oATIPRI2_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oATIPRI2_1_54.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDesPratica_1_59 as StdField with uid="QVGZWBBWIW",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DesPratica", cQueryName = "DesPratica",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 59604687,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=274, Top=382, InputMask=replicate('X',100)

  func oDesPratica_1_59.mHide()
    with this.Parent.oContained
      return (!ISALT())
    endwith
  endfunc


  add object oBtn_1_62 as StdButton with uid="TKCMXZXILU",left=726, top=490, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75160250;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDurataOre_1_65 as StdField with uid="PHQRCKNSLA",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DurataOre", cQueryName = "DurataOre",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 62791160,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=530, Top=354, cSayPict='"999"', cGetPict='"999"'

  add object oDurataMin_1_66 as StdField with uid="LKLOMSPJFE",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DurataMin", cQueryName = "DurataMin",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 62791295,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=568, Top=354

  func oDurataMin_1_66.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DurataMin<60)
    endwith
    return bRes
  endfunc

  add object oDataIniTimer_1_69 as StdField with uid="ZFOXPZZFPT",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DataIniTimer", cQueryName = "DataIniTimer",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 24713446,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=105, Top=355

  add object oDataFinTimer_1_83 as StdField with uid="YTWUTOZJQB",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DataFinTimer", cQueryName = "DataFinTimer",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 156690202,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=239, Top=355

  add object oDACODCOM_1_92 as StdField with uid="SLFAGJKYNX",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DACODCOM", cQueryName = "DACODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 44626819,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=105, Top=382, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_DACODCOM"

  func oDACODCOM_1_92.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oDACODCOM_1_92.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_92('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODCOM_1_92.ecpDrop(oSource)
    this.Parent.oContained.link_1_92('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODCOM_1_92.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oDACODCOM_1_92'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"",'GSFAZAEV.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oDACODCOM_1_92.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_DACODCOM
     i_obj.ecpSave()
  endproc

  add object oDESNOM_1_134 as StdField with uid="DIPBALCTVA",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 223934262,;
   bGlobalFont=.t.,;
    Height=21, Width=354, Left=203, Top=62, InputMask=replicate('X',60)


  add object oBtn_1_139 as StdButton with uid="YDGGHBTAGM",left=726, top=354, width=48,height=45,;
    CpPicture="bmp\DM_agenda.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per caricare l'attivit� da completare";
    , HelpContextID = 199636710;
    , caption='\<Attivit�';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_139.Click()
      this.parent.oContained.NotifyEvent("Attivit�")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_139.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.w_DurataOre+.w_DurataMin)>0 AND (!Empty(.w_CodPratica) OR !Isalt()) AND( (!EMPTY(.w_CodSer) AND !Isahe()) or  (!EMPTY(.w_Codice) AND Isahe())))
      endwith
    endif
  endfunc

  func oBtn_1_139.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((Not((.w_DurataOre+.w_DurataMin)>0 AND (!Empty(.w_CodPratica) OR !Isalt()) AND( (!EMPTY(.w_CodSer) AND !Isahe()) or  (!EMPTY(.w_Codice) AND Isahe())))) OR .w_SELEZIONE=0)
     endwith
    endif
  endfunc

  add object oCODICE_1_170 as StdField with uid="CIOBBOQBSZ",rtseq=142,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
    HelpContextID = 76747046,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=105, Top=409, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_CODICE"

  func oCODICE_1_170.mHide()
    with this.Parent.oContained
      return (!ISAHE())
    endwith
  endfunc

  func oCODICE_1_170.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_170('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_170.ecpDrop(oSource)
    this.Parent.oContained.link_1_170('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_170.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODICE_1_170'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"",'GSAGZKPM.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCODICE_1_170.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODICE
     i_obj.ecpSave()
  endproc

  add object oEDESATT_1_177 as StdField with uid="SNHNGLZQKZ",rtseq=149,rtrep=.t.,;
    cFormVar = "w_EDESATT", cQueryName = "EDESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 58529350,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=275, Top=409, InputMask=replicate('X',40)

  func oEDESATT_1_177.mHide()
    with this.Parent.oContained
      return (!ISAHE())
    endwith
  endfunc

  add object oRDESATT_1_178 as StdField with uid="SEYLJWTIHI",rtseq=150,rtrep=.t.,;
    cFormVar = "w_RDESATT", cQueryName = "RDESATT",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 58529558,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=275, Top=409, InputMask=replicate('X',40)

  func oRDESATT_1_178.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODSER) AND (.w_NOEDES='N' OR !Isalt() or cp_IsAdministrator()))
    endwith
   endif
  endfunc

  func oRDESATT_1_178.mHide()
    with this.Parent.oContained
      return (ISAHE())
    endwith
  endfunc

  add object oDESCAN_1_189 as StdField with uid="OCAKACUBJE",rtseq=161,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 225310518,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=274, Top=382, InputMask=replicate('X',100)

  func oDESCAN_1_189.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="KHYEUMTNCG",Visible=.t., Left=29, Top=38,;
    Alignment=1, Width=39, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="ZYMZLBHBOL",Visible=.t., Left=26, Top=11,;
    Alignment=1, Width=42, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="ETMYABWJAC",Visible=.t., Left=185, Top=38,;
    Alignment=1, Width=32, Height=18,;
    Caption="Dir.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="UXDSDXOTLI",Visible=.t., Left=336, Top=38,;
    Alignment=1, Width=50, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="FOAHLIOEEF",Visible=.t., Left=448, Top=38,;
    Alignment=1, Width=50, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="BAJOMKRHLC",Visible=.t., Left=561, Top=11,;
    Alignment=1, Width=46, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="FRQIRPIFFI",Visible=.t., Left=570, Top=38,;
    Alignment=1, Width=37, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="OEVLTSOEMQ",Visible=.t., Left=60, Top=385,;
    Alignment=1, Width=41, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="CHJQJSJBCK",Visible=.t., Left=32, Top=385,;
    Alignment=1, Width=69, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="FNKXTFHGOF",Visible=.t., Left=52, Top=412,;
    Alignment=1, Width=48, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="AUEJGKPLZP",Visible=.t., Left=32, Top=412,;
    Alignment=1, Width=68, Height=18,;
    Caption="Prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="FKQQIIZGHB",Visible=.t., Left=32, Top=444,;
    Alignment=1, Width=68, Height=18,;
    Caption="Descr. Agg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="ZERTRSSKLC",Visible=.t., Left=32, Top=514,;
    Alignment=1, Width=68, Height=18,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="KMGLYDPVXE",Visible=.t., Left=544, Top=339,;
    Alignment=0, Width=20, Height=18,;
    Caption="Ore"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="RXZWLCDQGP",Visible=.t., Left=573, Top=339,;
    Alignment=0, Width=22, Height=18,;
    Caption="Min."  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="EWDMWFXBKN",Visible=.t., Left=376, Top=355,;
    Alignment=1, Width=106, Height=18,;
    Caption="Totale tempo sel.: "  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="SXVXTBCMWU",Visible=.t., Left=105, Top=339,;
    Alignment=0, Width=28, Height=18,;
    Caption="Inizio"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="SECZYTGFWS",Visible=.t., Left=239, Top=339,;
    Alignment=0, Width=28, Height=18,;
    Caption="Fine"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="IEYBRQKZJF",Visible=.t., Left=564, Top=356,;
    Alignment=0, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_135 as StdString with uid="RGWBKYJXXO",Visible=.t., Left=3, Top=64,;
    Alignment=1, Width=65, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_154 as StdString with uid="WAGNKWKHVX",Visible=.t., Left=267, Top=514,;
    Alignment=1, Width=68, Height=18,;
    Caption="Nota spese:"  ;
  , bGlobalFont=.t.

  func oStr_1_154.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_kte','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsfa_kte
*--- Calcola tempo totale e datini e datfin
Function CalcTotTime(pObj, pParent)
   Local l_oldArea, l_TotOre, l_TotMin, l_Sec, l_Min, l_Hour, l_cursor, l_MinDat, l_MaxDat,l_pos
   l_TotOre = 0
   l_TotMin = 0
   l_MinDat = {//:}
   l_MaxDat = {//:}
   l_oldArea = Select()
   l_cursor = pObj.cCursor
   Select(l_cursor)
   IF Reccount(l_cursor)>0
   l_pos=Recno()
   Select(l_cursor)
   Go Top
   Scan For xChk=1
     If !Empty(m.l_MinDat)
       l_MinDat = MIN(NVL(EVDATINI,{//:}), m.l_MinDat) 
     Else
       l_MinDat = NVL(EVDATINI, {//:})
     Endif
     If !Empty(m.l_MaxDat)
       l_MaxDat = MAX(NVL(EVDATFIN,{//:}), m.l_MaxDat) 
     Else
       l_MaxDat = NVL(EVDATFIN, {//:})
     Endif     
     l_Sec = (NVL(EVDATFIN,0) - NVL(EVDATINI,0))
     l_Min = cp_ROUND(m.l_Sec/60,0)
     l_Hour = INT(m.l_Min/60)
     l_TotOre = m.l_TotOre + IIF(Empty(NVL(EVOREEFF,0)) And Empty(NVL(EVMINEFF,0)), m.l_Hour, NVL(EVOREEFF,0))
     l_TotMin = m.l_TotMin + IIF(Empty(NVL(EVOREEFF,0)) And Empty(NVL(EVMINEFF,0)), m.l_Min, NVL(EVMINEFF,0))
   EndScan
   Select(l_cursor)
   Go l_pos
   endif
   Select(l_oldArea)
   pParent.w_DataIniTimer = m.l_MinDat
   pParent.w_DataFinTimer = m.l_MaxDat
   pParent.mcalc(.t.)
   l_TotOre = m.l_TotOre + INT(m.l_TotMin/60)
   l_TotMin = MOD(m.l_TotMin, 60)
   Return Right("00"+Alltrim(Str(m.l_TotOre)),2)+":"+Right("00"+Alltrim(Str(m.l_TotMin)),2)
EndFunc
proc ChkEVENTO(obj)
    i_bRes=obj.w_TIPODRV='T'
  if not(i_bRes)
    do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
      obj.w_TETIPEVE = space(10)
      obj.w_TEDESCRI = space(50)
  endif
endproc

* --- Fine Area Manuale
