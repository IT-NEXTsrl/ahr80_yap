* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kar                                                        *
*              Attivit� ricorrenti                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-12-17                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kar",oParentObject))

* --- Class definition
define class tgsag_kar as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 616
  Height = 511
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-13"
  HelpContextID=132786025
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=84

  * --- Constant Properties
  _IDX = 0
  PAR_AGEN_IDX = 0
  MODCLDAT_IDX = 0
  CAUMATTI_IDX = 0
  TAB_CALE_IDX = 0
  cPrg = "gsag_kar"
  cComment = "Attivit� ricorrenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PAR_AGEN = space(5)
  w_INIT_METODO = space(3)
  w_TCFLDEFA = space(1)
  w_TCCODICE = space(5)
  w_DATINI_OR = ctod('  /  /  ')
  o_DATINI_OR = ctod('  /  /  ')
  w_DATFIN_OR = ctod('  /  /  ')
  o_DATFIN_OR = ctod('  /  /  ')
  w_GIOINI = space(3)
  w_GIOFIN = space(3)
  w_METODO = space(3)
  o_METODO = space(3)
  w_MD__WEEK = 0
  w_MD_MONTH = 0
  w_MD__FREQ = space(1)
  o_MD__FREQ = space(1)
  w_MDDATUNI = ctod('  /  /  ')
  w_MDNUMFRQ = 0
  w_MDMININT = 0
  w_MDIMPDAT = space(1)
  w_MD_GGFIS = 0
  w_ATCAUATT_ORIG = space(20)
  w_CARAGGST = space(1)
  w_PACHKFES = space(1)
  w_ATSERIAL_ORIG = space(20)
  w_ATCAITER_ORIG = space(20)
  w_ATNOTPIA_ORIG = space(0)
  w_ATCODPRA_ORIG = space(15)
  w_ATCENCOS_ORIG = space(15)
  w_ATCODNOM_ORIG = space(15)
  w_ATTELEFO_ORIG = space(18)
  w_ATCELLUL_ORIG = space(18)
  w_AT_EMAIL_ORIG = space(0)
  w_AT_EMPEC_ORIG = space(0)
  w_ATLOCALI_ORIG = space(30)
  w_AT__ENTE_ORIG = space(10)
  w_ATUFFICI_ORIG = space(10)
  w_ATCODSED_ORIG = space(5)
  w_ATPERSON_ORIG = space(60)
  w_DATINI_ORIG = ctod('  /  /  ')
  w_ORAINI_ORIG = space(2)
  w_MININI_ORIG = space(2)
  w_CADURORE = 0
  w_CADURMIN = 0
  w_CADTIN = ctot('')
  w_CAORASYS = space(1)
  w_CAMINPRE = 0
  w_CAFLPREA = space(1)
  w_ATCAUATT_ORIG = space(20)
  w_ATOGGETT_ORIG = space(254)
  w_DESCRI = space(50)
  w_Lun = 0
  w_Mar = 0
  w_Mer = 0
  w_Gio = 0
  w_Ven = 0
  w_Sab = 0
  w_Dom = 0
  w_Gen = 0
  w_Feb = 0
  w_Marz = 0
  w_Apr = 0
  w_Mag = 0
  w_Giu = 0
  w_Lug = 0
  w_Ago = 0
  w_Set = 0
  w_Ott = 0
  w_ORAFIN_OR = space(2)
  w_MINFIN_OR = space(2)
  w_Nov = 0
  w_Dic = 0
  w_TIPRIC = space(1)
  o_TIPRIC = space(1)
  w_DATAENT = ctod('  /  /  ')
  o_DATAENT = ctod('  /  /  ')
  w_NUMOCC = 0
  w_DATA_ENTRO = ctot('')
  w_SHOWWEEK = space(1)
  w_SHOWMONT = space(1)
  w_ATOGGETT_ORIG = space(254)
  w_ORAINI_OR = space(2)
  w_MININI_OR = space(2)
  w_CALEND = space(5)
  w_MDESCLUS = space(1)
  o_MDESCLUS = space(1)
  w_MDCALEND = space(5)
  w_MDORAINI = 0
  w_MDORAFIN = 0
  w_TCDESCRI = space(40)
  w_MDESCLUS = space(1)
  w_LBL = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_karPag1","gsag_kar",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMETODO_1_9
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_LBL = this.oPgFrm.Pages(1).oPag.LBL
    DoDefault()
    proc Destroy()
      this.w_LBL = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='PAR_AGEN'
    this.cWorkTables[2]='MODCLDAT'
    this.cWorkTables[3]='CAUMATTI'
    this.cWorkTables[4]='TAB_CALE'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAG_BAR with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PAR_AGEN=space(5)
      .w_INIT_METODO=space(3)
      .w_TCFLDEFA=space(1)
      .w_TCCODICE=space(5)
      .w_DATINI_OR=ctod("  /  /  ")
      .w_DATFIN_OR=ctod("  /  /  ")
      .w_GIOINI=space(3)
      .w_GIOFIN=space(3)
      .w_METODO=space(3)
      .w_MD__WEEK=0
      .w_MD_MONTH=0
      .w_MD__FREQ=space(1)
      .w_MDDATUNI=ctod("  /  /  ")
      .w_MDNUMFRQ=0
      .w_MDMININT=0
      .w_MDIMPDAT=space(1)
      .w_MD_GGFIS=0
      .w_ATCAUATT_ORIG=space(20)
      .w_CARAGGST=space(1)
      .w_PACHKFES=space(1)
      .w_ATSERIAL_ORIG=space(20)
      .w_ATCAITER_ORIG=space(20)
      .w_ATNOTPIA_ORIG=space(0)
      .w_ATCODPRA_ORIG=space(15)
      .w_ATCENCOS_ORIG=space(15)
      .w_ATCODNOM_ORIG=space(15)
      .w_ATTELEFO_ORIG=space(18)
      .w_ATCELLUL_ORIG=space(18)
      .w_AT_EMAIL_ORIG=space(0)
      .w_AT_EMPEC_ORIG=space(0)
      .w_ATLOCALI_ORIG=space(30)
      .w_AT__ENTE_ORIG=space(10)
      .w_ATUFFICI_ORIG=space(10)
      .w_ATCODSED_ORIG=space(5)
      .w_ATPERSON_ORIG=space(60)
      .w_DATINI_ORIG=ctod("  /  /  ")
      .w_ORAINI_ORIG=space(2)
      .w_MININI_ORIG=space(2)
      .w_CADURORE=0
      .w_CADURMIN=0
      .w_CADTIN=ctot("")
      .w_CAORASYS=space(1)
      .w_CAMINPRE=0
      .w_CAFLPREA=space(1)
      .w_ATCAUATT_ORIG=space(20)
      .w_ATOGGETT_ORIG=space(254)
      .w_DESCRI=space(50)
      .w_Lun=0
      .w_Mar=0
      .w_Mer=0
      .w_Gio=0
      .w_Ven=0
      .w_Sab=0
      .w_Dom=0
      .w_Gen=0
      .w_Feb=0
      .w_Marz=0
      .w_Apr=0
      .w_Mag=0
      .w_Giu=0
      .w_Lug=0
      .w_Ago=0
      .w_Set=0
      .w_Ott=0
      .w_ORAFIN_OR=space(2)
      .w_MINFIN_OR=space(2)
      .w_Nov=0
      .w_Dic=0
      .w_TIPRIC=space(1)
      .w_DATAENT=ctod("  /  /  ")
      .w_NUMOCC=0
      .w_DATA_ENTRO=ctot("")
      .w_SHOWWEEK=space(1)
      .w_SHOWMONT=space(1)
      .w_ATOGGETT_ORIG=space(254)
      .w_ORAINI_OR=space(2)
      .w_MININI_OR=space(2)
      .w_CALEND=space(5)
      .w_MDESCLUS=space(1)
      .w_MDCALEND=space(5)
      .w_MDORAINI=0
      .w_MDORAFIN=0
      .w_TCDESCRI=space(40)
      .w_MDESCLUS=space(1)
        .w_PAR_AGEN = i_CodAzi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PAR_AGEN))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_TCFLDEFA = "S"
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_TCFLDEFA))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_DATINI_OR = this.oParentObject.w_DATINI
        .w_DATFIN_OR = this.oParentObject.w_DATFIN
        .w_GIOINI = iif(!EMPTY(.w_DATINI_OR), LEFT(g_GIORNO[DOW(.w_DATINI_OR)], 3) , SPACE(3))
        .w_GIOFIN = iif(!EMPTY(.w_DATFIN_OR), LEFT(g_GIORNO[DOW(.w_DATFIN_OR)], 3) , SPACE(3))
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_METODO))
          .link_1_9('Full')
        endif
          .DoRTCalc(10,11,.f.)
        .w_MD__FREQ = 'S'
          .DoRTCalc(13,17,.f.)
        .w_ATCAUATT_ORIG = this.oParentObject.w_ATCAUATT
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_ATCAUATT_ORIG))
          .link_1_19('Full')
        endif
          .DoRTCalc(19,20,.f.)
        .w_ATSERIAL_ORIG = this.oParentObject.w_ATSERIAL
        .w_ATCAITER_ORIG = this.oParentObject.w_ATCAITER
        .w_ATNOTPIA_ORIG = this.oParentObject.w_ATNOTPIA
        .w_ATCODPRA_ORIG = this.oParentObject.w_ATCODPRA
        .w_ATCENCOS_ORIG = this.oParentObject.w_ATCENCOS
        .w_ATCODNOM_ORIG = this.oParentObject.w_ATCODNOM
        .w_ATTELEFO_ORIG = this.oParentObject.w_ATTELEFO
        .w_ATCELLUL_ORIG = this.oParentObject.w_ATCELLUL
        .w_AT_EMAIL_ORIG = this.oParentObject.w_AT_EMAIL
        .w_AT_EMPEC_ORIG = this.oParentObject.w_AT_EMPEC
        .w_ATLOCALI_ORIG = this.oParentObject.w_ATLOCALI
        .w_AT__ENTE_ORIG = this.oParentObject.w_AT__ENTE
        .w_ATUFFICI_ORIG = this.oParentObject.w_ATUFFICI
        .w_ATCODSED_ORIG = this.oParentObject.w_ATCODSED
        .w_ATPERSON_ORIG = this.oParentObject.w_ATPERSON
        .w_DATINI_ORIG = this.oParentObject.w_DATINI
        .w_ORAINI_ORIG = this.oParentObject.w_ORAINI
        .w_MININI_ORIG = this.oParentObject.w_MININI
          .DoRTCalc(39,44,.f.)
        .w_ATCAUATT_ORIG = this.oParentObject.w_ATCAUATT
        .w_ATOGGETT_ORIG = this.oParentObject.w_ATOGGETT
          .DoRTCalc(47,47,.f.)
        .w_Lun = BITAND(.w_MD__WEEK, 0x01)
        .w_Mar = BITAND(.w_MD__WEEK, 0x02)
        .w_Mer = BITAND(.w_MD__WEEK, 0x04)
        .w_Gio = BITAND(.w_MD__WEEK, 0x08)
        .w_Ven = BITAND(.w_MD__WEEK, 0x10)
        .w_Sab = BITAND(.w_MD__WEEK, 0x20)
        .w_Dom = BITAND(.w_MD__WEEK, 0x40)
        .w_Gen = BITAND(.w_MD_MONTH, 0x0001)
        .w_Feb = BITAND(.w_MD_MONTH, 0x0002)
        .w_Marz = BITAND(.w_MD_MONTH, 0x0004)
        .w_Apr = BITAND(.w_MD_MONTH, 0x0008)
        .w_Mag = BITAND(.w_MD_MONTH, 0x0010)
        .w_Giu = BITAND(.w_MD_MONTH, 0x0020)
        .w_Lug = BITAND(.w_MD_MONTH, 0x0040)
        .w_Ago = BITAND(.w_MD_MONTH, 0x0080)
        .w_Set = BITAND(.w_MD_MONTH, 0x0100)
        .w_Ott = BITAND(.w_MD_MONTH, 0x0200)
        .w_ORAFIN_OR = iif(Empty(this.oParentObject.w_ORAFIN),'00',this.oParentObject.w_ORAFIN)
        .w_MINFIN_OR = iif(Empty(this.oParentObject.w_MINFIN),'00',this.oParentObject.w_MINFIN)
        .w_Nov = BITAND(.w_MD_MONTH, 0x0400)
        .w_Dic = BITAND(.w_MD_MONTH, 0x0800)
        .w_TIPRIC = iif(.w_MD__FREQ<>'U' and Not Empty(.w_TIPRIC),.w_TIPRIC,'D')
        .w_DATAENT = i_DATSYS
        .w_NUMOCC = iif(.w_MD__FREQ<>'U'  and .w_NUMOCC>0,.w_NUMOCC,2)
        .w_DATA_ENTRO = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAENT),  DTOC(.w_DATAENT)+' '+.w_ORAFIN_OR+':'+.w_MINFIN_OR+':00', '  -  -       :  :  '))
        .w_SHOWWEEK = IIF( .w_MD__FREQ $ "G-S-Q", "S", "N")
        .w_SHOWMONT = IIF( .w_MD__FREQ $ "M-A", "S", "N")
        .w_ATOGGETT_ORIG = this.oParentObject.w_ATOGGETT
        .w_ORAINI_OR = this.oParentObject.w_ORAINI
        .w_MININI_OR = this.oParentObject.w_MININI
          .DoRTCalc(78,79,.f.)
        .w_MDCALEND = iif(.w_MDESCLUS='C',IIF(Empty(.w_CALEND),.w_TCCODICE,.w_CALEND),Space(5))
        .DoRTCalc(80,80,.f.)
        if not(empty(.w_MDCALEND))
          .link_1_96('Full')
        endif
      .oPgFrm.Page1.oPag.LBL.Calculate(ICASE(.w_MD__FREQ='G',ah_msgformat('Giorno\i'), .w_MD__FREQ='S', ah_msgformat('Settimana\e'), .w_MD__FREQ='M', ah_msgformat('Mese\i'), .w_MD__FREQ='A', ah_msgformat('Anno\i'), ''))
          .DoRTCalc(81,83,.f.)
        .w_MDESCLUS = iif(.w_MD__FREQ<>'U',.w_MDESCLUS,'N')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,6,.t.)
        if .o_DATINI_OR<>.w_DATINI_OR
            .w_GIOINI = iif(!EMPTY(.w_DATINI_OR), LEFT(g_GIORNO[DOW(.w_DATINI_OR)], 3) , SPACE(3))
        endif
        if .o_DATFIN_OR<>.w_DATFIN_OR
            .w_GIOFIN = iif(!EMPTY(.w_DATFIN_OR), LEFT(g_GIORNO[DOW(.w_DATFIN_OR)], 3) , SPACE(3))
        endif
        .DoRTCalc(9,17,.t.)
            .w_ATCAUATT_ORIG = this.oParentObject.w_ATCAUATT
          .link_1_19('Full')
        .DoRTCalc(19,47,.t.)
        if .o_METODO<>.w_METODO
            .w_Lun = BITAND(.w_MD__WEEK, 0x01)
        endif
        if .o_METODO<>.w_METODO
            .w_Mar = BITAND(.w_MD__WEEK, 0x02)
        endif
        if .o_METODO<>.w_METODO
            .w_Mer = BITAND(.w_MD__WEEK, 0x04)
        endif
        if .o_METODO<>.w_METODO
            .w_Gio = BITAND(.w_MD__WEEK, 0x08)
        endif
        if .o_METODO<>.w_METODO
            .w_Ven = BITAND(.w_MD__WEEK, 0x10)
        endif
        if .o_METODO<>.w_METODO
            .w_Sab = BITAND(.w_MD__WEEK, 0x20)
        endif
        if .o_METODO<>.w_METODO
            .w_Dom = BITAND(.w_MD__WEEK, 0x40)
        endif
        if .o_METODO<>.w_METODO
            .w_Gen = BITAND(.w_MD_MONTH, 0x0001)
        endif
        if .o_METODO<>.w_METODO
            .w_Feb = BITAND(.w_MD_MONTH, 0x0002)
        endif
        if .o_METODO<>.w_METODO
            .w_Marz = BITAND(.w_MD_MONTH, 0x0004)
        endif
        if .o_METODO<>.w_METODO
            .w_Apr = BITAND(.w_MD_MONTH, 0x0008)
        endif
        if .o_METODO<>.w_METODO
            .w_Mag = BITAND(.w_MD_MONTH, 0x0010)
        endif
        if .o_METODO<>.w_METODO
            .w_Giu = BITAND(.w_MD_MONTH, 0x0020)
        endif
        if .o_METODO<>.w_METODO
            .w_Lug = BITAND(.w_MD_MONTH, 0x0040)
        endif
        if .o_METODO<>.w_METODO
            .w_Ago = BITAND(.w_MD_MONTH, 0x0080)
        endif
        if .o_METODO<>.w_METODO
            .w_Set = BITAND(.w_MD_MONTH, 0x0100)
        endif
        if .o_METODO<>.w_METODO
            .w_Ott = BITAND(.w_MD_MONTH, 0x0200)
        endif
        .DoRTCalc(65,66,.t.)
        if .o_METODO<>.w_METODO
            .w_Nov = BITAND(.w_MD_MONTH, 0x0400)
        endif
        if .o_METODO<>.w_METODO
            .w_Dic = BITAND(.w_MD_MONTH, 0x0800)
        endif
        if .o_MD__FREQ<>.w_MD__FREQ
            .w_TIPRIC = iif(.w_MD__FREQ<>'U' and Not Empty(.w_TIPRIC),.w_TIPRIC,'D')
        endif
        if .o_TIPRIC<>.w_TIPRIC
            .w_DATAENT = i_DATSYS
        endif
        if .o_TIPRIC<>.w_TIPRIC.or. .o_MD__FREQ<>.w_MD__FREQ
            .w_NUMOCC = iif(.w_MD__FREQ<>'U'  and .w_NUMOCC>0,.w_NUMOCC,2)
        endif
        if .o_TIPRIC<>.w_TIPRIC.or. .o_DATAENT<>.w_DATAENT
            .w_DATA_ENTRO = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAENT),  DTOC(.w_DATAENT)+' '+.w_ORAFIN_OR+':'+.w_MINFIN_OR+':00', '  -  -       :  :  '))
        endif
            .w_SHOWWEEK = IIF( .w_MD__FREQ $ "G-S-Q", "S", "N")
            .w_SHOWMONT = IIF( .w_MD__FREQ $ "M-A", "S", "N")
        .DoRTCalc(75,79,.t.)
        if .o_MDESCLUS<>.w_MDESCLUS
            .w_MDCALEND = iif(.w_MDESCLUS='C',IIF(Empty(.w_CALEND),.w_TCCODICE,.w_CALEND),Space(5))
          .link_1_96('Full')
        endif
        .oPgFrm.Page1.oPag.LBL.Calculate(ICASE(.w_MD__FREQ='G',ah_msgformat('Giorno\i'), .w_MD__FREQ='S', ah_msgformat('Settimana\e'), .w_MD__FREQ='M', ah_msgformat('Mese\i'), .w_MD__FREQ='A', ah_msgformat('Anno\i'), ''))
        .DoRTCalc(81,83,.t.)
        if .o_MD__FREQ<>.w_MD__FREQ
            .w_MDESCLUS = iif(.w_MD__FREQ<>'U',.w_MDESCLUS,'N')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.LBL.Calculate(ICASE(.w_MD__FREQ='G',ah_msgformat('Giorno\i'), .w_MD__FREQ='S', ah_msgformat('Settimana\e'), .w_MD__FREQ='M', ah_msgformat('Mese\i'), .w_MD__FREQ='A', ah_msgformat('Anno\i'), ''))
    endwith
  return

  proc Calculate_EZKHVDUBVZ()
    with this
          * --- Inizializzo metodo
          .w_METODO = .w_INIT_METODO
          .link_1_9('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTIPRIC_1_76.enabled = this.oPgFrm.Page1.oPag.oTIPRIC_1_76.mCond()
    this.oPgFrm.Page1.oPag.oNUMOCC_1_78.enabled = this.oPgFrm.Page1.oPag.oNUMOCC_1_78.mCond()
    this.oPgFrm.Page1.oPag.oMDCALEND_1_96.enabled = this.oPgFrm.Page1.oPag.oMDCALEND_1_96.mCond()
    this.oPgFrm.Page1.oPag.oMDESCLUS_1_110.enabled = this.oPgFrm.Page1.oPag.oMDESCLUS_1_110.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMD__FREQ_1_13.visible=!this.oPgFrm.Page1.oPag.oMD__FREQ_1_13.mHide()
    this.oPgFrm.Page1.oPag.oMDDATUNI_1_14.visible=!this.oPgFrm.Page1.oPag.oMDDATUNI_1_14.mHide()
    this.oPgFrm.Page1.oPag.oMDNUMFRQ_1_15.visible=!this.oPgFrm.Page1.oPag.oMDNUMFRQ_1_15.mHide()
    this.oPgFrm.Page1.oPag.oMDMININT_1_16.visible=!this.oPgFrm.Page1.oPag.oMDMININT_1_16.mHide()
    this.oPgFrm.Page1.oPag.oMDIMPDAT_1_17.visible=!this.oPgFrm.Page1.oPag.oMDIMPDAT_1_17.mHide()
    this.oPgFrm.Page1.oPag.oMD_GGFIS_1_18.visible=!this.oPgFrm.Page1.oPag.oMD_GGFIS_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oLun_1_55.visible=!this.oPgFrm.Page1.oPag.oLun_1_55.mHide()
    this.oPgFrm.Page1.oPag.oMar_1_56.visible=!this.oPgFrm.Page1.oPag.oMar_1_56.mHide()
    this.oPgFrm.Page1.oPag.oMer_1_57.visible=!this.oPgFrm.Page1.oPag.oMer_1_57.mHide()
    this.oPgFrm.Page1.oPag.oGio_1_58.visible=!this.oPgFrm.Page1.oPag.oGio_1_58.mHide()
    this.oPgFrm.Page1.oPag.oVen_1_59.visible=!this.oPgFrm.Page1.oPag.oVen_1_59.mHide()
    this.oPgFrm.Page1.oPag.oSab_1_60.visible=!this.oPgFrm.Page1.oPag.oSab_1_60.mHide()
    this.oPgFrm.Page1.oPag.oDom_1_61.visible=!this.oPgFrm.Page1.oPag.oDom_1_61.mHide()
    this.oPgFrm.Page1.oPag.oGen_1_62.visible=!this.oPgFrm.Page1.oPag.oGen_1_62.mHide()
    this.oPgFrm.Page1.oPag.oFeb_1_63.visible=!this.oPgFrm.Page1.oPag.oFeb_1_63.mHide()
    this.oPgFrm.Page1.oPag.oMarz_1_64.visible=!this.oPgFrm.Page1.oPag.oMarz_1_64.mHide()
    this.oPgFrm.Page1.oPag.oApr_1_65.visible=!this.oPgFrm.Page1.oPag.oApr_1_65.mHide()
    this.oPgFrm.Page1.oPag.oMag_1_66.visible=!this.oPgFrm.Page1.oPag.oMag_1_66.mHide()
    this.oPgFrm.Page1.oPag.oGiu_1_67.visible=!this.oPgFrm.Page1.oPag.oGiu_1_67.mHide()
    this.oPgFrm.Page1.oPag.oLug_1_68.visible=!this.oPgFrm.Page1.oPag.oLug_1_68.mHide()
    this.oPgFrm.Page1.oPag.oAgo_1_69.visible=!this.oPgFrm.Page1.oPag.oAgo_1_69.mHide()
    this.oPgFrm.Page1.oPag.oSet_1_70.visible=!this.oPgFrm.Page1.oPag.oSet_1_70.mHide()
    this.oPgFrm.Page1.oPag.oOtt_1_71.visible=!this.oPgFrm.Page1.oPag.oOtt_1_71.mHide()
    this.oPgFrm.Page1.oPag.oNov_1_74.visible=!this.oPgFrm.Page1.oPag.oNov_1_74.mHide()
    this.oPgFrm.Page1.oPag.oDic_1_75.visible=!this.oPgFrm.Page1.oPag.oDic_1_75.mHide()
    this.oPgFrm.Page1.oPag.oDATAENT_1_77.visible=!this.oPgFrm.Page1.oPag.oDATAENT_1_77.mHide()
    this.oPgFrm.Page1.oPag.oNUMOCC_1_78.visible=!this.oPgFrm.Page1.oPag.oNUMOCC_1_78.mHide()
    this.oPgFrm.Page1.oPag.oMDESCLUS_1_95.visible=!this.oPgFrm.Page1.oPag.oMDESCLUS_1_95.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_97.visible=!this.oPgFrm.Page1.oPag.oStr_1_97.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_100.visible=!this.oPgFrm.Page1.oPag.oStr_1_100.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_103.visible=!this.oPgFrm.Page1.oPag.oStr_1_103.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_105.visible=!this.oPgFrm.Page1.oPag.oStr_1_105.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_106.visible=!this.oPgFrm.Page1.oPag.oStr_1_106.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_107.visible=!this.oPgFrm.Page1.oPag.oStr_1_107.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_108.visible=!this.oPgFrm.Page1.oPag.oStr_1_108.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_109.visible=!this.oPgFrm.Page1.oPag.oStr_1_109.mHide()
    this.oPgFrm.Page1.oPag.oMDESCLUS_1_110.visible=!this.oPgFrm.Page1.oPag.oMDESCLUS_1_110.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_111.visible=!this.oPgFrm.Page1.oPag.oStr_1_111.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_112.visible=!this.oPgFrm.Page1.oPag.oStr_1_112.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_113.visible=!this.oPgFrm.Page1.oPag.oStr_1_113.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_114.visible=!this.oPgFrm.Page1.oPag.oStr_1_114.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_115.visible=!this.oPgFrm.Page1.oPag.oStr_1_115.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_116.visible=!this.oPgFrm.Page1.oPag.oStr_1_116.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_117.visible=!this.oPgFrm.Page1.oPag.oStr_1_117.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_EZKHVDUBVZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.LBL.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kar
    IF CEVENT='Init'
       This.mcalc(.t.)
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PAR_AGEN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAR_AGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAR_AGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACHKFES,PACODMDC";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PAR_AGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PAR_AGEN)
            select PACODAZI,PACHKFES,PACODMDC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAR_AGEN = NVL(_Link_.PACODAZI,space(5))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
      this.w_INIT_METODO = NVL(_Link_.PACODMDC,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PAR_AGEN = space(5)
      endif
      this.w_PACHKFES = space(1)
      this.w_INIT_METODO = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAR_AGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TCFLDEFA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TCFLDEFA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TCFLDEFA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCFLDEFA,TCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TCFLDEFA="+cp_ToStrODBC(this.w_TCFLDEFA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCFLDEFA',this.w_TCFLDEFA)
            select TCFLDEFA,TCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TCFLDEFA = NVL(_Link_.TCFLDEFA,space(1))
      this.w_TCCODICE = NVL(_Link_.TCCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TCFLDEFA = space(1)
      endif
      this.w_TCCODICE = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCFLDEFA,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TCFLDEFA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=METODO
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_METODO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_amd',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_METODO)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI,MD__FREQ,MD__WEEK,MD_MONTH,MDESCLUS,MDCALEND,MDNUMFRQ,MDDATUNI,MDIMPDAT,MD_GGFIS,MDORAINI,MDORAFIN,MDMININT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_METODO))
          select MDCODICE,MDDESCRI,MD__FREQ,MD__WEEK,MD_MONTH,MDESCLUS,MDCALEND,MDNUMFRQ,MDDATUNI,MDIMPDAT,MD_GGFIS,MDORAINI,MDORAFIN,MDMININT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_METODO)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStrODBC(trim(this.w_METODO)+"%");

            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI,MD__FREQ,MD__WEEK,MD_MONTH,MDESCLUS,MDCALEND,MDNUMFRQ,MDDATUNI,MDIMPDAT,MD_GGFIS,MDORAINI,MDORAFIN,MDMININT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStr(trim(this.w_METODO)+"%");

            select MDCODICE,MDDESCRI,MD__FREQ,MD__WEEK,MD_MONTH,MDESCLUS,MDCALEND,MDNUMFRQ,MDDATUNI,MDIMPDAT,MD_GGFIS,MDORAINI,MDORAFIN,MDMININT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_METODO) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oMETODO_1_9'),i_cWhere,'gsar_amd',"Metodi di calcolo della periodicit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI,MD__FREQ,MD__WEEK,MD_MONTH,MDESCLUS,MDCALEND,MDNUMFRQ,MDDATUNI,MDIMPDAT,MD_GGFIS,MDORAINI,MDORAFIN,MDMININT";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI,MD__FREQ,MD__WEEK,MD_MONTH,MDESCLUS,MDCALEND,MDNUMFRQ,MDDATUNI,MDIMPDAT,MD_GGFIS,MDORAINI,MDORAFIN,MDMININT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_METODO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI,MD__FREQ,MD__WEEK,MD_MONTH,MDESCLUS,MDCALEND,MDNUMFRQ,MDDATUNI,MDIMPDAT,MD_GGFIS,MDORAINI,MDORAFIN,MDMININT";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_METODO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_METODO)
            select MDCODICE,MDDESCRI,MD__FREQ,MD__WEEK,MD_MONTH,MDESCLUS,MDCALEND,MDNUMFRQ,MDDATUNI,MDIMPDAT,MD_GGFIS,MDORAINI,MDORAFIN,MDMININT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_METODO = NVL(_Link_.MDCODICE,space(3))
      this.w_DESCRI = NVL(_Link_.MDDESCRI,space(50))
      this.w_MD__FREQ = NVL(_Link_.MD__FREQ,space(1))
      this.w_MD__WEEK = NVL(_Link_.MD__WEEK,0)
      this.w_MD_MONTH = NVL(_Link_.MD_MONTH,0)
      this.w_MDESCLUS = NVL(_Link_.MDESCLUS,space(1))
      this.w_CALEND = NVL(_Link_.MDCALEND,space(5))
      this.w_MDNUMFRQ = NVL(_Link_.MDNUMFRQ,0)
      this.w_MDDATUNI = NVL(cp_ToDate(_Link_.MDDATUNI),ctod("  /  /  "))
      this.w_MDIMPDAT = NVL(_Link_.MDIMPDAT,space(1))
      this.w_MD_GGFIS = NVL(_Link_.MD_GGFIS,0)
      this.w_MDORAINI = NVL(_Link_.MDORAINI,0)
      this.w_MDORAFIN = NVL(_Link_.MDORAFIN,0)
      this.w_MDMININT = NVL(_Link_.MDMININT,0)
    else
      if i_cCtrl<>'Load'
        this.w_METODO = space(3)
      endif
      this.w_DESCRI = space(50)
      this.w_MD__FREQ = space(1)
      this.w_MD__WEEK = 0
      this.w_MD_MONTH = 0
      this.w_MDESCLUS = space(1)
      this.w_CALEND = space(5)
      this.w_MDNUMFRQ = 0
      this.w_MDDATUNI = ctod("  /  /  ")
      this.w_MDIMPDAT = space(1)
      this.w_MD_GGFIS = 0
      this.w_MDORAINI = 0
      this.w_MDORAFIN = 0
      this.w_MDMININT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_METODO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCAUATT_ORIG
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCAUATT_ORIG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCAUATT_ORIG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CARAGGST,CADURORE,CADURMIN,CADATINI,CAORASYS,CAMINPRE,CAFLPREA";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ATCAUATT_ORIG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ATCAUATT_ORIG)
            select CACODICE,CARAGGST,CADURORE,CADURMIN,CADATINI,CAORASYS,CAMINPRE,CAFLPREA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCAUATT_ORIG = NVL(_Link_.CACODICE,space(20))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(1))
      this.w_CADURORE = NVL(_Link_.CADURORE,0)
      this.w_CADURMIN = NVL(_Link_.CADURMIN,0)
      this.w_CADTIN = NVL(_Link_.CADATINI,ctot(""))
      this.w_CAORASYS = NVL(_Link_.CAORASYS,space(1))
      this.w_CAMINPRE = NVL(_Link_.CAMINPRE,0)
      this.w_CAFLPREA = NVL(_Link_.CAFLPREA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCAUATT_ORIG = space(20)
      endif
      this.w_CARAGGST = space(1)
      this.w_CADURORE = 0
      this.w_CADURMIN = 0
      this.w_CADTIN = ctot("")
      this.w_CAORASYS = space(1)
      this.w_CAMINPRE = 0
      this.w_CAFLPREA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CARAGGST#'Z'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATCAUATT_ORIG = space(20)
        this.w_CARAGGST = space(1)
        this.w_CADURORE = 0
        this.w_CADURMIN = 0
        this.w_CADTIN = ctot("")
        this.w_CAORASYS = space(1)
        this.w_CAMINPRE = 0
        this.w_CAFLPREA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCAUATT_ORIG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MDCALEND
  func Link_1_96(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MDCALEND) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATL',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_MDCALEND)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_MDCALEND))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MDCALEND)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStrODBC(trim(this.w_MDCALEND)+"%");

            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TCDESCRI like "+cp_ToStr(trim(this.w_MDCALEND)+"%");

            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MDCALEND) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oMDCALEND_1_96'),i_cWhere,'GSAR_ATL',"Calendari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MDCALEND)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_MDCALEND);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_MDCALEND)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MDCALEND = NVL(_Link_.TCCODICE,space(5))
      this.w_TCDESCRI = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MDCALEND = space(5)
      endif
      this.w_TCDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MDCALEND Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_OR_1_5.value==this.w_DATINI_OR)
      this.oPgFrm.Page1.oPag.oDATINI_OR_1_5.value=this.w_DATINI_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_OR_1_6.value==this.w_DATFIN_OR)
      this.oPgFrm.Page1.oPag.oDATFIN_OR_1_6.value=this.w_DATFIN_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOINI_1_7.value==this.w_GIOINI)
      this.oPgFrm.Page1.oPag.oGIOINI_1_7.value=this.w_GIOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOFIN_1_8.value==this.w_GIOFIN)
      this.oPgFrm.Page1.oPag.oGIOFIN_1_8.value=this.w_GIOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMETODO_1_9.RadioValue()==this.w_METODO)
      this.oPgFrm.Page1.oPag.oMETODO_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMD__FREQ_1_13.RadioValue()==this.w_MD__FREQ)
      this.oPgFrm.Page1.oPag.oMD__FREQ_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMDDATUNI_1_14.value==this.w_MDDATUNI)
      this.oPgFrm.Page1.oPag.oMDDATUNI_1_14.value=this.w_MDDATUNI
    endif
    if not(this.oPgFrm.Page1.oPag.oMDNUMFRQ_1_15.value==this.w_MDNUMFRQ)
      this.oPgFrm.Page1.oPag.oMDNUMFRQ_1_15.value=this.w_MDNUMFRQ
    endif
    if not(this.oPgFrm.Page1.oPag.oMDMININT_1_16.value==this.w_MDMININT)
      this.oPgFrm.Page1.oPag.oMDMININT_1_16.value=this.w_MDMININT
    endif
    if not(this.oPgFrm.Page1.oPag.oMDIMPDAT_1_17.RadioValue()==this.w_MDIMPDAT)
      this.oPgFrm.Page1.oPag.oMDIMPDAT_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMD_GGFIS_1_18.value==this.w_MD_GGFIS)
      this.oPgFrm.Page1.oPag.oMD_GGFIS_1_18.value=this.w_MD_GGFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oATCAUATT_ORIG_1_19.value==this.w_ATCAUATT_ORIG)
      this.oPgFrm.Page1.oPag.oATCAUATT_ORIG_1_19.value=this.w_ATCAUATT_ORIG
    endif
    if not(this.oPgFrm.Page1.oPag.oLun_1_55.RadioValue()==this.w_Lun)
      this.oPgFrm.Page1.oPag.oLun_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMar_1_56.RadioValue()==this.w_Mar)
      this.oPgFrm.Page1.oPag.oMar_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMer_1_57.RadioValue()==this.w_Mer)
      this.oPgFrm.Page1.oPag.oMer_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGio_1_58.RadioValue()==this.w_Gio)
      this.oPgFrm.Page1.oPag.oGio_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVen_1_59.RadioValue()==this.w_Ven)
      this.oPgFrm.Page1.oPag.oVen_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSab_1_60.RadioValue()==this.w_Sab)
      this.oPgFrm.Page1.oPag.oSab_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDom_1_61.RadioValue()==this.w_Dom)
      this.oPgFrm.Page1.oPag.oDom_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGen_1_62.RadioValue()==this.w_Gen)
      this.oPgFrm.Page1.oPag.oGen_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFeb_1_63.RadioValue()==this.w_Feb)
      this.oPgFrm.Page1.oPag.oFeb_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMarz_1_64.RadioValue()==this.w_Marz)
      this.oPgFrm.Page1.oPag.oMarz_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oApr_1_65.RadioValue()==this.w_Apr)
      this.oPgFrm.Page1.oPag.oApr_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMag_1_66.RadioValue()==this.w_Mag)
      this.oPgFrm.Page1.oPag.oMag_1_66.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGiu_1_67.RadioValue()==this.w_Giu)
      this.oPgFrm.Page1.oPag.oGiu_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLug_1_68.RadioValue()==this.w_Lug)
      this.oPgFrm.Page1.oPag.oLug_1_68.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAgo_1_69.RadioValue()==this.w_Ago)
      this.oPgFrm.Page1.oPag.oAgo_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSet_1_70.RadioValue()==this.w_Set)
      this.oPgFrm.Page1.oPag.oSet_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOtt_1_71.RadioValue()==this.w_Ott)
      this.oPgFrm.Page1.oPag.oOtt_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_OR_1_72.value==this.w_ORAFIN_OR)
      this.oPgFrm.Page1.oPag.oORAFIN_OR_1_72.value=this.w_ORAFIN_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_OR_1_73.value==this.w_MINFIN_OR)
      this.oPgFrm.Page1.oPag.oMINFIN_OR_1_73.value=this.w_MINFIN_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oNov_1_74.RadioValue()==this.w_Nov)
      this.oPgFrm.Page1.oPag.oNov_1_74.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDic_1_75.RadioValue()==this.w_Dic)
      this.oPgFrm.Page1.oPag.oDic_1_75.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPRIC_1_76.RadioValue()==this.w_TIPRIC)
      this.oPgFrm.Page1.oPag.oTIPRIC_1_76.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAENT_1_77.value==this.w_DATAENT)
      this.oPgFrm.Page1.oPag.oDATAENT_1_77.value=this.w_DATAENT
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMOCC_1_78.value==this.w_NUMOCC)
      this.oPgFrm.Page1.oPag.oNUMOCC_1_78.value=this.w_NUMOCC
    endif
    if not(this.oPgFrm.Page1.oPag.oATOGGETT_ORIG_1_84.value==this.w_ATOGGETT_ORIG)
      this.oPgFrm.Page1.oPag.oATOGGETT_ORIG_1_84.value=this.w_ATOGGETT_ORIG
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_OR_1_87.value==this.w_ORAINI_OR)
      this.oPgFrm.Page1.oPag.oORAINI_OR_1_87.value=this.w_ORAINI_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_OR_1_88.value==this.w_MININI_OR)
      this.oPgFrm.Page1.oPag.oMININI_OR_1_88.value=this.w_MININI_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oMDESCLUS_1_95.RadioValue()==this.w_MDESCLUS)
      this.oPgFrm.Page1.oPag.oMDESCLUS_1_95.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMDCALEND_1_96.value==this.w_MDCALEND)
      this.oPgFrm.Page1.oPag.oMDCALEND_1_96.value=this.w_MDCALEND
    endif
    if not(this.oPgFrm.Page1.oPag.oTCDESCRI_1_102.value==this.w_TCDESCRI)
      this.oPgFrm.Page1.oPag.oTCDESCRI_1_102.value=this.w_TCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMDESCLUS_1_110.RadioValue()==this.w_MDESCLUS)
      this.oPgFrm.Page1.oPag.oMDESCLUS_1_110.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATFIN_OR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_OR_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN_OR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_METODO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMETODO_1_9.SetFocus()
            i_bnoObbl = !empty(.w_METODO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MDNUMFRQ)) or not(.w_MDNUMFRQ >0))  and not(Not (.w_MD__FREQ$"G-S-M-A"))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMDNUMFRQ_1_15.SetFocus()
            i_bnoObbl = !empty(.w_MDNUMFRQ)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La frequenza deve essere maggiore di 0")
          case   not(.w_MD_GGFIS>0 and .w_MD_GGFIS<=31)  and not(NOT( .w_MD__FREQ$'M-A' AND .w_MDIMPDAT$'D-G' ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMD_GGFIS_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore tra 1 e 31")
          case   not(.w_CARAGGST#'Z')  and not(empty(.w_ATCAUATT_ORIG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCAUATT_ORIG_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NUMOCC>1)  and not(.w_TIPRIC <>'D')  and (.w_MD__FREQ<>'U')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMOCC_1_78.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso! Nel numero di occorrenze deve essere inclusa l'attivit� di origine!")
          case   (empty(.w_MDCALEND))  and (.w_MDESCLUS='C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMDCALEND_1_96.SetFocus()
            i_bnoObbl = !empty(.w_MDCALEND)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_kar
      IF This.w_DATAENT < This.w_DATINI_OR and This.w_TIPRIC='E'
         i_bnoChk=.F.
         i_bRes=.F.
         i_cErrorMsg=Ah_MsgFormat("Data limite per generazione attivit� ricorrenti inferiore a quella dell'attivit� di origine")
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATINI_OR = this.w_DATINI_OR
    this.o_DATFIN_OR = this.w_DATFIN_OR
    this.o_METODO = this.w_METODO
    this.o_MD__FREQ = this.w_MD__FREQ
    this.o_TIPRIC = this.w_TIPRIC
    this.o_DATAENT = this.w_DATAENT
    this.o_MDESCLUS = this.w_MDESCLUS
    return

enddefine

* --- Define pages as container
define class tgsag_karPag1 as StdContainer
  Width  = 612
  height = 511
  stdWidth  = 612
  stdheight = 511
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_OR_1_5 as StdField with uid="PBNLMIZKKY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATINI_OR", cQueryName = "DATINI_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 163290203,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=85, Top=52

  add object oDATFIN_OR_1_6 as StdField with uid="SRJDXINBFP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATFIN_OR", cQueryName = "DATFIN_OR",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 84843611,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=85, Top=76

  add object oGIOINI_1_7 as StdField with uid="CLWGZXHNUV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_GIOINI", cQueryName = "GIOINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 163309978,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=49, Top=53, InputMask=replicate('X',3)

  add object oGIOFIN_1_8 as StdField with uid="HMZDOQVRPI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_GIOFIN", cQueryName = "GIOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 84863386,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=49, Top=76, InputMask=replicate('X',3)


  add object oMETODO_1_9 as StdTableCombo with uid="YYHKIEVTHB",rtseq=9,rtrep=.f.,left=61,top=116,width=363,height=21;
    , ToolTipText = "Metodo di calcolo della periodicit�";
    , HelpContextID = 72719674;
    , cFormVar="w_METODO",tablefilter="", bObbl = .t. , nPag = 1;
    , cLinkFile="MODCLDAT";
    , cTable='MODCLDAT',cKey='MDCODICE',cValue='MDDESCRI',cOrderBy='MDDESCRI',xDefault=space(3);
  , bGlobalFont=.t.


  func oMETODO_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMETODO_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oMD__FREQ_1_13 as StdCombo with uid="HLKJQVAHXB",rtseq=12,rtrep=.f.,left=128,top=142,width=172,height=21;
    , HelpContextID = 249238039;
    , cFormVar="w_MD__FREQ",RowSource=""+"Unica,"+"Giornaliera,"+"Settimanale,"+"Quindicinale (1-15),"+"Mensile,"+"Pluriennale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMD__FREQ_1_13.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'G',;
    iif(this.value =3,'S',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'M',;
    iif(this.value =6,'A',;
    space(1))))))))
  endfunc
  func oMD__FREQ_1_13.GetRadio()
    this.Parent.oContained.w_MD__FREQ = this.RadioValue()
    return .t.
  endfunc

  func oMD__FREQ_1_13.SetRadio()
    this.Parent.oContained.w_MD__FREQ=trim(this.Parent.oContained.w_MD__FREQ)
    this.value = ;
      iif(this.Parent.oContained.w_MD__FREQ=='U',1,;
      iif(this.Parent.oContained.w_MD__FREQ=='G',2,;
      iif(this.Parent.oContained.w_MD__FREQ=='S',3,;
      iif(this.Parent.oContained.w_MD__FREQ=='Q',4,;
      iif(this.Parent.oContained.w_MD__FREQ=='M',5,;
      iif(this.Parent.oContained.w_MD__FREQ=='A',6,;
      0))))))
  endfunc

  func oMD__FREQ_1_13.mHide()
    with this.Parent.oContained
      return (Empty(.w_METODO))
    endwith
  endfunc

  add object oMDDATUNI_1_14 as StdField with uid="GBCWLFJDVX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MDDATUNI", cQueryName = "MDDATUNI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 224697841,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=347, Top=142

  func oMDDATUNI_1_14.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'U')
    endwith
  endfunc

  add object oMDNUMFRQ_1_15 as StdField with uid="XHYSHNCPKJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MDNUMFRQ", cQueryName = "MDNUMFRQ",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La frequenza deve essere maggiore di 0",;
    ToolTipText = "Intervallo di frequenza",;
    HelpContextID = 213908969,;
   bGlobalFont=.t.,;
    Height=21, Width=36, Left=126, Top=171, cSayPict='"999"', cGetPict='"999"'

  func oMDNUMFRQ_1_15.mHide()
    with this.Parent.oContained
      return (Not (.w_MD__FREQ$"G-S-M-A"))
    endwith
  endfunc

  func oMDNUMFRQ_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MDNUMFRQ >0)
    endwith
    return bRes
  endfunc

  add object oMDMININT_1_16 as StdField with uid="DJLFRIDKKE",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MDMININT", cQueryName = "MDMININT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Intervallo in minuti di ripetibilit�",;
    HelpContextID = 163319270,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=347, Top=171, cSayPict='"9999"', cGetPict='"9999"'

  func oMDMININT_1_16.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc


  add object oMDIMPDAT_1_17 as StdCombo with uid="GZOFNZQNMX",rtseq=16,rtrep=.f.,left=126,top=203,width=172,height=21;
    , ToolTipText = "Fine mese/Data fissa/Quindicinale";
    , HelpContextID = 244862438;
    , cFormVar="w_MDIMPDAT",RowSource=""+"Nessun rinvio,"+"Nessun rinvio o Fine mese,"+"Fine mese,"+"Giorno fisso,"+"Fine mese + giorno fisso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMDIMPDAT_1_17.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'Z',;
    iif(this.value =3,'F',;
    iif(this.value =4,'D',;
    iif(this.value =5,'G',;
    space(1)))))))
  endfunc
  func oMDIMPDAT_1_17.GetRadio()
    this.Parent.oContained.w_MDIMPDAT = this.RadioValue()
    return .t.
  endfunc

  func oMDIMPDAT_1_17.SetRadio()
    this.Parent.oContained.w_MDIMPDAT=trim(this.Parent.oContained.w_MDIMPDAT)
    this.value = ;
      iif(this.Parent.oContained.w_MDIMPDAT=='N',1,;
      iif(this.Parent.oContained.w_MDIMPDAT=='Z',2,;
      iif(this.Parent.oContained.w_MDIMPDAT=='F',3,;
      iif(this.Parent.oContained.w_MDIMPDAT=='D',4,;
      iif(this.Parent.oContained.w_MDIMPDAT=='G',5,;
      0)))))
  endfunc

  func oMDIMPDAT_1_17.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'M' AND .w_MD__FREQ<>'A')
    endwith
  endfunc

  add object oMD_GGFIS_1_18 as StdField with uid="OAGFWVZWCO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MD_GGFIS", cQueryName = "MD_GGFIS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore tra 1 e 31",;
    ToolTipText = "Giorno per data fissa",;
    HelpContextID = 47387161,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=484, Top=203, cSayPict='"99"', cGetPict='"99"'

  func oMD_GGFIS_1_18.mHide()
    with this.Parent.oContained
      return (NOT( .w_MD__FREQ$'M-A' AND .w_MDIMPDAT$'D-G' ))
    endwith
  endfunc

  func oMD_GGFIS_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MD_GGFIS>0 and .w_MD_GGFIS<=31)
    endwith
    return bRes
  endfunc

  add object oATCAUATT_ORIG_1_19 as StdField with uid="TTYLZWPDVI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ATCAUATT_ORIG", cQueryName = "ATCAUATT_ORIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo attivita collegata",;
    HelpContextID = 57267786,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=49, Top=29, InputMask=replicate('X',20), cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_ATCAUATT_ORIG"

  func oATCAUATT_ORIG_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oBtn_1_20 as StdButton with uid="ERHOXVEEGI",left=502, top=454, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per generare una o piu' attivit� ricorrenti";
    , HelpContextID = 132757274;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_21 as StdButton with uid="TKCMXZXILU",left=554, top=454, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 125468602;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oLun_1_55 as StdCheck with uid="DWKRBZYVXJ",rtseq=48,rtrep=.f.,left=16, top=248, caption="Luned�",;
    ToolTipText = "Luned�",;
    HelpContextID = 132304202,;
    cFormVar="w_Lun", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLun_1_55.RadioValue()
    return(iif(this.value =1,0x01,;
    0))
  endfunc
  func oLun_1_55.GetRadio()
    this.Parent.oContained.w_Lun = this.RadioValue()
    return .t.
  endfunc

  func oLun_1_55.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Lun==0x01,1,;
      0)
  endfunc

  func oLun_1_55.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oMar_1_56 as StdCheck with uid="HNMBGUANZT",rtseq=49,rtrep=.f.,left=86, top=248, caption="Marted�",;
    ToolTipText = "Marted�",;
    HelpContextID = 132292922,;
    cFormVar="w_Mar", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMar_1_56.RadioValue()
    return(iif(this.value =1,0x02,;
    0))
  endfunc
  func oMar_1_56.GetRadio()
    this.Parent.oContained.w_Mar = this.RadioValue()
    return .t.
  endfunc

  func oMar_1_56.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Mar==0x02,1,;
      0)
  endfunc

  func oMar_1_56.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oMer_1_57 as StdCheck with uid="JEAPVCKXEA",rtseq=50,rtrep=.f.,left=158, top=248, caption="Mercoled�",;
    ToolTipText = "Mercoled�",;
    HelpContextID = 132291898,;
    cFormVar="w_Mer", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMer_1_57.RadioValue()
    return(iif(this.value =1,0x04,;
    0))
  endfunc
  func oMer_1_57.GetRadio()
    this.Parent.oContained.w_Mer = this.RadioValue()
    return .t.
  endfunc

  func oMer_1_57.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Mer==0x04,1,;
      0)
  endfunc

  func oMer_1_57.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oGio_1_58 as StdCheck with uid="MKDDNPQCDM",rtseq=51,rtrep=.f.,left=242, top=248, caption="Gioved�",;
    ToolTipText = "Gioved�",;
    HelpContextID = 132303258,;
    cFormVar="w_Gio", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGio_1_58.RadioValue()
    return(iif(this.value =1,0x08,;
    0))
  endfunc
  func oGio_1_58.GetRadio()
    this.Parent.oContained.w_Gio = this.RadioValue()
    return .t.
  endfunc

  func oGio_1_58.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Gio==0x08,1,;
      0)
  endfunc

  func oGio_1_58.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oVen_1_59 as StdCheck with uid="HTZYLSOFKR",rtseq=52,rtrep=.f.,left=308, top=248, caption="Venerd�",;
    ToolTipText = "Venerd�",;
    HelpContextID = 132308138,;
    cFormVar="w_Ven", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVen_1_59.RadioValue()
    return(iif(this.value =1,0x10,;
    0))
  endfunc
  func oVen_1_59.GetRadio()
    this.Parent.oContained.w_Ven = this.RadioValue()
    return .t.
  endfunc

  func oVen_1_59.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Ven==0x10,1,;
      0)
  endfunc

  func oVen_1_59.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oSab_1_60 as StdCheck with uid="BPNHBOTMXI",rtseq=53,rtrep=.f.,left=378, top=248, caption="Sabato",;
    ToolTipText = "Sabato",;
    HelpContextID = 132358362,;
    cFormVar="w_Sab", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSab_1_60.RadioValue()
    return(iif(this.value =1,0x20,;
    0))
  endfunc
  func oSab_1_60.GetRadio()
    this.Parent.oContained.w_Sab = this.RadioValue()
    return .t.
  endfunc

  func oSab_1_60.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Sab==0x20,1,;
      0)
  endfunc

  func oSab_1_60.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oDom_1_61 as StdCheck with uid="UYNGYBLVCG",rtseq=54,rtrep=.f.,left=450, top=248, caption="Domenica",;
    ToolTipText = "Domenica",;
    HelpContextID = 132309962,;
    cFormVar="w_Dom", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDom_1_61.RadioValue()
    return(iif(this.value =1,0x40,;
    0))
  endfunc
  func oDom_1_61.GetRadio()
    this.Parent.oContained.w_Dom = this.RadioValue()
    return .t.
  endfunc

  func oDom_1_61.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Dom==0x40,1,;
      0)
  endfunc

  func oDom_1_61.mHide()
    with this.Parent.oContained
      return (.w_SHOWWEEK<>'S')
    endwith
  endfunc

  add object oGen_1_62 as StdCheck with uid="WUMWUAEFSH",rtseq=55,rtrep=.f.,left=16, top=247, caption="Gennaio",;
    ToolTipText = "Gennaio",;
    HelpContextID = 132308378,;
    cFormVar="w_Gen", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGen_1_62.RadioValue()
    return(iif(this.value =1,0x0001,;
    0))
  endfunc
  func oGen_1_62.GetRadio()
    this.Parent.oContained.w_Gen = this.RadioValue()
    return .t.
  endfunc

  func oGen_1_62.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Gen==0x0001,1,;
      0)
  endfunc

  func oGen_1_62.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oFeb_1_63 as StdCheck with uid="VAYRBMKYYX",rtseq=56,rtrep=.f.,left=99, top=248, caption="Febbraio",;
    ToolTipText = "Febbraio",;
    HelpContextID = 132357546,;
    cFormVar="w_Feb", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFeb_1_63.RadioValue()
    return(iif(this.value =1,0x0002,;
    0))
  endfunc
  func oFeb_1_63.GetRadio()
    this.Parent.oContained.w_Feb = this.RadioValue()
    return .t.
  endfunc

  func oFeb_1_63.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Feb==0x0002,1,;
      0)
  endfunc

  func oFeb_1_63.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oMarz_1_64 as StdCheck with uid="CFPDFYPNDF",rtseq=57,rtrep=.f.,left=192, top=248, caption="Marzo",;
    ToolTipText = "Marzo",;
    HelpContextID = 124297530,;
    cFormVar="w_Marz", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMarz_1_64.RadioValue()
    return(iif(this.value =1,0x0004,;
    0))
  endfunc
  func oMarz_1_64.GetRadio()
    this.Parent.oContained.w_Marz = this.RadioValue()
    return .t.
  endfunc

  func oMarz_1_64.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Marz==0x0004,1,;
      0)
  endfunc

  func oMarz_1_64.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oApr_1_65 as StdCheck with uid="ZMFFJMGCGE",rtseq=58,rtrep=.f.,left=308, top=248, caption="Aprile",;
    ToolTipText = "Aprile",;
    HelpContextID = 132289274,;
    cFormVar="w_Apr", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oApr_1_65.RadioValue()
    return(iif(this.value =1,0x0008,;
    0))
  endfunc
  func oApr_1_65.GetRadio()
    this.Parent.oContained.w_Apr = this.RadioValue()
    return .t.
  endfunc

  func oApr_1_65.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Apr==0x0008,1,;
      0)
  endfunc

  func oApr_1_65.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oMag_1_66 as StdCheck with uid="RRZVXAQSCW",rtseq=59,rtrep=.f.,left=391, top=248, caption="Maggio",;
    ToolTipText = "Maggio",;
    HelpContextID = 132337978,;
    cFormVar="w_Mag", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMag_1_66.RadioValue()
    return(iif(this.value =1,0x0010,;
    0))
  endfunc
  func oMag_1_66.GetRadio()
    this.Parent.oContained.w_Mag = this.RadioValue()
    return .t.
  endfunc

  func oMag_1_66.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Mag==0x0010,1,;
      0)
  endfunc

  func oMag_1_66.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oGiu_1_67 as StdCheck with uid="XTRHLYDLFX",rtseq=60,rtrep=.f.,left=484, top=247, caption="Giugno",;
    ToolTipText = "Giugno",;
    HelpContextID = 132278682,;
    cFormVar="w_Giu", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGiu_1_67.RadioValue()
    return(iif(this.value =1,0x0020,;
    0))
  endfunc
  func oGiu_1_67.GetRadio()
    this.Parent.oContained.w_Giu = this.RadioValue()
    return .t.
  endfunc

  func oGiu_1_67.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Giu==0x0020,1,;
      0)
  endfunc

  func oGiu_1_67.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oLug_1_68 as StdCheck with uid="BIWUZCBSUZ",rtseq=61,rtrep=.f.,left=16, top=273, caption="Luglio",;
    ToolTipText = "Luglio",;
    HelpContextID = 132332874,;
    cFormVar="w_Lug", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLug_1_68.RadioValue()
    return(iif(this.value =1,0x0040,;
    0))
  endfunc
  func oLug_1_68.GetRadio()
    this.Parent.oContained.w_Lug = this.RadioValue()
    return .t.
  endfunc

  func oLug_1_68.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Lug==0x0040,1,;
      0)
  endfunc

  func oLug_1_68.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oAgo_1_69 as StdCheck with uid="FSPRCILROT",rtseq=62,rtrep=.f.,left=99, top=273, caption="Agosto",;
    ToolTipText = "Agosto",;
    HelpContextID = 132303866,;
    cFormVar="w_Ago", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAgo_1_69.RadioValue()
    return(iif(this.value =1,0x0080,;
    0))
  endfunc
  func oAgo_1_69.GetRadio()
    this.Parent.oContained.w_Ago = this.RadioValue()
    return .t.
  endfunc

  func oAgo_1_69.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Ago==0x0080,1,;
      0)
  endfunc

  func oAgo_1_69.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oSet_1_70 as StdCheck with uid="AGYDGGPXHP",rtseq=63,rtrep=.f.,left=192, top=273, caption="Settembre",;
    ToolTipText = "Settembre",;
    HelpContextID = 132283610,;
    cFormVar="w_Set", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSet_1_70.RadioValue()
    return(iif(this.value =1,0x0100,;
    0))
  endfunc
  func oSet_1_70.GetRadio()
    this.Parent.oContained.w_Set = this.RadioValue()
    return .t.
  endfunc

  func oSet_1_70.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Set==0x0100,1,;
      0)
  endfunc

  func oSet_1_70.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oOtt_1_71 as StdCheck with uid="BNBRVAAXWY",rtseq=64,rtrep=.f.,left=308, top=274, caption="Ottobre",;
    ToolTipText = "Ottobre",;
    HelpContextID = 132279834,;
    cFormVar="w_Ott", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOtt_1_71.RadioValue()
    return(iif(this.value =1,0x0200,;
    0))
  endfunc
  func oOtt_1_71.GetRadio()
    this.Parent.oContained.w_Ott = this.RadioValue()
    return .t.
  endfunc

  func oOtt_1_71.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Ott==0x0200,1,;
      0)
  endfunc

  func oOtt_1_71.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oORAFIN_OR_1_72 as StdField with uid="EBIXKXULCN",rtseq=65,rtrep=.f.,;
    cFormVar = "w_ORAFIN_OR", cQueryName = "ORAFIN_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 84916907,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=263, Top=76, InputMask=replicate('X',2)

  add object oMINFIN_OR_1_73 as StdField with uid="VQMTKUWDZZ",rtseq=66,rtrep=.f.,;
    cFormVar = "w_MINFIN_OR", cQueryName = "MINFIN_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 84865995,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=305, Top=76, InputMask=replicate('X',2)

  add object oNov_1_74 as StdCheck with uid="KIFRAHCZVI",rtseq=67,rtrep=.f.,left=391, top=274, caption="Novembre",;
    ToolTipText = "Novembre",;
    HelpContextID = 132272938,;
    cFormVar="w_Nov", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNov_1_74.RadioValue()
    return(iif(this.value =1,0x0400,;
    0))
  endfunc
  func oNov_1_74.GetRadio()
    this.Parent.oContained.w_Nov = this.RadioValue()
    return .t.
  endfunc

  func oNov_1_74.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Nov==0x0400,1,;
      0)
  endfunc

  func oNov_1_74.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc

  add object oDic_1_75 as StdCheck with uid="DLBUWJMODW",rtseq=68,rtrep=.f.,left=484, top=274, caption="Dicembre",;
    ToolTipText = "Dicembre",;
    HelpContextID = 132352458,;
    cFormVar="w_Dic", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDic_1_75.RadioValue()
    return(iif(this.value =1,0x0800,;
    0))
  endfunc
  func oDic_1_75.GetRadio()
    this.Parent.oContained.w_Dic = this.RadioValue()
    return .t.
  endfunc

  func oDic_1_75.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Dic==0x0800,1,;
      0)
  endfunc

  func oDic_1_75.mHide()
    with this.Parent.oContained
      return (.w_SHOWMONT<>'S')
    endwith
  endfunc


  add object oTIPRIC_1_76 as StdCombo with uid="WNHAAPNOXV",rtseq=69,rtrep=.f.,left=17,top=410,width=151,height=21;
    , ToolTipText = "Tipo ricorrenza";
    , HelpContextID = 186570;
    , cFormVar="w_TIPRIC",RowSource=""+"Fine dopo,"+"Fine entro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPRIC_1_76.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oTIPRIC_1_76.GetRadio()
    this.Parent.oContained.w_TIPRIC = this.RadioValue()
    return .t.
  endfunc

  func oTIPRIC_1_76.SetRadio()
    this.Parent.oContained.w_TIPRIC=trim(this.Parent.oContained.w_TIPRIC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPRIC=='D',1,;
      iif(this.Parent.oContained.w_TIPRIC=='E',2,;
      0))
  endfunc

  func oTIPRIC_1_76.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MD__FREQ<>'U')
    endwith
   endif
  endfunc

  add object oDATAENT_1_77 as StdField with uid="IXRQMRMCBL",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DATAENT", cQueryName = "DATAENT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data limite per generazione attivit� ricorrenti",;
    HelpContextID = 179068470,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=178, Top=411

  func oDATAENT_1_77.mHide()
    with this.Parent.oContained
      return (.w_TIPRIC <>'E')
    endwith
  endfunc

  add object oNUMOCC_1_78 as StdField with uid="PAKKTKKRJK",rtseq=71,rtrep=.f.,;
    cFormVar = "w_NUMOCC", cQueryName = "NUMOCC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso! Nel numero di occorrenze deve essere inclusa l'attivit� di origine!",;
    ToolTipText = "Numero occorrenze",;
    HelpContextID = 6683946,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=178, Top=411, cSayPict='"999"', cGetPict='"999"'

  func oNUMOCC_1_78.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MD__FREQ<>'U')
    endwith
   endif
  endfunc

  func oNUMOCC_1_78.mHide()
    with this.Parent.oContained
      return (.w_TIPRIC <>'D')
    endwith
  endfunc

  func oNUMOCC_1_78.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMOCC>1)
    endwith
    return bRes
  endfunc

  add object oATOGGETT_ORIG_1_84 as StdField with uid="QGGDHUPBWF",rtseq=75,rtrep=.f.,;
    cFormVar = "w_ATOGGETT_ORIG", cQueryName = "ATOGGETT_ORIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 110138954,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=206, Top=29, InputMask=replicate('X',254)

  add object oORAINI_OR_1_87 as StdField with uid="BZGWDFCLAT",rtseq=76,rtrep=.f.,;
    cFormVar = "w_ORAINI_OR", cQueryName = "ORAINI_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 163363499,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=263, Top=53, InputMask=replicate('X',2)

  add object oMININI_OR_1_88 as StdField with uid="TRJOELSXSC",rtseq=77,rtrep=.f.,;
    cFormVar = "w_MININI_OR", cQueryName = "MININI_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 163312587,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=305, Top=53, InputMask=replicate('X',2)


  add object oMDESCLUS_1_95 as StdCombo with uid="IEHQNEGQVY",rtseq=79,rtrep=.f.,left=251,top=315,width=241,height=21;
    , ToolTipText = "Esclusioni";
    , HelpContextID = 144536089;
    , cFormVar="w_MDESCLUS",RowSource=""+"Nessuna,"+"Domenica,"+"Sabato e domenica,"+"Da calendario", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMDESCLUS_1_95.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'S',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oMDESCLUS_1_95.GetRadio()
    this.Parent.oContained.w_MDESCLUS = this.RadioValue()
    return .t.
  endfunc

  func oMDESCLUS_1_95.SetRadio()
    this.Parent.oContained.w_MDESCLUS=trim(this.Parent.oContained.w_MDESCLUS)
    this.value = ;
      iif(this.Parent.oContained.w_MDESCLUS=='N',1,;
      iif(this.Parent.oContained.w_MDESCLUS=='D',2,;
      iif(this.Parent.oContained.w_MDESCLUS=='S',3,;
      iif(this.Parent.oContained.w_MDESCLUS=='C',4,;
      0))))
  endfunc

  func oMDESCLUS_1_95.mHide()
    with this.Parent.oContained
      return (Empty(.w_METODO) or (.w_MD__FREQ<>'M' AND .w_MD__FREQ<>'A'))
    endwith
  endfunc

  add object oMDCALEND_1_96 as StdField with uid="DYJFOXODGZ",rtseq=80,rtrep=.f.,;
    cFormVar = "w_MDCALEND", cQueryName = "MDCALEND",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Calendario per esclusione",;
    HelpContextID = 233090550,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=250, Top=343, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", cZoomOnZoom="GSAR_ATL", oKey_1_1="TCCODICE", oKey_1_2="this.w_MDCALEND"

  func oMDCALEND_1_96.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MDESCLUS='C')
    endwith
   endif
  endfunc

  func oMDCALEND_1_96.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_96('Part',this)
    endwith
    return bRes
  endfunc

  proc oMDCALEND_1_96.ecpDrop(oSource)
    this.Parent.oContained.link_1_96('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMDCALEND_1_96.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oMDCALEND_1_96'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATL',"Calendari",'',this.parent.oContained
  endproc
  proc oMDCALEND_1_96.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_MDCALEND
     i_obj.ecpSave()
  endproc

  add object oTCDESCRI_1_102 as StdField with uid="MXDDGYQLXF",rtseq=83,rtrep=.f.,;
    cFormVar = "w_TCDESCRI", cQueryName = "TCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 259038849,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=308, Top=343, InputMask=replicate('X',40)


  add object LBL as cp_calclbl with uid="ILXKCISRUM",left=166, top=172, width=109,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",alignment=0,;
    nPag=1;
    , HelpContextID = 45211622


  add object oMDESCLUS_1_110 as StdCombo with uid="THONKUJMYG",rtseq=84,rtrep=.f.,left=251,top=315,width=241,height=21;
    , ToolTipText = "Esclusioni";
    , HelpContextID = 144536089;
    , cFormVar="w_MDESCLUS",RowSource=""+"Nessuna,"+"Da calendario", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMDESCLUS_1_110.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oMDESCLUS_1_110.GetRadio()
    this.Parent.oContained.w_MDESCLUS = this.RadioValue()
    return .t.
  endfunc

  func oMDESCLUS_1_110.SetRadio()
    this.Parent.oContained.w_MDESCLUS=trim(this.Parent.oContained.w_MDESCLUS)
    this.value = ;
      iif(this.Parent.oContained.w_MDESCLUS=='N',1,;
      iif(this.Parent.oContained.w_MDESCLUS=='C',2,;
      0))
  endfunc

  func oMDESCLUS_1_110.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MD__FREQ<>'U')
    endwith
   endif
  endfunc

  func oMDESCLUS_1_110.mHide()
    with this.Parent.oContained
      return (Empty(.w_METODO)  or  NOT .w_MD__FREQ $ "U-G-S-Q")
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="IYKNXDATEI",Visible=.t., Left=6, Top=144,;
    Alignment=1, Width=120, Height=18,;
    Caption="Criterio di ricorrenza:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (Empty(.w_METODO))
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="HJBHNAPTJD",Visible=.t., Left=6, Top=378,;
    Alignment=0, Width=130, Height=18,;
    Caption="Intervallo di ricorrenza"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="CWCDXKCDXK",Visible=.t., Left=9, Top=117,;
    Alignment=1, Width=49, Height=18,;
    Caption="Metodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="BMXCFAXAEX",Visible=.t., Left=17, Top=30,;
    Alignment=1, Width=27, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="IVREAYBIIO",Visible=.t., Left=12, Top=55,;
    Alignment=1, Width=32, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="RDHFZDATNW",Visible=.t., Left=204, Top=55,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="NWZPATLZHT",Visible=.t., Left=291, Top=55,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="YDCJVCFCQQ",Visible=.t., Left=14, Top=78,;
    Alignment=1, Width=30, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="FZRJKEVHHA",Visible=.t., Left=204, Top=78,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="MVZCGHYUIT",Visible=.t., Left=291, Top=78,;
    Alignment=1, Width=9, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="FXSKSMUOTY",Visible=.t., Left=12, Top=5,;
    Alignment=0, Width=88, Height=18,;
    Caption="Attivit� di origine"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="KWPHSHBVRU",Visible=.t., Left=221, Top=413,;
    Alignment=0, Width=81, Height=18,;
    Caption="Occorrenze"  ;
  , bGlobalFont=.t.

  func oStr_1_97.mHide()
    with this.Parent.oContained
      return (.w_TIPRIC <>'D')
    endwith
  endfunc

  add object oStr_1_100 as StdString with uid="YPECBPKCEQ",Visible=.t., Left=116, Top=320,;
    Alignment=1, Width=131, Height=18,;
    Caption="Esclusioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_100.mHide()
    with this.Parent.oContained
      return (Empty(.w_METODO))
    endwith
  endfunc

  add object oStr_1_101 as StdString with uid="TCLEPAIOYT",Visible=.t., Left=77, Top=345,;
    Alignment=1, Width=170, Height=18,;
    Caption="Calendario per esclusione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_103 as StdString with uid="ZMMUNQXSIT",Visible=.t., Left=75, Top=172,;
    Alignment=1, Width=51, Height=18,;
    Caption="Ogni:"  ;
  , bGlobalFont=.t.

  func oStr_1_103.mHide()
    with this.Parent.oContained
      return (Not (.w_MD__FREQ$"G-S-M-A"))
    endwith
  endfunc

  add object oStr_1_105 as StdString with uid="YQMVADPNQU",Visible=.t., Left=302, Top=172,;
    Alignment=1, Width=45, Height=18,;
    Caption="Ogni:"  ;
  , bGlobalFont=.t.

  func oStr_1_105.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_106 as StdString with uid="GNQADKBNWC",Visible=.t., Left=388, Top=172,;
    Alignment=0, Width=47, Height=18,;
    Caption="Minuti"  ;
  , bGlobalFont=.t.

  func oStr_1_106.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_107 as StdString with uid="MGRUEYYQNQ",Visible=.t., Left=410, Top=203,;
    Alignment=1, Width=71, Height=18,;
    Caption="Il giorno:"  ;
  , bGlobalFont=.t.

  func oStr_1_107.mHide()
    with this.Parent.oContained
      return (NOT( .w_MD__FREQ$'M-A' AND .w_MDIMPDAT$'D-G' ))
    endwith
  endfunc

  add object oStr_1_108 as StdString with uid="YFOAJEGRRN",Visible=.t., Left=4, Top=203,;
    Alignment=1, Width=122, Height=18,;
    Caption="Rinvio a:"  ;
  , bGlobalFont=.t.

  func oStr_1_108.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'M' AND .w_MD__FREQ<>'A')
    endwith
  endfunc

  add object oStr_1_109 as StdString with uid="EVNCKOWZWY",Visible=.t., Left=302, Top=144,;
    Alignment=1, Width=45, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  func oStr_1_109.mHide()
    with this.Parent.oContained
      return (.w_MD__FREQ<>'U')
    endwith
  endfunc

  add object oStr_1_111 as StdString with uid="RPRMWHZCJB",Visible=.t., Left=262, Top=412,;
    Alignment=0, Width=124, Height=18,;
    Caption="Domenica"  ;
  , bGlobalFont=.t.

  func oStr_1_111.mHide()
    with this.Parent.oContained
      return (DOW(.w_DATAENT ) <> 1 or .w_TIPRIC='D')
    endwith
  endfunc

  add object oStr_1_112 as StdString with uid="QOGIGEYGZK",Visible=.t., Left=262, Top=412,;
    Alignment=0, Width=124, Height=18,;
    Caption="Luned�"  ;
  , bGlobalFont=.t.

  func oStr_1_112.mHide()
    with this.Parent.oContained
      return (DOW(.w_DATAENT ) <> 2  or .w_TIPRIC='D')
    endwith
  endfunc

  add object oStr_1_113 as StdString with uid="HJDWLDPWPT",Visible=.t., Left=262, Top=412,;
    Alignment=0, Width=124, Height=18,;
    Caption="Marted�"  ;
  , bGlobalFont=.t.

  func oStr_1_113.mHide()
    with this.Parent.oContained
      return (DOW(.w_DATAENT ) <> 3 or .w_TIPRIC='D')
    endwith
  endfunc

  add object oStr_1_114 as StdString with uid="RSXZRONXEG",Visible=.t., Left=262, Top=412,;
    Alignment=0, Width=124, Height=18,;
    Caption="Mercoled�"  ;
  , bGlobalFont=.t.

  func oStr_1_114.mHide()
    with this.Parent.oContained
      return (DOW(.w_DATAENT ) <> 4 or .w_TIPRIC='D')
    endwith
  endfunc

  add object oStr_1_115 as StdString with uid="GQVWSVZQVM",Visible=.t., Left=262, Top=412,;
    Alignment=0, Width=124, Height=18,;
    Caption="Gioved�"  ;
  , bGlobalFont=.t.

  func oStr_1_115.mHide()
    with this.Parent.oContained
      return (DOW(.w_DATAENT ) <> 5 or .w_TIPRIC='D')
    endwith
  endfunc

  add object oStr_1_116 as StdString with uid="ENHEIVLGKW",Visible=.t., Left=262, Top=412,;
    Alignment=0, Width=124, Height=18,;
    Caption="Venerd�"  ;
  , bGlobalFont=.t.

  func oStr_1_116.mHide()
    with this.Parent.oContained
      return (DOW(.w_DATAENT ) <> 6 or .w_TIPRIC='D')
    endwith
  endfunc

  add object oStr_1_117 as StdString with uid="JLGCMODRAL",Visible=.t., Left=262, Top=412,;
    Alignment=0, Width=124, Height=18,;
    Caption="Sabato"  ;
  , bGlobalFont=.t.

  func oStr_1_117.mHide()
    with this.Parent.oContained
      return (DOW(.w_DATAENT ) <> 7 or .w_TIPRIC='D')
    endwith
  endfunc

  add object oBox_1_51 as StdBox with uid="THKFMRFWYM",left=5, top=395, width=597,height=52

  add object oBox_1_82 as StdBox with uid="CMCUCARSOE",left=6, top=21, width=595,height=84
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kar','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
