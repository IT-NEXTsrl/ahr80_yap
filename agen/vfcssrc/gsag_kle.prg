* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kle                                                        *
*              Legenda                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-27                                                      *
* Last revis.: 2009-06-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kle",oParentObject))

* --- Class definition
define class tgsag_kle as StdForm
  Top    = 24
  Left   = 72

  * --- Standard Properties
  Width  = 412
  Height = 268
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-06-17"
  HelpContextID=216672105
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_kle"
  cComment = "Legenda"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_COLORPRM = space(10)
  w_COLORNNP = space(10)
  w_COLORHDR = space(10)
  w_COLORSEL = space(10)
  w_COLORATU = space(10)
  w_COLORATT = space(10)
  w_COLORATC = space(10)
  w_COLORACF = space(10)
  w_COLFUOSED = space(10)
  w_COLOCCUPA = space(10)
  w_COLURGENZE = space(10)
  w_COLLIBERO = space(10)
  w_Note = .NULL.
  w_DaFare = .NULL.
  w_Preavviso = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_klePag1","gsag_kle",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Note = this.oPgFrm.Pages(1).oPag.Note
    this.w_DaFare = this.oPgFrm.Pages(1).oPag.DaFare
    this.w_Preavviso = this.oPgFrm.Pages(1).oPag.Preavviso
    DoDefault()
    proc Destroy()
      this.w_Note = .NULL.
      this.w_DaFare = .NULL.
      this.w_Preavviso = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COLORPRM=space(10)
      .w_COLORNNP=space(10)
      .w_COLORHDR=space(10)
      .w_COLORSEL=space(10)
      .w_COLORATU=space(10)
      .w_COLORATT=space(10)
      .w_COLORATC=space(10)
      .w_COLORACF=space(10)
      .w_COLFUOSED=space(10)
      .w_COLOCCUPA=space(10)
      .w_COLURGENZE=space(10)
      .w_COLLIBERO=space(10)
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate(g_COLPRIME)
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate(g_COLNNPRM)
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate(g_COLHEADER)
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate(g_COLSELECT)
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate(g_AttUFore)
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate(g_AttTFore)
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate(g_AttCFore)
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate(g_ColFuoriSede)
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate(g_ColOccupato)
      .oPgFrm.Page1.oPag.oObj_1_35.Calculate(g_ColUrgenze)
      .oPgFrm.Page1.oPag.oObj_1_36.Calculate(g_ColLibero)
      .oPgFrm.Page1.oPag.Note.Calculate("BMP\note_small.bmp")
      .oPgFrm.Page1.oPag.DaFare.Calculate("BMP\DaFare.bmp")
      .oPgFrm.Page1.oPag.Preavviso.Calculate("BMP\Preavviso.bmp")
      .oPgFrm.Page1.oPag.oObj_1_50.Calculate(g_ColAttConf)
    endwith
    this.DoRTCalc(1,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(g_COLPRIME)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(g_COLNNPRM)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(g_COLHEADER)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(g_COLSELECT)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(g_AttUFore)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(g_AttTFore)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(g_AttCFore)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(g_ColFuoriSede)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(g_ColOccupato)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate(g_ColUrgenze)
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate(g_ColLibero)
        .oPgFrm.Page1.oPag.Note.Calculate("BMP\note_small.bmp")
        .oPgFrm.Page1.oPag.DaFare.Calculate("BMP\DaFare.bmp")
        .oPgFrm.Page1.oPag.Preavviso.Calculate("BMP\Preavviso.bmp")
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(g_ColAttConf)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(g_COLPRIME)
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(g_COLNNPRM)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(g_COLHEADER)
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(g_COLSELECT)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(g_AttUFore)
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(g_AttTFore)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(g_AttCFore)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(g_ColFuoriSede)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(g_ColOccupato)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate(g_ColUrgenze)
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate(g_ColLibero)
        .oPgFrm.Page1.oPag.Note.Calculate("BMP\note_small.bmp")
        .oPgFrm.Page1.oPag.DaFare.Calculate("BMP\DaFare.bmp")
        .oPgFrm.Page1.oPag.Preavviso.Calculate("BMP\Preavviso.bmp")
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(g_ColAttConf)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.Note.Event(cEvent)
      .oPgFrm.Page1.oPag.DaFare.Event(cEvent)
      .oPgFrm.Page1.oPag.Preavviso.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOLORPRM_1_9.value==this.w_COLORPRM)
      this.oPgFrm.Page1.oPag.oCOLORPRM_1_9.value=this.w_COLORPRM
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORNNP_1_10.value==this.w_COLORNNP)
      this.oPgFrm.Page1.oPag.oCOLORNNP_1_10.value=this.w_COLORNNP
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORHDR_1_11.value==this.w_COLORHDR)
      this.oPgFrm.Page1.oPag.oCOLORHDR_1_11.value=this.w_COLORHDR
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORSEL_1_12.value==this.w_COLORSEL)
      this.oPgFrm.Page1.oPag.oCOLORSEL_1_12.value=this.w_COLORSEL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORATU_1_13.value==this.w_COLORATU)
      this.oPgFrm.Page1.oPag.oCOLORATU_1_13.value=this.w_COLORATU
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORATT_1_14.value==this.w_COLORATT)
      this.oPgFrm.Page1.oPag.oCOLORATT_1_14.value=this.w_COLORATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORATC_1_15.value==this.w_COLORATC)
      this.oPgFrm.Page1.oPag.oCOLORATC_1_15.value=this.w_COLORATC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLORACF_1_16.value==this.w_COLORACF)
      this.oPgFrm.Page1.oPag.oCOLORACF_1_16.value=this.w_COLORACF
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLFUOSED_1_29.value==this.w_COLFUOSED)
      this.oPgFrm.Page1.oPag.oCOLFUOSED_1_29.value=this.w_COLFUOSED
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLOCCUPA_1_30.value==this.w_COLOCCUPA)
      this.oPgFrm.Page1.oPag.oCOLOCCUPA_1_30.value=this.w_COLOCCUPA
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLURGENZE_1_31.value==this.w_COLURGENZE)
      this.oPgFrm.Page1.oPag.oCOLURGENZE_1_31.value=this.w_COLURGENZE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOLLIBERO_1_32.value==this.w_COLLIBERO)
      this.oPgFrm.Page1.oPag.oCOLLIBERO_1_32.value=this.w_COLLIBERO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_klePag1 as StdContainer
  Width  = 408
  height = 268
  stdWidth  = 408
  stdheight = 268
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOLORPRM_1_9 as StdField with uid="EJURITPPVW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_COLORPRM", cQueryName = "COLORPRM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore utilizzato per lo sfondo (orario di lavoro per calendario)",;
    HelpContextID = 143256691,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=181, Top=34, InputMask=replicate('X',10)

  add object oCOLORNNP_1_10 as StdField with uid="CTYTNJTJUD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_COLORNNP", cQueryName = "COLORNNP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore utilizzato per il bordo nella disponibililitą risorse e per l'orario di chiusura  nel calendario",;
    HelpContextID = 158733194,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=181, Top=59, InputMask=replicate('X',10)

  add object oCOLORHDR_1_11 as StdField with uid="OVOMGXTRBL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_COLORHDR", cQueryName = "COLORHDR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore utilizzato per l'intestazione",;
    HelpContextID = 9038968,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=181, Top=84, InputMask=replicate('X',10)

  add object oCOLORSEL_1_12 as StdField with uid="JISWCHPTGQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_COLORSEL", cQueryName = "COLORSEL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore utilizzato per la selezione nella disponibilitą risorse e nel calendario",;
    HelpContextID = 193588338,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=181, Top=109, InputMask=replicate('X',10)

  add object oCOLORATU_1_13 as StdField with uid="MHRMHXCQGH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_COLORATU", cQueryName = "COLORATU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore utilizzato per le attivitą con prioritą urgente",;
    HelpContextID = 160033915,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=181, Top=155, InputMask=replicate('X',10)

  add object oCOLORATT_1_14 as StdField with uid="HCAYSCAVCK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_COLORATT", cQueryName = "COLORATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore utilizzato per le attivitą con prioritą scadenza termine",;
    HelpContextID = 160033914,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=181, Top=180, InputMask=replicate('X',10)

  add object oCOLORATC_1_15 as StdField with uid="NBUFGOGNYA",rtseq=7,rtrep=.f.,;
    cFormVar = "w_COLORATC", cQueryName = "COLORATC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore utilizzato per le attivitą con stato evasa o completata",;
    HelpContextID = 160033897,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=181, Top=205, InputMask=replicate('X',10)

  add object oCOLORACF_1_16 as StdField with uid="MOYJWEFVZW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_COLORACF", cQueryName = "COLORACF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore utilizzato per le attivitą con stato provvisoria",;
    HelpContextID = 160033900,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=181, Top=230, InputMask=replicate('X',10)


  add object oObj_1_17 as cp_setobjprop with uid="QFKIHTTZWY",left=73, top=505, width=148,height=23,;
    caption='Sfondo',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORPRM",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 147534886


  add object oObj_1_18 as cp_setobjprop with uid="DZKONMKWXT",left=73, top=528, width=148,height=23,;
    caption='Bordo',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORNNP",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 93230058


  add object oObj_1_19 as cp_setobjprop with uid="TTLSBSGVUQ",left=73, top=551, width=148,height=23,;
    caption='Intestazione',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORHDR",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 253682064


  add object oObj_1_20 as cp_setobjprop with uid="ERNGTOKCMQ",left=73, top=574, width=148,height=23,;
    caption='Selezione',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORSEL",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 69339620


  add object oObj_1_21 as cp_setobjprop with uid="GGEZSKDONA",left=231, top=505, width=148,height=23,;
    caption='Urgente',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORATU",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 27148218


  add object oObj_1_22 as cp_setobjprop with uid="AQTIQTPJPW",left=231, top=528, width=148,height=23,;
    caption='Scadenza termine',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORATT",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 135862252


  add object oObj_1_23 as cp_setobjprop with uid="XTFLGCCDVK",left=231, top=551, width=148,height=23,;
    caption='Completata',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORATC",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 256738503

  add object oCOLFUOSED_1_29 as StdField with uid="QZHINWIMOF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_COLFUOSED", cQueryName = "COLFUOSED",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore per identificare attivitą con disponibilitą 'fuori sede'",;
    HelpContextID = 129036459,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=369, Top=34, InputMask=replicate('X',10)

  add object oCOLOCCUPA_1_30 as StdField with uid="OAZFZTPRXJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_COLOCCUPA", cQueryName = "COLOCCUPA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore per identificare attivitą con disponibilitą 'occupato'",;
    HelpContextID = 177860742,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=369, Top=59, InputMask=replicate('X',10)

  add object oCOLURGENZE_1_31 as StdField with uid="YPNPVOHKJJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_COLURGENZE", cQueryName = "COLURGENZE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore per identificare attivitą con disponibilitą 'per urgenze'",;
    HelpContextID = 261109524,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=369, Top=84, InputMask=replicate('X',10)

  add object oCOLLIBERO_1_32 as StdField with uid="QNBMIMDLNZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COLLIBERO", cQueryName = "COLLIBERO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Colore per identificare attivitą con disponibilitą 'libero'",;
    HelpContextID = 167178600,;
   bGlobalFont=.t.,;
    Height=16, Width=16, Left=369, Top=109, InputMask=replicate('X',10)


  add object oObj_1_33 as cp_setobjprop with uid="FVNLNRBATQ",left=389, top=505, width=148,height=23,;
    caption='Fuori sede',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLFUOSED",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 169848315


  add object oObj_1_34 as cp_setobjprop with uid="QNDNVPGBZS",left=389, top=528, width=148,height=23,;
    caption='Occupato',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLOCCUPA",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 194081109


  add object oObj_1_35 as cp_setobjprop with uid="YJLHEYTRFU",left=389, top=551, width=148,height=23,;
    caption='Urgenze',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLURGENZE",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 194920378


  add object oObj_1_36 as cp_setobjprop with uid="EKQUXHPDRL",left=389, top=574, width=148,height=23,;
    caption='Libero',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLLIBERO",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 161572534


  add object Note as cp_showimage with uid="GATJSWSBZC",left=369, top=153, width=27,height=22,;
    caption='Note',;
   bGlobalFont=.t.,;
    stretch=0,default="BMP\note_small.bmp",BackStyle=0,;
    nPag=1;
    , ToolTipText = "Icona per identificare attivitą di tipologia 'note' (nel calendario)";
    , HelpContextID = 209548074


  add object DaFare as cp_showimage with uid="AUVXIWDWCI",left=369, top=178, width=27,height=22,;
    caption='DaFare',;
   bGlobalFont=.t.,;
    stretch=0,default="BMP\DaFare.bmp",BackStyle=0,;
    nPag=1;
    , ToolTipText = "Icona per identificare attivitą di tipologia 'cosa da fare' (nel calendario)";
    , HelpContextID = 261856822


  add object Preavviso as cp_showimage with uid="XMQPGWNVVH",left=369, top=203, width=27,height=22,;
    caption='Preavviso',;
   bGlobalFont=.t.,;
    stretch=0,default="BMP\Preavviso.bmp",BackStyle=0,;
    nPag=1;
    , ToolTipText = "Icona per identificare attivitą con preavviso (nel calendario)";
    , HelpContextID = 253909159


  add object oObj_1_50 as cp_setobjprop with uid="NKMUMTQXMK",left=231, top=574, width=148,height=23,;
    caption='Provvisoria',;
   bGlobalFont=.t.,;
    cProp="disabledbackcolor",cObj="w_COLORACF",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 66699397

  add object oStr_1_1 as StdString with uid="QBLKXBKHSS",Visible=.t., Left=43, Top=11,;
    Alignment=0, Width=124, Height=18,;
    Caption="Maschere"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_2 as StdString with uid="WNFEQKGPPT",Visible=.t., Left=24, Top=34,;
    Alignment=1, Width=147, Height=18,;
    Caption="Sfondo (orario di lavoro):"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="MQKGNZHOTC",Visible=.t., Left=28, Top=59,;
    Alignment=1, Width=143, Height=18,;
    Caption="Bordo (orario di chiusura):"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="CTOVLLOBVD",Visible=.t., Left=36, Top=84,;
    Alignment=1, Width=135, Height=18,;
    Caption="Intestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="CNYLVZTVVV",Visible=.t., Left=36, Top=109,;
    Alignment=1, Width=135, Height=18,;
    Caption="Selezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="XIVDKZSMPL",Visible=.t., Left=36, Top=155,;
    Alignment=1, Width=135, Height=18,;
    Caption="Urgente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="KVYHCKPFEI",Visible=.t., Left=36, Top=180,;
    Alignment=1, Width=135, Height=18,;
    Caption="In scadenza termine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="FOBXXLJOGA",Visible=.t., Left=8, Top=205,;
    Alignment=1, Width=163, Height=18,;
    Caption="Evasa o completata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="LYYNSTCOWF",Visible=.t., Left=232, Top=11,;
    Alignment=0, Width=95, Height=18,;
    Caption="Disponibilitą"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="ANNYPTECDE",Visible=.t., Left=224, Top=34,;
    Alignment=1, Width=135, Height=18,;
    Caption="Fuori sede:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="LYXGWNFEZO",Visible=.t., Left=224, Top=59,;
    Alignment=1, Width=135, Height=18,;
    Caption="Occupato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="FIFTPTCDDS",Visible=.t., Left=224, Top=84,;
    Alignment=1, Width=135, Height=18,;
    Caption="Per urgenze:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="STGGLCSLOK",Visible=.t., Left=224, Top=109,;
    Alignment=1, Width=135, Height=18,;
    Caption="Libero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="RUBSGHCKQF",Visible=.t., Left=224, Top=153,;
    Alignment=1, Width=135, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="QKVXKJXLLV",Visible=.t., Left=224, Top=178,;
    Alignment=1, Width=135, Height=18,;
    Caption="Cose da fare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="ZIXZWEQJIG",Visible=.t., Left=224, Top=203,;
    Alignment=1, Width=135, Height=18,;
    Caption="Preavviso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="BTOIAUUQGP",Visible=.t., Left=43, Top=132,;
    Alignment=0, Width=124, Height=18,;
    Caption="Testo/sfondo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="SBDCDFUBSZ",Visible=.t., Left=232, Top=132,;
    Alignment=0, Width=124, Height=18,;
    Caption="Task list"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="IJHHHJXOXT",Visible=.t., Left=36, Top=230,;
    Alignment=1, Width=135, Height=18,;
    Caption="Sfondo provvisoria:"  ;
  , bGlobalFont=.t.

  add object oBox_1_44 as StdBox with uid="WZCKMJELPH",left=43, top=28, width=152,height=1

  add object oBox_1_45 as StdBox with uid="OEOVSTDEKG",left=232, top=28, width=152,height=1

  add object oBox_1_47 as StdBox with uid="JHIQVFSDNP",left=232, top=149, width=152,height=1

  add object oBox_1_48 as StdBox with uid="FYBRRNHSMQ",left=43, top=149, width=152,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kle','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
