* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bpi                                                        *
*              Lancia Inserimento provvisorio prestazioni                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-24                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_COD_PRA,w_COD_PRE,w_COD_RAG,w_DESCRI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bpi",oParentObject,m.w_COD_PRA,m.w_COD_PRE,m.w_COD_RAG,m.w_DESCRI)
return(i_retval)

define class tgsag_bpi as StdBatch
  * --- Local variables
  w_COD_PRA = space(20)
  w_COD_PRE = space(20)
  w_COD_RAG = space(10)
  w_DESCRI = space(40)
  w_GSAG_KPR = .NULL.
  w_GENPRE = space(1)
  w_READAZI = space(5)
  w_MODSCHED = space(1)
  w_GSAG_KPM = .NULL.
  w_FlCompleta = space(1)
  * --- WorkFile variables
  PAR_ALTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Inserimento provvisorio prestazioni
    if VARTYPE(this.w_DESCRI)<>"C"
      this.w_DESCRI = " "
    endif
    if NOT EMPTY(this.w_COD_PRE) OR NOT EMPTY(this.w_COD_RAG)
      this.w_READAZI = i_CODAZI
      this.w_FlCompleta = "N"
      if IsAlt()
        * --- Read from PAR_ALTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PADMCOMP,PAGENPRE"+;
            " from "+i_cTable+" PAR_ALTE where ";
                +"PACODAZI = "+cp_ToStrODBC(this.w_READAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PADMCOMP,PAGENPRE;
            from (i_cTable) where;
                PACODAZI = this.w_READAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FlCompleta = NVL(cp_ToDate(_read_.PADMCOMP),cp_NullValue(_read_.PADMCOMP))
          this.w_GENPRE = NVL(cp_ToDate(_read_.PAGENPRE),cp_NullValue(_read_.PAGENPRE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Lancia la maschera di Inserimento provvisorio delle prestazioni
      this.w_GSAG_KPR = GSAG_KPR(this.w_GENPRE)
      * --- Valorizzo la pratica in GSAG_KPR
      SetValueLinked("M", this.w_GSAG_KPR, "w_CodPratica", this.w_COD_PRA)
      * --- Variabile per ripristinare il valore di g_SCHEDULER
      this.w_MODSCHED = "N"
      if VARTYPE(g_SCHEDULER)<>"C" Or g_SCHEDULER<>"S"
        public g_SCHEDULER
        g_SCHEDULER="S"
        this.w_MODSCHED = "S"
      endif
      * --- Lancio (in modalit� nascosta) la Scelta multipla delle prestazioni
      this.w_GSAG_KPR.w_VISMES = -1
      * --- Prima di tutto deselezioniamo le prestazioni gi� inserite
      this.w_GSAG_KPR.GSAG_MPR.NotifyEvent("DeselezionaPresenti")     
      do case
        case NOT EMPTY(this.w_COD_PRE)
          * --- Seleziona singola prestazione
          this.w_GSAG_KPM = GSAG_KPM(this.w_GSAG_KPR)
          this.w_GSAG_KPM.w_DESCRI = this.w_DESCRI
          UPDATE ( this.w_GSAG_KPM.w_AGKPM_ZOOM.cCursor ) SET XCHK = 1 WHERE CACODICE=this.w_COD_PRE
          this.w_GSAG_KPM.EcpSave()     
          this.w_GSAG_KPM = .Null.
          if this.w_FlCompleta="S"
            * --- La prestazione deve essere completata
            this.w_GSAG_KPR.NotifyEvent("Completa")     
          endif
        case NOT EMPTY(this.w_COD_RAG)
          * --- Valorizzo raggruppamento prestazioni (scatta la ricerca automaticamente delle prestazioni)
          this.w_GSAG_KPR.w_DM_FLCompleta = this.w_FlCompleta
          this.w_GSAG_KPR.w_SELRAGG = this.w_COD_RAG
          SetValueLinked("M", this.w_GSAG_KPR, "w_SELRAGG", this.w_COD_RAG)
      endcase
      * --- Conferma della maschera di Scelta multipla delle prestazioni
      this.w_GSAG_KPR.EcpSave()     
      this.w_GSAG_KPR = .null.
      if this.w_MODSCHED="S"
        g_SCHEDULER="N"
      endif
    endif
  endproc


  proc Init(oParentObject,w_COD_PRA,w_COD_PRE,w_COD_RAG,w_DESCRI)
    this.w_COD_PRA=w_COD_PRA
    this.w_COD_PRE=w_COD_PRE
    this.w_COD_RAG=w_COD_RAG
    this.w_DESCRI=w_DESCRI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_ALTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_COD_PRA,w_COD_PRE,w_COD_RAG,w_DESCRI"
endproc
