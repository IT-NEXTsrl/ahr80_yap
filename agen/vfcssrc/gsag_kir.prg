* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kir                                                        *
*              Ricerca impianti/componenti per attributo                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-03-28                                                      *
* Last revis.: 2015-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kir",oParentObject))

* --- Class definition
define class tgsag_kir as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 792
  Height = 555
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-29"
  HelpContextID=267003753
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  FAM_ATTR_IDX = 0
  IMP_MAST_IDX = 0
  cPrg = "gsag_kir"
  cComment = "Ricerca impianti/componenti per attributo"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ATTRIBUTO = space(10)
  w_RICERCA = space(50)
  w_IMPIANTO = space(10)
  w_COMPIMP = space(50)
  w_COMPIMPKEYREAD = 0
  w_COMPIMPKEY = 0
  w_OLDCOMP = space(50)
  w_IMDESCRI = space(50)
  w_ANDESCRI = space(30)
  w_CODNOM = space(15)
  w_CODICE = space(15)
  w_IMPIANT1 = space(10)
  w_CODIC1 = space(15)
  w_DESCRIMODELLO = space(40)
  w_DESCRGRUPPO = space(40)
  w_DESCRFAMIGLIA = space(40)
  w_NUMERORIGA = 0
  w_FRDESCRI = space(40)
  w_ASVALATT = space(20)
  w_DESIMP = space(50)
  w_DATFIN = ctod('  /  /  ')
  w_SEARCH = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kirPag1","gsag_kir",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oATTRIBUTO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_SEARCH = this.oPgFrm.Pages(1).oPag.SEARCH
    DoDefault()
    proc Destroy()
      this.w_SEARCH = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='FAM_ATTR'
    this.cWorkTables[2]='IMP_MAST'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ATTRIBUTO=space(10)
      .w_RICERCA=space(50)
      .w_IMPIANTO=space(10)
      .w_COMPIMP=space(50)
      .w_COMPIMPKEYREAD=0
      .w_COMPIMPKEY=0
      .w_OLDCOMP=space(50)
      .w_IMDESCRI=space(50)
      .w_ANDESCRI=space(30)
      .w_CODNOM=space(15)
      .w_CODICE=space(15)
      .w_IMPIANT1=space(10)
      .w_CODIC1=space(15)
      .w_DESCRIMODELLO=space(40)
      .w_DESCRGRUPPO=space(40)
      .w_DESCRFAMIGLIA=space(40)
      .w_NUMERORIGA=0
      .w_FRDESCRI=space(40)
      .w_ASVALATT=space(20)
      .w_DESIMP=space(50)
      .w_DATFIN=ctod("  /  /  ")
      .w_COMPIMP=oParentObject.w_COMPIMP
      .w_COMPIMPKEYREAD=oParentObject.w_COMPIMPKEYREAD
      .w_COMPIMPKEY=oParentObject.w_COMPIMPKEY
      .w_OLDCOMP=oParentObject.w_OLDCOMP
      .w_IMDESCRI=oParentObject.w_IMDESCRI
      .w_CODNOM=oParentObject.w_CODNOM
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_ATTRIBUTO))
          .link_1_2('Full')
        endif
      .oPgFrm.Page1.oPag.SEARCH.Calculate()
          .DoRTCalc(2,2,.f.)
        .w_IMPIANTO = this.oparentobject.w_IMPIANTO
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_IMPIANTO))
          .link_1_6('Full')
        endif
        .w_COMPIMP = .w_SEARCH.GETVAR('IMDESCON')
        .w_COMPIMPKEYREAD = .w_SEARCH.GETVAR('CPROWNUM')
        .w_COMPIMPKEY = .w_SEARCH.GETVAR('CPROWNUM')
        .w_OLDCOMP = .w_COMPIMP
        .w_IMDESCRI = .w_SEARCH.GETVAR('IMDESCRI')
        .w_ANDESCRI = .w_SEARCH.GETVAR('ANDESCRI')
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
          .DoRTCalc(10,11,.f.)
        .w_IMPIANT1 = .w_SEARCH.GETVAR('IMCODICE')
        .w_CODIC1 = .w_SEARCH.GETVAR('IMCODCON')
        .w_DESCRIMODELLO = .w_SEARCH.GETVAR('MADESCRI')
        .w_DESCRGRUPPO = .w_SEARCH.GETVAR('GRDESCRI')
        .w_DESCRFAMIGLIA = .w_SEARCH.GETVAR('FRDESCRI')
        .w_NUMERORIGA = .w_SEARCH.GETVAR('CPROWORD')
          .DoRTCalc(18,18,.f.)
        .w_ASVALATT = .w_SEARCH.GETVAR('ASVALATT')
    endwith
    this.DoRTCalc(20,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_COMPIMP=.w_COMPIMP
      .oParentObject.w_COMPIMPKEYREAD=.w_COMPIMPKEYREAD
      .oParentObject.w_COMPIMPKEY=.w_COMPIMPKEY
      .oParentObject.w_OLDCOMP=.w_OLDCOMP
      .oParentObject.w_IMDESCRI=.w_IMDESCRI
      .oParentObject.w_CODNOM=.w_CODNOM
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.SEARCH.Calculate()
        .DoRTCalc(1,3,.t.)
            .w_COMPIMP = .w_SEARCH.GETVAR('IMDESCON')
            .w_COMPIMPKEYREAD = .w_SEARCH.GETVAR('CPROWNUM')
            .w_COMPIMPKEY = .w_SEARCH.GETVAR('CPROWNUM')
            .w_OLDCOMP = .w_COMPIMP
            .w_IMDESCRI = .w_SEARCH.GETVAR('IMDESCRI')
            .w_ANDESCRI = .w_SEARCH.GETVAR('ANDESCRI')
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .DoRTCalc(10,11,.t.)
            .w_IMPIANT1 = .w_SEARCH.GETVAR('IMCODICE')
            .w_CODIC1 = .w_SEARCH.GETVAR('IMCODCON')
            .w_DESCRIMODELLO = .w_SEARCH.GETVAR('MADESCRI')
            .w_DESCRGRUPPO = .w_SEARCH.GETVAR('GRDESCRI')
            .w_DESCRFAMIGLIA = .w_SEARCH.GETVAR('FRDESCRI')
            .w_NUMERORIGA = .w_SEARCH.GETVAR('CPROWORD')
        .DoRTCalc(18,18,.t.)
            .w_ASVALATT = .w_SEARCH.GETVAR('ASVALATT')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(20,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.SEARCH.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_kir
    IF CEVENT='Blank'
       IF upper(this.oparentobject.class)='TCGSAG_MCP'
         this.w_IMPIANTO=this.oparentobject.w_CACODIMP
       else
         this.w_IMPIANTO=this.oparentobject.w_IMPIANTO
       endif
    
    ENDIF
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.SEARCH.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kir
    IF CEVENT='w_search selected' and upper(this.oparentobject.class)<>'TGSAG_BIR'
       IF upper(this.oparentobject.class)='TCGSAG_MCP'
         this.oparentobject.w_CACODIMP=this.w_IMPIANT1
         this.oparentobject.o_CACODIMP=this.w_IMPIANT1
         this.oparentobject.w_DESCOM=this.w_COMPIMP
         this.oparentobject.w_COCOM=this.w_COMPIMPKEYREAD
         this.oparentobject.w_CACODCOM=this.w_COMPIMPKEY
       else
         this.oparentobject.o_IMPIANTO=this.w_IMPIANT1
         this.oparentobject.w_IMPIANTO=this.w_IMPIANT1
         this.oparentobject.o_CODCON=this.w_CODIC1
         this.oparentobject.w_CODCON=this.w_CODIC1
         this.oparentobject.w_COMPIMP=this.w_COMPIMP
         this.oparentobject.w_COMPIMPKEYREAD=this.w_COMPIMPKEYREAD
         this.oparentobject.w_COMPIMPKEY=this.w_COMPIMPKEY
         this.oParentObject.NotifyEvent('Search')
       endif
       this.oparentobject.mEnableControls()
       this.ecpsave()
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ATTRIBUTO
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_lTable = "FAM_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2], .t., this.FAM_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTRIBUTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AAT',True,'FAM_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FRCODICE like "+cp_ToStrODBC(trim(this.w_ATTRIBUTO)+"%");

          i_ret=cp_SQL(i_nConn,"select FRCODICE,FRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FRCODICE',trim(this.w_ATTRIBUTO))
          select FRCODICE,FRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTRIBUTO)==trim(_Link_.FRCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FRDESCRI like "+cp_ToStrODBC(trim(this.w_ATTRIBUTO)+"%");

            i_ret=cp_SQL(i_nConn,"select FRCODICE,FRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FRDESCRI like "+cp_ToStr(trim(this.w_ATTRIBUTO)+"%");

            select FRCODICE,FRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATTRIBUTO) and !this.bDontReportError
            deferred_cp_zoom('FAM_ATTR','*','FRCODICE',cp_AbsName(oSource.parent,'oATTRIBUTO_1_2'),i_cWhere,'GSAR_AAT',"Attributi",'GSAG1KIR.FAM_ATTR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE,FRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',oSource.xKey(1))
            select FRCODICE,FRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTRIBUTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE,FRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(this.w_ATTRIBUTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',this.w_ATTRIBUTO)
            select FRCODICE,FRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTRIBUTO = NVL(_Link_.FRCODICE,space(10))
      this.w_FRDESCRI = NVL(_Link_.FRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ATTRIBUTO = space(10)
      endif
      this.w_FRDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.FRCODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTRIBUTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMPIANTO
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMPIANTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_IMPIANTO)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_IMPIANTO))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMPIANTO)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IMPIANTO) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oIMPIANTO_1_6'),i_cWhere,'',"Elenco impianti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMPIANTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMPIANTO = NVL(_Link_.IMCODICE,space(10))
      this.w_DESIMP = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IMPIANTO = space(10)
      endif
      this.w_DESIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMPIANTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATTRIBUTO_1_2.value==this.w_ATTRIBUTO)
      this.oPgFrm.Page1.oPag.oATTRIBUTO_1_2.value=this.w_ATTRIBUTO
    endif
    if not(this.oPgFrm.Page1.oPag.oRICERCA_1_3.value==this.w_RICERCA)
      this.oPgFrm.Page1.oPag.oRICERCA_1_3.value=this.w_RICERCA
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPIANTO_1_6.value==this.w_IMPIANTO)
      this.oPgFrm.Page1.oPag.oIMPIANTO_1_6.value=this.w_IMPIANTO
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCRI_1_11.value==this.w_IMDESCRI)
      this.oPgFrm.Page1.oPag.oIMDESCRI_1_11.value=this.w_IMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_12.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_12.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIMODELLO_1_23.value==this.w_DESCRIMODELLO)
      this.oPgFrm.Page1.oPag.oDESCRIMODELLO_1_23.value=this.w_DESCRIMODELLO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRGRUPPO_1_25.value==this.w_DESCRGRUPPO)
      this.oPgFrm.Page1.oPag.oDESCRGRUPPO_1_25.value=this.w_DESCRGRUPPO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRFAMIGLIA_1_27.value==this.w_DESCRFAMIGLIA)
      this.oPgFrm.Page1.oPag.oDESCRFAMIGLIA_1_27.value=this.w_DESCRFAMIGLIA
    endif
    if not(this.oPgFrm.Page1.oPag.oFRDESCRI_1_31.value==this.w_FRDESCRI)
      this.oPgFrm.Page1.oPag.oFRDESCRI_1_31.value=this.w_FRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_1_33.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_1_33.value=this.w_DESIMP
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_kirPag1 as StdContainer
  Width  = 788
  height = 555
  stdWidth  = 788
  stdheight = 555
  resizeXpos=285
  resizeYpos=171
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATTRIBUTO_1_2 as StdField with uid="ZXCOETZTBC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ATTRIBUTO", cQueryName = "ATTRIBUTO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 117274186,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=122, Top=12, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="FAM_ATTR", cZoomOnZoom="GSAR_AAT", oKey_1_1="FRCODICE", oKey_1_2="this.w_ATTRIBUTO"

  func oATTRIBUTO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTRIBUTO_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATTRIBUTO_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ATTR','*','FRCODICE',cp_AbsName(this.parent,'oATTRIBUTO_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AAT',"Attributi",'GSAG1KIR.FAM_ATTR_VZM',this.parent.oContained
  endproc
  proc oATTRIBUTO_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AAT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FRCODICE=this.parent.oContained.w_ATTRIBUTO
     i_obj.ecpSave()
  endproc

  add object oRICERCA_1_3 as AH_SEARCHFLD with uid="LMOCOKSDXU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RICERCA", cQueryName = "RICERCA",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Valore da ricercare in impianti/componenti",;
    HelpContextID = 125872362,;
   bGlobalFont=.t.,;
    Height=21, Width=393, Left=122, Top=37, InputMask=replicate('X',50)


  add object oBtn_1_4 as StdButton with uid="YXNVKPJNNY",left=723, top=14, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 178353942;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(NVL(.w_ATTRIBUTO,'')) OR NOT EMPTY(NVL(.w_RICERCA,'')))
      endwith
    endif
  endfunc


  add object SEARCH as cp_zoombox with uid="COGHNIBJGI",left=8, top=87, width=774,height=247,;
    caption='Object',;
   bGlobalFont=.t.,;
    bQueryOnDblClick=.t.,cZoomFile="GSAG_KIR",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bNoZoomGridShape=.f.,cZoomOnZoom="",cMenuFile="",cTable="IMP_MAST",bRetriveAllRows=.f.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 179429350

  add object oIMPIANTO_1_6 as StdField with uid="BWQXWWUATJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_IMPIANTO", cQueryName = "IMPIANTO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 41167573,;
   bGlobalFont=.t.,;
    Height=22, Width=127, Left=122, Top=62, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", oKey_1_1="IMCODICE", oKey_1_2="this.w_IMPIANTO"

  func oIMPIANTO_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oIMPIANTO_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMPIANTO_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oIMPIANTO_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco impianti",'',this.parent.oContained
  endproc

  add object oIMDESCRI_1_11 as StdField with uid="EWZQEETYGI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IMDESCRI", cQueryName = "IMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 124818737,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=365, Top=363, InputMask=replicate('X',50)

  add object oANDESCRI_1_12 as StdField with uid="JCWBFAPTCY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 124818609,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=365, Top=338, InputMask=replicate('X',30)


  add object oObj_1_16 as cp_outputCombo with uid="ZLERXIVPWT",left=365, top=472, width=406,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 179429350


  add object oBtn_1_17 as StdButton with uid="PYYBFJPVEW",left=669, top=502, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 125214170;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="XQXXJWXSFU",left=723, top=502, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 259686330;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRIMODELLO_1_23 as StdField with uid="PZWEPDMHUT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCRIMODELLO", cQueryName = "DESCRIMODELLO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 205562939,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=365, Top=389, InputMask=replicate('X',40)

  add object oDESCRGRUPPO_1_25 as StdField with uid="ZPJOTTFOTG",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCRGRUPPO", cQueryName = "DESCRGRUPPO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 58484853,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=365, Top=414, InputMask=replicate('X',40)

  add object oDESCRFAMIGLIA_1_27 as StdField with uid="PCNMZAMYKK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCRFAMIGLIA", cQueryName = "DESCRFAMIGLIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 2335213,;
   bGlobalFont=.t.,;
    Height=21, Width=406, Left=365, Top=439, InputMask=replicate('X',40)

  add object oFRDESCRI_1_31 as StdField with uid="OJPSNSLUJQ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_FRDESCRI", cQueryName = "FRDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 124817505,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=209, Top=12, InputMask=replicate('X',40)

  add object oDESIMP_1_33 as StdField with uid="RUXYCUYEQZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 181120458,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=250, Top=62, InputMask=replicate('X',50)

  add object oStr_1_1 as StdString with uid="EZGROXWKTJ",Visible=.t., Left=14, Top=37,;
    Alignment=1, Width=106, Height=18,;
    Caption="Valore da ricercare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="YLGQODOLTR",Visible=.t., Left=242, Top=363,;
    Alignment=1, Width=119, Height=18,;
    Caption="Descrizione impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="AHWYEENTXP",Visible=.t., Left=229, Top=338,;
    Alignment=1, Width=132, Height=18,;
    Caption="Descrizione intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="HRTFFGQQPR",Visible=.t., Left=274, Top=472,;
    Alignment=1, Width=87, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="AHQVYYKGXH",Visible=.t., Left=229, Top=389,;
    Alignment=1, Width=132, Height=18,;
    Caption="Descrizione modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="TDFYIRSJRG",Visible=.t., Left=229, Top=414,;
    Alignment=1, Width=132, Height=18,;
    Caption="Descrizione gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="VPTNGYWUZI",Visible=.t., Left=229, Top=439,;
    Alignment=1, Width=132, Height=18,;
    Caption="Descrizione famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="FJQEOWSVZU",Visible=.t., Left=73, Top=12,;
    Alignment=1, Width=47, Height=18,;
    Caption="Attributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="PLLUBZFTTC",Visible=.t., Left=64, Top=65,;
    Alignment=1, Width=56, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kir','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
