* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bct                                                        *
*              Cancellazione tipi attivit�                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-11-29                                                      *
* Last revis.: 2013-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bct",oParentObject)
return(i_retval)

define class tgsag_bct as StdBatch
  * --- Local variables
  w_MODSCHED = space(1)
  w_oERRORLOG = .NULL.
  w_OBJECT = .NULL.
  w_CodRagg = space(10)
  w_EliminaTipo = .f.
  w_CodTipoElim = space(20)
  w_CodTipoAtt = space(20)
  * --- WorkFile variables
  PRO_ITER_idx=0
  CAUMATTI_idx=0
  CAU_ATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                                          Cancellazione tipi attivit�
    * --- Variabile per ripristinare il valore di g_SCHEDULER
    this.w_MODSCHED = "N"
    SELECT * FROM ( this.oParentObject.w_AGKCT_SZOOM.cCursor ) WHERE XCHK = 1 INTO CURSOR TIPIATT
    if RECCOUNT("TIPIATT") = 0
      ah_ErrorMsg("Attenzione: selezionare almeno un tipo attivit�",,"")
      * --- Chiusura cursore
      if USED("TIPIATT")
        SELECT TIPIATT
        USE
      endif
      i_retcode = 'stop'
      return
    else
      this.w_oERRORLOG=createobject("AH_ErrorLog")
      if (VARTYPE(g_SCHEDULER)<>"C" Or g_SCHEDULER<>"S")
        public g_SCHEDULER
        g_SCHEDULER="S"
        this.w_MODSCHED = "S"
      endif
      SELECT TIPIATT
      GO TOP
      SCAN
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ENDSCAN
      if this.w_MODSCHED="S"
        g_SCHEDULER="N"
      endif
      if this.w_oERRORLOG.IsFullLog()
        if this.oParentObject.w_SoloPrest = "N"
          this.w_oERRORLOG.PrintLog("Cancellazione tipi attivit�",.f.,.t.,"Non sono stati eliminati alcuni tipi attivit�.%0Si desidera stampare il relativo elenco?")     
        else
          this.w_oERRORLOG.PrintLog("Cancellazione prestazioni tipi attivit�",.f.,.t.,"Non sono state eliminate le prestazioni di alcuni tipi attivit�.%0Si desidera stampare il relativo elenco?")     
        endif
      endif
      this.w_oERRORLOG = .NULL.
      This.oparentobject.Notifyevent("Ricerca")
    endif
    * --- Chiusura cursore
    if USED("TIPIATT")
      SELECT TIPIATT
      USE
    endif
    ah_ErrorMsg("Cancellazione ultimata",,"")
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina singolo tipo attivit�
    this.w_EliminaTipo = .T.
    this.w_CodTipoAtt = TIPIATT.CACODICE
    * --- Se disattivo il check  'Cancella solo le prestazioni nei tipi attivit�'
    if this.oParentObject.w_SoloPrest = "N"
      if this.oParentObject.w_SELRAGG="S"
        * --- Vengono eliminati i raggruppamenti contenenti il tipo attivit� in esame
        * --- Try
        local bErr_0390AAA8
        bErr_0390AAA8=bTrsErr
        this.Try_0390AAA8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_EliminaTipo = .F.
          this.w_oERRORLOG.AddMsgLog("Non � stato possibile eliminare il tipo attivit� %1", this.w_CodTipoAtt)     
        endif
        bTrsErr=bTrsErr or bErr_0390AAA8
        * --- End
      endif
      if this.w_EliminaTipo
        this.w_OBJECT = GSAG_MCA()
        * --- Controllo se ha passato il test di accesso
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.ECPFILTER()     
        this.w_OBJECT.w_CACODICE = this.w_CodTipoAtt
        this.w_OBJECT.ECPSAVE()     
        this.w_OBJECT.ECPDELETE()     
        this.w_OBJECT.ECPQUIT()     
        this.w_OBJECT = .NULL.
        * --- Controlla se � stato effettivamente eliminato il tipo attivit�
        * --- Read from CAUMATTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAUMATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODICE"+;
            " from "+i_cTable+" CAUMATTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_CodTipoAtt);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODICE;
            from (i_cTable) where;
                CACODICE = this.w_CodTipoAtt;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CodTipoElim = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_CodTipoElim)
          this.w_oERRORLOG.AddMsgLog("Non � stato possibile eliminare il tipo attivit� %1", this.w_CodTipoAtt)     
        endif
      endif
    else
      * --- Cancella solo le prestazioni del tipo attivit� in esame
      * --- Try
      local bErr_03909218
      bErr_03909218=bTrsErr
      this.Try_03909218()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        this.w_oERRORLOG.AddMsgLog("Non � stato possibile eliminare le prestazioni del tipo attivit� %1", this.w_CodTipoAtt)     
      endif
      bTrsErr=bTrsErr or bErr_03909218
      * --- End
    endif
  endproc
  proc Try_0390AAA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from PRO_ITER
    i_nConn=i_TableProp[this.PRO_ITER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_ITER_idx,2],.t.,this.PRO_ITER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select IPCODICE  from "+i_cTable+" PRO_ITER ";
          +" where IPCODCAU="+cp_ToStrODBC(this.w_CodTipoAtt)+"";
          +" group by IPCODICE";
           ,"_Curs_PRO_ITER")
    else
      select IPCODICE from (i_cTable);
       where IPCODCAU=this.w_CodTipoAtt;
       group by IPCODICE;
        into cursor _Curs_PRO_ITER
    endif
    if used('_Curs_PRO_ITER')
      select _Curs_PRO_ITER
      locate for 1=1
      do while not(eof())
      this.w_CodRagg = _Curs_PRO_ITER.IPCODICE
      * --- Delete from PRO_ITER
      i_nConn=i_TableProp[this.PRO_ITER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRO_ITER_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"IPCODICE = "+cp_ToStrODBC(this.w_CodRagg);
               )
      else
        delete from (i_cTable) where;
              IPCODICE = this.w_CodRagg;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
        select _Curs_PRO_ITER
        continue
      enddo
      use
    endif
    return
  proc Try_03909218()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from CAU_ATTI
    i_nConn=i_TableProp[this.CAU_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_ATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CodTipoAtt);
             )
    else
      delete from (i_cTable) where;
            CACODICE = this.w_CodTipoAtt;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PRO_ITER'
    this.cWorkTables[2]='CAUMATTI'
    this.cWorkTables[3]='CAU_ATTI'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PRO_ITER')
      use in _Curs_PRO_ITER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
