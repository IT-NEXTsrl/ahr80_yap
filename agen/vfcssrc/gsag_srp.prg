* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_srp                                                        *
*              Stampa raggruppamenti di prestazioni                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-08-01                                                      *
* Last revis.: 2011-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_srp",oParentObject))

* --- Class definition
define class tgsag_srp as StdForm
  Top    = 12
  Left   = 88

  * --- Standard Properties
  Width  = 514
  Height = 152
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-11-08"
  HelpContextID=160815255
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  OUT_PUTS_IDX = 0
  PRE_ITER_IDX = 0
  PAR_ALTE_IDX = 0
  cPrg = "gsag_srp"
  cComment = "Stampa raggruppamenti di prestazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_COD_AZI = space(5)
  o_COD_AZI = space(5)
  w_CODINI = space(10)
  o_CODINI = space(10)
  w_CODFIN = space(10)
  o_CODFIN = space(10)
  w_DESCRITER = space(40)
  w_DESCRITER2 = space(40)
  w_PAFLDESP = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_srpPag1","gsag_srp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='OUT_PUTS'
    this.cWorkTables[2]='PRE_ITER'
    this.cWorkTables[3]='PAR_ALTE'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsag_srp
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COD_AZI=space(5)
      .w_CODINI=space(10)
      .w_CODFIN=space(10)
      .w_DESCRITER=space(40)
      .w_DESCRITER2=space(40)
      .w_PAFLDESP=space(1)
        .w_COD_AZI = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_COD_AZI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODINI))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODFIN))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
    endwith
    this.DoRTCalc(4,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_COD_AZI<>.w_COD_AZI
            .w_COD_AZI = i_codazi
          .link_1_1('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        if .o_CODFIN<>.w_CODFIN
          .Calculate_TPIZXNARWL()
        endif
        if .o_CODINI<>.w_CODINI
          .Calculate_MTDEMWOJOO()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(2,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
    endwith
  return

  proc Calculate_TPIZXNARWL()
    with this
          * --- Check w_CODFIN
          GSAG_BSP(this;
              ,'CHK_CODFIN';
             )
    endwith
  endproc
  proc Calculate_MTDEMWOJOO()
    with this
          * --- Check w_CODINI
          GSAG_BSP(this;
              ,'CHK_CODINI';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODINI_1_2.visible=!this.oPgFrm.Page1.oPag.oCODINI_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCODFIN_1_3.visible=!this.oPgFrm.Page1.oPag.oCODFIN_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oDESCRITER_1_10.visible=!this.oPgFrm.Page1.oPag.oDESCRITER_1_10.mHide()
    this.oPgFrm.Page1.oPag.oDESCRITER2_1_11.visible=!this.oPgFrm.Page1.oPag.oDESCRITER2_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COD_AZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_AZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_AZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLDESP";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_COD_AZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_COD_AZI)
            select PACODAZI,PAFLDESP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_AZI = NVL(_Link_.PACODAZI,space(5))
      this.w_PAFLDESP = NVL(_Link_.PAFLDESP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COD_AZI = space(5)
      endif
      this.w_PAFLDESP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_AZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_lTable = "PRE_ITER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2], .t., this.PRE_ITER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIT',True,'PRE_ITER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ITCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ITCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ITCODICE',trim(this.w_CODINI))
          select ITCODICE,ITDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ITCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ITCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('PRE_ITER','*','ITCODICE',cp_AbsName(oSource.parent,'oCODINI_1_2'),i_cWhere,'GSAG_MIT',"Raggruppamenti di prestazioni",'GSAG_MIT.PRE_ITER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',oSource.xKey(1))
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',this.w_CODINI)
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ITCODICE,space(10))
      this.w_DESCRITER = NVL(_Link_.ITDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(10)
      endif
      this.w_DESCRITER = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI <= .w_CODFIN OR EMPTY(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande di quello finale")
        endif
        this.w_CODINI = space(10)
        this.w_DESCRITER = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])+'\'+cp_ToStr(_Link_.ITCODICE,1)
      cp_ShowWarn(i_cKey,this.PRE_ITER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_lTable = "PRE_ITER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2], .t., this.PRE_ITER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIT',True,'PRE_ITER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ITCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ITCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ITCODICE',trim(this.w_CODFIN))
          select ITCODICE,ITDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ITCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ITCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('PRE_ITER','*','ITCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_3'),i_cWhere,'GSAG_MIT',"Raggruppamenti di prestazioni",'GSAG_MIT.PRE_ITER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',oSource.xKey(1))
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',this.w_CODFIN)
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ITCODICE,space(10))
      this.w_DESCRITER2 = NVL(_Link_.ITDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(10)
      endif
      this.w_DESCRITER2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI <= .w_CODFIN OR EMPTY(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande di quello finale")
        endif
        this.w_CODFIN = space(10)
        this.w_DESCRITER2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])+'\'+cp_ToStr(_Link_.ITCODICE,1)
      cp_ShowWarn(i_cKey,this.PRE_ITER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_2.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_2.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_3.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_3.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRITER_1_10.value==this.w_DESCRITER)
      this.oPgFrm.Page1.oPag.oDESCRITER_1_10.value=this.w_DESCRITER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRITER2_1_11.value==this.w_DESCRITER2)
      this.oPgFrm.Page1.oPag.oDESCRITER2_1_11.value=this.w_DESCRITER2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CODINI <= .w_CODFIN OR EMPTY(.w_CODFIN))  and not(IsAlt())  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande di quello finale")
          case   not(.w_CODINI <= .w_CODFIN OR EMPTY(.w_CODFIN))  and not(IsAlt())  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COD_AZI = this.w_COD_AZI
    this.o_CODINI = this.w_CODINI
    this.o_CODFIN = this.w_CODFIN
    return

enddefine

* --- Define pages as container
define class tgsag_srpPag1 as StdContainer
  Width  = 510
  height = 152
  stdWidth  = 510
  stdheight = 152
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODINI_1_2 as StdField with uid="LUIWKXTMHR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande di quello finale",;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 138187738,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=105, Top=14, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRE_ITER", cZoomOnZoom="GSAG_MIT", oKey_1_1="ITCODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_2.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCODINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRE_ITER','*','ITCODICE',cp_AbsName(this.parent,'oCODINI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIT',"Raggruppamenti di prestazioni",'GSAG_MIT.PRE_ITER_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ITCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_3 as StdField with uid="XYBZFHDLGN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande di quello finale",;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 59741146,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=105, Top=38, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRE_ITER", cZoomOnZoom="GSAG_MIT", oKey_1_1="ITCODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_3.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCODFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRE_ITER','*','ITCODICE',cp_AbsName(this.parent,'oCODFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIT',"Raggruppamenti di prestazioni",'GSAG_MIT.PRE_ITER_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ITCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc


  add object oObj_1_4 as cp_outputCombo with uid="ZLERXIVPWT",left=105, top=67, width=396,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 198058010


  add object oBtn_1_5 as StdButton with uid="FFZUPFQMOE",left=399, top=101, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 234266074;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="DIOMPKAHZX",left=453, top=101, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 168132678;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRITER_1_10 as StdField with uid="HIHAVWGZOZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCRITER", cQueryName = "DESCRITER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 134109083,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=208, Top=14, InputMask=replicate('X',40)

  func oDESCRITER_1_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDESCRITER2_1_11 as StdField with uid="FYWXOPBPSY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCRITER2", cQueryName = "DESCRITER2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 134121883,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=208, Top=38, InputMask=replicate('X',40)

  func oDESCRITER2_1_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="HVQDALZAWB",Visible=.t., Left=2, Top=67,;
    Alignment=1, Width=99, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="ZYYLDVTSYC",Visible=.t., Left=2, Top=13,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="GORDTTCVUF",Visible=.t., Left=2, Top=37,;
    Alignment=1, Width=99, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_srp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
