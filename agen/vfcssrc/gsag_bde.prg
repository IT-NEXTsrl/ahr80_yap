* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bde                                                        *
*              Gestione dettaglio attivit�                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-09                                                      *
* Last revis.: 2015-10-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bde",oParentObject,m.pOper)
return(i_retval)

define class tgsag_bde as StdBatch
  * --- Local variables
  pOper = space(5)
  w_PADRE = .NULL.
  w_cKey = space(20)
  w_ELCONTRA = space(10)
  w_ELCODMOD = space(10)
  w_ELCODIMP = space(10)
  w_ELCODCOM = 0
  w_ELRINNOV = 0
  w_FL_ERROR = .f.
  w_STRUONLY = space(1)
  w_oMESS = .NULL.
  w_oPart = .NULL.
  w_CODESCON = space(50)
  w_MODESCRI = space(50)
  w_IMDESCRI = space(50)
  w_IMROWORD = 0
  w_IMDESCON = space(50)
  w_ELQTAMOV = 0
  w_ELQTACON = 0
  w_ELPREZZO = 0
  w_ELCODART = space(20)
  w_ELUNIMIS = space(3)
  w_CALCONC = 0
  * --- WorkFile variables
  ELE_CONT_idx=0
  TMPCONTR_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Richiamato da GSAG_MDA, GSAG_AAT e GSAG_KDL
    * --- Variabili utilizzate nel vzm se non esistono viene restituito errore
    this.w_PADRE = this.oParentObject
    do case
      case this.pOper=="PSTIN"
        * --- Sbiancamento flag durata effettiva
        this.oParentObject.w_DAFLDEFF = " "
        * --- Attiva il Post-In relativo al Codice Articolo associato al Codice di Ricerca
        this.w_cKey = cp_setAzi(i_TableProp[this.w_PADRE.ART_ICOL_IDX, 2]) + "\" + cp_ToStr(this.oParentObject.w_CACODART, 1)
        cp_ShowWarn(this.w_cKey, this.w_PADRE.ART_ICOL_IDX)
      case this.pOper=="ORECO"
        * --- Se sono in caricamento non vado a stornare le quantit� 
        *     precedentemente consumate ma verifico solo che quelle attuali
        *     siano disponibili
        * --- Se sono in cancellazione storno le quantit� precedentemente consumate,
        *     non verifico le quantit� attuali e non sottraggo le quantit� attuali
        * --- Se non sono in cancellazione, ne in caricamento leggo le quantit� residue di tutti i contratti
        *     associati e gli sommo le quantit� precedentemente consumate leggendole
        *     dal database (Se non sono in inserimento).
        *     Verifico che le quantit� da consumare inserite sulle righe siano disponibili
        *     se non sono disponibili notifico l'errore e annullo l'operazione.
        *     Se sono disponibili incremento la quantit� consumata con quanto presente nelle righe
        this.w_PADRE = this.w_PADRE.GSAG_MDA
        this.w_FL_ERROR = .F.
        this.w_STRUONLY = "N"
        if this.w_PADRE.cFunction<>"Query"
          * --- Recupero le quantit� su riga
          this.w_PADRE.Exec_Select("_RigheD_", "t_DACONTRA, t_DACODMOD, t_DACODIMP, t_DACOCOMP, t_DARINNOV, MAX(t_DAUNIMIS) AS t_DAUNIMIS, SUM(t_DAQTAMOV) AS t_DAQTAMOV", "!EMPTY(NVL(t_DACONTRA, ' '))", "", "t_DACONTRA, t_DACODMOD, t_DACODIMP, t_DACOCOMP, t_DARINNOV")     
          this.w_STRUONLY = "S"
          * --- Create temporary table TMPCONTR
          i_nIdx=cp_AddTableDef('TMPCONTR') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('gsag_bde',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPCONTR_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          this.w_STRUONLY = "N"
          * --- Inserisco nella tabella temporanea i contratti da analizzare
          SELECT("_RigheD_")
          GO TOP
          SCAN
          this.w_ELCONTRA = t_DACONTRA
          this.w_ELCODMOD = t_DACODMOD
          this.w_ELCODIMP = t_DACODIMP
          this.w_ELCODCOM = t_DACOCOMP
          this.w_ELRINNOV = t_DARINNOV
          * --- Insert into TMPCONTR
          i_nConn=i_TableProp[this.TMPCONTR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCONTR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsag1bde",this.TMPCONTR_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          SELECT("_RigheD_")
          ENDSCAN
          if this.w_PADRE.cFunction<>"Load"
            * --- Write into TMPCONTR
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPCONTR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPCONTR_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="ELCONTRA,ELCODMOD,ELCODIMP,ELCODCOM,ELRINNOV"
              do vq_exec with 'gsag_bde',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCONTR_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="TMPCONTR.ELCONTRA = _t2.ELCONTRA";
                      +" and "+"TMPCONTR.ELCODMOD = _t2.ELCODMOD";
                      +" and "+"TMPCONTR.ELCODIMP = _t2.ELCODIMP";
                      +" and "+"TMPCONTR.ELCODCOM = _t2.ELCODCOM";
                      +" and "+"TMPCONTR.ELRINNOV = _t2.ELRINNOV";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ELQTARES = _t2.ELQTARES";
                  +",DAQTACON = _t2.DAQTACON";
                  +i_ccchkf;
                  +" from "+i_cTable+" TMPCONTR, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="TMPCONTR.ELCONTRA = _t2.ELCONTRA";
                      +" and "+"TMPCONTR.ELCODMOD = _t2.ELCODMOD";
                      +" and "+"TMPCONTR.ELCODIMP = _t2.ELCODIMP";
                      +" and "+"TMPCONTR.ELCODCOM = _t2.ELCODCOM";
                      +" and "+"TMPCONTR.ELRINNOV = _t2.ELRINNOV";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCONTR, "+i_cQueryTable+" _t2 set ";
                  +"TMPCONTR.ELQTARES = _t2.ELQTARES";
                  +",TMPCONTR.DAQTACON = _t2.DAQTACON";
                  +Iif(Empty(i_ccchkf),"",",TMPCONTR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="TMPCONTR.ELCONTRA = t2.ELCONTRA";
                      +" and "+"TMPCONTR.ELCODMOD = t2.ELCODMOD";
                      +" and "+"TMPCONTR.ELCODIMP = t2.ELCODIMP";
                      +" and "+"TMPCONTR.ELCODCOM = t2.ELCODCOM";
                      +" and "+"TMPCONTR.ELRINNOV = t2.ELRINNOV";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCONTR set (";
                  +"ELQTARES,";
                  +"DAQTACON";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.ELQTARES,";
                  +"t2.DAQTACON";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="TMPCONTR.ELCONTRA = _t2.ELCONTRA";
                      +" and "+"TMPCONTR.ELCODMOD = _t2.ELCODMOD";
                      +" and "+"TMPCONTR.ELCODIMP = _t2.ELCODIMP";
                      +" and "+"TMPCONTR.ELCODCOM = _t2.ELCODCOM";
                      +" and "+"TMPCONTR.ELRINNOV = _t2.ELRINNOV";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPCONTR set ";
                  +"ELQTARES = _t2.ELQTARES";
                  +",DAQTACON = _t2.DAQTACON";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".ELCONTRA = "+i_cQueryTable+".ELCONTRA";
                      +" and "+i_cTable+".ELCODMOD = "+i_cQueryTable+".ELCODMOD";
                      +" and "+i_cTable+".ELCODIMP = "+i_cQueryTable+".ELCODIMP";
                      +" and "+i_cTable+".ELCODCOM = "+i_cQueryTable+".ELCODCOM";
                      +" and "+i_cTable+".ELRINNOV = "+i_cQueryTable+".ELRINNOV";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ELQTARES = (select ELQTARES from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",DAQTACON = (select DAQTACON from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          vq_Exec("gsag2bde.vqr", this.w_PADRE, "QtaEffet" )
        endif
        if USED("QtaEffet") AND USED("_RigheD_")
          * --- Verifico la disponibilit� dei vari elementi contratti
          SELECT ELCONTRA, ELCODMOD, ELCODIMP, ELCODCOM, ELRINNOV, MAX(t_DAUNIMIS) AS t_DAUNIMIS, MAX(ELQTARES) AS ELQTARES, ; 
 MAX(t_DAQTAMOV) AS t_DAQTAMOV , SUM(t_DAQTAMOV-ELQTARES) AS QTANODIS FROM "QtaEffet" INNER JOIN "_RigheD_" ON ; 
 ELCONTRA=t_DACONTRA AND ELCODMOD=t_DACODMOD AND ELCODIMP=t_DACODIMP AND ELCODCOM=t_DACOCOMP AND ELRINNOV=t_DARINNOV ; 
 GROUP BY ELCONTRA, ELCODMOD, ELCODIMP, ELCODCOM, ELRINNOV HAVING QTANODIS>0 INTO CURSOR "CntNoDis"
          if USED("CntNoDis")
            SELECT("CntNoDis")
            this.w_FL_ERROR = RECCOUNT() > 0
            if this.w_FL_ERROR
              GO TOP
              SCAN
              this.w_oPart = this.w_oMESS.addMsgPartNL( "La quantit� residua ( %1 %2 ) dell'elemento con codice contratto %3 e modello %5 � inferiore rispetto a quella utilizzata ( %1 %4) %0" )
              this.w_oPart.addParam(alltrim(t_DAUNIMIS))     
              this.w_oPart.addParam(alltrim(STR(ELQTARES)))     
              this.w_oPart.addParam(alltrim(ELCONTRA))     
              this.w_oPart.addParam(alltrim(STR(t_DAQTAMOV)))     
              this.w_oPart.addParam(alltrim(ELCODMOD))     
              ENDSCAN
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_oMESS.ComposeMessage()
            endif
          endif
        endif
        if !this.w_FL_ERROR
          * --- Se non sono presenti errori aggiorno il saldo della quantit� consumata
          if this.w_PADRE.cFunction<>"Load"
            * --- Storno le quantit� consumate all'ultimo salvataggio
            * --- Write into ELE_CONT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ELE_CONT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="ELCONTRA,ELCODMOD,ELCODIMP,ELCODCOM,ELRINNOV"
              do vq_exec with 'gsag_bde',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
                      +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
                      +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
                      +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
                      +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ELQTACON = ELE_CONT.ELQTACON-_t2.DAQTACON";
                  +i_ccchkf;
                  +" from "+i_cTable+" ELE_CONT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
                      +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
                      +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
                      +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
                      +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT, "+i_cQueryTable+" _t2 set ";
                  +"ELE_CONT.ELQTACON = ELE_CONT.ELQTACON-_t2.DAQTACON";
                  +Iif(Empty(i_ccchkf),"",",ELE_CONT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="ELE_CONT.ELCONTRA = t2.ELCONTRA";
                      +" and "+"ELE_CONT.ELCODMOD = t2.ELCODMOD";
                      +" and "+"ELE_CONT.ELCODIMP = t2.ELCODIMP";
                      +" and "+"ELE_CONT.ELCODCOM = t2.ELCODCOM";
                      +" and "+"ELE_CONT.ELRINNOV = t2.ELRINNOV";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT set (";
                  +"ELQTACON";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"ELQTACON-t2.DAQTACON";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
                      +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
                      +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
                      +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
                      +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT set ";
                  +"ELQTACON = ELE_CONT.ELQTACON-_t2.DAQTACON";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".ELCONTRA = "+i_cQueryTable+".ELCONTRA";
                      +" and "+i_cTable+".ELCODMOD = "+i_cQueryTable+".ELCODMOD";
                      +" and "+i_cTable+".ELCODIMP = "+i_cQueryTable+".ELCODIMP";
                      +" and "+i_cTable+".ELCODCOM = "+i_cQueryTable+".ELCODCOM";
                      +" and "+i_cTable+".ELRINNOV = "+i_cQueryTable+".ELRINNOV";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ELQTACON = (select "+i_cTable+".ELQTACON-DAQTACON from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if USED("_RigheD_")
            * --- Aggiungo le quantit� consumate attuali
            SELECT("_RigheD_")
            GO TOP
            SCAN
            * --- Try
            local bErr_031C6898
            bErr_031C6898=bTrsErr
            this.Try_031C6898()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_031C6898
            * --- End
            SELECT("_RigheD_")
            ENDSCAN
          endif
        endif
        if this.w_PADRE.cFunction<>"Query"
          * --- Drop temporary table TMPCONTR
          i_nIdx=cp_GetTableDefIdx('TMPCONTR')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPCONTR')
          endif
        endif
        USE IN SELECT("_RigheD_")
        USE IN SELECT("CntNoDis")
        USE IN SELECT("QtaEffet")
      case this.pOper=="DISAB" AND NOT ISALT()
        this.oParentObject.w_FLCNTCHK = !EMPTY(this.oParentObject.w_DACONTRA) AND (!EMPTY(.o_DACODATT) or !EMPTY(.o_DACODICE))
        this.oParentObject.w_DACONTRA = IIF(this.oParentObject.w_FLCNTCHK, SPACE(10), this.oParentObject.w_DACONTRA )
        this.oParentObject.w_DACODMOD = IIF(this.oParentObject.w_FLCNTCHK, SPACE(10), this.oParentObject.w_DACODMOD )
        this.oParentObject.w_DACODIMP = IIF(this.oParentObject.w_FLCNTCHK, SPACE(10), this.oParentObject.w_DACODIMP )
        this.oParentObject.w_DARINNOV = IIF(this.oParentObject.w_FLCNTCHK, 0, this.oParentObject.w_DARINNOV )
        this.oParentObject.w_FLCNTCHK = IIF( this.oParentObject.w_FLCNTCHK, ah_ErrorMsg("L'elemento contratto precedentemente abbinato � stato rimosso") , .T. )
      case this.pOper == "LEGGETARCONC"
        if ISALT()
          * --- Se non si vuole mantenere la tariffa e se sulla pratica � definita Tariffa concordata e se siamo in presenza di una tariffa a tempo
          if this.oParentObject.w_OKTARCON<>"N" AND this.oParentObject.oParentObject.w_ATTARTEM="C" AND this.oParentObject.w_ARPRESTA="P"
            * --- Dichiarazione array tariffe a tempo concordate
            DECLARE ARRTARCON (3,1)
            * --- Azzero l'array che verr� riempito dalla funzione Caltarcon()
            ARRTARCON(1)=0 
 ARRTARCON(2)= " " 
 ARRTARCON(3)=0
            this.w_CALCONC = CALTARCON(this.oParentObject.oParentObject.w_ATCODPRA, this.oParentObject.w_DACODRES, this.oParentObject.w_DACODATT, @ARRTARCON)
            * --- Tariffa concordata
            this.oParentObject.oParentObject.w_ATTARCON = ARRTARCON(1)
            this.oParentObject.w_DAUNIMIS = IIF(Not Empty(ARRTARCON(2)), ARRTARCON(2), this.oParentObject.w_DAUNIMIS)
            this.oParentObject.w_UNMIS1 = IIF(Not Empty(ARRTARCON(2)), ARRTARCON(2), this.oParentObject.w_UNMIS1)
            this.oParentObject.w_DAQTAMOV = IIF(Not Empty(ARRTARCON(3)), ARRTARCON(3), this.oParentObject.w_DAQTAMOV)
            * --- Read from UNIMIS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.UNIMIS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ,UMMODUM2"+;
                " from "+i_cTable+" UNIMIS where ";
                    +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UNMIS1);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ,UMMODUM2;
                from (i_cTable) where;
                    UMCODICE = this.oParentObject.w_UNMIS1;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_DESUNI = NVL(cp_ToDate(_read_.UMDESCRI),cp_NullValue(_read_.UMDESCRI))
              this.oParentObject.w_CHKTEMP = NVL(cp_ToDate(_read_.UMFLTEMP),cp_NullValue(_read_.UMFLTEMP))
              this.oParentObject.w_DUR_ORE = NVL(cp_ToDate(_read_.UMDURORE),cp_NullValue(_read_.UMDURORE))
              this.oParentObject.w_FL_FRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
              this.oParentObject.w_FLFRAZ1 = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
              this.oParentObject.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Per non fare rieseguire la routine nella mcalc
            this.oParentObject.o_DACODATT = this.oParentObject.w_DACODATT
            this.oParentObject.o_DACODRES = this.oParentObject.w_DACODRES
            * --- Ricalcola il prezzo
            this.oParentObject.NotifyEvent("Calcola")
          endif
        else
          * --- Per non fare rieseguire la routine nella mcalc
          this.oParentObject.o_DACODATT = this.oParentObject.w_DACODATT
          this.oParentObject.o_DACODRES = this.oParentObject.w_DACODRES
        endif
    endcase
  endproc
  proc Try_031C6898()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ELE_CONT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ELQTACON =ELQTACON+ "+cp_ToStrODBC(t_DAQTAMOV);
          +i_ccchkf ;
      +" where ";
          +"ELCONTRA = "+cp_ToStrODBC(t_DACONTRA);
          +" and ELCODMOD = "+cp_ToStrODBC(t_DACODMOD);
          +" and ELCODIMP = "+cp_ToStrODBC(t_DACODIMP);
          +" and ELCODCOM = "+cp_ToStrODBC(t_DACOCOMP);
          +" and ELRINNOV = "+cp_ToStrODBC(t_DARINNOV);
             )
    else
      update (i_cTable) set;
          ELQTACON = ELQTACON + t_DAQTAMOV;
          &i_ccchkf. ;
       where;
          ELCONTRA = t_DACONTRA;
          and ELCODMOD = t_DACODMOD;
          and ELCODIMP = t_DACODIMP;
          and ELCODCOM = t_DACOCOMP;
          and ELRINNOV = t_DARINNOV;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    this.w_oMESS=createobject("ah_Message")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ELE_CONT'
    this.cWorkTables[2]='*TMPCONTR'
    this.cWorkTables[3]='UNIMIS'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
