* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bai                                                        *
*              Generazione attivit� da ITER                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-16                                                      *
* Last revis.: 2015-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODITER,w_CODPRAT,w_DATRIF,w_CACHKINI,w_CACHKFOR,w_VALPRA,w_CURSORE,w_CC_CONTO,w_CENRIC,w_COMRIC,w_ATTRIC,w_ATTCOS,w_CODNOM,w_ORAINI,w_MININI,w_CUR_EVE,w_NOTE,w_FLGNOT,w_ONLYFIRST,w_InviaMail
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bai",oParentObject,m.w_CODITER,m.w_CODPRAT,m.w_DATRIF,m.w_CACHKINI,m.w_CACHKFOR,m.w_VALPRA,m.w_CURSORE,m.w_CC_CONTO,m.w_CENRIC,m.w_COMRIC,m.w_ATTRIC,m.w_ATTCOS,m.w_CODNOM,m.w_ORAINI,m.w_MININI,m.w_CUR_EVE,m.w_NOTE,m.w_FLGNOT,m.w_ONLYFIRST,m.w_InviaMail)
return(i_retval)

define class tgsag_bai as StdBatch
  * --- Local variables
  w_CODITER = space(20)
  w_CODPRAT = space(15)
  w_DATRIF = ctod("  /  /  ")
  w_CACHKINI = space(1)
  w_CACHKFOR = space(1)
  w_VALPRA = space(5)
  w_CURSORE = space(20)
  w_CC_CONTO = space(15)
  w_CENRIC = space(15)
  w_COMRIC = space(15)
  w_ATTRIC = space(15)
  w_ATTCOS = space(15)
  w_CODNOM = space(15)
  w_ORAINI = space(2)
  w_MININI = space(2)
  w_CUR_EVE = space(10)
  w_NOTE = space(0)
  w_FLGNOT = space(1)
  w_ONLYFIRST = space(1)
  w_InviaMail = .f.
  w_DECTOT = 0
  w_PACHKFES = space(1)
  w_LISTINOPAR = space(5)
  w_UFFICIO = space(10)
  w_ENTE = space(10)
  w_LISPRA = space(5)
  w_LOCALI = space(30)
  w_LOC_PRA = space(40)
  w_LOC_NOM = space(30)
  w_AT___FAX = space(18)
  w_AT_EMAIL = space(0)
  w_AT_EMPEC = space(0)
  w_ATCELLUL = space(18)
  w_ATTELEFO = space(18)
  w_LORAINI = space(2)
  w_LMININI = space(2)
  w_CALENDARIO = space(5)
  w_TIPPRA = space(10)
  w_MATPRA = space(10)
  w_CNTOBSO = 0
  w_PERSON = space(60)
  w_ATSEREVE = space(10)
  w_ATPERSON = space(60)
  w_ATTELEFO = space(18)
  w_AT_EMAIL = space(0)
  w_ATNOTPIA = space(0)
  w_ATCONTAT = space(5)
  w_ATEVANNO = space(4)
  w_KEYLISTB = space(10)
  w_ROWNUM = 0
  w_IPNUMGIO = 0
  w_IPRIFPRO = 0
  w_ROWORD_PRIMARIGA = 0
  w_DATA_ATT = ctot("")
  w_SAVREC = 0
  w_DATA_ATT = ctot("")
  w_GIO_SOSP = space(1)
  w_ATT_DATA = ctod("  /  /  ")
  w_SOTTRAE_GIO = space(1)
  w_SERIAL_PRIMA_ATT = space(20)
  CONTA_ATT = 0
  w_OBJ = .NULL.
  w_oERRORMESS = .NULL.
  w_ATRIFSER = space(20)
  w_SerialeAttivita = space(20)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_OK = .f.
  w_ATSERIAL = space(20)
  w_NOMSG = space(1)
  w_COD_CAUSALE = space(20)
  DESC_CAUSALE = space(254)
  DATAINI_ATT = ctot("")
  SER_ATT_RIF = space(20)
  NUM_GG = 0
  w_DataAppoPromem = ctot("")
  w_CAFLPREA = space(1)
  w_CARAGGST = space(1)
  w_CAMINPRE = 0
  w_CAUDOC = space(5)
  DATAINI = ctot("")
  DATA_INI = ctod("  /  /  ")
  DURATAORE = 0
  DURATAMIN = 0
  GG_PREAV = 0
  STATO = space(1)
  PRIORITA = 0
  TIPOLOGIA = space(5)
  PROMEM = ctot("")
  DISPONIBILITA = 0
  OREINI = space(2)
  MININI = space(2)
  OREFIN = space(2)
  MINFIN = space(2)
  DATAFIN = ctod("  /  /  ")
  OREPRO = space(2)
  MINPRO = space(2)
  VALATT = space(3)
  LISATT = space(5)
  FL_NON_SOG_PRE = space(1)
  w_ATFLNOTI = space(1)
  w_ATDATINI = ctot("")
  w_ATDATFIN = ctot("")
  w_DataCambiata = .f.
  w_ShowMsg = .f.
  w_SERIAL = space(20)
  w_FLGLIS = space(1)
  w_UDIENZA = 0
  w_CHKFOR = space(1)
  w_CHKINI = space(1)
  w_OBJ = .NULL.
  w_CODVAL = space(3)
  w_DECUNI = 0
  w_OGGETTO = space(80)
  w_DATACAL = ctot("")
  w_NOTEERRORE = space(254)
  SAV_REC = 0
  w_ROW_ORD = 0
  w_ERROR = 0
  CONTA_ATT = 0
  w_RESULT = space(254)
  CURSORE = space(10)
  w_COD_PART = space(5)
  w_NUMRIGA = 0
  w_TipoPartecip = space(1)
  w_DPGRUPRE = space(5)
  * --- WorkFile variables
  PAR_AGEN_idx=0
  OFF_ATTI_idx=0
  CAUMATTI_idx=0
  TIP_DOCU_idx=0
  VALUTE_idx=0
  OFF_NOMI_idx=0
  DIPENDEN_idx=0
  CAN_TIER_idx=0
  RIN_PART_idx=0
  CAL_AZIE_idx=0
  DEF_PART_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CARICAMENTO ATTIVITA' DA ITER
    * --- Codice Iter
    * --- Codice pratica
    * --- Data di riferimento
    * --- tipo controllo data iniziale (se previsto dalla gestione
    * --- tipo controllo data iniziale (se previsto dalla gestione
    * --- Valuta di riferimento per listini
    * --- Cursore partecipanti
    * --- Centro di costo
    * --- Centro di ricavo
    * --- Commessa ricavi
    * --- Attivit� ricavi
    * --- Attivit� costi
    * --- Codice nominativo
    * --- Ora iniziale da gestione
    * --- Minuti iniziale da gestione
    * --- Cursore eventi
    * --- Note attivit�
    * --- tipo applicazione note
    *     T su tutte le attivit� del raggruppamento
    *     A non applicare note
    *     P sulla prima attivit�
    *     U solo su quella di tipo udienza
    * --- Se valorizzato a S applica ora solo alla prima attivit� dell'iter
    if Vartype(this.w_ONLYFIRST)<>"C"
      this.w_ONLYFIRST = "N"
    endif
    this.w_CNTOBSO = 0
    this.w_LORAINI = this.w_ORAINI
    this.w_LMININI = this.w_MININI
    * --- Read from CAN_TIER
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNTIPPRA,CNMATPRA"+;
        " from "+i_cTable+" CAN_TIER where ";
            +"CNCODCAN = "+cp_ToStrODBC(this.w_CODPRAT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CNLOCALI,CN__ENTE,CNUFFICI,CNCODVAL,CNCODLIS,CNTIPPRA,CNMATPRA;
        from (i_cTable) where;
            CNCODCAN = this.w_CODPRAT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LOC_PRA = NVL(cp_ToDate(_read_.CNLOCALI),cp_NullValue(_read_.CNLOCALI))
      this.w_ENTE = NVL(cp_ToDate(_read_.CN__ENTE),cp_NullValue(_read_.CN__ENTE))
      this.w_UFFICIO = NVL(cp_ToDate(_read_.CNUFFICI),cp_NullValue(_read_.CNUFFICI))
      this.w_VALPRA = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
      this.w_LISPRA = NVL(cp_ToDate(_read_.CNCODLIS),cp_NullValue(_read_.CNCODLIS))
      this.w_LOC_PRA = NVL(cp_ToDate(_read_.CNLOCALI),cp_NullValue(_read_.CNLOCALI))
      this.w_TIPPRA = NVL(cp_ToDate(_read_.CNTIPPRA),cp_NullValue(_read_.CNTIPPRA))
      this.w_MATPRA = NVL(cp_ToDate(_read_.CNMATPRA),cp_NullValue(_read_.CNMATPRA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from PAR_AGEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PACODLIS,PACHKFES,PACALEND"+;
        " from "+i_cTable+" PAR_AGEN where ";
            +"PACODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PACODLIS,PACHKFES,PACALEND;
        from (i_cTable) where;
            PACODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LISTINOPAR = NVL(cp_ToDate(_read_.PACODLIS),cp_NullValue(_read_.PACODLIS))
      this.w_PACHKFES = NVL(cp_ToDate(_read_.PACHKFES),cp_NullValue(_read_.PACHKFES))
      this.w_CALENDARIO = NVL(cp_ToDate(_read_.PACALEND),cp_NullValue(_read_.PACALEND))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not Empty(this.w_CODNOM)
      * --- Read from OFF_NOMI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NODESCRI,NOLOCALI,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL"+;
          " from "+i_cTable+" OFF_NOMI where ";
              +"NOCODICE = "+cp_ToStrODBC(this.w_CODNOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NODESCRI,NOLOCALI,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL;
          from (i_cTable) where;
              NOCODICE = this.w_CODNOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERSON = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
        this.w_LOC_NOM = NVL(cp_ToDate(_read_.NOLOCALI),cp_NullValue(_read_.NOLOCALI))
        this.w_ATTELEFO = NVL(cp_ToDate(_read_.NOTELEFO),cp_NullValue(_read_.NOTELEFO))
        this.w_AT___FAX = NVL(cp_ToDate(_read_.NOTELFAX),cp_NullValue(_read_.NOTELFAX))
        this.w_AT_EMAIL = NVL(cp_ToDate(_read_.NO_EMAIL),cp_NullValue(_read_.NO_EMAIL))
        this.w_AT_EMPEC = NVL(cp_ToDate(_read_.NO_EMPEC),cp_NullValue(_read_.NO_EMPEC))
        this.w_ATCELLUL = NVL(cp_ToDate(_read_.NONUMCEL),cp_NullValue(_read_.NONUMCEL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_LOCALI = IIF(ISALT(), this.w_LOC_PRA, this.w_LOC_NOM)
    if g_AGFA="S" and VarType(this.w_CUR_EVE)="C" and Used((this.w_CUR_EVE)) AND Reccount((this.w_CUR_EVE)) >0
      * --- Determino evento padre
       
 Select((this.w_CUR_EVE)) 
 Locate for Nvl(EVFLIDPA," ")="S"
      if Found()
        this.w_ATCONTAT = NVL(EVCODPAR,SPACE(5))
        this.w_ATSEREVE = EVSERIAL
        this.w_ATEVANNO = NVL(EV__ANNO, SPACE(4))
        this.w_ATPERSON = LEFT(NVL(EVRIFPER, SPACE(60)), 60)
        this.w_ATTELEFO = NVL(EV_PHONE, SPACE(10) )
        this.w_AT_EMAIL = NVL(EV_EMAIL, " ")
        this.w_AT_EMPEC = NVL(EV_EMPEC, " ")
        this.w_ATNOTPIA = NVL(EV__NOTE, " ")
        this.w_ATCELLUL = LEFT(NVL(EVNUMCEL," "),18)
        this.w_LOCALI = NVL(EVLOCALI," ")
      endif
       
 Select((this.w_CUR_EVE)) 
 Go Top
    endif
    * --- Riempie il cursore Temp con le righe dell'Iter selezionato in maschera
    this.w_KEYLISTB = Sys(2015)
    DECLARE ArrDb (4,1)
    * --- Parametro della query w_CODITER
    vq_exec("Query\GSAG_BIT.vqr", this, "TEMP")
    * --- Rende scrivibile il cursore
    =wrcursor("TEMP")
    * --- Scorre il cursore TEMP andando a scrivervi la data (DateTime) dell' Attivit�
    this.w_SOTTRAE_GIO = "N"
     
 select TEMP 
 Go Top 
 SCAN
    this.w_IPNUMGIO = IPNUMGIO
    this.w_IPRIFPRO = IPRIFPRO
    this.w_DATA_ATT = dtot(this.w_DATRIF)
    if Not empty(this.w_IPRIFPRO)
      * --- Cerco in temp la riga a cui sono collegato per applicare la data attivit� se significativa
      this.w_SAVREC = recno()
      Locate For CPROWORD=this.w_IPRIFPRO and Not (isnull(DATA_ATT) OR empty(DATA_ATT))
      if Found()
        this.w_DATA_ATT = DATA_ATT
      endif
      select TEMP
      goto this.w_SAVREC
    endif
    select TEMP
    this.w_DATA_ATT = this.w_DATA_ATT + (IPNUMGIO * 3600 * 24)
    * --- Se la prima data  � un giorno di sospensione forense e se la riga in esame ha come riferimento la prima
    if this.w_SOTTRAE_GIO="S" AND IPRIFPRO = this.w_ROWORD_PRIMARIGA
      if IPNUMGIO >= 0
        * --- Alla seconda data del raggruppamento viene sottratto un giorno
        this.w_DATA_ATT = this.w_DATA_ATT - (1 * 3600 * 24)
      else
        * --- Alla seconda data del raggruppamento viene aggiunto un giorno
        this.w_DATA_ATT = this.w_DATA_ATT + (1 * 3600 * 24)
      endif
    endif
    * --- Scrive la data (DateTime) iniziale dell' Attivit� sul temporaneo
    if NOT EMPTY(CP_TODATE(CADTOBSO)) AND this.w_DATA_ATT>=CADTOBSO
      this.w_CNTOBSO = this.w_CNTOBSO+1
    endif
    replace DATA_ATT with this.w_DATA_ATT
    * --- Se la prima data  � un giorno di sospensione forense e se la riga in esame ha come riferimento la prima
    if this.w_SOTTRAE_GIO="S" AND IPRIFPRO = this.w_ROWORD_PRIMARIGA
      if IPNUMGIO >= 0
        * --- Al numero di giorni ne viene sottratto uno
        replace IPNUMGIO with IPNUMGIO - 1
      else
        * --- Al numero di giorni ne viene aggiunto uno
        replace IPNUMGIO with IPNUMGIO + 1
      endif
    endif
    if EMPTY(NVL(IPRIFPRO, 0)) AND EMPTY(NVL(IPNUMGIO, 0)) AND this.w_CACHKFOR="S" AND ISALT()
      this.w_ATT_DATA = TTOD(this.w_DATA_ATT)
      * --- Legge dal calendario predefinito
      * --- Read from CAL_AZIE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2],.t.,this.CAL_AZIE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CAFLSOSP"+;
          " from "+i_cTable+" CAL_AZIE where ";
              +"CACODCAL = "+cp_ToStrODBC(this.w_CALENDARIO);
              +" and CAGIORNO = "+cp_ToStrODBC(this.w_ATT_DATA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CAFLSOSP;
          from (i_cTable) where;
              CACODCAL = this.w_CALENDARIO;
              and CAGIORNO = this.w_ATT_DATA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_GIO_SOSP = NVL(cp_ToDate(_read_.CAFLSOSP),cp_NullValue(_read_.CAFLSOSP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se la prima data (priva del Rif. prog. e del N. di giorni) del raggruppamento � un giorno di sospensione forense
      if this.w_GIO_SOSP = "S"
        this.w_SOTTRAE_GIO = "S"
      endif
      select TEMP
      * --- Viene memorizzato il CPROWORD della prima riga del raggruppamento
      this.w_ROWORD_PRIMARIGA = CPROWORD
    endif
    ENDSCAN
    this.w_oERRORMESS=createobject("AH_ErrorLog")
    this.CONTA_ATT = 0
    this.w_SERIAL_PRIMA_ATT = space(20)
    this.w_DATA_ATT = dtot(this.w_DATRIF)
    * --- Scorre il temporaneo per generare le Attivit�
    this.w_OBJ = CREATEOBJECT("ActivityWriter",this,"OFF_ATTI")
    select TEMP 
 Go Top 
 SCAN
    this.CONTA_ATT = this.CONTA_ATT + 1
    this.w_ROWNUM = Nvl(CPROWNUM,0)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Inseriamo il seriale dell'attivit� poich� il dato � disponibile solamente alla fine della generazione attivit�
    select TEMP
    REPLACE ATSERIAL WITH this.w_SERIAL
    ENDSCAN
    if Isahe()
      gsag_bdl(this,"D",this.w_KEYLISTB,this.w_oERRORMESS,Space(5))
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Adesso aggiorniamo tutte le attivit� inserendo in ciascuna i riferimenti corretti
    * --- Scrivo seriale prima attivit� generata nel campo di collegamento 
    *     scrivo riferimenti attivit�  collegata precedentemente valorizzato in TEMP
    *     nel campo di collegamento ATRIFSER
     
 select TEMP 
 Go Top 
 SCAN
    this.w_ATRIFSER = SERATTRIF
    this.w_SerialeAttivita = ATSERIAL
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATRIFSER ="+cp_NullLink(cp_ToStrODBC(this.w_ATRIFSER),'OFF_ATTI','ATRIFSER');
      +",ATCAITER ="+cp_NullLink(cp_ToStrODBC(this.w_SERIAL_PRIMA_ATT),'OFF_ATTI','ATCAITER');
          +i_ccchkf ;
      +" where ";
          +"ATSERIAL = "+cp_ToStrODBC(this.w_SerialeAttivita);
             )
    else
      update (i_cTable) set;
          ATRIFSER = this.w_ATRIFSER;
          ,ATCAITER = this.w_SERIAL_PRIMA_ATT;
          &i_ccchkf. ;
       where;
          ATSERIAL = this.w_SerialeAttivita;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ENDSCAN
    * --- Chiude TEMP
    USE IN SELECT("TEMP")
    * --- oggetto per mess. incrementali
    this.w_oMESS=createobject("ah_message")
    if this.w_CNTOBSO=0
      this.w_oPART = this.w_oMESS.addmsgpartNL("Generate n. %1 attivit�")
      this.w_oPART.addParam(ALLTRIM(STR(this.CONTA_ATT)))     
    else
      this.w_oPART = this.w_oMESS.addmsgpartNL("Generate n. %1 attivit� di cui n. %2 con tipo attivit� obsoleto.%0Verificare manualmente le attivit� generate.")
      this.w_oPART.addParam(ALLTRIM(STR(this.CONTA_ATT)))     
      this.w_oPART.addParam(ALLTRIM(STR(this.w_CNTOBSO)))     
    endif
    this.w_oMESS.ah_ErrorMsg()     
    if this.w_ERROR>0
      this.w_oERRORMESS.PrintLog(this,"Errori /warning riscontrati")     
    endif
    this.w_OBJ = .Null.
    * --- Lancia lo zoom contenente le Attivit� generate dall'iter
    this.w_OK = .F.
    do GSAG_KAT with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if g_AGFA="S" and VarType(this.w_CUR_EVE)="C" and Used((this.w_CUR_EVE)) AND Reccount((this.w_CUR_EVE)) >0
      * --- Associo attivit� generate agli eventi
      this.w_NOMSG = iif(Ah_yesno("Si desidera processare gli eventi?"),"S","N")
      * --- Select from gsagebai
      do vq_exec with 'gsagebai',this,'_Curs_gsagebai','',.f.,.t.
      if used('_Curs_gsagebai')
        select _Curs_gsagebai
        locate for 1=1
        do while not(eof())
        this.w_ATSERIAL = _Curs_gsagebai.ATSERIAL
        gsfa_bae(this,this.w_ATSERIAL,this.w_CUR_EVE,this.w_NOMSG)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_gsagebai
          continue
        enddo
        use
      endif
      Use in (this.w_CUR_EVE)
    endif
    i_retcode = 'stop'
    i_retval = this.w_OK
    return
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CARICAMENTO ATTIVITA' DA ITER
    * --- Gestione promemoria
    * --- Legge dati dal cursore TEMP
    this.w_COD_CAUSALE = IPCODCAU
    this.DESC_CAUSALE = IPDESCAU
    this.w_IPRIFPRO = IPRIFPRO
    if Not empty(this.w_IPRIFPRO)
      * --- Cerco in temp la riga a cui sono collegato per applicare la data attivit� se significativa
      this.w_SAVREC = recno()
      Locate For CPROWORD=this.w_IPRIFPRO and Not (isnull(DATA_ATT) OR empty(DATA_ATT))
      if Found()
        this.w_DATA_ATT = DATA_ATT
      endif
      select TEMP
      goto this.w_SAVREC
    else
      this.w_DATA_ATT = dtot(this.w_DATRIF)
    endif
    select TEMP
    * --- Scrive la data (DateTime) iniziale dell' Attivit� sul temporaneo
    this.NUM_GG = IPNUMGIO
    this.DATAINI_ATT = this.w_DATA_ATT + (this.NUM_GG * 3600 * 24)
    this.SER_ATT_RIF = SERATTRIF
    * --- Variabili per GSAG_BAP
    this.w_ShowMsg = .F.
    this.w_SERIAL = space(20)
    * --- Legge i dati della causale attivit� presente sulla riga dell'iter
    * --- Read from CAUMATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CADATINI,CADISPON,CADURMIN,CADURORE,CAFLANAL,CAFLNOTI,CAFLNSAP,CAFLPREA,CAGGPREA,CAMINPRE,CAPRIORI,CAPROMEM,CASTAATT,CATIPATT,CACAUDOC,CACAUACQ,CACHKINI,CACHKFOR,CAORASYS,CARAGGST"+;
        " from "+i_cTable+" CAUMATTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_COD_CAUSALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CADATINI,CADISPON,CADURMIN,CADURORE,CAFLANAL,CAFLNOTI,CAFLNSAP,CAFLPREA,CAGGPREA,CAMINPRE,CAPRIORI,CAPROMEM,CASTAATT,CATIPATT,CACAUDOC,CACAUACQ,CACHKINI,CACHKFOR,CAORASYS,CARAGGST;
        from (i_cTable) where;
            CACODICE = this.w_COD_CAUSALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.DATAINI = NVL((_read_.CADATINI),cp_NullValue(_read_.CADATINI))
      this.DISPONIBILITA = NVL(cp_ToDate(_read_.CADISPON),cp_NullValue(_read_.CADISPON))
      this.DURATAMIN = NVL(cp_ToDate(_read_.CADURMIN),cp_NullValue(_read_.CADURMIN))
      this.DURATAORE = NVL(cp_ToDate(_read_.CADURORE),cp_NullValue(_read_.CADURORE))
      w_FLANAL = NVL(cp_ToDate(_read_.CAFLANAL),cp_NullValue(_read_.CAFLANAL))
      this.w_ATFLNOTI = NVL(cp_ToDate(_read_.CAFLNOTI),cp_NullValue(_read_.CAFLNOTI))
      this.FL_NON_SOG_PRE = NVL(cp_ToDate(_read_.CAFLNSAP),cp_NullValue(_read_.CAFLNSAP))
      this.w_CAFLPREA = NVL(cp_ToDate(_read_.CAFLPREA),cp_NullValue(_read_.CAFLPREA))
      this.GG_PREAV = NVL(cp_ToDate(_read_.CAGGPREA),cp_NullValue(_read_.CAGGPREA))
      this.w_CAMINPRE = NVL(cp_ToDate(_read_.CAMINPRE),cp_NullValue(_read_.CAMINPRE))
      this.PRIORITA = NVL(cp_ToDate(_read_.CAPRIORI),cp_NullValue(_read_.CAPRIORI))
      this.PROMEM = NVL((_read_.CAPROMEM),cp_NullValue(_read_.CAPROMEM))
      this.STATO = NVL(cp_ToDate(_read_.CASTAATT),cp_NullValue(_read_.CASTAATT))
      this.TIPOLOGIA = NVL(cp_ToDate(_read_.CATIPATT),cp_NullValue(_read_.CATIPATT))
      this.w_CAUDOC = NVL(cp_ToDate(_read_.CACAUDOC),cp_NullValue(_read_.CACAUDOC))
      this.PROMEM = NVL((_read_.CAPROMEM),cp_NullValue(_read_.CAPROMEM))
      this.DISPONIBILITA = NVL(cp_ToDate(_read_.CADISPON),cp_NullValue(_read_.CADISPON))
      this.FL_NON_SOG_PRE = NVL(cp_ToDate(_read_.CAFLNSAP),cp_NullValue(_read_.CAFLNSAP))
      this.w_ATFLNOTI = NVL(cp_ToDate(_read_.CAFLNOTI),cp_NullValue(_read_.CAFLNOTI))
      this.w_CAFLPREA = NVL(cp_ToDate(_read_.CAFLPREA),cp_NullValue(_read_.CAFLPREA))
      this.w_CAMINPRE = NVL(cp_ToDate(_read_.CAMINPRE),cp_NullValue(_read_.CAMINPRE))
      this.w_CAFLPREA = NVL(cp_ToDate(_read_.CAFLPREA),cp_NullValue(_read_.CAFLPREA))
      w_CAUACQ = NVL(cp_ToDate(_read_.CACAUACQ),cp_NullValue(_read_.CACAUACQ))
      this.w_CHKINI = NVL(cp_ToDate(_read_.CACHKINI),cp_NullValue(_read_.CACHKINI))
      this.w_CHKFOR = NVL(cp_ToDate(_read_.CACHKFOR),cp_NullValue(_read_.CACHKFOR))
      w_CAORASYS = NVL(cp_ToDate(_read_.CAORASYS),cp_NullValue(_read_.CAORASYS))
      this.w_CARAGGST = NVL(cp_ToDate(_read_.CARAGGST),cp_NullValue(_read_.CARAGGST))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_CARAGGST="U" and this.w_UDIENZA=0 and this.w_ONLYFIRST="U"
      * --- Marco la prima udienza
      this.w_UDIENZA = 1
    endif
    if this.w_ONLYFIRST="N" or (this.CONTA_ATT > 1 and this.w_ONLYFIRST= "P") or (this.w_UDIENZA <>1 and this.w_ONLYFIRST="U") or !Isalt()
      * --- Se ho opzione applica ora solo alla prima dalla seconda attivit� in poi devo ricalcolare l'ora
      *     altrimenti devo eseguire il ricalcolo anche nella prima attivit� dell'iter
      *     
      *     Se da tipo attivit� lo leggo dal tipo sia il metodo di calcolo
      *     che l'esclusione o meno delle sospensioni forensi
      this.w_ORAINI = PADL(ALLTRIM(STR(HOUR(IIF(w_CAORASYS="S",DATETIME(),this.DATAINI)))),2,"0")
      this.w_MININI = PADL(ALLTRIM(STR(MINUTE(IIF(w_CAORASYS="S",DATETIME(),this.DATAINI)))),2,"0")
    else
      if this.w_UDIENZA=1
        * --- Assegno ora passata alla prima udienza incontrata
        this.w_ORAINI = this.w_LORAINI
        this.w_MININI = this.w_LMININI
      endif
      * --- Eseguo il calcolo come impostato  in maschere. Le sospensioni forensi
      *     sempre escluse solo per alter ego (ed in base a quanto specificato in maschera)
      this.w_UDIENZA = 2
    endif
    if this.w_CACHKINI="T" 
      * --- Se da tipo attivit� lo leggo dal tipo sia il metodo di calcolo
      *     che l'esclusione o meno delle sospensioni forensi
      *     
      *     Eseguo il calcolo come impostato  in maschere. Le sospensioni forensi
      *     sempre escluse solo per alter ego (ed in base a quanto specificato in maschera)
    else
      this.w_CHKFOR = iif( isalt() , this.w_CACHKFOR ,"N")
      this.w_CHKINI = this.w_CACHKINI
    endif
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDFLGLIS"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDFLGLIS;
        from (i_cTable) where;
            TDTIPDOC = this.w_CAUDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLGLIS = NVL(cp_ToDate(_read_.TDFLGLIS),cp_NullValue(_read_.TDFLGLIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se non presenti le ore e i minuti iniziali sulla causale
    if Not empty(this.w_ORAINI)
      * --- Se passata applico ora da gestione
      this.OREINI = RIGHT("00"+Alltrim(this.w_ORAINI),2)
    else
      this.OREINI = PADL(ALLTRIM(STR(HOUR(IIF(w_CAORASYS="S",DATETIME(),this.DATAINI)))),2,"0")
    endif
    if Not empty(this.w_MININI)
      * --- Se passata applico ora da gestione
      this.MININI = RIGHT("00"+Alltrim(this.w_MININI),2)
    else
      this.MININI = PADL(ALLTRIM(STR(MINUTE(IIF(w_CAORASYS="S",DATETIME(),this.DATAINI)))),2,"0")
    endif
    this.DATA_INI = IIF(VARTYPE(this.DATAINI_ATT)="D", this.DATAINI_ATT, TTOD(this.DATAINI_ATT))
    * --- Data, ore e minuti finali
    this.DATAFIN = this.DATA_INI+INT( (VAL(this.OREINI)+this.DURATAORE+INT((VAL(this.MININI)+this.DURATAMIN)/60)) / 24)
    this.OREFIN = RIGHT("00"+ALLTRIM(STR(MOD((VAL(this.OREINI)+this.DURATAORE+INT((VAL(this.MININI)+this.DURATAMIN)/60)),24))),2)
    this.MINFIN = RIGHT("00"+ALLTRIM(STR(MOD((VAL(this.MININI)+this.DURATAMIN),60),2)),2)
    * --- Ore e minuti promemoria
    this.OREPRO = RIGHT("00"+ALLTRIM(STR(HOUR(this.PROMEM),2)),2)
    this.MINPRO = RIGHT("00"+ALLTRIM(STR(MINUTE(this.PROMEM),2)),2)
    * --- Se selezionata la pratica in maschera
    this.VALATT = g_PERVAL
    if Isalt()
      if NOT EMPTY(this.w_CODPRAT)
        * --- Valuta dell' Attivit�
        this.VALATT = iif(!empty(this.w_VALPRA), this.w_VALPRA, g_PERVAL)
        this.LISATT = this.w_LISPRA
      else
        this.VALATT = g_PERVAL
        * --- Read from PAR_AGEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PACODLIS"+;
            " from "+i_cTable+" PAR_AGEN where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PACODLIS;
            from (i_cTable) where;
                PACODAZI = i_codazi;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.LISATT = NVL(cp_ToDate(_read_.PACODLIS),cp_NullValue(_read_.PACODLIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    if empty(this.LISATT)
      * --- Read from PAR_AGEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PACODLIS,PALISACQ"+;
          " from "+i_cTable+" PAR_AGEN where ";
              +"PACODAZI = "+cp_ToStrODBC(i_codazi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PACODLIS,PALISACQ;
          from (i_cTable) where;
              PACODAZI = i_codazi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.LISATT = NVL(cp_ToDate(_read_.PACODLIS),cp_NullValue(_read_.PACODLIS))
        w_ATLISACQ = NVL(cp_ToDate(_read_.PALISACQ),cp_NullValue(_read_.PALISACQ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Inseriamo l'attivit� tramite l'oggetto ActivityWriter
    this.w_OBJ.bSospForensi = this.w_CHKFOR="S"
    this.w_ATDATINI = cp_CharToDatetime(DTOC(TTOD(this.DATAINI_ATT))+" "+this.OREINI+":"+this.MININI+":00")
    this.w_ATDATFIN = cp_CharToDatetime(DTOC(this.DATAFIN)+" "+this.OREFIN+":"+this.MINFIN+":00")
    * --- Gestione promemoria demandata all'oggetto ActivityWriter
    this.w_DataCambiata = .T.
    * --- Creiamo tutte le propriet� dell'oggetto in modo che l'attivit� abbia i valori corretti
    if Not Empty(this.w_NOTE)
      do case
        case this.w_FLGNOT="T"
          this.w_OBJ.w_ATNOTPIA = this.w_NOTE
        case this.w_FLGNOT="P"
          this.w_OBJ.w_ATNOTPIA = this.w_NOTE
          this.w_NOTE = " "
      endcase
    endif
    this.w_OBJ.w_ATDATINI = this.w_ATDATINI
    this.w_OBJ.w_ATDATFIN = this.w_ATDATFIN
    this.w_OBJ.w_UTCC = i_CodUte
    this.w_OBJ.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_OBJ.w_ATOGGETT = this.DESC_CAUSALE
    this.w_OBJ.w_ATFLNOTI = this.w_ATFLNOTI
    this.w_OBJ.w_ATCODATT = this.TIPOLOGIA
    this.w_OBJ.w_ATDATRIN = cp_CharToDatetime("  -  -       :  :  ")
    this.w_OBJ.w_ATDATPRO = cp_CharToDatetime("  -  -       :  :  ")
    this.w_OBJ.w_ATCAUATT = this.w_COD_CAUSALE
    this.w_OBJ.w_ATNUMGIO = this.NUM_GG
    this.w_OBJ.w_ATNUMPRI = this.PRIORITA
    this.w_OBJ.w_ATPERCOM = IIF(this.STATO="F", 100, 0)
    this.w_OBJ.w_ATPERSON = this.w_PERSON
    this.w_OBJ.w_ATSTATUS = this.STATO
    this.w_OBJ.w_ATSTAATT = this.DISPONIBILITA
    this.w_OBJ.w_ATTIPRIS = NULL
    this.w_OBJ.w_ATCODPRA = this.w_CODPRAT
    this.w_OBJ.w_ATLOCALI = this.w_LOCALI
    this.w_OBJ.w_AT__ENTE = this.w_ENTE
    this.w_OBJ.w_ATUFFICI = this.w_UFFICIO
    this.w_OBJ.w_ATCENCOS = this.w_CC_CONTO
    this.w_OBJ.w_ATCENRIC = this.w_CENRIC
    this.w_OBJ.w_ATATTRIC = this.w_ATTRIC
    this.w_OBJ.w_ATATTCOS = this.w_ATTCOS
    this.w_OBJ.w_ATCOMRIC = this.w_COMRIC
    * --- sfrutto valorizzazione del generatore attivit�
    this.w_OBJ.w_ATCODLIS = iif(Isalt(),this.LISATT,Space(5))
    this.w_OBJ.w_ATCODVAL = this.VALATT
    this.w_CODVAL = this.VALATT
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECUNI"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECUNI;
        from (i_cTable) where;
            VACODVAL = this.w_CODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Nominativo e dati ad esso correlati
    this.w_OBJ.w_ATCODNOM = this.w_CODNOM
    * --- Read from OFF_NOMI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "NOTIPNOM,NOCODCLI"+;
        " from "+i_cTable+" OFF_NOMI where ";
            +"NOCODICE = "+cp_ToStrODBC(this.w_CODNOM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        NOTIPNOM,NOCODCLI;
        from (i_cTable) where;
            NOCODICE = this.w_CODNOM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_TIPCLI = NVL(cp_ToDate(_read_.NOTIPNOM),cp_NullValue(_read_.NOTIPNOM))
      w_CODCLI = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Isahe() 
      gsag_bdl(this,"D",this.w_KEYLISTB,this.w_oERRORMESS,Space(5))
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_RESULT=CALCLIST("CREA",@ArrDb,"V","N",this.VALATT,w_TIPCLI,w_CODCLI,this.w_ATDATFIN," "," ",this.w_KEYLISTB,-20,"N"," ",Space(5))
    endif
    this.w_OBJ.w_ATPERSON = IIF(NOT EMPTY(this.w_ATPERSON), this.w_ATPERSON, this.w_PERSON)
    this.w_OBJ.w_ATCELLUL = this.w_ATCELLUL
    this.w_OBJ.w_ATTELEFO = this.w_ATTELEFO
    this.w_OBJ.w_AT_EMAIL = this.w_AT_EMAIL
    this.w_OBJ.w_AT_EMPEC = this.w_AT_EMPEC
    this.w_OBJ.w_AT__ENTE = this.w_ENTE
    this.w_OBJ.w_ATSEREVE = this.w_ATSEREVE
    this.w_OBJ.w_ATEVANNO = this.w_ATEVANNO
    this.w_OBJ.w_ATCONTAT = this.w_ATCONTAT
    this.w_OBJ.bSendMSG = .f.
    this.w_OBJ.bNoMSG = .t.
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_CHKINI<>"N"
      * --- Calcolo automatico della data inizio...
      this.w_OBJ.GetFirstDay()     
      this.w_DATACAL = this.w_OBJ.dStartDateTime
      * --- Se data differente e con conferma... o data vuota
      *     mostro la richiesta di nuova data
      if this.w_DATACAL<>this.w_ATDATINI And this.w_CHKINI="C" Or Empty( this.w_DATACAL )
        * --- data differente tra quanto determinato dall'iter e quando disponibile
        *     dai calendari...
        this.w_OGGETTO = this.w_OBJ.w_ATOGGETT
        * --- Visualizzazo il motivo dell'incongruenza (descrizione del giorno), in linea teorica, avendo valutato 
        *     pi� calendari � possibile che i motivi siano pi� di uno
        *     per cui leggo la descrizione del primo calendario che per il giono
        *     calcolato si hanno problemi...
        this.w_NOTEERRORE = this.w_OBJ.cStartDateTimeError
        do GSAG_SGF with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Se il calcolo o l'utente mantiene la data precedetemente calcolata non faccio niente
      *     altrimenti occorre ricalcolare la fine attivit�...
      if this.w_DATACAL<>this.w_ATDATINI And Not Empty( this.w_DATACAL)
        * --- Data, ore e minuti finali
        *     Se data calcolo vuota (esc sulla maschera, lascio il valore precedente..
        this.DATAFIN = TTOD( this.w_DATACAL ) + INT( ( Hour( this.w_DATACAL) +this.DURATAORE+INT( (Minute( this.w_DATACAL ) +this.DURATAMIN)/60)) / 24)
        this.OREFIN = RIGHT("00"+ALLTRIM(STR(MOD(( Hour( this.w_DATACAL) +this.DURATAORE+INT((Minute( this.w_DATACAL ) +this.DURATAMIN)/60)),24))),2)
        this.MINFIN = RIGHT("00"+ALLTRIM(STR(MOD(( Minute( this.w_DATACAL ) +this.DURATAMIN),60),2)),2)
        this.w_ATDATFIN = cp_CharToDatetime(DTOC(this.DATAFIN)+" "+this.OREFIN+":"+this.MINFIN+":00")
        this.w_OBJ.w_ATDATINI = this.w_DATACAL
        this.w_OBJ.w_ATDATFIN = this.w_ATDATFIN
      endif
    endif
    Select Temp 
 replace DATA_ATT with this.w_OBJ.w_ATDATINI
    * --- Try
    local bErr_04322AC8
    bErr_04322AC8=bTrsErr
    this.Try_04322AC8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_ERROR = this.w_ERROR+1
      this.w_RESULT = iif(Empty(this.w_RESULT),Message(),this.w_RESULT)
      this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Riscontrato errore su attivit� %1", space(4)+ALLTRIM(this.w_RESULT))     
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_04322AC8
    * --- End
    * --- Sbianco l'oggetto e valuto una nuova attivit�
    this.w_OBJ.Blank()     
  endproc
  proc Try_04322AC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_RESULT = this.w_OBJ.CreateActivity()
    if EMPTY(this.w_RESULT)
      * --- Creazione attivit� riuscita
      this.w_Serial = this.w_OBJ.w_ATSERIAL
      * --- FINE DA GSAG_BAG
      if this.CONTA_ATT = 1
        this.w_SERIAL_PRIMA_ATT = this.w_SERIAL
        * --- Aggiorna nel cursore TEMP il n. seriale dell'attivit� di riferimento
      endif
      * --- (La pagina � eseguita sotto transazione - vedi pag. 2)
      select TEMP
      this.w_ROW_ORD = CPROWORD
      this.SAV_REC = recno()
      * --- Assegno contenuto di ATRIFSER in funzione dell'iter impostato
       
 SCAN FOR IPRIFPRO = this.w_ROW_ORD 
 replace SERATTRIF with this.w_SERIAL 
 ENDSCAN 
 select TEMP 
 goto this.SAV_REC
      if this.w_InviaMail
        * --- Il terzo parametro serve per impedire il messaggio "Si desidera inviare gli avvisi?"
        GSAG_BAP(this,"C", this.w_SERIAL,.T.)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- commit
      cp_EndTrs(.t.)
    else
      if this.CONTA_ATT = 1
        this.CONTA_ATT = 0
      endif
      * --- Raise
      i_Error="Operazione abbandonata"
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- INSERIMENTO DEI PARTECIPANTI DELLA SINGOLA ATTIVITA'
    *     (la pagina � eseguita sotto transazione - vedi pag. 2)
    this.w_NUMRIGA = 0
    if Used((this.w_CURSORE))
      select (this.w_CURSORE)
      * --- Legge dal cursore i partecipanti selezionati
       
 scan for XCHK = 1 and CPROWNUM=this.w_ROWNUM
      this.w_TipoPartecip = " "
      this.w_NUMRIGA = this.w_NUMRIGA + 1
      this.w_COD_PART = DPCODICE
      * --- Legge il tipo di partecipante in modo che sia avvalorato correttamente in OFF_PART
      * --- Inserisce partecipante
      this.w_TipoPartecip = DPTIPRIS
      this.w_DPGRUPRE = DPGRUPRE
      this.w_OBJ.AddPart(0,0,this.w_TipoPartecip,this.w_COD_PART,this.w_DPGRUPRE)     
      endscan
    else
      * --- E' stato passato il seriale devo determinare i partecipanti 
      * --- Select from RIN_PART
      i_nConn=i_TableProp[this.RIN_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIN_PART_idx,2],.t.,this.RIN_PART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIN_PART ";
            +" where PASERIAL="+cp_ToStrODBC(this.w_CURSORE)+"";
             ,"_Curs_RIN_PART")
      else
        select * from (i_cTable);
         where PASERIAL=this.w_CURSORE;
          into cursor _Curs_RIN_PART
      endif
      if used('_Curs_RIN_PART')
        select _Curs_RIN_PART
        locate for 1=1
        do while not(eof())
        * --- Inserisce partecipante
        this.w_NUMRIGA = this.w_NUMRIGA + 1
        this.w_TipoPartecip = _Curs_RIN_PART.PATIPRIS
        this.w_COD_PART = _Curs_RIN_PART.PACODRIS
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPGRUPRE"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(this.w_COD_PART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPGRUPRE;
            from (i_cTable) where;
                DPCODICE = this.w_COD_PART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DPGRUPRE = NVL(cp_ToDate(_read_.DPGRUPRE),cp_NullValue(_read_.DPGRUPRE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_OBJ.AddPart(0,0,this.w_TipoPartecip,this.w_COD_PART,this.w_DPGRUPRE)     
          select _Curs_RIN_PART
          continue
        enddo
        use
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_CODITER,w_CODPRAT,w_DATRIF,w_CACHKINI,w_CACHKFOR,w_VALPRA,w_CURSORE,w_CC_CONTO,w_CENRIC,w_COMRIC,w_ATTRIC,w_ATTCOS,w_CODNOM,w_ORAINI,w_MININI,w_CUR_EVE,w_NOTE,w_FLGNOT,w_ONLYFIRST,w_InviaMail)
    this.w_CODITER=w_CODITER
    this.w_CODPRAT=w_CODPRAT
    this.w_DATRIF=w_DATRIF
    this.w_CACHKINI=w_CACHKINI
    this.w_CACHKFOR=w_CACHKFOR
    this.w_VALPRA=w_VALPRA
    this.w_CURSORE=w_CURSORE
    this.w_CC_CONTO=w_CC_CONTO
    this.w_CENRIC=w_CENRIC
    this.w_COMRIC=w_COMRIC
    this.w_ATTRIC=w_ATTRIC
    this.w_ATTCOS=w_ATTCOS
    this.w_CODNOM=w_CODNOM
    this.w_ORAINI=w_ORAINI
    this.w_MININI=w_MININI
    this.w_CUR_EVE=w_CUR_EVE
    this.w_NOTE=w_NOTE
    this.w_FLGNOT=w_FLGNOT
    this.w_ONLYFIRST=w_ONLYFIRST
    this.w_InviaMail=w_InviaMail
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='PAR_AGEN'
    this.cWorkTables[2]='OFF_ATTI'
    this.cWorkTables[3]='CAUMATTI'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='OFF_NOMI'
    this.cWorkTables[7]='DIPENDEN'
    this.cWorkTables[8]='CAN_TIER'
    this.cWorkTables[9]='RIN_PART'
    this.cWorkTables[10]='CAL_AZIE'
    this.cWorkTables[11]='DEF_PART'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_gsagebai')
      use in _Curs_gsagebai
    endif
    if used('_Curs_RIN_PART')
      use in _Curs_RIN_PART
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODITER,w_CODPRAT,w_DATRIF,w_CACHKINI,w_CACHKFOR,w_VALPRA,w_CURSORE,w_CC_CONTO,w_CENRIC,w_COMRIC,w_ATTRIC,w_ATTCOS,w_CODNOM,w_ORAINI,w_MININI,w_CUR_EVE,w_NOTE,w_FLGNOT,w_ONLYFIRST,w_InviaMail"
endproc
