* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ba5                                                        *
*              Esegue zoom ricerca attivita - note                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-09                                                      *
* Last revis.: 2011-02-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_ba5",oParentObject)
return(i_retval)

define class tgsag_ba5 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ZOOM - determinazione del giorno della settimana
    if USED(this.oParentObject.w_AGKRA_ZOOM3.cCursor)
      if RECCOUNT(this.oParentObject.w_AGKRA_ZOOM3.cCursor)>0
        * --- Sistemazione zoom Note/Cose da Fare/Preavvisi
        SELECT ( this.oParentObject.w_AGKRA_ZOOM3.cCursor )
        * --- Solo se la data "Entro il" non � vuota
        if NVL(DOW(ATDATORD),0)<>0
          REPLACE ALL GIOORD WITH LEFT(g_GIORNO[DOW(ATDATORD)],3)
        endif
        SELECT ( this.oParentObject.w_AGKRA_ZOOM3.cCursor )
        go top
        this.oParentObject.w_AGKRA_ZOOM3.Refresh()     
      endif
    endif
    this.bUpdateParentObject=.f.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
