* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_btm                                                        *
*              Timer - inserimento prestazioni                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-14                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pVisAttivita
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_btm",oParentObject,m.pVisAttivita)
return(i_retval)

define class tgsag_btm as StdBatch
  * --- Local variables
  pVisAttivita = space(1)
  w_OBJECT = .NULL.
  w_OBJ = .NULL.
  w_ObjMDA = .NULL.
  w_PADRE = .NULL.
  w_STATUS = space(1)
  w_MODIFICATO = .f.
  w_okCtrl = .f.
  NC = space(10)
  w_EVSERIAL = space(10)
  w_EV__ANNO = space(4)
  w_DATFIN = ctot("")
  w_OKEVE = .f.
  w_FLNASAP = space(1)
  w_EVNOMINA = space(15)
  w_GENPRE = space(1)
  w_LCODICE = space(20)
  w_NUMSOGG = space(15)
  w_CODSES = space(15)
  w_FLRESTO = .f.
  w_PARAME = 0
  w_CODPRA = space(15)
  w_TOTCALC = 0
  w_TOTPARAME = 0
  w_PRPREZZO = 0
  w_TOTALE = 0
  w_SERTOT = space(10)
  w_RESULT = space(254)
  w_RESOCON1 = space(0)
  w_ANNULLA = .f.
  w_MESBLOK = space(0)
  w_ObjMDP = .NULL.
  w_ROW = 0
  w_OLDCHKTEMP = space(1)
  w_CURSORE = space(10)
  w_RESOCON1 = space(0)
  w_ANNULLA = .f.
  w_MESBLOK = space(0)
  w_RETVAL = 0
  w_CODPRA = space(15)
  w_PROG = .NULL.
  w_TMPC = space(100)
  w_AECODPRA = space(15)
  w_AEORIFIL = space(100)
  w_AEOGGETT = space(220)
  w_AE_TESTO = space(0)
  w_FL_SAVE = .f.
  w_AEPATHFI = space(254)
  w_TIPOALL = space(5)
  w_CLASALL = space(5)
  w_AETIALCP = space(1)
  w_AECLALCP = space(1)
  w_AETYPEAL = space(1)
  w_AENOMEAL = space(1)
  w_AEDESPRA = space(50)
  w_CNCODCAN = space(15)
  w_LSELMUL = .f.
  w_LGFNOMEFILE = space(20)
  w_CNDESCAN = space(100)
  w_CODPRE = space(20)
  w_RAGPRE = space(10)
  w_PROG = .NULL.
  * --- WorkFile variables
  PAR_AGEN_idx=0
  DIPENDEN_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  UNIMIS_idx=0
  ANEVENTI_idx=0
  CAUMATTI_idx=0
  PAR_ALTE_idx=0
  RIS_ESTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da Gsag_KTM Caricare una nuova attivit� con la prestazione desiderata
    *     Da Gsag_KTE Inserimento telefonate e prestazioni
    * --- S completa attivit�
    *     N apri attivit�
    *     E pulsante nuovo
    this.w_PADRE = oParentObject
    if Upper(this.w_PADRE.class)="TGSAG_KTE"
      this.NC = this.oParentObject.w_EVENT.cCursor
      this.w_MODIFICATO = .F.
      Select (this.NC)
      go top
      SCAN FOR XCHK=1
      this.w_EVNOMINA = EVNOMINA
      this.w_MODIFICATO = .T.
      ENDSCAN
      if ! this.w_MODIFICATO
        ah_ErrorMsg("� necessario selezionare almeno un evento","!","")
        i_retcode = 'stop'
        return
      endif
    else
      * --- Salvo per riproporli
      if Isalt()
         this.w_PADRE.Defpratica=this.w_PADRE.w_CodPratica 
 this.w_PADRE.DefResponsabile=this.w_PADRE.w_CODPERS
      endif
    endif
    if this.pVisAttivita <>"E"
      * --- Stato attivit�: completata
      this.w_STATUS = "P"
      * --- Read from CAUMATTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAUMATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CAFLNSAP,CADESCRI,CACAUDOC,CAFLANAL"+;
          " from "+i_cTable+" CAUMATTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_CauDefault);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CAFLNSAP,CADESCRI,CACAUDOC,CAFLANAL;
          from (i_cTable) where;
              CACODICE = this.oParentObject.w_CauDefault;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_FLNSAP = NVL(cp_ToDate(_read_.CAFLNSAP),cp_NullValue(_read_.CAFLNSAP))
        this.oParentObject.w_TxtSel = NVL(cp_ToDate(_read_.CADESCRI),cp_NullValue(_read_.CADESCRI))
        this.oParentObject.w_CAUDOC = NVL(cp_ToDate(_read_.CACAUDOC),cp_NullValue(_read_.CACAUDOC))
        this.oParentObject.w_FLANAL = NVL(cp_ToDate(_read_.CAFLANAL),cp_NullValue(_read_.CAFLANAL))
        this.w_FLNASAP = NVL(cp_ToDate(_read_.CAFLNSAP),cp_NullValue(_read_.CAFLNSAP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Instanzio l'anagrafica
      if g_AGFA="S" and Not EMpty(this.oParentObject.w_EVTIPEVE) and this.oParentObject.w_GENEVE="S"
        * --- Creo evento
        this.w_OKEVE = .t.
        this.w_EV__ANNO = iif(Isahe(),STR(YEAR(i_DATSYS),4,0),CALCESER(i_DATSYS))
        this.w_DATFIN = this.oParentObject.w_DataIniTimer + (this.oParentObject.w_DurataOre*3600)+(this.oParentObject.w_DurataMin*60)
        * --- Try
        local bErr_00EB0660
        bErr_00EB0660=bTrsErr
        this.Try_00EB0660()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_00EB0660
        * --- End
        * --- Try
        local bErr_00EB0D88
        bErr_00EB0D88=bTrsErr
        this.Try_00EB0D88()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_OKEVE = .f.
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_00EB0D88
        * --- End
        if !this.w_OKEVE
          Ah_errormsg("Attenzione, errore nella generazione evento associato all'attivit�")
          i_retcode = 'stop'
          return
        endif
      endif
      if this.pVisAttivita="N"
        g_SCHEDULER="S"
        g_MSG=""
      else
        if this.oParentObject.w_DATIPRIG="N" AND this.oParentObject.w_DATIPRI2="N"
          * --- Se � stato premuto il bottone Attivit� e il tipo riga � non fatturabile allora
          *     Stato attivit�: evasa
          this.w_STATUS = "F"
        endif
      endif
      * --- Read from PAR_ALTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PAGENPRE,PAFLGZER"+;
          " from "+i_cTable+" PAR_ALTE where ";
              +"PACODAZI = "+cp_ToStrODBC(i_CodAzi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PAGENPRE,PAFLGZER;
          from (i_cTable) where;
              PACODAZI = i_CodAzi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_GENPRE = NVL(cp_ToDate(_read_.PAGENPRE),cp_NullValue(_read_.PAGENPRE))
        this.oParentObject.w_FLGZER = NVL(cp_ToDate(_read_.PAFLGZER),cp_NullValue(_read_.PAFLGZER))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Lower(This.oParentObject.class) = "tgsfa_kte"
        if ISAHE()
          this.w_LCODICE = this.oParentObject.w_CODICE
        else
          this.w_LCODICE = this.oParentObject.w_CODSER
        endif
      else
        this.w_LCODICE = this.oParentObject.w_CODSER
      endif
      this.w_okCtrl = .T.
      if this.w_GENPRE=="S"
        * --- Gestione della sola prestazione in PRE_STAZ
        * --- Controlla se l'utente non � amministratore e se � attivo il relativo 
        *     check nei Parametri attivit� e nelle Persone (responsabile)
        if NOT Cp_IsAdministrator() AND this.oParentObject.w_FLPRES = "S" AND this.oParentObject.w_CHKDATAPRE = "S" AND (TTOD(this.oParentObject.w_DataIniTimer) < this.oParentObject.w_DATAPREC OR TTOD(this.oParentObject.w_DataIniTimer) > this.oParentObject.w_DATASUCC)
          this.w_okCtrl = .F.
          if this.pVisAttivita="N"
            g_SCHEDULER="N"
          endif
          ah_ErrorMsg("Data non consentita dai controlli relativi al responsabile")
        else
          this.w_OBJ = CREATEOBJECT("ActivityWriter",this,"PRE_STAZ")
          this.w_OBJ.w_PRCODRES = this.oParentObject.w_CodPart
          this.w_OBJ.w_PRNUMPRA = this.oParentObject.w_CodPratica
          this.w_OBJ.w_PRCODATT = this.w_LCODICE
          this.w_OBJ.w_PR__DATA = TTOD(this.oParentObject.w_DataIniTimer)
          this.w_OBJ.w_PRCODESE = g_CODESE
          this.w_OBJ.w_PRDESPRE = this.oParentObject.w_DesSer
          this.w_OBJ.w_PRDESAGG = this.oParentObject.w_DesAgg
          this.w_OBJ.w_PROREEFF = this.oParentObject.w_DurataOre
          this.w_OBJ.w_PRMINEFF = this.oParentObject.w_DurataMin
          if this.oParentObject.w_UniMisTempo="S"
            this.w_OBJ.w_PRQTAMOV = this.oParentObject.w_QtaTimer
          else
            this.w_OBJ.w_PRQTAMOV = 1
          endif
          this.w_OBJ.w_PRUNIMIS = this.oParentObject.w_PrimaUniMis
          this.w_OBJ.w_PRTIPRIG = this.oParentObject.w_DATIPRIG
          this.w_OBJ.w_PRTIPRI2 = this.oParentObject.w_DATIPRI2
          this.w_OBJ.w_PRCENCOS = this.oParentObject.w_ATCENCOS
          this.w_OBJ.w_PRVOCCOS = this.oParentObject.w_DAVOCCOS
          this.w_OBJ.w_PRTCOINI = this.oParentObject.w_DATCOINI
          this.w_OBJ.w_PRTCOFIN = this.oParentObject.w_DATCOFIN
          this.w_OBJ.w_PRINICOM = this.oParentObject.w_DAINICOM
          this.w_OBJ.w_PRFINCOM = this.oParentObject.w_DAFINCOM
          this.w_OBJ.w_PRATTIVI = this.oParentObject.w_DAATTIVI
          this.w_OBJ.w_PR_SEGNO = this.oParentObject.w_DA_SEGNO
          this.w_OBJ.w_PRCODVAL = g_PERVAL
          if this.oParentObject.w_PARASS>1
            this.w_NUMSOGG = space(15)
            * --- Legge se c'� almeno un soggetto intestatario di parcelle all'interno della pratica
            * --- Read from RIS_ESTR
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "SRCODSES"+;
                " from "+i_cTable+" RIS_ESTR where ";
                    +"SRCODPRA = "+cp_ToStrODBC(this.w_OBJ.w_PRNUMPRA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                SRCODSES;
                from (i_cTable) where;
                    SRCODPRA = this.w_OBJ.w_PRNUMPRA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_NUMSOGG = NVL(cp_ToDate(_read_.SRCODSES),cp_NullValue(_read_.SRCODSES))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_OBJ.bParass = iif(this.oParentObject.w_PARASS>1 AND NOT EMPTY(this.w_NUMSOGG),.t.,.f.)
          * --- Deve calcolare il prezzo della prestazione
          this.w_OBJ.bCalcPrezzo = .T.
          this.w_OBJ.bCalcCosto = .T.
          if UPPER(This.oParentObject.Class)="TGSAG_KTM" 
            this.w_OBJ.bTarcon = .T.
          endif
          * --- Try
          local bErr_00DAA598
          bErr_00DAA598=bTrsErr
          this.Try_00DAA598()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            * --- accept error
            bTrsErr=.f.
            if Empty(this.w_RESULT)
              this.w_RESULT = Message()
            endif
          endif
          bTrsErr=bTrsErr or bErr_00DAA598
          * --- End
          if this.pVisAttivita="N"
            g_SCHEDULER="N"
            this.w_MESBLOK = ""
            this.w_RESOCON1 = g_MSG
            if Not empty(g_MSG) and Ah_yesno("Si desidera vedere log elaborazione?")
              do GSVE_KLG with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
      else
        * --- Attivit� + prestazione
        this.w_OBJECT = GSAG_AAT()
        * --- Controllo se ha passato il test di accesso
        * --- Per il codice utente, restituiamo il codice presente nel file DIPENDEN
        * --- Vado in caricamento
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.ECPLOAD()     
        this.w_OBJECT.w_ActByTimer = "S"
        this.w_OBJECT.w_ATCAUATT = this.oParentObject.w_CauDefault
        GSUT_BVP(this,this.w_OBJECT, "w_ATCAUATT", "["+this.w_OBJECT.w_ATCAUATT+"]")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_OBJECT.w_Noautom = Isalt() OR Lower(This.oParentObject.class) = "tgsfa_kte"
        if Isalt()
          this.w_OBJECT.w_ATCODPRA = this.oParentObject.w_CodPratica
        else
          this.w_OBJECT.w_ATCENCOS = this.oParentObject.w_ATCENCOS
          this.w_OBJECT.w_ATCENRIC = this.oParentObject.w_ATCENRIC
          this.w_OBJECT.w_ATCODPRA = this.oParentObject.w_ATCODPRA
          this.w_OBJECT.w_ATCOMRIC = this.oParentObject.w_ATCOMRIC
          this.w_OBJECT.w_ATATTCOS = this.oParentObject.w_ATATTCOS
          this.w_OBJECT.w_ATATTRIC = this.oParentObject.w_ATATTRIC
        endif
        this.w_OBJECT.NotifyEvent("Agglink")     
        if g_AGFA="S"
          if Lower(This.oParentObject.class) = "tgsfa_kte"
            SetValueLinked("M", this.w_OBJECT, "w_ATCODNOM", this.w_EVNOMINA)
          else
            SetValueLinked("M", this.w_OBJECT,"w_ATCODNOM", this.oParentObject.w_ATCODNOM)
          endif
        endif
        * --- La data inizio attivit� viene ricalcolata al salvataggio
        this.w_OBJECT.w_ATDATINI = this.oParentObject.w_DataIniTimer
        this.w_OBJECT.w_DATINI = TTOD(this.oParentObject.w_DataIniTimer)
        this.w_OBJECT.w_ORAINI = PADL(ALLTRIM(STR(HOUR(this.oParentObject.w_DataIniTimer))),2,"0")
        this.w_OBJECT.w_MININI = PADL(ALLTRIM(STR(MINUTE(this.oParentObject.w_DataIniTimer))),2,"0")
        * --- Forza la data di inizio attivit�
        this.w_OBJECT.w_FORZADTI = .T.
        * --- La data fine attivit� viene ricalcolata al salvataggio
        this.w_OBJECT.w_ATDATFIN = this.oParentObject.w_DataIniTimer+(this.oParentObject.w_DurataOre*3600)+(this.oParentObject.w_DurataMin*60)
        this.w_OBJECT.w_DATFIN = TTOD(this.oParentObject.w_DataIniTimer+(this.oParentObject.w_DurataOre*3600)+(this.oParentObject.w_DurataMin*60))
        this.w_OBJECT.w_ORAFIN = PADL(ALLTRIM(STR(HOUR(this.oParentObject.w_DataIniTimer+(this.oParentObject.w_DurataOre*3600)+(this.oParentObject.w_DurataMin*60)))),2,"0")
        this.w_OBJECT.w_MINFIN = PADL(ALLTRIM(STR(MINUTE(this.oParentObject.w_DataIniTimer+(this.oParentObject.w_DurataOre*3600)+(this.oParentObject.w_DurataMin*60)))),2,"0")
        * --- Forza la data di fine attivit�
        this.w_OBJECT.w_FORZADTF = .T.
        * --- Dobbiamo aggiungere una prestazione con il codice di default dell'operatore
        this.w_OBJECT.w_ATOREEFF = this.oParentObject.w_DurataOre
        this.w_OBJECT.w_ATMINEFF = this.oParentObject.w_DurataMin
        this.w_OBJECT.w_ATOGGETT = this.oParentObject.w_TxtSel
        this.w_OBJECT.w_tipomask = "C"
        if Not Empty(this.oParentObject.w_CODPERS)
          this.w_ObjMDP = this.w_OBJECT.GSAG_MPA
          this.w_ObjMDP = this.w_OBJECT.GSAG_MPA
          this.w_ObjMDP.w_PATIPRIS = "P"
          this.w_ObjMDP.w_TIPORISORSA = "P"
          this.w_ObjMDP.w_PACODRIS = this.oParentObject.w_CodPers
          this.w_ObjMDP.NotifyEvent("Aggioresp")     
        endif
        this.w_ObjMDA = this.w_OBJECT.GSAG_MDA
        * --- Sappiamo il codice della prestazione
        this.w_ObjMDA.AddRow()     
        this.w_ROW = this.w_ObjMDA.rowindex()
        * --- Responsabile valorizzato con il primo partecipante
        if this.w_FLNASAP <> "S" and (Isalt() or UPPER(This.oParentObject.Class)="TGSFA_KTE")
          if this.pVisAttivita="S"
            this.w_OBJECT.oPgFrm.ActivePage = 2
          endif
          = setvaluelinked ( "D" , this.w_ObjMDA, "w_DACODRES" , this.oParentObject.w_CodPart)
          if Isahe()
            this.w_ObjMDA.w_DACODICE = this.w_LCODICE
            this.w_ObjMDA.Notifyevent("Linkpre")     
            this.w_ObjMDA.Notifyevent("w_DACODICE Changed")     
            this.w_ObjMDA.o_DACODICE = this.w_ObjMDA.w_DACODICE
          else
            this.w_ObjMDA.w_DACODATT = this.w_LCODICE
            this.w_ObjMDA.Notifyevent("Linkpre")     
            this.w_ObjMDA.Notifyevent("w_DACODATT Changed")     
            this.w_ObjMDA.o_DACODATT = this.w_ObjMDA.w_DACODATT
          endif
          * --- AHE
          this.w_ObjMDA.Setrow(this.w_ROW)     
          this.w_ObjMDA.w_DAVOCCOS = this.oParentObject.w_DAVOCCOS
          this.w_ObjMDA.w_DACENCOS = this.oParentObject.w_DACENCOS
          this.w_ObjMDA.w_DACODCOM = this.oParentObject.w_DACODCOM
          this.w_ObjMDA.w_DAATTIVI = this.oParentObject.w_DAATTIVI
          this.w_ObjMDA.w_DAVOCRIC = this.oParentObject.w_DAVOCRIC
          this.w_ObjMDA.w_DACENRIC = this.oParentObject.w_DACENRIC
          this.w_ObjMDA.w_DACOMRIC = this.oParentObject.w_DACOMRIC
          this.w_ObjMDA.w_DAATTRIC = this.oParentObject.w_DAATTRIC
          this.w_ObjMDA.w_DA_SEGNO = this.oParentObject.w_DA_SEGNO
          this.w_ObjMDA.w_DAINICOM = this.oParentObject.w_DAINICOM
          this.w_ObjMDA.w_DAFINCOM = this.oParentObject.w_DAFINCOM
          this.w_ObjMDA.w_DATCOINI = this.oParentObject.w_DATCOINI
          this.w_ObjMDA.w_DATCOFIN = this.oParentObject.w_DATCOFIN
          this.w_ObjMDA.w_DATRIINI = this.oParentObject.w_DATRIINI
          this.w_ObjMDA.w_DATRIFIN = this.oParentObject.w_DATRIFIN
          if this.oParentObject.w_UniMisTempo="S"
            this.w_ObjMDA.Set("w_DAQTAMOV",this.oParentObject.w_QtaTimer,.f.,.t.)     
            this.w_ObjMDA.Notifyevent("Aggcosto")     
          endif
          this.w_ObjMDA.SetControlsValue()     
          if UPPER(This.oParentObject.Class)="TGSFA_KTE"
            this.w_OLDCHKTEMP = this.w_ObjMDA.w_CHKTEMP
            this.w_ObjMDA.w_CHKTEMP = "N"
            this.w_ObjMDA.mcalc(.T.)     
            this.w_ObjMDA.w_CHKTEMP = this.w_OLDCHKTEMP
          else
            this.w_ObjMDA.mcalc(.T.)     
          endif
          * --- Impostiamo o_ATCauAtt in modo da impedire la rilettura tarmite mCalc
          this.w_ObjMDA.w_DAOREEFF = this.oParentObject.w_DurataOre
          this.w_ObjMDA.w_DAMINEFF = this.oParentObject.w_DurataMin
          this.w_ObjMDA.w_DADESATT = this.oParentObject.w_DesSer
          this.w_ObjMDA.w_DADESAGG = this.oParentObject.w_DesAgg
          this.w_OBJECT.o_ATCAUATT = this.oParentObject.w_CauDefault
          this.w_ObjMDA.o_DACODRES = this.oParentObject.w_CodPart
          if Lower(This.oParentObject.class) <> "tgsfa_kte" Or this.pVisAttivita="N"
            this.w_OBJECT.w_ATSTATUS = this.w_STATUS
          endif
          this.w_ObjMDA.w_ETIPRIG = this.oParentObject.w_DATIPRIG
          this.w_ObjMDA.w_RTIPRIG = this.oParentObject.w_DATIPRIG
          this.w_ObjMDA.w_DTIPRIG = this.oParentObject.w_DTIPRIG
          this.w_ObjMDA.w_DATIPRIG = this.oParentObject.w_DATIPRIG
          this.w_ObjMDA.w_ATIPRIG = iIF(this.w_ObjMDA.w_TIPART="FO" and this.w_ObjMDA.w_DAPREZZO=0 and this.oParentObject.w_FLGZER="N", "N", IIF(this.oParentObject.w_DTIPRIG="N", "N", this.oParentObject.w_DATIPRIG))
          this.w_ObjMDA.w_DATIPRIG = iif(Isalt(),this.w_ObjMDA.w_ATIPRIG,this.w_ObjMDA.w_RTIPRIG)
          this.w_ObjMDA.w_ETIPRI2 = this.oParentObject.w_DATIPRI2
          this.w_ObjMDA.w_RTIPRI2 = this.oParentObject.w_DATIPRI2
          this.w_ObjMDA.w_DATIPRI2 = this.oParentObject.w_DATIPRI2
          this.w_ObjMDA.w_ATIPRI2 = iIF(this.w_ObjMDA.w_TIPART="FO" and this.w_ObjMDA.w_DAPREZZO=0 and this.oParentObject.w_FLGZER="N", "N", this.oParentObject.w_DATIPRI2)
          this.w_ObjMDA.w_DATIPRI2 = iif(Isalt(),this.w_ObjMDA.w_ATIPRI2,this.w_ObjMDA.w_RTIPRI2)
          this.w_ObjMDA.SaveRow()     
          this.w_ObjMDA.Refresh()     
        endif
        this.w_OBJECT.SetControlsValue()     
        if Not Empty(this.w_EVSERIAL)
          this.w_CURSORE = Sys(2015)
           
 Create cursor (this.w_CURSORE) (EVSERIAL C(10) ,EV__ANNO C(4)) 
 APPEND BLANK 
 Replace EVSERIAL with this.w_EVSERIAL 
 Replace EV__ANNO with this.w_EV__ANNO
          this.w_OBJECT.w_CUR_EVE = this.w_CURSORE
        endif
        * --- Da riconciliazione telefonate
        *     Aggiorno cursore eventi da associare all'attivit�
        if g_AGFA = "S" and Lower(This.oParentObject.class) = "tgsfa_kte"
          this.w_CURSORE = Sys(2015)
          Select EVSERIAL, EV__ANNO From (This.oParentObject.w_EVENT.cCursor) where xChk=1 into cursor (this.w_CURSORE)
          this.w_OBJECT.w_CUR_EVE = this.w_CURSORE
        endif
        * --- Grazie a queste 2 variabili l'anagrafica si chiude
        this.w_OBJECT.w_MaskCalend = this.oParentObject
        this.w_OBJECT.w_AggiornaCalend = .T.
        if this.pVisAttivita="N"
          this.w_OBJECT.EcpSave()     
          g_SCHEDULER="N"
          this.w_MESBLOK = ""
          this.w_RESOCON1 = g_MSG
          if Not empty(g_MSG) and Ah_yesno("Si desidera vedere log elaborazione?")
            do GSVE_KLG with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
      if UPPER(This.oParentObject.Class)<>"TGSFA_KTE" AND this.w_okCtrl
        * --- In entrambi i casi azzeriamo le var. del timer
        this.oParentObject.w_DurataMin = 0
        this.oParentObject.w_DurataOre = 0
        this.oParentObject.w_CodPratica = ""
        this.oParentObject.w_DesPratica = ""
        this.oParentObject.w_DesSer = ""
        this.oParentObject.w_DesAgg = ""
        this.oParentObject.w_DAVOCCOS = ""
        this.oParentObject.w_DAVOCRIC = ""
        this.oParentObject.w_DACENCOS = ""
        this.oParentObject.w_DACODCOM = ""
        this.oParentObject.w_DAATTIVI = ""
        this.oParentObject.w_DA_SEGNO = ""
        this.oParentObject.w_DAINICOM = ctod("  -  -  ")
        this.oParentObject.w_DAFINCOM = ctod("  -  -  ")
        this.oParentObject.w_DataIniTimer = DateTime()
        this.oParentObject.w_CODPERS = " "
        this.oParentObject.w_DPNOME = " "
        this.oParentObject.w_DPCOGNOM = " "
        * --- Azzero il timer
        This.oParentObject.Notifyevent("AzzeraTimer")
        = setvaluelinked ( "M" , this.w_PADRE, "w_CODSER" , this.oParentObject.w_SerUtente)
      endif
    else
      if i_VisualTheme = -1
        L_RETVAL=0
        DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT
        if g_AGFA="S"
          DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Evento")
        endif
        DEFINE BAR 2 OF popCmd PROMPT ah_MsgFormat("Post-in")
        DEFINE BAR 3 OF popCmd PROMPT ah_MsgFormat("E-mail")
        if g_INVIO="O" AND Isalt()
          DEFINE BAR 4 OF popCmd PROMPT ah_MsgFormat("E-mail da archiviare")
        endif
        DEFINE BAR 5 OF popCmd PROMPT ah_MsgFormat("Attivit�")
        DEFINE BAR 6 OF popCmd PROMPT ah_MsgFormat("Attivit� collegate")
        if g_AGFA="S"
          ON SELECTION BAR 1 OF popCmd L_RETVAL = 1
        endif
        ON SELECTION BAR 2 OF popCmd L_RETVAL = 2
        ON SELECTION BAR 3 OF popCmd L_RETVAL = 3
        if g_INVIO="O" AND Isalt()
          ON SELECTION BAR 4 OF popCmd L_RETVAL = 4
        endif
        ON SELECTION BAR 5 OF popCmd L_RETVAL = 5
        ON SELECTION BAR 6 OF popCmd L_RETVAL = 6
        ACTIVATE POPUP popCmd
        DEACTIVATE POPUP popCmd
        RELEASE POPUPS popCmd
        this.w_RETVAL = L_RETVAL
        Release L_RETVAL
      else
        local oBtnMenu, oMenuItem
        oBtnMenu = CreateObject("cbPopupMenu")
        oBtnMenu.oParentObject = This
        oBtnMenu.oPopupMenu.cOnClickProc="ExecScript(NAMEPROC)"
        if g_AGFA="S"
          oMenuItem = oBtnMenu.AddMenuItem()
          oMenuItem.Caption = Ah_Msgformat("Evento")
          oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=1"
          oMenuItem.Visible=.T.
        endif
        oMenuItem = oBtnMenu.AddMenuItem()
        oMenuItem.Caption = Ah_Msgformat("Post in")
        oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=2"
        oMenuItem.Visible=.T.
        oMenuItem = oBtnMenu.AddMenuItem()
        oMenuItem.Caption = Ah_Msgformat("E-mail")
        oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=3"
        oMenuItem.Visible=.T.
        if g_INVIO="O" AND Isalt()
          oMenuItem = oBtnMenu.AddMenuItem()
          oMenuItem.Caption = Ah_Msgformat("E-mail da archiviare")
          oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=4"
          oMenuItem.Visible=.T.
        endif
        oMenuItem = oBtnMenu.AddMenuItem()
        oMenuItem.Caption = Ah_Msgformat("Attivit�")
        oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=5"
        oMenuItem.Visible=.T.
        oMenuItem = oBtnMenu.AddMenuItem()
        oMenuItem.Caption = Ah_Msgformat("Attivit� Collegate")
        oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=6"
        oMenuItem.Visible=.T.
        oBtnMenu.InitMenu()
        oBtnMenu.ShowMenu()
        oBtnMenu = .NULL.
      endif
      if this.w_RETVAL>0
        this.w_CODPRA = this.oParentObject.w_CodPratica
        do case
          case this.w_RETVAL = 1
            * --- Nuovo evento
            this.w_PROG = GSFA_AEV()
            this.w_PROG.Ecpload()     
            GSUT_BVP(this,this.w_PROG, "w_EVCODPRA", "["+this.w_CODPRA+"]")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.w_RETVAL = 2
            * --- Nuovo Postin
            GSUT_BPT(this," ")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.w_RETVAL = 3
            * --- Nuova email
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.w_RETVAL = 4
            this.w_CNCODCAN = this.oParentObject.w_CodPratica
            this.w_AECODPRA = ALLTRIM(this.w_CNCODCAN)
            this.w_AEORIFIL = ""
            this.w_AEOGGETT = ""
            this.w_AE_TESTO = ""
            this.w_AEPATHFI = ""
            this.w_FL_SAVE = .F.
            this.w_TIPOALL = ""
            this.w_CLASALL = ""
            this.w_AETIALCP = "N"
            this.w_AECLALCP = "N"
            this.w_AETYPEAL = "O"
            this.w_AENOMEAL = "O"
            this.w_CNDESCAN = this.oParentObject.w_DesPratica
            do gspr_kae with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_FL_SAVE
              if Isalt() and g_AGEN="S" and (Not Empty(this.w_CODPRE) or Not Empty(this.w_RAGPRE)) and Not Empty(this.w_AECODPRA)
                GSAG_BPI(this,this.w_AECODPRA,this.w_CODPRE,this.w_RAGPRE,"")
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          case this.w_RETVAL = 5
            gsag_bra(this,"N")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.w_RETVAL = 6
            this.w_PROG = GSAG_KIT()
            GSUT_BVP(this,this.w_PROG, "w_CODPRAT", "["+this.w_CODPRA+"]")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
      endif
    endif
    this.w_PADRE = Null
  endproc
  proc Try_00EB0660()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.ANEVENTI_IDX, 3]
    cp_NextTableProg(This, i_Conn, "SEREVE", "i_CODAZI,w_EV__ANNO,w_EVSERIAL", .T. )
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_00EB0D88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ANEVENTI
    i_nConn=i_TableProp[this.ANEVENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ANEVENTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"EV__ANNO"+",EVSERIAL"+",EVOGGETT"+",EV__NOTE"+",EV_STATO"+",EVDIREVE"+",EVCODPRA"+",EVDATINI"+",EVDATFIN"+",EVOREEFF"+",EVMINEFF"+",EVNOMINA"+",EVPERSON"+",EVRIFPER"+",EVTIPEVE"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_EV__ANNO),'ANEVENTI','EV__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EVSERIAL),'ANEVENTI','EVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TxtSel),'ANEVENTI','EVOGGETT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DesAgg),'ANEVENTI','EV__NOTE');
      +","+cp_NullLink(cp_ToStrODBC("D"),'ANEVENTI','EV_STATO');
      +","+cp_NullLink(cp_ToStrODBC("E"),'ANEVENTI','EVDIREVE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODPRATICA),'ANEVENTI','EVCODPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DataIniTimer),'ANEVENTI','EVDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATFIN),'ANEVENTI','EVDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DurataOre),'ANEVENTI','EVOREEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DurataMin),'ANEVENTI','EVMINEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EVNOMINA),'ANEVENTI','EVNOMINA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CodPart),'ANEVENTI','EVPERSON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_EVRIFPER),'ANEVENTI','EVRIFPER');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_EVTIPEVE),'ANEVENTI','EVTIPEVE');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'ANEVENTI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'ANEVENTI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'ANEVENTI','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'ANEVENTI','UTDV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'EV__ANNO',this.w_EV__ANNO,'EVSERIAL',this.w_EVSERIAL,'EVOGGETT',this.oParentObject.w_TxtSel,'EV__NOTE',this.oParentObject.w_DesAgg,'EV_STATO',"D",'EVDIREVE',"E",'EVCODPRA',this.oParentObject.w_CODPRATICA,'EVDATINI',this.oParentObject.w_DataIniTimer,'EVDATFIN',this.w_DATFIN,'EVOREEFF',this.oParentObject.w_DurataOre,'EVMINEFF',this.oParentObject.w_DurataMin,'EVNOMINA',this.w_EVNOMINA)
      insert into (i_cTable) (EV__ANNO,EVSERIAL,EVOGGETT,EV__NOTE,EV_STATO,EVDIREVE,EVCODPRA,EVDATINI,EVDATFIN,EVOREEFF,EVMINEFF,EVNOMINA,EVPERSON,EVRIFPER,EVTIPEVE,UTCC,UTDC,UTCV,UTDV &i_ccchkf. );
         values (;
           this.w_EV__ANNO;
           ,this.w_EVSERIAL;
           ,this.oParentObject.w_TxtSel;
           ,this.oParentObject.w_DesAgg;
           ,"D";
           ,"E";
           ,this.oParentObject.w_CODPRATICA;
           ,this.oParentObject.w_DataIniTimer;
           ,this.w_DATFIN;
           ,this.oParentObject.w_DurataOre;
           ,this.oParentObject.w_DurataMin;
           ,this.w_EVNOMINA;
           ,this.oParentObject.w_CodPart;
           ,this.oParentObject.w_EVRIFPER;
           ,this.oParentObject.w_EVTIPEVE;
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_00DAA598()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Generazione singola Prestazione
    this.w_RESULT = this.w_OBJ.CreatePrest()
    if EMPTY(this.w_RESULT)
      * --- commit
      cp_EndTrs(.t.)
    else
      * --- Raise
      i_Error=this.w_RESULT
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_AEDESPRA = ALLTRIM(this.oParentObject.w_DesPratica)
    this.w_AECODPRA = ALLTRIM(this.oParentObject.w_CodPratica)
    DIMENSION L_INFOARCH(9)
    L_INFOARCH(1) = iif(Isalt(),"CAN_TIER","")
    L_INFOARCH(2) = this.w_AECODPRA
    L_INFOARCH(3) = this.w_AEPATHFI
    L_INFOARCH(4) = this.w_AEORIFIL
    L_INFOARCH(5) = this.w_AEOGGETT
    L_INFOARCH(6) = ""
    L_INFOARCH(7) = ""
    L_INFOARCH(8) = ""
    L_INFOARCH(9) = this.w_AEDESPRA
     
 Dimension ARRAYALLEG[1] 
 NumAlleg=1 
 ARRAYALLEG(NumAlleg) = " " 
 w_RET = EMAIL( @ARRAYALLEG , g_UEDIAL, .F., "", "", g_UEFIRM, .F., .F., @L_INFOARCH)
    * Variabili publiche per mail 
 i_EMAIL = " " 
 i_EMAILSUBJECT = " " 
 i_EMAILTEXT = " "
    release L_INFOARCH
    if not w_RET
      this.w_TMPC = "Impossibile inviare via email il documento selezionato"
      ah_ErrorMSG(this.w_TMPC,48)
    endif
  endproc


  proc Init(oParentObject,pVisAttivita)
    this.pVisAttivita=pVisAttivita
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='PAR_AGEN'
    this.cWorkTables[2]='DIPENDEN'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='UNIMIS'
    this.cWorkTables[6]='ANEVENTI'
    this.cWorkTables[7]='CAUMATTI'
    this.cWorkTables[8]='PAR_ALTE'
    this.cWorkTables[9]='RIS_ESTR'
    return(this.OpenAllTables(9))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pVisAttivita"
endproc
