* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ba1                                                        *
*              Notifica carica prestazioni                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-18                                                      *
* Last revis.: 2012-01-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_ba1",oParentObject,m.pAzione)
return(i_retval)

define class tgsag_ba1 as StdBatch
  * --- Local variables
  pAzione = space(10)
  w_ObjMPA = .NULL.
  w_NumRec = 0
  w_CODRIS = space(5)
  w_CONTRA = space(10)
  w_ObjMRP = .NULL.
  w_NumRec = 0
  * --- WorkFile variables
  DIPENDEN_idx=0
  DET_GEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pAzione="NOTIFICA"
        * --- Controlla che non ci siano partecipanti duplicati in gestione attivit�
        this.w_ObjMPA = this.oParentObject
        this.w_ObjMPA.MarkPos()     
        this.w_NumRec = this.w_ObjMPA.SEARCH("NVL(t_PACODRIS,space(5)) = "+cp_ToStrODBC( this.w_ObjMPA.w_PACODRIS )+" AND NVL( t_CPROWORD,0 ) <> "+cp_ToStrODBC( this.w_ObjMPA.w_CPROWORD )+" AND NOT DELETED()",0)
        this.w_ObjMPA.RePos()     
        if this.w_NumRec <> - 1
          * --- Il codice attivo � duplicato
          ah_ErrorMsg("Attenzione, partecipante duplicato")
          * --- Abbiamo trovato la riga relativa al partecipante duplicato
          this.w_ObjMPA.DeleteRow()     
          this.w_ObjMPA.Refresh()     
        else
          * --- Notifica Evento "Partecip" a GASG_AAT (Attivit�) - per lanciare GSAR_BAP('9')
          this.w_CODRIS = this.w_ObjMPA.w_PACODRIS
          * --- Read from DET_GEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DET_GEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DET_GEN_idx,2],.t.,this.DET_GEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MDCONTRA"+;
              " from "+i_cTable+" DET_GEN where ";
                  +"MDSERATT = "+cp_ToStrODBC(this.oParentObject.w_PASERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MDCONTRA;
              from (i_cTable) where;
                  MDSERATT = this.oParentObject.w_PASERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONTRA = NVL(cp_ToDate(_read_.MDCONTRA),cp_NullValue(_read_.MDCONTRA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Se l'attivit� � generata da contratto non rigenero il dettaglio
          * --- Read from DIPENDEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIPENDEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPCODICE"+;
              " from "+i_cTable+" DIPENDEN where ";
                  +"DPCODICE = "+cp_ToStrODBC(this.w_CODRIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPCODICE;
              from (i_cTable) where;
                  DPCODICE = this.w_CODRIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODRIS = NVL(cp_ToDate(_read_.DPCODICE),cp_NullValue(_read_.DPCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if (i_rows <>0 or empty(this.w_CODRIS)) and Empty(this.w_CONTRA)
            this.oParentObject.oParentObject.NotifyEvent("Partecip")
          endif
        endif
      case this.pAzione="CONTROLLA"
        * --- Controlla che non ci siano partecipanti duplicati nel rinvio attivit�
        this.w_ObjMRP = this.oParentObject
        this.w_ObjMRP.MarkPos()     
        this.w_NumRec = this.w_ObjMRP.SEARCH("NVL(t_PACODRIS,space(5)) = "+cp_ToStrODBC( this.w_ObjMRP.w_PACODRIS )+" AND NVL( t_CPROWNUM,0 ) <> "+cp_ToStrODBC( this.w_ObjMRP.w_CPROWNUM )+" AND NOT DELETED()",0)
        this.w_ObjMRP.RePos()     
        if this.w_NumRec <> - 1
          * --- Il codice attivo � duplicato
          ah_ErrorMsg("Attenzione, partecipante duplicato")
          * --- Abbiamo trovato la riga relativa al partecipante duplicato
          this.w_ObjMRP.DeleteRow()     
          this.w_ObjMRP.Refresh()     
        endif
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='DET_GEN'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
