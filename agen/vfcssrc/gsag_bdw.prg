* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bdw                                                        *
*              Duplica raggruppamenti prestazioni                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-12                                                      *
* Last revis.: 2012-05-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bdw",oParentObject,m.pAzione)
return(i_retval)

define class tgsag_bdw as StdBatch
  * --- Local variables
  pAzione = space(1)
  * --- WorkFile variables
  PRE_ITER_idx=0
  TMP_SERV_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                                                        Duplicazione Raggruppamento prestazioni
    * --- Tabelle Gestite:
    *     PRE_ITER
    * --- pAzione = 'C'  : controllo di esistenza codice raggruppamento prestazioni
    *                    = 'D'  : duplicazione raggruppamento prestazioni
    do case
      case this.pAzione = "D"
        * --- DUPLICAZIONE
        * --- begin transaction
        cp_BeginTrs()
        * --- Try
        local bErr_02E499A8
        bErr_02E499A8=bTrsErr
        this.Try_02E499A8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Duplicazione terminata con errori",,"")
        endif
        bTrsErr=bTrsErr or bErr_02E499A8
        * --- End
        * --- Drop temporary table TMP_SERV
        i_nIdx=cp_GetTableDefIdx('TMP_SERV')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_SERV')
        endif
      case this.pAzione = "C"
        * --- CONTROLLO ESISTENZA CODICE RAGGRUPPAMENTO PRESTAZIONI
        if NOT EMPTY(this.oParentObject.w_CODRAGPRE)
          * --- Select from PRE_ITER
          i_nConn=i_TableProp[this.PRE_ITER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRE_ITER_idx,2],.t.,this.PRE_ITER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ITCODICE  from "+i_cTable+" PRE_ITER ";
                +" where ITCODICE="+cp_ToStrODBC(this.oParentObject.w_CODRAGPRE)+"";
                +" group by ITCODICE";
                 ,"_Curs_PRE_ITER")
          else
            select ITCODICE from (i_cTable);
             where ITCODICE=this.oParentObject.w_CODRAGPRE;
             group by ITCODICE;
              into cursor _Curs_PRE_ITER
          endif
          if used('_Curs_PRE_ITER')
            select _Curs_PRE_ITER
            locate for 1=1
            do while not(eof())
            ah_ErrorMsg("Attenzione: codice raggruppamento prestazioni gi� esistente!",,"")
            this.oParentObject.w_CODRAGPRE = space(10)
              select _Curs_PRE_ITER
              continue
            enddo
            use
          endif
        endif
    endcase
  endproc
  proc Try_02E499A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Create temporary table TMP_SERV
    i_nIdx=cp_AddTableDef('TMP_SERV') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PRE_ITER_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_ITER_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where ITCODICE="+cp_ToStrODBC(this.oParentObject.w_RAGPREORIG)+"";
          )
    this.TMP_SERV_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_SERV
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_SERV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_SERV_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_SERV_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ITCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODRAGPRE),'TMP_SERV','ITCODICE');
      +",ITDESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESRAGPRE),'TMP_SERV','ITDESCRI');
          +i_ccchkf ;
             )
    else
      update (i_cTable) set;
          ITCODICE = this.oParentObject.w_CODRAGPRE;
          ,ITDESCRI = this.oParentObject.w_DESRAGPRE;
          &i_ccchkf. ;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore scrittura TMP Raggruppamenti'
      return
    endif
    * --- Insert into PRE_ITER
    i_nConn=i_TableProp[this.PRE_ITER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_ITER_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_SERV_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PRE_ITER_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento Raggruppamenti'
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Duplicazione completata",,"")
    return


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRE_ITER'
    this.cWorkTables[2]='*TMP_SERV'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PRE_ITER')
      use in _Curs_PRE_ITER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
