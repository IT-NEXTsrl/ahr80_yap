* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bsb                                                        *
*              Stampa nominativi/buste                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-11                                                      *
* Last revis.: 2014-05-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bsb",oParentObject,m.pParam)
return(i_retval)

define class tgsag_bsb as StdBatch
  * --- Local variables
  pParam = space(5)
  CURSORE = space(10)
  w_CNCODCAN = space(20)
  w_NumRec = 0
  w_COD_NOM = space(15)
  * --- WorkFile variables
  TMP_PRAT_idx=0
  TMPDPRES_idx=0
  OFF_NOMI_idx=0
  NAZIONI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Nota: tale routine � lanciata anche da GSAR_ANO (bottone Stampa Informativa nella pag. Attributi) con il parametro 'STAMPA_DA_NOM'
    * --- Utilizziamo la tabella TMP_PRAT per inserire le pratiche selezionate e permettere la join dei nominativi ad essi associate.
    *     Utilizziamo la tabella TMP_NOMI per inserire i codici nei nominativi selezionati in modo che siano visibili nelle query di stampa.
    * --- Dati studio
    * --- Dati destinatario inserito a mano
    * --- Variabili di stampa
    do case
      case this.pParam=="SELEZ_NOM"
        * --- Seleziona tutti i nominativi
        this.CURSORE = this.oParentObject.w_AGSBU_NOM.cCursor
        UPDATE (this.CURSORE) SET XCHK = 1
      case this.pParam=="DESEL_NOM"
        * --- Deseleziona tutti nominativi
        this.CURSORE = this.oParentObject.w_AGSBU_NOM.cCursor
        UPDATE (this.CURSORE) SET XCHK = 0
      case this.pParam=="SELEZ_PRA"
        * --- Seleziona tutte le pratiche
        this.CURSORE = this.oParentObject.w_AGSBU_PRA.cCursor
        UPDATE (this.CURSORE) SET XCHK = 1
        oParentObject.NotifyEvent("RicercaNomi")
      case this.pParam=="DESEL_PRA"
        * --- Deseleziona tutte le pratiche
        this.CURSORE = this.oParentObject.w_AGSBU_PRA.cCursor
        UPDATE (this.CURSORE) SET XCHK = 0
        oParentObject.NotifyEvent("RicercaNomi")
      case this.pParam=="RICERCANOMI" OR this.pParam=="ATTIVA1_E_RICERCA"
        * --- Deve aggiornare lo zoom dei nominativi ma NON � alla prima pagina 
        if this.pParam="ATTIVA1_E_RICERCA"
          oParentObject.oPgFrm.ActivePage = 1
        endif
        * --- Deve aggiornare lo zoom dei nominativi.
        *     Prima di tutto inserisce in TMP_PRAT le pratiche selezionate e poi procede con lo riempimento dello zoom dei nominativi
        * --- Create temporary table TMP_PRAT
        i_nIdx=cp_AddTableDef('TMP_PRAT') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMP_PRAT_proto';
              )
        this.TMP_PRAT_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        do case
          case this.oParentObject.w_Associa = "T"
            * --- Tutti i tipi di nominativi, lascia vuota la tabella temporanea TMP_PRAT
            * --- Viene associata allo zoom la query indicata
            this.oParentObject.w_AGSBU_NOM.NewExtQuery("QUERY\GSAG2BSB")
          case this.oParentObject.w_Associa = "A"
            * --- Associa alle pratiche selezionate, che sono inserite nel temporaneo TMP_PRAT.
            this.CURSORE = this.oParentObject.w_AGSBU_PRA.cCursor
            SELECT (this.CURSORE)
            * --- Salviamo la posizione in modo da tornare sulla riga corretta.
            this.w_NumRec = Recno()
            SCAN FOR XCHK = 1
            this.w_CNCODCAN = CNCODCAN
            * --- Insert into TMP_PRAT
            i_nConn=i_TableProp[this.TMP_PRAT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_PRAT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_PRAT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CNCODCAN"+",TIPREC"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(CNCODCAN),'TMP_PRAT','CNCODCAN');
              +","+cp_NullLink(cp_ToStrODBC("0"),'TMP_PRAT','TIPREC');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CNCODCAN',CNCODCAN,'TIPREC',"0")
              insert into (i_cTable) (CNCODCAN,TIPREC &i_ccchkf. );
                 values (;
                   CNCODCAN;
                   ,"0";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            ENDSCAN
            if RECCOUNT()>=this.w_NumRec
              GOTO this.w_NumRec
            endif
            * --- Viene associata allo zoom la query indicata
            this.oParentObject.w_AGSBU_NOM.NewExtQuery("QUERY\GSAG3BSB")
          case this.oParentObject.w_Associa = "N"
            * --- Non associa a nessuna pratica, quindi deve estrarre solo quei nominativi che non compaiono come risorse esterne
            * --- Viene associata allo zoom la query indicata
            this.oParentObject.w_AGSBU_NOM.NewExtQuery("QUERY\GSAG4BSB")
        endcase
        * --- Viene rieseguita la query relativa allo zoom
        this.oParentObject.w_AGSBU_NOM.Query()
        if this.pParam="ATTIVA1_E_RICERCA"
          * --- La SetFocus serve se proveniamo dalla seconda pagina.
          this.oParentObject.w_AGSBU_NOM.SetFocus()     
        endif
        * --- Chiude la tabella temporanea TMP_PRAT
        * --- Drop temporary table TMP_PRAT
        i_nIdx=cp_GetTableDefIdx('TMP_PRAT')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_PRAT')
        endif
      case this.pParam=="VIS_PRAT" OR this.pParam=="INGRANDISCI" OR this.pParam=="RIDUCI"
        * --- Visualizza o meno lo zoom delle pratiche
        do case
          case (this.oParentObject.w_Associa="A" AND this.oParentObject.w_DimScheda=1) OR this.pParam=="INGRANDISCI"
            this.oParentObject.w_DimScheda = 0
            this.oParentObject.w_AGSBU_NOM.Top = this.oParentObject.w_AGSBU_NOM.Top + 165
            this.oParentObject.w_AGSBU_NOM.cHeight = this.oParentObject.w_AGSBU_NOM.Height - 165
            this.oParentObject.w_AGSBU_NOM.Height = this.oParentObject.w_AGSBU_NOM.Height - 165
            this.oParentObject.w_AGSBU_PRA.Visible = .T.
          case (this.oParentObject.w_Associa#"A" AND this.oParentObject.w_DimScheda=0) OR this.pParam=="RIDUCI"
            this.oParentObject.w_AGSBU_PRA.Visible = .F.
            this.oParentObject.w_DimScheda = 1
            this.oParentObject.w_AGSBU_NOM.Top = this.oParentObject.w_AGSBU_NOM.Top - 165
            this.oParentObject.w_AGSBU_NOM.cHeight = this.oParentObject.w_AGSBU_NOM.Height + 165
            this.oParentObject.w_AGSBU_NOM.Height = this.oParentObject.w_AGSBU_NOM.Height + 165
        endcase
      case this.pParam=="STAMPA" OR this.pParam=="STAMPA_DA_NOM"
        if this.pParam=="STAMPA" AND this.oParentObject.w_ForzaNom="T"
          SELECT (this.oParentObject.w_AGSBU_NOM.cCursor)
          COUNT FOR XCHK=1 TO CONTA_REC
          if CONTA_REC <> 1
            AH_ERRORMSG("Per procedere con la stampa deve essere selezionato un solo nominativo")
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Inserisce i nominativi selezionati nella tabella temporanea in modo che siano visibili nelle query di stampa
        * --- Create temporary table TMPDPRES
        i_nIdx=cp_AddTableDef('TMPDPRES') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPDPRES_proto';
              )
        this.TMPDPRES_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        if this.pParam=="STAMPA"
          * --- Stampa nominativi
          this.CURSORE = this.oParentObject.w_AGSBU_NOM.cCursor
          SELECT (this.CURSORE)
          SCAN FOR XCHK = 1
          * --- Insert into TMPDPRES
          i_nConn=i_TableProp[this.TMPDPRES_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDPRES_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPDPRES_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"SECODSES"+",SECODPRA"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(NOCODICE),'TMPDPRES','SECODSES');
            +","+cp_NullLink(cp_ToStrODBC("X"),'TMPDPRES','SECODPRA');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'SECODSES',NOCODICE,'SECODPRA',"X")
            insert into (i_cTable) (SECODSES,SECODPRA &i_ccchkf. );
               values (;
                 NOCODICE;
                 ,"X";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          ENDSCAN
          if this.oParentObject.w_ForzaNom="S"
            * --- Insert into TMPDPRES
            i_nConn=i_TableProp[this.TMPDPRES_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPDPRES_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPDPRES_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"SECODSES"+",SECODPRA"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CodiceFittizio),'TMPDPRES','SECODSES');
              +","+cp_NullLink(cp_ToStrODBC("X"),'TMPDPRES','SECODPRA');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'SECODSES',this.oParentObject.w_CodiceFittizio,'SECODPRA',"X")
              insert into (i_cTable) (SECODSES,SECODPRA &i_ccchkf. );
                 values (;
                   this.oParentObject.w_CodiceFittizio;
                   ,"X";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          * --- Dichiarazione var. per la stampa buste
          L_RAGSTU = ""
          L_INDSTU = ""
          L_LOCSTU = ""
          L_CAPSTU = ""
          L_PROVSTU = ""
          L_MODSPED = ""
          L_NOTEB = ""
          L_MODSPED = this.oParentObject.w_MODSPED
          L_NOTEB = this.oParentObject.w_NOTEBUSTA
          if this.oParentObject.w_InsMitt="S"
            L_RAGSTU = this.oParentObject.w_RAGSTU
            L_INDSTU = this.oParentObject.w_INDSTU
            L_LOCSTU = this.oParentObject.w_LOCSTU
            L_CAPSTU = this.oParentObject.w_CAPSTU
            L_PROVSTU = this.oParentObject.w_PROVSTU
          endif
          vq_exec(this.oParentObject.w_OQRY,this,"__TMP__")
          CP_CHPRN(this.oParentObject.w_OREP,"",this.oParentObject)
        else
          * --- Stampa informativa trattamento dati personali del nominativo (bottone in GSAR_ANO)
          * --- Insert into TMPDPRES
          i_nConn=i_TableProp[this.TMPDPRES_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPDPRES_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPDPRES_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"SECODSES"+",SECODPRA"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOCODICE),'TMPDPRES','SECODSES');
            +","+cp_NullLink(cp_ToStrODBC("X"),'TMPDPRES','SECODPRA');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'SECODSES',this.oParentObject.w_NOCODICE,'SECODPRA',"X")
            insert into (i_cTable) (SECODSES,SECODPRA &i_ccchkf. );
               values (;
                 this.oParentObject.w_NOCODICE;
                 ,"X";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          vq_exec("QUERY\GSAG_SBU.VQR",this,"__TMP__")
          CP_CHPRN("QUERY\GSAG3SNO.FRX", "",this.oParentObject)
          if AH_YESNO("Vuoi attivare il flag Ricevuto consenso trattamento dati personali?")
            * --- Try
            local bErr_037CE558
            bErr_037CE558=bTrsErr
            this.Try_037CE558()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              AH_ERRORMSG("Impossibile effettuare l'aggiornamento")
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_037CE558
            * --- End
          endif
        endif
        * --- Drop temporary table TMPDPRES
        i_nIdx=cp_GetTableDefIdx('TMPDPRES')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPDPRES')
        endif
      case this.pParam=="INIT"
        * --- All'init nasconde lo zoom di selez. pratiche
        this.oParentObject.w_AGSBU_PRA.Visible = .F.
        this.oParentObject.w_DimScheda = 1
      case this.pParam=="PULISCIFILTRI"
        * --- Pulisce i filtri di selezione delle pratiche
        this.oParentObject.w_DESCAN = ""
        this.oParentObject.w_CODCAN = ""
        this.oParentObject.w_DESNOM = ""
        if this.oParentObject.w_ASSOCIA="A"
          oParentObject.NotifyEvent("AggiornaPratiche")
        endif
        oParentObject.NotifyEvent("RicercaNomi")
        * --- 'AGGIORNAPRATICHE
      case this.pParam=="AGGIORNAPRATICHE"
        * --- Notifica allo zoom di aggiornarsi
        oParentObject.NotifyEvent("AggiornaPratiche")
        if !oParentObject.oPgFrm.ActivePage = 1
          oParentObject.oPgFrm.ActivePage = 1
        endif
        oParentObject.NotifyEvent("RicercaNomi")
      case this.pParam=="COPIA_DATI"
        if this.oParentObject.w_ForzaNom="T"
          SELECT (this.oParentObject.w_AGSBU_NOM.cCursor)
          COUNT FOR XCHK=1 TO CONTA_REC
          if CONTA_REC = 1
            LOCATE FOR XCHK=1
            if FOUND()
              this.w_COD_NOM = NOCODICE
              * --- Read from OFF_NOMI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "NODESCRI,NOINDIRI,NOLOCALI,NO___CAP,NOPROVIN,NONAZION,NOCODSAL"+;
                  " from "+i_cTable+" OFF_NOMI where ";
                      +"NOCODICE = "+cp_ToStrODBC(this.w_COD_NOM);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  NODESCRI,NOINDIRI,NOLOCALI,NO___CAP,NOPROVIN,NONAZION,NOCODSAL;
                  from (i_cTable) where;
                      NOCODICE = this.w_COD_NOM;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_RAGSOC_Dest = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
                this.oParentObject.w_INDIRI_Dest = NVL(cp_ToDate(_read_.NOINDIRI),cp_NullValue(_read_.NOINDIRI))
                this.oParentObject.w_LOCALI_Dest = NVL(cp_ToDate(_read_.NOLOCALI),cp_NullValue(_read_.NOLOCALI))
                this.oParentObject.w_CAP_Dest = NVL(cp_ToDate(_read_.NO___CAP),cp_NullValue(_read_.NO___CAP))
                this.oParentObject.w_PROV_Dest = NVL(cp_ToDate(_read_.NOPROVIN),cp_NullValue(_read_.NOPROVIN))
                this.oParentObject.w_NAZ_Dest = NVL(cp_ToDate(_read_.NONAZION),cp_NullValue(_read_.NONAZION))
                this.oParentObject.w_SAL_Dest = NVL(cp_ToDate(_read_.NOCODSAL),cp_NullValue(_read_.NOCODSAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Read from NAZIONI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.NAZIONI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "NADESNAZ"+;
                  " from "+i_cTable+" NAZIONI where ";
                      +"NACODNAZ = "+cp_ToStrODBC(this.oParentObject.w_NAZ_Dest);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  NADESNAZ;
                  from (i_cTable) where;
                      NACODNAZ = this.oParentObject.w_NAZ_Dest;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_NADESNAZ = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          else
            AH_ERRORMSG("Per questa impostazione � prevista la selezione di un solo nominativo")
            this.oParentObject.w_ForzaNom = "N"
          endif
        else
          this.oParentObject.w_RAGSOC_Dest = space(100)
          this.oParentObject.w_INDIRI_Dest = space(100)
          this.oParentObject.w_LOCALI_Dest = space(100)
          this.oParentObject.w_CAP_Dest = space(9)
          this.oParentObject.w_PROV_Dest = space(2)
          this.oParentObject.w_NAZ_Dest = space(3)
          this.oParentObject.w_NADESNAZ = space(35)
          this.oParentObject.w_SAL_Dest = space(5)
          this.oParentObject.w_CortAtt_Dest = space(100)
        endif
    endcase
  endproc
  proc Try_037CE558()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into OFF_NOMI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"NOPRIVCY ="+cp_NullLink(cp_ToStrODBC("S"),'OFF_NOMI','NOPRIVCY');
          +i_ccchkf ;
      +" where ";
          +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_NOCODICE);
             )
    else
      update (i_cTable) set;
          NOPRIVCY = "S";
          &i_ccchkf. ;
       where;
          NOCODICE = this.oParentObject.w_NOCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Per visualizzare l'attivazione del check sull'anagrafica dei Nominativi viene ricaricato il record dalla tabella
    This.oparentobject.LoadRec()
    return


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='*TMP_PRAT'
    this.cWorkTables[2]='*TMPDPRES'
    this.cWorkTables[3]='OFF_NOMI'
    this.cWorkTables[4]='NAZIONI'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
