* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kvm                                                        *
*              Variazione modelli elementi                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-22                                                      *
* Last revis.: 2011-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kvm",oParentObject))

* --- Class definition
define class tgsag_kvm as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 732
  Height = 483+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-12-30"
  HelpContextID=219535511
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=51

  * --- Constant Properties
  _IDX = 0
  VOCIIVA_IDX = 0
  CAT_ELEM_IDX = 0
  MODCLDAT_IDX = 0
  MOD_ELEM_IDX = 0
  TIP_DOCU_IDX = 0
  CAUMATTI_IDX = 0
  DIPENDEN_IDX = 0
  ART_ICOL_IDX = 0
  LISTINI_IDX = 0
  cPrg = "gsag_kvm"
  cComment = "Variazione modelli elementi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_MOCATELE_FLAG = space(1)
  w_MOCATELE_ALL = space(1)
  o_MOCATELE_ALL = space(1)
  w_MOCATELE_FILTER = space(5)
  w_DESCAT = space(35)
  w_MOCATELE = space(5)
  o_MOCATELE = space(5)
  w_DESCAT1 = space(35)
  w_MOTIPAPL_ALL = space(1)
  w_MOTIPAPL_FLAG = space(1)
  w_MOTIPAPL_FILTER = space(5)
  w_MOTIPAPL = space(5)
  o_MOTIPAPL = space(5)
  w_MOCODICE1 = space(10)
  w_MOCODICE2 = space(10)
  w_MODESCRI1 = space(60)
  w_MODESCRI2 = space(60)
  w_MOCATELE1 = space(10)
  w_DESELE = space(60)
  w_MOTIPAPL1 = space(5)
  w_FLFATT = space(1)
  w_MOFLFATT1 = space(1)
  w_FLATTI = space(1)
  w_MOFLATTI1 = space(1)
  w_MOCAUDOC1 = space(5)
  w_DESDOC1 = space(35)
  w_MOTIPATT1 = space(20)
  w_DESATT1 = space(254)
  w_MOGRUPAR = space(5)
  w_TIPRIS = space(1)
  w_MOCODSER1 = space(20)
  w_DESART = space(40)
  w_TIPART = space(2)
  w_MOCODLIS1 = space(5)
  w_DESLIS = space(40)
  w_FLSCON = space(1)
  w_ESCRIN = space(1)
  w_MOESCRIN1 = space(1)
  w_MOPERCON1 = space(3)
  w_MDDESCRI = space(50)
  w_RINCON = space(1)
  w_MORINCON1 = space(1)
  w_MONUMGIO1 = 0
  w_MONUMGIO2 = 0
  w_ESTENDI = space(1)
  w_MOPERFAT_FLAG = space(1)
  w_MOPERFAT_ALL = space(1)
  w_MOPERFAT_FILTER = space(5)
  w_DESCAT = space(35)
  w_MOPERFAT = space(5)
  w_DESCAT1 = space(35)
  w_MOGIODOC_FLAG = space(1)
  w_MOGIODOC_ALL = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kvmPag1","gsag_kvm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Variazione dati")
      .Pages(2).addobject("oPag","tgsag_kvmPag2","gsag_kvm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMOCATELE_FLAG_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='CAT_ELEM'
    this.cWorkTables[3]='MODCLDAT'
    this.cWorkTables[4]='MOD_ELEM'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='CAUMATTI'
    this.cWorkTables[7]='DIPENDEN'
    this.cWorkTables[8]='ART_ICOL'
    this.cWorkTables[9]='LISTINI'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MOCATELE_FLAG=space(1)
      .w_MOCATELE_ALL=space(1)
      .w_MOCATELE_FILTER=space(5)
      .w_DESCAT=space(35)
      .w_MOCATELE=space(5)
      .w_DESCAT1=space(35)
      .w_MOTIPAPL_ALL=space(1)
      .w_MOTIPAPL_FLAG=space(1)
      .w_MOTIPAPL_FILTER=space(5)
      .w_MOTIPAPL=space(5)
      .w_MOCODICE1=space(10)
      .w_MOCODICE2=space(10)
      .w_MODESCRI1=space(60)
      .w_MODESCRI2=space(60)
      .w_MOCATELE1=space(10)
      .w_DESELE=space(60)
      .w_MOTIPAPL1=space(5)
      .w_FLFATT=space(1)
      .w_MOFLFATT1=space(1)
      .w_FLATTI=space(1)
      .w_MOFLATTI1=space(1)
      .w_MOCAUDOC1=space(5)
      .w_DESDOC1=space(35)
      .w_MOTIPATT1=space(20)
      .w_DESATT1=space(254)
      .w_MOGRUPAR=space(5)
      .w_TIPRIS=space(1)
      .w_MOCODSER1=space(20)
      .w_DESART=space(40)
      .w_TIPART=space(2)
      .w_MOCODLIS1=space(5)
      .w_DESLIS=space(40)
      .w_FLSCON=space(1)
      .w_ESCRIN=space(1)
      .w_MOESCRIN1=space(1)
      .w_MOPERCON1=space(3)
      .w_MDDESCRI=space(50)
      .w_RINCON=space(1)
      .w_MORINCON1=space(1)
      .w_MONUMGIO1=0
      .w_MONUMGIO2=0
      .w_ESTENDI=space(1)
      .w_MOPERFAT_FLAG=space(1)
      .w_MOPERFAT_ALL=space(1)
      .w_MOPERFAT_FILTER=space(5)
      .w_DESCAT=space(35)
      .w_MOPERFAT=space(5)
      .w_DESCAT1=space(35)
      .w_MOGIODOC_FLAG=space(1)
      .w_MOGIODOC_ALL=space(1)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
        .w_MOCATELE_FLAG = iif( Not Empty(.w_MOCATELE) ,'S' , ' ')
          .DoRTCalc(2,2,.f.)
        .w_MOCATELE_FILTER = IIF(.w_MOCATELE_ALL='S',SPACE(5),.w_MOCATELE_FILTER)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_MOCATELE_FILTER))
          .link_1_4('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_MOCATELE))
          .link_1_6('Full')
        endif
          .DoRTCalc(6,7,.f.)
        .w_MOTIPAPL_FLAG = iif( .w_MOTIPAPL<>'0' ,'S' , ' ')
        .w_MOTIPAPL_FILTER = "D"
        .w_MOTIPAPL = "D"
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MOCODICE1))
          .link_2_3('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_MOCODICE2))
          .link_2_4('Full')
        endif
        .DoRTCalc(13,15,.f.)
        if not(empty(.w_MOCATELE1))
          .link_2_7('Full')
        endif
          .DoRTCalc(16,16,.f.)
        .w_MOTIPAPL1 = "D"
        .w_FLFATT = "E"
        .w_MOFLFATT1 = IIF( .w_FLFATT = 'E', ' ', .w_FLFATT )
        .w_FLATTI = "E"
        .w_MOFLATTI1 = IIF( .w_FLATTI = 'E', ' ', .w_FLATTI )
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_MOCAUDOC1))
          .link_2_19('Full')
        endif
        .DoRTCalc(23,24,.f.)
        if not(empty(.w_MOTIPATT1))
          .link_2_22('Full')
        endif
        .DoRTCalc(25,26,.f.)
        if not(empty(.w_MOGRUPAR))
          .link_2_25('Full')
        endif
        .DoRTCalc(27,28,.f.)
        if not(empty(.w_MOCODSER1))
          .link_2_28('Full')
        endif
        .DoRTCalc(29,31,.f.)
        if not(empty(.w_MOCODLIS1))
          .link_2_32('Full')
        endif
          .DoRTCalc(32,33,.f.)
        .w_ESCRIN = "E"
        .w_MOESCRIN1 = IIF( .w_ESCRIN = 'E', ' ', .w_ESCRIN )
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_MOPERCON1))
          .link_2_39('Full')
        endif
          .DoRTCalc(37,37,.f.)
        .w_RINCON = "E"
        .w_MORINCON1 = IIF( .w_RINCON = 'E', ' ', .w_RINCON )
          .DoRTCalc(40,42,.f.)
        .w_MOPERFAT_FLAG = iif( Not Empty(.w_MOPERFAT) ,'S' , ' ')
          .DoRTCalc(44,44,.f.)
        .w_MOPERFAT_FILTER = IIF(.w_MOPERFAT_ALL='S',SPACE(5),.w_MOPERFAT_FILTER)
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_MOPERFAT_FILTER))
          .link_1_25('Full')
        endif
        .DoRTCalc(46,47,.f.)
        if not(empty(.w_MOPERFAT))
          .link_1_27('Full')
        endif
          .DoRTCalc(48,48,.f.)
        .w_MOGIODOC_FLAG = iif( Not Empty(.w_MOPERFAT) ,'S' , ' ')
          .DoRTCalc(50,50,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_MOCATELE<>.w_MOCATELE
            .w_MOCATELE_FLAG = iif( Not Empty(.w_MOCATELE) ,'S' , ' ')
        endif
        .DoRTCalc(2,2,.t.)
        if .o_MOCATELE_ALL<>.w_MOCATELE_ALL
            .w_MOCATELE_FILTER = IIF(.w_MOCATELE_ALL='S',SPACE(5),.w_MOCATELE_FILTER)
          .link_1_4('Full')
        endif
        .DoRTCalc(4,7,.t.)
        if .o_MOTIPAPL<>.w_MOTIPAPL
            .w_MOTIPAPL_FLAG = iif( .w_MOTIPAPL<>'0' ,'S' , ' ')
        endif
        .DoRTCalc(9,18,.t.)
            .w_MOFLFATT1 = IIF( .w_FLFATT = 'E', ' ', .w_FLFATT )
        .DoRTCalc(20,20,.t.)
            .w_MOFLATTI1 = IIF( .w_FLATTI = 'E', ' ', .w_FLATTI )
        .DoRTCalc(22,34,.t.)
            .w_MOESCRIN1 = IIF( .w_ESCRIN = 'E', ' ', .w_ESCRIN )
        .DoRTCalc(36,38,.t.)
            .w_MORINCON1 = IIF( .w_RINCON = 'E', ' ', .w_RINCON )
        .DoRTCalc(40,42,.t.)
        if .o_MOCATELE<>.w_MOCATELE
            .w_MOPERFAT_FLAG = iif( Not Empty(.w_MOPERFAT) ,'S' , ' ')
        endif
        .DoRTCalc(44,44,.t.)
        if .o_MOCATELE_ALL<>.w_MOCATELE_ALL
            .w_MOPERFAT_FILTER = IIF(.w_MOPERFAT_ALL='S',SPACE(5),.w_MOPERFAT_FILTER)
          .link_1_25('Full')
        endif
        .DoRTCalc(46,48,.t.)
        if .o_MOCATELE<>.w_MOCATELE
            .w_MOGIODOC_FLAG = iif( Not Empty(.w_MOPERFAT) ,'S' , ' ')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(50,51,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMOCATELE_FILTER_1_4.enabled = this.oPgFrm.Page1.oPag.oMOCATELE_FILTER_1_4.mCond()
    this.oPgFrm.Page1.oPag.oMOTIPAPL_FILTER_1_18.enabled = this.oPgFrm.Page1.oPag.oMOTIPAPL_FILTER_1_18.mCond()
    this.oPgFrm.Page1.oPag.oMOTIPAPL_1_19.enabled = this.oPgFrm.Page1.oPag.oMOTIPAPL_1_19.mCond()
    this.oPgFrm.Page1.oPag.oMOPERFAT_FILTER_1_25.enabled = this.oPgFrm.Page1.oPag.oMOPERFAT_FILTER_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oStr_2_31.visible=!this.oPgFrm.Page2.oPag.oStr_2_31.mHide()
    this.oPgFrm.Page2.oPag.oMOCODLIS1_2_32.visible=!this.oPgFrm.Page2.oPag.oMOCODLIS1_2_32.mHide()
    this.oPgFrm.Page2.oPag.oDESLIS_2_33.visible=!this.oPgFrm.Page2.oPag.oDESLIS_2_33.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MOCATELE_FILTER
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ELEM_IDX,3]
    i_lTable = "CAT_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2], .t., this.CAT_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCATELE_FILTER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACE',True,'CAT_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODELE like "+cp_ToStrODBC(trim(this.w_MOCATELE_FILTER)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODELE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODELE',trim(this.w_MOCATELE_FILTER))
          select CECODELE,CEDESELE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODELE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCATELE_FILTER)==trim(_Link_.CECODELE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCATELE_FILTER) and !this.bDontReportError
            deferred_cp_zoom('CAT_ELEM','*','CECODELE',cp_AbsName(oSource.parent,'oMOCATELE_FILTER_1_4'),i_cWhere,'GSAG_ACE',"Categorie elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                     +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',oSource.xKey(1))
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCATELE_FILTER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                   +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(this.w_MOCATELE_FILTER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',this.w_MOCATELE_FILTER)
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCATELE_FILTER = NVL(_Link_.CECODELE,space(5))
      this.w_DESCAT = NVL(_Link_.CEDESELE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MOCATELE_FILTER = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.CECODELE,1)
      cp_ShowWarn(i_cKey,this.CAT_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCATELE_FILTER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCATELE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ELEM_IDX,3]
    i_lTable = "CAT_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2], .t., this.CAT_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCATELE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACE',True,'CAT_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODELE like "+cp_ToStrODBC(trim(this.w_MOCATELE)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODELE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODELE',trim(this.w_MOCATELE))
          select CECODELE,CEDESELE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODELE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCATELE)==trim(_Link_.CECODELE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCATELE) and !this.bDontReportError
            deferred_cp_zoom('CAT_ELEM','*','CECODELE',cp_AbsName(oSource.parent,'oMOCATELE_1_6'),i_cWhere,'GSAG_ACE',"Categorie elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                     +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',oSource.xKey(1))
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCATELE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                   +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(this.w_MOCATELE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',this.w_MOCATELE)
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCATELE = NVL(_Link_.CECODELE,space(5))
      this.w_DESCAT1 = NVL(_Link_.CEDESELE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MOCATELE = space(5)
      endif
      this.w_DESCAT1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.CECODELE,1)
      cp_ShowWarn(i_cKey,this.CAT_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCATELE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCODICE1
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODICE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_AME',True,'MOD_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_MOCODICE1)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_MOCODICE1))
          select MOCODICE,MODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODICE1)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODICE1) and !this.bDontReportError
            deferred_cp_zoom('MOD_ELEM','*','MOCODICE',cp_AbsName(oSource.parent,'oMOCODICE1_2_3'),i_cWhere,'GSAG_AME',"Modelli elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODICE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_MOCODICE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_MOCODICE1)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODICE1 = NVL(_Link_.MOCODICE,space(10))
      this.w_MODESCRI1 = NVL(_Link_.MODESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODICE1 = space(10)
      endif
      this.w_MODESCRI1 = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODICE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCODICE2
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODICE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_AME',True,'MOD_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_MOCODICE2)+"%");

          i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCODICE',trim(this.w_MOCODICE2))
          select MOCODICE,MODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODICE2)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODICE2) and !this.bDontReportError
            deferred_cp_zoom('MOD_ELEM','*','MOCODICE',cp_AbsName(oSource.parent,'oMOCODICE2_2_4'),i_cWhere,'GSAG_AME',"Modelli elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',oSource.xKey(1))
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODICE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_MOCODICE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_MOCODICE2)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODICE2 = NVL(_Link_.MOCODICE,space(10))
      this.w_MODESCRI2 = NVL(_Link_.MODESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODICE2 = space(10)
      endif
      this.w_MODESCRI2 = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODICE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCATELE1
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ELEM_IDX,3]
    i_lTable = "CAT_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2], .t., this.CAT_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCATELE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ACE',True,'CAT_ELEM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CECODELE like "+cp_ToStrODBC(trim(this.w_MOCATELE1)+"%");

          i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CECODELE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CECODELE',trim(this.w_MOCATELE1))
          select CECODELE,CEDESELE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CECODELE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCATELE1)==trim(_Link_.CECODELE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCATELE1) and !this.bDontReportError
            deferred_cp_zoom('CAT_ELEM','*','CECODELE',cp_AbsName(oSource.parent,'oMOCATELE1_2_7'),i_cWhere,'GSAG_ACE',"Categorie elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                     +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',oSource.xKey(1))
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCATELE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CECODELE,CEDESELE";
                   +" from "+i_cTable+" "+i_lTable+" where CECODELE="+cp_ToStrODBC(this.w_MOCATELE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CECODELE',this.w_MOCATELE1)
            select CECODELE,CEDESELE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCATELE1 = NVL(_Link_.CECODELE,space(10))
      this.w_DESELE = NVL(_Link_.CEDESELE,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_MOCATELE1 = space(10)
      endif
      this.w_DESELE = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.CECODELE,1)
      cp_ShowWarn(i_cKey,this.CAT_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCATELE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCAUDOC1
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCAUDOC1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_MOCAUDOC1)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_MOCAUDOC1))
          select TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCAUDOC1)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCAUDOC1) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oMOCAUDOC1_2_19'),i_cWhere,'',"Causali documento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCAUDOC1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MOCAUDOC1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MOCAUDOC1)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCAUDOC1 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC1 = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MOCAUDOC1 = space(5)
      endif
      this.w_DESDOC1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCAUDOC1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOTIPATT1
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOTIPATT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MOTIPATT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MOTIPATT1))
          select CACODICE,CADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOTIPATT1)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOTIPATT1) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oMOTIPATT1_2_22'),i_cWhere,'',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOTIPATT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MOTIPATT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MOTIPATT1)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOTIPATT1 = NVL(_Link_.CACODICE,space(20))
      this.w_DESATT1 = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_MOTIPATT1 = space(20)
      endif
      this.w_DESATT1 = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOTIPATT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOGRUPAR
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOGRUPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_MOGRUPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_MOGRUPAR))
          select DPCODICE,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOGRUPAR)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOGRUPAR) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oMOGRUPAR_2_25'),i_cWhere,'',"",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOGRUPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_MOGRUPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_MOGRUPAR)
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOGRUPAR = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPRIS = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MOGRUPAR = space(5)
      endif
      this.w_TIPRIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIS='G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MOGRUPAR = space(5)
        this.w_TIPRIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOGRUPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCODSER1
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODSER1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_MOCODSER1)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_MOCODSER1))
          select ARCODART,ARDESART,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODSER1)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOCODSER1) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oMOCODSER1_2_28'),i_cWhere,'',"Servizi",'GSMA2AAS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODSER1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MOCODSER1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MOCODSER1)
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODSER1 = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODSER1 = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPART = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPART='FM' OR .w_TIPART='FO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_MOCODSER1 = space(20)
        this.w_DESART = space(40)
        this.w_TIPART = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODSER1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOCODLIS1
  func Link_2_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOCODLIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_MOCODLIS1)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_MOCODLIS1))
          select LSCODLIS,LSDESLIS,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOCODLIS1)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_MOCODLIS1)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_MOCODLIS1)+"%");

            select LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOCODLIS1) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oMOCODLIS1_2_32'),i_cWhere,'',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOCODLIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_MOCODLIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_MOCODLIS1)
            select LSCODLIS,LSDESLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOCODLIS1 = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_FLSCON = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MOCODLIS1 = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_FLSCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOCODLIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOPERCON1
  func Link_2_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOPERCON1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_MOPERCON1)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_MOPERCON1))
          select MDCODICE,MDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOPERCON1)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStrODBC(trim(this.w_MOPERCON1)+"%");

            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MDDESCRI like "+cp_ToStr(trim(this.w_MOPERCON1)+"%");

            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MOPERCON1) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oMOPERCON1_2_39'),i_cWhere,'',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOPERCON1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE,MDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_MOPERCON1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_MOPERCON1)
            select MDCODICE,MDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOPERCON1 = NVL(_Link_.MDCODICE,space(3))
      this.w_MDDESCRI = NVL(_Link_.MDDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_MOPERCON1 = space(3)
      endif
      this.w_MDDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOPERCON1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOPERFAT_FILTER
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOPERFAT_FILTER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_MOPERFAT_FILTER)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_MOPERFAT_FILTER))
          select MDCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOPERFAT_FILTER)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOPERFAT_FILTER) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oMOPERFAT_FILTER_1_25'),i_cWhere,'',"Metodi di calcolo periodicit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOPERFAT_FILTER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_MOPERFAT_FILTER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_MOPERFAT_FILTER)
            select MDCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOPERFAT_FILTER = NVL(_Link_.MDCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MOPERFAT_FILTER = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOPERFAT_FILTER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MOPERFAT
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MOPERFAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_MOPERFAT)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_MOPERFAT))
          select MDCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MOPERFAT)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MOPERFAT) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oMOPERFAT_1_27'),i_cWhere,'',"Categorie elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MOPERFAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_MOPERFAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_MOPERFAT)
            select MDCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MOPERFAT = NVL(_Link_.MDCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MOPERFAT = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MOPERFAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMOCATELE_FLAG_1_2.RadioValue()==this.w_MOCATELE_FLAG)
      this.oPgFrm.Page1.oPag.oMOCATELE_FLAG_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCATELE_ALL_1_3.RadioValue()==this.w_MOCATELE_ALL)
      this.oPgFrm.Page1.oPag.oMOCATELE_ALL_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCATELE_FILTER_1_4.value==this.w_MOCATELE_FILTER)
      this.oPgFrm.Page1.oPag.oMOCATELE_FILTER_1_4.value=this.w_MOCATELE_FILTER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_5.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_5.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oMOCATELE_1_6.value==this.w_MOCATELE)
      this.oPgFrm.Page1.oPag.oMOCATELE_1_6.value=this.w_MOCATELE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT1_1_7.value==this.w_DESCAT1)
      this.oPgFrm.Page1.oPag.oDESCAT1_1_7.value=this.w_DESCAT1
    endif
    if not(this.oPgFrm.Page1.oPag.oMOTIPAPL_ALL_1_16.RadioValue()==this.w_MOTIPAPL_ALL)
      this.oPgFrm.Page1.oPag.oMOTIPAPL_ALL_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOTIPAPL_FLAG_1_17.RadioValue()==this.w_MOTIPAPL_FLAG)
      this.oPgFrm.Page1.oPag.oMOTIPAPL_FLAG_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOTIPAPL_FILTER_1_18.RadioValue()==this.w_MOTIPAPL_FILTER)
      this.oPgFrm.Page1.oPag.oMOTIPAPL_FILTER_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOTIPAPL_1_19.RadioValue()==this.w_MOTIPAPL)
      this.oPgFrm.Page1.oPag.oMOTIPAPL_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODICE1_2_3.value==this.w_MOCODICE1)
      this.oPgFrm.Page2.oPag.oMOCODICE1_2_3.value=this.w_MOCODICE1
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODICE2_2_4.value==this.w_MOCODICE2)
      this.oPgFrm.Page2.oPag.oMOCODICE2_2_4.value=this.w_MOCODICE2
    endif
    if not(this.oPgFrm.Page2.oPag.oMODESCRI1_2_5.value==this.w_MODESCRI1)
      this.oPgFrm.Page2.oPag.oMODESCRI1_2_5.value=this.w_MODESCRI1
    endif
    if not(this.oPgFrm.Page2.oPag.oMODESCRI2_2_6.value==this.w_MODESCRI2)
      this.oPgFrm.Page2.oPag.oMODESCRI2_2_6.value=this.w_MODESCRI2
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCATELE1_2_7.value==this.w_MOCATELE1)
      this.oPgFrm.Page2.oPag.oMOCATELE1_2_7.value=this.w_MOCATELE1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESELE_2_9.value==this.w_DESELE)
      this.oPgFrm.Page2.oPag.oDESELE_2_9.value=this.w_DESELE
    endif
    if not(this.oPgFrm.Page2.oPag.oMOTIPAPL1_2_11.RadioValue()==this.w_MOTIPAPL1)
      this.oPgFrm.Page2.oPag.oMOTIPAPL1_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLFATT_2_12.RadioValue()==this.w_FLFATT)
      this.oPgFrm.Page2.oPag.oFLFATT_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLATTI_2_15.RadioValue()==this.w_FLATTI)
      this.oPgFrm.Page2.oPag.oFLATTI_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCAUDOC1_2_19.value==this.w_MOCAUDOC1)
      this.oPgFrm.Page2.oPag.oMOCAUDOC1_2_19.value=this.w_MOCAUDOC1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDOC1_2_20.value==this.w_DESDOC1)
      this.oPgFrm.Page2.oPag.oDESDOC1_2_20.value=this.w_DESDOC1
    endif
    if not(this.oPgFrm.Page2.oPag.oMOTIPATT1_2_22.value==this.w_MOTIPATT1)
      this.oPgFrm.Page2.oPag.oMOTIPATT1_2_22.value=this.w_MOTIPATT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT1_2_23.value==this.w_DESATT1)
      this.oPgFrm.Page2.oPag.oDESATT1_2_23.value=this.w_DESATT1
    endif
    if not(this.oPgFrm.Page2.oPag.oMOGRUPAR_2_25.value==this.w_MOGRUPAR)
      this.oPgFrm.Page2.oPag.oMOGRUPAR_2_25.value=this.w_MOGRUPAR
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODSER1_2_28.value==this.w_MOCODSER1)
      this.oPgFrm.Page2.oPag.oMOCODSER1_2_28.value=this.w_MOCODSER1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESART_2_29.value==this.w_DESART)
      this.oPgFrm.Page2.oPag.oDESART_2_29.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page2.oPag.oMOCODLIS1_2_32.value==this.w_MOCODLIS1)
      this.oPgFrm.Page2.oPag.oMOCODLIS1_2_32.value=this.w_MOCODLIS1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLIS_2_33.value==this.w_DESLIS)
      this.oPgFrm.Page2.oPag.oDESLIS_2_33.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oESCRIN_2_35.RadioValue()==this.w_ESCRIN)
      this.oPgFrm.Page2.oPag.oESCRIN_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMOPERCON1_2_39.value==this.w_MOPERCON1)
      this.oPgFrm.Page2.oPag.oMOPERCON1_2_39.value=this.w_MOPERCON1
    endif
    if not(this.oPgFrm.Page2.oPag.oMDDESCRI_2_40.value==this.w_MDDESCRI)
      this.oPgFrm.Page2.oPag.oMDDESCRI_2_40.value=this.w_MDDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oRINCON_2_41.RadioValue()==this.w_RINCON)
      this.oPgFrm.Page2.oPag.oRINCON_2_41.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMONUMGIO1_2_45.value==this.w_MONUMGIO1)
      this.oPgFrm.Page2.oPag.oMONUMGIO1_2_45.value=this.w_MONUMGIO1
    endif
    if not(this.oPgFrm.Page2.oPag.oMONUMGIO2_2_47.value==this.w_MONUMGIO2)
      this.oPgFrm.Page2.oPag.oMONUMGIO2_2_47.value=this.w_MONUMGIO2
    endif
    if not(this.oPgFrm.Page1.oPag.oESTENDI_1_21.RadioValue()==this.w_ESTENDI)
      this.oPgFrm.Page1.oPag.oESTENDI_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPERFAT_FLAG_1_23.RadioValue()==this.w_MOPERFAT_FLAG)
      this.oPgFrm.Page1.oPag.oMOPERFAT_FLAG_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPERFAT_ALL_1_24.RadioValue()==this.w_MOPERFAT_ALL)
      this.oPgFrm.Page1.oPag.oMOPERFAT_ALL_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPERFAT_FILTER_1_25.value==this.w_MOPERFAT_FILTER)
      this.oPgFrm.Page1.oPag.oMOPERFAT_FILTER_1_25.value=this.w_MOPERFAT_FILTER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_26.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_26.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oMOPERFAT_1_27.value==this.w_MOPERFAT)
      this.oPgFrm.Page1.oPag.oMOPERFAT_1_27.value=this.w_MOPERFAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT1_1_28.value==this.w_DESCAT1)
      this.oPgFrm.Page1.oPag.oDESCAT1_1_28.value=this.w_DESCAT1
    endif
    if not(this.oPgFrm.Page1.oPag.oMOGIODOC_FLAG_1_30.RadioValue()==this.w_MOGIODOC_FLAG)
      this.oPgFrm.Page1.oPag.oMOGIODOC_FLAG_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOGIODOC_ALL_1_31.RadioValue()==this.w_MOGIODOC_ALL)
      this.oPgFrm.Page1.oPag.oMOGIODOC_ALL_1_31.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPRIS='G')  and not(empty(.w_MOGRUPAR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMOGRUPAR_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPART='FM' OR .w_TIPART='FO')  and not(empty(.w_MOCODSER1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMOCODSER1_2_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MOCATELE_ALL = this.w_MOCATELE_ALL
    this.o_MOCATELE = this.w_MOCATELE
    this.o_MOTIPAPL = this.w_MOTIPAPL
    return

enddefine

* --- Define pages as container
define class tgsag_kvmPag1 as StdContainer
  Width  = 728
  height = 483
  stdWidth  = 728
  stdheight = 483
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMOCATELE_FLAG_1_2 as StdCheck with uid="LEZIBFLUTY",rtseq=1,rtrep=.f.,left=173, top=39, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la categoria elementi",;
    HelpContextID = 206661883,;
    cFormVar="w_MOCATELE_FLAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOCATELE_FLAG_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOCATELE_FLAG_1_2.GetRadio()
    this.Parent.oContained.w_MOCATELE_FLAG = this.RadioValue()
    return .t.
  endfunc

  func oMOCATELE_FLAG_1_2.SetRadio()
    this.Parent.oContained.w_MOCATELE_FLAG=trim(this.Parent.oContained.w_MOCATELE_FLAG)
    this.value = ;
      iif(this.Parent.oContained.w_MOCATELE_FLAG=='S',1,;
      0)
  endfunc

  add object oMOCATELE_ALL_1_3 as StdCheck with uid="LQVMEHOEYX",rtseq=2,rtrep=.f.,left=173, top=16, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le categorie elementi",;
    HelpContextID = 132932603,;
    cFormVar="w_MOCATELE_ALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOCATELE_ALL_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOCATELE_ALL_1_3.GetRadio()
    this.Parent.oContained.w_MOCATELE_ALL = this.RadioValue()
    return .t.
  endfunc

  func oMOCATELE_ALL_1_3.SetRadio()
    this.Parent.oContained.w_MOCATELE_ALL=trim(this.Parent.oContained.w_MOCATELE_ALL)
    this.value = ;
      iif(this.Parent.oContained.w_MOCATELE_ALL=='S',1,;
      0)
  endfunc

  add object oMOCATELE_FILTER_1_4 as StdField with uid="IANYSSOCGF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MOCATELE_FILTER", cQueryName = "MOCATELE_FILTER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria elementi originaria da sostituire",;
    HelpContextID = 231982853,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=286, Top=17, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_ELEM", cZoomOnZoom="GSAG_ACE", oKey_1_1="CECODELE", oKey_1_2="this.w_MOCATELE_FILTER"

  func oMOCATELE_FILTER_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOCATELE_ALL<>'S')
    endwith
   endif
  endfunc

  func oMOCATELE_FILTER_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCATELE_FILTER_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCATELE_FILTER_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ELEM','*','CECODELE',cp_AbsName(this.parent,'oMOCATELE_FILTER_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACE',"Categorie elementi",'',this.parent.oContained
  endproc
  proc oMOCATELE_FILTER_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODELE=this.parent.oContained.w_MOCATELE_FILTER
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_5 as StdField with uid="BROOVUECUP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177319370,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=376, Top=17, InputMask=replicate('X',35)

  add object oMOCATELE_1_6 as StdField with uid="DITGWVKJMP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MOCATELE", cQueryName = "MOCATELE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria elementi che � possibile inserire in sostituzione",;
    HelpContextID = 127622411,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=286, Top=40, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_ELEM", cZoomOnZoom="GSAG_ACE", oKey_1_1="CECODELE", oKey_1_2="this.w_MOCATELE"

  func oMOCATELE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCATELE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCATELE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ELEM','*','CECODELE',cp_AbsName(this.parent,'oMOCATELE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACE',"Categorie elementi",'',this.parent.oContained
  endproc
  proc oMOCATELE_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODELE=this.parent.oContained.w_MOCATELE
     i_obj.ecpSave()
  endproc

  add object oDESCAT1_1_7 as StdField with uid="KZOSDFSSXL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCAT1", cQueryName = "DESCAT1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177319370,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=376, Top=40, InputMask=replicate('X',35)


  add object oBtn_1_13 as StdButton with uid="XAOFRYAIID",left=540, top=432, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 219564262;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do GSAG_BVM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="KYQESPJULE",left=592, top=432, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 226852934;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMOTIPAPL_ALL_1_16 as StdCheck with uid="RIWPJGUOHK",rtseq=7,rtrep=.f.,left=172, top=74, caption="Tutti",;
    ToolTipText = "Se attivo: aggiorna il tipo applicazione in tutti i modelli",;
    HelpContextID = 206212094,;
    cFormVar="w_MOTIPAPL_ALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOTIPAPL_ALL_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOTIPAPL_ALL_1_16.GetRadio()
    this.Parent.oContained.w_MOTIPAPL_ALL = this.RadioValue()
    return .t.
  endfunc

  func oMOTIPAPL_ALL_1_16.SetRadio()
    this.Parent.oContained.w_MOTIPAPL_ALL=trim(this.Parent.oContained.w_MOTIPAPL_ALL)
    this.value = ;
      iif(this.Parent.oContained.w_MOTIPAPL_ALL=='S',1,;
      0)
  endfunc

  add object oMOTIPAPL_FLAG_1_17 as StdCheck with uid="KYHYZWFIJN",rtseq=8,rtrep=.f.,left=172, top=97, caption="Sostituire con:",;
    ToolTipText = "Se attivo: aggiorna con il tipo di applicazione specificato",;
    HelpContextID = 132482814,;
    cFormVar="w_MOTIPAPL_FLAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOTIPAPL_FLAG_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOTIPAPL_FLAG_1_17.GetRadio()
    this.Parent.oContained.w_MOTIPAPL_FLAG = this.RadioValue()
    return .t.
  endfunc

  func oMOTIPAPL_FLAG_1_17.SetRadio()
    this.Parent.oContained.w_MOTIPAPL_FLAG=trim(this.Parent.oContained.w_MOTIPAPL_FLAG)
    this.value = ;
      iif(this.Parent.oContained.w_MOTIPAPL_FLAG=='S',1,;
      0)
  endfunc


  add object oMOTIPAPL_FILTER_1_18 as StdCombo with uid="QPMYYKWHZF",rtseq=9,rtrep=.f.,left=285,top=74,width=137,height=21;
    , ToolTipText = "Tipo di applicazione originaria da sostituire";
    , HelpContextID = 34256638;
    , cFormVar="w_MOTIPAPL_FILTER",RowSource=""+"Differita,"+"Immediata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOTIPAPL_FILTER_1_18.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'C',;
    space(5))))
  endfunc
  func oMOTIPAPL_FILTER_1_18.GetRadio()
    this.Parent.oContained.w_MOTIPAPL_FILTER = this.RadioValue()
    return .t.
  endfunc

  func oMOTIPAPL_FILTER_1_18.SetRadio()
    this.Parent.oContained.w_MOTIPAPL_FILTER=trim(this.Parent.oContained.w_MOTIPAPL_FILTER)
    this.value = ;
      iif(this.Parent.oContained.w_MOTIPAPL_FILTER=='F',1,;
      iif(this.Parent.oContained.w_MOTIPAPL_FILTER=='C',2,;
      0))
  endfunc

  func oMOTIPAPL_FILTER_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPAPL_ALL<>'S')
    endwith
   endif
  endfunc


  add object oMOTIPAPL_1_19 as StdCombo with uid="KZNTCJCLSY",rtseq=10,rtrep=.f.,left=285,top=99,width=137,height=21;
    , ToolTipText = "Tipo di applicazione che � possibile inserire in sostituzione";
    , HelpContextID = 211522286;
    , cFormVar="w_MOTIPAPL",RowSource=""+"Differita,"+"Immediata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOTIPAPL_1_19.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'C',;
    space(5))))
  endfunc
  func oMOTIPAPL_1_19.GetRadio()
    this.Parent.oContained.w_MOTIPAPL = this.RadioValue()
    return .t.
  endfunc

  func oMOTIPAPL_1_19.SetRadio()
    this.Parent.oContained.w_MOTIPAPL=trim(this.Parent.oContained.w_MOTIPAPL)
    this.value = ;
      iif(this.Parent.oContained.w_MOTIPAPL=='F',1,;
      iif(this.Parent.oContained.w_MOTIPAPL=='C',2,;
      0))
  endfunc

  func oMOTIPAPL_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOTIPAPL_FLAG<>'S')
    endwith
   endif
  endfunc

  add object oESTENDI_1_21 as StdCheck with uid="CUUHGEGUSF",rtseq=42,rtrep=.f.,left=17, top=446, caption="Estendi modifiche agli elementi contratto",;
    ToolTipText = "Estende modifiche agli elementi contratto a cui � associato il modello elemento che viene aggiornato",;
    HelpContextID = 104886342,;
    cFormVar="w_ESTENDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESTENDI_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oESTENDI_1_21.GetRadio()
    this.Parent.oContained.w_ESTENDI = this.RadioValue()
    return .t.
  endfunc

  func oESTENDI_1_21.SetRadio()
    this.Parent.oContained.w_ESTENDI=trim(this.Parent.oContained.w_ESTENDI)
    this.value = ;
      iif(this.Parent.oContained.w_ESTENDI=='S',1,;
      0)
  endfunc

  add object oMOPERFAT_FLAG_1_23 as StdCheck with uid="VYSRFOUMAD",rtseq=43,rtrep=.f.,left=173, top=176, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la categoria elementi",;
    HelpContextID = 46778102,;
    cFormVar="w_MOPERFAT_FLAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOPERFAT_FLAG_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOPERFAT_FLAG_1_23.GetRadio()
    this.Parent.oContained.w_MOPERFAT_FLAG = this.RadioValue()
    return .t.
  endfunc

  func oMOPERFAT_FLAG_1_23.SetRadio()
    this.Parent.oContained.w_MOPERFAT_FLAG=trim(this.Parent.oContained.w_MOPERFAT_FLAG)
    this.value = ;
      iif(this.Parent.oContained.w_MOPERFAT_FLAG=='S',1,;
      0)
  endfunc

  add object oMOPERFAT_ALL_1_24 as StdCheck with uid="GUYCKDMEPP",rtseq=44,rtrep=.f.,left=173, top=153, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le periodicit� di generazione documento",;
    HelpContextID = 120507382,;
    cFormVar="w_MOPERFAT_ALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOPERFAT_ALL_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOPERFAT_ALL_1_24.GetRadio()
    this.Parent.oContained.w_MOPERFAT_ALL = this.RadioValue()
    return .t.
  endfunc

  func oMOPERFAT_ALL_1_24.SetRadio()
    this.Parent.oContained.w_MOPERFAT_ALL=trim(this.Parent.oContained.w_MOPERFAT_ALL)
    this.value = ;
      iif(this.Parent.oContained.w_MOPERFAT_ALL=='S',1,;
      0)
  endfunc

  add object oMOPERFAT_FILTER_1_25 as StdField with uid="DTCTIHDFFC",rtseq=45,rtrep=.f.,;
    cFormVar = "w_MOPERFAT_FILTER", cQueryName = "MOPERFAT_FILTER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Periodicit� documenti originaria da sostituire",;
    HelpContextID = 51448074,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=286, Top=154, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MODCLDAT", oKey_1_1="MDCODICE", oKey_1_2="this.w_MOPERFAT_FILTER"

  func oMOPERFAT_FILTER_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MOPERFAT_ALL<>'S')
    endwith
   endif
  endfunc

  func oMOPERFAT_FILTER_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOPERFAT_FILTER_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOPERFAT_FILTER_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oMOPERFAT_FILTER_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Metodi di calcolo periodicit�",'',this.parent.oContained
  endproc

  add object oDESCAT_1_26 as StdField with uid="UREEXNRNIB",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177319370,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=376, Top=154, InputMask=replicate('X',35)

  add object oMOPERFAT_1_27 as StdField with uid="LGEKWMMCIE",rtseq=47,rtrep=.f.,;
    cFormVar = "w_MOPERFAT", cQueryName = "MOPERFAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria elementi che � possibile inserire in sostituzione",;
    HelpContextID = 125817574,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=286, Top=177, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MODCLDAT", oKey_1_1="MDCODICE", oKey_1_2="this.w_MOPERFAT"

  func oMOPERFAT_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOPERFAT_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOPERFAT_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oMOPERFAT_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Categorie elementi",'',this.parent.oContained
  endproc

  add object oDESCAT1_1_28 as StdField with uid="OVNJOCQLAU",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESCAT1", cQueryName = "DESCAT1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177319370,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=376, Top=177, InputMask=replicate('X',35)

  add object oMOGIODOC_FLAG_1_30 as StdCheck with uid="QHOLFZVYXB",rtseq=49,rtrep=.f.,left=173, top=228, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la categoria elementi",;
    HelpContextID = 83252999,;
    cFormVar="w_MOGIODOC_FLAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOGIODOC_FLAG_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOGIODOC_FLAG_1_30.GetRadio()
    this.Parent.oContained.w_MOGIODOC_FLAG = this.RadioValue()
    return .t.
  endfunc

  func oMOGIODOC_FLAG_1_30.SetRadio()
    this.Parent.oContained.w_MOGIODOC_FLAG=trim(this.Parent.oContained.w_MOGIODOC_FLAG)
    this.value = ;
      iif(this.Parent.oContained.w_MOGIODOC_FLAG=='S',1,;
      0)
  endfunc

  add object oMOGIODOC_ALL_1_31 as StdCheck with uid="FRDSBGSRWL",rtseq=50,rtrep=.f.,left=173, top=205, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce i giorni di tolleranza per la generazione del documento",;
    HelpContextID = 156982279,;
    cFormVar="w_MOGIODOC_ALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOGIODOC_ALL_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMOGIODOC_ALL_1_31.GetRadio()
    this.Parent.oContained.w_MOGIODOC_ALL = this.RadioValue()
    return .t.
  endfunc

  func oMOGIODOC_ALL_1_31.SetRadio()
    this.Parent.oContained.w_MOGIODOC_ALL=trim(this.Parent.oContained.w_MOGIODOC_ALL)
    this.value = ;
      iif(this.Parent.oContained.w_MOGIODOC_ALL=='S',1,;
      0)
  endfunc

  add object oStr_1_1 as StdString with uid="XOSRJAQEIX",Visible=.t., Left=32, Top=19,;
    Alignment=1, Width=134, Height=18,;
    Caption="Categoria elementi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NILDDKGYYW",Visible=.t., Left=31, Top=77,;
    Alignment=1, Width=134, Height=18,;
    Caption="Tipo applicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="VBYPCQBOXE",Visible=.t., Left=29, Top=135,;
    Alignment=0, Width=427, Height=18,;
    Caption="Modifiche applicabili solo ai modelli che hanno attivo il check Gen. Documento"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="FQBINSBASK",Visible=.t., Left=32, Top=156,;
    Alignment=1, Width=134, Height=18,;
    Caption="Categoria elementi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="CIBOZOMFXT",Visible=.t., Left=10, Top=208,;
    Alignment=1, Width=156, Height=18,;
    Caption="Giorni di tolleranza:"  ;
  , bGlobalFont=.t.

  add object oBox_1_8 as StdBox with uid="TJNEYCGMWS",left=24, top=69, width=625,height=2

  add object oBox_1_9 as StdBox with uid="WUPUBDEDMS",left=24, top=126, width=625,height=2

  add object oBox_1_10 as StdBox with uid="FHWHXJRMRT",left=24, top=252, width=625,height=2

  add object oBox_1_11 as StdBox with uid="JSKTRNGBOW",left=24, top=310, width=625,height=2

  add object oBox_1_12 as StdBox with uid="HPKSGURETV",left=24, top=11, width=625,height=2
enddefine
define class tgsag_kvmPag2 as StdContainer
  Width  = 728
  height = 483
  stdWidth  = 728
  stdheight = 483
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMOCODICE1_2_3 as StdField with uid="CLUGIHRROC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MOCODICE1", cQueryName = "MOCODICE1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice modello elemento iniziale della selezione",;
    HelpContextID = 89563109,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=150, Top=21, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_ELEM", cZoomOnZoom="GSAG_AME", oKey_1_1="MOCODICE", oKey_1_2="this.w_MOCODICE1"

  func oMOCODICE1_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODICE1_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODICE1_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_ELEM','*','MOCODICE',cp_AbsName(this.parent,'oMOCODICE1_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_AME',"Modelli elementi",'',this.parent.oContained
  endproc
  proc oMOCODICE1_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAG_AME()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_MOCODICE1
     i_obj.ecpSave()
  endproc

  add object oMOCODICE2_2_4 as StdField with uid="OURXNUQMAK",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MOCODICE2", cQueryName = "MOCODICE2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice modello elemento finale della selezione",;
    HelpContextID = 89563093,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=150, Top=45, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_ELEM", cZoomOnZoom="GSAG_AME", oKey_1_1="MOCODICE", oKey_1_2="this.w_MOCODICE2"

  func oMOCODICE2_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODICE2_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODICE2_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_ELEM','*','MOCODICE',cp_AbsName(this.parent,'oMOCODICE2_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_AME',"Modelli elementi",'',this.parent.oContained
  endproc
  proc oMOCODICE2_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAG_AME()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MOCODICE=this.parent.oContained.w_MOCODICE2
     i_obj.ecpSave()
  endproc

  add object oMODESCRI1_2_5 as StdField with uid="QDGOBJERLI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MODESCRI1", cQueryName = "MODESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 175149025,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=238, Top=20, InputMask=replicate('X',60)

  add object oMODESCRI2_2_6 as StdField with uid="WJBVNEAZQV",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MODESCRI2", cQueryName = "MODESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 175149009,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=238, Top=45, InputMask=replicate('X',60)

  add object oMOCATELE1_2_7 as StdField with uid="ATVUGASTNW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MOCATELE1", cQueryName = "MOCATELE1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 127623195,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=150, Top=70, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ELEM", cZoomOnZoom="GSAG_ACE", oKey_1_1="CECODELE", oKey_1_2="this.w_MOCATELE1"

  func oMOCATELE1_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCATELE1_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCATELE1_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ELEM','*','CECODELE',cp_AbsName(this.parent,'oMOCATELE1_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ACE',"Categorie elementi",'',this.parent.oContained
  endproc
  proc oMOCATELE1_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ACE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CECODELE=this.parent.oContained.w_MOCATELE1
     i_obj.ecpSave()
  endproc

  add object oDESELE_2_9 as StdField with uid="DJXDYDVFQU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESELE", cQueryName = "DESELE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 148876746,;
   bGlobalFont=.t.,;
    Height=21, Width=330, Left=238, Top=70, InputMask=replicate('X',60)


  add object oMOTIPAPL1_2_11 as StdCombo with uid="QBLSEYDCTO",rtseq=17,rtrep=.f.,left=150,top=97,width=137,height=21;
    , ToolTipText = "Tipo di applicazione che � possibile inserire in sostituzione";
    , HelpContextID = 211521502;
    , cFormVar="w_MOTIPAPL1",RowSource=""+"Differita,"+"Immediata,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oMOTIPAPL1_2_11.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'C',;
    iif(this.value =3,'T',;
    space(5)))))
  endfunc
  func oMOTIPAPL1_2_11.GetRadio()
    this.Parent.oContained.w_MOTIPAPL1 = this.RadioValue()
    return .t.
  endfunc

  func oMOTIPAPL1_2_11.SetRadio()
    this.Parent.oContained.w_MOTIPAPL1=trim(this.Parent.oContained.w_MOTIPAPL1)
    this.value = ;
      iif(this.Parent.oContained.w_MOTIPAPL1=='F',1,;
      iif(this.Parent.oContained.w_MOTIPAPL1=='C',2,;
      iif(this.Parent.oContained.w_MOTIPAPL1=='T',3,;
      0)))
  endfunc


  add object oFLFATT_2_12 as StdCombo with uid="CJMCMLJOPW",rtseq=18,rtrep=.f.,left=150,top=122,width=137,height=21;
    , ToolTipText = "Filtra sul flag di generazione documento";
    , HelpContextID = 157578922;
    , cFormVar="w_FLFATT",RowSource=""+"Genera,"+"Non genera,"+"Entrambi", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLFATT_2_12.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oFLFATT_2_12.GetRadio()
    this.Parent.oContained.w_FLFATT = this.RadioValue()
    return .t.
  endfunc

  func oFLFATT_2_12.SetRadio()
    this.Parent.oContained.w_FLFATT=trim(this.Parent.oContained.w_FLFATT)
    this.value = ;
      iif(this.Parent.oContained.w_FLFATT=='S',1,;
      iif(this.Parent.oContained.w_FLFATT=='N',2,;
      iif(this.Parent.oContained.w_FLFATT=='E',3,;
      0)))
  endfunc


  add object oFLATTI_2_15 as StdCombo with uid="XANHSZOUXM",rtseq=20,rtrep=.f.,left=150,top=147,width=137,height=21;
    , ToolTipText = "Filtra sul flag di generazione attivit�";
    , HelpContextID = 72468138;
    , cFormVar="w_FLATTI",RowSource=""+"Genera,"+"Non genera,"+"Entrambi", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLATTI_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oFLATTI_2_15.GetRadio()
    this.Parent.oContained.w_FLATTI = this.RadioValue()
    return .t.
  endfunc

  func oFLATTI_2_15.SetRadio()
    this.Parent.oContained.w_FLATTI=trim(this.Parent.oContained.w_FLATTI)
    this.value = ;
      iif(this.Parent.oContained.w_FLATTI=='S',1,;
      iif(this.Parent.oContained.w_FLATTI=='N',2,;
      iif(this.Parent.oContained.w_FLATTI=='E',3,;
      0)))
  endfunc

  add object oMOCAUDOC1_2_19 as StdField with uid="CMVXZJREFN",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MOCAUDOC1", cQueryName = "MOCAUDOC1",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 156540903,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=149, Top=170, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MOCAUDOC1"

  func oMOCAUDOC1_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCAUDOC1_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCAUDOC1_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oMOCAUDOC1_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documento",'',this.parent.oContained
  endproc

  add object oDESDOC1_2_20 as StdField with uid="NZXKUALSRW",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESDOC1", cQueryName = "DESDOC1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 179350986,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=203, Top=171, InputMask=replicate('X',35)

  add object oMOTIPATT1_2_22 as StdField with uid="AJGKVSUDBD",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MOTIPATT1", cQueryName = "MOTIPATT1",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 211521494,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=149, Top=195, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", oKey_1_1="CACODICE", oKey_1_2="this.w_MOTIPATT1"

  func oMOTIPATT1_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOTIPATT1_2_22.ecpDrop(oSource)
    this.Parent.oContained.link_2_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOTIPATT1_2_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oMOTIPATT1_2_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc

  add object oDESATT1_2_23 as StdField with uid="ZYPITXDJMY",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESATT1", cQueryName = "DESATT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 157527498,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=305, Top=195, InputMask=replicate('X',254)

  add object oMOGRUPAR_2_25 as StdField with uid="TJPMVWVYAB",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MOGRUPAR", cQueryName = "MOGRUPAR",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 222520040,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=630, Top=194, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_MOGRUPAR"

  func oMOGRUPAR_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOGRUPAR_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOGRUPAR_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oMOGRUPAR_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oMOCODSER1_2_28 as StdField with uid="FGZGZXQZQY",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MOCODSER1", cQueryName = "MOCODSER1",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice servizio",;
    HelpContextID = 190226392,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=149, Top=221, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_MOCODSER1"

  func oMOCODSER1_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODSER1_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODSER1_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oMOCODSER1_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Servizi",'GSMA2AAS.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDESART_2_29 as StdField with uid="UNCGTQTYBK",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 159624650,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=305, Top=221, InputMask=replicate('X',40)

  add object oMOCODLIS1_2_32 as StdField with uid="VMAUGSBXUF",rtseq=31,rtrep=.f.,;
    cFormVar = "w_MOCODLIS1", cQueryName = "MOCODLIS1",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino gestito a sconti",;
    HelpContextID = 229204009,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=149, Top=248, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_MOCODLIS1"

  func oMOCODLIS1_2_32.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oMOCODLIS1_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOCODLIS1_2_32.ecpDrop(oSource)
    this.Parent.oContained.link_2_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOCODLIS1_2_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oMOCODLIS1_2_32'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc

  add object oDESLIS_2_33 as StdField with uid="JASNKZJIAR",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 185118154,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=201, Top=248, InputMask=replicate('X',40)

  func oDESLIS_2_33.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc


  add object oESCRIN_2_35 as StdCombo with uid="DTXMYXVXRF",rtseq=34,rtrep=.f.,left=151,top=276,width=137,height=21;
    , ToolTipText = "Filtra sul flag di esclusione rinnovo";
    , HelpContextID = 237498;
    , cFormVar="w_ESCRIN",RowSource=""+"S�,"+"No,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oESCRIN_2_35.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oESCRIN_2_35.GetRadio()
    this.Parent.oContained.w_ESCRIN = this.RadioValue()
    return .t.
  endfunc

  func oESCRIN_2_35.SetRadio()
    this.Parent.oContained.w_ESCRIN=trim(this.Parent.oContained.w_ESCRIN)
    this.value = ;
      iif(this.Parent.oContained.w_ESCRIN=='S',1,;
      iif(this.Parent.oContained.w_ESCRIN=='N',2,;
      iif(this.Parent.oContained.w_ESCRIN=='E',3,;
      0)))
  endfunc

  add object oMOPERCON1_2_39 as StdField with uid="UUBWUURPXT",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MOPERCON1", cQueryName = "MOPERCON1",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Periodicit� rinnovo contratti",;
    HelpContextID = 176148444,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=150, Top=299, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODCLDAT", oKey_1_1="MDCODICE", oKey_1_2="this.w_MOPERCON1"

  func oMOPERCON1_2_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oMOPERCON1_2_39.ecpDrop(oSource)
    this.Parent.oContained.link_2_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMOPERCON1_2_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODCLDAT','*','MDCODICE',cp_AbsName(this.parent,'oMOPERCON1_2_39'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Metodi di calcolo",'',this.parent.oContained
  endproc

  add object oMDDESCRI_2_40 as StdField with uid="ZGHHQSBPGH",rtseq=37,rtrep=.f.,;
    cFormVar = "w_MDDESCRI", cQueryName = "MDDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 175152625,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=202, Top=299, InputMask=replicate('X',50)


  add object oRINCON_2_41 as StdCombo with uid="TTFPLSAFMJ",rtseq=38,rtrep=.f.,left=152,top=327,width=137,height=21;
    , ToolTipText = "Filtra sul flag di rinnovo tacito";
    , HelpContextID = 263321834;
    , cFormVar="w_RINCON",RowSource=""+"S�,"+"No,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oRINCON_2_41.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oRINCON_2_41.GetRadio()
    this.Parent.oContained.w_RINCON = this.RadioValue()
    return .t.
  endfunc

  func oRINCON_2_41.SetRadio()
    this.Parent.oContained.w_RINCON=trim(this.Parent.oContained.w_RINCON)
    this.value = ;
      iif(this.Parent.oContained.w_RINCON=='S',1,;
      iif(this.Parent.oContained.w_RINCON=='N',2,;
      iif(this.Parent.oContained.w_RINCON=='E',3,;
      0)))
  endfunc

  add object oMONUMGIO1_2_45 as StdField with uid="ZHGPFDSKOV",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MONUMGIO1", cQueryName = "MONUMGIO1",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo di giorni di preavviso",;
    HelpContextID = 155193381,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=150, Top=352

  add object oMONUMGIO2_2_47 as StdField with uid="VDXNFXACXM",rtseq=41,rtrep=.f.,;
    cFormVar = "w_MONUMGIO2", cQueryName = "MONUMGIO2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero minimo di giorni di preavviso",;
    HelpContextID = 155193397,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=361, Top=351

  add object oStr_2_1 as StdString with uid="JFSSYFZXGG",Visible=.t., Left=-18, Top=23,;
    Alignment=1, Width=163, Height=18,;
    Caption="Da modello elemento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="FGTFJIZDGJ",Visible=.t., Left=-18, Top=47,;
    Alignment=1, Width=163, Height=18,;
    Caption="A modello elemento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="TFPQJRIABN",Visible=.t., Left=7, Top=72,;
    Alignment=1, Width=138, Height=18,;
    Caption="Categoria elementi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="FTFLGYSIJM",Visible=.t., Left=46, Top=98,;
    Alignment=0, Width=99, Height=18,;
    Caption="Tipo applicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="GWZYSJQXUC",Visible=.t., Left=-6, Top=122,;
    Alignment=1, Width=151, Height=18,;
    Caption="Gen. Documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="QWQWQGGVGU",Visible=.t., Left=-6, Top=147,;
    Alignment=1, Width=151, Height=18,;
    Caption="Gen. Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="AMFZNNHOMK",Visible=.t., Left=-11, Top=173,;
    Alignment=1, Width=156, Height=18,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="CEBBKKWXFB",Visible=.t., Left=43, Top=198,;
    Alignment=1, Width=102, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="ZRURRXYEMQ",Visible=.t., Left=571, Top=196,;
    Alignment=1, Width=56, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="ZCKEITXPFH",Visible=.t., Left=61, Top=224,;
    Alignment=1, Width=84, Height=18,;
    Caption="Servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="GYRSKJOCAZ",Visible=.t., Left=58, Top=250,;
    Alignment=1, Width=88, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_2_31.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_2_37 as StdString with uid="ZUAEEIJROE",Visible=.t., Left=-5, Top=276,;
    Alignment=1, Width=151, Height=18,;
    Caption="Escludi rinnovo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="HSMICCPTQO",Visible=.t., Left=-17, Top=302,;
    Alignment=1, Width=164, Height=18,;
    Caption="Periodicit� rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="GWGFHTMOSU",Visible=.t., Left=-4, Top=327,;
    Alignment=1, Width=151, Height=18,;
    Caption="Rinnovo tacito:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="VSULBXGDUY",Visible=.t., Left=15, Top=356,;
    Alignment=1, Width=133, Height=18,;
    Caption="Da giorni di preavviso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="WUNUTHCPRI",Visible=.t., Left=238, Top=355,;
    Alignment=1, Width=121, Height=18,;
    Caption="A giorni di preavviso:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kvm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
