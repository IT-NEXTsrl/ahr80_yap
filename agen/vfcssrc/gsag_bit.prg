* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bit                                                        *
*              Carica attivit� da un iter                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-02-23                                                      *
* Last revis.: 2014-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Param
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bit",oParentObject,m.Param)
return(i_retval)

define class tgsag_bit as StdBatch
  * --- Local variables
  Param = space(1)
  w_DPTIPRIS = space(1)
  w_OBJ = .NULL.
  w_RESULT = space(254)
  w_ERROR = 0
  CONTA = 0
  w_OBJECT = .NULL.
  NUMSERIAL = space(15)
  CURSORE = space(10)
  w_DAVOCCOS = space(15)
  w_DAVOCRIC = space(15)
  w_IPNUMGIO = 0
  w_IPRIFPRO = 0
  w_DATA_ATT = ctot("")
  w_SAVREC = 0
  CONTA_ATT = 0
  w_SERIAL_PRIMA_ATT = space(20)
  w_ATRIFSER = space(20)
  w_SerialeAttivita = space(20)
  w_ObjCaller = .NULL.
  STATO = space(1)
  FL_PRAT_OBBL = space(1)
  w_IPCODCAU = space(20)
  w_FL_GIUD = space(1)
  FL_NOM_OBBL = space(1)
  w_ControlloInviaMail = .f.
  w_CAFLNOTI = space(1)
  w_CAFLPREA = space(1)
  w_OK = .f.
  objAG_KRA = .NULL.
  w_PADRE = .NULL.
  w_CAUATT = space(5)
  w_CPROWNUM = 0
  w_DPCODICE = space(5)
  w_XCHK = 0
  w_ROWNUM = 0
  w_IPCODICE = space(10)
  w_MODAPP = space(1)
  w_PADRE = .NULL.
  * --- WorkFile variables
  CAUMATTI_idx=0
  OFF_ATTI_idx=0
  OFFDATTI_idx=0
  AHEUSRCO_idx=0
  OFF_PART_idx=0
  CAU_ATTI_idx=0
  ART_ICOL_idx=0
  DIPENDEN_idx=0
  PRA_ENTI_idx=0
  PAR_AGEN_idx=0
  KEY_ARTI_idx=0
  UNIMIS_idx=0
  TIP_DOCU_idx=0
  OFF_NOMI_idx=0
  LISTINI_idx=0
  COM_ATTI_idx=0
  VALUTE_idx=0
  ATT_DCOL_idx=0
  PRA_TIPI_idx=0
  TMPCONTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSAG_KIT e da GSAG_KAT
    * --- Param = C :   Esegue controllo sulle causali dell'iter e sui partecipanti
    *     Param = S :   Seleziona le righe dei partecipanti
    *     Param = A :   Carica attivit� da iter (con relativi dati)
    *     Param = V :   Accede all' attivit� in variazione 
    *     Param = E :   Cancella l'attivit�
    *     Param = T :   Cancella tutte le attivit�
    *     Param = R :  Refresh in Elenco Attivit�
    *     Param = G :  Determina giorno settimana in Attivit� collegate generate
    *     Param = Q :  Before query dettaglio partecipanti
    *     Param = D :  After query dettaglio partecipanti
    *     Param = K :   Cambio Iter
    *     Param = P :   Evento applica
    this.w_DPTIPRIS = "Q"
    this.w_ERROR = 0
    this.w_ObjCaller = this.oParentObject
    do case
      case this.Param = "C"
        this.oParentObject.w_RESCHK = 0
        if EMPTY(this.oParentObject.w_CODITER)
          ah_ErrorMsg("Selezionare il codice del raggruppamento.")
          this.oParentObject.w_RESCHK = -1
          this.w_OBJ = this.w_ObjCaller.Getctrl("w_CODITER")
          if vartype(this.w_OBJ)="O"
            * --- Viene posizionato il cursore sul campo del codice raggruppamento
            this.w_OBJ.Setfocus()     
          endif
        endif
        * --- Se in maschera non � stata selezionata la pratica si controlla se all'interno dell'iter sia presente almeno una causale con 'pratica obbligatoria'.
        if this.oParentObject.w_RESCHK <> -1 AND empty(this.oParentObject.w_CODPRAT)
          * --- Invece di leggere da PRO_ITER utilizziamo la query GSAGIKT che seleziona
          *     le causali omogenee alla pratica e che sono state visualizzate nella maschera
          * --- Select from ..\AGEN\EXE\QUERY\GSAGIKIT
          do vq_exec with '..\AGEN\EXE\QUERY\GSAGIKIT',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT','',.f.,.t.
          if used('_Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT')
            select _Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT
            locate for 1=1
            do while not(eof())
            this.w_IPCODCAU = IPCODCAU
            if this.oParentObject.w_RESCHK <> -1
              this.FL_PRAT_OBBL = space(1)
              * --- Read from CAUMATTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAUMATTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CACHKOBB"+;
                  " from "+i_cTable+" CAUMATTI where ";
                      +"CACODICE = "+cp_ToStrODBC(this.w_IPCODCAU);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CACHKOBB;
                  from (i_cTable) where;
                      CACODICE = this.w_IPCODCAU;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.FL_PRAT_OBBL = NVL(cp_ToDate(_read_.CACHKOBB),cp_NullValue(_read_.CACHKOBB))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.FL_PRAT_OBBL = "P"
                ah_ErrorMsg("Per il raggruppamento selezionato � necessario inserire la "+IIF(ISALT(),"pratica","commessa")+".")
                this.oParentObject.w_RESCHK = -1
                this.w_OBJ = this.w_ObjCaller.Getctrl("w_CODPRAT")
                if vartype(this.w_OBJ)="O"
                  * --- Viene posizionato il cursore sul campo della pratica
                  this.w_OBJ.Setfocus()     
                endif
              endif
            endif
              select _Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT
              continue
            enddo
            use
          endif
        endif
        * --- Se in maschera � stata selezionata la pratica che ha ente ma non ufficio, si controlla se � giudiziale, nel qual caso si segnala errore
        if IsAlt() AND this.oParentObject.w_RESCHK <> -1 AND !empty(this.oParentObject.w_CODPRAT) AND !EMPTY(this.oParentObject.w_TIPOPRAT) AND EMPTY(this.oParentObject.w_ENTE)
          * --- Pratica con ente ma non ufficio: nel caso in cui sia giudiziale deve segnalare errore
          this.w_FL_GIUD = " "
          * --- Read from PRA_TIPI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRA_TIPI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRA_TIPI_idx,2],.t.,this.PRA_TIPI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TPFLGIUD"+;
              " from "+i_cTable+" PRA_TIPI where ";
                  +"TPCODICE = "+cp_ToStrODBC(this.oParentObject.w_TIPOPRAT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TPFLGIUD;
              from (i_cTable) where;
                  TPCODICE = this.oParentObject.w_TIPOPRAT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FL_GIUD = NVL(cp_ToDate(_read_.TPFLGIUD),cp_NullValue(_read_.TPFLGIUD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NVL(this.w_FL_GIUD,"N") = "G" AND !Ah_yesno( "Attenzione, non � stato specificato l'ufficio giudiziario sulla pratica, la procedura non sar� in grado di calcolare gli onorari civili o penali. Continuare?")
            this.oParentObject.w_RESCHK = -1
          endif
        endif
        * --- Se in maschera non � stata selezionato un nominativo si controlla se all'interno dell'iter sia presente almeno una causale con 'nominativo obbligatorio'.
        if this.oParentObject.w_RESCHK <> -1 AND empty(this.oParentObject.w_CODNOM)
          * --- Invece di leggere da PRO_ITER utilizziamo la query GSAGIKT che seleziona
          *     le causali omogenee alla pratica e che sono state visualizzate nella maschera
          * --- Select from ..\AGEN\EXE\QUERY\GSAGIKIT
          do vq_exec with '..\AGEN\EXE\QUERY\GSAGIKIT',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT','',.f.,.t.
          if used('_Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT')
            select _Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT
            locate for 1=1
            do while not(eof())
            this.w_IPCODCAU = IPCODCAU
            if this.oParentObject.w_RESCHK <> -1
              this.FL_NOM_OBBL = space(1)
              * --- Read from CAUMATTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAUMATTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CACHKNOM"+;
                  " from "+i_cTable+" CAUMATTI where ";
                      +"CACODICE = "+cp_ToStrODBC(this.w_IPCODCAU);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CACHKNOM;
                  from (i_cTable) where;
                      CACODICE = this.w_IPCODCAU;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.FL_NOM_OBBL = NVL(cp_ToDate(_read_.CACHKNOM),cp_NullValue(_read_.CACHKNOM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.FL_NOM_OBBL = "P"
                ah_ErrorMsg("Per il raggruppamento selezionato � necessario inserire il nominativo.")
                this.oParentObject.w_RESCHK = -1
                this.w_OBJ = this.w_ObjCaller.Getctrl("w_CODNOM")
                if vartype(this.w_OBJ)="O"
                  * --- Viene posizionato il cursore sul campo del nominativo
                  this.w_OBJ.Setfocus()     
                endif
              endif
            endif
              select _Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT
              continue
            enddo
            use
          endif
        endif
        * --- Controlla che siano presenti partecipanti e che ne sia attivato almeno uno 
        this.w_ObjCaller.Notifyevent("SalvaImp")     
        if this.oParentObject.w_RESCHK <> -1 and Used("RigheUte")
          Select Max(xchk) as totpar from righeute into cursor CHK_CUR
          if CHK_CUR.TOTPAR = 0
            ah_ErrorMsg("Attenzione, selezionare almeno un partecipante alle attivit�")
            this.oParentObject.w_RESCHK = -1
          endif
          Use in CHK_CUR
        endif
        if this.oParentObject.w_RESCHK <> -1
          Select cprownum,Max(cauatt) as cauatt,sum(xchk) as totpar from righeute group by CPROWNUM Order by CPROWNUM Having totpar=0 into cursor CHK_CUR
          select CHK_CUR
          if reccount() > 0
            if Ah_yesno("Esistono tipi attivit� senza partecipanti, applicare partecipanti del tipo attivit� selezionato ")
              this.w_ObjCaller.Notifyevent("Applica")     
            else
              if TOTPAR = 0
                ah_ErrorMsg("Selezionare almeno un partecipante attivit� per il tipo attivit� %1.","","",Alltrim(CAUATT))
                this.oParentObject.w_RESCHK = -1
              endif
            endif
          else
            if Reccount("RigheUte")=0
              ah_ErrorMsg("Attenzione: non sono presenti partecipanti attivit�.")
              this.oParentObject.w_RESCHK = -1
            endif
          endif
          Use in CHK_CUR
        endif
        * --- Controllo sulla data 
        if this.oParentObject.w_RESCHK <> -1 AND this.oParentObject.w_DATRIF<I_DATSYS
          if !Ah_yesno( "Inserita data antecedente alla data odierna! Continuare?")
            this.oParentObject.w_RESCHK = -1
            this.w_OBJ = this.oParentObject.Getctrl("w_DATRIF")
            if vartype(this.w_OBJ)="O"
              * --- Viene attivata la pag. 1 di GSAG_AAT (Attivit�)
              this.oParentObject.oPgFrm.ActivePage=1
              * --- Viene posizionato il cursore sulla variabile DATFIN di GSAG_AAT (Attivit�)
              this.w_OBJ.Setfocus()     
            endif
          endif
        endif
        if this.oParentObject.w_RESCHK <> -1 AND NOT EMPTY(this.oParentObject.w_PACHKATT) AND this.oParentObject.w_DATRIF>I_DATSYS+this.oParentObject.w_PACHKATT
          if !Ah_yesno( "Inserita data superiore alla data odierna di oltre %1 giorni. Continuare?", "", alltrim(str(this.oParentObject.w_PACHKATT)))
            this.oParentObject.w_RESCHK = -1
            this.w_OBJ = this.oParentObject.Getctrl("w_DATRIF")
            if vartype(this.w_OBJ)="O"
              * --- Viene posizionato il cursore sulla variabile DATRIF di GSAG_KIT (Nuove attivit� collegate)
              this.w_OBJ.Setfocus()     
            endif
          endif
        endif
        * --- Chiede se inviare o meno la mail
        if this.oParentObject.w_RESCHK <> -1
          * --- Invece di leggere da PRO_ITER utilizziamo la query GSAGIKT che seleziona
          *     le causali omogenee alla pratica e che sono state visualizzate nella maschera
          * --- Select from ..\AGEN\EXE\QUERY\GSAGIKIT
          do vq_exec with '..\AGEN\EXE\QUERY\GSAGIKIT',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT','',.f.,.t.
          if used('_Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT')
            select _Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT
            locate for 1=1
            do while not(eof())
            this.w_IPCODCAU = IPCODCAU
            this.w_CAFLPREA = "N"
            * --- Read from CAUMATTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAUMATTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CAFLNOTI"+;
                " from "+i_cTable+" CAUMATTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_IPCODCAU);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CAFLNOTI;
                from (i_cTable) where;
                    CACODICE = this.w_IPCODCAU;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CAFLNOTI = NVL(cp_ToDate(_read_.CAFLNOTI),cp_NullValue(_read_.CAFLNOTI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_ControlloInviaMail = this.w_ControlloInviaMail OR (this.w_CAFLNOTI=="S")
              select _Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT
              continue
            enddo
            use
          endif
          if this.w_ControlloInviaMail
            this.oParentObject.w_InviaMail = Ah_YesNo("Attenzione: nel raggruppamento sono presenti tipi di attivit� che prevedono l'invio di avvisi. Si desidera procedere all'invio?")
          endif
        endif
      case this.Param = "S"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Param = "V"
        * --- ACCEDE ALL'ATTIVITA' IN VARIAZIONE
        * --- Istanzio l'anagrafica
        this.w_OBJECT = GSAG_AAT()
        * --- Controllo se ha passato il test di accesso
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Carico il record selezionato
        this.w_OBJECT.ECPFILTER()     
        this.w_OBJECT.w_ATSERIAL = this.oParentObject.w_ATSERIAL
        this.w_OBJECT.w_bNoCheckRis = .t.
        this.w_OBJECT.ECPSAVE()     
        this.w_OBJECT.w_bNoCheckRis = .f.
        * --- Assegno alla var.w_MaskEleAtt l'oggetto rappresentato da GSAG_KAT
        this.w_OBJECT.w_MaskEleAtt = this.oParentObject
        * --- Vado in modifica
        this.w_OBJECT.ECPEDIT()     
        this.w_OBJECT = null
      case this.Param = "E"
        * --- ELIMINAZIONE ATTIVITA' E DATI ASSOCIATI
        if Ah_yesno("Confermi cancellazione?")
          this.NUMSERIAL = this.oParentObject.w_ATSERIAL
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.Param = "T"
        * --- ELIMINAZIONE CUMULATIVA
        * --- Riesegue la query dello zoom per eliminare dal cursore eventuali attivit� cancellate tramite Gestione Attivit�
        this.oParentObject.w_AGKAT_ZOOM.ReQuery()     
        this.CURSORE = this.oParentObject.w_AGKAT_ZOOM.cCursor
        select (this.CURSORE)
        if reccount() > 0
          if Ah_yesno("Confermi cancellazione di tutte le attivit� generate in precedenza?")
            SCAN
            this.NUMSERIAL = ATSERIAL
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            ENDSCAN
          endif
        endif
      case this.Param = "A"
        if Used("RigheUte")
          this.w_OK = GSAG_BAI(.Null.,this.oParentObject.w_CODITER,this.oParentObject.w_CODPRAT,this.oParentObject.w_DATRIF,this.oParentObject.w_CACHKINI,this.oParentObject.w_CACHKFOR,this.oParentObject.w_VALPRA,"RigheUte",this.oParentObject.w_CC_CONTO,this.oParentObject.w_CENRIC,this.oParentObject.w_COMRIC,this.oParentObject.w_ATTRIC,this.oParentObject.w_ATTCOS,this.oParentObject.w_CODNOM,this.oParentObject.w_ORAINI,this.oParentObject.w_MININI,this.oParentObject.w_CUR_EVE,"","",this.oParentObject.w_ONLYFIRST,this.oParentObject.w_InviaMail)
          this.w_ObjCaller.BlankRec()     
          this.w_ObjCaller.Notifyevent("Azzera")     
        endif
      case this.Param = "R"
        * --- NOTIFICA EVENTO 'RICERCA' IN ELENCO ATTIVITA'
        if TYPE("this.oParentObject.oParentObject.oParentObject.oParentObject") = "O"
          this.objAG_KRA = this.oParentObject.oParentObject.oParentObject.oParentObject
          if upper(this.objAG_KRA.class)="TGSAG_KRA"
            * --- Notifica l'evento "Ricerca" alla maschera GSAG_KRA (Elenco Attivit�)
            this.objAG_KRA.NotifyEvent("Ricerca")     
          endif
        endif
      case this.Param = "G"
        * --- Determina giorno settimana in Attivit� collegate generate
        if USED(this.oParentObject.w_AGKAT_ZOOM.cCursor)
          if RECCOUNT(this.oParentObject.w_AGKAT_ZOOM.cCursor)>0
            SELECT ( this.oParentObject.w_AGKAT_ZOOM.cCursor )
            REPLACE ALL GIOINI WITH LEFT(g_GIORNO[DOW(ATDATINI)],3), GIOFIN WITH LEFT(g_GIORNO[DOW(ATDATFIN)],3)
            SELECT ( this.oParentObject.w_AGKAT_ZOOM.cCursor )
            go top
            this.oParentObject.w_AGKAT_ZOOM.Refresh()     
          endif
        endif
    endcase
    do case
      case this.Param="Q" and !this.oParentObject.w_NOBFQ
        * --- Before query
        this.w_PADRE = This.oparentobject
        ND=this.w_PADRE.w_AGKIT_ZOOM.cCursor
        * --- Salva in RigheUte le Variazioni Riportate sul Dettaglio partecipanti
        if USED(this.w_PADRE.w_AGKIT_ZOOM.cCursor) AND USED("RigheUte") 
          Select ( ND ) 
 Go Top
          this.w_CAUATT = this.w_PADRE.w_ITER_ZOOM.GetVar("IPCodcau")
          this.w_MODAPP = this.w_PADRE.w_ITER_ZOOM.GetVar("MODAPP")
          this.w_IPCODICE = this.w_PADRE.w_ITER_ZOOM.GetVar("IPCODICE")
          if Not Empty(this.w_CAUATT)
            this.w_CPROWNUM = this.w_PADRE.w_AGKIT_ZOOM.GetVar("CPROWNUM")
            SELECT XCHK,ORDINE,DPCODICE,DPCOGNOM,DPNOME,RIFLATTI,CPROWORD,; 
 DPTIPRIS,DESCRIZIONE,CAUATT,CPROWNUM,FLGPRE,DPGRUPRE FROM RigheUte WHERE CPROWNUM<>this.w_CPROWNUM ; 
 UNION ; 
 SELECT XCHK,ORDINE,DPCODICE,DPCOGNOM,DPNOME,RIFLATTI,CPROWORD,; 
 DPTIPRIS,DESCRIZIONE,CAUATT,CPROWNUM,FLGPRE,DPGRUPRE FROM (ND) ; 
 INTO CURSOR RigheUte
          endif
          SELECT (ND)
        endif
      case this.Param="P"
        if Used("Righeute")
          this.w_PADRE = This.oparentobject
           
 Select * from Righeute into cursor Tmp_Righe 
 =Wrcursor("Tmp_Righe") 
 ND=this.w_PADRE.w_AGKIT_ZOOM.cCursor 
 Select (ND) 
 Go top 
 Scan
          this.w_XCHK = XCHK
          this.w_DPCODICE = DPCODICE
           
 Select Tmp_Righe 
 REPLACE XCHK WITH this.w_XCHK For Tmp_Righe.DPCODICE=this.w_DPCODICE and IIF(ISALT(), .T. , XCHK=0 ) 
 Select (ND) 
 EndScan 
 Select * from Tmp_Righe into cursor Righeute 
 Use in Tmp_Righe
          this.w_PADRE = Null
        endif
      case this.Param="K"
        * --- Cambio Iter
        this.w_PADRE = This.oparentobject
        if iSALT()
          this.oParentObject.w_RICALCOLA = 0
          this.oParentObject.w_visallpart = "N"
        endif
        if USED(this.w_PADRE.w_ITER_ZOOM.cCursor) 
          * --- Create temporary table TMPCONTR
          i_nIdx=cp_AddTableDef('TMPCONTR') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('..\AGEN\EXE\QUERY\GSAGZBIT',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPCONTR_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
           
 Select IPCODICE,IPCODCAU,RIGAPAD,MODAPP from (this.w_PADRE.w_ITER_ZOOM.cCursor) into cursor TMP_CAU
          Select TMP_CAU 
 Go top 
 Scan
          this.w_CAUATT = IPCODCAU
          this.w_ROWNUM = RIGAPAD
          this.w_MODAPP = MODAPP
          this.w_IPCODICE = IPCODICE
          * --- Create temporary table TMPCONTR
          if cp_ExistTableDef('TMPCONTR')
             * add to existing table
            i_nIdx=cp_AddTableDef('TMPCONTR',.t.) && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('..\AGEN\EXE\QUERY\GSAGKKIT',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.t.)
            if this.TMPCONTR_idx=0
              this.TMPCONTR_idx=i_nIdx
            endif
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          else
            i_nIdx=cp_AddTableDef('TMPCONTR',.t.) && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('..\AGEN\EXE\QUERY\GSAGKKIT',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPCONTR_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
          Endscan
          * --- Aggiorno spunte partecipanti inizializzati flaggati
          * --- Write into TMPCONTR
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPCONTR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPCONTR_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPCONTR_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"XCHK ="+cp_NullLink(cp_ToStrODBC(1),'TMPCONTR','XCHK');
                +i_ccchkf ;
            +" where ";
                +"FLGPRE = "+cp_ToStrODBC("S");
                   )
          else
            update (i_cTable) set;
                XCHK = 1;
                &i_ccchkf. ;
             where;
                FLGPRE = "S";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          Use in TMP_CAU
          Vq_exec("..\Agen\exe\query\gsagtbit.vqr",This,"righeute")
          * --- Non ci sono partecipanti con attivit� nella pratica, quindi deve ricalcolare la query segnalando che deve estrarre tutti i dipendenti
          this.w_PADRE.w_MODAPP = this.w_PADRE.w_ITER_ZOOM.GetVar("MODAPP")
          this.w_PADRE.w_CAUATT = this.w_PADRE.w_ITER_ZOOM.GetVar("IPCODCAU")
          this.oParentObject.w_RICALCOLA = 0
          this.oParentObject.w_NOSAVE = Isalt()
          this.oParentObject.w_NOBFQ = .t.
          this.w_ObjCaller.NotifyEvent("Ricalcola")     
          * --- Drop temporary table TMPCONTR
          i_nIdx=cp_GetTableDefIdx('TMPCONTR')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPCONTR')
          endif
          this.oParentObject.w_NOSAVE = .f.
          this.oParentObject.w_NOBFQ = .f.
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ELIMINAZIONE ATTIVITA' E RELATIVI DATI ASSOCIATI
    * --- Try
    local bErr_039F44F8
    bErr_039F44F8=bTrsErr
    this.Try_039F44F8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      AH_ERRORMSG("Impossibile eliminare l'attivit�",48)
    endif
    bTrsErr=bTrsErr or bErr_039F44F8
    * --- End
    if this.Param = "E"
      * --- Riesegue la query dello zoom
      this.oParentObject.w_AGKAT_ZOOM.ReQuery()     
    endif
  endproc
  proc Try_039F44F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Elimina partecipanti attivit�
    * --- Delete from OFF_PART
    i_nConn=i_TableProp[this.OFF_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PASERIAL = "+cp_ToStrODBC(this.NUMSERIAL);
             )
    else
      delete from (i_cTable) where;
            PASERIAL = this.NUMSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Elimina dettaglio (prestazioni) attivit�
    * --- Delete from OFFDATTI
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DASERIAL = "+cp_ToStrODBC(this.NUMSERIAL);
             )
    else
      delete from (i_cTable) where;
            DASERIAL = this.NUMSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Elimina singola attivit� (padre) : va eliminato per ultimo !!
    * --- Delete from COM_ATTI
    i_nConn=i_TableProp[this.COM_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COM_ATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CASERIAL = "+cp_ToStrODBC(this.NUMSERIAL);
             )
    else
      delete from (i_cTable) where;
            CASERIAL = this.NUMSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ATT_DCOL
    i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ADSERIAL = "+cp_ToStrODBC(this.NUMSERIAL);
             )
    else
      delete from (i_cTable) where;
            ADSERIAL = this.NUMSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from OFF_ATTI
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.NUMSERIAL);
             )
    else
      delete from (i_cTable) where;
            ATSERIAL = this.NUMSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SELEZIONA RIGHE PARTECIPANTI
    this.w_PADRE = This.oparentobject
    this.w_MODAPP = this.w_PADRE.w_ITER_ZOOM.GetVar("MODAPP")
    this.CURSORE = this.oParentObject.w_AGKIT_ZOOM.cCursor
    if !empty(this.oParentObject.w_CODPRAT)
      SELECT (this.CURSORE)
      if RECCOUNT() = 0 AND this.oParentObject.w_RICALCOLA=0 and this.w_MODAPP<>"D"
        * --- Non ci sono partecipanti con attivit� nella pratica, quindi deve ricalcolare la query segnalando che deve estrarre tutti i dipendenti
        this.oParentObject.w_RICALCOLA = 1
        this.w_ObjCaller.NotifyEvent("Ricalcola")     
      else
        UPDATE (this.CURSORE) SET XCHK = 1 WHERE (RIFLATTI="S" or FLGPRE="S")
        this.oParentObject.w_RICALCOLA = 0
      endif
    endif
    * --- Evento After Query sul Dettaglio
    * --- Riporta sul Dettaglio le Variazioni precedentemente 
    *     definite e salvate in RigheUte
    this.w_PADRE = This.oparentobject
    ND=this.w_PADRE.w_AGKIT_ZOOM.cCursor
    if USED(this.w_PADRE.w_AGKIT_ZOOM.cCursor) AND USED("RigheUte") and ! this.oParentObject.w_NOSAVE
       
 SELECT (ND) 
 SCAN
       
 SELECT * FROM RigheUte WHERE RigheUte.CPROWNUM = &ND..CPROWNUM AND RigheUte.DPCODICE = &ND..DPCODICE INTO CURSOR APPCUR
      SELECT (ND)
      if RECCOUNT("APPCUR")<>0
         
 REPLACE XCHK WITH APPCUR.XCHK
      endif
      * --- Controllo Riga evasa a valore 0 (senza Import)
      * --- Controllo Riga Raggruppata in Importazione
      SELECT (ND)
      ENDSCAN
      if USED("APPCUR")
        SELECT APPCUR
        USE
        SELECT (ND)
      endif
    endif
  endproc


  proc Init(oParentObject,Param)
    this.Param=Param
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,20)]
    this.cWorkTables[1]='CAUMATTI'
    this.cWorkTables[2]='OFF_ATTI'
    this.cWorkTables[3]='OFFDATTI'
    this.cWorkTables[4]='AHEUSRCO'
    this.cWorkTables[5]='OFF_PART'
    this.cWorkTables[6]='CAU_ATTI'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='DIPENDEN'
    this.cWorkTables[9]='PRA_ENTI'
    this.cWorkTables[10]='PAR_AGEN'
    this.cWorkTables[11]='KEY_ARTI'
    this.cWorkTables[12]='UNIMIS'
    this.cWorkTables[13]='TIP_DOCU'
    this.cWorkTables[14]='OFF_NOMI'
    this.cWorkTables[15]='LISTINI'
    this.cWorkTables[16]='COM_ATTI'
    this.cWorkTables[17]='VALUTE'
    this.cWorkTables[18]='ATT_DCOL'
    this.cWorkTables[19]='PRA_TIPI'
    this.cWorkTables[20]='*TMPCONTR'
    return(this.OpenAllTables(20))

  proc CloseCursors()
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAGIKIT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Param"
endproc
