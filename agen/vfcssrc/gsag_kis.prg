* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kis                                                        *
*              Esporta profilo                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-09                                                      *
* Last revis.: 2014-11-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kis",oParentObject))

* --- Class definition
define class tgsag_kis as StdForm
  Top    = 2
  Left   = 9

  * --- Standard Properties
  Width  = 786
  Height = 528+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-19"
  HelpContextID=267003753
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=88

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  PRA_ENTI_IDX = 0
  PRA_UFFI_IDX = 0
  CPUSERS_IDX = 0
  OFF_NOMI_IDX = 0
  CAT_SOGG_IDX = 0
  OFFTIPAT_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  CENCOST_IDX = 0
  DES_DIVE_IDX = 0
  GEST_ATTI_IDX = 0
  TIP_RISE_IDX = 0
  PAR_AGEN_IDX = 0
  CAUMATTI_IDX = 0
  CON_TRAS_IDX = 0
  MOD_ELEM_IDX = 0
  CONTI_IDX = 0
  PRO_EXPO_IDX = 0
  cpusers_IDX = 0
  cPrg = "gsag_kis"
  cComment = "Esporta profilo"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_LCADESCRI = space(254)
  w_CACODICE = space(20)
  w_CADESCRI = space(254)
  w_LAYOUT = .F.
  w_READAZI = space(5)
  o_READAZI = space(5)
  w_FLTPART = space(5)
  o_FLTPART = space(5)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_OREFIN = space(2)
  o_OREFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_PATIPRIS = space(1)
  o_PATIPRIS = space(1)
  w_FLESGR = space(1)
  w_GRUPART = space(5)
  w_PRIORITA = 0
  w_STATO = space(1)
  w_RISERVE = space(1)
  w_DATA_INI = ctot('')
  w_DATA_FIN = ctot('')
  w_COD_UTE = space(5)
  w_COGNPART = space(40)
  w_NOMEPART = space(40)
  w_CODPART = space(5)
  o_CODPART = space(5)
  w_CODNOM = space(15)
  o_CODNOM = space(15)
  w_CODCAT = space(10)
  w_TIPNOM = space(1)
  w_CODCLI = space(15)
  w_ATTSTU = space(1)
  w_DAFARE = space(1)
  w_SESSTEL = space(1)
  w_MEMO = space(1)
  w_ATTGENER = space(1)
  w_ASSENZE = space(1)
  w_UDIENZE = space(1)
  w_DAINSPRE = space(1)
  w_ATTFUOSTU = space(1)
  w_DISPONIB = 0
  w_FiltPerson = space(60)
  w_CODTIPOL = space(5)
  w_NOTE = space(0)
  w_GIUDICE = space(15)
  w_ATLOCALI = space(30)
  w_TIPORIS = space(1)
  w_ATSERIAL = space(20)
  w_ATSTATUS = space(20)
  w_VARLOG = .F.
  w_OBTEST = ctod('  /  /  ')
  w_COMODO = space(30)
  w_DESNOM = space(40)
  w_DESCAT = space(30)
  w_TIPCAT = space(1)
  w_OFDATDOC = ctod('  /  /  ')
  w_EDITCODPRA = .F.
  w_EDITCODNOM = .F.
  w_DESTIPOL = space(50)
  w_DESCRPART = space(40)
  w_DPTIPRIS = space(1)
  w_DESNOMGIU = space(60)
  w_ATCODNOM = space(15)
  w_TGIUDICE = space(15)
  w_DATOBSONOM = ctod('  /  /  ')
  w_CAPNOM = space(9)
  w_INDNOM = space(35)
  w_COMODOGIU = space(60)
  w_OB_TEST = ctod('  /  /  ')
  w_TIPOGRUP = space(1)
  w_FLVISI = space(1)
  w_DatiAttivita = space(10)
  w_SERIAL = space(20)
  w_SERPRO = space(10)
  o_SERPRO = space(10)
  w_FILEXP = space(254)
  w_CODCOM = space(15)
  o_CODCOM = space(15)
  w_TIPANA = space(1)
  w_DESPRA = space(75)
  w_CODPRA = space(15)
  w_DESPRA = space(75)
  w_TIPDAT = space(1)
  w_PATHEX = space(60)
  w_PR__TIPO = space(1)
  w_PRDESCRI = space(50)
  w_PRFILIMP = space(60)
  w_PRPATHIM = space(254)
  w_ESCIGN = space(10)
  w_DENOM_PART = space(48)
  w_LNOTE = space(0)
  w_LATLOCALI = space(30)
  w_LFiltPerson = space(60)
  w_AGKRA_ZOOM = .NULL.
  w_BTNQRY = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_kis
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kisPag1","gsag_kis",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsag_kisPag2","gsag_kis",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettagli")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsag_kis
    if !isalt()
      local oAGKRA_ZOOM
      oAGKRA_ZOOM=this.Parent.GetCtrl("AGKRA_ZOOM")
      oAGKRA_ZOOM.cZoomfile="GSAGRKIS"
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGKRA_ZOOM = this.oPgFrm.Pages(1).oPag.AGKRA_ZOOM
    this.w_BTNQRY = this.oPgFrm.Pages(1).oPag.BTNQRY
    DoDefault()
    proc Destroy()
      this.w_AGKRA_ZOOM = .NULL.
      this.w_BTNQRY = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[21]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='PRA_ENTI'
    this.cWorkTables[4]='PRA_UFFI'
    this.cWorkTables[5]='CPUSERS'
    this.cWorkTables[6]='OFF_NOMI'
    this.cWorkTables[7]='CAT_SOGG'
    this.cWorkTables[8]='OFFTIPAT'
    this.cWorkTables[9]='IMP_MAST'
    this.cWorkTables[10]='IMP_DETT'
    this.cWorkTables[11]='CENCOST'
    this.cWorkTables[12]='DES_DIVE'
    this.cWorkTables[13]='GEST_ATTI'
    this.cWorkTables[14]='TIP_RISE'
    this.cWorkTables[15]='PAR_AGEN'
    this.cWorkTables[16]='CAUMATTI'
    this.cWorkTables[17]='CON_TRAS'
    this.cWorkTables[18]='MOD_ELEM'
    this.cWorkTables[19]='CONTI'
    this.cWorkTables[20]='PRO_EXPO'
    this.cWorkTables[21]='cpusers'
    return(this.OpenAllTables(21))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LCADESCRI=space(254)
      .w_CACODICE=space(20)
      .w_CADESCRI=space(254)
      .w_LAYOUT=.f.
      .w_READAZI=space(5)
      .w_FLTPART=space(5)
      .w_DATINI=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_DATFIN=ctod("  /  /  ")
      .w_OREFIN=space(2)
      .w_MINFIN=space(2)
      .w_PATIPRIS=space(1)
      .w_FLESGR=space(1)
      .w_GRUPART=space(5)
      .w_PRIORITA=0
      .w_STATO=space(1)
      .w_RISERVE=space(1)
      .w_DATA_INI=ctot("")
      .w_DATA_FIN=ctot("")
      .w_COD_UTE=space(5)
      .w_COGNPART=space(40)
      .w_NOMEPART=space(40)
      .w_CODPART=space(5)
      .w_CODNOM=space(15)
      .w_CODCAT=space(10)
      .w_TIPNOM=space(1)
      .w_CODCLI=space(15)
      .w_ATTSTU=space(1)
      .w_DAFARE=space(1)
      .w_SESSTEL=space(1)
      .w_MEMO=space(1)
      .w_ATTGENER=space(1)
      .w_ASSENZE=space(1)
      .w_UDIENZE=space(1)
      .w_DAINSPRE=space(1)
      .w_ATTFUOSTU=space(1)
      .w_DISPONIB=0
      .w_FiltPerson=space(60)
      .w_CODTIPOL=space(5)
      .w_NOTE=space(0)
      .w_GIUDICE=space(15)
      .w_ATLOCALI=space(30)
      .w_TIPORIS=space(1)
      .w_ATSERIAL=space(20)
      .w_ATSTATUS=space(20)
      .w_VARLOG=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_COMODO=space(30)
      .w_DESNOM=space(40)
      .w_DESCAT=space(30)
      .w_TIPCAT=space(1)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_EDITCODPRA=.f.
      .w_EDITCODNOM=.f.
      .w_DESTIPOL=space(50)
      .w_DESCRPART=space(40)
      .w_DPTIPRIS=space(1)
      .w_DESNOMGIU=space(60)
      .w_ATCODNOM=space(15)
      .w_TGIUDICE=space(15)
      .w_DATOBSONOM=ctod("  /  /  ")
      .w_CAPNOM=space(9)
      .w_INDNOM=space(35)
      .w_COMODOGIU=space(60)
      .w_OB_TEST=ctod("  /  /  ")
      .w_TIPOGRUP=space(1)
      .w_FLVISI=space(1)
      .w_DatiAttivita=space(10)
      .w_SERIAL=space(20)
      .w_SERPRO=space(10)
      .w_FILEXP=space(254)
      .w_CODCOM=space(15)
      .w_TIPANA=space(1)
      .w_DESPRA=space(75)
      .w_CODPRA=space(15)
      .w_DESPRA=space(75)
      .w_TIPDAT=space(1)
      .w_PATHEX=space(60)
      .w_PR__TIPO=space(1)
      .w_PRDESCRI=space(50)
      .w_PRFILIMP=space(60)
      .w_PRPATHIM=space(254)
      .w_ESCIGN=space(10)
      .w_DENOM_PART=space(48)
      .w_LNOTE=space(0)
      .w_LATLOCALI=space(30)
      .w_LFiltPerson=space(60)
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_CACODICE))
          .link_2_3('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_LAYOUT = Not Empty(  IIF(VARTYPE(this.oParentObject)='C',this.oParentObject,'') )
        .w_READAZI = i_CODAZI
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_READAZI))
          .link_1_3('Full')
        endif
        .w_FLTPART = readdipend( i_CodUte, 'C')
          .DoRTCalc(7,7,.f.)
        .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
          .DoRTCalc(10,10,.f.)
        .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (empty(.w_DATFIN) AND .w_OREFIN="23" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .w_PATIPRIS = 'P'
        .w_FLESGR = 'N'
        .w_GRUPART = Space(5)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_GRUPART))
          .link_1_22('Full')
        endif
        .w_PRIORITA = 0
        .w_STATO = IIF(NOT ISALT(), 'N', InitStato())
        .w_RISERVE = 'L'
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
        .DoRTCalc(22,24,.f.)
        if not(empty(.w_CODPART))
          .link_1_38('Full')
        endif
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_CODNOM))
          .link_2_5('Full')
        endif
        .w_CODCAT = ''
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_CODCAT))
          .link_2_6('Full')
        endif
          .DoRTCalc(27,36,.f.)
        .w_ATTFUOSTU = ' '
        .w_DISPONIB = 0
        .DoRTCalc(39,40,.f.)
        if not(empty(.w_CODTIPOL))
          .link_2_20('Full')
        endif
        .DoRTCalc(41,42,.f.)
        if not(empty(.w_GIUDICE))
          .link_2_22('Full')
        endif
          .DoRTCalc(43,43,.f.)
        .w_TIPORIS = SPACE(1)
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_TIPORIS))
          .link_2_25('Full')
        endif
        .w_ATSERIAL = .w_AGKRA_ZOOM.GetVar('ATSERIAL')
        .w_ATSTATUS = .w_AGKRA_ZOOM.GetVar('ATSTATUS')
          .DoRTCalc(47,47,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(49,52,.f.)
        .w_OFDATDOC = i_datsys
        .w_EDITCODPRA = .T.
        .w_EDITCODNOM = .T.
          .DoRTCalc(56,59,.f.)
        .w_ATCODNOM = .w_CODNOM
      .oPgFrm.Page1.oPag.AGKRA_ZOOM.Calculate()
          .DoRTCalc(61,65,.f.)
        .w_OB_TEST = i_INIDAT
      .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
          .DoRTCalc(67,69,.f.)
        .w_SERIAL = .w_AGKRA_ZOOM.GetVar('ATSERIAL')
        .w_SERPRO = iif(Vartype(oparentobject)='C',oparentobject,'')
        .DoRTCalc(71,71,.f.)
        if not(empty(.w_SERPRO))
          .link_1_58('Full')
        endif
        .DoRTCalc(72,73,.f.)
        if not(empty(.w_CODCOM))
          .link_2_45('Full')
        endif
        .w_TIPANA = 'C'
          .DoRTCalc(75,75,.f.)
        .w_CODPRA = .w_CODCOM
        .DoRTCalc(76,76,.f.)
        if not(empty(.w_CODPRA))
          .link_2_50('Full')
        endif
          .DoRTCalc(77,83,.f.)
        .w_ESCIGN = 'N'
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
      .oPgFrm.Page1.oPag.BTNQRY.Calculate()
    endwith
    this.DoRTCalc(86,88,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_2_3('Full')
        .DoRTCalc(3,4,.t.)
        if .o_READAZI<>.w_READAZI
            .w_READAZI = i_CODAZI
          .link_1_3('Full')
        endif
        if .o_FLTPART<>.w_FLTPART
            .w_FLTPART = readdipend( i_CodUte, 'C')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_OREINI<>.w_OREINI.or. .o_SERPRO<>.w_SERPRO
            .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        endif
        if .o_OREINI<>.w_OREINI
          .Calculate_MVFLEXPGGM()
        endif
        if .o_MININI<>.w_MININI.or. .o_SERPRO<>.w_SERPRO
            .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        endif
        if .o_MININI<>.w_MININI
          .Calculate_SUGRFFLBCH()
        endif
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(10,10,.t.)
        if .o_OREFIN<>.w_OREFIN.or. .o_DATFIN<>.w_DATFIN.or. .o_SERPRO<>.w_SERPRO
            .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        endif
        if .o_OREFIN<>.w_OREFIN
          .Calculate_FPJEAUJXWE()
        endif
        if .o_MINFIN<>.w_MINFIN.or. .o_DATFIN<>.w_DATFIN.or. .o_SERPRO<>.w_SERPRO
            .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (empty(.w_DATFIN) AND .w_OREFIN="23" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        endif
        if .o_MINFIN<>.w_MINFIN
          .Calculate_KTQUMLVLEF()
        endif
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .DoRTCalc(13,13,.t.)
        if .o_CODPART<>.w_CODPART
            .w_FLESGR = 'N'
        endif
        if .o_CODPART<>.w_CODPART
            .w_GRUPART = Space(5)
          .link_1_22('Full')
        endif
        .DoRTCalc(16,18,.t.)
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN<>.w_DATFIN.or. .o_OREFIN<>.w_OREFIN.or. .o_MINFIN<>.w_MINFIN
            .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_CODPART<>.w_CODPART
            .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
        endif
        .DoRTCalc(22,23,.t.)
        if .o_PATIPRIS<>.w_PATIPRIS.or. .o_SERPRO<>.w_SERPRO
          .link_1_38('Full')
        endif
          .link_2_5('Full')
        if .o_CODNOM<>.w_CODNOM
            .w_CODCAT = ''
          .link_2_6('Full')
        endif
        .DoRTCalc(27,39,.t.)
          .link_2_20('Full')
        .DoRTCalc(41,41,.t.)
          .link_2_22('Full')
        .DoRTCalc(43,43,.t.)
          .link_2_25('Full')
            .w_ATSERIAL = .w_AGKRA_ZOOM.GetVar('ATSERIAL')
            .w_ATSTATUS = .w_AGKRA_ZOOM.GetVar('ATSTATUS')
        .DoRTCalc(47,59,.t.)
            .w_ATCODNOM = .w_CODNOM
        .oPgFrm.Page1.oPag.AGKRA_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .DoRTCalc(61,69,.t.)
            .w_SERIAL = .w_AGKRA_ZOOM.GetVar('ATSERIAL')
        if .o_READAZI<>.w_READAZI
            .w_SERPRO = iif(Vartype(oparentobject)='C',oparentobject,'')
          .link_1_58('Full')
        endif
        .DoRTCalc(72,72,.t.)
          .link_2_45('Full')
        .DoRTCalc(74,75,.t.)
        if .o_CODCOM<>.w_CODCOM
            .w_CODPRA = .w_CODCOM
          .link_2_50('Full')
        endif
        if .o_SERPRO<>.w_SERPRO
          .Calculate_NHGZPPOKMU()
        endif
        .DoRTCalc(77,84,.t.)
        if .o_CODPART<>.w_CODPART.or. .o_SERPRO<>.w_SERPRO
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(86,88,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.AGKRA_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
    endwith
  return

  proc Calculate_NRTSRHMIVM()
    with this
          * --- Inizializza date
          gsag_ba3(this;
              ,'EXPO';
              ,.w_SERPRO;
             )
    endwith
  endproc
  proc Calculate_MVFLEXPGGM()
    with this
          * --- Resetto w_oreini
          .w_OREINI = .ZeroFill(.w_OREINI)
    endwith
  endproc
  proc Calculate_SUGRFFLBCH()
    with this
          * --- Resetto w_minini
          .w_MININI = .ZeroFill(.w_MININI)
    endwith
  endproc
  proc Calculate_FPJEAUJXWE()
    with this
          * --- Resetto w_orefin
          .w_OREFIN = .ZeroFill(.w_OREFIN)
    endwith
  endproc
  proc Calculate_KTQUMLVLEF()
    with this
          * --- Resetto w_minfin
          .w_MINFIN = .ZeroFill(.w_MINFIN)
    endwith
  endproc
  proc Calculate_DOBRBAQOOH()
    with this
          * --- Attiva la prima pagina
          .oPgFrm.ActivePage = 1
    endwith
  endproc
  proc Calculate_EPTWHVVJZD()
    with this
          * --- IncremGiorni
          .w_DATINI = IIF(NOT EMPTY(.w_DATINI), .w_DATINI + 1, .w_DATINI)
          .w_DATFIN = IIF(NOT EMPTY(.w_DATFIN), .w_DATFIN + 1, .w_DATFIN)
          .w_DATA_INI = IIF(NOT EMPTY(.w_DATINI), cp_CharToDatetime(DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00'), .w_DATA_INI)
          .w_DATA_FIN = IIF(NOT EMPTY(.w_DATFIN), cp_CharToDatetime(DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00'), .w_DATA_FIN)
    endwith
  endproc
  proc Calculate_PFKYJKKLTX()
    with this
          * --- DecremGiorni
          .w_DATINI = IIF(NOT EMPTY(.w_DATINI), .w_DATINI - 1, .w_DATINI)
          .w_DATFIN = IIF(NOT EMPTY(.w_DATFIN), .w_DATFIN - 1, .w_DATFIN)
          .w_DATA_INI = IIF(NOT EMPTY(.w_DATINI), cp_CharToDatetime(DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00'), .w_DATA_INI)
          .w_DATA_FIN = IIF(NOT EMPTY(.w_DATFIN), cp_CharToDatetime(DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00'), .w_DATA_FIN)
    endwith
  endproc
  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .w_CODPART = .w_CODPART
          .link_1_38('Full')
          .cComment = IIF(EMPTY(.w_CodPart), IIF(.w_LAYOUT, AH_MSGFORMAT("Esporta profilo"),AH_MSGFORMAT("Esporta profilo")),IIF(.w_LAYOUT, AH_MSGFORMAT("Esporta profilo %1",alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART)),AH_MSGFORMAT("Esporta profilo di %1",alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART))))
          .Caption = .cComment
          .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOREINI_1_7.enabled = this.oPgFrm.Page1.oPag.oOREINI_1_7.mCond()
    this.oPgFrm.Page1.oPag.oMININI_1_9.enabled = this.oPgFrm.Page1.oPag.oMININI_1_9.mCond()
    this.oPgFrm.Page1.oPag.oOREFIN_1_14.enabled = this.oPgFrm.Page1.oPag.oOREFIN_1_14.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_16.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_16.mCond()
    this.oPgFrm.Page1.oPag.oFILEXP_1_59.enabled = this.oPgFrm.Page1.oPag.oFILEXP_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oSTATO_1_24.visible=!this.oPgFrm.Page1.oPag.oSTATO_1_24.mHide()
    this.oPgFrm.Page2.oPag.oUDIENZE_2_15.visible=!this.oPgFrm.Page2.oPag.oUDIENZE_2_15.mHide()
    this.oPgFrm.Page2.oPag.oCODCOM_2_45.visible=!this.oPgFrm.Page2.oPag.oCODCOM_2_45.mHide()
    this.oPgFrm.Page2.oPag.oTIPANA_2_46.visible=!this.oPgFrm.Page2.oPag.oTIPANA_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_47.visible=!this.oPgFrm.Page2.oPag.oStr_2_47.mHide()
    this.oPgFrm.Page2.oPag.oDESPRA_2_48.visible=!this.oPgFrm.Page2.oPag.oDESPRA_2_48.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_49.visible=!this.oPgFrm.Page2.oPag.oStr_2_49.mHide()
    this.oPgFrm.Page2.oPag.oCODPRA_2_50.visible=!this.oPgFrm.Page2.oPag.oCODPRA_2_50.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_51.visible=!this.oPgFrm.Page2.oPag.oStr_2_51.mHide()
    this.oPgFrm.Page2.oPag.oDESPRA_2_52.visible=!this.oPgFrm.Page2.oPag.oDESPRA_2_52.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Aggdat")
          .Calculate_NRTSRHMIVM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
        if lower(cEvent)==lower("Ricerca")
          .Calculate_DOBRBAQOOH()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.AGKRA_ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
        if lower(cEvent)==lower("IncremGiorni")
          .Calculate_EPTWHVVJZD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DecremGiorni")
          .Calculate_PFKYJKKLTX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.BTNQRY.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kis
    if (Cevent='w_SERPRO Changed' OR Cevent='Blank' ) and Not Empty(This.w_SERPRO)
       if This.w_TIPDAT='D'
         This.Notifyevent('Aggdat')
       endif
       This.w_CADESCRI=Alltrim(This.w_LCADESCRI)
       This.w_NOTE=Alltrim(This.w_LNOTE)
       This.w_ATLOCALI=Alltrim(This.w_LATLOCALI)
       This.w_FiltPerson=Alltrim(This.w_LFiltPerson)
       This.Notifyevent('Ricerca')
    Endif
    If cevent='Ricerca'
       * seleziona tutte le attivit�
       update (this.w_AGKRA_ZOOM.cCursor) set XCHK = 1
       select (this.w_AGKRA_ZOOM.cCursor)
       go top
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODICE
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODICE)
            select CACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODICE = NVL(_Link_.CACODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CACODICE = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLVISI,PAFLAPP1,PAFLATG1,PAFLUDI1,PAFLSES1,PAFLASS1,PAFLINP1,PAFLNOT1,PAFLDAF1";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PAFLVISI,PAFLAPP1,PAFLATG1,PAFLUDI1,PAFLSES1,PAFLASS1,PAFLINP1,PAFLNOT1,PAFLDAF1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_FLVISI = NVL(_Link_.PAFLVISI,space(1))
      this.w_ATTSTU = NVL(_Link_.PAFLAPP1,space(1))
      this.w_ATTGENER = NVL(_Link_.PAFLATG1,space(1))
      this.w_UDIENZE = NVL(_Link_.PAFLUDI1,space(1))
      this.w_SESSTEL = NVL(_Link_.PAFLSES1,space(1))
      this.w_ASSENZE = NVL(_Link_.PAFLASS1,space(1))
      this.w_DAINSPRE = NVL(_Link_.PAFLINP1,space(1))
      this.w_MEMO = NVL(_Link_.PAFLNOT1,space(1))
      this.w_DAFARE = NVL(_Link_.PAFLDAF1,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_FLVISI = space(1)
      this.w_ATTSTU = space(1)
      this.w_ATTGENER = space(1)
      this.w_UDIENZE = space(1)
      this.w_SESSTEL = space(1)
      this.w_ASSENZE = space(1)
      this.w_DAINSPRE = space(1)
      this.w_MEMO = space(1)
      this.w_DAFARE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUPART
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GRUPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GRUPART)
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUPART = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPOGRUP = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GRUPART = space(5)
      endif
      this.w_TIPOGRUP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
        endif
        this.w_GRUPART = space(5)
        this.w_TIPOGRUP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPART
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODPART)
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPART = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODPART = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
      this.w_DPTIPRIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODPART = space(5)
        this.w_COGNPART = space(40)
        this.w_NOMEPART = space(40)
        this.w_DESCRPART = space(40)
        this.w_DPTIPRIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM,NOCODCLI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(30))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
      this.w_CODCLI = NVL(_Link_.NOCODCLI,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(15)
      endif
      this.w_DESNOM = space(40)
      this.w_COMODO = space(30)
      this.w_TIPNOM = space(1)
      this.w_CODCLI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM = space(15)
        this.w_DESNOM = space(40)
        this.w_COMODO = space(30)
        this.w_TIPNOM = space(1)
        this.w_CODCLI = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SOGG_IDX,3]
    i_lTable = "CAT_SOGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2], .t., this.CAT_SOGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',this.w_CODCAT)
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.CSCODCAT,space(10))
      this.w_DESCAT = NVL(_Link_.CSDESCAT,space(30))
      this.w_TIPCAT = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(10)
      endif
      this.w_DESCAT = space(30)
      this.w_TIPCAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCAT,1)
      cp_ShowWarn(i_cKey,this.CAT_SOGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODTIPOL
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFFTIPAT_IDX,3]
    i_lTable = "OFFTIPAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2], .t., this.OFFTIPAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODTIPOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODTIPOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(this.w_CODTIPOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',this.w_CODTIPOL)
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODTIPOL = NVL(_Link_.TACODTIP,space(5))
      this.w_DESTIPOL = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODTIPOL = space(5)
      endif
      this.w_DESTIPOL = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])+'\'+cp_ToStr(_Link_.TACODTIP,1)
      cp_ShowWarn(i_cKey,this.OFFTIPAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODTIPOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GIUDICE
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GIUDICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GIUDICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_GIUDICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_GIUDICE)
            select NOCODICE,NODESCRI,NODESCR2,NOINDIRI,NO___CAP,NODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GIUDICE = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOMGIU = NVL(_Link_.NODESCRI,space(60))
      this.w_COMODOGIU = NVL(_Link_.NODESCR2,space(60))
      this.w_INDNOM = NVL(_Link_.NOINDIRI,space(35))
      this.w_CAPNOM = NVL(_Link_.NO___CAP,space(9))
      this.w_DATOBSONOM = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GIUDICE = space(15)
      endif
      this.w_DESNOMGIU = space(60)
      this.w_COMODOGIU = space(60)
      this.w_INDNOM = space(35)
      this.w_CAPNOM = space(9)
      this.w_DATOBSONOM = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!Isalt() or ((.w_DATOBSONOM>i_datsys OR EMPTY(.w_DATOBSONOM) ) And CHKGIUDI( .w_GIUDICE ))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Nominativo inesistente o obsoleto non associato come giudice ad alcuna pratica")
        endif
        this.w_GIUDICE = space(15)
        this.w_DESNOMGIU = space(60)
        this.w_COMODOGIU = space(60)
        this.w_INDNOM = space(35)
        this.w_CAPNOM = space(9)
        this.w_DATOBSONOM = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GIUDICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPORIS
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_RISE_IDX,3]
    i_lTable = "TIP_RISE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2], .t., this.TIP_RISE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPORIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPORIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_TIPORIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_TIPORIS)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPORIS = NVL(_Link_.TRCODICE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPORIS = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_RISE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPORIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SERPRO
  func Link_1_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRO_EXPO_IDX,3]
    i_lTable = "PRO_EXPO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2], .t., this.PRO_EXPO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_APE',True,'PRO_EXPO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PRSERIAL like "+cp_ToStrODBC(trim(this.w_SERPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PRSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PRSERIAL',trim(this.w_SERPRO))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PRSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SERPRO)==trim(_Link_.PRSERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PRDESCRI like "+cp_ToStrODBC(trim(this.w_SERPRO)+"%");

            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PRDESCRI like "+cp_ToStr(trim(this.w_SERPRO)+"%");

            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SERPRO) and !this.bDontReportError
            deferred_cp_zoom('PRO_EXPO','*','PRSERIAL',cp_AbsName(oSource.parent,'oSERPRO_1_58'),i_cWhere,'GSAG_APE',"Profili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where PRSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRSERIAL',oSource.xKey(1))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where PRSERIAL="+cp_ToStrODBC(this.w_SERPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRSERIAL',this.w_SERPRO)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERPRO = NVL(_Link_.PRSERIAL,space(10))
      this.w_PRDESCRI = NVL(_Link_.PRDESCRI,space(50))
      this.w_CACODICE = NVL(_Link_.PRCAUATT,space(20))
      this.w_DATINI = NVL(cp_ToDate(_Link_.PRDATINI),ctod("  /  /  "))
      this.w_STATO = NVL(_Link_.PR_STATO,space(1))
      this.w_LCADESCRI = NVL(_Link_.PROGGETT,space(254))
      this.w_CODNOM = NVL(_Link_.PRNOMINA,space(15))
      this.w_DISPONIB = NVL(_Link_.PRDISPON,0)
      this.w_LFiltPerson = NVL(_Link_.PRRIFPER,space(60))
      this.w_CODTIPOL = NVL(_Link_.PRTIPOLO,space(5))
      this.w_LNOTE = NVL(_Link_.PR__NOTE,space(0))
      this.w_LATLOCALI = NVL(_Link_.PRLOCALI,space(30))
      this.w_ATTSTU = NVL(_Link_.PRAPPUNT,space(1))
      this.w_DAFARE = NVL(_Link_.PRDAFARE,space(1))
      this.w_SESSTEL = NVL(_Link_.PRSESTEL,space(1))
      this.w_MEMO = NVL(_Link_.PRCATNOT,space(1))
      this.w_ATTGENER = NVL(_Link_.PRATTGEN,space(1))
      this.w_ASSENZE = NVL(_Link_.PRASSENZ,space(1))
      this.w_UDIENZE = NVL(_Link_.PRUDIENZ,space(1))
      this.w_DAINSPRE = NVL(_Link_.PRINSPRE,space(1))
      this.w_CODPART = NVL(_Link_.PRCODUTE,space(5))
      this.w_CODCOM = NVL(_Link_.PRCODCOM,space(15))
      this.w_TIPANA = NVL(_Link_.PRTIPANA,space(1))
      this.w_CODPRA = NVL(_Link_.PRCODCOM,space(15))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.PRDATFIN),ctod("  /  /  "))
      this.w_TIPDAT = NVL(_Link_.PRTIPDAT,space(1))
      this.w_FILEXP = NVL(_Link_.PRPATHEX,space(254))
      this.w_PR__TIPO = NVL(_Link_.PR__TIPO,space(1))
      this.w_PRFILIMP = NVL(_Link_.PRPATHIM,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_SERPRO = space(10)
      endif
      this.w_PRDESCRI = space(50)
      this.w_CACODICE = space(20)
      this.w_DATINI = ctod("  /  /  ")
      this.w_STATO = space(1)
      this.w_LCADESCRI = space(254)
      this.w_CODNOM = space(15)
      this.w_DISPONIB = 0
      this.w_LFiltPerson = space(60)
      this.w_CODTIPOL = space(5)
      this.w_LNOTE = space(0)
      this.w_LATLOCALI = space(30)
      this.w_ATTSTU = space(1)
      this.w_DAFARE = space(1)
      this.w_SESSTEL = space(1)
      this.w_MEMO = space(1)
      this.w_ATTGENER = space(1)
      this.w_ASSENZE = space(1)
      this.w_UDIENZE = space(1)
      this.w_DAINSPRE = space(1)
      this.w_CODPART = space(5)
      this.w_CODCOM = space(15)
      this.w_TIPANA = space(1)
      this.w_CODPRA = space(15)
      this.w_DATFIN = ctod("  /  /  ")
      this.w_TIPDAT = space(1)
      this.w_FILEXP = space(254)
      this.w_PR__TIPO = space(1)
      this.w_PRFILIMP = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])+'\'+cp_ToStr(_Link_.PRSERIAL,1)
      cp_ShowWarn(i_cKey,this.PRO_EXPO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_2_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(75))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESPRA = space(75)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRA
  func Link_2_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(75))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESPRA = space(75)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page2.oPag.oCACODICE_2_3.value==this.w_CACODICE)
      this.oPgFrm.Page2.oPag.oCACODICE_2_3.value=this.w_CACODICE
    endif
    if not(this.oPgFrm.Page2.oPag.oCADESCRI_2_4.value==this.w_CADESCRI)
      this.oPgFrm.Page2.oPag.oCADESCRI_2_4.value=this.w_CADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_6.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_6.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINI_1_7.value==this.w_OREINI)
      this.oPgFrm.Page1.oPag.oOREINI_1_7.value=this.w_OREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_9.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_9.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_13.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_13.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOREFIN_1_14.value==this.w_OREFIN)
      this.oPgFrm.Page1.oPag.oOREFIN_1_14.value=this.w_OREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_16.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_16.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPATIPRIS_1_20.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page1.oPag.oPATIPRIS_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_24.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPART_1_38.value==this.w_CODPART)
      this.oPgFrm.Page1.oPag.oCODPART_1_38.value=this.w_CODPART
    endif
    if not(this.oPgFrm.Page2.oPag.oCODNOM_2_5.value==this.w_CODNOM)
      this.oPgFrm.Page2.oPag.oCODNOM_2_5.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oATTSTU_2_9.RadioValue()==this.w_ATTSTU)
      this.oPgFrm.Page2.oPag.oATTSTU_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDAFARE_2_10.RadioValue()==this.w_DAFARE)
      this.oPgFrm.Page2.oPag.oDAFARE_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSESSTEL_2_11.RadioValue()==this.w_SESSTEL)
      this.oPgFrm.Page2.oPag.oSESSTEL_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMEMO_2_12.RadioValue()==this.w_MEMO)
      this.oPgFrm.Page2.oPag.oMEMO_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATTGENER_2_13.RadioValue()==this.w_ATTGENER)
      this.oPgFrm.Page2.oPag.oATTGENER_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oASSENZE_2_14.RadioValue()==this.w_ASSENZE)
      this.oPgFrm.Page2.oPag.oASSENZE_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oUDIENZE_2_15.RadioValue()==this.w_UDIENZE)
      this.oPgFrm.Page2.oPag.oUDIENZE_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDISPONIB_2_18.RadioValue()==this.w_DISPONIB)
      this.oPgFrm.Page2.oPag.oDISPONIB_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFiltPerson_2_19.value==this.w_FiltPerson)
      this.oPgFrm.Page2.oPag.oFiltPerson_2_19.value=this.w_FiltPerson
    endif
    if not(this.oPgFrm.Page2.oPag.oCODTIPOL_2_20.value==this.w_CODTIPOL)
      this.oPgFrm.Page2.oPag.oCODTIPOL_2_20.value=this.w_CODTIPOL
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTE_2_21.value==this.w_NOTE)
      this.oPgFrm.Page2.oPag.oNOTE_2_21.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oATLOCALI_2_23.value==this.w_ATLOCALI)
      this.oPgFrm.Page2.oPag.oATLOCALI_2_23.value=this.w_ATLOCALI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOM_2_27.value==this.w_DESNOM)
      this.oPgFrm.Page2.oPag.oDESNOM_2_27.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESTIPOL_2_34.value==this.w_DESTIPOL)
      this.oPgFrm.Page2.oPag.oDESTIPOL_2_34.value=this.w_DESTIPOL
    endif
    if not(this.oPgFrm.Page1.oPag.oSERPRO_1_58.value==this.w_SERPRO)
      this.oPgFrm.Page1.oPag.oSERPRO_1_58.value=this.w_SERPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEXP_1_59.value==this.w_FILEXP)
      this.oPgFrm.Page1.oPag.oFILEXP_1_59.value=this.w_FILEXP
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCOM_2_45.value==this.w_CODCOM)
      this.oPgFrm.Page2.oPag.oCODCOM_2_45.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPANA_2_46.RadioValue()==this.w_TIPANA)
      this.oPgFrm.Page2.oPag.oTIPANA_2_46.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRA_2_48.value==this.w_DESPRA)
      this.oPgFrm.Page2.oPag.oDESPRA_2_48.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oCODPRA_2_50.value==this.w_CODPRA)
      this.oPgFrm.Page2.oPag.oCODPRA_2_50.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPRA_2_52.value==this.w_DESPRA)
      this.oPgFrm.Page2.oPag.oDESPRA_2_52.value=this.w_DESPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDESCRI_1_65.value==this.w_PRDESCRI)
      this.oPgFrm.Page1.oPag.oPRDESCRI_1_65.value=this.w_PRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_PART_1_70.value==this.w_DENOM_PART)
      this.oPgFrm.Page1.oPag.oDENOM_PART_1_70.value=this.w_DENOM_PART
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(VAL(.w_OREINI) < 24)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(VAL(.w_OREFIN) < 24)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_CODPART , .w_GRUPART))  and not(empty(.w_GRUPART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUPART_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
          case   not(EMPTY(.w_CODPART) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS)  and not(empty(.w_CODPART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPART_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and not(empty(.w_CODNOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODNOM_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!Isalt() or ((.w_DATOBSONOM>i_datsys OR EMPTY(.w_DATOBSONOM) ) And CHKGIUDI( .w_GIUDICE )))  and not(empty(.w_GIUDICE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGIUDICE_2_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Nominativo inesistente o obsoleto non associato come giudice ad alcuna pratica")
          case   (empty(.w_SERPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERPRO_1_58.SetFocus()
            i_bnoObbl = !empty(.w_SERPRO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FILEXP)) or not(DIRECTORY(JUSTPATH(.w_FILEXP)) and Not Empty(justfname(.w_FILEXP))))  and (Not Empty(.w_SERPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFILEXP_1_59.SetFocus()
            i_bnoObbl = !empty(.w_FILEXP)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, percorso file inesistente o nome file errato")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_READAZI = this.w_READAZI
    this.o_FLTPART = this.w_FLTPART
    this.o_DATINI = this.w_DATINI
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_DATFIN = this.w_DATFIN
    this.o_OREFIN = this.w_OREFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_PATIPRIS = this.w_PATIPRIS
    this.o_CODPART = this.w_CODPART
    this.o_CODNOM = this.w_CODNOM
    this.o_SERPRO = this.w_SERPRO
    this.o_CODCOM = this.w_CODCOM
    return

enddefine

* --- Define pages as container
define class tgsag_kisPag1 as StdContainer
  Width  = 782
  height = 528
  stdWidth  = 782
  stdheight = 528
  resizeXpos=266
  resizeYpos=231
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_6 as StdField with uid="VAJTUNAFFS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 29073866,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=57, Top=53

  add object oOREINI_1_7 as StdField with uid="WOKDXXPUIP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OREINI", cQueryName = "OREINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di inizio selezione",;
    HelpContextID = 29130778,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=197, Top=53, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREINI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oOREINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_9 as StdField with uid="QMRKNBACQP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio selezione",;
    HelpContextID = 29096250,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=231, Top=53, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oMININI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc


  add object oObj_1_11 as cp_runprogram with uid="VSYPGKICOH",left=2, top=578, width=211,height=26,;
    caption='Azzera ore e min iniziali',;
   bGlobalFont=.t.,;
    prg="gsag_ba2('1')",;
    cEvent = "w_DATINI Changed",;
    nPag=1;
    , HelpContextID = 115689985

  add object oDATFIN_1_13 as StdField with uid="VRHGJGBHQK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 219062730,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=57, Top=78

  func oDATFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oOREFIN_1_14 as StdField with uid="VKJCJSQZFK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_OREFIN", cQueryName = "OREFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di fine selezione",;
    HelpContextID = 219119642,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=197, Top=78, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREFIN_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oOREFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_16 as StdField with uid="GIWPXXGMLC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di fine selezione",;
    HelpContextID = 219085114,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=231, Top=78, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oMINFIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc


  add object oObj_1_18 as cp_runprogram with uid="GTGWZWHBZD",left=2, top=607, width=211,height=22,;
    caption='Azzera ore e min finali',;
   bGlobalFont=.t.,;
    prg="gsag_ba2('2')",;
    cEvent = "w_DATFIN Changed",;
    nPag=1;
    , HelpContextID = 177167363


  add object oPATIPRIS_1_20 as StdCombo with uid="FEFTOWFWCM",value=4,rtseq=13,rtrep=.f.,left=347,top=53,width=76,height=21, enabled=.f.;
    , HelpContextID = 144416951;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPATIPRIS_1_20.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    iif(this.value =3,'R',;
    iif(this.value =4,'',;
    space(1))))))
  endfunc
  func oPATIPRIS_1_20.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_1_20.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',3,;
      iif(this.Parent.oContained.w_PATIPRIS=='',4,;
      0))))
  endfunc


  add object oSTATO_1_24 as StdCombo with uid="HPCXRCGRJY",rtseq=17,rtrep=.f.,left=347,top=79,width=124,height=21, enabled=.f.;
    , ToolTipText = "Stato";
    , HelpContextID = 178372058;
    , cFormVar="w_STATO",RowSource=""+"Non evasa,"+"Da svolgere,"+"In corso,"+"Provvisoria,"+"Evasa,"+"Completata,"+"Evasa o completata,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_24.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'T',;
    iif(this.value =5,'F',;
    iif(this.value =6,'P',;
    iif(this.value =7,'B',;
    iif(this.value =8,'K',;
    space(1))))))))))
  endfunc
  func oSTATO_1_24.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_24.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='N',1,;
      iif(this.Parent.oContained.w_STATO=='D',2,;
      iif(this.Parent.oContained.w_STATO=='I',3,;
      iif(this.Parent.oContained.w_STATO=='T',4,;
      iif(this.Parent.oContained.w_STATO=='F',5,;
      iif(this.Parent.oContained.w_STATO=='P',6,;
      iif(this.Parent.oContained.w_STATO=='B',7,;
      iif(this.Parent.oContained.w_STATO=='K',8,;
      0))))))))
  endfunc

  func oSTATO_1_24.mHide()
    with this.Parent.oContained
      return (.w_RISERVE='S')
    endwith
  endfunc


  add object oBtn_1_32 as StdButton with uid="PHEWMSSLEL",left=731, top=78, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 178353942;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_SERPRO))
      endwith
    endif
  endfunc

  add object oCODPART_1_38 as StdField with uid="QJYWRUDDRU",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CODPART", cQueryName = "CODPART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice partecipante",;
    HelpContextID = 108686374,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=432, Top=53, InputMask=replicate('X',5), cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_CODPART"

  func oCODPART_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oBtn_1_44 as StdButton with uid="TKCMXZXILU",left=731, top=481, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 259686330;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AGKRA_ZOOM as cp_szoombox with uid="MPOPQLEMXY",left=-2, top=129, width=787,height=347,;
    caption='AGKRA_ZOOM',;
   bGlobalFont=.t.,;
    cTable="OFF_ATTI",cZoomFile="GSAG_KIS",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSAG_AAT",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,DisableSelMenu=.f.,bNoZoomGridShape=.f.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 209902011


  add object oObj_1_51 as cp_runprogram with uid="UAIOGLNCRD",left=216, top=578, width=307,height=26,;
    caption='Doppio click',;
   bGlobalFont=.t.,;
    prg="GSAG_BRA('V', 'A')",;
    cEvent = "w_AGKRA_ZOOM selected",;
    nPag=1;
    , HelpContextID = 110029913

  add object oSERPRO_1_58 as StdField with uid="AZKYDFMGSL",rtseq=71,rtrep=.f.,;
    cFormVar = "w_SERPRO", cQueryName = "SERPRO",nZero=10,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Profilo utente",;
    HelpContextID = 192199898,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=57, Top=19, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRO_EXPO", cZoomOnZoom="GSAG_APE", oKey_1_1="PRSERIAL", oKey_1_2="this.w_SERPRO"

  func oSERPRO_1_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_58('Part',this)
    endwith
    return bRes
  endfunc

  proc oSERPRO_1_58.ecpDrop(oSource)
    this.Parent.oContained.link_1_58('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSERPRO_1_58.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRO_EXPO','*','PRSERIAL',cp_AbsName(this.parent,'oSERPRO_1_58'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_APE',"Profili",'',this.parent.oContained
  endproc
  proc oSERPRO_1_58.mZoomOnZoom
    local i_obj
    i_obj=GSAG_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PRSERIAL=this.parent.oContained.w_SERPRO
     i_obj.ecpSave()
  endproc

  add object oFILEXP_1_59 as StdField with uid="AXZELWARXC",rtseq=72,rtrep=.f.,;
    cFormVar = "w_FILEXP", cQueryName = "FILEXP",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, percorso file inesistente o nome file errato",;
    ToolTipText = "FIle di export",;
    HelpContextID = 169875882,;
   bGlobalFont=.t.,;
    Height=21, Width=371, Left=52, Top=491, InputMask=replicate('X',254)

  func oFILEXP_1_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_SERPRO))
    endwith
   endif
  endfunc

  func oFILEXP_1_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (DIRECTORY(JUSTPATH(.w_FILEXP)) and Not Empty(justfname(.w_FILEXP)))
    endwith
    return bRes
  endfunc


  add object oBtn_1_62 as StdButton with uid="GBZKOFQPIX",left=681, top=481, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per esportare attivit�";
    , HelpContextID = 195842118;
    , Caption='\<Esporta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      with this.Parent.oContained
        gsag_bis(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPRDESCRI_1_65 as StdField with uid="TKTEUHTOMC",rtseq=81,rtrep=.f.,;
    cFormVar = "w_PRDESCRI", cQueryName = "PRDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 143618111,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=166, Top=19, InputMask=replicate('X',50)

  add object oDENOM_PART_1_70 as StdField with uid="SKXCKTCAOH",rtseq=85,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(48), bMultilanguage =  .f.,;
    HelpContextID = 70933399,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=506, Top=54, InputMask=replicate('X',48)


  add object BTNQRY as cp_askfile with uid="SVNANQOYOT",left=426, top=493, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_FILEXP",cext="ICS,VCS",;
    nPag=1;
    , ToolTipText = "Premere per selezionare il file di export";
    , HelpContextID = 266802730

  add object oStr_1_5 as StdString with uid="KLEZMLKSOB",Visible=.t., Left=4, Top=54,;
    Alignment=1, Width=50, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="JAHWAWKLCQ",Visible=.t., Left=4, Top=79,;
    Alignment=1, Width=50, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="JGUELPQNWG",Visible=.t., Left=292, Top=78,;
    Alignment=1, Width=52, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_RISERVE='S')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="EWZSPAOTVW",Visible=.t., Left=134, Top=54,;
    Alignment=1, Width=59, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="CTAHWBKCXZ",Visible=.t., Left=134, Top=78,;
    Alignment=1, Width=59, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="DAAABZBARA",Visible=.t., Left=226, Top=54,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="BTYOAEACQT",Visible=.t., Left=226, Top=79,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="ZGENJNNRPH",Visible=.t., Left=267, Top=54,;
    Alignment=1, Width=77, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="EKUQLQGTAB",Visible=.t., Left=19, Top=19,;
    Alignment=0, Width=38, Height=18,;
    Caption="Profilo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="ILOUNTOCKB",Visible=.t., Left=0, Top=492,;
    Alignment=1, Width=50, Height=18,;
    Caption="File :"  ;
  , bGlobalFont=.t.

  add object oBox_1_46 as StdBox with uid="LOIZABPOHX",left=430, top=51, width=68,height=27
enddefine
define class tgsag_kisPag2 as StdContainer
  Width  = 782
  height = 528
  stdWidth  = 782
  stdheight = 528
  resizeXpos=468
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODICE_2_3 as StdField with uid="MBGICDMGLD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CACODICE", cQueryName = "CACODICE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 229199467,;
   bGlobalFont=.t.,;
    Height=21, Width=171, Left=99, Top=11, InputMask=replicate('X',20), cLinkFile="CAUMATTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CACODICE"

  func oCACODICE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCADESCRI_2_4 as AH_SEARCHFLD with uid="AVSKMMIFVK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CADESCRI", cQueryName = "CADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione oggetto o parte di essa",;
    HelpContextID = 143613551,;
   bGlobalFont=.t.,;
    Height=21, Width=375, Left=373, Top=12, InputMask=replicate('X',254)

  add object oCODNOM_2_5 as StdField with uid="LDORSNJAGM",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo o soggetto esterno della pratica",;
    HelpContextID = 229086170,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=99, Top=42, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15), cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oATTSTU_2_9 as StdCheck with uid="FUHPBAIQSC",rtseq=29,rtrep=.f.,left=99, top=101, caption="Appuntamenti", enabled=.f.,;
    ToolTipText = "Se attivo: visualizza gli appuntamenti",;
    HelpContextID = 89231098,;
    cFormVar="w_ATTSTU", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATTSTU_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oATTSTU_2_9.GetRadio()
    this.Parent.oContained.w_ATTSTU = this.RadioValue()
    return .t.
  endfunc

  func oATTSTU_2_9.SetRadio()
    this.Parent.oContained.w_ATTSTU=trim(this.Parent.oContained.w_ATTSTU)
    this.value = ;
      iif(this.Parent.oContained.w_ATTSTU=='S',1,;
      0)
  endfunc

  add object oDAFARE_2_10 as StdCheck with uid="JYTLCSHGGN",rtseq=30,rtrep=.f.,left=265, top=101, caption="Cose da fare", enabled=.f.,;
    ToolTipText = "Se attivo: visualizza le cose da fare",;
    HelpContextID = 92570058,;
    cFormVar="w_DAFARE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDAFARE_2_10.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oDAFARE_2_10.GetRadio()
    this.Parent.oContained.w_DAFARE = this.RadioValue()
    return .t.
  endfunc

  func oDAFARE_2_10.SetRadio()
    this.Parent.oContained.w_DAFARE=trim(this.Parent.oContained.w_DAFARE)
    this.value = ;
      iif(this.Parent.oContained.w_DAFARE=='D',1,;
      0)
  endfunc

  proc oDAFARE_2_10.mAfter
    with this.Parent.oContained
      .w_AccodaDaFare='N'
    endwith
  endproc

  add object oSESSTEL_2_11 as StdCheck with uid="KYMNVXZYIW",rtseq=31,rtrep=.f.,left=403, top=101, caption="Sessioni telefoniche", enabled=.f.,;
    ToolTipText = "Se attivo: visualizza le sessioni telefoniche",;
    HelpContextID = 89238746,;
    cFormVar="w_SESSTEL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSESSTEL_2_11.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oSESSTEL_2_11.GetRadio()
    this.Parent.oContained.w_SESSTEL = this.RadioValue()
    return .t.
  endfunc

  func oSESSTEL_2_11.SetRadio()
    this.Parent.oContained.w_SESSTEL=trim(this.Parent.oContained.w_SESSTEL)
    this.value = ;
      iif(this.Parent.oContained.w_SESSTEL=='T',1,;
      0)
  endfunc

  add object oMEMO_2_12 as StdCheck with uid="ELRSHFYRWO",rtseq=32,rtrep=.f.,left=564, top=101, caption="Note", enabled=.f.,;
    ToolTipText = "Se attivo: visualizza le note",;
    HelpContextID = 261492026,;
    cFormVar="w_MEMO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMEMO_2_12.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oMEMO_2_12.GetRadio()
    this.Parent.oContained.w_MEMO = this.RadioValue()
    return .t.
  endfunc

  func oMEMO_2_12.SetRadio()
    this.Parent.oContained.w_MEMO=trim(this.Parent.oContained.w_MEMO)
    this.value = ;
      iif(this.Parent.oContained.w_MEMO=='M',1,;
      0)
  endfunc

  proc oMEMO_2_12.mAfter
    with this.Parent.oContained
      .w_AccodaNote='N'
    endwith
  endproc

  add object oATTGENER_2_13 as StdCheck with uid="KLBNRLNCJC",rtseq=33,rtrep=.f.,left=99, top=127, caption="Attivit� generiche", enabled=.f.,;
    ToolTipText = "Se attivo: visualizza le attivit� generiche",;
    HelpContextID = 45248856,;
    cFormVar="w_ATTGENER", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oATTGENER_2_13.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oATTGENER_2_13.GetRadio()
    this.Parent.oContained.w_ATTGENER = this.RadioValue()
    return .t.
  endfunc

  func oATTGENER_2_13.SetRadio()
    this.Parent.oContained.w_ATTGENER=trim(this.Parent.oContained.w_ATTGENER)
    this.value = ;
      iif(this.Parent.oContained.w_ATTGENER=='G',1,;
      0)
  endfunc

  add object oASSENZE_2_14 as StdCheck with uid="RPXCBGRCCL",rtseq=34,rtrep=.f.,left=265, top=127, caption="Assenze", enabled=.f.,;
    ToolTipText = "Se attivo: visualizza le assenze",;
    HelpContextID = 255877126,;
    cFormVar="w_ASSENZE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oASSENZE_2_14.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oASSENZE_2_14.GetRadio()
    this.Parent.oContained.w_ASSENZE = this.RadioValue()
    return .t.
  endfunc

  func oASSENZE_2_14.SetRadio()
    this.Parent.oContained.w_ASSENZE=trim(this.Parent.oContained.w_ASSENZE)
    this.value = ;
      iif(this.Parent.oContained.w_ASSENZE=='A',1,;
      0)
  endfunc

  add object oUDIENZE_2_15 as StdCheck with uid="HBQKVBXHMX",rtseq=35,rtrep=.f.,left=403, top=127, caption="Udienze", enabled=.f.,;
    ToolTipText = "Se attivo: visualizza le udienze",;
    HelpContextID = 255832646,;
    cFormVar="w_UDIENZE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oUDIENZE_2_15.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oUDIENZE_2_15.GetRadio()
    this.Parent.oContained.w_UDIENZE = this.RadioValue()
    return .t.
  endfunc

  func oUDIENZE_2_15.SetRadio()
    this.Parent.oContained.w_UDIENZE=trim(this.Parent.oContained.w_UDIENZE)
    this.value = ;
      iif(this.Parent.oContained.w_UDIENZE=='U',1,;
      0)
  endfunc

  func oUDIENZE_2_15.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc


  add object oDISPONIB_2_18 as StdCombo with uid="MRAFDOVVBK",value=5,rtseq=38,rtrep=.f.,left=99,top=168,width=118,height=21, enabled=.f.;
    , ToolTipText = "Disponibilit�";
    , HelpContextID = 212117896;
    , cFormVar="w_DISPONIB",RowSource=""+"Occupato,"+"Per urgenze,"+"Libero,"+"Fuori sede,"+"Tutte", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDISPONIB_2_18.RadioValue()
    return(iif(this.value =1,2,;
    iif(this.value =2,3,;
    iif(this.value =3,1,;
    iif(this.value =4,4,;
    iif(this.value =5,0,;
    0))))))
  endfunc
  func oDISPONIB_2_18.GetRadio()
    this.Parent.oContained.w_DISPONIB = this.RadioValue()
    return .t.
  endfunc

  func oDISPONIB_2_18.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DISPONIB==2,1,;
      iif(this.Parent.oContained.w_DISPONIB==3,2,;
      iif(this.Parent.oContained.w_DISPONIB==1,3,;
      iif(this.Parent.oContained.w_DISPONIB==4,4,;
      iif(this.Parent.oContained.w_DISPONIB==0,5,;
      0)))))
  endfunc

  add object oFiltPerson_2_19 as AH_SEARCHFLD with uid="MYRJGKWLFV",rtseq=39,rtrep=.f.,;
    cFormVar = "w_FiltPerson", cQueryName = "FiltPerson",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Persona o parte di essa",;
    HelpContextID = 177306553,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=99, Top=71, InputMask=replicate('X',60)

  add object oCODTIPOL_2_20 as StdField with uid="HQBAYRUABD",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CODTIPOL", cQueryName = "CODTIPOL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia",;
    HelpContextID = 184652686,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=99, Top=194, InputMask=replicate('X',5), cLinkFile="OFFTIPAT", oKey_1_1="TACODTIP", oKey_1_2="this.w_CODTIPOL"

  func oCODTIPOL_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oNOTE_2_21 as AH_SEARCHMEMO with uid="YFIDICHIXZ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Annotazioni o parte di esse",;
    HelpContextID = 262116138,;
   bGlobalFont=.t.,;
    Height=21, Width=676, Left=99, Top=223

  add object oATLOCALI_2_23 as AH_SEARCHFLD with uid="KNSKLUNXGJ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_ATLOCALI", cQueryName = "ATLOCALI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� o parte di essa",;
    HelpContextID = 174460593,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=315, Top=168, InputMask=replicate('X',30)

  add object oDESNOM_2_27 as StdField with uid="YBKTMCLSXL",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229027274,;
   bGlobalFont=.t.,;
    Height=21, Width=331, Left=239, Top=42, InputMask=replicate('X',40)

  add object oDESTIPOL_2_34 as StdField with uid="XDLBTHYTYA",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESTIPOL", cQueryName = "DESTIPOL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 184593790,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=174, Top=194, InputMask=replicate('X',50)

  add object oCODCOM_2_45 as StdField with uid="KRSMLUAVRO",rtseq=73,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 229807066,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=97, Top=279, InputMask=replicate('X',15), cLinkFile="CAN_TIER", cZoomOnZoom="gsar_bzz", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_2_45.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCODCOM_2_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oTIPANA_2_46 as StdCombo with uid="OOYOOVQSKI",rtseq=74,rtrep=.f.,left=97,top=309,width=146,height=21, enabled=.f.;
    , HelpContextID = 163829962;
    , cFormVar="w_TIPANA",RowSource=""+"Costo,"+"Ricavo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPANA_2_46.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oTIPANA_2_46.GetRadio()
    this.Parent.oContained.w_TIPANA = this.RadioValue()
    return .t.
  endfunc

  func oTIPANA_2_46.SetRadio()
    this.Parent.oContained.w_TIPANA=trim(this.Parent.oContained.w_TIPANA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPANA=='C',1,;
      iif(this.Parent.oContained.w_TIPANA=='R',2,;
      0))
  endfunc

  func oTIPANA_2_46.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oDESPRA_2_48 as StdField with uid="FPINBUYLXX",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(75), bMultilanguage =  .f.,;
    HelpContextID = 158641610,;
   bGlobalFont=.t.,;
    Height=21, Width=542, Left=232, Top=279, InputMask=replicate('X',75)

  func oDESPRA_2_48.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCODPRA_2_50 as StdField with uid="HXZHECNQQP",rtseq=76,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice pratica",;
    HelpContextID = 158700506,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=97, Top=279, InputMask=replicate('X',15), cLinkFile="CAN_TIER", cZoomOnZoom="gsar_bzz", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_2_50.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  func oCODPRA_2_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESPRA_2_52 as StdField with uid="NLHDFNFAKA",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(75), bMultilanguage =  .f.,;
    HelpContextID = 158641610,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=225, Top=280, InputMask=replicate('X',75)

  func oDESPRA_2_52.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_2_1 as StdString with uid="GXOWXVAARV",Visible=.t., Left=276, Top=13,;
    Alignment=1, Width=92, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="BSGKMHWZHM",Visible=.t., Left=28, Top=223,;
    Alignment=1, Width=67, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="RCYDFBJYOO",Visible=.t., Left=3, Top=46,;
    Alignment=1, Width=92, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="LBTIYCWQWU",Visible=.t., Left=3, Top=168,;
    Alignment=1, Width=92, Height=18,;
    Caption="Disponibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="YJIZXRPWAZ",Visible=.t., Left=28, Top=194,;
    Alignment=1, Width=67, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="CTBVQCRJMM",Visible=.t., Left=255, Top=168,;
    Alignment=1, Width=56, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="QVTFQRDCTZ",Visible=.t., Left=12, Top=13,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="DNFJJECVYE",Visible=.t., Left=1, Top=71,;
    Alignment=1, Width=94, Height=18,;
    Caption="Rifer. persona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="BPYZWLNTLX",Visible=.t., Left=1, Top=281,;
    Alignment=1, Width=92, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_47.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_49 as StdString with uid="KSBGPQZUEI",Visible=.t., Left=43, Top=308,;
    Alignment=1, Width=50, Height=18,;
    Caption="Natura:"  ;
  , bGlobalFont=.t.

  func oStr_2_49.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="HZALIZNENK",Visible=.t., Left=51, Top=281,;
    Alignment=1, Width=45, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_2_51.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kis','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
