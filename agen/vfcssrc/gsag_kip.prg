* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kip                                                        *
*              Scelta multipla delle prestazioni                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-25                                                      *
* Last revis.: 2012-05-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kip",oParentObject))

* --- Class definition
define class tgsag_kip as StdForm
  Top    = 7
  Left   = 16

  * --- Standard Properties
  Width  = 652
  Height = 522
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-23"
  HelpContextID=1431703
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  CAN_TIER_IDX = 0
  VALUTE_IDX = 0
  LISTINI_IDX = 0
  PAR_AGEN_IDX = 0
  PRA_ENTI_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  PRE_ITER_IDX = 0
  PAR_ALTE_IDX = 0
  cPrg = "gsag_kip"
  cComment = "Scelta multipla delle prestazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CACODINI = space(20)
  w_CACODFIN = space(20)
  w_DESCR = space(40)
  w_DESCRSUPP = space(0)
  w_TIPOLOGIA = space(1)
  w_SELRAGG = space(10)
  w_TipoPersona = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CADESINI = space(40)
  w_CADESFIN = space(40)
  w_CAARTINI = space(20)
  w_CADOBINI = ctod('  /  /  ')
  w_CAARTFIN = space(20)
  w_CADOBFIN = ctod('  /  /  ')
  w_OB_TEST = ctod('  /  /  ')
  w_RESCHK = 0
  w_SELEZI = space(1)
  w_FLSELE = 0
  w_PROG_PARENT = space(20)
  w_CACODICE = space(20)
  w_CACODART = space(20)
  w_COMODO = space(10)
  w_LetturaParAlte = space(5)
  o_LetturaParAlte = space(5)
  w_PAFLDESP = space(1)
  w_AGKPM_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kipPag1","gsag_kip",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCACODINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGKPM_ZOOM = this.oPgFrm.Pages(1).oPag.AGKPM_ZOOM
    DoDefault()
    proc Destroy()
      this.w_AGKPM_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='PAR_AGEN'
    this.cWorkTables[6]='PRA_ENTI'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='ART_ICOL'
    this.cWorkTables[9]='PRE_ITER'
    this.cWorkTables[10]='PAR_ALTE'
    return(this.OpenAllTables(10))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CACODINI=space(20)
      .w_CACODFIN=space(20)
      .w_DESCR=space(40)
      .w_DESCRSUPP=space(0)
      .w_TIPOLOGIA=space(1)
      .w_SELRAGG=space(10)
      .w_TipoPersona=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CADESINI=space(40)
      .w_CADESFIN=space(40)
      .w_CAARTINI=space(20)
      .w_CADOBINI=ctod("  /  /  ")
      .w_CAARTFIN=space(20)
      .w_CADOBFIN=ctod("  /  /  ")
      .w_OB_TEST=ctod("  /  /  ")
      .w_RESCHK=0
      .w_SELEZI=space(1)
      .w_FLSELE=0
      .w_PROG_PARENT=space(20)
      .w_CACODICE=space(20)
      .w_CACODART=space(20)
      .w_COMODO=space(10)
      .w_LetturaParAlte=space(5)
      .w_PAFLDESP=space(1)
      .w_COMODO=oParentObject.w_COMODO
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CACODINI))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CACODFIN))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_TIPOLOGIA = 'K'
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_SELRAGG))
          .link_1_6('Full')
        endif
        .w_TipoPersona = 'P'
        .w_OBTEST = i_datsys
      .oPgFrm.Page1.oPag.AGKPM_ZOOM.Calculate()
          .DoRTCalc(9,15,.f.)
        .w_OB_TEST = IIF(.w_PROG_PARENT = 'GSAG_AAT', this.oParentObject.w_DATINI, ctod("  -  -  "))
        .w_RESCHK = 0
        .w_SELEZI = 'D'
        .w_FLSELE = 0
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .w_PROG_PARENT = upper(oParentObject.cPrg)
        .w_CACODICE = .w_AGKPM_ZOOM.GetVar('CACODICE')
        .w_CACODART = .w_AGKPM_ZOOM.GetVar('CACODART')
          .DoRTCalc(23,23,.f.)
        .w_LetturaParAlte = i_CodAzi
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_LetturaParAlte))
          .link_1_39('Full')
        endif
    endwith
    this.DoRTCalc(25,25,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsag_kip
    If IsAlt()
      this.NotifyEvent('Ricerca')
    endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_COMODO=.w_COMODO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.AGKPM_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .DoRTCalc(1,20,.t.)
            .w_CACODICE = .w_AGKPM_ZOOM.GetVar('CACODICE')
            .w_CACODART = .w_AGKPM_ZOOM.GetVar('CACODART')
        .DoRTCalc(23,23,.t.)
        if .o_LetturaParalte<>.w_LetturaParalte
            .w_LetturaParAlte = i_CodAzi
          .link_1_39('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(25,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AGKPM_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
    endwith
  return

  proc Calculate_QTYFFRAAXR()
    with this
          * --- Inserimento prestazioni multiple
          GSAG_BIP(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCACODINI_1_1.enabled = this.oPgFrm.Page1.oPag.oCACODINI_1_1.mCond()
    this.oPgFrm.Page1.oPag.oCACODFIN_1_2.enabled = this.oPgFrm.Page1.oPag.oCACODFIN_1_2.mCond()
    this.oPgFrm.Page1.oPag.oDESCR_1_3.enabled = this.oPgFrm.Page1.oPag.oDESCR_1_3.mCond()
    this.oPgFrm.Page1.oPag.oDESCRSUPP_1_4.enabled = this.oPgFrm.Page1.oPag.oDESCRSUPP_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCACODINI_1_1.visible=!this.oPgFrm.Page1.oPag.oCACODINI_1_1.mHide()
    this.oPgFrm.Page1.oPag.oCACODFIN_1_2.visible=!this.oPgFrm.Page1.oPag.oCACODFIN_1_2.mHide()
    this.oPgFrm.Page1.oPag.oDESCR_1_3.visible=!this.oPgFrm.Page1.oPag.oDESCR_1_3.mHide()
    this.oPgFrm.Page1.oPag.oDESCRSUPP_1_4.visible=!this.oPgFrm.Page1.oPag.oDESCRSUPP_1_4.mHide()
    this.oPgFrm.Page1.oPag.oTIPOLOGIA_1_5.visible=!this.oPgFrm.Page1.oPag.oTIPOLOGIA_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCADESINI_1_19.visible=!this.oPgFrm.Page1.oPag.oCADESINI_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCADESFIN_1_21.visible=!this.oPgFrm.Page1.oPag.oCADESFIN_1_21.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_32.visible=!this.oPgFrm.Page1.oPag.oBtn_1_32.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_33.visible=!this.oPgFrm.Page1.oPag.oBtn_1_33.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_34.visible=!this.oPgFrm.Page1.oPag.oBtn_1_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AGKPM_ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CACODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CACODINI))
          select CACODICE,CADESART,CACODART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCACODINI_1_1'),i_cWhere,'GSMA_BZA',"Servizi",'GSAGZKPM.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODINI)
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODINI = NVL(_Link_.CACODICE,space(20))
      this.w_CADESINI = NVL(_Link_.CADESART,space(40))
      this.w_CAARTINI = NVL(_Link_.CACODART,space(20))
      this.w_CADOBINI = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CACODINI = space(20)
      endif
      this.w_CADESINI = space(40)
      this.w_CAARTINI = space(20)
      this.w_CADOBINI = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_CAARTINI, 'FM-FO-DE') AND (EMPTY(.w_CADOBINI) OR .w_CADOBINI>=i_DatSys) AND (EMPTY(.w_CACODFIN) OR .w_CACODINI<=.w_CACODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di ricerca non valido, obsoleto oppure codice iniziale maggiore del codice finale")
        endif
        this.w_CACODINI = space(20)
        this.w_CADESINI = space(40)
        this.w_CAARTINI = space(20)
        this.w_CADOBINI = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODFIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CACODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CACODFIN))
          select CACODICE,CADESART,CACODART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCACODFIN_1_2'),i_cWhere,'GSMA_BZA',"Servizi",'GSAGZKPM.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CACODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CACODFIN)
            select CACODICE,CADESART,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_CADESFIN = NVL(_Link_.CADESART,space(40))
      this.w_CAARTFIN = NVL(_Link_.CACODART,space(20))
      this.w_CADOBFIN = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CACODFIN = space(20)
      endif
      this.w_CADESFIN = space(40)
      this.w_CAARTFIN = space(20)
      this.w_CADOBFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_CAARTFIN, 'FM-FO-DE') AND (EMPTY(.w_CADOBFIN) OR .w_CADOBFIN>=i_DatSys) AND (EMPTY(.w_CACODINI) OR .w_CACODINI<=.w_CACODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di ricerca non valido, obsoleto oppure codice finale minore del codice iniziale")
        endif
        this.w_CACODFIN = space(20)
        this.w_CADESFIN = space(40)
        this.w_CAARTFIN = space(20)
        this.w_CADOBFIN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELRAGG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRE_ITER_IDX,3]
    i_lTable = "PRE_ITER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2], .t., this.PRE_ITER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELRAGG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MIT',True,'PRE_ITER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ITCODICE like "+cp_ToStrODBC(trim(this.w_SELRAGG)+"%");

          i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ITCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ITCODICE',trim(this.w_SELRAGG))
          select ITCODICE,ITDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ITCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SELRAGG)==trim(_Link_.ITCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStrODBC(trim(this.w_SELRAGG)+"%");

            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ITDESCRI like "+cp_ToStr(trim(this.w_SELRAGG)+"%");

            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SELRAGG) and !this.bDontReportError
            deferred_cp_zoom('PRE_ITER','*','ITCODICE',cp_AbsName(oSource.parent,'oSELRAGG_1_6'),i_cWhere,'GSAG_MIT',"Raggruppamenti prestazioni",'GSAG_MIT.PRE_ITER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',oSource.xKey(1))
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELRAGG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODICE,ITDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ITCODICE="+cp_ToStrODBC(this.w_SELRAGG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODICE',this.w_SELRAGG)
            select ITCODICE,ITDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELRAGG = NVL(_Link_.ITCODICE,space(10))
      this.w_COMODO = NVL(_Link_.ITDESCRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_SELRAGG = space(10)
      endif
      this.w_COMODO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRE_ITER_IDX,2])+'\'+cp_ToStr(_Link_.ITCODICE,1)
      cp_ShowWarn(i_cKey,this.PRE_ITER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELRAGG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LetturaParAlte
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LetturaParAlte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LetturaParAlte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLDESP";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LetturaParAlte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LetturaParAlte)
            select PACODAZI,PAFLDESP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LetturaParAlte = NVL(_Link_.PACODAZI,space(5))
      this.w_PAFLDESP = NVL(_Link_.PAFLDESP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LetturaParAlte = space(5)
      endif
      this.w_PAFLDESP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LetturaParAlte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCACODINI_1_1.value==this.w_CACODINI)
      this.oPgFrm.Page1.oPag.oCACODINI_1_1.value=this.w_CACODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODFIN_1_2.value==this.w_CACODFIN)
      this.oPgFrm.Page1.oPag.oCACODFIN_1_2.value=this.w_CACODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR_1_3.value==this.w_DESCR)
      this.oPgFrm.Page1.oPag.oDESCR_1_3.value=this.w_DESCR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRSUPP_1_4.value==this.w_DESCRSUPP)
      this.oPgFrm.Page1.oPag.oDESCRSUPP_1_4.value=this.w_DESCRSUPP
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOLOGIA_1_5.RadioValue()==this.w_TIPOLOGIA)
      this.oPgFrm.Page1.oPag.oTIPOLOGIA_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELRAGG_1_6.value==this.w_SELRAGG)
      this.oPgFrm.Page1.oPag.oSELRAGG_1_6.value=this.w_SELRAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESINI_1_19.value==this.w_CADESINI)
      this.oPgFrm.Page1.oPag.oCADESINI_1_19.value=this.w_CADESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESFIN_1_21.value==this.w_CADESFIN)
      this.oPgFrm.Page1.oPag.oCADESFIN_1_21.value=this.w_CADESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_28.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_28.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(ChkTipArt(.w_CAARTINI, 'FM-FO-DE') AND (EMPTY(.w_CADOBINI) OR .w_CADOBINI>=i_DatSys) AND (EMPTY(.w_CACODFIN) OR .w_CACODINI<=.w_CACODFIN))  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_CACODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di ricerca non valido, obsoleto oppure codice iniziale maggiore del codice finale")
          case   not(ChkTipArt(.w_CAARTFIN, 'FM-FO-DE') AND (EMPTY(.w_CADOBFIN) OR .w_CADOBFIN>=i_DatSys) AND (EMPTY(.w_CACODINI) OR .w_CACODINI<=.w_CACODFIN))  and not(IsAlt())  and (!IsAlt())  and not(empty(.w_CACODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODFIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di ricerca non valido, obsoleto oppure codice finale minore del codice iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      this.Calculate_QTYFFRAAXR()
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LetturaParAlte = this.w_LetturaParAlte
    return

enddefine

* --- Define pages as container
define class tgsag_kipPag1 as StdContainer
  Width  = 648
  height = 522
  stdWidth  = 648
  stdheight = 522
  resizeXpos=544
  resizeYpos=239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCACODINI_1_1 as StdField with uid="FFIMPKLFUQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CACODINI", cQueryName = "CACODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di ricerca non valido, obsoleto oppure codice iniziale maggiore del codice finale",;
    HelpContextID = 39235985,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=134, Top=7, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_CACODINI"

  func oCACODINI_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oCACODINI_1_1.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCACODINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCACODINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Servizi",'GSAGZKPM.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCACODINI_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CACODINI
     i_obj.ecpSave()
  endproc

  add object oCACODFIN_1_2 as StdField with uid="AOIHXNVPYJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CACODFIN", cQueryName = "CACODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di ricerca non valido, obsoleto oppure codice finale minore del codice iniziale",;
    HelpContextID = 178867828,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=134, Top=33, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_CACODFIN"

  func oCACODFIN_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oCACODFIN_1_2.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oCACODFIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODFIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODFIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCACODFIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Servizi",'GSAGZKPM.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCACODFIN_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CACODFIN
     i_obj.ecpSave()
  endproc

  add object oDESCR_1_3 as AH_SEARCHFLD with uid="KVDDVWNUZP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCR", cQueryName = "DESCR",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione prestazione o parte di essa",;
    HelpContextID = 92164662,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=134, Top=7, InputMask=replicate('X',40)

  func oDESCR_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oDESCR_1_3.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDESCRSUPP_1_4 as AH_SEARCHMEMO with uid="HIKHGEICJP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCRSUPP", cQueryName = "DESCRSUPP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva prestazione o parte di essa",;
    HelpContextID = 142497670,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=134, Top=33

  func oDESCRSUPP_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oDESCRSUPP_1_4.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oTIPOLOGIA_1_5 as StdCombo with uid="BBZBIAJDXM",rtseq=5,rtrep=.f.,left=134,top=58,width=144,height=22;
    , ToolTipText = "Classificazione prestazione";
    , HelpContextID = 69872527;
    , cFormVar="w_TIPOLOGIA",RowSource=""+"Onorario civile,"+"Onorario penale,"+"Onorario stragiudiziale,"+"Diritto stragiudiziale,"+"Diritto civile,"+"Prestazione generica,"+"Prestazione a tempo,"+"Spesa,"+"Anticipazione,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOLOGIA_1_5.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    iif(this.value =3,'T',;
    iif(this.value =4,'R',;
    iif(this.value =5,'C',;
    iif(this.value =6,'G',;
    iif(this.value =7,'P',;
    iif(this.value =8,'S',;
    iif(this.value =9,'A',;
    iif(this.value =10,'K',;
    space(1))))))))))))
  endfunc
  func oTIPOLOGIA_1_5.GetRadio()
    this.Parent.oContained.w_TIPOLOGIA = this.RadioValue()
    return .t.
  endfunc

  func oTIPOLOGIA_1_5.SetRadio()
    this.Parent.oContained.w_TIPOLOGIA=trim(this.Parent.oContained.w_TIPOLOGIA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOLOGIA=='I',1,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='E',2,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='T',3,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='R',4,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='C',5,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='G',6,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='P',7,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='S',8,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='A',9,;
      iif(this.Parent.oContained.w_TIPOLOGIA=='K',10,;
      0))))))))))
  endfunc

  func oTIPOLOGIA_1_5.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oSELRAGG_1_6 as StdField with uid="KEJKCSVKFW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SELRAGG", cQueryName = "SELRAGG",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Raggruppamento prestazioni",;
    HelpContextID = 192733990,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=487, Top=58, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRE_ITER", cZoomOnZoom="GSAG_MIT", oKey_1_1="ITCODICE", oKey_1_2="this.w_SELRAGG"

  func oSELRAGG_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELRAGG_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELRAGG_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRE_ITER','*','ITCODICE',cp_AbsName(this.parent,'oSELRAGG_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MIT',"Raggruppamenti prestazioni",'GSAG_MIT.PRE_ITER_VZM',this.parent.oContained
  endproc
  proc oSELRAGG_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MIT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ITCODICE=this.parent.oContained.w_SELRAGG
     i_obj.ecpSave()
  endproc


  add object oBtn_1_7 as StdButton with uid="PHEWMSSLEL",left=591, top=8, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 90081514;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="ERHOXVEEGI",left=540, top=469, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 1460454;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="TKCMXZXILU",left=591, top=469, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 8749126;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AGKPM_ZOOM as cp_szoombox with uid="CKWGSYWNMA",left=4, top=84, width=644,height=368,;
    caption='AGKPM_ZOOM',;
   bGlobalFont=.t.,;
    cTable="KEY_ARTI",cZoomFile="GSAG_KIP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,;
    cEvent = "Ricerca,w_SELRAGG Changed",;
    nPag=1;
    , HelpContextID = 70985285

  add object oCADESINI_1_19 as StdField with uid="VHYSFMYIRX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CADESINI", cQueryName = "CADESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 24158609,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=305, Top=7, InputMask=replicate('X',40)

  func oCADESINI_1_19.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCADESFIN_1_21 as StdField with uid="VJSBLGKGMF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CADESFIN", cQueryName = "CADESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 193945204,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=305, Top=33, InputMask=replicate('X',40)

  func oCADESFIN_1_21.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oSELEZI_1_28 as StdRadio with uid="VNICEXLLTR",rtseq=18,rtrep=.f.,left=15, top=460, width=127,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_28.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 16784602
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 16784602
      this.Buttons(2).Top=15
      this.SetAll("Width",125)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_28.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_28.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_1_30 as cp_runprogram with uid="JEGFKSDCLA",left=7, top=550, width=202,height=20,;
    caption='GSAG_BSL',;
   bGlobalFont=.t.,;
    prg="GSAG_BSL",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 128892750


  add object oBtn_1_32 as StdButton with uid="RJYBUCOVMY",left=230, top=469, width=48,height=45,;
    CpPicture="bmp\SALDI.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza la tariffa";
    , HelpContextID = 51981000;
    , caption='\<Tariffario';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        GSAR_BCR(this.Parent.oContained,"T")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CACODART))
      endwith
    endif
  endfunc

  func oBtn_1_32.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_CACODICE) or !Isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_33 as StdButton with uid="ARHYBOCNYM",left=282, top=469, width=48,height=45,;
    CpPicture="bmp\CODICI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il codice di ricerca voce del tariffario";
    , HelpContextID = 4879322;
    , caption='\<Codici';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSAR_BCR(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CACODICE))
      endwith
    endif
  endfunc

  func oBtn_1_33.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_CACODICE) or !Isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="QJTUZLZBDL",left=336, top=469, width=48,height=45,;
    CpPicture="bmp\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Nuova tariffa";
    , HelpContextID = 126042838;
    , caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSAR_BCR(this.Parent.oContained,"N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!Isalt())
     endwith
    endif
  endfunc

  add object oStr_1_10 as StdString with uid="YKTEKQIXND",Visible=.t., Left=4, Top=9,;
    Alignment=1, Width=127, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="EKPXIOSQAT",Visible=.t., Left=4, Top=35,;
    Alignment=1, Width=127, Height=18,;
    Caption="Descrizione suppl.:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="LTSCZWRIFR",Visible=.t., Left=34, Top=60,;
    Alignment=1, Width=95, Height=18,;
    Caption="Classificazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="UYQPQOKTUT",Visible=.t., Left=49, Top=9,;
    Alignment=1, Width=82, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="TXQNIIJISJ",Visible=.t., Left=20, Top=35,;
    Alignment=1, Width=111, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="GJFVMANWYG",Visible=.t., Left=380, Top=60,;
    Alignment=1, Width=103, Height=18,;
    Caption="Raggruppamento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kip','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
