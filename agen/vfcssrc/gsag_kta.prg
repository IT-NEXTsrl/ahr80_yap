* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kta                                                        *
*              Duplicazione tipo attivit�                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-09                                                      *
* Last revis.: 2009-06-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kta",oParentObject))

* --- Class definition
define class tgsag_kta as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 648
  Height = 170
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-06-17"
  HelpContextID=82454377
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_kta"
  cComment = "Duplicazione tipo attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPATORIG = space(20)
  w_DESATORIG = space(254)
  w_CODTIPATT = space(20)
  o_CODTIPATT = space(20)
  w_DESTIPATT = space(254)
  w_DUPLPRES = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_ktaPag1","gsag_kta",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODTIPATT_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSAG_BTA(this,"D")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPATORIG=space(20)
      .w_DESATORIG=space(254)
      .w_CODTIPATT=space(20)
      .w_DESTIPATT=space(254)
      .w_DUPLPRES=space(1)
        .w_TIPATORIG = this.oParentObject.w_CACODICE
        .w_DESATORIG = this.oParentObject.w_CADESCRI
          .DoRTCalc(3,3,.f.)
        .w_DESTIPATT = this.oParentObject.w_CADESCRI
        .w_DUPLPRES = IIF(this.oParentObject.w_CAFLNSAP='N', 'S', 'N')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_CODTIPATT<>.w_CODTIPATT
          .Calculate_ETRURKGOHO()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_ETRURKGOHO()
    with this
          * --- Controllo esistenza codice tipo attivit�
          GSAG_BTA(this;
              ,'C';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDUPLPRES_1_7.enabled = this.oPgFrm.Page1.oPag.oDUPLPRES_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPATORIG_1_2.value==this.w_TIPATORIG)
      this.oPgFrm.Page1.oPag.oTIPATORIG_1_2.value=this.w_TIPATORIG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATORIG_1_3.value==this.w_DESATORIG)
      this.oPgFrm.Page1.oPag.oDESATORIG_1_3.value=this.w_DESATORIG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODTIPATT_1_5.value==this.w_CODTIPATT)
      this.oPgFrm.Page1.oPag.oCODTIPATT_1_5.value=this.w_CODTIPATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIPATT_1_6.value==this.w_DESTIPATT)
      this.oPgFrm.Page1.oPag.oDESTIPATT_1_6.value=this.w_DESTIPATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDUPLPRES_1_7.RadioValue()==this.w_DUPLPRES)
      this.oPgFrm.Page1.oPag.oDUPLPRES_1_7.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsag_kta
      IF i_bRes=.t.
          if EMPTY(.w_CODTIPATT)
            ah_ErrorMsg("Inserire il codice del tipo attivit� da creare",,'')
            i_bRes=.f.
          endif
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODTIPATT = this.w_CODTIPATT
    return

enddefine

* --- Define pages as container
define class tgsag_ktaPag1 as StdContainer
  Width  = 644
  height = 170
  stdWidth  = 644
  stdheight = 170
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPATORIG_1_2 as StdField with uid="PFFUTAYNDX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TIPATORIG", cQueryName = "TIPATORIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 6542353,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=154, Top=21, InputMask=replicate('X',20)

  add object oDESATORIG_1_3 as StdField with uid="DPONJCUXHZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESATORIG", cQueryName = "DESATORIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 6531345,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=314, Top=21, InputMask=replicate('X',254)

  add object oCODTIPATT_1_5 as StdField with uid="GNIQOCSBJH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODTIPATT", cQueryName = "CODTIPATT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipo attivit� da creare",;
    HelpContextID = 101958,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=154, Top=53, InputMask=replicate('X',20)

  add object oDESTIPATT_1_6 as StdField with uid="FEJIRLBWUB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESTIPATT", cQueryName = "DESTIPATT",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto tipo attivit� da creare",;
    HelpContextID = 43062,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=314, Top=53, InputMask=replicate('X',254)

  add object oDUPLPRES_1_7 as StdCheck with uid="QXNLJDAWZN",rtseq=5,rtrep=.f.,left=154, top=85, caption="Duplica prestazioni",;
    ToolTipText = "Se attivo: saranno duplicate le prestazioni del tipo attivit� di origine",;
    HelpContextID = 40317577,;
    cFormVar="w_DUPLPRES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDUPLPRES_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDUPLPRES_1_7.GetRadio()
    this.Parent.oContained.w_DUPLPRES = this.RadioValue()
    return .t.
  endfunc

  func oDUPLPRES_1_7.SetRadio()
    this.Parent.oContained.w_DUPLPRES=trim(this.Parent.oContained.w_DUPLPRES)
    this.value = ;
      iif(this.Parent.oContained.w_DUPLPRES=='S',1,;
      0)
  endfunc

  func oDUPLPRES_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (this.parent.oContained.oParentObject.w_CAFLNSAP='N')
    endwith
   endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="UUNSDLSMLD",left=532, top=120, width=48,height=46,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 82425626;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="TKCMXZXILU",left=587, top=120, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 75136954;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="MWRDSLDLLN",Visible=.t., Left=13, Top=21,;
    Alignment=1, Width=135, Height=18,;
    Caption="Tipo attivit� di origine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="JEEVCYVQRW",Visible=.t., Left=7, Top=56,;
    Alignment=1, Width=141, Height=18,;
    Caption="Tipo attivit� da creare:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kta','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
