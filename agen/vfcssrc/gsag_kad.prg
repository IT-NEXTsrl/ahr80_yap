* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kad                                                        *
*              Aggiornamento data in attivit� collegate                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-03-07                                                      *
* Last revis.: 2010-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kad",oParentObject))

* --- Class definition
define class tgsag_kad as StdForm
  Top    = 37
  Left   = 46

  * --- Standard Properties
  Width  = 698
  Height = 438
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-02-11"
  HelpContextID=132786025
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  PAR_AGEN_IDX = 0
  cPrg = "gsag_kad"
  cComment = "Aggiornamento data in attivit� collegate"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PAR_AGEN = space(5)
  w_PACHKFES = space(1)
  w_SERIAL = space(20)
  w_DATA_INI = ctot('')
  w_CAITER = space(20)
  w_SELROW = 0
  w_RESCHK = 0
  w_DESC_ATT = space(254)
  w_DES_PRAT = space(100)
  w_CACHKINI = space(1)
  o_CACHKINI = space(1)
  w_CACHKFOR = space(1)
  w_AGKAD_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kadPag1","gsag_kad",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCACHKINI_1_17
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGKAD_ZOOM = this.oPgFrm.Pages(1).oPag.AGKAD_ZOOM
    DoDefault()
    proc Destroy()
      this.w_AGKAD_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PAR_AGEN'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSAG_BAD(this,"A")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PAR_AGEN=space(5)
      .w_PACHKFES=space(1)
      .w_SERIAL=space(20)
      .w_DATA_INI=ctot("")
      .w_CAITER=space(20)
      .w_SELROW=0
      .w_RESCHK=0
      .w_DESC_ATT=space(254)
      .w_DES_PRAT=space(100)
      .w_CACHKINI=space(1)
      .w_CACHKFOR=space(1)
      .w_SERIAL=oParentObject.w_SERIAL
      .w_DATA_INI=oParentObject.w_DATA_INI
      .w_CAITER=oParentObject.w_CAITER
      .w_DESC_ATT=oParentObject.w_DESC_ATT
      .w_DES_PRAT=oParentObject.w_DES_PRAT
        .w_PAR_AGEN = i_CodAzi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PAR_AGEN))
          .link_1_1('Full')
        endif
      .oPgFrm.Page1.oPag.AGKAD_ZOOM.Calculate()
          .DoRTCalc(2,5,.f.)
        .w_SELROW = 0
        .w_RESCHK = 0
          .DoRTCalc(8,9,.f.)
        .w_CACHKINI = 'T'
        .w_CACHKFOR = iif( IsAlt(), 'S' , ' ')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SERIAL=.w_SERIAL
      .oParentObject.w_DATA_INI=.w_DATA_INI
      .oParentObject.w_CAITER=.w_CAITER
      .oParentObject.w_DESC_ATT=.w_DESC_ATT
      .oParentObject.w_DES_PRAT=.w_DES_PRAT
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .oPgFrm.Page1.oPag.AGKAD_ZOOM.Calculate()
        .DoRTCalc(2,10,.t.)
        if .o_CACHKINI<>.w_CACHKINI
            .w_CACHKFOR = iif( IsAlt(), 'S' , ' ')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AGKAD_ZOOM.Calculate()
    endwith
  return

  proc Calculate_PGWMDAMXMT()
    with this
          * --- Calcolo selrow
          .w_SELROW = iif( upper(this.currentEvent)='W_AGKAD_ZOOM ROW CHECKED' , .w_SELROW+1 ,iif( upper(this.currentEvent)='W_AGKAD_ZOOM ROW UNCHECKED' , .w_SELROW-1 , 0))
    endwith
  endproc
  proc Calculate_PSWLDQHWYO()
    with this
          * --- Seleziona le attivit�
          GSAG_BAD(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_BNMKOBVVDZ()
    with this
          * --- Controlli a checkform
          GSAG_BAD(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_YOINESLRDQ()
    with this
          * --- Aggiorna data iniziale
          GSAG_BAD(this;
              ,'U';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCACHKINI_1_17.enabled = this.oPgFrm.Page1.oPag.oCACHKINI_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCACHKFOR_1_18.enabled = this.oPgFrm.Page1.oPag.oCACHKFOR_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.visible=!this.oPgFrm.Page1.oPag.oBtn_1_6.mHide()
    this.oPgFrm.Page1.oPag.oDES_PRAT_1_16.visible=!this.oPgFrm.Page1.oPag.oDES_PRAT_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCACHKFOR_1_18.visible=!this.oPgFrm.Page1.oPag.oCACHKFOR_1_18.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_20.visible=!this.oPgFrm.Page1.oPag.oBtn_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AGKAD_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_AGKAD_ZOOM before query") or lower(cEvent)==lower("w_AGKAD_ZOOM row checked") or lower(cEvent)==lower("w_AGKAD_ZOOM row unchecked")
          .Calculate_PGWMDAMXMT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_AGKAD_ZOOM after query")
          .Calculate_PSWLDQHWYO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_BNMKOBVVDZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("calcdata")
          .Calculate_YOINESLRDQ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kad
    * --- Al checked/unchecked della riga sullo zoom non � immediatamente visualizzato
    * --- o nascosto il OK completa (la HideControls scatta solo al cambio
    * --- di riga dello zoom, per cui forzo la chiamata).
    If upper(cEvent)='W_AGKAD_ZOOM ROW CHECKED' OR Upper(cEvent)='W_AGKAD_ZOOM ROW UNCHECKED'
        this.mHideControls()
    endif
    If upper(cEvent)='BLANK'
    This.Notifyevent('calcdata')
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PAR_AGEN
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAR_AGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAR_AGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACHKFES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PAR_AGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PAR_AGEN)
            select PACODAZI,PACHKFES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAR_AGEN = NVL(_Link_.PACODAZI,space(5))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PAR_AGEN = space(5)
      endif
      this.w_PACHKFES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAR_AGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATA_INI_1_4.value==this.w_DATA_INI)
      this.oPgFrm.Page1.oPag.oDATA_INI_1_4.value=this.w_DATA_INI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESC_ATT_1_15.value==this.w_DESC_ATT)
      this.oPgFrm.Page1.oPag.oDESC_ATT_1_15.value=this.w_DESC_ATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDES_PRAT_1_16.value==this.w_DES_PRAT)
      this.oPgFrm.Page1.oPag.oDES_PRAT_1_16.value=this.w_DES_PRAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCACHKINI_1_17.RadioValue()==this.w_CACHKINI)
      this.oPgFrm.Page1.oPag.oCACHKINI_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACHKFOR_1_18.RadioValue()==this.w_CACHKFOR)
      this.oPgFrm.Page1.oPag.oCACHKFOR_1_18.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsag_kad
      * --- Controlli Finali
      IF i_bRes=.t.
         .w_RESCHK=0
         .NotifyEvent('ChkFinali')
         WAIT CLEAR
         IF .w_RESCHK<>0
           i_bRes=.f.
         ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CACHKINI = this.w_CACHKINI
    return

enddefine

* --- Define pages as container
define class tgsag_kadPag1 as StdContainer
  Width  = 694
  height = 438
  stdWidth  = 694
  stdheight = 438
  resizeXpos=463
  resizeYpos=235
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATA_INI_1_4 as StdField with uid="QCITKHXZZR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATA_INI", cQueryName = "DATA_INI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 145990017,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=504, Top=14


  add object oBtn_1_6 as StdButton with uid="ERHOXVEEGI",left=585, top=385, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 132757274;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SELROW=0)
     endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="TKCMXZXILU",left=638, top=385, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 125468602;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AGKAD_ZOOM as cp_szoombox with uid="WODZOSNAJX",left=-2, top=101, width=697,height=273,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_ATTI",cZoomFile="GSAG_KAD",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",bqueryondblclick=.f.,cZoomOnZoom="",;
    cEvent = "Blank,Aggiorna",;
    nPag=1;
    , HelpContextID = 45211622

  add object oDESC_ATT_1_15 as StdField with uid="CJTQQCRMVY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESC_ATT", cQueryName = "DESC_ATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 256791178,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=126, Top=14, InputMask=replicate('X',254)

  add object oDES_PRAT_1_16 as StdField with uid="XNUFOXWBGI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DES_PRAT", cQueryName = "DES_PRAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 259674762,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=126, Top=38, InputMask=replicate('X',100)

  func oDES_PRAT_1_16.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oCACHKINI_1_17 as StdCombo with uid="JDBQLUXVZW",rtseq=10,rtrep=.f.,left=126,top=61,width=133,height=21;
    , ToolTipText = "Metodo di calcolo della data inizio attivit�";
    , HelpContextID = 166572433;
    , cFormVar="w_CACHKINI",RowSource=""+"Da tipo attivit�,"+"Con conferma,"+"Automatica,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCACHKINI_1_17.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    iif(this.value =3,'A',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oCACHKINI_1_17.GetRadio()
    this.Parent.oContained.w_CACHKINI = this.RadioValue()
    return .t.
  endfunc

  func oCACHKINI_1_17.SetRadio()
    this.Parent.oContained.w_CACHKINI=trim(this.Parent.oContained.w_CACHKINI)
    this.value = ;
      iif(this.Parent.oContained.w_CACHKINI=='T',1,;
      iif(this.Parent.oContained.w_CACHKINI=='C',2,;
      iif(this.Parent.oContained.w_CACHKINI=='A',3,;
      iif(this.Parent.oContained.w_CACHKINI=='N',4,;
      0))))
  endfunc

  func oCACHKINI_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PACHKFES<>'N')
    endwith
   endif
  endfunc

  add object oCACHKFOR_1_18 as StdCheck with uid="SZVHUJOINL",rtseq=11,rtrep=.f.,left=317, top=61, caption="Sospensioni forensi",;
    ToolTipText = "Se attivo nella determinazione della data inizio delle attivit� tiene conto anche della sospensione forense",;
    HelpContextID = 216904072,;
    cFormVar="w_CACHKFOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCACHKFOR_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCACHKFOR_1_18.GetRadio()
    this.Parent.oContained.w_CACHKFOR = this.RadioValue()
    return .t.
  endfunc

  func oCACHKFOR_1_18.SetRadio()
    this.Parent.oContained.w_CACHKFOR=trim(this.Parent.oContained.w_CACHKFOR)
    this.value = ;
      iif(this.Parent.oContained.w_CACHKFOR=='S',1,;
      0)
  endfunc

  func oCACHKFOR_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PACHKFES<>'N' And .w_PACHKFES<>'T')
    endwith
   endif
  endfunc

  func oCACHKFOR_1_18.mHide()
    with this.Parent.oContained
      return (not IsAlt() Or .w_CACHKINI='N' Or .w_CACHKINI='T')
    endwith
  endfunc


  add object oBtn_1_20 as StdButton with uid="XINTTQHTOF",left=638, top=44, width=48,height=45,;
    CpPicture="bmp\refresh.bmp", caption="", nPag=1;
    , ToolTipText = "Aggiorna data spostamento in base al calcolo previsto per la data iniziale";
    , HelpContextID = 24491111;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        gsag_bad(this.Parent.oContained,"U")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SELROW=0)
     endwith
    endif
  endfunc

  add object oStr_1_9 as StdString with uid="OZATNEHMGU",Visible=.t., Left=20, Top=91,;
    Alignment=0, Width=132, Height=18,;
    Caption="Attivit� collegate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="PFVPIBIGVQ",Visible=.t., Left=7, Top=14,;
    Alignment=1, Width=113, Height=18,;
    Caption="Attivit� di partenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="COTKAPENGA",Visible=.t., Left=458, Top=14,;
    Alignment=1, Width=37, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="FGAPFHFYQF",Visible=.t., Left=63, Top=38,;
    Alignment=1, Width=57, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="LGFPONFHBX",Visible=.t., Left=20, Top=412,;
    Alignment=0, Width=346, Height=18,;
    Caption="In rosso: le attivit� collegate con data ricalcolata"    , ForeColor=RGB(255,0,0);
  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ASITDWTOLS",Visible=.t., Left=12, Top=61,;
    Alignment=1, Width=108, Height=18,;
    Caption="Calcolo data:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kad','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
