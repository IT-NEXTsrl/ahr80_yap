* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bii                                                        *
*              Gestione attivit� importate ICS\VCS                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-12-17                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bii",oParentObject,m.pEXEC)
return(i_retval)

define class tgsag_bii as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_OBJ = .NULL.
  w_OKATT = .f.
  w_STATO = space(1)
  w_UFFICIO = space(10)
  w_ATSERIAL = space(20)
  w_ATCAITER = space(20)
  w_SERIAL = space(20)
  w_PATIPRIS = space(1)
  w_PACODRIS = space(5)
  w_PAGRURIS = space(5)
  w_ATCAUATT = space(20)
  w_READAZI = space(5)
  w_DATINI = ctod("  /  /  ")
  w_DESUTE = space(100)
  w_ATCODPRA = space(15)
  w_ATOGGETT = space(80)
  w_ORAINI = space(2)
  w_MININI = space(2)
  w_CAORASYS = space(1)
  w_RESULT = space(100)
  w_CADTIN = ctot("")
  w_CODRIS = space(5)
  w_TIPRIS = space(1)
  w_DPGRUPRE = space(5)
  w_DATOBSO = ctod("  /  /  ")
  w_RESULT = space(100)
  w_OKPART = .f.
  w_SEREVE = space(150)
  w_DATIMP = ctot("")
  w_oERRORMESS = .NULL.
  w_OKGEN = .f.
  w_AT___FAX = space(18)
  w_AT_EMAIL = space(0)
  w_AT_EMPEC = space(0)
  w_ATTELEFO = space(18)
  w_ATCELLUL = space(18)
  w_CNCALDIR = space(1)
  w_CNCOECAL = 0
  w_CNFLAPON = space(1)
  w_CNFLVALO = space(1)
  w_CNIMPORT = 0
  w_CODPRAT = space(15)
  w_COMODO = space(10)
  w_DATOBSONOM = ctod("  /  /  ")
  w_ENTE = space(10)
  w_FLAMPA = space(1)
  w_LISPRA = space(5)
  w_LOC_NOM = space(25)
  w_LOC_PRA = space(25)
  w_LOCALI = space(25)
  w_PARASS = 0
  w_OFDATDOC = ctod("  /  /  ")
  w_TIPOPRAT = space(10)
  w_ATTCOS = space(15)
  w_ATTRIC = space(15)
  w_CAUATT = space(5)
  w_CC_CONTO = space(15)
  w_CCDESPIA = space(40)
  w_CCDESRIC = space(40)
  w_CENOBSO = ctod("  /  /  ")
  w_CENRIC = space(15)
  w_CODCOS = space(5)
  w_CODNOM = space(15)
  w_COMRIC = space(15)
  w_DPTIPRIS = space(1)
  w_PATIPRIS = space(1)
  w_PERSON = space(60)
  w_TIPATT = space(10)
  w_CACODICE = space(20)
  w_CATEGO = space(1)
  w_OKRES = .f.
  w_OKDEL = .f.
  w_OKATT = .f.
  w_RESULT = space(100)
  w_ATLOCALI = space(50)
  w_ATNUMPRI = 0
  w_ATOGGETT = space(80)
  w_ATNOTPIA = space(0)
  w_ATDATINI = ctot("")
  w_ATDATFIN = ctot("")
  w_CO__NOTE = space(0)
  * --- WorkFile variables
  OFF_PART_idx=0
  OFFDATTI_idx=0
  COM_ATTI_idx=0
  OFF_ATTI_idx=0
  DIPENDEN_idx=0
  PAR_AGEN_idx=0
  CAUMATTI_idx=0
  CAN_TIER_idx=0
  DET_EXPO_idx=0
  DET_IMPO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da GSAG_KII
    *     A: Aggiorna
    *     N:Crea
    *     E:Escludi
    *     C:Cancella
    this.w_oERRORMESS=createobject("AH_ErrorLog")
     
 Select XCHK from (this.oParentObject.w_ZOOMIMPO.Ccursor) where XCHK=1 into cursor TMP_SEL
    this.w_OKRES = Reccount("TMP_SEL")<>0
    Use in TMP_SEL
    if ! this.w_OKRES
      Ah_errormsg("Attenzione nessun evento selezionato")
      i_retcode = 'stop'
      return
    endif
    if this.pEXEC $ "A-N"
      * --- Testo la presenza di almeno una riga nuova
      this.w_CATEGO = IIF(this.oParentObject.w_FLGCAT="N"," ",this.oParentObject.w_FLGCAT)
      if this.pEXEC="N"
        do gsag_kia with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if ! this.w_OKGEN
          Ah_errormsg("Elaborazione abbandonata")
          i_retcode = 'stop'
          return
        endif
        if Isalt() and Empty(this.w_CODPRAT)
          Ah_errormsg("Attenzione codice pratica non inserito")
          i_retcode = 'stop'
          return
        endif
        this.w_OBJ = CREATEOBJECT("ActivityWriter",this,"OFF_ATTI")
        * --- Informazioni che devo modificare da attivit� a attivit�
        this.w_OBJ.w_ATCODVAL = g_PERVAL
        this.w_OBJ.w_ATCAUATT = this.w_CACODICE
        this.w_OBJ.w_ATCODNOM = this.w_CODNOM
        this.w_OBJ.w_ATCODPRA = this.w_CODPRAT
        this.w_OBJ.w_ATCOMRIC = this.w_COMRIC
        this.w_OBJ.w_UTCC = i_CodUte
        this.w_OBJ.w_UTDC = SetInfoDate( g_CALUTD )
        this.w_OBJ.w_AT__ENTE = this.w_ENTE
        this.w_OBJ.w_ATUFFICI = this.w_UFFICIO
        this.w_OBJ.w_ATCODUID = this.w_SEREVE
      endif
    endif
    if this.pEXEC = "C"
      this.w_OKDEL = Ah_Yesno("Procedere con l'eliminazione dei record selezionati?")
      if this.w_OKDEL
        this.w_OKATT = Ah_Yesno("Attenzione! Cancellare anche attivit� collegate agli eventi ICS eliminati?")
      endif
    endif
    if this.pEXEC = "E"
      this.w_OKDEL = Ah_Yesno("Procedere con l'esclusione dei record selezionati?")
    endif
    * --- Data di creazione attvit�
    this.w_DATIMP = Datetime()
     
 Select (this.oParentObject.w_ZOOMIMPO.Ccursor) 
 Go top 
 Scan for XCHK=1
    this.w_SEREVE = Nvl(PICODUID," ")
    this.w_STATO = Nvl(STATO," ")
    do case
      case this.pEXEC= "E"
        if this.w_OKDEL
          * --- Try
          local bErr_039DDCE0
          bErr_039DDCE0=bTrsErr
          this.Try_039DDCE0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Riscontrato errore in aggiornamento attivit� relativa all'evento %1%0%2", space(4)+ALLTRIM(this.w_SEREVE),Message())     
          endif
          bTrsErr=bTrsErr or bErr_039DDCE0
          * --- End
        endif
      case this.pEXEC= "C"
        if this.w_okdel
          this.w_RESULT = ""
          * --- Try
          local bErr_039DD380
          bErr_039DD380=bTrsErr
          this.Try_039DD380()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            if Empty(this.w_RESULT)
              this.w_RESULT = Message()
            endif
            this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Riscontrato errore in cancellazione attivit� relativa all'evento %1%0%2", space(4)+ALLTRIM(this.w_SEREVE),this.w_RESULT)     
          endif
          bTrsErr=bTrsErr or bErr_039DD380
          * --- End
        endif
      case this.pEXEC= "I"
        * --- Try
        local bErr_039DD530
        bErr_039DD530=bTrsErr
        this.Try_039DD530()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Riscontrato errore in aggiornamento attivit� relativa all'evento %1%0%2", space(4)+ALLTRIM(this.w_SEREVE),Message())     
        endif
        bTrsErr=bTrsErr or bErr_039DD530
        * --- End
      otherwise
        * --- 3 -> 5-8 Normale
        *     1 ->1-4 Urgente
        *     4  ->9 Scadenza termine
         
 Select (this.oParentObject.w_ZOOMIMPO.Ccursor)
        this.w_ATNUMPRI = icase(Nvl(PINUMPRI ,0)<=4,1,Nvl(PINUMPRI ,0)<=8 and Nvl(PINUMPRI ,0)>4,3,4)
        do case
          case this.w_STATO=="N"
            * --- Solo se attivit� nuove
            this.w_OBJ.Blank()     
            if Used("TMP_PART")
               
 Select TMP_PART 
 Go top 
 Scan for XCHK=1
              this.w_OBJ.AddPart(0,0,DPTIPRIS,DPCODICE,DPGRUPRE)     
              this.w_OBJ.w_OFF_PART.SaveCurrentRecord()     
              this.w_OKPART = .T.
               
 Endscan
            endif
            if ! this.w_OKPART
              Ah_errormsg("Attenzione, nessun partecipante selezionato")
              this.w_OBJ = .Null.
              i_retcode = 'stop'
              return
            endif
            this.w_OBJ.w_ATCAUATT = this.w_CACODICE
            this.w_OBJ.w_ATCODNOM = this.w_CODNOM
            this.w_OBJ.w_ATCODPRA = this.w_CODPRAT
            this.w_OBJ.w_ATCENCOS = this.w_CC_CONTO
            this.w_OBJ.w_ATCENRIC = this.w_CENRIC
            this.w_OBJ.w_ATATTRIC = this.w_ATTRIC
            this.w_OBJ.w_ATATTCOS = this.w_ATTCOS
            this.w_OBJ.w_ATCOMRIC = this.w_COMRIC
            this.w_OBJ.w_UTCC = i_CodUte
            this.w_OBJ.w_UTDC = SetInfoDate( g_CALUTD )
            this.w_OBJ.w_AT__ENTE = this.w_ENTE
            this.w_OBJ.w_ATUFFICI = this.w_UFFICIO
            this.w_RESULT = ""
            this.w_OBJ.w_ATNUMPRI = this.w_ATNUMPRI
            this.w_OBJ.w_ATSTATUS = " "
             
 Select (this.oParentObject.w_ZOOMIMPO.Ccursor)
            this.w_OBJ.w_ATLOCALI = Nvl(PILOCALI," ")
            this.w_OBJ.w_ATOGGETT = Nvl(PIOGGETT ," ")
            this.w_OBJ.w_ATNOTPIA = Nvl(PIDESCRI, " ")
            this.w_OBJ.w_ATSERIAL = Space(20)
            this.w_OBJ.w_ATDATINI = PIDATINI
            this.w_OBJ.w_ATDATFIN = PIDATFIN
            this.w_RESULT = this.w_OBJ.CreateActivity()
            if Empty(this.w_RESULT)
              if Not EMpty(this.w_OBJ.w_ATSERIAL)
                * --- Creo legame con UID
                * --- Try
                local bErr_039C3F98
                bErr_039C3F98=bTrsErr
                this.Try_039C3F98()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_039C3F98
                * --- End
              endif
            else
              this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Riscontrato errore in generazione attivit� relativa all'evento %1%0%2", space(4)+ALLTRIM(this.w_SEREVE),ALLTRIM(this.w_RESULT))     
            endif
          case this.w_STATO=="M"
            * --- Cerco seriale attivit� da aggiornare
            this.w_ATLOCALI = Nvl(PILOCALI," ")
            this.w_ATOGGETT = Nvl(PIOGGETT ," ")
            this.w_ATNOTPIA = Nvl(PIDESCRI, " ")
            this.w_ATDATINI = PIDATINI
            this.w_ATDATFIN = PIDATFIN
            this.w_ATSERIAL = Nvl(COSERATT," ")
            if Not Empty(this.w_ATSERIAL)
              * --- Try
              local bErr_03A27B60
              bErr_03A27B60=bTrsErr
              this.Try_03A27B60()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Riscontrato errore in aggiornamento attivit� relativa all'evento %1%0%2", space(4)+ALLTRIM(this.w_SEREVE),Message())     
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_03A27B60
              * --- End
            endif
        endcase
        this.w_OKATT = .T.
    endcase
     
 Endscan
    if Used("TMP_PART")
       
 Select TMP_PART 
 Use
    endif
    if this.pEXEC $ "A-N"
      if !this.w_OKATT
        Ah_errormsg("Attenzione, nessuna attivit�  selezionata")
        this.w_OBJ = .Null.
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_oERRORMESS.PrintLog(this,"Errori /warning riscontrati")     
    This.oparentobject.Notifyevent("Ricerca")
    this.w_oERRORMESS = .Null.
    this.w_OBJ = .Null.
  endproc
  proc Try_039DDCE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DET_IMPO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DET_IMPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DET_IMPO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PI_STATO ="+cp_NullLink(cp_ToStrODBC("E"),'DET_IMPO','PI_STATO');
          +i_ccchkf ;
      +" where ";
          +"PICODPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
          +" and PICODUID = "+cp_ToStrODBC(this.w_SEREVE);
             )
    else
      update (i_cTable) set;
          PI_STATO = "E";
          &i_ccchkf. ;
       where;
          PICODPRO = this.oParentObject.w_SERPRO;
          and PICODUID = this.w_SEREVE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_039DD380()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Delete from DET_IMPO
    i_nConn=i_TableProp[this.DET_IMPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PICODPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
            +" and PICODUID = "+cp_ToStrODBC(this.w_SEREVE);
             )
    else
      delete from (i_cTable) where;
            PICODPRO = this.oParentObject.w_SERPRO;
            and PICODUID = this.w_SEREVE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if this.w_OKATT
      * --- Read from DET_EXPO
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DET_EXPO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DET_EXPO_idx,2],.t.,this.DET_EXPO_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COSERATT"+;
          " from "+i_cTable+" DET_EXPO where ";
              +"COSEREVE = "+cp_ToStrODBC(this.w_SEREVE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COSERATT;
          from (i_cTable) where;
              COSEREVE = this.w_SEREVE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ATSERIAL = NVL(cp_ToDate(_read_.COSERATT),cp_NullValue(_read_.COSERATT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not Empty(this.w_ATSERIAL)
        this.w_RESULT = gsag_bda(.Null.,this.w_ATSERIAL)
        if Not Empty(this.w_RESULT)
          i_retcode = 'stop'
          i_retval = "Transazione abbandonata"
          return
        else
          * --- Delete from DET_EXPO
          i_nConn=i_TableProp[this.DET_EXPO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DET_EXPO_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"COSERPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
                  +" and COSERATT = "+cp_ToStrODBC(this.w_ATSERIAL);
                   )
          else
            delete from (i_cTable) where;
                  COSERPRO = this.oParentObject.w_SERPRO;
                  and COSERATT = this.w_ATSERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_039DD530()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DET_EXPO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DET_EXPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_EXPO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DET_EXPO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"COFLGIGN ="+cp_NullLink(cp_ToStrODBC("S"),'DET_EXPO','COFLGIGN');
          +i_ccchkf ;
      +" where ";
          +"COSERPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
          +" and COSEREVE = "+cp_ToStrODBC(this.w_SEREVE);
             )
    else
      update (i_cTable) set;
          COFLGIGN = "S";
          &i_ccchkf. ;
       where;
          COSERPRO = this.oParentObject.w_SERPRO;
          and COSEREVE = this.w_SEREVE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Delete from DET_IMPO
    i_nConn=i_TableProp[this.DET_IMPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PICODPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
            +" and PICODUID = "+cp_ToStrODBC(this.w_SEREVE);
             )
    else
      delete from (i_cTable) where;
            PICODPRO = this.oParentObject.w_SERPRO;
            and PICODUID = this.w_SEREVE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_039C3F98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Elimino record dal dettaglio importazione
    * --- Read from DET_EXPO
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DET_EXPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_EXPO_idx,2],.t.,this.DET_EXPO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CO__NOTE"+;
        " from "+i_cTable+" DET_EXPO where ";
            +"COSERPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
            +" and COSEREVE = "+cp_ToStrODBC(this.w_SEREVE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CO__NOTE;
        from (i_cTable) where;
            COSERPRO = this.oParentObject.w_SERPRO;
            and COSEREVE = this.w_SEREVE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CO__NOTE = NVL(cp_ToDate(_read_.CO__NOTE),cp_NullValue(_read_.CO__NOTE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Delete from DET_IMPO
    i_nConn=i_TableProp[this.DET_IMPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PICODPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
            +" and PICODUID = "+cp_ToStrODBC(this.w_SEREVE);
             )
    else
      delete from (i_cTable) where;
            PICODPRO = this.oParentObject.w_SERPRO;
            and PICODUID = this.w_SEREVE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from DET_EXPO
    i_nConn=i_TableProp[this.DET_EXPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_EXPO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"COSERPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
            +" and COSEREVE = "+cp_ToStrODBC(this.w_SEREVE);
             )
    else
      delete from (i_cTable) where;
            COSERPRO = this.oParentObject.w_SERPRO;
            and COSEREVE = this.w_SEREVE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Inserisco record nel dettaglio attivit�
    * --- Insert into DET_EXPO
    i_nConn=i_TableProp[this.DET_EXPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_EXPO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_EXPO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"COSERPRO"+",COSERATT"+",COSEREVE"+",CODATESP"+",CO__NOTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERPRO),'DET_EXPO','COSERPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OBJ.w_ATSERIAL),'DET_EXPO','COSERATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SEREVE),'DET_EXPO','COSEREVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATIMP),'DET_EXPO','CODATESP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CO__NOTE),'DET_EXPO','CO__NOTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'COSERPRO',this.oParentObject.w_SERPRO,'COSERATT',this.w_OBJ.w_ATSERIAL,'COSEREVE',this.w_SEREVE,'CODATESP',this.w_DATIMP,'CO__NOTE',this.w_CO__NOTE)
      insert into (i_cTable) (COSERPRO,COSERATT,COSEREVE,CODATESP,CO__NOTE &i_ccchkf. );
         values (;
           this.oParentObject.w_SERPRO;
           ,this.w_OBJ.w_ATSERIAL;
           ,this.w_SEREVE;
           ,this.w_DATIMP;
           ,this.w_CO__NOTE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03A27B60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATLOCALI ="+cp_NullLink(cp_ToStrODBC(this.w_ATLOCALI),'OFF_ATTI','ATLOCALI');
      +",ATNUMPRI ="+cp_NullLink(cp_ToStrODBC(this.w_ATNUMPRI),'OFF_ATTI','ATNUMPRI');
      +",ATOGGETT ="+cp_NullLink(cp_ToStrODBC(this.w_ATOGGETT),'OFF_ATTI','ATOGGETT');
      +",ATNOTPIA ="+cp_NullLink(cp_ToStrODBC(this.w_ATNOTPIA),'OFF_ATTI','ATNOTPIA');
      +",ATDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_ATDATFIN),'OFF_ATTI','ATDATFIN');
      +",ATDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_ATDATINI),'OFF_ATTI','ATDATINI');
          +i_ccchkf ;
      +" where ";
          +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
             )
    else
      update (i_cTable) set;
          ATLOCALI = this.w_ATLOCALI;
          ,ATNUMPRI = this.w_ATNUMPRI;
          ,ATOGGETT = this.w_ATOGGETT;
          ,ATNOTPIA = this.w_ATNOTPIA;
          ,ATDATFIN = this.w_ATDATFIN;
          ,ATDATINI = this.w_ATDATINI;
          &i_ccchkf. ;
       where;
          ATSERIAL = this.w_ATSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Delete from DET_IMPO
    i_nConn=i_TableProp[this.DET_IMPO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_IMPO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PICODPRO = "+cp_ToStrODBC(this.oParentObject.w_SERPRO);
            +" and PICODUID = "+cp_ToStrODBC(this.w_SEREVE);
             )
    else
      delete from (i_cTable) where;
            PICODPRO = this.oParentObject.w_SERPRO;
            and PICODUID = this.w_SEREVE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='OFF_PART'
    this.cWorkTables[2]='OFFDATTI'
    this.cWorkTables[3]='COM_ATTI'
    this.cWorkTables[4]='OFF_ATTI'
    this.cWorkTables[5]='DIPENDEN'
    this.cWorkTables[6]='PAR_AGEN'
    this.cWorkTables[7]='CAUMATTI'
    this.cWorkTables[8]='CAN_TIER'
    this.cWorkTables[9]='DET_EXPO'
    this.cWorkTables[10]='DET_IMPO'
    return(this.OpenAllTables(10))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
