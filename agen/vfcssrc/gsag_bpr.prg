* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bpr                                                        *
*              Scarica prestazioni rapide                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-18                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bpr",oParentObject)
return(i_retval)

define class tgsag_bpr as StdBatch
  * --- Local variables
  w_TIPRIG = 0
  w_TIPRI2 = 0
  w_MODSCHED = space(1)
  w_ObjMPR = .NULL.
  w_GSAG_AAT = .NULL.
  w_OLD_KEY = space(10)
  w_NEW_KEY = space(10)
  w_CAU_ATTI = space(20)
  w_LOCALI = space(25)
  w_ENTE = space(10)
  w_UFFICI = space(10)
  w_SAV_t_PR__DATA = ctod("  /  /  ")
  w_SAV_t_PRNUMPRA = space(10)
  w_SAV_t_PRCODVAL = space(10)
  w_SAV_t_PRCODLIS = space(10)
  w_SAV_t_DACODNOM = space(15)
  w_SAV_t_DACODSED = space(5)
  w_SAV_CPROWNUM = 0
  w_SAV_t_DALISACQ = space(10)
  w_OK = .f.
  w_ISALT = .f.
  w_CACHKNOM = space(1)
  w_CACHKOBB = space(1)
  w_NOCODICE = space(15)
  w_NODESCRI = space(60)
  w_NOTELEFO = space(18)
  w_NO_EMAIL = space(254)
  w_NO_EMPEC = space(254)
  w_NONUMCEL = space(18)
  w_NOTELFAX = space(18)
  w_OBJ = .NULL.
  w_ATOGGETT = space(254)
  w_ATDATINI = ctot("")
  w_ATDATFIN = ctot("")
  w_ATGGPREA = 0
  w_ATNUMPRI = 0
  w_ATCODATT = space(5)
  w_ATPROMEM = ctot("")
  w_ATSTAATT = 0
  w_OREPRO = space(2)
  w_MINPRO = space(2)
  w_bFOUND = .f.
  w_RESULT = space(254)
  w_CAFLATRI = space(1)
  w_OLD_KEY_PART = space(10)
  w_GENPRE = space(1)
  w_VISPRESTAZ = space(1)
  NUM_PRE = 0
  w_ErroMsg = space(0)
  w_STATUS = space(1)
  w_ANNOPROG = space(4)
  w_ISAHE = .f.
  w_ISAHR = .f.
  w_CONPRA = space(15)
  w_NOMCUR = space(10)
  w_CAMPO = space(10)
  w_CODPRA = space(15)
  w_DESPRA = space(100)
  w_CODART = space(20)
  w_DESATT = space(40)
  w_ORDINA = space(1)
  w_CODATT = space(20)
  w_FLUNIV = space(1)
  w_NUMPRE = 0
  w_DATINI = ctod("  /  /  ")
  w_OCCORR = 0
  w_OLDPRA = space(15)
  w_OLDDES = space(15)
  w_ARTICO = space(20)
  w_CONFERMA = .f.
  w_MSG = space(0)
  w_RicalcolaPrz = space(1)
  w_MsgWarning = .f.
  w_PrzCalcolato = 0
  w_TOT_DAFATTU = 0
  w_OKPRES = .f.
  w_PRCODATT = space(20)
  w_PARASS = 0
  w_PRSERAGG = space(10)
  w_PRDESPRE = space(40)
  w_PRDESAGG = space(0)
  w_PRCODOPE = 0
  w_PRDATMOD = ctod("  /  /  ")
  w_PRUNIMIS = space(3)
  w_PRQTAMOV = 0
  w_PRCENCOS = space(15)
  w_PRPREZZO = 0
  w_PRVALRIG = 0
  w_PROREEFF = 0
  w_PRMINEFF = 0
  w_PRCOSINT = 0
  w_PRCOSUNI = 0
  w_PRFLDEFF = space(1)
  w_PRPREMIN = 0
  w_PRPREMAX = 0
  w_PRGAZUFF = space(6)
  w_PRVOCCOS = space(15)
  w_PR_SEGNO = space(1)
  w_PRATTIVI = space(15)
  w_PRINICOM = ctod("  /  /  ")
  w_PRFINCOM = ctod("  /  /  ")
  w_PRQTAUM1 = 0
  w_PRTIPRIG = space(1)
  w_PRTCOINI = ctod("  /  /  ")
  w_PRTCOFIN = ctod("  /  /  ")
  w_PRCODNOM = space(15)
  w_PRTIPRI2 = space(1)
  w_PRRIFPRE = 0
  w_PRRIGPRE = space(1)
  w_UTCC = 0
  w_UTDC = ctod("  /  /  ")
  w_UTDV = ctod("  /  /  ")
  w_LOBJECT = .NULL.
  w_CODSES = space(15)
  w_FLRESTO = .f.
  w_PARAME = 0
  w_CODPRA = space(15)
  w_TOTCALC = 0
  w_TOTPARAME = 0
  w_PRPREZZO = 0
  w_TOTALE = 0
  w_SERTOT = space(10)
  w_NUMSOGG = space(15)
  w_OKDelPRES = .f.
  w_OKCOND = .f.
  * --- WorkFile variables
  OFF_ATTI_idx=0
  PAR_AGEN_idx=0
  CAUMATTI_idx=0
  CAN_TIER_idx=0
  OFF_PART_idx=0
  OFFDATTI_idx=0
  DIPENDEN_idx=0
  COM_ATTI_idx=0
  OFF_NOMI_idx=0
  TIP_DOCU_idx=0
  ART_ICOL_idx=0
  RIS_ESTR_idx=0
  TMP_CERT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna le pratiche con le prestazioni rapide e le relative attivit�
    this.w_OLD_KEY = "01-01-1900"+repl("*", 15)+space(3+5+15+5)
    this.w_CAU_ATTI = space(20)
    this.w_ISALT = Isalt()
    this.w_ISAHE = Isahe()
    this.w_ISAHR = Isahr()
    * --- Legge il tipo attivit� in Parametri agenda
    * --- Read from PAR_AGEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PACAUPRE"+;
        " from "+i_cTable+" PAR_AGEN where ";
            +"PACODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PACAUPRE;
        from (i_cTable) where;
            PACODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAU_ATTI = NVL(cp_ToDate(_read_.PACAUPRE),cp_NullValue(_read_.PACAUPRE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_CAU_ATTI)
      ah_ErrorMsg("Inserire il tipo per prestazioni rapide nella tabella parametri",64,"")
      i_retcode = 'stop'
      return
    endif
    * --- Legge i dati della causale attivit� presente in Parametri agenda
    * --- Read from CAUMATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CADESCRI,CAGGPREA,CAPRIORI,CATIPATT,CAPROMEM,CADISPON,CACHKNOM,CACHKOBB,CAFLATRI,CASTAATT"+;
        " from "+i_cTable+" CAUMATTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CAU_ATTI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CADESCRI,CAGGPREA,CAPRIORI,CATIPATT,CAPROMEM,CADISPON,CACHKNOM,CACHKOBB,CAFLATRI,CASTAATT;
        from (i_cTable) where;
            CACODICE = this.w_CAU_ATTI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ATOGGETT = NVL(cp_ToDate(_read_.CADESCRI),cp_NullValue(_read_.CADESCRI))
      this.w_ATGGPREA = NVL(cp_ToDate(_read_.CAGGPREA),cp_NullValue(_read_.CAGGPREA))
      this.w_ATNUMPRI = NVL(cp_ToDate(_read_.CAPRIORI),cp_NullValue(_read_.CAPRIORI))
      this.w_ATCODATT = NVL(cp_ToDate(_read_.CATIPATT),cp_NullValue(_read_.CATIPATT))
      this.w_ATPROMEM = NVL((_read_.CAPROMEM),cp_NullValue(_read_.CAPROMEM))
      this.w_ATSTAATT = NVL(cp_ToDate(_read_.CADISPON),cp_NullValue(_read_.CADISPON))
      this.w_CACHKNOM = NVL(cp_ToDate(_read_.CACHKNOM),cp_NullValue(_read_.CACHKNOM))
      this.w_CACHKOBB = NVL(cp_ToDate(_read_.CACHKOBB),cp_NullValue(_read_.CACHKOBB))
      this.w_CAFLATRI = NVL(cp_ToDate(_read_.CAFLATRI),cp_NullValue(_read_.CAFLATRI))
      this.w_STATUS = NVL(cp_ToDate(_read_.CASTAATT),cp_NullValue(_read_.CASTAATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- valorizzato da default maschera
    this.w_NOCODICE = SPACE(20)
    this.w_NODESCRI = ""
    this.w_NOTELEFO = ""
    this.w_NO_EMAIL = ""
    this.w_NO_EMPEC = ""
    this.w_NONUMCEL = ""
    this.w_NOTELFAX = ""
    * --- Detail delle Prestazioni rapide
    this.w_ObjMPR = iif(this.w_ISALT,this.oParentObject.GSAG_MPR,this.oParentObject.GSAG_MPP)
    if this.w_ObjMPR.NumRow() = 0
      ah_ErrorMsg("Inserire almeno una prestazione",64,"")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_ATSTATUS="P"
      if Empty(this.oParentObject.w_CAUDOC) and this.w_ISALT and this.w_ObjMPR.Search(" (Not Empty(NVL(t_DACODICE, SPACE(41))) or Not Empty(NVL(t_DACODATT, SPACE(20)))) AND NOT DELETED() and (Nvl(t_DATIPRIG,' ') $ 'D-E' OR (Nvl(t_DATIPRI2,' ') $ 'D-E'))") > 0
        ah_ErrorMsg("Attenzione impossibile generare il documento di vendita, selezionare un valore documento coerente alla causale specificata nel tipo attivit�! ")
        i_retcode = 'stop'
        return
      endif
      if Empty(this.oParentObject.w_CAUACQ) and this.w_ISALT and this.oParentObject.w_FLANAL<>"S" and this.w_ObjMPR.Search(" (Not Empty(NVL(t_DACODICE, SPACE(41))) OR Not Empty(NVL(t_DACODATT, SPACE(20)))  ) AND NOT DELETED() and (Nvl(t_DATIPRIG,' ') $ 'A-E' OR (Nvl(t_DATIPRI2,' ') $ 'A-E') )") > 0
        ah_ErrorMsg("Attenzione impossibile generare il documento di acquisto, selezionare un valore documento coerente alla causale specificata nel tipo attivit�! ")
        i_retcode = 'stop'
        return
      endif
    endif
    if NVL(this.w_CACHKNOM," ")="P"
      this.w_ObjMPR.Exec_Select("TEMP", "t_PR__DATA", "NOT(Empty(t_PR__DATA)) AND (NOT(Empty(t_DACODATT)) or NOT(Empty(t_DACODICE))) AND NOT(Empty(t_PRCODVAL))  AND EMPTY(t_DACODNOM) and t_DATIPRIG<>'A'"+IIF(this.oParentObject.w_SELEZ="S"," AND t_FLCOMP=1",""), "", "", "")     
      if USED("TEMP")
        SELECT "TEMP"
        GO TOP
        if RECCOUNT()>0
          * --- Richiesto nominativo per inserimento attivit�
          ah_ErrorMsg("Il nominativo � obbligatorio per una o pi� righe prestazione!",64,"")
          USE IN SELECT("TEMP")
          i_retcode = 'stop'
          return
        endif
        USE IN SELECT("TEMP")
      endif
    endif
    if NVL(this.w_CACHKOBB," ")="P"
      this.w_ObjMPR.Exec_Select("TEMP", "t_PR__DATA", "NOT(Empty(t_PR__DATA)) AND (NOT(Empty(t_DACODATT)) or NOT(Empty(t_DACODICE))) AND NOT(Empty(t_PRCODVAL)) AND NOT(Empty(t_PRCODLIS)) AND EMPTY(t_PRNUMPRA)", "", "", "")     
      if USED("TEMP")
        SELECT "TEMP"
        GO TOP
        if RECCOUNT()>0
          * --- Richiesto nominativo per inserimento attivit�
          if this.w_ISALT
            ah_ErrorMsg("La pratica � obbligatoria su tutte le righe per confermare le attivit�",64,"")
          else
            ah_ErrorMsg("La commessa � obbligatoria su tutte le righe per confermare le attivit�",64,"")
          endif
          USE IN SELECT("TEMP")
          i_retcode = 'stop'
          return
        endif
        USE IN SELECT("TEMP")
      endif
    endif
    * --- Riempie il cursore Temp con le prestazioni rapide del responsabile selezionato
    *     in maschera, ordinandole per Data+Pratica+Valuta+Listino
    if this.w_ISALT
      this.w_GENPRE = this.w_ObjMPR.w_GENPRE
      this.w_VISPRESTAZ = this.w_ObjMPR.w_VISPRESTAZ
      this.w_OK = .t.
      this.w_ObjMPR.Exec_Select("TEMP", "t_DACODATT", "NOT Empty(t_PR__DATA) AND NOT Empty(t_DACODATT) AND NOT Empty(t_PRCODVAL) AND NOT Empty(t_PRCODLIS) AND empty(t_DAPREZZO)"+IIF(this.oParentObject.w_SELEZ="S"," AND t_FLCOMP=1",""), "t_PR__DATA, t_PRNUMPRA, t_PRCODVAL, t_PRCODLIS", "", "")     
      * --- Dobbiamo controllare se ci sono prestazioni con importi a zero e di tipo 'A quantit� e valore' oppure 'Descrittivo'
      Select "TEMP"
      if Reccount("TEMP")<>0
        this.w_OK = ah_YesNo("Attenzione: Ci sono prestazioni con importi a zero. Confermi ugualmente?")
      endif
      * --- Controllo univocit� prestazioni per pratica
      this.w_ObjMPR.Exec_Select("TMPUNI", "t_cacodart As Codart, t_prnumpra As Conpra, t_nomepra as Despra, Max(t_FLUNIV) as Univoc, Max(t_NUMPRE) as Maxpre", "","t_prnumpra","t_cacodart,t_prnumpra",'Max(t_FLUNIV)="S"')     
      if Reccount("TMPUNI")<>0
        CREATE CURSOR __tmp__ (CODPRA C(15), DESPRA C(100), CODART C(20), DESATT C(40), ORDINA C(1), CODATT C(20), NUMPRE N(4), DATINI D(8), OCCORR N(4))
        CREATE CURSOR TMPCUN (CODART C(20))
        * --- Ci sono prestazioni non univoche per la pratica
        this.w_NOMCUR = "TMPCUN"
        this.w_CAMPO = "CODART"
        this.w_OLDPRA = "@#@#@#@#@#@#@#@"
        Select "TMPUNI"
        GO TOP
        SCAN 
        this.w_CONPRA = Conpra
        this.w_ARTICO = Codart
        this.w_CODPRA = ""
        this.w_DESPRA = ""
        this.w_CODART = ""
        this.w_DESATT = ""
        this.w_ORDINA = ""
        this.w_CODATT = ""
        this.w_FLUNIV = ""
        this.w_NUMPRE = 0
        this.w_DATINI = CTOD("  -  -  ")
        this.w_OCCORR = 0
        if this.w_CONPRA=this.w_OLDPRA
          INSERT INTO TMPCUN (CODART) VALUES(this.w_ARTICO)
        else
          if this.w_OLDPRA="@#@#@#@#@#@#@#@"
            INSERT INTO TMPCUN (CODART) VALUES(this.w_ARTICO)
          else
            * --- Select from gsag2scu
            do vq_exec with 'gsag2scu',this,'_Curs_gsag2scu','',.f.,.t.
            if used('_Curs_gsag2scu')
              select _Curs_gsag2scu
              locate for 1=1
              do while not(eof())
              this.w_CODART = CODART
              this.w_DESATT = DESATT
              this.w_ORDINA = ORDINA
              this.w_CODATT = CODATT
              this.w_FLUNIV = FLUNIV
              this.w_NUMPRE = NUMPRE
              this.w_DATINI = TTOD(DATINI)
              * --- Sono in caricamento devo contare anche l'occorrenza della prestazione in quanto presente nel transitorio
              this.w_OCCORR = OCCORR+1
              * --- w_ORDINA='A' la riga viene dalla query (raggruppata) GSAG2SCU
              if this.w_OCCORR>this.w_NUMPRE
                * --- Se il numero di occorrenze � maggiore del massimo previsto
                *     inserisco la riga relativa al codice articolo
                INSERT INTO __tmp__ (CODPRA, DESPRA,CODART,DESATT,ORDINA,CODATT,NUMPRE,DATINI,OCCORR) VALUES(this.w_OLDPRA,this.w_OLDDES, this.w_CODART,this.w_DESATT,this.w_ORDINA,this.w_CODATT,this.w_NUMPRE,this.w_DATINI,this.w_OCCORR)
              endif
                select _Curs_gsag2scu
                continue
              enddo
              use
            endif
            Select "TMPCUN"
            zap
            INSERT INTO TMPCUN (CODART) VALUES(this.w_ARTICO)
          endif
          this.w_OLDPRA = this.w_CONPRA
        endif
        Select "TMPUNI"
        this.w_OLDDES = Despra
        ENDSCAN
        * --- Analizzo l'ultima pratica
        * --- Select from gsag2scu
        do vq_exec with 'gsag2scu',this,'_Curs_gsag2scu','',.f.,.t.
        if used('_Curs_gsag2scu')
          select _Curs_gsag2scu
          locate for 1=1
          do while not(eof())
          this.w_CODART = CODART
          this.w_DESATT = DESATT
          this.w_ORDINA = ORDINA
          this.w_CODATT = CODATT
          this.w_FLUNIV = FLUNIV
          this.w_NUMPRE = NUMPRE
          this.w_DATINI = TTOD(DATINI)
          * --- Sono in caricamento devo contare anche l'occorrenza della prestazione in quanto presente nel transitorio
          this.w_OCCORR = OCCORR+1
          * --- w_ORDINA='A' la riga viene dalla query (raggruppata) GSAG2SCU
          if this.w_OCCORR>this.w_NUMPRE
            * --- Se il numero di occorrenze � maggiore del massimo previsto
            *     inserisco la riga relativa al codice articolo
            INSERT INTO __tmp__ (CODPRA, DESPRA,CODART,DESATT,ORDINA,CODATT,NUMPRE,DATINI,OCCORR) VALUES(this.w_OLDPRA,this.w_OLDDES, this.w_CODART,this.w_DESATT,this.w_ORDINA,this.w_CODATT,this.w_NUMPRE,this.w_DATINI,this.w_OCCORR)
          endif
            select _Curs_gsag2scu
            continue
          enddo
          use
        endif
        USE IN SELECT("TMPCUN")
        if USED("__tmp__") AND RECCOUNT("__tmp__")>0
          this.w_CONFERMA = .F.
          this.w_MSG = "Attenzione, esistono prestazioni non univoche."+CHR(13)+CHR(10)+"Si desidera procedere con il completamento delle prestazioni?" 
          do GSAG1SCU with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if NOT this.w_CONFERMA
            USE IN SELECT("TMPUNI")
            i_retcode = 'stop'
            return
          endif
          * --- AH_YESNO( "Attenzione, esistono prestazione non univoche.%0Si desidera stamparle?" )
          *     Ah_yesno("Si desidera procedere con il completamento delle prestazioni?")
        endif
      endif
      USE IN SELECT("TMPUNI")
      * --- Controllo coerenza tra ente pratiche e pratiche di tipo onorario civile\penale
      this.w_ObjMPR.Exec_Select("TEMP", "t_DACODATT", "NOT Empty(t_PR__DATA) AND NOT Empty(t_DACODATT) AND NOT Empty(t_PRCODVAL) AND NOT Empty(t_PRCODLIS) AND empty(t_ENTE) and (t_PRESTA='I' or t_PRESTA='E')"+IIF(this.oParentObject.w_SELEZ="S"," AND t_FLCOMP=1",""), "t_PR__DATA, t_PRNUMPRA, t_PRCODVAL, t_PRCODLIS", "", "")     
      if Reccount("TEMP")<>0
        this.w_OK = !Ah_yesno("Attenzione: per il calcolo degli onorari civili o penali � necessario inserire l'ente nella pratica. Vuoi inserire?")
      endif
      if NOT this.w_OK
        USE IN SELECT("TEMP")
        i_retcode = 'stop'
        return
      endif
      * --- Controlla se l'utente non � amministratore e se � attivo il relativo check nei Parametri attivit� e nelle Persone (responsabile)
      if this.oParentObject.w_FLPRES = "S" AND this.oParentObject.w_CHKDATAPRE = "S" AND NOT Cp_IsAdministrator()
        * --- Intervallo di date
        this.w_ObjMPR.Exec_Select("TEMP", "t_PRNUMPRA, t_PR__DATA, t_DACODATT", "t_PR__DATA<  this.oparentobject.w_DATAPREC OR t_PR__DATA> this.oparentobject.w_DATASUCC" , "t_PR__DATA, t_PRNUMPRA", "", "")     
        if Reccount("TEMP") > 0
          ah_ErrorMsg("Data non consentita dai controlli relativi al responsabile")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Controlla servizi a valore zero con prezzo zero
    if this.w_ISAHR AND this.oParentObject.w_ATSTATUS = "P"
      * --- Verifiche servizi a valore con prezzo zero
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDPRZDES"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDPRZDES;
          from (i_cTable) where;
              TDTIPDOC = this.oParentObject.w_CAUDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RicalcolaPrz = NVL(cp_ToDate(_read_.TDPRZDES),cp_NullValue(_read_.TDPRZDES))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MsgWarning = .F.
      this.w_ObjMPR.Exec_Select("_PREST_0","*","NVL(t_PRVALRIG,0)=0 AND (t_DATIPRIG<>'N' OR t_DATIPRI2<>'N') AND t_TIPART='FO' AND (NOT EMPTY(NVL(t_DACODATT,'')) OR NOT EMPTY(NVL(t_DACODICE,'')))","","","")     
      if RECCOUNT()>0
        * --- Ci sono prestazioni a valore con prezzo zero
        * --- Dobbiamo controllare se la causale ha il flag di ricalcolo prezzi attivo e quindi leggiamo TDPRZDES
        if this.w_RicalcolaPrz<>"S"
          * --- La causale non ricalcola i prezzi, quindi deve segnalare che ci sono righe con prezzo a zero
          this.w_MsgWarning = .T.
        else
          * --- La causale ricalcola i prezzi, quindi deve controllare se avrebbero prezzo zero anche dopo
           
 select _PREST_0 
 Go Top 
 Scan
          * --- Legge l'eventuale codice cliente associato al nominativo
          * --- Read from OFF_NOMI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "NOCODCLI"+;
              " from "+i_cTable+" OFF_NOMI where ";
                  +"NOCODICE = "+cp_ToStrODBC(t_DACODNOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              NOCODCLI;
              from (i_cTable) where;
                  NOCODICE = t_DACODNOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_CODCLI = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_PrzCalcolato = GSAR_BPZ(this, t_DACODATT,w_CODCLI, "C", t_PRCODLIS, t_PRQTAMOV, t_PRCODVAL, t_PR__DATA, t_PRUNIMIS)
          if this.w_PrzCalcolato=0
            this.w_MsgWarning = .T.
            EXIT
          endif
          Endscan
        endif
      endif
      USE IN SELECT ("_PREST_0")
      if this.w_MsgWarning AND !AH_YESNO( "Attenzione! Nel dettaglio prestazioni sono presenti servizi di tipo 'a valore' che avranno prezzo a zero sul documento generato! Prosegui ugualmente?" )
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Controllo sulla data di fine attivit�
    if this.oParentObject.w_ATSTATUS$"T-D-I" and ! this.w_ISALT
      this.w_ObjMPR.Exec_Select("TEMP", "t_PRNUMPRA, t_PR__DATA, t_DACODATT", "t_PR__DATA<I_DATSYS", "t_PR__DATA, t_PRNUMPRA", "", "")     
      this.w_OK = .T.
      Select "TEMP" 
 GO TOP
      * --- Dobbiamo controllare se la data di fina attivit� � minore di quella di sistema
      if RECCOUNT()>0
        this.w_OK = ah_YesNo("Inserita data antecedente alla data odierna! Continuare?")
      endif
      if NOT this.w_OK
         
 USE IN SELECT("TEMP")
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_ATSTATUS$"T-D-I" and ! this.w_ISALT AND NOT EMPTY(this.oParentObject.w_PACHKATT)
      this.w_ObjMPR.Exec_Select("TEMP", "t_PRNUMPRA, t_PR__DATA, t_DACODATT", "t_PR__DATA>I_DATSYS+this.oparentobject.w_PACHKATT", "t_PR__DATA, t_PRNUMPRA", "", "")     
      this.w_OK = .T.
      Select "TEMP" 
 GO TOP
      * --- Dobbiamo controllare se la data di inizio attivit� � maggiore di quella di sistema + n giorni
      if RECCOUNT()>0
        this.w_OK = Ah_yesno( "Inserita data superiore alla data odierna di oltre %1 giorni. Continuare?", "", alltrim(str(this.oParentObject.w_PACHKATT)))
      endif
      if NOT this.w_OK
         
 USE IN SELECT("TEMP")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Riempie di nuovo il cursore Temp con le prestazioni rapide del responsabile selezionato
    *     in maschera, ordinandole per Data+Pratica+Valuta+Listino
    if this.w_ISALT
      this.w_ObjMPR.Exec_Select("TEMP", "*,iif(t_DARIFPRE>0,t_DARIFPRE,CPROWNUM) as ROWRIG","NOT(Empty(t_PR__DATA)) AND (NOT(Empty(t_DACODATT)) or NOT(Empty(t_DACODICE))) AND NOT(Empty(t_PRCODVAL)) AND NOT(Empty(t_PRCODLIS)) "+IIF(this.oParentObject.w_SELEZ="S"," AND t_FLCOMP=1",""), "t_PR__DATA, t_PRNUMPRA, t_PRCODVAL, t_PRCODLIS, t_DACODNOM, t_DACODSED,ROWRIG,t_DARIFPRE", "", "")     
    else
      this.w_ObjMPR.Exec_Select("TEMP", "*,CPROWNUM as ROWRIG","NOT(Empty(t_PR__DATA)) AND (NOT(Empty(t_DACODATT)) or NOT(Empty(t_DACODICE))) "+IIF(this.oParentObject.w_SELEZ="S"," AND t_FLCOMP=1",""), "t_PR__DATA, t_PRCODVAL, t_PRCODLIS, t_DACODNOM, t_DACODSED", "", "")     
    endif
    if this.w_GENPRE<>"S"
      this.w_OBJ = CREATEOBJECT("ActivityWriter",this,"OFF_ATTI")
    else
      this.w_OBJ = CREATEOBJECT("ActivityWriter",this,"PRE_STAZ")
      if this.w_VISPRESTAZ = "S"
        * --- Genera struttura della tabella temporanea (vuota)
        * --- Create temporary table TMP_CERT
        i_nIdx=cp_AddTableDef('TMP_CERT') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\gsag_kvv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMP_CERT_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    endif
    Select "TEMP"
    GO TOP
    SCAN FOR (!EMPTY(t_PRNUMPRA) and this.w_ISALT ) or Not this.w_ISALT
    * --- Legge dati pratica
    if this.w_ISALT
      this.w_UFFICI = Nvl(Temp.t_UFFICI," ")
      this.w_LOCALI = Nvl(Temp.t_LOCALI," ")
      this.w_ENTE = Nvl(Temp.t_ENTE," ")
    else
      * --- Legge la localit� del Nominativo
      * --- Read from OFF_NOMI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NOLOCALI"+;
          " from "+i_cTable+" OFF_NOMI where ";
              +"NOCODICE = "+cp_ToStrODBC(t_DACODNOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NOLOCALI;
          from (i_cTable) where;
              NOCODICE = t_DACODNOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LOCALI = NVL(cp_ToDate(_read_.NOLOCALI),cp_NullValue(_read_.NOLOCALI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_TIPRIG = Nvl(icase(this.w_ISAHE,t_ETIPRIG,this.w_ISAHR,t_RTIPRIG,t_ATIPRIG),0)
    this.w_TIPRI2 = Nvl(icase(this.w_ISAHE,t_ETIPRI2,this.w_ISAHR,t_RTIPRI2,t_ATIPRI2),0)
    Select "TEMP"
    this.w_NEW_KEY = dtoc(t_PR__DATA) + iif(this.w_ISALT, t_PRNUMPRA, SPACE(15)) + t_PRCODVAL
    this.w_NEW_KEY = this.w_NEW_KEY + iif(this.w_ISAHE, space(5), t_PRCODLIS) + iif(this.w_ISAHR, t_DALISACQ, space(5))
    this.w_NEW_KEY = this.w_NEW_KEY + t_DACODNOM + t_DACODSED + Icase(this.w_GENPRE="S",str(CPROWNUM),this.oParentObject.w_GENSINGATT="S", str(ROWRIG), "")
    if this.w_OLD_KEY <> "01-01-1900"+repl("*", 15)+space(3+5+15+5) AND this.w_OLD_KEY <> this.w_NEW_KEY
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if EMPTY(this.w_RESULT)
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_ErroMsg = this.w_ErroMsg + this.w_RESULT + CHR(13)
      endif
      Select "TEMP"
      this.w_OBJ.Blank()     
      this.w_OLD_KEY_PART = space(10)
      this.w_TOT_DAFATTU = 0
    endif
    if !EMPTY(this.oParentObject.w_CODRESP) AND this.w_OLD_KEY_PART<>NVL(this.oParentObject.w_CODRESP,space(5))+NVL(this.oParentObject.w_GRUPART,space(5)) and this.w_GENPRE<>"S"
      this.w_OLD_KEY_PART = NVL(this.oParentObject.w_CODRESP,space(5))+NVL(this.oParentObject.w_GRUPART,space(5))
      this.w_OBJ.AddPart(0,0,"P",this.oParentObject.w_CODRESP, this.oParentObject.w_GRUPART)     
    endif
    if this.w_GENPRE<>"S"
      this.w_OBJ.w_OFFDATTI.AppendBlank()     
      if this.w_ISALT
        this.w_OBJ.w_OFFDATTI.CPROWNUM = CPROWNUM
        this.w_OBJ.w_OFFDATTI.CPROWORD = Nvl(CPROWNUM,0)*10
      endif
      Select "TEMP"
      this.w_SAV_t_PR__DATA = t_PR__DATA
      this.w_SAV_t_PRNUMPRA = t_PRNUMPRA
      this.w_SAV_t_PRCODVAL = t_PRCODVAL
      this.w_SAV_t_PRCODLIS = t_PRCODLIS
      this.w_SAV_t_DALISACQ = t_DALISACQ
      this.w_SAV_t_DACODNOM = t_DACODNOM
      this.w_SAV_t_DACODSED = t_DACODSED
      this.w_SAV_CPROWNUM = ROWRIG
      this.w_OBJ.w_OFFDATTI.DACODATT = t_DACODATT
      this.w_OBJ.w_OFFDATTI.DACODICE = t_DACODICE
      this.w_OBJ.w_OFFDATTI.DADESATT = t_PRDESPRE
      this.w_OBJ.w_OFFDATTI.DADESAGG = t_PRDESAGG
      this.w_OBJ.w_OFFDATTI.DACODRES = this.oParentObject.w_CODRESP
      this.w_OBJ.w_OFFDATTI.DACODOPE = t_PRCODOPE
      this.w_OBJ.w_OFFDATTI.DADATMOD = t_PRDATMOD
      this.w_OBJ.w_OFFDATTI.DAUNIMIS = t_PRUNIMIS
      this.w_OBJ.w_OFFDATTI.DAQTAMOV = t_PRQTAMOV
      this.w_OBJ.w_OFFDATTI.DAPREZZO = t_DAPREZZO
      this.w_OBJ.w_OFFDATTI.DAVALRIG = t_PRVALRIG
      this.w_OBJ.w_OFFDATTI.DAOREEFF = t_PROREEFF
      this.w_OBJ.w_OFFDATTI.DAMINEFF = t_PRMINEFF
      this.w_OBJ.w_OFFDATTI.DACOSINT = t_PRCOSINT
      this.w_OBJ.w_OFFDATTI.DACOSUNI = t_DACOSUNI
      this.w_OBJ.w_OFFDATTI.DAFLDEFF = t_DAFLDEFF
      this.w_OBJ.w_OFFDATTI.DAPREMIN = t_DAPREMIN
      this.w_OBJ.w_OFFDATTI.DAPREMAX = t_DAPREMAX
      this.w_OBJ.w_OFFDATTI.DAGAZUFF = t_DAGAZUFF
      this.w_OBJ.w_OFFDATTI.DACENCOS = t_DACENCOS
      this.w_OBJ.w_OFFDATTI.DAVOCCOS = t_DAVOCCOS
      this.w_OBJ.w_OFFDATTI.DAVOCRIC = t_DAVOCRIC
      this.w_OBJ.w_OFFDATTI.DA_SEGNO = t_DA_SEGNO
      this.w_OBJ.w_OFFDATTI.DAATTIVI = t_DAATTIVI
      this.w_OBJ.w_OFFDATTI.DAINICOM = t_DAINICOM
      this.w_OBJ.w_OFFDATTI.DAFINCOM = t_DAFINCOM
      this.w_OBJ.w_OFFDATTI.DATIPRIG = ICASE(this.w_TIPRIG=1,"N",this.w_TIPRIG=2,"D",this.w_TIPRIG=3,"A","E")
      if this.w_ISALT
        this.w_OBJ.w_OFFDATTI.DATIPRI2 = ICASE(this.w_TIPRI2=1,"N",this.w_TIPRI2=2,"D",this.w_TIPRI2=3,"A","E")
        this.w_TOT_DAFATTU = IIF(this.w_TIPRIG>1 OR this.w_TIPRI2>1,this.w_TOT_DAFATTU+1,this.w_TOT_DAFATTU)
        this.w_OBJ.w_OFFDATTI.DACODCOM = t_PRNUMPRA
      else
        this.w_OBJ.w_OFFDATTI.DACODCOM = t_DACODCOM
      endif
      this.w_OBJ.w_OFFDATTI.DACOMRIC = t_DACOMRIC
      this.w_OBJ.w_OFFDATTI.DAATTRIC = t_DAATTRIC
      this.w_OBJ.w_OFFDATTI.DACENRIC = t_DACENRIC
      this.w_OBJ.w_OFFDATTI.DATCOINI = t_DATCOINI
      this.w_OBJ.w_OFFDATTI.DATCOFIN = t_DATCOFIN
      this.w_OBJ.w_OFFDATTI.DATRIINI = t_DATRIINI
      this.w_OBJ.w_OFFDATTI.DATRIFIN = t_DATRIFIN
      this.w_OBJ.w_OFFDATTI.DARIFPRE = t_DARIFPRE
      this.w_OBJ.w_OFFDATTI.DARIGPRE = t_DARIGPRE
      this.w_OBJ.w_OFFDATTI.DACONTRA = t_DACONTRA
      this.w_OBJ.w_OFFDATTI.DACODMOD = t_DACODMOD
      this.w_OBJ.w_OFFDATTI.DACODIMP = t_DACODIMP
      this.w_OBJ.w_OFFDATTI.DACOCOMP = t_DACOCOMP
      this.w_OBJ.w_OFFDATTI.DARINNOV = t_DARINNOV
      this.w_OBJ.w_OFFDATTI.SaveCurrentRecord()     
      this.w_OBJ.w_ATCAUATT = this.w_CAU_ATTI
      if this.w_ISALT
        * --- Se nessuna delle righe dell'attivit� creata � da fatturare l'attivit� creata non sar� confermata ma evasa
        this.w_OBJ.w_ATSTATUS = IIF(this.w_TOT_DAFATTU>0,"P","F")
      else
        this.w_OBJ.w_ATSTATUS = this.oParentObject.w_ATSTATUS
      endif
      this.w_OBJ.w_ATCODNOM = t_DACODNOM
      this.w_OBJ.w_ATLISACQ = t_DALISACQ
      this.w_ATDATINI = cp_CharToDatetime(DTOC(t_PR__DATA)+" "+this.oParentObject.w_ORAINI+":"+this.oParentObject.w_MININI+":00")
      this.w_ATDATFIN = cp_CharToDatetime(DTOC(t_PR__DATA)+" "+this.oParentObject.w_ORAFIN+":"+this.oParentObject.w_MINFIN+":00")
      this.w_ANNOPROG = STR(YEAR(CP_TODATE(t_PR__DATA)), 4, 0)
      this.w_OBJ.w_ATANNDOC = this.w_ANNOPROG
      this.w_OBJ.w_ATDATINI = this.w_ATDATINI
      this.w_OBJ.w_ATDATFIN = this.w_ATDATFIN
      this.w_OBJ.w_ATDATDOC = this.w_ATDATFIN
      this.w_OBJ.SyncDate()     
      * --- Ore e minuti w_ATPROMEMoria
      this.w_OREPRO = RIGHT("00"+ALLTRIM(STR(HOUR(this.w_ATPROMEM),2)),2)
      this.w_MINPRO = RIGHT("00"+ALLTRIM(STR(MINUTE(this.w_ATPROMEM),2)),2)
      Select "TEMP"
      this.w_OBJ.w_AT___FAX = this.w_NOTELFAX
      this.w_OBJ.w_AT__ENTE = this.w_ENTE
      this.w_OBJ.w_AT_EMAIL = this.w_NO_EMAIL
      this.w_OBJ.w_AT_EMPEC = this.w_NO_EMPEC
      this.w_OBJ.w_ATCELLUL = this.w_NONUMCEL
      this.w_OBJ.w_ATCODATT = this.w_ATCODATT
      this.w_OBJ.w_ATCODLIS = t_PRCODLIS
      this.w_OBJ.w_ATCODNOM = t_DACODNOM
      if this.w_ISALT
        this.w_OBJ.w_ATCODPRA = t_PRNUMPRA
      endif
      this.w_OBJ.w_ATCODSED = t_DACODSED
      this.w_OBJ.w_ATCODVAL = t_PRCODVAL
      this.w_OBJ.w_ATDATOUT = DTOT(i_datsys)
      this.w_OBJ.w_ATDATPRO = cp_CharToDatetime("  -  -       :  :  ")
      this.w_OBJ.w_ATDATRIN = cp_CharToDatetime("  -  -       :  :  ")
      this.w_OBJ.w_ATFLATRI = "N"
      this.w_OBJ.w_ATFLNOTI = "N"
      this.w_OBJ.w_ATGGPREA = this.w_ATGGPREA
      this.w_OBJ.w_ATKEYOUT = 0
      this.w_OBJ.w_ATLOCALI = this.w_LOCALI
      this.w_OBJ.w_ATMINEFF = 0
      this.w_OBJ.w_ATNUMGIO = 0
      this.w_OBJ.w_ATNUMPRI = this.w_ATNUMPRI
      this.w_OBJ.w_ATOGGETT = this.w_ATOGGETT
      this.w_OBJ.w_ATOPERAT = 0
      this.w_OBJ.w_ATOREEFF = 0
      this.w_OBJ.w_ATPERCOM = 0
      this.w_OBJ.w_ATPERSON = this.w_NODESCRI
      this.w_OBJ.w_ATPROMEM = cp_CharToDatetime(DTOC(this.w_ATDATINI)+" "+this.w_OREPRO+":"+this.w_MINPRO+":00")
      this.w_OBJ.w_ATSTAATT = this.w_ATSTAATT
      this.w_OBJ.w_ATFLATRI = this.w_CAFLATRI
      this.w_OBJ.w_ATTELEFO = this.w_NOTELEFO
      this.w_OBJ.w_ATTIPRIS = NULL
      this.w_OBJ.w_ATUFFICI = this.w_UFFICI
      this.w_OBJ.w_UTCC = i_CODUTE
      this.w_OBJ.w_UTCV = 0
      this.w_OBJ.w_UTDC = SetInfoDate( g_CALUTD )
      this.w_OBJ.w_UTDV = cp_CharToDate("  -  -    ")
    else
      Select "TEMP"
      this.w_SAV_t_PR__DATA = t_PR__DATA
      this.w_SAV_t_PRNUMPRA = t_PRNUMPRA
      this.w_SAV_t_PRCODVAL = t_PRCODVAL
      this.w_SAV_t_PRCODLIS = t_PRCODLIS
      this.w_SAV_t_DALISACQ = t_DALISACQ
      this.w_SAV_t_DACODNOM = t_DACODNOM
      this.w_SAV_t_DACODSED = t_DACODSED
      this.w_SAV_CPROWNUM = ROWRIG
      this.w_OBJ.w_PRCODRES = this.oParentObject.w_CODRESP
      this.w_OBJ.w_PRGRURES = this.oParentObject.w_GRUPART
      this.w_OBJ.w_PRCODESE = g_CODESE
      this.w_OBJ.w_PR__DATA = t_PR__DATA
      this.w_OBJ.w_PRNUMPRA = t_PRNUMPRA
      this.w_OBJ.w_PRCODVAL = t_PRCODVAL
      this.w_OBJ.w_PRCODLIS = t_PRCODLIS
      this.w_OBJ.w_PRCODNOM = t_DACODNOM
      this.w_OBJ.w_PRCODSED = t_DACODSED
      this.w_OBJ.w_PRCODATT = t_DACODATT
      this.w_OBJ.w_PRDESPRE = t_PRDESPRE
      this.w_OBJ.w_PRDESAGG = t_PRDESAGG
      this.w_OBJ.w_PRDATMOD = t_PRDATMOD
      this.w_OBJ.w_PRCODOPE = t_PRCODOPE
      this.w_OBJ.w_PRUNIMIS = t_PRUNIMIS
      this.w_OBJ.w_PRQTAMOV = t_PRQTAMOV
      this.w_OBJ.w_PRPREZZO = t_DAPREZZO
      this.w_OBJ.w_PRVALRIG = t_PRVALRIG
      this.w_OBJ.w_PROREEFF = t_PROREEFF
      this.w_OBJ.w_PRMINEFF = t_PRMINEFF
      this.w_OBJ.w_PRCOSINT = t_PRCOSINT
      this.w_OBJ.w_PRCOSUNI = t_DACOSUNI
      this.w_OBJ.w_PRFLDEFF = t_DAFLDEFF
      this.w_OBJ.w_PRPREMIN = t_DAPREMIN
      this.w_OBJ.w_PRPREMAX = t_DAPREMAX
      this.w_OBJ.w_PRGAZUFF = t_DAGAZUFF
      this.w_OBJ.w_PRCENCOS = t_DACENRIC
      this.w_OBJ.w_PRVOCCOS = t_DAVOCRIC
      this.w_OBJ.w_PR_SEGNO = t_DA_SEGNO
      this.w_OBJ.w_PRATTIVI = t_DAATTIVI
      this.w_OBJ.w_PRINICOM = t_DAINICOM
      this.w_OBJ.w_PRFINCOM = t_DAFINCOM
      this.w_OBJ.w_PRQTAUM1 = t_QTAUM1
      this.w_OBJ.w_PRTIPRIG = ICASE(this.w_TIPRIG=1,"N",this.w_TIPRIG=2,"D",this.w_TIPRIG=3,"A","E")
      this.w_OBJ.w_PRTCOINI = t_DATCOINI
      this.w_OBJ.w_PRTCOFIN = t_DATCOFIN
      this.w_OBJ.w_PRTIPRI2 = ICASE(this.w_TIPRI2=1,"N",this.w_TIPRI2=2,"D",this.w_TIPRI2=3,"A","E")
      this.w_OBJ.w_PRRIGPRE = t_DARIGPRE
      this.w_OBJ.w_PRTIPDOC = this.oParentObject.w_CAUDOC
      this.w_OBJ.cCODANT = t_PRCODANT
      this.w_OBJ.cCODSPE = t_PRCODSPE
      this.w_OBJ.nIMPSPE = t_PRIMPSPE
      this.w_OBJ.nIMPANT = t_PRIMPANT
      this.w_PRRIFPRE = t_DARIFPRE
      this.w_PARASS = Nvl(t_PARASS,0)
    endif
    this.w_OLD_KEY = this.w_NEW_KEY
    Select "TEMP"
    this.w_OKPRES = Not this.w_ISALT OR ( (!EMPTY(t_PRNUMPRA) and this.w_ISALT ) and (this.w_GENPRE<>"S" OR Nvl(t_TIPART," ")<>"FO" or Nvl(t_DAPREZZO,0)<>0 or this.w_ObjMPR.w_FLGZER="S"))
    ENDSCAN
    if this.w_OKPRES
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if EMPTY(this.w_RESULT)
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_ErroMsg = this.w_ErroMsg + this.w_RESULT + CHR(13)
      endif
    endif
    this.w_OBJ = .NULL.
    if not empty(this.w_ErroMsg)
      do GSAG_KER with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      if this.w_VISPRESTAZ = "S"
        * --- Se sono state generate prestazioni
        if this.NUM_PRE > 0
          do GSAG_KVS with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
    if this.w_VISPRESTAZ = "S"
      * --- Drop temporary table TMP_CERT
      i_nIdx=cp_GetTableDefIdx('TMP_CERT')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_CERT')
      endif
    endif
    * --- Se w_VISMES=-1 allora non deve dare il messaggio 
    *     "Esistono n prestazioni provvisorie. Si consiglia di renderle definitive premendo il bottone Aggiorna. Vuoi procedere con l'aggiornamento?"
    *     Controllato nella routine GSAG_BCK
    this.oParentObject.w_VISMES = -1
    * --- Salvo dettaglio
    if this.w_ISALT
       
 this.oParentObject.Save_GSAG_MPR(.f.)
       
 this.w_ObjMPR.Loadrec() 
 this.w_ObjMPR.refresh()
      this.w_ObjMPR.bupdated = .t.
      this.w_LOBJECT = this.w_ObjMPR.getbodyctrl("w_PRNUMPRA")
      this.w_LOBJECT.Setfocus()     
    else
       
 this.oParentObject.Save_GSAG_MPP(.f.)
       
 this.w_ObjMPR.Loadrec() 
 this.w_ObjMPR.refresh()
      this.w_ObjMPR.bupdated = .t.
      this.w_LOBJECT = this.w_ObjMPR.getbodyctrl("w_PR__DATA")
      this.w_LOBJECT.Setfocus()     
    endif
    this.w_LOBJECT = .null.
    this.w_OBJ = Null
    this.w_GSAG_AAT = Null
    this.w_ObjMPR = Null
    * --- Chiude TEMP
    USE IN SELECT("TEMP")
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    ah_Msg("Generazione attivit� in corso ...",.T.,.F.,.F.)
    Select "TEMP"
    if this.w_OKPRES
      * --- Try
      local bErr_039E5CF0
      bErr_039E5CF0=bTrsErr
      this.Try_039E5CF0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- accept error
        bTrsErr=.f.
        if Empty(this.w_RESULT)
          this.w_RESULT = Message()
        endif
      endif
      bTrsErr=bTrsErr or bErr_039E5CF0
      * --- End
    endif
  endproc
  proc Try_039E5CF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.w_GENPRE<>"S"
      this.w_RESULT = this.w_OBJ.CreateActivity()
    else
      if this.w_OBJ.w_PRRIGPRE="S" or this.w_PRRIFPRE=0
        this.w_PRSERAGG = Space(10)
        this.w_OBJ.w_PRSERAGG = Space(10)
      endif
      if this.w_PRRIFPRE>0
        this.w_OBJ.w_PRSERAGG = this.w_PRSERAGG
        this.w_OBJ.w_PRRIFPRE = Val(this.w_OBJ.w_PRSERAGG)
      endif
      if this.w_PARASS>1
        this.w_NUMSOGG = space(15)
        * --- Legge se c'� almeno un soggetto intestatario di parcelle all'interno della pratica
        * --- Read from RIS_ESTR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SRCODSES"+;
            " from "+i_cTable+" RIS_ESTR where ";
                +"SRCODPRA = "+cp_ToStrODBC(this.w_OBJ.w_PRNUMPRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SRCODSES;
            from (i_cTable) where;
                SRCODPRA = this.w_OBJ.w_PRNUMPRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NUMSOGG = NVL(cp_ToDate(_read_.SRCODSES),cp_NullValue(_read_.SRCODSES))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Generazione singola Prestazione
      this.w_OBJ.bParass = iif(this.w_PARASS>1 AND NOT EMPTY(this.w_NUMSOGG), .t. , .f.)
      this.w_RESULT = this.w_OBJ.CreatePrest()
      this.w_CODPRA = this.w_OBJ.w_PRNUMPRA
      if this.w_OBJ.w_PRRIGPRE="S"
        this.w_PRSERAGG = this.w_OBJ.w_PRSERIAL
      endif
    endif
    if EMPTY(this.w_RESULT)
      * --- commit
      cp_EndTrs(.t.)
      if this.w_VISPRESTAZ = "S"
        * --- Insert into TMP_CERT
        i_nConn=i_TableProp[this.TMP_CERT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_CERT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_CERT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PRSERIAL"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_OBJ.w_PRSERIAL),'TMP_CERT','PRSERIAL');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PRSERIAL',this.w_OBJ.w_PRSERIAL)
          insert into (i_cTable) (PRSERIAL &i_ccchkf. );
             values (;
               this.w_OBJ.w_PRSERIAL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.NUM_PRE = this.NUM_PRE + 1
      endif
    else
      * --- Raise
      i_Error=this.w_RESULT
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione prestazioni rapide associate ad una attivit�
    this.w_ObjMPR.FirstRow()     
    this.w_OKDelPRES = .T.
    do while ! this.w_ObjMPR.Eof_Trs()
      this.w_ObjMPR.SetRow()     
      do case
        case this.w_ISALT
          this.w_OKCOND = this.w_ObjMPR.w_PR__DATA = this.w_SAV_t_PR__DATA AND this.w_ObjMPR.w_PRNUMPRA = this.w_SAV_t_PRNUMPRA AND this.w_ObjMPR.w_PRCODVAL = this.w_SAV_t_PRCODVAL AND this.w_ObjMPR.w_PRCODLIS = this.w_SAV_t_PRCODLIS AND this.w_ObjMPR.w_DACODNOM= this.w_SAV_t_DACODNOM AND this.w_ObjMPR.w_DACODSED=this.w_SAV_t_DACODSED AND IIF(this.oParentObject.w_GENSINGATT="S", IIF(this.w_ObjMPR.w_DARIFPRE>0,this.w_ObjMPR.w_DARIFPRE,this.w_ObjMPR.Get("CPROWNUM"))=this.w_SAV_CPROWNUM, .t.) AND (this.oParentObject.w_SELEZ<>"S" OR this.w_ObjMPR.w_FLCOMP="S")
        case this.w_ISAHE
          this.w_OKCOND = this.w_ObjMPR.w_PR__DATA = this.w_SAV_t_PR__DATA AND this.w_ObjMPR.w_PRCODVAL = this.w_SAV_t_PRCODVAL AND this.w_ObjMPR.w_DACODNOM= this.w_SAV_t_DACODNOM AND this.w_ObjMPR.w_DACODSED=this.w_SAV_t_DACODSED AND IIF(this.oParentObject.w_GENSINGATT="S", IIF(this.w_ObjMPR.w_DARIFPRE>0,this.w_ObjMPR.w_DARIFPRE,this.w_ObjMPR.Get("CPROWNUM"))=this.w_SAV_CPROWNUM, .t.) AND (this.oParentObject.w_SELEZ<>"S" OR this.w_ObjMPR.w_FLCOMP="S")
        case this.w_ISAHR
          this.w_OKCOND = this.w_ObjMPR.w_PR__DATA = this.w_SAV_t_PR__DATA AND this.w_ObjMPR.w_PRCODVAL = this.w_SAV_t_PRCODVAL AND this.w_ObjMPR.w_PRCODLIS = this.w_SAV_t_PRCODLIS AND this.w_ObjMPR.w_DACODNOM= this.w_SAV_t_DACODNOM AND this.w_ObjMPR.w_DACODSED=this.w_SAV_t_DACODSED AND IIF(this.oParentObject.w_GENSINGATT="S", IIF(this.w_ObjMPR.w_DARIFPRE>0,this.w_ObjMPR.w_DARIFPRE,this.w_ObjMPR.Get("CPROWNUM"))=this.w_SAV_CPROWNUM, .t.) AND (this.oParentObject.w_SELEZ<>"S" OR this.w_ObjMPR.w_FLCOMP="S") AND this.w_ObjMPR.w_DALISACQ=this.w_SAV_t_DALISACQ
      endcase
      if this.w_OKCOND
        this.w_OKDelPRES = Not this.w_ISALT OR ( (!EMPTY(this.w_ObjMPR.w_PRNUMPRA) and this.w_ISALT ) and (this.w_GENPRE<>"S" OR Nvl(this.w_ObjMPR.w_TIPART," ")<>"FO" or Nvl(this.w_ObjMPR.w_DAPREZZO,0)<>0 or this.w_ObjMPR.w_FLGZER="S"))
        if this.w_OKDelPRES
          this.w_ObjMPR.DeleteRow()     
        endif
      endif
      this.w_ObjMPR.NextRow()     
    enddo
    * --- Si riposiziona sul cursore TEMP
    Select "TEMP"
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='PAR_AGEN'
    this.cWorkTables[3]='CAUMATTI'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='OFF_PART'
    this.cWorkTables[6]='OFFDATTI'
    this.cWorkTables[7]='DIPENDEN'
    this.cWorkTables[8]='COM_ATTI'
    this.cWorkTables[9]='OFF_NOMI'
    this.cWorkTables[10]='TIP_DOCU'
    this.cWorkTables[11]='ART_ICOL'
    this.cWorkTables[12]='RIS_ESTR'
    this.cWorkTables[13]='*TMP_CERT'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_gsag2scu')
      use in _Curs_gsag2scu
    endif
    if used('_Curs_gsag2scu')
      use in _Curs_gsag2scu
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
