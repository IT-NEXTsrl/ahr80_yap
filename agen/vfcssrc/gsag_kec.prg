* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kec                                                        *
*              Abbina elementi contratto                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-09-29                                                      *
* Last revis.: 2015-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kec",oParentObject))

* --- Class definition
define class tgsag_kec as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 796
  Height = 520
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-23"
  HelpContextID=65677161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  IMP_DETT_IDX = 0
  IMP_MAST_IDX = 0
  OFF_NOMI_IDX = 0
  cPrg = "gsag_kec"
  cComment = "Abbina elementi contratto"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  w_CODICE = space(15)
  o_CODICE = space(15)
  w_DESCRI = space(50)
  w_COMPIMPKEYREAD = 0
  w_DADATINI = ctod('  /  /  ')
  w_COMPIMPKEY = 0
  o_COMPIMPKEY = 0
  w_ATCODSED = space(5)
  w_DATFIN = ctod('  /  /  ')
  w_OLDCOMP = space(10)
  w_MODESCRI = space(50)
  w_CODESCON_ZOOM = space(50)
  w_IMDESCRI_ZOOM = space(50)
  w_IMDESCON = space(50)
  w_ANDESCRI = space(50)
  w_IMPIANTO = space(10)
  o_IMPIANTO = space(10)
  w_DESIMP = space(50)
  w_COMPIMP = space(50)
  o_COMPIMP = space(50)
  w_DACONTRA = space(10)
  w_DACODMOD = space(10)
  w_DACODIMP = space(10)
  w_DACOCOMP = 0
  w_DARINNOV = 0
  w_DACODATT = space(20)
  w_DACODICE = space(20)
  w_DAUNIMIS = space(3)
  w_FLUPDRIG = .F.
  w_DAPREZZO = 0
  w_DACODSER = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_DAQTAMOV = 0
  w_IMDESCRI = space(50)
  w_CODCLI = space(15)
  o_CODCLI = space(15)
  w_CODNOM = space(15)
  w_ARDESART = space(50)
  w_zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kecPag1","gsag_kec",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_zoom = this.oPgFrm.Pages(1).oPag.zoom
    DoDefault()
    proc Destroy()
      this.w_zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='IMP_DETT'
    this.cWorkTables[3]='IMP_MAST'
    this.cWorkTables[4]='OFF_NOMI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_CODICE=space(15)
      .w_DESCRI=space(50)
      .w_COMPIMPKEYREAD=0
      .w_DADATINI=ctod("  /  /  ")
      .w_COMPIMPKEY=0
      .w_ATCODSED=space(5)
      .w_DATFIN=ctod("  /  /  ")
      .w_OLDCOMP=space(10)
      .w_MODESCRI=space(50)
      .w_CODESCON_ZOOM=space(50)
      .w_IMDESCRI_ZOOM=space(50)
      .w_IMDESCON=space(50)
      .w_ANDESCRI=space(50)
      .w_IMPIANTO=space(10)
      .w_DESIMP=space(50)
      .w_COMPIMP=space(50)
      .w_DACONTRA=space(10)
      .w_DACODMOD=space(10)
      .w_DACODIMP=space(10)
      .w_DACOCOMP=0
      .w_DARINNOV=0
      .w_DACODATT=space(20)
      .w_DACODICE=space(20)
      .w_DAUNIMIS=space(3)
      .w_FLUPDRIG=.f.
      .w_DAPREZZO=0
      .w_DACODSER=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_DAQTAMOV=0
      .w_IMDESCRI=space(50)
      .w_CODCLI=space(15)
      .w_CODNOM=space(15)
      .w_ARDESART=space(50)
      .w_DADATINI=oParentObject.w_DADATINI
      .w_DACONTRA=oParentObject.w_DACONTRA
      .w_DACODMOD=oParentObject.w_DACODMOD
      .w_DACODIMP=oParentObject.w_DACODIMP
      .w_DACOCOMP=oParentObject.w_DACOCOMP
      .w_DARINNOV=oParentObject.w_DARINNOV
      .w_DACODATT=oParentObject.w_DACODATT
      .w_DACODICE=oParentObject.w_DACODICE
      .w_DAUNIMIS=oParentObject.w_DAUNIMIS
      .w_FLUPDRIG=oParentObject.w_FLUPDRIG
      .w_DAPREZZO=oParentObject.w_DAPREZZO
      .w_DACODSER=oParentObject.w_DACODSER
      .w_DAQTAMOV=oParentObject.w_DAQTAMOV
        .w_TIPCON = 'C'
        .w_CODICE = this.oparentobject.w_CODCLI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODICE))
          .link_1_2('Full')
        endif
      .oPgFrm.Page1.oPag.zoom.Calculate()
          .DoRTCalc(3,3,.f.)
        .w_COMPIMPKEYREAD = .w_COMPIMPKEY
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_COMPIMPKEYREAD))
          .link_1_7('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_COMPIMPKEY = 0
          .DoRTCalc(7,7,.f.)
        .w_DATFIN = THIS.OPARENTOBJECT.w_DATFIN
          .DoRTCalc(9,9,.f.)
        .w_MODESCRI = nvl(.w_zoom.GETVAR("MODESCRI"),'')
        .w_CODESCON_ZOOM = nvl(.w_zoom.GETVAR("CODESCON"),'')
        .w_IMDESCRI_ZOOM = nvl(.w_zoom.GETVAR("IMDESCRI"),'')
        .w_IMDESCON = nvl(.w_zoom.GETVAR("IMDESCON"),'')
        .w_ANDESCRI = nvl(.w_zoom.GETVAR("ANDESCRI"),'')
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_IMPIANTO))
          .link_1_23('Full')
        endif
          .DoRTCalc(16,17,.f.)
        .w_DACONTRA = .w_ZOOM.GETVAR('ELCONTRA')
        .w_DACODMOD = .w_ZOOM.GETVAR('ELCODMOD')
        .w_DACODIMP = .w_ZOOM.GETVAR('ELCODIMP')
        .w_DACOCOMP = .w_ZOOM.GETVAR('ELCODCOM')
        .w_DARINNOV = .w_ZOOM.GETVAR('ELRINNOV')
        .w_DACODATT = IIF(UPPER(g_APPLICATION)=='ADHOC REVOLUTION' ,.w_ZOOM.GETVAR('ELCODART'),SPACE(20))
        .w_DACODICE = IIF(UPPER(g_APPLICATION)=='AD HOC ENTERPRISE' ,.w_ZOOM.GETVAR('ELCODART'),SPACE(20))
          .DoRTCalc(25,25,.f.)
        .w_FLUPDRIG = .T.
          .DoRTCalc(27,27,.f.)
        .w_DACODSER = IIF(isAhe(), this.oParentObject.w_DACODICE, this.oParentObject.w_DACODATT )
        .w_OBTEST = i_DATSYS
          .DoRTCalc(30,31,.f.)
        .w_CODCLI = .w_CODICE
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_CODCLI))
          .link_1_47('Full')
        endif
          .DoRTCalc(33,33,.f.)
        .w_ARDESART = nvl(.w_zoom.GETVAR("ARDESART"),'')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DADATINI=.w_DADATINI
      .oParentObject.w_DACONTRA=.w_DACONTRA
      .oParentObject.w_DACODMOD=.w_DACODMOD
      .oParentObject.w_DACODIMP=.w_DACODIMP
      .oParentObject.w_DACOCOMP=.w_DACOCOMP
      .oParentObject.w_DARINNOV=.w_DARINNOV
      .oParentObject.w_DACODATT=.w_DACODATT
      .oParentObject.w_DACODICE=.w_DACODICE
      .oParentObject.w_DAUNIMIS=.w_DAUNIMIS
      .oParentObject.w_FLUPDRIG=.w_FLUPDRIG
      .oParentObject.w_DAPREZZO=.w_DAPREZZO
      .oParentObject.w_DACODSER=.w_DACODSER
      .oParentObject.w_DAQTAMOV=.w_DAQTAMOV
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPCON = 'C'
        if .o_CODCLI<>.w_CODCLI
            .w_CODICE = this.oparentobject.w_CODCLI
          .link_1_2('Full')
        endif
        .oPgFrm.Page1.oPag.zoom.Calculate()
        .DoRTCalc(3,3,.t.)
        if .o_IMPIANTO<>.w_IMPIANTO.or. .o_COMPIMPKEY<>.w_COMPIMPKEY
            .w_COMPIMPKEYREAD = .w_COMPIMPKEY
          .link_1_7('Full')
        endif
        .DoRTCalc(5,7,.t.)
            .w_DATFIN = THIS.OPARENTOBJECT.w_DATFIN
        .DoRTCalc(9,9,.t.)
            .w_MODESCRI = nvl(.w_zoom.GETVAR("MODESCRI"),'')
            .w_CODESCON_ZOOM = nvl(.w_zoom.GETVAR("CODESCON"),'')
            .w_IMDESCRI_ZOOM = nvl(.w_zoom.GETVAR("IMDESCRI"),'')
            .w_IMDESCON = nvl(.w_zoom.GETVAR("IMDESCON"),'')
            .w_ANDESCRI = nvl(.w_zoom.GETVAR("ANDESCRI"),'')
        .DoRTCalc(15,17,.t.)
            .w_DACONTRA = .w_ZOOM.GETVAR('ELCONTRA')
            .w_DACODMOD = .w_ZOOM.GETVAR('ELCODMOD')
            .w_DACODIMP = .w_ZOOM.GETVAR('ELCODIMP')
            .w_DACOCOMP = .w_ZOOM.GETVAR('ELCODCOM')
            .w_DARINNOV = .w_ZOOM.GETVAR('ELRINNOV')
            .w_DACODATT = IIF(UPPER(g_APPLICATION)=='ADHOC REVOLUTION' ,.w_ZOOM.GETVAR('ELCODART'),SPACE(20))
            .w_DACODICE = IIF(UPPER(g_APPLICATION)=='AD HOC ENTERPRISE' ,.w_ZOOM.GETVAR('ELCODART'),SPACE(20))
        .DoRTCalc(25,25,.t.)
            .w_FLUPDRIG = .T.
        .DoRTCalc(27,27,.t.)
            .w_DACODSER = IIF(isAhe(), this.oParentObject.w_DACODICE, this.oParentObject.w_DACODATT )
        .DoRTCalc(29,31,.t.)
        if .o_CODICE<>.w_CODICE
            .w_CODCLI = .w_CODICE
          .link_1_47('Full')
        endif
        .DoRTCalc(33,33,.t.)
            .w_ARDESART = nvl(.w_zoom.GETVAR("ARDESART"),'')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.zoom.Calculate()
    endwith
  return

  proc Calculate_VGOPZFWXMA()
    with this
          * --- Sbianca contratto e impianto
          .w_IMPIANTO = SPACE( 10 )
          .w_COMPIMP = SPACE( 50 )
          .w_DESIMP = SPACE( 50 )
          .w_COMPIMPKEY = 0
    endwith
  endproc
  proc Calculate_ZSHLSAQNMN()
    with this
          * --- Sbianca impianto
          .w_COMPIMP = SPACE( 50 )
          .w_COMPIMPKEY = 0
    endwith
  endproc
  proc Calculate_MDCVMGCFSR()
    with this
          * --- Sbianca componente
          .w_COMPIMPKEY = 0
    endwith
  endproc
  proc Calculate_ONGSRUUVWO()
    with this
          * --- Assegno intestatario
          .w_CODCLI = .OPARENTOBJECT.w_CODCLI
          .link_1_47('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCOMPIMP_1_25.enabled = this.oPgFrm.Page1.oPag.oCOMPIMP_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_9.visible=!this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCOMPIMP_1_25.visible=!this.oPgFrm.Page1.oPag.oCOMPIMP_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_45.visible=!this.oPgFrm.Page1.oPag.oBtn_1_45.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.zoom.Event(cEvent)
        if lower(cEvent)==lower("w_CODCLI Changed")
          .Calculate_VGOPZFWXMA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_IMPIANTO Changed")
          .Calculate_ZSHLSAQNMN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_COMPIMP Changed")
          .Calculate_MDCVMGCFSR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_ONGSRUUVWO()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kec
    IF CEVENT='w_zoom selected'
       This.oparentobject.oparentobject.w_DACONTRA=.w_DACONTRA
       This.oparentobject.oparentobject.w_DACODMOD=.w_DACODMOD
       This.oparentobject.oparentobject.w_DACODIMP=.w_DACODIMP
       This.oparentobject.oparentobject.w_DACOCOMP=.w_DACOCOMP
       This.oparentobject.oparentobject.w_DARINNOV=.w_DARINNOV
       This.oparentobject.oparentobject.w_DACODATT=.w_DACODATT
       This.oparentobject.oparentobject.w_DACODICE=.w_DACODICE
       This.oparentobject.oparentobject.w_DAUNIMIS=.w_DAUNIMIS
       This.oparentobject.oparentobject.w_DAPREZZO=.w_DAPREZZO
       This.oparentobject.oparentobject.w_DACODSER=.w_DACODSER
       This.oparentobject.oparentobject.w_DADATINI=.w_DADATINI
       This.oparentobject.oparentobject.w_DAQTAMOV=.w_DAQTAMOV
       THIS.ECPSAVE()
    ENDIF
    IF CEVENT='Blank'
       this.notifyevent('Aggiorna')
       this.mcalc(.t.)
    endif
    if cEvent='apri'
      opengest("A","gsag_ael","ELCODMOD", this.w_DACODMOD,"ELCONTRA", this.w_DACONTRA,"ELCODIMP", this.w_DACODIMP,"ELCODCOM", this.w_DACOCOMP, "ELRINNOV", this.w_DARINNOV)
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODICE))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODICE_1_2'),i_cWhere,'GSAR_BZC',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODICE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODICE)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(15)
      endif
      this.w_DESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMPIMPKEYREAD
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMPIMPKEYREAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMPIMPKEYREAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_COMPIMPKEYREAD);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO;
                       ,'CPROWNUM',this.w_COMPIMPKEYREAD)
            select IMCODICE,CPROWNUM,IMDESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMPIMPKEYREAD = NVL(_Link_.CPROWNUM,0)
      this.w_COMPIMP = NVL(_Link_.IMDESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_COMPIMPKEYREAD = 0
      endif
      this.w_COMPIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMPIMPKEYREAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IMPIANTO
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IMPIANTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsag_mim',True,'IMP_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IMCODICE like "+cp_ToStrODBC(trim(this.w_IMPIANTO)+"%");

          i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IMCODICE',trim(this.w_IMPIANTO))
          select IMCODICE,IMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IMPIANTO)==trim(_Link_.IMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IMPIANTO) and !this.bDontReportError
            deferred_cp_zoom('IMP_MAST','*','IMCODICE',cp_AbsName(oSource.parent,'oIMPIANTO_1_23'),i_cWhere,'gsag_mim',"Impianti",'GSAG_KEC.IMP_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',oSource.xKey(1))
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IMPIANTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_IMPIANTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_IMPIANTO)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IMPIANTO = NVL(_Link_.IMCODICE,space(10))
      this.w_DESIMP = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_IMPIANTO = space(10)
      endif
      this.w_DESIMP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IMPIANTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLI
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOTIPCLI,NOCODCLI,NOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODCLI="+cp_ToStrODBC(this.w_CODCLI);
                   +" and NOTIPCLI="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOTIPCLI',this.w_TIPCON;
                       ,'NOCODCLI',this.w_CODCLI)
            select NOTIPCLI,NOCODCLI,NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.NOCODCLI,space(15))
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_CODNOM = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOTIPCLI,1)+'\'+cp_ToStr(_Link_.NOCODCLI,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_2.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_2.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_4.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_4.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_9.value==this.w_COMPIMPKEY)
      this.oPgFrm.Page1.oPag.oCOMPIMPKEY_1_9.value=this.w_COMPIMPKEY
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESCRI_1_14.value==this.w_MODESCRI)
      this.oPgFrm.Page1.oPag.oMODESCRI_1_14.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_ZOOM_1_17.value==this.w_CODESCON_ZOOM)
      this.oPgFrm.Page1.oPag.oCODESCON_ZOOM_1_17.value=this.w_CODESCON_ZOOM
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCRI_ZOOM_1_18.value==this.w_IMDESCRI_ZOOM)
      this.oPgFrm.Page1.oPag.oIMDESCRI_ZOOM_1_18.value=this.w_IMDESCRI_ZOOM
    endif
    if not(this.oPgFrm.Page1.oPag.oIMDESCON_1_20.value==this.w_IMDESCON)
      this.oPgFrm.Page1.oPag.oIMDESCON_1_20.value=this.w_IMDESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_22.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_22.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPIANTO_1_23.value==this.w_IMPIANTO)
      this.oPgFrm.Page1.oPag.oIMPIANTO_1_23.value=this.w_IMPIANTO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMP_1_24.value==this.w_DESIMP)
      this.oPgFrm.Page1.oPag.oDESIMP_1_24.value=this.w_DESIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPIMP_1_25.value==this.w_COMPIMP)
      this.oPgFrm.Page1.oPag.oCOMPIMP_1_25.value=this.w_COMPIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESART_1_50.value==this.w_ARDESART)
      this.oPgFrm.Page1.oPag.oARDESART_1_50.value=this.w_ARDESART
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODICE = this.w_CODICE
    this.o_COMPIMPKEY = this.w_COMPIMPKEY
    this.o_IMPIANTO = this.w_IMPIANTO
    this.o_COMPIMP = this.w_COMPIMP
    this.o_CODCLI = this.w_CODCLI
    return

enddefine

* --- Define pages as container
define class tgsag_kecPag1 as StdContainer
  Width  = 792
  height = 520
  stdWidth  = 792
  stdheight = 520
  resizeXpos=554
  resizeYpos=262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODICE_1_2 as StdField with uid="MVTWFZQGLW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 174887898,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=98, Top=14, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODICE"

  func oCODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODICE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"",'',this.parent.oContained
  endproc
  proc oCODICE_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODICE
     i_obj.ecpSave()
  endproc


  add object zoom as cp_zoombox with uid="RBULWSFKGL",left=1, top=90, width=791,height=287,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.f.,cTable="CON_TRAS",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",bQueryOnLoad=.t.,bReadOnly=.t.,cMenuFile="",cZoomFile="GSAG_KDL",;
    cEvent = "Ricerca,Aggiorna,",;
    nPag=1;
    , HelpContextID = 112320486

  add object oDESCRI_1_4 as StdField with uid="CVEARYPQZU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 92384714,;
   bGlobalFont=.t.,;
    Height=21, Width=388, Left=222, Top=14, InputMask=replicate('X',50)


  add object oBtn_1_6 as StdButton with uid="PHEWMSSLEL",left=733, top=43, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 111245078;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOMPIMPKEY_1_9 as StdField with uid="SBSCAGJTGI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_COMPIMPKEY", cQueryName = "COMPIMPKEY",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 33859135,;
   bGlobalFont=.t.,;
    Height=22, Width=96, Left=9, Top=19

  func oCOMPIMPKEY_1_9.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oMODESCRI_1_14 as StdField with uid="YUTQSCCACI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 76508431,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=346, Top=378, InputMask=replicate('X',50)

  add object oCODESCON_ZOOM_1_17 as StdField with uid="EWFOXIKFWQ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODESCON_ZOOM", cQueryName = "CODESCON_ZOOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 105661340,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=346, Top=424, InputMask=replicate('X',50)

  add object oIMDESCRI_ZOOM_1_18 as StdField with uid="RWUOGMWYUL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_IMDESCRI_ZOOM", cQueryName = "IMDESCRI_ZOOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 162773695,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=346, Top=447, InputMask=replicate('X',50)

  add object oIMDESCON_1_20 as StdField with uid="FYHRCQDSWS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_IMDESCON", cQueryName = "IMDESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 191927596,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=346, Top=470, InputMask=replicate('X',50)

  add object oANDESCRI_1_22 as StdField with uid="ACNTZAUZOM",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 76507983,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=346, Top=401, InputMask=replicate('X',50)

  add object oIMPIANTO_1_23 as StdField with uid="VLMHLODQDK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_IMPIANTO", cQueryName = "IMPIANTO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 242494165,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=98, Top=41, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="IMP_MAST", cZoomOnZoom="gsag_mim", oKey_1_1="IMCODICE", oKey_1_2="this.w_IMPIANTO"

  func oIMPIANTO_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
      if .not. empty(.w_COMPIMPKEYREAD)
        bRes2=.link_1_7('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oIMPIANTO_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIMPIANTO_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'IMP_MAST','*','IMCODICE',cp_AbsName(this.parent,'oIMPIANTO_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'gsag_mim',"Impianti",'GSAG_KEC.IMP_MAST_VZM',this.parent.oContained
  endproc
  proc oIMPIANTO_1_23.mZoomOnZoom
    local i_obj
    i_obj=gsag_mim()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IMCODICE=this.parent.oContained.w_IMPIANTO
     i_obj.ecpSave()
  endproc

  add object oDESIMP_1_24 as StdField with uid="HGCGXFNTXW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESIMP", cQueryName = "DESIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 248229322,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=247, Top=41, InputMask=replicate('X',50)

  add object oCOMPIMP_1_25 as StdField with uid="AKXQSRDOOH",rtseq=17,rtrep=.f.,;
    cFormVar = "w_COMPIMP", cQueryName = "COMPIMP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Componente dell'impianto",;
    HelpContextID = 33883098,;
   bGlobalFont=.t.,;
    Height=21, Width=512, Left=98, Top=68, InputMask=replicate('X',50), bHasZoom = .t. 

  func oCOMPIMP_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_IMPIANTO))
    endwith
   endif
  endfunc

  func oCOMPIMP_1_25.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  proc oCOMPIMP_1_25.mBefore
    with this.Parent.oContained
      .w_OLDCOMP=.w_COMPIMP
    endwith
  endproc

  proc oCOMPIMP_1_25.mAfter
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M")
      endwith
  endproc

  proc oCOMPIMP_1_25.mZoom
      with this.Parent.oContained
        gsag_bat(this.Parent.oContained,.w_IMPIANTO,.w_COMPIMP,.w_OLDCOMP,CHR(87)+"_COMPIMPKEY","M",.T.)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oBtn_1_45 as StdButton with uid="MNVZAMDXTB",left=220, top=41, width=22,height=21,;
    CpPicture="bmp\ZOOM.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per ricercare per attributo";
    , HelpContextID = 65476138;
    , IMGSIZE=16;
  , bGlobalFont=.t.

    proc oBtn_1_45.Click()
      do gsag_kir with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_45.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsALT())
     endwith
    endif
  endfunc

  add object oARDESART_1_50 as StdField with uid="TMGFPNXJAL",rtseq=34,rtrep=.f.,;
    cFormVar = "w_ARDESART", cQueryName = "ARDESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 42954586,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=346, Top=493, InputMask=replicate('X',50)


  add object oBtn_1_51 as StdButton with uid="RDXFXQLGGN",left=733, top=380, width=48,height=45,;
    CpPicture="BMP\visuali.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire l'elemento contratto selezionato";
    , HelpContextID = 58299130;
    , Caption='A\<pri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_51.Click()
      this.parent.oContained.NotifyEvent("apri")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="AZEXFNHBYX",Visible=.t., Left=54, Top=14,;
    Alignment=1, Width=42, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="JMNZWRVMBC",Visible=.t., Left=189, Top=378,;
    Alignment=1, Width=156, Height=18,;
    Caption="Descrizione modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="EGSAJTEXEB",Visible=.t., Left=189, Top=424,;
    Alignment=1, Width=156, Height=18,;
    Caption="Descrizione contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="QCJADCOLDU",Visible=.t., Left=189, Top=447,;
    Alignment=1, Width=156, Height=18,;
    Caption="Descrizione impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="HLGFFRTGDE",Visible=.t., Left=189, Top=470,;
    Alignment=1, Width=156, Height=18,;
    Caption="Descrizione componente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ZPSOZWYUGS",Visible=.t., Left=189, Top=401,;
    Alignment=1, Width=156, Height=18,;
    Caption="Descrizione cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="WDSNKSJKQG",Visible=.t., Left=45, Top=41,;
    Alignment=1, Width=51, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="AURUTIHBUT",Visible=.t., Left=9, Top=68,;
    Alignment=1, Width=87, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (IsALT())
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="HWQUAQKEXQ",Visible=.t., Left=189, Top=493,;
    Alignment=1, Width=156, Height=18,;
    Caption="Descrizione servizio:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kec','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
