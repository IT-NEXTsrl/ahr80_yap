* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bgd                                                        *
*              Generazione documento da attivita                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-28                                                      *
* Last revis.: 2014-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pChiamante
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bgd",oParentObject,m.pChiamante)
return(i_retval)

define class tgsag_bgd as StdBatch
  * --- Local variables
  pChiamante = space(1)
  w_OKANA = .f.
  w_MVDATPLA = ctod("  /  /  ")
  w_READAZI = space(5)
  w_PLADOC = space(1)
  w_NUMSCO = 0
  w_MV__ANNO = 0
  w_MV__MESE = 0
  w_PAGMOR = space(5)
  w_DATMOR = ctod("  /  /  ")
  w_CLFPAG = space(5)
  w_GSIM_BAC = .NULL.
  w_GSAL_BIA = .NULL.
  w_RIGPRELE = space(1)
  w_MVSERIAL = space(10)
  w_MVALFDOC = space(10)
  w_MVALFEST = space(10)
  w_MVCAOVAL = 0
  w_MVCAUCON = space(5)
  w_MVCLADOC = space(2)
  w_MVCODAG2 = space(5)
  w_MVCODAGE = space(5)
  w_MVCODBA2 = space(15)
  w_MVCODBAN = space(10)
  w_MVCODCON = space(15)
  w_MVCODDES = space(5)
  w_MVCODESE = space(4)
  w_MVCODPAG = space(5)
  w_MVCODUTE = 0
  w_MVCODVAL = space(3)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVDATDIV = ctod("  /  /  ")
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATEST = ctod("  /  /  ")
  w_MVDATREG = ctod("  /  /  ")
  w_MVFLACCO = space(1)
  w_MVFLINTE = space(1)
  w_MVFLPROV = space(1)
  w_MVFLVEAC = space(1)
  w_MVNUMDOC = 0
  w_MVNUMEST = 0
  w_MVNUMREG = 0
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVTCAMAG = space(5)
  w_MVTCOLIS = space(5)
  w_MVTCONTR = space(15)
  w_MVTFRAGG = space(1)
  w_MVTIPCON = space(1)
  w_MVTIPDOC = space(5)
  w_MVVALNAZ = space(3)
  w_MVANNDOC = space(4)
  w_MVANNPRO = space(4)
  w_MVSCOPAG = 0
  w_MVCODIVE = space(5)
  w_MVFLGIOM = space(1)
  w_MVFLCONT = space(1)
  w_MVSCONTI = 0
  w_MVFLFOSC = space(1)
  w_MVSPEINC = 0
  w_MVIVAINC = space(5)
  w_MVFLRINC = space(1)
  w_MVSPETRA = 0
  w_MVIVATRA = space(5)
  w_MVFLRTRA = space(1)
  w_MVSPEIMB = 0
  w_MVIVAIMB = space(5)
  w_MVFLRIMB = space(1)
  w_MVSPEBOL = 0
  w_MVIVABOL = space(5)
  w_MVIMPARR = 0
  w_MVACCONT = 0
  w_MVCODSPE = space(3)
  w_MVCODVET = space(5)
  w_MVCODVE2 = space(5)
  w_MVCODVE3 = space(5)
  w_MVCODPOR = space(1)
  w_MVCONCON = space(1)
  w_MVASPEST = space(30)
  w_MVNOTAGG = space(40)
  w_MVQTACOL = 0
  w_MVQTAPES = 0
  w_MVQTALOR = 0
  w_MVTIPORN = space(1)
  w_MVCODORN = space(15)
  w_MVORATRA = space(2)
  w_MVDATTRA = ctod("  /  /  ")
  w_MVMINTRA = space(2)
  w_MVGENEFF = space(1)
  w_MVGENPRO = space(1)
  w_MVACIVA1 = space(5)
  w_MVACIVA2 = space(5)
  w_MVACIVA3 = space(5)
  w_MVACIVA4 = space(5)
  w_MVACIVA5 = space(5)
  w_MVACIVA6 = space(5)
  w_MVAIMPN1 = 0
  w_MVAIMPN2 = 0
  w_MVAIMPN3 = 0
  w_MVAIMPN4 = 0
  w_MVAIMPN5 = 0
  w_MVAIMPN6 = 0
  w_MVAIMPS1 = 0
  w_MVAIMPS2 = 0
  w_MVAIMPS3 = 0
  w_MVAIMPS4 = 0
  w_MVAIMPS5 = 0
  w_MVAIMPS6 = 0
  w_MVAFLOM1 = space(1)
  w_MVAFLOM2 = space(1)
  w_MVAFLOM3 = space(1)
  w_MVAFLOM4 = space(1)
  w_MVAFLOM5 = space(1)
  w_MVAFLOM6 = space(1)
  w_MVPRD = space(2)
  w_MVRIFCON = space(10)
  w_UTCV = 0
  w_UTDV = ctod("  /  /  ")
  w_UTCC = 0
  w_UTDC = ctod("  /  /  ")
  w_MVPRP = space(2)
  w_MVTINCOM = ctod("  /  /  ")
  w_MVTFICOM = ctod("  /  /  ")
  w_MVRIFDIC = space(15)
  w_MVFLSCOR = space(1)
  w_MVDESDOC = space(50)
  w_MVTIPOPE = space(10)
  w_MVCATOPE = space(2)
  w_MVNUMCOR = space(25)
  w_MVIVAARR = space(5)
  w_MVACCPRE = 0
  w_MVMAXACC = 0
  w_MVACCSUC = 0
  w_MVTOTRIT = 0
  w_MVTOTENA = 0
  w_MVRIFPIA = space(10)
  w_MVRIFACC = space(10)
  w_MVRIFFAD = space(10)
  w_MVPERRET = 0
  w_MVANNRET = space(4)
  w_MVFLINTR = space(1)
  w_MVTRAINT = 0
  w_MV__NOTE = space(0)
  w_MVFLSALD = space(1)
  w_MVFLCAPA = space(1)
  w_MVFLSCAF = space(1)
  w_MVCODASP = space(3)
  w_MVFLVABD = space(1)
  w_MVFLSCOM = space(1)
  w_MVDATPLA = ctod("  /  /  ")
  w_MVTIPPER = space(1)
  w_MVRIFESP = space(10)
  w_MVMOVCOM = space(10)
  w_MVRIFODL = space(10)
  w_MVRIFDCO = space(10)
  w_MVFLBLOC = space(1)
  w_MVSERDDT = space(10)
  w_MVROWDDT = 0
  w_MVFLOFFE = space(1)
  w_MVCODSED = space(5)
  w_MVACCOLD = 0
  w_MVSERWEB = space(50)
  w_MVRITPRE = 0
  w_MVPERFIN = 0
  w_MVIMPFIN = 0
  w_MVFLSFIN = space(1)
  w_MVCAUIMB = 0
  w_MVIVACAU = space(5)
  w_MVRITATT = 0
  w_MVFLFOCA = space(1)
  w_MVTIPIMB = space(1)
  w_MVSEREST = space(30)
  w_MVFLSEND = space(1)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MVNUMRIF = 0
  w_MVCODICE = space(20)
  w_MVTIPRIG = space(1)
  w_MVDESART = space(40)
  w_MVCODART = space(20)
  w_MVUNIMIS = space(3)
  w_MVCATCON = space(5)
  w_MVDESSUP = space(0)
  w_MVCONIND = space(15)
  w_MVCAUMAG = space(5)
  w_MVCONTRO = space(15)
  w_MVCAUCOL = space(5)
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVCODCLA = space(3)
  w_MVCODLIS = space(5)
  w_MVQTAMOV = 0
  w_MVQTAUM1 = 0
  w_MVPREZZO = 0
  w_MVSCONT1 = 0
  w_MVSCONT2 = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_MVFLOMAG = space(1)
  w_MVCODIVA = space(5)
  w_MVVALRIG = 0
  w_MVIMPACC = 0
  w_MVCONTRA = space(15)
  w_MVVALMAG = 0
  w_MVIMPNAZ = 0
  w_MVIMPSCO = 0
  w_MVFLCASC = space(1)
  w_MVF2CASC = space(1)
  w_MVKEYSAL = space(20)
  w_MVF2ORDI = space(1)
  w_MVFLORDI = space(1)
  w_MVF2IMPE = space(1)
  w_MVFLIMPE = space(0)
  w_MVF2RISE = space(1)
  w_MVFLRISE = space(0)
  w_MVPERPRO = 0
  w_MVIMPPRO = 0
  w_MVPESNET = 0
  w_MVFLTRAS = space(1)
  w_MVNOMENC = space(8)
  w_MVUMSUPP = space(3)
  w_MVNAZPRO = space(3)
  w_MVMOLSUP = 0
  w_MVPROORD = space(2)
  w_MVNUMCOL = 0
  w_MVTIPCOL = space(20)
  w_MVCODCOM = space(15)
  w_MVQTAEVA = 0
  w_MVQTAEV1 = 0
  w_MVFLRAGG = space(1)
  w_MVIMPEVA = 0
  w_MVQTASAL = 0
  w_MVEFFEVA = ctod("  /  /  ")
  w_MVDATEVA = ctod("  /  /  ")
  w_MVFLELGM = space(1)
  w_MVFLEVAS = space(1)
  w_MVROWRIF = 0
  w_MVSERRIF = space(10)
  w_MVQTANOC = 0
  w_MVFLARIF = space(1)
  w_MVFLRESC = space(1)
  w_MVFLERIF = space(1)
  w_MVTIPATT = space(1)
  w_MVQTARES = 0
  w_MVCODCOS = space(5)
  w_MVFLRIAP = space(1)
  w_MVPRECON = 0
  w_MVFLORCO = space(1)
  w_MVFLCOCO = space(1)
  w_MVFLULPV = space(1)
  w_MVFLULCA = space(1)
  w_MVIMPCOM = 0
  w_MVVALULT = 0
  w_MVINICOM = ctod("  /  /  ")
  w_MVDATGEN = ctod("  /  /  ")
  w_MVFINCOM = ctod("  /  /  ")
  w_MVRIFORD = space(10)
  w_MVCODATT = space(15)
  w_MVCODODL = space(15)
  w_MVCODCEN = space(15)
  w_MVVOCCEN = space(15)
  w_MVCODCOL = space(5)
  w_MVFLRIPA = space(1)
  w_MVIMPRBA = 0
  w_MVFLELAN = space(1)
  w_MVRIFESC = space(10)
  w_MVRIGMAT = 0
  w_MVF2LOTT = space(1)
  w_MVFLLOTT = space(1)
  w_MVCODLOT = space(20)
  w_MVCODUB2 = space(20)
  w_MV_FLAGG = space(1)
  w_MVFLRVCL = space(1)
  w_MVCODUBI = space(20)
  w_MVQTAIM1 = 0
  w_MVTIPPRO = space(2)
  w_MV_SEGNO = space(1)
  w_MVCODCES = space(20)
  w_MVCESSER = space(10)
  w_MVQTAIMP = 0
  w_MVIMPAC2 = 0
  w_MVUNILOG = space(18)
  w_MVPROCAP = 0
  w_MVIMPCAP = 0
  w_MVTIPPR2 = space(2)
  w_MVFLNOAN = space(1)
  w_MVMC_PER = space(3)
  w_MVDATOAI = ctod("  /  /  ")
  w_MVDATOAF = ctod("  /  /  ")
  w_MVPAEFOR = space(3)
  w_MVAIRPOR = space(10)
  w_MVRIFEDI = space(14)
  w_MVCODRES = space(5)
  w_ARUTISER = space(1)
  w_MVRIGPRE = space(1)
  w_MVRIFPRE = 0
  w_DEOREEFF = 0
  w_DEMINEFF = 0
  w_DECOSINT = 0
  w_DEPREMIN = 0
  w_DEPREMAX = 0
  w_DEGAZUFF = space(6)
  w_DETIPPRA = space(10)
  w_DEMATPRA = space(10)
  w_CHIRIG = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  ESERCIZI_idx=0
  VALUTE_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  DOC_DETT_idx=0
  OFF_ATTI_idx=0
  CON_PAGA_idx=0
  DOC_RATE_idx=0
  OFFDATTI_idx=0
  ALT_DETT_idx=0
  CAN_TIER_idx=0
  AZIENDA_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Documento da Attivit� da modulo import GSAL_BI2
    * --- Evito l'aggiornamento della gestione chiamante
    *     (in questo caso l'oggetto che contiene le attivit� da creare
    *     le cui variabili non necessitano di aggiornamenti al termine della routine)
    this.bUpdateParentObject = .F.
    if this.oParentObject.w_ATSTATUS="P"
      this.w_OKANA = .f.
      if VARTYPE(this.pChiamante)<>"C" OR (VARTYPE(this.pChiamante)="C" AND this.pChiamante="AT")
        * --- Scrive DOC_MAST se proviene da attivit� oppure se sta inserendo in GSAL_BIA l'attivit� OFF_ATTI
        if this.oParentObject.w_CFUNC="Load" OR EMPTY(this.oParentObject.w_ATRIFDOC)
          this.w_MVSERIAL = space(10)
          i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
          cp_NextTableProg(this, i_Conn, "SEDOC", "i_codazi,w_MVSERIAL")
        else
          this.w_MVSERIAL = this.oParentObject.w_ATRIFDOC
          * --- Try
          local bErr_03A7D368
          bErr_03A7D368=bTrsErr
          this.Try_03A7D368()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03A7D368
          * --- End
        endif
        * --- Esercizio
        this.w_MVCODESE = g_CodEse
        this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
        this.w_READAZI = i_CODAZI
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZPLADOC,AZFLVEBD"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.w_READAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZPLADOC,AZFLVEBD;
            from (i_cTable) where;
                AZCODAZI = this.w_READAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PLADOC = NVL(cp_ToDate(_read_.AZPLADOC),cp_NullValue(_read_.AZPLADOC))
          this.w_MVFLVABD = NVL(cp_ToDate(_read_.AZFLVEBD),cp_NullValue(_read_.AZFLVEBD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Utente
        this.w_MVTIPCON = ""
        this.w_MVCODCON = ""
        this.w_MVCLADOC = "DI"
        this.w_MVCAUCON = .NULL.
        this.w_MVTCAMAG = "NULL"
        this.w_MVCODMAG = .NULL.
        this.w_MVFLACCO = ""
        this.w_MVFLINTE = "N"
        this.w_MVFLVEAC = "V"
        this.w_MVTFRAGG = "1"
        * --- Lettura da Causale Documento
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDALFDOC,TDCATDOC,TDCAUMAG,TDCODMAG,TDFLACCO,TDFLINTE,TDFLVEAC,TDPRODOC,TFFLRAGG,TDCAUCON,TDFLELAN,TD_SEGNO,TDNUMSCO"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDALFDOC,TDCATDOC,TDCAUMAG,TDCODMAG,TDFLACCO,TDFLINTE,TDFLVEAC,TDPRODOC,TFFLRAGG,TDCAUCON,TDFLELAN,TD_SEGNO,TDNUMSCO;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_CAUDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
          this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
          this.w_MVTCAMAG = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
          this.w_MVCODMAG = NVL(cp_ToDate(_read_.TDCODMAG),cp_NullValue(_read_.TDCODMAG))
          this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
          this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
          this.w_MVPRD = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
          this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
          this.w_MVCAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
          this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          this.w_MVFLELAN = NVL(cp_ToDate(_read_.TDFLELAN),cp_NullValue(_read_.TDFLELAN))
          this.w_MV_SEGNO = NVL(cp_ToDate(_read_.TD_SEGNO),cp_NullValue(_read_.TD_SEGNO))
          this.w_NUMSCO = NVL(cp_ToDate(_read_.TDNUMSCO),cp_NullValue(_read_.TDNUMSCO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NVL(this.w_MVFLINTE, "N")<>"N" and Vartype( this.oParentObject.w_CODCLI )="C"
          this.w_MVTIPCON = this.oParentObject.w_ATTIPCLI
          this.w_MVCODCON = this.oParentObject.w_CODCLI
        endif
        * --- Causale Documento da Attivit�
        this.w_MVTIPDOC = this.oParentObject.w_CAUDOC
        * --- Data Registrazione
        this.w_MVDATREG = TTOD(this.oParentObject.w_ATDATINI)
        * --- Data Prestazione
        this.w_MVDATOAI = this.w_MVDATREG
        * --- Data Documento
        this.w_MVDATDOC = this.w_MVDATREG
        this.w_MV__ANNO = YEAR(this.w_MVDATDOC)
        this.w_MV__MESE = MONTH(this.w_MVDATDOC)
        this.w_MVDATPLA = IIF(this.w_PLADOC="S",this.w_MVDATDOC,this.w_MVDATREG)
        this.w_MVPRP = "NN"
        * --- Data Competenza IVA
        this.w_MVDATCIV = i_datsys
        * --- Scorporo a piede fattura
        this.w_MVFLSCOR = "N"
        * --- Lettura valuta di esercizio
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESVALNAZ"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(this.w_MVCODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESVALNAZ;
            from (i_cTable) where;
                ESCODAZI = i_CODAZI;
                and ESCODESE = this.w_MVCODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVVALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Valuta documento da Attvivit�
        this.w_MVCODVAL = this.oParentObject.w_ATCODVAL
        * --- Lettura cambio
        this.w_MVCAOVAL = GETCAM(this.w_MVCODVAL, this.w_MVDATDOC, 7)
        * --- Listino da Attivit�
        this.w_MVTCOLIS = this.oParentObject.w_ATCODLIS
        * --- Documento Confermato
        this.w_MVFLPROV = "N"
        * --- Anno documento: valorizzzato tramite la data documento
        this.w_MVANNDOC = str(year(this.w_MVDATDOC),4,0)
        * --- Codici IVA di testata: Trasporto, Imballo, Incasso e Bolli
        this.w_MVIVATRA = g_COITRA
        this.w_MVIVAIMB = g_COIIMB
        this.w_MVIVAINC = g_COIINC
        this.w_MVIVABOL = g_COIBOL
        this.w_MVMINTRA = substr(time(),4,2)
        this.w_MVORATRA = left(time(),2)
        this.w_MVDATTRA = this.w_MVDATREG
        this.w_MVFLSCOM = IIF(g_FLSCOM="S" AND this.w_MVDATDOC<g_DTSCOM AND !empty(nvl(g_DTSCOM,cp_CharToDate("  /  /    ")))," ",g_FLSCOM)
        this.w_MVIVACAU = g_COICAU
        if Not isalt()
          * --- Non gestito da alterego
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANPAGPAR,ANDATMOR,ANCODPAG"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANPAGPAR,ANDATMOR,ANCODPAG;
              from (i_cTable) where;
                  ANTIPCON = this.w_MVTIPCON;
                  and ANCODICE = this.w_MVCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PAGMOR = NVL(cp_ToDate(_read_.ANPAGPAR),cp_NullValue(_read_.ANPAGPAR))
            this.w_DATMOR = NVL(cp_ToDate(_read_.ANDATMOR),cp_NullValue(_read_.ANDATMOR))
            this.w_CLFPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_MVCODPAG = IIF(this.w_DATMOR>=this.w_MVDATDOC AND NOT EMPTY(this.w_PAGMOR), this.w_PAGMOR,this.w_CLFPAG)
        endif
        * --- Aggiorna progressivo Numero Documento
        if EMPTY(this.oParentObject.w_ATRIFDOC) 
          * --- Solo in caricamento, in variazione ho gi� salvato numero documento e numero registrazione
          if this.w_MVPRD<>"NN"
            i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
            cp_NextTableProg(this, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
          endif
          * --- Numero Registrazione
          this.w_MVNUMREG = this.w_MVNUMDOC
        endif
        * --- Note da Attivit�
        this.w_MV__NOTE = this.oParentObject.w_ATNOTPIA
        * --- Try
        local bErr_03A72B58
        bErr_03A72B58=bTrsErr
        this.Try_03A72B58()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03A72B58
        * --- End
        * --- Write into DOC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVCODESE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODESE),'DOC_MAST','MVCODESE');
          +",MVCODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODUTE),'DOC_MAST','MVCODUTE');
          +",MVTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPCON),'DOC_MAST','MVTIPCON');
          +",MVCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCON),'DOC_MAST','MVCODCON');
          +",MVALFDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'DOC_MAST','MVALFDOC');
          +",MVCLADOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVCLADOC),'DOC_MAST','MVCLADOC');
          +",MVTCAMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVTCAMAG),'DOC_MAST','MVTCAMAG');
          +",MVFLACCO ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLACCO),'DOC_MAST','MVFLACCO');
          +",MVFLINTE ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLINTE),'DOC_MAST','MVFLINTE');
          +",MVFLVEAC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVFLVEAC');
          +",MVEMERIC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVEMERIC');
          +",MVPRD ="+cp_NullLink(cp_ToStrODBC(this.w_MVPRD),'DOC_MAST','MVPRD');
          +",MVTFRAGG ="+cp_NullLink(cp_ToStrODBC(this.w_MVTFRAGG),'DOC_MAST','MVTFRAGG');
          +",MVCAUCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUCON),'DOC_MAST','MVCAUCON');
          +",MVTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPDOC),'DOC_MAST','MVTIPDOC');
          +",MVDATREG ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATREG),'DOC_MAST','MVDATREG');
          +",MVDATDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DOC_MAST','MVDATDOC');
          +",MVPRP ="+cp_NullLink(cp_ToStrODBC(this.w_MVPRP),'DOC_MAST','MVPRP');
          +",MVDATCIV ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATCIV),'DOC_MAST','MVDATCIV');
          +",MVFLSCOR ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOR),'DOC_MAST','MVFLSCOR');
          +",MVVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALNAZ),'DOC_MAST','MVVALNAZ');
          +",MVCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODVAL),'DOC_MAST','MVCODVAL');
          +",MVCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAOVAL),'DOC_MAST','MVCAOVAL');
          +",MVTCOLIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVTCOLIS),'DOC_MAST','MVTCOLIS');
          +",MVFLPROV ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLPROV),'DOC_MAST','MVFLPROV');
          +",MVANNDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVANNDOC),'DOC_MAST','MVANNDOC');
          +",MVIVATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVATRA),'DOC_MAST','MVIVATRA');
          +",MVIVAIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAIMB),'DOC_MAST','MVIVAIMB');
          +",MVIVAINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAINC),'DOC_MAST','MVIVAINC');
          +",MVIVABOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVABOL),'DOC_MAST','MVIVABOL');
          +",MVFLVABD ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLVABD),'DOC_MAST','MVFLVABD');
          +",MVNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
          +",MV__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_MV__NOTE),'DOC_MAST','MV__NOTE');
          +",MVFLSCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOM),'DOC_MAST','MVFLSCOM');
          +",MVNUMREG ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'DOC_MAST','MVNUMREG');
          +",MVMINTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVMINTRA),'DOC_MAST','MVMINTRA');
          +",MVDATTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATTRA),'DOC_MAST','MVDATTRA');
          +",MVDATPLA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATPLA),'DOC_MAST','MVDATPLA');
          +",MVORATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVORATRA),'DOC_MAST','MVORATRA');
          +",MVIVACAU ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVACAU),'DOC_MAST','MVIVACAU');
          +",MV__ANNO ="+cp_NullLink(cp_ToStrODBC(this.w_MV__ANNO),'DOC_MAST','MV__ANNO');
          +",MVCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODPAG),'DOC_MAST','MVCODPAG');
          +",MV__MESE ="+cp_NullLink(cp_ToStrODBC(this.w_MV__MESE),'DOC_MAST','MV__MESE');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                 )
        else
          update (i_cTable) set;
              MVCODESE = this.w_MVCODESE;
              ,MVCODUTE = this.w_MVCODUTE;
              ,MVTIPCON = this.w_MVTIPCON;
              ,MVCODCON = this.w_MVCODCON;
              ,MVALFDOC = this.w_MVALFDOC;
              ,MVCLADOC = this.w_MVCLADOC;
              ,MVTCAMAG = this.w_MVTCAMAG;
              ,MVFLACCO = this.w_MVFLACCO;
              ,MVFLINTE = this.w_MVFLINTE;
              ,MVFLVEAC = this.w_MVFLVEAC;
              ,MVEMERIC = this.w_MVFLVEAC;
              ,MVPRD = this.w_MVPRD;
              ,MVTFRAGG = this.w_MVTFRAGG;
              ,MVCAUCON = this.w_MVCAUCON;
              ,MVTIPDOC = this.w_MVTIPDOC;
              ,MVDATREG = this.w_MVDATREG;
              ,MVDATDOC = this.w_MVDATDOC;
              ,MVPRP = this.w_MVPRP;
              ,MVDATCIV = this.w_MVDATCIV;
              ,MVFLSCOR = this.w_MVFLSCOR;
              ,MVVALNAZ = this.w_MVVALNAZ;
              ,MVCODVAL = this.w_MVCODVAL;
              ,MVCAOVAL = this.w_MVCAOVAL;
              ,MVTCOLIS = this.w_MVTCOLIS;
              ,MVFLPROV = this.w_MVFLPROV;
              ,MVANNDOC = this.w_MVANNDOC;
              ,MVIVATRA = this.w_MVIVATRA;
              ,MVIVAIMB = this.w_MVIVAIMB;
              ,MVIVAINC = this.w_MVIVAINC;
              ,MVIVABOL = this.w_MVIVABOL;
              ,MVFLVABD = this.w_MVFLVABD;
              ,MVNUMDOC = this.w_MVNUMDOC;
              ,MV__NOTE = this.w_MV__NOTE;
              ,MVFLSCOM = this.w_MVFLSCOM;
              ,MVNUMREG = this.w_MVNUMDOC;
              ,MVMINTRA = this.w_MVMINTRA;
              ,MVDATTRA = this.w_MVDATTRA;
              ,MVDATPLA = this.w_MVDATPLA;
              ,MVORATRA = this.w_MVORATRA;
              ,MVIVACAU = this.w_MVIVACAU;
              ,MV__ANNO = this.w_MV__ANNO;
              ,MVCODPAG = this.w_MVCODPAG;
              ,MV__MESE = this.w_MV__MESE;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore scrittura testata documento'
          return
        endif
      endif
      do case
        case VARTYPE(this.pChiamante)="C" AND (UPPER(this.pChiamante)=="PR" OR UPPER(this.pChiamante)=="NP")
          * --- Sta inserendo in GSAL_BIA le prestazioni in OFFDATTI
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case VARTYPE(this.pChiamante)="C" AND UPPER(this.pChiamante)=="AT"
          * --- Restituisce a GSAL_BIA il seriale del dcumento
          this.oParentObject.w_AppoDocInt = this.w_MVSERIAL
      endcase
    endif
  endproc
  proc Try_03A7D368()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Recupero Numero Registrazione e Numero Documento
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVNUMREG,MVNUMDOC"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVNUMREG,MVNUMDOC;
        from (i_cTable) where;
            MVSERIAL = this.w_MVSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVNUMREG = NVL(cp_ToDate(_read_.MVNUMREG),cp_NullValue(_read_.MVNUMREG))
      this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Elimina Documento
    * --- Delete from CON_PAGA
    i_nConn=i_TableProp[this.CON_PAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_PAGA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CPSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      delete from (i_cTable) where;
            CPSERIAL = this.w_MVSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from DOC_RATE
    i_nConn=i_TableProp[this.DOC_RATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RSSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      delete from (i_cTable) where;
            RSSERIAL = this.w_MVSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      delete from (i_cTable) where;
            MVSERIAL = this.w_MVSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if IsAlt()
      * --- Delete from ALT_DETT
      i_nConn=i_TableProp[this.ALT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ALT_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"DESERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
               )
      else
        delete from (i_cTable) where;
              DESERIAL = this.w_MVSERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Delete from DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      delete from (i_cTable) where;
            MVSERIAL = this.w_MVSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_03A72B58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_MAST','MVSERIAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_MVSERIAL)
      insert into (i_cTable) (MVSERIAL &i_ccchkf. );
         values (;
           this.w_MVSERIAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento testata documento'
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Singolo detail da GSAL_BIA
    * --- Numero Riga
    this.w_CPROWNUM = this.oParentObject.w_DAROWNUM
    this.w_CPROWORD = this.oParentObject.w_DAROWNUM*10
    this.w_MVNUMRIF = IIF(UPPER(this.pChiamante)=="PR", -20, -70)
    * --- Chiave di riga del dettaglio attivit�
    this.w_CHIRIG = this.w_CPROWNUM
    * --- Prestazione
    this.w_MVCODICE = this.oParentObject.w_DACODATT
    * --- Lettura dai codici di ricerca dell'Articolo (Prestazione) e tipologia riga
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CA__TIPO,CACODART"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CA__TIPO,CACODART;
        from (i_cTable) where;
            CACODICE = this.w_MVCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
      this.w_MVCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MVCODCLA = "   "
    * --- Lettura dagli articoli della tipologia iva,  Tipologia per doc. e categoria contabile
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARCODIVA,ARCODCLA,ARCATCON,ARUTISER"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARCODIVA,ARCODCLA,ARCATCON,ARUTISER;
        from (i_cTable) where;
            ARCODART = this.w_MVCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVCODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
      this.w_MVCODCLA = NVL(cp_ToDate(_read_.ARCODCLA),cp_NullValue(_read_.ARCODCLA))
      this.w_MVCATCON = NVL(cp_ToDate(_read_.ARCATCON),cp_NullValue(_read_.ARCATCON))
      this.w_ARUTISER = NVL(cp_ToDate(_read_.ARUTISER),cp_NullValue(_read_.ARUTISER))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Descrizione
    this.w_MVDESART = this.oParentObject.w_DADESATT
    * --- Unit� di Misura
    this.w_MVUNIMIS = w_DAUNIMIS
    * --- Descrizione aggiuntiva
    this.w_MVDESSUP = this.oParentObject.w_DADESAGG
    * --- Causale magazzino (uguale a quella di testata - da causale documento - deve essere 'NULL')
    this.w_MVCAUMAG = this.w_MVTCAMAG
    this.w_MVQTAMOV = this.oParentObject.w_DAQTAMOV
    this.w_MVPREZZO = this.oParentObject.w_DAPREZZO
    this.w_MVSCONT1 = this.oParentObject.w_DASCONT1
    this.w_MVSCONT2 = this.oParentObject.w_DASCONT2
    this.w_MVSCONT3 = this.oParentObject.w_DASCONT3
    this.w_MVSCONT4 = this.oParentObject.w_DASCONT4
    this.w_MVVALRIG = this.oParentObject.w_DAVALRIG
    this.w_MVQTAUM1 = this.w_MVQTAMOV
    this.w_MVVALMAG = this.w_MVVALRIG
    this.w_MVIMPNAZ = this.w_MVVALRIG
    this.w_MVDATOAI = TTOD(this.oParentObject.w_ATDATINI)
    this.w_MVFLOMAG = "X"
    this.w_MVFLTRAS = IIF(this.w_ARUTISER="S","S"," ")
    this.w_MVTIPPR2 = IIF(g_CALPRO="DI","DC",IIF(g_CALPRO="GD","ST","CT"))
    * --- Commessa (Pratica)
    this.w_MVCODCOM = this.oParentObject.w_ATCODPRA
    * --- Responsabile
    this.w_MVCODRES = w_DACODRES
    * --- Dati evasione
    this.w_MVIMPEVA = IIF(this.oParentObject.w_DAFLEVAS=" " OR this.w_MVTIPRIG<>"F",0,this.w_MVPREZZO)
    this.w_MVQTAEVA = IIF(this.oParentObject.w_DAFLEVAS=" ",0,this.w_MVQTAMOV)
    this.w_MVQTAEV1 = IIF(this.oParentObject.w_DAFLEVAS=" ",0,this.w_MVQTAUM1)
    * --- Spese e anticipazioni collegate
    this.w_MVRIGPRE = this.oParentObject.w_DARIGPRE
    this.w_MVRIFPRE = this.oParentObject.w_DARIFPRE
    this.w_GSIM_BAC = this.oparentobject.oparentobject.oparentobject
    this.w_GSAL_BIA = this.oparentobject.oparentobject
    if NOT(this.oParentObject.w_DATIPACC="S")
      * --- Try
      local bErr_0356F468
      bErr_0356F468=bTrsErr
      this.Try_0356F468()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0356F468
      * --- End
      * --- Try
      local bErr_0363BB40
      bErr_0363BB40=bTrsErr
      this.Try_0363BB40()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0363BB40
      * --- End
      * --- Try
      local bErr_036327E0
      bErr_036327E0=bTrsErr
      this.Try_036327E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_GSIM_BAC.w_ResoDett = AH_MSGFORMAT("Errore scrittura riga prestazione%0%1" , iif(Empty(i_TRSMSG),Message(),i_TRSMSG) )
        this.w_GSIM_BAC.w_Corretto = .f.
        this.w_GSIM_BAC.w_ResoMode = "SCARTO"
        this.w_GSIM_BAC.w_ResoTipo = "E"
      endif
      bTrsErr=bTrsErr or bErr_036327E0
      * --- End
    endif
    if this.oParentObject.w_DAFLEVAS="S" OR this.oParentObject.w_DAFLEVAS="P" OR this.oParentObject.w_DAFLEVAS="A" OR this.oParentObject.w_DAFLEVAS="X"
      do case
        case this.oParentObject.w_DAFLEVAS="S"
          * --- In fattura fittizia
          do case
            case this.oParentObject.w_DASERDOC=1
              this.w_MVSERIAL = "AAAAAAAAAA"
            case this.oParentObject.w_DASERDOC=2
              this.w_MVSERIAL = "AAAAAAAAA2"
            case this.oParentObject.w_DASERDOC=3
              this.w_MVSERIAL = "AAAAAAAAA3"
            case this.oParentObject.w_DASERDOC=4
              this.w_MVSERIAL = "AAAAAAAAA4"
            case this.oParentObject.w_DASERDOC=5
              this.w_MVSERIAL = "AAAAAAAAA5"
            otherwise
              * --- w_DASERDOC=99, stiamo passando anche numero, alfa e data fattura.
              *     Deve leggere MVSERIAL da DOC_MAST e il max di cprownum da DOC_DETT
              * --- Read from DOC_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MVSERIAL"+;
                  " from "+i_cTable+" DOC_MAST where ";
                      +"MVNUMDOC = "+cp_ToStrODBC(this.oParentObject.w_DANUMFATTURA);
                      +" and MVALFDOC = "+cp_ToStrODBC(this.oParentObject.w_DAALFFATTURA);
                      +" and MVDATDOC = "+cp_ToStrODBC(this.oParentObject.w_DADATFATTURA);
                      +" and MVCLADOC = "+cp_ToStrODBC("FA");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MVSERIAL;
                  from (i_cTable) where;
                      MVNUMDOC = this.oParentObject.w_DANUMFATTURA;
                      and MVALFDOC = this.oParentObject.w_DAALFFATTURA;
                      and MVDATDOC = this.oParentObject.w_DADATFATTURA;
                      and MVCLADOC = "FA";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_MVSERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Adesso leggiamo il MAX del CPROWNUM
              * --- Select from DOC_DETT
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM  from "+i_cTable+" DOC_DETT ";
                    +" where MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)+"";
                     ,"_Curs_DOC_DETT")
              else
                select MAX(CPROWNUM) AS CPROWNUM from (i_cTable);
                 where MVSERIAL=this.w_MVSERIAL;
                  into cursor _Curs_DOC_DETT
              endif
              if used('_Curs_DOC_DETT')
                select _Curs_DOC_DETT
                locate for 1=1
                do while not(eof())
                this.oParentObject.w_DAROWRIF = NVL(CPROWNUM,0)+1
                  select _Curs_DOC_DETT
                  continue
                enddo
                use
              endif
          endcase
          this.w_GSAL_BIA.w_RIFFAT = this.w_MVSERIAL
          this.w_GSAL_BIA.w_ROWFAT = this.oParentObject.w_DAROWRIF
        case this.oParentObject.w_DAFLEVAS="P"
          * --- In profroma fittizia
          do case
            case this.oParentObject.w_DASERDOC=1
              this.w_MVSERIAL = "PPPPPPPPPP"
            case this.oParentObject.w_DASERDOC=2
              this.w_MVSERIAL = "PPPPPPPPP2"
            case this.oParentObject.w_DASERDOC=3
              this.w_MVSERIAL = "PPPPPPPPP3"
            case this.oParentObject.w_DASERDOC=4
              this.w_MVSERIAL = "PPPPPPPPP4"
            case this.oParentObject.w_DASERDOC=5
              this.w_MVSERIAL = "PPPPPPPPP5"
            case this.oParentObject.w_DASERDOC=6
              this.w_MVSERIAL = "PPPPPPPPP6"
            case this.oParentObject.w_DASERDOC=7
              this.w_MVSERIAL = "PPPPPPPPP7"
            case this.oParentObject.w_DASERDOC=8
              this.w_MVSERIAL = "PPPPPPPPP8"
            case this.oParentObject.w_DASERDOC=9
              this.w_MVSERIAL = "PPPPPPPPP9"
            case this.oParentObject.w_DASERDOC=10
              this.w_MVSERIAL = "PPPPPPPP10"
            case this.oParentObject.w_DASERDOC=11
              this.w_MVSERIAL = "PPPPPPPP11"
            case this.oParentObject.w_DASERDOC=12
              this.w_MVSERIAL = "PPPPPPPP12"
            case this.oParentObject.w_DASERDOC=13
              this.w_MVSERIAL = "PPPPPPPP13"
            case this.oParentObject.w_DASERDOC=14
              this.w_MVSERIAL = "PPPPPPPP14"
            case this.oParentObject.w_DASERDOC=15
              this.w_MVSERIAL = "PPPPPPPP15"
            case this.oParentObject.w_DASERDOC=16
              this.w_MVSERIAL = "PPPPPPPP16"
            case this.oParentObject.w_DASERDOC=17
              this.w_MVSERIAL = "PPPPPPPP17"
            case this.oParentObject.w_DASERDOC=18
              this.w_MVSERIAL = "PPPPPPPP18"
            case this.oParentObject.w_DASERDOC=19
              this.w_MVSERIAL = "PPPPPPPP19"
            case this.oParentObject.w_DASERDOC=20
              this.w_MVSERIAL = "PPPPPPPP20"
            case this.oParentObject.w_DASERDOC=21
              this.w_MVSERIAL = "PPPPPPPP21"
            case this.oParentObject.w_DASERDOC=22
              this.w_MVSERIAL = "PPPPPPPP22"
            case this.oParentObject.w_DASERDOC=23
              this.w_MVSERIAL = "PPPPPPPP23"
            otherwise
              * --- w_DASERDOC=99, stiamo passando anche numero, alfa e data fattura.
              *     Deve leggere MVSERIAL da DOC_MAST e il max di cprownum da DOC_DETT
              * --- Read from DOC_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MVSERIAL"+;
                  " from "+i_cTable+" DOC_MAST where ";
                      +"MVNUMDOC = "+cp_ToStrODBC(this.oParentObject.w_DANUMFATTURA);
                      +" and MVALFDOC = "+cp_ToStrODBC(this.oParentObject.w_DAALFFATTURA);
                      +" and MVDATDOC = "+cp_ToStrODBC(this.oParentObject.w_DADATFATTURA);
                      +" and MVCLADOC = "+cp_ToStrODBC("DI");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MVSERIAL;
                  from (i_cTable) where;
                      MVNUMDOC = this.oParentObject.w_DANUMFATTURA;
                      and MVALFDOC = this.oParentObject.w_DAALFFATTURA;
                      and MVDATDOC = this.oParentObject.w_DADATFATTURA;
                      and MVCLADOC = "DI";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_MVSERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Adesso leggiamo il MAX del CPROWNUM
              * --- Select from DOC_DETT
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM  from "+i_cTable+" DOC_DETT ";
                    +" where MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)+"";
                     ,"_Curs_DOC_DETT")
              else
                select MAX(CPROWNUM) AS CPROWNUM from (i_cTable);
                 where MVSERIAL=this.w_MVSERIAL;
                  into cursor _Curs_DOC_DETT
              endif
              if used('_Curs_DOC_DETT')
                select _Curs_DOC_DETT
                locate for 1=1
                do while not(eof())
                this.oParentObject.w_DAROWRIF = NVL(CPROWNUM,0)+1
                  select _Curs_DOC_DETT
                  continue
                enddo
                use
              endif
          endcase
          this.w_GSAL_BIA.w_RIFPRO = this.w_MVSERIAL
          this.w_GSAL_BIA.w_ROWPRO = this.oParentObject.w_DAROWRIF
        case this.oParentObject.w_DAFLEVAS="A"
          * --- In fattura d'acconto fittizia
          this.w_MVSERIAL = "AAAAAAAAAX"
          this.w_GSAL_BIA.w_RIFFAA = this.w_MVSERIAL
          this.w_GSAL_BIA.w_ROWFAA = this.oParentObject.w_DAROWRIF
        case this.oParentObject.w_DAFLEVAS="X"
          * --- In profroma d'acconto fittizia
          this.w_MVSERIAL = "PPPPPPPPPX"
          this.w_GSAL_BIA.w_RIFPRA = this.w_MVSERIAL
          this.w_GSAL_BIA.w_ROWPRA = this.oParentObject.w_DAROWRIF
      endcase
      * --- Try
      local bErr_03A2FD18
      bErr_03A2FD18=bTrsErr
      this.Try_03A2FD18()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03A2FD18
      * --- End
      * --- Try
      local bErr_03A32C58
      bErr_03A32C58=bTrsErr
      this.Try_03A32C58()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03A32C58
      * --- End
      * --- Dati evasione
      this.w_MVFLERIF = "S"
      this.w_MVFLARIF = "+"
      this.w_MVQTAIMP = this.w_MVQTAMOV
      this.w_MVQTAIM1 = this.w_MVQTAUM1
      * --- Riga accorpata di tipo diritto/onorario
      this.w_RIGPRELE = "S"
      if this.w_MVRIGPRE="N" AND this.w_MVRIFPRE>0
        * --- Lettura dallo stesso documento (filtro su MVSERIAL) delle prestazioni aventi la stessa descrizione (filtro mvdesart) e che siano accorpate (filtro MVRIGPRE)
        * --- se pi� righe soddisfano la condizione allora viene preso il CPROWNUM pi� grande (corretto l'ordinamento presente nella sorgente dati)
        * --- Select from DOC_DETT
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CPROWNUM  from "+i_cTable+" DOC_DETT ";
              +" where MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)+" AND MVRIGPRE="+cp_ToStrODBC(this.w_RIGPRELE)+"";
              +" order by CPROWNUM";
               ,"_Curs_DOC_DETT")
        else
          select CPROWNUM from (i_cTable);
           where MVSERIAL=this.w_MVSERIAL AND MVRIGPRE=this.w_RIGPRELE;
           order by CPROWNUM;
            into cursor _Curs_DOC_DETT
        endif
        if used('_Curs_DOC_DETT')
          select _Curs_DOC_DETT
          locate for 1=1
          do while not(eof())
          this.w_MVRIFPRE = _Curs_DOC_DETT.CPROWNUM
            select _Curs_DOC_DETT
            continue
          enddo
          use
        endif
      endif
      * --- Try
      local bErr_03AD8918
      bErr_03AD8918=bTrsErr
      this.Try_03AD8918()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- ResoTipo 
        this.w_GSIM_BAC.w_ResoDett = AH_MSGFORMAT("Errore scrittura proforma/fattura%0%1" , i_TRSMSG )
        this.w_GSIM_BAC.w_Corretto = .f.
        this.w_GSIM_BAC.w_ResoTipo = "E"
        this.w_GSIM_BAC.w_ResoMode = "SCARTO"
      endif
      bTrsErr=bTrsErr or bErr_03AD8918
      * --- End
    endif
    this.w_GSIM_BAC = .NULL.
  endproc
  proc Try_0356F468()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DocInt),'DOC_DETT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DOC_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.oParentObject.w_DocInt,'CPROWNUM',this.w_CPROWNUM,'MVNUMRIF',this.w_MVNUMRIF)
      insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF &i_ccchkf. );
         values (;
           this.oParentObject.w_DocInt;
           ,this.w_CPROWNUM;
           ,this.w_MVNUMRIF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0363BB40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ALT_DETT
    i_nConn=i_TableProp[this.ALT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ALT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DESERIAL"+",DEROWNUM"+",DENUMRIF"+",DEOREEFF"+",DEMINEFF"+",DECOSINT"+",DEPREMIN"+",DEPREMAX"+",DEGAZUFF"+",DETIPPRA"+",DEMATPRA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DocInt),'ALT_DETT','DESERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'ALT_DETT','DEROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'ALT_DETT','DENUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTOREEFF),'ALT_DETT','DEOREEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTMINEFF),'ALT_DETT','DEMINEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTCOSINT),'ALT_DETT','DECOSINT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTPREMIN),'ALT_DETT','DEPREMIN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTPREMAX),'ALT_DETT','DEPREMAX');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTGAZUFF),'ALT_DETT','DEGAZUFF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTTIPPRA),'ALT_DETT','DETIPPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTMATPRA),'ALT_DETT','DEMATPRA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.oParentObject.w_DocInt,'DEROWNUM',this.w_CPROWNUM,'DENUMRIF',this.w_MVNUMRIF,'DEOREEFF',this.oParentObject.w_PTOREEFF,'DEMINEFF',this.oParentObject.w_PTMINEFF,'DECOSINT',this.oParentObject.w_PTCOSINT,'DEPREMIN',this.oParentObject.w_PTPREMIN,'DEPREMAX',this.oParentObject.w_PTPREMAX,'DEGAZUFF',this.oParentObject.w_PTGAZUFF,'DETIPPRA',this.oParentObject.w_PTTIPPRA,'DEMATPRA',this.oParentObject.w_PTMATPRA)
      insert into (i_cTable) (DESERIAL,DEROWNUM,DENUMRIF,DEOREEFF,DEMINEFF,DECOSINT,DEPREMIN,DEPREMAX,DEGAZUFF,DETIPPRA,DEMATPRA &i_ccchkf. );
         values (;
           this.oParentObject.w_DocInt;
           ,this.w_CPROWNUM;
           ,this.w_MVNUMRIF;
           ,this.oParentObject.w_PTOREEFF;
           ,this.oParentObject.w_PTMINEFF;
           ,this.oParentObject.w_PTCOSINT;
           ,this.oParentObject.w_PTPREMIN;
           ,this.oParentObject.w_PTPREMAX;
           ,this.oParentObject.w_PTGAZUFF;
           ,this.oParentObject.w_PTTIPPRA;
           ,this.oParentObject.w_PTMATPRA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_036327E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_MVDATOAI = TTOD(this.oParentObject.w_ATDATINI)
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'DOC_DETT','CPROWORD');
      +",MVCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
      +",MVTIPRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
      +",MVCODART ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
      +",MVCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
      +",MVDESART ="+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
      +",MVUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
      +",MVDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
      +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
      +",MVPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
      +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
      +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
      +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
      +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
      +",MVFLOMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
      +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
      +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
      +",MVCODRES ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODRES),'DOC_DETT','MVCODRES');
      +",MVDATOAI ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATOAI),'DOC_DETT','MVDATOAI');
      +",MVCODCLA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
      +",MVCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
      +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(IIF(this.oParentObject.w_DAFLEVAS=" "," ","S")),'DOC_DETT','MVFLEVAS');
      +",MVFLTRAS ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
      +",MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
      +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELAN),'DOC_DETT','MVFLELAN');
      +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
      +",MVCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
      +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'DOC_DETT','MVTIPATT');
      +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
      +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPEVA),'DOC_DETT','MVIMPEVA');
      +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAEVA),'DOC_DETT','MVQTAEVA');
      +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAEV1),'DOC_DETT','MVQTAEV1');
      +",MVRIGPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIGPRE),'DOC_DETT','MVRIGPRE');
      +",MVRIFPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIFPRE),'DOC_DETT','MVRIFPRE');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DocInt);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
          +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
             )
    else
      update (i_cTable) set;
          CPROWORD = this.w_CPROWORD;
          ,MVCODICE = this.w_MVCODICE;
          ,MVTIPRIG = this.w_MVTIPRIG;
          ,MVCODART = this.w_MVCODART;
          ,MVCODIVA = this.w_MVCODIVA;
          ,MVDESART = this.w_MVDESART;
          ,MVUNIMIS = this.w_MVUNIMIS;
          ,MVDESSUP = this.w_MVDESSUP;
          ,MVCAUMAG = this.w_MVCAUMAG;
          ,MVQTAMOV = this.w_MVQTAMOV;
          ,MVPREZZO = this.w_MVPREZZO;
          ,MVVALRIG = this.w_MVVALRIG;
          ,MVQTAUM1 = this.w_MVQTAUM1;
          ,MVVALMAG = this.w_MVVALMAG;
          ,MVIMPNAZ = this.w_MVIMPNAZ;
          ,MVFLOMAG = this.w_MVFLOMAG;
          ,MV_SEGNO = this.w_MV_SEGNO;
          ,MVCODCOM = this.w_MVCODCOM;
          ,MVCODRES = this.w_MVCODRES;
          ,MVDATOAI = this.w_MVDATOAI;
          ,MVCODCLA = this.w_MVCODCLA;
          ,MVCATCON = this.w_MVCATCON;
          ,MVFLEVAS = IIF(this.oParentObject.w_DAFLEVAS=" "," ","S");
          ,MVFLTRAS = this.w_MVFLTRAS;
          ,MVTIPPR2 = this.w_MVTIPPR2;
          ,MVFLELAN = this.w_MVFLELAN;
          ,MVVOCCEN = this.w_MVVOCCEN;
          ,MVCODCEN = this.w_MVCODCEN;
          ,MVTIPATT = this.w_MVTIPATT;
          ,MVCODATT = this.w_MVCODATT;
          ,MVIMPEVA = this.w_MVIMPEVA;
          ,MVQTAEVA = this.w_MVQTAEVA;
          ,MVQTAEV1 = this.w_MVQTAEV1;
          ,MVRIGPRE = this.w_MVRIGPRE;
          ,MVRIFPRE = this.w_MVRIFPRE;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_DocInt;
          and CPROWNUM = this.w_CPROWNUM;
          and MVNUMRIF = this.w_MVNUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03A2FD18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",CPROWNUM"+",MVNUMRIF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_DETT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DAROWRIF),'DOC_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_MVSERIAL,'CPROWNUM',this.oParentObject.w_DAROWRIF,'MVNUMRIF',this.w_MVNUMRIF)
      insert into (i_cTable) (MVSERIAL,CPROWNUM,MVNUMRIF &i_ccchkf. );
         values (;
           this.w_MVSERIAL;
           ,this.oParentObject.w_DAROWRIF;
           ,this.w_MVNUMRIF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento proforma/fattura'
      return
    endif
    return
  proc Try_03A32C58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ALT_DETT
    i_nConn=i_TableProp[this.ALT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ALT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DESERIAL"+",DEROWNUM"+",DENUMRIF"+",DEOREEFF"+",DEMINEFF"+",DECOSINT"+",DEPREMIN"+",DEPREMAX"+",DEGAZUFF"+",DETIPPRA"+",DEMATPRA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'ALT_DETT','DESERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DAROWRIF),'ALT_DETT','DEROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'ALT_DETT','DENUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTOREEFF),'ALT_DETT','DEOREEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTMINEFF),'ALT_DETT','DEMINEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTCOSINT),'ALT_DETT','DECOSINT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTPREMIN),'ALT_DETT','DEPREMIN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTPREMAX),'ALT_DETT','DEPREMAX');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTGAZUFF),'ALT_DETT','DEGAZUFF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTTIPPRA),'ALT_DETT','DETIPPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PTMATPRA),'ALT_DETT','DEMATPRA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_MVSERIAL,'DEROWNUM',this.oParentObject.w_DAROWRIF,'DENUMRIF',this.w_MVNUMRIF,'DEOREEFF',this.oParentObject.w_PTOREEFF,'DEMINEFF',this.oParentObject.w_PTMINEFF,'DECOSINT',this.oParentObject.w_PTCOSINT,'DEPREMIN',this.oParentObject.w_PTPREMIN,'DEPREMAX',this.oParentObject.w_PTPREMAX,'DEGAZUFF',this.oParentObject.w_PTGAZUFF,'DETIPPRA',this.oParentObject.w_PTTIPPRA,'DEMATPRA',this.oParentObject.w_PTMATPRA)
      insert into (i_cTable) (DESERIAL,DEROWNUM,DENUMRIF,DEOREEFF,DEMINEFF,DECOSINT,DEPREMIN,DEPREMAX,DEGAZUFF,DETIPPRA,DEMATPRA &i_ccchkf. );
         values (;
           this.w_MVSERIAL;
           ,this.oParentObject.w_DAROWRIF;
           ,this.w_MVNUMRIF;
           ,this.oParentObject.w_PTOREEFF;
           ,this.oParentObject.w_PTMINEFF;
           ,this.oParentObject.w_PTCOSINT;
           ,this.oParentObject.w_PTPREMIN;
           ,this.oParentObject.w_PTPREMAX;
           ,this.oParentObject.w_PTGAZUFF;
           ,this.oParentObject.w_PTTIPPRA;
           ,this.oParentObject.w_PTMATPRA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03AD8918()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DAROWRIF*10),'DOC_DETT','CPROWORD');
      +",MVCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
      +",MVTIPRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
      +",MVCODART ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
      +",MVCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
      +",MVDESART ="+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
      +",MVUNIMIS ="+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
      +",MVDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
      +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +",MVQTAMOV ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
      +",MVPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
      +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
      +",MVQTAUM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
      +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
      +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
      +",MVFLOMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
      +",MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
      +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
      +",MVCODRES ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODRES),'DOC_DETT','MVCODRES');
      +",MVDATOAI ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATOAI),'DOC_DETT','MVDATOAI');
      +",MVCODCLA ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
      +",MVCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
      +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
      +",MVSERRIF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DocInt),'DOC_DETT','MVSERRIF');
      +",MVROWRIF ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DOC_DETT','MVROWRIF');
      +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLERIF),'DOC_DETT','MVFLERIF');
      +",MVFLARIF ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLARIF),'DOC_DETT','MVFLARIF');
      +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAIMP),'DOC_DETT','MVQTAIMP');
      +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTAIM1),'DOC_DETT','MVQTAIM1');
      +",MVSCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
      +",MVSCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
      +",MVSCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
      +",MVSCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
      +",MVRIGPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIGPRE),'DOC_DETT','MVRIGPRE');
      +",MVRIFPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVRIFPRE),'DOC_DETT','MVRIFPRE');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_DAROWRIF);
          +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
             )
    else
      update (i_cTable) set;
          CPROWORD = this.oParentObject.w_DAROWRIF*10;
          ,MVCODICE = this.w_MVCODICE;
          ,MVTIPRIG = this.w_MVTIPRIG;
          ,MVCODART = this.w_MVCODART;
          ,MVCODIVA = this.w_MVCODIVA;
          ,MVDESART = this.w_MVDESART;
          ,MVUNIMIS = this.w_MVUNIMIS;
          ,MVDESSUP = this.w_MVDESSUP;
          ,MVCAUMAG = this.w_MVCAUMAG;
          ,MVQTAMOV = this.w_MVQTAMOV;
          ,MVPREZZO = this.w_MVPREZZO;
          ,MVVALRIG = this.w_MVVALRIG;
          ,MVQTAUM1 = this.w_MVQTAUM1;
          ,MVVALMAG = this.w_MVVALMAG;
          ,MVIMPNAZ = this.w_MVIMPNAZ;
          ,MVFLOMAG = this.w_MVFLOMAG;
          ,MV_SEGNO = this.w_MV_SEGNO;
          ,MVCODCOM = this.w_MVCODCOM;
          ,MVCODRES = this.w_MVCODRES;
          ,MVDATOAI = this.w_MVDATOAI;
          ,MVCODCLA = this.w_MVCODCLA;
          ,MVCATCON = this.w_MVCATCON;
          ,MVFLEVAS = " ";
          ,MVSERRIF = this.oParentObject.w_DocInt;
          ,MVROWRIF = this.w_CPROWNUM;
          ,MVFLERIF = this.w_MVFLERIF;
          ,MVFLARIF = this.w_MVFLARIF;
          ,MVQTAIMP = this.w_MVQTAIMP;
          ,MVQTAIM1 = this.w_MVQTAIM1;
          ,MVSCONT1 = this.w_MVSCONT1;
          ,MVSCONT2 = this.w_MVSCONT2;
          ,MVSCONT3 = this.w_MVSCONT3;
          ,MVSCONT4 = this.w_MVSCONT4;
          ,MVRIGPRE = this.w_MVRIGPRE;
          ,MVRIFPRE = this.w_MVRIFPRE;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;
          and CPROWNUM = this.oParentObject.w_DAROWRIF;
          and MVNUMRIF = this.w_MVNUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore scrittura proforma/fattura'
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione Variabili
    * --- Gestite nella routine GSIM_BMM in maniera diversa tra AHE e AHR
    * --- Non gestite nella routine GSIM_BMM
    * --- Variabile per scrittura sul dettaglio attivit� della chiave di riga del documento interno
    * --- Var. per ALT_DETT
  endproc


  proc Init(oParentObject,pChiamante)
    this.pChiamante=pChiamante
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,15)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='KEY_ARTI'
    this.cWorkTables[6]='ART_ICOL'
    this.cWorkTables[7]='DOC_DETT'
    this.cWorkTables[8]='OFF_ATTI'
    this.cWorkTables[9]='CON_PAGA'
    this.cWorkTables[10]='DOC_RATE'
    this.cWorkTables[11]='OFFDATTI'
    this.cWorkTables[12]='ALT_DETT'
    this.cWorkTables[13]='CAN_TIER'
    this.cWorkTables[14]='AZIENDA'
    this.cWorkTables[15]='CONTI'
    return(this.OpenAllTables(15))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pChiamante"
endproc
