* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kuc                                                        *
*              Avvalora costo e durata effettiva in prestazioni                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-07-31                                                      *
* Last revis.: 2012-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kuc",oParentObject))

* --- Class definition
define class tgsag_kuc as StdForm
  Top    = 23
  Left   = 60

  * --- Standard Properties
  Width  = 606
  Height = 402
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-17"
  HelpContextID=65677161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  cPrg = "gsag_kuc"
  cComment = "Avvalora costo e durata effettiva in prestazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TipoRisorsa = space(1)
  w_CODPERS = space(5)
  o_CODPERS = space(5)
  w_COGNOME = space(40)
  o_COGNOME = space(40)
  w_NOME = space(40)
  o_NOME = space(40)
  w_COST_ORA = 0
  w_DESCRIZ = space(80)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_OB_TEST = ctod('  /  /  ')
  w_Dur_Ore = 0
  w_Dur_Min = 0
  w_FLDurata = space(1)
  o_FLDurata = space(1)
  w_FL_Costo = space(1)
  w_MinDurata = 0
  w_MaxCosto = 0
  w_DATA_INI = ctot('')
  w_DATA_FIN = ctot('')
  w_FlEsclSp = space(1)
  o_FlEsclSp = space(1)
  w_EsclSpe = space(1)
  w_EsclAnt = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kucPag1","gsag_kuc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODPERS_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DIPENDEN'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TipoRisorsa=space(1)
      .w_CODPERS=space(5)
      .w_COGNOME=space(40)
      .w_NOME=space(40)
      .w_COST_ORA=0
      .w_DESCRIZ=space(80)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_OB_TEST=ctod("  /  /  ")
      .w_Dur_Ore=0
      .w_Dur_Min=0
      .w_FLDurata=space(1)
      .w_FL_Costo=space(1)
      .w_MinDurata=0
      .w_MaxCosto=0
      .w_DATA_INI=ctot("")
      .w_DATA_FIN=ctot("")
      .w_FlEsclSp=space(1)
      .w_EsclSpe=space(1)
      .w_EsclAnt=space(1)
        .w_TipoRisorsa = 'P'
        .w_CODPERS = ReadDipend(i_codute, "C")
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODPERS))
          .link_1_3('Full')
        endif
          .DoRTCalc(3,5,.f.)
        .w_DESCRIZ = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .w_DATINI = i_datsys
        .w_DATFIN = i_datsys
        .w_OB_TEST = i_INIDAT
          .DoRTCalc(10,11,.f.)
        .w_FLDurata = 'N'
        .w_FL_Costo = 'N'
        .w_MinDurata = IIF(.w_FLDurata='S',0,1)
        .w_MaxCosto = IIF(.w_FL_Costo='S',0,1)
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' 00:00:00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' 23:59:00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        .w_FlEsclSp = 'N'
        .w_EsclSpe = IIF(.w_FlEsclSp='S','S','')
        .w_EsclAnt = IIF(.w_FlEsclSp='S','A','')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_CODPERS<>.w_CODPERS.or. .o_COGNOME<>.w_COGNOME.or. .o_NOME<>.w_NOME
            .w_DESCRIZ = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        endif
        .DoRTCalc(7,13,.t.)
        if .o_FLDurata<>.w_FLDurata
            .w_MinDurata = IIF(.w_FLDurata='S',0,1)
        endif
        if .o_FLDurata<>.w_FLDurata
            .w_MaxCosto = IIF(.w_FL_Costo='S',0,1)
        endif
        if .o_DATINI<>.w_DATINI
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' 00:00:00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN<>.w_DATFIN
            .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' 23:59:00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        .DoRTCalc(18,18,.t.)
        if .o_FlEsclSp<>.w_FlEsclSp
            .w_EsclSpe = IIF(.w_FlEsclSp='S','S','')
        endif
        if .o_FlEsclSp<>.w_FlEsclSp
            .w_EsclAnt = IIF(.w_FlEsclSp='S','A','')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDur_Ore_1_19.enabled = this.oPgFrm.Page1.oPag.oDur_Ore_1_19.mCond()
    this.oPgFrm.Page1.oPag.oDur_Min_1_21.enabled = this.oPgFrm.Page1.oPag.oDur_Min_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDur_Ore_1_19.visible=!this.oPgFrm.Page1.oPag.oDur_Ore_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oDur_Min_1_21.visible=!this.oPgFrm.Page1.oPag.oDur_Min_1_21.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODPERS
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPERS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_CODPERS))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPERS)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPERS) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCODPERS_1_3'),i_cWhere,'GSAR_BDZ',"Persone",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPERS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPERS);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_CODPERS)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCOSORA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPERS = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOME = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME = NVL(_Link_.DPNOME,space(40))
      this.w_COST_ORA = NVL(_Link_.DPCOSORA,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODPERS = space(5)
      endif
      this.w_COGNOME = space(40)
      this.w_NOME = space(40)
      this.w_COST_ORA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPERS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODPERS_1_3.value==this.w_CODPERS)
      this.oPgFrm.Page1.oPag.oCODPERS_1_3.value=this.w_CODPERS
    endif
    if not(this.oPgFrm.Page1.oPag.oCOST_ORA_1_6.value==this.w_COST_ORA)
      this.oPgFrm.Page1.oPag.oCOST_ORA_1_6.value=this.w_COST_ORA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIZ_1_7.value==this.w_DESCRIZ)
      this.oPgFrm.Page1.oPag.oDESCRIZ_1_7.value=this.w_DESCRIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_10.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_10.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDur_Ore_1_19.value==this.w_Dur_Ore)
      this.oPgFrm.Page1.oPag.oDur_Ore_1_19.value=this.w_Dur_Ore
    endif
    if not(this.oPgFrm.Page1.oPag.oDur_Min_1_21.value==this.w_Dur_Min)
      this.oPgFrm.Page1.oPag.oDur_Min_1_21.value=this.w_Dur_Min
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDurata_1_22.RadioValue()==this.w_FLDurata)
      this.oPgFrm.Page1.oPag.oFLDurata_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFL_Costo_1_23.RadioValue()==this.w_FL_Costo)
      this.oPgFrm.Page1.oPag.oFL_Costo_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFlEsclSp_1_30.RadioValue()==this.w_FlEsclSp)
      this.oPgFrm.Page1.oPag.oFlEsclSp_1_30.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(.w_Dur_Min>=0 AND .w_Dur_Min<60)  and not(.w_FLDurata<>'S')  and (.w_FLDurata='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDur_Min_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 0 e 59")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODPERS = this.w_CODPERS
    this.o_COGNOME = this.w_COGNOME
    this.o_NOME = this.w_NOME
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    this.o_FLDurata = this.w_FLDurata
    this.o_FlEsclSp = this.w_FlEsclSp
    return

enddefine

* --- Define pages as container
define class tgsag_kucPag1 as StdContainer
  Width  = 602
  height = 402
  stdWidth  = 602
  stdheight = 402
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODPERS_1_3 as StdField with uid="UDZFBXMUCT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODPERS", cQueryName = "CODPERS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona",;
    HelpContextID = 45771814,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=101, Top=149, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_CODPERS"

  func oCODPERS_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPERS_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPERS_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCODPERS_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oCODPERS_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TipoRisorsa
     i_obj.w_DPCODICE=this.parent.oContained.w_CODPERS
     i_obj.ecpSave()
  endproc

  add object oCOST_ORA_1_6 as StdField with uid="HEULTETHSM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_COST_ORA", cQueryName = "COST_ORA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo orario da riportare nelle prestazioni",;
    HelpContextID = 23026791,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=101, Top=181, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oDESCRIZ_1_7 as StdField with uid="KMZNHYKWWA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCRIZ", cQueryName = "DESCRIZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 92384714,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=167, Top=149, InputMask=replicate('X',80)

  add object oDATINI_1_10 as StdField with uid="VAJTUNAFFS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 96182730,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=101, Top=213

  func oDATINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_11 as StdField with uid="VRHGJGBHQK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 17736138,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=212, Top=211

  func oDATFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDur_Ore_1_19 as StdField with uid="HMNUCWLUYH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_Dur_Ore", cQueryName = "Dur_Ore",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Durata effettiva in ore",;
    HelpContextID = 57438774,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=463, Top=246, cSayPict='"999"', cGetPict='"999"'

  func oDur_Ore_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDurata='S')
    endwith
   endif
  endfunc

  func oDur_Ore_1_19.mHide()
    with this.Parent.oContained
      return (.w_FLDurata<>'S')
    endwith
  endfunc

  add object oDur_Min_1_21 as StdField with uid="SBKPSNOZLO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_Dur_Min", cQueryName = "Dur_Min",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 0 e 59",;
    ToolTipText = "Durata effettiva in minuti",;
    HelpContextID = 95653322,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=507, Top=246, cSayPict='"99"', cGetPict='"99"'

  func oDur_Min_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDurata='S')
    endwith
   endif
  endfunc

  func oDur_Min_1_21.mHide()
    with this.Parent.oContained
      return (.w_FLDurata<>'S')
    endwith
  endfunc

  func oDur_Min_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Dur_Min>=0 AND .w_Dur_Min<60)
    endwith
    return bRes
  endfunc

  add object oFLDurata_1_22 as StdCheck with uid="YPZNASSILE",rtseq=12,rtrep=.f.,left=101, top=245, caption="Avvalora la durata effettiva nelle prestazioni con durata vuota",;
    ToolTipText = "Se attivo, avvalora la durata effettiva nelle prestazioni con durata vuota",;
    HelpContextID = 78604727,;
    cFormVar="w_FLDurata", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDurata_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLDurata_1_22.GetRadio()
    this.Parent.oContained.w_FLDurata = this.RadioValue()
    return .t.
  endfunc

  func oFLDurata_1_22.SetRadio()
    this.Parent.oContained.w_FLDurata=trim(this.Parent.oContained.w_FLDurata)
    this.value = ;
      iif(this.Parent.oContained.w_FLDurata=='S',1,;
      0)
  endfunc

  add object oFL_Costo_1_23 as StdCheck with uid="BTYZEUJBIS",rtseq=13,rtrep=.f.,left=101, top=278, caption="Aggiorna anche le prestazioni con costo gi� avvalorato",;
    ToolTipText = "Se attivo, saranno aggiornate anche le prestazioni con costo avvalorato",;
    HelpContextID = 105847237,;
    cFormVar="w_FL_Costo", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFL_Costo_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFL_Costo_1_23.GetRadio()
    this.Parent.oContained.w_FL_Costo = this.RadioValue()
    return .t.
  endfunc

  func oFL_Costo_1_23.SetRadio()
    this.Parent.oContained.w_FL_Costo=trim(this.Parent.oContained.w_FL_Costo)
    this.value = ;
      iif(this.Parent.oContained.w_FL_Costo=='S',1,;
      0)
  endfunc


  add object oBtn_1_24 as StdButton with uid="MZNLQBMNEX",left=489, top=347, width=48,height=45,;
    CpPicture="bmp\Genera.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per avvalorare il costo delle prestazioni";
    , HelpContextID = 37663591;
    , Caption='\<Avvalora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        do GSAG_BUC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_25 as StdButton with uid="TKCMXZXILU",left=542, top=346, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 58359738;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFlEsclSp_1_30 as StdCheck with uid="ABEOTSUPGW",rtseq=18,rtrep=.f.,left=100, top=311, caption="Escludi spese e anticipazioni ",;
    ToolTipText = "Se attivo, esclude dall'elaborazione le voci di prestazioni relative a spese ed anticipazioni",;
    HelpContextID = 247306694,;
    cFormVar="w_FlEsclSp", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFlEsclSp_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFlEsclSp_1_30.GetRadio()
    this.Parent.oContained.w_FlEsclSp = this.RadioValue()
    return .t.
  endfunc

  func oFlEsclSp_1_30.SetRadio()
    this.Parent.oContained.w_FlEsclSp=trim(this.Parent.oContained.w_FlEsclSp)
    this.value = ;
      iif(this.Parent.oContained.w_FlEsclSp=='S',1,;
      0)
  endfunc

  add object oStr_1_2 as StdString with uid="DCOEKCFVVD",Visible=.t., Left=5, Top=149,;
    Alignment=1, Width=90, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="MTWJWPCNSZ",Visible=.t., Left=5, Top=213,;
    Alignment=1, Width=90, Height=18,;
    Caption="Prestazioni da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XIZCGGXJOW",Visible=.t., Left=185, Top=212,;
    Alignment=1, Width=23, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="CVPVSHVRSK",Visible=.t., Left=12, Top=11,;
    Alignment=0, Width=453, Height=18,;
    Caption="Questa procedura avvalora il costo delle prestazioni che hanno come responsabile"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="AAZICEFYQT",Visible=.t., Left=12, Top=37,;
    Alignment=0, Width=453, Height=18,;
    Caption="quello specificato in maschera utilizzando come costo orario l'importo digitato."  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="NRQGDFCVDB",Visible=.t., Left=12, Top=63,;
    Alignment=0, Width=453, Height=18,;
    Caption="Nel caso in cui non venga specificato il responsabile viene valorizzato il costo"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="URRFPLIYQW",Visible=.t., Left=12, Top=89,;
    Alignment=0, Width=587, Height=18,;
    Caption="delle prestazioni di tutti gli operatori dello studio utilizzando il costo orario impostato"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="SHNKPYIEMQ",Visible=.t., Left=12, Top=115,;
    Alignment=0, Width=453, Height=18,;
    Caption="in manutenzione utenti."  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="SICPARRRWM",Visible=.t., Left=5, Top=181,;
    Alignment=1, Width=90, Height=18,;
    Caption="Costo orario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="IIVCRAQVVZ",Visible=.t., Left=499, Top=250,;
    Alignment=0, Width=10, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_FLDurata<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kuc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
