* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_aat                                                        *
*              Attivit�                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-26                                                      *
* Last revis.: 2015-11-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_aat"))

* --- Class definition
define class tgsag_aat as StdForm
  Top    = 4
  Left   = 9

  * --- Standard Properties
  Width  = 810
  Height = 570+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-02"
  HelpContextID=143271785
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=356

  * --- Constant Properties
  OFF_ATTI_IDX = 0
  OFFTIPAT_IDX = 0
  CAUMATTI_IDX = 0
  CAN_TIER_IDX = 0
  PRA_ENTI_IDX = 0
  PRA_UFFI_IDX = 0
  LISTINI_IDX = 0
  VALUTE_IDX = 0
  DOC_MAST_IDX = 0
  PAR_AGEN_IDX = 0
  OFF_NOMI_IDX = 0
  NOM_CONT_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  CENCOST_IDX = 0
  TIP_DOCU_IDX = 0
  CDC_MANU_IDX = 0
  DES_DIVE_IDX = 0
  DET_GEN_IDX = 0
  TIP_RISE_IDX = 0
  BUSIUNIT_IDX = 0
  OFF_ERTE_IDX = 0
  CONTI_IDX = 0
  PRA_CONT_IDX = 0
  SAL_NOMI_IDX = 0
  FRA_MODE_IDX = 0
  ANEVENTI_IDX = 0
  TIPEVENT_IDX = 0
  CAU_CONT_IDX = 0
  CAM_AGAZ_IDX = 0
  cFile = "OFF_ATTI"
  cKeySelect = "ATSERIAL"
  cKeyWhere  = "ATSERIAL=this.w_ATSERIAL"
  cKeyWhereODBC = '"ATSERIAL="+cp_ToStrODBC(this.w_ATSERIAL)';

  cKeyWhereODBCqualified = '"OFF_ATTI.ATSERIAL="+cp_ToStrODBC(this.w_ATSERIAL)';

  cPrg = "gsag_aat"
  cComment = "Attivit�"
  icon = "anag.ico"
  cAutoZoom = 'gsag_kat'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ISALT = .F.
  w_CFUNC = space(10)
  w_READAZI = space(5)
  w_LetturaParAgen = space(5)
  o_LetturaParAgen = space(5)
  w_ATSERIAL = space(20)
  o_ATSERIAL = space(20)
  w_VALATT = space(20)
  w_TABKEY = space(8)
  w_DELIMM = .F.
  w_TIPOMASK = space(10)
  w_ATCAUATT = space(20)
  o_ATCAUATT = space(20)
  w_FLPDOC = space(1)
  w_CA_TIMER = space(1)
  o_CA_TIMER = space(1)
  w_FLACQ = space(1)
  w_FLACOMAQ = space(1)
  w_ATOGGETT = space(254)
  w_ATDATINI = ctot('')
  o_ATDATINI = ctot('')
  w_CADTIN = ctot('')
  o_CADTIN = ctot('')
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_ATDATFIN = ctot('')
  o_ATDATFIN = ctot('')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_ORAFIN = space(2)
  o_ORAFIN = space(2)
  w_ATANNDOC = space(4)
  o_ATANNDOC = space(4)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_ATOPPOFF = space(10)
  w_ATSTATUS = space(1)
  o_ATSTATUS = space(1)
  w_ATSTATUS = space(1)
  w_ATNUMPRI = 0
  w_ATPERCOM = 0
  o_ATPERCOM = 0
  w_ATFLNOTI = space(1)
  w_ATGGPREA = 0
  w_ATFLPROM = space(1)
  o_ATFLPROM = space(1)
  w_DATAPRO = ctod('  /  /  ')
  o_DATAPRO = ctod('  /  /  ')
  w_ORAPRO = space(2)
  o_ORAPRO = space(2)
  w_MINPRO = space(2)
  o_MINPRO = space(2)
  w_ATNUMDOC = 0
  w_ONUMDOC = 0
  w_ATALFDOC = space(10)
  w_OALFDOC = space(10)
  w_ATNOTPIA = space(0)
  w_ATDATDOC = ctod('  /  /  ')
  o_ATDATDOC = ctod('  /  /  ')
  w_ATSTAATT = 0
  w_ATFLATRI = space(1)
  o_ATFLATRI = space(1)
  w_ATCODATT = space(5)
  o_ATCODATT = space(5)
  w_DESTIPOL = space(50)
  w_ATPROMEM = ctot('')
  w_ATOPERAT = 0
  w_ATDATRIN = ctot('')
  o_ATDATRIN = ctot('')
  w_DATRIN = ctod('  /  /  ')
  o_DATRIN = ctod('  /  /  ')
  w_ORARIN = space(2)
  o_ORARIN = space(2)
  w_MINRIN = space(2)
  o_MINRIN = space(2)
  w_ATTIPRIS = space(1)
  w_AT_ESITO = space(0)
  w_ATCAUDOC = space(5)
  o_ATCAUDOC = space(5)
  w_ATCAUACQ = space(5)
  o_ATCAUACQ = space(5)
  w_CAUDOC = space(5)
  w_ATCODESI = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_ATCODPRA = space(15)
  o_ATCODPRA = space(15)
  w_ATCODPRA = space(15)
  w_ATCENCOS = space(15)
  o_ATCENCOS = space(15)
  w_ATPUBWEB = space(1)
  w_STARIC = space(1)
  w_GEN_ATT = space(1)
  w_ATCODBUN = space(3)
  w_GIOINI = space(10)
  w_ATDATPRO = ctot('')
  o_ATDATPRO = ctot('')
  w_ATCODNOM = space(15)
  o_ATCODNOM = space(15)
  w_NCODLIS = space(5)
  w_ATCONTAT = space(5)
  o_ATCONTAT = space(5)
  w_ATPERSON = space(60)
  w_FlCreaPers = space(1)
  w_NewCodPer = space(5)
  w_ATTELEFO = space(18)
  w_ATCELLUL = space(18)
  w_AT_EMAIL = space(0)
  w_ATLOCALI = space(30)
  w_AT__ENTE = space(10)
  w_ATUFFICI = space(10)
  w_AT_EMPEC = space(0)
  w_ATCODSED = space(5)
  o_ATCODSED = space(5)
  w_NOMDES = space(40)
  o_NOMDES = space(40)
  w_UTDC = ctot('')
  w_UTCV = 0
  w_UTDV = ctot('')
  w_ATCAITER = space(20)
  w_ATKEYOUT = 0
  w_ATEVANNO = space(4)
  o_ATEVANNO = space(4)
  w_EV__ANNO = space(4)
  w_ATDATOUT = ctot('')
  w_UTCC = 0
  w_FLNSAP = space(1)
  w_GIORIN = space(10)
  w_DESCAN = space(100)
  w_GIOPRV = space(10)
  w_RESCHK = 0
  w_CACHKOBB = space(1)
  o_CACHKOBB = space(1)
  w_GIOFIN = space(10)
  w_CODLIS = space(5)
  w_COLIPR = space(5)
  w_COVAPR = space(3)
  w_CARAGGST = space(10)
  w_FLTRIS = space(1)
  w_ATFLAPON = space(1)
  w_ATDURORE = 0
  o_ATDURORE = 0
  w_ATDURMIN = 0
  o_ATDURMIN = 0
  w_ATRIFSER = space(20)
  w_ATNUMGIO = 0
  w_USCITA = space(1)
  w_DATINI_CHANGED = space(1)
  w_STATOATT = space(1)
  w_ATRIFDOC = space(10)
  w_MVSERIAL = space(10)
  w_MVFLVEAC = space(1)
  w_NUMDOC = 0
  w_NUMDO1 = 0
  w_DATDOC = ctod('  /  /  ')
  w_DATDO1 = ctod('  /  /  ')
  w_NUMRIF = 0
  w_NUMRI1 = 0
  w_DATRIF = ctod('  /  /  ')
  w_DATRI1 = ctod('  /  /  ')
  w_CLADOC = space(2)
  w_ALFDOC = space(10)
  w_ALFDO1 = space(2)
  w_ALFRIF = space(2)
  w_ALFRI1 = space(2)
  w_VSRIF = space(2)
  w_DOCESP = space(1)
  w_LEGIND = space(1)
  w_SERIALE = space(10)
  w_FLRINV = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_ENTE = space(10)
  w_PRENTTIP = space(1)
  o_PRENTTIP = space(1)
  w_AT___FAX = space(18)
  w_NODESCRI = space(40)
  w_DATPRV = ctod('  /  /  ')
  o_DATPRV = ctod('  /  /  ')
  w_ORAPRV = space(2)
  o_ORAPRV = space(2)
  w_MINPRV = space(2)
  o_MINPRV = space(2)
  w_OFDATDOC = ctod('  /  /  ')
  w_OFCODNOM = space(15)
  w_TIPENT = space(1)
  o_TIPENT = space(1)
  w_TIPOENTE = space(1)
  w_ATOREEFF = 0
  w_ATMINEFF = 0
  w_DataCambiata = .F.
  w_GIOPRM = space(10)
  w_CACHKNOM = space(1)
  w_COMODO = space(10)
  w_ATTIPCLI = space(1)
  w_CCDESPIA = space(40)
  w_VisTracciab = space(1)
  w_MATETIPOL = space(1)
  w_TIPNOM = space(1)
  w_DATOBSONOM = ctod('  /  /  ')
  w_CAMINPRE = 0
  w_CAORASYS = space(1)
  w_PACHKCAR = space(1)
  w_PACHKFES = space(1)
  w_FLGCOM = space(1)
  w_FLANAL = space(1)
  w_CENOBSO = ctod('  /  /  ')
  w_FLDANA = space(1)
  w_ATCODVAL = space(3)
  o_ATCODVAL = space(3)
  w_ATCODLIS = space(5)
  o_ATCODLIS = space(5)
  w_DESVAL = space(35)
  w_PARASS = 0
  w_FLAMPA = space(1)
  w_VALLIS = space(3)
  w_ATSTATUS = space(1)
  w_ATSTATUS = space(1)
  w_ATFLVALO = space(1)
  w_ATIMPORT = 0
  w_ATCALDIR = space(1)
  w_ATCOECAL = 0
  w_despra = space(100)
  w_codpra = space(15)
  w_datpre = ctod('  /  /  ')
  w_TipoRiso = space(1)
  o_TipoRiso = space(1)
  w_PARAME = space(2)
  w_ROWORI = 0
  w_RIFCON = space(10)
  w_SERIAL = space(10)
  o_SERIAL = space(10)
  w_PRESTA = space(10)
  w_MVDATREG = ctod('  /  /  ')
  w_ROWNUM = 0
  o_ROWNUM = 0
  w_SERTRA = space(10)
  w_RIGA = 0
  w_SERIAL2 = space(10)
  w_despra = space(100)
  w_codpra = space(15)
  w_datpre = ctod('  /  /  ')
  w_ATRIFMOV = space(10)
  w_CODCLI = space(15)
  o_CODCLI = space(15)
  w_PAFLVISI = space(1)
  o_PAFLVISI = space(1)
  w_LOC_PRA = space(30)
  w_LOC_NOM = space(30)
  w_PRZVAC = space(1)
  w_SCOLIS = space(1)
  w_AGG_ATT_ORIGINE = space(1)
  w_DATINI_ORIG = ctod('  /  /  ')
  w_ORAINI_ORIG = space(2)
  w_MININI_ORIG = space(2)
  w_DECUNI = 0
  w_DECTOT = 0
  w_CALCPICU = 0
  w_CALCPICT = 0
  w_OLDSTATUS = space(1)
  w_ASERIAL = space(20)
  w_SERPIAN = space(20)
  w_FLINTE = space(1)
  w_FLPRAT = space(1)
  w_GESRIS = space(1)
  w_CICLO = space(1)
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_IVALIS = space(1)
  w_QUALIS = space(1)
  w_FLSCOR = space(1)
  w_VOCECR = space(1)
  w_NUMLIS = space(5)
  w_IVACLI = space(1)
  w_ATTPROSC = space(5)
  o_ATTPROSC = space(5)
  w_ATTPROLI = space(5)
  o_ATTPROLI = space(5)
  w_ATTSCLIS = space(5)
  o_ATTSCLIS = space(5)
  w_ATTCOLIS = space(5)
  o_ATTCOLIS = space(5)
  w_ATCENRIC = space(15)
  o_ATCENRIC = space(15)
  w_ATCOMRIC = space(15)
  w_ATATTRIC = space(15)
  w_ATATTCOS = space(15)
  w_CAUACQ = space(5)
  w_KEYLISTB = space(10)
  w_FLGLIS = space(1)
  w_NULLA = space(1)
  w_LISACQ = space(5)
  w_ATLISACQ = space(5)
  w_OKGEN = .F.
  w_ATTARTEM = space(1)
  w_ATTARCON = 0
  w_CODLIN = space(3)
  w_ATFLVALO1 = space(1)
  w_CHKPRE = space(1)
  w_ActByTimer = space(1)
  w_CONTRACN = space(5)
  w_TIPCONCO = space(1)
  w_LISCOLCO = space(5)
  w_STATUSCO = space(1)
  w_FORZADTI = .F.
  w_FORZADTF = .F.
  w_CAFLSTAT = space(1)
  w_FLCCRAUTO = space(1)
  w_SCOLIC = space(1)
  w_FLGCON = space(1)
  w_ATFLRICO = space(1)
  w_AggiornaRicorr = .F.
  w_OLDDATINI = ctot('')
  w_OLDDATFIN = ctot('')
  w_HASEVENT = .F.
  w_HASEVCOP = space(50)
  w_HASEVENT = .F.
  w_HASEVCOP = space(50)
  w_PRIEML = 0
  w_COLPOS = 0
  w_IDRICH = space(15)
  w_ATSEREVE = space(10)
  w_TIPEVE = space(10)
  w_NEWID = .F.
  o_NEWID = .F.
  w_TIPDIR = space(1)
  w_CAUABB = space(5)
  w_PCAUABB = space(5)
  w_RESET = .F.
  w_TIMER = .F.
  w_TMRDELTA = 0
  w_DATALIST = ctod('  /  /  ')
  w_INILIS2 = ctod('  /  /  ')
  w_FINLIS2 = ctod('  /  /  ')
  w_FINLIS3 = ctod('  /  /  ')
  w_INILIS3 = ctod('  /  /  ')
  w_FINLIS4 = ctod('  /  /  ')
  w_INILIS4 = ctod('  /  /  ')
  w_FLGCIC = space(1)
  w_FLGCIC2 = space(1)
  w_FLGCIC3 = space(1)
  w_FLGCIC4 = space(1)
  w_TIPLIS4 = space(1)
  w_TIPLIS3 = space(1)
  w_TIPLIS = space(1)
  w_TIPLIS2 = space(1)
  w_FLSTAT = space(1)
  w_F2STAT = space(1)
  w_F3STAT = space(1)
  w_F4STAT = space(1)
  w_DesNomin = space(60)
  o_DesNomin = space(60)
  w_Pers_Descri = space(60)
  w_Pers_Telefono = space(18)
  w_Pers_Cellulare = space(18)
  w_Pers_E_Mail = space(254)
  w_Pers_E_Mpec = space(254)
  w_ControlloPers = space(5)
  w_PACHKATT = 0
  w_FlagPosiz = space(1)
  w_NOMSG = space(1)
  w_ATLISACQ = space(5)
  w_INIACQ = ctod('  /  /  ')
  w_FINACQ = ctod('  /  /  ')
  w_IVAACQ = space(1)
  w_VALACQ = space(3)
  w_FLSCOAC = space(1)
  w_DESLIS = space(40)
  w_BUFLANAL = space(1)
  w_FLCCRAUTC = space(1)
  w_OLD_ATSTATUS = space(1)
  w_OLD_ATTIPRIS = space(1)
  w_OLD_ATCODPRA = space(15)
  w_MODAPP = space(1)
  w_SELRAGG = space(10)
  w_COMODO = space(10)
  w_GiudicePrat = space(100)
  w_CAUCON = space(5)
  w_FLCOSE = space(1)
  w_NUMSCO = 0
  w_PAATTRIS = space(20)
  w_TipPra = space(10)
  w_MatPra = space(10)
  w_CATIPPRA = space(10)
  w_CAMATPRA = space(10)
  w_CA__ENTE = space(10)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_MailAttivitaRicorrente = space(1)
  w_ACAPO = space(10)
  w_GENDAEV = .F.
  w_CNDATFIN = ctod('  /  /  ')
  w_NOMSGEN = .F.
  w_CATCOM = space(3)
  w_CADTOBSO = ctod('  /  /  ')
  w_ATINIRIC = ctod('  /  /  ')
  w_ATFINRIC = ctod('  /  /  ')
  w_ATINICOS = ctod('  /  /  ')
  w_ATFINCOS = ctod('  /  /  ')
  w_FLPRES = space(1)
  w_CNMATOBB = space(1)
  w_CNASSCTP = space(1)
  w_CNCOMPLX = space(1)
  w_CNPROFMT = space(1)
  w_CNESIPOS = space(1)
  w_CNPERPLX = 0
  w_CNPERPOS = 0
  w_CNTIPPRA = space(10)
  w_TIPRIF = space(2)
  w_CNFLVMLQ = space(1)
  w_ATCODUID = space(254)
  w_OLDANNO = space(4)
  w_OLANDO = space(4)
  w_OLALDO = space(2)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_ATSERIAL = this.W_ATSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_ATANNDOC = this.W_ATANNDOC
  op_ATALFDOC = this.W_ATALFDOC
  op_ATNUMDOC = this.W_ATNUMDOC

  * --- Children pointers
  GSAG_MPA = .NULL.
  GSAG_MCP = .NULL.
  GSAG_MDA = .NULL.
  gsag_mdd = .NULL.
  w_ZoomDocInt = .NULL.
  w_ZoomTrRig = .NULL.
  w_CLOCK = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_aat
  * --- Per bypassare problema canview e visibilit� dei dati
  bCanviewLoaded =.f.
  
  * --- Se impostato a .t. non esegue il check riservato (x elaborazioni batch che funzionalmemnte devo modificare attivit� anche se riservate)
  w_bNoCheckRis = .f.
  
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  
  * Dichiariamo qui le due variabili che servono per aggiornare la maschera
  * del calendario ed Elenco Attivit� in modo che la F_BLANK non cambi il loro valore
  w_MaskCalend=''
  w_AggiornaCalend=.F.
  w_MaskEleAtt=''
  w_AggiornaEleAtt=.F.
  w_CodPart=''
  save_atserial=' '
  w_CUR_EVE=' '
  w_Noautom=.f.
  w_GENERA=.f.
  
  * -- Var. in cui si memorizza il codice della pratica
  * -- in modo che la F_BLANK non cambi il suo valore
  SalvaPrat = space(15)
  
  * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
  * --- Lanciato solo se in stato di Query per prevenire il controllo quando si
  * --- ri preme il tasto in modifica / cariacamento
  Func ah_HasCPEvents(i_cOp)
  	This.w_HASEVCOP=i_cop
    this.w_HASEVENT=.t.
  	If (isAlt() And Upper(This.cFunction)$"EDIT|LOAD" And Upper(i_cop)='ECPF6')
  		this.gsag_mda.NotifyEvent('HasEvent')
  		return(this.w_HASEVENT)
  	Else
  		return(.t.)
  	endif
  EndFunc
  
  PROCEDURE ecpSave()
      local save_atserial
      private fl_gen_att
      fl_gen_att = this.w_GEN_ATT
      DoDefault()
      if this.cFunction="Load" and fl_gen_att $ 'S-R'
       If this.w_RESCHK <> -1
         GSAG_BLD(this , this.save_atserial,fl_gen_att)
        Endif
      else
        IF VARTYPE(this.w_MaskCalend)='O' AND this.w_AggiornaCalend
          * Deveo aggiornare il calendario, chiudiamo subito l'anagrafica
          this.cFunction="Query"
          this.ecpQuit()
        ENDIF
        if  this.w_GENERA
         Do Gsag_Bra with This,'N','A','',This.w_PAATTRIS
        Endif
      endif
  ENDPROC
  
  PROCEDURE ecpQuit()
      * L'anagrafica si chiude se in caricamento si proviene da agenda, elenco attivit� oppure se in AE non sono stati inseriti dati significativi.
      IF (VARTYPE(this.w_MaskCalend)='O' OR VARTYPE(this.w_MaskEleAtt)='O' OR (this.cFunction="Load" AND EMPTY(this.w_ATCAUATT) AND EMPTY(this.w_ATCODPRA) AND IsAlt()))
         * -- Per non fare proporre la domanda Abbandoni le modifiche?
         this.cFunction="Query"
      ENDIF
      If Used((this.w_CUR_EVE))
        Select (this.w_CUR_EVE)
        Use
      Endif
      DoDefault()
      ENDPROC
  
  PROCEDURE ecpdelete()
     This.SalvaPrat=This.w_ATCODPRA
     DoDefault()
     if  this.w_GENERA
       This.w_codpra=This.SalvaPrat
       Do Gsag_Bra with This,'N','A','',This.w_PAATTRIS
     Endif
     IF VARTYPE(this.w_MaskCalend)='O'
     this.ecpQuit()
     this.w_MaskCalend.Notifyevent('Ricalcola')
     ENDIF
  ENDPROC
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'OFF_ATTI','gsag_aat')
    stdPageFrame::Init()
    *set procedure to GSAG_MPA additive
    *set procedure to GSAG_MCP additive
    with this
      .Pages(1).addobject("oPag","tgsag_aatPag1","gsag_aat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generali")
      .Pages(1).HelpContextID = 268295473
      .Pages(2).addobject("oPag","tgsag_aatPag2","gsag_aat",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Prestazioni")
      .Pages(2).HelpContextID = 264854193
      .Pages(3).addobject("oPag","tgsag_aatPag3","gsag_aat",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Tracciabilit�")
      .Pages(3).HelpContextID = 92955432
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAG_MPA
    *release procedure GSAG_MCP
    * --- Area Manuale = Init Page Frame
    * --- gsag_aat
     if VARTYPE(g_SCHEDULER)='C' AND g_SCHEDULER='S'
       this.parent.left=_screen.width+1
     endif
     WITH THIS.PARENT
        IF IsAlt()
         .cAutoZoom = 'GSAG_KAT'
        ELSE
         .cAutoZoom = 'GSAGRKAT'
       ENDIF
     ENDWITH
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_ZoomDocInt = this.oPgFrm.Pages(3).oPag.ZoomDocInt
      this.w_ZoomTrRig = this.oPgFrm.Pages(3).oPag.ZoomTrRig
      this.w_CLOCK = this.oPgFrm.Pages(1).oPag.CLOCK
      DoDefault()
    proc Destroy()
      this.w_ZoomDocInt = .NULL.
      this.w_ZoomTrRig = .NULL.
      this.w_CLOCK = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[30]
    this.cWorkTables[1]='OFFTIPAT'
    this.cWorkTables[2]='CAUMATTI'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='PRA_ENTI'
    this.cWorkTables[5]='PRA_UFFI'
    this.cWorkTables[6]='LISTINI'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='DOC_MAST'
    this.cWorkTables[9]='PAR_AGEN'
    this.cWorkTables[10]='OFF_NOMI'
    this.cWorkTables[11]='NOM_CONT'
    this.cWorkTables[12]='IMP_MAST'
    this.cWorkTables[13]='IMP_DETT'
    this.cWorkTables[14]='CENCOST'
    this.cWorkTables[15]='TIP_DOCU'
    this.cWorkTables[16]='CDC_MANU'
    this.cWorkTables[17]='DES_DIVE'
    this.cWorkTables[18]='DET_GEN'
    this.cWorkTables[19]='TIP_RISE'
    this.cWorkTables[20]='BUSIUNIT'
    this.cWorkTables[21]='OFF_ERTE'
    this.cWorkTables[22]='CONTI'
    this.cWorkTables[23]='PRA_CONT'
    this.cWorkTables[24]='SAL_NOMI'
    this.cWorkTables[25]='FRA_MODE'
    this.cWorkTables[26]='ANEVENTI'
    this.cWorkTables[27]='TIPEVENT'
    this.cWorkTables[28]='CAU_CONT'
    this.cWorkTables[29]='CAM_AGAZ'
    this.cWorkTables[30]='OFF_ATTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(30))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.OFF_ATTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.OFF_ATTI_IDX,3]
  return

  function CreateChildren()
    this.GSAG_MPA = CREATEOBJECT('stdDynamicChild',this,'GSAG_MPA',this.oPgFrm.Page1.oPag.oLinkPC_1_78)
    this.GSAG_MPA.createrealchild()
    this.GSAG_MCP = CREATEOBJECT('stdDynamicChild',this,'GSAG_MCP',this.oPgFrm.Page1.oPag.oLinkPC_1_108)
    this.GSAG_MCP.createrealchild()
    this.GSAG_MDA = CREATEOBJECT('stdDynamicChild',this,'GSAG_MDA',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    this.GSAG_MDA.createrealchild()
    this.gsag_mdd = CREATEOBJECT('stdLazyChild',this,'gsag_mdd')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAG_MPA)
      this.GSAG_MPA.DestroyChildrenChain()
      this.GSAG_MPA=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_78')
    if !ISNULL(this.GSAG_MCP)
      this.GSAG_MCP.DestroyChildrenChain()
      this.GSAG_MCP=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_108')
    if !ISNULL(this.GSAG_MDA)
      this.GSAG_MDA.DestroyChildrenChain()
      this.GSAG_MDA=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    if !ISNULL(this.gsag_mdd)
      this.gsag_mdd.DestroyChildrenChain()
      this.gsag_mdd=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAG_MPA.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAG_MCP.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAG_MDA.IsAChildUpdated()
      i_bRes = i_bRes .or. this.gsag_mdd.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAG_MPA.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAG_MCP.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAG_MDA.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.gsag_mdd.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAG_MPA.NewDocument()
    this.GSAG_MCP.NewDocument()
    this.GSAG_MDA.NewDocument()
    this.gsag_mdd.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAG_MPA.SetKey(;
            .w_ATSERIAL,"PASERIAL";
            )
      this.GSAG_MCP.SetKey(;
            .w_ATSERIAL,"CASERIAL";
            )
      this.GSAG_MDA.SetKey(;
            .w_ATSERIAL,"DASERIAL";
            )
      this.gsag_mdd.SetKey(;
            .w_ATSERIAL,"ADSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAG_MPA.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATSERIAL,"PASERIAL";
             )
      .WriteTo_GSAG_MPA()
      .GSAG_MCP.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATSERIAL,"CASERIAL";
             )
      .WriteTo_GSAG_MCP()
      .GSAG_MDA.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATSERIAL,"DASERIAL";
             )
      .WriteTo_GSAG_MDA()
      .gsag_mdd.ChangeRow(this.cRowID+'      1',1;
             ,.w_ATSERIAL,"ADSERIAL";
             )
      .WriteTo_gsag_mdd()
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAG_MPA)
        i_f=.GSAG_MPA.BuildFilter()
        if !(i_f==.GSAG_MPA.cQueryFilter)
          i_fnidx=.GSAG_MPA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAG_MPA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAG_MPA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAG_MPA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAG_MPA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAG_MCP)
        i_f=.GSAG_MCP.BuildFilter()
        if !(i_f==.GSAG_MCP.cQueryFilter)
          i_fnidx=.GSAG_MCP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAG_MCP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAG_MCP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAG_MCP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAG_MCP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAG_MDA)
        i_f=.GSAG_MDA.BuildFilter()
        if !(i_f==.GSAG_MDA.cQueryFilter)
          i_fnidx=.GSAG_MDA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAG_MDA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAG_MDA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAG_MDA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAG_MDA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSAG_MPA()
  if at('gsag_mpa',lower(this.GSAG_MPA.class))<>0
    if this.GSAG_MPA.w_PAFLVISI<>this.w_PAFLVISI or this.GSAG_MPA.w_OB_TEST<>this.w_DATFIN
      this.GSAG_MPA.w_PAFLVISI = this.w_PAFLVISI
      this.GSAG_MPA.w_OB_TEST = this.w_DATFIN
      this.GSAG_MPA.mCalc(.t.)
    endif
  endif
  return

procedure WriteTo_GSAG_MCP()
  if at('gsag_mcp',lower(this.GSAG_MCP.class))<>0
    if this.GSAG_MCP.w_CODCLI<>this.w_CODCLI or this.GSAG_MCP.w_DATINI<>this.w_DATINI or this.GSAG_MCP.w_ATCODNOM<>this.w_ATCODNOM or this.GSAG_MCP.w_ATCODSED<>this.w_ATCODSED or this.GSAG_MCP.w_DesNomin<>this.w_DesNomin or this.GSAG_MCP.w_NOMDES<>this.w_NOMDES or this.GSAG_MCP.w_DATFIN<>this.w_DATFIN
      this.GSAG_MCP.w_CODCLI = this.w_CODCLI
      this.GSAG_MCP.w_DATINI = this.w_DATINI
      this.GSAG_MCP.w_ATCODNOM = this.w_ATCODNOM
      this.GSAG_MCP.w_ATCODSED = this.w_ATCODSED
      this.GSAG_MCP.w_DesNomin = this.w_DesNomin
      this.GSAG_MCP.w_NOMDES = this.w_NOMDES
      this.GSAG_MCP.w_DATFIN = this.w_DATFIN
      this.GSAG_MCP.mCalc(.t.)
    endif
  endif
  return

procedure WriteTo_GSAG_MDA()
  if at('gsag_mda',lower(this.GSAG_MDA.class))<>0
    if this.GSAG_MDA.w_TipoRisorsa<>this.w_TipoRiso or this.GSAG_MDA.w_CENCOS<>this.w_ATCENCOS or this.GSAG_MDA.w_CODCOM<>this.w_ATCODPRA or this.GSAG_MDA.w_CAUATT<>this.w_ATCAUATT or this.GSAG_MDA.w_OB_TEST<>this.w_DATINI or this.GSAG_MDA.w_CENRIC<>this.w_ATCENRIC or this.GSAG_MDA.w_CODCLI<>this.w_CODCLI
      this.GSAG_MDA.w_TipoRisorsa = this.w_TipoRiso
      this.GSAG_MDA.w_CENCOS = this.w_ATCENCOS
      this.GSAG_MDA.w_CODCOM = this.w_ATCODPRA
      this.GSAG_MDA.w_CAUATT = this.w_ATCAUATT
      this.GSAG_MDA.w_OB_TEST = this.w_DATINI
      this.GSAG_MDA.w_CENRIC = this.w_ATCENRIC
      this.GSAG_MDA.w_CODCLI = this.w_CODCLI
      this.GSAG_MDA.mCalc(.t.)
    endif
  endif
  return

procedure WriteTo_gsag_mdd()
  if at('gsag_mdd',lower(this.gsag_mdd.class))<>0
    if this.gsag_mdd.cnt.w_CODCLI<>this.w_CODCLI
      this.gsag_mdd.cnt.w_CODCLI = this.w_CODCLI
      this.gsag_mdd.cnt.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ATSERIAL = NVL(ATSERIAL,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_48_joined
    link_1_48_joined=.f.
    local link_1_59_joined
    link_1_59_joined=.f.
    local link_1_60_joined
    link_1_60_joined=.f.
    local link_1_68_joined
    link_1_68_joined=.f.
    local link_1_69_joined
    link_1_69_joined=.f.
    local link_1_70_joined
    link_1_70_joined=.f.
    local link_1_90_joined
    link_1_90_joined=.f.
    local link_1_92_joined
    link_1_92_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_1_331_joined
    link_1_331_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- gsag_aat
    * --- per bypassare visiblit� dati se canview fallisce
    this.bCanviewLoaded =.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from OFF_ATTI where ATSERIAL=KeySet.ATSERIAL
    *
    i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('OFF_ATTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "OFF_ATTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' OFF_ATTI '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_48_joined=this.AddJoinedLink_1_48(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_59_joined=this.AddJoinedLink_1_59(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_60_joined=this.AddJoinedLink_1_60(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_68_joined=this.AddJoinedLink_1_68(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_69_joined=this.AddJoinedLink_1_69(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_70_joined=this.AddJoinedLink_1_70(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_90_joined=this.AddJoinedLink_1_90(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_92_joined=this.AddJoinedLink_1_92(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_331_joined=this.AddJoinedLink_1_331(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ISALT = IsAlt()
        .w_VALATT = .w_ATSERIAL
        .w_TABKEY = 'OFF_ATTI'
        .w_DELIMM = .f.
        .w_FLPDOC = space(1)
        .w_CA_TIMER = ""
        .w_CADTIN = ctot("")
        .w_DATAPRO = ctod("  /  /  ")
        .w_ORAPRO = space(2)
        .w_MINPRO = space(2)
        .w_DESTIPOL = space(50)
        .w_OBTEST = i_datsys
        .w_GEN_ATT = 'N'
        .w_NCODLIS = space(5)
        .w_NOMDES = space(40)
        .w_FLNSAP = 'N'
        .w_DESCAN = space(100)
        .w_RESCHK = 0
        .w_CACHKOBB = space(1)
        .w_CODLIS = space(5)
        .w_COLIPR = space(5)
        .w_COVAPR = space(3)
        .w_CARAGGST = space(10)
        .w_FLTRIS = space(1)
        .w_ATFLAPON = space(1)
        .w_ATDURORE = 0
        .w_ATDURMIN = 0
        .w_USCITA = 'N'
        .w_DATINI_CHANGED = 'N'
        .w_STATOATT = space(1)
        .w_MVFLVEAC = space(1)
        .w_NUMDOC = 0
        .w_NUMDO1 = 0
        .w_DATDOC = ctod("  /  /  ")
        .w_DATDO1 = ctod("  /  /  ")
        .w_NUMRIF = 0
        .w_NUMRI1 = 0
        .w_DATRIF = ctod("  /  /  ")
        .w_DATRI1 = ctod("  /  /  ")
        .w_CLADOC = space(2)
        .w_ALFDOC = space(10)
        .w_ALFDO1 = space(2)
        .w_ALFRIF = space(2)
        .w_ALFRI1 = space(2)
        .w_VSRIF = space(2)
        .w_DOCESP = space(1)
        .w_LEGIND = 'S'
        .w_SERIALE = space(10)
        .w_FLRINV = space(1)
        .w_DATOBSO = ctod("  /  /  ")
        .w_ENTE = space(10)
        .w_PRENTTIP = space(1)
        .w_NODESCRI = space(40)
        .w_OFDATDOC = i_datsys
        .w_TIPENT = space(1)
        .w_DataCambiata = IIF(VARTYPE(.w_MaskCalend)='O',.T.,.F.)
        .w_GIOPRM = space(10)
        .w_CACHKNOM = space(1)
        .w_COMODO = space(10)
        .w_ATTIPCLI = space(1)
        .w_CCDESPIA = space(40)
        .w_VisTracciab = space(1)
        .w_MATETIPOL = space(1)
        .w_TIPNOM = space(1)
        .w_DATOBSONOM = ctod("  /  /  ")
        .w_CAMINPRE = 0
        .w_CAORASYS = space(1)
        .w_PACHKCAR = space(1)
        .w_PACHKFES = space(1)
        .w_FLANAL = space(1)
        .w_CENOBSO = ctod("  /  /  ")
        .w_DESVAL = space(35)
        .w_PARASS = 0
        .w_FLAMPA = space(1)
        .w_VALLIS = space(3)
        .w_ATFLVALO = space(1)
        .w_ATIMPORT = 0
        .w_ATCALDIR = space(1)
        .w_ATCOECAL = 0
        .w_TipoRiso = IIF(.w_ISALT,'P','N')
        .w_PARAME = space(2)
        .w_CODCLI = space(15)
        .w_PAFLVISI = space(1)
        .w_LOC_PRA = space(30)
        .w_LOC_NOM = space(30)
        .w_PRZVAC = space(1)
        .w_SCOLIS = space(1)
        .w_AGG_ATT_ORIGINE = space(1)
        .w_DATINI_ORIG = ctod("  /  /  ")
        .w_ORAINI_ORIG = space(2)
        .w_MININI_ORIG = space(2)
        .w_DECUNI = 0
        .w_DECTOT = 0
        .w_SERPIAN = space(20)
        .w_FLINTE = space(1)
        .w_FLPRAT = space(1)
        .w_GESRIS = space(1)
        .w_CICLO = 'E'
        .w_INILIS = ctod("  /  /  ")
        .w_FINLIS = ctod("  /  /  ")
        .w_IVALIS = space(1)
        .w_QUALIS = space(1)
        .w_FLSCOR = space(1)
        .w_NUMLIS = space(5)
        .w_IVACLI = space(1)
        .w_KEYLISTB = SYS(2015)
        .w_FLGLIS = space(1)
        .w_LISACQ = space(5)
        .w_OKGEN = .T.
        .w_ATTARTEM = space(1)
        .w_ATTARCON = 0
        .w_CODLIN = space(3)
        .w_ATFLVALO1 = space(1)
        .w_CHKPRE = space(1)
        .w_ActByTimer = space(1)
        .w_CONTRACN = space(5)
        .w_TIPCONCO = space(1)
        .w_LISCOLCO = space(5)
        .w_STATUSCO = space(1)
        .w_FORZADTI = .F.
        .w_FORZADTF = .F.
        .w_CAFLSTAT = space(1)
        .w_SCOLIC = space(1)
        .w_FLGCON = space(1)
        .w_AggiornaRicorr = .F.
        .w_HASEVENT = .f.
        .w_HASEVCOP = space(50)
        .w_HASEVENT = .f.
        .w_HASEVCOP = space(50)
        .w_PRIEML = 0
        .w_COLPOS = 0
        .w_IDRICH = space(15)
        .w_TIPEVE = space(10)
        .w_NEWID = .f.
        .w_TIPDIR = space(1)
        .w_CAUABB = space(5)
        .w_PCAUABB = space(5)
        .w_RESET = .f.
        .w_TIMER = .F.
        .w_TMRDELTA = 0
        .w_DATALIST = ctod("  /  /  ")
        .w_INILIS2 = ctod("  /  /  ")
        .w_FINLIS2 = ctod("  /  /  ")
        .w_FINLIS3 = ctod("  /  /  ")
        .w_INILIS3 = ctod("  /  /  ")
        .w_FINLIS4 = ctod("  /  /  ")
        .w_INILIS4 = ctod("  /  /  ")
        .w_FLGCIC = space(1)
        .w_FLGCIC2 = space(1)
        .w_FLGCIC3 = space(1)
        .w_FLGCIC4 = space(1)
        .w_TIPLIS4 = space(1)
        .w_TIPLIS3 = space(1)
        .w_TIPLIS = space(1)
        .w_TIPLIS2 = space(1)
        .w_FLSTAT = space(1)
        .w_F2STAT = space(1)
        .w_F3STAT = space(1)
        .w_F4STAT = space(1)
        .w_DesNomin = space(60)
        .w_Pers_Descri = space(60)
        .w_Pers_Telefono = space(18)
        .w_Pers_Cellulare = space(18)
        .w_Pers_E_Mail = space(254)
        .w_Pers_E_Mpec = space(254)
        .w_ControlloPers = space(5)
        .w_PACHKATT = 0
        .w_FlagPosiz = 'N'
        .w_NOMSG = ' '
        .w_INIACQ = ctod("  /  /  ")
        .w_FINACQ = ctod("  /  /  ")
        .w_IVAACQ = space(1)
        .w_VALACQ = space(3)
        .w_FLSCOAC = space(1)
        .w_DESLIS = space(40)
        .w_BUFLANAL = space(1)
        .w_OLD_ATSTATUS = space(1)
        .w_OLD_ATTIPRIS = space(1)
        .w_OLD_ATCODPRA = space(15)
        .w_MODAPP = space(1)
        .w_SELRAGG = space(10)
        .w_COMODO = space(10)
        .w_CAUCON = space(5)
        .w_FLCOSE = space(1)
        .w_NUMSCO = 0
        .w_PAATTRIS = space(20)
        .w_TipPra = space(10)
        .w_MatPra = space(10)
        .w_CATIPPRA = space(10)
        .w_CAMATPRA = space(10)
        .w_CA__ENTE = space(10)
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_ACAPO = chr(13)
        .w_GENDAEV = .F.
        .w_CNDATFIN = ctod("  /  /  ")
        .w_NOMSGEN = .f.
        .w_CATCOM = space(3)
        .w_CADTOBSO = ctod("  /  /  ")
        .w_FLPRES = space(1)
        .w_CNMATOBB = space(1)
        .w_CNASSCTP = space(1)
        .w_CNCOMPLX = space(1)
        .w_CNPROFMT = space(1)
        .w_CNESIPOS = space(1)
        .w_CNPERPLX = 0
        .w_CNPERPOS = 0
        .w_CNTIPPRA = space(10)
        .w_TIPRIF = space(2)
        .w_CNFLVMLQ = space(1)
        .w_OLANDO = space(4)
        .w_OLALDO = space(2)
        .w_CFUNC = this.cFunction
        .w_READAZI = i_CODAZI
        .w_LetturaParAgen = i_CodAzi
          .link_1_4('Load')
        .w_ATSERIAL = NVL(ATSERIAL,space(20))
        .op_ATSERIAL = .w_ATSERIAL
        .w_TIPOMASK = IIF(VARTYPE(This.w_MaskCalend)='O','C','U')
        .w_ATCAUATT = NVL(ATCAUATT,space(20))
          if link_1_10_joined
            this.w_ATCAUATT = NVL(CACODICE110,NVL(this.w_ATCAUATT,space(20)))
            this.w_ATOGGETT = NVL(CADESCRI110,space(254))
            this.w_ATDURORE = NVL(CADURORE110,0)
            this.w_ATDURMIN = NVL(CADURMIN110,0)
            this.w_CADTIN = NVL(CADATINI110,ctot(""))
            this.w_ATGGPREA = NVL(CAGGPREA110,0)
            this.w_ATSTATUS = NVL(CASTAATT110,space(1))
            this.w_ATNUMPRI = NVL(CAPRIORI110,0)
            this.w_ATCODATT = NVL(CATIPATT110,space(5))
            this.w_FLNSAP = NVL(CAFLNSAP110,space(1))
            this.w_ATCAUDOC = NVL(CACAUDOC110,space(5))
            this.w_CACHKOBB = NVL(CACHKOBB110,space(1))
            this.w_CARAGGST = NVL(CARAGGST110,space(10))
            this.w_ATSTAATT = NVL(CADISPON110,0)
            this.w_FLTRIS = NVL(CAFLTRIS110,space(1))
            this.w_FLRINV = NVL(CAFLRINV110,space(1))
            this.w_ATFLNOTI = NVL(CAFLNOTI110,space(1))
            this.w_CACHKNOM = NVL(CACHKNOM110,space(1))
            this.w_CAMINPRE = NVL(CAMINPRE110,0)
            this.w_CAORASYS = NVL(CAORASYS110,space(1))
            this.w_ATFLPROM = NVL(CAFLPREA110,space(1))
            this.w_FLANAL = NVL(CAFLANAL110,space(1))
            this.w_ATPUBWEB = NVL(CAPUBWEB110,space(1))
            this.w_ATCAUACQ = NVL(CACAUACQ110,space(5))
            this.w_ATFLATRI = NVL(CAFLATRI110,space(1))
            this.w_CAFLSTAT = NVL(CAFLSTAT110,space(1))
            this.w_COLPOS = NVL(CACOLPOS110,0)
            this.w_PRIEML = NVL(CAPRIEML110,0)
            this.w_CAUABB = NVL(CACAUABB110,space(5))
            this.w_CA_TIMER = NVL(CA_TIMER110,space(1))
            this.w_TIPEVE = NVL(CATIPEVE110,space(10))
            this.w_ATNOTPIA = NVL(CA__NOTE110,space(0))
            this.w_MODAPP = NVL(CAMODAPP110,space(1))
            this.w_CATIPPRA = NVL(CATIPPRA110,space(10))
            this.w_CAMATPRA = NVL(CAMATPRA110,space(10))
            this.w_CA__ENTE = NVL(CA__ENTE110,space(10))
            this.w_CADTOBSO = NVL(cp_ToDate(CADTOBSO110),ctod("  /  /  "))
            this.w_FLPDOC = NVL(CAFLPDOC110,space(1))
            this.w_ATALFDOC = NVL(CASERDOC110,space(10))
          else
          .link_1_10('Load')
          endif
        .w_FLACQ = IIF(Not Empty(.w_CAUACQ),Docgesana(.w_CAUACQ,'A'),.w_FLANAL)
        .w_FLACOMAQ = Docgesana(.w_CAUACQ,'C')
        .w_ATOGGETT = NVL(ATOGGETT,space(254))
        .w_ATDATINI = NVL(ATDATINI,ctot(""))
        .w_DATINI = IIF(.w_CFUNC='Load' and .w_TIPOMASK='U' and not .w_FORZADTI,i_datsys,TTOD(.w_ATDATINI))
        .w_ORAINI = PADL(ALLTRIM(STR(HOUR(IIF(.w_CFUNC='Load' and .w_TIPOMASK='U' and not .w_FORZADTI,IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN),cp_CharToDatetime( IIF(NOT EMPTY(.w_ATDATINI), DTOC(.w_ATDATINI)+' '+PADL(ALLTRIM(STR(HOUR(.w_ATDATINI),2)),2,'0')+':'+PADL(ALLTRIM(STR(MINUTE(.w_ATDATINI),2)),2,'0')+':00', '  -  -       :  :  ')))))),2,'0')
        .w_MININI = PADL(ALLTRIM(STR(MINUTE(IIF(.w_CFUNC='Load' and .w_TIPOMASK='U' and not .w_FORZADTI,IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN),cp_CharToDatetime( IIF(NOT EMPTY(.w_ATDATINI), DTOC(.w_ATDATINI)+' '+PADL(ALLTRIM(STR(HOUR(.w_ATDATINI),2)),2,'0')+':'+PADL(ALLTRIM(STR(MINUTE(.w_ATDATINI),2)),2,'0')+':00', '  -  -       :  :  ')))))),2,'0')
        .w_ATDATFIN = NVL(ATDATFIN,ctot(""))
        .w_DATFIN = IIF(.w_CFUNC='Load' and not .w_FORZADTF,.w_DATINI+INT( (VAL(.w_ORAINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)) / 24),TTOD(.w_ATDATFIN))
        .w_ORAFIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load' and not .w_FORZADTF,STR(MOD( (VAL(.w_ORAINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)), 24)),STR(HOUR(.w_ATDATFIN),2))),2)
        .w_ATANNDOC = NVL(ATANNDOC,space(4))
        .op_ATANNDOC = .w_ATANNDOC
        .w_MINFIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load' and not .w_FORZADTF,STR(MOD( (VAL(.w_MININI)+.w_ATDURMIN), 60),2),STR(MINUTE(.w_ATDATFIN),2))),2)
        .w_ATOPPOFF = NVL(ATOPPOFF,space(10))
          * evitabile
          *.link_1_27('Load')
        .w_ATSTATUS = NVL(ATSTATUS,space(1))
        .w_ATSTATUS = NVL(ATSTATUS,space(1))
        .w_ATNUMPRI = NVL(ATNUMPRI,0)
        .w_ATPERCOM = NVL(ATPERCOM,0)
        .w_ATFLNOTI = NVL(ATFLNOTI,space(1))
        .w_ATGGPREA = NVL(ATGGPREA,0)
        .w_ATFLPROM = NVL(ATFLPROM,space(1))
        .w_ATNUMDOC = NVL(ATNUMDOC,0)
        .op_ATNUMDOC = .w_ATNUMDOC
        .w_ONUMDOC = .w_ATNUMDOC
        .w_ATALFDOC = NVL(ATALFDOC,space(10))
        .op_ATALFDOC = .w_ATALFDOC
        .w_OALFDOC = .w_ATALFDOC
        .w_ATNOTPIA = NVL(ATNOTPIA,space(0))
        .w_ATDATDOC = NVL(cp_ToDate(ATDATDOC),ctod("  /  /  "))
        .w_ATSTAATT = NVL(ATSTAATT,0)
        .w_ATFLATRI = NVL(ATFLATRI,space(1))
        .w_ATCODATT = NVL(ATCODATT,space(5))
          if link_1_48_joined
            this.w_ATCODATT = NVL(TACODTIP148,NVL(this.w_ATCODATT,space(5)))
            this.w_DESTIPOL = NVL(TADESCRI148,space(50))
          else
          .link_1_48('Load')
          endif
        .w_ATPROMEM = NVL(ATPROMEM,ctot(""))
        .w_ATOPERAT = NVL(ATOPERAT,0)
        .w_ATDATRIN = NVL(ATDATRIN,ctot(""))
        .w_DATRIN = IIF(.w_CFUNC='Load',IIF(.w_ATSTATUS $ 'FPT' AND NOT EMPTY(.w_DATRIN),.w_DATRIN,CTOD('  -  -  ')),TTOD(.w_ATDATRIN))
        .w_ORARIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',IIF(.w_ATSTATUS $ 'FPT' AND NOT EMPTY(.w_ORARIN),.w_ORARIN,'00'),STR(HOUR(.w_ATDATRIN),2))),2)
        .w_MINRIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',IIF(.w_ATSTATUS $ 'FPT' AND NOT EMPTY(.w_MINRIN),.w_MINRIN,'00'),STR(MINUTE(.w_ATDATRIN),2))),2)
        .w_ATTIPRIS = NVL(ATTIPRIS,space(1))
          * evitabile
          *.link_1_56('Load')
        .w_AT_ESITO = NVL(AT_ESITO,space(0))
        .w_ATCAUDOC = NVL(ATCAUDOC,space(5))
          if link_1_59_joined
            this.w_ATCAUDOC = NVL(TDTIPDOC159,NVL(this.w_ATCAUDOC,space(5)))
            this.w_PARAME = NVL(TDCATDOC159,space(2))
            this.w_PRZVAC = NVL(TDPRZVAC159,space(1))
            this.w_FLINTE = NVL(TDFLINTE159,space(1))
            this.w_FLPRAT = NVL(TDFLPRAT159,space(1))
            this.w_FLDANA = NVL(TDFLANAL159,space(1))
            this.w_FLGCOM = NVL(TDFLCOMM159,space(1))
            this.w_MVFLVEAC = NVL(TDFLVEAC159,space(1))
            this.w_FLGLIS = NVL(TDFLGLIS159,space(1))
            this.w_CAUCON = NVL(TDCAUCON159,space(5))
            this.w_NUMSCO = NVL(TDNUMSCO159,0)
          else
          .link_1_59('Load')
          endif
        .w_ATCAUACQ = NVL(ATCAUACQ,space(5))
          if link_1_60_joined
            this.w_ATCAUACQ = NVL(TDTIPDOC160,NVL(this.w_ATCAUACQ,space(5)))
            this.w_PRZVAC = NVL(TDPRZVAC160,space(1))
            this.w_FLPRAT = NVL(TDFLPRAT160,space(1))
            this.w_MVFLVEAC = NVL(TDFLVEAC160,space(1))
          else
          .link_1_60('Load')
          endif
        .w_CAUDOC = .w_ATCAUDOC
        .w_ATCODESI = NVL(ATCODESI,space(5))
        .w_ATCODPRA = NVL(ATCODPRA,space(15))
          if link_1_68_joined
            this.w_ATCODPRA = NVL(CNCODCAN168,NVL(this.w_ATCODPRA,space(15)))
            this.w_DESCAN = NVL(CNDESCAN168,space(100))
            this.w_LOC_PRA = NVL(CNLOCALI168,space(30))
            this.w_AT__ENTE = NVL(CN__ENTE168,space(10))
            this.w_ATUFFICI = NVL(CNUFFICI168,space(10))
            this.w_COLIPR = NVL(CNCODLIS168,space(5))
            this.w_ATFLVALO = NVL(CNFLVALO168,space(1))
            this.w_ATIMPORT = NVL(CNIMPORT168,0)
            this.w_ATCALDIR = NVL(CNCALDIR168,space(1))
            this.w_ATCOECAL = NVL(CNCOECAL168,0)
            this.w_ATFLAPON = NVL(CNFLAPON168,space(1))
            this.w_COVAPR = NVL(CNCODVAL168,space(3))
            this.w_PARASS = NVL(CNPARASS168,0)
            this.w_FLAMPA = NVL(CNFLAMPA168,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CNDTOBSO168),ctod("  /  /  "))
            this.w_ENTE = NVL(CN__ENTE168,space(10))
            this.w_ATTARTEM = NVL(CNTARTEM168,space(1))
            this.w_ATTARCON = NVL(CNTARCON168,0)
            this.w_CONTRACN = NVL(CNCONTRA168,space(5))
            this.w_ATFLVALO1 = NVL(CNFLDIND168,space(1))
            this.w_CNDATFIN = NVL(cp_ToDate(CNDATFIN168),ctod("  /  /  "))
            this.w_CNMATOBB = NVL(CNMATOBB168,space(1))
            this.w_CNASSCTP = NVL(CNASSCTP168,space(1))
            this.w_CNCOMPLX = NVL(CNCOMPLX168,space(1))
            this.w_CNPROFMT = NVL(CNPROFMT168,space(1))
            this.w_CNESIPOS = NVL(CNESIPOS168,space(1))
            this.w_CNPERPLX = NVL(CNPERPLX168,0)
            this.w_CNPERPOS = NVL(CNPERPOS168,0)
            this.w_CNTIPPRA = NVL(CNTIPPRA168,space(10))
            this.w_CNFLVMLQ = NVL(CNFLVMLQ168,space(1))
          else
          .link_1_68('Load')
          endif
        .w_ATCODPRA = NVL(ATCODPRA,space(15))
          if link_1_69_joined
            this.w_ATCODPRA = NVL(CNCODCAN169,NVL(this.w_ATCODPRA,space(15)))
            this.w_DESCAN = NVL(CNDESCAN169,space(100))
            this.w_DATOBSO = NVL(cp_ToDate(CNDTOBSO169),ctod("  /  /  "))
            this.w_ENTE = NVL(CN__ENTE169,space(10))
            this.w_TipPra = NVL(CNTIPPRA169,space(10))
            this.w_MatPra = NVL(CNMATPRA169,space(10))
          else
          .link_1_69('Load')
          endif
        .w_ATCENCOS = NVL(ATCENCOS,space(15))
          if link_1_70_joined
            this.w_ATCENCOS = NVL(CC_CONTO170,NVL(this.w_ATCENCOS,space(15)))
            this.w_CCDESPIA = NVL(CCDESPIA170,space(40))
            this.w_CENOBSO = NVL(cp_ToDate(CCDTOBSO170),ctod("  /  /  "))
          else
          .link_1_70('Load')
          endif
        .w_ATPUBWEB = NVL(ATPUBWEB,space(1))
        .w_STARIC = IIF(.w_newid,'A',.w_STARIC)
        .w_ATCODBUN = NVL(ATCODBUN,space(3))
          .link_1_76('Load')
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .w_GIOINI = IIF(NOT EMPTY(.w_DATINI),LEFT(g_GIORNO[DOW(.w_DATINI)],3), '   ')
        .w_ATDATPRO = NVL(ATDATPRO,ctot(""))
        .w_ATCODNOM = NVL(ATCODNOM,space(15))
          if link_1_90_joined
            this.w_ATCODNOM = NVL(NOCODICE190,NVL(this.w_ATCODNOM,space(15)))
            this.w_DesNomin = NVL(NODESCRI190,space(60))
            this.w_COMODO = NVL(NODESCR2190,space(10))
            this.w_ATTELEFO = NVL(NOTELEFO190,space(18))
            this.w_AT___FAX = NVL(NOTELFAX190,space(18))
            this.w_AT_EMAIL = NVL(NO_EMAIL190,space(0))
            this.w_AT_EMPEC = NVL(NO_EMPEC190,space(0))
            this.w_ATCELLUL = NVL(NONUMCEL190,space(18))
            this.w_ATTIPCLI = NVL(NOTIPCLI190,space(1))
            this.w_TIPNOM = NVL(NOTIPNOM190,space(1))
            this.w_DATOBSONOM = NVL(cp_ToDate(NODTOBSO190),ctod("  /  /  "))
            this.w_CODCLI = NVL(NOCODCLI190,space(15))
            this.w_LOC_NOM = NVL(NOLOCALI190,space(30))
            this.w_CODLIN = NVL(NOCODLIN190,space(3))
            this.w_NCODLIS = NVL(NONUMLIS190,space(5))
          else
          .link_1_90('Load')
          endif
        .w_ATCONTAT = NVL(ATCONTAT,space(5))
          if link_1_92_joined
            this.w_ATCONTAT = NVL(NCCODCON192,NVL(this.w_ATCONTAT,space(5)))
            this.w_Pers_Descri = NVL(NCPERSON192,space(60))
            this.w_Pers_Telefono = NVL(NCTELEFO192,space(18))
            this.w_Pers_Cellulare = NVL(NCNUMCEL192,space(18))
            this.w_Pers_E_Mail = NVL(NC_EMAIL192,space(254))
            this.w_Pers_E_Mpec = NVL(NC_EMPEC192,space(254))
          else
          .link_1_92('Load')
          endif
        .w_ATPERSON = NVL(ATPERSON,space(60))
        .w_FlCreaPers = IIF(.cFunction='Load' AND !EMPTY(.w_ATCODNOM) AND EMPTY(.w_ATCONTAT),.w_FlCreaPers,'N')
        .w_NewCodPer = IIF(.cFunction='Load' AND !EMPTY(.w_ATCODNOM) AND EMPTY(.w_ATCONTAT),.w_NewCodPer,'')
          .link_1_97('Load')
        .w_ATTELEFO = NVL(ATTELEFO,space(18))
        .w_ATCELLUL = NVL(ATCELLUL,space(18))
        .w_AT_EMAIL = NVL(AT_EMAIL,space(0))
        .w_ATLOCALI = NVL(ATLOCALI,space(30))
        .w_AT__ENTE = NVL(AT__ENTE,space(10))
          * evitabile
          *.link_1_103('Load')
        .w_ATUFFICI = NVL(ATUFFICI,space(10))
          * evitabile
          *.link_1_104('Load')
        .w_AT_EMPEC = NVL(AT_EMPEC,space(0))
        .w_ATCODSED = NVL(ATCODSED,space(5))
          .link_1_106('Load')
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_ATCAITER = NVL(ATCAITER,space(20))
        .w_ATKEYOUT = NVL(ATKEYOUT,0)
        .w_ATEVANNO = NVL(ATEVANNO,space(4))
        .w_EV__ANNO = .w_ATEVANNO
        .w_ATDATOUT = NVL(ATDATOUT,ctot(""))
        .w_UTCC = NVL(UTCC,0)
        .w_GIORIN = IIF(EMPTY(.w_ATDATRIN),'',LEFT(g_GIORNO[DOW(.w_ATDATRIN)],3))
        .w_GIOPRV = IIF(EMPTY(.w_ATDATPRO),'',LEFT(g_GIORNO[DOW(.w_ATDATPRO)],3))
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_144.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_145.Calculate()
        .w_GIOFIN = IIF(NOT EMPTY(.w_DATFIN),LEFT(g_GIORNO[DOW(.w_DATFIN)],3), '   ')
        .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
        .w_ATRIFSER = NVL(ATRIFSER,space(20))
        .w_ATNUMGIO = NVL(ATNUMGIO,0)
        .oPgFrm.Page1.oPag.oObj_1_168.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_171.Calculate()
        .w_ATRIFDOC = NVL(ATRIFDOC,space(10))
          .link_1_173('Load')
        .w_MVSERIAL = .w_ATRIFDOC
          .link_1_199('Load')
        .w_AT___FAX = NVL(AT___FAX,space(18))
        .w_DATPRV = IIF(.w_CFUNC='Load',CTOD('  -  -  '),TTOD(.w_ATDATPRO))
        .w_ORAPRV = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',.w_ORAPRV,STR(HOUR(.w_ATDATPRO),2))),2)
        .w_MINPRV = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',.w_MINPRV,STR(MINUTE(.w_ATDATPRO),2))),2)
        .oPgFrm.Page1.oPag.oObj_1_212.Calculate()
        .w_OFCODNOM = .w_ATCODNOM
        .w_TIPOENTE = IIF(EMPTY(.w_TIPENT),.w_PRENTTIP,.w_TIPENT)
        .w_ATOREEFF = NVL(ATOREEFF,0)
        .w_ATMINEFF = NVL(ATMINEFF,0)
        .oPgFrm.Page1.oPag.oObj_1_244.Calculate()
        .w_FLGCOM = Docgesana(.w_ATCAUDOC,'C')
        .w_FLDANA = Docgesana(.w_ATCAUDOC,'A')
        .w_ATCODVAL = NVL(ATCODVAL,space(3))
          if link_2_2_joined
            this.w_ATCODVAL = NVL(VACODVAL202,NVL(this.w_ATCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL202,space(35))
            this.w_DECUNI = NVL(VADECUNI202,0)
            this.w_DECTOT = NVL(VADECTOT202,0)
          else
          .link_2_2('Load')
          endif
        .w_ATCODLIS = NVL(ATCODLIS,space(5))
          .link_2_3('Load')
        .w_ATSTATUS = NVL(ATSTATUS,space(1))
        .w_ATSTATUS = NVL(ATSTATUS,space(1))
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
        .w_despra = .w_DESCAN
        .w_codpra = .w_ATCODPRA
        .w_datpre = .w_DATINI
        .oPgFrm.Page3.oPag.ZoomDocInt.Calculate()
        .w_ROWORI = Nvl(.w_ZoomDocInt.GetVar('CPROWORD'),0)
        .w_RIFCON = Nvl(.w_ZoomDocInt.GetVar('MVRIFCON'),' ')
        .w_SERIAL = Nvl(.w_ZoomDocInt.GetVar('MVSERIAL'),' ')
        .w_PRESTA = Nvl(.w_ZoomDocInt.GetVar('ARPRESTA'),' ')
        .w_MVDATREG = .w_ZoomDocInt.GetVar('MVDATREG')
        .w_ROWNUM = IIF(.w_ZoomDocInt.GetVar('CPROWNUM')=0,1,.w_ZoomDocInt.GetVar('CPROWNUM'))
        .w_SERTRA = IIF(EMPTY(.w_SERIAL),.w_ATRIFDOC,.w_SERIAL)
        .w_RIGA = .w_ROWNUM
        .oPgFrm.Page3.oPag.ZoomTrRig.Calculate()
        .w_SERIAL2 = .w_ZoomTrRig.getVar('MVSERIAL')
        .oPgFrm.Page3.oPag.oObj_3_14.Calculate()
        .w_despra = .w_DESCAN
        .w_codpra = .w_ATCODPRA
        .w_datpre = .w_DATINI
        .w_ATRIFMOV = NVL(ATRIFMOV,space(10))
          * evitabile
          *.link_2_31('Load')
          .link_1_251('Load')
        .oPgFrm.Page1.oPag.oObj_1_255.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_256.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_261.Calculate()
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_OLDSTATUS = .w_ATSTATUS
        .w_ASERIAL = .w_ATSERIAL
          .link_1_266('Load')
        .w_VOCECR = Docgesana(.w_ATCAUACQ,'V')
          .link_1_274('Load')
        .w_ATTPROSC = NVL(ATTPROSC,space(5))
        .w_ATTPROLI = NVL(ATTPROLI,space(5))
        .w_ATTSCLIS = NVL(ATTSCLIS,space(5))
        .w_ATTCOLIS = NVL(ATTCOLIS,space(5))
        .w_ATCENRIC = NVL(ATCENRIC,space(15))
        .w_ATCOMRIC = NVL(ATCOMRIC,space(15))
        .w_ATATTRIC = NVL(ATATTRIC,space(15))
        .w_ATATTCOS = NVL(ATATTCOS,space(15))
        .oPgFrm.Page1.oPag.oObj_1_285.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_286.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_287.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_288.Calculate()
        .w_CAUACQ = .w_ATCAUACQ
        .w_NULLA = 'X'
        .oPgFrm.Page1.oPag.oObj_1_293.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_294.Calculate()
        .w_ATLISACQ = NVL(ATLISACQ,space(5))
        .oPgFrm.Page2.oPag.oObj_2_45.Calculate()
          .link_1_304('Load')
        .oPgFrm.Page1.oPag.oObj_1_312.Calculate()
        .w_FLCCRAUTO = Docgesana(.w_ATCAUDOC,'R')
        .w_ATFLRICO = NVL(ATFLRICO,space(1))
        .w_OLDDATINI = .w_ATDATINI
        .w_OLDDATFIN = .w_ATDATFIN
        .w_ATSEREVE = NVL(ATSEREVE,space(10))
          if link_1_331_joined
            this.w_ATSEREVE = NVL(EVSERIAL431,NVL(this.w_ATSEREVE,space(10)))
            this.w_IDRICH = NVL(EVIDRICH431,space(15))
            this.w_STARIC = NVL(EVSTARIC431,space(1))
            this.w_TIPEVE = NVL(EVTIPEVE431,space(10))
          else
          .link_1_331('Load')
          endif
          .link_1_333('Load')
        .oPgFrm.Page1.oPag.CLOCK.Calculate()
        .w_ATLISACQ = NVL(ATLISACQ,space(5))
          .link_2_49('Load')
        .oPgFrm.Page2.oPag.oObj_2_57.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_58.Calculate()
        .w_FLCCRAUTC = Docgesana(.w_CAUACQ,'R')
        .oPgFrm.Page1.oPag.oObj_1_382.Calculate()
        .w_GiudicePrat = IIF(.w_ISALT AND !EMPTY(.w_ATCODPRA),GetGiudici(.w_ATCODPRA, '',.w_DATFIN, 100, ''),'')
          .link_1_396('Load')
        .w_MailAttivitaRicorrente = 'N'
        .w_ATINIRIC = NVL(cp_ToDate(ATINIRIC),ctod("  /  /  "))
        .w_ATFINRIC = NVL(cp_ToDate(ATFINRIC),ctod("  /  /  "))
        .w_ATINICOS = NVL(cp_ToDate(ATINICOS),ctod("  /  /  "))
        .w_ATFINCOS = NVL(cp_ToDate(ATFINCOS),ctod("  /  /  "))
        .w_ATCODUID = NVL(ATCODUID,space(254))
        .w_OLDANNO = .w_ATANNDOC
        .oPgFrm.Page1.oPag.oObj_1_442.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_443.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_444.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'OFF_ATTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_79.enabled = this.oPgFrm.Page1.oPag.oBtn_1_79.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_109.enabled = this.oPgFrm.Page1.oPag.oBtn_1_109.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_110.enabled = this.oPgFrm.Page1.oPag.oBtn_1_110.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_111.enabled = this.oPgFrm.Page1.oPag.oBtn_1_111.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_113.enabled = this.oPgFrm.Page1.oPag.oBtn_1_113.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_222.enabled = this.oPgFrm.Page1.oPag.oBtn_1_222.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_223.enabled = this.oPgFrm.Page1.oPag.oBtn_1_223.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_11.enabled = this.oPgFrm.Page2.oPag.oBtn_2_11.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_29.enabled = this.oPgFrm.Page2.oPag.oBtn_2_29.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_32.enabled = this.oPgFrm.Page2.oPag.oBtn_2_32.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_43.enabled = this.oPgFrm.Page2.oPag.oBtn_2_43.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_388.enabled = this.oPgFrm.Page1.oPag.oBtn_1_388.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_389.enabled = this.oPgFrm.Page1.oPag.oBtn_1_389.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_390.enabled = this.oPgFrm.Page1.oPag.oBtn_1_390.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_412.enabled = this.oPgFrm.Page1.oPag.oBtn_1_412.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_441.enabled = this.oPgFrm.Page1.oPag.oBtn_1_441.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsag_aat
    this.w_OLANDO=this.w_ATANNDOC
    this.w_OLALDO=this.w_ATALFDOC
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ISALT = .f.
      .w_CFUNC = space(10)
      .w_READAZI = space(5)
      .w_LetturaParAgen = space(5)
      .w_ATSERIAL = space(20)
      .w_VALATT = space(20)
      .w_TABKEY = space(8)
      .w_DELIMM = .f.
      .w_TIPOMASK = space(10)
      .w_ATCAUATT = space(20)
      .w_FLPDOC = space(1)
      .w_CA_TIMER = space(1)
      .w_FLACQ = space(1)
      .w_FLACOMAQ = space(1)
      .w_ATOGGETT = space(254)
      .w_ATDATINI = ctot("")
      .w_CADTIN = ctot("")
      .w_DATINI = ctod("  /  /  ")
      .w_ORAINI = space(2)
      .w_MININI = space(2)
      .w_ATDATFIN = ctot("")
      .w_DATFIN = ctod("  /  /  ")
      .w_ORAFIN = space(2)
      .w_ATANNDOC = space(4)
      .w_MINFIN = space(2)
      .w_ATOPPOFF = space(10)
      .w_ATSTATUS = space(1)
      .w_ATSTATUS = space(1)
      .w_ATNUMPRI = 0
      .w_ATPERCOM = 0
      .w_ATFLNOTI = space(1)
      .w_ATGGPREA = 0
      .w_ATFLPROM = space(1)
      .w_DATAPRO = ctod("  /  /  ")
      .w_ORAPRO = space(2)
      .w_MINPRO = space(2)
      .w_ATNUMDOC = 0
      .w_ONUMDOC = 0
      .w_ATALFDOC = space(10)
      .w_OALFDOC = space(10)
      .w_ATNOTPIA = space(0)
      .w_ATDATDOC = ctod("  /  /  ")
      .w_ATSTAATT = 0
      .w_ATFLATRI = space(1)
      .w_ATCODATT = space(5)
      .w_DESTIPOL = space(50)
      .w_ATPROMEM = ctot("")
      .w_ATOPERAT = 0
      .w_ATDATRIN = ctot("")
      .w_DATRIN = ctod("  /  /  ")
      .w_ORARIN = space(2)
      .w_MINRIN = space(2)
      .w_ATTIPRIS = space(1)
      .w_AT_ESITO = space(0)
      .w_ATCAUDOC = space(5)
      .w_ATCAUACQ = space(5)
      .w_CAUDOC = space(5)
      .w_ATCODESI = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_ATCODPRA = space(15)
      .w_ATCODPRA = space(15)
      .w_ATCENCOS = space(15)
      .w_ATPUBWEB = space(1)
      .w_STARIC = space(1)
      .w_GEN_ATT = space(1)
      .w_ATCODBUN = space(3)
      .w_GIOINI = space(10)
      .w_ATDATPRO = ctot("")
      .w_ATCODNOM = space(15)
      .w_NCODLIS = space(5)
      .w_ATCONTAT = space(5)
      .w_ATPERSON = space(60)
      .w_FlCreaPers = space(1)
      .w_NewCodPer = space(5)
      .w_ATTELEFO = space(18)
      .w_ATCELLUL = space(18)
      .w_AT_EMAIL = space(0)
      .w_ATLOCALI = space(30)
      .w_AT__ENTE = space(10)
      .w_ATUFFICI = space(10)
      .w_AT_EMPEC = space(0)
      .w_ATCODSED = space(5)
      .w_NOMDES = space(40)
      .w_UTDC = ctot("")
      .w_UTCV = 0
      .w_UTDV = ctot("")
      .w_ATCAITER = space(20)
      .w_ATKEYOUT = 0
      .w_ATEVANNO = space(4)
      .w_EV__ANNO = space(4)
      .w_ATDATOUT = ctot("")
      .w_UTCC = 0
      .w_FLNSAP = space(1)
      .w_GIORIN = space(10)
      .w_DESCAN = space(100)
      .w_GIOPRV = space(10)
      .w_RESCHK = 0
      .w_CACHKOBB = space(1)
      .w_GIOFIN = space(10)
      .w_CODLIS = space(5)
      .w_COLIPR = space(5)
      .w_COVAPR = space(3)
      .w_CARAGGST = space(10)
      .w_FLTRIS = space(1)
      .w_ATFLAPON = space(1)
      .w_ATDURORE = 0
      .w_ATDURMIN = 0
      .w_ATRIFSER = space(20)
      .w_ATNUMGIO = 0
      .w_USCITA = space(1)
      .w_DATINI_CHANGED = space(1)
      .w_STATOATT = space(1)
      .w_ATRIFDOC = space(10)
      .w_MVSERIAL = space(10)
      .w_MVFLVEAC = space(1)
      .w_NUMDOC = 0
      .w_NUMDO1 = 0
      .w_DATDOC = ctod("  /  /  ")
      .w_DATDO1 = ctod("  /  /  ")
      .w_NUMRIF = 0
      .w_NUMRI1 = 0
      .w_DATRIF = ctod("  /  /  ")
      .w_DATRI1 = ctod("  /  /  ")
      .w_CLADOC = space(2)
      .w_ALFDOC = space(10)
      .w_ALFDO1 = space(2)
      .w_ALFRIF = space(2)
      .w_ALFRI1 = space(2)
      .w_VSRIF = space(2)
      .w_DOCESP = space(1)
      .w_LEGIND = space(1)
      .w_SERIALE = space(10)
      .w_FLRINV = space(1)
      .w_DATOBSO = ctod("  /  /  ")
      .w_ENTE = space(10)
      .w_PRENTTIP = space(1)
      .w_AT___FAX = space(18)
      .w_NODESCRI = space(40)
      .w_DATPRV = ctod("  /  /  ")
      .w_ORAPRV = space(2)
      .w_MINPRV = space(2)
      .w_OFDATDOC = ctod("  /  /  ")
      .w_OFCODNOM = space(15)
      .w_TIPENT = space(1)
      .w_TIPOENTE = space(1)
      .w_ATOREEFF = 0
      .w_ATMINEFF = 0
      .w_DataCambiata = .f.
      .w_GIOPRM = space(10)
      .w_CACHKNOM = space(1)
      .w_COMODO = space(10)
      .w_ATTIPCLI = space(1)
      .w_CCDESPIA = space(40)
      .w_VisTracciab = space(1)
      .w_MATETIPOL = space(1)
      .w_TIPNOM = space(1)
      .w_DATOBSONOM = ctod("  /  /  ")
      .w_CAMINPRE = 0
      .w_CAORASYS = space(1)
      .w_PACHKCAR = space(1)
      .w_PACHKFES = space(1)
      .w_FLGCOM = space(1)
      .w_FLANAL = space(1)
      .w_CENOBSO = ctod("  /  /  ")
      .w_FLDANA = space(1)
      .w_ATCODVAL = space(3)
      .w_ATCODLIS = space(5)
      .w_DESVAL = space(35)
      .w_PARASS = 0
      .w_FLAMPA = space(1)
      .w_VALLIS = space(3)
      .w_ATSTATUS = space(1)
      .w_ATSTATUS = space(1)
      .w_ATFLVALO = space(1)
      .w_ATIMPORT = 0
      .w_ATCALDIR = space(1)
      .w_ATCOECAL = 0
      .w_despra = space(100)
      .w_codpra = space(15)
      .w_datpre = ctod("  /  /  ")
      .w_TipoRiso = space(1)
      .w_PARAME = space(2)
      .w_ROWORI = 0
      .w_RIFCON = space(10)
      .w_SERIAL = space(10)
      .w_PRESTA = space(10)
      .w_MVDATREG = ctod("  /  /  ")
      .w_ROWNUM = 0
      .w_SERTRA = space(10)
      .w_RIGA = 0
      .w_SERIAL2 = space(10)
      .w_despra = space(100)
      .w_codpra = space(15)
      .w_datpre = ctod("  /  /  ")
      .w_ATRIFMOV = space(10)
      .w_CODCLI = space(15)
      .w_PAFLVISI = space(1)
      .w_LOC_PRA = space(30)
      .w_LOC_NOM = space(30)
      .w_PRZVAC = space(1)
      .w_SCOLIS = space(1)
      .w_AGG_ATT_ORIGINE = space(1)
      .w_DATINI_ORIG = ctod("  /  /  ")
      .w_ORAINI_ORIG = space(2)
      .w_MININI_ORIG = space(2)
      .w_DECUNI = 0
      .w_DECTOT = 0
      .w_CALCPICU = 0
      .w_CALCPICT = 0
      .w_OLDSTATUS = space(1)
      .w_ASERIAL = space(20)
      .w_SERPIAN = space(20)
      .w_FLINTE = space(1)
      .w_FLPRAT = space(1)
      .w_GESRIS = space(1)
      .w_CICLO = space(1)
      .w_INILIS = ctod("  /  /  ")
      .w_FINLIS = ctod("  /  /  ")
      .w_IVALIS = space(1)
      .w_QUALIS = space(1)
      .w_FLSCOR = space(1)
      .w_VOCECR = space(1)
      .w_NUMLIS = space(5)
      .w_IVACLI = space(1)
      .w_ATTPROSC = space(5)
      .w_ATTPROLI = space(5)
      .w_ATTSCLIS = space(5)
      .w_ATTCOLIS = space(5)
      .w_ATCENRIC = space(15)
      .w_ATCOMRIC = space(15)
      .w_ATATTRIC = space(15)
      .w_ATATTCOS = space(15)
      .w_CAUACQ = space(5)
      .w_KEYLISTB = space(10)
      .w_FLGLIS = space(1)
      .w_NULLA = space(1)
      .w_LISACQ = space(5)
      .w_ATLISACQ = space(5)
      .w_OKGEN = .f.
      .w_ATTARTEM = space(1)
      .w_ATTARCON = 0
      .w_CODLIN = space(3)
      .w_ATFLVALO1 = space(1)
      .w_CHKPRE = space(1)
      .w_ActByTimer = space(1)
      .w_CONTRACN = space(5)
      .w_TIPCONCO = space(1)
      .w_LISCOLCO = space(5)
      .w_STATUSCO = space(1)
      .w_FORZADTI = .f.
      .w_FORZADTF = .f.
      .w_CAFLSTAT = space(1)
      .w_FLCCRAUTO = space(1)
      .w_SCOLIC = space(1)
      .w_FLGCON = space(1)
      .w_ATFLRICO = space(1)
      .w_AggiornaRicorr = .f.
      .w_OLDDATINI = ctot("")
      .w_OLDDATFIN = ctot("")
      .w_HASEVENT = .f.
      .w_HASEVCOP = space(50)
      .w_HASEVENT = .f.
      .w_HASEVCOP = space(50)
      .w_PRIEML = 0
      .w_COLPOS = 0
      .w_IDRICH = space(15)
      .w_ATSEREVE = space(10)
      .w_TIPEVE = space(10)
      .w_NEWID = .f.
      .w_TIPDIR = space(1)
      .w_CAUABB = space(5)
      .w_PCAUABB = space(5)
      .w_RESET = .f.
      .w_TIMER = .f.
      .w_TMRDELTA = 0
      .w_DATALIST = ctod("  /  /  ")
      .w_INILIS2 = ctod("  /  /  ")
      .w_FINLIS2 = ctod("  /  /  ")
      .w_FINLIS3 = ctod("  /  /  ")
      .w_INILIS3 = ctod("  /  /  ")
      .w_FINLIS4 = ctod("  /  /  ")
      .w_INILIS4 = ctod("  /  /  ")
      .w_FLGCIC = space(1)
      .w_FLGCIC2 = space(1)
      .w_FLGCIC3 = space(1)
      .w_FLGCIC4 = space(1)
      .w_TIPLIS4 = space(1)
      .w_TIPLIS3 = space(1)
      .w_TIPLIS = space(1)
      .w_TIPLIS2 = space(1)
      .w_FLSTAT = space(1)
      .w_F2STAT = space(1)
      .w_F3STAT = space(1)
      .w_F4STAT = space(1)
      .w_DesNomin = space(60)
      .w_Pers_Descri = space(60)
      .w_Pers_Telefono = space(18)
      .w_Pers_Cellulare = space(18)
      .w_Pers_E_Mail = space(254)
      .w_Pers_E_Mpec = space(254)
      .w_ControlloPers = space(5)
      .w_PACHKATT = 0
      .w_FlagPosiz = space(1)
      .w_NOMSG = space(1)
      .w_ATLISACQ = space(5)
      .w_INIACQ = ctod("  /  /  ")
      .w_FINACQ = ctod("  /  /  ")
      .w_IVAACQ = space(1)
      .w_VALACQ = space(3)
      .w_FLSCOAC = space(1)
      .w_DESLIS = space(40)
      .w_BUFLANAL = space(1)
      .w_FLCCRAUTC = space(1)
      .w_OLD_ATSTATUS = space(1)
      .w_OLD_ATTIPRIS = space(1)
      .w_OLD_ATCODPRA = space(15)
      .w_MODAPP = space(1)
      .w_SELRAGG = space(10)
      .w_COMODO = space(10)
      .w_GiudicePrat = space(100)
      .w_CAUCON = space(5)
      .w_FLCOSE = space(1)
      .w_NUMSCO = 0
      .w_PAATTRIS = space(20)
      .w_TipPra = space(10)
      .w_MatPra = space(10)
      .w_CATIPPRA = space(10)
      .w_CAMATPRA = space(10)
      .w_CA__ENTE = space(10)
      .w_DTBSO_CAUMATTI = ctod("  /  /  ")
      .w_MailAttivitaRicorrente = space(1)
      .w_ACAPO = space(10)
      .w_GENDAEV = .f.
      .w_CNDATFIN = ctod("  /  /  ")
      .w_NOMSGEN = .f.
      .w_CATCOM = space(3)
      .w_CADTOBSO = ctod("  /  /  ")
      .w_ATINIRIC = ctod("  /  /  ")
      .w_ATFINRIC = ctod("  /  /  ")
      .w_ATINICOS = ctod("  /  /  ")
      .w_ATFINCOS = ctod("  /  /  ")
      .w_FLPRES = space(1)
      .w_CNMATOBB = space(1)
      .w_CNASSCTP = space(1)
      .w_CNCOMPLX = space(1)
      .w_CNPROFMT = space(1)
      .w_CNESIPOS = space(1)
      .w_CNPERPLX = 0
      .w_CNPERPOS = 0
      .w_CNTIPPRA = space(10)
      .w_TIPRIF = space(2)
      .w_CNFLVMLQ = space(1)
      .w_ATCODUID = space(254)
      .w_OLDANNO = space(4)
      .w_OLANDO = space(4)
      .w_OLALDO = space(2)
      if .cFunction<>"Filter"
        .w_ISALT = IsAlt()
        .w_CFUNC = this.cFunction
        .w_READAZI = i_CODAZI
        .w_LetturaParAgen = i_CodAzi
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_LetturaParAgen))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,5,.f.)
        .w_VALATT = .w_ATSERIAL
        .w_TABKEY = 'OFF_ATTI'
        .w_DELIMM = .f.
        .w_TIPOMASK = IIF(VARTYPE(This.w_MaskCalend)='O','C','U')
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_ATCAUATT))
          .link_1_10('Full')
          endif
          .DoRTCalc(11,11,.f.)
        .w_CA_TIMER = ""
        .w_FLACQ = IIF(Not Empty(.w_CAUACQ),Docgesana(.w_CAUACQ,'A'),.w_FLANAL)
        .w_FLACOMAQ = Docgesana(.w_CAUACQ,'C')
          .DoRTCalc(15,17,.f.)
        .w_DATINI = IIF(.w_CFUNC='Load' and .w_TIPOMASK='U' and not .w_FORZADTI,i_datsys,TTOD(.w_ATDATINI))
        .w_ORAINI = PADL(ALLTRIM(STR(HOUR(IIF(.w_CFUNC='Load' and .w_TIPOMASK='U' and not .w_FORZADTI,IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN),cp_CharToDatetime( IIF(NOT EMPTY(.w_ATDATINI), DTOC(.w_ATDATINI)+' '+PADL(ALLTRIM(STR(HOUR(.w_ATDATINI),2)),2,'0')+':'+PADL(ALLTRIM(STR(MINUTE(.w_ATDATINI),2)),2,'0')+':00', '  -  -       :  :  ')))))),2,'0')
        .w_MININI = PADL(ALLTRIM(STR(MINUTE(IIF(.w_CFUNC='Load' and .w_TIPOMASK='U' and not .w_FORZADTI,IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN),cp_CharToDatetime( IIF(NOT EMPTY(.w_ATDATINI), DTOC(.w_ATDATINI)+' '+PADL(ALLTRIM(STR(HOUR(.w_ATDATINI),2)),2,'0')+':'+PADL(ALLTRIM(STR(MINUTE(.w_ATDATINI),2)),2,'0')+':00', '  -  -       :  :  ')))))),2,'0')
          .DoRTCalc(21,21,.f.)
        .w_DATFIN = IIF(.w_CFUNC='Load' and not .w_FORZADTF,.w_DATINI+INT( (VAL(.w_ORAINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)) / 24),TTOD(.w_ATDATFIN))
        .w_ORAFIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load' and not .w_FORZADTF,STR(MOD( (VAL(.w_ORAINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)), 24)),STR(HOUR(.w_ATDATFIN),2))),2)
        .w_ATANNDOC = STR(YEAR(CP_TODATE(.w_DATFIN)), 4, 0)
        .w_MINFIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load' and not .w_FORZADTF,STR(MOD( (VAL(.w_MININI)+.w_ATDURMIN), 60),2),STR(MINUTE(.w_ATDATFIN),2))),2)
        .DoRTCalc(26,26,.f.)
          if not(empty(.w_ATOPPOFF))
          .link_1_27('Full')
          endif
        .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        .w_ATNUMPRI = IIF(EMPTY(.w_ATNUMPRI),1,.w_ATNUMPRI)
        .w_ATPERCOM = ICASE(.w_ATSTATUS $ 'FP' , 100 , .w_ATSTATUS='D' , 0, .w_ATPERCOM)
        .w_ATFLNOTI = 'N'
          .DoRTCalc(32,32,.f.)
        .w_ATFLPROM = 'N'
          .DoRTCalc(34,37,.f.)
        .w_ONUMDOC = .w_ATNUMDOC
          .DoRTCalc(39,39,.f.)
        .w_OALFDOC = .w_ATALFDOC
          .DoRTCalc(41,41,.f.)
        .w_ATDATDOC = IIF(.w_FLNSAP<>'S',.w_DATFIN,cp_Chartodate('  -  -    '))
        .w_ATSTAATT = 1
        .w_ATFLATRI = IIF(EMPTY(NVL(.w_ATFLATRI, ' ')), 'N', .w_ATFLATRI)
        .DoRTCalc(45,45,.f.)
          if not(empty(.w_ATCODATT))
          .link_1_48('Full')
          endif
          .DoRTCalc(46,47,.f.)
        .w_ATOPERAT = i_codute
          .DoRTCalc(49,49,.f.)
        .w_DATRIN = IIF(.w_CFUNC='Load',IIF(.w_ATSTATUS $ 'FPT' AND NOT EMPTY(.w_DATRIN),.w_DATRIN,CTOD('  -  -  ')),TTOD(.w_ATDATRIN))
        .w_ORARIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',IIF(.w_ATSTATUS $ 'FPT' AND NOT EMPTY(.w_ORARIN),.w_ORARIN,'00'),STR(HOUR(.w_ATDATRIN),2))),2)
        .w_MINRIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',IIF(.w_ATSTATUS $ 'FPT' AND NOT EMPTY(.w_MINRIN),.w_MINRIN,'00'),STR(MINUTE(.w_ATDATRIN),2))),2)
        .w_ATTIPRIS = IIF(.w_ATSTATUS $ 'FP',.w_ATTIPRIS,space(1))
        .DoRTCalc(53,53,.f.)
          if not(empty(.w_ATTIPRIS))
          .link_1_56('Full')
          endif
        .DoRTCalc(54,55,.f.)
          if not(empty(.w_ATCAUDOC))
          .link_1_59('Full')
          endif
        .DoRTCalc(56,56,.f.)
          if not(empty(.w_ATCAUACQ))
          .link_1_60('Full')
          endif
        .w_CAUDOC = .w_ATCAUDOC
          .DoRTCalc(58,58,.f.)
        .w_OBTEST = i_datsys
        .w_ATCODPRA = iif(.w_ISALT,this.SalvaPrat,space(15))
        .DoRTCalc(60,60,.f.)
          if not(empty(.w_ATCODPRA))
          .link_1_68('Full')
          endif
        .w_ATCODPRA = iif(.w_ISALT,this.SalvaPrat,space(15))
        .DoRTCalc(61,61,.f.)
          if not(empty(.w_ATCODPRA))
          .link_1_69('Full')
          endif
        .DoRTCalc(62,62,.f.)
          if not(empty(.w_ATCENCOS))
          .link_1_70('Full')
          endif
        .w_ATPUBWEB = 'S'
        .w_STARIC = IIF(.w_newid,'A',.w_STARIC)
        .w_GEN_ATT = 'N'
        .w_ATCODBUN = g_CODBUN
        .DoRTCalc(66,66,.f.)
          if not(empty(.w_ATCODBUN))
          .link_1_76('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .w_GIOINI = IIF(NOT EMPTY(.w_DATINI),LEFT(g_GIORNO[DOW(.w_DATINI)],3), '   ')
        .DoRTCalc(68,69,.f.)
          if not(empty(.w_ATCODNOM))
          .link_1_90('Full')
          endif
        .DoRTCalc(70,71,.f.)
          if not(empty(.w_ATCONTAT))
          .link_1_92('Full')
          endif
          .DoRTCalc(72,72,.f.)
        .w_FlCreaPers = IIF(.cFunction='Load' AND !EMPTY(.w_ATCODNOM) AND EMPTY(.w_ATCONTAT),.w_FlCreaPers,'N')
        .w_NewCodPer = IIF(.cFunction='Load' AND !EMPTY(.w_ATCODNOM) AND EMPTY(.w_ATCONTAT),.w_NewCodPer,'')
        .DoRTCalc(74,74,.f.)
          if not(empty(.w_NewCodPer))
          .link_1_97('Full')
          endif
          .DoRTCalc(75,77,.f.)
        .w_ATLOCALI = IIF(.w_ISALT, .w_LOC_PRA, .w_LOC_NOM)
        .DoRTCalc(79,79,.f.)
          if not(empty(.w_AT__ENTE))
          .link_1_103('Full')
          endif
        .DoRTCalc(80,80,.f.)
          if not(empty(.w_ATUFFICI))
          .link_1_104('Full')
          endif
        .DoRTCalc(81,82,.f.)
          if not(empty(.w_ATCODSED))
          .link_1_106('Full')
          endif
          .DoRTCalc(83,88,.f.)
        .w_ATEVANNO = IIF(EMPTY(.w_IDRICH) AND (.w_CFUNC='Load'  OR empty(.w_ATEVANNO)),CALCESER(.w_DATFIN),.w_ATEVANNO)
        .w_EV__ANNO = .w_ATEVANNO
          .DoRTCalc(91,92,.f.)
        .w_FLNSAP = 'N'
        .w_GIORIN = IIF(EMPTY(.w_ATDATRIN),'',LEFT(g_GIORNO[DOW(.w_ATDATRIN)],3))
          .DoRTCalc(95,95,.f.)
        .w_GIOPRV = IIF(EMPTY(.w_ATDATPRO),'',LEFT(g_GIORNO[DOW(.w_ATDATPRO)],3))
        .w_RESCHK = 0
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_144.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_145.Calculate()
          .DoRTCalc(98,98,.f.)
        .w_GIOFIN = IIF(NOT EMPTY(.w_DATFIN),LEFT(g_GIORNO[DOW(.w_DATFIN)],3), '   ')
        .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
          .DoRTCalc(100,108,.f.)
        .w_ATNUMGIO = IIF(.cFunction='Load' AND !EMPTY(.w_ATCAITER), INT((cp_CharToDatetime(DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00' ) - cp_CharToDatetime(DTOC(.w_DATINI_ORIG)+' '+.w_ORAINI_ORIG+':'+.w_MININI_ORIG+':00' )) / (3600 * 24)), .w_ATNUMGIO)
        .oPgFrm.Page1.oPag.oObj_1_168.Calculate()
        .w_USCITA = 'N'
        .w_DATINI_CHANGED = 'N'
        .oPgFrm.Page1.oPag.oObj_1_171.Calculate()
        .DoRTCalc(112,113,.f.)
          if not(empty(.w_ATRIFDOC))
          .link_1_173('Full')
          endif
        .w_MVSERIAL = .w_ATRIFDOC
          .DoRTCalc(115,130,.f.)
        .w_LEGIND = 'S'
        .DoRTCalc(132,135,.f.)
          if not(empty(.w_ENTE))
          .link_1_199('Full')
          endif
          .DoRTCalc(136,138,.f.)
        .w_DATPRV = IIF(.w_CFUNC='Load',CTOD('  -  -  '),TTOD(.w_ATDATPRO))
        .w_ORAPRV = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',.w_ORAPRV,STR(HOUR(.w_ATDATPRO),2))),2)
        .w_MINPRV = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',.w_MINPRV,STR(MINUTE(.w_ATDATPRO),2))),2)
        .oPgFrm.Page1.oPag.oObj_1_212.Calculate()
        .w_OFDATDOC = i_datsys
        .w_OFCODNOM = .w_ATCODNOM
          .DoRTCalc(144,144,.f.)
        .w_TIPOENTE = IIF(EMPTY(.w_TIPENT),.w_PRENTTIP,.w_TIPENT)
          .DoRTCalc(146,147,.f.)
        .w_DataCambiata = IIF(VARTYPE(.w_MaskCalend)='O',.T.,.F.)
        .oPgFrm.Page1.oPag.oObj_1_244.Calculate()
          .DoRTCalc(149,161,.f.)
        .w_FLGCOM = Docgesana(.w_ATCAUDOC,'C')
          .DoRTCalc(163,164,.f.)
        .w_FLDANA = Docgesana(.w_ATCAUDOC,'A')
        .w_ATCODVAL = IIF(EMPTY(.w_COVAPR),g_PERVAL,.w_COVAPR)
        .DoRTCalc(166,166,.f.)
          if not(empty(.w_ATCODVAL))
          .link_2_2('Full')
          endif
        .w_ATCODLIS = ICASE(! EMPTY(.w_NUMLIS),.w_NUMLIS,! EMPTY(.w_COLIPR),.w_COLIPR, ! Empty(.w_NCODLIS),.w_NCODLIS,.w_CODLIS)
        .DoRTCalc(167,167,.f.)
          if not(empty(.w_ATCODLIS))
          .link_2_3('Full')
          endif
          .DoRTCalc(168,171,.f.)
        .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
          .DoRTCalc(174,177,.f.)
        .w_despra = .w_DESCAN
        .w_codpra = .w_ATCODPRA
        .w_datpre = .w_DATINI
        .w_TipoRiso = IIF(.w_ISALT,'P','N')
        .oPgFrm.Page3.oPag.ZoomDocInt.Calculate()
          .DoRTCalc(182,182,.f.)
        .w_ROWORI = Nvl(.w_ZoomDocInt.GetVar('CPROWORD'),0)
        .w_RIFCON = Nvl(.w_ZoomDocInt.GetVar('MVRIFCON'),' ')
        .w_SERIAL = Nvl(.w_ZoomDocInt.GetVar('MVSERIAL'),' ')
        .w_PRESTA = Nvl(.w_ZoomDocInt.GetVar('ARPRESTA'),' ')
        .w_MVDATREG = .w_ZoomDocInt.GetVar('MVDATREG')
        .w_ROWNUM = IIF(.w_ZoomDocInt.GetVar('CPROWNUM')=0,1,.w_ZoomDocInt.GetVar('CPROWNUM'))
        .w_SERTRA = IIF(EMPTY(.w_SERIAL),.w_ATRIFDOC,.w_SERIAL)
        .w_RIGA = .w_ROWNUM
        .oPgFrm.Page3.oPag.ZoomTrRig.Calculate()
        .w_SERIAL2 = .w_ZoomTrRig.getVar('MVSERIAL')
        .oPgFrm.Page3.oPag.oObj_3_14.Calculate()
        .w_despra = .w_DESCAN
        .w_codpra = .w_ATCODPRA
        .w_datpre = .w_DATINI
        .DoRTCalc(195,195,.f.)
          if not(empty(.w_ATRIFMOV))
          .link_2_31('Full')
          endif
        .DoRTCalc(196,196,.f.)
          if not(empty(.w_CODCLI))
          .link_1_251('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_255.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_256.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_261.Calculate()
          .DoRTCalc(197,207,.f.)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_OLDSTATUS = .w_ATSTATUS
        .w_ASERIAL = .w_ATSERIAL
        .DoRTCalc(211,211,.f.)
          if not(empty(.w_ASERIAL))
          .link_1_266('Full')
          endif
          .DoRTCalc(212,215,.f.)
        .w_CICLO = 'E'
          .DoRTCalc(217,221,.f.)
        .w_VOCECR = Docgesana(.w_ATCAUACQ,'V')
        .DoRTCalc(223,223,.f.)
          if not(empty(.w_NUMLIS))
          .link_1_274('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_285.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_286.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_287.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_288.Calculate()
          .DoRTCalc(224,232,.f.)
        .w_CAUACQ = .w_ATCAUACQ
        .w_KEYLISTB = SYS(2015)
          .DoRTCalc(235,235,.f.)
        .w_NULLA = 'X'
        .oPgFrm.Page1.oPag.oObj_1_293.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_294.Calculate()
          .DoRTCalc(237,237,.f.)
        .w_ATLISACQ = .w_LISACQ
        .oPgFrm.Page2.oPag.oObj_2_45.Calculate()
        .w_OKGEN = .T.
        .DoRTCalc(240,246,.f.)
          if not(empty(.w_CONTRACN))
          .link_1_304('Full')
          endif
          .DoRTCalc(247,249,.f.)
        .w_FORZADTI = .F.
        .w_FORZADTF = .F.
        .oPgFrm.Page1.oPag.oObj_1_312.Calculate()
          .DoRTCalc(252,252,.f.)
        .w_FLCCRAUTO = Docgesana(.w_ATCAUDOC,'R')
          .DoRTCalc(254,255,.f.)
        .w_ATFLRICO = 'N'
        .w_AggiornaRicorr = .F.
        .w_OLDDATINI = .w_ATDATINI
        .w_OLDDATFIN = .w_ATDATFIN
        .DoRTCalc(260,267,.f.)
          if not(empty(.w_ATSEREVE))
          .link_1_331('Full')
          endif
        .DoRTCalc(268,268,.f.)
          if not(empty(.w_TIPEVE))
          .link_1_333('Full')
          endif
        .w_NEWID = .f.
        .oPgFrm.Page1.oPag.CLOCK.Calculate()
          .DoRTCalc(270,273,.f.)
        .w_TIMER = .F.
        .w_TMRDELTA = 0
          .DoRTCalc(276,302,.f.)
        .w_FlagPosiz = 'N'
        .w_NOMSG = ' '
        .DoRTCalc(305,305,.f.)
          if not(empty(.w_ATLISACQ))
          .link_2_49('Full')
          endif
        .oPgFrm.Page2.oPag.oObj_2_57.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_58.Calculate()
          .DoRTCalc(306,312,.f.)
        .w_FLCCRAUTC = Docgesana(.w_CAUACQ,'R')
        .oPgFrm.Page1.oPag.oObj_1_382.Calculate()
          .DoRTCalc(314,319,.f.)
        .w_GiudicePrat = IIF(.w_ISALT AND !EMPTY(.w_ATCODPRA),GetGiudici(.w_ATCODPRA, '',.w_DATFIN, 100, ''),'')
        .DoRTCalc(321,321,.f.)
          if not(empty(.w_CAUCON))
          .link_1_396('Full')
          endif
          .DoRTCalc(322,329,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_MailAttivitaRicorrente = 'N'
        .w_ACAPO = chr(13)
        .w_GENDAEV = .F.
          .DoRTCalc(334,334,.f.)
        .w_NOMSGEN = .f.
          .DoRTCalc(336,353,.f.)
        .w_OLDANNO = .w_ATANNDOC
        .oPgFrm.Page1.oPag.oObj_1_442.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_443.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_444.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'OFF_ATTI')
    this.DoRTCalc(355,356,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_79.enabled = this.oPgFrm.Page1.oPag.oBtn_1_79.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_109.enabled = this.oPgFrm.Page1.oPag.oBtn_1_109.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_110.enabled = this.oPgFrm.Page1.oPag.oBtn_1_110.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_111.enabled = this.oPgFrm.Page1.oPag.oBtn_1_111.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_113.enabled = this.oPgFrm.Page1.oPag.oBtn_1_113.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_222.enabled = this.oPgFrm.Page1.oPag.oBtn_1_222.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_223.enabled = this.oPgFrm.Page1.oPag.oBtn_1_223.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_11.enabled = this.oPgFrm.Page2.oPag.oBtn_2_11.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_29.enabled = this.oPgFrm.Page2.oPag.oBtn_2_29.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_32.enabled = this.oPgFrm.Page2.oPag.oBtn_2_32.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_43.enabled = this.oPgFrm.Page2.oPag.oBtn_2_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_388.enabled = this.oPgFrm.Page1.oPag.oBtn_1_388.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_389.enabled = this.oPgFrm.Page1.oPag.oBtn_1_389.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_390.enabled = this.oPgFrm.Page1.oPag.oBtn_1_390.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_412.enabled = this.oPgFrm.Page1.oPag.oBtn_1_412.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_441.enabled = this.oPgFrm.Page1.oPag.oBtn_1_441.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsag_aat
    endproc
    
    proc Destroy()
      this.w_ZoomDocInt = .NULL.
      this.w_ZoomTrRig = .NULL.
     DODEFAULT()
     local oForm
     For Each oForm in _Screen.Forms
       if Upper(oForm.class) == 'TGSAG_KCL'
         RAISEEVENT(oForm, "Activate")
         exit
       Endif
     Endfor
     oForm = .null.
    
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEATT","i_codazi,w_ATSERIAL")
      if .w_FLPDOC='D'
        cp_AskTableProg(this,i_nConn,"NUATT","i_codazi,w_ATANNDOC,w_ATALFDOC,w_ATNUMDOC")
      endif
      .op_codazi = .w_codazi
      .op_ATSERIAL = .w_ATSERIAL
      .op_codazi = .w_codazi
      .op_ATANNDOC = .w_ATANNDOC
      .op_ATALFDOC = .w_ATALFDOC
      .op_ATNUMDOC = .w_ATNUMDOC
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oATCAUATT_1_10.enabled = i_bVal
      .Page1.oPag.oATOGGETT_1_15.enabled = i_bVal
      .Page1.oPag.oDATINI_1_18.enabled = i_bVal
      .Page1.oPag.oORAINI_1_20.enabled = i_bVal
      .Page1.oPag.oMININI_1_21.enabled = i_bVal
      .Page1.oPag.oDATFIN_1_23.enabled = i_bVal
      .Page1.oPag.oORAFIN_1_24.enabled = i_bVal
      .Page1.oPag.oMINFIN_1_26.enabled = i_bVal
      .Page1.oPag.oATOPPOFF_1_27.enabled = i_bVal
      .Page1.oPag.oATSTATUS_1_28.enabled = i_bVal
      .Page1.oPag.oATSTATUS_1_29.enabled = i_bVal
      .Page1.oPag.oATNUMPRI_1_30.enabled = i_bVal
      .Page1.oPag.oATPERCOM_1_31.enabled = i_bVal
      .Page1.oPag.oATFLNOTI_1_32.enabled = i_bVal
      .Page1.oPag.oATGGPREA_1_33.enabled = i_bVal
      .Page1.oPag.oATFLPROM_1_34.enabled = i_bVal
      .Page1.oPag.oDATAPRO_1_35.enabled = i_bVal
      .Page1.oPag.oORAPRO_1_36.enabled = i_bVal
      .Page1.oPag.oMINPRO_1_37.enabled = i_bVal
      .Page1.oPag.oATNUMDOC_1_39.enabled = i_bVal
      .Page1.oPag.oATALFDOC_1_41.enabled = i_bVal
      .Page1.oPag.oATNOTPIA_1_43.enabled = i_bVal
      .Page1.oPag.oATDATDOC_1_44.enabled = i_bVal
      .Page1.oPag.oATSTAATT_1_45.enabled = i_bVal
      .Page1.oPag.oATFLATRI_1_46.enabled = i_bVal
      .Page1.oPag.oATCODATT_1_48.enabled = i_bVal
      .Page1.oPag.oATTIPRIS_1_56.enabled = i_bVal
      .Page1.oPag.oATCODPRA_1_68.enabled = i_bVal
      .Page1.oPag.oATCODPRA_1_69.enabled = i_bVal
      .Page1.oPag.oATCENCOS_1_70.enabled = i_bVal
      .Page1.oPag.oATPUBWEB_1_71.enabled = i_bVal
      .Page1.oPag.oSTARIC_1_72.enabled = i_bVal
      .Page1.oPag.oGEN_ATT_1_73.enabled = i_bVal
      .Page1.oPag.oATCODBUN_1_76.enabled = i_bVal
      .Page1.oPag.oATCODNOM_1_90.enabled = i_bVal
      .Page1.oPag.oATCONTAT_1_92.enabled = i_bVal
      .Page1.oPag.oATPERSON_1_95.enabled = i_bVal
      .Page1.oPag.oFlCreaPers_1_96.enabled = i_bVal
      .Page1.oPag.oNewCodPer_1_97.enabled = i_bVal
      .Page1.oPag.oATTELEFO_1_99.enabled = i_bVal
      .Page1.oPag.oATCELLUL_1_100.enabled = i_bVal
      .Page1.oPag.oAT_EMAIL_1_101.enabled = i_bVal
      .Page1.oPag.oATLOCALI_1_102.enabled = i_bVal
      .Page1.oPag.oAT__ENTE_1_103.enabled = i_bVal
      .Page1.oPag.oATUFFICI_1_104.enabled = i_bVal
      .Page1.oPag.oAT_EMPEC_1_105.enabled = i_bVal
      .Page1.oPag.oATCODSED_1_106.enabled = i_bVal
      .Page1.oPag.oDATPRV_1_209.enabled = i_bVal
      .Page1.oPag.oORAPRV_1_210.enabled = i_bVal
      .Page1.oPag.oMINPRV_1_211.enabled = i_bVal
      .Page2.oPag.oATCODLIS_2_3.enabled = i_bVal
      .Page2.oPag.oATSTATUS_2_10.enabled = i_bVal
      .Page2.oPag.oATSTATUS_2_12.enabled = i_bVal
      .Page1.oPag.oCODCLI_1_251.enabled = i_bVal
      .Page1.oPag.oIDRICH_1_330.enabled = i_bVal
      .Page1.oPag.oATSEREVE_1_331.enabled = i_bVal
      .Page1.oPag.oTIPEVE_1_333.enabled = i_bVal
      .Page2.oPag.oATLISACQ_2_49.enabled = i_bVal
      .Page1.oPag.oBtn_1_47.enabled = .Page1.oPag.oBtn_1_47.mCond()
      .Page1.oPag.oBtn_1_79.enabled = .Page1.oPag.oBtn_1_79.mCond()
      .Page1.oPag.oBtn_1_109.enabled = .Page1.oPag.oBtn_1_109.mCond()
      .Page1.oPag.oBtn_1_110.enabled = .Page1.oPag.oBtn_1_110.mCond()
      .Page1.oPag.oBtn_1_111.enabled = .Page1.oPag.oBtn_1_111.mCond()
      .Page1.oPag.oBtn_1_113.enabled = .Page1.oPag.oBtn_1_113.mCond()
      .Page1.oPag.oBtn_1_222.enabled = .Page1.oPag.oBtn_1_222.mCond()
      .Page1.oPag.oBtn_1_223.enabled = .Page1.oPag.oBtn_1_223.mCond()
      .Page2.oPag.oBtn_2_11.enabled = .Page2.oPag.oBtn_2_11.mCond()
      .Page2.oPag.oBtn_2_13.enabled = .Page2.oPag.oBtn_2_13.mCond()
      .Page2.oPag.oBtn_2_29.enabled = .Page2.oPag.oBtn_2_29.mCond()
      .Page2.oPag.oBtn_2_32.enabled = .Page2.oPag.oBtn_2_32.mCond()
      .Page2.oPag.oBtn_2_43.enabled = .Page2.oPag.oBtn_2_43.mCond()
      .Page1.oPag.oBtn_1_327.enabled = i_bVal
      .Page1.oPag.oBtn_1_328.enabled = i_bVal
      .Page1.oPag.oBtn_1_339.enabled = i_bVal
      .Page1.oPag.oBtn_1_388.enabled = .Page1.oPag.oBtn_1_388.mCond()
      .Page1.oPag.oBtn_1_389.enabled = .Page1.oPag.oBtn_1_389.mCond()
      .Page1.oPag.oBtn_1_390.enabled = .Page1.oPag.oBtn_1_390.mCond()
      .Page1.oPag.oBtn_1_395.enabled = i_bVal
      .Page1.oPag.oBtn_1_402.enabled = i_bVal
      .Page3.oPag.oBtn_3_23.enabled = i_bVal
      .Page3.oPag.oBtn_3_24.enabled = i_bVal
      .Page1.oPag.oBtn_1_412.enabled = .Page1.oPag.oBtn_1_412.mCond()
      .Page1.oPag.oBtn_1_435.enabled = i_bVal
      .Page1.oPag.oBtn_1_441.enabled = .Page1.oPag.oBtn_1_441.mCond()
      .Page2.oPag.oBtn_2_62.enabled = i_bVal
      .Page1.oPag.oObj_1_83.enabled = i_bVal
      .Page1.oPag.oObj_1_84.enabled = i_bVal
      .Page1.oPag.oObj_1_142.enabled = i_bVal
      .Page1.oPag.oObj_1_143.enabled = i_bVal
      .Page1.oPag.oObj_1_144.enabled = i_bVal
      .Page1.oPag.oObj_1_145.enabled = i_bVal
      .Page1.oPag.oObj_1_151.enabled = i_bVal
      .Page1.oPag.oObj_1_168.enabled = i_bVal
      .Page1.oPag.oObj_1_171.enabled = i_bVal
      .Page1.oPag.oObj_1_212.enabled = i_bVal
      .Page1.oPag.oObj_1_244.enabled = i_bVal
      .Page2.oPag.oObj_2_14.enabled = i_bVal
      .Page3.oPag.oObj_3_14.enabled = i_bVal
      .Page1.oPag.oObj_1_255.enabled = i_bVal
      .Page1.oPag.oObj_1_256.enabled = i_bVal
      .Page1.oPag.oObj_1_261.enabled = i_bVal
      .Page1.oPag.oObj_1_285.enabled = i_bVal
      .Page1.oPag.oObj_1_286.enabled = i_bVal
      .Page1.oPag.oObj_1_287.enabled = i_bVal
      .Page1.oPag.oObj_1_288.enabled = i_bVal
      .Page1.oPag.oObj_1_293.enabled = i_bVal
      .Page1.oPag.oObj_1_294.enabled = i_bVal
      .Page2.oPag.oObj_2_45.enabled = i_bVal
      .Page1.oPag.oObj_1_312.enabled = i_bVal
      .Page1.oPag.CLOCK.enabled = i_bVal
      .Page2.oPag.oObj_2_57.enabled = i_bVal
      .Page2.oPag.oObj_2_58.enabled = i_bVal
      .Page1.oPag.oObj_1_382.enabled = i_bVal
      .Page1.oPag.oObj_1_442.enabled = i_bVal
      .Page1.oPag.oObj_1_443.enabled = i_bVal
      .Page1.oPag.oObj_1_444.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oATCAUATT_1_10.enabled = .t.
        .Page1.oPag.oATOGGETT_1_15.enabled = .t.
      endif
    endwith
    this.GSAG_MPA.SetStatus(i_cOp)
    this.GSAG_MCP.SetStatus(i_cOp)
    this.GSAG_MDA.SetStatus(i_cOp)
    this.gsag_mdd.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'OFF_ATTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAG_MPA.SetChildrenStatus(i_cOp)
  *  this.GSAG_MCP.SetChildrenStatus(i_cOp)
  *  this.GSAG_MDA.SetChildrenStatus(i_cOp)
  *  this.gsag_mdd.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSERIAL,"ATSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCAUATT,"ATCAUATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATOGGETT,"ATOGGETT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATINI,"ATDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATFIN,"ATDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATANNDOC,"ATANNDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATOPPOFF,"ATOPPOFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSTATUS,"ATSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSTATUS,"ATSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNUMPRI,"ATNUMPRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPERCOM,"ATPERCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATFLNOTI,"ATFLNOTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATGGPREA,"ATGGPREA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATFLPROM,"ATFLPROM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNUMDOC,"ATNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATALFDOC,"ATALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNOTPIA,"ATNOTPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATDOC,"ATDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSTAATT,"ATSTAATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATFLATRI,"ATFLATRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODATT,"ATCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPROMEM,"ATPROMEM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATOPERAT,"ATOPERAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATRIN,"ATDATRIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTIPRIS,"ATTIPRIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT_ESITO,"AT_ESITO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCAUDOC,"ATCAUDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCAUACQ,"ATCAUACQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODESI,"ATCODESI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODPRA,"ATCODPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODPRA,"ATCODPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCENCOS,"ATCENCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPUBWEB,"ATPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODBUN,"ATCODBUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATPRO,"ATDATPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODNOM,"ATCODNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCONTAT,"ATCONTAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATPERSON,"ATPERSON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTELEFO,"ATTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCELLUL,"ATCELLUL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT_EMAIL,"AT_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATLOCALI,"ATLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT__ENTE,"AT__ENTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATUFFICI,"ATUFFICI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT_EMPEC,"AT_EMPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODSED,"ATCODSED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCAITER,"ATCAITER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATKEYOUT,"ATKEYOUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATEVANNO,"ATEVANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATDATOUT,"ATDATOUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATRIFSER,"ATRIFSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATNUMGIO,"ATNUMGIO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATRIFDOC,"ATRIFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AT___FAX,"AT___FAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATOREEFF,"ATOREEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATMINEFF,"ATMINEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODVAL,"ATCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODLIS,"ATCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSTATUS,"ATSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSTATUS,"ATSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATRIFMOV,"ATRIFMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTPROSC,"ATTPROSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTPROLI,"ATTPROLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTSCLIS,"ATTSCLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATTCOLIS,"ATTCOLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCENRIC,"ATCENRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCOMRIC,"ATCOMRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATATTRIC,"ATATTRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATATTCOS,"ATATTCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATLISACQ,"ATLISACQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATFLRICO,"ATFLRICO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATSEREVE,"ATSEREVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATLISACQ,"ATLISACQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATINIRIC,"ATINIRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATFINRIC,"ATFINRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATINICOS,"ATINICOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATFINCOS,"ATFINCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ATCODUID,"ATCODUID",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsag_aat
    i_cORDERBY="order by  ATOGGETT"
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
    i_lTable = "OFF_ATTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.OFF_ATTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        GSAG_BAD(this,"P")
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.OFF_ATTI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEATT","i_codazi,w_ATSERIAL")
        if .w_FLPDOC='D'
          cp_NextTableProg(this,i_nConn,"NUATT","i_codazi,w_ATANNDOC,w_ATALFDOC,w_ATNUMDOC")
        endif
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- gsag_aat
      THIS.w_ATCODUID=THIS.w_ATSERIAL
      * --- Fine Area Manuale
      *
      * insert into OFF_ATTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'OFF_ATTI')
        i_extval=cp_InsertValODBCExtFlds(this,'OFF_ATTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ATSERIAL,ATCAUATT,ATOGGETT,ATDATINI,ATDATFIN"+;
                  ",ATANNDOC,ATOPPOFF,ATSTATUS,ATNUMPRI,ATPERCOM"+;
                  ",ATFLNOTI,ATGGPREA,ATFLPROM,ATNUMDOC,ATALFDOC"+;
                  ",ATNOTPIA,ATDATDOC,ATSTAATT,ATFLATRI,ATCODATT"+;
                  ",ATPROMEM,ATOPERAT,ATDATRIN,ATTIPRIS,AT_ESITO"+;
                  ",ATCAUDOC,ATCAUACQ,ATCODESI,ATCODPRA,ATCENCOS"+;
                  ",ATPUBWEB,ATCODBUN,ATDATPRO,ATCODNOM,ATCONTAT"+;
                  ",ATPERSON,ATTELEFO,ATCELLUL,AT_EMAIL,ATLOCALI"+;
                  ",AT__ENTE,ATUFFICI,AT_EMPEC,ATCODSED,UTDC"+;
                  ",UTCV,UTDV,ATCAITER,ATKEYOUT,ATEVANNO"+;
                  ",ATDATOUT,UTCC,ATRIFSER,ATNUMGIO,ATRIFDOC"+;
                  ",AT___FAX,ATOREEFF,ATMINEFF,ATCODVAL,ATCODLIS"+;
                  ",ATRIFMOV,ATTPROSC,ATTPROLI,ATTSCLIS,ATTCOLIS"+;
                  ",ATCENRIC,ATCOMRIC,ATATTRIC,ATATTCOS,ATLISACQ"+;
                  ",ATFLRICO,ATSEREVE,ATINIRIC,ATFINRIC,ATINICOS"+;
                  ",ATFINCOS,ATCODUID "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ATSERIAL)+;
                  ","+cp_ToStrODBCNull(this.w_ATCAUATT)+;
                  ","+cp_ToStrODBC(this.w_ATOGGETT)+;
                  ","+cp_ToStrODBC(this.w_ATDATINI)+;
                  ","+cp_ToStrODBC(this.w_ATDATFIN)+;
                  ","+cp_ToStrODBC(this.w_ATANNDOC)+;
                  ","+cp_ToStrODBCNull(this.w_ATOPPOFF)+;
                  ","+cp_ToStrODBC(this.w_ATSTATUS)+;
                  ","+cp_ToStrODBC(this.w_ATNUMPRI)+;
                  ","+cp_ToStrODBC(this.w_ATPERCOM)+;
                  ","+cp_ToStrODBC(this.w_ATFLNOTI)+;
                  ","+cp_ToStrODBC(this.w_ATGGPREA)+;
                  ","+cp_ToStrODBC(this.w_ATFLPROM)+;
                  ","+cp_ToStrODBC(this.w_ATNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_ATALFDOC)+;
                  ","+cp_ToStrODBC(this.w_ATNOTPIA)+;
                  ","+cp_ToStrODBC(this.w_ATDATDOC)+;
                  ","+cp_ToStrODBC(this.w_ATSTAATT)+;
                  ","+cp_ToStrODBC(this.w_ATFLATRI)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODATT)+;
                  ","+cp_ToStrODBC(this.w_ATPROMEM)+;
                  ","+cp_ToStrODBC(this.w_ATOPERAT)+;
                  ","+cp_ToStrODBC(this.w_ATDATRIN)+;
                  ","+cp_ToStrODBCNull(this.w_ATTIPRIS)+;
                  ","+cp_ToStrODBC(this.w_AT_ESITO)+;
                  ","+cp_ToStrODBCNull(this.w_ATCAUDOC)+;
                  ","+cp_ToStrODBCNull(this.w_ATCAUACQ)+;
                  ","+cp_ToStrODBC(this.w_ATCODESI)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODPRA)+;
                  ","+cp_ToStrODBCNull(this.w_ATCENCOS)+;
                  ","+cp_ToStrODBC(this.w_ATPUBWEB)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODBUN)+;
                  ","+cp_ToStrODBC(this.w_ATDATPRO)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODNOM)+;
                  ","+cp_ToStrODBCNull(this.w_ATCONTAT)+;
                  ","+cp_ToStrODBC(this.w_ATPERSON)+;
                  ","+cp_ToStrODBC(this.w_ATTELEFO)+;
                  ","+cp_ToStrODBC(this.w_ATCELLUL)+;
                  ","+cp_ToStrODBC(this.w_AT_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_ATLOCALI)+;
                  ","+cp_ToStrODBCNull(this.w_AT__ENTE)+;
                  ","+cp_ToStrODBCNull(this.w_ATUFFICI)+;
                  ","+cp_ToStrODBC(this.w_AT_EMPEC)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODSED)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_ATCAITER)+;
                  ","+cp_ToStrODBC(this.w_ATKEYOUT)+;
                  ","+cp_ToStrODBC(this.w_ATEVANNO)+;
                  ","+cp_ToStrODBC(this.w_ATDATOUT)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_ATRIFSER)+;
                  ","+cp_ToStrODBC(this.w_ATNUMGIO)+;
                  ","+cp_ToStrODBCNull(this.w_ATRIFDOC)+;
                  ","+cp_ToStrODBC(this.w_AT___FAX)+;
                  ","+cp_ToStrODBC(this.w_ATOREEFF)+;
                  ","+cp_ToStrODBC(this.w_ATMINEFF)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODVAL)+;
                  ","+cp_ToStrODBCNull(this.w_ATCODLIS)+;
                  ","+cp_ToStrODBCNull(this.w_ATRIFMOV)+;
                  ","+cp_ToStrODBC(this.w_ATTPROSC)+;
                  ","+cp_ToStrODBC(this.w_ATTPROLI)+;
                  ","+cp_ToStrODBC(this.w_ATTSCLIS)+;
                  ","+cp_ToStrODBC(this.w_ATTCOLIS)+;
                  ","+cp_ToStrODBC(this.w_ATCENRIC)+;
                  ","+cp_ToStrODBC(this.w_ATCOMRIC)+;
                  ","+cp_ToStrODBC(this.w_ATATTRIC)+;
                  ","+cp_ToStrODBC(this.w_ATATTCOS)+;
                  ","+cp_ToStrODBC(this.w_ATLISACQ)+;
                  ","+cp_ToStrODBC(this.w_ATFLRICO)+;
                  ","+cp_ToStrODBCNull(this.w_ATSEREVE)+;
                  ","+cp_ToStrODBC(this.w_ATINIRIC)+;
                  ","+cp_ToStrODBC(this.w_ATFINRIC)+;
                  ","+cp_ToStrODBC(this.w_ATINICOS)+;
                  ","+cp_ToStrODBC(this.w_ATFINCOS)+;
                  ","+cp_ToStrODBC(this.w_ATCODUID)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'OFF_ATTI')
        i_extval=cp_InsertValVFPExtFlds(this,'OFF_ATTI')
        cp_CheckDeletedKey(i_cTable,0,'ATSERIAL',this.w_ATSERIAL)
        INSERT INTO (i_cTable);
              (ATSERIAL,ATCAUATT,ATOGGETT,ATDATINI,ATDATFIN,ATANNDOC,ATOPPOFF,ATSTATUS,ATNUMPRI,ATPERCOM,ATFLNOTI,ATGGPREA,ATFLPROM,ATNUMDOC,ATALFDOC,ATNOTPIA,ATDATDOC,ATSTAATT,ATFLATRI,ATCODATT,ATPROMEM,ATOPERAT,ATDATRIN,ATTIPRIS,AT_ESITO,ATCAUDOC,ATCAUACQ,ATCODESI,ATCODPRA,ATCENCOS,ATPUBWEB,ATCODBUN,ATDATPRO,ATCODNOM,ATCONTAT,ATPERSON,ATTELEFO,ATCELLUL,AT_EMAIL,ATLOCALI,AT__ENTE,ATUFFICI,AT_EMPEC,ATCODSED,UTDC,UTCV,UTDV,ATCAITER,ATKEYOUT,ATEVANNO,ATDATOUT,UTCC,ATRIFSER,ATNUMGIO,ATRIFDOC,AT___FAX,ATOREEFF,ATMINEFF,ATCODVAL,ATCODLIS,ATRIFMOV,ATTPROSC,ATTPROLI,ATTSCLIS,ATTCOLIS,ATCENRIC,ATCOMRIC,ATATTRIC,ATATTCOS,ATLISACQ,ATFLRICO,ATSEREVE,ATINIRIC,ATFINRIC,ATINICOS,ATFINCOS,ATCODUID  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ATSERIAL;
                  ,this.w_ATCAUATT;
                  ,this.w_ATOGGETT;
                  ,this.w_ATDATINI;
                  ,this.w_ATDATFIN;
                  ,this.w_ATANNDOC;
                  ,this.w_ATOPPOFF;
                  ,this.w_ATSTATUS;
                  ,this.w_ATNUMPRI;
                  ,this.w_ATPERCOM;
                  ,this.w_ATFLNOTI;
                  ,this.w_ATGGPREA;
                  ,this.w_ATFLPROM;
                  ,this.w_ATNUMDOC;
                  ,this.w_ATALFDOC;
                  ,this.w_ATNOTPIA;
                  ,this.w_ATDATDOC;
                  ,this.w_ATSTAATT;
                  ,this.w_ATFLATRI;
                  ,this.w_ATCODATT;
                  ,this.w_ATPROMEM;
                  ,this.w_ATOPERAT;
                  ,this.w_ATDATRIN;
                  ,this.w_ATTIPRIS;
                  ,this.w_AT_ESITO;
                  ,this.w_ATCAUDOC;
                  ,this.w_ATCAUACQ;
                  ,this.w_ATCODESI;
                  ,this.w_ATCODPRA;
                  ,this.w_ATCENCOS;
                  ,this.w_ATPUBWEB;
                  ,this.w_ATCODBUN;
                  ,this.w_ATDATPRO;
                  ,this.w_ATCODNOM;
                  ,this.w_ATCONTAT;
                  ,this.w_ATPERSON;
                  ,this.w_ATTELEFO;
                  ,this.w_ATCELLUL;
                  ,this.w_AT_EMAIL;
                  ,this.w_ATLOCALI;
                  ,this.w_AT__ENTE;
                  ,this.w_ATUFFICI;
                  ,this.w_AT_EMPEC;
                  ,this.w_ATCODSED;
                  ,this.w_UTDC;
                  ,this.w_UTCV;
                  ,this.w_UTDV;
                  ,this.w_ATCAITER;
                  ,this.w_ATKEYOUT;
                  ,this.w_ATEVANNO;
                  ,this.w_ATDATOUT;
                  ,this.w_UTCC;
                  ,this.w_ATRIFSER;
                  ,this.w_ATNUMGIO;
                  ,this.w_ATRIFDOC;
                  ,this.w_AT___FAX;
                  ,this.w_ATOREEFF;
                  ,this.w_ATMINEFF;
                  ,this.w_ATCODVAL;
                  ,this.w_ATCODLIS;
                  ,this.w_ATRIFMOV;
                  ,this.w_ATTPROSC;
                  ,this.w_ATTPROLI;
                  ,this.w_ATTSCLIS;
                  ,this.w_ATTCOLIS;
                  ,this.w_ATCENRIC;
                  ,this.w_ATCOMRIC;
                  ,this.w_ATATTRIC;
                  ,this.w_ATATTCOS;
                  ,this.w_ATLISACQ;
                  ,this.w_ATFLRICO;
                  ,this.w_ATSEREVE;
                  ,this.w_ATINIRIC;
                  ,this.w_ATFINRIC;
                  ,this.w_ATINICOS;
                  ,this.w_ATFINCOS;
                  ,this.w_ATCODUID;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.OFF_ATTI_IDX,i_nConn)
      *
      * update OFF_ATTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'OFF_ATTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ATCAUATT="+cp_ToStrODBCNull(this.w_ATCAUATT)+;
             ",ATOGGETT="+cp_ToStrODBC(this.w_ATOGGETT)+;
             ",ATDATINI="+cp_ToStrODBC(this.w_ATDATINI)+;
             ",ATDATFIN="+cp_ToStrODBC(this.w_ATDATFIN)+;
             ",ATANNDOC="+cp_ToStrODBC(this.w_ATANNDOC)+;
             ",ATOPPOFF="+cp_ToStrODBCNull(this.w_ATOPPOFF)+;
             ",ATSTATUS="+cp_ToStrODBC(this.w_ATSTATUS)+;
             ",ATNUMPRI="+cp_ToStrODBC(this.w_ATNUMPRI)+;
             ",ATPERCOM="+cp_ToStrODBC(this.w_ATPERCOM)+;
             ",ATFLNOTI="+cp_ToStrODBC(this.w_ATFLNOTI)+;
             ",ATGGPREA="+cp_ToStrODBC(this.w_ATGGPREA)+;
             ",ATFLPROM="+cp_ToStrODBC(this.w_ATFLPROM)+;
             ",ATNUMDOC="+cp_ToStrODBC(this.w_ATNUMDOC)+;
             ",ATALFDOC="+cp_ToStrODBC(this.w_ATALFDOC)+;
             ",ATNOTPIA="+cp_ToStrODBC(this.w_ATNOTPIA)+;
             ",ATDATDOC="+cp_ToStrODBC(this.w_ATDATDOC)+;
             ",ATSTAATT="+cp_ToStrODBC(this.w_ATSTAATT)+;
             ",ATFLATRI="+cp_ToStrODBC(this.w_ATFLATRI)+;
             ",ATCODATT="+cp_ToStrODBCNull(this.w_ATCODATT)+;
             ",ATPROMEM="+cp_ToStrODBC(this.w_ATPROMEM)+;
             ",ATOPERAT="+cp_ToStrODBC(this.w_ATOPERAT)+;
             ",ATDATRIN="+cp_ToStrODBC(this.w_ATDATRIN)+;
             ",ATTIPRIS="+cp_ToStrODBCNull(this.w_ATTIPRIS)+;
             ",AT_ESITO="+cp_ToStrODBC(this.w_AT_ESITO)+;
             ",ATCAUDOC="+cp_ToStrODBCNull(this.w_ATCAUDOC)+;
             ",ATCAUACQ="+cp_ToStrODBCNull(this.w_ATCAUACQ)+;
             ",ATCODESI="+cp_ToStrODBC(this.w_ATCODESI)+;
             ",ATCODPRA="+cp_ToStrODBCNull(this.w_ATCODPRA)+;
             ",ATCENCOS="+cp_ToStrODBCNull(this.w_ATCENCOS)+;
             ",ATPUBWEB="+cp_ToStrODBC(this.w_ATPUBWEB)+;
             ",ATCODBUN="+cp_ToStrODBCNull(this.w_ATCODBUN)+;
             ",ATDATPRO="+cp_ToStrODBC(this.w_ATDATPRO)+;
             ",ATCODNOM="+cp_ToStrODBCNull(this.w_ATCODNOM)+;
             ",ATCONTAT="+cp_ToStrODBCNull(this.w_ATCONTAT)+;
             ",ATPERSON="+cp_ToStrODBC(this.w_ATPERSON)+;
             ",ATTELEFO="+cp_ToStrODBC(this.w_ATTELEFO)+;
             ",ATCELLUL="+cp_ToStrODBC(this.w_ATCELLUL)+;
             ",AT_EMAIL="+cp_ToStrODBC(this.w_AT_EMAIL)+;
             ",ATLOCALI="+cp_ToStrODBC(this.w_ATLOCALI)+;
             ",AT__ENTE="+cp_ToStrODBCNull(this.w_AT__ENTE)+;
             ",ATUFFICI="+cp_ToStrODBCNull(this.w_ATUFFICI)+;
             ",AT_EMPEC="+cp_ToStrODBC(this.w_AT_EMPEC)+;
             ",ATCODSED="+cp_ToStrODBCNull(this.w_ATCODSED)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",ATCAITER="+cp_ToStrODBC(this.w_ATCAITER)+;
             ",ATKEYOUT="+cp_ToStrODBC(this.w_ATKEYOUT)+;
             ",ATEVANNO="+cp_ToStrODBC(this.w_ATEVANNO)+;
             ",ATDATOUT="+cp_ToStrODBC(this.w_ATDATOUT)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",ATRIFSER="+cp_ToStrODBC(this.w_ATRIFSER)+;
             ",ATNUMGIO="+cp_ToStrODBC(this.w_ATNUMGIO)+;
             ",ATRIFDOC="+cp_ToStrODBCNull(this.w_ATRIFDOC)+;
             ",AT___FAX="+cp_ToStrODBC(this.w_AT___FAX)+;
             ",ATOREEFF="+cp_ToStrODBC(this.w_ATOREEFF)+;
             ",ATMINEFF="+cp_ToStrODBC(this.w_ATMINEFF)+;
             ",ATCODVAL="+cp_ToStrODBCNull(this.w_ATCODVAL)+;
             ",ATCODLIS="+cp_ToStrODBCNull(this.w_ATCODLIS)+;
             ",ATRIFMOV="+cp_ToStrODBCNull(this.w_ATRIFMOV)+;
             ",ATTPROSC="+cp_ToStrODBC(this.w_ATTPROSC)+;
             ",ATTPROLI="+cp_ToStrODBC(this.w_ATTPROLI)+;
             ",ATTSCLIS="+cp_ToStrODBC(this.w_ATTSCLIS)+;
             ",ATTCOLIS="+cp_ToStrODBC(this.w_ATTCOLIS)+;
             ",ATCENRIC="+cp_ToStrODBC(this.w_ATCENRIC)+;
             ",ATCOMRIC="+cp_ToStrODBC(this.w_ATCOMRIC)+;
             ",ATATTRIC="+cp_ToStrODBC(this.w_ATATTRIC)+;
             ",ATATTCOS="+cp_ToStrODBC(this.w_ATATTCOS)+;
             ",ATLISACQ="+cp_ToStrODBC(this.w_ATLISACQ)+;
             ",ATFLRICO="+cp_ToStrODBC(this.w_ATFLRICO)+;
             ",ATSEREVE="+cp_ToStrODBCNull(this.w_ATSEREVE)+;
             ",ATINIRIC="+cp_ToStrODBC(this.w_ATINIRIC)+;
             ",ATFINRIC="+cp_ToStrODBC(this.w_ATFINRIC)+;
             ",ATINICOS="+cp_ToStrODBC(this.w_ATINICOS)+;
             ",ATFINCOS="+cp_ToStrODBC(this.w_ATFINCOS)+;
             ",ATCODUID="+cp_ToStrODBC(this.w_ATCODUID)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'OFF_ATTI')
        i_cWhere = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
        UPDATE (i_cTable) SET;
              ATCAUATT=this.w_ATCAUATT;
             ,ATOGGETT=this.w_ATOGGETT;
             ,ATDATINI=this.w_ATDATINI;
             ,ATDATFIN=this.w_ATDATFIN;
             ,ATANNDOC=this.w_ATANNDOC;
             ,ATOPPOFF=this.w_ATOPPOFF;
             ,ATSTATUS=this.w_ATSTATUS;
             ,ATNUMPRI=this.w_ATNUMPRI;
             ,ATPERCOM=this.w_ATPERCOM;
             ,ATFLNOTI=this.w_ATFLNOTI;
             ,ATGGPREA=this.w_ATGGPREA;
             ,ATFLPROM=this.w_ATFLPROM;
             ,ATNUMDOC=this.w_ATNUMDOC;
             ,ATALFDOC=this.w_ATALFDOC;
             ,ATNOTPIA=this.w_ATNOTPIA;
             ,ATDATDOC=this.w_ATDATDOC;
             ,ATSTAATT=this.w_ATSTAATT;
             ,ATFLATRI=this.w_ATFLATRI;
             ,ATCODATT=this.w_ATCODATT;
             ,ATPROMEM=this.w_ATPROMEM;
             ,ATOPERAT=this.w_ATOPERAT;
             ,ATDATRIN=this.w_ATDATRIN;
             ,ATTIPRIS=this.w_ATTIPRIS;
             ,AT_ESITO=this.w_AT_ESITO;
             ,ATCAUDOC=this.w_ATCAUDOC;
             ,ATCAUACQ=this.w_ATCAUACQ;
             ,ATCODESI=this.w_ATCODESI;
             ,ATCODPRA=this.w_ATCODPRA;
             ,ATCENCOS=this.w_ATCENCOS;
             ,ATPUBWEB=this.w_ATPUBWEB;
             ,ATCODBUN=this.w_ATCODBUN;
             ,ATDATPRO=this.w_ATDATPRO;
             ,ATCODNOM=this.w_ATCODNOM;
             ,ATCONTAT=this.w_ATCONTAT;
             ,ATPERSON=this.w_ATPERSON;
             ,ATTELEFO=this.w_ATTELEFO;
             ,ATCELLUL=this.w_ATCELLUL;
             ,AT_EMAIL=this.w_AT_EMAIL;
             ,ATLOCALI=this.w_ATLOCALI;
             ,AT__ENTE=this.w_AT__ENTE;
             ,ATUFFICI=this.w_ATUFFICI;
             ,AT_EMPEC=this.w_AT_EMPEC;
             ,ATCODSED=this.w_ATCODSED;
             ,UTDC=this.w_UTDC;
             ,UTCV=this.w_UTCV;
             ,UTDV=this.w_UTDV;
             ,ATCAITER=this.w_ATCAITER;
             ,ATKEYOUT=this.w_ATKEYOUT;
             ,ATEVANNO=this.w_ATEVANNO;
             ,ATDATOUT=this.w_ATDATOUT;
             ,UTCC=this.w_UTCC;
             ,ATRIFSER=this.w_ATRIFSER;
             ,ATNUMGIO=this.w_ATNUMGIO;
             ,ATRIFDOC=this.w_ATRIFDOC;
             ,AT___FAX=this.w_AT___FAX;
             ,ATOREEFF=this.w_ATOREEFF;
             ,ATMINEFF=this.w_ATMINEFF;
             ,ATCODVAL=this.w_ATCODVAL;
             ,ATCODLIS=this.w_ATCODLIS;
             ,ATRIFMOV=this.w_ATRIFMOV;
             ,ATTPROSC=this.w_ATTPROSC;
             ,ATTPROLI=this.w_ATTPROLI;
             ,ATTSCLIS=this.w_ATTSCLIS;
             ,ATTCOLIS=this.w_ATTCOLIS;
             ,ATCENRIC=this.w_ATCENRIC;
             ,ATCOMRIC=this.w_ATCOMRIC;
             ,ATATTRIC=this.w_ATATTRIC;
             ,ATATTCOS=this.w_ATATTCOS;
             ,ATLISACQ=this.w_ATLISACQ;
             ,ATFLRICO=this.w_ATFLRICO;
             ,ATSEREVE=this.w_ATSEREVE;
             ,ATINIRIC=this.w_ATINIRIC;
             ,ATFINRIC=this.w_ATFINRIC;
             ,ATINICOS=this.w_ATINICOS;
             ,ATFINCOS=this.w_ATFINCOS;
             ,ATCODUID=this.w_ATCODUID;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAG_MPA : Saving
      this.GSAG_MPA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATSERIAL,"PASERIAL";
             )
      this.GSAG_MPA.mReplace()
      * --- GSAG_MCP : Saving
      this.GSAG_MCP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATSERIAL,"CASERIAL";
             )
      this.GSAG_MCP.mReplace()
      * --- GSAG_MDA : Saving
      this.GSAG_MDA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATSERIAL,"DASERIAL";
             )
      this.GSAG_MDA.mReplace()
      * --- gsag_mdd : Saving
      this.gsag_mdd.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ATSERIAL,"ADSERIAL";
             )
      this.gsag_mdd.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsag_aat
    * -- Assegno Propiet� per generazione attivit�  alla conferma
    this.save_atserial=this.w_ATSERIAL
    if this.w_FLPDOC='L' and this.w_ATNUMDOC<>0
       this.op_ATNUMDOC=0
       local i_Conn
       i_Conn=i_TableProp[this.OFF_ATTI_IDX, 3]
       cp_NextTableProg(this, i_Conn,  "NUATT", "i_codazi,w_ATANNDOC,w_ATALFDOC,w_ATNUMDOC")
    ENDIF
    
    if this.cFunction='Edit' and this.w_OALFDOC<>THIS.w_ATALFDOC
       this.notifyevent('Aggioprog')
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- gsag_aat
    this.NotifyEvent('Before Delete start')
    * --- Fine Area Manuale
    * --- GSAG_MPA : Deleting
    this.GSAG_MPA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATSERIAL,"PASERIAL";
           )
    this.GSAG_MPA.mDelete()
    * --- GSAG_MCP : Deleting
    this.GSAG_MCP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATSERIAL,"CASERIAL";
           )
    this.GSAG_MCP.mDelete()
    * --- GSAG_MDA : Deleting
    this.GSAG_MDA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATSERIAL,"DASERIAL";
           )
    this.GSAG_MDA.mDelete()
    * --- gsag_mdd : Deleting
    this.gsag_mdd.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ATSERIAL,"ADSERIAL";
           )
    this.gsag_mdd.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.OFF_ATTI_IDX,i_nConn)
      *
      * delete OFF_ATTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ATSERIAL',this.w_ATSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsag_aat
    if this.cFunction='Query'
       this.notifyevent('Delaggioprog')
    endif
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OFF_ATTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ATTI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_CFUNC = this.cFunction
            .w_READAZI = i_CODAZI
        if .o_LetturaParAgen<>.w_LetturaParAgen
            .w_LetturaParAgen = i_CodAzi
          .link_1_4('Full')
        endif
        .DoRTCalc(5,8,.t.)
            .w_TIPOMASK = IIF(VARTYPE(This.w_MaskCalend)='O','C','U')
        if .o_ATCAUATT<>.w_ATCAUATT
          .link_1_10('Full')
        endif
        .DoRTCalc(11,12,.t.)
            .w_FLACQ = IIF(Not Empty(.w_CAUACQ),Docgesana(.w_CAUACQ,'A'),.w_FLANAL)
            .w_FLACOMAQ = Docgesana(.w_CAUACQ,'C')
        .DoRTCalc(15,17,.t.)
        if .o_ATDATINI<>.w_ATDATINI
            .w_DATINI = IIF(.w_CFUNC='Load' and .w_TIPOMASK='U' and not .w_FORZADTI,i_datsys,TTOD(.w_ATDATINI))
        endif
        if .o_DATINI<>.w_DATINI
          .Calculate_DEZAGPQRKH()
        endif
        if .o_CADTIN<>.w_CADTIN.or. .o_ATDATINI<>.w_ATDATINI
            .w_ORAINI = PADL(ALLTRIM(STR(HOUR(IIF(.w_CFUNC='Load' and .w_TIPOMASK='U' and not .w_FORZADTI,IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN),cp_CharToDatetime( IIF(NOT EMPTY(.w_ATDATINI), DTOC(.w_ATDATINI)+' '+PADL(ALLTRIM(STR(HOUR(.w_ATDATINI),2)),2,'0')+':'+PADL(ALLTRIM(STR(MINUTE(.w_ATDATINI),2)),2,'0')+':00', '  -  -       :  :  ')))))),2,'0')
        endif
        if .o_CADTIN<>.w_CADTIN.or. .o_ATDATINI<>.w_ATDATINI
            .w_MININI = PADL(ALLTRIM(STR(MINUTE(IIF(.w_CFUNC='Load' and .w_TIPOMASK='U' and not .w_FORZADTI,IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN),cp_CharToDatetime( IIF(NOT EMPTY(.w_ATDATINI), DTOC(.w_ATDATINI)+' '+PADL(ALLTRIM(STR(HOUR(.w_ATDATINI),2)),2,'0')+':'+PADL(ALLTRIM(STR(MINUTE(.w_ATDATINI),2)),2,'0')+':00', '  -  -       :  :  ')))))),2,'0')
        endif
        .DoRTCalc(21,21,.t.)
        if .o_ATDATFIN<>.w_ATDATFIN.or. .o_ATCAUATT<>.w_ATCAUATT.or. .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI.or. .o_DATINI<>.w_DATINI
            .w_DATFIN = IIF(.w_CFUNC='Load' and not .w_FORZADTF,.w_DATINI+INT( (VAL(.w_ORAINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)) / 24),TTOD(.w_ATDATFIN))
        endif
        if .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI.or. .o_ATDURORE<>.w_ATDURORE.or. .o_ATDURMIN<>.w_ATDURMIN.or. .o_ATDATFIN<>.w_ATDATFIN.or. .o_ATCAUATT<>.w_ATCAUATT
            .w_ORAFIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load' and not .w_FORZADTF,STR(MOD( (VAL(.w_ORAINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)), 24)),STR(HOUR(.w_ATDATFIN),2))),2)
        endif
            .w_ATANNDOC = STR(YEAR(CP_TODATE(.w_DATFIN)), 4, 0)
        if .o_MININI<>.w_MININI.or. .o_ATDURMIN<>.w_ATDURMIN.or. .o_ATDATFIN<>.w_ATDATFIN.or. .o_ATCAUATT<>.w_ATCAUATT
            .w_MINFIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load' and not .w_FORZADTF,STR(MOD( (VAL(.w_MININI)+.w_ATDURMIN), 60),2),STR(MINUTE(.w_ATDATFIN),2))),2)
        endif
        .DoRTCalc(26,26,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT.or. .o_ATPERCOM<>.w_ATPERCOM
            .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        endif
        if .o_ATCAUATT<>.w_ATCAUATT.or. .o_ATPERCOM<>.w_ATPERCOM
            .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        endif
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_ATNUMPRI = IIF(EMPTY(.w_ATNUMPRI),1,.w_ATNUMPRI)
        endif
        if .o_ATSTATUS<>.w_ATSTATUS
            .w_ATPERCOM = ICASE(.w_ATSTATUS $ 'FP' , 100 , .w_ATSTATUS='D' , 0, .w_ATPERCOM)
        endif
        .DoRTCalc(31,37,.t.)
        if .o_ATSERIAL<>.w_ATSERIAL
            .w_ONUMDOC = .w_ATNUMDOC
        endif
        .DoRTCalc(39,39,.t.)
        if .o_ATSERIAL<>.w_ATSERIAL
            .w_OALFDOC = .w_ATALFDOC
        endif
        .DoRTCalc(41,41,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT.or. .o_DATFIN<>.w_DATFIN
            .w_ATDATDOC = IIF(.w_FLNSAP<>'S',.w_DATFIN,cp_Chartodate('  -  -    '))
        endif
        .DoRTCalc(43,43,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_ATFLATRI = IIF(EMPTY(NVL(.w_ATFLATRI, ' ')), 'N', .w_ATFLATRI)
        endif
        if .o_ATCAUATT<>.w_ATCAUATT.or. .o_ATCODATT<>.w_ATCODATT
          .link_1_48('Full')
        endif
        .DoRTCalc(46,49,.t.)
        if .o_ATDATRIN<>.w_ATDATRIN.or. .o_ATSTATUS<>.w_ATSTATUS
            .w_DATRIN = IIF(.w_CFUNC='Load',IIF(.w_ATSTATUS $ 'FPT' AND NOT EMPTY(.w_DATRIN),.w_DATRIN,CTOD('  -  -  ')),TTOD(.w_ATDATRIN))
        endif
        if .o_ATDATRIN<>.w_ATDATRIN.or. .o_ATSTATUS<>.w_ATSTATUS
            .w_ORARIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',IIF(.w_ATSTATUS $ 'FPT' AND NOT EMPTY(.w_ORARIN),.w_ORARIN,'00'),STR(HOUR(.w_ATDATRIN),2))),2)
        endif
        if .o_ATDATRIN<>.w_ATDATRIN.or. .o_ATSTATUS<>.w_ATSTATUS
            .w_MINRIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',IIF(.w_ATSTATUS $ 'FPT' AND NOT EMPTY(.w_MINRIN),.w_MINRIN,'00'),STR(MINUTE(.w_ATDATRIN),2))),2)
        endif
        if .o_ATSTATUS<>.w_ATSTATUS
            .w_ATTIPRIS = IIF(.w_ATSTATUS $ 'FP',.w_ATTIPRIS,space(1))
          .link_1_56('Full')
        endif
        .DoRTCalc(54,54,.t.)
          .link_1_59('Full')
          .link_1_60('Full')
        if .o_ATCAUDOC<>.w_ATCAUDOC
            .w_CAUDOC = .w_ATCAUDOC
        endif
        if .o_ORAINI<>.w_ORAINI
          .Calculate_SLCYSAMDOK()
        endif
        if .o_MININI<>.w_MININI
          .Calculate_XLTWHZKGZW()
        endif
        .DoRTCalc(58,63,.t.)
        if .o_NEWID<>.w_NEWID
            .w_STARIC = IIF(.w_newid,'A',.w_STARIC)
        endif
        if .o_MINPRO<>.w_MINPRO
          .Calculate_TTQFMXGTWH()
        endif
        if .o_ORAPRO<>.w_ORAPRO
          .Calculate_EMHXGAZDWG()
        endif
        if  .o_PAFLVISI<>.w_PAFLVISI.or. .o_DATFIN<>.w_DATFIN
          .WriteTo_GSAG_MPA()
        endif
        if .o_MINFIN<>.w_MINFIN
          .Calculate_LTYWYERFMH()
        endif
        if .o_ORAFIN<>.w_ORAFIN
          .Calculate_FKSPWJFZRZ()
        endif
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .DoRTCalc(65,66,.t.)
        if .o_DATINI<>.w_DATINI
            .w_GIOINI = IIF(NOT EMPTY(.w_DATINI),LEFT(g_GIORNO[DOW(.w_DATINI)],3), '   ')
        endif
        if .o_ATCONTAT<>.w_ATCONTAT
          .Calculate_HGNKUUFFOF()
        endif
        if .o_ATCODNOM<>.w_ATCODNOM
          .Calculate_AYUDKZDNID()
        endif
        .DoRTCalc(68,72,.t.)
        if .o_ATCODNOM<>.w_ATCODNOM.or. .o_ATCONTAT<>.w_ATCONTAT
            .w_FlCreaPers = IIF(.cFunction='Load' AND !EMPTY(.w_ATCODNOM) AND EMPTY(.w_ATCONTAT),.w_FlCreaPers,'N')
        endif
        if .o_ATCODNOM<>.w_ATCODNOM.or. .o_ATCONTAT<>.w_ATCONTAT
            .w_NewCodPer = IIF(.cFunction='Load' AND !EMPTY(.w_ATCODNOM) AND EMPTY(.w_ATCONTAT),.w_NewCodPer,'')
          .link_1_97('Full')
        endif
        .DoRTCalc(75,77,.t.)
        if .o_ATCODPRA<>.w_ATCODPRA.or. .o_ATCODNOM<>.w_ATCODNOM
            .w_ATLOCALI = IIF(.w_ISALT, .w_LOC_PRA, .w_LOC_NOM)
        endif
        if  .o_CODCLI<>.w_CODCLI.or. .o_DATINI<>.w_DATINI.or. .o_ATCODNOM<>.w_ATCODNOM.or. .o_ATCODSED<>.w_ATCODSED.or. .o_DesNomin<>.w_DesNomin.or. .o_NOMDES<>.w_NOMDES.or. .o_DATFIN<>.w_DATFIN
          .WriteTo_GSAG_MCP()
        endif
        .DoRTCalc(79,88,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_ATEVANNO = IIF(EMPTY(.w_IDRICH) AND (.w_CFUNC='Load'  OR empty(.w_ATEVANNO)),CALCESER(.w_DATFIN),.w_ATEVANNO)
        endif
        if .o_ATSERIAL<>.w_ATSERIAL.or. .o_ATEVANNO<>.w_ATEVANNO
            .w_EV__ANNO = .w_ATEVANNO
        endif
        if .o_ORARIN<>.w_ORARIN
          .Calculate_HZCHXLWSAF()
        endif
        if .o_MINRIN<>.w_MINRIN
          .Calculate_TSBTAGYSWL()
        endif
        if .o_DATRIN<>.w_DATRIN.or. .o_ORARIN<>.w_ORARIN.or. .o_MINRIN<>.w_MINRIN.or. .o_ATSTATUS<>.w_ATSTATUS
          .Calculate_JQEWOCXQFP()
        endif
        .DoRTCalc(91,93,.t.)
        if .o_ATDATRIN<>.w_ATDATRIN
            .w_GIORIN = IIF(EMPTY(.w_ATDATRIN),'',LEFT(g_GIORNO[DOW(.w_ATDATRIN)],3))
        endif
        if .o_ORAPRV<>.w_ORAPRV
          .Calculate_ICLGAQFJZG()
        endif
        if .o_MINPRV<>.w_MINPRV
          .Calculate_AZZPKRFRYF()
        endif
        if .o_DATPRV<>.w_DATPRV.or. .o_ORAPRV<>.w_ORAPRV.or. .o_MINPRV<>.w_MINPRV
          .Calculate_BIPWALJCNG()
        endif
        .DoRTCalc(95,95,.t.)
        if .o_ATDATPRO<>.w_ATDATPRO
            .w_GIOPRV = IIF(EMPTY(.w_ATDATPRO),'',LEFT(g_GIORNO[DOW(.w_ATDATPRO)],3))
        endif
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_144.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_145.Calculate()
        .DoRTCalc(97,98,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_GIOFIN = IIF(NOT EMPTY(.w_DATFIN),LEFT(g_GIORNO[DOW(.w_DATFIN)],3), '   ')
        endif
        .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
        .DoRTCalc(100,108,.t.)
        if .o_DATINI<>.w_DATINI
            .w_ATNUMGIO = IIF(.cFunction='Load' AND !EMPTY(.w_ATCAITER), INT((cp_CharToDatetime(DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00' ) - cp_CharToDatetime(DTOC(.w_DATINI_ORIG)+' '+.w_ORAINI_ORIG+':'+.w_MININI_ORIG+':00' )) / (3600 * 24)), .w_ATNUMGIO)
        endif
        .oPgFrm.Page1.oPag.oObj_1_168.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_171.Calculate()
        .DoRTCalc(110,112,.t.)
          .link_1_173('Full')
            .w_MVSERIAL = .w_ATRIFDOC
        if .o_DATRIN<>.w_DATRIN
          .Calculate_CRGECAJXEX()
        endif
        if .o_DATRIN<>.w_DATRIN
          .Calculate_FSFZWCMJFH()
        endif
        .DoRTCalc(115,134,.t.)
          .link_1_199('Full')
        if .o_ATCODPRA<>.w_ATCODPRA
          .Calculate_DIIVHDQCJN()
        endif
        if .o_CACHKOBB<>.w_CACHKOBB
          .Calculate_UROXKXOOGR()
        endif
        .DoRTCalc(136,138,.t.)
        if .o_ATDATPRO<>.w_ATDATPRO
            .w_DATPRV = IIF(.w_CFUNC='Load',CTOD('  -  -  '),TTOD(.w_ATDATPRO))
        endif
        if .o_ATDATPRO<>.w_ATDATPRO
            .w_ORAPRV = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',.w_ORAPRV,STR(HOUR(.w_ATDATPRO),2))),2)
        endif
        if .o_ATDATPRO<>.w_ATDATPRO
            .w_MINPRV = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load',.w_MINPRV,STR(MINUTE(.w_ATDATPRO),2))),2)
        endif
        .oPgFrm.Page1.oPag.oObj_1_212.Calculate()
        .DoRTCalc(142,142,.t.)
        if .o_ATCODNOM<>.w_ATCODNOM
            .w_OFCODNOM = .w_ATCODNOM
        endif
        .DoRTCalc(144,144,.t.)
        if .o_TIPENT<>.w_TIPENT.or. .o_PRENTTIP<>.w_PRENTTIP
            .w_TIPOENTE = IIF(EMPTY(.w_TIPENT),.w_PRENTTIP,.w_TIPENT)
        endif
        if .o_DATINI<>.w_DATINI.or. .o_DATFIN<>.w_DATFIN.or. .o_ORAINI<>.w_ORAINI.or. .o_ORAFIN<>.w_ORAFIN.or. .o_MININI<>.w_MININI.or. .o_MINFIN<>.w_MINFIN
          .Calculate_VIECHUQVWU()
        endif
        if .o_DATINI<>.w_DATINI.or. .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI.or. .o_ATFLPROM<>.w_ATFLPROM.or. .o_ATCAUATT<>.w_ATCAUATT
          .Calculate_GKYMFCTJZJ()
        endif
        if .o_DATAPRO<>.w_DATAPRO.or. .o_ORAPRO<>.w_ORAPRO.or. .o_MINPRO<>.w_MINPRO
          .Calculate_MYDPUJWBKE()
        endif
        if .o_DATAPRO<>.w_DATAPRO
          .Calculate_SNDQJCYYHT()
        endif
        .oPgFrm.Page1.oPag.oObj_1_244.Calculate()
        .DoRTCalc(146,161,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_FLGCOM = Docgesana(.w_ATCAUDOC,'C')
        endif
        .DoRTCalc(163,164,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_FLDANA = Docgesana(.w_ATCAUDOC,'A')
        endif
        if  .o_TipoRiso<>.w_TipoRiso.or. .o_ATCENCOS<>.w_ATCENCOS.or. .o_ATCODPRA<>.w_ATCODPRA.or. .o_ATCAUATT<>.w_ATCAUATT.or. .o_DATINI<>.w_DATINI.or. .o_ATCENRIC<>.w_ATCENRIC.or. .o_CODCLI<>.w_CODCLI
          .WriteTo_GSAG_MDA()
        endif
        if .o_ATCODPRA<>.w_ATCODPRA
          .link_2_2('Full')
        endif
        if .o_ATCODPRA<>.w_ATCODPRA.or. .o_ATCODNOM<>.w_ATCODNOM
            .w_ATCODLIS = ICASE(! EMPTY(.w_NUMLIS),.w_NUMLIS,! EMPTY(.w_COLIPR),.w_COLIPR, ! Empty(.w_NCODLIS),.w_NCODLIS,.w_CODLIS)
          .link_2_3('Full')
        endif
        .DoRTCalc(168,171,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT.or. .o_ATPERCOM<>.w_ATPERCOM
            .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        endif
        if .o_ATCAUATT<>.w_ATCAUATT.or. .o_ATPERCOM<>.w_ATPERCOM
            .w_ATSTATUS = IIF(EMPTY(.w_ATSTATUS),'D',IIF(.w_ATSTATUS='D',IIF(.w_ATPERCOM=0,'D','I'),.w_ATSTATUS))
        endif
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
        .DoRTCalc(174,177,.t.)
            .w_despra = .w_DESCAN
            .w_codpra = .w_ATCODPRA
            .w_datpre = .w_DATINI
        .oPgFrm.Page3.oPag.ZoomDocInt.Calculate()
        .DoRTCalc(181,182,.t.)
            .w_ROWORI = Nvl(.w_ZoomDocInt.GetVar('CPROWORD'),0)
            .w_RIFCON = Nvl(.w_ZoomDocInt.GetVar('MVRIFCON'),' ')
            .w_SERIAL = Nvl(.w_ZoomDocInt.GetVar('MVSERIAL'),' ')
            .w_PRESTA = Nvl(.w_ZoomDocInt.GetVar('ARPRESTA'),' ')
            .w_MVDATREG = .w_ZoomDocInt.GetVar('MVDATREG')
            .w_ROWNUM = IIF(.w_ZoomDocInt.GetVar('CPROWNUM')=0,1,.w_ZoomDocInt.GetVar('CPROWNUM'))
        if .o_SERIAL<>.w_SERIAL
            .w_SERTRA = IIF(EMPTY(.w_SERIAL),.w_ATRIFDOC,.w_SERIAL)
        endif
        if .o_ROWNUM<>.w_ROWNUM
            .w_RIGA = .w_ROWNUM
        endif
        .oPgFrm.Page3.oPag.ZoomTrRig.Calculate()
            .w_SERIAL2 = .w_ZoomTrRig.getVar('MVSERIAL')
        if .o_ROWNUM<>.w_ROWNUM
        .oPgFrm.Page3.oPag.oObj_3_14.Calculate()
        endif
        if .o_ROWNUM<>.w_ROWNUM
          .Calculate_URCBBSJQVI()
        endif
            .w_despra = .w_DESCAN
            .w_codpra = .w_ATCODPRA
            .w_datpre = .w_DATINI
          .link_2_31('Full')
        .oPgFrm.Page1.oPag.oObj_1_255.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_256.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_261.Calculate()
        .DoRTCalc(196,207,.t.)
        if .o_ATCODVAL<>.w_ATCODVAL
            .w_CALCPICU = DEFPIU(.w_DECUNI)
        endif
        if .o_ATCODVAL<>.w_ATCODVAL
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_ATSERIAL<>.w_ATSERIAL
            .w_OLDSTATUS = .w_ATSTATUS
        endif
        if .o_ATSERIAL<>.w_ATSERIAL
            .w_ASERIAL = .w_ATSERIAL
          .link_1_266('Full')
        endif
        if .o_ATFLATRI<>.w_ATFLATRI
          .Calculate_JZKXQUXVIN()
        endif
        .DoRTCalc(212,221,.t.)
            .w_VOCECR = Docgesana(.w_ATCAUACQ,'V')
          .link_1_274('Full')
        .oPgFrm.Page1.oPag.oObj_1_285.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_286.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_287.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_288.Calculate()
        .DoRTCalc(224,232,.t.)
        if .o_ATCAUACQ<>.w_ATCAUACQ
            .w_CAUACQ = .w_ATCAUACQ
        endif
        .DoRTCalc(234,235,.t.)
        Local l_Dep1,l_Dep2
        l_Dep1= .o_ATTCOLIS<>.w_ATTCOLIS .or. .o_ATTPROLI<>.w_ATTPROLI .or. .o_ATTPROSC<>.w_ATTPROSC .or. .o_ATTSCLIS<>.w_ATTSCLIS .or. .o_ATCODVAL<>.w_ATCODVAL
        l_Dep2= .o_ATCAUATT<>.w_ATCAUATT .or. .o_ATDATDOC<>.w_ATDATDOC .or. .o_ATCODNOM<>.w_ATCODNOM .or. .o_ATCODLIS<>.w_ATCODLIS .or. .o_CA_TIMER<>.w_CA_TIMER
        if m.l_Dep1 .or. m.l_Dep2
            .w_NULLA = 'X'
        endif
        .oPgFrm.Page1.oPag.oObj_1_293.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_294.Calculate()
        if  .o_CODCLI<>.w_CODCLI
          .WriteTo_gsag_mdd()
        endif
        .oPgFrm.Page2.oPag.oObj_2_45.Calculate()
        .DoRTCalc(237,245,.t.)
          .link_1_304('Full')
        .oPgFrm.Page1.oPag.oObj_1_312.Calculate()
        .DoRTCalc(247,252,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_FLCCRAUTO = Docgesana(.w_ATCAUDOC,'R')
        endif
        .DoRTCalc(254,257,.t.)
        if .o_ATSERIAL<>.w_ATSERIAL
            .w_OLDDATINI = .w_ATDATINI
        endif
        if .o_ATSERIAL<>.w_ATSERIAL
            .w_OLDDATFIN = .w_ATDATFIN
        endif
        .DoRTCalc(260,267,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
          .link_1_333('Full')
        endif
        .oPgFrm.Page1.oPag.CLOCK.Calculate()
        if .o_CA_TIMER<>.w_CA_TIMER
          .Calculate_NQIEQWIEPI()
        endif
        if .o_ATCAUATT<>.w_ATCAUATT
          .Calculate_CAKWIAAGCU()
        endif
        if .o_ATCAUATT<>.w_ATCAUATT
          .Calculate_JVRGHRXZTE()
        endif
        if .o_DATFIN<>.w_DATFIN.or. .o_ORAFIN<>.w_ORAFIN.or. .o_MINFIN<>.w_MINFIN
          .Calculate_DYCXZYIKIF()
        endif
        .oPgFrm.Page2.oPag.oObj_2_57.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_58.Calculate()
        .DoRTCalc(269,312,.t.)
        if .o_ATCAUATT<>.w_ATCAUATT
            .w_FLCCRAUTC = Docgesana(.w_CAUACQ,'R')
        endif
        .oPgFrm.Page1.oPag.oObj_1_382.Calculate()
        .DoRTCalc(314,319,.t.)
        if .o_ATCODPRA<>.w_ATCODPRA.or. .o_ATDATFIN<>.w_ATDATFIN.or. .o_DATFIN<>.w_DATFIN.or. .o_ATCAUATT<>.w_ATCAUATT
            .w_GiudicePrat = IIF(.w_ISALT AND !EMPTY(.w_ATCODPRA),GetGiudici(.w_ATCODPRA, '',.w_DATFIN, 100, ''),'')
        endif
          .link_1_396('Full')
        .DoRTCalc(322,330,.t.)
            .w_MailAttivitaRicorrente = 'N'
        .DoRTCalc(332,353,.t.)
        if .o_ATSERIAL<>.w_ATSERIAL
            .w_OLDANNO = .w_ATANNDOC
        endif
        .oPgFrm.Page1.oPag.oObj_1_442.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_443.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_444.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        this.Calculate_QYQODPLTRV()
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEATT","i_codazi,w_ATSERIAL")
          .op_ATSERIAL = .w_ATSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_ATANNDOC<>.w_ATANNDOC .or. .op_ATALFDOC<>.w_ATALFDOC
          if .w_FLPDOC='D'
             cp_AskTableProg(this,i_nConn,"NUATT","i_codazi,w_ATANNDOC,w_ATALFDOC,w_ATNUMDOC")
          endif
          .op_ATNUMDOC = .w_ATNUMDOC
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_ATANNDOC = .w_ATANNDOC
        .op_ATALFDOC = .w_ATALFDOC
      endwith
      this.DoRTCalc(355,356,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_84.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_142.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_143.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_144.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_145.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_168.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_171.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_212.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_244.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
        .oPgFrm.Page3.oPag.ZoomDocInt.Calculate()
        .oPgFrm.Page3.oPag.ZoomTrRig.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_255.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_256.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_261.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_285.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_286.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_287.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_288.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_293.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_294.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_312.Calculate()
        .oPgFrm.Page1.oPag.CLOCK.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_57.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_58.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_382.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_442.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_443.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_444.Calculate()
    endwith
  return

  proc Calculate_DEZAGPQRKH()
    with this
          * --- Resetto w_datini_changed
          .w_DATINI_CHANGED = 'S'
    endwith
  endproc
  proc Calculate_SLCYSAMDOK()
    with this
          * --- Resetto w_oraini
          .w_ORAINI = .ZeroFill(.w_ORAINI)
    endwith
  endproc
  proc Calculate_XLTWHZKGZW()
    with this
          * --- Resetto w_minini
          .w_MININI = .ZeroFill(.w_MININI)
    endwith
  endproc
  proc Calculate_TTQFMXGTWH()
    with this
          * --- Resetto w_minpro
          .w_MINPRO = .ZeroFill(.w_MINPRO)
    endwith
  endproc
  proc Calculate_EMHXGAZDWG()
    with this
          * --- Resetto w_orapro
          .w_ORAPRO = .ZeroFill(.w_ORAPRO)
    endwith
  endproc
  proc Calculate_ZQLIBXAFGH()
    with this
          * --- Init var. promemoria
          .w_DATAPRO = IIF(.w_ATFLPROM='S' AND !EMPTY(NVL(.w_ATPROMEM,cp_CharToDate("  -  -    "))),TTOD(.w_ATPROMEM),cp_CharToDate("  -  -    "))
          .w_ORAPRO = IIF(.w_ATFLPROM='S' AND !EMPTY(.w_ATPROMEM),PADL(ALLTRIM(STR(HOUR(.w_ATPROMEM))),2,'0'),SPACE(2))
          .w_MINPRO = IIF(.w_ATFLPROM='S' AND !EMPTY(.w_ATPROMEM),PADL(ALLTRIM(STR(MINUTE(.w_ATPROMEM))),2,'0'),SPACE(2))
          .w_GIOPRM = IIF(.w_ATFLPROM='S' AND !EMPTY(.w_ATPROMEM),LEFT(g_GIORNO[DOW(.w_ATPROMEM)],3),SPACE(3))
    endwith
  endproc
  proc Calculate_LTYWYERFMH()
    with this
          * --- Resetto w_minfin
          .w_MINFIN = .ZeroFill(.w_MINFIN)
    endwith
  endproc
  proc Calculate_FKSPWJFZRZ()
    with this
          * --- Resetto w_orafin
          .w_ORAFIN = .ZeroFill(.w_ORAFIN)
    endwith
  endproc
  proc Calculate_HGNKUUFFOF()
    with this
          * --- Avvalora con dati persona di riferimento
          .w_ATPERSON = EVL(left(.w_Pers_Descri,60),.w_ATPERSON)
          .w_AT_EMAIL = EVL(.w_Pers_E_Mail,.w_AT_EMAIL)
          .w_AT_EMPEC = EVL(.w_Pers_E_Mpec,.w_AT_EMPEC)
          .w_ATTELEFO = EVL(left(.w_Pers_Telefono,18),.w_ATTELEFO)
          .w_ATCELLUL = EVL(left(.w_Pers_Cellulare,18),.w_ATCELLUL)
    endwith
  endproc
  proc Calculate_AYUDKZDNID()
    with this
          * --- Avvalora con dati persona di riferimento
          .w_ATPERSON = ''
          .w_ATCONTAT = ''
    endwith
  endproc
  proc Calculate_CFBUOHVOXL()
    with this
          * --- Avvalora la data per Outlook
          .w_ATDATOUT = DATETIME()
    endwith
  endproc
  proc Calculate_HZCHXLWSAF()
    with this
          * --- Resetto w_orarin
          .w_ORARIN = .ZeroFill(.w_ORARIN)
    endwith
  endproc
  proc Calculate_TSBTAGYSWL()
    with this
          * --- Resetto w_minrin
          .w_MINRIN = .ZeroFill(.w_MINRIN)
    endwith
  endproc
  proc Calculate_JQEWOCXQFP()
    with this
          * --- Setta w_atdatrin
          .w_ATDATRIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATRIN) AND .w_ATSTATUS $ 'FP',  DTOC(.w_DATRIN)+' '+.w_ORARIN+':'+.w_MINRIN+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_ICLGAQFJZG()
    with this
          * --- Resetto w_oraprv
          .w_ORAPRV = .ZeroFill(.w_ORAPRV)
    endwith
  endproc
  proc Calculate_AZZPKRFRYF()
    with this
          * --- Resetto w_minprv
          .w_MINPRV = .ZeroFill(.w_MINPRV)
    endwith
  endproc
  proc Calculate_BIPWALJCNG()
    with this
          * --- Setta w_atdatpro
          .w_ATDATPRO = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATPRV),  DTOC(.w_DATPRV)+' '+.w_ORAPRV+':'+.w_MINPRV+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_WMFTZGPBJG()
    with this
          * --- Calcoli e controlli a checkform
          .w_RESET = IIF(.w_TIMER AND .w_CA_TIMER="S" And .cFunction='Load', .NotifyEvent("Clock") , .F.)
          .w_ATDATINI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  '))
          .o_ATDATINI = .w_ATDATINI
          .w_ATDATFIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI), IIF(NOT EMPTY(.w_DATFIN),DTOC(.w_DATFIN),DTOC(.w_DATINI) ) +' '+.w_ORAFIN+':'+.w_MINFIN+':00', '  -  -       :  :  ') )
          .w_ORAFIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load' and not .w_FORZADTF and .w_ORAFIN='00' ,STR(MOD( (VAL(.w_ORAINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)), 24)),STR(HOUR(.w_ATDATFIN),2))),2)
          .w_MINFIN = RIGHT('00'+ALLTRIM(IIF(.w_CFUNC='Load' and not .w_FORZADTF and .w_MINFIN='00',STR(MOD( (VAL(.w_MININI)+.w_ATDURMIN), 60),2),STR(MINUTE(.w_ATDATFIN),2))),2)
          .o_ATDATFIN = .w_ATDATFIN
          gsag_bco(this;
              ,'A';
             )
    endwith
  endproc
  proc Calculate_AZCWEXDNXN()
    with this
          * --- Default partecipanti
          gsag_bap(this;
              ,'8';
             )
    endwith
  endproc
  proc Calculate_VSEQIYWONJ()
    with this
          * --- Notifica al calendario di aggiornarsi
          gsag_bap(this;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_JAESMIQPKQ()
    with this
          * --- F10: aggiornacalend=.T.
          gsag_bap(this;
              ,'T';
             )
    endwith
  endproc
  proc Calculate_CRGECAJXEX()
    with this
          * --- Svuota w_attipris
          .w_ATTIPRIS = iif(!empty(.w_DATRIN), space(1), .w_ATTIPRIS)
    endwith
  endproc
  proc Calculate_FSFZWCMJFH()
    with this
          * --- Resetto ora e min rinvio
          .w_ORARIN = iif(empty(.w_DATRIN), '00', .w_ORARIN)
          .w_MINRIN = iif(empty(.w_DATRIN), '00', .w_MINRIN)
    endwith
  endproc
  proc Calculate_DIIVHDQCJN()
    with this
          * --- Aggiorna la var. SalvaPrat
          .SalvaPrat = .w_ATCODPRA
    endwith
  endproc
  proc Calculate_UROXKXOOGR()
    with this
          * --- Aggiorna la var. w_ATCODPRA + GSAG_BAP
          .w_ATCODPRA = IIF(ISALT() AND NOT EMPTY(.w_ATCAUATT) AND EMPTY(.w_CACHKOBB), SPACE(15), .w_ATCODPRA)
          .link_1_68('Full')
          .link_1_69('Full')
          gsag_bap(this;
              ,('B');
             )
    endwith
  endproc
  proc Calculate_VIECHUQVWU()
    with this
          * --- Memorizza che deve inviare la mail
          .w_DataCambiata = .T.
    endwith
  endproc
  proc Calculate_WNOMGDHFOQ()
    with this
          * --- Invia la mail al salvataggio
          gsag_bap(this;
              ,'M';
              ,.w_ATSERIAL;
              ,iif(.w_ATFLRICO='S',IIF(.w_MailAttivitaRicorrente='S',.f.,.t.),.f.);
             )
          .w_MailAttivitaRicorrente = 'N'
    endwith
  endproc
  proc Calculate_MJGUYPNTDH()
    with this
          * --- Invia la mail alla cancellazione
          gsag_bap(this;
              ,'D';
              ,.w_ATSERIAL;
             )
    endwith
  endproc
  proc Calculate_HNGDUYESPH()
    with this
          * --- Invia la mail alla creazione
          gsag_bap(this;
              ,'C';
              ,.w_ATSERIAL;
             )
    endwith
  endproc
  proc Calculate_VEWFNFWPRR()
    with this
          * --- Gestione cambio check Avviso
          .w_DataCambiata = IIF(.w_ATFLNOTI='S',.T.,.F.)
    endwith
  endproc
  proc Calculate_GKYMFCTJZJ()
    with this
          * --- Setta var. promemoria
          .w_ATPROMEM = IIF(.w_ATFLPROM='S' ,cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  '))-IIF(.w_CAMINPRE>0,.w_CAMINPRE*60,0),DTOT(cp_CharToDate("  -  -    ")))
          .w_DATAPRO = IIF(.w_ATFLPROM='S',TTOD(.w_ATPROMEM),cp_CharToDate("  -  -    "))
          .w_ORAPRO = IIF(.w_ATFLPROM='S',PADL(ALLTRIM(STR(HOUR(.w_ATPROMEM))),2,'0'),'')
          .w_MINPRO = IIF(.w_ATFLPROM='S',PADL(ALLTRIM(STR(MINUTE(.w_ATPROMEM))),2,'0'),'')
          .w_GIOPRM = IIF(.w_ATFLPROM='S' AND NOT EMPTY(.w_ATPROMEM),LEFT(g_GIORNO[DOW(.w_ATPROMEM)],3),'')
    endwith
  endproc
  proc Calculate_MYDPUJWBKE()
    with this
          * --- Setta w_atpromem
          .w_ATPROMEM = cp_CharToDatetime( IIF(.w_ATFLPROM='S' AND NOT EMPTY(.w_DATAPRO) ,  DTOC(.w_DATAPRO)+' '+.w_ORAPRO+':'+.w_MINPRO+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_SNDQJCYYHT()
    with this
          * --- Aggiorna w_GIOPRM
          .w_GIOPRM = IIF(NOT EMPTY(.w_DATAPRO), LEFT(g_GIORNO[DOW(.w_DATAPRO)],3), SPACE(3))
    endwith
  endproc
  proc Calculate_URCBBSJQVI()
    with this
          * --- Gsag_btr
          gsag_btr(this;
             )
    endwith
  endproc
  proc Calculate_QJTQJPIEZT()
    with this
          * --- Doppio click tracciabilit� di riga
          .w_SERIALE = .w_SERIAL2
          gsor_btd(this;
              ,'D';
             )
    endwith
  endproc
  proc Calculate_NSBXCYBNWL()
    with this
          * --- Resetto la sede
          .w_ATCODSED = ' '
          .w_NOMDES = ' '
    endwith
  endproc
  proc Calculate_JZKXQUXVIN()
    with this
          * --- Disabilita pubblica su Web se attivit� riservata
          .w_ATPUBWEB = IIF(.w_ATFLATRI='S' , 'N' , .w_ATPUBWEB)
    endwith
  endproc
  proc Calculate_VHMXIBNYXE()
    with this
          * --- Deve aggiornare le attivit� ricorrenti collegate
          .w_AggiornaRicorr = .T.
    endwith
  endproc
  proc Calculate_NQIEQWIEPI()
    with this
          * --- Mostra/Nascondi timer
          .w_CLOCK.Visible = .w_CA_TIMER="S" And .cFunction='Load'
    endwith
  endproc
  proc Calculate_CAKWIAAGCU()
    with this
          * --- AggiornaTimer
          .w_TIMER = .f.
          .w_RESET = .w_Clock.SetHour(0)
          .w_RESET = .w_Clock.SetMinute(0)
          .w_RESET = .w_Clock.SetSec(0)
          .w_RESET = iif(.w_CA_TIMER='S' AND ! .w_CLOCK.oTimer.Enabled ,.NotifyEvent("Clock"),.F.)
          .w_TIMER = IIF(.w_CA_TIMER<>'S',.F.,IIF(! .w_CLOCK.oTimer.Enabled,.F.,.T.))
    endwith
  endproc
  proc Calculate_JVRGHRXZTE()
    with this
          * --- Aggiorno tipo evento da tipo attivit�
          .w_TIPEVE = .w_TIPEVE
          .link_1_333('Full')
    endwith
  endproc
  proc Calculate_DYCXZYIKIF()
    with this
          * --- Reimposta timer
          .w_TMRDELTA = IIF(.w_TMRDELTA>=0, cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI), IIF(NOT EMPTY(.w_DATFIN),DTOC(.w_DATFIN),DTOC(.w_DATINI) ) +' '+.w_ORAFIN+':'+.w_MINFIN+':00', '  -  -       :  :  ') ) - cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  ')), .w_TMRDELTA)
          .w_RESET = IIF(!.w_TIMER AND .w_TMRDELTA>=0 AND .w_CA_TIMER="S" And .cFunction='Load', .w_Clock.SetHour(INT(.w_TMRDELTA/3600)), .F.)
          .w_RESET = IIF(!.w_TIMER AND .w_TMRDELTA>=0 AND .w_CA_TIMER="S" And .cFunction='Load', .w_Clock.SetMinute(INT(.w_TMRDELTA/60)), .F.)
          .w_RESET = IIF(!.w_TIMER AND .w_TMRDELTA>=0 AND .w_CA_TIMER="S" And .cFunction='Load', .w_Clock.SetSec(.w_TMRDELTA - INT(.w_TMRDELTA/3600)*3600 - INT(.w_TMRDELTA/60)*60 ), .F.)
          .w_TMRDELTA = 0
    endwith
  endproc
  proc Calculate_JYIDUYEAXU()
    with this
          * --- Verifiche e aggiornamenti quantit� elementi contratto associati
          gsag_bde(this;
              ,'ORECO';
             )
    endwith
  endproc
  proc Calculate_WOVXFKBIID()
    with this
          * --- Inizializza OLD_ATSTATUS
          .w_OLD_ATSTATUS = .w_ATSTATUS
          .w_OLD_ATTIPRIS = .w_ATTIPRIS
          .w_OLD_ATCODPRA = .w_ATCODPRA
    endwith
  endproc
  proc Calculate_RMJLZZAIQK()
    with this
          * --- Default partecipanti da tipo attivit�
          gsag_bap(this;
              ,'P';
             )
    endwith
  endproc
  proc Calculate_CZGHMRBITS()
    with this
          * --- Cancellazione allegati collegati
          gsma_bae(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_JCQCBTCGLD()
    with this
          * --- Aggiorna direzione
          .w_TIPEVE = .w_TIPEVE
          .link_1_333('Full')
    endwith
  endproc
  proc Calculate_GIJHIOBYBS()
    with this
          * --- Link listini
          .w_ATCODLIS = .w_ATCODLIS
          .link_2_3('Full')
    endwith
  endproc
  proc Calculate_QYQODPLTRV()
    with this
          * --- Riassegno alfa documento\tipo attivit� in  caso numerazione libera
          .op_ATALFDOC = iif(.w_FLPDOC $ 'N-L' ,.w_ATALFDOC,.op_ATALFDOC)
          .op_ATANNDOC = iif(.w_FLPDOC $ 'N-L' ,.w_ATANNDOC,.op_ATANNDOC)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oATCAUATT_1_10.enabled = this.oPgFrm.Page1.oPag.oATCAUATT_1_10.mCond()
    this.oPgFrm.Page1.oPag.oDATFIN_1_23.enabled = this.oPgFrm.Page1.oPag.oDATFIN_1_23.mCond()
    this.oPgFrm.Page1.oPag.oORAFIN_1_24.enabled = this.oPgFrm.Page1.oPag.oORAFIN_1_24.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_26.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_26.mCond()
    this.oPgFrm.Page1.oPag.oATSTATUS_1_28.enabled = this.oPgFrm.Page1.oPag.oATSTATUS_1_28.mCond()
    this.oPgFrm.Page1.oPag.oATSTATUS_1_29.enabled = this.oPgFrm.Page1.oPag.oATSTATUS_1_29.mCond()
    this.oPgFrm.Page1.oPag.oDATAPRO_1_35.enabled = this.oPgFrm.Page1.oPag.oDATAPRO_1_35.mCond()
    this.oPgFrm.Page1.oPag.oORAPRO_1_36.enabled = this.oPgFrm.Page1.oPag.oORAPRO_1_36.mCond()
    this.oPgFrm.Page1.oPag.oMINPRO_1_37.enabled = this.oPgFrm.Page1.oPag.oMINPRO_1_37.mCond()
    this.oPgFrm.Page1.oPag.oATTIPRIS_1_56.enabled = this.oPgFrm.Page1.oPag.oATTIPRIS_1_56.mCond()
    this.oPgFrm.Page1.oPag.oATCODPRA_1_68.enabled = this.oPgFrm.Page1.oPag.oATCODPRA_1_68.mCond()
    this.oPgFrm.Page1.oPag.oATCODPRA_1_69.enabled = this.oPgFrm.Page1.oPag.oATCODPRA_1_69.mCond()
    this.oPgFrm.Page1.oPag.oATCODBUN_1_76.enabled = this.oPgFrm.Page1.oPag.oATCODBUN_1_76.mCond()
    this.oPgFrm.Page1.oPag.oATCONTAT_1_92.enabled = this.oPgFrm.Page1.oPag.oATCONTAT_1_92.mCond()
    this.oPgFrm.Page1.oPag.oFlCreaPers_1_96.enabled = this.oPgFrm.Page1.oPag.oFlCreaPers_1_96.mCond()
    this.oPgFrm.Page1.oPag.oNewCodPer_1_97.enabled = this.oPgFrm.Page1.oPag.oNewCodPer_1_97.mCond()
    this.oPgFrm.Page1.oPag.oAT__ENTE_1_103.enabled = this.oPgFrm.Page1.oPag.oAT__ENTE_1_103.mCond()
    this.oPgFrm.Page1.oPag.oATUFFICI_1_104.enabled = this.oPgFrm.Page1.oPag.oATUFFICI_1_104.mCond()
    this.oPgFrm.Page1.oPag.oATCODSED_1_106.enabled = this.oPgFrm.Page1.oPag.oATCODSED_1_106.mCond()
    this.oPgFrm.Page2.oPag.oATSTATUS_2_10.enabled = this.oPgFrm.Page2.oPag.oATSTATUS_2_10.mCond()
    this.oPgFrm.Page2.oPag.oATSTATUS_2_12.enabled = this.oPgFrm.Page2.oPag.oATSTATUS_2_12.mCond()
    this.oPgFrm.Page1.oPag.oIDRICH_1_330.enabled = this.oPgFrm.Page1.oPag.oIDRICH_1_330.mCond()
    this.oPgFrm.Page1.oPag.oATSEREVE_1_331.enabled = this.oPgFrm.Page1.oPag.oATSEREVE_1_331.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_79.enabled = this.oPgFrm.Page1.oPag.oBtn_1_79.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_109.enabled = this.oPgFrm.Page1.oPag.oBtn_1_109.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_110.enabled = this.oPgFrm.Page1.oPag.oBtn_1_110.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_111.enabled = this.oPgFrm.Page1.oPag.oBtn_1_111.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_113.enabled = this.oPgFrm.Page1.oPag.oBtn_1_113.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_222.enabled = this.oPgFrm.Page1.oPag.oBtn_1_222.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_223.enabled = this.oPgFrm.Page1.oPag.oBtn_1_223.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_11.enabled = this.oPgFrm.Page2.oPag.oBtn_2_11.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_327.enabled = this.oPgFrm.Page1.oPag.oBtn_1_327.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_328.enabled = this.oPgFrm.Page1.oPag.oBtn_1_328.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_388.enabled = this.oPgFrm.Page1.oPag.oBtn_1_388.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_389.enabled = this.oPgFrm.Page1.oPag.oBtn_1_389.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_390.enabled = this.oPgFrm.Page1.oPag.oBtn_1_390.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_395.enabled = this.oPgFrm.Page1.oPag.oBtn_1_395.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_402.enabled = this.oPgFrm.Page1.oPag.oBtn_1_402.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_24.enabled = this.oPgFrm.Page3.oPag.oBtn_3_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_412.enabled = this.oPgFrm.Page1.oPag.oBtn_1_412.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_441.enabled = this.oPgFrm.Page1.oPag.oBtn_1_441.mCond()
    this.GSAG_MCP.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_108.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(3).enabled=not(!this.w_VisTracciab=='S')
    local i_show1
    i_show1=not(this.w_FLNSAP="S" OR this.w_CARAGGST='A')
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Prestazioni"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    local i_show2
    i_show2=not(!this.w_VisTracciab=='S' OR this.w_FLNSAP="S" OR this.w_CARAGGST='A')
    this.oPgFrm.Pages(3).enabled=i_show2 and not(!this.w_VisTracciab=='S')
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Tracciabilit�"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oATOPPOFF_1_27.visible=!this.oPgFrm.Page1.oPag.oATOPPOFF_1_27.mHide()
    this.oPgFrm.Page1.oPag.oATSTATUS_1_28.visible=!this.oPgFrm.Page1.oPag.oATSTATUS_1_28.mHide()
    this.oPgFrm.Page1.oPag.oATSTATUS_1_29.visible=!this.oPgFrm.Page1.oPag.oATSTATUS_1_29.mHide()
    this.oPgFrm.Page1.oPag.oATFLPROM_1_34.visible=!this.oPgFrm.Page1.oPag.oATFLPROM_1_34.mHide()
    this.oPgFrm.Page1.oPag.oDATAPRO_1_35.visible=!this.oPgFrm.Page1.oPag.oDATAPRO_1_35.mHide()
    this.oPgFrm.Page1.oPag.oORAPRO_1_36.visible=!this.oPgFrm.Page1.oPag.oORAPRO_1_36.mHide()
    this.oPgFrm.Page1.oPag.oMINPRO_1_37.visible=!this.oPgFrm.Page1.oPag.oMINPRO_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oATNUMDOC_1_39.visible=!this.oPgFrm.Page1.oPag.oATNUMDOC_1_39.mHide()
    this.oPgFrm.Page1.oPag.oATALFDOC_1_41.visible=!this.oPgFrm.Page1.oPag.oATALFDOC_1_41.mHide()
    this.oPgFrm.Page1.oPag.oATDATDOC_1_44.visible=!this.oPgFrm.Page1.oPag.oATDATDOC_1_44.mHide()
    this.oPgFrm.Page1.oPag.oATSTAATT_1_45.visible=!this.oPgFrm.Page1.oPag.oATSTAATT_1_45.mHide()
    this.oPgFrm.Page1.oPag.oATFLATRI_1_46.visible=!this.oPgFrm.Page1.oPag.oATFLATRI_1_46.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_47.visible=!this.oPgFrm.Page1.oPag.oBtn_1_47.mHide()
    this.oPgFrm.Page1.oPag.oDATRIN_1_53.visible=!this.oPgFrm.Page1.oPag.oDATRIN_1_53.mHide()
    this.oPgFrm.Page1.oPag.oORARIN_1_54.visible=!this.oPgFrm.Page1.oPag.oORARIN_1_54.mHide()
    this.oPgFrm.Page1.oPag.oMINRIN_1_55.visible=!this.oPgFrm.Page1.oPag.oMINRIN_1_55.mHide()
    this.oPgFrm.Page1.oPag.oATTIPRIS_1_56.visible=!this.oPgFrm.Page1.oPag.oATTIPRIS_1_56.mHide()
    this.oPgFrm.Page1.oPag.oATCODPRA_1_68.visible=!this.oPgFrm.Page1.oPag.oATCODPRA_1_68.mHide()
    this.oPgFrm.Page1.oPag.oATCODPRA_1_69.visible=!this.oPgFrm.Page1.oPag.oATCODPRA_1_69.mHide()
    this.oPgFrm.Page1.oPag.oATCENCOS_1_70.visible=!this.oPgFrm.Page1.oPag.oATCENCOS_1_70.mHide()
    this.oPgFrm.Page1.oPag.oATPUBWEB_1_71.visible=!this.oPgFrm.Page1.oPag.oATPUBWEB_1_71.mHide()
    this.oPgFrm.Page1.oPag.oSTARIC_1_72.visible=!this.oPgFrm.Page1.oPag.oSTARIC_1_72.mHide()
    this.oPgFrm.Page1.oPag.oGEN_ATT_1_73.visible=!this.oPgFrm.Page1.oPag.oGEN_ATT_1_73.mHide()
    this.oPgFrm.Page1.oPag.oATCODBUN_1_76.visible=!this.oPgFrm.Page1.oPag.oATCODBUN_1_76.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_79.visible=!this.oPgFrm.Page1.oPag.oBtn_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_88.visible=!this.oPgFrm.Page1.oPag.oStr_1_88.mHide()
    this.oPgFrm.Page1.oPag.oATCODNOM_1_90.visible=!this.oPgFrm.Page1.oPag.oATCODNOM_1_90.mHide()
    this.oPgFrm.Page1.oPag.oATCONTAT_1_92.visible=!this.oPgFrm.Page1.oPag.oATCONTAT_1_92.mHide()
    this.oPgFrm.Page1.oPag.oATPERSON_1_95.visible=!this.oPgFrm.Page1.oPag.oATPERSON_1_95.mHide()
    this.oPgFrm.Page1.oPag.oFlCreaPers_1_96.visible=!this.oPgFrm.Page1.oPag.oFlCreaPers_1_96.mHide()
    this.oPgFrm.Page1.oPag.oNewCodPer_1_97.visible=!this.oPgFrm.Page1.oPag.oNewCodPer_1_97.mHide()
    this.oPgFrm.Page1.oPag.oATTELEFO_1_99.visible=!this.oPgFrm.Page1.oPag.oATTELEFO_1_99.mHide()
    this.oPgFrm.Page1.oPag.oATCELLUL_1_100.visible=!this.oPgFrm.Page1.oPag.oATCELLUL_1_100.mHide()
    this.oPgFrm.Page1.oPag.oAT_EMAIL_1_101.visible=!this.oPgFrm.Page1.oPag.oAT_EMAIL_1_101.mHide()
    this.oPgFrm.Page1.oPag.oAT__ENTE_1_103.visible=!this.oPgFrm.Page1.oPag.oAT__ENTE_1_103.mHide()
    this.oPgFrm.Page1.oPag.oATUFFICI_1_104.visible=!this.oPgFrm.Page1.oPag.oATUFFICI_1_104.mHide()
    this.oPgFrm.Page1.oPag.oAT_EMPEC_1_105.visible=!this.oPgFrm.Page1.oPag.oAT_EMPEC_1_105.mHide()
    this.oPgFrm.Page1.oPag.oATCODSED_1_106.visible=!this.oPgFrm.Page1.oPag.oATCODSED_1_106.mHide()
    this.oPgFrm.Page1.oPag.oNOMDES_1_107.visible=!this.oPgFrm.Page1.oPag.oNOMDES_1_107.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_108.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_108.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_109.visible=!this.oPgFrm.Page1.oPag.oBtn_1_109.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_110.visible=!this.oPgFrm.Page1.oPag.oBtn_1_110.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_111.visible=!this.oPgFrm.Page1.oPag.oBtn_1_111.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_112.visible=!this.oPgFrm.Page1.oPag.oStr_1_112.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_113.visible=!this.oPgFrm.Page1.oPag.oBtn_1_113.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_114.visible=!this.oPgFrm.Page1.oPag.oStr_1_114.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_128.visible=!this.oPgFrm.Page1.oPag.oStr_1_128.mHide()
    this.oPgFrm.Page1.oPag.oGIORIN_1_129.visible=!this.oPgFrm.Page1.oPag.oGIORIN_1_129.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_135.visible=!this.oPgFrm.Page1.oPag.oStr_1_135.mHide()
    this.oPgFrm.Page1.oPag.oDESCAN_1_137.visible=!this.oPgFrm.Page1.oPag.oDESCAN_1_137.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_138.visible=!this.oPgFrm.Page1.oPag.oStr_1_138.mHide()
    this.oPgFrm.Page1.oPag.oGIOPRV_1_139.visible=!this.oPgFrm.Page1.oPag.oGIOPRV_1_139.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_152.visible=!this.oPgFrm.Page1.oPag.oStr_1_152.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_155.visible=!this.oPgFrm.Page1.oPag.oStr_1_155.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_204.visible=!this.oPgFrm.Page1.oPag.oStr_1_204.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_206.visible=!this.oPgFrm.Page1.oPag.oStr_1_206.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_208.visible=!this.oPgFrm.Page1.oPag.oStr_1_208.mHide()
    this.oPgFrm.Page1.oPag.oDATPRV_1_209.visible=!this.oPgFrm.Page1.oPag.oDATPRV_1_209.mHide()
    this.oPgFrm.Page1.oPag.oORAPRV_1_210.visible=!this.oPgFrm.Page1.oPag.oORAPRV_1_210.mHide()
    this.oPgFrm.Page1.oPag.oMINPRV_1_211.visible=!this.oPgFrm.Page1.oPag.oMINPRV_1_211.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_222.visible=!this.oPgFrm.Page1.oPag.oBtn_1_222.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_223.visible=!this.oPgFrm.Page1.oPag.oBtn_1_223.mHide()
    this.oPgFrm.Page1.oPag.oGIOPRM_1_227.visible=!this.oPgFrm.Page1.oPag.oGIOPRM_1_227.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_228.visible=!this.oPgFrm.Page1.oPag.oStr_1_228.mHide()
    this.oPgFrm.Page1.oPag.oCCDESPIA_1_236.visible=!this.oPgFrm.Page1.oPag.oCCDESPIA_1_236.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_237.visible=!this.oPgFrm.Page1.oPag.oStr_1_237.mHide()
    this.oPgFrm.Page2.oPag.oATCODLIS_2_3.visible=!this.oPgFrm.Page2.oPag.oATCODLIS_2_3.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_4.visible=!this.oPgFrm.Page2.oPag.oStr_2_4.mHide()
    this.oPgFrm.Page2.oPag.oPARASS_2_7.visible=!this.oPgFrm.Page2.oPag.oPARASS_2_7.mHide()
    this.oPgFrm.Page2.oPag.oFLAMPA_2_8.visible=!this.oPgFrm.Page2.oPag.oFLAMPA_2_8.mHide()
    this.oPgFrm.Page2.oPag.oATSTATUS_2_10.visible=!this.oPgFrm.Page2.oPag.oATSTATUS_2_10.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_11.visible=!this.oPgFrm.Page2.oPag.oBtn_2_11.mHide()
    this.oPgFrm.Page2.oPag.oATSTATUS_2_12.visible=!this.oPgFrm.Page2.oPag.oATSTATUS_2_12.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_13.visible=!this.oPgFrm.Page2.oPag.oBtn_2_13.mHide()
    this.oPgFrm.Page2.oPag.oATFLVALO_2_15.visible=!this.oPgFrm.Page2.oPag.oATFLVALO_2_15.mHide()
    this.oPgFrm.Page2.oPag.oATIMPORT_2_16.visible=!this.oPgFrm.Page2.oPag.oATIMPORT_2_16.mHide()
    this.oPgFrm.Page2.oPag.oATCALDIR_2_17.visible=!this.oPgFrm.Page2.oPag.oATCALDIR_2_17.mHide()
    this.oPgFrm.Page2.oPag.oATCOECAL_2_18.visible=!this.oPgFrm.Page2.oPag.oATCOECAL_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_19.visible=!this.oPgFrm.Page2.oPag.oStr_2_19.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_20.visible=!this.oPgFrm.Page2.oPag.oStr_2_20.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_21.visible=!this.oPgFrm.Page2.oPag.oStr_2_21.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_23.visible=!this.oPgFrm.Page2.oPag.oStr_2_23.mHide()
    this.oPgFrm.Page2.oPag.odespra_2_24.visible=!this.oPgFrm.Page2.oPag.odespra_2_24.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_25.visible=!this.oPgFrm.Page2.oPag.oStr_2_25.mHide()
    this.oPgFrm.Page2.oPag.ocodpra_2_26.visible=!this.oPgFrm.Page2.oPag.ocodpra_2_26.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_29.visible=!this.oPgFrm.Page2.oPag.oBtn_2_29.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_16.visible=!this.oPgFrm.Page3.oPag.oStr_3_16.mHide()
    this.oPgFrm.Page1.oPag.oCODCLI_1_251.visible=!this.oPgFrm.Page1.oPag.oCODCLI_1_251.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_32.visible=!this.oPgFrm.Page2.oPag.oBtn_2_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_253.visible=!this.oPgFrm.Page1.oPag.oStr_1_253.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_43.visible=!this.oPgFrm.Page2.oPag.oBtn_2_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_284.visible=!this.oPgFrm.Page1.oPag.oStr_1_284.mHide()
    this.oPgFrm.Page2.oPag.oLinkPC_2_44.visible=!this.oPgFrm.Page2.oPag.oLinkPC_2_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_310.visible=!this.oPgFrm.Page1.oPag.oStr_1_310.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_315.visible=!this.oPgFrm.Page1.oPag.oStr_1_315.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_317.visible=!this.oPgFrm.Page1.oPag.oStr_1_317.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_327.visible=!this.oPgFrm.Page1.oPag.oBtn_1_327.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_328.visible=!this.oPgFrm.Page1.oPag.oBtn_1_328.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_329.visible=!this.oPgFrm.Page1.oPag.oStr_1_329.mHide()
    this.oPgFrm.Page1.oPag.oIDRICH_1_330.visible=!this.oPgFrm.Page1.oPag.oIDRICH_1_330.mHide()
    this.oPgFrm.Page1.oPag.oATSEREVE_1_331.visible=!this.oPgFrm.Page1.oPag.oATSEREVE_1_331.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_332.visible=!this.oPgFrm.Page1.oPag.oStr_1_332.mHide()
    this.oPgFrm.Page1.oPag.oTIPEVE_1_333.visible=!this.oPgFrm.Page1.oPag.oTIPEVE_1_333.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_334.visible=!this.oPgFrm.Page1.oPag.oStr_1_334.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_339.visible=!this.oPgFrm.Page1.oPag.oBtn_1_339.mHide()
    this.oPgFrm.Page1.oPag.oDesNomin_1_367.visible=!this.oPgFrm.Page1.oPag.oDesNomin_1_367.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_368.visible=!this.oPgFrm.Page1.oPag.oStr_1_368.mHide()
    this.oPgFrm.Page2.oPag.oATLISACQ_2_49.visible=!this.oPgFrm.Page2.oPag.oATLISACQ_2_49.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_385.visible=!this.oPgFrm.Page1.oPag.oStr_1_385.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_386.visible=!this.oPgFrm.Page1.oPag.oStr_1_386.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_387.visible=!this.oPgFrm.Page1.oPag.oStr_1_387.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_388.visible=!this.oPgFrm.Page1.oPag.oBtn_1_388.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_389.visible=!this.oPgFrm.Page1.oPag.oBtn_1_389.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_390.visible=!this.oPgFrm.Page1.oPag.oBtn_1_390.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_393.visible=!this.oPgFrm.Page1.oPag.oStr_1_393.mHide()
    this.oPgFrm.Page1.oPag.oGiudicePrat_1_394.visible=!this.oPgFrm.Page1.oPag.oGiudicePrat_1_394.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_395.visible=!this.oPgFrm.Page1.oPag.oBtn_1_395.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_23.visible=!this.oPgFrm.Page3.oPag.oBtn_3_23.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_24.visible=!this.oPgFrm.Page3.oPag.oBtn_3_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_412.visible=!this.oPgFrm.Page1.oPag.oBtn_1_412.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_433.visible=!this.oPgFrm.Page1.oPag.oStr_1_433.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_434.visible=!this.oPgFrm.Page1.oPag.oStr_1_434.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_435.visible=!this.oPgFrm.Page1.oPag.oBtn_1_435.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_440.visible=!this.oPgFrm.Page1.oPag.oStr_1_440.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_441.visible=!this.oPgFrm.Page1.oPag.oBtn_1_441.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_62.visible=!this.oPgFrm.Page2.oPag.oBtn_2_62.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_aat
    If isalt() and cEvent='w_ATCODPRA Changed'
         this.NotifyEvent('DettPartAlt')
    endif
    If (cEvent='w_GEN_ATT Changed' or cEvent='w_DATINI Changed' or  cEvent='w_DATFIN Changed') AND !(this.w_GEN_ATT<>'R' or (this.w_GEN_ATT='R' AND this.w_DATINI=this.w_DATFIN))
        Ah_ErrorMsg('Data iniziale e data finale non corrispondono! %0Impossibile generare attivit� ricorrenti',64,'')
        this.w_GEN_ATT='N'
    endif
    if cevent='Progre'
       cp_BeginTrs()
       This.w_IDRICH=Space(15)
       i_Conn=i_TableProp[this.ANEVENTI_IDX, 3]
       cp_NextTableProg(this, i_Conn, "IDRICH", "i_codazi,w_IDRICH")
       this.w_NEWID=.t.
       cp_EndTrs(.t.)
    Endif
    
    if cEvent='Clock' 
    *--- Aggiornamento da Timer
    If this.w_TIMER
       local l_minute, l_hour
       this.o_DATFIN = this.w_DATFIN
       this.o_ORAFIN = this.w_ORAFIN
       this.o_MINFIN = this.w_MINFIN
       this.o_ATCAUATT = this.w_ATCAUATT
       l_minute = IIF(this.w_Clock.GetMinute()=0 And this.w_Clock.GetHour() =0, 1, IIF(this.w_Clock.GetSec()>=0 And this.w_Clock.GetSec()<30, this.w_Clock.GetMinute(), Minute(this.w_Clock.GetTime()+(60-Sec(this.w_Clock.GetTime())))))
       l_hour = 60*IIF(this.w_Clock.GetSec()>=0 And this.w_Clock.GetSec()<30, this.w_Clock.GetHour(), Hour(this.w_Clock.GetTime()+(60-Sec(this.w_Clock.GetTime()))))
       this.w_ATDURMIN = l_minute + l_hour
       this.w_TIMER = .F.
       this.w_TMRDELTA=-1
       this.NotifyEvent("w_ATDURMIN Changed")
    Else
       this.w_TIMER = .T.
       this.o_DATFIN = this.w_DATFIN
       this.o_ORAFIN = this.w_ORAFIN
       this.o_MINFIN = this.w_MINFIN
       this.NotifyEvent("AggDir")
       this.o_ATCAUATT = this.w_ATCAUATT
    EndIf
    Endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Load")
          .Calculate_ZQLIBXAFGH()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_83.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_84.Event(cEvent)
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_CFBUOHVOXL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ChkFinali")
          .Calculate_WMFTZGPBJG()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_142.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_143.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_144.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_145.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_151.Event(cEvent)
        if lower(cEvent)==lower("DettPartAlt") or lower(cEvent)==lower("New record") or lower(cEvent)==lower("w_CODPART Changed")
          .Calculate_AZCWEXDNXN()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_168.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_171.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_VSEQIYWONJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_JAESMIQPKQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AggLink")
          .Calculate_UROXKXOOGR()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_212.Event(cEvent)
        if lower(cEvent)==lower("w_ATNOTPIA Changed")
          .Calculate_VIECHUQVWU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Updated")
          .Calculate_WNOMGDHFOQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("MailCancellazione")
          .Calculate_MJGUYPNTDH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Inserted")
          .Calculate_HNGDUYESPH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ATFLNOTI Changed")
          .Calculate_VEWFNFWPRR()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_244.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_14.Event(cEvent)
      .oPgFrm.Page3.oPag.ZoomDocInt.Event(cEvent)
      .oPgFrm.Page3.oPag.ZoomTrRig.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_14.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 3")
          .Calculate_URCBBSJQVI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZoomTrRig selected")
          .Calculate_QJTQJPIEZT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ATCODNOM Changed")
          .Calculate_NSBXCYBNWL()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_255.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_256.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_261.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_285.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_286.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_287.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_288.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_293.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_294.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_45.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_312.Event(cEvent)
        if lower(cEvent)==lower("w_ATNOTPIA Changed") or lower(cEvent)==lower("w_ATOGGETT Changed")
          .Calculate_VHMXIBNYXE()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.CLOCK.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_NQIEQWIEPI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Reimposta")
          .Calculate_DYCXZYIKIF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Before Delete start") or lower(cEvent)==lower("Insert start") or lower(cEvent)==lower("Update start")
          .Calculate_JYIDUYEAXU()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_57.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_58.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_WOVXFKBIID()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_382.Event(cEvent)
        if lower(cEvent)==lower("w_ATCAUATT Changed")
          .Calculate_RMJLZZAIQK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete start")
          .Calculate_CZGHMRBITS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AggDir")
          .Calculate_JCQCBTCGLD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AggList")
          .Calculate_GIJHIOBYBS()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_442.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_443.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_444.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_aat
    if cevent='ActivatePage 3'
       *attivo bottoni
       This.mHideControls()
    endif
    if cevent='w_ATCAUATT Changed'
       This.w_Clock.SetHour(0)
       This.w_Clock.SetMinute(0)
       This.w_Clock.SetSec(0)
       This.w_RESET=iif(This.w_CA_TIMER='S' AND ! This.w_CLOCK.oTimer.Enabled ,This.NotifyEvent("Clock"),.F.)
       This.w_TIMER=IIF(This.w_CA_TIMER<>'S',.F.,IIF(! This.w_CLOCK.oTimer.Enabled,.F.,.T.))
    endif
    if cevent='Forzadoc' or cevent='w_ATCAUATT Changed' 
       local i_Conn
       i_Conn=i_TableProp[this.OFF_ATTI_IDX, 3]
       cp_AskTableProg(this, i_Conn,  "NUATT", "i_codazi,w_ATANNDOC,w_ATALFDOC,w_ATNUMDOC")
    ENDIF
    IF CEVENT='New record'  OR (CEVENT='w_ATCAUATT Changed' AND This.w_FLPDOC $ 'N-L' )
       This.w_ATNUMDOC=0
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LetturaParAgen
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LetturaParAgen) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LetturaParAgen)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAVISTRC,PACHKCAR,PACHKFES,PAFLVISI,PACODLIS,PALISACQ,PAGESRIS,PACHKPRE,PCCAUABB,PACHKATT,PAATTRIS,PAFLPRES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LetturaParAgen);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LetturaParAgen)
            select PACODAZI,PAVISTRC,PACHKCAR,PACHKFES,PAFLVISI,PACODLIS,PALISACQ,PAGESRIS,PACHKPRE,PCCAUABB,PACHKATT,PAATTRIS,PAFLPRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LetturaParAgen = NVL(_Link_.PACODAZI,space(5))
      this.w_VisTracciab = NVL(_Link_.PAVISTRC,space(1))
      this.w_PACHKCAR = NVL(_Link_.PACHKCAR,space(1))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
      this.w_PAFLVISI = NVL(_Link_.PAFLVISI,space(1))
      this.w_CODLIS = NVL(_Link_.PACODLIS,space(5))
      this.w_LISACQ = NVL(_Link_.PALISACQ,space(5))
      this.w_GESRIS = NVL(_Link_.PAGESRIS,space(1))
      this.w_CHKPRE = NVL(_Link_.PACHKPRE,space(1))
      this.w_PCAUABB = NVL(_Link_.PCCAUABB,space(5))
      this.w_ATLISACQ = NVL(_Link_.PALISACQ,space(5))
      this.w_PACHKATT = NVL(_Link_.PACHKATT,0)
      this.w_PAATTRIS = NVL(_Link_.PAATTRIS,space(20))
      this.w_ATTCOLIS = NVL(_Link_.PACODLIS,space(5))
      this.w_FLPRES = NVL(_Link_.PAFLPRES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LetturaParAgen = space(5)
      endif
      this.w_VisTracciab = space(1)
      this.w_PACHKCAR = space(1)
      this.w_PACHKFES = space(1)
      this.w_PAFLVISI = space(1)
      this.w_CODLIS = space(5)
      this.w_LISACQ = space(5)
      this.w_GESRIS = space(1)
      this.w_CHKPRE = space(1)
      this.w_PCAUABB = space(5)
      this.w_ATLISACQ = space(5)
      this.w_PACHKATT = 0
      this.w_PAATTRIS = space(20)
      this.w_ATTCOLIS = space(5)
      this.w_FLPRES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LetturaParAgen Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCAUATT
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCAUATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_ATCAUATT)+"%");

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_ATCAUATT))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCAUATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_ATCAUATT)+"%");

            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_ATCAUATT)+"%");

            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCAUATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oATCAUATT_1_10'),i_cWhere,'GSAG_MCA',"Tipi attivita",''+iif(NOT IsAlt(),"GSAG1AAT", "GSAG3AAT")+'.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCAUATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ATCAUATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ATCAUATT)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCAUATT = NVL(_Link_.CACODICE,space(20))
      this.w_ATOGGETT = NVL(_Link_.CADESCRI,space(254))
      this.w_ATDURORE = NVL(_Link_.CADURORE,0)
      this.w_ATDURMIN = NVL(_Link_.CADURMIN,0)
      this.w_CADTIN = NVL(_Link_.CADATINI,ctot(""))
      this.w_ATGGPREA = NVL(_Link_.CAGGPREA,0)
      this.w_ATSTATUS = NVL(_Link_.CASTAATT,space(1))
      this.w_ATNUMPRI = NVL(_Link_.CAPRIORI,0)
      this.w_ATCODATT = NVL(_Link_.CATIPATT,space(5))
      this.w_FLNSAP = NVL(_Link_.CAFLNSAP,space(1))
      this.w_ATCAUDOC = NVL(_Link_.CACAUDOC,space(5))
      this.w_CACHKOBB = NVL(_Link_.CACHKOBB,space(1))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(10))
      this.w_ATSTAATT = NVL(_Link_.CADISPON,0)
      this.w_FLTRIS = NVL(_Link_.CAFLTRIS,space(1))
      this.w_FLRINV = NVL(_Link_.CAFLRINV,space(1))
      this.w_ATFLNOTI = NVL(_Link_.CAFLNOTI,space(1))
      this.w_CACHKNOM = NVL(_Link_.CACHKNOM,space(1))
      this.w_CAMINPRE = NVL(_Link_.CAMINPRE,0)
      this.w_CAORASYS = NVL(_Link_.CAORASYS,space(1))
      this.w_ATFLPROM = NVL(_Link_.CAFLPREA,space(1))
      this.w_FLANAL = NVL(_Link_.CAFLANAL,space(1))
      this.w_ATPUBWEB = NVL(_Link_.CAPUBWEB,space(1))
      this.w_ATCAUACQ = NVL(_Link_.CACAUACQ,space(5))
      this.w_ATFLATRI = NVL(_Link_.CAFLATRI,space(1))
      this.w_CAFLSTAT = NVL(_Link_.CAFLSTAT,space(1))
      this.w_COLPOS = NVL(_Link_.CACOLPOS,0)
      this.w_PRIEML = NVL(_Link_.CAPRIEML,0)
      this.w_CAUABB = NVL(_Link_.CACAUABB,space(5))
      this.w_CA_TIMER = NVL(_Link_.CA_TIMER,space(1))
      this.w_TIPEVE = NVL(_Link_.CATIPEVE,space(10))
      this.w_ATNOTPIA = NVL(_Link_.CA__NOTE,space(0))
      this.w_MODAPP = NVL(_Link_.CAMODAPP,space(1))
      this.w_CATIPPRA = NVL(_Link_.CATIPPRA,space(10))
      this.w_CAMATPRA = NVL(_Link_.CAMATPRA,space(10))
      this.w_CA__ENTE = NVL(_Link_.CA__ENTE,space(10))
      this.w_CADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_FLPDOC = NVL(_Link_.CAFLPDOC,space(1))
      this.w_ATALFDOC = NVL(_Link_.CASERDOC,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ATCAUATT = space(20)
      endif
      this.w_ATOGGETT = space(254)
      this.w_ATDURORE = 0
      this.w_ATDURMIN = 0
      this.w_CADTIN = ctot("")
      this.w_ATGGPREA = 0
      this.w_ATSTATUS = space(1)
      this.w_ATNUMPRI = 0
      this.w_ATCODATT = space(5)
      this.w_FLNSAP = space(1)
      this.w_ATCAUDOC = space(5)
      this.w_CACHKOBB = space(1)
      this.w_CARAGGST = space(10)
      this.w_ATSTAATT = 0
      this.w_FLTRIS = space(1)
      this.w_FLRINV = space(1)
      this.w_ATFLNOTI = space(1)
      this.w_CACHKNOM = space(1)
      this.w_CAMINPRE = 0
      this.w_CAORASYS = space(1)
      this.w_ATFLPROM = space(1)
      this.w_FLANAL = space(1)
      this.w_ATPUBWEB = space(1)
      this.w_ATCAUACQ = space(5)
      this.w_ATFLATRI = space(1)
      this.w_CAFLSTAT = space(1)
      this.w_COLPOS = 0
      this.w_PRIEML = 0
      this.w_CAUABB = space(5)
      this.w_CA_TIMER = space(1)
      this.w_TIPEVE = space(10)
      this.w_ATNOTPIA = space(0)
      this.w_MODAPP = space(1)
      this.w_CATIPPRA = space(10)
      this.w_CAMATPRA = space(10)
      this.w_CA__ENTE = space(10)
      this.w_CADTOBSO = ctod("  /  /  ")
      this.w_FLPDOC = space(1)
      this.w_ATALFDOC = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CARAGGST#'Z' OR .w_ActByTimer='S') AND (EMPTY(.w_CADTOBSO) OR .w_CADTOBSO>=.w_DTBSO_CAUMATTI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_ATCAUATT = space(20)
        this.w_ATOGGETT = space(254)
        this.w_ATDURORE = 0
        this.w_ATDURMIN = 0
        this.w_CADTIN = ctot("")
        this.w_ATGGPREA = 0
        this.w_ATSTATUS = space(1)
        this.w_ATNUMPRI = 0
        this.w_ATCODATT = space(5)
        this.w_FLNSAP = space(1)
        this.w_ATCAUDOC = space(5)
        this.w_CACHKOBB = space(1)
        this.w_CARAGGST = space(10)
        this.w_ATSTAATT = 0
        this.w_FLTRIS = space(1)
        this.w_FLRINV = space(1)
        this.w_ATFLNOTI = space(1)
        this.w_CACHKNOM = space(1)
        this.w_CAMINPRE = 0
        this.w_CAORASYS = space(1)
        this.w_ATFLPROM = space(1)
        this.w_FLANAL = space(1)
        this.w_ATPUBWEB = space(1)
        this.w_ATCAUACQ = space(5)
        this.w_ATFLATRI = space(1)
        this.w_CAFLSTAT = space(1)
        this.w_COLPOS = 0
        this.w_PRIEML = 0
        this.w_CAUABB = space(5)
        this.w_CA_TIMER = space(1)
        this.w_TIPEVE = space(10)
        this.w_ATNOTPIA = space(0)
        this.w_MODAPP = space(1)
        this.w_CATIPPRA = space(10)
        this.w_CAMATPRA = space(10)
        this.w_CA__ENTE = space(10)
        this.w_CADTOBSO = ctod("  /  /  ")
        this.w_FLPDOC = space(1)
        this.w_ATALFDOC = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCAUATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 39 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+39<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.CACODICE as CACODICE110"+ ",link_1_10.CADESCRI as CADESCRI110"+ ",link_1_10.CADURORE as CADURORE110"+ ",link_1_10.CADURMIN as CADURMIN110"+ ",link_1_10.CADATINI as CADATINI110"+ ",link_1_10.CAGGPREA as CAGGPREA110"+ ",link_1_10.CASTAATT as CASTAATT110"+ ",link_1_10.CAPRIORI as CAPRIORI110"+ ",link_1_10.CATIPATT as CATIPATT110"+ ",link_1_10.CAFLNSAP as CAFLNSAP110"+ ",link_1_10.CACAUDOC as CACAUDOC110"+ ",link_1_10.CACHKOBB as CACHKOBB110"+ ",link_1_10.CARAGGST as CARAGGST110"+ ",link_1_10.CADISPON as CADISPON110"+ ",link_1_10.CAFLTRIS as CAFLTRIS110"+ ",link_1_10.CAFLRINV as CAFLRINV110"+ ",link_1_10.CAFLNOTI as CAFLNOTI110"+ ",link_1_10.CACHKNOM as CACHKNOM110"+ ",link_1_10.CAMINPRE as CAMINPRE110"+ ",link_1_10.CAORASYS as CAORASYS110"+ ",link_1_10.CAFLPREA as CAFLPREA110"+ ",link_1_10.CAFLANAL as CAFLANAL110"+ ",link_1_10.CAPUBWEB as CAPUBWEB110"+ ",link_1_10.CACAUACQ as CACAUACQ110"+ ",link_1_10.CAFLATRI as CAFLATRI110"+ ",link_1_10.CAFLSTAT as CAFLSTAT110"+ ",link_1_10.CACOLPOS as CACOLPOS110"+ ",link_1_10.CAPRIEML as CAPRIEML110"+ ",link_1_10.CACAUABB as CACAUABB110"+ ",link_1_10.CA_TIMER as CA_TIMER110"+ ",link_1_10.CATIPEVE as CATIPEVE110"+ ",link_1_10.CA__NOTE as CA__NOTE110"+ ",link_1_10.CAMODAPP as CAMODAPP110"+ ",link_1_10.CATIPPRA as CATIPPRA110"+ ",link_1_10.CAMATPRA as CAMATPRA110"+ ",link_1_10.CA__ENTE as CA__ENTE110"+ ",link_1_10.CADTOBSO as CADTOBSO110"+ ",link_1_10.CAFLPDOC as CAFLPDOC110"+ ",link_1_10.CASERDOC as CASERDOC110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on OFF_ATTI.ATCAUATT=link_1_10.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+39
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCAUATT=link_1_10.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+39
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATOPPOFF
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_ERTE_IDX,3]
    i_lTable = "OFF_ERTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2], .t., this.OFF_ERTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATOPPOFF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFF_ERTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OFSERIAL like "+cp_ToStrODBC(trim(this.w_ATOPPOFF)+"%");

          i_ret=cp_SQL(i_nConn,"select OFSERIAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OFSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OFSERIAL',trim(this.w_ATOPPOFF))
          select OFSERIAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OFSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATOPPOFF)==trim(_Link_.OFSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATOPPOFF) and !this.bDontReportError
            deferred_cp_zoom('OFF_ERTE','*','OFSERIAL',cp_AbsName(oSource.parent,'oATOPPOFF_1_27'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OFSERIAL";
                     +" from "+i_cTable+" "+i_lTable+" where OFSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OFSERIAL',oSource.xKey(1))
            select OFSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATOPPOFF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OFSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where OFSERIAL="+cp_ToStrODBC(this.w_ATOPPOFF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OFSERIAL',this.w_ATOPPOFF)
            select OFSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATOPPOFF = NVL(_Link_.OFSERIAL,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ATOPPOFF = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_ERTE_IDX,2])+'\'+cp_ToStr(_Link_.OFSERIAL,1)
      cp_ShowWarn(i_cKey,this.OFF_ERTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATOPPOFF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODATT
  func Link_1_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFFTIPAT_IDX,3]
    i_lTable = "OFFTIPAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2], .t., this.OFFTIPAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ATA',True,'OFFTIPAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODTIP like "+cp_ToStrODBC(trim(this.w_ATCODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODTIP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODTIP',trim(this.w_ATCODATT))
          select TACODTIP,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODTIP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODATT)==trim(_Link_.TACODTIP) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStrODBC(trim(this.w_ATCODATT)+"%");

            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TADESCRI like "+cp_ToStr(trim(this.w_ATCODATT)+"%");

            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCODATT) and !this.bDontReportError
            deferred_cp_zoom('OFFTIPAT','*','TACODTIP',cp_AbsName(oSource.parent,'oATCODATT_1_48'),i_cWhere,'GSAG_ATA',"Tipologie attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',oSource.xKey(1))
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODTIP,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODTIP="+cp_ToStrODBC(this.w_ATCODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODTIP',this.w_ATCODATT)
            select TACODTIP,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODATT = NVL(_Link_.TACODTIP,space(5))
      this.w_DESTIPOL = NVL(_Link_.TADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODATT = space(5)
      endif
      this.w_DESTIPOL = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])+'\'+cp_ToStr(_Link_.TACODTIP,1)
      cp_ShowWarn(i_cKey,this.OFFTIPAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_48(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFFTIPAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFFTIPAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_48.TACODTIP as TACODTIP148"+ ",link_1_48.TADESCRI as TADESCRI148"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_48 on OFF_ATTI.ATCODATT=link_1_48.TACODTIP"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_48"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCODATT=link_1_48.TACODTIP(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATTIPRIS
  func Link_1_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_RISE_IDX,3]
    i_lTable = "TIP_RISE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2], .t., this.TIP_RISE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATTIPRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_ATR',True,'TIP_RISE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_ATTIPRIS)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_ATTIPRIS))
          select TRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATTIPRIS)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATTIPRIS) and !this.bDontReportError
            deferred_cp_zoom('TIP_RISE','*','TRCODICE',cp_AbsName(oSource.parent,'oATTIPRIS_1_56'),i_cWhere,'GSAG_ATR',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATTIPRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_ATTIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_ATTIPRIS)
            select TRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATTIPRIS = NVL(_Link_.TRCODICE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATTIPRIS = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_RISE_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_RISE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATTIPRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCAUDOC
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDCATDOC,TDPRZVAC,TDFLINTE,TDFLPRAT,TDFLANAL,TDFLCOMM,TDFLVEAC,TDFLGLIS,TDCAUCON,TDNUMSCO";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_ATCAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_ATCAUDOC)
            select TDTIPDOC,TDCATDOC,TDPRZVAC,TDFLINTE,TDFLPRAT,TDFLANAL,TDFLCOMM,TDFLVEAC,TDFLGLIS,TDCAUCON,TDNUMSCO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_PARAME = NVL(_Link_.TDCATDOC,space(2))
      this.w_PRZVAC = NVL(_Link_.TDPRZVAC,space(1))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_FLPRAT = NVL(_Link_.TDFLPRAT,space(1))
      this.w_FLDANA = NVL(_Link_.TDFLANAL,space(1))
      this.w_FLGCOM = NVL(_Link_.TDFLCOMM,space(1))
      this.w_MVFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLGLIS = NVL(_Link_.TDFLGLIS,space(1))
      this.w_CAUCON = NVL(_Link_.TDCAUCON,space(5))
      this.w_NUMSCO = NVL(_Link_.TDNUMSCO,0)
    else
      if i_cCtrl<>'Load'
        this.w_ATCAUDOC = space(5)
      endif
      this.w_PARAME = space(2)
      this.w_PRZVAC = space(1)
      this.w_FLINTE = space(1)
      this.w_FLPRAT = space(1)
      this.w_FLDANA = space(1)
      this.w_FLGCOM = space(1)
      this.w_MVFLVEAC = space(1)
      this.w_FLGLIS = space(1)
      this.w_CAUCON = space(5)
      this.w_NUMSCO = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_59(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 11 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+11<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_59.TDTIPDOC as TDTIPDOC159"+ ",link_1_59.TDCATDOC as TDCATDOC159"+ ",link_1_59.TDPRZVAC as TDPRZVAC159"+ ",link_1_59.TDFLINTE as TDFLINTE159"+ ",link_1_59.TDFLPRAT as TDFLPRAT159"+ ",link_1_59.TDFLANAL as TDFLANAL159"+ ",link_1_59.TDFLCOMM as TDFLCOMM159"+ ",link_1_59.TDFLVEAC as TDFLVEAC159"+ ",link_1_59.TDFLGLIS as TDFLGLIS159"+ ",link_1_59.TDCAUCON as TDCAUCON159"+ ",link_1_59.TDNUMSCO as TDNUMSCO159"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_59 on OFF_ATTI.ATCAUDOC=link_1_59.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+11
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_59"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCAUDOC=link_1_59.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+11
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCAUACQ
  func Link_1_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCAUACQ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCAUACQ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDPRZVAC,TDFLPRAT,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_ATCAUACQ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_ATCAUACQ)
            select TDTIPDOC,TDPRZVAC,TDFLPRAT,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCAUACQ = NVL(_Link_.TDTIPDOC,space(5))
      this.w_PRZVAC = NVL(_Link_.TDPRZVAC,space(1))
      this.w_FLPRAT = NVL(_Link_.TDFLPRAT,space(1))
      this.w_MVFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCAUACQ = space(5)
      endif
      this.w_PRZVAC = space(1)
      this.w_FLPRAT = space(1)
      this.w_MVFLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCAUACQ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_60(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_60.TDTIPDOC as TDTIPDOC160"+ ",link_1_60.TDPRZVAC as TDPRZVAC160"+ ",link_1_60.TDFLPRAT as TDFLPRAT160"+ ",link_1_60.TDFLVEAC as TDFLVEAC160"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_60 on OFF_ATTI.ATCAUACQ=link_1_60.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_60"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCAUACQ=link_1_60.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCODPRA
  func Link_1_68(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZZ',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ATCODPRA))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_ATCODPRA)+"%");

            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oATCODPRA_1_68'),i_cWhere,'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSAG_AAT.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ATCODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ATCODPRA)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(100))
      this.w_LOC_PRA = NVL(_Link_.CNLOCALI,space(30))
      this.w_AT__ENTE = NVL(_Link_.CN__ENTE,space(10))
      this.w_ATUFFICI = NVL(_Link_.CNUFFICI,space(10))
      this.w_COLIPR = NVL(_Link_.CNCODLIS,space(5))
      this.w_ATFLVALO = NVL(_Link_.CNFLVALO,space(1))
      this.w_ATIMPORT = NVL(_Link_.CNIMPORT,0)
      this.w_ATCALDIR = NVL(_Link_.CNCALDIR,space(1))
      this.w_ATCOECAL = NVL(_Link_.CNCOECAL,0)
      this.w_ATFLAPON = NVL(_Link_.CNFLAPON,space(1))
      this.w_COVAPR = NVL(_Link_.CNCODVAL,space(3))
      this.w_PARASS = NVL(_Link_.CNPARASS,0)
      this.w_FLAMPA = NVL(_Link_.CNFLAMPA,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_ENTE = NVL(_Link_.CN__ENTE,space(10))
      this.w_ATTARTEM = NVL(_Link_.CNTARTEM,space(1))
      this.w_ATTARCON = NVL(_Link_.CNTARCON,0)
      this.w_CONTRACN = NVL(_Link_.CNCONTRA,space(5))
      this.w_ATFLVALO1 = NVL(_Link_.CNFLDIND,space(1))
      this.w_CNDATFIN = NVL(cp_ToDate(_Link_.CNDATFIN),ctod("  /  /  "))
      this.w_CNMATOBB = NVL(_Link_.CNMATOBB,space(1))
      this.w_CNASSCTP = NVL(_Link_.CNASSCTP,space(1))
      this.w_CNCOMPLX = NVL(_Link_.CNCOMPLX,space(1))
      this.w_CNPROFMT = NVL(_Link_.CNPROFMT,space(1))
      this.w_CNESIPOS = NVL(_Link_.CNESIPOS,space(1))
      this.w_CNPERPLX = NVL(_Link_.CNPERPLX,0)
      this.w_CNPERPOS = NVL(_Link_.CNPERPOS,0)
      this.w_CNTIPPRA = NVL(_Link_.CNTIPPRA,space(10))
      this.w_CNFLVMLQ = NVL(_Link_.CNFLVMLQ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODPRA = space(15)
      endif
      this.w_DESCAN = space(100)
      this.w_LOC_PRA = space(30)
      this.w_AT__ENTE = space(10)
      this.w_ATUFFICI = space(10)
      this.w_COLIPR = space(5)
      this.w_ATFLVALO = space(1)
      this.w_ATIMPORT = 0
      this.w_ATCALDIR = space(1)
      this.w_ATCOECAL = 0
      this.w_ATFLAPON = space(1)
      this.w_COVAPR = space(3)
      this.w_PARASS = 0
      this.w_FLAMPA = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_ENTE = space(10)
      this.w_ATTARTEM = space(1)
      this.w_ATTARCON = 0
      this.w_CONTRACN = space(5)
      this.w_ATFLVALO1 = space(1)
      this.w_CNDATFIN = ctod("  /  /  ")
      this.w_CNMATOBB = space(1)
      this.w_CNASSCTP = space(1)
      this.w_CNCOMPLX = space(1)
      this.w_CNPROFMT = space(1)
      this.w_CNESIPOS = space(1)
      this.w_CNPERPLX = 0
      this.w_CNPERPOS = 0
      this.w_CNTIPPRA = space(10)
      this.w_CNFLVMLQ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_ATSTATUS='P' AND NOT EMPTY(.w_ATCODPRA)) OR .w_ATSTATUS<>'P') AND (.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATCODPRA = space(15)
        this.w_DESCAN = space(100)
        this.w_LOC_PRA = space(30)
        this.w_AT__ENTE = space(10)
        this.w_ATUFFICI = space(10)
        this.w_COLIPR = space(5)
        this.w_ATFLVALO = space(1)
        this.w_ATIMPORT = 0
        this.w_ATCALDIR = space(1)
        this.w_ATCOECAL = 0
        this.w_ATFLAPON = space(1)
        this.w_COVAPR = space(3)
        this.w_PARASS = 0
        this.w_FLAMPA = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_ENTE = space(10)
        this.w_ATTARTEM = space(1)
        this.w_ATTARCON = 0
        this.w_CONTRACN = space(5)
        this.w_ATFLVALO1 = space(1)
        this.w_CNDATFIN = ctod("  /  /  ")
        this.w_CNMATOBB = space(1)
        this.w_CNASSCTP = space(1)
        this.w_CNCOMPLX = space(1)
        this.w_CNPROFMT = space(1)
        this.w_CNESIPOS = space(1)
        this.w_CNPERPLX = 0
        this.w_CNPERPOS = 0
        this.w_CNTIPPRA = space(10)
        this.w_CNFLVMLQ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_bRes and i_cCtrl='Drop'
      with this
        if i_bRes and !( (.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) OR ! Isalt() )
          i_bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
          if not(i_bRes)
            this.w_ATCODPRA = space(15)
            this.w_DESCAN = space(100)
            this.w_LOC_PRA = space(30)
            this.w_AT__ENTE = space(10)
            this.w_ATUFFICI = space(10)
            this.w_COLIPR = space(5)
            this.w_ATFLVALO = space(1)
            this.w_ATIMPORT = 0
            this.w_ATCALDIR = space(1)
            this.w_ATCOECAL = 0
            this.w_ATFLAPON = space(1)
            this.w_COVAPR = space(3)
            this.w_PARASS = 0
            this.w_FLAMPA = space(1)
            this.w_DATOBSO = ctod("  /  /  ")
            this.w_ENTE = space(10)
            this.w_ATTARTEM = space(1)
            this.w_ATTARCON = 0
            this.w_CONTRACN = space(5)
            this.w_ATFLVALO1 = space(1)
            this.w_CNDATFIN = ctod("  /  /  ")
            this.w_CNMATOBB = space(1)
            this.w_CNASSCTP = space(1)
            this.w_CNCOMPLX = space(1)
            this.w_CNPROFMT = space(1)
            this.w_CNESIPOS = space(1)
            this.w_CNPERPLX = 0
            this.w_CNPERPOS = 0
            this.w_CNTIPPRA = space(10)
            this.w_CNFLVMLQ = space(1)
          endif
        endif
      endwith
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_68(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 30 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+30<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_68.CNCODCAN as CNCODCAN168"+ ",link_1_68.CNDESCAN as CNDESCAN168"+ ",link_1_68.CNLOCALI as CNLOCALI168"+ ",link_1_68.CN__ENTE as CN__ENTE168"+ ",link_1_68.CNUFFICI as CNUFFICI168"+ ",link_1_68.CNCODLIS as CNCODLIS168"+ ",link_1_68.CNFLVALO as CNFLVALO168"+ ",link_1_68.CNIMPORT as CNIMPORT168"+ ",link_1_68.CNCALDIR as CNCALDIR168"+ ",link_1_68.CNCOECAL as CNCOECAL168"+ ",link_1_68.CNFLAPON as CNFLAPON168"+ ",link_1_68.CNCODVAL as CNCODVAL168"+ ",link_1_68.CNPARASS as CNPARASS168"+ ",link_1_68.CNFLAMPA as CNFLAMPA168"+ ",link_1_68.CNDTOBSO as CNDTOBSO168"+ ",link_1_68.CN__ENTE as CN__ENTE168"+ ",link_1_68.CNTARTEM as CNTARTEM168"+ ",link_1_68.CNTARCON as CNTARCON168"+ ",link_1_68.CNCONTRA as CNCONTRA168"+ ",link_1_68.CNFLDIND as CNFLDIND168"+ ",link_1_68.CNDATFIN as CNDATFIN168"+ ",link_1_68.CNMATOBB as CNMATOBB168"+ ",link_1_68.CNASSCTP as CNASSCTP168"+ ",link_1_68.CNCOMPLX as CNCOMPLX168"+ ",link_1_68.CNPROFMT as CNPROFMT168"+ ",link_1_68.CNESIPOS as CNESIPOS168"+ ",link_1_68.CNPERPLX as CNPERPLX168"+ ",link_1_68.CNPERPOS as CNPERPOS168"+ ",link_1_68.CNTIPPRA as CNTIPPRA168"+ ",link_1_68.CNFLVMLQ as CNFLVMLQ168"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_68 on OFF_ATTI.ATCODPRA=link_1_68.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+30
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_68"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCODPRA=link_1_68.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+30
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCODPRA
  func Link_1_69(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZZ',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO,CN__ENTE,CNTIPPRA,CNMATPRA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ATCODPRA))
          select CNCODCAN,CNDESCAN,CNDTOBSO,CN__ENTE,CNTIPPRA,CNMATPRA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO,CN__ENTE,CNTIPPRA,CNMATPRA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_ATCODPRA)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO,CN__ENTE,CNTIPPRA,CNMATPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oATCODPRA_1_69'),i_cWhere,'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",''+iif(NOT IsAlt(),"default", "GSAG_AAT")+'.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO,CN__ENTE,CNTIPPRA,CNMATPRA";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO,CN__ENTE,CNTIPPRA,CNMATPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO,CN__ENTE,CNTIPPRA,CNMATPRA";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ATCODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ATCODPRA)
            select CNCODCAN,CNDESCAN,CNDTOBSO,CN__ENTE,CNTIPPRA,CNMATPRA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(100))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_ENTE = NVL(_Link_.CN__ENTE,space(10))
      this.w_TipPra = NVL(_Link_.CNTIPPRA,space(10))
      this.w_MatPra = NVL(_Link_.CNMATPRA,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODPRA = space(15)
      endif
      this.w_DESCAN = space(100)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_ENTE = space(10)
      this.w_TipPra = space(10)
      this.w_MatPra = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_ATSTATUS='P' AND NOT EMPTY(.w_ATCODPRA)) OR .w_ATSTATUS<>'P') AND (.w_CFUNC<>'Load' OR .w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATCODPRA = space(15)
        this.w_DESCAN = space(100)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_ENTE = space(10)
        this.w_TipPra = space(10)
        this.w_MatPra = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_69(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_69.CNCODCAN as CNCODCAN169"+ ",link_1_69.CNDESCAN as CNDESCAN169"+ ",link_1_69.CNDTOBSO as CNDTOBSO169"+ ",link_1_69.CN__ENTE as CN__ENTE169"+ ",link_1_69.CNTIPPRA as CNTIPPRA169"+ ",link_1_69.CNMATPRA as CNMATPRA169"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_69 on OFF_ATTI.ATCODPRA=link_1_69.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_69"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCODPRA=link_1_69.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCENCOS
  func Link_1_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ATCENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ATCENCOS))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oATCENCOS_1_70'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ATCENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ATCENCOS)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCENCOS = space(15)
      endif
      this.w_CCDESPIA = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO) and isahe()) or (!isahe() AND CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
        endif
        this.w_ATCENCOS = space(15)
        this.w_CCDESPIA = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_70(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_70.CC_CONTO as CC_CONTO170"+ ",link_1_70.CCDESPIA as CCDESPIA170"+ ",link_1_70.CCDTOBSO as CCDTOBSO170"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_70 on OFF_ATTI.ATCENCOS=link_1_70.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_70"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCENCOS=link_1_70.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCODBUN
  func Link_1_76(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_ATCODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_READAZI);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUFLANAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_READAZI;
                     ,'BUCODICE',trim(this.w_ATCODBUN))
          select BUCODAZI,BUCODICE,BUFLANAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODBUN)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODBUN) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oATCODBUN_1_76'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_READAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUFLANAL;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_READAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_ATCODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_READAZI;
                       ,'BUCODICE',this.w_ATCODBUN)
            select BUCODAZI,BUCODICE,BUFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_BUFLANAL = NVL(_Link_.BUFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODBUN = space(3)
      endif
      this.w_BUFLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODNOM
  func Link_1_90(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_ATCODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOTIPCLI,NOTIPNOM,NODTOBSO,NOCODCLI,NOLOCALI,NOCODLIN,NONUMLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_ATCODNOM))
          select NOCODICE,NODESCRI,NODESCR2,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOTIPCLI,NOTIPNOM,NODTOBSO,NOCODCLI,NOLOCALI,NOCODLIN,NONUMLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_ATCODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOTIPCLI,NOTIPNOM,NODTOBSO,NOCODCLI,NOLOCALI,NOCODLIN,NONUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_ATCODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOTIPCLI,NOTIPNOM,NODTOBSO,NOCODCLI,NOLOCALI,NOCODLIN,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_ATCODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOTIPCLI,NOTIPNOM,NODTOBSO,NOCODCLI,NOLOCALI,NOCODLIN,NONUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_ATCODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOTIPCLI,NOTIPNOM,NODTOBSO,NOCODCLI,NOLOCALI,NOCODLIN,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oATCODNOM_1_90'),i_cWhere,'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOTIPCLI,NOTIPNOM,NODTOBSO,NOCODCLI,NOLOCALI,NOCODLIN,NONUMLIS";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOTIPCLI,NOTIPNOM,NODTOBSO,NOCODCLI,NOLOCALI,NOCODLIN,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOTIPCLI,NOTIPNOM,NODTOBSO,NOCODCLI,NOLOCALI,NOCODLIN,NONUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_ATCODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NOTELEFO,NOTELFAX,NO_EMAIL,NO_EMPEC,NONUMCEL,NOTIPCLI,NOTIPNOM,NODTOBSO,NOCODCLI,NOLOCALI,NOCODLIN,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_DesNomin = NVL(_Link_.NODESCRI,space(60))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(10))
      this.w_ATTELEFO = NVL(_Link_.NOTELEFO,space(18))
      this.w_AT___FAX = NVL(_Link_.NOTELFAX,space(18))
      this.w_AT_EMAIL = NVL(_Link_.NO_EMAIL,space(0))
      this.w_AT_EMPEC = NVL(_Link_.NO_EMPEC,space(0))
      this.w_ATCELLUL = NVL(_Link_.NONUMCEL,space(18))
      this.w_ATTIPCLI = NVL(_Link_.NOTIPCLI,space(1))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
      this.w_DATOBSONOM = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_CODCLI = NVL(_Link_.NOCODCLI,space(15))
      this.w_LOC_NOM = NVL(_Link_.NOLOCALI,space(30))
      this.w_CODLIN = NVL(_Link_.NOCODLIN,space(3))
      this.w_NCODLIS = NVL(_Link_.NONUMLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODNOM = space(15)
      endif
      this.w_DesNomin = space(60)
      this.w_COMODO = space(10)
      this.w_ATTELEFO = space(18)
      this.w_AT___FAX = space(18)
      this.w_AT_EMAIL = space(0)
      this.w_AT_EMPEC = space(0)
      this.w_ATCELLUL = space(18)
      this.w_ATTIPCLI = space(1)
      this.w_TIPNOM = space(1)
      this.w_DATOBSONOM = ctod("  /  /  ")
      this.w_CODCLI = space(15)
      this.w_LOC_NOM = space(30)
      this.w_CODLIN = space(3)
      this.w_NCODLIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DATOBSONOM>.w_OFDATDOC OR EMPTY(.w_DATOBSONOM)) AND .w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATCODNOM = space(15)
        this.w_DesNomin = space(60)
        this.w_COMODO = space(10)
        this.w_ATTELEFO = space(18)
        this.w_AT___FAX = space(18)
        this.w_AT_EMAIL = space(0)
        this.w_AT_EMPEC = space(0)
        this.w_ATCELLUL = space(18)
        this.w_ATTIPCLI = space(1)
        this.w_TIPNOM = space(1)
        this.w_DATOBSONOM = ctod("  /  /  ")
        this.w_CODCLI = space(15)
        this.w_LOC_NOM = space(30)
        this.w_CODLIN = space(3)
        this.w_NCODLIS = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_90(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 15 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFF_NOMI_IDX,3] and i_nFlds+15<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_90.NOCODICE as NOCODICE190"+ ",link_1_90.NODESCRI as NODESCRI190"+ ",link_1_90.NODESCR2 as NODESCR2190"+ ",link_1_90.NOTELEFO as NOTELEFO190"+ ",link_1_90.NOTELFAX as NOTELFAX190"+ ",link_1_90.NO_EMAIL as NO_EMAIL190"+ ",link_1_90.NO_EMPEC as NO_EMPEC190"+ ",link_1_90.NONUMCEL as NONUMCEL190"+ ",link_1_90.NOTIPCLI as NOTIPCLI190"+ ",link_1_90.NOTIPNOM as NOTIPNOM190"+ ",link_1_90.NODTOBSO as NODTOBSO190"+ ",link_1_90.NOCODCLI as NOCODCLI190"+ ",link_1_90.NOLOCALI as NOLOCALI190"+ ",link_1_90.NOCODLIN as NOCODLIN190"+ ",link_1_90.NONUMLIS as NONUMLIS190"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_90 on OFF_ATTI.ATCODNOM=link_1_90.NOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+15
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_90"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCODNOM=link_1_90.NOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+15
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCONTAT
  func Link_1_92(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCONTAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCCODCON like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

          i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCCODICE,NCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCCODICE',this.w_ATCODNOM;
                     ,'NCCODCON',trim(this.w_ATCONTAT))
          select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCCODICE,NCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCONTAT)==trim(_Link_.NCCODCON) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NCPERSON like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NCPERSON like "+cp_ToStr(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStr(this.w_ATCODNOM);

            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NCTELEFO like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NCTELEFO like "+cp_ToStr(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStr(this.w_ATCODNOM);

            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NCNUMCEL like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NCNUMCEL like "+cp_ToStr(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStr(this.w_ATCODNOM);

            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NC_EMAIL like "+cp_ToStrODBC(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NC_EMAIL like "+cp_ToStr(trim(this.w_ATCONTAT)+"%");
                   +" and NCCODICE="+cp_ToStr(this.w_ATCODNOM);

            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCONTAT) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(oSource.parent,'oATCONTAT_1_92'),i_cWhere,'',"Persone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODNOM<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                     +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',oSource.xKey(1);
                       ,'NCCODCON',oSource.xKey(2))
            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCONTAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC";
                   +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(this.w_ATCONTAT);
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',this.w_ATCODNOM;
                       ,'NCCODCON',this.w_ATCONTAT)
            select NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NCNUMCEL,NC_EMAIL,NC_EMPEC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCONTAT = NVL(_Link_.NCCODCON,space(5))
      this.w_Pers_Descri = NVL(_Link_.NCPERSON,space(60))
      this.w_Pers_Telefono = NVL(_Link_.NCTELEFO,space(18))
      this.w_Pers_Cellulare = NVL(_Link_.NCNUMCEL,space(18))
      this.w_Pers_E_Mail = NVL(_Link_.NC_EMAIL,space(254))
      this.w_Pers_E_Mpec = NVL(_Link_.NC_EMPEC,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_ATCONTAT = space(5)
      endif
      this.w_Pers_Descri = space(60)
      this.w_Pers_Telefono = space(18)
      this.w_Pers_Cellulare = space(18)
      this.w_Pers_E_Mail = space(254)
      this.w_Pers_E_Mpec = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCCODICE,1)+'\'+cp_ToStr(_Link_.NCCODCON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCONTAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_92(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NOM_CONT_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_92.NCCODCON as NCCODCON192"+ ",link_1_92.NCPERSON as NCPERSON192"+ ",link_1_92.NCTELEFO as NCTELEFO192"+ ",link_1_92.NCNUMCEL as NCNUMCEL192"+ ",link_1_92.NC_EMAIL as NC_EMAIL192"+ ",link_1_92.NC_EMPEC as NC_EMPEC192"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_92 on OFF_ATTI.ATCONTAT=link_1_92.NCCODCON"+" and OFF_ATTI.ATCODNOM=link_1_92.NCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_92"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCONTAT=link_1_92.NCCODCON(+)"'+'+" and OFF_ATTI.ATCODNOM=link_1_92.NCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NewCodPer
  func Link_1_97(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NewCodPer) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MCN',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCCODCON like "+cp_ToStrODBC(trim(this.w_NewCodPer)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);

          i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCCODICE,NCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCCODICE',this.w_ATCODNOM;
                     ,'NCCODCON',trim(this.w_NewCodPer))
          select NCCODICE,NCCODCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCCODICE,NCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NewCodPer)==trim(_Link_.NCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NewCodPer) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(oSource.parent,'oNewCodPer_1_97'),i_cWhere,'GSAR_MCN',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODNOM<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select NCCODICE,NCCODCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Specificare un codice persona diverso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON";
                     +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',oSource.xKey(1);
                       ,'NCCODCON',oSource.xKey(2))
            select NCCODICE,NCCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NewCodPer)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON";
                   +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(this.w_NewCodPer);
                   +" and NCCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',this.w_ATCODNOM;
                       ,'NCCODCON',this.w_NewCodPer)
            select NCCODICE,NCCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_NewCodPer = NVL(_Link_.NCCODCON,space(5))
      this.w_ControlloPers = NVL(_Link_.NCCODCON,space(5))
    else
      this.w_ControlloPers = space(5)
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_ControlloPers)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Specificare un codice persona diverso")
        endif
        this.w_NewCodPer = space(5)
        this.w_ControlloPers = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCCODICE,1)+'\'+cp_ToStr(_Link_.NCCODCON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NewCodPer Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AT__ENTE
  func Link_1_103(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AT__ENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_AEP',True,'PRA_ENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EPCODICE like "+cp_ToStrODBC(trim(this.w_AT__ENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select EPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EPCODICE',trim(this.w_AT__ENTE))
          select EPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AT__ENTE)==trim(_Link_.EPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_AT__ENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_AT__ENTE)+"%");

            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AT__ENTE) and !this.bDontReportError
            deferred_cp_zoom('PRA_ENTI','*','EPCODICE',cp_AbsName(oSource.parent,'oAT__ENTE_1_103'),i_cWhere,'GSPR_AEP',"Enti pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',oSource.xKey(1))
            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AT__ENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_AT__ENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_AT__ENTE)
            select EPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AT__ENTE = NVL(_Link_.EPCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_AT__ENTE = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AT__ENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATUFFICI
  func Link_1_104(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_UFFI_IDX,3]
    i_lTable = "PRA_UFFI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2], .t., this.PRA_UFFI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATUFFICI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_AUF',True,'PRA_UFFI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UFCODICE like "+cp_ToStrODBC(trim(this.w_ATUFFICI)+"%");

          i_ret=cp_SQL(i_nConn,"select UFCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UFCODICE',trim(this.w_ATUFFICI))
          select UFCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATUFFICI)==trim(_Link_.UFCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_ATUFFICI)+"%");

            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_ATUFFICI)+"%");

            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATUFFICI) and !this.bDontReportError
            deferred_cp_zoom('PRA_UFFI','*','UFCODICE',cp_AbsName(oSource.parent,'oATUFFICI_1_104'),i_cWhere,'GSPR_AUF',"Uffici pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',oSource.xKey(1))
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATUFFICI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(this.w_ATUFFICI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',this.w_ATUFFICI)
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATUFFICI = NVL(_Link_.UFCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ATUFFICI = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_UFFI_IDX,2])+'\'+cp_ToStr(_Link_.UFCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_UFFI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATUFFICI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODSED
  func Link_1_106(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_ATCODSED)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_ATTIPCLI);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_ATTIPCLI;
                     ,'DDCODICE',this.w_CODCLI;
                     ,'DDCODDES',trim(this.w_ATCODSED))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODSED)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODSED) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oATCODSED_1_106'),i_cWhere,'',"Sedi",'gsag_des.DES_DIVE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATTIPCLI<>oSource.xKey(1);
           .or. this.w_CODCLI<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_ATTIPCLI);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_ATCODSED);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_ATTIPCLI);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_ATTIPCLI;
                       ,'DDCODICE',this.w_CODCLI;
                       ,'DDCODDES',this.w_ATCODSED)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODSED = NVL(_Link_.DDCODDES,space(5))
      this.w_NOMDES = NVL(_Link_.DDNOMDES,space(40))
      this.w_TIPRIF = NVL(_Link_.DDTIPRIF,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODSED = space(5)
      endif
      this.w_NOMDES = space(40)
      this.w_TIPRIF = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIF='CO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        endif
        this.w_ATCODSED = space(5)
        this.w_NOMDES = space(40)
        this.w_TIPRIF = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATRIFDOC
  func Link_1_173(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATRIFDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATRIFDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_ATRIFDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_ATRIFDOC)
            select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATRIFDOC = NVL(_Link_.MVSERIAL,space(10))
      this.w_NUMDOC = NVL(_Link_.MVNUMDOC,0)
      this.w_ALFDOC = NVL(_Link_.MVALFDOC,space(10))
      this.w_DATDOC = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATRIFDOC = space(10)
      endif
      this.w_NUMDOC = 0
      this.w_ALFDOC = space(10)
      this.w_DATDOC = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATRIFDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ENTE
  func Link_1_199(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EP__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_ENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_ENTE)
            select EPCODICE,EP__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ENTE = NVL(_Link_.EPCODICE,space(10))
      this.w_PRENTTIP = NVL(_Link_.EP__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ENTE = space(10)
      endif
      this.w_PRENTTIP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODVAL
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECUNI,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_ATCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_ATCODVAL)
            select VACODVAL,VADESVAL,VADECUNI,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_ATCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DECUNI = 0
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.VACODVAL as VACODVAL202"+ ",link_2_2.VADESVAL as VADESVAL202"+ ",link_2_2.VADECUNI as VADECUNI202"+ ",link_2_2.VADECTOT as VADECTOT202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on OFF_ATTI.ATCODVAL=link_2_2.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATCODVAL=link_2_2.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ATCODLIS
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ATCODLIS)+"%");
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);

          i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSVALLIS,LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSVALLIS',this.w_ATCODVAL;
                     ,'LSCODLIS',trim(this.w_ATCODLIS))
          select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSVALLIS,LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(oSource.parent,'oATCODLIS_2_3'),i_cWhere,'',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODVAL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, listino non valido")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',oSource.xKey(1);
                       ,'LSCODLIS',oSource.xKey(2))
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ATCODLIS);
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',this.w_ATCODVAL;
                       ,'LSCODLIS',this.w_ATCODLIS)
            select LSVALLIS,LSCODLIS,LSDESLIS,LSFLSCON,LSDTINVA,LSDTOBSO,LSIVALIS,LSQUANTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_SCOLIS = NVL(_Link_.LSFLSCON,space(1))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_QUALIS = NVL(_Link_.LSQUANTI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_SCOLIS = space(1)
      this.w_INILIS = ctod("  /  /  ")
      this.w_FINLIS = ctod("  /  /  ")
      this.w_IVALIS = space(1)
      this.w_QUALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Isahe() or .w_FLNSAP="S" or CHKLISD(.w_ATCODLIS,.w_IVALIS,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_ATCODVAL, .w_DATINI) 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, listino non valido")
        endif
        this.w_ATCODLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_VALLIS = space(3)
        this.w_SCOLIS = space(1)
        this.w_INILIS = ctod("  /  /  ")
        this.w_FINLIS = ctod("  /  /  ")
        this.w_IVALIS = space(1)
        this.w_QUALIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSVALLIS,1)+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATRIFMOV
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CDC_MANU_IDX,3]
    i_lTable = "CDC_MANU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CDC_MANU_IDX,2], .t., this.CDC_MANU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CDC_MANU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATRIFMOV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATRIFMOV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_ATRIFMOV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_ATRIFMOV)
            select CMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATRIFMOV = NVL(_Link_.CMCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ATRIFMOV = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CDC_MANU_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CDC_MANU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATRIFMOV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLI
  func Link_1_251(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ATTIPCLI);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS,ANCATCOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ATTIPCLI;
                     ,'ANCODICE',trim(this.w_CODCLI))
          select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS,ANCATCOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLI_1_251'),i_cWhere,'GSAR_BZC',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATTIPCLI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS,ANCATCOM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS,ANCATCOM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS,ANCATCOM";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ATTIPCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS,ANCATCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS,ANCATCOM";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ATTIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ATTIPCLI;
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS,ANCATCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_FLSCOR = NVL(_Link_.ANSCORPO,space(1))
      this.w_NUMLIS = NVL(_Link_.ANNUMLIS,space(5))
      this.w_CATCOM = NVL(_Link_.ANCATCOM,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_FLSCOR = space(1)
      this.w_NUMLIS = space(5)
      this.w_CATCOM = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ASERIAL
  func Link_1_266(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DET_GEN_IDX,3]
    i_lTable = "DET_GEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DET_GEN_IDX,2], .t., this.DET_GEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DET_GEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ASERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ASERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDSERATT,MDSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where MDSERATT="+cp_ToStrODBC(this.w_ASERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDSERATT',this.w_ASERIAL)
            select MDSERATT,MDSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ASERIAL = NVL(_Link_.MDSERATT,space(20))
      this.w_SERPIAN = NVL(_Link_.MDSERIAL,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_ASERIAL = space(20)
      endif
      this.w_SERPIAN = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DET_GEN_IDX,2])+'\'+cp_ToStr(_Link_.MDSERATT,1)
      cp_ShowWarn(i_cKey,this.DET_GEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ASERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMLIS
  func Link_1_274(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSIVALIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_NUMLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_NUMLIS)
            select LSCODLIS,LSIVALIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_IVACLI = NVL(_Link_.LSIVALIS,space(1))
      this.w_SCOLIC = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NUMLIS = space(5)
      endif
      this.w_IVACLI = space(1)
      this.w_SCOLIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTRACN
  func Link_1_304(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_CONT_IDX,3]
    i_lTable = "PRA_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_CONT_IDX,2], .t., this.PRA_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTRACN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTRACN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPCODCON,CPTIPCON,CPLISCOL,CPSTATUS";
                   +" from "+i_cTable+" "+i_lTable+" where CPCODCON="+cp_ToStrODBC(this.w_CONTRACN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPCODCON',this.w_CONTRACN)
            select CPCODCON,CPTIPCON,CPLISCOL,CPSTATUS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTRACN = NVL(_Link_.CPCODCON,space(5))
      this.w_TIPCONCO = NVL(_Link_.CPTIPCON,space(1))
      this.w_LISCOLCO = NVL(_Link_.CPLISCOL,space(5))
      this.w_STATUSCO = NVL(_Link_.CPSTATUS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONTRACN = space(5)
      endif
      this.w_TIPCONCO = space(1)
      this.w_LISCOLCO = space(5)
      this.w_STATUSCO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CPCODCON,1)
      cp_ShowWarn(i_cKey,this.PRA_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTRACN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATSEREVE
  func Link_1_331(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_lTable = "ANEVENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2], .t., this.ANEVENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATSEREVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ANEVENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EVSERIAL like "+cp_ToStrODBC(trim(this.w_ATSEREVE)+"%");
                   +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);

          i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EVTIPEVE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EV__ANNO,EVSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EV__ANNO',this.w_ATEVANNO;
                     ,'EVSERIAL',trim(this.w_ATSEREVE))
          select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EVTIPEVE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EV__ANNO,EVSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATSEREVE)==trim(_Link_.EVSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATSEREVE) and !this.bDontReportError
            deferred_cp_zoom('ANEVENTI','*','EV__ANNO,EVSERIAL',cp_AbsName(oSource.parent,'oATSEREVE_1_331'),i_cWhere,'',"",'GSFA_ZID.ANEVENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATEVANNO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EVTIPEVE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EVTIPEVE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EVTIPEVE";
                     +" from "+i_cTable+" "+i_lTable+" where EVSERIAL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EV__ANNO',oSource.xKey(1);
                       ,'EVSERIAL',oSource.xKey(2))
            select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EVTIPEVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATSEREVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EVTIPEVE";
                   +" from "+i_cTable+" "+i_lTable+" where EVSERIAL="+cp_ToStrODBC(this.w_ATSEREVE);
                   +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EV__ANNO',this.w_ATEVANNO;
                       ,'EVSERIAL',this.w_ATSEREVE)
            select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EVTIPEVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATSEREVE = NVL(_Link_.EVSERIAL,space(10))
      this.w_IDRICH = NVL(_Link_.EVIDRICH,space(15))
      this.w_STARIC = NVL(_Link_.EVSTARIC,space(1))
      this.w_TIPEVE = NVL(_Link_.EVTIPEVE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ATSEREVE = space(10)
      endif
      this.w_IDRICH = space(15)
      this.w_STARIC = space(1)
      this.w_TIPEVE = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])+'\'+cp_ToStr(_Link_.EV__ANNO,1)+'\'+cp_ToStr(_Link_.EVSERIAL,1)
      cp_ShowWarn(i_cKey,this.ANEVENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATSEREVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_331(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ANEVENTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_331.EVSERIAL as EVSERIAL431"+ ",link_1_331.EVIDRICH as EVIDRICH431"+ ",link_1_331.EVSTARIC as EVSTARIC431"+ ",link_1_331.EVTIPEVE as EVTIPEVE431"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_331 on OFF_ATTI.ATSEREVE=link_1_331.EVSERIAL"+" and OFF_ATTI.ATEVANNO=link_1_331.EV__ANNO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_331"
          i_cKey=i_cKey+'+" and OFF_ATTI.ATSEREVE=link_1_331.EVSERIAL(+)"'+'+" and OFF_ATTI.ATEVANNO=link_1_331.EV__ANNO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TIPEVE
  func Link_1_333(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPEVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIPEVENT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TETIPEVE like "+cp_ToStrODBC(trim(this.w_TIPEVE)+"%");

          i_ret=cp_SQL(i_nConn,"select TETIPEVE,TETIPDIR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TETIPEVE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TETIPEVE',trim(this.w_TIPEVE))
          select TETIPEVE,TETIPDIR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TETIPEVE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPEVE)==trim(_Link_.TETIPEVE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPEVE) and !this.bDontReportError
            deferred_cp_zoom('TIPEVENT','*','TETIPEVE',cp_AbsName(oSource.parent,'oTIPEVE_1_333'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TETIPDIR";
                     +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',oSource.xKey(1))
            select TETIPEVE,TETIPDIR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPEVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TETIPDIR";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_TIPEVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_TIPEVE)
            select TETIPEVE,TETIPDIR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPEVE = NVL(_Link_.TETIPEVE,space(10))
      this.w_TIPDIR = NVL(_Link_.TETIPDIR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPEVE = space(10)
      endif
      this.w_TIPDIR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPEVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATLISACQ
  func Link_2_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATLISACQ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ATLISACQ)+"%");
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);

          i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSVALLIS,LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSVALLIS',this.w_ATCODVAL;
                     ,'LSCODLIS',trim(this.w_ATLISACQ))
          select LSVALLIS,LSCODLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSVALLIS,LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATLISACQ)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATLISACQ) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(oSource.parent,'oATLISACQ_2_49'),i_cWhere,'',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODVAL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LSVALLIS,LSCODLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSFLSCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, listino inesistente o di tipo Iva inclusa o di tipo sconti")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',oSource.xKey(1);
                       ,'LSCODLIS',oSource.xKey(2))
            select LSVALLIS,LSCODLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATLISACQ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ATLISACQ);
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_ATCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',this.w_ATCODVAL;
                       ,'LSCODLIS',this.w_ATLISACQ)
            select LSVALLIS,LSCODLIS,LSIVALIS,LSDTINVA,LSDTOBSO,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATLISACQ = NVL(_Link_.LSCODLIS,space(5))
      this.w_IVAACQ = NVL(_Link_.LSIVALIS,space(1))
      this.w_VALACQ = NVL(_Link_.LSVALLIS,space(3))
      this.w_INIACQ = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINACQ = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_FLSCOAC = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ATLISACQ = space(5)
      endif
      this.w_IVAACQ = space(1)
      this.w_VALACQ = space(3)
      this.w_INIACQ = ctod("  /  /  ")
      this.w_FINACQ = ctod("  /  /  ")
      this.w_FLSCOAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Isahe() or (.w_FLSCOAC <>'S' AND .w_IVAACQ<>'L' AND CHKLISD(.w_ATLISACQ,.w_IVAACQ,.w_VALACQ,.w_INIACQ,.w_FINLIS,'N', .w_ATCODVAL, .w_DATINI) )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, listino inesistente o di tipo Iva inclusa o di tipo sconti")
        endif
        this.w_ATLISACQ = space(5)
        this.w_IVAACQ = space(1)
        this.w_VALACQ = space(3)
        this.w_INIACQ = ctod("  /  /  ")
        this.w_FINACQ = ctod("  /  /  ")
        this.w_FLSCOAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSVALLIS,1)+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATLISACQ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCON
  func Link_1_396(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCFLCOSE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUCON)
            select CCCODICE,CCFLCOSE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_FLCOSE = NVL(_Link_.CCFLCOSE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCON = space(5)
      endif
      this.w_FLCOSE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oATCAUATT_1_10.value==this.w_ATCAUATT)
      this.oPgFrm.Page1.oPag.oATCAUATT_1_10.value=this.w_ATCAUATT
    endif
    if not(this.oPgFrm.Page1.oPag.oATOGGETT_1_15.value==this.w_ATOGGETT)
      this.oPgFrm.Page1.oPag.oATOGGETT_1_15.value=this.w_ATOGGETT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_18.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_18.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oORAINI_1_20.value==this.w_ORAINI)
      this.oPgFrm.Page1.oPag.oORAINI_1_20.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_21.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_21.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_23.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_23.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oORAFIN_1_24.value==this.w_ORAFIN)
      this.oPgFrm.Page1.oPag.oORAFIN_1_24.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_26.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_26.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oATOPPOFF_1_27.value==this.w_ATOPPOFF)
      this.oPgFrm.Page1.oPag.oATOPPOFF_1_27.value=this.w_ATOPPOFF
    endif
    if not(this.oPgFrm.Page1.oPag.oATSTATUS_1_28.RadioValue()==this.w_ATSTATUS)
      this.oPgFrm.Page1.oPag.oATSTATUS_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATSTATUS_1_29.RadioValue()==this.w_ATSTATUS)
      this.oPgFrm.Page1.oPag.oATSTATUS_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATNUMPRI_1_30.RadioValue()==this.w_ATNUMPRI)
      this.oPgFrm.Page1.oPag.oATNUMPRI_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATPERCOM_1_31.value==this.w_ATPERCOM)
      this.oPgFrm.Page1.oPag.oATPERCOM_1_31.value=this.w_ATPERCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oATFLNOTI_1_32.RadioValue()==this.w_ATFLNOTI)
      this.oPgFrm.Page1.oPag.oATFLNOTI_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATGGPREA_1_33.value==this.w_ATGGPREA)
      this.oPgFrm.Page1.oPag.oATGGPREA_1_33.value=this.w_ATGGPREA
    endif
    if not(this.oPgFrm.Page1.oPag.oATFLPROM_1_34.RadioValue()==this.w_ATFLPROM)
      this.oPgFrm.Page1.oPag.oATFLPROM_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAPRO_1_35.value==this.w_DATAPRO)
      this.oPgFrm.Page1.oPag.oDATAPRO_1_35.value=this.w_DATAPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oORAPRO_1_36.value==this.w_ORAPRO)
      this.oPgFrm.Page1.oPag.oORAPRO_1_36.value=this.w_ORAPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oMINPRO_1_37.value==this.w_MINPRO)
      this.oPgFrm.Page1.oPag.oMINPRO_1_37.value=this.w_MINPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oATNUMDOC_1_39.value==this.w_ATNUMDOC)
      this.oPgFrm.Page1.oPag.oATNUMDOC_1_39.value=this.w_ATNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oATALFDOC_1_41.value==this.w_ATALFDOC)
      this.oPgFrm.Page1.oPag.oATALFDOC_1_41.value=this.w_ATALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oATNOTPIA_1_43.value==this.w_ATNOTPIA)
      this.oPgFrm.Page1.oPag.oATNOTPIA_1_43.value=this.w_ATNOTPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oATDATDOC_1_44.value==this.w_ATDATDOC)
      this.oPgFrm.Page1.oPag.oATDATDOC_1_44.value=this.w_ATDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oATSTAATT_1_45.RadioValue()==this.w_ATSTAATT)
      this.oPgFrm.Page1.oPag.oATSTAATT_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATFLATRI_1_46.RadioValue()==this.w_ATFLATRI)
      this.oPgFrm.Page1.oPag.oATFLATRI_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODATT_1_48.value==this.w_ATCODATT)
      this.oPgFrm.Page1.oPag.oATCODATT_1_48.value=this.w_ATCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIPOL_1_49.value==this.w_DESTIPOL)
      this.oPgFrm.Page1.oPag.oDESTIPOL_1_49.value=this.w_DESTIPOL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATRIN_1_53.value==this.w_DATRIN)
      this.oPgFrm.Page1.oPag.oDATRIN_1_53.value=this.w_DATRIN
    endif
    if not(this.oPgFrm.Page1.oPag.oORARIN_1_54.value==this.w_ORARIN)
      this.oPgFrm.Page1.oPag.oORARIN_1_54.value=this.w_ORARIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINRIN_1_55.value==this.w_MINRIN)
      this.oPgFrm.Page1.oPag.oMINRIN_1_55.value=this.w_MINRIN
    endif
    if not(this.oPgFrm.Page1.oPag.oATTIPRIS_1_56.RadioValue()==this.w_ATTIPRIS)
      this.oPgFrm.Page1.oPag.oATTIPRIS_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODPRA_1_68.value==this.w_ATCODPRA)
      this.oPgFrm.Page1.oPag.oATCODPRA_1_68.value=this.w_ATCODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODPRA_1_69.value==this.w_ATCODPRA)
      this.oPgFrm.Page1.oPag.oATCODPRA_1_69.value=this.w_ATCODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oATCENCOS_1_70.value==this.w_ATCENCOS)
      this.oPgFrm.Page1.oPag.oATCENCOS_1_70.value=this.w_ATCENCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oATPUBWEB_1_71.RadioValue()==this.w_ATPUBWEB)
      this.oPgFrm.Page1.oPag.oATPUBWEB_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTARIC_1_72.RadioValue()==this.w_STARIC)
      this.oPgFrm.Page1.oPag.oSTARIC_1_72.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGEN_ATT_1_73.RadioValue()==this.w_GEN_ATT)
      this.oPgFrm.Page1.oPag.oGEN_ATT_1_73.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODBUN_1_76.value==this.w_ATCODBUN)
      this.oPgFrm.Page1.oPag.oATCODBUN_1_76.value=this.w_ATCODBUN
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOINI_1_85.value==this.w_GIOINI)
      this.oPgFrm.Page1.oPag.oGIOINI_1_85.value=this.w_GIOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODNOM_1_90.value==this.w_ATCODNOM)
      this.oPgFrm.Page1.oPag.oATCODNOM_1_90.value=this.w_ATCODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oATCONTAT_1_92.value==this.w_ATCONTAT)
      this.oPgFrm.Page1.oPag.oATCONTAT_1_92.value=this.w_ATCONTAT
    endif
    if not(this.oPgFrm.Page1.oPag.oATPERSON_1_95.value==this.w_ATPERSON)
      this.oPgFrm.Page1.oPag.oATPERSON_1_95.value=this.w_ATPERSON
    endif
    if not(this.oPgFrm.Page1.oPag.oFlCreaPers_1_96.RadioValue()==this.w_FlCreaPers)
      this.oPgFrm.Page1.oPag.oFlCreaPers_1_96.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNewCodPer_1_97.value==this.w_NewCodPer)
      this.oPgFrm.Page1.oPag.oNewCodPer_1_97.value=this.w_NewCodPer
    endif
    if not(this.oPgFrm.Page1.oPag.oATTELEFO_1_99.value==this.w_ATTELEFO)
      this.oPgFrm.Page1.oPag.oATTELEFO_1_99.value=this.w_ATTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oATCELLUL_1_100.value==this.w_ATCELLUL)
      this.oPgFrm.Page1.oPag.oATCELLUL_1_100.value=this.w_ATCELLUL
    endif
    if not(this.oPgFrm.Page1.oPag.oAT_EMAIL_1_101.value==this.w_AT_EMAIL)
      this.oPgFrm.Page1.oPag.oAT_EMAIL_1_101.value=this.w_AT_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oATLOCALI_1_102.value==this.w_ATLOCALI)
      this.oPgFrm.Page1.oPag.oATLOCALI_1_102.value=this.w_ATLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oAT__ENTE_1_103.RadioValue()==this.w_AT__ENTE)
      this.oPgFrm.Page1.oPag.oAT__ENTE_1_103.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATUFFICI_1_104.RadioValue()==this.w_ATUFFICI)
      this.oPgFrm.Page1.oPag.oATUFFICI_1_104.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAT_EMPEC_1_105.value==this.w_AT_EMPEC)
      this.oPgFrm.Page1.oPag.oAT_EMPEC_1_105.value=this.w_AT_EMPEC
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODSED_1_106.value==this.w_ATCODSED)
      this.oPgFrm.Page1.oPag.oATCODSED_1_106.value=this.w_ATCODSED
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMDES_1_107.value==this.w_NOMDES)
      this.oPgFrm.Page1.oPag.oNOMDES_1_107.value=this.w_NOMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oGIORIN_1_129.value==this.w_GIORIN)
      this.oPgFrm.Page1.oPag.oGIORIN_1_129.value=this.w_GIORIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_137.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_137.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOPRV_1_139.value==this.w_GIOPRV)
      this.oPgFrm.Page1.oPag.oGIOPRV_1_139.value=this.w_GIOPRV
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOFIN_1_150.value==this.w_GIOFIN)
      this.oPgFrm.Page1.oPag.oGIOFIN_1_150.value=this.w_GIOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCARAGGST_1_160.RadioValue()==this.w_CARAGGST)
      this.oPgFrm.Page1.oPag.oCARAGGST_1_160.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATPRV_1_209.value==this.w_DATPRV)
      this.oPgFrm.Page1.oPag.oDATPRV_1_209.value=this.w_DATPRV
    endif
    if not(this.oPgFrm.Page1.oPag.oORAPRV_1_210.value==this.w_ORAPRV)
      this.oPgFrm.Page1.oPag.oORAPRV_1_210.value=this.w_ORAPRV
    endif
    if not(this.oPgFrm.Page1.oPag.oMINPRV_1_211.value==this.w_MINPRV)
      this.oPgFrm.Page1.oPag.oMINPRV_1_211.value=this.w_MINPRV
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOPRM_1_227.value==this.w_GIOPRM)
      this.oPgFrm.Page1.oPag.oGIOPRM_1_227.value=this.w_GIOPRM
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA_1_236.value==this.w_CCDESPIA)
      this.oPgFrm.Page1.oPag.oCCDESPIA_1_236.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page2.oPag.oATCODVAL_2_2.value==this.w_ATCODVAL)
      this.oPgFrm.Page2.oPag.oATCODVAL_2_2.value=this.w_ATCODVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oATCODLIS_2_3.value==this.w_ATCODLIS)
      this.oPgFrm.Page2.oPag.oATCODLIS_2_3.value=this.w_ATCODLIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVAL_2_6.value==this.w_DESVAL)
      this.oPgFrm.Page2.oPag.oDESVAL_2_6.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oPARASS_2_7.value==this.w_PARASS)
      this.oPgFrm.Page2.oPag.oPARASS_2_7.value=this.w_PARASS
    endif
    if not(this.oPgFrm.Page2.oPag.oFLAMPA_2_8.RadioValue()==this.w_FLAMPA)
      this.oPgFrm.Page2.oPag.oFLAMPA_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATSTATUS_2_10.RadioValue()==this.w_ATSTATUS)
      this.oPgFrm.Page2.oPag.oATSTATUS_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATSTATUS_2_12.RadioValue()==this.w_ATSTATUS)
      this.oPgFrm.Page2.oPag.oATSTATUS_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATFLVALO_2_15.RadioValue()==this.w_ATFLVALO)
      this.oPgFrm.Page2.oPag.oATFLVALO_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATIMPORT_2_16.value==this.w_ATIMPORT)
      this.oPgFrm.Page2.oPag.oATIMPORT_2_16.value=this.w_ATIMPORT
    endif
    if not(this.oPgFrm.Page2.oPag.oATCALDIR_2_17.RadioValue()==this.w_ATCALDIR)
      this.oPgFrm.Page2.oPag.oATCALDIR_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oATCOECAL_2_18.value==this.w_ATCOECAL)
      this.oPgFrm.Page2.oPag.oATCOECAL_2_18.value=this.w_ATCOECAL
    endif
    if not(this.oPgFrm.Page2.oPag.odespra_2_24.value==this.w_despra)
      this.oPgFrm.Page2.oPag.odespra_2_24.value=this.w_despra
    endif
    if not(this.oPgFrm.Page2.oPag.ocodpra_2_26.value==this.w_codpra)
      this.oPgFrm.Page2.oPag.ocodpra_2_26.value=this.w_codpra
    endif
    if not(this.oPgFrm.Page3.oPag.odespra_3_18.value==this.w_despra)
      this.oPgFrm.Page3.oPag.odespra_3_18.value=this.w_despra
    endif
    if not(this.oPgFrm.Page3.oPag.ocodpra_3_20.value==this.w_codpra)
      this.oPgFrm.Page3.oPag.ocodpra_3_20.value=this.w_codpra
    endif
    if not(this.oPgFrm.Page3.oPag.odatpre_3_22.value==this.w_datpre)
      this.oPgFrm.Page3.oPag.odatpre_3_22.value=this.w_datpre
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLI_1_251.value==this.w_CODCLI)
      this.oPgFrm.Page1.oPag.oCODCLI_1_251.value=this.w_CODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oIDRICH_1_330.value==this.w_IDRICH)
      this.oPgFrm.Page1.oPag.oIDRICH_1_330.value=this.w_IDRICH
    endif
    if not(this.oPgFrm.Page1.oPag.oATSEREVE_1_331.value==this.w_ATSEREVE)
      this.oPgFrm.Page1.oPag.oATSEREVE_1_331.value=this.w_ATSEREVE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPEVE_1_333.RadioValue()==this.w_TIPEVE)
      this.oPgFrm.Page1.oPag.oTIPEVE_1_333.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDesNomin_1_367.value==this.w_DesNomin)
      this.oPgFrm.Page1.oPag.oDesNomin_1_367.value=this.w_DesNomin
    endif
    if not(this.oPgFrm.Page2.oPag.oATLISACQ_2_49.value==this.w_ATLISACQ)
      this.oPgFrm.Page2.oPag.oATLISACQ_2_49.value=this.w_ATLISACQ
    endif
    if not(this.oPgFrm.Page1.oPag.oGiudicePrat_1_394.value==this.w_GiudicePrat)
      this.oPgFrm.Page1.oPag.oGiudicePrat_1_394.value=this.w_GiudicePrat
    endif
    cp_SetControlsValueExtFlds(this,'OFF_ATTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ATCAUATT)) or not((.w_CARAGGST#'Z' OR .w_ActByTimer='S') AND (EMPTY(.w_CADTOBSO) OR .w_CADTOBSO>=.w_DTBSO_CAUMATTI)))  and (.w_CFUNC='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCAUATT_1_10.SetFocus()
            i_bnoObbl = !empty(.w_ATCAUATT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   (empty(.w_ATOGGETT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATOGGETT_1_15.SetFocus()
            i_bnoObbl = !empty(.w_ATOGGETT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ORAINI)) or not(VAL(.w_ORAINI) < 24))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAINI_1_20.SetFocus()
            i_bnoObbl = !empty(.w_ORAINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   ((empty(.w_MININI)) or not(VAL(.w_MININI) < 60))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_21.SetFocus()
            i_bnoObbl = !empty(.w_MININI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   (empty(.w_DATFIN))  and (!.w_TIMER)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_23.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ORAFIN)) or not(VAL(.w_ORAFIN) < 24))  and (!.w_TIMER)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAFIN_1_24.SetFocus()
            i_bnoObbl = !empty(.w_ORAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   ((empty(.w_MINFIN)) or not(VAL(.w_MINFIN) < 60))  and (!.w_TIMER)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_26.SetFocus()
            i_bnoObbl = !empty(.w_MINFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_ATPERCOM <= 100 AND .w_ATPERCOM >= 0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATPERCOM_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore non inferiore a 0 e non superiore a 100")
          case   (empty(.w_DATAPRO))  and not(g_JBSH<>'S')  and (.w_ATFLPROM='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAPRO_1_35.SetFocus()
            i_bnoObbl = !empty(.w_DATAPRO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ORAPRO)) or not(VAL(.w_ORAPRO) < 24 AND IIF(!EMPTY(.w_DATAPRO) AND !EMPTY(.w_ORAPRO) AND !EMPTY(.w_MINPRO),cp_CharToDateTime( ALLTRIM(STR(DAY(.w_DATAPRO)))+'-'+ALLTRIM(STR(MONTH(.w_DATAPRO)))+'-'+ALLTR(STR(YEAR(.w_DATAPRO)))+' '+iif(empty(.w_ORAPRO),'00', .w_ORAPRO)+':'+iif(empty(.w_MINPRO),'00', .w_MINPRO), 'dd-mm-yyyy hh:nn:ss')<=cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  ')),.T.)))  and not(g_JBSH<>'S')  and (.w_ATFLPROM='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAPRO_1_36.SetFocus()
            i_bnoObbl = !empty(.w_ORAPRO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23 ed inferiore all'ora di inizio attivit�")
          case   ((empty(.w_MINPRO)) or not(VAL(.w_MINPRO) < 60 AND IIF(!EMPTY(.w_DATAPRO) AND !EMPTY(.w_ORAPRO) AND !EMPTY(.w_MINPRO),cp_CharToDateTime( ALLTRIM(STR(DAY(.w_DATAPRO)))+'-'+ALLTRIM(STR(MONTH(.w_DATAPRO)))+'-'+ALLTR(STR(YEAR(.w_DATAPRO)))+' '+.w_ORAPRO+':'+.w_MINPRO, 'dd-mm-yyyy hh:nn:ss')<=cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  ')),.T.)))  and not(g_JBSH<>'S')  and (.w_ATFLPROM='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINPRO_1_37.SetFocus()
            i_bnoObbl = !empty(.w_MINPRO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59 ed inferiore all'ora di inizio attivit�")
          case   (empty(.w_ATDATDOC))  and not(.w_FLNSAP='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATDATDOC_1_44.SetFocus()
            i_bnoObbl = !empty(.w_ATDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_ATSTATUS='P' AND NOT EMPTY(.w_ATCODPRA)) OR .w_ATSTATUS<>'P') AND (.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)))  and not(.w_CFUNC<>'Load' OR (EMPTY(.w_CACHKOBB) AND !(EMPTY(.w_ATCAUATT) AND !EMPTY(.w_ATCODPRA))) or ! .w_ISALT )  and (.w_CFUNC='Load' AND NOT EMPTY(.w_CACHKOBB))  and not(empty(.w_ATCODPRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODPRA_1_68.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_ATSTATUS='P' AND NOT EMPTY(.w_ATCODPRA)) OR .w_ATSTATUS<>'P') AND (.w_CFUNC<>'Load' OR .w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)))  and not(.w_CFUNC='Load' OR (EMPTY(.w_CACHKOBB) AND !(EMPTY(.w_ATCAUATT) AND !EMPTY(.w_ATCODPRA))) or  ! .w_ISALT )  and (.w_CFUNC<>'Load' AND NOT EMPTY(.w_CACHKOBB))  and not(empty(.w_ATCODPRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODPRA_1_69.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO) and isahe()) or (!isahe() AND CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.)))  and not(NOT (g_COAN='S' AND (.w_FLDANA='S' OR .w_FLANAL='S')) or  ! .w_ISALT or .w_FLANAL='N')  and not(empty(.w_ATCENCOS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCENCOS_1_70.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Centro di ricavo incongruente o obsoleto")
          case   (empty(.w_ATCODBUN))  and not(!isahe())  and (g_PERBUN='T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODBUN_1_76.SetFocus()
            i_bnoObbl = !empty(.w_ATCODBUN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_DATOBSONOM>.w_OFDATDOC OR EMPTY(.w_DATOBSONOM)) AND .w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and not(EMPTY(.w_CACHKNOM))  and not(empty(.w_ATCODNOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODNOM_1_90.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_ControlloPers))  and not(EMPTY(.w_CACHKNOM) or !(.cFunction='Load') OR .w_FlCreaPers='N')  and (.w_FlCreaPers='S')  and not(empty(.w_NewCodPer))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNewCodPer_1_97.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare un codice persona diverso")
          case   not(.w_TIPRIF='CO')  and not(EMPTY(.w_CACHKNOM) or .w_ISALT)  and (.w_TIPNOM='C')  and not(empty(.w_ATCODSED))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODSED_1_106.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
          case   not(VAL(.w_ORAPRV) < 24)  and not(!.w_ISALT)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORAPRV_1_210.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINPRV) < 60)  and not(!.w_ISALT)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINPRV_1_211.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   (empty(.w_ATCODVAL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCODVAL_2_2.SetFocus()
            i_bnoObbl = !empty(.w_ATCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Isahe() or .w_FLNSAP="S" or CHKLISD(.w_ATCODLIS,.w_IVALIS,.w_VALLIS,.w_INILIS,.w_FINLIS,.w_FLSCOR, .w_ATCODVAL, .w_DATINI) )  and not(Isahe())  and not(empty(.w_ATCODLIS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATCODLIS_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, listino non valido")
          case   not(Isahe() or (.w_FLSCOAC <>'S' AND .w_IVAACQ<>'L' AND CHKLISD(.w_ATLISACQ,.w_IVAACQ,.w_VALACQ,.w_INIACQ,.w_FINLIS,'N', .w_ATCODVAL, .w_DATINI) ))  and not(! Isahr())  and not(empty(.w_ATLISACQ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oATLISACQ_2_49.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, listino inesistente o di tipo Iva inclusa o di tipo sconti")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAG_MPA.CheckForm()
      if i_bres
        i_bres=  .GSAG_MPA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAG_MCP.CheckForm()
      if i_bres
        i_bres=  .GSAG_MCP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAG_MDA.CheckForm()
      if i_bres
        i_bres=  .GSAG_MDA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .gsag_mdd.CheckForm()
      if i_bres
        i_bres=  .gsag_mdd.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsag_aat
      * --- Controlli Finali
      IF i_bRes
         IF .w_TIMER
           * ---Se il timer attivo lo fermo simulando la pressione del bottone
           local oBtn
           oBtn = this.GetCtrl("LOEDVOSBAB")
           oBtn.Click()
         ENDIF
         .w_RESCHK=0
         Ah_Msg('Controlli finali...',.T.)
         .NotifyEvent('ChkFinali')
         WAIT CLEAR
         IF .w_RESCHK<>0
           This.w_CLOCK.Playstoptime()
            IF .w_CA_TIMER="S"
             This.w_TIMER=.T.
            ENDIF
           This.mEnablecontrols()
           i_bRes=.f.
         ENDIF
      ENDIF
      
      IF i_bRes
          * -- Notifica l'evento per eseguire la cancellazione
          * -- delle prestazioni con importo a zero e A valore.
          .NotifyEvent('CancPrest')
          WAIT CLEAR
          IF .w_RESCHK<>0
            i_bRes=.f.
          ENDIF
      ENDIF
      
      *--- Disattivo generazione in caso di fallimento checkform
      fl_gen_att = IIF(i_bRes, fl_gen_att, 'N')
      
      if ((vartype(this.w_MaskCalend)='O' AND upper(this.w_MaskCalend.CLASS)='TGSAG_KAG') AND !(.w_PACHKFES='N' OR Chkfestivi(.w_DATINI,.w_PACHKFES)))
         Ah_Msg('Attenzione, giorno festivo',.T.)
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LetturaParAgen = this.w_LetturaParAgen
    this.o_ATSERIAL = this.w_ATSERIAL
    this.o_ATCAUATT = this.w_ATCAUATT
    this.o_CA_TIMER = this.w_CA_TIMER
    this.o_ATDATINI = this.w_ATDATINI
    this.o_CADTIN = this.w_CADTIN
    this.o_DATINI = this.w_DATINI
    this.o_ORAINI = this.w_ORAINI
    this.o_MININI = this.w_MININI
    this.o_ATDATFIN = this.w_ATDATFIN
    this.o_DATFIN = this.w_DATFIN
    this.o_ORAFIN = this.w_ORAFIN
    this.o_ATANNDOC = this.w_ATANNDOC
    this.o_MINFIN = this.w_MINFIN
    this.o_ATSTATUS = this.w_ATSTATUS
    this.o_ATPERCOM = this.w_ATPERCOM
    this.o_ATFLPROM = this.w_ATFLPROM
    this.o_DATAPRO = this.w_DATAPRO
    this.o_ORAPRO = this.w_ORAPRO
    this.o_MINPRO = this.w_MINPRO
    this.o_ATDATDOC = this.w_ATDATDOC
    this.o_ATFLATRI = this.w_ATFLATRI
    this.o_ATCODATT = this.w_ATCODATT
    this.o_ATDATRIN = this.w_ATDATRIN
    this.o_DATRIN = this.w_DATRIN
    this.o_ORARIN = this.w_ORARIN
    this.o_MINRIN = this.w_MINRIN
    this.o_ATCAUDOC = this.w_ATCAUDOC
    this.o_ATCAUACQ = this.w_ATCAUACQ
    this.o_ATCODPRA = this.w_ATCODPRA
    this.o_ATCENCOS = this.w_ATCENCOS
    this.o_ATDATPRO = this.w_ATDATPRO
    this.o_ATCODNOM = this.w_ATCODNOM
    this.o_ATCONTAT = this.w_ATCONTAT
    this.o_ATCODSED = this.w_ATCODSED
    this.o_NOMDES = this.w_NOMDES
    this.o_ATEVANNO = this.w_ATEVANNO
    this.o_CACHKOBB = this.w_CACHKOBB
    this.o_ATDURORE = this.w_ATDURORE
    this.o_ATDURMIN = this.w_ATDURMIN
    this.o_PRENTTIP = this.w_PRENTTIP
    this.o_DATPRV = this.w_DATPRV
    this.o_ORAPRV = this.w_ORAPRV
    this.o_MINPRV = this.w_MINPRV
    this.o_TIPENT = this.w_TIPENT
    this.o_ATCODVAL = this.w_ATCODVAL
    this.o_ATCODLIS = this.w_ATCODLIS
    this.o_TipoRiso = this.w_TipoRiso
    this.o_SERIAL = this.w_SERIAL
    this.o_ROWNUM = this.w_ROWNUM
    this.o_CODCLI = this.w_CODCLI
    this.o_PAFLVISI = this.w_PAFLVISI
    this.o_ATTPROSC = this.w_ATTPROSC
    this.o_ATTPROLI = this.w_ATTPROLI
    this.o_ATTSCLIS = this.w_ATTSCLIS
    this.o_ATTCOLIS = this.w_ATTCOLIS
    this.o_ATCENRIC = this.w_ATCENRIC
    this.o_NEWID = this.w_NEWID
    this.o_DesNomin = this.w_DesNomin
    * --- GSAG_MPA : Depends On
    this.GSAG_MPA.SaveDependsOn()
    * --- GSAG_MCP : Depends On
    this.GSAG_MCP.SaveDependsOn()
    * --- GSAG_MDA : Depends On
    this.GSAG_MDA.SaveDependsOn()
    * --- gsag_mdd : Depends On
    this.gsag_mdd.SaveDependsOn()
    return

  func CanView()
    local i_res
    i_res=this.w_bNoCheckRis Or IsRisAtt( this.w_ATSERIAL , this.w_ATFLATRI, this ) 
    if !i_res
      this.BlankRec()
      this.SetControlsValue()
      cp_ErrorMsg(thisform.msgFmt("Attivit� riservata impossibile accedere."))
    endif
    return(i_res)
  func CanEdit()
    local i_res
    i_res=Not Empty(  this.w_ATSERIAL )
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attivit� riservata impossibile modificare."))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=GSMA_BAE(this,'G')
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione annullata"))
    endif
    return(i_res)
  func deleteWarn()
    local i_res
    i_res = EMPTY(NVL(this.w_SERPIAN,''))
    if !i_res
      i_res=cp_YesNo(thisform.msgFmt("Attenzione: attivit� generata da piano di generazione attivit� %w_SERPIAN% %w_ACAPO%La cancellazione manuale di attivit� non comporta gli aggiornamenti automatici delle date prossima attivit� sugli elementi contratto.%w_ACAPO%Confermi ugualmente"),'?')
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsag_aatPag1 as StdContainer
  Width  = 809
  height = 573
  stdWidth  = 809
  stdheight = 573
  resizeXpos=311
  resizeYpos=306
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oATCAUATT_1_10 as StdField with uid="YQSGOVWGCH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ATCAUATT", cQueryName = "ATCAUATT",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Tipo attivit� al fine di una compilazione automatica della stessa",;
    HelpContextID = 235626842,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=79, Top=8, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_ATCAUATT"

  func oATCAUATT_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFUNC='Load')
    endwith
   endif
  endfunc

  proc oATCAUATT_1_10.mAfter
    with this.Parent.oContained
      .w_CARAGGST= iif(empty(.w_ATCAUATT), space(1), .w_CARAGGST)
    endwith
  endproc

  func oATCAUATT_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCAUATT_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCAUATT_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oATCAUATT_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivita",''+iif(NOT IsAlt(),"GSAG1AAT", "GSAG3AAT")+'.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oATCAUATT_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_ATCAUATT
     i_obj.ecpSave()
  endproc

  add object oATOGGETT_1_15 as StdField with uid="KFKFSNIJLA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ATOGGETT", cQueryName = "ATOGGETT",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto dell'attivit�",;
    HelpContextID = 20062554,;
   bGlobalFont=.t.,;
    Height=21, Width=437, Left=249, Top=8, InputMask=replicate('X',254)

  proc oATOGGETT_1_15.mBefore
    with this.Parent.oContained
      this.cQueryName = "ATSERIAL,ATOGGETT"
    endwith
  endproc

  add object oDATINI_1_18 as StdField with uid="QXSPXYXHLS",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio attivit�",;
    HelpContextID = 173777354,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=119, Top=39

  proc oDATINI_1_18.mAfter
    with this.Parent.oContained
      .w_FORZADTI=.F.
    endwith
  endproc

  func oDATINI_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_PACHKFES='N' OR Chkfestivi(.w_DATINI,.w_PACHKFES))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oORAINI_1_20 as StdField with uid="IFZETBJSDN",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "ORAINI",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora inizio attivit�",;
    HelpContextID = 173850650,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=249, Top=39, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  proc oORAINI_1_20.mAfter
    with this.Parent.oContained
      .w_FORZADTI=.F.
    endwith
  endproc

  func oORAINI_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_21 as StdField with uid="BMAZHAYBOV",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti inizio attivit�",;
    HelpContextID = 173799738,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=282, Top=39, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  proc oMININI_1_21.mAfter
    with this.Parent.oContained
      .w_FORZADTI=.F.
    endwith
  endproc

  func oMININI_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_23 as StdField with uid="MWGRFCSLAO",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine attivit�",;
    HelpContextID = 95330762,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=119, Top=70

  func oDATFIN_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_TIMER)
    endwith
   endif
  endfunc

  proc oDATFIN_1_23.mAfter
    with this.Parent.oContained
      .w_FORZADTF=.F.
    endwith
  endproc

  func oDATFIN_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !((.w_PACHKFES='N' OR Chkfestivi(.w_DATFIN,.w_PACHKFES)))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oORAFIN_1_24 as StdField with uid="PBNKIXNQKG",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "ORAFIN",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora fine attivit�",;
    HelpContextID = 95404058,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=249, Top=70, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAFIN_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_TIMER)
    endwith
   endif
  endfunc

  proc oORAFIN_1_24.mAfter
    with this.Parent.oContained
      .w_FORZADTF=.F.
    endwith
  endproc

  func oORAFIN_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_26 as StdField with uid="IFSBCOBBWT",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti fine attivit�",;
    HelpContextID = 95353146,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=282, Top=70, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_TIMER)
    endwith
   endif
  endfunc

  proc oMINFIN_1_26.mAfter
    with this.Parent.oContained
      .w_FORZADTF=.F.
    endwith
  endproc

  func oMINFIN_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc

  add object oATOPPOFF_1_27 as StdField with uid="XZXEOEIUAS",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ATOPPOFF", cQueryName = "ATOPPOFF",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 197861708,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=394, Top=306, InputMask=replicate('X',10), cLinkFile="OFF_ERTE", oKey_1_1="OFSERIAL", oKey_1_2="this.w_ATOPPOFF"

  func oATOPPOFF_1_27.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  func oATOPPOFF_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oATOPPOFF_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oATSTATUS_1_28 as StdCombo with uid="ERRFIJARMC",rtseq=27,rtrep=.f.,left=372,top=39,width=116,height=21;
    , ToolTipText = "Stato da assegnare all'attivit�";
    , HelpContextID = 266297689;
    , cFormVar="w_ATSTATUS",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa,"+"Completata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATSTATUS_1_28.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oATSTATUS_1_28.GetRadio()
    this.Parent.oContained.w_ATSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oATSTATUS_1_28.SetRadio()
    this.Parent.oContained.w_ATSTATUS=trim(this.Parent.oContained.w_ATSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_ATSTATUS=='T',1,;
      iif(this.Parent.oContained.w_ATSTATUS=='D',2,;
      iif(this.Parent.oContained.w_ATSTATUS=='I',3,;
      iif(this.Parent.oContained.w_ATSTATUS=='F',4,;
      iif(this.Parent.oContained.w_ATSTATUS=='P',5,;
      0)))))
  endfunc

  func oATSTATUS_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP<>'S')
    endwith
   endif
  endfunc

  func oATSTATUS_1_28.mHide()
    with this.Parent.oContained
      return (.w_FLNSAP='S')
    endwith
  endfunc


  add object oATSTATUS_1_29 as StdCombo with uid="MGRBZGPSKC",rtseq=28,rtrep=.f.,left=372,top=39,width=116,height=21;
    , ToolTipText = "Stato da assegnare all'attivit�";
    , HelpContextID = 266297689;
    , cFormVar="w_ATSTATUS",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATSTATUS_1_29.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    space(1))))))
  endfunc
  func oATSTATUS_1_29.GetRadio()
    this.Parent.oContained.w_ATSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oATSTATUS_1_29.SetRadio()
    this.Parent.oContained.w_ATSTATUS=trim(this.Parent.oContained.w_ATSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_ATSTATUS=='T',1,;
      iif(this.Parent.oContained.w_ATSTATUS=='D',2,;
      iif(this.Parent.oContained.w_ATSTATUS=='I',3,;
      iif(this.Parent.oContained.w_ATSTATUS=='F',4,;
      0))))
  endfunc

  func oATSTATUS_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP='S')
    endwith
   endif
  endfunc

  func oATSTATUS_1_29.mHide()
    with this.Parent.oContained
      return (.w_FLNSAP<>'S')
    endwith
  endfunc


  add object oATNUMPRI_1_30 as StdCombo with uid="OLRHIUHGZO",rtseq=29,rtrep=.f.,left=371,top=70,width=116,height=21;
    , ToolTipText = "Priorit� da assegnare all'attivit� (normale, urgente, scadenza termine=da fare in un determinato giorno ad un'ora prestabilita)";
    , HelpContextID = 211816783;
    , cFormVar="w_ATNUMPRI",RowSource=""+"Normale,"+"Urgente,"+"Scadenza termine", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATNUMPRI_1_30.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,3,;
    iif(this.value =3,4,;
    0))))
  endfunc
  func oATNUMPRI_1_30.GetRadio()
    this.Parent.oContained.w_ATNUMPRI = this.RadioValue()
    return .t.
  endfunc

  func oATNUMPRI_1_30.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ATNUMPRI==1,1,;
      iif(this.Parent.oContained.w_ATNUMPRI==3,2,;
      iif(this.Parent.oContained.w_ATNUMPRI==4,3,;
      0)))
  endfunc

  add object oATPERCOM_1_31 as StdField with uid="UKQAIHPZWR",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ATPERCOM", cQueryName = "ATPERCOM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore non inferiore a 0 e non superiore a 100",;
    ToolTipText = "% di completamento dell'attivit�",;
    HelpContextID = 2084525,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=509, Top=39, cSayPict='"999"', cGetPict='"999"'

  func oATPERCOM_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ATPERCOM <= 100 AND .w_ATPERCOM >= 0)
    endwith
    return bRes
  endfunc


  add object oATFLNOTI_1_32 as StdCombo with uid="KPPGARVYNC",rtseq=31,rtrep=.f.,left=545,top=39,width=118,height=21;
    , ToolTipText = "Permette di inviare una e-mail o un post-in ai partecipanti dell'attivit� in creazione o in cancellazione dell'attivit�, in modifica della data oppure in modifica, aggiunta o cancellazione di partecipanti";
    , HelpContextID = 195465551;
    , cFormVar="w_ATFLNOTI",RowSource=""+"Nessun avviso,"+"Avviso,"+"Avviso con VCS,"+"Avviso con ICS,"+"Avviso con VCS e ICS", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATFLNOTI_1_32.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    iif(this.value =4,'I',;
    iif(this.value =5,'E',;
    space(1)))))))
  endfunc
  func oATFLNOTI_1_32.GetRadio()
    this.Parent.oContained.w_ATFLNOTI = this.RadioValue()
    return .t.
  endfunc

  func oATFLNOTI_1_32.SetRadio()
    this.Parent.oContained.w_ATFLNOTI=trim(this.Parent.oContained.w_ATFLNOTI)
    this.value = ;
      iif(this.Parent.oContained.w_ATFLNOTI=='N',1,;
      iif(this.Parent.oContained.w_ATFLNOTI=='S',2,;
      iif(this.Parent.oContained.w_ATFLNOTI=='C',3,;
      iif(this.Parent.oContained.w_ATFLNOTI=='I',4,;
      iif(this.Parent.oContained.w_ATFLNOTI=='E',5,;
      0)))))
  endfunc

  add object oATGGPREA_1_33 as StdField with uid="OZSGMZXQRC",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ATGGPREA", cQueryName = "ATGGPREA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni di preavviso prima della scadenza",;
    HelpContextID = 247570759,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=768, Top=39, cSayPict='"999"', cGetPict='"999"'

  add object oATFLPROM_1_34 as StdCheck with uid="HSEXDEJXVK",rtseq=33,rtrep=.f.,left=491, top=72, caption="Promemoria",;
    ToolTipText = "Se attivo, abilita la gestione dei promemoria",;
    HelpContextID = 20541101,;
    cFormVar="w_ATFLPROM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATFLPROM_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oATFLPROM_1_34.GetRadio()
    this.Parent.oContained.w_ATFLPROM = this.RadioValue()
    return .t.
  endfunc

  func oATFLPROM_1_34.SetRadio()
    this.Parent.oContained.w_ATFLPROM=trim(this.Parent.oContained.w_ATFLPROM)
    this.value = ;
      iif(this.Parent.oContained.w_ATFLPROM=='S',1,;
      0)
  endfunc

  func oATFLPROM_1_34.mHide()
    with this.Parent.oContained
      return (g_JBSH<>'S')
    endwith
  endfunc

  add object oDATAPRO_1_35 as StdField with uid="XQMEDUWBMS",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DATAPRO", cQueryName = "DATAPRO",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data promemoria",;
    HelpContextID = 21209546,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=621, Top=70

  func oDATAPRO_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATFLPROM='S')
    endwith
   endif
  endfunc

  func oDATAPRO_1_35.mHide()
    with this.Parent.oContained
      return (g_JBSH<>'S')
    endwith
  endfunc

  func oDATAPRO_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_DATAPRO<=.w_DATINI AND IIF(!EMPTY(.w_DATAPRO) AND !EMPTY(.w_ORAPRO) AND !EMPTY(.w_MINPRO),cp_CharToDateTime( ALLTRIM(STR(DAY(.w_DATAPRO)))+'-'+ALLTRIM(STR(MONTH(.w_DATAPRO)))+'-'+ALLTR(STR(YEAR(.w_DATAPRO)))+' '+.w_ORAPRO+':'+.w_MINPRO, 'dd-mm-yyyy hh:nn:ss')<=cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  ')),.T.) AND (.w_PACHKFES='N' OR Chkfestivi(.w_DATAPRO,.w_PACHKFES)))
         bRes=(cp_WarningMsg(thisform.msgFmt("Impostare un valore inferiore alla data di inizio attivit�")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oORAPRO_1_36 as StdField with uid="TWVCQEVEXZ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_ORAPRO", cQueryName = "ORAPRO",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23 ed inferiore all'ora di inizio attivit�",;
    ToolTipText = "Ora promemoria",;
    HelpContextID = 68534298,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=744, Top=70, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAPRO_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATFLPROM='S')
    endwith
   endif
  endfunc

  func oORAPRO_1_36.mHide()
    with this.Parent.oContained
      return (g_JBSH<>'S')
    endwith
  endfunc

  func oORAPRO_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAPRO) < 24 AND IIF(!EMPTY(.w_DATAPRO) AND !EMPTY(.w_ORAPRO) AND !EMPTY(.w_MINPRO),cp_CharToDateTime( ALLTRIM(STR(DAY(.w_DATAPRO)))+'-'+ALLTRIM(STR(MONTH(.w_DATAPRO)))+'-'+ALLTR(STR(YEAR(.w_DATAPRO)))+' '+iif(empty(.w_ORAPRO),'00', .w_ORAPRO)+':'+iif(empty(.w_MINPRO),'00', .w_MINPRO), 'dd-mm-yyyy hh:nn:ss')<=cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  ')),.T.))
    endwith
    return bRes
  endfunc

  add object oMINPRO_1_37 as StdField with uid="VEYPJJGXRE",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MINPRO", cQueryName = "MINPRO",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59 ed inferiore all'ora di inizio attivit�",;
    ToolTipText = "Minuti promemoria",;
    HelpContextID = 68483386,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=776, Top=70, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINPRO_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATFLPROM='S')
    endwith
   endif
  endfunc

  func oMINPRO_1_37.mHide()
    with this.Parent.oContained
      return (g_JBSH<>'S')
    endwith
  endfunc

  func oMINPRO_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINPRO) < 60 AND IIF(!EMPTY(.w_DATAPRO) AND !EMPTY(.w_ORAPRO) AND !EMPTY(.w_MINPRO),cp_CharToDateTime( ALLTRIM(STR(DAY(.w_DATAPRO)))+'-'+ALLTRIM(STR(MONTH(.w_DATAPRO)))+'-'+ALLTR(STR(YEAR(.w_DATAPRO)))+' '+.w_ORAPRO+':'+.w_MINPRO, 'dd-mm-yyyy hh:nn:ss')<=cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  ')),.T.))
    endwith
    return bRes
  endfunc

  add object oATNUMDOC_1_39 as StdField with uid="YXHCGCKRSQ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_ATNUMDOC", cQueryName = "ATNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'attivit�",;
    HelpContextID = 257945271,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=79, Top=101, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oATNUMDOC_1_39.mHide()
    with this.Parent.oContained
      return (.w_ISALT OR .w_FLPDOC='N' OR EMPTY(.w_ATCAUATT))
    endwith
  endfunc

  add object oATALFDOC_1_41 as StdField with uid="ZTOBVTOCJO",rtseq=39,rtrep=.f.,;
    cFormVar = "w_ATALFDOC", cQueryName = "ATALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie dell'attivit�",;
    HelpContextID = 265928375,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=225, Top=101, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  func oATALFDOC_1_41.mHide()
    with this.Parent.oContained
      return (.w_ISALT OR .w_FLPDOC='N' OR EMPTY(.w_ATCAUATT))
    endwith
  endfunc

  add object oATNOTPIA_1_43 as StdMemo with uid="HMIECCCEWZ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_ATNOTPIA", cQueryName = "ATNOTPIA",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note dell'attivit�",;
    HelpContextID = 49671865,;
   bGlobalFont=.t.,;
    Height=69, Width=272, Left=82, Top=134

  add object oATDATDOC_1_44 as StdField with uid="RFLOCJAVKJ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_ATDATDOC", cQueryName = "ATDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 251956919,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=726, Top=134

  func oATDATDOC_1_44.mHide()
    with this.Parent.oContained
      return (.w_FLNSAP='S')
    endwith
  endfunc


  add object oATSTAATT_1_45 as StdCombo with uid="HAOGFHYTTF",rtseq=43,rtrep=.f.,left=450,top=159,width=114,height=21;
    , ToolTipText = "Disponibilit�";
    , HelpContextID = 215966042;
    , cFormVar="w_ATSTAATT",RowSource=""+"Occupato,"+"Fuori sede,"+"Per urgenze,"+"Libero", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATSTAATT_1_45.RadioValue()
    return(iif(this.value =1,2,;
    iif(this.value =2,4,;
    iif(this.value =3,3,;
    iif(this.value =4,1,;
    0)))))
  endfunc
  func oATSTAATT_1_45.GetRadio()
    this.Parent.oContained.w_ATSTAATT = this.RadioValue()
    return .t.
  endfunc

  func oATSTAATT_1_45.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ATSTAATT==2,1,;
      iif(this.Parent.oContained.w_ATSTAATT==4,2,;
      iif(this.Parent.oContained.w_ATSTAATT==3,3,;
      iif(this.Parent.oContained.w_ATSTAATT==1,4,;
      0))))
  endfunc

  func oATSTAATT_1_45.mHide()
    with this.Parent.oContained
      return (.w_CARAGGST $ 'MD')
    endwith
  endfunc


  add object oATFLATRI_1_46 as StdCombo with uid="LSXODQYDOG",rtseq=44,rtrep=.f.,left=664,top=159,width=138,height=21;
    , ToolTipText = "Attributo che determina la visibilit� dell'attivit�";
    , HelpContextID = 265720143;
    , cFormVar="w_ATFLATRI",RowSource=""+"Riservata,"+"Libera,"+"Nascosta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oATFLATRI_1_46.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'H',;
    space(1)))))
  endfunc
  func oATFLATRI_1_46.GetRadio()
    this.Parent.oContained.w_ATFLATRI = this.RadioValue()
    return .t.
  endfunc

  func oATFLATRI_1_46.SetRadio()
    this.Parent.oContained.w_ATFLATRI=trim(this.Parent.oContained.w_ATFLATRI)
    this.value = ;
      iif(this.Parent.oContained.w_ATFLATRI=='S',1,;
      iif(this.Parent.oContained.w_ATFLATRI=='N',2,;
      iif(this.Parent.oContained.w_ATFLATRI=='H',3,;
      0)))
  endfunc

  func oATFLATRI_1_46.mHide()
    with this.Parent.oContained
      return (.w_GESRIS<>'S')
    endwith
  endfunc


  add object oBtn_1_47 as StdButton with uid="TIBGUGRRAH",left=82, top=209, width=48,height=45,;
    CpPicture="bmp\explode.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare dati di testata";
    , HelpContextID = 135889354;
    , Caption='\<Dati';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      do GSAG_KDT with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISALT)
     endwith
    endif
  endfunc

  add object oATCODATT_1_48 as StdField with uid="DPJUXQSURM",rtseq=45,rtrep=.f.,;
    cFormVar = "w_ATCODATT", cQueryName = "ATCODATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipologia attivit�",;
    HelpContextID = 218718554,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=450, Top=184, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="OFFTIPAT", cZoomOnZoom="GSAG_ATA", oKey_1_1="TACODTIP", oKey_1_2="this.w_ATCODATT"

  func oATCODATT_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODATT_1_48.ecpDrop(oSource)
    this.Parent.oContained.link_1_48('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODATT_1_48.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFFTIPAT','*','TACODTIP',cp_AbsName(this.parent,'oATCODATT_1_48'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_ATA',"Tipologie attivit�",'',this.parent.oContained
  endproc
  proc oATCODATT_1_48.mZoomOnZoom
    local i_obj
    i_obj=GSAG_ATA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TACODTIP=this.parent.oContained.w_ATCODATT
     i_obj.ecpSave()
  endproc

  add object oDESTIPOL_1_49 as StdField with uid="CXLJNTENUW",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESTIPOL", cQueryName = "DESTIPOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 60861822,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=522, Top=184, InputMask=replicate('X',50)

  add object oDATRIN_1_53 as StdField with uid="OIWXZASVYX",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DATRIN", cQueryName = "DATRIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data rinvio",;
    HelpContextID = 94544330,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=413, Top=101

  func oDATRIN_1_53.mHide()
    with this.Parent.oContained
      return (NOT(.w_ATSTATUS $ 'FPT') OR .w_FLRINV#'S' OR !.w_ISALT)
    endwith
  endfunc

  add object oORARIN_1_54 as StdField with uid="PDESJDNVGA",rtseq=51,rtrep=.f.,;
    cFormVar = "w_ORARIN", cQueryName = "ORARIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora rinvio",;
    HelpContextID = 94617626,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=540, Top=101, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORARIN_1_54.mHide()
    with this.Parent.oContained
      return (NOT(.w_ATSTATUS $ 'FPT') OR .w_FLRINV#'S' OR !.w_ISALT)
    endwith
  endfunc

  add object oMINRIN_1_55 as StdField with uid="DGIUHJHMIJ",rtseq=52,rtrep=.f.,;
    cFormVar = "w_MINRIN", cQueryName = "MINRIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti rinvio",;
    HelpContextID = 94566714,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=576, Top=101, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINRIN_1_55.mHide()
    with this.Parent.oContained
      return (NOT(.w_ATSTATUS $ 'FPT') OR .w_FLRINV#'S' OR !.w_ISALT)
    endwith
  endfunc


  add object oATTIPRIS_1_56 as StdTableCombo with uid="JWWLYAUCQA",rtseq=53,rtrep=.f.,left=711,top=101,width=91,height=21;
    , ToolTipText = "Tipo riserva";
    , HelpContextID = 20680359;
    , cFormVar="w_ATTIPRIS",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="TIP_RISE";
    , cTable='TIP_RISE',cKey='TRCODICE',cValue='TRDESTIP',cOrderBy='TRDESTIP',xDefault=space(1);
  , bGlobalFont=.t.


  func oATTIPRIS_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATSTATUS $ 'FP')
    endwith
   endif
  endfunc

  func oATTIPRIS_1_56.mHide()
    with this.Parent.oContained
      return (NOT(.w_ATSTATUS $ 'FP') OR .w_FLTRIS#'S' OR NOT EMPTY(.w_DATRIN))
    endwith
  endfunc

  func oATTIPRIS_1_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_56('Part',this)
    endwith
    return bRes
  endfunc

  proc oATTIPRIS_1_56.ecpDrop(oSource)
    this.Parent.oContained.link_1_56('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oATCODPRA_1_68 as StdField with uid="MWXUZUWBBD",rtseq=60,rtrep=.f.,;
    cFormVar = "w_ATCODPRA", cQueryName = "ATCODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 201941319,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=82, Top=207, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_BZZ", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ATCODPRA"

  func oATCODPRA_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFUNC='Load' AND NOT EMPTY(.w_CACHKOBB))
    endwith
   endif
  endfunc

  func oATCODPRA_1_68.mHide()
    with this.Parent.oContained
      return (.w_CFUNC<>'Load' OR (EMPTY(.w_CACHKOBB) AND !(EMPTY(.w_ATCAUATT) AND !EMPTY(.w_ATCODPRA))) or ! .w_ISALT )
    endwith
  endfunc

  func oATCODPRA_1_68.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_68('Part',this)
      if bRes and !( (.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) OR ! Isalt() )
         bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oATCODPRA_1_68.ecpDrop(oSource)
    this.Parent.oContained.link_1_68('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODPRA_1_68.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oATCODPRA_1_68'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSAG_AAT.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oATCODPRA_1_68.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_ATCODPRA
     i_obj.ecpSave()
  endproc

  add object oATCODPRA_1_69 as StdField with uid="CYATJEEEDX",rtseq=61,rtrep=.f.,;
    cFormVar = "w_ATCODPRA", cQueryName = "ATCODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 201941319,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=82, Top=207, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_BZZ", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ATCODPRA"

  func oATCODPRA_1_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CFUNC<>'Load' AND NOT EMPTY(.w_CACHKOBB))
    endwith
   endif
  endfunc

  func oATCODPRA_1_69.mHide()
    with this.Parent.oContained
      return (.w_CFUNC='Load' OR (EMPTY(.w_CACHKOBB) AND !(EMPTY(.w_ATCAUATT) AND !EMPTY(.w_ATCODPRA))) or  ! .w_ISALT )
    endwith
  endfunc

  func oATCODPRA_1_69.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_69('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODPRA_1_69.ecpDrop(oSource)
    this.Parent.oContained.link_1_69('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODPRA_1_69.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oATCODPRA_1_69'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZZ',""+IIF(IsAlt(),"Pratiche","Commesse")+"",''+iif(NOT IsAlt(),"default", "GSAG_AAT")+'.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oATCODPRA_1_69.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_ATCODPRA
     i_obj.ecpSave()
  endproc

  add object oATCENCOS_1_70 as StdField with uid="XMAJYYJCZO",rtseq=62,rtrep=.f.,;
    cFormVar = "w_ATCENCOS", cQueryName = "ATCENCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Centro di ricavo incongruente o obsoleto",;
    HelpContextID = 6332071,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=82, Top=231, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ATCENCOS"

  func oATCENCOS_1_70.mHide()
    with this.Parent.oContained
      return (NOT (g_COAN='S' AND (.w_FLDANA='S' OR .w_FLANAL='S')) or  ! .w_ISALT or .w_FLANAL='N')
    endwith
  endfunc

  func oATCENCOS_1_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_70('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCENCOS_1_70.ecpDrop(oSource)
    this.Parent.oContained.link_1_70('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCENCOS_1_70.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oATCENCOS_1_70'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oATCENCOS_1_70.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ATCENCOS
     i_obj.ecpSave()
  endproc

  add object oATPUBWEB_1_71 as StdCheck with uid="MVEDNCBAWC",rtseq=63,rtrep=.f.,left=611, top=233, caption="Pubblica su Web",;
    ToolTipText = "Se attivo, l'attivit� sar� pubblicata su Web",;
    HelpContextID = 49295688,;
    cFormVar="w_ATPUBWEB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATPUBWEB_1_71.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oATPUBWEB_1_71.GetRadio()
    this.Parent.oContained.w_ATPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oATPUBWEB_1_71.SetRadio()
    this.Parent.oContained.w_ATPUBWEB=trim(this.Parent.oContained.w_ATPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_ATPUBWEB=='S',1,;
      0)
  endfunc

  func oATPUBWEB_1_71.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc


  add object oSTARIC_1_72 as StdCombo with uid="NNSKIDFEAC",rtseq=64,rtrep=.f.,left=236,top=233,width=107,height=21;
    , ToolTipText = "Stato della richiesta  aperto, in corso, in attesa, chiuso";
    , HelpContextID = 10730970;
    , cFormVar="w_STARIC",RowSource=""+"Aperta,"+"In corso,"+"In attesa,"+"Chiusa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTARIC_1_72.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'I',;
    iif(this.value =3,'S',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oSTARIC_1_72.GetRadio()
    this.Parent.oContained.w_STARIC = this.RadioValue()
    return .t.
  endfunc

  func oSTARIC_1_72.SetRadio()
    this.Parent.oContained.w_STARIC=trim(this.Parent.oContained.w_STARIC)
    this.value = ;
      iif(this.Parent.oContained.w_STARIC=='A',1,;
      iif(this.Parent.oContained.w_STARIC=='I',2,;
      iif(this.Parent.oContained.w_STARIC=='S',3,;
      iif(this.Parent.oContained.w_STARIC=='C',4,;
      0))))
  endfunc

  func oSTARIC_1_72.mHide()
    with this.Parent.oContained
      return ((g_AGFA <>'S'  OR .w_ISALT) OR (Empty(.w_IDRICH)))
    endwith
  endfunc


  add object oGEN_ATT_1_73 as StdCombo with uid="JPNDWASQLN",rtseq=65,rtrep=.f.,left=713,top=211,width=89,height=21;
    , ToolTipText = "Alla conferma permette di generare un'attivit� collegata o attivit� ricorrenti";
    , HelpContextID = 266994278;
    , cFormVar="w_GEN_ATT",RowSource=""+"Collegata,"+"Ricorrenti,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGEN_ATT_1_73.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'R',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oGEN_ATT_1_73.GetRadio()
    this.Parent.oContained.w_GEN_ATT = this.RadioValue()
    return .t.
  endfunc

  func oGEN_ATT_1_73.SetRadio()
    this.Parent.oContained.w_GEN_ATT=trim(this.Parent.oContained.w_GEN_ATT)
    this.value = ;
      iif(this.Parent.oContained.w_GEN_ATT=='S',1,;
      iif(this.Parent.oContained.w_GEN_ATT=='R',2,;
      iif(this.Parent.oContained.w_GEN_ATT=='N',3,;
      0)))
  endfunc

  func oGEN_ATT_1_73.mHide()
    with this.Parent.oContained
      return (NOT(.cFunction='Load'))
    endwith
  endfunc

  add object oATCODBUN_1_76 as StdField with uid="OKLOHGFQAA",rtseq=66,rtrep=.t.,;
    cFormVar = "w_ATCODBUN", cQueryName = "ATCODBUN",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Business Unit di appartenenza",;
    HelpContextID = 235495764,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=450, Top=208, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", oKey_1_1="BUCODAZI", oKey_1_2="this.w_READAZI", oKey_2_1="BUCODICE", oKey_2_2="this.w_ATCODBUN"

  func oATCODBUN_1_76.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERBUN='T')
    endwith
   endif
  endfunc

  func oATCODBUN_1_76.mHide()
    with this.Parent.oContained
      return (!isahe())
    endwith
  endfunc

  func oATCODBUN_1_76.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_76('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODBUN_1_76.ecpDrop(oSource)
    this.Parent.oContained.link_1_76('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODBUN_1_76.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_READAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_READAZI)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oATCODBUN_1_76'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oLinkPC_1_78 as stdDynamicChildContainer with uid="FBJHUVWJZT",left=31, top=263, width=663, height=75, bOnScreen=.t.;



  add object oBtn_1_79 as StdButton with uid="IEFMPACXSW",left=704, top=260, width=48,height=45,;
    CpPicture="bmp\Genera.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per generare una o piu' attivit� ricorrenti";
    , HelpContextID = 17555492;
    , Caption='\<Ricorrenza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_79.Click()
      do GSAG_KAR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_79.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' AND NOT EMPTY(.w_ATCAUATT) and (EMPTY(.w_ATCAITER) or .w_ATFLRICO='S'))
      endwith
    endif
  endfunc

  func oBtn_1_79.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(.cFunction='Query' AND NOT EMPTY(.w_ATCAUATT) and (EMPTY(.w_ATCAITER) or .w_ATFLRICO='S')) or .w_DATINI<> .w_DATFIN)
     endwith
    endif
  endfunc


  add object oObj_1_83 as cp_runprogram with uid="MCZGDKJYAD",left=567, top=578, width=221,height=26,;
    caption='GSAG_BAP',;
   bGlobalFont=.t.,;
    prg="GSAG_BAP('1')",;
    cEvent = "ActivatePage 2",;
    nPag=1;
    , HelpContextID = 5160778


  add object oObj_1_84 as cp_runprogram with uid="DIANRSLJRI",left=713, top=649, width=221,height=26,;
    caption='GSAG_BAP',;
   bGlobalFont=.t.,;
    prg="GSAG_BAP('2')",;
    cEvent = "w_ATCODPRA Changed",;
    nPag=1;
    , HelpContextID = 5160778

  add object oGIOINI_1_85 as StdField with uid="FDLKJPVEHW",rtseq=67,rtrep=.f.,;
    cFormVar = "w_GIOINI", cQueryName = "GIOINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 173795738,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=79, Top=39, InputMask=replicate('X',10)

  add object oATCODNOM_1_90 as StdField with uid="UTACBYLWZX",rtseq=69,rtrep=.f.,;
    cFormVar = "w_ATCODNOM", cQueryName = "ATCODNOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 100048557,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=82, Top=356, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_ATCODNOM"

  func oATCODNOM_1_90.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  proc oATCODNOM_1_90.mAfter
    with this.Parent.oContained
      .w_ATCONTAT=''
    endwith
  endproc

  func oATCODNOM_1_90.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_90('Part',this)
      if .not. empty(.w_ATCONTAT)
        bRes2=.link_1_92('Full')
      endif
      if .not. empty(.w_NewCodPer)
        bRes2=.link_1_97('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oATCODNOM_1_90.ecpDrop(oSource)
    this.Parent.oContained.link_1_90('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODNOM_1_90.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oATCODNOM_1_90'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oATCODNOM_1_90.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_ATCODNOM
     i_obj.ecpSave()
  endproc

  add object oATCONTAT_1_92 as StdField with uid="BBGZZDKGHP",rtseq=71,rtrep=.f.,;
    cFormVar = "w_ATCONTAT", cQueryName = "ATCONTAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona del nominativo",;
    HelpContextID = 11100506,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=82, Top=381, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="NOM_CONT", oKey_1_1="NCCODICE", oKey_1_2="this.w_ATCODNOM", oKey_2_1="NCCODCON", oKey_2_2="this.w_ATCONTAT"

  func oATCONTAT_1_92.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_ATCODNOM))
    endwith
   endif
  endfunc

  func oATCONTAT_1_92.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM) OR .w_FlCreaPers='S')
    endwith
  endfunc

  func oATCONTAT_1_92.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_92('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCONTAT_1_92.ecpDrop(oSource)
    this.Parent.oContained.link_1_92('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCONTAT_1_92.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.NOM_CONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_ATCODNOM)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStr(this.Parent.oContained.w_ATCODNOM)
    endif
    do cp_zoom with 'NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(this.parent,'oATCONTAT_1_92'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Persone",'',this.parent.oContained
  endproc

  add object oATPERSON_1_95 as StdField with uid="IGFNYJIELV",rtseq=72,rtrep=.f.,;
    cFormVar = "w_ATPERSON", cQueryName = "ATPERSON",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento persona",;
    HelpContextID = 2084524,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=224, Top=381, InputMask=replicate('X',60)

  func oATPERSON_1_95.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  add object oFlCreaPers_1_96 as StdCheck with uid="PKFURTLTRM",rtseq=73,rtrep=.f.,left=613, top=381, caption="Crea la persona",;
    ToolTipText = "Se attivo, alla conferma dell'attivit� salva i dati in una nuova persona del nominativo",;
    HelpContextID = 255652827,;
    cFormVar="w_FlCreaPers", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFlCreaPers_1_96.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFlCreaPers_1_96.GetRadio()
    this.Parent.oContained.w_FlCreaPers = this.RadioValue()
    return .t.
  endfunc

  func oFlCreaPers_1_96.SetRadio()
    this.Parent.oContained.w_FlCreaPers=trim(this.Parent.oContained.w_FlCreaPers)
    this.value = ;
      iif(this.Parent.oContained.w_FlCreaPers=='S',1,;
      0)
  endfunc

  func oFlCreaPers_1_96.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_ATCODNOM) AND EMPTY(.w_ATCONTAT))
    endwith
   endif
  endfunc

  func oFlCreaPers_1_96.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM) or !(.cFunction='Load'))
    endwith
  endfunc

  add object oNewCodPer_1_97 as StdField with uid="KWOWRGLKQC",rtseq=74,rtrep=.f.,;
    cFormVar = "w_NewCodPer", cQueryName = "NewCodPer",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Specificare un codice persona diverso",;
    ToolTipText = "Codice da assegnare alla persona creata",;
    HelpContextID = 45136475,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=82, Top=381, InputMask=replicate('X',5), cLinkFile="NOM_CONT", cZoomOnZoom="GSAR_MCN", oKey_1_1="NCCODICE", oKey_1_2="this.w_ATCODNOM", oKey_2_1="NCCODCON", oKey_2_2="this.w_NewCodPer"

  func oNewCodPer_1_97.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FlCreaPers='S')
    endwith
   endif
  endfunc

  func oNewCodPer_1_97.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM) or !(.cFunction='Load') OR .w_FlCreaPers='N')
    endwith
  endfunc

  proc oNewCodPer_1_97.mAfter
    with this.Parent.oContained
      .w_FlagPosiz='S'
    endwith
  endproc

  func oNewCodPer_1_97.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_97('Part',this)
    endwith
    return bRes
  endfunc

  proc oNewCodPer_1_97.ecpDrop(oSource)
    this.Parent.oContained.link_1_97('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oATTELEFO_1_99 as StdField with uid="CPNQCURMBK",rtseq=75,rtrep=.f.,;
    cFormVar = "w_ATTELEFO", cQueryName = "ATTELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Telefono",;
    HelpContextID = 25194837,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=82, Top=406, InputMask=replicate('X',18)

  func oATTELEFO_1_99.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  proc oATTELEFO_1_99.mBefore
    with this.Parent.oContained
      GSAG_BCO(This.Parent.oContained,'P')
    endwith
  endproc

  add object oATCELLUL_1_100 as StdField with uid="KAKFAJGFTW",rtseq=76,rtrep=.f.,;
    cFormVar = "w_ATCELLUL", cQueryName = "ATCELLUL",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Cellulare",;
    HelpContextID = 142565714,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=283, Top=406, InputMask=replicate('X',18)

  func oATCELLUL_1_100.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  add object oAT_EMAIL_1_101 as StdMemo with uid="YJZPELDPAQ",rtseq=77,rtrep=.f.,;
    cFormVar = "w_AT_EMAIL", cQueryName = "AT_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo email",;
    HelpContextID = 227615058,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=493, Top=406

  func oAT_EMAIL_1_101.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  add object oATLOCALI_1_102 as StdField with uid="ZKUQUAJHSM",rtseq=78,rtrep=.f.,;
    cFormVar = "w_ATLOCALI", cQueryName = "ATLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� in cui si svolge l'attivit�",;
    HelpContextID = 50728625,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=82, Top=431, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oATLOCALI_1_102.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_ATLOCALI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oAT__ENTE_1_103 as StdTableComboEnti with uid="ATGDRWEVNU",rtseq=79,rtrep=.f.,left=82,top=456,width=251,height=21;
    , ToolTipText = "Codice ente/ufficio giudiziario";
    , HelpContextID = 170598731;
    , cFormVar="w_AT__ENTE",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="PRA_ENTI";
    , cTable='QUERY\PRAENTIZOOM.VQR',cKey='EPCODICE',cValue='EPDESCRI',cOrderBy='',xDefault=space(10);
  , bGlobalFont=.t.


  func oAT__ENTE_1_103.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
   endif
  endfunc

  func oAT__ENTE_1_103.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  func oAT__ENTE_1_103.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_103('Part',this)
    endwith
    return bRes
  endfunc

  proc oAT__ENTE_1_103.ecpDrop(oSource)
    this.Parent.oContained.link_1_103('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oATUFFICI_1_104 as StdTableCombo with uid="ZXNWGVXISZ",rtseq=80,rtrep=.f.,left=493,top=456,width=251,height=21;
    , ToolTipText = "Codice ufficio/sezione del foro";
    , HelpContextID = 86081871;
    , cFormVar="w_ATUFFICI",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="PRA_UFFI";
    , cTable='PRA_UFFI',cKey='UFCODICE',cValue='UFDESCRI',cOrderBy='UFDESCRI',xDefault=space(10);
  , bGlobalFont=.t.


  func oATUFFICI_1_104.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ISALT)
    endwith
   endif
  endfunc

  func oATUFFICI_1_104.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  func oATUFFICI_1_104.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_104('Part',this)
    endwith
    return bRes
  endfunc

  proc oATUFFICI_1_104.ecpDrop(oSource)
    this.Parent.oContained.link_1_104('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oAT_EMPEC_1_105 as StdMemo with uid="BCICKQHKYV",rtseq=81,rtrep=.f.,;
    cFormVar = "w_AT_EMPEC", cQueryName = "AT_EMPEC",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo email PEC",;
    HelpContextID = 210837833,;
   bGlobalFont=.t.,;
    Height=21, Width=291, Left=493, Top=431

  func oAT_EMPEC_1_105.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  add object oATCODSED_1_106 as StdField with uid="ICQGXJIXQE",rtseq=82,rtrep=.f.,;
    cFormVar = "w_ATCODSED", cQueryName = "ATCODSED",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice destinazione inesistente o di tipo diverso da consegna",;
    ToolTipText = "Codice sede",;
    HelpContextID = 252272970,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=82, Top=456, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_ATTIPCLI", oKey_2_1="DDCODICE", oKey_2_2="this.w_CODCLI", oKey_3_1="DDCODDES", oKey_3_2="this.w_ATCODSED"

  func oATCODSED_1_106.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPNOM='C')
    endwith
   endif
  endfunc

  func oATCODSED_1_106.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM) or .w_ISALT)
    endwith
  endfunc

  func oATCODSED_1_106.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_106('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODSED_1_106.ecpDrop(oSource)
    this.Parent.oContained.link_1_106('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODSED_1_106.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ATTIPCLI)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_ATTIPCLI)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CODCLI)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oATCODSED_1_106'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Sedi",'gsag_des.DES_DIVE_VZM',this.parent.oContained
  endproc

  add object oNOMDES_1_107 as StdField with uid="CVTHZVRZPM",rtseq=83,rtrep=.f.,;
    cFormVar = "w_NOMDES", cQueryName = "NOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 15794986,;
   bGlobalFont=.t.,;
    Height=21, Width=452, Left=144, Top=456, InputMask=replicate('X',40)

  func oNOMDES_1_107.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM) or .w_ISALT)
    endwith
  endfunc


  add object oLinkPC_1_108 as stdDynamicChildContainer with uid="SXLSNRZKRJ",left=31, top=481, width=620, height=92, bOnScreen=.t.;


  func oLinkPC_1_108.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPNOM='C')
      endwith
    endif
  endfunc

  func oLinkPC_1_108.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISALT)
     endwith
    endif
  endfunc


  add object oBtn_1_109 as StdButton with uid="UVCTHODNGY",left=705, top=481, width=48,height=45,;
    CpPicture="BMP\visualicat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza allegati";
    , HelpContextID = 100474768;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_109.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"OFF_ATTI",.w_ATSERIAL,"GSAG_AAT","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_109.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ATSERIAL) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_109.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISALT)
     endwith
    endif
  endfunc


  add object oBtn_1_110 as StdButton with uid="ANLYFEHJWV",left=657, top=481, width=48,height=45,;
    CpPicture="bmp\cattura.bmp", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Cattura un allegato da unit� disco";
    , HelpContextID = 21069350;
    , UID = 'CATTURA', disabledpicture = 'bmp\catturad.bmp', caption='\<Cattura';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_110.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"OFF_ATTI",.w_ATSERIAL,"GSAG_AAT","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_110.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ATSERIAL) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_110.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISALT)
     endwith
    endif
  endfunc


  add object oBtn_1_111 as StdButton with uid="GZXVTORMVD",left=657, top=527, width=48,height=45,;
    CpPicture="bmp\explode.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare dati offerte";
    , HelpContextID = 50439142;
    , Caption='\<Dati off.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_111.Click()
      do GSAG_KOF with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_111.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_OFFE='S')
      endwith
    endif
  endfunc

  func oBtn_1_111.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_OFFE<>'S'  or .w_ISALT)
     endwith
    endif
  endfunc


  add object oBtn_1_113 as StdButton with uid="LDHSBAFGBW",left=705, top=527, width=48,height=45,;
    CpPicture="bmp\visuali.ico", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare eventi associati";
    , HelpContextID = 137012038;
    , Caption='\<Eventi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_113.Click()
      do GSFA_KEA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_113.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.cFunction='Query' OR .cFunction='Edit') AND NOT EMPTY(.w_ATCAUATT) and !.w_ISALT and g_AGFA='S')
      endwith
    endif
  endfunc

  func oBtn_1_113.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (! ((.cFunction='Query' OR .cFunction='Edit') AND NOT EMPTY(.w_ATCAUATT) and !.w_ISALT and g_AGFA='S'))
     endwith
    endif
  endfunc

  add object oGIORIN_1_129 as StdField with uid="ZNMNSFZAEK",rtseq=94,rtrep=.f.,;
    cFormVar = "w_GIORIN", cQueryName = "GIORIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 94562714,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=371, Top=101, InputMask=replicate('X',10)

  func oGIORIN_1_129.mHide()
    with this.Parent.oContained
      return (NOT(.w_ATSTATUS $ 'FPT') OR .w_FLRINV#'S' OR !.w_ISALT)
    endwith
  endfunc

  add object oDESCAN_1_137 as StdField with uid="VBWZLHUZGU",rtseq=95,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto della pratica",;
    HelpContextID = 103919050,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=217, Top=207, InputMask=replicate('X',100)

  func oDESCAN_1_137.mHide()
    with this.Parent.oContained
      return ((EMPTY(.w_CACHKOBB) AND !(EMPTY(.w_ATCAUATT) AND !EMPTY(.w_ATCODPRA))) or ! .w_ISALT)
    endwith
  endfunc

  add object oGIOPRV_1_139 as StdField with uid="WQJEJPRRRJ",rtseq=96,rtrep=.f.,;
    cFormVar = "w_GIOPRV", cQueryName = "GIOPRV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 48961126,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=79, Top=101, InputMask=replicate('X',10)

  func oGIOPRV_1_139.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc


  add object oObj_1_142 as cp_runprogram with uid="UBTNPPFITF",left=-6, top=734, width=283,height=26,;
    caption='GSAG_BAD(T)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('T')",;
    cEvent = "CancPrest",;
    nPag=1;
    , HelpContextID = 4970710


  add object oObj_1_143 as cp_runprogram with uid="CVPSHPUDIA",left=-6, top=786, width=283,height=26,;
    caption='GSAG_BED(1)',;
   bGlobalFont=.t.,;
    prg="GSAG_BED('1')",;
    cEvent = "Delete start,Update start",;
    nPag=1;
    , HelpContextID = 263455786


  add object oObj_1_144 as cp_runprogram with uid="DSNNANEJXO",left=-6, top=812, width=283,height=26,;
    caption='GSAG_BED(2)',;
   bGlobalFont=.t.,;
    prg="GSAG_BED('2')",;
    cEvent = "w_ATSTATUS Changed",;
    nPag=1;
    , HelpContextID = 263456042


  add object oObj_1_145 as cp_runprogram with uid="CYORIVGKMT",left=-6, top=760, width=283,height=26,;
    caption='GSAG_BED(3)',;
   bGlobalFont=.t.,;
    prg="GSAG_BED('3')",;
    cEvent = "Record Inserted,Record Updated,Record Deleted",;
    nPag=1;
    , HelpContextID = 263456298

  add object oGIOFIN_1_150 as StdField with uid="GWEHUCPCHB",rtseq=99,rtrep=.f.,;
    cFormVar = "w_GIOFIN", cQueryName = "GIOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 95349146,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=79, Top=70, InputMask=replicate('X',10)


  add object oObj_1_151 as cp_runprogram with uid="HPHXRWFWXN",left=713, top=678, width=221,height=26,;
    caption='GSAG_BAP',;
   bGlobalFont=.t.,;
    prg="GSAG_BAP('9')",;
    cEvent = "Partecip",;
    nPag=1;
    , HelpContextID = 5160778


  add object oCARAGGST_1_160 as StdCombo with uid="VAAWEXCMBR",rtseq=103,rtrep=.f.,left=450,top=134,width=167,height=21, enabled=.f.;
    , HelpContextID = 53231226;
    , cFormVar="w_CARAGGST",RowSource=""+"Generico,"+"Udienze,"+"Appuntamento,"+"Cose da fare,"+"Note,"+"Sessione telefonica,"+"Assenze,"+"Da inserimento prestazioni", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCARAGGST_1_160.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'U',;
    iif(this.value =3,'S',;
    iif(this.value =4,'D',;
    iif(this.value =5,'M',;
    iif(this.value =6,'T',;
    iif(this.value =7,'A',;
    iif(this.value =8,'Z',;
    space(10))))))))))
  endfunc
  func oCARAGGST_1_160.GetRadio()
    this.Parent.oContained.w_CARAGGST = this.RadioValue()
    return .t.
  endfunc

  func oCARAGGST_1_160.SetRadio()
    this.Parent.oContained.w_CARAGGST=trim(this.Parent.oContained.w_CARAGGST)
    this.value = ;
      iif(this.Parent.oContained.w_CARAGGST=='G',1,;
      iif(this.Parent.oContained.w_CARAGGST=='U',2,;
      iif(this.Parent.oContained.w_CARAGGST=='S',3,;
      iif(this.Parent.oContained.w_CARAGGST=='D',4,;
      iif(this.Parent.oContained.w_CARAGGST=='M',5,;
      iif(this.Parent.oContained.w_CARAGGST=='T',6,;
      iif(this.Parent.oContained.w_CARAGGST=='A',7,;
      iif(this.Parent.oContained.w_CARAGGST=='Z',8,;
      0))))))))
  endfunc


  add object oObj_1_168 as cp_runprogram with uid="OBSKEOPXNY",left=-6, top=838, width=283,height=26,;
    caption='GSAG_BAD(M)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('M')",;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 4972502


  add object oObj_1_171 as cp_runprogram with uid="JIFQRABVKS",left=-6, top=864, width=283,height=26,;
    caption='GSAG_BAD(E)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('E')",;
    cEvent = "Record Deleted",;
    nPag=1;
    , HelpContextID = 4974550

  add object oDATPRV_1_209 as StdField with uid="UFCOKDPWGO",rtseq=139,rtrep=.f.,;
    cFormVar = "w_DATPRV", cQueryName = "DATPRV",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data provenienza",;
    HelpContextID = 48979510,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=119, Top=101

  func oDATPRV_1_209.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  func oDATPRV_1_209.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(IIF(.w_CFUNC='Load', .T., .w_PACHKFES='N' OR Chkfestivi(.w_DATPRV,.w_PACHKFES)))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oORAPRV_1_210 as StdField with uid="TKZIUDYLHV",rtseq=140,rtrep=.f.,;
    cFormVar = "w_ORAPRV", cQueryName = "ORAPRV",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora provenienza",;
    HelpContextID = 48906214,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=249, Top=101, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAPRV_1_210.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  func oORAPRV_1_210.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAPRV) < 24)
    endwith
    return bRes
  endfunc

  add object oMINPRV_1_211 as StdField with uid="MAZPJXUQNH",rtseq=141,rtrep=.f.,;
    cFormVar = "w_MINPRV", cQueryName = "MINPRV",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti provenienza",;
    HelpContextID = 48957126,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=282, Top=101, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINPRV_1_211.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  func oMINPRV_1_211.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINPRV) < 60)
    endwith
    return bRes
  endfunc


  add object oObj_1_212 as cp_runprogram with uid="ORRITPFUUC",left=713, top=705, width=221,height=26,;
    caption='GSAG_BAP',;
   bGlobalFont=.t.,;
    prg="GSAG_BAP('4')",;
    cEvent = "w_AT__ENTE Changed",;
    nPag=1;
    , HelpContextID = 5160778


  add object oBtn_1_222 as StdButton with uid="AUGLUSADWT",left=754, top=260, width=48,height=45,;
    CpPicture="bmp\Genera.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per generare una nuova attivit� collegata";
    , HelpContextID = 87624742;
    , Caption='\<Gen. att.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_222.Click()
      do GSAG_KCL with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_222.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' AND NOT EMPTY(.w_ATCAUATT))
      endwith
    endif
  endfunc

  func oBtn_1_222.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(.cFunction='Query' AND NOT EMPTY(.w_ATCAUATT)))
     endwith
    endif
  endfunc


  add object oBtn_1_223 as StdButton with uid="YCKNIOFYMC",left=754, top=308, width=48,height=45,;
    CpPicture="bmp\explode.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare le attivit� collegate";
    , HelpContextID = 87626474;
    , Caption='\<Collegate';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_223.Click()
      with this.Parent.oContained
        GSAG_BAD(this.Parent.oContained,"K")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_223.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' AND NOT EMPTY(.w_ATCAUATT) AND (not empty(.w_ATCAITER) OR .w_ATFLRICO='S'))
      endwith
    endif
  endfunc

  func oBtn_1_223.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(.cFunction='Query' AND NOT EMPTY(.w_ATCAUATT) AND (not empty(.w_ATCAITER) OR .w_ATFLRICO='S') ))
     endwith
    endif
  endfunc

  add object oGIOPRM_1_227 as StdField with uid="JPMOSKQXFU",rtseq=149,rtrep=.f.,;
    cFormVar = "w_GIOPRM", cQueryName = "GIOPRM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 102033818,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=582, Top=70, InputMask=replicate('X',10)

  func oGIOPRM_1_227.mHide()
    with this.Parent.oContained
      return (g_JBSH<>'S')
    endwith
  endfunc

  add object oCCDESPIA_1_236 as StdField with uid="QVKOSEQBBL",rtseq=153,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 51421081,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=217, Top=231, InputMask=replicate('X',40)

  func oCCDESPIA_1_236.mHide()
    with this.Parent.oContained
      return (NOT (g_COAN='S' AND (.w_FLDANA='S' OR .w_FLANAL='S')) or  ! .w_ISALT or .w_FLANAL='N')
    endwith
  endfunc


  add object oObj_1_244 as cp_runprogram with uid="YXOSCJYDXE",left=282, top=851, width=191,height=26,;
    caption='GSAG_BAD(N)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('N')",;
    cEvent = "w_ATCODNOM Changed, w_ATCODSED Changed",;
    nPag=1;
    , HelpContextID = 4972246

  add object oCODCLI_1_251 as StdField with uid="DAYQRIQUCS",rtseq=196,rtrep=.f.,;
    cFormVar = "w_CODCLI", cQueryName = "CODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 176329690,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=370, Top=306, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ATTIPCLI", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLI"

  func oCODCLI_1_251.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  func oCODCLI_1_251.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_251('Part',this)
      if .not. empty(.w_ATCODSED)
        bRes2=.link_1_106('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCLI_1_251.ecpDrop(oSource)
    this.Parent.oContained.link_1_251('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oObj_1_255 as cp_runprogram2 with uid="PSOQWGJLSI",left=475, top=849, width=253,height=19,;
    caption='GSAG_BAD(G)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('G')",;
    cEvent = "w_ATCENCOS Changed",;
    nPag=1;
    , HelpContextID = 4974038


  add object oObj_1_256 as cp_runprogram with uid="UOTXMBCBIG",left=475, top=831, width=253,height=19,;
    caption='GSAG_BAD(B)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('B')",;
    cEvent = "w_ATCODPRA Changed",;
    nPag=1;
    , HelpContextID = 4975318


  add object oObj_1_261 as cp_runprogram with uid="DRWVTMXNFE",left=-6, top=892, width=283,height=26,;
    caption='GSAG_BAD(Z)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('Z')",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , HelpContextID = 4969174


  add object oObj_1_285 as cp_runprogram with uid="APXVQQFCEF",left=475, top=885, width=253,height=19,;
    caption='GSAG_BAD(R)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('R')",;
    cEvent = "w_ATCOMRIC Changed",;
    nPag=1;
    , HelpContextID = 4971222


  add object oObj_1_286 as cp_runprogram with uid="ODNUADUCZU",left=475, top=867, width=253,height=19,;
    caption='GSAG_BAD(F)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('F')",;
    cEvent = "w_ATCENRIC Changed",;
    nPag=1;
    , HelpContextID = 4974294


  add object oObj_1_287 as cp_runprogram with uid="WSSOFOIPNW",left=733, top=863, width=253,height=19,;
    caption='GSAG_BAD(H)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('H')",;
    cEvent = "w_ATATTRIC Changed",;
    nPag=1;
    , HelpContextID = 4973782


  add object oObj_1_288 as cp_runprogram with uid="QUTTQGAUKE",left=733, top=845, width=253,height=19,;
    caption='GSAG_BAD(I)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('I')",;
    cEvent = "w_ATATTCOS Changed",;
    nPag=1;
    , HelpContextID = 4973526


  add object oObj_1_293 as cp_runprogram with uid="XVSJBZEUTR",left=733, top=881, width=253,height=19,;
    caption='GSAG_BM2(E)',;
   bGlobalFont=.t.,;
    prg="GSAG_BM2('E')",;
    cEvent = "w_ATCODVAL Changed, Edit Started, w_ATCAUATT Changed, w_ATDATDOC Changed,w_DATFIN Changed,w_ATCODNOM Changed,w_ATDATDOC Changed",;
    nPag=1;
    , HelpContextID = 4974568


  add object oObj_1_294 as cp_runprogram with uid="GETBPRLFVL",left=733, top=902, width=253,height=19,;
    caption='GSAG_BM2(F)',;
   bGlobalFont=.t.,;
    prg="GSAG_BM2('F')",;
    cEvent = "Edit Aborted,Record Updated,Record Inserted",;
    nPag=1;
    , HelpContextID = 4974312


  add object oObj_1_312 as cp_runprogram with uid="DJESPVDVFB",left=-4, top=579, width=283,height=26,;
    caption='GSAG_BAD(L)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('L')",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , HelpContextID = 4972758


  add object oBtn_1_327 as StdButton with uid="JMPPTWBRZQ",left=355, top=207, width=20,height=22,;
    caption="?", nPag=1;
    , ToolTipText = "Premere per selezionare ID richiesta";
    , HelpContextID = 143270682;
  , bGlobalFont=.t.

    proc oBtn_1_327.Click()
      do GSFA_KID with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_327.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (empty(.w_IDRICH))
      endwith
    endif
  endfunc

  func oBtn_1_327.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((g_AGFA <>'S' OR .w_ISALT)  OR not empty(.w_IDRICH))
     endwith
    endif
  endfunc


  add object oBtn_1_328 as StdButton with uid="OSSBCQXAVZ",left=376, top=207, width=20,height=22,;
    caption="+", nPag=1;
    , ToolTipText = "Nuovo ID richiesta";
    , HelpContextID = 143271002;
  , bGlobalFont=.t.

    proc oBtn_1_328.Click()
      this.parent.oContained.NotifyEvent("Progre")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_328.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Empty(.w_IDRICH) )
      endwith
    endif
  endfunc

  func oBtn_1_328.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_AGFA <>'S' OR .w_ISALT OR not empty(.w_IDRICH))
     endwith
    endif
  endfunc

  add object oIDRICH_1_330 as StdField with uid="EQVYVMNRIG",rtseq=266,rtrep=.f.,;
    cFormVar = "w_IDRICH", cQueryName = "IDRICH",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 202096250,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=236, Top=207, InputMask=replicate('X',15)

  func oIDRICH_1_330.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oIDRICH_1_330.mHide()
    with this.Parent.oContained
      return (g_AGFA <>'S'  OR .w_ISALT)
    endwith
  endfunc

  proc oIDRICH_1_330.mAfter
      with this.Parent.oContained
        gsag_ble(this.Parent.oContained,.w_IDRICH,".w_ATSEREVE",This.Parent.oContained,".w_ATCODNOM")
      endwith
  endproc

  add object oATSEREVE_1_331 as StdField with uid="NLHFSSPLFM",rtseq=267,rtrep=.f.,;
    cFormVar = "w_ATSEREVE", cQueryName = "ATSEREVE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 31482187,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=280, Top=306, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="ANEVENTI", oKey_1_1="EV__ANNO", oKey_1_2="this.w_ATEVANNO", oKey_2_1="EVSERIAL", oKey_2_2="this.w_ATSEREVE"

  func oATSEREVE_1_331.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=0)
    endwith
   endif
  endfunc

  func oATSEREVE_1_331.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  func oATSEREVE_1_331.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_331('Part',this)
    endwith
    return bRes
  endfunc

  proc oATSEREVE_1_331.ecpDrop(oSource)
    this.Parent.oContained.link_1_331('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATSEREVE_1_331.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ANEVENTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"EV__ANNO="+cp_ToStrODBC(this.Parent.oContained.w_ATEVANNO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"EV__ANNO="+cp_ToStr(this.Parent.oContained.w_ATEVANNO)
    endif
    do cp_zoom with 'ANEVENTI','*','EV__ANNO,EVSERIAL',cp_AbsName(this.parent,'oATSEREVE_1_331'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSFA_ZID.ANEVENTI_VZM',this.parent.oContained
  endproc


  add object oTIPEVE_1_333 as StdTableComboEnti with uid="OUCBIOSCVN",rtseq=268,rtrep=.f.,left=430,top=232,width=172,height=21;
    , ToolTipText = "Tipo evento da utilizzare per la creazione nuovo ID";
    , HelpContextID = 232773834;
    , cFormVar="w_TIPEVE",tablefilter="", bObbl = .f. , nPag = 1;
    , sErrorMsg = "Attenzione, tipo evento mancante";
    , cLinkFile="TIPEVENT";
    , cTable='..\AGEN\EXE\QUERY\GSAG_QCT.VQR',cKey='TETIPEVE',cValue='TEDESCRI',cOrderBy='TEDESCRI',xDefault=space(10);
  , bGlobalFont=.t.


  func oTIPEVE_1_333.mHide()
    with this.Parent.oContained
      return (g_AGFA <>'S'  OR .w_ISALT OR Empty(.w_IDRICH) or !.w_NEWID)
    endwith
  endfunc

  func oTIPEVE_1_333.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_333('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPEVE_1_333.ecpDrop(oSource)
    this.Parent.oContained.link_1_333('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oBtn_1_339 as StdButton with uid="LOEDVOSBAB",left=784, top=8, width=20,height=22,;
    CpPicture=".\bmp\sched_job.bmp", caption="", nPag=1;
    , ToolTipText = "Arresta timer";
    , HelpContextID = 143271690;
    , ImgSize = 16;
  , bGlobalFont=.t.

    proc oBtn_1_339.Click()
      this.parent.oContained.NotifyEvent("Clock")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_339.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_CA_TIMER<>"S" Or .cFunction<>'Load')
     endwith
    endif
  endfunc


  add object CLOCK as cp_clock with uid="EGCZLMZOKX",left=689, top=-2, width=82,height=31,;
    caption='CLOCK',;
   bGlobalFont=.t.,;
    FontName="Zucchetti Digital",FontSize=20,FontColor=48896,bShowSecond=.t.,Visible=.t.,tStartTime=Datetime(),;
    cEvent = "Clock",;
    nPag=1;
    , HelpContextID = 59893466

  add object oDesNomin_1_367 as StdField with uid="GKJJHNZHNS",rtseq=295,rtrep=.f.,;
    cFormVar = "w_DesNomin", cQueryName = "DesNomin",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione nominativo",;
    HelpContextID = 71601500,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=224, Top=356, InputMask=replicate('X',60)

  func oDesNomin_1_367.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc


  add object oObj_1_382 as cp_runprogram with uid="XAHSRJHRDX",left=490, top=997, width=200,height=26,;
    caption='GSAG_BAD(Q)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAD('Q')",;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 4971478


  add object oBtn_1_388 as StdButton with uid="GEBMEVIXXW",left=221, top=406, width=22,height=22,;
    CpPicture="BMP\phone.bmp", caption="", nPag=1;
    , ToolTipText = "Permette di effettuare chiamate al numero indicato";
    , HelpContextID = 29500634;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_388.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_ATTELEFO, "MN", .w_ATCODNOM)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_388.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_ATTELEFO) And (g_SkypeService='C' Or !Empty(g_PhoneService)))
      endwith
    endif
  endfunc

  func oBtn_1_388.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_ATTELEFO) Or (g_SkypeService<>'C' And Empty(g_PhoneService)))
     endwith
    endif
  endfunc


  add object oBtn_1_389 as StdButton with uid="WOZGFFSNHG",left=422, top=406, width=22,height=22,;
    CpPicture="BMP\Phone.bmp", caption="", nPag=1;
    , ToolTipText = "Permette di effettuare chiamate al numero indicato";
    , HelpContextID = 29500634;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_389.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_ATCELLUL, "MM", .w_ATCODNOM)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_389.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_ATCELLUL) And (g_SkypeService='C' Or !Empty(g_PhoneService)))
      endwith
    endif
  endfunc

  func oBtn_1_389.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_ATCELLUL) Or (g_SkypeService<>'C' And Empty(g_PhoneService)))
     endwith
    endif
  endfunc


  add object oBtn_1_390 as StdButton with uid="PFZYTOTFXB",left=784, top=406, width=22,height=22,;
    CpPicture="BMP\btsend.bmp", caption="", nPag=1;
    , ToolTipText = "Manda una e-mail all'indirizzo specificato";
    , HelpContextID = 22717882;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_390.Click()
      with this.Parent.oContained
        MAILTO(.w_AT_EMAIL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_390.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_AT_EMAIL))
      endwith
    endif
  endfunc

  func oBtn_1_390.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_AT_EMAIL))
     endwith
    endif
  endfunc

  add object oGiudicePrat_1_394 as StdField with uid="JDMZYUWXTW",rtseq=320,rtrep=.f.,;
    cFormVar = "w_GiudicePrat", cQueryName = "GiudicePrat",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 24723158,;
   bGlobalFont=.t.,;
    Height=21, Width=720, Left=82, Top=481, InputMask=replicate('X',100)

  func oGiudicePrat_1_394.mHide()
    with this.Parent.oContained
      return ((EMPTY(.w_CACHKOBB) AND !(EMPTY(.w_ATCAUATT) AND !EMPTY(.w_ATCODPRA))) or Not .w_ISALT)
    endwith
  endfunc


  add object oBtn_1_395 as StdButton with uid="CAMAQSYQUC",left=5, top=485, width=22,height=20,;
    CpPicture="bmp\INS_RAP.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire il caricamento multiplo degli impianti";
    , HelpContextID = 143070762;
    , IMGSIZE=16;
  , bGlobalFont=.t.

    proc oBtn_1_395.Click()
      do gsag_kmi with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_395.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Query' and .w_TIPNOM='C')
      endwith
    endif
  endfunc

  func oBtn_1_395.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISALT)
     endwith
    endif
  endfunc


  add object oBtn_1_402 as StdButton with uid="ZAPOEQADXT",left=5, top=266, width=22,height=20,;
    CpPicture="bmp\INS_RAP.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire il caricamento multiplo dei partecipanti";
    , HelpContextID = 143070762;
    , IMGSIZE=16;
  , bGlobalFont=.t.

    proc oBtn_1_402.Click()
      do gsag_kpg with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_402.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Query')
      endwith
    endif
  endfunc


  add object oBtn_1_412 as StdButton with uid="NHCFXOXZXK",left=657, top=527, width=48,height=45,;
    CpPicture="bmp\explode.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare dati offerte";
    , HelpContextID = 205721791;
    , Caption='\<Dati prev.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_412.Click()
      do GSAG_KOF with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_412.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_OFFE='S')
      endwith
    endif
  endfunc

  func oBtn_1_412.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_OFFE<>'S' or !.w_ISALT)
     endwith
    endif
  endfunc


  add object oBtn_1_435 as StdButton with uid="KLCDDCDGAM",left=311, top=102, width=22,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per forzare il progressivo attivit�";
    , HelpContextID = 143070762;
    , IMGSIZE=16;
  , bGlobalFont=.t.

    proc oBtn_1_435.Click()
      this.parent.oContained.NotifyEvent("Forzadoc")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_435.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISALT OR .w_FLPDOC<>'L' OR EMPTY(.w_ATCAUATT))
     endwith
    endif
  endfunc


  add object oBtn_1_441 as StdButton with uid="KRYGEKEMLA",left=784, top=431, width=22,height=22,;
    CpPicture="BMP\fxmail_pec.bmp", caption="", nPag=1;
    , ToolTipText = "Manda una e-mail PEC all'indirizzo specificato";
    , HelpContextID = 32355770;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_441.Click()
      with this.Parent.oContained
        MAILTO(.w_AT_EMPEC,,,.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_441.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_AT_EMPEC))
      endwith
    endif
  endfunc

  func oBtn_1_441.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_AT_EMPEC))
     endwith
    endif
  endfunc


  add object oObj_1_442 as cp_runprogram with uid="QBOHTKJQIX",left=-4, top=1004, width=152,height=21,;
    caption='GSAG_BRP',;
   bGlobalFont=.t.,;
    prg="GSAG_BRP(w_ATANNDOC,w_ONUMDOC,w_OALFDOC,w_ATCAUATT)",;
    cEvent = "Aggioprog",;
    nPag=1;
    , HelpContextID = 263274678


  add object oObj_1_443 as cp_runprogram with uid="LGDDOULPNR",left=-4, top=1027, width=152,height=21,;
    caption='GSAG_BRP',;
   bGlobalFont=.t.,;
    prg="GSAG_BRP(w_ATANNDOC,w_ATNUMDOC,w_ATALFDOC,w_ATCAUATT)",;
    cEvent = "Delaggioprog",;
    nPag=1;
    , HelpContextID = 263274678


  add object oObj_1_444 as cp_runprogram with uid="OAYRZTVSJA",left=-5, top=708, width=139,height=22,;
    caption='GSAG_BKK(V)',;
   bGlobalFont=.t.,;
    prg="gsag_bkk('V')",;
    cEvent = "Abbinacontratti",;
    nPag=1;
    , HelpContextID = 4970191

  add object oStr_1_38 as StdString with uid="QWTHOZTTVE",Visible=.t., Left=771, Top=72,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (g_JBSH<>'S')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="YPLEOESJNZ",Visible=.t., Left=13, Top=10,;
    Alignment=1, Width=63, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="JNTPKPVZCH",Visible=.t., Left=13, Top=41,;
    Alignment=1, Width=63, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="JURFODMQES",Visible=.t., Left=676, Top=41,;
    Alignment=1, Width=91, Height=18,;
    Caption="Giorni preavviso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="DWXUKAMXAQ",Visible=.t., Left=276, Top=41,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="LRDPZVZQBN",Visible=.t., Left=203, Top=41,;
    Alignment=1, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="UXGBQHUKCC",Visible=.t., Left=624, Top=103,;
    Alignment=1, Width=85, Height=18,;
    Caption="Tipo riserva:"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (NOT(.w_ATSTATUS $ 'FP') OR .w_FLTRIS#'S' OR NOT EMPTY(.w_DATRIN))
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="PQNSVDUSIY",Visible=.t., Left=318, Top=103,;
    Alignment=1, Width=49, Height=18,;
    Caption="Rinvio:"  ;
  , bGlobalFont=.t.

  func oStr_1_88.mHide()
    with this.Parent.oContained
      return (NOT(.w_ATSTATUS $ 'FPT') OR .w_FLRINV#'S' OR !.w_ISALT)
    endwith
  endfunc

  add object oStr_1_112 as StdString with uid="BIFKCELIBU",Visible=.t., Left=2, Top=103,;
    Alignment=1, Width=74, Height=18,;
    Caption="Proviene:"  ;
  , bGlobalFont=.t.

  func oStr_1_112.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_1_114 as StdString with uid="CZSLSBHUJE",Visible=.t., Left=495, Top=103,;
    Alignment=0, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  func oStr_1_114.mHide()
    with this.Parent.oContained
      return (NOT(.w_ATSTATUS $ 'FPT') OR .w_FLRINV#'S' OR !.w_ISALT)
    endwith
  endfunc

  add object oStr_1_128 as StdString with uid="TAVHLTLIKD",Visible=.t., Left=203, Top=101,;
    Alignment=1, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  func oStr_1_128.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_1_133 as StdString with uid="QBPBQFLZLN",Visible=.t., Left=318, Top=72,;
    Alignment=1, Width=49, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_134 as StdString with uid="YYXYCLPDIS",Visible=.t., Left=318, Top=43,;
    Alignment=1, Width=49, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_135 as StdString with uid="TGJGPZRGSW",Visible=.t., Left=4, Top=208,;
    Alignment=1, Width=74, Height=19,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_135.mHide()
    with this.Parent.oContained
      return ((EMPTY(.w_CACHKOBB) AND !(EMPTY(.w_ATCAUATT) AND !EMPTY(.w_ATCODPRA))) or Not .w_ISALT)
    endwith
  endfunc

  add object oStr_1_136 as StdString with uid="IHSSZERKLM",Visible=.t., Left=492, Top=43,;
    Alignment=1, Width=14, Height=18,;
    Caption="%:"  ;
  , bGlobalFont=.t.

  add object oStr_1_138 as StdString with uid="RMOEZHKGHO",Visible=.t., Left=276, Top=103,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_138.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_1_147 as StdString with uid="GHQIBNDBZL",Visible=.t., Left=13, Top=72,;
    Alignment=1, Width=63, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_148 as StdString with uid="WZFLEPMIEC",Visible=.t., Left=276, Top=72,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_149 as StdString with uid="YHXXGJBLXE",Visible=.t., Left=203, Top=72,;
    Alignment=1, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_152 as StdString with uid="QEYSSMBVBZ",Visible=.t., Left=569, Top=103,;
    Alignment=0, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_152.mHide()
    with this.Parent.oContained
      return (NOT(.w_ATSTATUS $ 'FPT') OR .w_FLRINV#'S' OR !.w_ISALT)
    endwith
  endfunc

  add object oStr_1_154 as StdString with uid="KNCKNPWFKX",Visible=.t., Left=4, Top=134,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_155 as StdString with uid="KTFWCDVPJN",Visible=.t., Left=361, Top=159,;
    Alignment=1, Width=85, Height=18,;
    Caption="Disponibilit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_155.mHide()
    with this.Parent.oContained
      return (.w_CARAGGST $ 'MD')
    endwith
  endfunc

  add object oStr_1_159 as StdString with uid="BDFRLCZJIE",Visible=.t., Left=361, Top=134,;
    Alignment=1, Width=85, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_204 as StdString with uid="XCNTBWTQWS",Visible=.t., Left=4, Top=356,;
    Alignment=1, Width=74, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_204.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  add object oStr_1_206 as StdString with uid="RXMHEDVHRC",Visible=.t., Left=445, Top=456,;
    Alignment=1, Width=46, Height=18,;
    Caption="Ufficio:"  ;
  , bGlobalFont=.t.

  func oStr_1_206.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_1_207 as StdString with uid="ZMZKSZAKLZ",Visible=.t., Left=4, Top=431,;
    Alignment=1, Width=74, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_208 as StdString with uid="ZPQFKFPUGN",Visible=.t., Left=36, Top=456,;
    Alignment=1, Width=45, Height=18,;
    Caption="Ente:"  ;
  , bGlobalFont=.t.

  func oStr_1_208.mHide()
    with this.Parent.oContained
      return (!.w_ISALT)
    endwith
  endfunc

  add object oStr_1_228 as StdString with uid="TLJFLYESCF",Visible=.t., Left=699, Top=72,;
    Alignment=1, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  func oStr_1_228.mHide()
    with this.Parent.oContained
      return (g_JBSH<>'S')
    endwith
  endfunc

  add object oStr_1_234 as StdString with uid="ASPOEYALXT",Visible=.t., Left=361, Top=184,;
    Alignment=1, Width=85, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_237 as StdString with uid="UPPNGAYCVJ",Visible=.t., Left=4, Top=233,;
    Alignment=1, Width=74, Height=18,;
    Caption="C/C.R.:"  ;
  , bGlobalFont=.t.

  func oStr_1_237.mHide()
    with this.Parent.oContained
      return (NOT (g_COAN='S' AND (.w_FLDANA='S' OR .w_FLANAL='S')) or  ! .w_ISALT or .w_FLANAL='N')
    endwith
  endfunc

  add object oStr_1_253 as StdString with uid="LUTQPKJDOJ",Visible=.t., Left=7, Top=456,;
    Alignment=1, Width=74, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  func oStr_1_253.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM) or .w_ISALT)
    endwith
  endfunc

  add object oStr_1_284 as StdString with uid="OUEWYQOZZB",Visible=.t., Left=650, Top=134,;
    Alignment=1, Width=74, Height=18,;
    Caption="Data doc.:"  ;
  , bGlobalFont=.t.

  func oStr_1_284.mHide()
    with this.Parent.oContained
      return (.w_FLNSAP='S')
    endwith
  endfunc

  add object oStr_1_310 as StdString with uid="NILVTXDFAF",Visible=.t., Left=599, Top=159,;
    Alignment=1, Width=62, Height=18,;
    Caption="Visibilit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_310.mHide()
    with this.Parent.oContained
      return (.w_GESRIS<>'S')
    endwith
  endfunc

  add object oStr_1_315 as StdString with uid="QPERMGWJRZ",Visible=.t., Left=388, Top=211,;
    Alignment=1, Width=58, Height=18,;
    Caption="B.Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_315.mHide()
    with this.Parent.oContained
      return (!isahe())
    endwith
  endfunc

  add object oStr_1_317 as StdString with uid="ZNUJTWFFRV",Visible=.t., Left=603, Top=211,;
    Alignment=1, Width=106, Height=18,;
    Caption="Gen. att. alla conf.:"  ;
  , bGlobalFont=.t.

  func oStr_1_317.mHide()
    with this.Parent.oContained
      return (NOT(.cFunction='Load'))
    endwith
  endfunc

  add object oStr_1_329 as StdString with uid="CZXHGRLBZS",Visible=.t., Left=154, Top=208,;
    Alignment=1, Width=79, Height=18,;
    Caption="ID richiesta:"  ;
  , bGlobalFont=.t.

  func oStr_1_329.mHide()
    with this.Parent.oContained
      return (g_AGFA <>'S'  OR .w_ISALT)
    endwith
  endfunc

  add object oStr_1_332 as StdString with uid="ROKAGEDAUD",Visible=.t., Left=158, Top=232,;
    Alignment=1, Width=75, Height=18,;
    Caption="Stato  rich.:"  ;
  , bGlobalFont=.t.

  func oStr_1_332.mHide()
    with this.Parent.oContained
      return ((g_AGFA <>'S'  OR .w_ISALT) OR (Empty(.w_IDRICH)))
    endwith
  endfunc

  add object oStr_1_334 as StdString with uid="PUHZIUBYVH",Visible=.t., Left=360, Top=232,;
    Alignment=1, Width=69, Height=18,;
    Caption="Tipo evento:"  ;
  , bGlobalFont=.t.

  func oStr_1_334.mHide()
    with this.Parent.oContained
      return (g_AGFA <>'S'  OR .w_ISALT OR Empty(.w_IDRICH)  or !.w_NEWID)
    endwith
  endfunc

  add object oStr_1_368 as StdString with uid="TPTJGPKIMQ",Visible=.t., Left=4, Top=381,;
    Alignment=1, Width=74, Height=18,;
    Caption="Rif. persona:"  ;
  , bGlobalFont=.t.

  func oStr_1_368.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  add object oStr_1_385 as StdString with uid="POEQLJNHLQ",Visible=.t., Left=15, Top=408,;
    Alignment=1, Width=63, Height=18,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  func oStr_1_385.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  add object oStr_1_386 as StdString with uid="OAFGLRTNYG",Visible=.t., Left=448, Top=408,;
    Alignment=1, Width=43, Height=18,;
    Caption="Email:"  ;
  , bGlobalFont=.t.

  func oStr_1_386.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  add object oStr_1_387 as StdString with uid="CYAFQZGMHM",Visible=.t., Left=246, Top=410,;
    Alignment=1, Width=33, Height=16,;
    Caption="Cel.:"  ;
  , bGlobalFont=.t.

  func oStr_1_387.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  add object oStr_1_393 as StdString with uid="GCHFUQGWVC",Visible=.t., Left=4, Top=481,;
    Alignment=1, Width=74, Height=18,;
    Caption="Giudice:"  ;
  , bGlobalFont=.t.

  func oStr_1_393.mHide()
    with this.Parent.oContained
      return ((EMPTY(.w_CACHKOBB) AND !(EMPTY(.w_ATCAUATT) AND !EMPTY(.w_ATCODPRA))) or Not .w_ISALT)
    endwith
  endfunc

  add object oStr_1_433 as StdString with uid="OSKULPNSKI",Visible=.t., Left=215, Top=104,;
    Alignment=2, Width=7, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_433.mHide()
    with this.Parent.oContained
      return (.w_ISALT OR .w_FLPDOC='N' OR EMPTY(.w_ATCAUATT))
    endwith
  endfunc

  add object oStr_1_434 as StdString with uid="BSVRDNPWAL",Visible=.t., Left=25, Top=103,;
    Alignment=1, Width=51, Height=18,;
    Caption="Num.:"  ;
  , bGlobalFont=.t.

  func oStr_1_434.mHide()
    with this.Parent.oContained
      return (.w_ISALT OR .w_FLPDOC='N' OR EMPTY(.w_ATCAUATT))
    endwith
  endfunc

  add object oStr_1_440 as StdString with uid="KIHYCYKETG",Visible=.t., Left=400, Top=435,;
    Alignment=1, Width=91, Height=18,;
    Caption="Email PEC:"  ;
  , bGlobalFont=.t.

  func oStr_1_440.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACHKNOM))
    endwith
  endfunc

  add object oBox_1_153 as StdBox with uid="NOAVXMARKC",left=3, top=128, width=806,height=1
enddefine
define class tgsag_aatPag2 as StdContainer
  Width  = 809
  height = 573
  stdWidth  = 809
  stdheight = 573
  resizeXpos=611
  resizeYpos=291
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="TRBPPTSIMK",left=11, top=95, width=787, height=359, bOnScreen=.t.;


  add object oATCODVAL_2_2 as StdField with uid="SBDSWQBFYD",rtseq=166,rtrep=.f.,;
    cFormVar = "w_ATCODVAL", cQueryName = "ATCODVAL",enabled=.f.,;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 34169170,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=64, Top=56, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_ATCODVAL"

  func oATCODVAL_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_ATCODLIS)
        bRes2=.link_2_3('Full')
      endif
      if .not. empty(.w_ATLISACQ)
        bRes2=.link_2_49('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oATCODLIS_2_3 as StdField with uid="VMCOWPZUQZ",rtseq=167,rtrep=.f.,;
    cFormVar = "w_ATCODLIS", cQueryName = "ATCODLIS",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, listino non valido",;
    ToolTipText = "Codice listino",;
    HelpContextID = 133602983,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=373, Top=56, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSVALLIS", oKey_1_2="this.w_ATCODVAL", oKey_2_1="LSCODLIS", oKey_2_2="this.w_ATCODLIS"

  func oATCODLIS_2_3.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  func oATCODLIS_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODLIS_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODLIS_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LISTINI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStrODBC(this.Parent.oContained.w_ATCODVAL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStr(this.Parent.oContained.w_ATCODVAL)
    endif
    do cp_zoom with 'LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(this.parent,'oATCODLIS_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc

  add object oDESVAL_2_6 as StdField with uid="YDYFJEYAXP",rtseq=168,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione valuta",;
    HelpContextID = 136228298,;
   bGlobalFont=.t.,;
    Height=21, Width=166, Left=129, Top=56, InputMask=replicate('X',35)

  add object oPARASS_2_7 as StdField with uid="TFINLKDQQH",rtseq=169,rtrep=.f.,;
    cFormVar = "w_PARASS", cQueryName = "PARASS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di parti in assistenza con la stessa posizione processuale",;
    HelpContextID = 1294602,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=746, Top=465, cSayPict='"999"', cGetPict='"999"'

  func oPARASS_2_7.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA) OR !.w_ISALT)
    endwith
  endfunc

  add object oFLAMPA_2_8 as StdCheck with uid="RWAQPEAYTI",rtseq=170,rtrep=.f.,left=626, top=489, caption="Non applicare maggiorazione", enabled=.f.,;
    ToolTipText = "Se attivo, non applica la maggiorazione degli onorari nel caso di assistenza a pi� persone aventi la stessa posizione processuale (del 20% fino a dieci e del 5% fino ad un massimo di venti)",;
    HelpContextID = 37275306,;
    cFormVar="w_FLAMPA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLAMPA_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLAMPA_2_8.GetRadio()
    this.Parent.oContained.w_FLAMPA = this.RadioValue()
    return .t.
  endfunc

  func oFLAMPA_2_8.SetRadio()
    this.Parent.oContained.w_FLAMPA=trim(this.Parent.oContained.w_FLAMPA)
    this.value = ;
      iif(this.Parent.oContained.w_FLAMPA=='S',1,;
      0)
  endfunc

  func oFLAMPA_2_8.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA) OR !.w_ISALT)
    endwith
  endfunc


  add object oATSTATUS_2_10 as StdCombo with uid="RSMQZOVXLE",rtseq=172,rtrep=.f.,left=636,top=56,width=116,height=21;
    , ToolTipText = "Stato da assegnare all'attivit�";
    , HelpContextID = 266297689;
    , cFormVar="w_ATSTATUS",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa,"+"Completata", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATSTATUS_2_10.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oATSTATUS_2_10.GetRadio()
    this.Parent.oContained.w_ATSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oATSTATUS_2_10.SetRadio()
    this.Parent.oContained.w_ATSTATUS=trim(this.Parent.oContained.w_ATSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_ATSTATUS=='T',1,;
      iif(this.Parent.oContained.w_ATSTATUS=='D',2,;
      iif(this.Parent.oContained.w_ATSTATUS=='I',3,;
      iif(this.Parent.oContained.w_ATSTATUS=='F',4,;
      iif(this.Parent.oContained.w_ATSTATUS=='P',5,;
      0)))))
  endfunc

  func oATSTATUS_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP<>'S')
    endwith
   endif
  endfunc

  func oATSTATUS_2_10.mHide()
    with this.Parent.oContained
      return (.w_FLNSAP='S')
    endwith
  endfunc


  add object oBtn_2_11 as StdButton with uid="MUTRVGDKAB",left=705, top=7, width=48,height=45,;
    CpPicture="bmp\AUTO.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per rigenerare le prestazioni";
    , HelpContextID = 246082761;
    , Caption='\<Rig.Prest.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      with this.Parent.oContained
        GSAG_BAP(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (inlist(.cfunction,"Edit","Load"))
      endwith
    endif
  endfunc

  func oBtn_2_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!inlist(.cfunction,"Edit","Load") OR .w_ISALT)
     endwith
    endif
  endfunc


  add object oATSTATUS_2_12 as StdCombo with uid="MOUYEXOEQZ",rtseq=173,rtrep=.f.,left=636,top=56,width=116,height=21;
    , ToolTipText = "Stato da assegnare all'attivit�";
    , HelpContextID = 266297689;
    , cFormVar="w_ATSTATUS",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATSTATUS_2_12.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    space(1))))))
  endfunc
  func oATSTATUS_2_12.GetRadio()
    this.Parent.oContained.w_ATSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oATSTATUS_2_12.SetRadio()
    this.Parent.oContained.w_ATSTATUS=trim(this.Parent.oContained.w_ATSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_ATSTATUS=='T',1,;
      iif(this.Parent.oContained.w_ATSTATUS=='D',2,;
      iif(this.Parent.oContained.w_ATSTATUS=='I',3,;
      iif(this.Parent.oContained.w_ATSTATUS=='F',4,;
      0))))
  endfunc

  func oATSTATUS_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP='S')
    endwith
   endif
  endfunc

  func oATSTATUS_2_12.mHide()
    with this.Parent.oContained
      return (.w_FLNSAP<>'S')
    endwith
  endfunc


  add object oBtn_2_13 as StdButton with uid="NQXZKOURAE",left=756, top=7, width=48,height=45,;
    CpPicture="bmp\PARAMETRI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per eseguire il caricamento multiplo delle prestazioni";
    , HelpContextID = 210010406;
    , Caption='\<Ins. Mult.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      do GSAG_KPM with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (inlist(.cfunction,"Edit","Load"))
      endwith
    endif
  endfunc

  func oBtn_2_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (! inlist(.cfunction,"Edit","Load"))
     endwith
    endif
  endfunc


  add object oObj_2_14 as cp_runprogram with uid="DYDBOWKKNH",left=1, top=592, width=377,height=19,;
    caption='GSAG_BAP(3)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAP('3')",;
    cEvent = "w_ATCODLIS Changed",;
    nPag=2;
    , HelpContextID = 4979146


  add object oATFLVALO_2_15 as StdCombo with uid="QPYAKXTNHK",rtseq=174,rtrep=.f.,left=53,top=465,width=136,height=21, enabled=.f.;
    , ToolTipText = "Check valore (indica se la pratica ha un valore definito, indeterminabile o di particolare importanza e indeterminabile)";
    , HelpContextID = 31026859;
    , cFormVar="w_ATFLVALO",RowSource=""+"Definito,"+"Indeterminabile,"+"Partic. import. e ind.,"+"Straord. import. e ind.", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATFLVALO_2_15.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'I',;
    iif(this.value =3,'P',;
    iif(this.value =4,'S',;
    space(1))))))
  endfunc
  func oATFLVALO_2_15.GetRadio()
    this.Parent.oContained.w_ATFLVALO = this.RadioValue()
    return .t.
  endfunc

  func oATFLVALO_2_15.SetRadio()
    this.Parent.oContained.w_ATFLVALO=trim(this.Parent.oContained.w_ATFLVALO)
    this.value = ;
      iif(this.Parent.oContained.w_ATFLVALO=='D',1,;
      iif(this.Parent.oContained.w_ATFLVALO=='I',2,;
      iif(this.Parent.oContained.w_ATFLVALO=='P',3,;
      iif(this.Parent.oContained.w_ATFLVALO=='S',4,;
      0))))
  endfunc

  func oATFLVALO_2_15.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA) OR !.w_ISALT)
    endwith
  endfunc

  add object oATIMPORT_2_16 as StdField with uid="DRJARYPQJO",rtseq=175,rtrep=.f.,;
    cFormVar = "w_ATIMPORT", cQueryName = "ATIMPORT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore della pratica",;
    HelpContextID = 197640538,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=227, Top=465, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oATIMPORT_2_16.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA) OR .w_ATFLVALO<>'D' OR !.w_ISALT)
    endwith
  endfunc


  add object oATCALDIR_2_17 as StdCombo with uid="TVUUGKFDTD",rtseq=176,rtrep=.f.,left=475,top=465,width=135,height=21, enabled=.f.;
    , ToolTipText = "Criterio utilizzato per calcolare i diritti e gli onorari";
    , HelpContextID = 260349608;
    , cFormVar="w_ATCALDIR",RowSource=""+"% di sconto su min,"+"Applica il minimo,"+"Applica il medio,"+"Applica il massimo,"+"Coeff. di calcolo,"+"Raddoppia il max,"+"Quadruplica il max,"+"% di aumento su max", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oATCALDIR_2_17.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'I',;
    iif(this.value =3,'M',;
    iif(this.value =4,'X',;
    iif(this.value =5,'C',;
    iif(this.value =6,'R',;
    iif(this.value =7,'Q',;
    iif(this.value =8,'A',;
    space(1))))))))))
  endfunc
  func oATCALDIR_2_17.GetRadio()
    this.Parent.oContained.w_ATCALDIR = this.RadioValue()
    return .t.
  endfunc

  func oATCALDIR_2_17.SetRadio()
    this.Parent.oContained.w_ATCALDIR=trim(this.Parent.oContained.w_ATCALDIR)
    this.value = ;
      iif(this.Parent.oContained.w_ATCALDIR=='S',1,;
      iif(this.Parent.oContained.w_ATCALDIR=='I',2,;
      iif(this.Parent.oContained.w_ATCALDIR=='M',3,;
      iif(this.Parent.oContained.w_ATCALDIR=='X',4,;
      iif(this.Parent.oContained.w_ATCALDIR=='C',5,;
      iif(this.Parent.oContained.w_ATCALDIR=='R',6,;
      iif(this.Parent.oContained.w_ATCALDIR=='Q',7,;
      iif(this.Parent.oContained.w_ATCALDIR=='A',8,;
      0))))))))
  endfunc

  func oATCALDIR_2_17.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA) OR !.w_ISALT)
    endwith
  endfunc

  add object oATCOECAL_2_18 as StdField with uid="WGJZYRZXPS",rtseq=177,rtrep=.f.,;
    cFormVar = "w_ATCOECAL", cQueryName = "ATCOECAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente di calcolo, % di sconto, % di maggiorazione",;
    HelpContextID = 15113902,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=612, Top=465, cSayPict='"999.99"', cGetPict='"999.99"'

  func oATCOECAL_2_18.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA) OR !.w_ISALT)
    endwith
  endfunc

  add object odespra_2_24 as StdField with uid="SUJPOZISCD",rtseq=178,rtrep=.f.,;
    cFormVar = "w_despra", cQueryName = "despra",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto della pratica",;
    HelpContextID = 881718,;
   bGlobalFont=.t.,;
    Height=21, Width=440, Left=201, Top=27, InputMask=replicate('X',100)

  func odespra_2_24.mHide()
    with this.Parent.oContained
      return (not .w_ISALT)
    endwith
  endfunc

  add object ocodpra_2_26 as StdField with uid="AKRSNXCDSA",rtseq=179,rtrep=.f.,;
    cFormVar = "w_codpra", cQueryName = "codpra",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice della pratica",;
    HelpContextID = 822822,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=64, Top=27, InputMask=replicate('X',15)

  func ocodpra_2_26.mHide()
    with this.Parent.oContained
      return (not .w_ISALT)
    endwith
  endfunc


  add object oBtn_2_29 as StdButton with uid="MBXAIGGBZE",left=756, top=521, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza il documento associato all'attivit�";
    , HelpContextID = 100474768;
    , caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_29.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_ATRIFDOC,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISALT or EMPTY(.w_ATRIFDOC) or Isahr())
     endwith
    endif
  endfunc


  add object oBtn_2_32 as StdButton with uid="PGWDUJBTRZ",left=705, top=521, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza il movimento di analitica collegato";
    , HelpContextID = 89412364;
    , Caption='M\<ov. An.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_32.Click()
      with this.Parent.oContained
        GSCA_BZP(this.Parent.oContained,.w_ATRIFMOV, iif(! Isahe(),"(Man.","Man."),"",0,"")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_32.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COAN <> 'S' or EMPTY(NVL(.w_ATRIFMOV,'')))
     endwith
    endif
  endfunc


  add object oBtn_2_43 as StdButton with uid="SRLMYFASDK",left=756, top=521, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=2;
    , ToolTipText = "Visualizza il documento associato all'attivit�";
    , HelpContextID = 100474768;
    , caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_43.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_ATRIFDOC,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_43.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (isahe() or EMPTY(.w_ATRIFDOC) or .w_ISALT)
     endwith
    endif
  endfunc


  add object oLinkPC_2_44 as StdButton with uid="IWENXPYYBM",left=756, top=521, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , HelpContextID = 54428618;
    , caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_44.Click()
      this.Parent.oContained.gsag_mdd.LinkPCClick()
      this.Parent.oContained.WriteTo_gsag_mdd()
    endproc

  func oLinkPC_2_44.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ISALT)
     endwith
    endif
  endfunc


  add object oObj_2_45 as cp_runprogram with uid="PBBZICWIPO",left=1, top=614, width=377,height=19,;
    caption='GSAG_BAP(A)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAP('A')",;
    cEvent = "w_ATCODNOM Changed",;
    nPag=2;
    , HelpContextID = 4975562

  add object oATLISACQ_2_49 as StdField with uid="XTJQTXEKNJ",rtseq=305,rtrep=.f.,;
    cFormVar = "w_ATLISACQ", cQueryName = "ATLISACQ",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, listino inesistente o di tipo Iva inclusa o di tipo sconti",;
    ToolTipText = "Codice listino",;
    HelpContextID = 234090839,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=518, Top=56, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSVALLIS", oKey_1_2="this.w_ATCODVAL", oKey_2_1="LSCODLIS", oKey_2_2="this.w_ATLISACQ"

  func oATLISACQ_2_49.mHide()
    with this.Parent.oContained
      return (! Isahr())
    endwith
  endfunc

  func oATLISACQ_2_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oATLISACQ_2_49.ecpDrop(oSource)
    this.Parent.oContained.link_2_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATLISACQ_2_49.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LISTINI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStrODBC(this.Parent.oContained.w_ATCODVAL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStr(this.Parent.oContained.w_ATCODVAL)
    endif
    do cp_zoom with 'LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(this.parent,'oATLISACQ_2_49'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc


  add object oObj_2_57 as cp_runprogram with uid="IMYOCKTOXT",left=385, top=594, width=478,height=19,;
    caption='GSAG_BAP(E)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAP('E')",;
    cEvent = "w_ATLISACQ Changed",;
    nPag=2;
    , HelpContextID = 4974538


  add object oObj_2_58 as cp_runprogram with uid="RSNFUGZTID",left=1, top=635, width=378,height=19,;
    caption='GSAG_BAP(V)',;
   bGlobalFont=.t.,;
    prg="GSAG_BAP('V')",;
    cEvent = "w_ATCODVAL Changed",;
    nPag=2;
    , HelpContextID = 4970186


  add object oBtn_2_62 as StdButton with uid="KVEKAONFBD",left=654, top=7, width=48,height=45,;
    CpPicture="BMP\CONTRATT.ICO", caption="", nPag=2;
    , ToolTipText = "Premere il bottone per ottenere l'abbinamento automatico del primo elemento contratto a pacchetto disponibile";
    , HelpContextID = 1127210;
    , Caption='\<Contratti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_62.Click()
      with this.Parent.oContained
        GSAG_BKK(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_62.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Query')
     endwith
    endif
  endfunc

  add object oStr_2_4 as StdString with uid="BEMKKJLFRL",Visible=.t., Left=298, Top=59,;
    Alignment=1, Width=74, Height=18,;
    Caption="Listino ricavi:"  ;
  , bGlobalFont=.t.

  func oStr_2_4.mHide()
    with this.Parent.oContained
      return (Isahe())
    endwith
  endfunc

  add object oStr_2_5 as StdString with uid="JLOFQSCABR",Visible=.t., Left=5, Top=59,;
    Alignment=1, Width=55, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="OQCIQSAIMJ",Visible=.t., Left=5, Top=465,;
    Alignment=1, Width=46, Height=18,;
    Caption="Valore:"  ;
  , bGlobalFont=.t.

  func oStr_2_19.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA) OR !.w_ISALT)
    endwith
  endfunc

  add object oStr_2_20 as StdString with uid="CAUAKIDNRA",Visible=.t., Left=194, Top=465,;
    Alignment=1, Width=31, Height=18,;
    Caption="EUR:"  ;
  , bGlobalFont=.t.

  func oStr_2_20.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA) OR .w_ATFLVALO<>'D' OR !.w_ISALT)
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="WYSTBXPUJI",Visible=.t., Left=369, Top=465,;
    Alignment=1, Width=104, Height=18,;
    Caption="Calc.dir./onor.:"  ;
  , bGlobalFont=.t.

  func oStr_2_21.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA) OR !.w_ISALT)
    endwith
  endfunc

  add object oStr_2_22 as StdString with uid="BYIDANWHGB",Visible=.t., Left=591, Top=59,;
    Alignment=1, Width=42, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="QRYJTBYCXY",Visible=.t., Left=702, Top=465,;
    Alignment=1, Width=41, Height=18,;
    Caption="Parti:"  ;
  , bGlobalFont=.t.

  func oStr_2_23.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ATCODPRA) OR !.w_ISALT)
    endwith
  endfunc

  add object oStr_2_25 as StdString with uid="TMQDKASBOY",Visible=.t., Left=11, Top=29,;
    Alignment=1, Width=49, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_2_25.mHide()
    with this.Parent.oContained
      return (not .w_ISALT)
    endwith
  endfunc

  add object oStr_2_50 as StdString with uid="DQQRGJTIWR",Visible=.t., Left=447, Top=60,;
    Alignment=1, Width=69, Height=18,;
    Caption="Listino costi:"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return (! Isahr())
    endwith
  endfunc
enddefine
define class tgsag_aatPag3 as StdContainer
  Width  = 809
  height = 573
  stdWidth  = 809
  stdheight = 573
  resizeXpos=430
  resizeYpos=178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomDocInt as cp_zoombox with uid="JDHDICZBPC",left=2, top=61, width=754,height=191,;
    caption='Object',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.f.,cTable="DOC_DETT",bRetriveAllRows=.f.,cZoomOnZoom="",cMenuFile="",cZoomFile="GSAG_DOC",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Load,New record,Riesegui",;
    nPag=3;
    , ToolTipText = "Zoom di visualizzazione righe documento interno associato all'attivit�";
    , HelpContextID = 34725862


  add object ZoomTrRig as cp_zoombox with uid="AFAKIKFXFS",left=2, top=274, width=754,height=191,;
    caption='Object',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.f.,cTable="DOC_DETT",bRetriveAllRows=.f.,cZoomOnZoom="",cMenuFile="",cZoomFile="GSAG_TRR",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnDblClick=.t.,;
    nPag=3;
    , ToolTipText = "Zoom di visualizzazione tracciabilit� della riga selezionata del documento interno associato all'attivit�";
    , HelpContextID = 34725862


  add object oObj_3_14 as cp_runprogram with uid="XEZOWSOFUQ",left=17, top=653, width=205,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAG_BTR",;
    cEvent = "w_ZoomDocInt selected",;
    nPag=3;
    , HelpContextID = 34725862

  add object odespra_3_18 as StdField with uid="QHHXKHROOW",rtseq=192,rtrep=.f.,;
    cFormVar = "w_despra", cQueryName = "despra",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto della pratica",;
    HelpContextID = 881718,;
   bGlobalFont=.t.,;
    Height=21, Width=449, Left=331, Top=35, InputMask=replicate('X',100)

  add object ocodpra_3_20 as StdField with uid="SRZTNTUXLT",rtseq=193,rtrep=.f.,;
    cFormVar = "w_codpra", cQueryName = "codpra",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice della pratica",;
    HelpContextID = 822822,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=197, Top=35, InputMask=replicate('X',15)

  add object odatpre_3_22 as StdField with uid="QOPYHYZYXC",rtseq=194,rtrep=.f.,;
    cFormVar = "w_datpre", cQueryName = "datpre",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della prestazione",;
    HelpContextID = 67993654,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=60, Top=35


  add object oBtn_3_23 as StdButton with uid="LAWWCIBYZX",left=758, top=76, width=48,height=45,;
    CpPicture="bmp\Elabora.ico", caption="", nPag=3;
    , ToolTipText = "Permette di contabilizzare le anticipazioni contenute in questa attivit�";
    , HelpContextID = 261468894;
    , Caption='C\<ont.Ant.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_23.Click()
      with this.Parent.oContained
        do Contabilizza with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_23.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Not Empty(.w_RIFCON) or Empty(.w_SERIAL) OR ! .w_PRESTA $ 'S-A')
     endwith
    endif
  endfunc


  add object oBtn_3_24 as StdButton with uid="RWVHEPYZPP",left=758, top=76, width=48,height=45,;
    CpPicture="BMP\teso.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per per accedere alla registrazione contabile associata";
    , HelpContextID = 2747382;
    , TabStop=.f.,Caption='\<Primanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_24.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_RIFCON)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!(EMPTY(.w_RIFCON) or g_COGE<>'S'))
      endwith
    endif
  endfunc

  func oBtn_3_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_RIFCON) or g_COGE<>'S')
     endwith
    endif
  endfunc

  add object oStr_3_12 as StdString with uid="SOQMAOWJBN",Visible=.t., Left=20, Top=255,;
    Alignment=0, Width=285, Height=18,;
    Caption="Tracciabilit� delle prestazioni"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="OMFPETZYXW",Visible=.t., Left=20, Top=15,;
    Alignment=0, Width=440, Height=18,;
    Caption="Elenco prestazioni completate"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="JTGIEABWLT",Visible=.t., Left=267, Top=649,;
    Alignment=0, Width=439, Height=18,;
    Caption="ATTENZIONE: La routine della tracciabilit� parte all'evento ActivatePage 4"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_16.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_3_19 as StdString with uid="DRJRAFADLU",Visible=.t., Left=137, Top=37,;
    Alignment=1, Width=57, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  add object oStr_3_21 as StdString with uid="TIOPSWKVEH",Visible=.t., Left=4, Top=37,;
    Alignment=1, Width=54, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_aat','OFF_ATTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ATSERIAL=OFF_ATTI.ATSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsag_aat
define class StdTableComboEnti as StdTableCombo
proc Reload()
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    vq_exec(this.cTable,Thisform,i_curs)
    i_fk=this.cKey
    i_fd=this.cValue
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()+1
      dimension this.combovalues[MAX(1,this.nValues)]
      i_bCharKey=type(i_fk)='C'
      *this.AddItem(cp_Translate('<Nessun Valore>'))
      *--- Svuoto la combo
      this.Clear()
      this.AddItem(' ')
      this.combovalues[1]=iif(i_bCharKey,Space(1),0)
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()+1]=trim(&i_fk)
        else
          this.combovalues[recno()+1]=&i_fk
        endif
        skip
        enddo
        use
    ENDIF
enddefine

Proc RivalorizzoCombo(pParent)
   local obj
   obj = pParent.getCtrl("w_AT__ENTE")
   obj.Reload()
   obj.SetRadio()
   pParent.w_AT__ENTE=obj.RadioValue()
   obj = .null.
Endproc

* --- classe per superare il problema canview che per un
* --- attimo mostra i dati
define class cp_runprogram2 as Cp_RunProgram
 proc Calculate(xValue)
 if isalt()
   LOCAL bok,ccurs,Workarea
   bok=.t.
   Workarea=ALIAS()
   if EMPTY(xValue) AND this.parent.oContained.bCanviewLoaded
      this.parent.oContained.bCanviewLoaded=.f.
      *controllo riservato
      bok=this.parent.oContained.Canview()
      if bok
       * verifico se l'attivit� � visibile
       ccurs=sys(2015)
       vq_exec('gsag_canv',this.parent.oContained,ccurs)
       if used(ccurs) 
         bok=Reccount((ccurs))<>0
         Use in (ccurs)
       endif
     endif
       if !bok
         this.parent.oContained.BlankRec()
       endif
   endif
   if Not empty(Workarea)
     Select (Workarea)
   endif
  Endif
 endproc
Enddefine
Proc Contabilizza(pParent)
   Local oGsag_kga
   exec="oGsag_kga=gsal_kga()"
   &exec
   oGsag_kga.left=_screen.width+1
   oGsag_kga.w_MVSERIAL=pParent.w_SERIAL
   oGsag_kga.w_INIDATA=pParent.w_MVDATREG
   oGsag_kga.ecpsave()
   oGsag_kga=.Null.
   pParent.Notifyevent('Riesegui')
Endproc
* --- Fine Area Manuale
