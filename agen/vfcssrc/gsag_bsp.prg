* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bsp                                                        *
*              Stampa/ricerca prestazioni                                      *
*                                                                              *
*      Author: Zucchetti TAM S.p.a.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_66]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-28                                                      *
* Last revis.: 2014-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bsp",oParentObject,m.pParam)
return(i_retval)

define class tgsag_bsp as StdBatch
  * --- Local variables
  pParam = space(10)
  w_PADRE = .NULL.
  w_cKey = space(20)
  w_Cli = .f.
  w_Msg = space(10)
  w_ObjMDA = .NULL.
  w_OBJECT = .NULL.
  w_ROWNUMPRE = 0
  w_STATO_ATT = space(1)
  w_ATSERIAL = space(15)
  w_GSAG_KRP = .NULL.
  w_GRD0 = .NULL.
  w_TMPN = 0
  w_TIPOPRE = space(1)
  w_ROWNUM = 0
  w_OK = .f.
  w_Zoom = space(10)
  w_LIVELLO = 0
  w_Elabora = .f.
  w_AppoSerial = space(20)
  w_PAFLRDOC = space(1)
  w_TIPO = space(1)
  w_ALTE = 0
  * --- WorkFile variables
  TMPSTPRE_idx=0
  TMPDPOFD_idx=0
  TMPDPOFF_idx=0
  OFFDATTI_idx=0
  PAR_AGEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili comuni a maschera di ricerca/stampa
    do case
      case this.pParam=="CONTROLLO"
        * --- Controllo esistenza parametri
        if IsAlt()
          * --- Se AlterEgo pu� lanciare il batch di controllo
          do GSAL_BCK with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pParam=="CLIENTE"
        if ISALT()
          * --- Controllo che il codice digitato sia un cliente
          this.w_Cli = .F.
          if !EMPTY(this.oParentObject.w_CODPRA)
            this.w_Msg = "Selezionare un cliente per la pratica indicata"
          else
            this.w_Msg = "Selezionare un cliente"
          endif
          * --- Cerchiamo se nelle risorse esterne compare come nominativo.
          *     La query � utilizzata anche dallo zoom GSAG_BSP, quindi per filtrare sul codice utilizziamo la var. di appoggio w_CODCLICONTR
          this.oParentObject.w_CODCLICONTR = this.oParentObject.w_CODNOM
          * --- Select from query\GSAG3BSP.VQR
          do vq_exec with 'query\GSAG3BSP.VQR',this,'_Curs_query_GSAG3BSP_d_VQR','',.f.,.t.
          if used('_Curs_query_GSAG3BSP_d_VQR')
            select _Curs_query_GSAG3BSP_d_VQR
            locate for 1=1
            do while not(eof())
            this.w_Cli = .T.
              select _Curs_query_GSAG3BSP_d_VQR
              continue
            enddo
            use
          endif
          this.oParentObject.w_CODCLICONTR = SPACE(20)
          if !this.w_Cli
            * --- Il nominativo selezionato non compare come cliente in nessuna pratica
            this.oParentObject.w_CODNOM = SPACE(20)
            this.oParentObject.w_DESNOM = SPACE(20)
            ah_ErrorMsg( this.w_Msg )
          endif
        endif
      case this.pParam=="ELIMINA"
        * --- ELIMINAZIONE PRESTAZIONE
        this.w_TIPOPRE = "A"
        this.w_ATSERIAL = g_oMenu.getbyindexkeyvalue(1)
        if Vartype("g_omenu.oparentobject.w_TIPOPRE")="C"
          this.w_TIPOPRE = g_omenu.oparentobject.w_TIPOPRE
        endif
        if this.w_TIPOPRE="A"
          * --- Read from OFFDATTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.OFFDATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2],.t.,this.OFFDATTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWNUM"+;
              " from "+i_cTable+" OFFDATTI where ";
                  +"DASERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWNUM;
              from (i_cTable) where;
                  DASERIAL = this.w_ATSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_ROWNUM = i_ROWS
          if this.w_ROWNUM=1
            this.w_OK = ah_YesNo("La cancellazione di questa prestazione prevede l'eliminazione anche dell'attivit�.%0Confermi cancellazione?")
          else
            this.w_OK = ah_YesNo("Confermi cancellazione?")
          endif
          if this.w_OK
            * --- Assegno alla variabile il valore relativo alla chiave primaria 1 (ATSERIAL) delle Attivit�
            * --- Istanzio l'anagrafica
            this.w_OBJECT = GSAG_AAT()
            * --- Controllo se ha passato il test di accesso
            if !(this.w_OBJECT.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- Carico il record selezionato
            this.w_OBJECT.ECPFILTER()     
            this.w_OBJECT.w_ATSERIAL = this.w_ATSERIAL
            this.w_OBJECT.ECPSAVE()     
            if Empty( this.w_OBJECT.w_ATSERIAL )
              * --- Se attivit� non disponibile esco...
              this.w_OBJECT.ECPQUIT()     
              this.w_OBJECT = Null
              i_retcode = 'stop'
              return
            endif
            if this.w_ROWNUM=1
              this.w_OBJECT.ECPDELETE()     
            else
              this.w_OBJECT.ECPEDIT()     
              this.w_OBJECT.w_NOMSGEN = .t.
              this.w_STATO_ATT = this.w_OBJECT.w_STATOATT
              if this.w_STATO_ATT#"N" AND this.w_STATO_ATT#"U"
                this.w_ObjMDA = this.w_OBJECT.GSAG_MDA
                this.w_ObjMDA.FirstRow()     
                this.w_ROWNUMPRE = this.w_ObjMDA.Search("CPROWNUM = "+ cp_ToStrODBC(g_oMenu.oParentObject.w_ROWNUM) + " AND NOT DELETED()")
                this.w_ObjMDA.SetRow(this.w_ROWNUMPRE)     
                this.w_ObjMDA.DeleteRow()     
                this.w_OBJECT.ECPSAVE()     
              endif
            endif
            this.w_OBJECT.ECPQUIT()     
            * --- Fa fare il refresh della maschera
            this.w_OBJECT = g_oMenu.oParentObject
             
 public w_TIMER 
 w_TIMER = createobject("MyTimer", This.w_OBJECT) 
 w_TIMER.Interval = 100
          endif
        else
          if ah_YesNo("Confermi cancellazione?")
            this.w_OBJECT = GSAG_APR()
            * --- Controllo se ha passato il test di accesso
            if !(this.w_OBJECT.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- Carico il record selezionato
            this.w_OBJECT.ECPFILTER()     
            this.w_OBJECT.w_PRSERIAL = this.w_ATSERIAL
            this.w_OBJECT.ECPSAVE()     
            this.w_OBJECT.ECPDELETE()     
          endif
          * --- Fa fare il refresh della maschera
          this.w_OBJECT = g_oMenu.oParentObject
           
 public w_TIMER 
 w_TIMER = createobject("MyTimer", This.w_OBJECT) 
 w_TIMER.Interval = 100
        endif
        this.w_OBJECT = .null.
      case this.pParam=="AGG_ZOOM"
        * --- NON PIU' UTILIZZATO
        * --- Configura lo zoom Elenco prestazioni
      case this.pParam=="POSTIN"
        * --- Attiva il Post-In relativo al Codice Articolo associato al Codice di Ricerca
        this.w_PADRE = this.oParentObject
        this.w_cKey = cp_setAzi(i_TableProp[this.w_PADRE.ART_ICOL_IDX, 2]) + "\" + cp_ToStr(this.oParentObject.w_CACODART, 1)
        cp_ShowWarn(this.w_cKey, this.w_PADRE.ART_ICOL_IDX)
      case this.pParam=="CHK_CODINI" OR this.pParam=="CHK_CODFIN"
        if NOT(this.oParentObject.w_CODINI<=this.oParentObject.w_CODFIN OR EMPTY(this.oParentObject.w_CODFIN))
          ah_ErrorMsg("Il codice iniziale � pi� grande di quello finale")
          do case
            case this.pParam=="CHK_CODINI"
              this.oParentObject.w_CODINI = SPACE(10)
              this.oParentObject.w_DESCRITER = SPACE(40)
            case this.pParam=="CHK_CODFIN"
              this.oParentObject.w_CODFIN = SPACE(10)
              this.oParentObject.w_DESCRITER2 = SPACE(40)
          endcase
        endif
      case this.pParam=="NESSUNA_AZIONE" 
        * --- Necessario per gestire l'azione vuota in GSAL_KUP
      otherwise
        * --- Stampa/ricerca
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riempimento tabella temporanea
    * --- Variabili chiamante - uguali sia per stampa che per ricerca
    * --- Zoom ricerca prestazioni
    * --- Inseriamo nel temporaneo TMP_SERIAL i seriali delle attivit� che soddisfano la query GSAG_SAT.
    *     Il livello iniziale � zero.
    this.w_LIVELLO = 0
    if this.pParam=="STAMPA" AND NOT EMPTY(this.oParentObject.w_DesSer)
      * --- Viene riaggiornata la w_DesAggPre poich� altrimenti non conterrebbe la % iniziale e finale (AH_SEARCHFLD).
      *     In Ricerca la w_DesAggPre viene aggiornata dalla mcalc sulla maschera a seguito del ritardo causato dal timer.
      this.oParentObject.w_DesAggPre = this.oParentObject.w_DesSer
    endif
    this.oParentobject.oPgFrm.ActivePage=1
    * --- Creiamo la tabella direttamente dalle query dato che hanno la struttura corretta
    if IsAlt()
      * --- Legge il flag Ricerca per data documento
      * --- Read from PAR_AGEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PAFLRDOC"+;
          " from "+i_cTable+" PAR_AGEN where ";
              +"PACODAZI = "+cp_ToStrODBC(i_codazi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PAFLRDOC;
          from (i_cTable) where;
              PACODAZI = i_codazi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PAFLRDOC = NVL(cp_ToDate(_read_.PAFLRDOC),cp_NullValue(_read_.PAFLRDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Create temporary table TMPSTPRE
      i_nIdx=cp_AddTableDef('TMPSTPRE') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_ETVTEPTFZN[1]
      indexes_ETVTEPTFZN[1]='ATRIFDOC'
      vq_exec('QUERY\gsag_bsp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_ETVTEPTFZN,.f.)
      this.TMPSTPRE_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table TMPSTPRE
      i_nIdx=cp_AddTableDef('TMPSTPRE') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\gsag8bsp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPSTPRE_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    this.w_Elabora = .F.
    * --- InDocum � stato nascosto (non editabile e inizializzato a 'T')
    *     Se � attivo un filtro sul tipo di documento (editabile solo se stato='P'), procediamo con la tracciabilit�
    if this.oParentObject.w_STATO="P" AND !this.oParentObject.w_InDocum="T"
      this.w_Elabora = .T.
      do while this.w_Elabora
        * --- Insert into TMPSTPRE
        i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSAG2BSP",this.TMPSTPRE_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_LIVELLO = this.w_LIVELLO+1
        * --- Read from TMPSTPRE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2],.t.,this.TMPSTPRE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATRIFDOC"+;
            " from "+i_cTable+" TMPSTPRE where ";
                +"LIVELLO = "+cp_ToStrODBC(this.w_LIVELLO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATRIFDOC;
            from (i_cTable) where;
                LIVELLO = this.w_LIVELLO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_AppoSerial = NVL(cp_ToDate(_read_.ATRIFDOC),cp_NullValue(_read_.ATRIFDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Non c'� pi� nessun livello successivo da elaborare
        if EMPTY(this.w_AppoSerial)
          this.w_Elabora = .F.
        endif
      enddo
      * --- Adesso abbiamo in TMPDPOFP tutti i seriali delle prestazioni replicati per ciascun documento emesso.
      *     Le tipologie sono le seguenti:
      *     B - Bozza interna, N - nota spese, P - proforma d'acconto, S - proforma a saldo
      *     A - fattura d'acconto, F - fattura, C - Nota di credito, T - prestazioni
      *     Cancelliamo le bozze, le note spese, le note di credito, le prestazioni, inoltre proforma e fattura d'acconto
      * --- Delete from TMPSTPRE
      i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
        i_cTempTable=cp_GetTempTableName(i_nConn)
        i_aIndex[1]='DASERIAL,ROWPRE'
        cp_CreateTempTable(i_nConn,i_cTempTable,"DASERIAL,ROWPRE "," from "+i_cQueryTable+" where PRTIPCAU='C'",.f.,@i_aIndex)
        i_cQueryTable=i_cTempTable
        i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
              +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
      
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where ";
              +""+i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
              +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
              +")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Bozze - PRTIPCAU='B'
      * --- Delete from TMPSTPRE
      i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PRTIPCAU = "+cp_ToStrODBC("B");
               )
      else
        delete from (i_cTable) where;
              PRTIPCAU = "B";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Note spese - PRTIPCAU='N'
      * --- Delete from TMPSTPRE
      i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PRTIPCAU = "+cp_ToStrODBC("N");
               )
      else
        delete from (i_cTable) where;
              PRTIPCAU = "N";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Nota di credito
      * --- Delete from TMPSTPRE
      i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PRTIPCAU = "+cp_ToStrODBC("C");
               )
      else
        delete from (i_cTable) where;
              PRTIPCAU = "C";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Prestazioni
      * --- Delete from TMPSTPRE
      i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PRTIPCAU = "+cp_ToStrODBC("T");
               )
      else
        delete from (i_cTable) where;
              PRTIPCAU = "T";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Proforma d'acconto
      * --- Delete from TMPSTPRE
      i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PRTIPCAU = "+cp_ToStrODBC("P");
              +" and TIPOPRE = "+cp_ToStrODBC("D");
               )
      else
        delete from (i_cTable) where;
              PRTIPCAU = "P";
              and TIPOPRE = "D";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Fattura d'acconto
      if this.oParentObject.w_EscFac <>"S"
        * --- Delete from TMPSTPRE
        i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"PRTIPCAU = "+cp_ToStrODBC("A");
                +" and TIPOPRE = "+cp_ToStrODBC("D");
                 )
        else
          delete from (i_cTable) where;
                PRTIPCAU = "A";
                and TIPOPRE = "D";

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- Adesso nel temporaneo abbiamo solo i record con tipo vuoto, S o F
      *     Procediamo escludendo le prestazioni che non soddisfano la richiesta
      do case
        case this.oParentObject.w_InDocum="N"
          * --- Da proformare e da fatturare
          * --- Cancelliamo tutte quelle per cui esiste una proforma o una fattura
          if this.oParentObject.w_DelPreAtt="S"
            * --- Elimina tutte le prestazioni dell'attivit� - o tutte o nessuna
            * --- Delete from TMPSTPRE
            i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
              i_cTempTable=cp_GetTempTableName(i_nConn)
              i_aIndex[1]='DASERIAL'
              cp_CreateTempTable(i_nConn,i_cTempTable,"DASERIAL "," from "+i_cQueryTable+" where PRTIPCAU='F' OR PRTIPCAU='S'",.f.,@i_aIndex)
              i_cQueryTable=i_cTempTable
              i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
            
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                    +" where exists( select 1 from "+i_cQueryTable+" where ";
                    +""+i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                    +")")
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          else
            * --- Delete from TMPSTPRE
            i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
              i_cTempTable=cp_GetTempTableName(i_nConn)
              i_aIndex[1]='DASERIAL,ROWPRE'
              cp_CreateTempTable(i_nConn,i_cTempTable,"DASERIAL,ROWPRE "," from "+i_cQueryTable+" where PRTIPCAU='F' OR PRTIPCAU='S'",.f.,@i_aIndex)
              i_cQueryTable=i_cTempTable
              i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                    +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
            
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                    +" where exists( select 1 from "+i_cQueryTable+" where ";
                    +""+i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                    +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
                    +")")
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.oParentObject.w_InDocum="P"
          * --- Proformate non fatturate
          * --- Cancelliamo tutte quelle per cui esiste una fattura
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex
            declare i_aIndex[1]
            i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
            i_cTempTable=cp_GetTempTableName(i_nConn)
            i_aIndex[1]='DASERIAL,ROWPRE'
            cp_CreateTempTable(i_nConn,i_cTempTable,"DASERIAL,ROWPRE "," from "+i_cQueryTable+" where PRTIPCAU='F'",.f.,@i_aIndex)
            i_cQueryTable=i_cTempTable
            i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
          
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where ";
                  +""+i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
                  +")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Inseriamo nella tabella temporanea TMPDPOFD (duplicazione pratica) tutte le chiavi di prestazioni che hanno un record S (proforma a saldo)
          * --- Create temporary table TMPDPOFD
          i_nIdx=cp_AddTableDef('TMPDPOFD') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3] && recupera la connessione
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          cp_CreateTempTable(i_nConn,i_cTempTable,"DASERIAL,CPROWNUM,MAX(CPROWORD) AS CPROWORD,ROWPRE,Max(TIPOPRE) as TIPOPRE "," from "+i_cTable;
                +" where PRTIPCAU='S'";
                +" group by DASERIAL, ROWPRE,CPROWORD, CPROWNUM";
                +" order by DASERIAL,ROWPRE,CPROWORD,CPROWNUM";
                )
          this.TMPDPOFD_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Vuotiamo TMPSTPRE e poi lo riempiamo di nuovo con i dati di TMPDPOFD
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex
            declare i_aIndex[1]
            i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
            i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
          
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where ";
                  +""+i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
                  +")")
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Insert into TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_cTempTable=cp_SetAzi(i_TableProp[this.TMPDPOFD_idx,2])
            i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"DASERIAL ,ROWPRE,CPROWNUM, CPROWORD, SPACE(10) AS ATRIFDOC, SPACE(5) AS MVTIPDOC, SPACE(2) AS MVCLADOC,SPACE(1) AS PRTIPCAU, 0 AS LIVELLO,TIPOPRE"," from "+i_cTempTable,this.TMPSTPRE_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Drop temporary table TMPDPOFD
          i_nIdx=cp_GetTableDefIdx('TMPDPOFD')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPDPOFD')
          endif
        case this.oParentObject.w_InDocum="A"
          * --- Proformate e fatturate
          * --- Inseriamo nel temporaneo TMPDPOFD tutte le prestazioni che hanno una proforma (e quindi tipo S) e che hanno una fattura (tipo F)
          * --- Create temporary table TMPDPOFD
          i_nIdx=cp_AddTableDef('TMPDPOFD') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\gsag1bsp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPDPOFD_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Vuotiamo TMPSTPRE e poi lo riempiamo di nuovo con i dati di TMPDPOFD
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PRTIPCAU = "+cp_ToStrODBC("A");
                   )
          else
            delete from (i_cTable) where;
                  PRTIPCAU = "A";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex
            declare i_aIndex[1]
            i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
            i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
          
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where ";
                  +""+i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
                  +")")
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Insert into TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_cTempTable=cp_SetAzi(i_TableProp[this.TMPDPOFD_idx,2])
            i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"DASERIAL ,ROWPRE,CPROWNUM, CPROWORD, SPACE(10) AS ATRIFDOC, SPACE(5) AS MVTIPDOC, SPACE(2) AS MVCLADOC,SPACE(1) AS PRTIPCAU, 0 AS LIVELLO,TIPOPRE"," from "+i_cTempTable,this.TMPSTPRE_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Drop temporary table TMPDPOFD
          i_nIdx=cp_GetTableDefIdx('TMPDPOFD')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPDPOFD')
          endif
        case this.oParentObject.w_InDocum="F"
          * --- Fatturate
          * --- Manteniamo solo le prestazioni con record F: ne avremo uno solo per ciascuna prestazione. 
          *     Per farlo, cancelliamo i tiprec vuoto o con S (proforma a saldo)
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PRTIPCAU = "+cp_ToStrODBC("A");
                  +" and TIPOPRE = "+cp_ToStrODBC("Z");
                   )
          else
            delete from (i_cTable) where;
                  PRTIPCAU = "A";
                  and TIPOPRE = "Z";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PRTIPCAU = "+cp_ToStrODBC(" ");
                   )
          else
            delete from (i_cTable) where;
                  PRTIPCAU = " ";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PRTIPCAU = "+cp_ToStrODBC("S");
                   )
          else
            delete from (i_cTable) where;
                  PRTIPCAU = "S";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        case this.oParentObject.w_InDocum="D"
          * --- Da fatturare
          * --- Cancelliamo tutte quelle per cui esiste una fattura e successivamente eliminiamo i record S di proforma a saldo
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex
            declare i_aIndex[1]
            i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
            i_cTempTable=cp_GetTempTableName(i_nConn)
            i_aIndex[1]='DASERIAL,ROWPRE'
            cp_CreateTempTable(i_nConn,i_cTempTable,"DASERIAL,ROWPRE "," from "+i_cQueryTable+" where PRTIPCAU='F'",.f.,@i_aIndex)
            i_cQueryTable=i_cTempTable
            i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
          
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where ";
                  +""+i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
                  +")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PRTIPCAU = "+cp_ToStrODBC("S");
                  +" and TIPOPRE = "+cp_ToStrODBC("A");
                   )
          else
            delete from (i_cTable) where;
                  PRTIPCAU = "S";
                  and TIPOPRE = "A";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        case this.oParentObject.w_InDocum="M"
          * --- Proformate o fatturate
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PRTIPCAU = "+cp_ToStrODBC("A");
                  +" and TIPOPRE = "+cp_ToStrODBC("Z");
                   )
          else
            delete from (i_cTable) where;
                  PRTIPCAU = "A";
                  and TIPOPRE = "Z";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PRTIPCAU = "+cp_ToStrODBC(" ");
                   )
          else
            delete from (i_cTable) where;
                  PRTIPCAU = " ";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Elimina tutte le prestazioni proformate per cui esiste la relativa fatturata
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex
            declare i_aIndex[1]
            i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
            i_cTempTable=cp_GetTempTableName(i_nConn)
            i_aIndex[1]='DASERIAL,PRTIPCAU'
            cp_CreateTempTable(i_nConn,i_cTempTable,"DASERIAL, 'S' AS PRTIPCAU "," from "+i_cQueryTable+" where PRTIPCAU='F'",.f.,@i_aIndex)
            i_cQueryTable=i_cTempTable
            i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".PRTIPCAU = "+i_cQueryTable+".PRTIPCAU";
          
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where ";
                  +""+i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".PRTIPCAU = "+i_cQueryTable+".PRTIPCAU";
                  +")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
      endcase
    endif
    if Isalt()
      * --- Elimino prestazioni collegate ad attvit� per le quali esiste un  documento
      * --- Delete from TMPSTPRE
      i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
              +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
      
        do vq_exec with 'gsagdbsp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Se � attivo un filtro sul tipo di documento (editabile solo se stato='P'), procediamo con la tracciabilit�
    if this.oParentObject.w_STATO="P" AND !this.oParentObject.w_Evasio="T"
      * --- Elabora solo un livello
      * --- Cambiare il nome TMPSTPRE con quello di una tabella temporanea
      * --- Create temporary table TMPDPOFF
      i_nIdx=cp_AddTableDef('TMPDPOFF') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSAG4BSP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPDPOFF_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      do case
        case this.oParentObject.w_Evasio="N"
          * --- Cancelliamo le prestazioni evase
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
          
            do vq_exec with 'QUERY\GSAG5BSP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- QUERY\GSAG5BSP realizzata con inner
          *     delete from TMPSTPRE where chiave in TMPDPOFF
        case this.oParentObject.w_Evasio="S"
          * --- Cancelliamo le prestazioni NON evase
          * --- Delete from TMPSTPRE
          i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
          
            do vq_exec with 'QUERY\GSAG6BSP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- QUERY\GSAG6BSP realizzata con left outer e notemptystr della chiave di TMPDPOFF
          *     delete from TMPSTPRE where chiave NOT in TMPDPOFF
      endcase
      * --- Drop temporary table TMPDPOFF
      i_nIdx=cp_GetTableDefIdx('TMPDPOFF')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPDPOFF')
      endif
    endif
    if Isalt()
      * --- Inserisco le prestazioni da anagrafica in una tabella temporanea d'appoggio.
      *     Le query hanno pochi filtri in modo da rendere pi� veloce la ricerca. Sar� poi la inner join con la GSAGXBSP a filtrare solo le prestazioni che soddisfano i filtri in maschera
      if this.oParentObject.w_InDocum="T"
        * --- Create temporary table TMPDPOFD
        i_nIdx=cp_AddTableDef('TMPDPOFD') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('gsagzbsp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPDPOFD_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Prestazioni senza nessun documento - QUERY\GSAGHBSP
        * --- In proforma a saldo - QUERY\GSAGPBSP
        * --- In proforma e in fattura a saldo - QUERY\GSAGSBSP
        * --- In fattura a saldo - QUERY\GSAGFBSP
        * --- In fattura d'acconto - QUERY\GSAGABSP
        * --- TUTTE - gsagzbsp
        do case
          case this.oParentObject.w_InDocum="N"
            * --- Da proformare e da fatturare
            * --- Inseriamo solo le prestazioni senza nessun documento
            * --- Create temporary table TMPDPOFD
            i_nIdx=cp_AddTableDef('TMPDPOFD') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('QUERY\GSAGHBSP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPDPOFD_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          case this.oParentObject.w_InDocum="P"
            * --- Proformate non fatturate
            * --- In proforma a saldo - QUERY\GSAGPBSP
            * --- Create temporary table TMPDPOFD
            i_nIdx=cp_AddTableDef('TMPDPOFD') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('QUERY\GSAGPBSP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPDPOFD_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          case this.oParentObject.w_InDocum="A"
            * --- Proformate e fatturate
            * --- In proforma e in fattura a saldo - QUERY\GSAGSBSP
            * --- Create temporary table TMPDPOFD
            i_nIdx=cp_AddTableDef('TMPDPOFD') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('QUERY\GSAGSBSP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPDPOFD_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          case this.oParentObject.w_InDocum="F"
            * --- Fatturate
            * --- In fattura a saldo - QUERY\GSAGFBSP
            * --- Create temporary table TMPDPOFD
            i_nIdx=cp_AddTableDef('TMPDPOFD') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('QUERY\GSAGFBSP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPDPOFD_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          case this.oParentObject.w_InDocum="D"
            * --- Da fatturare
            * --- Inseriamo solo le prestazioni senza nessun documento
            * --- Create temporary table TMPDPOFD
            i_nIdx=cp_AddTableDef('TMPDPOFD') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('QUERY\GSAGHBSP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPDPOFD_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            * --- Proformate
            * --- Insert into TMPDPOFD
            i_nConn=i_TableProp[this.TMPDPOFD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPDPOFD_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSAGPBSP",this.TMPDPOFD_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            if this.oParentObject.w_EscFac <>"S"
              * --- Inseriamo anche le fatture d'acconto
              * --- Insert into TMPDPOFD
              i_nConn=i_TableProp[this.TMPDPOFD_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPDPOFD_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSAGABSP",this.TMPDPOFD_idx)
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          case this.oParentObject.w_InDocum="M"
            * --- Proformate o fatturate
            * --- In proforma a saldo - QUERY\GSAGPBSP
            * --- Create temporary table TMPDPOFD
            i_nIdx=cp_AddTableDef('TMPDPOFD') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('QUERY\GSAGPBSP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPDPOFD_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            * --- Fatturate
            * --- Insert into TMPDPOFD
            i_nConn=i_TableProp[this.TMPDPOFD_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPDPOFD_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSAGFBSP",this.TMPDPOFD_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          otherwise
            * --- Tutte
            * --- Create temporary table TMPDPOFD
            i_nIdx=cp_AddTableDef('TMPDPOFD') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            vq_exec('gsagzbsp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
            this.TMPDPOFD_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endcase
      endif
      * --- Adesso facciamo la inner join con le pratiche in modo da filtrare solo quelle prestazioni che soddifano i filtri in maschera
      * --- Insert into TMPSTPRE
      i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSAGXBSP",this.TMPSTPRE_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Drop temporary table TMPDPOFD
      i_nIdx=cp_GetTableDefIdx('TMPDPOFD')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPDPOFD')
      endif
    endif
    * --- Adesso abbiamo in TMPSTPRE tutti i dati necessari da passare alle query
    if this.pParam=="STAMPA"
      * --- Attivato da stampa prestazioni
      this.w_ALTE = IIF(IsAlt(),1,0)
      L_DataIni = this.oParentObject.w_DATINI
      L_DataFin = this.oParentObject.w_DATFIN
      L_Stato = this.oParentObject.w_STATO
      L_TIPANA = this.oParentObject.w_TIPANA
      L_DatiPra = this.oParentObject.w_DatiPratica
      vq_exec(this.oParentObject.w_OQRY,this,"__TMP__")
      CP_CHPRN(this.oParentObject.w_OREP,"",this.oParentObject)
      * --- Drop temporary table TMPSTPRE
      i_nIdx=cp_GetTableDefIdx('TMPSTPRE')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPSTPRE')
      endif
    else
      * --- Attivato da ricerca prestazioni: dobbiamo riempire i campi dello zoom inserendo i dati richiesti
      if Isalt()
        * --- Se si proviene dalla maschera di 'Dissociazione spese e anticipazioni dalle prestazioni'
        if Vartype(this.oParentObject)="O" AND UPPER(this.oParentObject.class)="TGSAL_KUP" AND this.oParentObject.w_Dupl_Sposta="A"
          this.oParentObject.w_ALKDZ_ZOOM.NewExtQuery("..\ALTE\EXE\QUERY\GSAL_KDZ1")     
        else
          this.oParentObject.w_AGZRP_ZOOM.NewExtQuery("QUERY\GSAGZZRP")     
        endif
      else
        this.oParentObject.w_AGZRP_ZOOM.NewExtQuery("QUERY\GSAG1ZRP")     
      endif
      if this.pParam<>"RICERCA_ZOOM"
        if Vartype(this.oParentObject)="O" AND UPPER(this.oParentObject.class)="TGSAL_KUP" AND this.oParentObject.w_Dupl_Sposta="A"
          this.oParentObject.w_ALKDZ_ZOOM.Query()     
        else
          this.oParentObject.w_AGZRP_ZOOM.Query()     
        endif
      endif
      this.w_GSAG_KRP = this.oParentObject
      this.w_ZOOM = this.oParentObject.w_AGZRP_ZOOM
      if ( NOT (Vartype(this.oParentObject)="O" AND UPPER(this.oParentObject.class)="TGSAL_KUP" AND this.oParentObject.w_Dupl_Sposta="A") ) OR this.pParam="RICERCA_ZOOM"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if ISALT()
      this.oParentObject.w_TOTCON = 0
      this.oParentObject.w_TOTSPE = 0
      this.oParentObject.w_TOTANT = 0
      SELECT (this.w_ZOOM.cCursor)
      GO TOP
      SUM DAOREEFF, DAMINEFF , iif(UMFLTEMP="S",INT(DAQTAMOV*UMDURORE),0),IIF(UMFLTEMP="S", INT(Round((UMDURORE * DAQTAMOV - INT(UMDURORE * DAQTAMOV)) * 60,0)), 0) TO this.oParentObject.w_DURTOTORE, this.oParentObject.w_DURTOTMIN,this.oParentObject.w_TOTQTAORE,this.oParentObject.w_TOTQTAMIN
      this.oParentObject.w_DURTOTORE = this.oParentObject.w_DURTOTORE+INT(this.oParentObject.w_DURTOTMIN/60)
      this.oParentObject.w_DURTOTMIN = Mod(this.oParentObject.w_DURTOTMIN,60)
      this.oParentObject.w_TOTQTAORE = this.oParentObject.w_TOTQTAORE+INT(this.oParentObject.w_TOTQTAMIN/60)
      this.oParentObject.w_TOTQTAMIN = Mod(this.oParentObject.w_TOTQTAMIN,60)
      if !this.pParam=="STAMPA"
        if VARTYPE( this.oParentObject.w_VisTotali )="C"
          if this.oParentObject.w_VisTotali="S"
            this.oParentObject.w_FLESPA = "S"
            * --- Abbiamo attivo il flag di visualizzazione dei totali
            SUM Nvl(DAVALRIG,0) TO this.oParentObject.w_TOTCON FOR Nvl(ARPRESTA," ") $ "IETPGRC"
            SUM Nvl(DAVALRIG,0) TO this.oParentObject.w_TOTSPE FOR Nvl(ARPRESTA," ") = "S"
            SUM Nvl(DAVALRIG,0) TO this.oParentObject.w_TOTANT FOR Nvl(ARPRESTA," ") = "A"
          endif
        endif
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.oParentObject.w_EscFac="S"
      * --- Escludo fatture d'acconto
      if this.oParentObject.w_DelPreAtt="S"
        * --- Elimina tutte le prestazioni dell'attivit� - o tutte o nessuna
        * --- Delete from TMPSTPRE
        i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='DASERIAL'
          cp_CreateTempTable(i_nConn,i_cTempTable,"DASERIAL "," from "+i_cQueryTable+" where PRTIPCAU='A'",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                +")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        * --- Delete from TMPSTPRE
        i_nConn=i_TableProp[this.TMPSTPRE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSTPRE_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='DASERIAL,ROWPRE'
          cp_CreateTempTable(i_nConn,i_cTempTable,"DASERIAL,ROWPRE "," from "+i_cQueryTable+" where PRTIPCAU='A'",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
        
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where ";
                +""+i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                +" and "+i_cTable+".ROWPRE = "+i_cQueryTable+".ROWPRE";
                +")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='*TMPSTPRE'
    this.cWorkTables[2]='*TMPDPOFD'
    this.cWorkTables[3]='*TMPDPOFF'
    this.cWorkTables[4]='OFFDATTI'
    this.cWorkTables[5]='PAR_AGEN'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_query_GSAG3BSP_d_VQR')
      use in _Curs_query_GSAG3BSP_d_VQR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsag_bsp
  enddef
  
  define class MyTimer as timer
    * -- Ulteriore propriet� risprtto alla classe standard timer
    pParent=.null.
    
  proc init(p_parent)
    * -- Viene eseguita al momento della creazione dell'oggetto MyTimer
    this.pParent=p_parent
    interval=0     && In tal modo non viene eseguita la proc Timer()
   
  proc Timer()
    * -- Viene eseguita quando viene posto Interval a 100
    if type("this.pParent.class")='C'
      this.pParent.NotifyEvent("Ricerca")
    endif
    this.interval=0
    release w_TIMER
    this.destroy()
  endproc 
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
