* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bsl                                                        *
*              Seleziona/Deseleziona da inserimento multiplo                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-03-03                                                      *
* Last revis.: 2009-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bsl",oParentObject)
return(i_retval)

define class tgsag_bsl as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona/Deseleziona Tutto
    this.oParentObject.w_FLSELE = IIF ( this.oParentObject.w_SELEZI="S" ,1, 0 )
    UPDATE ( this.oParentObject.w_AGKPM_ZOOM.cCursor ) SET XCHK = this.oParentObject.w_FLSELE
     
 Select ( this.oParentObject.w_AGKPM_ZOOM.cCursor ) 
 GO TOP
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
