* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_ace                                                        *
*              Anagrafica eventi                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-19                                                      *
* Last revis.: 2015-01-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsfa_ace")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsfa_ace")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsfa_ace")
  return

* --- Class definition
define class tgsfa_ace as StdPCForm
  Width  = 770
  Height = 196+35
  Top    = 10
  Left   = 10
  cComment = "Anagrafica eventi"
  cPrg = "gsfa_ace"
  HelpContextID=109740649
  add object cnt as tcgsfa_ace
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsfa_ace as PCContext
  w_TIPOPE = space(1)
  w_EVTIPEVE = space(10)
  w_EV_STATO = space(1)
  w_EVDIREVE = space(1)
  w_EVNOMINA = space(15)
  w_EV__ANNO = space(4)
  w_EVSERIAL = space(10)
  w_EVRIFPER = space(254)
  w_EV_EMAIL = space(254)
  w_EV_EMPEC = space(254)
  w_EV_PHONE = space(18)
  w_EVCODPRA = space(15)
  w_EVDATINI = space(14)
  w_EVDATFIN = space(14)
  w_EVOREEFF = 0
  w_EVMINEFF = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = space(8)
  w_UTDV = space(8)
  w_EVOGGETT = space(100)
  w_DESNOM = space(60)
  w_DESCAN = space(100)
  w_CODPRA = space(15)
  w_FLSTATO = space(1)
  w_FLNOMIN = space(1)
  w_FLDATIN = space(1)
  w_FLPRAT = space(1)
  w_FLORE = space(1)
  w_FLMIN = space(1)
  w_FLRIFP = space(1)
  w_FLDATFI = space(1)
  w_OBTEST = space(8)
  w_EV__NOTE = space(10)
  w_FLNOTE = space(1)
  w_HTML_URL = space(254)
  w_EV_STATO = space(1)
  w_EVNOMINA = space(15)
  w_DESNOM = space(60)
  w_EVCODPRA = space(15)
  w_DESCAN = space(100)
  w_EVRIFPER = space(254)
  w_ATSERIAL = space(20)
  w_IDSERIAL = space(10)
  w_CNCODCAN = space(15)
  w_TEDRIVER = space(10)
  w_ATCODNOM = space(15)
  w_DE__TIPO = space(1)
  w_ZOOMPRA = space(10)
  w_EVIDRICH = space(15)
  w_STARIC = space(1)
  w_EVSTARIC = space(1)
  w_IDRICH = space(15)
  w_ATSEREVE = space(10)
  w_EVFLIDPA = space(1)
  w_EVTELINT = space(18)
  w_FLIDEV = space(1)
  w_NULLA = space(10)
  w_FLSTID = space(1)
  w_EVSERDOC = space(10)
  w_MVSERIAL = space(10)
  w_ATEVANNO = space(4)
  w_IDRICH = space(10)
  w_STARIC = space(1)
  w_EVCODPAR = space(5)
  w_EVLOCALI = space(30)
  w_EVNUMCEL = space(20)
  w_CODCLI = space(15)
  w_TIPNOM = space(1)
  w_i_datsys = space(8)
  w_OFDATDOC = space(8)
  proc Save(oFrom)
    this.w_TIPOPE = oFrom.w_TIPOPE
    this.w_EVTIPEVE = oFrom.w_EVTIPEVE
    this.w_EV_STATO = oFrom.w_EV_STATO
    this.w_EVDIREVE = oFrom.w_EVDIREVE
    this.w_EVNOMINA = oFrom.w_EVNOMINA
    this.w_EV__ANNO = oFrom.w_EV__ANNO
    this.w_EVSERIAL = oFrom.w_EVSERIAL
    this.w_EVRIFPER = oFrom.w_EVRIFPER
    this.w_EV_EMAIL = oFrom.w_EV_EMAIL
    this.w_EV_EMPEC = oFrom.w_EV_EMPEC
    this.w_EV_PHONE = oFrom.w_EV_PHONE
    this.w_EVCODPRA = oFrom.w_EVCODPRA
    this.w_EVDATINI = oFrom.w_EVDATINI
    this.w_EVDATFIN = oFrom.w_EVDATFIN
    this.w_EVOREEFF = oFrom.w_EVOREEFF
    this.w_EVMINEFF = oFrom.w_EVMINEFF
    this.w_UTCC = oFrom.w_UTCC
    this.w_UTCV = oFrom.w_UTCV
    this.w_UTDC = oFrom.w_UTDC
    this.w_UTDV = oFrom.w_UTDV
    this.w_EVOGGETT = oFrom.w_EVOGGETT
    this.w_DESNOM = oFrom.w_DESNOM
    this.w_DESCAN = oFrom.w_DESCAN
    this.w_CODPRA = oFrom.w_CODPRA
    this.w_FLSTATO = oFrom.w_FLSTATO
    this.w_FLNOMIN = oFrom.w_FLNOMIN
    this.w_FLDATIN = oFrom.w_FLDATIN
    this.w_FLPRAT = oFrom.w_FLPRAT
    this.w_FLORE = oFrom.w_FLORE
    this.w_FLMIN = oFrom.w_FLMIN
    this.w_FLRIFP = oFrom.w_FLRIFP
    this.w_FLDATFI = oFrom.w_FLDATFI
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_EV__NOTE = oFrom.w_EV__NOTE
    this.w_FLNOTE = oFrom.w_FLNOTE
    this.w_HTML_URL = oFrom.w_HTML_URL
    this.w_EV_STATO = oFrom.w_EV_STATO
    this.w_EVNOMINA = oFrom.w_EVNOMINA
    this.w_DESNOM = oFrom.w_DESNOM
    this.w_EVCODPRA = oFrom.w_EVCODPRA
    this.w_DESCAN = oFrom.w_DESCAN
    this.w_EVRIFPER = oFrom.w_EVRIFPER
    this.w_ATSERIAL = oFrom.w_ATSERIAL
    this.w_IDSERIAL = oFrom.w_IDSERIAL
    this.w_CNCODCAN = oFrom.w_CNCODCAN
    this.w_TEDRIVER = oFrom.w_TEDRIVER
    this.w_ATCODNOM = oFrom.w_ATCODNOM
    this.w_DE__TIPO = oFrom.w_DE__TIPO
    this.w_ZOOMPRA = oFrom.w_ZOOMPRA
    this.w_EVIDRICH = oFrom.w_EVIDRICH
    this.w_STARIC = oFrom.w_STARIC
    this.w_EVSTARIC = oFrom.w_EVSTARIC
    this.w_IDRICH = oFrom.w_IDRICH
    this.w_ATSEREVE = oFrom.w_ATSEREVE
    this.w_EVFLIDPA = oFrom.w_EVFLIDPA
    this.w_EVTELINT = oFrom.w_EVTELINT
    this.w_FLIDEV = oFrom.w_FLIDEV
    this.w_NULLA = oFrom.w_NULLA
    this.w_FLSTID = oFrom.w_FLSTID
    this.w_EVSERDOC = oFrom.w_EVSERDOC
    this.w_MVSERIAL = oFrom.w_MVSERIAL
    this.w_ATEVANNO = oFrom.w_ATEVANNO
    this.w_IDRICH = oFrom.w_IDRICH
    this.w_STARIC = oFrom.w_STARIC
    this.w_EVCODPAR = oFrom.w_EVCODPAR
    this.w_EVLOCALI = oFrom.w_EVLOCALI
    this.w_EVNUMCEL = oFrom.w_EVNUMCEL
    this.w_CODCLI = oFrom.w_CODCLI
    this.w_TIPNOM = oFrom.w_TIPNOM
    this.w_i_datsys = oFrom.w_i_datsys
    this.w_OFDATDOC = oFrom.w_OFDATDOC
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_TIPOPE = this.w_TIPOPE
    oTo.w_EVTIPEVE = this.w_EVTIPEVE
    oTo.w_EV_STATO = this.w_EV_STATO
    oTo.w_EVDIREVE = this.w_EVDIREVE
    oTo.w_EVNOMINA = this.w_EVNOMINA
    oTo.w_EV__ANNO = this.w_EV__ANNO
    oTo.w_EVSERIAL = this.w_EVSERIAL
    oTo.w_EVRIFPER = this.w_EVRIFPER
    oTo.w_EV_EMAIL = this.w_EV_EMAIL
    oTo.w_EV_EMPEC = this.w_EV_EMPEC
    oTo.w_EV_PHONE = this.w_EV_PHONE
    oTo.w_EVCODPRA = this.w_EVCODPRA
    oTo.w_EVDATINI = this.w_EVDATINI
    oTo.w_EVDATFIN = this.w_EVDATFIN
    oTo.w_EVOREEFF = this.w_EVOREEFF
    oTo.w_EVMINEFF = this.w_EVMINEFF
    oTo.w_UTCC = this.w_UTCC
    oTo.w_UTCV = this.w_UTCV
    oTo.w_UTDC = this.w_UTDC
    oTo.w_UTDV = this.w_UTDV
    oTo.w_EVOGGETT = this.w_EVOGGETT
    oTo.w_DESNOM = this.w_DESNOM
    oTo.w_DESCAN = this.w_DESCAN
    oTo.w_CODPRA = this.w_CODPRA
    oTo.w_FLSTATO = this.w_FLSTATO
    oTo.w_FLNOMIN = this.w_FLNOMIN
    oTo.w_FLDATIN = this.w_FLDATIN
    oTo.w_FLPRAT = this.w_FLPRAT
    oTo.w_FLORE = this.w_FLORE
    oTo.w_FLMIN = this.w_FLMIN
    oTo.w_FLRIFP = this.w_FLRIFP
    oTo.w_FLDATFI = this.w_FLDATFI
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_EV__NOTE = this.w_EV__NOTE
    oTo.w_FLNOTE = this.w_FLNOTE
    oTo.w_HTML_URL = this.w_HTML_URL
    oTo.w_EV_STATO = this.w_EV_STATO
    oTo.w_EVNOMINA = this.w_EVNOMINA
    oTo.w_DESNOM = this.w_DESNOM
    oTo.w_EVCODPRA = this.w_EVCODPRA
    oTo.w_DESCAN = this.w_DESCAN
    oTo.w_EVRIFPER = this.w_EVRIFPER
    oTo.w_ATSERIAL = this.w_ATSERIAL
    oTo.w_IDSERIAL = this.w_IDSERIAL
    oTo.w_CNCODCAN = this.w_CNCODCAN
    oTo.w_TEDRIVER = this.w_TEDRIVER
    oTo.w_ATCODNOM = this.w_ATCODNOM
    oTo.w_DE__TIPO = this.w_DE__TIPO
    oTo.w_ZOOMPRA = this.w_ZOOMPRA
    oTo.w_EVIDRICH = this.w_EVIDRICH
    oTo.w_STARIC = this.w_STARIC
    oTo.w_EVSTARIC = this.w_EVSTARIC
    oTo.w_IDRICH = this.w_IDRICH
    oTo.w_ATSEREVE = this.w_ATSEREVE
    oTo.w_EVFLIDPA = this.w_EVFLIDPA
    oTo.w_EVTELINT = this.w_EVTELINT
    oTo.w_FLIDEV = this.w_FLIDEV
    oTo.w_NULLA = this.w_NULLA
    oTo.w_FLSTID = this.w_FLSTID
    oTo.w_EVSERDOC = this.w_EVSERDOC
    oTo.w_MVSERIAL = this.w_MVSERIAL
    oTo.w_ATEVANNO = this.w_ATEVANNO
    oTo.w_IDRICH = this.w_IDRICH
    oTo.w_STARIC = this.w_STARIC
    oTo.w_EVCODPAR = this.w_EVCODPAR
    oTo.w_EVLOCALI = this.w_EVLOCALI
    oTo.w_EVNUMCEL = this.w_EVNUMCEL
    oTo.w_CODCLI = this.w_CODCLI
    oTo.w_TIPNOM = this.w_TIPNOM
    oTo.w_i_datsys = this.w_i_datsys
    oTo.w_OFDATDOC = this.w_OFDATDOC
    PCContext::Load(oTo)
enddefine

define class tcgsfa_ace as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 770
  Height = 196+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-23"
  HelpContextID=109740649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=71

  * --- Constant Properties
  ANEVENTI_IDX = 0
  TIPEVENT_IDX = 0
  OFF_NOMI_IDX = 0
  CAN_TIER_IDX = 0
  DRVEVENT_IDX = 0
  PRODINDI_IDX = 0
  cFile = "ANEVENTI"
  cKeySelect = "EV__ANNO,EVSERIAL"
  cKeyWhere  = "EV__ANNO=this.w_EV__ANNO and EVSERIAL=this.w_EVSERIAL"
  cKeyWhereODBC = '"EV__ANNO="+cp_ToStrODBC(this.w_EV__ANNO)';
      +'+" and EVSERIAL="+cp_ToStrODBC(this.w_EVSERIAL)';

  cKeyWhereODBCqualified = '"ANEVENTI.EV__ANNO="+cp_ToStrODBC(this.w_EV__ANNO)';
      +'+" and ANEVENTI.EVSERIAL="+cp_ToStrODBC(this.w_EVSERIAL)';

  cPrg = "gsfa_ace"
  cComment = "Anagrafica eventi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPOPE = space(1)
  w_EVTIPEVE = space(10)
  w_EV_STATO = space(1)
  w_EVDIREVE = space(1)
  w_EVNOMINA = space(15)
  o_EVNOMINA = space(15)
  w_EV__ANNO = space(4)
  w_EVSERIAL = space(10)
  o_EVSERIAL = space(10)
  w_EVRIFPER = space(254)
  w_EV_EMAIL = space(254)
  w_EV_EMPEC = space(254)
  w_EV_PHONE = space(18)
  w_EVCODPRA = space(15)
  o_EVCODPRA = space(15)
  w_EVDATINI = ctot('')
  w_EVDATFIN = ctot('')
  w_EVOREEFF = 0
  w_EVMINEFF = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_EVOGGETT = space(100)
  w_DESNOM = space(60)
  w_DESCAN = space(100)
  w_CODPRA = space(15)
  w_FLSTATO = space(1)
  w_FLNOMIN = space(1)
  w_FLDATIN = space(1)
  w_FLPRAT = space(1)
  w_FLORE = space(1)
  w_FLMIN = space(1)
  w_FLRIFP = space(1)
  w_FLDATFI = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_EV__NOTE = space(0)
  w_FLNOTE = space(1)
  w_HTML_URL = space(254)
  w_EV_STATO = space(1)
  w_EVNOMINA = space(15)
  w_DESNOM = space(60)
  w_EVCODPRA = space(15)
  w_DESCAN = space(100)
  w_EVRIFPER = space(254)
  w_ATSERIAL = space(20)
  w_IDSERIAL = space(10)
  w_CNCODCAN = space(15)
  w_TEDRIVER = space(10)
  w_ATCODNOM = space(15)
  w_DE__TIPO = space(1)
  w_ZOOMPRA = space(10)
  w_EVIDRICH = space(15)
  o_EVIDRICH = space(15)
  w_STARIC = space(1)
  w_EVSTARIC = space(1)
  w_IDRICH = space(15)
  w_ATSEREVE = space(10)
  w_EVFLIDPA = space(1)
  w_EVTELINT = space(18)
  w_FLIDEV = space(1)
  w_NULLA = space(10)
  w_FLSTID = space(1)
  w_EVSERDOC = space(10)
  w_MVSERIAL = space(10)
  w_ATEVANNO = space(4)
  w_IDRICH = space(10)
  w_STARIC = space(1)
  w_EVCODPAR = space(5)
  w_EVLOCALI = space(30)
  w_EVNUMCEL = space(20)
  w_CODCLI = space(15)
  w_TIPNOM = space(1)
  w_i_datsys = ctod('  /  /  ')
  w_OFDATDOC = ctod('  /  /  ')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_EV__ANNO = this.W_EV__ANNO
  op_EVSERIAL = this.W_EVSERIAL
  w_HTMLBROW = .NULL.
  w_ZOOMPRAT = .NULL.
  w_ZOOMUTE = .NULL.
  w_ZOOMIND = .NULL.
  w_ZOOMATT = .NULL.
  w_ZOOMDOC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=7, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsfa_acePag1","gsfa_ace",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Evento")
      .Pages(1).HelpContextID = 2771014
      .Pages(2).addobject("oPag","tgsfa_acePag2","gsfa_ace",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Anteprima")
      .Pages(2).HelpContextID = 220055933
      .Pages(3).addobject("oPag","tgsfa_acePag3","gsfa_ace",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Pratiche")
      .Pages(3).HelpContextID = 209713829
      .Pages(4).addobject("oPag","tgsfa_acePag4","gsfa_ace",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Soggetti")
      .Pages(4).HelpContextID = 70476431
      .Pages(5).addobject("oPag","tgsfa_acePag5","gsfa_ace",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Indici")
      .Pages(5).HelpContextID = 152383622
      .Pages(6).addobject("oPag","tgsfa_acePag6","gsfa_ace",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Attivit�")
      .Pages(6).HelpContextID = 172373734
      .Pages(7).addobject("oPag","tgsfa_acePag7","gsfa_ace",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Documenti")
      .Pages(7).HelpContextID = 96544826
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oEV_STATO_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
    proc Init()
      this.w_HTMLBROW = this.oPgFrm.Pages(2).oPag.HTMLBROW
      this.w_ZOOMPRAT = this.oPgFrm.Pages(3).oPag.ZOOMPRAT
      this.w_ZOOMUTE = this.oPgFrm.Pages(4).oPag.ZOOMUTE
      this.w_ZOOMIND = this.oPgFrm.Pages(5).oPag.ZOOMIND
      this.w_ZOOMATT = this.oPgFrm.Pages(6).oPag.ZOOMATT
      this.w_ZOOMDOC = this.oPgFrm.Pages(7).oPag.ZOOMDOC
      DoDefault()
    proc Destroy()
      this.w_HTMLBROW = .NULL.
      this.w_ZOOMPRAT = .NULL.
      this.w_ZOOMUTE = .NULL.
      this.w_ZOOMIND = .NULL.
      this.w_ZOOMATT = .NULL.
      this.w_ZOOMDOC = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='TIPEVENT'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='DRVEVENT'
    this.cWorkTables[5]='PRODINDI'
    this.cWorkTables[6]='ANEVENTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANEVENTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANEVENTI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsfa_ace'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ANEVENTI where EV__ANNO=KeySet.EV__ANNO
    *                            and EVSERIAL=KeySet.EVSERIAL
    *
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANEVENTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANEVENTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANEVENTI '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'EV__ANNO',this.w_EV__ANNO  ,'EVSERIAL',this.w_EVSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESNOM = space(60)
        .w_DESCAN = space(100)
        .w_FLSTATO = 'N'
        .w_FLNOMIN = 'N'
        .w_FLDATIN = 'N'
        .w_FLPRAT = 'N'
        .w_FLORE = 'N'
        .w_FLMIN = 'N'
        .w_FLRIFP = 'N'
        .w_FLDATFI = 'N'
        .w_FLNOTE = 'N'
        .w_DESNOM = space(60)
        .w_DESCAN = space(100)
        .w_TEDRIVER = space(10)
        .w_DE__TIPO = space(1)
        .w_STARIC = space(1)
        .w_IDRICH = space(15)
        .w_ATSEREVE = .w_EVSERIAL
        .w_FLIDEV = 'N'
        .w_FLSTID = 'N'
        .w_ATEVANNO = .w_EV__ANNO
        .w_IDRICH = space(10)
        .w_STARIC = space(1)
        .w_CODCLI = space(15)
        .w_TIPNOM = space(1)
        .w_i_datsys = i_DatSys
        .w_OFDATDOC = i_DatSys
        .w_TIPOPE = This.oparentobject .w_TIPOPE
        .w_EVTIPEVE = NVL(EVTIPEVE,space(10))
          if link_1_2_joined
            this.w_EVTIPEVE = NVL(TETIPEVE102,NVL(this.w_EVTIPEVE,space(10)))
            this.w_TEDRIVER = NVL(TEDRIVER102,space(10))
          else
          .link_1_2('Load')
          endif
        .w_EV_STATO = NVL(EV_STATO,space(1))
        .w_EVDIREVE = NVL(EVDIREVE,space(1))
        .w_EVNOMINA = NVL(EVNOMINA,space(15))
          if link_1_6_joined
            this.w_EVNOMINA = NVL(NOCODICE106,NVL(this.w_EVNOMINA,space(15)))
            this.w_DESNOM = NVL(NODESCRI106,space(60))
            this.w_CODCLI = NVL(NOCODCLI106,space(15))
            this.w_TIPNOM = NVL(NOTIPNOM106,space(1))
          else
          .link_1_6('Load')
          endif
        .w_EV__ANNO = NVL(EV__ANNO,space(4))
        .op_EV__ANNO = .w_EV__ANNO
        .w_EVSERIAL = NVL(EVSERIAL,space(10))
        .op_EVSERIAL = .w_EVSERIAL
        .w_EVRIFPER = NVL(EVRIFPER,space(254))
        .w_EV_EMAIL = NVL(EV_EMAIL,space(254))
        .w_EV_EMPEC = NVL(EV_EMPEC,space(254))
        .w_EV_PHONE = NVL(EV_PHONE,space(18))
        .w_EVCODPRA = NVL(EVCODPRA,space(15))
          if link_1_16_joined
            this.w_EVCODPRA = NVL(CNCODCAN116,NVL(this.w_EVCODPRA,space(15)))
            this.w_DESCAN = NVL(CNDESCAN116,space(100))
          else
          .link_1_16('Load')
          endif
        .w_EVDATINI = NVL(EVDATINI,ctot(""))
        .w_EVDATFIN = NVL(EVDATFIN,ctot(""))
        .w_EVOREEFF = NVL(EVOREEFF,0)
        .w_EVMINEFF = NVL(EVMINEFF,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_EVOGGETT = NVL(EVOGGETT,space(100))
        .w_CODPRA = .w_EVCODPRA
        .w_OBTEST = i_datsys
        .w_EV__NOTE = NVL(EV__NOTE,space(0))
        .w_HTML_URL = .w_HTML_URL
        .oPgFrm.Page2.oPag.HTMLBROW.Calculate(.w_HTML_URL)
        .w_EV_STATO = NVL(EV_STATO,space(1))
        .w_EVNOMINA = NVL(EVNOMINA,space(15))
          if link_2_5_joined
            this.w_EVNOMINA = NVL(NOCODICE205,NVL(this.w_EVNOMINA,space(15)))
            this.w_DESNOM = NVL(NODESCRI205,space(60))
          else
          .link_2_5('Load')
          endif
        .w_EVCODPRA = NVL(EVCODPRA,space(15))
          if link_2_8_joined
            this.w_EVCODPRA = NVL(CNCODCAN208,NVL(this.w_EVCODPRA,space(15)))
            this.w_DESCAN = NVL(CNDESCAN208,space(100))
          else
          .link_2_8('Load')
          endif
        .w_EVRIFPER = NVL(EVRIFPER,space(254))
        .oPgFrm.Page3.oPag.ZOOMPRAT.Calculate(.w_EVNOMINA)
        .oPgFrm.Page4.oPag.ZOOMUTE.Calculate(.w_EVCODPRA)
        .oPgFrm.Page5.oPag.ZOOMIND.Calculate(.w_EVSERIAL)
        .oPgFrm.Page6.oPag.ZOOMATT.Calculate(.w_EVSERIAL)
        .w_ATSERIAL = .w_ZOOMATT.GETVAR('ATSERIAL')
        .w_IDSERIAL = .w_ZOOMIND.GETVAR('IDSERIAL')
        .w_CNCODCAN = .w_ZOOMPRAT.GETVAR('CNCODCAN')
          .link_1_44('Load')
        .w_ATCODNOM = .w_EVNOMINA
        .w_ZOOMPRA = IIF(Isalt(),'GSFAZAEV',' ')
        .w_EVIDRICH = NVL(EVIDRICH,space(15))
        .w_EVSTARIC = NVL(EVSTARIC,space(1))
          .link_1_54('Load')
        .w_EVFLIDPA = NVL(EVFLIDPA,space(1))
        .w_EVTELINT = NVL(EVTELINT,space(18))
        .w_NULLA = 'X'
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .w_EVSERDOC = NVL(EVSERDOC,space(10))
        .w_MVSERIAL = .w_ZOOMDOC.GETVAR('MVSERIAL')
        .oPgFrm.Page7.oPag.ZOOMDOC.Calculate(.w_EVSERIAL)
        .w_EVCODPAR = NVL(EVCODPAR,space(5))
        .w_EVLOCALI = NVL(EVLOCALI,space(30))
        .w_EVNUMCEL = NVL(EVNUMCEL,space(20))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ANEVENTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_TIPOPE = space(1)
      .w_EVTIPEVE = space(10)
      .w_EV_STATO = space(1)
      .w_EVDIREVE = space(1)
      .w_EVNOMINA = space(15)
      .w_EV__ANNO = space(4)
      .w_EVSERIAL = space(10)
      .w_EVRIFPER = space(254)
      .w_EV_EMAIL = space(254)
      .w_EV_EMPEC = space(254)
      .w_EV_PHONE = space(18)
      .w_EVCODPRA = space(15)
      .w_EVDATINI = ctot("")
      .w_EVDATFIN = ctot("")
      .w_EVOREEFF = 0
      .w_EVMINEFF = 0
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_EVOGGETT = space(100)
      .w_DESNOM = space(60)
      .w_DESCAN = space(100)
      .w_CODPRA = space(15)
      .w_FLSTATO = space(1)
      .w_FLNOMIN = space(1)
      .w_FLDATIN = space(1)
      .w_FLPRAT = space(1)
      .w_FLORE = space(1)
      .w_FLMIN = space(1)
      .w_FLRIFP = space(1)
      .w_FLDATFI = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_EV__NOTE = space(0)
      .w_FLNOTE = space(1)
      .w_HTML_URL = space(254)
      .w_EV_STATO = space(1)
      .w_EVNOMINA = space(15)
      .w_DESNOM = space(60)
      .w_EVCODPRA = space(15)
      .w_DESCAN = space(100)
      .w_EVRIFPER = space(254)
      .w_ATSERIAL = space(20)
      .w_IDSERIAL = space(10)
      .w_CNCODCAN = space(15)
      .w_TEDRIVER = space(10)
      .w_ATCODNOM = space(15)
      .w_DE__TIPO = space(1)
      .w_ZOOMPRA = space(10)
      .w_EVIDRICH = space(15)
      .w_STARIC = space(1)
      .w_EVSTARIC = space(1)
      .w_IDRICH = space(15)
      .w_ATSEREVE = space(10)
      .w_EVFLIDPA = space(1)
      .w_EVTELINT = space(18)
      .w_FLIDEV = space(1)
      .w_NULLA = space(10)
      .w_FLSTID = space(1)
      .w_EVSERDOC = space(10)
      .w_MVSERIAL = space(10)
      .w_ATEVANNO = space(4)
      .w_IDRICH = space(10)
      .w_STARIC = space(1)
      .w_EVCODPAR = space(5)
      .w_EVLOCALI = space(30)
      .w_EVNUMCEL = space(20)
      .w_CODCLI = space(15)
      .w_TIPNOM = space(1)
      .w_i_datsys = ctod("  /  /  ")
      .w_OFDATDOC = ctod("  /  /  ")
      if .cFunction<>"Filter"
        .w_TIPOPE = This.oparentobject .w_TIPOPE
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_EVTIPEVE))
          .link_1_2('Full')
          endif
        .w_EV_STATO = 'D'
        .w_EVDIREVE = 'E'
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_EVNOMINA))
          .link_1_6('Full')
          endif
        .w_EV__ANNO = CALCESER(i_DATSYS)
        .DoRTCalc(7,12,.f.)
          if not(empty(.w_EVCODPRA))
          .link_1_16('Full')
          endif
          .DoRTCalc(13,23,.f.)
        .w_CODPRA = .w_EVCODPRA
        .w_FLSTATO = 'N'
        .w_FLNOMIN = 'N'
        .w_FLDATIN = 'N'
        .w_FLPRAT = 'N'
        .w_FLORE = 'N'
        .w_FLMIN = 'N'
        .w_FLRIFP = 'N'
        .w_FLDATFI = 'N'
        .w_OBTEST = i_datsys
          .DoRTCalc(34,34,.f.)
        .w_FLNOTE = 'N'
        .w_HTML_URL = .w_HTML_URL
        .oPgFrm.Page2.oPag.HTMLBROW.Calculate(.w_HTML_URL)
        .w_EV_STATO = 'D'
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_EVNOMINA))
          .link_2_5('Full')
          endif
        .DoRTCalc(39,40,.f.)
          if not(empty(.w_EVCODPRA))
          .link_2_8('Full')
          endif
        .oPgFrm.Page3.oPag.ZOOMPRAT.Calculate(.w_EVNOMINA)
        .oPgFrm.Page4.oPag.ZOOMUTE.Calculate(.w_EVCODPRA)
        .oPgFrm.Page5.oPag.ZOOMIND.Calculate(.w_EVSERIAL)
        .oPgFrm.Page6.oPag.ZOOMATT.Calculate(.w_EVSERIAL)
          .DoRTCalc(41,42,.f.)
        .w_ATSERIAL = .w_ZOOMATT.GETVAR('ATSERIAL')
        .w_IDSERIAL = .w_ZOOMIND.GETVAR('IDSERIAL')
        .w_CNCODCAN = .w_ZOOMPRAT.GETVAR('CNCODCAN')
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_TEDRIVER))
          .link_1_44('Full')
          endif
        .w_ATCODNOM = .w_EVNOMINA
          .DoRTCalc(48,48,.f.)
        .w_ZOOMPRA = IIF(Isalt(),'GSFAZAEV',' ')
          .DoRTCalc(50,51,.f.)
        .w_EVSTARIC = iif(.w_TIPOPE='C',.w_STARIC,' ')
          .DoRTCalc(53,53,.f.)
        .w_ATSEREVE = .w_EVSERIAL
        .DoRTCalc(54,54,.f.)
          if not(empty(.w_ATSEREVE))
          .link_1_54('Full')
          endif
          .DoRTCalc(55,56,.f.)
        .w_FLIDEV = 'N'
        .w_NULLA = 'X'
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .w_FLSTID = 'N'
          .DoRTCalc(60,60,.f.)
        .w_MVSERIAL = .w_ZOOMDOC.GETVAR('MVSERIAL')
        .oPgFrm.Page7.oPag.ZOOMDOC.Calculate(.w_EVSERIAL)
        .w_ATEVANNO = .w_EV__ANNO
          .DoRTCalc(63,69,.f.)
        .w_i_datsys = i_DatSys
        .w_OFDATDOC = i_DatSys
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANEVENTI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEREVE","i_codazi,w_EV__ANNO,w_EVSERIAL")
      .op_codazi = .w_codazi
      .op_EV__ANNO = .w_EV__ANNO
      .op_EVSERIAL = .w_EVSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oEV_STATO_1_4.enabled = i_bVal
      .Page1.oPag.oEVNOMINA_1_6.enabled = i_bVal
      .Page1.oPag.oEVRIFPER_1_11.enabled = i_bVal
      .Page1.oPag.oEVCODPRA_1_16.enabled = i_bVal
      .Page1.oPag.oEVOREEFF_1_19.enabled = i_bVal
      .Page1.oPag.oEVMINEFF_1_20.enabled = i_bVal
      .Page1.oPag.oFLSTATO_1_32.enabled = i_bVal
      .Page1.oPag.oFLNOMIN_1_33.enabled = i_bVal
      .Page1.oPag.oFLPRAT_1_35.enabled = i_bVal
      .Page1.oPag.oFLORE_1_36.enabled = i_bVal
      .Page1.oPag.oFLMIN_1_37.enabled = i_bVal
      .Page1.oPag.oFLRIFP_1_38.enabled = i_bVal
      .Page1.oPag.oEV__NOTE_1_41.enabled = i_bVal
      .Page1.oPag.oFLNOTE_1_42.enabled = i_bVal
      .Page2.oPag.oEV_STATO_2_3.enabled = i_bVal
      .Page2.oPag.oEVNOMINA_2_5.enabled = i_bVal
      .Page2.oPag.oEVCODPRA_2_8.enabled = i_bVal
      .Page2.oPag.oEVRIFPER_2_12.enabled = i_bVal
      .Page1.oPag.oEVSTARIC_1_51.enabled = i_bVal
      .Page1.oPag.oATSEREVE_1_54.enabled = i_bVal
      .Page1.oPag.oFLSTID_1_59.enabled = i_bVal
      .Page1.oPag.oBtn_1_3.enabled = i_bVal
      .Page2.oPag.HTMLBROW.enabled = i_bVal
      .Page3.oPag.ZOOMPRAT.enabled = i_bVal
      .Page4.oPag.ZOOMUTE.enabled = i_bVal
      .Page5.oPag.ZOOMIND.enabled = i_bVal
      .Page6.oPag.ZOOMATT.enabled = i_bVal
      .Page1.oPag.oObj_1_58.enabled = i_bVal
      .Page7.oPag.ZOOMDOC.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ANEVENTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVTIPEVE,"EVTIPEVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV_STATO,"EV_STATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVDIREVE,"EVDIREVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVNOMINA,"EVNOMINA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV__ANNO,"EV__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVSERIAL,"EVSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVRIFPER,"EVRIFPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV_EMAIL,"EV_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV_EMPEC,"EV_EMPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV_PHONE,"EV_PHONE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVCODPRA,"EVCODPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVDATINI,"EVDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVDATFIN,"EVDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVOREEFF,"EVOREEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVMINEFF,"EVMINEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVOGGETT,"EVOGGETT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV__NOTE,"EV__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EV_STATO,"EV_STATO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVNOMINA,"EVNOMINA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVCODPRA,"EVCODPRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVRIFPER,"EVRIFPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVIDRICH,"EVIDRICH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVSTARIC,"EVSTARIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVFLIDPA,"EVFLIDPA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVTELINT,"EVTELINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVSERDOC,"EVSERDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVCODPAR,"EVCODPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVLOCALI,"EVLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EVNUMCEL,"EVNUMCEL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ANEVENTI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEREVE","i_codazi,w_EV__ANNO,w_EVSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ANEVENTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANEVENTI')
        i_extval=cp_InsertValODBCExtFlds(this,'ANEVENTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(EVTIPEVE,EV_STATO,EVDIREVE,EVNOMINA,EV__ANNO"+;
                  ",EVSERIAL,EVRIFPER,EV_EMAIL,EV_EMPEC,EV_PHONE"+;
                  ",EVCODPRA,EVDATINI,EVDATFIN,EVOREEFF,EVMINEFF"+;
                  ",UTCC,UTCV,UTDC,UTDV,EVOGGETT"+;
                  ",EV__NOTE,EVIDRICH,EVSTARIC,EVFLIDPA,EVTELINT"+;
                  ",EVSERDOC,EVCODPAR,EVLOCALI,EVNUMCEL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_EVTIPEVE)+;
                  ","+cp_ToStrODBC(this.w_EV_STATO)+;
                  ","+cp_ToStrODBC(this.w_EVDIREVE)+;
                  ","+cp_ToStrODBCNull(this.w_EVNOMINA)+;
                  ","+cp_ToStrODBC(this.w_EV__ANNO)+;
                  ","+cp_ToStrODBC(this.w_EVSERIAL)+;
                  ","+cp_ToStrODBC(this.w_EVRIFPER)+;
                  ","+cp_ToStrODBC(this.w_EV_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_EV_EMPEC)+;
                  ","+cp_ToStrODBC(this.w_EV_PHONE)+;
                  ","+cp_ToStrODBCNull(this.w_EVCODPRA)+;
                  ","+cp_ToStrODBC(this.w_EVDATINI)+;
                  ","+cp_ToStrODBC(this.w_EVDATFIN)+;
                  ","+cp_ToStrODBC(this.w_EVOREEFF)+;
                  ","+cp_ToStrODBC(this.w_EVMINEFF)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_EVOGGETT)+;
                  ","+cp_ToStrODBC(this.w_EV__NOTE)+;
                  ","+cp_ToStrODBC(this.w_EVIDRICH)+;
                  ","+cp_ToStrODBC(this.w_EVSTARIC)+;
                  ","+cp_ToStrODBC(this.w_EVFLIDPA)+;
                  ","+cp_ToStrODBC(this.w_EVTELINT)+;
                  ","+cp_ToStrODBC(this.w_EVSERDOC)+;
                  ","+cp_ToStrODBC(this.w_EVCODPAR)+;
                  ","+cp_ToStrODBC(this.w_EVLOCALI)+;
                  ","+cp_ToStrODBC(this.w_EVNUMCEL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANEVENTI')
        i_extval=cp_InsertValVFPExtFlds(this,'ANEVENTI')
        cp_CheckDeletedKey(i_cTable,0,'EV__ANNO',this.w_EV__ANNO,'EVSERIAL',this.w_EVSERIAL)
        INSERT INTO (i_cTable);
              (EVTIPEVE,EV_STATO,EVDIREVE,EVNOMINA,EV__ANNO,EVSERIAL,EVRIFPER,EV_EMAIL,EV_EMPEC,EV_PHONE,EVCODPRA,EVDATINI,EVDATFIN,EVOREEFF,EVMINEFF,UTCC,UTCV,UTDC,UTDV,EVOGGETT,EV__NOTE,EVIDRICH,EVSTARIC,EVFLIDPA,EVTELINT,EVSERDOC,EVCODPAR,EVLOCALI,EVNUMCEL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_EVTIPEVE;
                  ,this.w_EV_STATO;
                  ,this.w_EVDIREVE;
                  ,this.w_EVNOMINA;
                  ,this.w_EV__ANNO;
                  ,this.w_EVSERIAL;
                  ,this.w_EVRIFPER;
                  ,this.w_EV_EMAIL;
                  ,this.w_EV_EMPEC;
                  ,this.w_EV_PHONE;
                  ,this.w_EVCODPRA;
                  ,this.w_EVDATINI;
                  ,this.w_EVDATFIN;
                  ,this.w_EVOREEFF;
                  ,this.w_EVMINEFF;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_EVOGGETT;
                  ,this.w_EV__NOTE;
                  ,this.w_EVIDRICH;
                  ,this.w_EVSTARIC;
                  ,this.w_EVFLIDPA;
                  ,this.w_EVTELINT;
                  ,this.w_EVSERDOC;
                  ,this.w_EVCODPAR;
                  ,this.w_EVLOCALI;
                  ,this.w_EVNUMCEL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ANEVENTI_IDX,i_nConn)
      *
      * update ANEVENTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ANEVENTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " EVTIPEVE="+cp_ToStrODBCNull(this.w_EVTIPEVE)+;
             ",EV_STATO="+cp_ToStrODBC(this.w_EV_STATO)+;
             ",EVDIREVE="+cp_ToStrODBC(this.w_EVDIREVE)+;
             ",EVNOMINA="+cp_ToStrODBCNull(this.w_EVNOMINA)+;
             ",EVRIFPER="+cp_ToStrODBC(this.w_EVRIFPER)+;
             ",EV_EMAIL="+cp_ToStrODBC(this.w_EV_EMAIL)+;
             ",EV_EMPEC="+cp_ToStrODBC(this.w_EV_EMPEC)+;
             ",EV_PHONE="+cp_ToStrODBC(this.w_EV_PHONE)+;
             ",EVCODPRA="+cp_ToStrODBCNull(this.w_EVCODPRA)+;
             ",EVDATINI="+cp_ToStrODBC(this.w_EVDATINI)+;
             ",EVDATFIN="+cp_ToStrODBC(this.w_EVDATFIN)+;
             ",EVOREEFF="+cp_ToStrODBC(this.w_EVOREEFF)+;
             ",EVMINEFF="+cp_ToStrODBC(this.w_EVMINEFF)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",EVOGGETT="+cp_ToStrODBC(this.w_EVOGGETT)+;
             ",EV__NOTE="+cp_ToStrODBC(this.w_EV__NOTE)+;
             ",EVIDRICH="+cp_ToStrODBC(this.w_EVIDRICH)+;
             ",EVSTARIC="+cp_ToStrODBC(this.w_EVSTARIC)+;
             ",EVFLIDPA="+cp_ToStrODBC(this.w_EVFLIDPA)+;
             ",EVTELINT="+cp_ToStrODBC(this.w_EVTELINT)+;
             ",EVSERDOC="+cp_ToStrODBC(this.w_EVSERDOC)+;
             ",EVCODPAR="+cp_ToStrODBC(this.w_EVCODPAR)+;
             ",EVLOCALI="+cp_ToStrODBC(this.w_EVLOCALI)+;
             ",EVNUMCEL="+cp_ToStrODBC(this.w_EVNUMCEL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ANEVENTI')
        i_cWhere = cp_PKFox(i_cTable  ,'EV__ANNO',this.w_EV__ANNO  ,'EVSERIAL',this.w_EVSERIAL  )
        UPDATE (i_cTable) SET;
              EVTIPEVE=this.w_EVTIPEVE;
             ,EV_STATO=this.w_EV_STATO;
             ,EVDIREVE=this.w_EVDIREVE;
             ,EVNOMINA=this.w_EVNOMINA;
             ,EVRIFPER=this.w_EVRIFPER;
             ,EV_EMAIL=this.w_EV_EMAIL;
             ,EV_EMPEC=this.w_EV_EMPEC;
             ,EV_PHONE=this.w_EV_PHONE;
             ,EVCODPRA=this.w_EVCODPRA;
             ,EVDATINI=this.w_EVDATINI;
             ,EVDATFIN=this.w_EVDATFIN;
             ,EVOREEFF=this.w_EVOREEFF;
             ,EVMINEFF=this.w_EVMINEFF;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,EVOGGETT=this.w_EVOGGETT;
             ,EV__NOTE=this.w_EV__NOTE;
             ,EVIDRICH=this.w_EVIDRICH;
             ,EVSTARIC=this.w_EVSTARIC;
             ,EVFLIDPA=this.w_EVFLIDPA;
             ,EVTELINT=this.w_EVTELINT;
             ,EVSERDOC=this.w_EVSERDOC;
             ,EVCODPAR=this.w_EVCODPAR;
             ,EVLOCALI=this.w_EVLOCALI;
             ,EVNUMCEL=this.w_EVNUMCEL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ANEVENTI_IDX,i_nConn)
      *
      * delete ANEVENTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'EV__ANNO',this.w_EV__ANNO  ,'EVSERIAL',this.w_EVSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    if i_bUpd
      with this
            .w_TIPOPE = This.oparentobject .w_TIPOPE
          .link_1_2('Full')
        .DoRTCalc(3,23,.t.)
        if .o_EVCODPRA<>.w_EVCODPRA
            .w_CODPRA = .w_EVCODPRA
        endif
        .DoRTCalc(25,32,.t.)
            .w_OBTEST = i_datsys
        .DoRTCalc(34,35,.t.)
            .w_HTML_URL = .w_HTML_URL
        .oPgFrm.Page2.oPag.HTMLBROW.Calculate(.w_HTML_URL)
        if .o_EVNOMINA<>.w_EVNOMINA
        .oPgFrm.Page3.oPag.ZOOMPRAT.Calculate(.w_EVNOMINA)
        endif
        if .o_EVCODPRA<>.w_EVCODPRA
        .oPgFrm.Page4.oPag.ZOOMUTE.Calculate(.w_EVCODPRA)
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
        .oPgFrm.Page5.oPag.ZOOMIND.Calculate(.w_EVSERIAL)
        endif
        if .o_EVSERIAL<>.w_EVSERIAL
        .oPgFrm.Page6.oPag.ZOOMATT.Calculate(.w_EVSERIAL)
        endif
        .DoRTCalc(37,42,.t.)
            .w_ATSERIAL = .w_ZOOMATT.GETVAR('ATSERIAL')
            .w_IDSERIAL = .w_ZOOMIND.GETVAR('IDSERIAL')
            .w_CNCODCAN = .w_ZOOMPRAT.GETVAR('CNCODCAN')
          .link_1_44('Full')
        if .o_EVNOMINA<>.w_EVNOMINA
            .w_ATCODNOM = .w_EVNOMINA
        endif
        .DoRTCalc(48,48,.t.)
            .w_ZOOMPRA = IIF(Isalt(),'GSFAZAEV',' ')
        .DoRTCalc(50,57,.t.)
        if .o_EVSERIAL<>.w_EVSERIAL.or. .o_EVNOMINA<>.w_EVNOMINA.or. .o_EVCODPRA<>.w_EVCODPRA
            .w_NULLA = 'X'
        endif
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .DoRTCalc(59,60,.t.)
            .w_MVSERIAL = .w_ZOOMDOC.GETVAR('MVSERIAL')
        if .o_EVSERIAL<>.w_EVSERIAL
        .oPgFrm.Page7.oPag.ZOOMDOC.Calculate(.w_EVSERIAL)
        endif
        if .o_EVIDRICH<>.w_EVIDRICH
          .Calculate_KOVDKNYGKF()
        endif
        * --- Area Manuale = Calculate
        * --- gsfa_ace
        This.bupdated=.f.
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi .or. .op_EV__ANNO<>.w_EV__ANNO
           cp_AskTableProg(this,i_nConn,"SEREVE","i_codazi,w_EV__ANNO,w_EVSERIAL")
          .op_EVSERIAL = .w_EVSERIAL
        endif
        .op_codazi = .w_codazi
        .op_EV__ANNO = .w_EV__ANNO
      endwith
      this.DoRTCalc(62,71,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.HTMLBROW.Calculate(.w_HTML_URL)
        .oPgFrm.Page3.oPag.ZOOMPRAT.Calculate(.w_EVNOMINA)
        .oPgFrm.Page4.oPag.ZOOMUTE.Calculate(.w_EVCODPRA)
        .oPgFrm.Page5.oPag.ZOOMIND.Calculate(.w_EVSERIAL)
        .oPgFrm.Page6.oPag.ZOOMATT.Calculate(.w_EVSERIAL)
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page7.oPag.ZOOMDOC.Calculate(.w_EVSERIAL)
    endwith
  return

  proc Calculate_PCCPPBWXPE()
    with this
          * --- Apre attivit�
          .a = opengest('A','GSAG_AAT','ATSERIAL',.w_ATSERIAL)
    endwith
  endproc
  proc Calculate_PVSAMCKYJR()
    with this
          * --- Apre indice
          .a = opengest('A','GSUT_AID','IDSERIAL',.w_IDSERIAL)
    endwith
  endproc
  proc Calculate_DVGCPGZYMB()
    with this
          * --- Apre pratica
          .a = opengest('A','GSPR_ACN','CNCODCAN',.w_CNCODCAN)
    endwith
  endproc
  proc Calculate_OPYTELXVXZ()
    with this
          * --- Apre documenti ahr
          GSAR_BZM(this;
              ,.w_MVSERIAL;
              ,-20;
             )
    endwith
  endproc
  proc Calculate_XMBQKGYYTP()
    with this
          * --- Apre documenti ahe
          GSMA_BZM(this;
              ,.w_MVSERIAL;
              ,-20;
             )
    endwith
  endproc
  proc Calculate_KOVDKNYGKF()
    with this
          * --- Cambio stato richiesta
          .w_EVSTARIC = IIF(EMPTY(.w_EVIDRICH),' ','A')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oEVSTARIC_1_51.enabled = this.oPgFrm.Page1.oPag.oEVSTARIC_1_51.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show2
    i_show2=not(!Isalt())
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Pratiche"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    local i_show3
    i_show3=not(!Isalt())
    this.oPgFrm.Pages(4).enabled=i_show3
    this.oPgFrm.Pages(4).caption=iif(i_show3,cp_translate("Soggetti"),"")
    this.oPgFrm.Pages(4).oPag.visible=this.oPgFrm.Pages(4).enabled
    this.oPgFrm.Page1.oPag.oBtn_1_3.visible=!this.oPgFrm.Page1.oPag.oBtn_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_10.visible=!this.oPgFrm.Page2.oPag.oStr_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_11.visible=!this.oPgFrm.Page2.oPag.oStr_2_11.mHide()
    this.oPgFrm.Page1.oPag.oEVIDRICH_1_48.visible=!this.oPgFrm.Page1.oPag.oEVIDRICH_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oEVSTARIC_1_51.visible=!this.oPgFrm.Page1.oPag.oEVSTARIC_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oATSEREVE_1_54.visible=!this.oPgFrm.Page1.oPag.oATSEREVE_1_54.mHide()
    this.oPgFrm.Page1.oPag.oFLSTID_1_59.visible=!this.oPgFrm.Page1.oPag.oFLSTID_1_59.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsfa_ace
    *--- Abilito bottone collega indici
    If m.cEvent="ActivatePage"
       This.oParentObject.mEnableControls()
    EndIf
    if cevent='Progre'
       cp_BeginTrs()
       This.w_EVIDRICH=Space(15)
       i_Conn=i_TableProp[this.ANEVENTI_IDX, 3]
       cp_NextTableProg(this, i_Conn, "IDRICH", "i_codazi,w_EVIDRICH")
       This.w_EVFLIDPA='S'
       cp_EndTrs(.t.)
       This.Notifyevent("AggEventi")
    Endif
    IF CEVENT='w_zoomdoc selected'
     if Isahe()
       This.Notifyevent('ApriAhe')
     else
       This.Notifyevent('ApriAhr')
     endif
    Endif
    This.bupdated=.f.
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.HTMLBROW.Event(cEvent)
      .oPgFrm.Page3.oPag.ZOOMPRAT.Event(cEvent)
      .oPgFrm.Page4.oPag.ZOOMUTE.Event(cEvent)
      .oPgFrm.Page5.oPag.ZOOMIND.Event(cEvent)
      .oPgFrm.Page6.oPag.ZOOMATT.Event(cEvent)
        if lower(cEvent)==lower("w_ZOOMATT selected")
          .Calculate_PCCPPBWXPE()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMIND selected")
          .Calculate_PVSAMCKYJR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMPRAT selected")
          .Calculate_DVGCPGZYMB()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
      .oPgFrm.Page7.oPag.ZOOMDOC.Event(cEvent)
        if lower(cEvent)==lower("ApriAhr")
          .Calculate_OPYTELXVXZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ApriAhe")
          .Calculate_XMBQKGYYTP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=EVTIPEVE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPEVENT_IDX,3]
    i_lTable = "TIPEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2], .t., this.TIPEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVTIPEVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVTIPEVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TETIPEVE,TEDRIVER";
                   +" from "+i_cTable+" "+i_lTable+" where TETIPEVE="+cp_ToStrODBC(this.w_EVTIPEVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TETIPEVE',this.w_EVTIPEVE)
            select TETIPEVE,TEDRIVER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVTIPEVE = NVL(_Link_.TETIPEVE,space(10))
      this.w_TEDRIVER = NVL(_Link_.TEDRIVER,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_EVTIPEVE = space(10)
      endif
      this.w_TEDRIVER = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])+'\'+cp_ToStr(_Link_.TETIPEVE,1)
      cp_ShowWarn(i_cKey,this.TIPEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVTIPEVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPEVENT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPEVENT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.TETIPEVE as TETIPEVE102"+ ",link_1_2.TEDRIVER as TEDRIVER102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on ANEVENTI.EVTIPEVE=link_1_2.TETIPEVE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and ANEVENTI.EVTIPEVE=link_1_2.TETIPEVE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EVNOMINA
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVNOMINA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_EVNOMINA)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOCODCLI,NOTIPNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_EVNOMINA))
          select NOCODICE,NODESCRI,NOCODCLI,NOTIPNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVNOMINA)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_EVNOMINA)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOCODCLI,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_EVNOMINA)+"%");

            select NOCODICE,NODESCRI,NOCODCLI,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_EVNOMINA) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oEVNOMINA_1_6'),i_cWhere,'GSAR_ANO',"Elenco Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOCODCLI,NOTIPNOM";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NOCODCLI,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVNOMINA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NOCODCLI,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_EVNOMINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_EVNOMINA)
            select NOCODICE,NODESCRI,NOCODCLI,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVNOMINA = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(60))
      this.w_CODCLI = NVL(_Link_.NOCODCLI,space(15))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_EVNOMINA = space(15)
      endif
      this.w_DESNOM = space(60)
      this.w_CODCLI = space(15)
      this.w_TIPNOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_EVNOMINA = space(15)
        this.w_DESNOM = space(60)
        this.w_CODCLI = space(15)
        this.w_TIPNOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVNOMINA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFF_NOMI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.NOCODICE as NOCODICE106"+ ",link_1_6.NODESCRI as NODESCRI106"+ ",link_1_6.NOCODCLI as NOCODCLI106"+ ",link_1_6.NOTIPNOM as NOTIPNOM106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on ANEVENTI.EVNOMINA=link_1_6.NOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and ANEVENTI.EVNOMINA=link_1_6.NOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EVCODPRA
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVCODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_EVCODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_EVCODPRA))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVCODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_EVCODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_EVCODPRA)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_EVCODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oEVCODPRA_1_16'),i_cWhere,'GSPR_ACN',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSFAZAEV.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVCODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_EVCODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_EVCODPRA)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVCODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_EVCODPRA = space(15)
      endif
      this.w_DESCAN = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVCODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.CNCODCAN as CNCODCAN116"+ ",link_1_16.CNDESCAN as CNDESCAN116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on ANEVENTI.EVCODPRA=link_1_16.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and ANEVENTI.EVCODPRA=link_1_16.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EVNOMINA
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVNOMINA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_EVNOMINA)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_EVNOMINA))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVNOMINA)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_EVNOMINA)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_EVNOMINA)+"%");

            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_EVNOMINA) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oEVNOMINA_2_5'),i_cWhere,'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVNOMINA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_EVNOMINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_EVNOMINA)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVNOMINA = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_EVNOMINA = space(15)
      endif
      this.w_DESNOM = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_EVNOMINA = space(15)
        this.w_DESNOM = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVNOMINA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFF_NOMI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.NOCODICE as NOCODICE205"+ ",link_2_5.NODESCRI as NODESCRI205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on ANEVENTI.EVNOMINA=link_2_5.NOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and ANEVENTI.EVNOMINA=link_2_5.NOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=EVCODPRA
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EVCODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_EVCODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_EVCODPRA))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EVCODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_EVCODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_EVCODPRA)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_EVCODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oEVCODPRA_2_8'),i_cWhere,'GSPR_ACN',"Elenco pratiche",'GSFAZAEV.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EVCODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_EVCODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_EVCODPRA)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EVCODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_EVCODPRA = space(15)
      endif
      this.w_DESCAN = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EVCODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.CNCODCAN as CNCODCAN208"+ ",link_2_8.CNDESCAN as CNDESCAN208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on ANEVENTI.EVCODPRA=link_2_8.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and ANEVENTI.EVCODPRA=link_2_8.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TEDRIVER
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_lTable = "DRVEVENT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2], .t., this.DRVEVENT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TEDRIVER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TEDRIVER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DEDRIVER,DE__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where DEDRIVER="+cp_ToStrODBC(this.w_TEDRIVER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DEDRIVER',this.w_TEDRIVER)
            select DEDRIVER,DE__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TEDRIVER = NVL(_Link_.DEDRIVER,space(10))
      this.w_DE__TIPO = NVL(_Link_.DE__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TEDRIVER = space(10)
      endif
      this.w_DE__TIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])+'\'+cp_ToStr(_Link_.DEDRIVER,1)
      cp_ShowWarn(i_cKey,this.DRVEVENT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TEDRIVER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATSEREVE
  func Link_1_54(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANEVENTI_IDX,3]
    i_lTable = "ANEVENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2], .t., this.ANEVENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATSEREVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ANEVENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EVSERIAL like "+cp_ToStrODBC(trim(this.w_ATSEREVE)+"%");
                   +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);

          i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVNOMINA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EV__ANNO,EVSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EV__ANNO',this.w_ATEVANNO;
                     ,'EVSERIAL',trim(this.w_ATSEREVE))
          select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVNOMINA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EV__ANNO,EVSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATSEREVE)==trim(_Link_.EVSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATSEREVE) and !this.bDontReportError
            deferred_cp_zoom('ANEVENTI','*','EV__ANNO,EVSERIAL',cp_AbsName(oSource.parent,'oATSEREVE_1_54'),i_cWhere,'',"Elenco ID richieste",'GSFA_ZID.ANEVENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATEVANNO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVNOMINA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVNOMINA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVNOMINA";
                     +" from "+i_cTable+" "+i_lTable+" where EVSERIAL="+cp_ToStrODBC(oSource.xKey(2));
                     +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EV__ANNO',oSource.xKey(1);
                       ,'EVSERIAL',oSource.xKey(2))
            select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVNOMINA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATSEREVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVNOMINA";
                   +" from "+i_cTable+" "+i_lTable+" where EVSERIAL="+cp_ToStrODBC(this.w_ATSEREVE);
                   +" and EV__ANNO="+cp_ToStrODBC(this.w_ATEVANNO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EV__ANNO',this.w_ATEVANNO;
                       ,'EVSERIAL',this.w_ATSEREVE)
            select EV__ANNO,EVSERIAL,EVIDRICH,EVSTARIC,EV__NOTE,EVOGGETT,EVRIFPER,EV_PHONE,EVTELINT,EV_EMAIL,EV_EMPEC,EVNOMINA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATSEREVE = NVL(_Link_.EVSERIAL,space(10))
      this.w_IDRICH = NVL(_Link_.EVIDRICH,space(10))
      this.w_STARIC = NVL(_Link_.EVSTARIC,space(1))
      this.w_EV__NOTE = NVL(_Link_.EV__NOTE,space(0))
      this.w_EVOGGETT = NVL(_Link_.EVOGGETT,space(100))
      this.w_EVRIFPER = NVL(_Link_.EVRIFPER,space(254))
      this.w_EV_PHONE = NVL(_Link_.EV_PHONE,space(18))
      this.w_EVTELINT = NVL(_Link_.EVTELINT,space(18))
      this.w_EV_EMAIL = NVL(_Link_.EV_EMAIL,space(254))
      this.w_EV_EMPEC = NVL(_Link_.EV_EMPEC,space(254))
      this.w_EVNOMINA = NVL(_Link_.EVNOMINA,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ATSEREVE = space(10)
      endif
      this.w_IDRICH = space(10)
      this.w_STARIC = space(1)
      this.w_EV__NOTE = space(0)
      this.w_EVOGGETT = space(100)
      this.w_EVRIFPER = space(254)
      this.w_EV_PHONE = space(18)
      this.w_EVTELINT = space(18)
      this.w_EV_EMAIL = space(254)
      this.w_EV_EMPEC = space(254)
      this.w_EVNOMINA = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANEVENTI_IDX,2])+'\'+cp_ToStr(_Link_.EV__ANNO,1)+'\'+cp_ToStr(_Link_.EVSERIAL,1)
      cp_ShowWarn(i_cKey,this.ANEVENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATSEREVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oEV_STATO_1_4.RadioValue()==this.w_EV_STATO)
      this.oPgFrm.Page1.oPag.oEV_STATO_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEVNOMINA_1_6.value==this.w_EVNOMINA)
      this.oPgFrm.Page1.oPag.oEVNOMINA_1_6.value=this.w_EVNOMINA
    endif
    if not(this.oPgFrm.Page1.oPag.oEVRIFPER_1_11.value==this.w_EVRIFPER)
      this.oPgFrm.Page1.oPag.oEVRIFPER_1_11.value=this.w_EVRIFPER
    endif
    if not(this.oPgFrm.Page1.oPag.oEVCODPRA_1_16.value==this.w_EVCODPRA)
      this.oPgFrm.Page1.oPag.oEVCODPRA_1_16.value=this.w_EVCODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oEVOREEFF_1_19.value==this.w_EVOREEFF)
      this.oPgFrm.Page1.oPag.oEVOREEFF_1_19.value=this.w_EVOREEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oEVMINEFF_1_20.value==this.w_EVMINEFF)
      this.oPgFrm.Page1.oPag.oEVMINEFF_1_20.value=this.w_EVMINEFF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_27.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_27.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_28.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_28.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSTATO_1_32.RadioValue()==this.w_FLSTATO)
      this.oPgFrm.Page1.oPag.oFLSTATO_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOMIN_1_33.RadioValue()==this.w_FLNOMIN)
      this.oPgFrm.Page1.oPag.oFLNOMIN_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPRAT_1_35.RadioValue()==this.w_FLPRAT)
      this.oPgFrm.Page1.oPag.oFLPRAT_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLORE_1_36.RadioValue()==this.w_FLORE)
      this.oPgFrm.Page1.oPag.oFLORE_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMIN_1_37.RadioValue()==this.w_FLMIN)
      this.oPgFrm.Page1.oPag.oFLMIN_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLRIFP_1_38.RadioValue()==this.w_FLRIFP)
      this.oPgFrm.Page1.oPag.oFLRIFP_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEV__NOTE_1_41.value==this.w_EV__NOTE)
      this.oPgFrm.Page1.oPag.oEV__NOTE_1_41.value=this.w_EV__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOTE_1_42.RadioValue()==this.w_FLNOTE)
      this.oPgFrm.Page1.oPag.oFLNOTE_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEV_STATO_2_3.RadioValue()==this.w_EV_STATO)
      this.oPgFrm.Page2.oPag.oEV_STATO_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEVNOMINA_2_5.value==this.w_EVNOMINA)
      this.oPgFrm.Page2.oPag.oEVNOMINA_2_5.value=this.w_EVNOMINA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOM_2_7.value==this.w_DESNOM)
      this.oPgFrm.Page2.oPag.oDESNOM_2_7.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oEVCODPRA_2_8.value==this.w_EVCODPRA)
      this.oPgFrm.Page2.oPag.oEVCODPRA_2_8.value=this.w_EVCODPRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAN_2_9.value==this.w_DESCAN)
      this.oPgFrm.Page2.oPag.oDESCAN_2_9.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page2.oPag.oEVRIFPER_2_12.value==this.w_EVRIFPER)
      this.oPgFrm.Page2.oPag.oEVRIFPER_2_12.value=this.w_EVRIFPER
    endif
    if not(this.oPgFrm.Page1.oPag.oEVIDRICH_1_48.value==this.w_EVIDRICH)
      this.oPgFrm.Page1.oPag.oEVIDRICH_1_48.value=this.w_EVIDRICH
    endif
    if not(this.oPgFrm.Page1.oPag.oEVSTARIC_1_51.RadioValue()==this.w_EVSTARIC)
      this.oPgFrm.Page1.oPag.oEVSTARIC_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATSEREVE_1_54.value==this.w_ATSEREVE)
      this.oPgFrm.Page1.oPag.oATSEREVE_1_54.value=this.w_ATSEREVE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSTID_1_59.RadioValue()==this.w_FLSTID)
      this.oPgFrm.Page1.oPag.oFLSTID_1_59.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'ANEVENTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and not(empty(.w_EVNOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEVNOMINA_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EV__ANNO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEV__ANNO_1_7.SetFocus()
            i_bnoObbl = !empty(.w_EV__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F')  and not(empty(.w_EVNOMINA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oEVNOMINA_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_EVNOMINA = this.w_EVNOMINA
    this.o_EVSERIAL = this.w_EVSERIAL
    this.o_EVCODPRA = this.w_EVCODPRA
    this.o_EVIDRICH = this.w_EVIDRICH
    return

enddefine

* --- Define pages as container
define class tgsfa_acePag1 as StdContainer
  Width  = 1050
  height = 196
  stdWidth  = 1050
  stdheight = 196
  resizeXpos=514
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_3 as StdButton with uid="OSSBCQXAVZ",left=222, top=22, width=20,height=22,;
    caption="+", nPag=1;
    , ToolTipText = "Nuovo ID richiesta";
    , HelpContextID = 109739866;
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      this.parent.oContained.NotifyEvent("Progre")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Isalt() or Not Empty(.w_EVIDRICH))
     endwith
    endif
  endfunc


  add object oEV_STATO_1_4 as StdCombo with uid="EUROVJPAAT",rtseq=3,rtrep=.f.,left=98,top=50,width=145,height=21;
    , ToolTipText = "Stato evento";
    , HelpContextID = 968853;
    , cFormVar="w_EV_STATO",RowSource=""+"Processato,"+"Da processare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEV_STATO_1_4.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oEV_STATO_1_4.GetRadio()
    this.Parent.oContained.w_EV_STATO = this.RadioValue()
    return .t.
  endfunc

  func oEV_STATO_1_4.SetRadio()
    this.Parent.oContained.w_EV_STATO=trim(this.Parent.oContained.w_EV_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_EV_STATO=='P',1,;
      iif(this.Parent.oContained.w_EV_STATO=='D',2,;
      0))
  endfunc

  add object oEVNOMINA_1_6 as StdField with uid="UECGMQMOJG",rtseq=5,rtrep=.f.,;
    cFormVar = "w_EVNOMINA", cQueryName = "EVNOMINA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 140920697,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=98, Top=76, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_EVNOMINA"

  func oEVNOMINA_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVNOMINA_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVNOMINA_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oEVNOMINA_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Elenco Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oEVNOMINA_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_EVNOMINA
     i_obj.ecpSave()
  endproc

  add object oEVRIFPER_1_11 as StdField with uid="KEZMGKXFDT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_EVRIFPER", cQueryName = "EVRIFPER",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 237238424,;
   bGlobalFont=.t.,;
    Height=21, Width=370, Left=98, Top=102, InputMask=replicate('X',254)

  add object oEVCODPRA_1_16 as StdField with uid="OWQFVICYMF",rtseq=12,rtrep=.f.,;
    cFormVar = "w_EVCODPRA", cQueryName = "EVCODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 235473031,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=98, Top=128, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSPR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_EVCODPRA"

  func oEVCODPRA_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVCODPRA_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVCODPRA_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oEVCODPRA_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPR_ACN',""+IIF(IsAlt(),"Pratiche","Commesse")+"",'GSFAZAEV.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oEVCODPRA_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSPR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_EVCODPRA
     i_obj.ecpSave()
  endproc

  add object oEVOREEFF_1_19 as StdField with uid="HOPKSKORTN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_EVOREEFF", cQueryName = "EVOREEFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 52217996,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=98, Top=154, cSayPict='"999"', cGetPict='"999"'

  add object oEVMINEFF_1_20 as StdField with uid="REGHTMCSSO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_EVMINEFF", cQueryName = "EVMINEFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 61057164,;
   bGlobalFont=.t.,;
    Height=21, Width=31, Left=138, Top=154, cSayPict='"99"', cGetPict='"99"'

  add object oDESNOM_1_27 as StdField with uid="MKEZBKLLAH",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 196671286,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=225, Top=76, InputMask=replicate('X',60)

  add object oDESCAN_1_28 as StdField with uid="FKWZHSWJXJ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 198047542,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=225, Top=128, InputMask=replicate('X',100)

  add object oFLSTATO_1_32 as StdCheck with uid="VIVZMQZRBE",rtseq=25,rtrep=.f.,left=250, top=50, caption="",;
    ToolTipText = "Se attivo informazione da aggiornare",;
    HelpContextID = 31391318,;
    cFormVar="w_FLSTATO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSTATO_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLSTATO_1_32.GetRadio()
    this.Parent.oContained.w_FLSTATO = this.RadioValue()
    return .t.
  endfunc

  func oFLSTATO_1_32.SetRadio()
    this.Parent.oContained.w_FLSTATO=trim(this.Parent.oContained.w_FLSTATO)
    this.value = ;
      iif(this.Parent.oContained.w_FLSTATO=='S',1,;
      0)
  endfunc

  add object oFLNOMIN_1_33 as StdCheck with uid="ZQXXJVBMFY",rtseq=26,rtrep=.f.,left=474, top=76, caption="",;
    ToolTipText = "Se attivo informazione da aggiornare",;
    HelpContextID = 140923306,;
    cFormVar="w_FLNOMIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLNOMIN_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLNOMIN_1_33.GetRadio()
    this.Parent.oContained.w_FLNOMIN = this.RadioValue()
    return .t.
  endfunc

  func oFLNOMIN_1_33.SetRadio()
    this.Parent.oContained.w_FLNOMIN=trim(this.Parent.oContained.w_FLNOMIN)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOMIN=='S',1,;
      0)
  endfunc

  add object oFLPRAT_1_35 as StdCheck with uid="IUALXXENDY",rtseq=28,rtrep=.f.,left=474, top=128, caption="",;
    ToolTipText = "Se attivo informazione da aggiornare",;
    HelpContextID = 31247958,;
    cFormVar="w_FLPRAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLPRAT_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLPRAT_1_35.GetRadio()
    this.Parent.oContained.w_FLPRAT = this.RadioValue()
    return .t.
  endfunc

  func oFLPRAT_1_35.SetRadio()
    this.Parent.oContained.w_FLPRAT=trim(this.Parent.oContained.w_FLPRAT)
    this.value = ;
      iif(this.Parent.oContained.w_FLPRAT=='S',1,;
      0)
  endfunc

  add object oFLORE_1_36 as StdCheck with uid="IUSLHQAQSY",rtseq=29,rtrep=.f.,left=173, top=154, caption="",;
    ToolTipText = "Se attivo informazione da aggiornare",;
    HelpContextID = 31670698,;
    cFormVar="w_FLORE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLORE_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLORE_1_36.GetRadio()
    this.Parent.oContained.w_FLORE = this.RadioValue()
    return .t.
  endfunc

  func oFLORE_1_36.SetRadio()
    this.Parent.oContained.w_FLORE=trim(this.Parent.oContained.w_FLORE)
    this.value = ;
      iif(this.Parent.oContained.w_FLORE=='S',1,;
      0)
  endfunc

  add object oFLMIN_1_37 as StdCheck with uid="ODFGAPCLJL",rtseq=30,rtrep=.f.,left=190, top=154, caption="",;
    ToolTipText = "Se attivo informazione da aggiornare",;
    HelpContextID = 22831530,;
    cFormVar="w_FLMIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLMIN_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLMIN_1_37.GetRadio()
    this.Parent.oContained.w_FLMIN = this.RadioValue()
    return .t.
  endfunc

  func oFLMIN_1_37.SetRadio()
    this.Parent.oContained.w_FLMIN=trim(this.Parent.oContained.w_FLMIN)
    this.value = ;
      iif(this.Parent.oContained.w_FLMIN=='S',1,;
      0)
  endfunc

  add object oFLRIFP_1_38 as StdCheck with uid="YSIAWGTKFY",rtseq=31,rtrep=.f.,left=474, top=102, caption="",;
    ToolTipText = "Se attivo informazione da aggiornare",;
    HelpContextID = 237235798,;
    cFormVar="w_FLRIFP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLRIFP_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLRIFP_1_38.GetRadio()
    this.Parent.oContained.w_FLRIFP = this.RadioValue()
    return .t.
  endfunc

  func oFLRIFP_1_38.SetRadio()
    this.Parent.oContained.w_FLRIFP=trim(this.Parent.oContained.w_FLRIFP)
    this.value = ;
      iif(this.Parent.oContained.w_FLRIFP=='S',1,;
      0)
  endfunc

  add object oEV__NOTE_1_41 as StdMemo with uid="GHSEQOHDIW",rtseq=34,rtrep=.f.,;
    cFormVar = "w_EV__NOTE", cQueryName = "EV__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 230344843,;
   bGlobalFont=.t.,;
    Height=166, Width=254, Left=493, Top=22

  add object oFLNOTE_1_42 as StdCheck with uid="UZITWQZFUE",rtseq=35,rtrep=.f.,left=749, top=23, caption="",;
    ToolTipText = "Se attivo informazione da aggiornare",;
    HelpContextID = 67743318,;
    cFormVar="w_FLNOTE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLNOTE_1_42.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLNOTE_1_42.GetRadio()
    this.Parent.oContained.w_FLNOTE = this.RadioValue()
    return .t.
  endfunc

  func oFLNOTE_1_42.SetRadio()
    this.Parent.oContained.w_FLNOTE=trim(this.Parent.oContained.w_FLNOTE)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOTE=='S',1,;
      0)
  endfunc

  add object oEVIDRICH_1_48 as StdField with uid="TJBDRPPGWK",rtseq=50,rtrep=.f.,;
    cFormVar = "w_EVIDRICH", cQueryName = "EVIDRICH",enabled=.f.,nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 132016270,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=98, Top=22, InputMask=replicate('X',15)

  func oEVIDRICH_1_48.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  proc oEVIDRICH_1_48.mAfter
      with this.Parent.oContained
        gsag_ble(this.Parent.oContained,.w_EVIDRICH,".w_ATSEREVE",This.Parent.oContained,".w_EVNOMINA")
      endwith
  endproc


  add object oEVSTARIC_1_51 as StdCombo with uid="TRFHGFVSFK",rtseq=52,rtrep=.f.,left=360,top=22,width=107,height=21;
    , ToolTipText = "Stato della richiesta  aperto, in corso, in attesa, chiuso";
    , HelpContextID = 2160503;
    , cFormVar="w_EVSTARIC",RowSource=""+"Aperta,"+"In corso,"+"In attesa,"+"Chiusa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEVSTARIC_1_51.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'I',;
    iif(this.value =3,'S',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oEVSTARIC_1_51.GetRadio()
    this.Parent.oContained.w_EVSTARIC = this.RadioValue()
    return .t.
  endfunc

  func oEVSTARIC_1_51.SetRadio()
    this.Parent.oContained.w_EVSTARIC=trim(this.Parent.oContained.w_EVSTARIC)
    this.value = ;
      iif(this.Parent.oContained.w_EVSTARIC=='A',1,;
      iif(this.Parent.oContained.w_EVSTARIC=='I',2,;
      iif(this.Parent.oContained.w_EVSTARIC=='S',3,;
      iif(this.Parent.oContained.w_EVSTARIC=='C',4,;
      0))))
  endfunc

  func oEVSTARIC_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EVFLIDPA='S')
    endwith
   endif
  endfunc

  func oEVSTARIC_1_51.mHide()
    with this.Parent.oContained
      return ( Isalt())
    endwith
  endfunc

  add object oATSEREVE_1_54 as StdField with uid="CGPWQHXDGB",rtseq=54,rtrep=.f.,;
    cFormVar = "w_ATSEREVE", cQueryName = "ATSEREVE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 65013323,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=967, Top=109, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="ANEVENTI", oKey_1_1="EV__ANNO", oKey_1_2="this.w_ATEVANNO", oKey_2_1="EVSERIAL", oKey_2_2="this.w_ATSEREVE"

  func oATSEREVE_1_54.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  func oATSEREVE_1_54.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_54('Part',this)
    endwith
    return bRes
  endfunc

  proc oATSEREVE_1_54.ecpDrop(oSource)
    this.Parent.oContained.link_1_54('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATSEREVE_1_54.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ANEVENTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"EV__ANNO="+cp_ToStrODBC(this.Parent.oContained.w_ATEVANNO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"EV__ANNO="+cp_ToStr(this.Parent.oContained.w_ATEVANNO)
    endif
    do cp_zoom with 'ANEVENTI','*','EV__ANNO,EVSERIAL',cp_AbsName(this.parent,'oATSEREVE_1_54'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco ID richieste",'GSFA_ZID.ANEVENTI_VZM',this.parent.oContained
  endproc


  add object oObj_1_58 as cp_runprogram with uid="HWWEHUFKJK",left=18, top=219, width=250,height=27,;
    caption='GSFA_BVS(AGGEVENTI)',;
   bGlobalFont=.t.,;
    prg="gsfa_bvs('AggEventi')",;
    cEvent = "AggEventi",;
    nPag=1;
    , ToolTipText = "Aggiorna eventi";
    , HelpContextID = 206161863

  add object oFLSTID_1_59 as StdCheck with uid="SAMQACXJQH",rtseq=59,rtrep=.f.,left=474, top=22, caption="",;
    ToolTipText = "Se attivo informazione da aggiornare",;
    HelpContextID = 39779926,;
    cFormVar="w_FLSTID", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSTID_1_59.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLSTID_1_59.GetRadio()
    this.Parent.oContained.w_FLSTID = this.RadioValue()
    return .t.
  endfunc

  func oFLSTID_1_59.SetRadio()
    this.Parent.oContained.w_FLSTID=trim(this.Parent.oContained.w_FLSTID)
    this.value = ;
      iif(this.Parent.oContained.w_FLSTID=='S',1,;
      0)
  endfunc

  func oFLSTID_1_59.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="PMOROHSOHQ",Visible=.t., Left=24, Top=50,;
    Alignment=1, Width=70, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="GTGEBESZPR",Visible=.t., Left=29, Top=76,;
    Alignment=1, Width=65, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="JJITEIHJEK",Visible=.t., Left=5, Top=102,;
    Alignment=1, Width=89, Height=18,;
    Caption="Rif. persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="NETKWXLZPH",Visible=.t., Left=3, Top=154,;
    Alignment=1, Width=91, Height=18,;
    Caption="Durata effettiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="CDDNXNAPUW",Visible=.t., Left=53, Top=128,;
    Alignment=1, Width=41, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="LLDNDSKWUX",Visible=.t., Left=25, Top=128,;
    Alignment=1, Width=69, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="EBKDKQGJLA",Visible=.t., Left=133, Top=154,;
    Alignment=0, Width=5, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="CZXHGRLBZS",Visible=.t., Left=8, Top=22,;
    Alignment=1, Width=86, Height=18,;
    Caption="ID richiesta:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="ROKAGEDAUD",Visible=.t., Left=282, Top=22,;
    Alignment=1, Width=75, Height=18,;
    Caption="Stato  rich.:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc
enddefine
define class tgsfa_acePag2 as StdContainer
  Width  = 1050
  height = 196
  stdWidth  = 1050
  stdheight = 196
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object HTMLBROW as cp_Browser with uid="RRRNFFMDMJ",left=6, top=68, width=758,height=126,;
    caption='HTMLBROW',;
   bGlobalFont=.t.,;
    nPag=2;
    , HelpContextID = 1661235


  add object oEV_STATO_2_3 as StdCombo with uid="ALVAAYDNHY",rtseq=37,rtrep=.f.,left=79,top=8,width=145,height=21;
    , ToolTipText = "Stato evento";
    , HelpContextID = 968853;
    , cFormVar="w_EV_STATO",RowSource=""+"Processato,"+"Da processare", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oEV_STATO_2_3.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oEV_STATO_2_3.GetRadio()
    this.Parent.oContained.w_EV_STATO = this.RadioValue()
    return .t.
  endfunc

  func oEV_STATO_2_3.SetRadio()
    this.Parent.oContained.w_EV_STATO=trim(this.Parent.oContained.w_EV_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_EV_STATO=='P',1,;
      iif(this.Parent.oContained.w_EV_STATO=='D',2,;
      0))
  endfunc

  add object oEVNOMINA_2_5 as StdField with uid="JRTYGNSRRI",rtseq=38,rtrep=.f.,;
    cFormVar = "w_EVNOMINA", cQueryName = "EVNOMINA",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 140920697,;
   bGlobalFont=.t.,;
    Height=21, Width=154, Left=296, Top=8, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_EVNOMINA"

  func oEVNOMINA_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVNOMINA_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVNOMINA_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oEVNOMINA_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oEVNOMINA_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_EVNOMINA
     i_obj.ecpSave()
  endproc

  add object oDESNOM_2_7 as StdField with uid="TTQYYSHUQL",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 196671286,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=455, Top=8, InputMask=replicate('X',60)

  add object oEVCODPRA_2_8 as StdField with uid="JDIVLLSADE",rtseq=40,rtrep=.f.,;
    cFormVar = "w_EVCODPRA", cQueryName = "EVCODPRA",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 235473031,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=79, Top=35, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSPR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_EVCODPRA"

  func oEVCODPRA_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oEVCODPRA_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEVCODPRA_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oEVCODPRA_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPR_ACN',"Elenco pratiche",'GSFAZAEV.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oEVCODPRA_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSPR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_EVCODPRA
     i_obj.ecpSave()
  endproc

  add object oDESCAN_2_9 as StdField with uid="FBEOOCPLKW",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 198047542,;
   bGlobalFont=.t.,;
    Height=21, Width=252, Left=198, Top=35, InputMask=replicate('X',100)

  add object oEVRIFPER_2_12 as StdField with uid="KRASJTAZKS",rtseq=42,rtrep=.f.,;
    cFormVar = "w_EVRIFPER", cQueryName = "EVRIFPER",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 237238424,;
   bGlobalFont=.t.,;
    Height=21, Width=225, Left=535, Top=35, InputMask=replicate('X',254)

  add object oStr_2_4 as StdString with uid="TPSRUCDBGO",Visible=.t., Left=4, Top=8,;
    Alignment=1, Width=70, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="QACTQTTZJR",Visible=.t., Left=229, Top=8,;
    Alignment=1, Width=65, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="TOJDILOZLX",Visible=.t., Left=33, Top=35,;
    Alignment=1, Width=41, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_2_10.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_2_11 as StdString with uid="QHBZMHTEGI",Visible=.t., Left=5, Top=35,;
    Alignment=1, Width=69, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_11.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_2_13 as StdString with uid="LKSOWGMWBV",Visible=.t., Left=452, Top=35,;
    Alignment=1, Width=80, Height=18,;
    Caption="Rif. persona:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsfa_acePag3 as StdContainer
  Width  = 1050
  height = 196
  stdWidth  = 1050
  stdheight = 196
  resizeXpos=691
  resizeYpos=108
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMPRAT as cp_zoombox with uid="SLTIXWKFUA",left=6, top=10, width=754,height=178,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CAN_TIER",cZoomFile="GSFAPKCE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSAR_BZZ",bRetriveAllRows=.f.,;
    nPag=3;
    , HelpContextID = 68256998
enddefine
define class tgsfa_acePag4 as StdContainer
  Width  = 1050
  height = 196
  stdWidth  = 1050
  stdheight = 196
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMUTE as cp_zoombox with uid="MEFMQEYELK",left=5, top=12, width=757,height=181,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DIPENDEN",cZoomFile="GSFASKCE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSAR_BDZ",bRetriveAllRows=.f.,;
    nPag=4;
    , HelpContextID = 68256998
enddefine
define class tgsfa_acePag5 as StdContainer
  Width  = 1050
  height = 196
  stdWidth  = 1050
  stdheight = 196
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMIND as cp_szoombox with uid="VAPGYJFIPY",left=4, top=11, width=758,height=183,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PROMINDI",cZoomFile="GSFADKCE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSUT_AID",bRetriveAllRows=.f.,;
    nPag=5;
    , HelpContextID = 68256998
enddefine
define class tgsfa_acePag6 as StdContainer
  Width  = 1050
  height = 196
  stdWidth  = 1050
  stdheight = 196
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMATT as cp_szoombox with uid="RJQXZMULBL",left=5, top=13, width=757,height=178,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_ATTI",cZoomFile="GSFAAKCE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSAG_AAT",bRetriveAllRows=.f.,;
    nPag=6;
    , HelpContextID = 68256998
enddefine
define class tgsfa_acePag7 as StdContainer
  Width  = 1050
  height = 196
  stdWidth  = 1050
  stdheight = 196
  resizeXpos=684
  resizeYpos=123
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMDOC as cp_szoombox with uid="JFKBNLIYSV",left=2, top=15, width=757,height=178,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSFAKKCE",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom=" ",bRetriveAllRows=.f.,;
    nPag=7;
    , HelpContextID = 68256998
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_ace','ANEVENTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".EV__ANNO=ANEVENTI.EV__ANNO";
  +" and "+i_cAliasName2+".EVSERIAL=ANEVENTI.EVSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
