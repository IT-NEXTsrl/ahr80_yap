* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ktm                                                        *
*              Timer                                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-07                                                      *
* Last revis.: 2014-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsag_ktm
* rimuovo la classe dalla memoria per
* definire la maschera modale o meno
clear class tgsag_ktm
* --- Fine Area Manuale
return(createobject("tgsag_ktm",oParentObject))

* --- Class definition
define class tgsag_ktm as StdForm
  Top    = 43
  Left   = 64

  * --- Standard Properties
  Width  = 641
  Height = 342
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-17"
  HelpContextID=185981079
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=119

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  KEY_ARTI_IDX = 0
  DIPENDEN_IDX = 0
  ART_ICOL_IDX = 0
  CAUMATTI_IDX = 0
  PAR_AGEN_IDX = 0
  UNIMIS_IDX = 0
  TIP_DOCU_IDX = 0
  PAR_ALTE_IDX = 0
  CENCOST_IDX = 0
  ATTIVITA_IDX = 0
  OFF_NOMI_IDX = 0
  IMP_DETT_IDX = 0
  BUSIUNIT_IDX = 0
  cPrg = "gsag_ktm"
  cComment = "Timer"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TipoRisorsa = space(1)
  w_LetturaParAgen = space(5)
  w_ZoomName = space(20)
  w_DAGEST = .F.
  w_CODPERS = space(5)
  o_CODPERS = space(5)
  w_EVTIPEVE = space(10)
  w_CodPart = space(5)
  o_CodPart = space(5)
  w_TIPCAU = space(1)
  w_Leggialte = space(10)
  w_NOEDES = space(30)
  w_DurataOre = 0
  o_DurataOre = 0
  w_DTIPRIG = space(1)
  w_DurataMin = 0
  o_DurataMin = 0
  w_SerUtente = space(10)
  w_CauDefault = space(20)
  o_CauDefault = space(20)
  w_FLNSAP = space(10)
  w_CodPratica = space(15)
  w_ATCENCOS = space(15)
  w_ATCODPRA = space(15)
  w_CODSER = space(20)
  o_CODSER = space(20)
  w_DesSer = space(40)
  w_ATATTCOS = space(15)
  w_DesAgg = space(0)
  w_ATCENRIC = space(15)
  w_ATCOMRIC = space(15)
  w_FLACQ = space(10)
  w_ATATTRIC = space(15)
  w_ATCODNOM = space(15)
  w_DATIPRIG = space(1)
  w_DATIPRI2 = space(1)
  w_GENPRE = space(1)
  w_DataIniTimer = ctot('')
  w_Attivo = space(1)
  w_Comodo = space(10)
  w_DesPratica = space(100)
  w_OBTEST = ctod('  /  /  ')
  w_CACODART = space(20)
  w_TIPOENTE = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_TxtSel = space(10)
  w_ENTE = space(10)
  w_PrimaUniMis = space(10)
  w_UniMisTempo = space(10)
  o_UniMisTempo = space(10)
  w_ConversioneOre = 0
  w_QtaTimerOre = 0
  w_QtaTimerMinuti = 0
  w_QtaTimer = 0
  w_DAVOCCOS = space(15)
  w_DAVOCRIC = space(15)
  w_DACENCOS = space(15)
  w_DACODCOM = space(15)
  w_DAATTIVI = space(15)
  w_CAUDOC = space(5)
  w_DA_SEGNO = space(1)
  w_DAINICOM = ctod('  /  /  ')
  w_DAFINCOM = ctod('  /  /  ')
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_FLDANA = space(1)
  w_VOCECR = space(1)
  w_COMODO = space(30)
  w_EVSERIAL = space(10)
  w_RESET = .F.
  w_DASCONT1 = 0
  w_DASCONT2 = 0
  w_DASCONT3 = 0
  w_DASCONT4 = 0
  w_DACONCOD = space(10)
  w_EVNOMINA = space(15)
  w_EVRIFPER = space(254)
  w_GENEVE = space(1)
  w_DESCAU = space(254)
  w_DACONTRA = space(10)
  w_DACODMOD = space(10)
  w_DACODIMP = space(10)
  w_CODCLI = space(15)
  w_DACODATT = space(20)
  w_DARINNOV = 0
  w_DAUNIMIS = space(3)
  w_DAPREZZO = 0
  w_DACODICE = space(20)
  w_DACOCOMP = 0
  w_CCDESPIA = space(40)
  w_CCDESRIC = space(40)
  w_CAUACQ = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_CENOBSO = ctod('  /  /  ')
  w_TIPATT = space(10)
  w_DESNOM = space(60)
  w_DADESATT = space(40)
  w_DASERIAL = space(20)
  w_CAUATT = space(10)
  w_ARTIPRIG = space(1)
  w_ARTIPRI2 = space(1)
  w_FLGZER = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_CNDATFIN = ctod('  /  /  ')
  w_DATAOBSO = ctod('  /  /  ')
  w_DACENRIC = space(15)
  w_DACOMRIC = space(15)
  w_DAATTRIC = space(15)
  w_DATCOINI = ctod('  /  /  ')
  w_DATCOFIN = ctod('  /  /  ')
  w_DATRIINI = ctod('  /  /  ')
  w_DATRIFIN = ctod('  /  /  ')
  w_DATA = ctod('  /  /  ')
  o_DATA = ctod('  /  /  ')
  w_ORA = space(2)
  o_ORA = space(2)
  w_MINUTI = space(2)
  o_MINUTI = space(2)
  w_PARASS = 0
  w_FLPRES = space(1)
  w_CHKDATAPRE = space(1)
  w_GG_PRE = 0
  w_GG_SUC = 0
  w_DATAPREC = ctod('  /  /  ')
  w_DATASUCC = ctod('  /  /  ')
  w_DPCOGNOM = space(50)
  w_DPNOME = space(50)
  w_DESRES = space(100)
  w_CADTOBSO = ctod('  /  /  ')
  w_Clock = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_ktm
   * rendo la finestra non modale se non richiamata da men�
   WindowType =iif(Type("oparentobject.w_EVSERIAL")="C",1,0)
  Defpratica=' '
  DefResponsabile=' '
   * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_ktmPag1","gsag_ktm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODPERS_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Clock = this.oPgFrm.Pages(1).oPag.Clock
    DoDefault()
    proc Destroy()
      this.w_Clock = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='DIPENDEN'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='CAUMATTI'
    this.cWorkTables[6]='PAR_AGEN'
    this.cWorkTables[7]='UNIMIS'
    this.cWorkTables[8]='TIP_DOCU'
    this.cWorkTables[9]='PAR_ALTE'
    this.cWorkTables[10]='CENCOST'
    this.cWorkTables[11]='ATTIVITA'
    this.cWorkTables[12]='OFF_NOMI'
    this.cWorkTables[13]='IMP_DETT'
    this.cWorkTables[14]='BUSIUNIT'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TipoRisorsa=space(1)
      .w_LetturaParAgen=space(5)
      .w_ZoomName=space(20)
      .w_DAGEST=.f.
      .w_CODPERS=space(5)
      .w_EVTIPEVE=space(10)
      .w_CodPart=space(5)
      .w_TIPCAU=space(1)
      .w_Leggialte=space(10)
      .w_NOEDES=space(30)
      .w_DurataOre=0
      .w_DTIPRIG=space(1)
      .w_DurataMin=0
      .w_SerUtente=space(10)
      .w_CauDefault=space(20)
      .w_FLNSAP=space(10)
      .w_CodPratica=space(15)
      .w_ATCENCOS=space(15)
      .w_ATCODPRA=space(15)
      .w_CODSER=space(20)
      .w_DesSer=space(40)
      .w_ATATTCOS=space(15)
      .w_DesAgg=space(0)
      .w_ATCENRIC=space(15)
      .w_ATCOMRIC=space(15)
      .w_FLACQ=space(10)
      .w_ATATTRIC=space(15)
      .w_ATCODNOM=space(15)
      .w_DATIPRIG=space(1)
      .w_DATIPRI2=space(1)
      .w_GENPRE=space(1)
      .w_DataIniTimer=ctot("")
      .w_Attivo=space(1)
      .w_Comodo=space(10)
      .w_DesPratica=space(100)
      .w_OBTEST=ctod("  /  /  ")
      .w_CACODART=space(20)
      .w_TIPOENTE=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_TxtSel=space(10)
      .w_ENTE=space(10)
      .w_PrimaUniMis=space(10)
      .w_UniMisTempo=space(10)
      .w_ConversioneOre=0
      .w_QtaTimerOre=0
      .w_QtaTimerMinuti=0
      .w_QtaTimer=0
      .w_DAVOCCOS=space(15)
      .w_DAVOCRIC=space(15)
      .w_DACENCOS=space(15)
      .w_DACODCOM=space(15)
      .w_DAATTIVI=space(15)
      .w_CAUDOC=space(5)
      .w_DA_SEGNO=space(1)
      .w_DAINICOM=ctod("  /  /  ")
      .w_DAFINCOM=ctod("  /  /  ")
      .w_FLANAL=space(1)
      .w_FLGCOM=space(1)
      .w_FLDANA=space(1)
      .w_VOCECR=space(1)
      .w_COMODO=space(30)
      .w_EVSERIAL=space(10)
      .w_RESET=.f.
      .w_DASCONT1=0
      .w_DASCONT2=0
      .w_DASCONT3=0
      .w_DASCONT4=0
      .w_DACONCOD=space(10)
      .w_EVNOMINA=space(15)
      .w_EVRIFPER=space(254)
      .w_GENEVE=space(1)
      .w_DESCAU=space(254)
      .w_DACONTRA=space(10)
      .w_DACODMOD=space(10)
      .w_DACODIMP=space(10)
      .w_CODCLI=space(15)
      .w_DACODATT=space(20)
      .w_DARINNOV=0
      .w_DAUNIMIS=space(3)
      .w_DAPREZZO=0
      .w_DACODICE=space(20)
      .w_DACOCOMP=0
      .w_CCDESPIA=space(40)
      .w_CCDESRIC=space(40)
      .w_CAUACQ=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_CENOBSO=ctod("  /  /  ")
      .w_TIPATT=space(10)
      .w_DESNOM=space(60)
      .w_DADESATT=space(40)
      .w_DASERIAL=space(20)
      .w_CAUATT=space(10)
      .w_ARTIPRIG=space(1)
      .w_ARTIPRI2=space(1)
      .w_FLGZER=space(1)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      .w_CNDATFIN=ctod("  /  /  ")
      .w_DATAOBSO=ctod("  /  /  ")
      .w_DACENRIC=space(15)
      .w_DACOMRIC=space(15)
      .w_DAATTRIC=space(15)
      .w_DATCOINI=ctod("  /  /  ")
      .w_DATCOFIN=ctod("  /  /  ")
      .w_DATRIINI=ctod("  /  /  ")
      .w_DATRIFIN=ctod("  /  /  ")
      .w_DATA=ctod("  /  /  ")
      .w_ORA=space(2)
      .w_MINUTI=space(2)
      .w_PARASS=0
      .w_FLPRES=space(1)
      .w_CHKDATAPRE=space(1)
      .w_GG_PRE=0
      .w_GG_SUC=0
      .w_DATAPREC=ctod("  /  /  ")
      .w_DATASUCC=ctod("  /  /  ")
      .w_DPCOGNOM=space(50)
      .w_DPNOME=space(50)
      .w_DESRES=space(100)
      .w_CADTOBSO=ctod("  /  /  ")
          .DoRTCalc(1,1,.f.)
        .w_LetturaParAgen = i_CodAzi
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_LetturaParAgen))
          .link_1_2('Full')
        endif
        .w_ZoomName = iif(IsAlt(),'TarTempo','GSFA_KTE')
        .w_DAGEST = .f.
        .w_CODPERS = ReadResp(i_codute, "C", .w_CODPRATICA)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODPERS))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_CodPart = iif(Not empty(.w_CODPERS),.w_CODPERS,iif(Isalt(),readdipend(i_CodUte, 'C'),ReadResp(i_codute, "C", .w_CODPRATICA)))
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CodPart))
          .link_1_7('Full')
        endif
        .w_TIPCAU = IIF(Type("oparentobject")="O",'P','I')
        .w_Leggialte = i_CodAzi
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_Leggialte))
          .link_1_9('Full')
        endif
          .DoRTCalc(10,14,.f.)
        .w_CauDefault = Gettipass(.w_EVTIPEVE,.w_TIPCAU)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CauDefault))
          .link_1_17('Full')
        endif
        .DoRTCalc(16,17,.f.)
        if not(empty(.w_CodPratica))
          .link_1_19('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_ATCENCOS))
          .link_1_20('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_ATCODPRA))
          .link_1_21('Full')
        endif
        .w_CODSER = .w_SerUtente
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CODSER))
          .link_1_22('Full')
        endif
        .DoRTCalc(21,22,.f.)
        if not(empty(.w_ATATTCOS))
          .link_1_24('Full')
        endif
        .DoRTCalc(23,24,.f.)
        if not(empty(.w_ATCENRIC))
          .link_1_26('Full')
        endif
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_ATCOMRIC))
          .link_1_27('Full')
        endif
        .w_FLACQ = IIF(Not Empty(.w_CAUACQ),Docgesana(.w_CAUACQ,'A'),.w_FLANAL)
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_ATATTRIC))
          .link_1_29('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_ATCODNOM))
          .link_1_30('Full')
        endif
        .w_DATIPRIG = 'D'
        .w_DATIPRI2 = 'N'
          .DoRTCalc(31,31,.f.)
        .w_DataIniTimer = DateTime()
        .w_Attivo = 'N'
          .DoRTCalc(34,35,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_CACODART))
          .link_1_47('Full')
        endif
          .DoRTCalc(38,38,.f.)
        .w_OB_TEST = i_datsys
        .DoRTCalc(40,42,.f.)
        if not(empty(.w_PrimaUniMis))
          .link_1_52('Full')
        endif
          .DoRTCalc(43,44,.f.)
        .w_QtaTimerOre = IIF(.w_UniMisTempo='S',(.w_DurataOre+cp_round(.w_DurataMin/60,2)),1)
        .w_QtaTimerMinuti = IIF(.w_UniMisTempo='S',.w_DurataOre*60+.w_DurataMin,1)
        .w_QtaTimer = IIF(.w_UniMisTempo='S',IIF( .w_ConversioneOre= 0.01667,.w_QtaTimerMinuti,.w_QtaTimerOre*.w_ConversioneOre),1)
        .DoRTCalc(48,53,.f.)
        if not(empty(.w_CAUDOC))
          .link_1_68('Full')
        endif
        .w_DA_SEGNO = 'D'
          .DoRTCalc(55,57,.f.)
        .w_FLGCOM = Docgesana(.w_CAUDOC,'C')
        .w_FLDANA = Docgesana(.w_CAUDOC,'A')
        .w_VOCECR = Docgesana(.w_CAUDOC,'V')
      .oPgFrm.Page1.oPag.Clock.Calculate()
          .DoRTCalc(61,70,.f.)
        .w_GENEVE = 'N'
          .DoRTCalc(72,87,.f.)
        .w_TIPATT = 'A'
          .DoRTCalc(89,89,.f.)
        .w_DADESATT = .w_DesSer
          .DoRTCalc(91,95,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
          .DoRTCalc(97,105,.f.)
        .w_DATA = DATE()
        .w_ORA = '00'
        .w_MINUTI = '00'
          .DoRTCalc(109,113,.f.)
        .w_DATAPREC = Date() - .w_GG_PRE
        .w_DATASUCC = Date() + .w_GG_SUC
          .DoRTCalc(116,117,.f.)
        .w_DESRES = alltrim(.w_DPCOGNOM)+ ' ' +alltrim(.w_DPNOME)
    endwith
    this.DoRTCalc(119,119,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,6,.t.)
        if .o_CODPERS<>.w_CODPERS
            .w_CodPart = iif(Not empty(.w_CODPERS),.w_CODPERS,iif(Isalt(),readdipend(i_CodUte, 'C'),ReadResp(i_codute, "C", .w_CODPRATICA)))
          .link_1_7('Full')
        endif
            .w_TIPCAU = IIF(Type("oparentobject")="O",'P','I')
          .link_1_9('Full')
        .DoRTCalc(10,25,.t.)
        if .o_CAUDEFAULT<>.w_CAUDEFAULT
            .w_FLACQ = IIF(Not Empty(.w_CAUACQ),Docgesana(.w_CAUACQ,'A'),.w_FLANAL)
        endif
        .DoRTCalc(27,36,.t.)
        if .o_CODSER<>.w_CODSER
          .link_1_47('Full')
        endif
        .DoRTCalc(38,41,.t.)
        if .o_CODSER<>.w_CODSER
          .link_1_52('Full')
        endif
        .DoRTCalc(43,44,.t.)
        if .o_DurataOre<>.w_DurataOre.or. .o_DurataMin<>.w_DurataMin.or. .o_UniMisTempo<>.w_UniMisTempo.or. .o_CODSER<>.w_CODSER
            .w_QtaTimerOre = IIF(.w_UniMisTempo='S',(.w_DurataOre+cp_round(.w_DurataMin/60,2)),1)
        endif
        if .o_DurataOre<>.w_DurataOre.or. .o_DurataMin<>.w_DurataMin.or. .o_UniMisTempo<>.w_UniMisTempo.or. .o_CODSER<>.w_CODSER
            .w_QtaTimerMinuti = IIF(.w_UniMisTempo='S',.w_DurataOre*60+.w_DurataMin,1)
        endif
        if .o_DurataOre<>.w_DurataOre.or. .o_DurataMin<>.w_DurataMin.or. .o_UniMisTempo<>.w_UniMisTempo.or. .o_CODSER<>.w_CODSER
            .w_QtaTimer = IIF(.w_UniMisTempo='S',IIF( .w_ConversioneOre= 0.01667,.w_QtaTimerMinuti,.w_QtaTimerOre*.w_ConversioneOre),1)
        endif
        .DoRTCalc(48,52,.t.)
          .link_1_68('Full')
        .DoRTCalc(54,57,.t.)
            .w_FLGCOM = Docgesana(.w_CAUDOC,'C')
            .w_FLDANA = Docgesana(.w_CAUDOC,'A')
            .w_VOCECR = Docgesana(.w_CAUDOC,'V')
        .oPgFrm.Page1.oPag.Clock.Calculate()
        .DoRTCalc(61,87,.t.)
            .w_TIPATT = 'A'
        .DoRTCalc(89,89,.t.)
            .w_DADESATT = .w_DesSer
        if .o_ORA<>.w_ORA
          .Calculate_BMBWKDYNQX()
        endif
        if .o_MINUTI<>.w_MINUTI
          .Calculate_FTJUDWIPWM()
        endif
        if .o_DATA<>.w_DATA.or. .o_ORA<>.w_ORA.or. .o_MINUTI<>.w_MINUTI
          .Calculate_GKURJEFWAS()
        endif
        .DoRTCalc(91,113,.t.)
        if .o_CodPart<>.w_CodPart
            .w_DATAPREC = Date() - .w_GG_PRE
        endif
        if .o_CodPart<>.w_CodPart
            .w_DATASUCC = Date() + .w_GG_SUC
        endif
        .DoRTCalc(116,117,.t.)
        if .o_CODPERS<>.w_CODPERS
            .w_DESRES = alltrim(.w_DPCOGNOM)+ ' ' +alltrim(.w_DPNOME)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(119,119,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Clock.Calculate()
    endwith
  return

  proc Calculate_QZYVDFKUED()
    with this
          * --- Stop al timer
          .w_Attivo = 'N'
          .w_DurataMin = IIF(.w_Clock.GetMinute()=0 And .w_Clock.GetHour() =0, 1, IIF(.w_Clock.GetSec()>=0 And .w_Clock.GetSec()<30, .w_Clock.GetMinute(), Minute(.w_Clock.GetTime()+(60-Sec(.w_Clock.GetTime())))))
          .w_DurataOre = IIF(.w_Clock.GetSec()>=0 And .w_Clock.GetSec()<30, .w_Clock.GetHour(), Hour(.w_Clock.GetTime()+(60-Sec(.w_Clock.GetTime()))))
    endwith
  endproc
  proc Calculate_YJDICSGHNH()
    with this
          * --- Azzera il timer
          .w_DataIniTimer = IIF(NOT EMPTY(.w_DATA),  cp_CharToDatetime(DTOC(.w_DATA)+' '+.w_ORA+':'+.w_MINUTI+':00'), .w_DataIniTimer)
          .w_DurataOre = 0
          .w_DurataMin = 0
          .w_RESET = .w_CLOCK.ResetTime()
    endwith
  endproc
  proc Calculate_VSVLPEJGRG()
    with this
          * --- Inizializza il timer
          .w_Attivo = 'S'
          .w_DataIniTimer = EVL(.w_DataIniTimer, DateTime())
    endwith
  endproc
  proc Calculate_CAKWIAAGCU()
    with this
          * --- AggiornaTimer
          .w_RESET = .w_Clock.SetMinute(.w_DurataMin)
          .w_RESET = .w_Clock.SetHour(.w_DurataOre)
    endwith
  endproc
  proc Calculate_ATRHLQSOIB()
    with this
          * --- DaCostoaRicavo
          .w_ATCENRIC = IIF(Empty(.w_ATCENRIC) AND (g_PERCCM='S' OR IsAhr()),.w_ATCENCOS,.w_ATCENRIC)
          .link_1_26('Full')
          .w_ATCOMRIC = IIF(Empty(.w_ATCOMRIC),.w_ATCODPRA,.w_ATCOMRIC)
          .link_1_27('Full')
          .w_ATATTRIC = IIF(Empty(.w_ATATTRIC) AND .w_ATCOMRIC=.w_ATCODPRA  AND g_PERCAN='S',.w_ATATTCOS,.w_ATATTRIC)
          .link_1_29('Full')
    endwith
  endproc
  proc Calculate_LKWTWVRCIM()
    with this
          * --- DaRicavoaCosto
          .w_ATCENCOS = IIF(Empty(.w_ATCENCOS) AND g_COAN = 'S',.w_ATCENRIC,.w_ATCENCOS)
          .link_1_20('Full')
          .w_ATCODPRA = IIF(Empty(.w_ATCODPRA),.w_ATCOMRIC,.w_ATCODPRA)
          .link_1_21('Full')
          .w_ATATTCOS = IIF(Empty(.w_ATATTCOS) AND .w_ATCOMRIC=.w_ATCODPRA  AND g_PERCAN='S',.w_ATATTRIC,.w_ATATTCOS)
          .link_1_24('Full')
    endwith
  endproc
  proc Calculate_BMBWKDYNQX()
    with this
          * --- Resetta w_ORA
          .w_ORA = PADL(alltrim(.w_ORA),2, '0')
    endwith
  endproc
  proc Calculate_FTJUDWIPWM()
    with this
          * --- Resetta w_MINUTI
          .w_MINUTI = PADL(alltrim(.w_MINUTI),2, '0')
    endwith
  endproc
  proc Calculate_GKURJEFWAS()
    with this
          * --- Aggiorna data\ora
          .w_DataIniTimer = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATA),  DTOC(.w_DATA)+' '+.w_ORA+':'+.w_MINUTI+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_JDQDCBCCWS()
    with this
          * --- DaComp
          GSAG_BTM(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_VGBEWBFWSK()
    with this
          * --- Nuovo
          GSAG_BTM(this;
              ,'E';
             )
    endwith
  endproc
  proc Calculate_QFOKEGONKQ()
    with this
          * --- Completa
          GSAG_BTM(this;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_MKTQHXEZKT()
    with this
          * --- Assegna  dati da riproporre
          .w_CodPratica = .DefPratica
          .link_1_19('Full')
          .w_CODPERS = .DefResponsabile
          .link_1_5('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDurataOre_1_13.enabled = this.oPgFrm.Page1.oPag.oDurataOre_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDurataMin_1_15.enabled = this.oPgFrm.Page1.oPag.oDurataMin_1_15.mCond()
    this.oPgFrm.Page1.oPag.oATCENCOS_1_20.enabled = this.oPgFrm.Page1.oPag.oATCENCOS_1_20.mCond()
    this.oPgFrm.Page1.oPag.oDesSer_1_23.enabled = this.oPgFrm.Page1.oPag.oDesSer_1_23.mCond()
    this.oPgFrm.Page1.oPag.oATATTCOS_1_24.enabled = this.oPgFrm.Page1.oPag.oATATTCOS_1_24.mCond()
    this.oPgFrm.Page1.oPag.oATCENRIC_1_26.enabled = this.oPgFrm.Page1.oPag.oATCENRIC_1_26.mCond()
    this.oPgFrm.Page1.oPag.oATATTRIC_1_29.enabled = this.oPgFrm.Page1.oPag.oATATTRIC_1_29.mCond()
    this.oPgFrm.Page1.oPag.oDATIPRIG_1_31.enabled = this.oPgFrm.Page1.oPag.oDATIPRIG_1_31.mCond()
    this.oPgFrm.Page1.oPag.oDATIPRI2_1_32.enabled = this.oPgFrm.Page1.oPag.oDATIPRI2_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_61.enabled = this.oPgFrm.Page1.oPag.oBtn_1_61.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_156.enabled = this.oPgFrm.Page1.oPag.oBtn_1_156.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODPERS_1_5.visible=!this.oPgFrm.Page1.oPag.oCODPERS_1_5.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCauDefault_1_17.visible=!this.oPgFrm.Page1.oPag.oCauDefault_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCodPratica_1_19.visible=!this.oPgFrm.Page1.oPag.oCodPratica_1_19.mHide()
    this.oPgFrm.Page1.oPag.oATCENCOS_1_20.visible=!this.oPgFrm.Page1.oPag.oATCENCOS_1_20.mHide()
    this.oPgFrm.Page1.oPag.oATCODPRA_1_21.visible=!this.oPgFrm.Page1.oPag.oATCODPRA_1_21.mHide()
    this.oPgFrm.Page1.oPag.oCODSER_1_22.visible=!this.oPgFrm.Page1.oPag.oCODSER_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDesSer_1_23.visible=!this.oPgFrm.Page1.oPag.oDesSer_1_23.mHide()
    this.oPgFrm.Page1.oPag.oATATTCOS_1_24.visible=!this.oPgFrm.Page1.oPag.oATATTCOS_1_24.mHide()
    this.oPgFrm.Page1.oPag.oDesAgg_1_25.visible=!this.oPgFrm.Page1.oPag.oDesAgg_1_25.mHide()
    this.oPgFrm.Page1.oPag.oATCENRIC_1_26.visible=!this.oPgFrm.Page1.oPag.oATCENRIC_1_26.mHide()
    this.oPgFrm.Page1.oPag.oATCOMRIC_1_27.visible=!this.oPgFrm.Page1.oPag.oATCOMRIC_1_27.mHide()
    this.oPgFrm.Page1.oPag.oATATTRIC_1_29.visible=!this.oPgFrm.Page1.oPag.oATATTRIC_1_29.mHide()
    this.oPgFrm.Page1.oPag.oATCODNOM_1_30.visible=!this.oPgFrm.Page1.oPag.oATCODNOM_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDATIPRIG_1_31.visible=!this.oPgFrm.Page1.oPag.oDATIPRIG_1_31.mHide()
    this.oPgFrm.Page1.oPag.oDATIPRI2_1_32.visible=!this.oPgFrm.Page1.oPag.oDATIPRI2_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oDesPratica_1_44.visible=!this.oPgFrm.Page1.oPag.oDesPratica_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_57.visible=!this.oPgFrm.Page1.oPag.oBtn_1_57.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_59.visible=!this.oPgFrm.Page1.oPag.oBtn_1_59.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_60.visible=!this.oPgFrm.Page1.oPag.oBtn_1_60.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_61.visible=!this.oPgFrm.Page1.oPag.oBtn_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_76.visible=!this.oPgFrm.Page1.oPag.oStr_1_76.mHide()
    this.oPgFrm.Page1.oPag.oDESCAU_1_90.visible=!this.oPgFrm.Page1.oPag.oDESCAU_1_90.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page1.oPag.oCCDESPIA_1_103.visible=!this.oPgFrm.Page1.oPag.oCCDESPIA_1_103.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_104.visible=!this.oPgFrm.Page1.oPag.oStr_1_104.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_105.visible=!this.oPgFrm.Page1.oPag.oStr_1_105.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_106.visible=!this.oPgFrm.Page1.oPag.oStr_1_106.mHide()
    this.oPgFrm.Page1.oPag.oCCDESRIC_1_107.visible=!this.oPgFrm.Page1.oPag.oCCDESRIC_1_107.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_108.visible=!this.oPgFrm.Page1.oPag.oStr_1_108.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_109.visible=!this.oPgFrm.Page1.oPag.oStr_1_109.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_110.visible=!this.oPgFrm.Page1.oPag.oStr_1_110.mHide()
    this.oPgFrm.Page1.oPag.oDESNOM_1_115.visible=!this.oPgFrm.Page1.oPag.oDESNOM_1_115.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_116.visible=!this.oPgFrm.Page1.oPag.oStr_1_116.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_124.visible=!this.oPgFrm.Page1.oPag.oStr_1_124.mHide()
    this.oPgFrm.Page1.oPag.oDATA_1_136.visible=!this.oPgFrm.Page1.oPag.oDATA_1_136.mHide()
    this.oPgFrm.Page1.oPag.oORA_1_137.visible=!this.oPgFrm.Page1.oPag.oORA_1_137.mHide()
    this.oPgFrm.Page1.oPag.oMINUTI_1_138.visible=!this.oPgFrm.Page1.oPag.oMINUTI_1_138.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_139.visible=!this.oPgFrm.Page1.oPag.oStr_1_139.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_140.visible=!this.oPgFrm.Page1.oPag.oStr_1_140.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_141.visible=!this.oPgFrm.Page1.oPag.oStr_1_141.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_152.visible=!this.oPgFrm.Page1.oPag.oStr_1_152.mHide()
    this.oPgFrm.Page1.oPag.oDESRES_1_155.visible=!this.oPgFrm.Page1.oPag.oDESRES_1_155.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_156.visible=!this.oPgFrm.Page1.oPag.oBtn_1_156.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_ktm
    If cevent ='Blank' and Type('Oparentobject')='O'
           this.w_DataIniTimer = Oparentobject.w_DataIniTimer
            this.w_DurataMin = Oparentobject.w_DurataMin
            this.w_DurataOre = Oparentobject.w_DurataOre
            this.w_CODPRATICA = Oparentobject.GSFA_ACE.w_EVCODPRA
            = setvaluelinked ( "M" , this ,"w_CODPRATICA", this.w_CODPRATICA)
            this.w_ATTIVO = "N"
            this.w_DAGEST = .T.
            this.w_EVRIFPER = Oparentobject.GSFA_ACE.w_EVRIFPER
            this.w_EVNOMINA = Oparentobject.GSFA_ACE.w_EVNOMINA
            this.w_EVTIPEVE = Oparentobject.w_TETIPEVE
            this.w_RESET = This.w_Clock.SetHour(this.w_DurataOre)
            this.w_RESET = This.w_Clock.SetMinute(this.w_DurataMin)
            this.mcalc(.T.)
    endif
    
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("StopTimer")
          .Calculate_QZYVDFKUED()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AzzeraTimer")
          .Calculate_YJDICSGHNH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("InitTimer")
          .Calculate_VSVLPEJGRG()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.Clock.Event(cEvent)
        if lower(cEvent)==lower("w_DurataMin Changed") or lower(cEvent)==lower("w_DurataOre Changed") or lower(cEvent)==lower("AzzeraTimer")
          .Calculate_CAKWIAAGCU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaCostoaRicavo")
          .Calculate_ATRHLQSOIB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaRicavoaCosto")
          .Calculate_LKWTWVRCIM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("DaComp")
          .Calculate_JDQDCBCCWS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Nuovo")
          .Calculate_VGBEWBFWSK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Completa")
          .Calculate_QFOKEGONKQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Assegna")
          .Calculate_MKTQHXEZKT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_ktm
    * Valorizzo dati analitica
    If Cevent='w_ATATTRIC Changed' or  Cevent='w_ATCENRIC Changed' or Cevent='w_ATCOMRIC Changed' and !Isalt()
       This.Notifyevent('DaRicavoaCosto')
    Endif
    If Cevent='w_ATCODPRA Changed' or  Cevent='w_ATATTCOS Changed' or Cevent='w_ATCENCOS Changed' and !Isalt()
       This.Notifyevent('DaCostoaRicavo')
    Endif
    If (cevent='Completa' or cevent='DaComp' or cevent='Nuovo') and Isalt()
       This.Notifyevent('Assegna')
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LetturaParAgen
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LetturaParAgen) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LetturaParAgen)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLPRES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LetturaParAgen);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LetturaParAgen)
            select PACODAZI,PAFLPRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LetturaParAgen = NVL(_Link_.PACODAZI,space(5))
      this.w_FLPRES = NVL(_Link_.PAFLPRES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LetturaParAgen = space(5)
      endif
      this.w_FLPRES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LetturaParAgen Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPERS
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPERS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_CODPERS))
          select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPERS)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODPERS)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODPERS)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPERS) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oCODPERS_1_5'),i_cWhere,'GSAR_BDZ',"Responsabili",'GSAG_KPR.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPERS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPERS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODPERS)
            select DPCODICE,DPCOGNOM,DPNOME,DPDTOBSO,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPERS = NVL(_Link_.DPCODICE,space(5))
      this.w_DPCOGNOM = NVL(_Link_.DPCOGNOM,space(50))
      this.w_DPNOME = NVL(_Link_.DPNOME,space(50))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
      this.w_TipoRisorsa = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODPERS = space(5)
      endif
      this.w_DPCOGNOM = space(50)
      this.w_DPNOME = space(50)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TipoRisorsa = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST) AND .w_TipoRisorsa='P'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore inesistente o obsoleto")
        endif
        this.w_CODPERS = space(5)
        this.w_DPCOGNOM = space(50)
        this.w_DPNOME = space(50)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TipoRisorsa = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPERS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodPart
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodPart) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodPart)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPSERPRE,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CodPart);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CodPart)
            select DPCODICE,DPSERPRE,DPTIPRIG,DPCTRPRE,DPGG_PRE,DPGG_SUC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodPart = NVL(_Link_.DPCODICE,space(5))
      this.w_SerUtente = NVL(_Link_.DPSERPRE,space(10))
      this.w_DTIPRIG = NVL(_Link_.DPTIPRIG,space(1))
      this.w_CHKDATAPRE = NVL(_Link_.DPCTRPRE,space(1))
      this.w_GG_PRE = NVL(_Link_.DPGG_PRE,0)
      this.w_GG_SUC = NVL(_Link_.DPGG_SUC,0)
    else
      if i_cCtrl<>'Load'
        this.w_CodPart = space(5)
      endif
      this.w_SerUtente = space(10)
      this.w_DTIPRIG = space(1)
      this.w_CHKDATAPRE = space(1)
      this.w_GG_PRE = 0
      this.w_GG_SUC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodPart Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=Leggialte
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_Leggialte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_Leggialte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PANOEDES,PAFLGZER,PAGENPRE";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_Leggialte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_Leggialte)
            select PACODAZI,PANOEDES,PAFLGZER,PAGENPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_Leggialte = NVL(_Link_.PACODAZI,space(10))
      this.w_NOEDES = NVL(_Link_.PANOEDES,space(30))
      this.w_FLGZER = NVL(_Link_.PAFLGZER,space(1))
      this.w_GENPRE = NVL(_Link_.PAGENPRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_Leggialte = space(10)
      endif
      this.w_NOEDES = space(30)
      this.w_FLGZER = space(1)
      this.w_GENPRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_Leggialte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CauDefault
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CauDefault) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CauDefault)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACAUACQ,CAFLANAL,CACAUDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CauDefault))
          select CACODICE,CADESCRI,CACAUACQ,CAFLANAL,CACAUDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CauDefault)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_CauDefault)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACAUACQ,CAFLANAL,CACAUDOC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_CauDefault)+"%");

            select CACODICE,CADESCRI,CACAUACQ,CAFLANAL,CACAUDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CauDefault) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oCauDefault_1_17'),i_cWhere,'',"",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACAUACQ,CAFLANAL,CACAUDOC";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CACAUACQ,CAFLANAL,CACAUDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CauDefault)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CACAUACQ,CAFLANAL,CACAUDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CauDefault);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CauDefault)
            select CACODICE,CADESCRI,CACAUACQ,CAFLANAL,CACAUDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CauDefault = NVL(_Link_.CACODICE,space(20))
      this.w_DESCAU = NVL(_Link_.CADESCRI,space(254))
      this.w_CAUACQ = NVL(_Link_.CACAUACQ,space(5))
      this.w_FLANAL = NVL(_Link_.CAFLANAL,space(1))
      this.w_CAUDOC = NVL(_Link_.CACAUDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CauDefault = space(20)
      endif
      this.w_DESCAU = space(254)
      this.w_CAUACQ = space(5)
      this.w_FLANAL = space(1)
      this.w_CAUDOC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CauDefault Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodPratica
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodPratica) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CodPratica)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CodPratica))
          select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CodPratica)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CodPratica)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CodPratica)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStrODBC(trim(this.w_CodPratica)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFGIU like "+cp_ToStr(trim(this.w_CodPratica)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStrODBC(trim(this.w_CodPratica)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNRIFERI like "+cp_ToStr(trim(this.w_CodPratica)+"%");

            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CodPratica) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCodPratica_1_19'),i_cWhere,'',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodPratica)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CodPratica);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CodPratica)
            select CNCODCAN,CNDESCAN,CNRIFGIU,CNRIFERI,CN__ENTE,CNDATFIN,CNDTOBSO,CNPARASS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodPratica = NVL(_Link_.CNCODCAN,space(15))
      this.w_DesPratica = NVL(_Link_.CNDESCAN,space(100))
      this.w_COMODO = NVL(_Link_.CNRIFGIU,space(30))
      this.w_COMODO = NVL(_Link_.CNRIFERI,space(30))
      this.w_ENTE = NVL(_Link_.CN__ENTE,space(10))
      this.w_CNDATFIN = NVL(cp_ToDate(_Link_.CNDATFIN),ctod("  /  /  "))
      this.w_DATAOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_PARASS = NVL(_Link_.CNPARASS,0)
    else
      if i_cCtrl<>'Load'
        this.w_CodPratica = space(15)
      endif
      this.w_DesPratica = space(100)
      this.w_COMODO = space(30)
      this.w_COMODO = space(30)
      this.w_ENTE = space(10)
      this.w_CNDATFIN = ctod("  /  /  ")
      this.w_DATAOBSO = ctod("  /  /  ")
      this.w_PARASS = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATAOBSO>.w_OBTEST OR EMPTY(.w_DATAOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente od obsoleto")
        endif
        this.w_CodPratica = space(15)
        this.w_DesPratica = space(100)
        this.w_COMODO = space(30)
        this.w_COMODO = space(30)
        this.w_ENTE = space(10)
        this.w_CNDATFIN = ctod("  /  /  ")
        this.w_DATAOBSO = ctod("  /  /  ")
        this.w_PARASS = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_bRes and i_cCtrl='Drop'
      with this
        if i_bRes and !( ((.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) and (.w_DATAOBSO>.w_OBTEST OR EMPTY(.w_DATAOBSO))) OR ! Isalt())
          i_bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
          if not(i_bRes)
            this.w_CodPratica = space(15)
            this.w_DesPratica = space(100)
            this.w_COMODO = space(30)
            this.w_COMODO = space(30)
            this.w_ENTE = space(10)
            this.w_CNDATFIN = ctod("  /  /  ")
            this.w_DATAOBSO = ctod("  /  /  ")
            this.w_PARASS = 0
          endif
        endif
      endwith
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodPratica Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCENCOS
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ATCENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ATCENCOS))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oATCENCOS_1_20'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ATCENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ATCENCOS)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCENCOS = space(15)
      endif
      this.w_CCDESPIA = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATCENCOS = space(15)
        this.w_CCDESPIA = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODPRA
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ATCODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ATCODPRA))
          select CNCODCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oATCODPRA_1_21'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ATCODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ATCODPRA)
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODPRA = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_ATCODPRA = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSER
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODSER))
          select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSER)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStrODBC(trim(this.w_CODSER)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CACODART like "+cp_ToStr(trim(this.w_CODSER)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODSER) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODSER_1_22'),i_cWhere,'GSMA_BZA',""+iif(not isalt (), "Codici articoli", "Prestazioni") +"",'TarTempo.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODSER)
            select CACODICE,CADESART,CADESSUP,CACODART,CADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSER = NVL(_Link_.CACODICE,space(20))
      this.w_DesSer = NVL(_Link_.CADESART,space(40))
      this.w_DesAgg = NVL(_Link_.CADESSUP,space(0))
      this.w_CACODART = NVL(_Link_.CACODART,space(20))
      this.w_CADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODSER = space(20)
      endif
      this.w_DesSer = space(40)
      this.w_DesAgg = space(0)
      this.w_CACODART = space(20)
      this.w_CADTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_CACODART, 'FM-FO-DE') AND (EMPTY(.w_CADTOBSO) OR .w_CADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_CODSER = space(20)
        this.w_DesSer = space(40)
        this.w_DesAgg = space(0)
        this.w_CACODART = space(20)
        this.w_CADTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATATTCOS
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATATTCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATATTCOS)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCODPRA);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_ATCODPRA;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_ATATTCOS))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATATTCOS)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATATTCOS) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATATTCOS_1_24'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCODPRA<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCODPRA);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATATTCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATATTCOS);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCODPRA);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_ATCODPRA;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_ATATTCOS)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATATTCOS = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ATATTCOS = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATATTCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCENRIC
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCENRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_ATCENRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_ATCENRIC))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCENRIC)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCENRIC) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oATCENRIC_1_26'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCENRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_ATCENRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_ATCENRIC)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCENRIC = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESRIC = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCENRIC = space(15)
      endif
      this.w_CCDESRIC = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ATCENRIC = space(15)
        this.w_CCDESRIC = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCENRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCOMRIC
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCOMRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_ATCOMRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_ATCOMRIC))
          select CNCODCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCOMRIC)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATCOMRIC) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oATCOMRIC_1_27'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCOMRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_ATCOMRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_ATCOMRIC)
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCOMRIC = NVL(_Link_.CNCODCAN,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ATCOMRIC = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_ATCOMRIC = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCOMRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATATTRIC
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATATTRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_ATATTRIC)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCOMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_ATCOMRIC;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_ATATTRIC))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATATTRIC)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ATATTRIC) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oATATTRIC_1_29'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ATCOMRIC<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCOMRIC);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATATTRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_ATATTRIC);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_ATCOMRIC);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_ATCOMRIC;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_ATATTRIC)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATATTRIC = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ATATTRIC = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATATTRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ATCODNOM
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ATCODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_ATCODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_ATCODNOM))
          select NOCODICE,NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ATCODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_ATCODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_ATCODNOM)+"%");

            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ATCODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oATCODNOM_1_30'),i_cWhere,'GSAR_ANO',"Elenco Nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ATCODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_ATCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_ATCODNOM)
            select NOCODICE,NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ATCODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ATCODNOM = space(15)
      endif
      this.w_DESNOM = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ATCODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODART
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARTIPRIG,ARTIPRI2";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CACODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CACODART)
            select ARCODART,ARUNMIS1,ARTIPRIG,ARTIPRI2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODART = NVL(_Link_.ARCODART,space(20))
      this.w_PrimaUniMis = NVL(_Link_.ARUNMIS1,space(10))
      this.w_ARTIPRIG = NVL(_Link_.ARTIPRIG,space(1))
      this.w_ARTIPRI2 = NVL(_Link_.ARTIPRI2,space(1))
      this.w_DATIPRIG = NVL(_Link_.ARTIPRIG,space(1))
      this.w_DATIPRI2 = NVL(_Link_.ARTIPRI2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACODART = space(20)
      endif
      this.w_PrimaUniMis = space(10)
      this.w_ARTIPRIG = space(1)
      this.w_ARTIPRI2 = space(1)
      this.w_DATIPRIG = space(1)
      this.w_DATIPRI2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PrimaUniMis
  func Link_1_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PrimaUniMis) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PrimaUniMis)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLTEMP,UMDURORE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_PrimaUniMis);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_PrimaUniMis)
            select UMCODICE,UMFLTEMP,UMDURORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PrimaUniMis = NVL(_Link_.UMCODICE,space(10))
      this.w_UniMisTempo = NVL(_Link_.UMFLTEMP,space(10))
      this.w_ConversioneOre = NVL(_Link_.UMDURORE,0)
    else
      if i_cCtrl<>'Load'
        this.w_PrimaUniMis = space(10)
      endif
      this.w_UniMisTempo = space(10)
      this.w_ConversioneOre = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PrimaUniMis Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUDOC
  func Link_1_68(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUDOC)
            select TDTIPDOC,TDFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_FLDANA = NVL(_Link_.TDFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUDOC = space(5)
      endif
      this.w_FLDANA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODPERS_1_5.value==this.w_CODPERS)
      this.oPgFrm.Page1.oPag.oCODPERS_1_5.value=this.w_CODPERS
    endif
    if not(this.oPgFrm.Page1.oPag.oDurataOre_1_13.value==this.w_DurataOre)
      this.oPgFrm.Page1.oPag.oDurataOre_1_13.value=this.w_DurataOre
    endif
    if not(this.oPgFrm.Page1.oPag.oDurataMin_1_15.value==this.w_DurataMin)
      this.oPgFrm.Page1.oPag.oDurataMin_1_15.value=this.w_DurataMin
    endif
    if not(this.oPgFrm.Page1.oPag.oCauDefault_1_17.value==this.w_CauDefault)
      this.oPgFrm.Page1.oPag.oCauDefault_1_17.value=this.w_CauDefault
    endif
    if not(this.oPgFrm.Page1.oPag.oCodPratica_1_19.value==this.w_CodPratica)
      this.oPgFrm.Page1.oPag.oCodPratica_1_19.value=this.w_CodPratica
    endif
    if not(this.oPgFrm.Page1.oPag.oATCENCOS_1_20.value==this.w_ATCENCOS)
      this.oPgFrm.Page1.oPag.oATCENCOS_1_20.value=this.w_ATCENCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODPRA_1_21.value==this.w_ATCODPRA)
      this.oPgFrm.Page1.oPag.oATCODPRA_1_21.value=this.w_ATCODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSER_1_22.value==this.w_CODSER)
      this.oPgFrm.Page1.oPag.oCODSER_1_22.value=this.w_CODSER
    endif
    if not(this.oPgFrm.Page1.oPag.oDesSer_1_23.value==this.w_DesSer)
      this.oPgFrm.Page1.oPag.oDesSer_1_23.value=this.w_DesSer
    endif
    if not(this.oPgFrm.Page1.oPag.oATATTCOS_1_24.value==this.w_ATATTCOS)
      this.oPgFrm.Page1.oPag.oATATTCOS_1_24.value=this.w_ATATTCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oDesAgg_1_25.value==this.w_DesAgg)
      this.oPgFrm.Page1.oPag.oDesAgg_1_25.value=this.w_DesAgg
    endif
    if not(this.oPgFrm.Page1.oPag.oATCENRIC_1_26.value==this.w_ATCENRIC)
      this.oPgFrm.Page1.oPag.oATCENRIC_1_26.value=this.w_ATCENRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oATCOMRIC_1_27.value==this.w_ATCOMRIC)
      this.oPgFrm.Page1.oPag.oATCOMRIC_1_27.value=this.w_ATCOMRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oATATTRIC_1_29.value==this.w_ATATTRIC)
      this.oPgFrm.Page1.oPag.oATATTRIC_1_29.value=this.w_ATATTRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oATCODNOM_1_30.value==this.w_ATCODNOM)
      this.oPgFrm.Page1.oPag.oATCODNOM_1_30.value=this.w_ATCODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDATIPRIG_1_31.RadioValue()==this.w_DATIPRIG)
      this.oPgFrm.Page1.oPag.oDATIPRIG_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATIPRI2_1_32.RadioValue()==this.w_DATIPRI2)
      this.oPgFrm.Page1.oPag.oDATIPRI2_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDataIniTimer_1_36.value==this.w_DataIniTimer)
      this.oPgFrm.Page1.oPag.oDataIniTimer_1_36.value=this.w_DataIniTimer
    endif
    if not(this.oPgFrm.Page1.oPag.oDesPratica_1_44.value==this.w_DesPratica)
      this.oPgFrm.Page1.oPag.oDesPratica_1_44.value=this.w_DesPratica
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_90.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_90.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA_1_103.value==this.w_CCDESPIA)
      this.oPgFrm.Page1.oPag.oCCDESPIA_1_103.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESRIC_1_107.value==this.w_CCDESRIC)
      this.oPgFrm.Page1.oPag.oCCDESRIC_1_107.value=this.w_CCDESRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_115.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_115.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA_1_136.value==this.w_DATA)
      this.oPgFrm.Page1.oPag.oDATA_1_136.value=this.w_DATA
    endif
    if not(this.oPgFrm.Page1.oPag.oORA_1_137.value==this.w_ORA)
      this.oPgFrm.Page1.oPag.oORA_1_137.value=this.w_ORA
    endif
    if not(this.oPgFrm.Page1.oPag.oMINUTI_1_138.value==this.w_MINUTI)
      this.oPgFrm.Page1.oPag.oMINUTI_1_138.value=this.w_MINUTI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRES_1_155.value==this.w_DESRES)
      this.oPgFrm.Page1.oPag.oDESRES_1_155.value=this.w_DESRES
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST) AND .w_TipoRisorsa='P')  and not(!Isalt())  and not(empty(.w_CODPERS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPERS_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore inesistente o obsoleto")
          case   not(.w_DurataMin<60)  and (.w_Attivo='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDurataMin_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CauDefault))  and not(Isalt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCauDefault_1_17.SetFocus()
            i_bnoObbl = !empty(.w_CauDefault)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATAOBSO>.w_OBTEST OR EMPTY(.w_DATAOBSO))  and not(!Isalt())  and not(empty(.w_CodPratica))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCodPratica_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente od obsoleto")
          case   not(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO))  and not(Isalt())  and (.w_FLACQ $ 'S-I' AND (IsAhr() OR g_PERCCM='S'))  and not(empty(.w_ATCENCOS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCENCOS_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(Isalt())  and not(empty(.w_ATCODPRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCODPRA_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
          case   not(ChkTipArt(.w_CACODART, 'FM-FO-DE') AND (EMPTY(.w_CADTOBSO) OR .w_CADTOBSO>=i_datsys))  and not(!Isalt())  and not(empty(.w_CODSER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODSER_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
          case   not(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO))  and not(Isalt())  and ((.w_FLANAL='S' or .w_FLDANA='S' ) AND (IsAhr() OR g_PERCCM='S'))  and not(empty(.w_ATCENRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCENRIC_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(Isalt())  and not(empty(.w_ATCOMRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oATCOMRIC_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
          case   (empty(.w_DATA))  and not(NOT ISALT())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA_1_136.SetFocus()
            i_bnoObbl = !empty(.w_DATA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ORA))  and not(NOT ISALT())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORA_1_137.SetFocus()
            i_bnoObbl = !empty(.w_ORA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MINUTI))  and not(NOT ISALT())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINUTI_1_138.SetFocus()
            i_bnoObbl = !empty(.w_MINUTI)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODPERS = this.w_CODPERS
    this.o_CodPart = this.w_CodPart
    this.o_DurataOre = this.w_DurataOre
    this.o_DurataMin = this.w_DurataMin
    this.o_CauDefault = this.w_CauDefault
    this.o_CODSER = this.w_CODSER
    this.o_UniMisTempo = this.w_UniMisTempo
    this.o_DATA = this.w_DATA
    this.o_ORA = this.w_ORA
    this.o_MINUTI = this.w_MINUTI
    return

enddefine

* --- Define pages as container
define class tgsag_ktmPag1 as StdContainer
  Width  = 637
  height = 342
  stdWidth  = 637
  stdheight = 342
  resizeXpos=323
  resizeYpos=249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODPERS_1_5 as StdField with uid="OEMKVQBEYB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODPERS", cQueryName = "CODPERS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valore inesistente o obsoleto",;
    ToolTipText = "Responsabile",;
    HelpContextID = 239440858,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=117, Top=86, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPCODICE", oKey_1_2="this.w_CODPERS"

  func oCODPERS_1_5.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  func oCODPERS_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPERS_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPERS_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oCODPERS_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Responsabili",'GSAG_KPR.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oCODPERS_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DPCODICE=this.parent.oContained.w_CODPERS
     i_obj.ecpSave()
  endproc


  add object oBtn_1_11 as StdButton with uid="QHYJCJDXQP",left=86, top=6, width=48,height=45,;
    CpPicture="bmp\play.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per attivare il timer";
    , HelpContextID = 47079974;
    , caption='\<Start';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      this.parent.oContained.NotifyEvent("InitTimer")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Attivo='N')
      endwith
    endif
  endfunc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_Attivo<>'N' or .w_DAGEST)
     endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="YZQCVUXQYK",left=86, top=6, width=48,height=45,;
    CpPicture="bmp\stop.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per arrestare il timer (anche parzialmente)";
    , HelpContextID = 193806886;
    , Caption='\<Stop';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      this.parent.oContained.NotifyEvent("StopTimer")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Attivo<>'N')
      endwith
    endif
  endfunc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_Attivo='N' or .w_DAGEST)
     endwith
    endif
  endfunc

  add object oDurataOre_1_13 as StdField with uid="PHQRCKNSLA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DurataOre", cQueryName = "DurataOre",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 205621000,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=334, Top=26

  func oDurataOre_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attivo='N')
    endwith
   endif
  endfunc

  add object oDurataMin_1_15 as StdField with uid="LKLOMSPJFE",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DurataMin", cQueryName = "DurataMin",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 205620865,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=378, Top=26

  func oDurataMin_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Attivo='N')
    endwith
   endif
  endfunc

  func oDurataMin_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DurataMin<60)
    endwith
    return bRes
  endfunc

  add object oCauDefault_1_17 as StdField with uid="TZJAPKKMEI",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CauDefault", cQueryName = "CauDefault",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 139327141,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=117, Top=86, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CauDefault"

  func oCauDefault_1_17.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oCauDefault_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCauDefault_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCauDefault_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oCauDefault_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc

  add object oCodPratica_1_19 as StdField with uid="NHEKFOTWVB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CodPratica", cQueryName = "CodPratica",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente od obsoleto",;
    ToolTipText = "Codice",;
    HelpContextID = 208866369,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=117, Top=112, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CodPratica"

  func oCodPratica_1_19.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  func oCodPratica_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
      if bRes and !( ((.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) and (.w_DATAOBSO>.w_OBTEST OR EMPTY(.w_DATAOBSO))) OR ! Isalt())
         bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oCodPratica_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodPratica_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCodPratica_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc

  add object oATCENCOS_1_20 as StdField with uid="ASYVZTQKNE",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ATCENCOS", cQueryName = "ATCENCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo",;
    HelpContextID = 213950119,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=117, Top=112, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ATCENCOS"

  func oATCENCOS_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLACQ $ 'S-I' AND (IsAhr() OR g_PERCCM='S'))
    endwith
   endif
  endfunc

  func oATCENCOS_1_20.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oATCENCOS_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCENCOS_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCENCOS_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oATCENCOS_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oATCENCOS_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ATCENCOS
     i_obj.ecpSave()
  endproc

  add object oATCODPRA_1_21 as StdField with uid="LQGIVCDTQM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ATCODPRA", cQueryName = "ATCODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 5676729,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=117, Top=138, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ATCODPRA"

  func oATCODPRA_1_21.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oATCODPRA_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
      if .not. empty(.w_ATATTCOS)
        bRes2=.link_1_24('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oATCODPRA_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODPRA_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oATCODPRA_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oATCODPRA_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_ATCODPRA
     i_obj.ecpSave()
  endproc

  add object oCODSER_1_22 as StdField with uid="NQAZQYRMPT",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODSER", cQueryName = "CODSER",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 239244250,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=117, Top=138, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_CODSER"

  func oCODSER_1_22.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  func oCODSER_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSER_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSER_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODSER_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',""+iif(not isalt (), "Codici articoli", "Prestazioni") +"",'TarTempo.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oCODSER_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODSER
     i_obj.ecpSave()
  endproc

  add object oDesSer_1_23 as StdField with uid="JWVQJSFOLX",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DesSer", cQueryName = "DesSer",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della prestazione",;
    HelpContextID = 205491658,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=287, Top=138, InputMask=replicate('X',40)

  func oDesSer_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODSER) AND (.w_NOEDES='N' OR !Isalt() or cp_IsAdministrator()))
    endwith
   endif
  endfunc

  func oDesSer_1_23.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oATATTCOS_1_24 as StdField with uid="MPATSJEAEP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ATATTCOS", cQueryName = "ATATTCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 206683815,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=489, Top=138, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_ATCODPRA", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_ATATTCOS"

  func oATATTCOS_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ATCODPRA))
    endwith
   endif
  endfunc

  func oATATTCOS_1_24.mHide()
    with this.Parent.oContained
      return (g_PERCAN <>'S' or Isalt())
    endwith
  endfunc

  func oATATTCOS_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oATATTCOS_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATATTCOS_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_ATCODPRA)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_ATCODPRA)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATATTCOS_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oATATTCOS_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_ATCODPRA
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_ATATTCOS
     i_obj.ecpSave()
  endproc

  add object oDesAgg_1_25 as StdMemo with uid="XENUFWOWED",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DesAgg", cQueryName = "DesAgg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva della prestazione",;
    HelpContextID = 120688074,;
   bGlobalFont=.t.,;
    Height=66, Width=491, Left=117, Top=191

  func oDesAgg_1_25.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oATCENRIC_1_26 as StdField with uid="GWCBQUFVPT",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ATCENRIC", cQueryName = "ATCENRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di ricavo",;
    HelpContextID = 37708105,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=117, Top=165, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_ATCENRIC"

  func oATCENRIC_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLANAL='S' or .w_FLDANA='S' ) AND (IsAhr() OR g_PERCCM='S'))
    endwith
   endif
  endfunc

  func oATCENRIC_1_26.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oATCENRIC_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCENRIC_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCENRIC_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oATCENRIC_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oATCENRIC_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_ATCENRIC
     i_obj.ecpSave()
  endproc

  add object oATCOMRIC_1_27 as StdField with uid="CCYDSSPMTJ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ATCOMRIC", cQueryName = "ATCOMRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 37314889,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=117, Top=191, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_ATCOMRIC"

  func oATCOMRIC_1_27.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oATCOMRIC_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
      if .not. empty(.w_ATATTRIC)
        bRes2=.link_1_29('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oATCOMRIC_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCOMRIC_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oATCOMRIC_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oATCOMRIC_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_ATCOMRIC
     i_obj.ecpSave()
  endproc

  add object oATATTRIC_1_29 as StdField with uid="LTBYOWJTQX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ATATTRIC", cQueryName = "ATATTRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 44974409,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=489, Top=191, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_ATCOMRIC", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_ATATTRIC"

  func oATATTRIC_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ATCOMRIC))
    endwith
   endif
  endfunc

  func oATATTRIC_1_29.mHide()
    with this.Parent.oContained
      return (g_PERCAN <>'S' or Isalt())
    endwith
  endfunc

  func oATATTRIC_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oATATTRIC_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATATTRIC_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_ATCOMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_ATCOMRIC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oATATTRIC_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oATATTRIC_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_ATCOMRIC
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_ATATTRIC
     i_obj.ecpSave()
  endproc

  add object oATCODNOM_1_30 as StdField with uid="HOITNAUZKJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ATCODNOM", cQueryName = "ATCODNOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 39231149,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=117, Top=217, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_ATCODNOM"

  func oATCODNOM_1_30.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oATCODNOM_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oATCODNOM_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oATCODNOM_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oATCODNOM_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Elenco Nominativi",'',this.parent.oContained
  endproc
  proc oATCODNOM_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_ATCODNOM
     i_obj.ecpSave()
  endproc


  add object oDATIPRIG_1_31 as StdCombo with uid="SVQWWRTAHC",rtseq=29,rtrep=.f.,left=117,top=264,width=126,height=21;
    , HelpContextID = 40132221;
    , cFormVar="w_DATIPRIG",RowSource=""+"Non fatturabile,"+"Fatturabile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDATIPRIG_1_31.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oDATIPRIG_1_31.GetRadio()
    this.Parent.oContained.w_DATIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oDATIPRIG_1_31.SetRadio()
    this.Parent.oContained.w_DATIPRIG=trim(this.Parent.oContained.w_DATIPRIG)
    this.value = ;
      iif(this.Parent.oContained.w_DATIPRIG=='N',1,;
      iif(this.Parent.oContained.w_DATIPRIG=='D',2,;
      0))
  endfunc

  func oDATIPRIG_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oDATIPRIG_1_31.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oDATIPRI2_1_32 as StdCombo with uid="FRQXSPBWLJ",rtseq=30,rtrep=.f.,left=335,top=264,width=126,height=21;
    , HelpContextID = 40132200;
    , cFormVar="w_DATIPRI2",RowSource=""+"Senza nota spese,"+"Con nota spese", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDATIPRI2_1_32.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oDATIPRI2_1_32.GetRadio()
    this.Parent.oContained.w_DATIPRI2 = this.RadioValue()
    return .t.
  endfunc

  func oDATIPRI2_1_32.SetRadio()
    this.Parent.oContained.w_DATIPRI2=trim(this.Parent.oContained.w_DATIPRI2)
    this.value = ;
      iif(this.Parent.oContained.w_DATIPRI2=='N',1,;
      iif(this.Parent.oContained.w_DATIPRI2=='D',2,;
      0))
  endfunc

  func oDATIPRI2_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oDATIPRI2_1_32.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDataIniTimer_1_36 as StdField with uid="ZFOXPZZFPT",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DataIniTimer", cQueryName = "DataIniTimer",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 243745306,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=189, Top=26

  add object oDesPratica_1_44 as StdField with uid="QVGZWBBWIW",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DesPratica", cQueryName = "DesPratica",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 208807473,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=287, Top=112, InputMask=replicate('X',100)

  func oDesPratica_1_44.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc


  add object oBtn_1_57 as StdButton with uid="IFNDLMJVTH",left=335, top=294, width=48,height=45,;
    CpPicture="BMP\carica.ico", caption="", nPag=1;
    , ToolTipText = "Nuovo";
    , HelpContextID = 42156758;
    , caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_57.Click()
      this.parent.oContained.NotifyEvent("Nuovo")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_57.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Type("oparentobject .w_EVSERIAL")="C")
     endwith
    endif
  endfunc


  add object oBtn_1_59 as StdButton with uid="KHCAYSAWPW",left=385, top=294, width=48,height=45,;
    CpPicture="bmp\carica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per caricare dati analitica";
    , HelpContextID = 102261113;
    , TabStop=.f.,Caption='A\<nalitica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_59.Click()
      do GSAG_KDA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_59.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COAN <> 'S'  or  !Isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_60 as StdButton with uid="YDGGHBTAGM",left=435, top=294, width=48,height=45,;
    CpPicture="bmp\DM_agenda.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per caricare l'attivit� da completare";
    , HelpContextID = 229020106;
    , caption='\<Attivit�';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_60.Click()
      this.parent.oContained.NotifyEvent("DaComp")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_60.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Attivo='N' AND (.w_DurataOre+.w_DurataMin)>0 AND ((!EMPTY(.w_CodPratica) OR !Isalt()) AND !EMPTY(.w_CodSer) or !Isalt()))
      endwith
    endif
  endfunc

  func oBtn_1_60.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_GENPRE='S' OR NOT(.w_Attivo='N' AND (.w_DurataOre+.w_DurataMin)>0 AND ((!EMPTY(.w_CodPratica) OR !Isalt()) AND !EMPTY(.w_CodSer) or !Isalt())))
     endwith
    endif
  endfunc


  add object oBtn_1_61 as StdButton with uid="ESZNQMWTGW",left=485, top=294, width=48,height=45,;
    CpPicture="bmp\genera.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per completare la prestazione";
    , HelpContextID = 145941369;
    , caption='\<Completa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_61.Click()
      this.parent.oContained.NotifyEvent("Completa")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_61.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Attivo='N' AND  (.w_DurataOre+.w_DurataMin)>0 AND !(.w_DATIPRIG=='N' AND .w_DATIPRI2=='N' and .w_GENPRE<>'S') AND (!EMPTY(.w_CodPratica) OR !Isalt()) AND !EMPTY(.w_CodSer))
      endwith
    endif
  endfunc

  func oBtn_1_61.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(.w_Attivo='N' AND  (.w_DurataOre+.w_DurataMin)>0 AND !(.w_DATIPRIG=='N' AND .w_DATIPRI2=='N' and .w_GENPRE<>'S') AND (!EMPTY(.w_CodPratica) OR !Isalt()) AND !EMPTY(.w_CodSer)))
     endwith
    endif
  endfunc


  add object oBtn_1_62 as StdButton with uid="TKCMXZXILU",left=536, top=294, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193298502;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object Clock as cp_clock with uid="FZCXQEGJLK",left=454, top=7, width=121,height=52,;
    caption='Object',;
   bGlobalFont=.t.,;
    bShowSecond=.t.,FontName="Zucchetti Digital",Visible=.t.,FontSize=32,FontColor=48896,;
    cEvent = "StopTimer,InitTimer",;
    nPag=1;
    , HelpContextID = 172892186

  add object oDESCAU_1_90 as StdField with uid="KZEUJZOFPA",rtseq=72,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 194096586,;
   bGlobalFont=.t.,;
    Height=21, Width=335, Left=273, Top=86, InputMask=replicate('X',254)

  func oDESCAU_1_90.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCCDESPIA_1_103 as StdField with uid="ZJDVIETETW",rtseq=83,rtrep=.t.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 9396327,;
   bGlobalFont=.t.,;
    Height=21, Width=333, Left=274, Top=112, InputMask=replicate('X',40)

  func oCCDESPIA_1_103.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCCDESRIC_1_107 as StdField with uid="OQBJBQEOLA",rtseq=84,rtrep=.t.,;
    cFormVar = "w_CCDESRIC", cQueryName = "CCDESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42950761,;
   bGlobalFont=.t.,;
    Height=21, Width=333, Left=274, Top=165, InputMask=replicate('X',40)

  func oCCDESRIC_1_107.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oDESNOM_1_115 as StdField with uid="ODSRYIXGUJ",rtseq=89,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 44477898,;
   bGlobalFont=.t.,;
    Height=21, Width=333, Left=274, Top=217, InputMask=replicate('X',60)

  func oDESNOM_1_115.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oDATA_1_136 as StdField with uid="YATNTWGUYK",rtseq=106,rtrep=.f.,;
    cFormVar = "w_DATA", cQueryName = "DATA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data attribuita in fase di caricamento",;
    HelpContextID = 190602806,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=189, Top=57

  func oDATA_1_136.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oORA_1_137 as StdField with uid="JMSVOEFSNI",rtseq=107,rtrep=.f.,;
    cFormVar = "w_ORA", cQueryName = "ORA",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Ora attribuita in fase di caricamento",;
    HelpContextID = 186269670,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=273, Top=57, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORA_1_137.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oMINUTI_1_138 as StdField with uid="OOERJNFSYU",rtseq=108,rtrep=.f.,;
    cFormVar = "w_MINUTI", cQueryName = "MINUTI",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Minuti attribuiti in fase di caricamento",;
    HelpContextID = 105904442,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=301, Top=57, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINUTI_1_138.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oDESRES_1_155 as StdField with uid="GQULESBOVK",rtseq=118,rtrep=.f.,;
    cFormVar = "w_DESRES", cQueryName = "DESRES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 222473674,;
   bGlobalFont=.t.,;
    Height=21, Width=432, Left=175, Top=86, InputMask=replicate('X',100)

  func oDESRES_1_155.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc


  add object oBtn_1_156 as StdButton with uid="IRQDZWWWVS",left=137, top=6, width=48,height=45,;
    CpPicture="bmp\reset.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per azzerare il timer";
    , HelpContextID = 207423738;
    , caption='\<Azzera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_156.Click()
      this.parent.oContained.NotifyEvent("AzzeraTimer")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_156.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_Attivo='N')
      endwith
    endif
  endfunc

  func oBtn_1_156.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_Attivo='N'  or .w_DAGEST)
     endwith
    endif
  endfunc

  add object oStr_1_39 as StdString with uid="JMZTRKLLZU",Visible=.t., Left=189, Top=6,;
    Alignment=0, Width=28, Height=18,;
    Caption="Inizio"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="KMGLYDPVXE",Visible=.t., Left=348, Top=6,;
    Alignment=0, Width=20, Height=18,;
    Caption="Ore"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="RXZWLCDQGP",Visible=.t., Left=383, Top=6,;
    Alignment=0, Width=22, Height=18,;
    Caption="Min."  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="OEVLTSOEMQ",Visible=.t., Left=72, Top=114,;
    Alignment=1, Width=41, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="AUEJGKPLZP",Visible=.t., Left=45, Top=138,;
    Alignment=1, Width=68, Height=18,;
    Caption="Prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_76 as StdString with uid="FKQQIIZGHB",Visible=.t., Left=45, Top=191,;
    Alignment=1, Width=68, Height=18,;
    Caption="Descr. Agg.:"  ;
  , bGlobalFont=.t.

  func oStr_1_76.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_91 as StdString with uid="ICJMJMKTCW",Visible=.t., Left=49, Top=88,;
    Alignment=1, Width=64, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_102 as StdString with uid="ZERTRSSKLC",Visible=.t., Left=45, Top=265,;
    Alignment=1, Width=68, Height=18,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_104 as StdString with uid="PQYBVXIDQQ",Visible=.t., Left=22, Top=114,;
    Alignment=1, Width=91, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_104.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_105 as StdString with uid="CCZQHWQSDD",Visible=.t., Left=2, Top=138,;
    Alignment=1, Width=111, Height=18,;
    Caption="Comm. costi:"  ;
  , bGlobalFont=.t.

  func oStr_1_105.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_106 as StdString with uid="PFALGHYVEF",Visible=.t., Left=394, Top=139,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_106.mHide()
    with this.Parent.oContained
      return (g_PERCAN <>'S' or Isalt())
    endwith
  endfunc

  add object oStr_1_108 as StdString with uid="NDNECIPGFN",Visible=.t., Left=9, Top=165,;
    Alignment=1, Width=104, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_1_108.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_109 as StdString with uid="HTOLFADPTS",Visible=.t., Left=5, Top=191,;
    Alignment=1, Width=108, Height=18,;
    Caption="Comm. ricavi:"  ;
  , bGlobalFont=.t.

  func oStr_1_109.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_110 as StdString with uid="THZJZYTFBJ",Visible=.t., Left=394, Top=191,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_110.mHide()
    with this.Parent.oContained
      return (g_PERCAN <>'S' or Isalt())
    endwith
  endfunc

  add object oStr_1_116 as StdString with uid="WBYXVZPWLF",Visible=.t., Left=5, Top=217,;
    Alignment=1, Width=108, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  func oStr_1_116.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_124 as StdString with uid="IVSWOATHEM",Visible=.t., Left=263, Top=265,;
    Alignment=1, Width=68, Height=18,;
    Caption="Nota spese:"  ;
  , bGlobalFont=.t.

  func oStr_1_124.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_139 as StdString with uid="ONCQIKWXXZ",Visible=.t., Left=85, Top=59,;
    Alignment=1, Width=100, Height=18,;
    Caption="Data/ora attribuita:"  ;
  , bGlobalFont=.t.

  func oStr_1_139.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_140 as StdString with uid="ZEVWDRLIWV",Visible=.t., Left=267, Top=58,;
    Alignment=0, Width=5, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_140.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_141 as StdString with uid="MTWFKDAHXL",Visible=.t., Left=295, Top=59,;
    Alignment=0, Width=5, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  func oStr_1_141.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oStr_1_152 as StdString with uid="BYMSOKZIRI",Visible=.t., Left=32, Top=87,;
    Alignment=1, Width=81, Height=18,;
    Caption="Responsabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_152.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_ktm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
