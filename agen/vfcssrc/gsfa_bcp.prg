* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bcp                                                        *
*              Collega indice documentale a pratica                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-13                                                      *
* Last revis.: 2010-04-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bcp",oParentObject)
return(i_retval)

define class tgsfa_bcp as StdBatch
  * --- Local variables
  w_FILETMP = space(254)
  w_ZOOM = .NULL.
  w_STRFILE = space(0)
  w_SERINDICE = space(20)
  w_IDMODALL = space(1)
  w_IDPATALL = space(254)
  w_IDDATRAG = ctod("  /  /  ")
  w_IDCLADOC = space(15)
  w_IDNOMFIL = space(100)
  w_IDORIGIN = space(100)
  w_IDORIFIL = space(100)
  w_TMPC = space(100)
  w_TmpPat = space(10)
  w_PATSTD = space(100)
  w_CODPRA = space(15)
  w_NOMFILE = space(254)
  * --- WorkFile variables
  PROMINDI_idx=0
  PROMCLAS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia/Sposta Indici su pratica
    * --- Variabili per GSUT_BBI
    this.w_FILETMP = ADDBS(TEMPADHOC())+SYS(2015)+".TXT"
    this.w_ZOOM = This.oParentObject.GSFA_ACE.w_ZOOMIND
    this.w_CODPRA = This.oParentObject.GSFA_ACE.w_EVCODPRA
    this.w_STRFILE = ""
    SELECT * FROM (this.w_ZOOM.cCursor) WHERE xChk=1 INTO CURSOR __TMPIND__
    SCAN
    this.w_SERINDICE = __TMPIND__.IDSERIAL
    * --- Read from PROMINDI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IDCLADOC,IDNOMFIL,IDORIGIN,IDORIFIL,IDMODALL,IDPATALL,IDDATRAG"+;
        " from "+i_cTable+" PROMINDI where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.w_SERINDICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IDCLADOC,IDNOMFIL,IDORIGIN,IDORIFIL,IDMODALL,IDPATALL,IDDATRAG;
        from (i_cTable) where;
            IDSERIAL = this.w_SERINDICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_IDCLADOC = NVL(cp_ToDate(_read_.IDCLADOC),cp_NullValue(_read_.IDCLADOC))
      this.w_IDNOMFIL = NVL(cp_ToDate(_read_.IDNOMFIL),cp_NullValue(_read_.IDNOMFIL))
      this.w_IDORIGIN = NVL(cp_ToDate(_read_.IDORIGIN),cp_NullValue(_read_.IDORIGIN))
      this.w_IDORIFIL = NVL(cp_ToDate(_read_.IDORIFIL),cp_NullValue(_read_.IDORIFIL))
      this.w_IDMODALL = NVL(cp_ToDate(_read_.IDMODALL),cp_NullValue(_read_.IDMODALL))
      this.w_IDPATALL = NVL(cp_ToDate(_read_.IDPATALL),cp_NullValue(_read_.IDPATALL))
      this.w_IDDATRAG = NVL(cp_ToDate(_read_.IDDATRAG),cp_NullValue(_read_.IDDATRAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from PROMCLAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PROMCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CDPATSTD"+;
        " from "+i_cTable+" PROMCLAS where ";
            +"CDCODCLA = "+cp_ToStrODBC(this.w_IDCLADOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CDPATSTD;
        from (i_cTable) where;
            CDCODCLA = this.w_IDCLADOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PATSTD = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_NOMFILE = GSUT_BBI(This, "PATHALLEGATO") 
    if Cp_fileexist(Alltrim(this.w_NOMFILE))
      this.w_STRFILE = this.w_STRFILE + Alltrim(this.w_NOMFILE) + CHR(13)
    endif
    SELECT("__TMPIND__")
    ENDSCAN
    USE IN SELECT("__TMPIND__")
    if !Empty(this.w_STRFILE)
      STRTOFILE(this.w_STRFILE, this.w_FILETMP)
      GSUT_BSO(this,"", this.w_FILETMP)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      DELETE FILE (this.w_FILETMP)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PROMINDI'
    this.cWorkTables[2]='PROMCLAS'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
