* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kex                                                        *
*              Sincronizza con Outlook                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-22                                                      *
* Last revis.: 2015-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kex",oParentObject))

* --- Class definition
define class tgsag_kex as StdForm
  Top    = 53
  Left   = 101

  * --- Standard Properties
  Width  = 528
  Height = 308
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-02-27"
  HelpContextID=65677161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Constant Properties
  _IDX = 0
  CAUMATTI_IDX = 0
  DIPENDEN_IDX = 0
  cPrg = "gsag_kex"
  cComment = "Sincronizza con Outlook"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TipoOper = space(10)
  w_Causale = space(20)
  o_Causale = space(20)
  w_DesCau = space(254)
  w_ImportaApp = space(1)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_OREFIN = space(2)
  o_OREFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_CancApp = space(1)
  w_DATA_INI = ctot('')
  w_DATA_FIN = ctot('')
  w_GestDataFin = space(1)
  o_GestDataFin = space(1)
  w_PATIPRIS = space(1)
  o_PATIPRIS = space(1)
  w_CODPART_Outl = space(5)
  o_CODPART_Outl = space(5)
  w_DENOM_PART = space(48)
  w_ATTSTU = space(1)
  w_ATTGENER = space(1)
  w_UDIENZE = space(1)
  w_SESSTEL = space(1)
  w_ASSENZE = space(1)
  w_DAINSPRE = space(1)
  w_MEMO = space(1)
  w_DAFARE = space(1)
  w_COGNPART = space(40)
  w_NOMEPART = space(40)
  w_DESCRPART = space(40)
  w_DPTIPRIS = space(1)
  w_ATTFUOSTU = space(1)
  w_STATO = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_CODPARTApp = space(5)
  w_TIPPartApp = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kexPag1","gsag_kex",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oImportaApp_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CAUMATTI'
    this.cWorkTables[2]='DIPENDEN'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gsag_bex(this,.w_TipoOper)
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TipoOper=space(10)
      .w_Causale=space(20)
      .w_DesCau=space(254)
      .w_ImportaApp=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_DATFIN=ctod("  /  /  ")
      .w_OREFIN=space(2)
      .w_MINFIN=space(2)
      .w_CancApp=space(1)
      .w_DATA_INI=ctot("")
      .w_DATA_FIN=ctot("")
      .w_GestDataFin=space(1)
      .w_PATIPRIS=space(1)
      .w_CODPART_Outl=space(5)
      .w_DENOM_PART=space(48)
      .w_ATTSTU=space(1)
      .w_ATTGENER=space(1)
      .w_UDIENZE=space(1)
      .w_SESSTEL=space(1)
      .w_ASSENZE=space(1)
      .w_DAINSPRE=space(1)
      .w_MEMO=space(1)
      .w_DAFARE=space(1)
      .w_COGNPART=space(40)
      .w_NOMEPART=space(40)
      .w_DESCRPART=space(40)
      .w_DPTIPRIS=space(1)
      .w_ATTFUOSTU=space(1)
      .w_STATO=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_CODPARTApp=space(5)
      .w_TIPPartApp=space(1)
        .w_TipoOper = IIF(VARTYPE(this.oParentObject)='C',ALLTRIM(this.oParentObject),'SINCRO')
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_Causale))
          .link_1_3('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_ImportaApp = 'N'
        .w_DATINI = i_DatSys
        .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
          .DoRTCalc(8,8,.f.)
        .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (empty(.w_DATFIN) AND .w_OREFIN="23" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        .w_CancApp = 'N'
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        .w_GestDataFin = '1'
        .w_PATIPRIS = 'P'
        .w_CODPART_Outl = readdipend(i_CodUte, 'C')
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODPART_Outl))
          .link_1_25('Full')
        endif
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        .w_ATTSTU = 'S'
        .w_ATTGENER = ' '
        .w_UDIENZE = 'U'
        .w_SESSTEL = ' '
        .w_ASSENZE = ' '
        .w_DAINSPRE = ' '
        .w_MEMO = ' '
        .w_DAFARE = ' '
          .DoRTCalc(26,29,.f.)
        .w_ATTFUOSTU = ' '
        .w_STATO = IIF(.w_TipoOper='SINCRO','N','B')
        .w_OB_TEST = i_INIDAT
        .w_CODPARTApp = .w_CODPART_Outl
        .w_TIPPartApp = .w_PATIPRIS
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TipoOper = IIF(VARTYPE(this.oParentObject)='C',ALLTRIM(this.oParentObject),'SINCRO')
        if .o_GestDataFin<>.w_GestDataFin
          .Calculate_DBAKUTKFDI()
        endif
        if .o_Causale<>.w_Causale
          .link_1_3('Full')
        endif
        .DoRTCalc(3,5,.t.)
        if .o_OREINI<>.w_OREINI
            .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        endif
        if .o_MININI<>.w_MININI
            .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        endif
        .DoRTCalc(8,8,.t.)
        if .o_OREFIN<>.w_OREFIN.or. .o_DATFIN<>.w_DATFIN
            .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        endif
        if .o_MINFIN<>.w_MINFIN.or. .o_DATFIN<>.w_DATFIN
            .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (empty(.w_DATFIN) AND .w_OREFIN="23" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        endif
        .DoRTCalc(11,11,.t.)
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI.or. .o_GestDataFin<>.w_GestDataFin
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN<>.w_DATFIN.or. .o_OREFIN<>.w_OREFIN.or. .o_MINFIN<>.w_MINFIN.or. .o_GestDataFin<>.w_GestDataFin
            .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        .DoRTCalc(14,15,.t.)
        if .o_PATIPRIS<>.w_PATIPRIS
          .link_1_25('Full')
        endif
        if .o_CODPART_Outl<>.w_CODPART_Outl
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        .DoRTCalc(18,32,.t.)
            .w_CODPARTApp = .w_CODPART_Outl
            .w_TIPPartApp = .w_PATIPRIS
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_DBAKUTKFDI()
    with this
          * --- Gestisce la data finale
          gsag_bex(this;
              ,'DATAFINE';
             )
    endwith
  endproc
  proc Calculate_KFTDSBDHGA()
    with this
          * --- Avvalora la causale di defaul
          gsag_bex(this;
              ,'INITVAR';
             )
    endwith
  endproc
  proc Calculate_AXROSJSFQU()
    with this
          * --- Cambia caption form
          .cComment = IIF(.w_TipoOper='SINCRO',.cComment,AH_MSGFORMAT('Cancellazione attivit� in Outlook'))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOREINI_1_9.enabled = this.oPgFrm.Page1.oPag.oOREINI_1_9.mCond()
    this.oPgFrm.Page1.oPag.oMININI_1_10.enabled = this.oPgFrm.Page1.oPag.oMININI_1_10.mCond()
    this.oPgFrm.Page1.oPag.oOREFIN_1_13.enabled = this.oPgFrm.Page1.oPag.oOREFIN_1_13.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_14.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_14.mCond()
    this.oPgFrm.Page1.oPag.oPATIPRIS_1_24.enabled = this.oPgFrm.Page1.oPag.oPATIPRIS_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oImportaApp_1_5.visible=!this.oPgFrm.Page1.oPag.oImportaApp_1_5.mHide()
    this.oPgFrm.Page1.oPag.oUDIENZE_1_29.visible=!this.oPgFrm.Page1.oPag.oUDIENZE_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oSTATO_1_43.visible=!this.oPgFrm.Page1.oPag.oSTATO_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_DBAKUTKFDI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_KFTDSBDHGA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_AXROSJSFQU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=Causale
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_Causale) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_Causale)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_Causale);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_Causale)
            select CACODICE,CADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_Causale = NVL(_Link_.CACODICE,space(20))
      this.w_DesCau = NVL(_Link_.CADESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_Causale = space(20)
      endif
      this.w_DesCau = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_Causale Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPART_Outl
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPART_Outl) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPART_Outl)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_CODPART_Outl))
          select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPART_Outl)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODPART_Outl)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODPART_Outl)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODPART_Outl)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODPART_Outl)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_CODPART_Outl)+"%");

            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_CODPART_Outl)+"%");

            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPART_Outl) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oCODPART_Outl_1_25'),i_cWhere,'',"Partecipanti",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPART_Outl)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPART_Outl);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODPART_Outl)
            select DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPART_Outl = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODPART_Outl = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
      this.w_DPTIPRIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CODPART_Outl) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODPART_Outl = space(5)
        this.w_COGNPART = space(40)
        this.w_NOMEPART = space(40)
        this.w_DESCRPART = space(40)
        this.w_DPTIPRIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPART_Outl Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oImportaApp_1_5.RadioValue()==this.w_ImportaApp)
      this.oPgFrm.Page1.oPag.oImportaApp_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_8.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_8.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINI_1_9.value==this.w_OREINI)
      this.oPgFrm.Page1.oPag.oOREINI_1_9.value=this.w_OREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_10.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_10.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_12.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_12.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOREFIN_1_13.value==this.w_OREFIN)
      this.oPgFrm.Page1.oPag.oOREFIN_1_13.value=this.w_OREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_14.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_14.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGestDataFin_1_23.RadioValue()==this.w_GestDataFin)
      this.oPgFrm.Page1.oPag.oGestDataFin_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATIPRIS_1_24.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page1.oPag.oPATIPRIS_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPART_Outl_1_25.value==this.w_CODPART_Outl)
      this.oPgFrm.Page1.oPag.oCODPART_Outl_1_25.value=this.w_CODPART_Outl
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_PART_1_26.value==this.w_DENOM_PART)
      this.oPgFrm.Page1.oPag.oDENOM_PART_1_26.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page1.oPag.oATTSTU_1_27.RadioValue()==this.w_ATTSTU)
      this.oPgFrm.Page1.oPag.oATTSTU_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATTGENER_1_28.RadioValue()==this.w_ATTGENER)
      this.oPgFrm.Page1.oPag.oATTGENER_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUDIENZE_1_29.RadioValue()==this.w_UDIENZE)
      this.oPgFrm.Page1.oPag.oUDIENZE_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSESSTEL_1_30.RadioValue()==this.w_SESSTEL)
      this.oPgFrm.Page1.oPag.oSESSTEL_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASSENZE_1_31.RadioValue()==this.w_ASSENZE)
      this.oPgFrm.Page1.oPag.oASSENZE_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMEMO_1_33.RadioValue()==this.w_MEMO)
      this.oPgFrm.Page1.oPag.oMEMO_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAFARE_1_34.RadioValue()==this.w_DAFARE)
      this.oPgFrm.Page1.oPag.oDAFARE_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_43.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_43.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(VAL(.w_OREINI) < 24)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(VAL(.w_OREFIN) < 24)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(EMPTY(.w_CODPART_Outl) OR EMPTY(.w_PATIPRIS) OR .w_PATIPRIS=.w_DPTIPRIS)  and not(empty(.w_CODPART_Outl))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPART_Outl_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_Causale = this.w_Causale
    this.o_DATINI = this.w_DATINI
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_DATFIN = this.w_DATFIN
    this.o_OREFIN = this.w_OREFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_GestDataFin = this.w_GestDataFin
    this.o_PATIPRIS = this.w_PATIPRIS
    this.o_CODPART_Outl = this.w_CODPART_Outl
    return

enddefine

* --- Define pages as container
define class tgsag_kexPag1 as StdContainer
  Width  = 524
  height = 308
  stdWidth  = 524
  stdheight = 308
  resizeXpos=399
  resizeYpos=200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oImportaApp_1_5 as StdCheck with uid="OBMVWJZRMK",rtseq=4,rtrep=.f.,left=33, top=261, caption="Importa gli appuntamenti caricati in Outlook",;
    ToolTipText = "Se attivo, importa gli appuntamenti caricati in Outlook",;
    HelpContextID = 128762311,;
    cFormVar="w_ImportaApp", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oImportaApp_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oImportaApp_1_5.GetRadio()
    this.Parent.oContained.w_ImportaApp = this.RadioValue()
    return .t.
  endfunc

  func oImportaApp_1_5.SetRadio()
    this.Parent.oContained.w_ImportaApp=trim(this.Parent.oContained.w_ImportaApp)
    this.value = ;
      iif(this.Parent.oContained.w_ImportaApp=='S',1,;
      0)
  endfunc

  func oImportaApp_1_5.mHide()
    with this.Parent.oContained
      return (!.w_TipoOper='SINCRO')
    endwith
  endfunc

  add object oDATINI_1_8 as StdField with uid="VAJTUNAFFS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 172252726,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=103, Top=14

  proc oDATINI_1_8.mAfter
    with this.Parent.oContained
      .w_DATFIN=IIF(.w_DATINI<.w_DATFIN,.w_DATFIN,.w_DATINI)
    endwith
  endproc

  add object oOREINI_1_9 as StdField with uid="WOKDXXPUIP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_OREINI", cQueryName = "OREINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di inizio selezione",;
    HelpContextID = 172195814,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=257, Top=14, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREINI_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oOREINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_10 as StdField with uid="QMRKNBACQP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio selezione",;
    HelpContextID = 172230342,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=295, Top=14, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oMININI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_12 as StdField with uid="VRHGJGBHQK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 250699318,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=103, Top=38

  func oDATFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oOREFIN_1_13 as StdField with uid="VKJCJSQZFK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_OREFIN", cQueryName = "OREFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di fine selezione",;
    HelpContextID = 250642406,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=257, Top=38, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREFIN_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oOREFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_14 as StdField with uid="GIWPXXGMLC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di fine selezione",;
    HelpContextID = 250676934,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=295, Top=38, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oMINFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc


  add object oGestDataFin_1_23 as StdCombo with uid="GTYLCCGUSH",rtseq=14,rtrep=.f.,left=390,top=38,width=118,height=21;
    , ToolTipText = "Avvalora la data di fine selezione";
    , HelpContextID = 237453273;
    , cFormVar="w_GestDataFin",RowSource=""+"Un mese,"+"Due mesi,"+"Tre mesi,"+"Sei mesi,"+"Un anno,"+"Oggi,"+"Vuoto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGestDataFin_1_23.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'6',;
    iif(this.value =5,'A',;
    iif(this.value =6,'O',;
    iif(this.value =7,'V',;
    space(1)))))))))
  endfunc
  func oGestDataFin_1_23.GetRadio()
    this.Parent.oContained.w_GestDataFin = this.RadioValue()
    return .t.
  endfunc

  func oGestDataFin_1_23.SetRadio()
    this.Parent.oContained.w_GestDataFin=trim(this.Parent.oContained.w_GestDataFin)
    this.value = ;
      iif(this.Parent.oContained.w_GestDataFin=='1',1,;
      iif(this.Parent.oContained.w_GestDataFin=='2',2,;
      iif(this.Parent.oContained.w_GestDataFin=='3',3,;
      iif(this.Parent.oContained.w_GestDataFin=='6',4,;
      iif(this.Parent.oContained.w_GestDataFin=='A',5,;
      iif(this.Parent.oContained.w_GestDataFin=='O',6,;
      iif(this.Parent.oContained.w_GestDataFin=='V',7,;
      0)))))))
  endfunc


  add object oPATIPRIS_1_24 as StdCombo with uid="FEFTOWFWCM",rtseq=15,rtrep=.f.,left=103,top=72,width=76,height=21;
    , HelpContextID = 211525815;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Gruppi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPATIPRIS_1_24.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'G',;
    space(1))))
  endfunc
  func oPATIPRIS_1_24.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_1_24.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='G',2,;
      0))
  endfunc

  func oPATIPRIS_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  add object oCODPART_Outl_1_25 as StdField with uid="QJYWRUDDRU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODPART_Outl", cQueryName = "CODPART_Outl",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice partecipante",;
    HelpContextID = 219273611,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=72, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_CODPART_Outl"

  func oCODPART_Outl_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPART_Outl_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPART_Outl_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIPENDEN','*','DPCODICE',cp_AbsName(this.parent,'oCODPART_Outl_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Partecipanti",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oDENOM_PART_1_26 as StdField with uid="SKXCKTCAOH",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(48), bMultilanguage =  .f.,;
    HelpContextID = 3824535,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=257, Top=72, InputMask=replicate('X',48)

  add object oATTSTU_1_27 as StdCheck with uid="FUHPBAIQSC",rtseq=18,rtrep=.f.,left=33, top=134, caption="Appuntamenti",;
    ToolTipText = "Se attivo: visualizza gli appuntamenti",;
    HelpContextID = 112095494,;
    cFormVar="w_ATTSTU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATTSTU_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oATTSTU_1_27.GetRadio()
    this.Parent.oContained.w_ATTSTU = this.RadioValue()
    return .t.
  endfunc

  func oATTSTU_1_27.SetRadio()
    this.Parent.oContained.w_ATTSTU=trim(this.Parent.oContained.w_ATTSTU)
    this.value = ;
      iif(this.Parent.oContained.w_ATTSTU=='S',1,;
      0)
  endfunc

  add object oATTGENER_1_28 as StdCheck with uid="KLBNRLNCJC",rtseq=19,rtrep=.f.,left=33, top=163, caption="Attivit� generiche",;
    ToolTipText = "Se attivo: visualizza le attivit� generiche",;
    HelpContextID = 21860008,;
    cFormVar="w_ATTGENER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATTGENER_1_28.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oATTGENER_1_28.GetRadio()
    this.Parent.oContained.w_ATTGENER = this.RadioValue()
    return .t.
  endfunc

  func oATTGENER_1_28.SetRadio()
    this.Parent.oContained.w_ATTGENER=trim(this.Parent.oContained.w_ATTGENER)
    this.value = ;
      iif(this.Parent.oContained.w_ATTGENER=='G',1,;
      0)
  endfunc

  add object oUDIENZE_1_29 as StdCheck with uid="HBQKVBXHMX",rtseq=20,rtrep=.f.,left=33, top=192, caption="Udienze",;
    ToolTipText = "Se attivo: visualizza le udienze",;
    HelpContextID = 79711674,;
    cFormVar="w_UDIENZE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUDIENZE_1_29.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oUDIENZE_1_29.GetRadio()
    this.Parent.oContained.w_UDIENZE = this.RadioValue()
    return .t.
  endfunc

  func oUDIENZE_1_29.SetRadio()
    this.Parent.oContained.w_UDIENZE=trim(this.Parent.oContained.w_UDIENZE)
    this.value = ;
      iif(this.Parent.oContained.w_UDIENZE=='U',1,;
      0)
  endfunc

  func oUDIENZE_1_29.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oSESSTEL_1_30 as StdCheck with uid="KYMNVXZYIW",rtseq=21,rtrep=.f.,left=199, top=134, caption="Sessioni telefoniche",;
    ToolTipText = "Se attivo: visualizza le sessioni telefoniche",;
    HelpContextID = 156347610,;
    cFormVar="w_SESSTEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSESSTEL_1_30.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oSESSTEL_1_30.GetRadio()
    this.Parent.oContained.w_SESSTEL = this.RadioValue()
    return .t.
  endfunc

  func oSESSTEL_1_30.SetRadio()
    this.Parent.oContained.w_SESSTEL=trim(this.Parent.oContained.w_SESSTEL)
    this.value = ;
      iif(this.Parent.oContained.w_SESSTEL=='T',1,;
      0)
  endfunc

  add object oASSENZE_1_31 as StdCheck with uid="RPXCBGRCCL",rtseq=22,rtrep=.f.,left=199, top=163, caption="Assenze",;
    ToolTipText = "Se attivo: visualizza le assenze",;
    HelpContextID = 79667194,;
    cFormVar="w_ASSENZE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASSENZE_1_31.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oASSENZE_1_31.GetRadio()
    this.Parent.oContained.w_ASSENZE = this.RadioValue()
    return .t.
  endfunc

  func oASSENZE_1_31.SetRadio()
    this.Parent.oContained.w_ASSENZE=trim(this.Parent.oContained.w_ASSENZE)
    this.value = ;
      iif(this.Parent.oContained.w_ASSENZE=='A',1,;
      0)
  endfunc

  add object oMEMO_1_33 as StdCheck with uid="ELRSHFYRWO",rtseq=24,rtrep=.f.,left=365, top=134, caption="Note",;
    ToolTipText = "Se attivo: visualizza le note",;
    HelpContextID = 60165434,;
    cFormVar="w_MEMO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMEMO_1_33.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oMEMO_1_33.GetRadio()
    this.Parent.oContained.w_MEMO = this.RadioValue()
    return .t.
  endfunc

  func oMEMO_1_33.SetRadio()
    this.Parent.oContained.w_MEMO=trim(this.Parent.oContained.w_MEMO)
    this.value = ;
      iif(this.Parent.oContained.w_MEMO=='M',1,;
      0)
  endfunc

  add object oDAFARE_1_34 as StdCheck with uid="JYTLCSHGGN",rtseq=25,rtrep=.f.,left=365, top=163, caption="Cose da fare",;
    ToolTipText = "Se attivo: visualizza le cose da fare",;
    HelpContextID = 108756534,;
    cFormVar="w_DAFARE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAFARE_1_34.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oDAFARE_1_34.GetRadio()
    this.Parent.oContained.w_DAFARE = this.RadioValue()
    return .t.
  endfunc

  func oDAFARE_1_34.SetRadio()
    this.Parent.oContained.w_DAFARE=trim(this.Parent.oContained.w_DAFARE)
    this.value = ;
      iif(this.Parent.oContained.w_DAFARE=='D',1,;
      0)
  endfunc


  add object oBtn_1_36 as StdButton with uid="TKCMXZXILU",left=405, top=256, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 65648410;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        gsag_bex(this.Parent.oContained,.w_TipoOper)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_38 as StdButton with uid="ISDICPRYHO",left=460, top=256, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 58359738;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oSTATO_1_43 as StdCombo with uid="EFCXIVUXIZ",rtseq=31,rtrep=.f.,left=194,top=200,width=181,height=21;
    , ToolTipText = "Stato";
    , HelpContextID = 245480922;
    , cFormVar="w_STATO",RowSource=""+"Non evasa,"+"Da svolgere,"+"In corso,"+"Provvisoria,"+"Evasa,"+"Completata,"+"Evasa o completata,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_43.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'T',;
    iif(this.value =5,'F',;
    iif(this.value =6,'P',;
    iif(this.value =7,'B',;
    iif(this.value =8,'K',;
    space(1))))))))))
  endfunc
  func oSTATO_1_43.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_43.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='N',1,;
      iif(this.Parent.oContained.w_STATO=='D',2,;
      iif(this.Parent.oContained.w_STATO=='I',3,;
      iif(this.Parent.oContained.w_STATO=='T',4,;
      iif(this.Parent.oContained.w_STATO=='F',5,;
      iif(this.Parent.oContained.w_STATO=='P',6,;
      iif(this.Parent.oContained.w_STATO=='B',7,;
      iif(this.Parent.oContained.w_STATO=='K',8,;
      0))))))))
  endfunc

  func oSTATO_1_43.mHide()
    with this.Parent.oContained
      return (.w_TipoOper='SINCRO')
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="KLEZMLKSOB",Visible=.t., Left=46, Top=14,;
    Alignment=1, Width=50, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="JAHWAWKLCQ",Visible=.t., Left=72, Top=38,;
    Alignment=1, Width=24, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="EWZSPAOTVW",Visible=.t., Left=193, Top=14,;
    Alignment=1, Width=59, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="CTAHWBKCXZ",Visible=.t., Left=193, Top=38,;
    Alignment=1, Width=59, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="DAAABZBARA",Visible=.t., Left=284, Top=14,;
    Alignment=1, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="BTYOAEACQT",Visible=.t., Left=284, Top=38,;
    Alignment=1, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="ZGENJNNRPH",Visible=.t., Left=6, Top=72,;
    Alignment=1, Width=92, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="WOGMEQHMJH",Visible=.t., Left=265, Top=312,;
    Alignment=0, Width=582, Height=18,;
    Caption="ATTENZIONE!!! La gestione di Data_Ini e Data_Fin deve essere successiva in sequenza rispetto al batch!"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="AIHRBIXPUP",Visible=.t., Left=30, Top=237,;
    Alignment=0, Width=298, Height=18,;
    Caption="Opzioni per import e sincronizzazione con Outlook"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (!.w_TipoOper='SINCRO')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="JGXETUZHHV",Visible=.t., Left=16, Top=106,;
    Alignment=0, Width=337, Height=18,;
    Caption="Opzioni per esportazione e sincronizzazione con Outlook"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (!.w_TipoOper='SINCRO')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kex','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
