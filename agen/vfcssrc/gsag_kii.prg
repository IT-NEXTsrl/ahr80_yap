* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kii                                                        *
*              Gestione attivit� importate                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-02-22                                                      *
* Last revis.: 2014-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kii",oParentObject))

* --- Class definition
define class tgsag_kii as StdForm
  Top    = 18
  Left   = 79

  * --- Standard Properties
  Width  = 656
  Height = 509+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-12"
  HelpContextID=1431703
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  CENCOST_IDX = 0
  OFF_NOMI_IDX = 0
  PAR_AGEN_IDX = 0
  VOC_COST_IDX = 0
  ATTIVITA_IDX = 0
  CAUMATTI_IDX = 0
  PRO_EXPO_IDX = 0
  cPrg = "gsag_kii"
  cComment = "Gestione attivit� importate"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SERPRO = space(10)
  w_PRCODUTE = space(5)
  w_PAR_AGEN = space(5)
  w_FLGCA1 = space(1)
  o_FLGCA1 = space(1)
  w_STATO = space(1)
  w_FLGCA2 = space(1)
  o_FLGCA2 = space(1)
  w_FLGCAT = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_ORARIF = space(2)
  w_MINRIF = space(2)
  w_DPTIPRIS = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_PACHKFES = space(1)
  w_CODCOS = space(5)
  w_VOCOBSO = ctod('  /  /  ')
  w_TIPVOR = space(1)
  w_InviaMail = .F.
  w_OB_TEST = ctod('  /  /  ')
  w_TIPOEVENTO = space(10)
  w_PACALDAT = space(1)
  w_PACHKATT = 0
  w_PRDESCRI = space(50)
  w_UID = space(20)
  o_UID = space(20)
  w_COSERATT = space(20)
  w_CODEVE = space(150)
  w_DATINI = ctot('')
  w_DATFIN = ctot('')
  w_OGGETTO = space(254)
  w_DESCRIZ = space(254)
  w_DESCRI = space(0)
  w_ZOOMIMPO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kiiPag1","gsag_kii",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Attivit� importate")
      .Pages(2).addobject("oPag","tgsag_kiiPag2","gsag_kii",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Ulteriori selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSERPRO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMIMPO = this.oPgFrm.Pages(1).oPag.ZOOMIMPO
    DoDefault()
    proc Destroy()
      this.w_ZOOMIMPO = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CENCOST'
    this.cWorkTables[3]='OFF_NOMI'
    this.cWorkTables[4]='PAR_AGEN'
    this.cWorkTables[5]='VOC_COST'
    this.cWorkTables[6]='ATTIVITA'
    this.cWorkTables[7]='CAUMATTI'
    this.cWorkTables[8]='PRO_EXPO'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAG_BII with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SERPRO=space(10)
      .w_PRCODUTE=space(5)
      .w_PAR_AGEN=space(5)
      .w_FLGCA1=space(1)
      .w_STATO=space(1)
      .w_FLGCA2=space(1)
      .w_FLGCAT=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_ORARIF=space(2)
      .w_MINRIF=space(2)
      .w_DPTIPRIS=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_PACHKFES=space(1)
      .w_CODCOS=space(5)
      .w_VOCOBSO=ctod("  /  /  ")
      .w_TIPVOR=space(1)
      .w_InviaMail=.f.
      .w_OB_TEST=ctod("  /  /  ")
      .w_TIPOEVENTO=space(10)
      .w_PACALDAT=space(1)
      .w_PACHKATT=0
      .w_PRDESCRI=space(50)
      .w_UID=space(20)
      .w_COSERATT=space(20)
      .w_CODEVE=space(150)
      .w_DATINI=ctot("")
      .w_DATFIN=ctot("")
      .w_OGGETTO=space(254)
      .w_DESCRIZ=space(254)
      .w_DESCRI=space(0)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_SERPRO))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_PAR_AGEN = i_CodAzi
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PAR_AGEN))
          .link_1_3('Full')
        endif
        .w_FLGCA1 = 'N'
        .w_STATO = ' '
        .w_FLGCA2 = 'O'
        .w_FLGCAT = iif(isalt(),.w_FLGCA2,.w_FLGCA1)
      .oPgFrm.Page1.oPag.ZOOMIMPO.Calculate()
        .w_OBTEST = i_datsys
        .w_ORARIF = '00'
        .w_MINRIF = '00'
        .w_DPTIPRIS = 'Q'
          .DoRTCalc(12,17,.f.)
        .w_OB_TEST = i_datsys
          .DoRTCalc(19,22,.f.)
        .w_UID = .w_ZOOMIMPO.GETVAR('PICODUID')
        .w_COSERATT = .w_ZOOMIMPO.GETVAR('COSERATT')
          .DoRTCalc(25,29,.f.)
        .w_DESCRI = .w_ZOOMIMPO.GETVAR('PIDESCRI')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,6,.t.)
        if .o_FLGCA2<>.w_FLGCA2.or. .o_FLGCA1<>.w_FLGCA1
            .w_FLGCAT = iif(isalt(),.w_FLGCA2,.w_FLGCA1)
        endif
        .oPgFrm.Page1.oPag.ZOOMIMPO.Calculate()
        .DoRTCalc(8,22,.t.)
            .w_UID = .w_ZOOMIMPO.GETVAR('PICODUID')
            .w_COSERATT = .w_ZOOMIMPO.GETVAR('COSERATT')
        .DoRTCalc(25,29,.t.)
        if .o_UID<>.w_UID
            .w_DESCRI = .w_ZOOMIMPO.GETVAR('PIDESCRI')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMIMPO.Calculate()
    endwith
  return

  proc Calculate_PCCPPBWXPE()
    with this
          * --- Apre anagrafica eventi
          .a = IIF(Not Empty(.w_COSERATT),opengest('A','GSAG_AAT','ATSERIAL',.w_COSERATT),'')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLGCA1_1_4.visible=!this.oPgFrm.Page1.oPag.oFLGCA1_1_4.mHide()
    this.oPgFrm.Page1.oPag.oFLGCA2_1_6.visible=!this.oPgFrm.Page1.oPag.oFLGCA2_1_6.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_15.visible=!this.oPgFrm.Page1.oPag.oBtn_1_15.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_33.visible=!this.oPgFrm.Page1.oPag.oBtn_1_33.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_35.visible=!this.oPgFrm.Page1.oPag.oBtn_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_37.visible=!this.oPgFrm.Page1.oPag.oBtn_1_37.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMIMPO.Event(cEvent)
        if lower(cEvent)==lower("w_ZOOMIMPO selected")
          .Calculate_PCCPPBWXPE()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kii
    if Cevent='w_FLGCA1 Changed' or Cevent='w_FLGCA2 Changed'
       This.w_FLGCAT=iif(isalt(),This.w_FLGCA2,This.w_FLGCA1)
       This.Notifyevent('Ricerca')
    endif
    If cevent='Ricerca' or Cevent='w_STATO Changed' or Cevent='w_SERPRO Changed'
       * seleziona tutte le attivit�
       update (this.w_ZOOMIMPO.cCursor) set XCHK = 1
       select (this.w_ZOOMIMPO.cCursor)
       go top
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SERPRO
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRO_EXPO_IDX,3]
    i_lTable = "PRO_EXPO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2], .t., this.PRO_EXPO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_APE',True,'PRO_EXPO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PRSERIAL like "+cp_ToStrODBC(trim(this.w_SERPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select PRSERIAL,PRDESCRI,PRCODUTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PRSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PRSERIAL',trim(this.w_SERPRO))
          select PRSERIAL,PRDESCRI,PRCODUTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PRSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SERPRO)==trim(_Link_.PRSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SERPRO) and !this.bDontReportError
            deferred_cp_zoom('PRO_EXPO','*','PRSERIAL',cp_AbsName(oSource.parent,'oSERPRO_1_1'),i_cWhere,'GSAG_APE',"Profili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRSERIAL,PRDESCRI,PRCODUTE";
                     +" from "+i_cTable+" "+i_lTable+" where PRSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRSERIAL',oSource.xKey(1))
            select PRSERIAL,PRDESCRI,PRCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRSERIAL,PRDESCRI,PRCODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where PRSERIAL="+cp_ToStrODBC(this.w_SERPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRSERIAL',this.w_SERPRO)
            select PRSERIAL,PRDESCRI,PRCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERPRO = NVL(_Link_.PRSERIAL,space(10))
      this.w_PRDESCRI = NVL(_Link_.PRDESCRI,space(50))
      this.w_PRCODUTE = NVL(_Link_.PRCODUTE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SERPRO = space(10)
      endif
      this.w_PRDESCRI = space(50)
      this.w_PRCODUTE = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRO_EXPO_IDX,2])+'\'+cp_ToStr(_Link_.PRSERIAL,1)
      cp_ShowWarn(i_cKey,this.PRO_EXPO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PAR_AGEN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAR_AGEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAR_AGEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACHKFES,PACALDAT,PACHKATT";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PAR_AGEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PAR_AGEN)
            select PACODAZI,PACHKFES,PACALDAT,PACHKATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAR_AGEN = NVL(_Link_.PACODAZI,space(5))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
      this.w_PACALDAT = NVL(_Link_.PACALDAT,space(1))
      this.w_PACHKATT = NVL(_Link_.PACHKATT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PAR_AGEN = space(5)
      endif
      this.w_PACHKFES = space(1)
      this.w_PACALDAT = space(1)
      this.w_PACHKATT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAR_AGEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSERPRO_1_1.value==this.w_SERPRO)
      this.oPgFrm.Page1.oPag.oSERPRO_1_1.value=this.w_SERPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGCA1_1_4.RadioValue()==this.w_FLGCA1)
      this.oPgFrm.Page1.oPag.oFLGCA1_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_5.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGCA2_1_6.RadioValue()==this.w_FLGCA2)
      this.oPgFrm.Page1.oPag.oFLGCA2_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDESCRI_1_27.value==this.w_PRDESCRI)
      this.oPgFrm.Page1.oPag.oPRDESCRI_1_27.value=this.w_PRDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODEVE_2_1.value==this.w_CODEVE)
      this.oPgFrm.Page2.oPag.oCODEVE_2_1.value=this.w_CODEVE
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINI_2_3.value==this.w_DATINI)
      this.oPgFrm.Page2.oPag.oDATINI_2_3.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFIN_2_5.value==this.w_DATFIN)
      this.oPgFrm.Page2.oPag.oDATFIN_2_5.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oOGGETTO_2_7.value==this.w_OGGETTO)
      this.oPgFrm.Page2.oPag.oOGGETTO_2_7.value=this.w_OGGETTO
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCRIZ_2_9.value==this.w_DESCRIZ)
      this.oPgFrm.Page2.oPag.oDESCRIZ_2_9.value=this.w_DESCRIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_38.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_38.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SERPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSERPRO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_SERPRO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLGCA1 = this.w_FLGCA1
    this.o_FLGCA2 = this.w_FLGCA2
    this.o_UID = this.w_UID
    return

enddefine

* --- Define pages as container
define class tgsag_kiiPag1 as StdContainer
  Width  = 652
  height = 509
  stdWidth  = 652
  stdheight = 509
  resizeXpos=350
  resizeYpos=287
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSERPRO_1_1 as StdField with uid="AZKYDFMGSL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SERPRO", cQueryName = "SERPRO",nZero=10,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Profilo utente",;
    HelpContextID = 76235558,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=97, Top=12, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRO_EXPO", cZoomOnZoom="GSAG_APE", oKey_1_1="PRSERIAL", oKey_1_2="this.w_SERPRO"

  func oSERPRO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oSERPRO_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSERPRO_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRO_EXPO','*','PRSERIAL',cp_AbsName(this.parent,'oSERPRO_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_APE',"Profili",'',this.parent.oContained
  endproc
  proc oSERPRO_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAG_APE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PRSERIAL=this.parent.oContained.w_SERPRO
     i_obj.ecpSave()
  endproc


  add object oFLGCA1_1_4 as StdCombo with uid="OYQEKKKYZV",rtseq=4,rtrep=.f.,left=97,top=41,width=179,height=21;
    , ToolTipText = "Categorie attivit�";
    , HelpContextID = 91068758;
    , cFormVar="w_FLGCA1",RowSource=""+"Tutte,"+"Nessuna,"+"Generica,"+"Appuntamento,"+"Cosa da fare,"+"Nota,"+"Sessione telefonica,"+"Assenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLGCA1_1_4.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'N',;
    iif(this.value =3,'G',;
    iif(this.value =4,'S',;
    iif(this.value =5,'D',;
    iif(this.value =6,'M',;
    iif(this.value =7,'T',;
    iif(this.value =8,'A',;
    space(1))))))))))
  endfunc
  func oFLGCA1_1_4.GetRadio()
    this.Parent.oContained.w_FLGCA1 = this.RadioValue()
    return .t.
  endfunc

  func oFLGCA1_1_4.SetRadio()
    this.Parent.oContained.w_FLGCA1=trim(this.Parent.oContained.w_FLGCA1)
    this.value = ;
      iif(this.Parent.oContained.w_FLGCA1=='O',1,;
      iif(this.Parent.oContained.w_FLGCA1=='N',2,;
      iif(this.Parent.oContained.w_FLGCA1=='G',3,;
      iif(this.Parent.oContained.w_FLGCA1=='S',4,;
      iif(this.Parent.oContained.w_FLGCA1=='D',5,;
      iif(this.Parent.oContained.w_FLGCA1=='M',6,;
      iif(this.Parent.oContained.w_FLGCA1=='T',7,;
      iif(this.Parent.oContained.w_FLGCA1=='A',8,;
      0))))))))
  endfunc

  func oFLGCA1_1_4.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oSTATO_1_5 as StdCombo with uid="BTZENZDRTL",value=1,rtseq=5,rtrep=.f.,left=397,top=41,width=144,height=21;
    , ToolTipText = "Stato";
    , HelpContextID = 90063398;
    , cFormVar="w_STATO",RowSource=""+"Tutti,"+"Cancellati,"+"Modificati,"+"Nuovi,"+"Esclusi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_5.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'C',;
    iif(this.value =3,'M',;
    iif(this.value =4,'N',;
    iif(this.value =5,'E',;
    space(1)))))))
  endfunc
  func oSTATO_1_5.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_5.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='',1,;
      iif(this.Parent.oContained.w_STATO=='C',2,;
      iif(this.Parent.oContained.w_STATO=='M',3,;
      iif(this.Parent.oContained.w_STATO=='N',4,;
      iif(this.Parent.oContained.w_STATO=='E',5,;
      0)))))
  endfunc


  add object oFLGCA2_1_6 as StdCombo with uid="INFHFKHBRN",rtseq=6,rtrep=.f.,left=97,top=41,width=179,height=21;
    , ToolTipText = "Categorie attivit�";
    , HelpContextID = 107845974;
    , cFormVar="w_FLGCA2",RowSource=""+"Tutte,"+"Nessuna,"+"Generica,"+"Udienza,"+"Appuntamento,"+"Cosa da fare,"+"Nota,"+"Sessione telefonica,"+"Assenza", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLGCA2_1_6.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'N',;
    iif(this.value =3,'G',;
    iif(this.value =4,'U',;
    iif(this.value =5,'S',;
    iif(this.value =6,'D',;
    iif(this.value =7,'M',;
    iif(this.value =8,'T',;
    iif(this.value =9,'A',;
    space(1)))))))))))
  endfunc
  func oFLGCA2_1_6.GetRadio()
    this.Parent.oContained.w_FLGCA2 = this.RadioValue()
    return .t.
  endfunc

  func oFLGCA2_1_6.SetRadio()
    this.Parent.oContained.w_FLGCA2=trim(this.Parent.oContained.w_FLGCA2)
    this.value = ;
      iif(this.Parent.oContained.w_FLGCA2=='O',1,;
      iif(this.Parent.oContained.w_FLGCA2=='N',2,;
      iif(this.Parent.oContained.w_FLGCA2=='G',3,;
      iif(this.Parent.oContained.w_FLGCA2=='U',4,;
      iif(this.Parent.oContained.w_FLGCA2=='S',5,;
      iif(this.Parent.oContained.w_FLGCA2=='D',6,;
      iif(this.Parent.oContained.w_FLGCA2=='M',7,;
      iif(this.Parent.oContained.w_FLGCA2=='T',8,;
      iif(this.Parent.oContained.w_FLGCA2=='A',9,;
      0)))))))))
  endfunc

  func oFLGCA2_1_6.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oBtn_1_8 as StdButton with uid="JQUMWEMXPE",left=584, top=39, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 178345750;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOMIMPO as cp_szoombox with uid="MPOPQLEMXY",left=8, top=90, width=628,height=287,;
    caption='ZOOMIMPO',;
   bGlobalFont=.t.,;
    cTable="DET_IMPO",cZoomFile="GSAG_KII",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,DisableSelMenu=.f.,;
    cEvent = "Ricerca,w_SERPRO Changed,w_STATO Changed",;
    nPag=1;
    , HelpContextID = 33037797


  add object oBtn_1_15 as StdButton with uid="ERHOXVEEGI",left=534, top=459, width=48,height=45,;
    CpPicture="bmp\applica.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare attivit�";
    , HelpContextID = 158708839;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSAG_BII(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_STATO<> 'M')
     endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="TKCMXZXILU",left=584, top=459, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 8749126;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPRDESCRI_1_27 as StdField with uid="GOZOFNWDIA",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PRDESCRI", cQueryName = "PRDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 143618111,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=236, Top=13, InputMask=replicate('X',50)


  add object oBtn_1_32 as StdButton with uid="HEACLVWGWF",left=12, top=459, width=48,height=45,;
    CpPicture="bmp\DM_agenda.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare\inserire attivit�";
    , HelpContextID = 253324826;
    , Caption='\<Attivit�';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        =opengest('A','GSAG_AAT','ATSERIAL',.w_COSERATT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_COSERATT))
      endwith
    endif
  endfunc


  add object oBtn_1_33 as StdButton with uid="PDWBSCSQQC",left=483, top=459, width=48,height=45,;
    CpPicture="bmp\t_cancel.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare\inserire attivit�";
    , HelpContextID = 69697466;
    , Caption='\<Escludi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSAG_BII(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_STATO $ 'T-E' )
     endwith
    endif
  endfunc


  add object oBtn_1_35 as StdButton with uid="DWTKABFCFI",left=534, top=459, width=48,height=45,;
    CpPicture="bmp\genera.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per inserire attivit�";
    , HelpContextID = 8232742;
    , Caption='\<Crea';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      with this.Parent.oContained
        GSAG_BII(this.Parent.oContained,"N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_STATO <>'N' or .w_FLGCAT='O')
     endwith
    endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="GAMXLMQAUX",left=534, top=459, width=48,height=45,;
    CpPicture="bmp\ELIMINA2.bmp", caption="", nPag=1;
    , ToolTipText = "Premere eliminare attivit� importata";
    , HelpContextID = 182855354;
    , Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSAG_BII(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!(.w_STATO $ 'C-E'))
     endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="KQWPAFEHMR",left=433, top=459, width=48,height=45,;
    CpPicture="bmp\nonela.bmp", caption="", nPag=1;
    , ToolTipText = "Premere ignorare gli eventi per successive importazioni";
    , HelpContextID = 145499270;
    , Caption='\<Ignora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSAG_BII(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_STATO $ 'T-N-E'  or  Empty(.w_STATO))
     endwith
    endif
  endfunc

  add object oDESCRI_1_38 as StdMemo with uid="MTGBJBAXDC",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 243159606,;
   bGlobalFont=.t.,;
    Height=71, Width=624, Left=10, Top=385, tabstop = .f., readonly = .t.

  add object oStr_1_10 as StdString with uid="DWBPIVPSUP",Visible=.t., Left=4, Top=13,;
    Alignment=1, Width=86, Height=18,;
    Caption="Profilo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="SQZZYZVKOG",Visible=.t., Left=33, Top=40,;
    Alignment=0, Width=57, Height=18,;
    Caption="Categorie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="XIFCRLPBTB",Visible=.t., Left=362, Top=40,;
    Alignment=1, Width=31, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsag_kiiPag2 as StdContainer
  Width  = 652
  height = 509
  stdWidth  = 652
  stdheight = 509
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODEVE_2_1 as StdField with uid="XVXTICPNFJ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CODEVE", cQueryName = "CODEVE",;
    bObbl = .f. , nPag = 2, value=space(150), bMultilanguage =  .f.,;
    HelpContextID = 180317222,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=85, Top=30, InputMask=replicate('X',150)

  add object oDATINI_2_3 as StdField with uid="KBFPWCHCCX",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 2, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio",;
    HelpContextID = 239361590,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=85, Top=56, bHasZoom = .t. 

  proc oDATINI_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oDATINI_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDATFIN_2_5 as StdField with uid="LZEVUAJEVJ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 2, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "Data fine",;
    HelpContextID = 49372726,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=361, Top=56, bHasZoom = .t. 

  proc oDATFIN_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with '','*','',cp_AbsName(this.parent,'oDATFIN_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oOGGETTO_2_7 as AH_SEARCHFLD with uid="JPEOPZDKJK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_OGGETTO", cQueryName = "OGGETTO",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto",;
    HelpContextID = 161453286,;
   bGlobalFont=.t.,;
    Height=21, Width=401, Left=85, Top=86, InputMask=replicate('X',254)

  add object oDESCRIZ_2_9 as AH_SEARCHFLD with uid="XROQNXNGJE",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCRIZ", cQueryName = "DESCRIZ",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 25275850,;
   bGlobalFont=.t.,;
    Height=21, Width=401, Left=85, Top=112, InputMask=replicate('X',254)

  add object oStr_2_2 as StdString with uid="CQIDCLCWQX",Visible=.t., Left=56, Top=33,;
    Alignment=1, Width=22, Height=18,;
    Caption="Uid:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="BTABIJNRPO",Visible=.t., Left=18, Top=58,;
    Alignment=1, Width=60, Height=18,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="FUZKQGRXUN",Visible=.t., Left=298, Top=58,;
    Alignment=1, Width=60, Height=18,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="LAOBKUOWEU",Visible=.t., Left=32, Top=87,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="GNERFOYQSA",Visible=.t., Left=10, Top=113,;
    Alignment=1, Width=68, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kii','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
