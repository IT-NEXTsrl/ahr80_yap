* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bom                                                        *
*              Inputazione ora minuti per quantit�                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-03-12                                                      *
* Last revis.: 2013-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bom",oParentObject,m.pEXEC)
return(i_retval)

define class tgsag_bom as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_LDUR_ORE = 0
  w_MINEFF = 0
  w_OREEFF = 0
  w_QTAMOV = 0
  w_UNIMIS = space(3)
  w_ARROT = 0
  w_OKMSK = .f.
  w_PADRE = .NULL.
  w_TIME = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito in F9 
    *     campo PRQTAMOV  in GSAG_MPR,GSAG_APR
    *     campo DAQTAMOV  in GSAG_MDA
    * --- Possibile origine 
    *     I    Inserimento provvisorio
    *     D  Dettaglio attivit�
    *     A  Anagrafica prestazioni
    *     K Gsag_kpd
    do case
      case this.Pexec$ "I-A"
        this.w_UNIMIS = this.oParentObject.w_PRUNIMIS
      case this.Pexec $ "D-K"
        this.w_UNIMIS = this.oParentObject.w_DAUNIMIS
    endcase
    do case
      case this.pEXEC $ "I-D-A-K"
        if this.oParentObject.w_CHKTEMP="S"
          this.w_LDUR_ORE = this.oParentObject.w_DUR_ORE
          if this.Pexec $ "I-A"
            this.w_MINEFF = this.oParentObject.w_PRMINEFF
            this.w_OREEFF = this.oParentObject.w_PROREEFF
            this.w_QTAMOV = this.oParentObject.w_PRQTAMOV
          else
            this.w_MINEFF = this.oParentObject.w_DAMINEFF
            this.w_OREEFF = this.oParentObject.w_DAOREEFF
          endif
          this.w_ARROT = iif(this.w_LDUR_ORE>=1,g_perpqt,0)
          this.w_QTAMOV = cp_ROUND(INT(cp_Round(this.w_OREEFF/this.w_LDUR_ORE,0) )+(this.w_MINEFF/60)/this.w_LDUR_ORE,this.w_ARROT)
          do gsag_kom with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_PADRE = This.oparentobject
          if this.w_OKMSK
            do case
              case this.Pexec= "I"
                this.w_PADRE.Set("w_PRMINEFF" , this.w_MINEFF,.F.,.T.)     
                this.w_PADRE.Set("w_PROREEFF" , this.w_OREEFF,.F.,.T.)     
                this.w_PADRE.Set("w_PRQTAMOV" , this.w_QTAMOV,.F.,.T.)     
              case this.Pexec= "A"
                this.w_PADRE.w_PRMINEFF = this.w_MINEFF
                this.w_PADRE.w_PROREEFF = this.w_OREEFF
                this.w_PADRE.w_PRQTAMOV = this.w_QTAMOV
              case this.Pexec= "K"
                this.w_PADRE.w_DAMINEFF = this.w_MINEFF
                this.w_PADRE.w_DAOREEFF = this.w_OREEFF
                this.w_PADRE.w_DAQTAMOV = this.w_QTAMOV
              otherwise
                this.w_PADRE.Set("w_DAMINEFF" , this.w_MINEFF,.F.,.T.)     
                this.w_PADRE.Set("w_DAOREEFF" , this.w_OREEFF,.F.,.T.)     
                this.w_PADRE.Set("w_DAQTAMOV" , this.w_QTAMOV,.F.,.T.)     
            endcase
          endif
        endif
      case this.pEXEC = "C"
        this.w_LDUR_ORE = This.oparentobject.w_LDUR_ORE
        this.w_MINEFF = This.oparentobject.w_MINEFF
        this.w_OREEFF = This.oparentobject.w_OREEFF
        this.w_ARROT = iif(this.w_LDUR_ORE>=1,g_perpqt,0)
        this.w_QTAMOV = cp_ROUND(INT(cp_Round(this.w_OREEFF/this.w_LDUR_ORE,0) )+(this.w_MINEFF/60)/this.w_LDUR_ORE,this.w_ARROT)
        this.w_TIME = Min(INT(this.w_LDUR_ORE * this.w_QTAMOV),999) + INT((this.w_LDUR_ORE * this.w_QTAMOV - INT(this.w_LDUR_ORE * this.w_QTAMOV)) * 60)
        do case
          case this.w_TIME>this.w_OREEFF+this.w_MINEFF
            this.w_QTAMOV = cp_Round(this.w_QTAMOV -VAL("0,"+REPL("0",G_PERPQT)+"499"),g_PERPQT)
          case this.w_TIME<this.w_OREEFF+this.w_MINEFF
            this.w_QTAMOV = cp_Round(this.w_QTAMOV +VAL("0,"+REPL("0",G_PERPQT)+"499"),g_PERPQT)
        endcase
        This.oparentobject.w_QTAMOV=this.w_QTAMOV
    endcase
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
