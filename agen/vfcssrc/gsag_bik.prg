* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bik                                                        *
*              Controllo impianto                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-11-11                                                      *
* Last revis.: 2011-11-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bik",oParentObject)
return(i_retval)

define class tgsag_bik as StdBatch
  * --- Local variables
  w_MESS = space(100)
  * --- WorkFile variables
  COM_ATTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla che l'impianto non sia utilizzato nell'attivit�
    * --- Read from COM_ATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.COM_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COM_ATTI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" COM_ATTI where ";
            +"CACODIMP = "+cp_ToStrODBC(this.oParentObject.w_IMCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            CACODIMP = this.oParentObject.w_IMCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows<>0
      this.w_MESS = Ah_MsgFormat("Esistono una o pi� attivit� collegate all'impianto %1 %0Impossibile cancellare!",this.oParentObject.w_IMCODICE)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='COM_ATTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
