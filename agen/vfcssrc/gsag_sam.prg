* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_sam                                                        *
*              Stampa attivit� multistudio                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-09                                                      *
* Last revis.: 2013-09-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsag_sam
IF NOT Accedimask()
    AH_ErrorMsg('Impossibile accedere se nei parametri attivit� la modalit� di visualizzazione � Da struttura aziendale')
    return
ENDIF
* --- Fine Area Manuale
return(createobject("tgsag_sam",oParentObject))

* --- Class definition
define class tgsag_sam as StdForm
  Top    = 15
  Left   = 12

  * --- Standard Properties
  Width  = 825
  Height = 550
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-09-06"
  HelpContextID=144038039
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=67

  * --- Constant Properties
  _IDX = 0
  PRA_ENTI_IDX = 0
  PRA_UFFI_IDX = 0
  TIP_RISE_IDX = 0
  PAR_AGEN_IDX = 0
  DIPENDEN_IDX = 0
  cPrg = "gsag_sam"
  cComment = "Stampa attivit� multistudio"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READAZI = space(5)
  o_READAZI = space(5)
  w_FLVISI = space(1)
  w_FLTPART = space(5)
  o_FLTPART = space(5)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_OREFIN = space(2)
  o_OREFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_STATO = space(1)
  w_RISERVE = space(1)
  w_TIPORIS = space(1)
  w_CADESCRI = space(254)
  w_ATTSTU = space(1)
  w_ATTGENER = space(1)
  w_UDIENZE = space(1)
  w_SESSTEL = space(1)
  w_ASSENZE = space(1)
  w_DAINSPRE = space(1)
  w_MEMO = space(1)
  w_DAFARE = space(1)
  w_PARTECIP = space(60)
  w_FiltPerson = space(60)
  w_DISPONIB = 0
  w_PRIORITA = 0
  w_NOTE = space(0)
  w_ATLOCALI = space(30)
  w_FiltroWeb = space(1)
  o_FiltroWeb = space(1)
  w_ChkProm = space(1)
  o_ChkProm = space(1)
  w_DATA_PROM = ctod('  /  /  ')
  o_DATA_PROM = ctod('  /  /  ')
  w_ORA_PROM = space(2)
  o_ORA_PROM = space(2)
  w_MIN_PROM = space(2)
  o_MIN_PROM = space(2)
  w_AccodaPreavvisi = space(1)
  w_AccodaNote = space(1)
  w_AccodaDaFare = space(1)
  w_DATINI_Cose = ctod('  /  /  ')
  o_DATINI_Cose = ctod('  /  /  ')
  w_DATFIN_Cose = ctod('  /  /  ')
  o_DATFIN_Cose = ctod('  /  /  ')
  w_VISUDI = space(1)
  w_Annotazioni = space(1)
  w_DatiPratica = space(1)
  o_DatiPratica = space(1)
  w_Descagg = space(1)
  w_DATA_INI = ctot('')
  w_DATA_FIN = ctot('')
  w_COD_UTE = space(5)
  w_VARLOG = .F.
  w_OBTEST = ctod('  /  /  ')
  w_COMODO = space(30)
  w_ATTFUOSTU = space(1)
  w_OFDATDOC = ctod('  /  /  ')
  w_FILTPROM = ctot('')
  o_FILTPROM = ctot('')
  w_FiltPubWeb = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_OB_TEST = ctod('  /  /  ')
  w_TIPOGRUP = space(1)
  w_ELTIPCON = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_OREINI_Cose = space(2)
  o_OREINI_Cose = space(2)
  w_MININI_Cose = space(2)
  o_MININI_Cose = space(2)
  w_OREFIN_Cose = space(2)
  o_OREFIN_Cose = space(2)
  w_MINFIN_Cose = space(2)
  o_MINFIN_Cose = space(2)
  w_DATA_INI_Cose = ctot('')
  w_DATA_FIN_Cose = ctot('')
  w_TIPANA = space(1)
  w_PATIPRIS = space(1)
  o_PATIPRIS = space(1)
  w_CURPART = space(10)
  w_COMPANYLIST = space(50)
  w_CODPART = space(5)
  o_CODPART = space(5)
  w_OUT = .NULL.
  w_AZI_ZOOM = .NULL.
  w_PART_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsag_sam
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_samPag1","gsag_sam",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_OUT = this.oPgFrm.Pages(1).oPag.OUT
    this.w_AZI_ZOOM = this.oPgFrm.Pages(1).oPag.AZI_ZOOM
    this.w_PART_ZOOM = this.oPgFrm.Pages(1).oPag.PART_ZOOM
    DoDefault()
    proc Destroy()
      this.w_OUT = .NULL.
      this.w_AZI_ZOOM = .NULL.
      this.w_PART_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='PRA_ENTI'
    this.cWorkTables[2]='PRA_UFFI'
    this.cWorkTables[3]='TIP_RISE'
    this.cWorkTables[4]='PAR_AGEN'
    this.cWorkTables[5]='DIPENDEN'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READAZI=space(5)
      .w_FLVISI=space(1)
      .w_FLTPART=space(5)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_OREFIN=space(2)
      .w_MINFIN=space(2)
      .w_STATO=space(1)
      .w_RISERVE=space(1)
      .w_TIPORIS=space(1)
      .w_CADESCRI=space(254)
      .w_ATTSTU=space(1)
      .w_ATTGENER=space(1)
      .w_UDIENZE=space(1)
      .w_SESSTEL=space(1)
      .w_ASSENZE=space(1)
      .w_DAINSPRE=space(1)
      .w_MEMO=space(1)
      .w_DAFARE=space(1)
      .w_PARTECIP=space(60)
      .w_FiltPerson=space(60)
      .w_DISPONIB=0
      .w_PRIORITA=0
      .w_NOTE=space(0)
      .w_ATLOCALI=space(30)
      .w_FiltroWeb=space(1)
      .w_ChkProm=space(1)
      .w_DATA_PROM=ctod("  /  /  ")
      .w_ORA_PROM=space(2)
      .w_MIN_PROM=space(2)
      .w_AccodaPreavvisi=space(1)
      .w_AccodaNote=space(1)
      .w_AccodaDaFare=space(1)
      .w_DATINI_Cose=ctod("  /  /  ")
      .w_DATFIN_Cose=ctod("  /  /  ")
      .w_VISUDI=space(1)
      .w_Annotazioni=space(1)
      .w_DatiPratica=space(1)
      .w_Descagg=space(1)
      .w_DATA_INI=ctot("")
      .w_DATA_FIN=ctot("")
      .w_COD_UTE=space(5)
      .w_VARLOG=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_COMODO=space(30)
      .w_ATTFUOSTU=space(1)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_FILTPROM=ctot("")
      .w_FiltPubWeb=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OB_TEST=ctod("  /  /  ")
      .w_TIPOGRUP=space(1)
      .w_ELTIPCON=space(1)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      .w_OREINI_Cose=space(2)
      .w_MININI_Cose=space(2)
      .w_OREFIN_Cose=space(2)
      .w_MINFIN_Cose=space(2)
      .w_DATA_INI_Cose=ctot("")
      .w_DATA_FIN_Cose=ctot("")
      .w_TIPANA=space(1)
      .w_PATIPRIS=space(1)
      .w_CURPART=space(10)
      .w_COMPANYLIST=space(50)
      .w_CODPART=space(5)
        .w_READAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READAZI))
          .link_1_2('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_FLTPART = readdipend( i_CodUte, 'C')
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
          .DoRTCalc(4,5,.f.)
        .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (.w_OREFIN="00" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        .w_STATO = InitStato()
        .w_RISERVE = 'L'
          .DoRTCalc(12,23,.f.)
        .w_DISPONIB = 0
        .w_PRIORITA = 0
          .DoRTCalc(26,27,.f.)
        .w_FiltroWeb = 'T'
        .w_ChkProm = ' '
          .DoRTCalc(30,30,.f.)
        .w_ORA_PROM = PADL(ALLTRIM(STR(VAL(.w_ORA_PROM))),2,'0')
        .w_MIN_PROM = PADL(ALLTRIM(STR(VAL(.w_MIN_PROM))),2,'0')
        .w_AccodaPreavvisi = 'N'
        .w_AccodaNote = 'N'
        .w_AccodaDaFare = 'N'
          .DoRTCalc(36,37,.f.)
        .w_VISUDI = 'N'
        .w_Annotazioni = 'S'
        .w_DatiPratica = 'S'
        .w_Descagg = 'N'
        .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
          .DoRTCalc(45,45,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(47,47,.f.)
        .w_ATTFUOSTU = ' '
        .w_OFDATDOC = i_datsys
        .w_FILTPROM = IIF(.w_ChkProm='S' AND !EMPTY(.w_DATA_PROM),cp_CharToDatetime(ALLTR(STR(DAY(.w_DATA_PROM)))+'-'+ALLTR(STR(MONTH(.w_DATA_PROM)))+'-'+ALLTR(STR(YEAR(.w_DATA_PROM)))+' '+.w_ORA_PROM+':'+.w_MIN_PROM+':00', 'dd-mm-yyyy hh:nn:ss'),DTOT(CTOD('  -  -  ')))
        .w_FiltPubWeb = IIF(.w_FiltroWeb='T','',.w_FiltroWeb)
          .DoRTCalc(52,52,.f.)
        .w_OB_TEST = i_INIDAT
          .DoRTCalc(54,54,.f.)
        .w_ELTIPCON = 'T'
      .oPgFrm.Page1.oPag.OUT.Calculate()
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_OREINI_Cose = RIGHT('00'+IIF(EMPTY(.w_OREINI_Cose),'00',.w_OREINI_Cose),2)
        .w_MININI_Cose = RIGHT('00'+IIF(empty(.w_MININI_Cose),'00',.w_MININI_Cose),2)
        .w_OREFIN_Cose = RIGHT('00'+IIF(empty(.w_OREFIN_Cose) OR .w_OREFIN_Cose="00", '23', .w_OREFIN_Cose),2)
        .w_MINFIN_Cose = RIGHT('00'+IIF(empty(.w_MINFIN_Cose) OR (empty(.w_DATFIN_Cose) AND .w_OREFIN_Cose="23" AND .w_MINFIN_Cose="00"), '59', .w_MINFIN_Cose),2)
        .w_DATA_INI_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI_Cose),  ALLTR(STR(DAY(.w_DATINI_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATINI_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATINI_Cose)))+' '+.w_OREINI_Cose+':'+.w_MININI_Cose+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        .w_DATA_FIN_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN_Cose),  ALLTR(STR(DAY(.w_DATFIN_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATFIN_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATFIN_Cose)))+' '+.w_OREFIN_Cose+':'+.w_MINFIN_Cose+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        .w_TIPANA = 'C'
      .oPgFrm.Page1.oPag.AZI_ZOOM.Calculate()
      .oPgFrm.Page1.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
        .w_PATIPRIS = ''
        .w_CURPART = .w_PART_ZOOM.cCursor
    endwith
    this.DoRTCalc(66,67,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_81.enabled = this.oPgFrm.Page1.oPag.oBtn_1_81.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_82.enabled = this.oPgFrm.Page1.oPag.oBtn_1_82.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_READAZI<>.w_READAZI
            .w_READAZI = i_CODAZI
          .link_1_2('Full')
        endif
        .DoRTCalc(2,2,.t.)
        if .o_FLTPART<>.w_FLTPART
            .w_FLTPART = readdipend( i_CodUte, 'C')
        endif
        if .o_OREINI<>.w_OREINI
          .Calculate_MVFLEXPGGM()
        endif
        if .o_MININI<>.w_MININI
          .Calculate_SUGRFFLBCH()
        endif
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        if .o_OREFIN<>.w_OREFIN
          .Calculate_FPJEAUJXWE()
        endif
        if .o_MINFIN<>.w_MINFIN
          .Calculate_KTQUMLVLEF()
        endif
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(4,5,.t.)
        if .o_OREINI<>.w_OREINI
            .w_OREINI = RIGHT('00'+IIF(EMPTY(.w_OREINI),'00',.w_OREINI),2)
        endif
        if .o_MININI<>.w_MININI
            .w_MININI = RIGHT('00'+IIF(empty(.w_MININI),'00',.w_MININI),2)
        endif
        if .o_OREFIN<>.w_OREFIN.or. .o_DATFIN<>.w_DATFIN
            .w_OREFIN = RIGHT('00'+IIF(empty(.w_OREFIN) OR .w_OREFIN="00", '23', .w_OREFIN),2)
        endif
        if .o_MINFIN<>.w_MINFIN.or. .o_DATFIN<>.w_DATFIN
            .w_MINFIN = RIGHT('00'+IIF(empty(.w_MINFIN) OR (.w_OREFIN="00" AND .w_MINFIN="00"), '59', .w_MINFIN),2)
        endif
        .DoRTCalc(10,30,.t.)
        if .o_ORA_PROM<>.w_ORA_PROM
            .w_ORA_PROM = PADL(ALLTRIM(STR(VAL(.w_ORA_PROM))),2,'0')
        endif
        if .o_MIN_PROM<>.w_MIN_PROM
            .w_MIN_PROM = PADL(ALLTRIM(STR(VAL(.w_MIN_PROM))),2,'0')
        endif
        .DoRTCalc(33,41,.t.)
        if .o_DATINI<>.w_DATINI.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  ALLTR(STR(DAY(.w_DATINI)))+'-'+ALLTR(STR(MONTH(.w_DATINI)))+'-'+ALLTR(STR(YEAR(.w_DATINI)))+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_IniDat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN<>.w_DATFIN.or. .o_OREFIN<>.w_OREFIN.or. .o_MINFIN<>.w_MINFIN
            .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  ALLTR(STR(DAY(.w_DATFIN)))+'-'+ALLTR(STR(MONTH(.w_DATFIN)))+'-'+ALLTR(STR(YEAR(.w_DATFIN)))+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_FinDat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_CODPART<>.w_CODPART
            .w_COD_UTE = iif(empty(.w_CODPART), ReadDipend(i_codute,"C"), space(5) )
        endif
        if .o_ChkProm<>.w_ChkProm
          .Calculate_WWJSDCQGIA()
        endif
        .DoRTCalc(45,49,.t.)
        if .o_DATA_PROM<>.w_DATA_PROM.or. .o_MIN_PROM<>.w_MIN_PROM.or. .o_ORA_PROM<>.w_ORA_PROM.or. .o_FILTPROM<>.w_FILTPROM.or. .o_ChkProm<>.w_ChkProm
            .w_FILTPROM = IIF(.w_ChkProm='S' AND !EMPTY(.w_DATA_PROM),cp_CharToDatetime(ALLTR(STR(DAY(.w_DATA_PROM)))+'-'+ALLTR(STR(MONTH(.w_DATA_PROM)))+'-'+ALLTR(STR(YEAR(.w_DATA_PROM)))+' '+.w_ORA_PROM+':'+.w_MIN_PROM+':00', 'dd-mm-yyyy hh:nn:ss'),DTOT(CTOD('  -  -  ')))
        endif
        if .o_FiltroWeb<>.w_FiltroWeb
            .w_FiltPubWeb = IIF(.w_FiltroWeb='T','',.w_FiltroWeb)
        endif
        if .o_DatiPratica<>.w_DatiPratica
          .Calculate_DIDIGCQSMK()
        endif
        .oPgFrm.Page1.oPag.OUT.Calculate()
        .DoRTCalc(52,56,.t.)
        if .o_OREINI_Cose<>.w_OREINI_Cose
            .w_OREINI_Cose = RIGHT('00'+IIF(EMPTY(.w_OREINI_Cose),'00',.w_OREINI_Cose),2)
        endif
        if .o_MININI_Cose<>.w_MININI_Cose
            .w_MININI_Cose = RIGHT('00'+IIF(empty(.w_MININI_Cose),'00',.w_MININI_Cose),2)
        endif
        if .o_OREFIN_Cose<>.w_OREFIN_Cose.or. .o_DATFIN_Cose<>.w_DATFIN_Cose
            .w_OREFIN_Cose = RIGHT('00'+IIF(empty(.w_OREFIN_Cose) OR .w_OREFIN_Cose="00", '23', .w_OREFIN_Cose),2)
        endif
        if .o_MINFIN_Cose<>.w_MINFIN_Cose.or. .o_DATFIN_Cose<>.w_DATFIN_Cose
            .w_MINFIN_Cose = RIGHT('00'+IIF(empty(.w_MINFIN_Cose) OR (empty(.w_DATFIN_Cose) AND .w_OREFIN_Cose="23" AND .w_MINFIN_Cose="00"), '59', .w_MINFIN_Cose),2)
        endif
        if .o_DATINI_Cose<>.w_DATINI_Cose
          .Calculate_HWTLQYNXHT()
        endif
        if .o_DATFIN_Cose<>.w_DATFIN_Cose
          .Calculate_JBMQEMONXV()
        endif
        if .o_DATINI_Cose<>.w_DATINI_Cose.or. .o_OREINI_Cose<>.w_OREINI_Cose.or. .o_MININI_Cose<>.w_MININI_Cose
            .w_DATA_INI_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI_Cose),  ALLTR(STR(DAY(.w_DATINI_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATINI_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATINI_Cose)))+' '+.w_OREINI_Cose+':'+.w_MININI_Cose+':00', DTOC(i_inidat)+' 00:00:00'), 'dd-mm-yyyy hh:nn:ss')
        endif
        if .o_DATFIN_Cose<>.w_DATFIN_Cose.or. .o_OREFIN_Cose<>.w_OREFIN_Cose.or. .o_MINFIN_Cose<>.w_MINFIN_Cose
            .w_DATA_FIN_Cose = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN_Cose),  ALLTR(STR(DAY(.w_DATFIN_Cose)))+'-'+ALLTR(STR(MONTH(.w_DATFIN_Cose)))+'-'+ALLTR(STR(YEAR(.w_DATFIN_Cose)))+' '+.w_OREFIN_Cose+':'+.w_MINFIN_Cose+':00', DTOC(i_findat)+' 23:59:59'), 'dd-mm-yyyy hh:nn:ss')
        endif
        .oPgFrm.Page1.oPag.AZI_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
        if .o_PATIPRIS<>.w_PATIPRIS
          .Calculate_BLTFQIRAHO()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(63,67,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.OUT.Calculate()
        .oPgFrm.Page1.oPag.AZI_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.PART_ZOOM.Calculate(.w_PATIPRIS)
    endwith
  return

  proc Calculate_NRTSRHMIVM()
    with this
          * --- Inizializza date
          gsag_ba3(this;
              ,'AGEN';
             )
    endwith
  endproc
  proc Calculate_MVFLEXPGGM()
    with this
          * --- Resetto w_oreini
          .w_OREINI = .ZeroFill(.w_OREINI)
    endwith
  endproc
  proc Calculate_SUGRFFLBCH()
    with this
          * --- Resetto w_minini
          .w_MININI = .ZeroFill(.w_MININI)
    endwith
  endproc
  proc Calculate_FPJEAUJXWE()
    with this
          * --- Resetto w_orefin
          .w_OREFIN = .ZeroFill(.w_OREFIN)
    endwith
  endproc
  proc Calculate_KTQUMLVLEF()
    with this
          * --- Resetto w_minfin
          .w_MINFIN = .ZeroFill(.w_MINFIN)
    endwith
  endproc
  proc Calculate_LYBISUKBKM()
    with this
          * --- Imposta data odierna
          .w_DATINI = i_DatSys
          .w_DATFIN = i_DatSys
          .w_OREINI = '00'
          .w_MININI = '00'
          .w_OREFIN = '23'
          .w_MINFIN = '59'
          .w_DATA_INI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATINI),  DTOC(.w_DATINI)+' '+.w_OREINI+':'+.w_MININI+':00', DTOC(i_inidat)+' 00:00:00'))
          .w_DATA_FIN = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATFIN),  DTOC(.w_DATFIN)+' '+.w_OREFIN+':'+.w_MINFIN+':00', DTOC(i_findat)+' 23:59:59'))
    endwith
  endproc
  proc Calculate_WWJSDCQGIA()
    with this
          * --- Imposta data/ora filtro promemoria
          .w_FILTPROM = IIF(.w_ChkProm='S',DATETIME(),DTOT(CTOD('  -  -  ')))
          .w_DATA_PROM = IIF(.w_ChkProm='S',TTOD(.w_FILTPROM),CTOD('  -  -  '))
          .w_ORA_PROM = IIF(.w_ChkProm='S',PADL(ALLTRIM(STR(HOUR(.w_FILTPROM))),2,'0'),'00')
          .w_MIN_PROM = IIF(.w_ChkProm='S',PADL(ALLTRIM(STR(MINUTE(.w_FILTPROM))),2,'0'),'00')
    endwith
  endproc
  proc Calculate_DIDIGCQSMK()
    with this
          * --- Azzera w_VISUDI
          .w_VISUDI = IIF(.w_DatiPratica<>'S', 'N', .w_VISUDI)
    endwith
  endproc
  proc Calculate_HWTLQYNXHT()
    with this
          * --- Azzera ore- minuti inizio
          .w_OREINI_Cose = IIF(EMPTY(.w_DATINI_Cose),'00',.w_OREINI_Cose)
          .w_MININI_Cose = IIF(EMPTY(.w_DATINI_Cose),'00',.w_MININI_Cose)
    endwith
  endproc
  proc Calculate_JBMQEMONXV()
    with this
          * --- Azzera ore- minuti fine
          .w_OREFIN_Cose = IIF(EMPTY(.w_DATFIN_Cose),'00',.w_OREFIN_Cose)
          .w_MINFIN_Cose = IIF(EMPTY(.w_DATFIN_Cose),'00',.w_MINFIN_Cose)
    endwith
  endproc
  proc Calculate_BLTFQIRAHO()
    with this
          * --- Attiva zoom selezionando le righe
          GSAG_BPP(this;
              ,'ATTIVAZOOM';
              ,.w_PART_ZOOM;
              ,.w_CODPART;
              ,this;
             )
    endwith
  endproc
  proc Calculate_CPEHBHWZII()
    with this
          * --- Seleziona righe dello zoom aziende
          GSAG_BAM(this;
              ,'SELRIGHEAZ';
             )
    endwith
  endproc
  proc Calculate_CSCEQXJOQK()
    with this
          * --- Aggiorna Companylist e riesegue zoom dei partecipanti
          GSAG_BAM(this;
              ,'AGGLIST';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oOREINI_1_13.enabled = this.oPgFrm.Page1.oPag.oOREINI_1_13.mCond()
    this.oPgFrm.Page1.oPag.oMININI_1_14.enabled = this.oPgFrm.Page1.oPag.oMININI_1_14.mCond()
    this.oPgFrm.Page1.oPag.oOREFIN_1_15.enabled = this.oPgFrm.Page1.oPag.oOREFIN_1_15.mCond()
    this.oPgFrm.Page1.oPag.oMINFIN_1_16.enabled = this.oPgFrm.Page1.oPag.oMINFIN_1_16.mCond()
    this.oPgFrm.Page1.oPag.oDATA_PROM_1_39.enabled = this.oPgFrm.Page1.oPag.oDATA_PROM_1_39.mCond()
    this.oPgFrm.Page1.oPag.oORA_PROM_1_40.enabled = this.oPgFrm.Page1.oPag.oORA_PROM_1_40.mCond()
    this.oPgFrm.Page1.oPag.oMIN_PROM_1_41.enabled = this.oPgFrm.Page1.oPag.oMIN_PROM_1_41.mCond()
    this.oPgFrm.Page1.oPag.oAccodaNote_1_43.enabled = this.oPgFrm.Page1.oPag.oAccodaNote_1_43.mCond()
    this.oPgFrm.Page1.oPag.oAccodaDaFare_1_44.enabled = this.oPgFrm.Page1.oPag.oAccodaDaFare_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_81.enabled = this.oPgFrm.Page1.oPag.oBtn_1_81.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSTATO_1_19.visible=!this.oPgFrm.Page1.oPag.oSTATO_1_19.mHide()
    this.oPgFrm.Page1.oPag.oTIPORIS_1_21.visible=!this.oPgFrm.Page1.oPag.oTIPORIS_1_21.mHide()
    this.oPgFrm.Page1.oPag.oUDIENZE_1_25.visible=!this.oPgFrm.Page1.oPag.oUDIENZE_1_25.mHide()
    this.oPgFrm.Page1.oPag.oFiltroWeb_1_37.visible=!this.oPgFrm.Page1.oPag.oFiltroWeb_1_37.mHide()
    this.oPgFrm.Page1.oPag.oAccodaNote_1_43.visible=!this.oPgFrm.Page1.oPag.oAccodaNote_1_43.mHide()
    this.oPgFrm.Page1.oPag.oAccodaDaFare_1_44.visible=!this.oPgFrm.Page1.oPag.oAccodaDaFare_1_44.mHide()
    this.oPgFrm.Page1.oPag.oDATINI_Cose_1_45.visible=!this.oPgFrm.Page1.oPag.oDATINI_Cose_1_45.mHide()
    this.oPgFrm.Page1.oPag.oDATFIN_Cose_1_46.visible=!this.oPgFrm.Page1.oPag.oDATFIN_Cose_1_46.mHide()
    this.oPgFrm.Page1.oPag.oVISUDI_1_47.visible=!this.oPgFrm.Page1.oPag.oVISUDI_1_47.mHide()
    this.oPgFrm.Page1.oPag.oDatiPratica_1_49.visible=!this.oPgFrm.Page1.oPag.oDatiPratica_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_79.visible=!this.oPgFrm.Page1.oPag.oStr_1_79.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_90.visible=!this.oPgFrm.Page1.oPag.oStr_1_90.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NRTSRHMIVM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
        if lower(cEvent)==lower("ImpostaDataOdierna")
          .Calculate_LYBISUKBKM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.OUT.Event(cEvent)
      .oPgFrm.Page1.oPag.AZI_ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.PART_ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_PART_ZOOM after query")
          .Calculate_BLTFQIRAHO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_AZI_ZOOM after query")
          .Calculate_CPEHBHWZII()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("AZI_ZOOM multiple rows checked") or lower(cEvent)==lower("AZI_ZOOM multiple rows unchecked") or lower(cEvent)==lower("AZI_ZOOM row checked") or lower(cEvent)==lower("AZI_ZOOM row unchecked") or lower(cEvent)==lower("AZI_ZOOM rowcheckall") or lower(cEvent)==lower("AZI_ZOOM rowcheckfrom") or lower(cEvent)==lower("AZI_ZOOM rowcheckto") or lower(cEvent)==lower("AZI_ZOOM rowinvertselection") or lower(cEvent)==lower("AZI_ZOOM rowuncheckall") or lower(cEvent)==lower("w_AZI_ZOOM after query")
          .Calculate_CSCEQXJOQK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_sam
    * --- Al checked/unchecked della riga sullo zoom non � immediatamente visualizzato
    * --- o nascosto il bottone completa (la HideControls scatta solo al cambio
    * --- di riga dello zoom, per cui forzo la chiamata).
    If upper(cEvent)='W_AGKRA_ZOOM ROW CHECKED' OR Upper(cEvent)='W_AGKRA_ZOOM ROW UNCHECKED' OR upper(cEvent)='W_AGKRA_ZOOM3 ROW CHECKED' OR Upper(cEvent)='W_AGKRA_ZOOM3 ROW UNCHECKED'
        this.mHideControls()
    endif
    
    
    
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLVISI,PAFLAPP3,PAFLATG3,PAFLUDI3,PAFLSES3,PAFLASS3,PAFLINP3,PAFLNOT3,PAFLDAF3";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PAFLVISI,PAFLAPP3,PAFLATG3,PAFLUDI3,PAFLSES3,PAFLASS3,PAFLINP3,PAFLNOT3,PAFLDAF3;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_FLVISI = NVL(_Link_.PAFLVISI,space(1))
      this.w_ATTSTU = NVL(_Link_.PAFLAPP3,space(1))
      this.w_ATTGENER = NVL(_Link_.PAFLATG3,space(1))
      this.w_UDIENZE = NVL(_Link_.PAFLUDI3,space(1))
      this.w_SESSTEL = NVL(_Link_.PAFLSES3,space(1))
      this.w_ASSENZE = NVL(_Link_.PAFLASS3,space(1))
      this.w_DAINSPRE = NVL(_Link_.PAFLINP3,space(1))
      this.w_MEMO = NVL(_Link_.PAFLNOT3,space(1))
      this.w_DAFARE = NVL(_Link_.PAFLDAF3,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_FLVISI = space(1)
      this.w_ATTSTU = space(1)
      this.w_ATTGENER = space(1)
      this.w_UDIENZE = space(1)
      this.w_SESSTEL = space(1)
      this.w_ASSENZE = space(1)
      this.w_DAINSPRE = space(1)
      this.w_MEMO = space(1)
      this.w_DAFARE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_11.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_11.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_12.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_12.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINI_1_13.value==this.w_OREINI)
      this.oPgFrm.Page1.oPag.oOREINI_1_13.value=this.w_OREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_14.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_14.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oOREFIN_1_15.value==this.w_OREFIN)
      this.oPgFrm.Page1.oPag.oOREFIN_1_15.value=this.w_OREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_16.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_16.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_1_19.RadioValue()==this.w_STATO)
      this.oPgFrm.Page1.oPag.oSTATO_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRISERVE_1_20.RadioValue()==this.w_RISERVE)
      this.oPgFrm.Page1.oPag.oRISERVE_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPORIS_1_21.RadioValue()==this.w_TIPORIS)
      this.oPgFrm.Page1.oPag.oTIPORIS_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESCRI_1_22.value==this.w_CADESCRI)
      this.oPgFrm.Page1.oPag.oCADESCRI_1_22.value=this.w_CADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oATTSTU_1_23.RadioValue()==this.w_ATTSTU)
      this.oPgFrm.Page1.oPag.oATTSTU_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATTGENER_1_24.RadioValue()==this.w_ATTGENER)
      this.oPgFrm.Page1.oPag.oATTGENER_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUDIENZE_1_25.RadioValue()==this.w_UDIENZE)
      this.oPgFrm.Page1.oPag.oUDIENZE_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSESSTEL_1_26.RadioValue()==this.w_SESSTEL)
      this.oPgFrm.Page1.oPag.oSESSTEL_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASSENZE_1_27.RadioValue()==this.w_ASSENZE)
      this.oPgFrm.Page1.oPag.oASSENZE_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAINSPRE_1_28.RadioValue()==this.w_DAINSPRE)
      this.oPgFrm.Page1.oPag.oDAINSPRE_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMEMO_1_29.RadioValue()==this.w_MEMO)
      this.oPgFrm.Page1.oPag.oMEMO_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAFARE_1_30.RadioValue()==this.w_DAFARE)
      this.oPgFrm.Page1.oPag.oDAFARE_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPARTECIP_1_31.value==this.w_PARTECIP)
      this.oPgFrm.Page1.oPag.oPARTECIP_1_31.value=this.w_PARTECIP
    endif
    if not(this.oPgFrm.Page1.oPag.oFiltPerson_1_32.value==this.w_FiltPerson)
      this.oPgFrm.Page1.oPag.oFiltPerson_1_32.value=this.w_FiltPerson
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPONIB_1_33.RadioValue()==this.w_DISPONIB)
      this.oPgFrm.Page1.oPag.oDISPONIB_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRIORITA_1_34.RadioValue()==this.w_PRIORITA)
      this.oPgFrm.Page1.oPag.oPRIORITA_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_35.value==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_35.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oATLOCALI_1_36.value==this.w_ATLOCALI)
      this.oPgFrm.Page1.oPag.oATLOCALI_1_36.value=this.w_ATLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oFiltroWeb_1_37.RadioValue()==this.w_FiltroWeb)
      this.oPgFrm.Page1.oPag.oFiltroWeb_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oChkProm_1_38.RadioValue()==this.w_ChkProm)
      this.oPgFrm.Page1.oPag.oChkProm_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA_PROM_1_39.value==this.w_DATA_PROM)
      this.oPgFrm.Page1.oPag.oDATA_PROM_1_39.value=this.w_DATA_PROM
    endif
    if not(this.oPgFrm.Page1.oPag.oORA_PROM_1_40.value==this.w_ORA_PROM)
      this.oPgFrm.Page1.oPag.oORA_PROM_1_40.value=this.w_ORA_PROM
    endif
    if not(this.oPgFrm.Page1.oPag.oMIN_PROM_1_41.value==this.w_MIN_PROM)
      this.oPgFrm.Page1.oPag.oMIN_PROM_1_41.value=this.w_MIN_PROM
    endif
    if not(this.oPgFrm.Page1.oPag.oAccodaPreavvisi_1_42.RadioValue()==this.w_AccodaPreavvisi)
      this.oPgFrm.Page1.oPag.oAccodaPreavvisi_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAccodaNote_1_43.RadioValue()==this.w_AccodaNote)
      this.oPgFrm.Page1.oPag.oAccodaNote_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAccodaDaFare_1_44.RadioValue()==this.w_AccodaDaFare)
      this.oPgFrm.Page1.oPag.oAccodaDaFare_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_Cose_1_45.value==this.w_DATINI_Cose)
      this.oPgFrm.Page1.oPag.oDATINI_Cose_1_45.value=this.w_DATINI_Cose
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_Cose_1_46.value==this.w_DATFIN_Cose)
      this.oPgFrm.Page1.oPag.oDATFIN_Cose_1_46.value=this.w_DATFIN_Cose
    endif
    if not(this.oPgFrm.Page1.oPag.oVISUDI_1_47.RadioValue()==this.w_VISUDI)
      this.oPgFrm.Page1.oPag.oVISUDI_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAnnotazioni_1_48.RadioValue()==this.w_Annotazioni)
      this.oPgFrm.Page1.oPag.oAnnotazioni_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDatiPratica_1_49.RadioValue()==this.w_DatiPratica)
      this.oPgFrm.Page1.oPag.oDatiPratica_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDescagg_1_50.RadioValue()==this.w_Descagg)
      this.oPgFrm.Page1.oPag.oDescagg_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATIPRIS_1_103.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page1.oPag.oPATIPRIS_1_103.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(VAL(.w_OREINI) < 24)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREINI_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)  and (!empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(VAL(.w_OREFIN) < 24)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)  and (!empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(VAL(.w_ORA_PROM) < 24)  and (.w_ChkProm='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORA_PROM_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MIN_PROM) < 60)  and (.w_ChkProm='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMIN_PROM_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(.w_DATINI_Cose<=.w_DATFIN_Cose OR EMPTY(.w_DATFIN_Cose))  and not(.w_AccodaDaFare<>'S' AND .w_AccodaNote<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_Cose_1_46.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_READAZI = this.w_READAZI
    this.o_FLTPART = this.w_FLTPART
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_OREFIN = this.w_OREFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_FiltroWeb = this.w_FiltroWeb
    this.o_ChkProm = this.w_ChkProm
    this.o_DATA_PROM = this.w_DATA_PROM
    this.o_ORA_PROM = this.w_ORA_PROM
    this.o_MIN_PROM = this.w_MIN_PROM
    this.o_DATINI_Cose = this.w_DATINI_Cose
    this.o_DATFIN_Cose = this.w_DATFIN_Cose
    this.o_DatiPratica = this.w_DatiPratica
    this.o_FILTPROM = this.w_FILTPROM
    this.o_OREINI_Cose = this.w_OREINI_Cose
    this.o_MININI_Cose = this.w_MININI_Cose
    this.o_OREFIN_Cose = this.w_OREFIN_Cose
    this.o_MINFIN_Cose = this.w_MINFIN_Cose
    this.o_PATIPRIS = this.w_PATIPRIS
    this.o_CODPART = this.w_CODPART
    return

enddefine

* --- Define pages as container
define class tgsag_samPag1 as StdContainer
  Width  = 821
  height = 550
  stdWidth  = 821
  stdheight = 550
  resizeXpos=710
  resizeYpos=401
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_7 as cp_runprogram with uid="VSYPGKICOH",left=14, top=609, width=211,height=26,;
    caption='Azzera ore e min iniziali',;
   bGlobalFont=.t.,;
    prg="gsag_ba2('1')",;
    cEvent = "w_DATINI Changed",;
    nPag=1;
    , HelpContextID = 26916351


  add object oObj_1_10 as cp_runprogram with uid="GTGWZWHBZD",left=14, top=638, width=211,height=26,;
    caption='Azzera ore e min finali',;
   bGlobalFont=.t.,;
    prg="gsag_ba2('2')",;
    cEvent = "w_DATFIN Changed",;
    nPag=1;
    , HelpContextID = 217097213

  add object oDATINI_1_11 as StdField with uid="VAJTUNAFFS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 154902986,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=54, Top=16

  func oDATINI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_12 as StdField with uid="VRHGJGBHQK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 76456394,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=54, Top=40

  func oDATFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oOREINI_1_13 as StdField with uid="WOKDXXPUIP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_OREINI", cQueryName = "OREINI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di inizio selezione",;
    HelpContextID = 154959898,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=181, Top=16, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREINI_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oOREINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_14 as StdField with uid="QMRKNBACQP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di inizio selezione",;
    HelpContextID = 154925370,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=219, Top=16, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATINI))
    endwith
   endif
  endfunc

  func oMININI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oOREFIN_1_15 as StdField with uid="VKJCJSQZFK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OREFIN", cQueryName = "OREFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora di fine selezione",;
    HelpContextID = 76513306,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=181, Top=40, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREFIN_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oOREFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_16 as StdField with uid="GIWPXXGMLC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti di fine selezione",;
    HelpContextID = 76478778,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=219, Top=40, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_DATFIN))
    endwith
   endif
  endfunc

  func oMINFIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc


  add object oBtn_1_17 as StdButton with uid="BQJHHOQZPU",left=250, top=16, width=48,height=45,;
    CpPicture="bmp\date-time.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per impostare la data odierna";
    , HelpContextID = 151368934;
    , caption='\<Oggi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      this.parent.oContained.NotifyEvent("ImpostaDataOdierna")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oSTATO_1_19 as StdCombo with uid="HROEOHMDQV",rtseq=10,rtrep=.f.,left=54,top=72,width=183,height=21;
    , ToolTipText = "Stato";
    , HelpContextID = 232669734;
    , cFormVar="w_STATO",RowSource=""+"Non evasa,"+"Da svolgere,"+"In corso,"+"Provvisoria,"+"Evasa,"+"Completata,"+"Evasa o completata,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_1_19.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'T',;
    iif(this.value =5,'F',;
    iif(this.value =6,'P',;
    iif(this.value =7,'B',;
    iif(this.value =8,'K',;
    space(1))))))))))
  endfunc
  func oSTATO_1_19.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_1_19.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='N',1,;
      iif(this.Parent.oContained.w_STATO=='D',2,;
      iif(this.Parent.oContained.w_STATO=='I',3,;
      iif(this.Parent.oContained.w_STATO=='T',4,;
      iif(this.Parent.oContained.w_STATO=='F',5,;
      iif(this.Parent.oContained.w_STATO=='P',6,;
      iif(this.Parent.oContained.w_STATO=='B',7,;
      iif(this.Parent.oContained.w_STATO=='K',8,;
      0))))))))
  endfunc

  func oSTATO_1_19.mHide()
    with this.Parent.oContained
      return (.w_RISERVE='S')
    endwith
  endfunc


  add object oRISERVE_1_20 as StdCombo with uid="MIFARZNGXZ",rtseq=11,rtrep=.f.,left=237,top=72,width=106,height=21;
    , ToolTipText = "Considera o meno tutte le attivit� in riserva a prescindere dai filtri sullo stato e sulle date";
    , HelpContextID = 201304298;
    , cFormVar="w_RISERVE",RowSource=""+"Escludi riserve,"+"Aggiungi riserve,"+"No filtro riserve,"+"Solo riserve", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRISERVE_1_20.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'A',;
    iif(this.value =3,'L',;
    iif(this.value =4,'S',;
    space(1))))))
  endfunc
  func oRISERVE_1_20.GetRadio()
    this.Parent.oContained.w_RISERVE = this.RadioValue()
    return .t.
  endfunc

  func oRISERVE_1_20.SetRadio()
    this.Parent.oContained.w_RISERVE=trim(this.Parent.oContained.w_RISERVE)
    this.value = ;
      iif(this.Parent.oContained.w_RISERVE=='E',1,;
      iif(this.Parent.oContained.w_RISERVE=='A',2,;
      iif(this.Parent.oContained.w_RISERVE=='L',3,;
      iif(this.Parent.oContained.w_RISERVE=='S',4,;
      0))))
  endfunc


  add object oTIPORIS_1_21 as StdTableCombo with uid="XMEKXWDJPL",rtseq=12,rtrep=.f.,left=415,top=72,width=123,height=21;
    , HelpContextID = 150329546;
    , cFormVar="w_TIPORIS",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='TIP_RISE',cKey='TRCODICE',cValue='TRDESTIP',cOrderBy='TRDESTIP',xDefault=space(1);
  , bGlobalFont=.t.


  func oTIPORIS_1_21.mHide()
    with this.Parent.oContained
      return (.w_RISERVE<> 'S' )
    endwith
  endfunc

  add object oCADESCRI_1_22 as AH_SEARCHFLD with uid="AVSKMMIFVK",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CADESCRI", cQueryName = "CADESCRI",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione oggetto o parte di essa",;
    HelpContextID = 250651025,;
   bGlobalFont=.t.,;
    Height=21, Width=482, Left=53, Top=103, InputMask=replicate('X',254)

  add object oATTSTU_1_23 as StdCheck with uid="FUHPBAIQSC",rtseq=14,rtrep=.f.,left=53, top=135, caption="Appuntamenti",;
    ToolTipText = "Se attivo: visualizza gli appuntamenti",;
    HelpContextID = 215060218,;
    cFormVar="w_ATTSTU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATTSTU_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oATTSTU_1_23.GetRadio()
    this.Parent.oContained.w_ATTSTU = this.RadioValue()
    return .t.
  endfunc

  func oATTSTU_1_23.SetRadio()
    this.Parent.oContained.w_ATTSTU=trim(this.Parent.oContained.w_ATTSTU)
    this.value = ;
      iif(this.Parent.oContained.w_ATTSTU=='S',1,;
      0)
  endfunc

  add object oATTGENER_1_24 as StdCheck with uid="KLBNRLNCJC",rtseq=15,rtrep=.f.,left=53, top=155, caption="Attivit� generiche",;
    ToolTipText = "Se attivo: visualizza le attivit� generiche",;
    HelpContextID = 80580264,;
    cFormVar="w_ATTGENER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATTGENER_1_24.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oATTGENER_1_24.GetRadio()
    this.Parent.oContained.w_ATTGENER = this.RadioValue()
    return .t.
  endfunc

  func oATTGENER_1_24.SetRadio()
    this.Parent.oContained.w_ATTGENER=trim(this.Parent.oContained.w_ATTGENER)
    this.value = ;
      iif(this.Parent.oContained.w_ATTGENER=='G',1,;
      0)
  endfunc

  add object oUDIENZE_1_25 as StdCheck with uid="HBQKVBXHMX",rtseq=16,rtrep=.f.,left=53, top=175, caption="Udienze",;
    ToolTipText = "Se attivo: visualizza le udienze",;
    HelpContextID = 138431930,;
    cFormVar="w_UDIENZE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUDIENZE_1_25.RadioValue()
    return(iif(this.value =1,'U',;
    ' '))
  endfunc
  func oUDIENZE_1_25.GetRadio()
    this.Parent.oContained.w_UDIENZE = this.RadioValue()
    return .t.
  endfunc

  func oUDIENZE_1_25.SetRadio()
    this.Parent.oContained.w_UDIENZE=trim(this.Parent.oContained.w_UDIENZE)
    this.value = ;
      iif(this.Parent.oContained.w_UDIENZE=='U',1,;
      0)
  endfunc

  func oUDIENZE_1_25.mHide()
    with this.Parent.oContained
      return (!IsALT())
    endwith
  endfunc

  add object oSESSTEL_1_26 as StdCheck with uid="KYMNVXZYIW",rtseq=17,rtrep=.f.,left=184, top=135, caption="Sessioni telefoniche",;
    ToolTipText = "Se attivo: visualizza le sessioni telefoniche",;
    HelpContextID = 53367590,;
    cFormVar="w_SESSTEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSESSTEL_1_26.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func oSESSTEL_1_26.GetRadio()
    this.Parent.oContained.w_SESSTEL = this.RadioValue()
    return .t.
  endfunc

  func oSESSTEL_1_26.SetRadio()
    this.Parent.oContained.w_SESSTEL=trim(this.Parent.oContained.w_SESSTEL)
    this.value = ;
      iif(this.Parent.oContained.w_SESSTEL=='T',1,;
      0)
  endfunc

  add object oASSENZE_1_27 as StdCheck with uid="RPXCBGRCCL",rtseq=18,rtrep=.f.,left=184, top=155, caption="Assenze",;
    ToolTipText = "Se attivo: visualizza le assenze",;
    HelpContextID = 138387450,;
    cFormVar="w_ASSENZE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASSENZE_1_27.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oASSENZE_1_27.GetRadio()
    this.Parent.oContained.w_ASSENZE = this.RadioValue()
    return .t.
  endfunc

  func oASSENZE_1_27.SetRadio()
    this.Parent.oContained.w_ASSENZE=trim(this.Parent.oContained.w_ASSENZE)
    this.value = ;
      iif(this.Parent.oContained.w_ASSENZE=='A',1,;
      0)
  endfunc

  add object oDAINSPRE_1_28 as StdCheck with uid="AMMYJSOHVW",rtseq=19,rtrep=.f.,left=184, top=175, caption="Da inserimento prestazioni",;
    ToolTipText = "Se attivo: visualizza le attivit� generate dall'inserimento delle prestazioni",;
    HelpContextID = 31936901,;
    cFormVar="w_DAINSPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAINSPRE_1_28.RadioValue()
    return(iif(this.value =1,'Z',;
    ' '))
  endfunc
  func oDAINSPRE_1_28.GetRadio()
    this.Parent.oContained.w_DAINSPRE = this.RadioValue()
    return .t.
  endfunc

  func oDAINSPRE_1_28.SetRadio()
    this.Parent.oContained.w_DAINSPRE=trim(this.Parent.oContained.w_DAINSPRE)
    this.value = ;
      iif(this.Parent.oContained.w_DAINSPRE=='Z',1,;
      0)
  endfunc

  add object oMEMO_1_29 as StdCheck with uid="ELRSHFYRWO",rtseq=20,rtrep=.f.,left=360, top=135, caption="Note",;
    ToolTipText = "Se attivo: visualizza le note",;
    HelpContextID = 149549766,;
    cFormVar="w_MEMO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMEMO_1_29.RadioValue()
    return(iif(this.value =1,'M',;
    ' '))
  endfunc
  func oMEMO_1_29.GetRadio()
    this.Parent.oContained.w_MEMO = this.RadioValue()
    return .t.
  endfunc

  func oMEMO_1_29.SetRadio()
    this.Parent.oContained.w_MEMO=trim(this.Parent.oContained.w_MEMO)
    this.value = ;
      iif(this.Parent.oContained.w_MEMO=='M',1,;
      0)
  endfunc

  proc oMEMO_1_29.mAfter
    with this.Parent.oContained
      .w_AccodaNote='N'
    endwith
  endproc

  add object oDAFARE_1_30 as StdCheck with uid="JYTLCSHGGN",rtseq=21,rtrep=.f.,left=360, top=155, caption="Cose da fare",;
    ToolTipText = "Se attivo: visualizza le cose da fare",;
    HelpContextID = 218399178,;
    cFormVar="w_DAFARE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAFARE_1_30.RadioValue()
    return(iif(this.value =1,'D',;
    ' '))
  endfunc
  func oDAFARE_1_30.GetRadio()
    this.Parent.oContained.w_DAFARE = this.RadioValue()
    return .t.
  endfunc

  func oDAFARE_1_30.SetRadio()
    this.Parent.oContained.w_DAFARE=trim(this.Parent.oContained.w_DAFARE)
    this.value = ;
      iif(this.Parent.oContained.w_DAFARE=='D',1,;
      0)
  endfunc

  proc oDAFARE_1_30.mAfter
    with this.Parent.oContained
      .w_AccodaDaFare='N'
    endwith
  endproc

  add object oPARTECIP_1_31 as AH_SEARCHFLD with uid="EZLHTZDQUK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PARTECIP", cQueryName = "PARTECIP",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Cognome e nome o parte di essi",;
    HelpContextID = 4144966,;
   bGlobalFont=.t.,;
    Height=21, Width=409, Left=126, Top=205, InputMask=replicate('X',60)

  add object oFiltPerson_1_32 as AH_SEARCHFLD with uid="MYRJGKWLFV",rtseq=23,rtrep=.f.,;
    cFormVar = "w_FiltPerson", cQueryName = "FiltPerson",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Persona o parte di essa",;
    HelpContextID = 216958023,;
   bGlobalFont=.t.,;
    Height=21, Width=409, Left=126, Top=232, InputMask=replicate('X',60)


  add object oDISPONIB_1_33 as StdCombo with uid="MRAFDOVVBK",value=5,rtseq=24,rtrep=.f.,left=126,top=259,width=118,height=21;
    , ToolTipText = "Disponibilit�";
    , HelpContextID = 198923896;
    , cFormVar="w_DISPONIB",RowSource=""+"Occupato,"+"Per urgenze,"+"Libero,"+"Fuori sede,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDISPONIB_1_33.RadioValue()
    return(iif(this.value =1,2,;
    iif(this.value =2,3,;
    iif(this.value =3,1,;
    iif(this.value =4,4,;
    iif(this.value =5,0,;
    0))))))
  endfunc
  func oDISPONIB_1_33.GetRadio()
    this.Parent.oContained.w_DISPONIB = this.RadioValue()
    return .t.
  endfunc

  func oDISPONIB_1_33.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DISPONIB==2,1,;
      iif(this.Parent.oContained.w_DISPONIB==3,2,;
      iif(this.Parent.oContained.w_DISPONIB==1,3,;
      iif(this.Parent.oContained.w_DISPONIB==4,4,;
      iif(this.Parent.oContained.w_DISPONIB==0,5,;
      0)))))
  endfunc


  add object oPRIORITA_1_34 as StdCombo with uid="TPBXXBSCEW",value=4,rtseq=25,rtrep=.f.,left=310,top=259,width=118,height=21;
    , ToolTipText = "Priorit�";
    , HelpContextID = 150355913;
    , cFormVar="w_PRIORITA",RowSource=""+"Normale,"+"Urgente,"+"Scadenza termine,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRIORITA_1_34.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,3,;
    iif(this.value =3,4,;
    iif(this.value =4,0,;
    0)))))
  endfunc
  func oPRIORITA_1_34.GetRadio()
    this.Parent.oContained.w_PRIORITA = this.RadioValue()
    return .t.
  endfunc

  func oPRIORITA_1_34.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PRIORITA==1,1,;
      iif(this.Parent.oContained.w_PRIORITA==3,2,;
      iif(this.Parent.oContained.w_PRIORITA==4,3,;
      iif(this.Parent.oContained.w_PRIORITA==0,4,;
      0))))
  endfunc

  add object oNOTE_1_35 as AH_SEARCHMEMO with uid="YFIDICHIXZ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Annotazioni o parte di esse",;
    HelpContextID = 148925654,;
   bGlobalFont=.t.,;
    Height=21, Width=482, Left=53, Top=285, bDisableFrasimodello=.T.

  add object oATLOCALI_1_36 as AH_SEARCHFLD with uid="KNSKLUNXGJ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ATLOCALI", cQueryName = "ATLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� o parte di essa",;
    HelpContextID = 236581199,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=53, Top=314, InputMask=replicate('X',30)

  add object oFiltroWeb_1_37 as StdRadio with uid="NOISYYKDWQ",rtseq=28,rtrep=.f.,left=366, top=313, width=136,height=49;
    , ToolTipText = "Filtro su flag di pubblicazione su Web";
    , cFormVar="w_FiltroWeb", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oFiltroWeb_1_37.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Solo pubblicabili"
      this.Buttons(1).HelpContextID = 254872795
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo non pubblicabili"
      this.Buttons(2).HelpContextID = 254872795
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 254872795
      this.Buttons(3).Top=30
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Filtro su flag di pubblicazione su Web")
      StdRadio::init()
    endproc

  func oFiltroWeb_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFiltroWeb_1_37.GetRadio()
    this.Parent.oContained.w_FiltroWeb = this.RadioValue()
    return .t.
  endfunc

  func oFiltroWeb_1_37.SetRadio()
    this.Parent.oContained.w_FiltroWeb=trim(this.Parent.oContained.w_FiltroWeb)
    this.value = ;
      iif(this.Parent.oContained.w_FiltroWeb=='S',1,;
      iif(this.Parent.oContained.w_FiltroWeb=='N',2,;
      iif(this.Parent.oContained.w_FiltroWeb=='T',3,;
      0)))
  endfunc

  func oFiltroWeb_1_37.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oChkProm_1_38 as StdCheck with uid="XNJEQKNWQH",rtseq=29,rtrep=.f.,left=53, top=370, caption="Filtra le attivit� con promemoria inferiore a",;
    ToolTipText = "Se attivo: seleziona le attivit� con promemoria inferiore alla data selezionata",;
    HelpContextID = 252507430,;
    cFormVar="w_ChkProm", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oChkProm_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oChkProm_1_38.GetRadio()
    this.Parent.oContained.w_ChkProm = this.RadioValue()
    return .t.
  endfunc

  func oChkProm_1_38.SetRadio()
    this.Parent.oContained.w_ChkProm=trim(this.Parent.oContained.w_ChkProm)
    this.value = ;
      iif(this.Parent.oContained.w_ChkProm=='S',1,;
      0)
  endfunc

  add object oDATA_PROM_1_39 as StdField with uid="SWIUVBMUDH",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DATA_PROM", cQueryName = "DATA_PROM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 20159659,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=313, Top=370

  func oDATA_PROM_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ChkProm='S')
    endwith
   endif
  endfunc

  add object oORA_PROM_1_40 as StdField with uid="QRHLNNKGBQ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_ORA_PROM", cQueryName = "ORA_PROM",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora promemoria",;
    HelpContextID = 267993139,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=433, Top=370, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORA_PROM_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ChkProm='S')
    endwith
   endif
  endfunc

  func oORA_PROM_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORA_PROM) < 24)
    endwith
    return bRes
  endfunc

  add object oMIN_PROM_1_41 as StdField with uid="BUHARTDGYH",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MIN_PROM", cQueryName = "MIN_PROM",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti promemoria",;
    HelpContextID = 268044051,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=468, Top=370, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMIN_PROM_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ChkProm='S')
    endwith
   endif
  endfunc

  func oMIN_PROM_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MIN_PROM) < 60)
    endwith
    return bRes
  endfunc

  add object oAccodaPreavvisi_1_42 as StdCheck with uid="ZFZLPXQWWQ",rtseq=33,rtrep=.f.,left=53, top=407, caption="Accoda preavvisi",;
    ToolTipText = "Se attivo, accoda i preavvisi",;
    HelpContextID = 94816312,;
    cFormVar="w_AccodaPreavvisi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAccodaPreavvisi_1_42.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAccodaPreavvisi_1_42.GetRadio()
    this.Parent.oContained.w_AccodaPreavvisi = this.RadioValue()
    return .t.
  endfunc

  func oAccodaPreavvisi_1_42.SetRadio()
    this.Parent.oContained.w_AccodaPreavvisi=trim(this.Parent.oContained.w_AccodaPreavvisi)
    this.value = ;
      iif(this.Parent.oContained.w_AccodaPreavvisi=='S',1,;
      0)
  endfunc

  add object oAccodaNote_1_43 as StdCheck with uid="WKLVDZRVGV",rtseq=34,rtrep=.f.,left=53, top=430, caption="Accoda le note",;
    ToolTipText = "Se attivo, accoda le note",;
    HelpContextID = 263463755,;
    cFormVar="w_AccodaNote", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAccodaNote_1_43.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAccodaNote_1_43.GetRadio()
    this.Parent.oContained.w_AccodaNote = this.RadioValue()
    return .t.
  endfunc

  func oAccodaNote_1_43.SetRadio()
    this.Parent.oContained.w_AccodaNote=trim(this.Parent.oContained.w_AccodaNote)
    this.value = ;
      iif(this.Parent.oContained.w_AccodaNote=='S',1,;
      0)
  endfunc

  func oAccodaNote_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_MEMO='M')
    endwith
   endif
  endfunc

  func oAccodaNote_1_43.mHide()
    with this.Parent.oContained
      return (.w_MEMO='M')
    endwith
  endfunc

  add object oAccodaDaFare_1_44 as StdCheck with uid="ZOGZDBZCXV",rtseq=35,rtrep=.f.,left=172, top=407, caption="Accoda le cose da fare",;
    ToolTipText = "Se attivo, accoda le cose da fare",;
    HelpContextID = 256379449,;
    cFormVar="w_AccodaDaFare", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAccodaDaFare_1_44.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAccodaDaFare_1_44.GetRadio()
    this.Parent.oContained.w_AccodaDaFare = this.RadioValue()
    return .t.
  endfunc

  func oAccodaDaFare_1_44.SetRadio()
    this.Parent.oContained.w_AccodaDaFare=trim(this.Parent.oContained.w_AccodaDaFare)
    this.value = ;
      iif(this.Parent.oContained.w_AccodaDaFare=='S',1,;
      0)
  endfunc

  func oAccodaDaFare_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_DAFARE='D')
    endwith
   endif
  endfunc

  func oAccodaDaFare_1_44.mHide()
    with this.Parent.oContained
      return (.w_DAFARE='D')
    endwith
  endfunc

  add object oDATINI_Cose_1_45 as StdField with uid="QWTXKWCFBP",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DATINI_Cose", cQueryName = "DATINI_Cose",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione per note e cose da fare",;
    HelpContextID = 113977449,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=338, Top=407

  func oDATINI_Cose_1_45.mHide()
    with this.Parent.oContained
      return (.w_AccodaDaFare<>'S' AND .w_AccodaNote<>'S')
    endwith
  endfunc

  add object oDATFIN_Cose_1_46 as StdField with uid="DETMKSVCXW",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DATFIN_Cose", cQueryName = "DATFIN_Cose",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione per note e cose da fare",;
    HelpContextID = 192424041,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=433, Top=407

  func oDATFIN_Cose_1_46.mHide()
    with this.Parent.oContained
      return (.w_AccodaDaFare<>'S' AND .w_AccodaNote<>'S')
    endwith
  endfunc

  func oDATFIN_Cose_1_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI_Cose<=.w_DATFIN_Cose OR EMPTY(.w_DATFIN_Cose))
    endwith
    return bRes
  endfunc

  add object oVISUDI_1_47 as StdCheck with uid="QNKFDXRENN",rtseq=38,rtrep=.f.,left=172, top=430, caption="Dati udienza",;
    ToolTipText = "Se attivo, per ogni attivit� non di tipo Udienza/Da inser.prestaz./Assenza/Nota vengono riportate in stampa le date della prossima udienza da evadere e quella dell'ultima udienza evasa.",;
    HelpContextID = 164604074,;
    cFormVar="w_VISUDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVISUDI_1_47.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVISUDI_1_47.GetRadio()
    this.Parent.oContained.w_VISUDI = this.RadioValue()
    return .t.
  endfunc

  func oVISUDI_1_47.SetRadio()
    this.Parent.oContained.w_VISUDI=trim(this.Parent.oContained.w_VISUDI)
    this.value = ;
      iif(this.Parent.oContained.w_VISUDI=='S',1,;
      0)
  endfunc

  func oVISUDI_1_47.mHide()
    with this.Parent.oContained
      return (NOT ISALT() OR .w_DatiPratica<>'S')
    endwith
  endfunc

  add object oAnnotazioni_1_48 as StdCheck with uid="BOCSCACCBJ",rtseq=39,rtrep=.f.,left=53, top=475, caption="Note attivit�",;
    ToolTipText = "Se attivo, stampa le note attivit�",;
    HelpContextID = 22229087,;
    cFormVar="w_Annotazioni", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAnnotazioni_1_48.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAnnotazioni_1_48.GetRadio()
    this.Parent.oContained.w_Annotazioni = this.RadioValue()
    return .t.
  endfunc

  func oAnnotazioni_1_48.SetRadio()
    this.Parent.oContained.w_Annotazioni=trim(this.Parent.oContained.w_Annotazioni)
    this.value = ;
      iif(this.Parent.oContained.w_Annotazioni=='S',1,;
      0)
  endfunc

  add object oDatiPratica_1_49 as StdCheck with uid="MTQOGAYNLY",rtseq=40,rtrep=.f.,left=201, top=475, caption="Dati pratica",;
    ToolTipText = "Se attivo, stampa i dati della pratica",;
    HelpContextID = 267585478,;
    cFormVar="w_DatiPratica", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDatiPratica_1_49.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDatiPratica_1_49.GetRadio()
    this.Parent.oContained.w_DatiPratica = this.RadioValue()
    return .t.
  endfunc

  func oDatiPratica_1_49.SetRadio()
    this.Parent.oContained.w_DatiPratica=trim(this.Parent.oContained.w_DatiPratica)
    this.value = ;
      iif(this.Parent.oContained.w_DatiPratica=='S',1,;
      0)
  endfunc

  func oDatiPratica_1_49.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oDescagg_1_50 as StdCheck with uid="FSYHMFZRNQ",rtseq=41,rtrep=.f.,left=335, top=475, caption="Descrizione aggiuntiva",;
    ToolTipText = "Se attivo, stampa la descrizione aggiuntiva",;
    HelpContextID = 101741110,;
    cFormVar="w_Descagg", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDescagg_1_50.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDescagg_1_50.GetRadio()
    this.Parent.oContained.w_Descagg = this.RadioValue()
    return .t.
  endfunc

  func oDescagg_1_50.SetRadio()
    this.Parent.oContained.w_Descagg=trim(this.Parent.oContained.w_Descagg)
    this.value = ;
      iif(this.Parent.oContained.w_Descagg=='S',1,;
      0)
  endfunc


  add object OUT as cp_outputCombo with uid="ZLERXIVPWT",left=126, top=524, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 214835226


  add object oBtn_1_81 as StdButton with uid="MBXAIGGBZE",left=716, top=503, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 251043290;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_81.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_81.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_82 as StdButton with uid="ISDICPRYHO",left=766, top=503, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 151355462;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_82.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AZI_ZOOM as cp_szoombox with uid="XLHFHKADOY",left=542, top=16, width=280,height=187,;
    caption='AZI_ZOOM',;
   bGlobalFont=.t.,;
    cTable="AZIENDA",cZoomFile="GSAG_SAM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 228181843


  add object PART_ZOOM as cp_szoombox with uid="IIKPFDHECD",left=542, top=237, width=280,height=259,;
    caption='PART_ZOOM',;
   bGlobalFont=.t.,;
    cTable="DIPENDEN",cZoomFile="GSAG1MPP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Init,Esegui",;
    nPag=1;
    , HelpContextID = 119585771


  add object oPATIPRIS_1_103 as StdCombo with uid="OWYFYNUFGT",value=3,rtseq=64,rtrep=.f.,left=626,top=217,width=76,height=21;
    , HelpContextID = 266624841;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persone,"+"Risorse,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPATIPRIS_1_103.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'R',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oPATIPRIS_1_103.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_1_103.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',2,;
      iif(this.Parent.oContained.w_PATIPRIS=='',3,;
      0)))
  endfunc

  add object oStr_1_18 as StdString with uid="KLEZMLKSOB",Visible=.t., Left=17, Top=20,;
    Alignment=1, Width=32, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="UASYQLKIAH",Visible=.t., Left=-1, Top=568,;
    Alignment=0, Width=862, Height=18,;
    Caption="Attenzione! la seq. della calculation deve essere inferiore a FiltProm in modo che questo ricalcoli dopo l'assegnamento, cancellando i secondi!!!"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="JAHWAWKLCQ",Visible=.t., Left=22, Top=44,;
    Alignment=1, Width=27, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="JGUELPQNWG",Visible=.t., Left=8, Top=73,;
    Alignment=1, Width=41, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (!IsAlt() or .w_RISERVE='S')
    endwith
  endfunc

  add object oStr_1_71 as StdString with uid="UQSAZJMDFM",Visible=.t., Left=248, Top=260,;
    Alignment=1, Width=59, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="EWZSPAOTVW",Visible=.t., Left=132, Top=20,;
    Alignment=1, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="CTAHWBKCXZ",Visible=.t., Left=132, Top=44,;
    Alignment=1, Width=44, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="DAAABZBARA",Visible=.t., Left=208, Top=20,;
    Alignment=1, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="BTYOAEACQT",Visible=.t., Left=208, Top=44,;
    Alignment=1, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="GXOWXVAARV",Visible=.t., Left=-33, Top=106,;
    Alignment=1, Width=83, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="BSGKMHWZHM",Visible=.t., Left=6, Top=289,;
    Alignment=1, Width=44, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="QSNQCLUHKK",Visible=.t., Left=339, Top=73,;
    Alignment=1, Width=73, Height=18,;
    Caption="Tipo riserva:"  ;
  , bGlobalFont=.t.

  func oStr_1_79.mHide()
    with this.Parent.oContained
      return (.w_RISERVE<> 'S' )
    endwith
  endfunc

  add object oStr_1_80 as StdString with uid="GWTPKWGUGW",Visible=.t., Left=-33, Top=316,;
    Alignment=1, Width=83, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="HVQDALZAWB",Visible=.t., Left=9, Top=524,;
    Alignment=1, Width=112, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="LBTIYCWQWU",Visible=.t., Left=41, Top=260,;
    Alignment=1, Width=83, Height=18,;
    Caption="Disponibilit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="MMVSQAYOEN",Visible=.t., Left=459, Top=372,;
    Alignment=1, Width=7, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="NUKLUCBHUI",Visible=.t., Left=398, Top=374,;
    Alignment=1, Width=33, Height=18,;
    Caption="Ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="DNFJJECVYE",Visible=.t., Left=-1, Top=233,;
    Alignment=1, Width=125, Height=18,;
    Caption="Riferimento persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="KMXRIRFDUM",Visible=.t., Left=416, Top=407,;
    Alignment=1, Width=15, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  func oStr_1_90.mHide()
    with this.Parent.oContained
      return (.w_AccodaDaFare<>'S' AND .w_AccodaNote<>'S')
    endwith
  endfunc

  add object oStr_1_91 as StdString with uid="DACIAVJCND",Visible=.t., Left=314, Top=407,;
    Alignment=1, Width=22, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (.w_AccodaDaFare<>'S' AND .w_AccodaNote<>'S')
    endwith
  endfunc

  add object oStr_1_104 as StdString with uid="ZRYUFIMMYG",Visible=.t., Left=549, Top=218,;
    Alignment=1, Width=72, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_111 as StdString with uid="JZUSRGEJSQ",Visible=.t., Left=52, Top=205,;
    Alignment=1, Width=72, Height=18,;
    Caption="Partecipante:"  ;
  , bGlobalFont=.t.

  add object oBox_1_84 as StdBox with uid="EGABWCSCSO",left=44, top=464, width=465,height=47
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_sam','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
