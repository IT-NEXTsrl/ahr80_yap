* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bcd                                                        *
*              Calcola prezzo tariffario da attivita                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-17                                                      *
* Last revis.: 2014-10-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bcd",oParentObject,m.pParame)
return(i_retval)

define class tgsag_bcd as StdBatch
  * --- Local variables
  pParame = space(1)
  w_PADRE = .NULL.
  w_GESTIONE = .NULL.
  w_GRAPAT = space(1)
  w_NamePrg = space(10)
  w_NameClass = space(10)
  w_TACODART = space(20)
  w_TACODLIS = space(5)
  w_TADATATT = ctod("  /  /  ")
  w_TAAUTORI = space(10)
  w_TAGAZUFF = space(3)
  w_TAMANSION = space(5)
  w_TAFLVALO = space(1)
  w_TAIMPORT = 0
  w_TAPREZZO = 0
  w_TAPREMIN = 0
  w_TAPREMAX = 0
  w_TAQTAMOV = 0
  w_TACALDIR = space(1)
  w_TACOECAL = 0
  w_TATIPPRE = space(1)
  w_ONORARBI = space(1)
  w_FLPRPE = space(1)
  w_NOTIFICA = space(1)
  w_TAFLAPON = space(1)
  w_AUTCAL = space(10)
  w_PARASS = 0
  w_FLAMPA = space(1)
  w_DACODRES = space(5)
  w_DACODATT = space(20)
  w_COD1ATT = space(5)
  w_TAUNIMIS = space(3)
  w_NUMDECUNI = 0
  w_OK = .f.
  o_DACODRES = space(5)
  w_GSAL_BIA_Unimis = space(3)
  w_TATARTEM = space(1)
  w_TATARCON = 0
  w_CONTRACN = space(5)
  w_TIPCONCO = space(1)
  w_LISCOLCO = space(5)
  w_STATUSCO = space(1)
  w_TIPOPRAT = space(10)
  w_FLGIUD = space(1)
  w_CNMATOBB = space(1)
  w_CNASSCTP = space(1)
  w_CNCOMPLX = space(1)
  w_CNPROFMT = space(1)
  w_CNESIPOS = space(1)
  w_CNPERPLX = 0
  w_CNPERPOS = 0
  w_CNFLVMLQ = space(1)
  w_TAPARAM = space(1)
  w_CHKTEMP = space(1)
  w_TAQTAUM1 = 0
  w_ATLISACQ = space(5)
  w_CAUACQ = space(5)
  w_ATCODNOM = space(15)
  w_COND = .f.
  w_LUNIMIS = space(3)
  w_COND = .f.
  w_LUNIMIS = space(3)
  w_FLQRIO = space(1)
  w_FLPREV = space(1)
  w_ATTIPCLI = space(1)
  w_ATCODVAL = space(5)
  w_PRZVAC = space(1)
  w_CODCLI = space(1)
  w_CLIFOR = space(1)
  w_SCOLIS = space(1)
  w_ATDATINI = ctod("  /  /  ")
  w_DACODICE = space(41)
  w_OLDCODNOM = space(15)
  w_CAUDOC = space(5)
  w_PREZZO = 0
  w_AGGPRZ = .f.
  w_COSTO = 0
  w_TIPART = space(1)
  w_COD_LISTINO = space(5)
  w_CALPRZ = 0
  w_MMPZ_1 = 0
  w_MMPZ_2 = 0
  w_MMPZ_3 = 0
  w_ATTCOLIS = space(5)
  w_ATTSCLIS = space(5)
  w_ATTPROLI = space(5)
  w_ATTPROSC = space(5)
  w_FLGLIS = space(1)
  w_KEYLISTB = space(10)
  w_TIPRIS = space(1)
  w_COST_ORA = 0
  w_ATLISACQ = space(5)
  w_ATCODNOM = space(15)
  * --- WorkFile variables
  DIPENDEN_idx=0
  ART_ICOL_idx=0
  PRA_ENTI_idx=0
  UNIMIS_idx=0
  TIP_DOCU_idx=0
  LISTINI_idx=0
  PRA_TIPI_idx=0
  TAR_IFFE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- La presente routine � lanciata da:
    *     - GSAG_APR
    *     - GSAG_BGP
    *     - GSAG_MDA
    *     - GSAG_MPP
    *     - GSAG_MPR
    *     - GSAL_BIA
    *     - GSAL_MAP
    *     - GSPR_MDO
    *     - GSVE_MDV
    * --- 0= Aggiorna listini da dati di testata
    *     1= evento calcola
    *     2= evento calnot  (in alter ego esegue un calcolo supplementare nel prezzo
    *     3= Evento Aggiorna da gsag_mpr
    *     4=Evento Impprez da gsag_mpr
    *     5= cambio responsabile\partecipante no messaggio avviso cambio prezzo
    *     6= aggiorna listini da testata evento agglis
    *     7=aggiorna listini da dati di riga evento LisRig
    *     8=Calcola costo interno
    *     9= Ricalcola prezzi al cambiare del nominativo senza domanda e su tutte le righe
    *     I=ricalcolo prezzo da modulo import gsal_bia senza Msg nella calprat()
    * --- ATTENZIONE!!! Il controllo su VARTYPE .cprg � indispensabile in quanto GSAG_BCD � richiamato anche da GSAL_BIA
    this.w_PADRE = this.oparentobject
    if VARTYPE(this.w_PADRE.cPrg)="C"
      this.w_NamePrg = ALLTRIM(UPPER(this.w_PADRE.cPrg))
    else
      this.w_NamePrg = ""
    endif
    * --- Tutti gli oggetti hanno propriet� CLASS ma testiamo per sicurezza
    if VARTYPE(this.w_PADRE.class)="C"
      this.w_NameClass = UPPER(this.w_PADRE.class)
    else
      this.w_NameClass = ""
    endif
    if !EMPTY(this.w_NamePrg) AND NOT (this.w_NamePrg=="GSVE_MDV" ) AND NOT (this.w_NamePrg=="GSAG_APR" )
      this.w_GESTIONE = this.oparentobject.oparentobject
    endif
    * --- Codice Prestazione
    * --- Codice listino
    * --- Data inizio attivit�
    * --- Autorit�
    * --- Gazzetta Ufficiale
    * --- Mansione
    * --- Check valore (definito/indeterminabile/indeterminabile di particolare importanza)
    * --- Valore Pratica
    * --- Prezzo unitario
    * --- Prezzo minimo
    * --- Prezzo massimo
    * --- Quantit�
    * --- Calcolo diritti/onorari
    * --- Coefficiente di calcolo, % di sconto, % di maggiorazione
    * --- Tipologia prestazione della prestazione
    * --- Onorario arbitrale
    * --- Flag prezzo prestazione in percentuale
    * --- Notificazione
    * --- Check applica onorari forensi in base alla data di inserimento
    * --- Autorit� per calcolo tariffa
    * --- Parti in assistenza
    * --- Flag non applicare maggiorazione parti in assistenza
    * --- Codice Responsabile
    * --- Codice Prestazione
    * --- Codice Articolo
    * --- Numero decimali per valori unitari
    this.w_OK = .T.
    * --- Unit� di misura per GSAL_BIA
    * --- Tipo di tariffazione a tempo
    * --- Importo della tariffa concordata
    * --- Contratto della pratica
    * --- Tipo di contratto
    * --- Listino collegato al contratto
    * --- Stato del contratto
    * --- Codice listino
    if TYPE("this.w_PADRE.w_CHKTEMP")="C"
      this.w_CHKTEMP = this.w_PADRE.w_CHKTEMP
    else
      this.w_CHKTEMP = ""
    endif
    do case
      case (EMPTY(this.w_NamePrg) AND !this.w_NameClass=="TGSAG_BGP") OR this.w_NamePrg=="GSVE_MDV"
        this.w_DACODRES = this.w_PADRE.w_MVCODRES
        this.w_DACODATT = this.w_PADRE.w_MVCODICE
        this.w_COD1ATT = this.w_PADRE.w_MVCODART
        this.w_TAPREZZO = this.w_PADRE.w_MVPREZZO
        this.w_TAPREMIN = this.w_PADRE.w_DEPREMIN
        this.w_TAPREMAX = this.w_PADRE.w_DEPREMAX
        this.w_TAQTAMOV = iif(this.w_PADRE.w_MVQTAMOV=0 and isalt(),1,this.w_PADRE.w_MVQTAMOV)
        this.w_TAUNIMIS = this.w_PADRE.w_MVUNIMIS
        this.w_TACODLIS = this.w_PADRE.w_MVTCOLIS
        this.w_TAQTAUM1 = CALQTA(this.w_PADRE.w_MVQTAMOV,this.w_PADRE.w_MVUNIMIS,this.w_PADRE.w_UNMIS2,this.w_PADRE.w_OPERAT, this.w_PADRE.w_MOLTIP, this.w_PADRE.w_FLUSEP, this.w_PADRE.w_FLFRAZ, this.w_PADRE.w_MODUM2, " ", this.w_PADRE.w_UNMIS3, this.w_PADRE.w_OPERA3, this.w_PADRE.w_MOLTI3, IIF(this.w_PADRE.w_FLFRAZ="S" OR this.w_CHKTEMP="S", 10, .F.), IIF(this.w_PADRE.w_FLFRAZ="S" OR this.w_CHKTEMP="S", "D", .F.))
        this.w_TAQTAUM1 = iif(this.w_TAQTAUM1=0,1,this.w_TAQTAUM1)
        this.w_NUMDECUNI = this.w_PADRE.w_DECUNI
      case this.w_NamePrg=="GSPR_MDO"
        this.w_GESTIONE = this.oparentobject.oparentobject
        this.w_DACODRES = this.w_GESTIONE.w_OFCODRES
        this.w_DACODATT = this.w_PADRE.w_ODCODICE
        this.w_COD1ATT = this.w_PADRE.w_ODCODART
        this.w_TAPREZZO = this.w_PADRE.w_ODPREZZO
        this.w_TAPREMIN = this.w_PADRE.w_ODPREMIN
        this.w_TAPREMAX = this.w_PADRE.w_ODPREMAX
        this.w_TAQTAMOV = iif(this.w_PADRE.w_ODQTAMOV=0 and isalt(),1,this.w_PADRE.w_ODQTAMOV)
        this.w_TAUNIMIS = this.w_PADRE.w_ODUNIMIS
        this.w_TACODLIS = this.w_GESTIONE.w_OFCODLIS
        this.w_TAQTAUM1 = CALQTA(this.w_PADRE.w_ODQTAMOV,this.w_PADRE.w_ODUNIMIS,this.w_PADRE.w_UNMIS2,this.w_PADRE.w_OPERAT, this.w_PADRE.w_MOLTIP, this.w_PADRE.w_FLUSEP, this.w_PADRE.w_FLFRAZ, this.w_PADRE.w_MODUM2, " ", this.w_PADRE.w_UNMIS3, this.w_PADRE.w_OPERA3, this.w_PADRE.w_MOLTI3, IIF(this.w_PADRE.w_FLFRAZ="S" OR this.w_CHKTEMP="S", 10, .F.), IIF(this.w_PADRE.w_FLFRAZ="S" OR this.w_CHKTEMP="S", "D", .F.))
        this.w_TAQTAUM1 = iif(this.w_TAQTAUM1=0,1,this.w_TAQTAUM1)
        this.w_NUMDECUNI = this.w_PADRE.w_DECUNI
      case this.w_NamePrg=="GSAG_APR" OR this.w_NameClass=="TGSAG_BGP"
        * --- Prestazioni da manutenzione
        this.w_DACODRES = this.w_PADRE.w_PRCODRES
        this.w_DACODATT = this.w_PADRE.w_PRCODATT
        this.w_COD1ATT = this.w_PADRE.w_PRCODART
        this.w_TAPREZZO = this.w_PADRE.w_PRPREZZO
        this.w_TAPREMIN = this.w_PADRE.w_PRPREMIN
        this.w_TAPREMAX = this.w_PADRE.w_PRPREMAX
        this.w_TAQTAMOV = iif(this.w_PADRE.w_PRQTAMOV=0 and isalt(),1,this.w_PADRE.w_PRQTAMOV)
        this.w_TAUNIMIS = this.w_PADRE.w_PRUNIMIS
        this.w_TACODLIS = this.w_PADRE.w_PRCODLIS
        this.w_TAQTAUM1 = CALQTA(this.w_PADRE.w_PRQTAMOV,this.w_PADRE.w_PRUNIMIS,this.w_PADRE.w_UNMIS2,this.w_PADRE.w_OPERAT, this.w_PADRE.w_MOLTIP, this.w_PADRE.w_FLUSEP, this.w_PADRE.w_FLFRAZ1, this.w_PADRE.w_MODUM2, " ", this.w_PADRE.w_UNMIS3, this.w_PADRE.w_OPERA3, this.w_PADRE.w_MOLTI3, IIF(this.w_PADRE.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", 10, .F.), IIF(this.w_PADRE.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", "D", .F.))
        this.w_TAQTAUM1 = iif(this.w_TAQTAUM1=0,1,this.w_TAQTAUM1)
        this.w_NUMDECUNI = this.w_PADRE.w_DECUNI
        this.w_ATCODVAL = this.w_PADRE.w_PRCODVAL
      case this.w_NamePrg=="GSAG_MPR" OR this.w_NamePrg=="GSAL_MAP" OR this.w_NamePrg=="GSAG_MPP"
        * --- Prestazioni rapide
        this.w_DACODRES = this.w_PADRE.w_DACODRES
        this.w_DACODATT = this.w_PADRE.w_DACODATT
        this.w_DACODICE = this.w_PADRE.w_DACODICE
        this.w_COD1ATT = IIF(Isahe(),this.w_PADRE.w_ECOD1ATT,this.w_PADRE.w_RCOD1ATT)
        this.w_TAPREZZO = this.w_PADRE.w_DAPREZZO
        this.w_TAPREMIN = this.w_PADRE.w_DAPREMIN
        this.w_TAPREMAX = this.w_PADRE.w_DAPREMAX
        if !EMPTY(this.w_NamePrg) AND (this.w_NamePrg=="GSAG_MPR" OR this.w_NamePrg=="GSAG_MPP")
          if Isahe()
            this.w_COND = this.w_padre.w_DACODICE <>this.w_padre.o_DACODICE
          else
            this.w_COND = this.w_padre.w_DACODATT <>this.w_padre.o_DACODATT
          endif
          this.w_PADRE.w_PRQTAMOV = iif(this.w_PADRE.w_PRQTAMOV=0,IIF(this.w_PADRE.w_TIPART="DE", 0, 1),this.w_PADRE.w_PRQTAMOV)
          this.w_TAQTAMOV = this.w_PADRE.w_PRQTAMOV
          this.w_TAUNIMIS = IIF(this.w_COND,SPACE(3),this.w_PADRE.w_PRUNIMIS)
          this.w_LUNIMIS = this.w_PADRE.w_PRUNIMIS
        else
          * --- se cambio articolo o responsabile devo forzare la rilettura della unit� di misura
          if Isahe()
            this.w_COND = this.w_padre.w_DACODICE <>this.w_padre.o_DACODICE OR this.w_padre.w_DACODRES <>this.w_padre.o_DACODRES
          else
            this.w_COND = this.w_padre.w_DACODATT <>this.w_padre.o_DACODATT
          endif
          this.w_TAQTAMOV = this.w_PADRE.w_DAQTAMOV
          this.w_TAUNIMIS = IIF(this.w_COND,SPACE(3),this.w_PADRE.w_DAUNIMIS)
          this.w_LUNIMIS = this.w_PADRE.w_DAUNIMIS
        endif
        if IsAlt()
          this.w_TACODLIS = IIF(this.w_PADRE.w_CCODLIS<>this.w_PADRE.o_CCODLIS,this.w_PADRE.w_CCODLIS,this.w_PADRE.w_PRCODLIS)
        else
          this.w_TACODLIS = IIF(EMPTY(this.w_PADRE.w_PRCODLIS),this.w_PADRE.w_CODLIS,this.w_PADRE.w_PRCODLIS)
        endif
        this.w_CAUDOC = this.w_PADRE.w_CAUDOC
        this.w_CAUACQ = this.w_GESTIONE.w_CAUACQ
        this.w_ATTIPCLI = this.w_PADRE.w_NOTIPCLI
        this.w_PRZVAC = this.w_PADRE.w_PRZVAC
        this.w_CODCLI = this.w_PADRE.w_NOCODCLI
        this.w_CLIFOR = this.w_PADRE.w_NOTIPCLI
        this.w_SCOLIS = this.w_PADRE.w_SCOLIS
        this.w_ATCODVAL = this.w_PADRE.w_PRCODVAL
        this.w_NUMDECUNI = this.w_PADRE.w_DECUNI
        this.w_ATLISACQ = this.w_PADRE.w_DALISACQ
        if this.w_NamePrg=="GSAG_MPR" OR this.w_NamePrg=="GSAG_MPP"
          this.w_ATDATINI = this.w_PADRE.w_PR__DATA
          this.w_TAQTAUM1 = CALQTA(this.w_PADRE.w_PRQTAMOV,this.w_PADRE.w_PRUNIMIS,this.w_PADRE.w_UNMIS2,this.w_PADRE.w_OPERAT, this.w_PADRE.w_MOLTIP, this.w_PADRE.w_FLUSEP, this.w_PADRE.w_FLFRAZ1, this.w_PADRE.w_MODUM2, " ", this.w_PADRE.w_UNMIS3, this.w_PADRE.w_OPERA3, this.w_PADRE.w_MOLTI3, IIF(this.w_PADRE.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", 10, .F.), IIF(this.w_PADRE.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", "D", .F.))
        else
          * --- GSAL_MAP
          this.w_ATDATINI = this.w_PADRE.w_DADATPRE
          this.w_TAQTAUM1 = CALQTA(this.w_PADRE.w_DAQTAMOV,this.w_PADRE.w_DAUNIMIS,this.w_PADRE.w_UNMIS2,this.w_PADRE.w_OPERAT, this.w_PADRE.w_MOLTIP, this.w_PADRE.w_FLUSEP, this.w_PADRE.w_FLFRAZ1, this.w_PADRE.w_MODUM2, " ", this.w_PADRE.w_UNMIS3, this.w_PADRE.w_OPERA3, this.w_PADRE.w_MOLTI3, IIF(this.w_PADRE.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", 10, .F.), IIF(this.w_PADRE.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", "D", .F.))
        endif
        * --- Applico listini di testata
        this.w_ATTPROSC = this.w_PADRE.w_DAPROSCO
        this.w_ATTPROLI = this.w_PADRE.w_DAPROLIS
        this.w_ATTSCLIS = this.w_PADRE.w_DASCOLIS
        this.w_ATTCOLIS = this.w_PADRE.w_DACODLIS
        this.w_ATCODNOM = this.w_PADRE.w_DACODNOM
        this.w_OLDCODNOM = this.w_PADRE.o_DACODNOM
        this.w_FLGLIS = "N"
      otherwise
        this.w_DACODRES = this.w_PADRE.w_DACODRES
        this.w_DACODATT = this.w_PADRE.w_DACODATT
        this.w_DACODICE = this.w_PADRE.w_DACODICE
        this.w_COD1ATT = IIF(Isahe(),this.w_PADRE.w_ECOD1ATT,this.w_PADRE.w_RCOD1ATT)
        this.w_TAPREZZO = this.w_PADRE.w_DAPREZZO
        this.w_TAPREMIN = this.w_PADRE.w_DAPREMIN
        this.w_TAPREMAX = this.w_PADRE.w_DAPREMAX
        if !EMPTY(this.w_NamePrg) AND (this.w_NamePrg=="GSAG_MPR" OR this.w_NamePrg=="GSAG_MPP")
          if Isahe()
            this.w_COND = this.w_padre.w_DACODICE <>this.w_padre.o_DACODICE
          else
            this.w_COND = this.w_padre.w_DACODATT <>this.w_padre.o_DACODATT
          endif
          if NOT ISALT()
            this.w_PADRE.w_PRQTAMOV = iif(this.w_PADRE.w_PRQTAMOV=0,IIF(this.w_PADRE.w_TIPART="DE", 0, 1),this.w_PADRE.w_PRQTAMOV)
          endif
          this.w_TAQTAMOV = this.w_PADRE.w_PRQTAMOV
          this.w_TAUNIMIS = IIF(this.w_COND,SPACE(3),this.w_PADRE.w_PRUNIMIS)
          this.w_LUNIMIS = this.w_PADRE.w_PRUNIMIS
        else
          * --- se cambio articolo o responsabile devo forzare la rilettura della unit� di misura
          if Isahe()
            this.w_COND = this.w_padre.w_DACODICE <>this.w_padre.o_DACODICE OR this.w_padre.w_DACODRES <>this.w_padre.o_DACODRES
          else
            this.w_COND = this.w_padre.w_DACODATT <>this.w_padre.o_DACODATT
          endif
          this.w_TAQTAMOV = this.w_PADRE.w_DAQTAMOV
          this.w_TAUNIMIS = IIF(this.w_COND,SPACE(3),this.w_PADRE.w_DAUNIMIS)
          this.w_LUNIMIS = this.w_PADRE.w_DAUNIMIS
        endif
        * --- Attivit�
        this.w_GESTIONE = this.oparentobject.oparentobject
        this.w_ATLISACQ = this.w_GESTIONE.w_ATLISACQ
        this.w_CAUDOC = this.w_GESTIONE.w_CAUDOC
        this.w_CAUACQ = this.w_GESTIONE.w_CAUACQ
        this.w_ATCODVAL = this.w_GESTIONE.w_ATCODVAL
        this.w_ATDATINI = IIF(Isahe(),this.w_GESTIONE.w_ATDATDOC,this.w_GESTIONE.w_DATINI)
        this.w_ATTIPCLI = this.w_GESTIONE.w_ATTIPCLI
        this.w_PRZVAC = this.w_GESTIONE.w_PRZVAC
        this.w_CODCLI = this.w_GESTIONE.w_CODCLI
        this.w_SCOLIS = this.w_GESTIONE.w_SCOLIS
        this.w_FLGLIS = this.w_GESTIONE.w_FLGLIS
        this.w_TACODLIS = this.w_GESTIONE.w_atcodlis
        this.w_NUMDECUNI = this.w_GESTIONE.w_DECUNI
        this.o_DACODRES = this.w_PADRE.o_DACODRES
        this.w_TAQTAUM1 = CALQTA(this.w_PADRE.w_DAQTAMOV,this.w_PADRE.w_DAUNIMIS,this.w_PADRE.w_UNMIS2,this.w_PADRE.w_OPERAT, this.w_PADRE.w_MOLTIP, this.w_PADRE.w_FLUSEP, this.w_PADRE.w_FLFRAZ1, this.w_PADRE.w_MODUM2, " ", this.w_PADRE.w_UNMIS3, this.w_PADRE.w_OPERA3, this.w_PADRE.w_MOLTI3, IIF(this.w_PADRE.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", 10, .F.), IIF(this.w_PADRE.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", "D", .F.))
        * --- Devo rileggere le informazioni perch� in base all'evento di lancio 
        *     potrebbero non essere ancora aggiornate
        if this.pParame="5"
          * --- Applico in anticipo valorizzazioni per poter calcolare prezzo nel caso di prestazione predefinita
          if NOT EMPTY(this.w_PADRE.w_SERPER) 
            if Isahe()
              if EMPTY(this.w_DACODICE)
                this.w_DACODICE = this.w_PADRE.w_SERPER
                this.w_PADRE.w_DACODICE = this.w_DACODICE
              endif
            else
              if EMPTY(this.w_DACODATT)
                this.w_DACODATT = this.w_PADRE.w_SERPER
                this.w_PADRE.w_DACODATT = this.w_DACODATT
              endif
            endif
          endif
          if isalt()
            this.w_COD1ATT = iif(Not Empty(this.w_PADRE.w_SERPER),this.w_PADRE.w_SERPER,this.w_PADRE.w_RCACODART)
          endif
        endif
        this.w_ATTPROSC = this.w_GESTIONE.w_ATTPROSC
        this.w_ATTPROLI = this.w_GESTIONE.w_ATTPROLI
        this.w_ATTSCLIS = this.w_GESTIONE.w_ATTSCLIS
        this.w_ATTCOLIS = this.w_GESTIONE.w_ATTCOLIS
        this.w_ATCODNOM = this.w_GESTIONE.w_ATCODNOM
        this.w_OLDCODNOM = this.w_GESTIONE.o_ATCODNOM
    endcase
    if this.pParame="5"
      * --- Lettura costo orario del partecipante
      * --- Read from DIPENDEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DPCOSORA"+;
          " from "+i_cTable+" DIPENDEN where ";
              +"DPCODICE = "+cp_ToStrODBC(this.w_DACODRES);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DPCOSORA;
          from (i_cTable) where;
              DPCODICE = this.w_DACODRES;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COST_ORA = NVL(cp_ToDate(_read_.DPCOSORA),cp_NullValue(_read_.DPCOSORA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_COST_ORA = this.w_PADRE.w_COST_ORA
    endif
    if ! Isalt()
      if empty(this.w_TAUNIMIS)
        * --- Non � ancora scattato li link 
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_COD1ATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1;
            from (i_cTable) where;
                ARCODART = this.w_COD1ATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TAUNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        this.w_TAUNIMIS = this.w_LUNIMIS
      endif
      * --- Read from UNIMIS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.UNIMIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "UMFLTEMP"+;
          " from "+i_cTable+" UNIMIS where ";
              +"UMCODICE = "+cp_ToStrODBC(this.w_TAUNIMIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          UMFLTEMP;
          from (i_cTable) where;
              UMCODICE = this.w_TAUNIMIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CHKTEMP = NVL(cp_ToDate(_read_.UMFLTEMP),cp_NullValue(_read_.UMFLTEMP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    do case
      case Isahr()
        this.w_TIPRIS = this.w_PADRE.w_TIPRIS
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLANAL,TDFLQRIO,TDFLPREV,TDPRZVAC"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLANAL,TDFLQRIO,TDFLPREV,TDPRZVAC;
            from (i_cTable) where;
                TDTIPDOC = this.w_CAUDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          w_FLDANA = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
          this.w_FLQRIO = NVL(cp_ToDate(_read_.TDFLQRIO),cp_NullValue(_read_.TDFLQRIO))
          this.w_FLPREV = NVL(cp_ToDate(_read_.TDFLPREV),cp_NullValue(_read_.TDFLPREV))
          this.w_PRZVAC = NVL(cp_ToDate(_read_.TDPRZVAC),cp_NullValue(_read_.TDPRZVAC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_NamePrg <>"GSVE_MDV" AND (this.pParame $ "1-I" or this.pParame="3" or this.pParame="4" or this.pParame="6" or this.pParame="9" or this.pParame="0" or (this.pParame="5" and this.w_TAPREZZO=0) )
          this.w_TAQTAMOV = IIF(this.w_TAQTAMOV=0,IIF(this.w_PADRE.w_TIPART="DE", 0, 1),this.w_TAQTAMOV)
          if empty(this.w_CODCLI) or this.w_ATCODNOM<> this.w_OLDCODNOM
            this.w_PADRE.Set("w_DACONCOD", SPACE(15), .t., .t.)     
          endif
          if not empty(this.w_DACODATT)
            if !(this.pParame="3" or this.pParame="4" or this.pParame="6" or this.pParame="9" or this.pParame="0") AND this.w_TAPREZZO<>0
              this.w_AGGPRZ = Ah_yesno("Si desidera aggiornare il prezzo su riga?")
            else
              this.w_AGGPRZ = .T.
            endif
          endif
          if this.w_AGGPRZ
            * --- Nel caso di or pParame='3' or pParame='4'  pParame='6' non devo dare msg di conferma perch� gi� previsto
            *     nel chiamante.
            DECLARE L_ArrRet (16,1)
            this.w_PREZZO = gsar_bpz(this,this.w_DACODATT,this.w_CODCLI,"C",this.w_TACODLIS,this.w_TAQTAMOV,this.w_ATCODVAL,IIF(Vartype(this.w_ATDATINI)="T",ttod(this.w_ATDATINI),this.w_ATDATINI),this.w_TAUNIMIS, iif(this.pParame="Z",REPL("X",16),this.w_PADRE.w_DACONCOD))
            this.w_PADRE.Set("w_DAPREZZO", this.w_PREZZO, .t., .t.)     
            this.w_PADRE.Set("w_DASCONT1", L_ArrRet[1,1], .t., .t.)     
            if this.oParentObject.w_NUMSCO>1
              this.w_PADRE.Set("w_DASCONT2", L_ArrRet[2,1], .t., .t.)     
            endif
            if this.oParentObject.w_NUMSCO>2
              this.w_PADRE.Set("w_DASCONT3", L_ArrRet[3,1], .t., .t.)     
            endif
            if this.oParentObject.w_NUMSCO>3
              this.w_PADRE.Set("w_DASCONT4", L_ArrRet[4,1], .t., .t.)     
            endif
            this.w_PADRE.Set("w_DACONCOD", L_ArrRet[9,1], .t., .t.)     
            RELEASE L_ArrRet
          endif
        endif
        if this.pParame<>"6" and (this.pParame="1" OR this.pParame="5" or this.pParame="9" or this.pParame="8" ) 
          if this.pParame="5" and this.w_COST_ORA<>0
            * --- Ho cambiato il responsabile il mio costo dui partenza � quello letto dal partecipante
            this.w_PADRE.Set("w_LICOSTO", this.w_COST_ORA, .t., .t.)     
          else
            * --- mantengo costo presente sulla riga
            this.w_PADRE.Set("w_LICOSTO", this.w_PADRE.w_DACOSUNI, .t., .t.)     
          endif
          if ((this.w_COST_ORA=0 AND this.w_TIPRIS $ "P-R") and (this.pParame $ "1-I" or this.pParame="8" or this.pParame="5" )) 
 
 
            if this.w_PADRE.w_DACOSUNI=0 or this.pParame="8" or Ah_Yesno("Si desidera aggiornare il costo di riga?")
              if Not empty(this.w_ATLISACQ) 
                * --- Aggiungo listino acquisti
                DECLARE L_ArrRet (16,1)
                this.w_COSTO = gsar_bpz(this,this.w_DACODATT,this.w_CODCLI,"C",this.w_ATLISACQ,this.w_TAQTAMOV,this.w_ATCODVAL,IIF(Vartype(this.w_ATDATINI)="T",ttod(this.w_ATDATINI),this.w_ATDATINI),this.w_TAUNIMIS, REPL("X",16))
                this.w_PADRE.Set("w_LICOSTO", this.w_COSTO, .t., .t.)     
                RELEASE L_ArrRet
              else
                this.w_PADRE.Set("w_LICOSTO", 0, .t., .t.)     
              endif
            endif
          endif
        endif
        if EMPTY(this.w_NamePrg) OR this.w_NamePrg=="GSVE_MDV"
          this.w_PADRE.w_MVPREZZO = 0
        endif
      case Isalt()
        * --- Codice prestazione
        if this.w_TAPREZZO<>0 and this.pParame="5" and (VARTYPE(this.w_PADRE.w_SERPRE)="U" OR (VARTYPE(this.w_PADRE.w_SERPRE)="C" and empty(this.w_PADRE.w_SERPRE))) and Ah_yesno("Vuoi mantenere la tariffa?")
          if this.w_NamePrg=="GSAG_MDA" 
            * --- Posta ad 'N' per non far eseguire la lettura della tariffa concordata all'interno di GSAG_MDA
            this.w_PADRE.w_OKTARCON = "N"
          endif
          i_retcode = 'stop'
          return
        endif
        if NOT EMPTY(NVL(this.w_TACODLIS,""))
          this.w_TACODART = this.w_COD1ATT
          * --- Lettura della tipologia prestazione della prestazione
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARPRESTA,ARSTASUP,ARSTACOD,ARFLPRPE,ARUNMIS1,ARTIPART,ARPARAME"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_TACODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARPRESTA,ARSTASUP,ARSTACOD,ARFLPRPE,ARUNMIS1,ARTIPART,ARPARAME;
              from (i_cTable) where;
                  ARCODART = this.w_TACODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TATIPPRE = NVL(cp_ToDate(_read_.ARPRESTA),cp_NullValue(_read_.ARPRESTA))
            this.w_ONORARBI = NVL(cp_ToDate(_read_.ARSTASUP),cp_NullValue(_read_.ARSTASUP))
            this.w_NOTIFICA = NVL(cp_ToDate(_read_.ARSTACOD),cp_NullValue(_read_.ARSTACOD))
            this.w_FLPRPE = NVL(cp_ToDate(_read_.ARFLPRPE),cp_NullValue(_read_.ARFLPRPE))
            this.w_GSAL_BIA_Unimis = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
            this.w_TAPARAM = NVL(cp_ToDate(_read_.ARPARAME),cp_NullValue(_read_.ARPARAME))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          do case
            case (EMPTY(this.w_NamePrg) AND !this.w_NameClass=="TGSAG_BGP") OR this.w_NamePrg=="GSAG_MPR"
              * --- Prestazioni rapide
              this.w_TAFLAPON = this.w_PADRE.w_flapon
            case this.w_NamePrg=="GSVE_MDV" OR this.w_NamePrg=="GSAG_APR" OR this.w_NameClass=="TGSAG_BGP"
              * --- Documenti di vendita or prestazioni da manutenzione
              this.w_TAFLAPON = this.w_PADRE.w_CNFLAPON
            case this.w_NamePrg=="GSPR_MDO"
              this.w_TAFLAPON = this.w_GESTIONE.w_OFFLAPON
            otherwise
              * --- Attivit�
              this.w_TAFLAPON = this.w_GESTIONE.w_atflapon
              if this.w_PADRE.w_DAQTAMOV=0 
                this.w_PADRE.w_TIPART = this.w_TIPART
                this.w_PADRE.w_DAQTAMOV = IIF(this.w_TIPART="DE", 0, 1)
                this.w_TAQTAMOV = this.w_PADRE.w_DAQTAMOV
                this.w_TAQTAUM1 = CALQTA(this.w_PADRE.w_DAQTAMOV,this.w_PADRE.w_DAUNIMIS,this.w_PADRE.w_UNMIS2,this.w_PADRE.w_OPERAT, this.w_PADRE.w_MOLTIP, this.w_PADRE.w_FLUSEP, this.w_PADRE.w_FLFRAZ1, this.w_PADRE.w_MODUM2, " ", this.w_PADRE.w_UNMIS3, this.w_PADRE.w_OPERA3, this.w_PADRE.w_MOLTI3, IIF(this.w_PADRE.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", 10, .F.), IIF(this.w_PADRE.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", "D", .F.))
              endif
          endcase
          do case
            case (this.w_NamePrg=="GSAG_MPR" OR this.w_NamePrg=="GSAL_MAP" OR this.w_NamePrg=="GSAG_APR") OR this.w_NameClass=="TGSAG_BGP"
              * --- Prestazioni rapide o prestazioni da manutenzione
              this.w_TADATATT = this.w_PADRE.w_PR__DATA
            case EMPTY(this.w_NamePrg) OR this.w_NamePrg=="GSVE_MDV"
              * --- Documenti di vendita
              this.w_TADATATT = this.w_PADRE.w_MVDATREG
            case !EMPTY(this.w_NamePrg) and this.w_NamePrg=="GSPR_MDO"
              this.w_TADATATT = this.w_GESTIONE.w_OFDATDOC
            otherwise
              * --- Attivit�
              this.w_TADATATT = this.w_GESTIONE.w_datini
          endcase
          do case
            case (EMPTY(this.w_NamePrg) AND !this.w_NameClass=="TGSAG_BGP") OR this.w_NamePrg=="GSAG_MPR"
              * --- Prestazioni rapide
              this.w_TAAUTORI = this.w_PADRE.w_ENTE
            case this.w_NamePrg=="GSVE_MDV" OR this.w_NamePrg=="GSAG_APR" OR this.w_NameClass=="TGSAG_BGP"
              * --- Documenti di vendita
              this.w_TAAUTORI = this.w_PADRE.w_CN__ENTE
            case this.w_NamePrg=="GSPR_MDO"
              this.w_TAAUTORI = this.w_GESTIONE.w_OF__ENTE
            otherwise
              * --- Attivit� o GSAL_MAP
              this.w_TAAUTORI = this.w_GESTIONE.w_at__ente
          endcase
          this.w_AUTCAL = ""
          if NOT EMPTY(this.w_TAAUTORI)
            * --- Legge se sulle tariffe della voce in esame c'� una riga con listino e autorit� pari a quelli della pratica 
            *     (questo � stato introdotto con il tariffario 2014)
            * --- Read from TAR_IFFE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TAR_IFFE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TAR_IFFE_idx,2],.t.,this.TAR_IFFE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TACODLIS"+;
                " from "+i_cTable+" TAR_IFFE where ";
                    +"TACODART = "+cp_ToStrODBC(this.w_TACODART);
                    +" and TACODLIS = "+cp_ToStrODBC(this.w_TACODLIS);
                    +" and TAAUTORI = "+cp_ToStrODBC(this.w_TAAUTORI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TACODLIS;
                from (i_cTable) where;
                    TACODART = this.w_TACODART;
                    and TACODLIS = this.w_TACODLIS;
                    and TAAUTORI = this.w_TAAUTORI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_COD_LISTINO = NVL(cp_ToDate(_read_.TACODLIS),cp_NullValue(_read_.TACODLIS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- In caso affermativo viene forzata l'autorit� con quella della pratica
            if NOT EMPTY(this.w_COD_LISTINO)
              this.w_AUTCAL = this.w_TAAUTORI
            else
              * --- Read from PRA_ENTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2],.t.,this.PRA_ENTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "EPCALTAR"+;
                  " from "+i_cTable+" PRA_ENTI where ";
                      +"EPCODICE = "+cp_ToStrODBC(this.w_TAAUTORI);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  EPCALTAR;
                  from (i_cTable) where;
                      EPCODICE = this.w_TAAUTORI;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_AUTCAL = NVL(cp_ToDate(_read_.EPCALTAR),cp_NullValue(_read_.EPCALTAR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if ALLTRIM(NVL(this.w_AUTCAL,""))==""
                this.w_AUTCAL = this.w_TAAUTORI
              endif
            endif
          endif
          * --- Gazzetta Ufficiale
          * --- Check valore (definito/indeterminabile/indeterminabile di particolare importanza)
          * --- Calcolo diritti/onorari
          * --- Coefficiente di calcolo, % di sconto, % di maggiorazione
          * --- Parti in assistenza
          do case
            case (EMPTY(this.w_NamePrg) AND !this.w_NameClass=="TGSAG_BGP" ) OR this.w_NamePrg=="GSAG_MPR"
              * --- Prestazioni rapide
              this.w_TAGAZUFF = this.w_PADRE.w_UFFICI
              * --- Se � un diritto con valore pratica indeterminabile viene considerato il check
              *     "Calcolo dei diritti per valore indeterminabile" presente sulla pratica.
              if this.w_TATIPPRE $ "RC" AND this.w_PADRE.w_FLVALO $ "IPS"
                this.w_TAFLVALO = this.w_PADRE.w_FLVALO1
                this.w_TACALDIR = "M"
                this.w_TACOECAL = 50
              else
                this.w_TAFLVALO = this.w_PADRE.w_FLVALO
                this.w_TACALDIR = this.w_PADRE.w_CALDIR
                this.w_TACOECAL = this.w_PADRE.w_COECAL
              endif
              if !EMPTY(this.w_NameClass) AND this.w_NameClass="TGSAL_BIA"
                this.w_TIPOPRAT = this.w_PADRE.w_GSAL_BIA_TIPPRA
                this.w_CNMATOBB = this.w_PADRE.w_GSAL_BIA_MATOBB
                this.w_CNASSCTP = this.w_PADRE.w_GSAL_BIA_ASSCTP
                this.w_CNCOMPLX = this.w_PADRE.w_GSAL_BIA_COMPLX
                this.w_CNPROFMT = this.w_PADRE.w_GSAL_BIA_PROFMT
                this.w_CNESIPOS = this.w_PADRE.w_GSAL_BIA_ESIPOS
                this.w_CNPERPLX = this.w_PADRE.w_GSAL_BIA_PERPLX
                this.w_CNPERPOS = this.w_PADRE.w_GSAL_BIA_PERPOS
                this.w_CNFLVMLQ = this.w_PADRE.w_GSAL_BIA_FLVMLQ
              else
                this.w_TIPOPRAT = this.w_PADRE.w_CNTIPPRA
                this.w_CNMATOBB = this.w_PADRE.w_CNMATOBB
                this.w_CNASSCTP = this.w_PADRE.w_CNASSCTP
                this.w_CNCOMPLX = this.w_PADRE.w_CNCOMPLX
                this.w_CNPROFMT = this.w_PADRE.w_CNPROFMT
                this.w_CNESIPOS = this.w_PADRE.w_CNESIPOS
                this.w_CNPERPLX = this.w_PADRE.w_CNPERPLX
                this.w_CNPERPOS = this.w_PADRE.w_CNPERPOS
                this.w_CNFLVMLQ = this.w_PADRE.w_CNFLVMLQ
              endif
              this.w_PARASS = this.w_PADRE.w_PARASS
              this.w_FLAMPA = this.w_PADRE.w_FLAMPA
              this.w_TATARTEM = this.w_PADRE.w_TARTEM
              this.w_TATARCON = this.w_PADRE.w_TARCON
              this.w_CONTRACN = this.w_PADRE.w_CONTRACN
              this.w_TIPCONCO = this.w_PADRE.w_TIPCONCO
              this.w_LISCOLCO = this.w_PADRE.w_LISCOLCO
              this.w_STATUSCO = this.w_PADRE.w_STATUSCO
            case this.w_NamePrg=="GSPR_MDO"
              * --- Preventivi
              this.w_TAGAZUFF = this.w_GESTIONE.w_OFUFFICI
              * --- Se � un diritto con valore pratica indeterminabile viene considerato il check
              *     "Calcolo dei diritti per valore indeterminabile" presente sulla pratica.
              if this.w_TATIPPRE $ "RC" AND this.w_GESTIONE.w_OFFLVALO $ "IPS"
                this.w_TAFLVALO = this.w_GESTIONE.w_OFFLDIND
                this.w_TACALDIR = "M"
                this.w_TACOECAL = 50
              else
                this.w_TAFLVALO = this.w_GESTIONE.w_OFFLVALO
                this.w_TACALDIR = this.w_GESTIONE.w_OFCALDIR
                this.w_TACOECAL = this.w_GESTIONE.w_OFCOECAL
              endif
              this.w_PARASS = this.w_GESTIONE.w_OFPARASS
              this.w_FLAMPA = " "
              this.w_TATARTEM = this.w_GESTIONE.w_OFTARTEM
              this.w_TATARCON = this.w_GESTIONE.w_OFTARCON
              this.w_CONTRACN = this.w_GESTIONE.w_OFCONTRA
              this.w_TIPCONCO = this.w_GESTIONE.w_TIPCONCO
              this.w_LISCOLCO = this.w_GESTIONE.w_LISCOLCO
              this.w_STATUSCO = this.w_GESTIONE.w_STATUSCO
              this.w_TIPOPRAT = this.w_GESTIONE.w_OFTIPPRA
              this.w_CNMATOBB = this.w_GESTIONE.w_OFMATOBB
              this.w_CNASSCTP = this.w_GESTIONE.w_OFASSCTP
              this.w_CNCOMPLX = this.w_GESTIONE.w_OFCOMPLX
              this.w_CNPROFMT = this.w_GESTIONE.w_OFPROFMT
              this.w_CNESIPOS = this.w_GESTIONE.w_OFESIPOS
              this.w_CNPERPLX = this.w_GESTIONE.w_OFPERPLX
              this.w_CNPERPOS = this.w_GESTIONE.w_OFPERPOS
              this.w_CNFLVMLQ = this.w_GESTIONE.w_OFFLVMLQ
            case this.w_NamePrg=="GSVE_MDV" OR this.w_NamePrg=="GSAG_APR" OR this.w_NameClass=="TGSAG_BGP"
              * --- Documenti di vendita
              this.w_TAGAZUFF = this.w_PADRE.w_CNUFFICI
              * --- Se � un diritto con valore pratica indeterminabile viene considerato il check
              *     "Calcolo dei diritti per valore indeterminabile" presente sulla pratica.
              if this.w_TATIPPRE $ "RC" AND this.w_PADRE.w_CNFLVALO $ "IPS"
                this.w_TAFLVALO = this.w_PADRE.w_CNFLVALO1
                this.w_TACALDIR = "M"
                this.w_TACOECAL = 50
              else
                this.w_TAFLVALO = this.w_PADRE.w_CNFLVALO
                this.w_TACALDIR = this.w_PADRE.w_CNCALDIR
                this.w_TACOECAL = this.w_PADRE.w_CNCOECAL
              endif
              this.w_PARASS = this.w_PADRE.w_CNPARASS
              this.w_FLAMPA = this.w_PADRE.w_CNFLAMPA
              this.w_TATARTEM = this.w_PADRE.w_CNTARTEM
              this.w_TATARCON = this.w_PADRE.w_CNTARCON
              this.w_CONTRACN = this.w_PADRE.w_CONTRACN
              this.w_TIPCONCO = this.w_PADRE.w_TIPCONCO
              this.w_LISCOLCO = this.w_PADRE.w_LISCOLCO
              this.w_STATUSCO = this.w_PADRE.w_STATUSCO
              this.w_TIPOPRAT = this.w_PADRE.w_DETIPPRA
              this.w_CNMATOBB = this.w_PADRE.w_CNMATOBB
              this.w_CNASSCTP = this.w_PADRE.w_CNASSCTP
              this.w_CNCOMPLX = this.w_PADRE.w_CNCOMPLX
              this.w_CNPROFMT = this.w_PADRE.w_CNPROFMT
              this.w_CNESIPOS = this.w_PADRE.w_CNESIPOS
              this.w_CNPERPLX = this.w_PADRE.w_CNPERPLX
              this.w_CNPERPOS = this.w_PADRE.w_CNPERPOS
              this.w_CNFLVMLQ = this.w_PADRE.w_CNFLVMLQ
            otherwise
              * --- Variabili dichiarate in: GSAG_AAT, GSAL_KCI, GSAL_BIA
              this.w_TAGAZUFF = this.w_GESTIONE.w_atuffici
              * --- Se � un diritto con valore pratica indeterminabile viene considerato il check
              *     "Calcolo dei diritti per valore indeterminabile" presente sulla pratica.
              if this.w_TATIPPRE $ "RC" AND this.w_GESTIONE.w_ATFLVALO $ "IPS"
                this.w_TAFLVALO = this.w_GESTIONE.w_ATFLVALO1
                this.w_TACALDIR = "M"
                this.w_TACOECAL = 50
              else
                this.w_TAFLVALO = this.w_GESTIONE.w_ATFLVALO
                this.w_TACALDIR = this.w_GESTIONE.w_ATCALDIR
                this.w_TACOECAL = this.w_GESTIONE.w_ATCOECAL
              endif
              this.w_PARASS = this.w_GESTIONE.w_PARASS
              this.w_FLAMPA = this.w_GESTIONE.w_FLAMPA
              this.w_TATARTEM = this.w_GESTIONE.w_ATTARTEM
              this.w_TATARCON = this.w_GESTIONE.w_ATTARCON
              this.w_CONTRACN = this.w_GESTIONE.w_CONTRACN
              this.w_TIPCONCO = this.w_GESTIONE.w_TIPCONCO
              this.w_LISCOLCO = this.w_GESTIONE.w_LISCOLCO
              this.w_STATUSCO = this.w_GESTIONE.w_STATUSCO
              this.w_TIPOPRAT = this.w_GESTIONE.w_CNTIPPRA
              this.w_CNMATOBB = this.w_GESTIONE.w_CNMATOBB
              this.w_CNASSCTP = this.w_GESTIONE.w_CNASSCTP
              this.w_CNCOMPLX = this.w_GESTIONE.w_CNCOMPLX
              this.w_CNPROFMT = this.w_GESTIONE.w_CNPROFMT
              this.w_CNESIPOS = this.w_GESTIONE.w_CNESIPOS
              this.w_CNPERPLX = this.w_GESTIONE.w_CNPERPLX
              this.w_CNPERPOS = this.w_GESTIONE.w_CNPERPOS
              this.w_CNFLVMLQ = this.w_GESTIONE.w_CNFLVMLQ
          endcase
          * --- Lettura della mansione del partecipante
          * --- Read from DIPENDEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIPENDEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPMANSION"+;
              " from "+i_cTable+" DIPENDEN where ";
                  +"DPCODICE = "+cp_ToStrODBC(this.w_DACODRES);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPMANSION;
              from (i_cTable) where;
                  DPCODICE = this.w_DACODRES;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TAMANSION = NVL(cp_ToDate(_read_.DPMANSION),cp_NullValue(_read_.DPMANSION))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Valore Pratica
          do case
            case this.w_TAFLVALO="D"
              do case
                case (EMPTY(this.w_NamePrg) AND !this.w_NameClass=="TGSAG_BGP") OR this.w_NamePrg=="GSAG_MPR"
                  * --- Prestazioni rapide
                  this.w_TAIMPORT = this.w_PADRE.w_IMPORT
                case this.w_NamePrg=="GSVE_MDV" OR this.w_NamePrg=="GSAG_APR" OR this.w_NameClass=="TGSAG_BGP"
                  * --- Documenti di vendita
                  this.w_TAIMPORT = this.w_PADRE.w_CNIMPORT
                case this.w_NamePrg=="GSPR_MDO"
                  * --- Preventivi
                  this.w_TAIMPORT = this.w_GESTIONE.w_OFIMPORT
                otherwise
                  * --- Attivit�
                  this.w_TAIMPORT = this.w_GESTIONE.w_ATIMPORT
              endcase
            case this.w_TAFLVALO="I"
              this.w_TAIMPORT = -1
            case this.w_TAFLVALO$"PS"
              this.w_TAIMPORT = -2
          endcase
          * --- Legge la classificazione del tipo pratica
          * --- Read from PRA_TIPI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRA_TIPI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRA_TIPI_idx,2],.t.,this.PRA_TIPI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TPFLGIUD"+;
              " from "+i_cTable+" PRA_TIPI where ";
                  +"TPCODICE = "+cp_ToStrODBC(this.w_TIPOPRAT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TPFLGIUD;
              from (i_cTable) where;
                  TPCODICE = this.w_TIPOPRAT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLGIUD = NVL(cp_ToDate(_read_.TPFLGIUD),cp_NullValue(_read_.TPFLGIUD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Dichiarazione array prezzi
          DECLARE ARRPRE (4,1)
          * --- Azzero l'Array che verr� riempito dalla Funzione
          ARRPRE(1)=0
          * --- Se proveniamo da GSAL_BIA dobbiamo utilizzare l'unit� di misura letta dagli articoli
          if !EMPTY(this.w_NameClass) AND this.w_NameClass="TGSAL_BIA"
            this.w_TAUNIMIS = this.w_GSAL_BIA_Unimis
            this.w_PADRE.w_MVUNIMIS = this.w_TAUNIMIS
          endif
          * --- Calcolo del prezzo unitario
          do case
            case this.w_TATIPPRE="P" AND (this.w_TATARTEM="C" OR (this.w_TATARTEM="D" AND this.w_TIPCONCO<>"L" AND this.w_STATUSCO="I"))
              * --- Se Prestazione a tempo e (Tipo di tariffazione a tempo uguale a Tariffa Concordata o 
              *     (Tipo di tariffazione a tempo uguale a Tariffa Da Contratto e Tipo condizioni del contratto diverso da monte ore/listino collegato)):
              *     applicazione tariffa concordata
              ARRPRE(1)=this.w_TATARCON
              ARRPRE(2)=this.w_TATARCON
              ARRPRE(3)=this.w_TATARCON
              ARRPRE(4)=Space(1)
            case this.w_TATIPPRE="P" AND this.w_TATARTEM="D" AND this.w_TIPCONCO="L" AND this.w_STATUSCO="I"
              * --- Se Prestazione a tempo e Tipo di tariffazione a tempo uguale a Tariffa Da Contratto e Tipo condizioni del contratto uguale a Da monte ore/listino collegato
              *     applicazione listino collegato (da contratto)
              this.w_GRAPAT = "N"
              this.w_CALPRZ = CALPRAT(this.w_TACODART,this.w_TATIPPRE,this.w_ONORARBI,this.w_FLPRPE,this.w_NOTIFICA,this.w_TAFLAPON,this.w_LISCOLCO,this.w_GRAPAT,this.w_AUTCAL,this.w_DACODRES,this.w_TAMANSION,this.w_TADATATT,this.w_TAFLVALO,this.w_TAIMPORT,this.w_TACALDIR,this.w_TACOECAL,this.w_PARASS,this.w_FLAMPA,@ARRPRE,this.w_NUMDECUNI,iif(this.pParame = "I" or this.w_NameClass=="TGSAG_BGP" ,.t.,.f.), "*NNNNN"+this.w_CNFLVMLQ+this.w_TAPARAM, 0, 0)
            otherwise
              * --- Caso normale
              *     applicazione listino (della pratica)
              * --- Read from LISTINI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.LISTINI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "LSGRAPAT"+;
                  " from "+i_cTable+" LISTINI where ";
                      +"LSCODLIS = "+cp_ToStrODBC(this.w_TACODLIS);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  LSGRAPAT;
                  from (i_cTable) where;
                      LSCODLIS = this.w_TACODLIS;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_GRAPAT = NVL(cp_ToDate(_read_.LSGRAPAT),cp_NullValue(_read_.LSGRAPAT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_CALPRZ = CALPRAT(this.w_TACODART,this.w_TATIPPRE,this.w_ONORARBI,this.w_FLPRPE,this.w_NOTIFICA,this.w_TAFLAPON,this.w_TACODLIS,this.w_GRAPAT,this.w_AUTCAL,this.w_DACODRES,this.w_TAMANSION,this.w_TADATATT,this.w_TAFLVALO,this.w_TAIMPORT,this.w_TACALDIR,this.w_TACOECAL,this.w_PARASS,this.w_FLAMPA,@ARRPRE,this.w_NUMDECUNI,iif(this.pParame = "I" or this.w_NameClass=="TGSAG_BGP" ,.t.,.f.), this.w_FLGIUD+this.w_CNMATOBB+this.w_CNASSCTP+this.w_CNCOMPLX+this.w_CNPROFMT+this.w_CNESIPOS+this.w_CNFLVMLQ+this.w_TAPARAM, this.w_CNPERPLX,this.w_CNPERPOS)
          endcase
          if (this.pParame $ "1-I" or this.pParame="3" or this.pParame="4" or this.pParame="5") or ( this.pParame="2" and ((this.w_NOTIFICA="S" and this.w_TAQTAMOV=1) or this.w_NOTIFICA<>"S"))
            this.w_MMPZ_1 = cp_Round(CALMMPZ(ARRPRE(1), this.w_TAQTAMOV, this.w_TAQTAUM1, "N", 0, g_perpul),g_perpul)
            this.w_MMPZ_2 = cp_Round(CALMMPZ(ARRPRE(2), this.w_TAQTAMOV, this.w_TAQTAUM1, "N", 0, g_perpul),g_perpul)
            this.w_MMPZ_3 = cp_Round(CALMMPZ(ARRPRE(3), this.w_TAQTAMOV, this.w_TAQTAUM1, "N", 0, g_perpul),g_perpul)
            do case
              case (EMPTY(this.w_NamePrg) AND !this.w_NameClass=="TGSAG_BGP") OR this.w_NamePrg=="GSVE_MDV"
                * --- Prezzo unitario
                this.w_PADRE.w_MVPREZZO =this.w_MMPZ_1
                * --- Prezzo minimo
                this.w_PADRE.w_DEPREMIN = this.w_MMPZ_2
                * --- Prezzo massimo
                this.w_PADRE.w_DEPREMAX = this.w_MMPZ_3
                * --- Gazzetta Ufficiale
                this.w_PADRE.w_DEGAZUFF = ARRPRE(4)
              case this.w_NamePrg=="GSPR_MDO"
                * --- Prezzo unitario
                this.w_PADRE.w_ODPREZZO =this.w_MMPZ_1
                 
 this.w_PADRE.w_LIPREZZO =this.w_PADRE.w_ODPREZZO 
 this.w_PADRE.o_LIPREZZO =this.w_PADRE.w_ODPREZZO
                * --- Prezzo minimo
                this.w_PADRE.w_ODPREMIN = this.w_MMPZ_2
                * --- Prezzo massimo
                this.w_PADRE.w_ODPREMAX = this.w_MMPZ_3
                * --- Gazzetta Ufficiale
                this.w_PADRE.w_ODGAZUFF = ARRPRE(4)
              case this.w_NamePrg=="GSAG_APR" OR this.w_NameClass=="TGSAG_BGP"
                * --- Prestazioni da manutenzione
                * --- Prezzo unitario
                this.w_PADRE.w_PRPREZZO =this.w_MMPZ_1
                * --- Prezzo minimo
                this.w_PADRE.w_PRPREMIN = this.w_MMPZ_2
                * --- Prezzo massimo
                this.w_PADRE.w_PRPREMAX = this.w_MMPZ_3
                * --- Gazzetta Ufficiale
                this.w_PADRE.w_PRGAZUFF = ARRPRE(4)
              otherwise
                * --- Prezzo unitario
                this.w_PADRE.w_DAPREZZO = this.w_MMPZ_1
                * --- Prezzo minimo
                this.w_PADRE.w_DAPREMIN = this.w_MMPZ_2
                * --- Prezzo massimo
                this.w_PADRE.w_DAPREMAX = this.w_MMPZ_3
                * --- Gazzetta Ufficiale
                this.w_PADRE.w_DAGAZUFF = ARRPRE(4)
            endcase
          else
            if this.w_NOTIFICA="S" and this.w_TAQTAMOV >1
              * --- Gestione caso con pi� notifiche
              if this.w_NamePrg=="GSVE_MDV"
                * --- Per determinare il prezzo utilizzato (Prezzo minimo e massimo) - Ricalcola il prezzo "normalmente" per quantit� 1
                * --- Prezzo unitario
                this.w_TAPREZZO = ARRPRE(1)
                this.w_TAPREMIN = ARRPRE(2)
                this.w_TAPREMAX = ARRPRE(3)
              endif
              this.w_CALPRZ = CALPRNO(this.w_TAFLVALO,this.w_TAIMPORT,this.w_TAPREZZO,this.w_TAPREMIN,this.w_TAPREMAX,this.w_TAQTAMOV,this.w_TACALDIR,this.w_TACOECAL,@ARRPRE,this.w_NUMDECUNI)
              * --- Prezzo unitario
              do case
                case this.w_NamePrg=="GSVE_MDV"
                  this.w_PADRE.w_MVPREZZO =ARRPRE(1)
                case !EMPTY(this.w_NamePrg) and this.w_NamePrg=="GSPR_MDO"
                  this.w_PADRE.w_ODPREZZO =ARRPRE(1)
                otherwise
                  this.w_PADRE.w_DAPREZZO = ARRPRE(1)
              endcase
            endif
          endif
        else
          * --- Gestione errore: listino non definito
          do case
            case this.w_NamePrg=="GSVE_MDV"
              this.w_PADRE.w_MVPREZZO = 0
            case this.w_NamePrg=="GSPR_MDO"
              this.w_PADRE.w_ODPREZZO = 0
            case this.w_NamePrg=="GSAG_APR"
              this.w_PADRE.w_PRPREZZO = 0
            otherwise
              this.w_PADRE.w_DAPREZZO = 0
          endcase
        endif
      case Isahe()
        this.w_TIPRIS = this.w_PADRE.w_TIPRIS
        this.w_KEYLISTB = this.w_PADRE.w_KEYLISTB
        this.w_TACODLIS = this.w_PADRE.w_CODLIS
        if this.w_FLGLIS<>"S"
          if this.pParame="7"
            * --- Applico listino impostato nei dati di riga
            this.w_ATTPROSC = this.w_PADRE.w_DAPROSCO
            this.w_ATTPROLI = this.w_PADRE.w_DAPROLIS
            this.w_ATTSCLIS = this.w_PADRE.w_DASCOLIS
            this.w_ATTCOLIS = this.w_PADRE.w_DACODLIS
          else
            * --- Azzero listini di riga perch� devo applicare il calcolo non tenendone conto
            this.w_ATTPROSC = Space(5)
            this.w_ATTPROLI = Space(5)
            this.w_ATTSCLIS = Space(5)
            this.w_ATTCOLIS = Space(5)
          endif
        endif
        if Not Empty(this.w_ATCODNOM) and Empty(this.w_CODCLI) and this.pParame<>"7"
          * --- Se nominativo non di tipo cliente devo applicare listino ricavi dei parametri attivit�
          this.w_ATTCOLIS = this.w_TACODLIS
        endif
        if Not empty(this.w_CAUDOC)
          if (this.pParame $ "1-I" or this.pParame="3" or this.pParame="4" or (this.pParame="5" AND this.w_TAPREZZO=0) or this.pParame="6" or this.pParame="7" or this.pParame="9")
            this.w_TAQTAMOV = IIF(this.w_TAQTAMOV=0,IIF(this.w_PADRE.w_TIPART="DE", 0, 1),this.w_TAQTAMOV)
            DECLARE ARRET (10,1) 
 ARRET[1]= "" 
 ARRET[2]= "" 
 ARRET[3]= "" 
 ARRET[4]= "" 
 ARRET[5]=0 
 ARRET[6]=0 
 ARRET[7]=0 
 ARRET[8]=0 
 ARRET[9]=0 
 ARRET[10]=0
            if this.w_TAPREZZO=0 or this.pParame="3" or this.pParame="4" or this.pParame="6" or this.pParame="9" or Ah_yesno("Si desidera aggiornare il prezzo su riga?")
              * --- Azzero l'Array che verr� riempito dalla Funzione
              this.w_CALPRZ = gsar_bpz(this.w_PADRE,this.w_ATTCOLIS,this.w_ATTPROLI,this.w_ATTSCLIS,this.w_ATTPROSC,this.w_DACODICE,this.w_ATTIPCLI,this.w_CODCLI,this.w_TAQTAMOV,this.w_ATCODVAL,IIF(Vartype(this.w_ATDATINI)="T",ttod(this.w_ATDATINI),this.w_ATDATINI),this.w_TAUNIMIS,this.w_CAUDOC,this.w_FLGLIS,this.w_KEYLISTB,@ARRET)
              this.w_PADRE.Set("w_DACODLIS", ARRET[1], .t., .t.)     
              this.w_PADRE.Set("w_DAPROLIS", ARRET[2], .t., .t.)     
              this.w_PADRE.Set("w_DASCOLIS", ARRET[3], .t., .t.)     
              this.w_PADRE.Set("w_DAPROSCO", ARRET[4], .t., .t.)     
              this.w_PADRE.Set("w_DAPREZZO", ARRET[5], .t., .t.)     
              this.w_PADRE.Set("w_DASCONT1", ARRET[6], .t., .t.)     
              this.w_PADRE.Set("w_DASCONT2", ARRET[7], .t., .t.)     
              this.w_PADRE.Set("w_DASCONT3", ARRET[8], .t., .t.)     
              this.w_PADRE.Set("w_DASCONT4", ARRET[9], .t., .t.)     
              if ! (this.w_NamePrg=="GSAG_MPR" OR this.w_NamePrg=="GSAG_MPP" OR this.w_NamePrg=="GSAL_MAP")
                this.w_PADRE.Set("w_DAVALRIG", ARRET[10], .t., .t.)     
              endif
            endif
          endif
        endif
        if this.pParame="5" and this.w_COST_ORA<>0
          * --- Ho cambiato il responsabile il mio costo dui partenza � quello letto dal partecipante
          this.w_PADRE.Set("w_LICOSTO", this.w_COST_ORA, .t., .t.)     
        else
          * --- mantengo costo presente sulla riga
          this.w_PADRE.Set("w_LICOSTO", this.w_PADRE.w_DACOSUNI, .t., .t.)     
        endif
        if (this.w_COST_ORA=0 AND this.w_TIPRIS $ "P-R") and (this.pParame $ "1-I" or this.pParame="8" or this.pParame="5" ) 
          if this.w_PADRE.w_DACOSUNI=0 or this.pParame="8" or Ah_Yesno("Si desidera aggiornare il costo di riga?")
            DECLARE ARRET (10,1) 
 ARRET[1]= "" 
 ARRET[2]= "" 
 ARRET[3]= "" 
 ARRET[4]= "" 
 ARRET[5]=0 
 ARRET[6]=0 
 ARRET[7]=0 
 ARRET[8]=0 
 ARRET[9]=0 
 ARRET[10]=0
            if Not empty(this.w_ATLISACQ)
              * --- Calcolo costo interno in base al listino impostato
              * --- Aggiungo listino acquisti
              gsag_bdl(this,"I",this.w_KEYLISTB,this,this.w_ATLISACQ)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_CALPRZ = gsar_bpz(this.w_PADRE,this.w_ATLISACQ,Space(5),Space(5),Space(5),this.w_DACODICE," ",Space(15),this.w_TAQTAMOV,this.w_ATCODVAL,IIF(Vartype(this.w_ATDATINI)="T",ttod(this.w_ATDATINI),this.w_ATDATINI),this.w_TAUNIMIS,this.w_CAUACQ,this.w_FLGLIS,this.w_KEYLISTB,@ARRET)
              * --- Elimino listino acquisti
              gsag_bdl(this,"E",this.w_KEYLISTB,this,this.w_ATLISACQ)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.w_PADRE.Set("w_LICOSTO", ARRET[5], .t., .t.)     
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='PRA_ENTI'
    this.cWorkTables[4]='UNIMIS'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='LISTINI'
    this.cWorkTables[7]='PRA_TIPI'
    this.cWorkTables[8]='TAR_IFFE'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
