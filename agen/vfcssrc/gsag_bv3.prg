* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bv3                                                        *
*              Abbina/Disabbina elementi contratto                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-07                                                      *
* Last revis.: 2015-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAMETRO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bv3",oParentObject,m.pPARAMETRO)
return(i_retval)

define class tgsag_bv3 as StdBatch
  * --- Local variables
  pPARAMETRO = space(10)
  w_ELCONTRA = space(10)
  w_ELCODMOD = space(10)
  w_ELCODIMP = space(10)
  w_ELCODCOM = 0
  w_ELRINNOV = 0
  w_NUMERO = 0
  w_OK = .f.
  w_RECNO = 0
  w_RECCOUNT = 0
  w_COCODCON = space(15)
  w_TEST = .f.
  w_IMDATINI = ctod("  /  /  ")
  w_IMDATFIN = ctod("  /  /  ")
  w_ELDATATT = ctod("  /  /  ")
  w_oERRORLOG = .NULL.
  w_OKMSG = .f.
  * --- WorkFile variables
  ELE_CONT_idx=0
  IMP_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Abbina/Disabbina elementi contratto
    if EMPTY( this.oParentObject.w_COMPIMP ) AND this.oParentObject.w_FLTYPEOP="A"
      this.oParentObject.w_COMPIMPKEYREAD = 0
    endif
    do case
      case this.pPARAMETRO == "SELEZ_ALL" OR this.pPARAMETRO == "DESEL_ALL" OR this.pPARAMETRO == "INV_SELEZ"
        SELECT ( this.oParentObject.w_ZOOM.cCURSOR )
        if RECCOUNT() <> 0 
          this.w_RECNO = MIN( RECNO(), RECCOUNT() )
          UPDATE ( this.oParentObject.w_ZOOM.cCURSOR ) SET XCHK = ICASE(this.pPARAMETRO=="SELEZ_ALL", 1, this.pPARAMETRO=="DESEL_ALL", 0, IIF(XCHK=1, 0, 1) )
          SELECT ( this.oParentObject.w_ZOOM.cCURSOR )
          if this.w_RECNO > 0
            GOTO this.w_RECNO
          endif
        endif
      case this.pPARAMETRO = "DISABBINA"
        SELECT( this.oParentObject.w_ZOOM.cCURSOR )
        GO TOP
        SCAN FOR XCHK<>0
        this.w_TEST = .T.
        ENDSCAN
        if !this.w_TEST
          AH_ERRORMSG( "Non ci sono elementi contratto da disabbinare." )
          i_retcode = 'stop'
          return
        endif
        SELECT( this.oParentObject.w_ZOOM.cCURSOR )
        GO TOP
        do while NOT EOF()
          if XCHK = 1
            this.w_ELCONTRA = ELCONTRA
            this.w_ELCODMOD = ELCODMOD
            this.w_ELCODIMP = ELCODIMP
            this.w_ELCODCOM = ELCODCOM
            this.w_ELRINNOV = ELRINNOV
            * --- Try
            local bErr_030E7AC8
            bErr_030E7AC8=bTrsErr
            this.Try_030E7AC8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              if AH_YESNO( "L'elemento avente contratto %1, modello %2, n. rinnovo %3 � gi� esistente e non pu� essere disabbinato.%0Si desidera procedere con la sua cancellazione?", , ALLTRIM(this.w_ELCONTRA), ALLTRIM(this.w_ELCODMOD), ALLTRIM(STR(this.w_ELRINNOV)))
                * --- Delete from ELE_CONT
                i_nConn=i_TableProp[this.ELE_CONT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"ELCONTRA = "+cp_ToStrODBC(this.w_ELCONTRA);
                        +" and ELCODMOD = "+cp_ToStrODBC(this.w_ELCODMOD);
                        +" and ELRINNOV = "+cp_ToStrODBC(this.w_ELRINNOV);
                        +" and ELCODIMP = "+cp_ToStrODBC(this.w_ELCODIMP);
                        +" and ELCODCOM = "+cp_ToStrODBC(this.w_ELCODCOM);
                         )
                else
                  delete from (i_cTable) where;
                        ELCONTRA = this.w_ELCONTRA;
                        and ELCODMOD = this.w_ELCODMOD;
                        and ELRINNOV = this.w_ELRINNOV;
                        and ELCODIMP = this.w_ELCODIMP;
                        and ELCODCOM = this.w_ELCODCOM;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
              endif
            endif
            bTrsErr=bTrsErr or bErr_030E7AC8
            * --- End
          endif
          SKIP
        enddo
        this.oParentObject.NotifyEvent("Search")
        AH_ErrorMsg( "Operazione completata", 64 )
      case this.pPARAMETRO = "ABBINA"
        SELECT( this.oParentObject.w_ZOOM.cCURSOR )
        GO TOP
        SCAN FOR XCHK<>0
        this.w_TEST = .T.
        ENDSCAN
        if !this.w_TEST
          AH_ERRORMSG( "Non ci sono elementi contratto da abbinare." )
          i_retcode = 'stop'
          return
        endif
        if EMPTY( this.oParentObject.w_IMPIANTO )
          AH_ERRORMSG( "Per abbinare gli elementi occorre selezionare un impianto." )
          i_retcode = 'stop'
          return
        endif
        * --- Si controlla se gli elementi selezionati che devono essere abbinati all'impianto
        *     hanno una data di prossima attivit� (ELDATATT) compatibile con l'intervallo
        *     di validit� dell'impianto
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        * --- Read from IMP_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.IMP_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.IMP_MAST_idx,2],.t.,this.IMP_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IMDATINI,IMDATFIN"+;
            " from "+i_cTable+" IMP_MAST where ";
                +"IMCODICE = "+cp_ToStrODBC(this.oParentObject.w_IMPIANTO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IMDATINI,IMDATFIN;
            from (i_cTable) where;
                IMCODICE = this.oParentObject.w_IMPIANTO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_IMDATINI = NVL(cp_ToDate(_read_.IMDATINI),cp_NullValue(_read_.IMDATINI))
          this.w_IMDATFIN = NVL(cp_ToDate(_read_.IMDATFIN),cp_NullValue(_read_.IMDATFIN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT( this.oParentObject.w_ZOOM.cCURSOR )
        GO TOP
        do while NOT EOF()
          if XCHK = 1
            this.w_ELCONTRA = ELCONTRA
            this.w_ELCODMOD = ELCODMOD
            this.w_ELCODIMP = ELCODIMP
            this.w_ELCODCOM = ELCODCOM
            this.w_ELRINNOV = ELRINNOV
            this.w_ELDATATT = ELDATATT
            if NOT EMPTY( NVL( this.w_ELDATATT, CTOD( "  -  -  " ) ) )
              * --- L'elemento ha una data di inzio prossima attivit�
              if NOT EMPTY( NVL( this.w_IMDATINI, CTOD( "  -  -  " ) ) )
                * --- L'impianto ha una data di inizio validit�
                if this.w_IMDATINI > this.w_ELDATATT
                  this.w_oERRORLOG.AddMsgLog("All'elemento contratto %1, modello %2, numero rinnovi %3 non pu� essere associato l'impianto selezionato",this.w_ELCONTRA,this.w_ELCODMOD,this.w_ELRINNOV)     
                  this.w_oERRORLOG.AddMsgLog("La data di inizio validit� dell'impianto (%1) � successiva alla data di prossima attivit� dell'elemento contratto(%2)", DTOC( this.w_IMDATINI ), DTOC( this.w_ELDATATT ))     
                endif
              endif
              if NOT EMPTY( NVL( this.w_IMDATFIN, CTOD( "  -  -  " ) ) )
                * --- L'impianto ha una data di fine validit�
                if this.w_IMDATFIN < this.w_ELDATATT
                  this.w_oERRORLOG.AddMsgLog("All'elemento contratto %1, modello %2, numero rinnovi %3 non pu� essere associato l'impianto selezionato",this.w_ELCONTRA,this.w_ELCODMOD,this.w_ELRINNOV)     
                  this.w_oERRORLOG.AddMsgLog("La data di fine validit� dell'impianto (%1) � precedente alla data di prossima attivit� dell'elemento contratto(%2)", DTOC( this.w_IMDATINI ), DTOC( this.w_ELDATATT ))     
                endif
              endif
            endif
          endif
          SKIP
        enddo
        if this.w_oERRORLOG.IsFullLog()
          this.w_oERRORLOG.PrintLog("Elementi contratto",.f.,.t.,"Sono stati rilevati elementi a cui non pu� essere associato l'impianto selezionato.%0Si desidera stampare un resoconto?")     
          i_retcode = 'stop'
          return
        endif
        if EMPTY( this.oParentObject.w_COMPIMPKEYREAD )
          this.w_OK = AH_YESNO( "Si desidera abbinare gli elementi selezionati all'impianto %1 %2?", , ALLTRIM( this.oParentObject.w_IMPIANTO ), ALLTRIM( this.oParentObject.w_IMDESCRI ) )
        else
          this.w_OK = AH_YESNO( "Si desidera abbinare gli elementi selezionati al componente %1 dell'impianto %2 %3?", , ALLTRIM( this.oParentObject.w_COMPIMP ), ALLTRIM( this.oParentObject.w_IMPIANTO ), ALLTRIM( this.oParentObject.w_IMDESCRI ) )
        endif
        if this.w_OK
          SELECT( this.oParentObject.w_ZOOM.cCURSOR )
          GO TOP
          do while NOT EOF()
            if XCHK = 1
              this.w_ELCONTRA = ELCONTRA
              this.w_ELCODMOD = ELCODMOD
              this.w_ELCODIMP = ELCODIMP
              this.w_ELCODCOM = ELCODCOM
              this.w_ELRINNOV = ELRINNOV
              this.w_COCODCON = COCODCON
              if this.oParentObject.w_IMCODCON<>this.w_COCODCON and !this.w_OKMSG
                this.w_OK = AH_YESNO( "Attenzione! Si stanno abbinando uno o pi� elementi contratto ad impianto con intestatario diverso!%0Procedo ugualmente?")
                this.w_OKMSG = .t.
              endif
              if this.w_OK
                * --- Write into ELE_CONT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ELE_CONT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ELCODIMP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_IMPIANTO),'ELE_CONT','ELCODIMP');
                  +",ELCODCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COMPIMPKEYREAD),'ELE_CONT','ELCODCOM');
                      +i_ccchkf ;
                  +" where ";
                      +"ELCONTRA = "+cp_ToStrODBC(this.w_ELCONTRA);
                      +" and ELCODMOD = "+cp_ToStrODBC(this.w_ELCODMOD);
                      +" and ELRINNOV = "+cp_ToStrODBC(this.w_ELRINNOV);
                      +" and ELCODIMP = "+cp_ToStrODBC(this.w_ELCODIMP);
                      +" and ELCODCOM = "+cp_ToStrODBC(this.w_ELCODCOM);
                         )
                else
                  update (i_cTable) set;
                      ELCODIMP = this.oParentObject.w_IMPIANTO;
                      ,ELCODCOM = this.oParentObject.w_COMPIMPKEYREAD;
                      &i_ccchkf. ;
                   where;
                      ELCONTRA = this.w_ELCONTRA;
                      and ELCODMOD = this.w_ELCODMOD;
                      and ELRINNOV = this.w_ELRINNOV;
                      and ELCODIMP = this.w_ELCODIMP;
                      and ELCODCOM = this.w_ELCODCOM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
            SKIP
          enddo
          if this.w_OK
            this.oParentObject.NotifyEvent("Search")
            this.oParentObject.NotifyEvent("w_CODICE Changed")
            AH_ErrorMsg( "Operazione completata", 64 )
          endif
        endif
    endcase
  endproc
  proc Try_030E7AC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ELE_CONT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ELCODIMP ="+cp_NullLink(cp_ToStrODBC(SPACE( 10 )),'ELE_CONT','ELCODIMP');
      +",ELCODCOM ="+cp_NullLink(cp_ToStrODBC(0),'ELE_CONT','ELCODCOM');
          +i_ccchkf ;
      +" where ";
          +"ELCONTRA = "+cp_ToStrODBC(this.w_ELCONTRA);
          +" and ELCODMOD = "+cp_ToStrODBC(this.w_ELCODMOD);
          +" and ELRINNOV = "+cp_ToStrODBC(this.w_ELRINNOV);
          +" and ELCODIMP = "+cp_ToStrODBC(this.w_ELCODIMP);
          +" and ELCODCOM = "+cp_ToStrODBC(this.w_ELCODCOM);
             )
    else
      update (i_cTable) set;
          ELCODIMP = SPACE( 10 );
          ,ELCODCOM = 0;
          &i_ccchkf. ;
       where;
          ELCONTRA = this.w_ELCONTRA;
          and ELCODMOD = this.w_ELCODMOD;
          and ELRINNOV = this.w_ELRINNOV;
          and ELCODIMP = this.w_ELCODIMP;
          and ELCODCOM = this.w_ELCODCOM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pPARAMETRO)
    this.pPARAMETRO=pPARAMETRO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ELE_CONT'
    this.cWorkTables[2]='IMP_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAMETRO"
endproc
