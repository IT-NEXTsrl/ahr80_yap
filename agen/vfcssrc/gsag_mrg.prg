* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mrg                                                        *
*              Resoconto giornaliero                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-14                                                      *
* Last revis.: 2014-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_mrg"))

* --- Class definition
define class tgsag_mrg as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 749
  Height = 387+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-07"
  HelpContextID=113911657
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=33

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  RIL_GIOR_IDX = 0
  DIPENDEN_IDX = 0
  CAUMATTI_IDX = 0
  CAL_AZIE_IDX = 0
  cFile = "RIL_GIOR"
  cKeySelect = "RGCODPER,RG__DATA"
  cKeyWhere  = "RGCODPER=this.w_RGCODPER and RG__DATA=this.w_RG__DATA"
  cKeyDetail  = "RGCODPER=this.w_RGCODPER and RG__DATA=this.w_RG__DATA"
  cKeyWhereODBC = '"RGCODPER="+cp_ToStrODBC(this.w_RGCODPER)';
      +'+" and RG__DATA="+cp_ToStrODBC(this.w_RG__DATA,"D")';

  cKeyDetailWhereODBC = '"RGCODPER="+cp_ToStrODBC(this.w_RGCODPER)';
      +'+" and RG__DATA="+cp_ToStrODBC(this.w_RG__DATA,"D")';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"RIL_GIOR.RGCODPER="+cp_ToStrODBC(this.w_RGCODPER)';
      +'+" and RIL_GIOR.RG__DATA="+cp_ToStrODBC(this.w_RG__DATA,"D")';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'RIL_GIOR.CPROWNUM '
  cPrg = "gsag_mrg"
  cComment = "Resoconto giornaliero"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TipoRisorsa = space(1)
  w_RGCODPER = space(5)
  o_RGCODPER = space(5)
  w_CALEPERS = space(5)
  w_GruppoPred = space(5)
  w_CALEGRUP = space(5)
  w_CalUsed = space(5)
  w_RG__DATA = ctod('  /  /  ')
  o_RG__DATA = ctod('  /  /  ')
  w_RGCAUATT = space(20)
  w_FLSINGPRE = space(1)
  w_STATOATT = space(1)
  w_RGOGGETT = space(254)
  w_RGOREPRE = 0
  w_RGMINPRE = 0
  w_RGOREEFF = 0
  w_RGMINEFF = 0
  w_RG__NOTE = space(0)
  w_CARAGGST = space(1)
  w_COGNOME = space(40)
  w_NOME = space(40)
  w_PERSONA = space(40)
  w_RGATTSER = space(20)
  w_TOTOREEFF = 0
  w_TOTMINEFF = 0
  w_TOT_MIN_EFF = 0
  w_TOT_ORE_EFF = 0
  w_MinOreLav = 0
  w_GIOLAV = space(1)
  o_GIOLAV = space(1)
  w_Comodo = space(10)
  w_ChkData = ctod('  /  /  ')
  w_GestioneMessaggi = .F.
  w_OB_TEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RIL_GIOR','gsag_mrg')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_mrgPag1","gsag_mrg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Rilevazione")
      .Pages(1).HelpContextID = 234304401
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRGCODPER_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CAUMATTI'
    this.cWorkTables[3]='CAL_AZIE'
    this.cWorkTables[4]='RIL_GIOR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RIL_GIOR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RIL_GIOR_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_RGCODPER = NVL(RGCODPER,space(5))
      .w_RG__DATA = NVL(RG__DATA,ctod("  /  /  "))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from RIL_GIOR where RGCODPER=KeySet.RGCODPER
    *                            and RG__DATA=KeySet.RG__DATA
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.RIL_GIOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIL_GIOR_IDX,2],this.bLoadRecFilter,this.RIL_GIOR_IDX,"gsag_mrg")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RIL_GIOR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RIL_GIOR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RIL_GIOR '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RGCODPER',this.w_RGCODPER  ,'RG__DATA',this.w_RG__DATA  )
      select * from (i_cTable) RIL_GIOR where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TipoRisorsa = 'P'
        .w_CALEPERS = space(5)
        .w_GruppoPred = space(5)
        .w_CALEGRUP = space(5)
        .w_FLSINGPRE = IIF(ISALT(), 'S', 'N')
        .w_STATOATT = 'K'
        .w_COGNOME = space(40)
        .w_NOME = space(40)
        .w_TOTOREEFF = 0
        .w_TOTMINEFF = 0
        .w_MinOreLav = 0
        .w_GIOLAV = space(1)
        .w_ChkData = ctod("  /  /  ")
        .w_GestioneMessaggi = .f.
        .w_DATOBSO = ctod("  /  /  ")
        .w_DTBSO_CAUMATTI = i_DatSys
        .w_RGCODPER = NVL(RGCODPER,space(5))
          .link_1_2('Load')
          .link_1_4('Load')
        .w_CalUsed = IIF(!EMPTY(.w_CALEPERS), .w_CALEPERS, IIF(!EMPTY(.w_CALEGRUP), .w_CALEGRUP, GetCalRis(.w_RGCODPER) ) )
        .w_RG__DATA = NVL(cp_ToDate(RG__DATA),ctod("  /  /  "))
          .link_1_8('Load')
        .w_PERSONA = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .w_TOT_MIN_EFF = MOD(.w_TOTMINEFF, 60)
        .w_TOT_ORE_EFF = .w_TOTOREEFF+INT(.w_TOTMINEFF / 60)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(IIF(!EMPTY(.w_RG__DATA),Ah_MsgFormat(ALLTRIM(g_GIORNO[DOW(.w_RG__DATA)])),''))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(IIF(.w_GIOLAV=='S' OR EMPTY(.w_RG__DATA),'',Ah_MsgFormat('Giorno festivo')))
        .w_Comodo = ' '
        .w_OB_TEST = IIF(EMPTY(.w_RG__DATA), i_INIDAT, .w_RG__DATA)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'RIL_GIOR')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTOREEFF = 0
      this.w_TOTMINEFF = 0
      scan
        with this
          .w_CARAGGST = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_RGCAUATT = NVL(RGCAUATT,space(20))
          if link_2_1_joined
            this.w_RGCAUATT = NVL(CACODICE201,NVL(this.w_RGCAUATT,space(20)))
            this.w_RGOGGETT = NVL(CADESCRI201,space(254))
            this.w_CARAGGST = NVL(CARAGGST201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_RGOGGETT = NVL(RGOGGETT,space(254))
          .w_RGOREPRE = NVL(RGOREPRE,0)
          .w_RGMINPRE = NVL(RGMINPRE,0)
          .w_RGOREEFF = NVL(RGOREEFF,0)
          .w_RGMINEFF = NVL(RGMINEFF,0)
          .w_RG__NOTE = NVL(RG__NOTE,space(0))
          .w_RGATTSER = NVL(RGATTSER,space(20))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTOREEFF = .w_TOTOREEFF+.w_RGOREEFF
          .w_TOTMINEFF = .w_TOTMINEFF+.w_RGMINEFF
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CalUsed = IIF(!EMPTY(.w_CALEPERS), .w_CALEPERS, IIF(!EMPTY(.w_CALEGRUP), .w_CALEGRUP, GetCalRis(.w_RGCODPER) ) )
        .w_PERSONA = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .w_TOT_MIN_EFF = MOD(.w_TOTMINEFF, 60)
        .w_TOT_ORE_EFF = .w_TOTOREEFF+INT(.w_TOTMINEFF / 60)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(IIF(!EMPTY(.w_RG__DATA),Ah_MsgFormat(ALLTRIM(g_GIORNO[DOW(.w_RG__DATA)])),''))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(IIF(.w_GIOLAV=='S' OR EMPTY(.w_RG__DATA),'',Ah_MsgFormat('Giorno festivo')))
        .w_Comodo = ' '
        .w_OB_TEST = IIF(EMPTY(.w_RG__DATA), i_INIDAT, .w_RG__DATA)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TipoRisorsa=space(1)
      .w_RGCODPER=space(5)
      .w_CALEPERS=space(5)
      .w_GruppoPred=space(5)
      .w_CALEGRUP=space(5)
      .w_CalUsed=space(5)
      .w_RG__DATA=ctod("  /  /  ")
      .w_RGCAUATT=space(20)
      .w_FLSINGPRE=space(1)
      .w_STATOATT=space(1)
      .w_RGOGGETT=space(254)
      .w_RGOREPRE=0
      .w_RGMINPRE=0
      .w_RGOREEFF=0
      .w_RGMINEFF=0
      .w_RG__NOTE=space(0)
      .w_CARAGGST=space(1)
      .w_COGNOME=space(40)
      .w_NOME=space(40)
      .w_PERSONA=space(40)
      .w_RGATTSER=space(20)
      .w_TOTOREEFF=0
      .w_TOTMINEFF=0
      .w_TOT_MIN_EFF=0
      .w_TOT_ORE_EFF=0
      .w_MinOreLav=0
      .w_GIOLAV=space(1)
      .w_Comodo=space(10)
      .w_ChkData=ctod("  /  /  ")
      .w_GestioneMessaggi=.f.
      .w_OB_TEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .w_TipoRisorsa = 'P'
        .w_RGCODPER = IIF(.cFunction='Load', ReadDipend(i_codute, "C"), .w_RGCODPER)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_RGCODPER))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_GruppoPred))
         .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        .w_CalUsed = IIF(!EMPTY(.w_CALEPERS), .w_CALEPERS, IIF(!EMPTY(.w_CALEGRUP), .w_CALEGRUP, GetCalRis(.w_RGCODPER) ) )
        .w_RG__DATA = IIF(.cFunction='Load', i_datsys, .w_RG__DATA)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_RG__DATA))
         .link_1_8('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_RGCAUATT))
         .link_2_1('Full')
        endif
        .w_FLSINGPRE = IIF(ISALT(), 'S', 'N')
        .w_STATOATT = 'K'
        .DoRTCalc(11,19,.f.)
        .w_PERSONA = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .DoRTCalc(21,23,.f.)
        .w_TOT_MIN_EFF = MOD(.w_TOTMINEFF, 60)
        .w_TOT_ORE_EFF = .w_TOTOREEFF+INT(.w_TOTMINEFF / 60)
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(IIF(!EMPTY(.w_RG__DATA),Ah_MsgFormat(ALLTRIM(g_GIORNO[DOW(.w_RG__DATA)])),''))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(IIF(.w_GIOLAV=='S' OR EMPTY(.w_RG__DATA),'',Ah_MsgFormat('Giorno festivo')))
        .DoRTCalc(26,27,.f.)
        .w_Comodo = ' '
        .DoRTCalc(29,30,.f.)
        .w_OB_TEST = IIF(EMPTY(.w_RG__DATA), i_INIDAT, .w_RG__DATA)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(32,32,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
      endif
    endwith
    cp_BlankRecExtFlds(this,'RIL_GIOR')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRGCODPER_1_2.enabled = i_bVal
      .Page1.oPag.oRG__DATA_1_8.enabled = i_bVal
      .Page1.oPag.oFLSINGPRE_1_10.enabled = i_bVal
      .Page1.oPag.oSTATOATT_1_11.enabled = i_bVal
      .Page1.oPag.oRG__NOTE_2_7.enabled = i_bVal
      .Page1.oPag.oBtn_1_12.enabled = i_bVal
      .Page1.oPag.oObj_1_21.enabled = i_bVal
      .Page1.oPag.oObj_1_23.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRGCODPER_1_2.enabled = .f.
        .Page1.oPag.oRG__DATA_1_8.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRGCODPER_1_2.enabled = .t.
        .Page1.oPag.oRG__DATA_1_8.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'RIL_GIOR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RIL_GIOR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RGCODPER,"RGCODPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RG__DATA,"RG__DATA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RIL_GIOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIL_GIOR_IDX,2])
    i_lTable = "RIL_GIOR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RIL_GIOR_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_RGCAUATT C(20);
      ,t_RGOGGETT C(254);
      ,t_RGOREPRE N(3);
      ,t_RGMINPRE N(2);
      ,t_RGOREEFF N(3);
      ,t_RGMINEFF N(2);
      ,t_RG__NOTE M(10);
      ,CPROWNUM N(10);
      ,t_CARAGGST C(1);
      ,t_RGATTSER C(20);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mrgbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRGCAUATT_2_1.controlsource=this.cTrsName+'.t_RGCAUATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRGOGGETT_2_2.controlsource=this.cTrsName+'.t_RGOGGETT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRGOREPRE_2_3.controlsource=this.cTrsName+'.t_RGOREPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRGMINPRE_2_4.controlsource=this.cTrsName+'.t_RGMINPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRGOREEFF_2_5.controlsource=this.cTrsName+'.t_RGOREEFF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRGMINEFF_2_6.controlsource=this.cTrsName+'.t_RGMINEFF'
    this.oPgFRm.Page1.oPag.oRG__NOTE_2_7.controlsource=this.cTrsName+'.t_RG__NOTE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(169)
    this.AddVLine(579)
    this.AddVLine(618)
    this.AddVLine(653)
    this.AddVLine(695)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGCAUATT_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RIL_GIOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIL_GIOR_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RIL_GIOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIL_GIOR_IDX,2])
      *
      * insert into RIL_GIOR
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RIL_GIOR')
        i_extval=cp_InsertValODBCExtFlds(this,'RIL_GIOR')
        i_cFldBody=" "+;
                  "(RGCODPER,RG__DATA,RGCAUATT,RGOGGETT,RGOREPRE"+;
                  ",RGMINPRE,RGOREEFF,RGMINEFF,RG__NOTE,RGATTSER,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_RGCODPER)+","+cp_ToStrODBCNull(this.w_RG__DATA)+","+cp_ToStrODBCNull(this.w_RGCAUATT)+","+cp_ToStrODBC(this.w_RGOGGETT)+","+cp_ToStrODBC(this.w_RGOREPRE)+;
             ","+cp_ToStrODBC(this.w_RGMINPRE)+","+cp_ToStrODBC(this.w_RGOREEFF)+","+cp_ToStrODBC(this.w_RGMINEFF)+","+cp_ToStrODBC(this.w_RG__NOTE)+","+cp_ToStrODBC(this.w_RGATTSER)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RIL_GIOR')
        i_extval=cp_InsertValVFPExtFlds(this,'RIL_GIOR')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'RGCODPER',this.w_RGCODPER,'RG__DATA',this.w_RG__DATA)
        INSERT INTO (i_cTable) (;
                   RGCODPER;
                  ,RG__DATA;
                  ,RGCAUATT;
                  ,RGOGGETT;
                  ,RGOREPRE;
                  ,RGMINPRE;
                  ,RGOREEFF;
                  ,RGMINEFF;
                  ,RG__NOTE;
                  ,RGATTSER;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_RGCODPER;
                  ,this.w_RG__DATA;
                  ,this.w_RGCAUATT;
                  ,this.w_RGOGGETT;
                  ,this.w_RGOREPRE;
                  ,this.w_RGMINPRE;
                  ,this.w_RGOREEFF;
                  ,this.w_RGMINEFF;
                  ,this.w_RG__NOTE;
                  ,this.w_RGATTSER;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.RIL_GIOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIL_GIOR_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_RGCAUATT))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'RIL_GIOR')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'RIL_GIOR')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_RGCAUATT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update RIL_GIOR
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'RIL_GIOR')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " RGCAUATT="+cp_ToStrODBCNull(this.w_RGCAUATT)+;
                     ",RGOGGETT="+cp_ToStrODBC(this.w_RGOGGETT)+;
                     ",RGOREPRE="+cp_ToStrODBC(this.w_RGOREPRE)+;
                     ",RGMINPRE="+cp_ToStrODBC(this.w_RGMINPRE)+;
                     ",RGOREEFF="+cp_ToStrODBC(this.w_RGOREEFF)+;
                     ",RGMINEFF="+cp_ToStrODBC(this.w_RGMINEFF)+;
                     ",RG__NOTE="+cp_ToStrODBC(this.w_RG__NOTE)+;
                     ",RGATTSER="+cp_ToStrODBC(this.w_RGATTSER)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'RIL_GIOR')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      RGCAUATT=this.w_RGCAUATT;
                     ,RGOGGETT=this.w_RGOGGETT;
                     ,RGOREPRE=this.w_RGOREPRE;
                     ,RGMINPRE=this.w_RGMINPRE;
                     ,RGOREEFF=this.w_RGOREEFF;
                     ,RGMINEFF=this.w_RGMINEFF;
                     ,RG__NOTE=this.w_RG__NOTE;
                     ,RGATTSER=this.w_RGATTSER;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RIL_GIOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RIL_GIOR_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_RGCAUATT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete RIL_GIOR
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_RGCAUATT))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RIL_GIOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RIL_GIOR_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_RGCODPER<>.w_RGCODPER
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_RGCODPER<>.w_RGCODPER
          .w_CalUsed = IIF(!EMPTY(.w_CALEPERS), .w_CALEPERS, IIF(!EMPTY(.w_CALEGRUP), .w_CALEGRUP, GetCalRis(.w_RGCODPER) ) )
        endif
        .DoRTCalc(7,19,.t.)
        if .o_RGCODPER<>.w_RGCODPER
          .w_PERSONA = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        endif
        .DoRTCalc(21,23,.t.)
          .w_TOT_MIN_EFF = MOD(.w_TOTMINEFF, 60)
          .w_TOT_ORE_EFF = .w_TOTOREEFF+INT(.w_TOTMINEFF / 60)
        if .o_RG__DATA<>.w_RG__DATA
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(IIF(!EMPTY(.w_RG__DATA),Ah_MsgFormat(ALLTRIM(g_GIORNO[DOW(.w_RG__DATA)])),''))
        endif
        if .o_GIOLAV<>.w_GIOLAV.or. .o_RG__DATA<>.w_RG__DATA.or. .o_RGCODPER<>.w_RGCODPER
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(IIF(.w_GIOLAV=='S' OR EMPTY(.w_RG__DATA),'',Ah_MsgFormat('Giorno festivo')))
        endif
        .DoRTCalc(26,27,.t.)
        if .o_RG__DATA<>.w_RG__DATA.or. .o_RGCODPER<>.w_RGCODPER.or. .o_GIOLAV<>.w_GIOLAV
          .w_Comodo = ' '
        endif
        if .o_RGCODPER<>.w_RGCODPER
          .Calculate_XMWWQCTJJM()
        endif
        .DoRTCalc(29,30,.t.)
        if .o_RG__DATA<>.w_RG__DATA
          .w_OB_TEST = IIF(EMPTY(.w_RG__DATA), i_INIDAT, .w_RG__DATA)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(32,33,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CARAGGST with this.w_CARAGGST
      replace t_RGATTSER with this.w_RGATTSER
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(IIF(!EMPTY(.w_RG__DATA),Ah_MsgFormat(ALLTRIM(g_GIORNO[DOW(.w_RG__DATA)])),''))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate(IIF(.w_GIOLAV=='S' OR EMPTY(.w_RG__DATA),'',Ah_MsgFormat('Giorno festivo')))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_JWVTOROHFR()
    with this
          * --- Messaggio assenza calendario init
          .w_GestioneMessaggi = IIF(EMPTY(.w_CALUSED), ah_ErrorMsg("Attenzione, non � specificato nessun calendario di default"), .T.)
    endwith
  endproc
  proc Calculate_XMWWQCTJJM()
    with this
          * --- Messaggio assenza calendario per la risorsa
          .w_GestioneMessaggi = IIF(!EMPTY(.w_RGCODPER) AND EMPTY(.w_CALUSED), ah_ErrorMsg("Attenzione, non � specificato nessun calendario di default"), .T.)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLSINGPRE_1_10.visible=!this.oPgFrm.Page1.oPag.oFLSINGPRE_1_10.mHide()
    this.oPgFrm.Page1.oPag.oSTATOATT_1_11.visible=!this.oPgFrm.Page1.oPag.oSTATOATT_1_11.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_JWVTOROHFR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("New record")
          .Calculate_XMWWQCTJJM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RGCODPER
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RGCODPER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_RGCODPER)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_RGCODPER))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RGCODPER)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_RGCODPER)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_RGCODPER)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_RGCODPER)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_RGCODPER)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RGCODPER) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oRGCODPER_1_2'),i_cWhere,'GSAR_BDZ',"Persone",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RGCODPER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_RGCODPER);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_RGCODPER)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODCAL,DPGRUPRE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RGCODPER = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOME = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME = NVL(_Link_.DPNOME,space(40))
      this.w_CALEPERS = NVL(_Link_.DPCODCAL,space(5))
      this.w_GruppoPred = NVL(_Link_.DPGRUPRE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RGCODPER = space(5)
      endif
      this.w_COGNOME = space(40)
      this.w_NOME = space(40)
      this.w_CALEPERS = space(5)
      this.w_GruppoPred = space(5)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_RGCODPER = space(5)
        this.w_COGNOME = space(40)
        this.w_NOME = space(40)
        this.w_CALEPERS = space(5)
        this.w_GruppoPred = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RGCODPER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GruppoPred
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GruppoPred) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GruppoPred)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCODCAL";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_GruppoPred);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_GruppoPred)
            select DPCODICE,DPCODCAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GruppoPred = NVL(_Link_.DPCODICE,space(5))
      this.w_CALEGRUP = NVL(_Link_.DPCODCAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_GruppoPred = space(5)
      endif
      this.w_CALEGRUP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GruppoPred Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RG__DATA
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAL_AZIE_IDX,3]
    i_lTable = "CAL_AZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2], .t., this.CAL_AZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RG__DATA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAL_AZIE')
        if i_nConn<>0
          i_cWhere = " CAGIORNO="+cp_ToStrODBC(this.w_RG__DATA);
                   +" and CACODCAL="+cp_ToStrODBC(this.w_CalUsed);

          i_ret=cp_SQL(i_nConn,"select CACODCAL,CAGIORNO,CANUMORE,CAGIOLAV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODCAL',this.w_CalUsed;
                     ,'CAGIORNO',this.w_RG__DATA)
          select CACODCAL,CAGIORNO,CANUMORE,CAGIOLAV;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_RG__DATA) and !this.bDontReportError
            deferred_cp_zoom('CAL_AZIE','*','CACODCAL,CAGIORNO',cp_AbsName(oSource.parent,'oRG__DATA_1_8'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CalUsed<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODCAL,CAGIORNO,CANUMORE,CAGIOLAV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CACODCAL,CAGIORNO,CANUMORE,CAGIOLAV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Giorno non presente in calendario")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODCAL,CAGIORNO,CANUMORE,CAGIOLAV";
                     +" from "+i_cTable+" "+i_lTable+" where CAGIORNO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CACODCAL="+cp_ToStrODBC(this.w_CalUsed);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODCAL',oSource.xKey(1);
                       ,'CAGIORNO',oSource.xKey(2))
            select CACODCAL,CAGIORNO,CANUMORE,CAGIOLAV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RG__DATA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODCAL,CAGIORNO,CANUMORE,CAGIOLAV";
                   +" from "+i_cTable+" "+i_lTable+" where CAGIORNO="+cp_ToStrODBC(this.w_RG__DATA);
                   +" and CACODCAL="+cp_ToStrODBC(this.w_CalUsed);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODCAL',this.w_CalUsed;
                       ,'CAGIORNO',this.w_RG__DATA)
            select CACODCAL,CAGIORNO,CANUMORE,CAGIOLAV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RG__DATA = NVL(cp_todate(_Link_.CAGIORNO),ctod("  /  /  "))
      this.w_MinOreLav = NVL(_Link_.CANUMORE,0)
      this.w_GioLav = NVL(_Link_.CAGIOLAV,space(1))
      this.w_ChkData = NVL(cp_ToDate(_Link_.CAGIORNO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_RG__DATA = ctod("  /  /  ")
      endif
      this.w_MinOreLav = 0
      this.w_GioLav = space(1)
      this.w_ChkData = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAL_AZIE_IDX,2])+'\'+cp_ToStr(_Link_.CACODCAL,1)+'\'+cp_ToStr(_Link_.CAGIORNO,1)
      cp_ShowWarn(i_cKey,this.CAL_AZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RG__DATA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RGCAUATT
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RGCAUATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_RGCAUATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_RGCAUATT))
          select CACODICE,CADESCRI,CARAGGST;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RGCAUATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStrODBC(trim(this.w_RGCAUATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESCRI like "+cp_ToStr(trim(this.w_RGCAUATT)+"%");

            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RGCAUATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oRGCAUATT_2_1'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'GSAG1AAT.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RGCAUATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CARAGGST";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_RGCAUATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_RGCAUATT)
            select CACODICE,CADESCRI,CARAGGST;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RGCAUATT = NVL(_Link_.CACODICE,space(20))
      this.w_RGOGGETT = NVL(_Link_.CADESCRI,space(254))
      this.w_CARAGGST = NVL(_Link_.CARAGGST,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_RGCAUATT = space(20)
      endif
      this.w_RGOGGETT = space(254)
      this.w_CARAGGST = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RGCAUATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAUMATTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CACODICE as CACODICE201"+ ",link_2_1.CADESCRI as CADESCRI201"+ ",link_2_1.CARAGGST as CARAGGST201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on RIL_GIOR.RGCAUATT=link_2_1.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and RIL_GIOR.RGCAUATT=link_2_1.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oRGCODPER_1_2.value==this.w_RGCODPER)
      this.oPgFrm.Page1.oPag.oRGCODPER_1_2.value=this.w_RGCODPER
    endif
    if not(this.oPgFrm.Page1.oPag.oRG__DATA_1_8.value==this.w_RG__DATA)
      this.oPgFrm.Page1.oPag.oRG__DATA_1_8.value=this.w_RG__DATA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSINGPRE_1_10.RadioValue()==this.w_FLSINGPRE)
      this.oPgFrm.Page1.oPag.oFLSINGPRE_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATOATT_1_11.RadioValue()==this.w_STATOATT)
      this.oPgFrm.Page1.oPag.oSTATOATT_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRG__NOTE_2_7.value==this.w_RG__NOTE)
      this.oPgFrm.Page1.oPag.oRG__NOTE_2_7.value=this.w_RG__NOTE
      replace t_RG__NOTE with this.oPgFrm.Page1.oPag.oRG__NOTE_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPERSONA_1_16.value==this.w_PERSONA)
      this.oPgFrm.Page1.oPag.oPERSONA_1_16.value=this.w_PERSONA
    endif
    if not(this.oPgFrm.Page1.oPag.oTOT_MIN_EFF_3_4.value==this.w_TOT_MIN_EFF)
      this.oPgFrm.Page1.oPag.oTOT_MIN_EFF_3_4.value=this.w_TOT_MIN_EFF
    endif
    if not(this.oPgFrm.Page1.oPag.oTOT_ORE_EFF_3_5.value==this.w_TOT_ORE_EFF)
      this.oPgFrm.Page1.oPag.oTOT_ORE_EFF_3_5.value=this.w_TOT_ORE_EFF
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGCAUATT_2_1.value==this.w_RGCAUATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGCAUATT_2_1.value=this.w_RGCAUATT
      replace t_RGCAUATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGCAUATT_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGOGGETT_2_2.value==this.w_RGOGGETT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGOGGETT_2_2.value=this.w_RGOGGETT
      replace t_RGOGGETT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGOGGETT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGOREPRE_2_3.value==this.w_RGOREPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGOREPRE_2_3.value=this.w_RGOREPRE
      replace t_RGOREPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGOREPRE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGMINPRE_2_4.value==this.w_RGMINPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGMINPRE_2_4.value=this.w_RGMINPRE
      replace t_RGMINPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGMINPRE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGOREEFF_2_5.value==this.w_RGOREEFF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGOREEFF_2_5.value=this.w_RGOREEFF
      replace t_RGOREEFF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGOREEFF_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGMINEFF_2_6.value==this.w_RGMINEFF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGMINEFF_2_6.value=this.w_RGMINEFF
      replace t_RGMINEFF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGMINEFF_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'RIL_GIOR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_RGCODPER) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oRGCODPER_1_2.SetFocus()
            i_bnoObbl = !empty(.w_RGCODPER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsag_mrg
      IF i_bRes = .t.
          IF .w_TOT_ORE_EFF > 24 OR (.w_TOT_ORE_EFF=24 AND .w_TOT_MIN_EFF>0)
             i_bRes = .f.
             i_bnoChk = .f.
             i_cErrorMsg = Ah_MsgFormat("Sono state inserite pi� di 24 ore")
          ENDIF
          IF i_bRes AND .w_GIOLAV='S' AND .w_TOT_ORE_EFF < .w_MINORELAV
             IF NOT AH_YESNO("Inserite meno ore di quelle previste: prosegui ugualmente?")
               i_bRes = .f.
             ENDIF
          ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_RGCAUATT)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_RGOREEFF <= 24) and (not(Empty(.w_RGCAUATT)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGOREEFF_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 0 e 24")
        case   not(.w_RGMINEFF < 60) and (not(Empty(.w_RGCAUATT)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRGMINEFF_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 0 e 59")
      endcase
      if not(Empty(.w_RGCAUATT))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RGCODPER = this.w_RGCODPER
    this.o_RG__DATA = this.w_RG__DATA
    this.o_GIOLAV = this.w_GIOLAV
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_RGCAUATT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_RGCAUATT=space(20)
      .w_RGOGGETT=space(254)
      .w_RGOREPRE=0
      .w_RGMINPRE=0
      .w_RGOREEFF=0
      .w_RGMINEFF=0
      .w_RG__NOTE=space(0)
      .w_CARAGGST=space(1)
      .w_RGATTSER=space(20)
      .DoRTCalc(1,8,.f.)
      if not(empty(.w_RGCAUATT))
        .link_2_1('Full')
      endif
    endwith
    this.DoRTCalc(9,33,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_RGCAUATT = t_RGCAUATT
    this.w_RGOGGETT = t_RGOGGETT
    this.w_RGOREPRE = t_RGOREPRE
    this.w_RGMINPRE = t_RGMINPRE
    this.w_RGOREEFF = t_RGOREEFF
    this.w_RGMINEFF = t_RGMINEFF
    this.w_RG__NOTE = t_RG__NOTE
    this.w_CARAGGST = t_CARAGGST
    this.w_RGATTSER = t_RGATTSER
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_RGCAUATT with this.w_RGCAUATT
    replace t_RGOGGETT with this.w_RGOGGETT
    replace t_RGOREPRE with this.w_RGOREPRE
    replace t_RGMINPRE with this.w_RGMINPRE
    replace t_RGOREEFF with this.w_RGOREEFF
    replace t_RGMINEFF with this.w_RGMINEFF
    replace t_RG__NOTE with this.w_RG__NOTE
    replace t_CARAGGST with this.w_CARAGGST
    replace t_RGATTSER with this.w_RGATTSER
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTOREEFF = .w_TOTOREEFF-.w_rgoreeff
        .w_TOTMINEFF = .w_TOTMINEFF-.w_rgmineff
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mrgPag1 as StdContainer
  Width  = 745
  height = 387
  stdWidth  = 745
  stdheight = 387
  resizeXpos=497
  resizeYpos=256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRGCODPER_1_2 as StdField with uid="GKQPVBFXUP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RGCODPER", cQueryName = "RGCODPER",nZero=5,;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    ToolTipText = "Codice persona",;
    HelpContextID = 37137048,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=62, Top=10, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_RGCODPER"

  func oRGCODPER_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oRGCODPER_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRGCODPER_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oRGCODPER_1_2.readonly and this.parent.oRGCODPER_1_2.isprimarykey)
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oRGCODPER_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
   endif
  endproc
  proc oRGCODPER_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TipoRisorsa
     i_obj.w_DPCODICE=this.parent.oContained.w_RGCODPER
    i_obj.ecpSave()
  endproc

  add object oRG__DATA_1_8 as StdField with uid="YSOVUUDDSP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_RG__DATA", cQueryName = "RGCODPER,RG__DATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Giorno non presente in calendario",;
    ToolTipText = "Data",;
    HelpContextID = 249238871,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=62, Top=37, cLinkFile="CAL_AZIE", oKey_1_1="CACODCAL", oKey_1_2="this.w_CalUsed", oKey_2_1="CAGIORNO", oKey_2_2="this.w_RG__DATA"

  func oRG__DATA_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oRG__DATA_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oFLSINGPRE_1_10 as StdCheck with uid="GSGHSZDVLA",rtseq=9,rtrep=.f.,left=502, top=10, caption="Inserisci una riga per ogni prestaz.",;
    ToolTipText = "Se attivo, per le attivit� provenienti da inserimento provvisorio inserisce una riga per ogni prestazione con la relativa descrizione.",;
    HelpContextID = 90463736,;
    cFormVar="w_FLSINGPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSINGPRE_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLSINGPRE,&i_cF..t_FLSINGPRE),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oFLSINGPRE_1_10.GetRadio()
    this.Parent.oContained.w_FLSINGPRE = this.RadioValue()
    return .t.
  endfunc

  func oFLSINGPRE_1_10.ToRadio()
    this.Parent.oContained.w_FLSINGPRE=trim(this.Parent.oContained.w_FLSINGPRE)
    return(;
      iif(this.Parent.oContained.w_FLSINGPRE=='S',1,;
      0))
  endfunc

  func oFLSINGPRE_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oFLSINGPRE_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction="Query" OR NOT ISALT())
    endwith
    endif
  endfunc


  add object oSTATOATT_1_11 as StdCombo with uid="BEXRHNORFF",rtseq=10,rtrep=.f.,left=560,top=32,width=123,height=21;
    , HelpContextID = 259932794;
    , cFormVar="w_STATOATT",RowSource=""+"Tutte,"+"Evase o completate", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATOATT_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..STATOATT,&i_cF..t_STATOATT),this.value)
    return(iif(xVal =1,'K',;
    iif(xVal =2,'B',;
    space(1))))
  endfunc
  func oSTATOATT_1_11.GetRadio()
    this.Parent.oContained.w_STATOATT = this.RadioValue()
    return .t.
  endfunc

  func oSTATOATT_1_11.ToRadio()
    this.Parent.oContained.w_STATOATT=trim(this.Parent.oContained.w_STATOATT)
    return(;
      iif(this.Parent.oContained.w_STATOATT=='K',1,;
      iif(this.Parent.oContained.w_STATOATT=='B',2,;
      0)))
  endfunc

  func oSTATOATT_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oSTATOATT_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction="Query" )
    endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="HYLMITTXNR",left=695, top=10, width=48,height=45,;
    CpPicture="bmp\Controlla.ico", caption="", nPag=1;
    , ToolTipText = "Premere per importare le attivit� presenti in agenda";
    , HelpContextID = 80497286;
    , Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSAG_BRE(this.Parent.oContained,"S", .w_RG__DATA, .w_RG__DATA, .w_RGCODPER, .w_STATOATT, .w_FLSINGPRE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction="Query")
    endwith
   endif
  endfunc

  add object oPERSONA_1_16 as StdField with uid="IZVOFQWVWV",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PERSONA", cQueryName = "PERSONA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 209601270,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=130, Top=10, InputMask=replicate('X',40)


  add object oObj_1_21 as cp_calclbl with uid="JLTULURDTA",left=144, top=37, width=131,height=25,;
    caption='Giorno',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 261043814


  add object oObj_1_23 as cp_calclbl with uid="DAEYHFRNBG",left=283, top=37, width=148,height=25,;
    caption='DesFestivo',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 220993281


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=14, top=81, width=728,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="RGCAUATT",Label1="Tipo attivit�",Field2="RGOGGETT",Label2="Oggetto",Field3="RGOREPRE",Label3="hh",Field4="RGMINPRE",Label4="mm",Field5="RGOREEFF",Label5="hh",Field6="RGMINEFF",Label6="mm",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202179194

  add object oStr_1_7 as StdString with uid="FWMGKYXTTV",Visible=.t., Left=-1, Top=12,;
    Alignment=1, Width=58, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="QLXSRKNDUZ",Visible=.t., Left=13, Top=39,;
    Alignment=1, Width=44, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="AIRSRUBLAN",Visible=.t., Left=17, Top=297,;
    Alignment=0, Width=175, Height=18,;
    Caption="Note attivit�"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="FNKNXVUPUF",Visible=.t., Left=570, Top=65,;
    Alignment=2, Width=89, Height=15,;
    Caption="Durata pianif."  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="ITFFYTCETG",Visible=.t., Left=666, Top=65,;
    Alignment=2, Width=75, Height=15,;
    Caption="Durata eff."  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="RWQOVZEDRN",Visible=.t., Left=29, Top=442,;
    Alignment=0, Width=587, Height=18,;
    Caption="Attenzione! CalUsed deve essere in sequenza prima di RG___DATA e TipoRisorsa prima di RGCODUTE"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="YENSPNETWA",Visible=.t., Left=475, Top=37,;
    Alignment=1, Width=82, Height=18,;
    Caption="Importa attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.cFunction="Query" )
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=4,top=100,;
    width=724+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=5,top=101,width=723+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CAUMATTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oRG__NOTE_2_7.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CAUMATTI'
        oDropInto=this.oBodyCol.oRow.oRGCAUATT_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oRG__NOTE_2_7 as StdTrsMemo with uid="MZALYJRQWM",rtseq=16,rtrep=.t.,;
    cFormVar="w_RG__NOTE",value=space(0),;
    HelpContextID = 226170203,;
    cTotal="", bFixedPos=.t., cQueryName = "RG__NOTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=68, Width=726, Left=14, Top=317

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOT_MIN_EFF_3_4 as StdField with uid="OEXRBCLVXS",rtseq=24,rtrep=.f.,;
    cFormVar="w_TOT_MIN_EFF",value=0,enabled=.f.,;
    HelpContextID = 124721125,;
    cQueryName = "TOT_MIN_EFF",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=695, Top=294, cSayPict=["99"], cGetPict=["99"]

  add object oTOT_ORE_EFF_3_5 as StdField with uid="JDMQURZKIE",rtseq=25,rtrep=.f.,;
    cFormVar="w_TOT_ORE_EFF",value=0,enabled=.f.,;
    HelpContextID = 259057691,;
    cQueryName = "TOT_ORE_EFF",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=659, Top=294, cSayPict=["999"], cGetPict=["999"]

  add object oStr_3_1 as StdString with uid="TKIJYALGAD",Visible=.t., Left=557, Top=295,;
    Alignment=1, Width=95, Height=18,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsag_mrgBodyRow as CPBodyRowCnt
  Width=714
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oRGCAUATT_2_1 as StdTrsField with uid="USHJSBCRHX",rtseq=8,rtrep=.t.,;
    cFormVar="w_RGCAUATT",value=space(20),;
    ToolTipText = "Tipo attivit�",;
    HelpContextID = 264983914,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=-2, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_RGCAUATT"

  func oRGCAUATT_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oRGCAUATT_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRGCAUATT_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oRGCAUATT_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'GSAG1AAT.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oRGCAUATT_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_RGCAUATT
    i_obj.ecpSave()
  endproc

  add object oRGOGGETT_2_2 as StdTrsField with uid="YGBTJHMLZN",rtseq=11,rtrep=.t.,;
    cFormVar="w_RGOGGETT",value=space(254),;
    ToolTipText = "Oggetto dell'attivit�",;
    HelpContextID = 49419626,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=405, Left=156, Top=0, InputMask=replicate('X',254)

  add object oRGOREPRE_2_3 as StdTrsField with uid="BOMNXXIGQL",rtseq=12,rtrep=.t.,;
    cFormVar="w_RGOREPRE",value=0,enabled=.f.,;
    ToolTipText = "Durata pianificata dell'attivit� (in ore)",;
    HelpContextID = 232592731,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=566, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oRGMINPRE_2_4 as StdTrsField with uid="FHKMTKYQKU",rtseq=13,rtrep=.t.,;
    cFormVar="w_RGMINPRE",value=0,enabled=.f.,;
    ToolTipText = "Durata pianificata dell'attivit� (in minuti)",;
    HelpContextID = 241431899,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=605, Top=0, cSayPict=["99"], cGetPict=["99"]

  add object oRGOREEFF_2_5 as StdTrsField with uid="GAJYHWDACQ",rtseq=14,rtrep=.t.,;
    cFormVar="w_RGOREEFF",value=0,;
    ToolTipText = "Durata effettiva dell'attivit� (in ore)",;
    HelpContextID = 220392100,;
    cTotal = "this.Parent.oContained.w_totoreeff", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 0 e 24",;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=643, Top=0, cSayPict=["999"], cGetPict=["999"]

  func oRGOREEFF_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RGOREEFF <= 24)
    endwith
    return bRes
  endfunc

  add object oRGMINEFF_2_6 as StdTrsField with uid="IEFQJWSBCP",rtseq=15,rtrep=.t.,;
    cFormVar="w_RGMINEFF",value=0,;
    ToolTipText = "Durata effettiva dell'attivit� (in minuti)",;
    HelpContextID = 211552932,;
    cTotal = "this.Parent.oContained.w_totmineff", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 0 e 59",;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=682, Top=0, cSayPict=["99"], cGetPict=["99"]

  func oRGMINEFF_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_RGMINEFF < 60)
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oRGCAUATT_2_1.When()
    return(.t.)
  proc oRGCAUATT_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oRGCAUATT_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mrg','RIL_GIOR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RGCODPER=RIL_GIOR.RGCODPER";
  +" and "+i_cAliasName2+".RG__DATA=RIL_GIOR.RG__DATA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
