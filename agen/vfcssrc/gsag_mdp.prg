* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mdp                                                        *
*              Dettaglio partecipanti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-03-01                                                      *
* Last revis.: 2012-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsag_mdp")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsag_mdp")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsag_mdp")
  return

* --- Class definition
define class tgsag_mdp as StdPCForm
  Width  = 672
  Height = 234
  Top    = 12
  Left   = 10
  cComment = "Dettaglio partecipanti"
  cPrg = "gsag_mdp"
  HelpContextID=188078231
  add object cnt as tcgsag_mdp
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsag_mdp as PCContext
  w_DFCODICE = space(20)
  w_CPROWORD = 0
  w_DFTIPRIS = space(1)
  w_COGNOME = space(40)
  w_DFCODRIS = space(5)
  w_TIPOGRUP = space(1)
  w_DFGRURIS = space(5)
  w_PATIPRIS = space(1)
  w_DENOM = space(101)
  w_OB_TEST = space(8)
  w_DESCRI = space(40)
  w_TipoRisorsa = space(1)
  w_CODUTE = 0
  w_DATOBSO = space(8)
  w_PAFLVISI = space(1)
  w_DFFLGPRE = space(1)
  w_NOME = space(40)
  w_PAFLVISI = space(1)
  proc Save(i_oFrom)
    this.w_DFCODICE = i_oFrom.w_DFCODICE
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_DFTIPRIS = i_oFrom.w_DFTIPRIS
    this.w_COGNOME = i_oFrom.w_COGNOME
    this.w_DFCODRIS = i_oFrom.w_DFCODRIS
    this.w_TIPOGRUP = i_oFrom.w_TIPOGRUP
    this.w_DFGRURIS = i_oFrom.w_DFGRURIS
    this.w_PATIPRIS = i_oFrom.w_PATIPRIS
    this.w_DENOM = i_oFrom.w_DENOM
    this.w_OB_TEST = i_oFrom.w_OB_TEST
    this.w_DESCRI = i_oFrom.w_DESCRI
    this.w_TipoRisorsa = i_oFrom.w_TipoRisorsa
    this.w_CODUTE = i_oFrom.w_CODUTE
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_PAFLVISI = i_oFrom.w_PAFLVISI
    this.w_DFFLGPRE = i_oFrom.w_DFFLGPRE
    this.w_NOME = i_oFrom.w_NOME
    this.w_PAFLVISI = i_oFrom.w_PAFLVISI
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DFCODICE = this.w_DFCODICE
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_DFTIPRIS = this.w_DFTIPRIS
    i_oTo.w_COGNOME = this.w_COGNOME
    i_oTo.w_DFCODRIS = this.w_DFCODRIS
    i_oTo.w_TIPOGRUP = this.w_TIPOGRUP
    i_oTo.w_DFGRURIS = this.w_DFGRURIS
    i_oTo.w_PATIPRIS = this.w_PATIPRIS
    i_oTo.w_DENOM = this.w_DENOM
    i_oTo.w_OB_TEST = this.w_OB_TEST
    i_oTo.w_DESCRI = this.w_DESCRI
    i_oTo.w_TipoRisorsa = this.w_TipoRisorsa
    i_oTo.w_CODUTE = this.w_CODUTE
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_PAFLVISI = this.w_PAFLVISI
    i_oTo.w_DFFLGPRE = this.w_DFFLGPRE
    i_oTo.w_NOME = this.w_NOME
    i_oTo.w_PAFLVISI = this.w_PAFLVISI
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsag_mdp as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 672
  Height = 234
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-04-10"
  HelpContextID=188078231
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DEF_PART_IDX = 0
  DIPENDEN_IDX = 0
  cFile = "DEF_PART"
  cKeySelect = "DFCODICE"
  cKeyWhere  = "DFCODICE=this.w_DFCODICE"
  cKeyDetail  = "DFCODICE=this.w_DFCODICE"
  cKeyWhereODBC = '"DFCODICE="+cp_ToStrODBC(this.w_DFCODICE)';

  cKeyDetailWhereODBC = '"DFCODICE="+cp_ToStrODBC(this.w_DFCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DEF_PART.DFCODICE="+cp_ToStrODBC(this.w_DFCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DEF_PART.CPROWORD '
  cPrg = "gsag_mdp"
  cComment = "Dettaglio partecipanti"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DFCODICE = space(20)
  w_CPROWORD = 0
  w_DFTIPRIS = space(1)
  o_DFTIPRIS = space(1)
  w_COGNOME = space(40)
  w_DFCODRIS = space(5)
  o_DFCODRIS = space(5)
  w_TIPOGRUP = space(1)
  w_DFGRURIS = space(5)
  w_PATIPRIS = space(1)
  w_DENOM = space(101)
  w_OB_TEST = ctod('  /  /  ')
  w_DESCRI = space(40)
  w_TipoRisorsa = space(1)
  w_CODUTE = 0
  w_DATOBSO = ctod('  /  /  ')
  w_PAFLVISI = space(1)
  w_DFFLGPRE = space(1)
  w_NOME = space(40)
  w_PAFLVISI = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_mdpPag1","gsag_mdp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='DEF_PART'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DEF_PART_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DEF_PART_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsag_mdp'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_6_joined
    link_2_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DEF_PART where DFCODICE=KeySet.DFCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DEF_PART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DEF_PART_IDX,2],this.bLoadRecFilter,this.DEF_PART_IDX,"gsag_mdp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DEF_PART')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DEF_PART.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DEF_PART '
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DFCODICE',this.w_DFCODICE  )
      select * from (i_cTable) DEF_PART where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OB_TEST = ctod("  /  /  ")
        .w_PAFLVISI = space(1)
        .w_PAFLVISI = space(1)
        .w_DFCODICE = NVL(DFCODICE,space(20))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DEF_PART')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_COGNOME = space(40)
        .w_TIPOGRUP = 'G'
          .w_DESCRI = space(40)
        .w_TipoRisorsa = 'P'
          .w_CODUTE = 0
          .w_DATOBSO = ctod("  /  /  ")
          .w_NOME = space(40)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DFTIPRIS = NVL(DFTIPRIS,space(1))
          .w_DFCODRIS = NVL(DFCODRIS,space(5))
          .link_2_4('Load')
          .w_DFGRURIS = NVL(DFGRURIS,space(5))
          if link_2_6_joined
            this.w_DFGRURIS = NVL(DPCODICE206,NVL(this.w_DFGRURIS,space(5)))
            this.w_TIPOGRUP = NVL(DPTIPRIS206,space(1))
          else
          .link_2_6('Load')
          endif
        .w_PATIPRIS = .w_DFTIPRIS
        .w_DENOM = IIF(.w_DFTIPRIS='P',alltrim(.w_COGNOME)+' '+alltrim(.w_NOME),alltrim(.w_DESCRI))
          .w_DFFLGPRE = NVL(DFFLGPRE,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DFCODICE=space(20)
      .w_CPROWORD=10
      .w_DFTIPRIS=space(1)
      .w_COGNOME=space(40)
      .w_DFCODRIS=space(5)
      .w_TIPOGRUP=space(1)
      .w_DFGRURIS=space(5)
      .w_PATIPRIS=space(1)
      .w_DENOM=space(101)
      .w_OB_TEST=ctod("  /  /  ")
      .w_DESCRI=space(40)
      .w_TipoRisorsa=space(1)
      .w_CODUTE=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_PAFLVISI=space(1)
      .w_DFFLGPRE=space(1)
      .w_NOME=space(40)
      .w_PAFLVISI=space(1)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,2,.f.)
        .w_DFTIPRIS = 'P'
        .DoRTCalc(4,4,.f.)
        .w_DFCODRIS = ''
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_DFCODRIS))
         .link_2_4('Full')
        endif
        .w_TIPOGRUP = 'G'
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_DFGRURIS))
         .link_2_6('Full')
        endif
        .w_PATIPRIS = .w_DFTIPRIS
        .w_DENOM = IIF(.w_DFTIPRIS='P',alltrim(.w_COGNOME)+' '+alltrim(.w_NOME),alltrim(.w_DESCRI))
        .DoRTCalc(10,11,.f.)
        .w_TipoRisorsa = 'P'
      endif
    endwith
    cp_BlankRecExtFlds(this,'DEF_PART')
    this.DoRTCalc(13,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DEF_PART',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DEF_PART_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCODICE,"DFCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_DFTIPRIS N(3);
      ,t_DFCODRIS C(5);
      ,t_DFGRURIS C(5);
      ,t_DENOM C(101);
      ,t_DFFLGPRE N(3);
      ,CPROWNUM N(10);
      ,t_COGNOME C(40);
      ,t_TIPOGRUP C(1);
      ,t_PATIPRIS C(1);
      ,t_DESCRI C(40);
      ,t_TipoRisorsa C(1);
      ,t_CODUTE N(4);
      ,t_DATOBSO D(8);
      ,t_NOME C(40);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mdpbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPRIS_2_2.controlsource=this.cTrsName+'.t_DFTIPRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODRIS_2_4.controlsource=this.cTrsName+'.t_DFCODRIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFGRURIS_2_6.controlsource=this.cTrsName+'.t_DFGRURIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDENOM_2_8.controlsource=this.cTrsName+'.t_DENOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLGPRE_2_13.controlsource=this.cTrsName+'.t_DFFLGPRE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(27)
    this.AddVLine(78)
    this.AddVLine(194)
    this.AddVLine(266)
    this.AddVLine(331)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DEF_PART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DEF_PART_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DEF_PART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DEF_PART_IDX,2])
      *
      * insert into DEF_PART
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DEF_PART')
        i_extval=cp_InsertValODBCExtFlds(this,'DEF_PART')
        i_cFldBody=" "+;
                  "(DFCODICE,CPROWORD,DFTIPRIS,DFCODRIS,DFGRURIS"+;
                  ",DFFLGPRE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DFCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_DFTIPRIS)+","+cp_ToStrODBCNull(this.w_DFCODRIS)+","+cp_ToStrODBCNull(this.w_DFGRURIS)+;
             ","+cp_ToStrODBC(this.w_DFFLGPRE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DEF_PART')
        i_extval=cp_InsertValVFPExtFlds(this,'DEF_PART')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DFCODICE',this.w_DFCODICE)
        INSERT INTO (i_cTable) (;
                   DFCODICE;
                  ,CPROWORD;
                  ,DFTIPRIS;
                  ,DFCODRIS;
                  ,DFGRURIS;
                  ,DFFLGPRE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DFCODICE;
                  ,this.w_CPROWORD;
                  ,this.w_DFTIPRIS;
                  ,this.w_DFCODRIS;
                  ,this.w_DFGRURIS;
                  ,this.w_DFFLGPRE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DEF_PART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DEF_PART_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DFCODRIS)) and not(Empty(t_CPROWORD))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DEF_PART')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DEF_PART')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DFCODRIS)) and not(Empty(t_CPROWORD))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DEF_PART
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DEF_PART')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DFTIPRIS="+cp_ToStrODBC(this.w_DFTIPRIS)+;
                     ",DFCODRIS="+cp_ToStrODBCNull(this.w_DFCODRIS)+;
                     ",DFGRURIS="+cp_ToStrODBCNull(this.w_DFGRURIS)+;
                     ",DFFLGPRE="+cp_ToStrODBC(this.w_DFFLGPRE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DEF_PART')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,DFTIPRIS=this.w_DFTIPRIS;
                     ,DFCODRIS=this.w_DFCODRIS;
                     ,DFGRURIS=this.w_DFGRURIS;
                     ,DFFLGPRE=this.w_DFFLGPRE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DEF_PART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DEF_PART_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DFCODRIS)) and not(Empty(t_CPROWORD))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DEF_PART
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DFCODRIS)) and not(Empty(t_CPROWORD))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DEF_PART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DEF_PART_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,4,.t.)
        if .o_DFTIPRIS<>.w_DFTIPRIS
          .w_DFCODRIS = ''
          .link_2_4('Full')
        endif
        .DoRTCalc(6,6,.t.)
        if .o_DFTIPRIS<>.w_DFTIPRIS
          .link_2_6('Full')
        endif
        if .o_DFTIPRIS<>.w_DFTIPRIS
          .w_PATIPRIS = .w_DFTIPRIS
        endif
        if .o_DFCODRIS<>.w_DFCODRIS
          .w_DENOM = IIF(.w_DFTIPRIS='P',alltrim(.w_COGNOME)+' '+alltrim(.w_NOME),alltrim(.w_DESCRI))
        endif
        if .o_DFCODRIS<>.w_DFCODRIS
          .Calculate_AOEAIMFQYQ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_COGNOME with this.w_COGNOME
      replace t_TIPOGRUP with this.w_TIPOGRUP
      replace t_PATIPRIS with this.w_PATIPRIS
      replace t_DESCRI with this.w_DESCRI
      replace t_TipoRisorsa with this.w_TipoRisorsa
      replace t_CODUTE with this.w_CODUTE
      replace t_DATOBSO with this.w_DATOBSO
      replace t_NOME with this.w_NOME
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_AOEAIMFQYQ()
    with this
          * --- Cambio codice risorsa aggiorno TIPOGRUP (PACODRIS deve restare sia in depends on che in event pena il non funzionamento tramite zoom)
          .w_TIPOGRUP = IIF(.w_DFTIPRIS='G', '', 'G')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFGRURIS_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFGRURIS_2_6.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("w_DFCODRIS Changed")
          .Calculate_AOEAIMFQYQ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DFCODRIS
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFCODRIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_DFCODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DFTIPRIS);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_DFTIPRIS;
                     ,'DPCODICE',trim(this.w_DFCODRIS))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFCODRIS)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_DFCODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DFTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_DFCODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DFTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_DFCODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DFTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_DFCODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DFTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_DFCODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DFTIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_DFCODRIS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_DFTIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DFCODRIS) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oDFCODRIS_2_4'),i_cWhere,'GSAR_BDZ',"Persone/Risorse/Gruppi",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DFTIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_DFTIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFCODRIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_DFCODRIS);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_DFTIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_DFTIPRIS;
                       ,'DPCODICE',this.w_DFCODRIS)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI,DPGRUPRE,DPCODUTE,DPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFCODRIS = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOME = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRI = NVL(_Link_.DPDESCRI,space(40))
      this.w_TipoRisorsa = NVL(_Link_.DPTIPRIS,space(1))
      this.w_DFGRURIS = NVL(_Link_.DPGRUPRE,space(5))
      this.w_CODUTE = NVL(_Link_.DPCODUTE,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.DPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DFCODRIS = space(5)
      endif
      this.w_COGNOME = space(40)
      this.w_NOME = space(40)
      this.w_DESCRI = space(40)
      this.w_TipoRisorsa = space(1)
      this.w_DFGRURIS = space(5)
      this.w_CODUTE = 0
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DFTIPRIS=.w_TipoRisorsa AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_DFCODRIS = space(5)
        this.w_COGNOME = space(40)
        this.w_NOME = space(40)
        this.w_DESCRI = space(40)
        this.w_TipoRisorsa = space(1)
        this.w_DFGRURIS = space(5)
        this.w_CODUTE = 0
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFCODRIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DFGRURIS
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFGRURIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_DFGRURIS)+"%");

          i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPCODICE',trim(this.w_DFGRURIS))
          select DPCODICE,DPTIPRIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DFGRURIS)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DFGRURIS) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPCODICE',cp_AbsName(oSource.parent,'oDFGRURIS_2_6'),i_cWhere,'',"Gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',oSource.xKey(1))
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFGRURIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPTIPRIS";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_DFGRURIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_DFGRURIS)
            select DPCODICE,DPTIPRIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFGRURIS = NVL(_Link_.DPCODICE,space(5))
      this.w_TIPOGRUP = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DFGRURIS = space(5)
      endif
      this.w_TIPOGRUP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_DFCODRIS , .w_DFGRURIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
        endif
        this.w_DFGRURIS = space(5)
        this.w_TIPOGRUP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFGRURIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DIPENDEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.DPCODICE as DPCODICE206"+ ",link_2_6.DPTIPRIS as DPTIPRIS206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on DEF_PART.DFGRURIS=link_2_6.DPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and DEF_PART.DFGRURIS=link_2_6.DPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPRIS_2_2.RadioValue()==this.w_DFTIPRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPRIS_2_2.SetRadio()
      replace t_DFTIPRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPRIS_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODRIS_2_4.value==this.w_DFCODRIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODRIS_2_4.value=this.w_DFCODRIS
      replace t_DFCODRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODRIS_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFGRURIS_2_6.value==this.w_DFGRURIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFGRURIS_2_6.value=this.w_DFGRURIS
      replace t_DFGRURIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFGRURIS_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENOM_2_8.value==this.w_DENOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENOM_2_8.value=this.w_DENOM
      replace t_DENOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDENOM_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLGPRE_2_13.RadioValue()==this.w_DFFLGPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLGPRE_2_13.SetRadio()
      replace t_DFFLGPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLGPRE_2_13.value
    endif
    cp_SetControlsValueExtFlds(this,'DEF_PART')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_DFTIPRIS) and (not(Empty(.w_DFCODRIS)) and not(Empty(.w_CPROWORD)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPRIS_2_2
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(.w_DFTIPRIS=.w_TipoRisorsa AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OB_TEST)) and not(empty(.w_DFCODRIS)) and (not(Empty(.w_DFCODRIS)) and not(Empty(.w_CPROWORD)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFCODRIS_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
        case   not(.w_TIPOGRUP='G' AND GSAR1BGP( this , 'CHK', .w_DFCODRIS , .w_DFGRURIS)) and (.w_DFTIPRIS='P' AND !EMPTY(.w_DFCODRIS) ) and not(empty(.w_DFGRURIS)) and (not(Empty(.w_DFCODRIS)) and not(Empty(.w_CPROWORD)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFGRURIS_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Gruppo inesistente o non associato al partecipante")
      endcase
      if not(Empty(.w_DFCODRIS)) and not(Empty(.w_CPROWORD))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DFTIPRIS = this.w_DFTIPRIS
    this.o_DFCODRIS = this.w_DFCODRIS
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DFCODRIS)) and not(Empty(t_CPROWORD)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_DFTIPRIS=space(1)
      .w_COGNOME=space(40)
      .w_DFCODRIS=space(5)
      .w_TIPOGRUP=space(1)
      .w_DFGRURIS=space(5)
      .w_PATIPRIS=space(1)
      .w_DENOM=space(101)
      .w_DESCRI=space(40)
      .w_TipoRisorsa=space(1)
      .w_CODUTE=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_DFFLGPRE=space(1)
      .w_NOME=space(40)
      .DoRTCalc(1,2,.f.)
        .w_DFTIPRIS = 'P'
      .DoRTCalc(4,4,.f.)
        .w_DFCODRIS = ''
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_DFCODRIS))
        .link_2_4('Full')
      endif
        .w_TIPOGRUP = 'G'
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_DFGRURIS))
        .link_2_6('Full')
      endif
        .w_PATIPRIS = .w_DFTIPRIS
        .w_DENOM = IIF(.w_DFTIPRIS='P',alltrim(.w_COGNOME)+' '+alltrim(.w_NOME),alltrim(.w_DESCRI))
      .DoRTCalc(10,11,.f.)
        .w_TipoRisorsa = 'P'
    endwith
    this.DoRTCalc(13,18,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_DFTIPRIS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPRIS_2_2.RadioValue(.t.)
    this.w_COGNOME = t_COGNOME
    this.w_DFCODRIS = t_DFCODRIS
    this.w_TIPOGRUP = t_TIPOGRUP
    this.w_DFGRURIS = t_DFGRURIS
    this.w_PATIPRIS = t_PATIPRIS
    this.w_DENOM = t_DENOM
    this.w_DESCRI = t_DESCRI
    this.w_TipoRisorsa = t_TipoRisorsa
    this.w_CODUTE = t_CODUTE
    this.w_DATOBSO = t_DATOBSO
    this.w_DFFLGPRE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLGPRE_2_13.RadioValue(.t.)
    this.w_NOME = t_NOME
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DFTIPRIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFTIPRIS_2_2.ToRadio()
    replace t_COGNOME with this.w_COGNOME
    replace t_DFCODRIS with this.w_DFCODRIS
    replace t_TIPOGRUP with this.w_TIPOGRUP
    replace t_DFGRURIS with this.w_DFGRURIS
    replace t_PATIPRIS with this.w_PATIPRIS
    replace t_DENOM with this.w_DENOM
    replace t_DESCRI with this.w_DESCRI
    replace t_TipoRisorsa with this.w_TipoRisorsa
    replace t_CODUTE with this.w_CODUTE
    replace t_DATOBSO with this.w_DATOBSO
    replace t_DFFLGPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLGPRE_2_13.ToRadio()
    replace t_NOME with this.w_NOME
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mdpPag1 as StdContainer
  Width  = 668
  height = 234
  stdWidth  = 668
  stdheight = 234
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=15, width=657,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="DFFLGPRE",Label1="",Field2="CPROWORD",Label2="Riga",Field3="DFTIPRIS",Label3="Tipo partecipante",Field4="DFCODRIS",Label4="Partecipante  ",Field5="DFGRURIS",Label5="Gruppo  ",Field6="DENOM",Label6="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 32701830

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=35,;
    width=651+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=36,width=650+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='DIPENDEN|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='DIPENDEN'
        oDropInto=this.oBodyCol.oRow.oDFCODRIS_2_4
      case cFile='DIPENDEN'
        oDropInto=this.oBodyCol.oRow.oDFGRURIS_2_6
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsag_mdpBodyRow as CPBodyRowCnt
  Width=641
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="WXGSKRPPXH",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 373398,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=20, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oDFTIPRIS_2_2 as StdTrsCombo with uid="UTRDUISWXK",rtrep=.t.,;
    cFormVar="w_DFTIPRIS", RowSource=""+"Persona,"+"Risorsa,"+"Gruppo" , ;
    ToolTipText = "Tipo risorsa",;
    HelpContextID = 42230665,;
    Height=21, Width=112, Left=72, Top=1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDFTIPRIS_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DFTIPRIS,&i_cF..t_DFTIPRIS),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'R',;
    iif(xVal =3,'G',;
    space(1)))))
  endfunc
  func oDFTIPRIS_2_2.GetRadio()
    this.Parent.oContained.w_DFTIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oDFTIPRIS_2_2.ToRadio()
    this.Parent.oContained.w_DFTIPRIS=trim(this.Parent.oContained.w_DFTIPRIS)
    return(;
      iif(this.Parent.oContained.w_DFTIPRIS=='P',1,;
      iif(this.Parent.oContained.w_DFTIPRIS=='R',2,;
      iif(this.Parent.oContained.w_DFTIPRIS=='G',3,;
      0))))
  endfunc

  func oDFTIPRIS_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDFTIPRIS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DFCODRIS)
        bRes2=.link_2_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDFCODRIS_2_4 as StdTrsField with uid="OFBNCUROJE",rtseq=5,rtrep=.t.,;
    cFormVar="w_DFCODRIS",value=space(5),nZero=5,;
    ToolTipText = "Codice partecipante",;
    HelpContextID = 29971337,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=68, Left=188, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_DFTIPRIS", oKey_2_1="DPCODICE", oKey_2_2="this.w_DFCODRIS"

  func oDFCODRIS_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFCODRIS_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDFCODRIS_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_DFTIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_DFTIPRIS)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oDFCODRIS_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone/Risorse/Gruppi",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oDFCODRIS_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_DFTIPRIS
     i_obj.w_DPCODICE=this.parent.oContained.w_DFCODRIS
    i_obj.ecpSave()
  endproc

  add object oDFGRURIS_2_6 as StdTrsField with uid="KCQVZGWJVF",rtseq=7,rtrep=.t.,;
    cFormVar="w_DFGRURIS",value=space(5),;
    ToolTipText = "Gruppo partecipante",;
    HelpContextID = 48010121,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo inesistente o non associato al partecipante",;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=260, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPCODICE", oKey_1_2="this.w_DFGRURIS"

  func oDFGRURIS_2_6.mCond()
    with this.Parent.oContained
      return (.w_DFTIPRIS='P' AND !EMPTY(.w_DFCODRIS) )
    endwith
  endfunc

  func oDFGRURIS_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oDFGRURIS_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDFGRURIS_2_6.mZoom
      with this.Parent.oContained
        GSAR1BGP(this.Parent.oContained,  "", .w_DFCODRIS, "", "DFGRURIS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDENOM_2_8 as StdTrsField with uid="FTUJLXKVIQ",rtseq=9,rtrep=.t.,;
    cFormVar="w_DENOM",value=space(101),enabled=.f.,;
    ToolTipText = "Descrizione partecipante",;
    HelpContextID = 5898806,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=312, Left=324, Top=0, InputMask=replicate('X',101)

  add object oDFFLGPRE_2_13 as StdTrsCheck with uid="DXXIJUANLU",rtrep=.t.,;
    cFormVar="w_DFFLGPRE",  caption="",;
    HelpContextID = 621701,;
    Left=-2, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oDFFLGPRE_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DFFLGPRE,&i_cF..t_DFFLGPRE),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDFFLGPRE_2_13.GetRadio()
    this.Parent.oContained.w_DFFLGPRE = this.RadioValue()
    return .t.
  endfunc

  func oDFFLGPRE_2_13.ToRadio()
    this.Parent.oContained.w_DFFLGPRE=trim(this.Parent.oContained.w_DFFLGPRE)
    return(;
      iif(this.Parent.oContained.w_DFFLGPRE=='S',1,;
      0))
  endfunc

  func oDFFLGPRE_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mdp','DEF_PART','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DFCODICE=DEF_PART.DFCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
