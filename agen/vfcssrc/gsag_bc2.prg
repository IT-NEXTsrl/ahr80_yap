* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bc2                                                        *
*              Aggiorna elementi contratto                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-09                                                      *
* Last revis.: 2011-10-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bc2",oParentObject)
return(i_retval)

define class tgsag_bc2 as StdBatch
  * --- Local variables
  w_ELCONTRA = space(10)
  w_ELCODMOD = space(10)
  w_ELCODIMP = space(10)
  w_ELCODCOM = 0
  w_ELRINNOV = 0
  * --- WorkFile variables
  ELE_CONT_idx=0
  MOD_ELEM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna gli elementi contratto al salvataggio del contratto
    * --- Batch sotto transazione eliminata domanda aggiornamento elementi collegati e spostata prima dell'inizio della transazione
    if this.oParentObject.w_AGGIORNAELEMENTI
      * --- Select from ELE_CONT
      i_nConn=i_TableProp[this.ELE_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2],.t.,this.ELE_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ELE_CONT ";
            +" where ELCONTRA = "+cp_ToStrODBC(this.oParentObject.w_COSERIAL)+"";
             ,"_Curs_ELE_CONT")
      else
        select * from (i_cTable);
         where ELCONTRA = this.oParentObject.w_COSERIAL;
          into cursor _Curs_ELE_CONT
      endif
      if used('_Curs_ELE_CONT')
        select _Curs_ELE_CONT
        locate for 1=1
        do while not(eof())
        this.w_ELCONTRA = _Curs_ELE_CONT.ELCONTRA
        this.w_ELCODMOD = _Curs_ELE_CONT.ELCODMOD
        this.w_ELCODIMP = _Curs_ELE_CONT.ELCODIMP
        this.w_ELCODCOM = _Curs_ELE_CONT.ELCODCOM
        this.w_ELRINNOV = _Curs_ELE_CONT.ELRINNOV
        * --- Aggiorna l'elemento
        GSAG_BA6(this,this.w_ELCONTRA, this.w_ELCODMOD, this.w_ELCODIMP, this.w_ELCODCOM, this.w_ELRINNOV)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_ELE_CONT
          continue
        enddo
        use
      endif
    endif
    if this.oParentObject.w_OKSTAMPA AND AH_YESNO("Si desidera stampare l'elenco degli impianti con intestatario incongruente?")
      vq_exec("..\AGEN\EXE\QUERY\GSAG_BC1.VQR",this,"__tmp__")
      CP_CHPRN("..\AGEN\EXE\QUERY\GSAG_BC1.FRX", " ", this)
    endif
    USE IN SELECT("__tmp__")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ELE_CONT'
    this.cWorkTables[2]='MOD_ELEM'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_ELE_CONT')
      use in _Curs_ELE_CONT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
