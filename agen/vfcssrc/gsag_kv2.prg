* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kv2                                                        *
*              Selezione elementi contratto                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-17                                                      *
* Last revis.: 2010-04-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kv2",oParentObject))

* --- Class definition
define class tgsag_kv2 as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 788
  Height = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-04-27"
  HelpContextID=48899945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  DES_DIVE_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  CON_TRAS_IDX = 0
  cPrg = "gsag_kv2"
  cComment = "Selezione elementi contratto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PARENTFORM = space(8)
  w_TIPCON = space(1)
  w_CODNOM = space(15)
  w_CODSED = space(5)
  o_CODSED = space(5)
  w_CONTRATTO = space(10)
  w_MODELLO = space(10)
  w_SELEZI = space(1)
  w_MODESCRI = space(50)
  w_ANDESCRI = space(50)
  w_CODESCON_ZOOM = space(50)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kv2Pag1","gsag_kv2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_12
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DES_DIVE'
    this.cWorkTables[3]='IMP_MAST'
    this.cWorkTables[4]='IMP_DETT'
    this.cWorkTables[5]='CON_TRAS'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARENTFORM=space(8)
      .w_TIPCON=space(1)
      .w_CODNOM=space(15)
      .w_CODSED=space(5)
      .w_CONTRATTO=space(10)
      .w_MODELLO=space(10)
      .w_SELEZI=space(1)
      .w_MODESCRI=space(50)
      .w_ANDESCRI=space(50)
      .w_CODESCON_ZOOM=space(50)
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_TIPCON = THIS.OPARENTOBJECT.OPARENTOBJECT.w_IMTIPCON
        .w_CODNOM = THIS.OPARENTOBJECT.OPARENTOBJECT.w_IMCODCON
        .w_CODSED = THIS.OPARENTOBJECT.OPARENTOBJECT.w_IM__SEDE
          .DoRTCalc(5,6,.f.)
        .w_SELEZI = 'D'
        .w_MODESCRI = .w_ZOOM.GETVAR("MODESCRI")
        .w_ANDESCRI = .w_ZOOM.GETVAR("ANDESCRI")
        .w_CODESCON_ZOOM = .w_ZOOM.GETVAR("CODESCON")
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .DoRTCalc(1,7,.t.)
            .w_MODESCRI = .w_ZOOM.GETVAR("MODESCRI")
            .w_ANDESCRI = .w_ZOOM.GETVAR("ANDESCRI")
            .w_CODESCON_ZOOM = .w_ZOOM.GETVAR("CODESCON")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_11.visible=!this.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
    this.oPgFrm.Page1.oPag.oSELEZI_1_12.visible=!this.oPgFrm.Page1.oPag.oSELEZI_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_kv2
    if cEvent='Init'
       this.w_PARENTFORM = UPPER( this.oParentObject.cPrg )
       this.SetControlsValue()
       this.notifyevent("Search")
       
       * Evidenzia in verde i record che sono gi� stati selezionati dalla gestione corrente
       * e in rosso i record selezionati dall'altra gestione
       if used ( this.oParentObject.oParentObject.w_ADDEDELEMENTS )
         select( this.oParentObject.oParentObject.w_ADDEDELEMENTS )
         go top
         scan
           L_ELCONTRA = ELCONTRA
           L_ELCODMOD = ELCODMOD
           L_ELCODCOM = ELCODCOM
           L_PARENTFORM = PARENTFORM
           L_COLORE = IIF( L_PARENTFORM = this.w_PARENTFORM , rgb(0, 255, 0), rgb(255, 0, 0))
           update ( this.w_ZOOM.cCursor ) set xchk=1, ;
           COLORE = L_COLORE, ;
           ELCODCOM = L_ELCODCOM, ;
           PARENTFORM = L_PARENTFORM ;
           where ELCONTRA = L_ELCONTRA and ELCODMOD = L_ELCODMOD
         endscan
         go top
         this.w_ZOOM.REFRESH()
       endif
    endif
    
    if cEvent = "w_zoom row checked"
         * E' stata selezionata una riga
         
         * Legge dallo zoom i campi della riga selezionata
         this.w_CONTRATTO = this.w_ZOOM.GetVar("ELCONTRA")
         this.w_MODELLO = this.w_ZOOM.GetVar("ELCODMOD")
         
         * Se possibile memorizza nel cursore w_Zoom.cCursor la gestione selezionante e colora la riga
         SELECT( this.w_Zoom.cCursor )
         REPLACE COLORE WITH rgb(0, 255, 0), PARENTFORM WITH this.w_PARENTFORM
         if this.w_PARENTFORM = "GSAG_MEC"
           replace ELCODCOM with 0
         else
           replace ELCODCOM with this.oParentObject.oParentObject.w_CPROWNUM
         endif
         this.w_ZOOM.REFRESH()
    endif
    
    if cEvent = "w_zoom row unchecked"
        select (this.w_Zoom.cCursor)
        if PARENTFORM <> this.w_PARENTFORM
         this.w_ZOOM.REFRESH()
         if this.w_PARENTFORM = "GSAG_MDE"
           L_MESSAGGIO = "La riga corrente � stata selezionata dalla gestione collegata alla testata"
         else
           L_MESSAGGIO = "La riga corrente � stata selezionata dalla gestione collegata al dettaglio"
         endif
         ah_errormsg(L_MESSAGGIO,"!","")
        endif
        REPLACE COLORE WITH rgb(255, 255, 255), PARENTFORM WITH "XXXXXXXXXX", ELCODCOM with 0
    endif
    
    if cEvent = "Update end"
       * memorizza le variazioni nel cursore ADDEDELEMENTS
       SELECT ELCONTRA, ELCODMOD, ELCODCOM, PARENTFORM from ( this.w_Zoom.cCursor ) where xchk = 1 into cursor ( this.oParentObject.oParentObject.w_ADDEDELEMENTS )
       * Forza il salvataggio
       this.oParentObject.oParentObject.bHeaderUpdated = .t.
       this.oParentObject.oParentObject.bUpdated = .t.
       if this.w_PARENTFORM = "GSAG_MDE"
         this.oParentObject.oParentObject.SaveRow()
         ah_errormsg("Attenzione: al salvataggio gli elementi contratto selezionati saranno abbinati al componente dell'impianto")
       else
         ah_errormsg("Attenzione: al salvataggio gli elementi contratto selezionati saranno abbinati all'impianto")
       endif
    endif
    
    if cEvent = "w_SELEZI Changed"
       * Seleziona o deseleziona tutto
       if this.w_SELEZI = "S"
         this.notifyevent("SelezionaTutto")
       else
         this.notifyevent("DeselezionaTutto")
       endif
    endif
    
    if cEvent = "SelezionaTutto"
       * Seleziona tutto
       select (this.w_Zoom.cCursor)
       L_RECNO = RECNO()
       Go top
       Scan for PARENTFORM= "XXXXXXXXXX"
         REPLACE xchk with 1
         this.NotifyEvent("w_zoom row checked")
       Endscan
       Go L_RECNO
    endif
    
    if cEvent = "DeselezionaTutto"
       * Seleziona tutto
       select (this.w_Zoom.cCursor)
       L_RECNO = RECNO()
       Go top
       Scan for PARENTFORM = this.w_PARENTFORM
         REPLACE xchk with 0
         this.NotifyEvent("w_zoom row unchecked")
       Endscan
       Go L_RECNO
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_12.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMODESCRI_1_15.value==this.w_MODESCRI)
      this.oPgFrm.Page1.oPag.oMODESCRI_1_15.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_16.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_16.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_ZOOM_1_18.value==this.w_CODESCON_ZOOM)
      this.oPgFrm.Page1.oPag.oCODESCON_ZOOM_1_18.value=this.w_CODESCON_ZOOM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODSED = this.w_CODSED
    return

enddefine

* --- Define pages as container
define class tgsag_kv2Pag1 as StdContainer
  Width  = 784
  height = 430
  stdWidth  = 784
  stdheight = 430
  resizeXpos=516
  resizeYpos=175
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOM as cp_szoombox with uid="VZMOVSYNOQ",left=8, top=7, width=767,height=347,;
    caption='ZOOM',;
   bGlobalFont=.t.,;
    cTable="ELE_CONT",bRetriveAllRows=.f.,cZoomFile="gsag_kv2",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Search",;
    nPag=1;
    , HelpContextID = 43508330


  add object oBtn_1_8 as StdButton with uid="UUNSDLSMLD",left=675, top=379, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Conferma";
    , HelpContextID = 48871194;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="TKCMXZXILU",left=728, top=379, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 41582522;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="JBFSHNCMPX",left=20, top=366, width=23,height=23,;
    CpPicture="bmp\Check_small.bmp", caption="", nPag=1;
    , ToolTipText = "Seleziona tutti i nominativi";
    , HelpContextID = 48899850;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      this.parent.oContained.NotifyEvent("SelezionaTutto")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="GZBYUQIAUW",left=45, top=366, width=23,height=23,;
    CpPicture="bmp\unCheck_small.bmp", caption="", nPag=1;
    , ToolTipText = "Deseleziona tutti i nominativi";
    , HelpContextID = 48899850;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      this.parent.oContained.NotifyEvent("DeselezionaTutto")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!IsAlt())
     endwith
    endif
  endfunc

  add object oSELEZI_1_12 as StdRadio with uid="VNICEXLLTR",rtseq=7,rtrep=.f.,left=10, top=367, width=127,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona tutte le righe";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 67116250
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 67116250
      this.Buttons(2).Top=15
      this.SetAll("Width",125)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutte le righe")
      StdRadio::init()
    endproc

  func oSELEZI_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_12.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_12.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_1_12.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oMODESCRI_1_15 as StdField with uid="YUTQSCCACI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 93285647,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=302, Top=359, InputMask=replicate('X',50)

  add object oANDESCRI_1_16 as StdField with uid="ACNTZAUZOM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 93285199,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=302, Top=381, InputMask=replicate('X',50)

  add object oCODESCON_ZOOM_1_18 as StdField with uid="EWFOXIKFWQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODESCON_ZOOM", cQueryName = "CODESCON_ZOOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 88884124,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=302, Top=403, InputMask=replicate('X',50)

  add object oStr_1_13 as StdString with uid="JMNZWRVMBC",Visible=.t., Left=137, Top=359,;
    Alignment=1, Width=156, Height=18,;
    Caption="Descrizione modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="ZPSOZWYUGS",Visible=.t., Left=137, Top=381,;
    Alignment=1, Width=156, Height=18,;
    Caption="Ragione sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="EGSAJTEXEB",Visible=.t., Left=137, Top=403,;
    Alignment=1, Width=156, Height=18,;
    Caption="Descrizione contratto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kv2','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
