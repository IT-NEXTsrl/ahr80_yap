* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bd2                                                        *
*              Abbinamento elementi contratto                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-02                                                      *
* Last revis.: 2010-02-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAMETER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bd2",oParentObject,m.pPARAMETER)
return(i_retval)

define class tgsag_bd2 as StdBatch
  * --- Local variables
  pPARAMETER = space(10)
  w_FILTER = 0
  w_ELCONTRA = space(10)
  w_ELCODMOD = space(10)
  * --- WorkFile variables
  ELE_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSAG_MIM al salvataggio, associa alla testata o alle righe gli elementi contratto precedentemente selezionati
    * --- pPARAMETER = 'MASTER' abbina gli elementi contratto alla tabella IMP_MAST
    * --- pPARAMETER = 'DETAIL' abbina gli elementi contratto alla tabella IMP_DETT
    * --- w_ADDEDELEMENTS � il cursore che contiene gli elementi contratto selezionati tramite la funzione di abbinamento
    * --- w_IMCODICE � il codice dell'impianto
    * --- w_LRIGA � utilizzato nel legame tra IMP_MAST e ELCODCOM (ELCODCOM = 0)
    * --- w_CPROWNUM � utilizzato nel legame tra IMP_MAST e ELCODCOM (ELCODCOM = CPROWNUM)
    * --- Utilizzato per selezionare i record da abbinare alla testata o ad una riga
    if this.pPARAMETER = "MASTER"
      * --- Vengono elaborati i record da legare alla testata
      this.w_FILTER = this.oParentObject.w_LRIGA
    else
      * --- Vengono elaborati i record da legare alla riga di dettaglio
      this.w_FILTER = this.oParentObject.w_CPROWNUM
    endif
    * --- w_ELCONTRA e w_ELCODMOD sono le chiavi primarie della tabella ELE_CONT
    if USED( this.oParentObject.w_ADDEDELEMENTS )
      SELECT( this.oParentObject.w_ADDEDELEMENTS )
      GO TOP
      SCAN FOR ELCODCOM = this.w_FILTER
      this.w_ELCONTRA = ELCONTRA
      this.w_ELCODMOD = ELCODMOD
      * --- Write into ELE_CONT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ELE_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ELCODIMP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_IMCODICE),'ELE_CONT','ELCODIMP');
        +",ELCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_FILTER),'ELE_CONT','ELCODCOM');
            +i_ccchkf ;
        +" where ";
            +"ELCONTRA = "+cp_ToStrODBC(this.w_ELCONTRA);
            +" and ELCODMOD = "+cp_ToStrODBC(this.w_ELCODMOD);
            +" and ELCODIMP = "+cp_ToStrODBC(SPACE( 10 ));
               )
      else
        update (i_cTable) set;
            ELCODIMP = this.oParentObject.w_IMCODICE;
            ,ELCODCOM = this.w_FILTER;
            &i_ccchkf. ;
         where;
            ELCONTRA = this.w_ELCONTRA;
            and ELCODMOD = this.w_ELCODMOD;
            and ELCODIMP = SPACE( 10 );

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      ENDSCAN
    endif
  endproc


  proc Init(oParentObject,pPARAMETER)
    this.pPARAMETER=pPARAMETER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ELE_CONT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAMETER"
endproc
