* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bdm                                                        *
*              Elimino prestazioni                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-06-15                                                      *
* Last revis.: 2012-06-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ATSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bdm",oParentObject,m.w_ATSERIAL)
return(i_retval)

define class tgsag_bdm as StdBatch
  * --- Local variables
  w_ATSERIAL = space(20)
  * --- WorkFile variables
  PRE_STAZ_idx=0
  DET_PART_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimino orestazioni collegate ad attivit�
    * --- Select from gsag1bdm
    do vq_exec with 'gsag1bdm',this,'_Curs_gsag1bdm','',.f.,.t.
    if used('_Curs_gsag1bdm')
      select _Curs_gsag1bdm
      locate for 1=1
      do while not(eof())
      i_retcode = 'stop'
      i_retval = Ah_msgformat("Esistono documenti associati alle prestazioni")
      return
        select _Curs_gsag1bdm
        continue
      enddo
      use
    endif
    * --- Try
    local bErr_03835488
    bErr_03835488=bTrsErr
    this.Try_03835488()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      i_retcode = 'stop'
      i_retval = Ah_msgformat("Errore nella cancellazione delle prestazioni")
      return
    endif
    bTrsErr=bTrsErr or bErr_03835488
    * --- End
    i_retcode = 'stop'
    i_retval = " "
    return
  endproc
  proc Try_03835488()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from DET_PART
    i_nConn=i_TableProp[this.DET_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_PART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".DPSERIAL = "+i_cQueryTable+".DPSERIAL";
    
      do vq_exec with 'GSAG_BDM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PRE_STAZ
    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".PRSERIAL = "+i_cQueryTable+".PRSERIAL";
    
      do vq_exec with 'GSAG_BDM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_ATSERIAL)
    this.w_ATSERIAL=w_ATSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRE_STAZ'
    this.cWorkTables[2]='DET_PART'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_gsag1bdm')
      use in _Curs_gsag1bdm
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ATSERIAL"
endproc
