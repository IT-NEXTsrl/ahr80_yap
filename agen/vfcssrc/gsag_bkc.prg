* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bkc                                                        *
*              Ricostruzione saldi contratti a pacchetto                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-09-30                                                      *
* Last revis.: 2012-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bkc",oParentObject,m.pOPER)
return(i_retval)

define class tgsag_bkc as StdBatch
  * --- Local variables
  pOPER = space(5)
  w_ATDATINI = ctot("")
  w_ATDATFIN = ctot("")
  w_CONTRAGG = 0
  * --- WorkFile variables
  ELE_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pOPER=="OK"
        if ( !EMPTY(this.oParentObject.w_FLDATFIN) OR !EMPTY(this.oParentObject.w_FLDATINI) ) AND !ah_YesNo("Utilizzando il filtro sulle date alcune attivit� con abbinamenti a contratti a pacchetto potrebbero essere escluse dalla ricostruzione dei saldi.%0Continuare?")
          i_retcode = 'stop'
          return
        endif
        * --- Try
        local bErr_03824EA0
        bErr_03824EA0=bTrsErr
        this.Try_03824EA0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Errore durante l'aggiornamento degli elementi contratto%0%1", 48, , ALLTRIM( MESSAGE() ) )
        endif
        bTrsErr=bTrsErr or bErr_03824EA0
        * --- End
    endcase
  endproc
  proc Try_03824EA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Converto le date inserite in datetime per poter far filtro nella query.
    *     Alla data di fine aggiyunto i secondi necessari ad arrivare alle 23:59:59
    *     per potre includere tutte le qttivit� della giornata di fine filtro
    if !EMPTY(this.oParentObject.w_FLDATINI)
      this.w_ATDATINI = DTOT(this.oParentObject.w_FLDATINI)
    endif
    if !EMPTY(this.oParentObject.w_FLDATFIN)
      this.w_ATDATFIN = DTOT(this.oParentObject.w_FLDATFIN) + 86399
    endif
    * --- Azzero il saldo della quantit� consumata
    * --- Analizzo le attivit� collegate al contratto selezionato,
    *     sommo le quantit� consumate per aggiornare il saldo sull'elemento
    *     contratto
    * --- Write into ELE_CONT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ELE_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ELCONTRA,ELCODMOD,ELCODIMP,ELCODCOM,ELRINNOV"
      do vq_exec with 'gsag1bkc',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
              +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
              +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
              +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
              +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ELQTACON = _t2.DAQTAMOV";
          +i_ccchkf;
          +" from "+i_cTable+" ELE_CONT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
              +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
              +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
              +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
              +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT, "+i_cQueryTable+" _t2 set ";
          +"ELE_CONT.ELQTACON = _t2.DAQTAMOV";
          +Iif(Empty(i_ccchkf),"",",ELE_CONT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="ELE_CONT.ELCONTRA = t2.ELCONTRA";
              +" and "+"ELE_CONT.ELCODMOD = t2.ELCODMOD";
              +" and "+"ELE_CONT.ELCODIMP = t2.ELCODIMP";
              +" and "+"ELE_CONT.ELCODCOM = t2.ELCODCOM";
              +" and "+"ELE_CONT.ELRINNOV = t2.ELRINNOV";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT set (";
          +"ELQTACON";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.DAQTAMOV";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="ELE_CONT.ELCONTRA = _t2.ELCONTRA";
              +" and "+"ELE_CONT.ELCODMOD = _t2.ELCODMOD";
              +" and "+"ELE_CONT.ELCODIMP = _t2.ELCODIMP";
              +" and "+"ELE_CONT.ELCODCOM = _t2.ELCODCOM";
              +" and "+"ELE_CONT.ELRINNOV = _t2.ELRINNOV";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" ELE_CONT set ";
          +"ELQTACON = _t2.DAQTAMOV";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ELCONTRA = "+i_cQueryTable+".ELCONTRA";
              +" and "+i_cTable+".ELCODMOD = "+i_cQueryTable+".ELCODMOD";
              +" and "+i_cTable+".ELCODIMP = "+i_cQueryTable+".ELCODIMP";
              +" and "+i_cTable+".ELCODCOM = "+i_cQueryTable+".ELCODCOM";
              +" and "+i_cTable+".ELRINNOV = "+i_cQueryTable+".ELRINNOV";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ELQTACON = (select DAQTAMOV from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_CONTRAGG = i_Rows
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Operazione completata.%0Aggiornati %1 elementi contratto", 64, , ALLTRIM(STR(this.w_CONTRAGG)) )
    return


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ELE_CONT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
