* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bdr                                                        *
*              Duplica raggruppamenti attivita                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-12                                                      *
* Last revis.: 2008-05-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bdr",oParentObject,m.pAzione)
return(i_retval)

define class tgsag_bdr as StdBatch
  * --- Local variables
  pAzione = space(1)
  * --- WorkFile variables
  PRO_ITER_idx=0
  TMP_SERV_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                                                        Duplicazione Raggruppamento attivit�
    * --- Tabelle Gestite:
    *     PRO_ITER
    * --- pAzione = 'C'  : controllo di esistenza codice raggruppamento attivit�
    *                    = 'D'  : duplicazione raggruppamento attivit�
    do case
      case this.pAzione = "D"
        * --- DUPLICAZIONE
        * --- begin transaction
        cp_BeginTrs()
        * --- Try
        local bErr_03AD0158
        bErr_03AD0158=bTrsErr
        this.Try_03AD0158()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Duplicazione terminata con errori",,"")
        endif
        bTrsErr=bTrsErr or bErr_03AD0158
        * --- End
        * --- Drop temporary table TMP_SERV
        i_nIdx=cp_GetTableDefIdx('TMP_SERV')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_SERV')
        endif
      case this.pAzione = "C"
        * --- CONTROLLO ESISTENZA CODICE RAGGRUPPAMENTO ATTIVITA'
        if NOT EMPTY(this.oParentObject.w_CODRAGATT)
          * --- Select from PRO_ITER
          i_nConn=i_TableProp[this.PRO_ITER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRO_ITER_idx,2],.t.,this.PRO_ITER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select IPCODICE  from "+i_cTable+" PRO_ITER ";
                +" where IPCODICE="+cp_ToStrODBC(this.oParentObject.w_CODRAGATT)+"";
                +" group by IPCODICE";
                 ,"_Curs_PRO_ITER")
          else
            select IPCODICE from (i_cTable);
             where IPCODICE=this.oParentObject.w_CODRAGATT;
             group by IPCODICE;
              into cursor _Curs_PRO_ITER
          endif
          if used('_Curs_PRO_ITER')
            select _Curs_PRO_ITER
            locate for 1=1
            do while not(eof())
            ah_ErrorMsg("Attenzione: codice raggruppamento attivit� gi� esistente!",,"")
            this.oParentObject.w_CODRAGATT = space(10)
              select _Curs_PRO_ITER
              continue
            enddo
            use
          endif
        endif
    endcase
  endproc
  proc Try_03AD0158()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Create temporary table TMP_SERV
    i_nIdx=cp_AddTableDef('TMP_SERV') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PRO_ITER_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_ITER_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where IPCODICE="+cp_ToStrODBC(this.oParentObject.w_RAGATORIG)+"";
          )
    this.TMP_SERV_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMP_SERV
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_SERV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_SERV_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_SERV_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IPCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODRAGATT),'TMP_SERV','IPCODICE');
      +",IPDESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DESRAGATT),'TMP_SERV','IPDESCRI');
          +i_ccchkf ;
             )
    else
      update (i_cTable) set;
          IPCODICE = this.oParentObject.w_CODRAGATT;
          ,IPDESCRI = this.oParentObject.w_DESRAGATT;
          &i_ccchkf. ;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore scrittura TMP Raggruppamenti'
      return
    endif
    * --- Insert into PRO_ITER
    i_nConn=i_TableProp[this.PRO_ITER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_ITER_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_SERV_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.PRO_ITER_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento Raggruppamenti'
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Fine duplicazione",,"")
    return


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRO_ITER'
    this.cWorkTables[2]='*TMP_SERV'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PRO_ITER')
      use in _Curs_PRO_ITER
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
