* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_brp                                                        *
*              Ripristina progressivi                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_24]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-29                                                      *
* Last revis.: 2015-05-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pANNDOC,pNUMDOC,pALFDOC,pCAUATT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_brp",oParentObject,m.pANNDOC,m.pNUMDOC,m.pALFDOC,m.pCAUATT)
return(i_retval)

define class tgsag_brp as StdBatch
  * --- Local variables
  pANNDOC = space(4)
  pNUMDOC = 0
  pALFDOC = space(10)
  pCAUATT = space(20)
  w_ANNDOC = space(4)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_TESTNDOC = 0
  w_TESTPDOC = 0
  w_CHIAVE = space(100)
  w_OK_WARN = 0
  w_CAUATT = space(20)
  w_ULTIMINUMERI = space(10)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per recupero Buchi numerazione Prograssivo nelle attivit� da GSVE_BMK
    *     Solo nel caso cancello il documento che contiene l'ultimo progressivo Utile
    this.w_ANNDOC = this.pANNDOC
    this.w_NUMDOC = this.pNUMDOC
    this.w_ALFDOC = this.pALFDOC
    this.w_CAUATT = this.pCAUATT
    this.w_TESTNDOC = this.w_NUMDOC
    i_Conn=i_TableProp[this.OFF_ATTI_IDX, 3]
    cp_AskTableProg(this, i_Conn, "NUATT", "i_codazi,w_ANNDOC,w_ALFDOC,w_NUMDOC")
    * --- Dopo aver ricalcolato il Numero documento, se il numero documento che sto cancellando �
    *     l'ultimo progressivo calcolato, riporto indietro anche il progressivo
    if this.w_TESTNDOC = this.w_NUMDOC - 1
      this.w_ULTIMINUMERI = SYS( 2015 )
      * --- Si estraggono i massimi numeri documento presenti in archivio
      VQ_EXEC( "QUERY\GSAG_BRP" , THIS , this.w_ULTIMINUMERI )
      if RECCOUNT( this.w_ULTIMINUMERI ) > 0
        SELECT( this.w_ULTIMINUMERI )
        GO TOP
        this.w_TESTNDOC = NVL( NUMDOC , 0 )
        * --- w_TESTNDOC � il massimo tra i numeri documento presenti in archivio
        USE
      else
        this.w_TESTNDOC = 0
      endif
      * --- Aggiorno il progressivo all'ultimo documento inserito
      this.w_CHIAVE = "prog\NUATT\"+cp_ToStrODBC(alltrim(i_CODAZI))+"\"+cp_ToStrODBC(this.w_ANNDOC)+"\"+cp_ToStrODBC(this.w_ALFDOC)
      this.w_OK_WARN = sqlexec(i_Conn,"UPDATE CPWARN SET AUTONUM="+cp_ToStrODBC(this.w_TESTNDOC)+" WHERE TABLECODE="+cp_ToStrODBC(this.w_CHIAVE))
    endif
  endproc


  proc Init(oParentObject,pANNDOC,pNUMDOC,pALFDOC,pCAUATT)
    this.pANNDOC=pANNDOC
    this.pNUMDOC=pNUMDOC
    this.pALFDOC=pALFDOC
    this.pCAUATT=pCAUATT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_ATTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pANNDOC,pNUMDOC,pALFDOC,pCAUATT"
endproc
