* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bem                                                        *
*              Evade attivita                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-15                                                      *
* Last revis.: 2010-09-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCURSORE,pAUTOCLOSE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bem",oParentObject,m.pCURSORE,m.pAUTOCLOSE)
return(i_retval)

define class tgsag_bem as StdBatch
  * --- Local variables
  pCURSORE = space(10)
  pAUTOCLOSE = .f.
  w_KEYATT = space(10)
  w_RETVAL = space(254)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Evade le attivit�
    this.w_RETVAL = ""
    if USED(this.pCURSORE)
      * --- Try
      local bErr_03A36DC8
      bErr_03A36DC8=bTrsErr
      this.Try_03A36DC8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_RETVAL = "Attenzione: impossibile evadere l'attivit�."
      endif
      bTrsErr=bTrsErr or bErr_03A36DC8
      * --- End
    endif
    if this.pAUTOCLOSE
      USE IN SELECT (this.pCURSORE)
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc
  proc Try_03A36DC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT (this.pCURSORE)
    GO TOP
    SCAN
    this.w_KEYATT = ATSERIAL
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATSTATUS ="+cp_NullLink(cp_ToStrODBC("F"),'OFF_ATTI','ATSTATUS');
      +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_ATTI','UTCV');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'OFF_ATTI','UTDV');
          +i_ccchkf ;
      +" where ";
          +"ATSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
             )
    else
      update (i_cTable) set;
          ATSTATUS = "F";
          ,ATDATOUT = DATETIME();
          ,UTCV = i_CODUTE;
          ,UTDV = SetInfoDate( g_CALUTD );
          &i_ccchkf. ;
       where;
          ATSERIAL = this.w_KEYATT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    SELECT (this.pCURSORE)
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pCURSORE,pAUTOCLOSE)
    this.pCURSORE=pCURSORE
    this.pAUTOCLOSE=pAUTOCLOSE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_ATTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCURSORE,pAUTOCLOSE"
endproc
