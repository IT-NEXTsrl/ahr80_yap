* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_ade                                                        *
*              Driver eventi                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-19                                                      *
* Last revis.: 2010-04-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsfa_ade"))

* --- Class definition
define class tgsfa_ade as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 471
  Height = 108+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-04-19"
  HelpContextID=92963433
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  DRVEVENT_IDX = 0
  cFile = "DRVEVENT"
  cKeySelect = "DEDRIVER"
  cKeyWhere  = "DEDRIVER=this.w_DEDRIVER"
  cKeyWhereODBC = '"DEDRIVER="+cp_ToStrODBC(this.w_DEDRIVER)';

  cKeyWhereODBCqualified = '"DRVEVENT.DEDRIVER="+cp_ToStrODBC(this.w_DEDRIVER)';

  cPrg = "gsfa_ade"
  cComment = "Driver eventi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DEDRIVER = space(10)
  w_DEDESCRI = space(50)
  w_DE__TIPO = space(1)
  w_DEROUTIN = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DRVEVENT','gsfa_ade')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsfa_adePag1","gsfa_ade",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Driver evento")
      .Pages(1).HelpContextID = 179163387
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDEDRIVER_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DRVEVENT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DRVEVENT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DRVEVENT_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DEDRIVER = NVL(DEDRIVER,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DRVEVENT where DEDRIVER=KeySet.DEDRIVER
    *
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DRVEVENT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DRVEVENT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DRVEVENT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DEDRIVER',this.w_DEDRIVER  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DEDRIVER = NVL(DEDRIVER,space(10))
        .w_DEDESCRI = NVL(DEDESCRI,space(50))
        .w_DE__TIPO = NVL(DE__TIPO,space(1))
        .w_DEROUTIN = NVL(DEROUTIN,space(20))
        cp_LoadRecExtFlds(this,'DRVEVENT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DEDRIVER = space(10)
      .w_DEDESCRI = space(50)
      .w_DE__TIPO = space(1)
      .w_DEROUTIN = space(20)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_DE__TIPO = 'A'
      endif
    endwith
    cp_BlankRecExtFlds(this,'DRVEVENT')
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDEDRIVER_1_1.enabled = i_bVal
      .Page1.oPag.oDEDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oDE__TIPO_1_5.enabled = i_bVal
      .Page1.oPag.oDEROUTIN_1_7.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDEDRIVER_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDEDRIVER_1_1.enabled = .t.
        .Page1.oPag.oDEDESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DRVEVENT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEDRIVER,"DEDRIVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEDESCRI,"DEDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DE__TIPO,"DE__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEROUTIN,"DEROUTIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    i_lTable = "DRVEVENT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DRVEVENT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DRVEVENT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DRVEVENT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DRVEVENT')
        i_extval=cp_InsertValODBCExtFlds(this,'DRVEVENT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DEDRIVER,DEDESCRI,DE__TIPO,DEROUTIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DEDRIVER)+;
                  ","+cp_ToStrODBC(this.w_DEDESCRI)+;
                  ","+cp_ToStrODBC(this.w_DE__TIPO)+;
                  ","+cp_ToStrODBC(this.w_DEROUTIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DRVEVENT')
        i_extval=cp_InsertValVFPExtFlds(this,'DRVEVENT')
        cp_CheckDeletedKey(i_cTable,0,'DEDRIVER',this.w_DEDRIVER)
        INSERT INTO (i_cTable);
              (DEDRIVER,DEDESCRI,DE__TIPO,DEROUTIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DEDRIVER;
                  ,this.w_DEDESCRI;
                  ,this.w_DE__TIPO;
                  ,this.w_DEROUTIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DRVEVENT_IDX,i_nConn)
      *
      * update DRVEVENT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DRVEVENT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DEDESCRI="+cp_ToStrODBC(this.w_DEDESCRI)+;
             ",DE__TIPO="+cp_ToStrODBC(this.w_DE__TIPO)+;
             ",DEROUTIN="+cp_ToStrODBC(this.w_DEROUTIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DRVEVENT')
        i_cWhere = cp_PKFox(i_cTable  ,'DEDRIVER',this.w_DEDRIVER  )
        UPDATE (i_cTable) SET;
              DEDESCRI=this.w_DEDESCRI;
             ,DE__TIPO=this.w_DE__TIPO;
             ,DEROUTIN=this.w_DEROUTIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DRVEVENT_IDX,i_nConn)
      *
      * delete DRVEVENT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DEDRIVER',this.w_DEDRIVER  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DRVEVENT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DRVEVENT_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDEDRIVER_1_1.value==this.w_DEDRIVER)
      this.oPgFrm.Page1.oPag.oDEDRIVER_1_1.value=this.w_DEDRIVER
    endif
    if not(this.oPgFrm.Page1.oPag.oDEDESCRI_1_3.value==this.w_DEDESCRI)
      this.oPgFrm.Page1.oPag.oDEDESCRI_1_3.value=this.w_DEDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDE__TIPO_1_5.RadioValue()==this.w_DE__TIPO)
      this.oPgFrm.Page1.oPag.oDE__TIPO_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEROUTIN_1_7.value==this.w_DEROUTIN)
      this.oPgFrm.Page1.oPag.oDEROUTIN_1_7.value=this.w_DEROUTIN
    endif
    cp_SetControlsValueExtFlds(this,'DRVEVENT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DEDRIVER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDEDRIVER_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DEDRIVER)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsfa_adePag1 as StdContainer
  Width  = 467
  height = 108
  stdWidth  = 467
  stdheight = 108
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDEDRIVER_1_1 as StdField with uid="BVNZOSLJOW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DEDRIVER", cQueryName = "DEDRIVER",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice driver",;
    HelpContextID = 89917320,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=100, Top=11, InputMask=replicate('X',10)

  add object oDEDESCRI_1_3 as StdField with uid="LHRXFKVFSZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DEDESCRI", cQueryName = "DEDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione driver",;
    HelpContextID = 49219455,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=100, Top=36, InputMask=replicate('X',50)


  add object oDE__TIPO_1_5 as StdCombo with uid="LZSLRCBODF",rtseq=3,rtrep=.f.,left=100,top=61,width=153,height=21;
    , ToolTipText = "Tipo driver";
    , HelpContextID = 152745861;
    , cFormVar="w_DE__TIPO",RowSource=""+"Mail,"+"Telefonata,"+"Fax,"+"Documento,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDE__TIPO_1_5.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    iif(this.value =3,'F',;
    iif(this.value =4,'D',;
    iif(this.value =5,'A',;
    space(1)))))))
  endfunc
  func oDE__TIPO_1_5.GetRadio()
    this.Parent.oContained.w_DE__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oDE__TIPO_1_5.SetRadio()
    this.Parent.oContained.w_DE__TIPO=trim(this.Parent.oContained.w_DE__TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_DE__TIPO=='M',1,;
      iif(this.Parent.oContained.w_DE__TIPO=='T',2,;
      iif(this.Parent.oContained.w_DE__TIPO=='F',3,;
      iif(this.Parent.oContained.w_DE__TIPO=='D',4,;
      iif(this.Parent.oContained.w_DE__TIPO=='A',5,;
      0)))))
  endfunc

  add object oDEROUTIN_1_7 as StdField with uid="OEWTSVRRYL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DEROUTIN", cQueryName = "DEROUTIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome routine gestione driver evento",;
    HelpContextID = 199628924,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=100, Top=85, InputMask=replicate('X',20)

  add object oStr_1_2 as StdString with uid="CBHMWRNZOQ",Visible=.t., Left=21, Top=13,;
    Alignment=1, Width=75, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="ARRLGUSLAE",Visible=.t., Left=19, Top=38,;
    Alignment=1, Width=77, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="KVFUWGZEPP",Visible=.t., Left=36, Top=63,;
    Alignment=1, Width=60, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="PREGFDABVD",Visible=.t., Left=9, Top=87,;
    Alignment=1, Width=87, Height=18,;
    Caption="Nome routine:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsfa_ade','DRVEVENT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DEDRIVER=DRVEVENT.DEDRIVER";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
