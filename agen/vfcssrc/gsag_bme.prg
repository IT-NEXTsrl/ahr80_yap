* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bme                                                        *
*              Gestione modello elemento                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-11-05                                                      *
* Last revis.: 2010-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bme",oParentObject,m.pOPER)
return(i_retval)

define class tgsag_bme as StdBatch
  * --- Local variables
  pOPER = space(5)
  w_RETVALUE = .f.
  * --- WorkFile variables
  ELE_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_RETVALUE = .T.
    do case
      case this.pOPER=="CHKEL"
        * --- Read from ELE_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ELE_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2],.t.,this.ELE_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" ELE_CONT where ";
                +"ELCODMOD = "+cp_ToStrODBC(this.oParentObject.w_MOCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                ELCODMOD = this.oParentObject.w_MOCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_RETVALUE = i_Rows=0
    endcase
    i_retcode = 'stop'
    i_retval = this.w_RETVALUE
    return
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ELE_CONT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
