* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bam                                                        *
*              Elaborazioni stampa attivit� multistudio                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-02-04                                                      *
* Last revis.: 2013-09-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bam",oParentObject,m.pOpe)
return(i_retval)

define class tgsag_bam as StdBatch
  * --- Local variables
  pOpe = space(15)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pOpe == "AGGLIST"
        this.oParentObject.w_COMPANYLIST = ""
        SELECT( this.oParentObject.w_AZI_ZOOM.cCursor )
        if RECCOUNT() > 0
          l_RecNo = Recno()
          SCAN FOR XCHK=1
          this.oParentObject.w_COMPANYLIST = this.oParentObject.w_COMPANYLIST + iif(empty(this.oParentObject.w_COMPANYLIST), "", ",") + alltrim(AZCODAZI)
          SELECT(this.oParentObject.w_AZI_ZOOM.cCursor)
          ENDSCAN
          SELECT(this.oParentObject.w_AZI_ZOOM.cCursor)
          if NOT EMPTY(l_RecNo)
            goto (l_RecNo)
          endif
          * --- Riesegue lo zoom dei partecipanti tramite la notifica dell'evento 'Esegui'
          this.oparentobject.NotifyEvent("Esegui")
        endif
      case this.pOpe == "SELRIGHEAZ"
        * --- Seleziona tutte le righe dello zoom delle aziende
        UPDATE (this.oParentObject.w_AZI_ZOOM.cCursor) SET XCHK = 1
        this.bUpdateParentObject=.F.
    endcase
  endproc


  proc Init(oParentObject,pOpe)
    this.pOpe=pOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpe"
endproc
