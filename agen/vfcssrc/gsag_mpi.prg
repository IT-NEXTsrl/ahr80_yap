* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mpi                                                        *
*              Dettaglio importazione                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-06-10                                                      *
* Last revis.: 2012-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsag_mpi")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsag_mpi")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsag_mpi")
  return

* --- Class definition
define class tgsag_mpi as StdPCForm
  Width  = 648
  Height = 321
  Top    = 10
  Left   = 9
  cComment = "Dettaglio importazione"
  cPrg = "gsag_mpi"
  HelpContextID=120969367
  add object cnt as tcgsag_mpi
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsag_mpi as PCContext
  w_PICODPRO = space(10)
  w_PICODUID = space(150)
  w_PIDATFIN = space(14)
  w_PIDATINI = space(14)
  w_PINUMPRI = 0
  w_PIDATIMP = space(14)
  w_PI_STATO = space(1)
  w_PICATEGO = space(1)
  w_PIOGGETT = space(254)
  w_PIDESCRI = space(10)
  w_PILOCALI = space(30)
  proc Save(i_oFrom)
    this.w_PICODPRO = i_oFrom.w_PICODPRO
    this.w_PICODUID = i_oFrom.w_PICODUID
    this.w_PIDATFIN = i_oFrom.w_PIDATFIN
    this.w_PIDATINI = i_oFrom.w_PIDATINI
    this.w_PINUMPRI = i_oFrom.w_PINUMPRI
    this.w_PIDATIMP = i_oFrom.w_PIDATIMP
    this.w_PI_STATO = i_oFrom.w_PI_STATO
    this.w_PICATEGO = i_oFrom.w_PICATEGO
    this.w_PIOGGETT = i_oFrom.w_PIOGGETT
    this.w_PIDESCRI = i_oFrom.w_PIDESCRI
    this.w_PILOCALI = i_oFrom.w_PILOCALI
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PICODPRO = this.w_PICODPRO
    i_oTo.w_PICODUID = this.w_PICODUID
    i_oTo.w_PIDATFIN = this.w_PIDATFIN
    i_oTo.w_PIDATINI = this.w_PIDATINI
    i_oTo.w_PINUMPRI = this.w_PINUMPRI
    i_oTo.w_PIDATIMP = this.w_PIDATIMP
    i_oTo.w_PI_STATO = this.w_PI_STATO
    i_oTo.w_PICATEGO = this.w_PICATEGO
    i_oTo.w_PIOGGETT = this.w_PIOGGETT
    i_oTo.w_PIDESCRI = this.w_PIDESCRI
    i_oTo.w_PILOCALI = this.w_PILOCALI
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsag_mpi as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 648
  Height = 321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-03-15"
  HelpContextID=120969367
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DET_IMPO_IDX = 0
  cFile = "DET_IMPO"
  cKeySelect = "PICODPRO"
  cKeyWhere  = "PICODPRO=this.w_PICODPRO"
  cKeyDetail  = "PICODPRO=this.w_PICODPRO and PICODUID=this.w_PICODUID"
  cKeyWhereODBC = '"PICODPRO="+cp_ToStrODBC(this.w_PICODPRO)';

  cKeyDetailWhereODBC = '"PICODPRO="+cp_ToStrODBC(this.w_PICODPRO)';
      +'+" and PICODUID="+cp_ToStrODBC(this.w_PICODUID)';

  cKeyWhereODBCqualified = '"DET_IMPO.PICODPRO="+cp_ToStrODBC(this.w_PICODPRO)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsag_mpi"
  cComment = "Dettaglio importazione"
  i_nRowNum = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PICODPRO = space(10)
  w_PICODUID = space(150)
  w_PIDATFIN = ctot('')
  w_PIDATINI = ctot('')
  w_PINUMPRI = 0
  w_PIDATIMP = ctot('')
  w_PI_STATO = space(1)
  w_PICATEGO = space(1)
  w_PIOGGETT = space(254)
  w_PIDESCRI = space(0)
  w_PILOCALI = space(30)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_mpiPag1","gsag_mpi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DET_IMPO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DET_IMPO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DET_IMPO_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsag_mpi'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DET_IMPO where PICODPRO=KeySet.PICODPRO
    *                            and PICODUID=KeySet.PICODUID
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DET_IMPO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_IMPO_IDX,2],this.bLoadRecFilter,this.DET_IMPO_IDX,"gsag_mpi")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DET_IMPO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DET_IMPO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DET_IMPO '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PICODPRO',this.w_PICODPRO  )
      select * from (i_cTable) DET_IMPO where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PICODPRO = NVL(PICODPRO,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DET_IMPO')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_PICODUID = NVL(PICODUID,space(150))
          .w_PIDATFIN = NVL(PIDATFIN,ctot(""))
          .w_PIDATINI = NVL(PIDATINI,ctot(""))
          .w_PINUMPRI = NVL(PINUMPRI,0)
          .w_PIDATIMP = NVL(PIDATIMP,ctot(""))
          .w_PI_STATO = NVL(PI_STATO,space(1))
          .w_PICATEGO = NVL(PICATEGO,space(1))
          .w_PIOGGETT = NVL(PIOGGETT,space(254))
          .w_PIDESCRI = NVL(PIDESCRI,space(0))
          .w_PILOCALI = NVL(PILOCALI,space(30))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace PICODUID with .w_PICODUID
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_PICODPRO=space(10)
      .w_PICODUID=space(150)
      .w_PIDATFIN=ctot("")
      .w_PIDATINI=ctot("")
      .w_PINUMPRI=0
      .w_PIDATIMP=ctot("")
      .w_PI_STATO=space(1)
      .w_PICATEGO=space(1)
      .w_PIOGGETT=space(254)
      .w_PIDESCRI=space(0)
      .w_PILOCALI=space(30)
      if .cFunction<>"Filter"
        .DoRTCalc(1,6,.f.)
        .w_PI_STATO = 'N'
        .w_PICATEGO = ' '
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DET_IMPO')
    this.DoRTCalc(9,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPIOGGETT_2_8.enabled = i_bVal
      .Page1.oPag.oPIDESCRI_2_9.enabled = i_bVal
      .Page1.oPag.oPILOCALI_2_10.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DET_IMPO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsag_mpi
    * Abilito il campo note sul dettaglio per scorrere eventuali log lunghi
    Local L_Ctrl
    L_Ctrl=This.GetCtrl('w_PIDESCRI')
    L_Ctrl.Enabled=.t.
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DET_IMPO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PICODPRO,"PICODPRO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PICODUID C(150);
      ,t_PIDATFIN T(14);
      ,t_PIDATINI T(14);
      ,t_PINUMPRI N(6);
      ,t_PIDATIMP T(14);
      ,t_PI_STATO N(3);
      ,t_PICATEGO N(3);
      ,t_PIOGGETT C(254);
      ,t_PIDESCRI M(10);
      ,t_PILOCALI C(30);
      ,PICODUID C(150);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mpibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPICODUID_2_1.controlsource=this.cTrsName+'.t_PICODUID'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATFIN_2_2.controlsource=this.cTrsName+'.t_PIDATFIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATINI_2_3.controlsource=this.cTrsName+'.t_PIDATINI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPINUMPRI_2_4.controlsource=this.cTrsName+'.t_PINUMPRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATIMP_2_5.controlsource=this.cTrsName+'.t_PIDATIMP'
    this.oPgFRm.Page1.oPag.oPI_STATO_2_6.controlsource=this.cTrsName+'.t_PI_STATO'
    this.oPgFRm.Page1.oPag.oPICATEGO_2_7.controlsource=this.cTrsName+'.t_PICATEGO'
    this.oPgFRm.Page1.oPag.oPIOGGETT_2_8.controlsource=this.cTrsName+'.t_PIOGGETT'
    this.oPgFRm.Page1.oPag.oPIDESCRI_2_9.controlsource=this.cTrsName+'.t_PIDESCRI'
    this.oPgFRm.Page1.oPag.oPILOCALI_2_10.controlsource=this.cTrsName+'.t_PILOCALI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(228)
    this.AddVLine(341)
    this.AddVLine(453)
    this.AddVLine(509)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPICODUID_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DET_IMPO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_IMPO_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DET_IMPO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_IMPO_IDX,2])
      *
      * insert into DET_IMPO
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DET_IMPO')
        i_extval=cp_InsertValODBCExtFlds(this,'DET_IMPO')
        i_cFldBody=" "+;
                  "(PICODPRO,PICODUID,PIDATFIN,PIDATINI,PINUMPRI"+;
                  ",PIDATIMP,PI_STATO,PICATEGO,PIOGGETT,PIDESCRI"+;
                  ",PILOCALI,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PICODPRO)+","+cp_ToStrODBC(this.w_PICODUID)+","+cp_ToStrODBC(this.w_PIDATFIN)+","+cp_ToStrODBC(this.w_PIDATINI)+","+cp_ToStrODBC(this.w_PINUMPRI)+;
             ","+cp_ToStrODBC(this.w_PIDATIMP)+","+cp_ToStrODBC(this.w_PI_STATO)+","+cp_ToStrODBC(this.w_PICATEGO)+","+cp_ToStrODBC(this.w_PIOGGETT)+","+cp_ToStrODBC(this.w_PIDESCRI)+;
             ","+cp_ToStrODBC(this.w_PILOCALI)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DET_IMPO')
        i_extval=cp_InsertValVFPExtFlds(this,'DET_IMPO')
        cp_CheckDeletedKey(i_cTable,0,'PICODPRO',this.w_PICODPRO,'PICODUID',this.w_PICODUID)
        INSERT INTO (i_cTable) (;
                   PICODPRO;
                  ,PICODUID;
                  ,PIDATFIN;
                  ,PIDATINI;
                  ,PINUMPRI;
                  ,PIDATIMP;
                  ,PI_STATO;
                  ,PICATEGO;
                  ,PIOGGETT;
                  ,PIDESCRI;
                  ,PILOCALI;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PICODPRO;
                  ,this.w_PICODUID;
                  ,this.w_PIDATFIN;
                  ,this.w_PIDATINI;
                  ,this.w_PINUMPRI;
                  ,this.w_PIDATIMP;
                  ,this.w_PI_STATO;
                  ,this.w_PICATEGO;
                  ,this.w_PIOGGETT;
                  ,this.w_PIDESCRI;
                  ,this.w_PILOCALI;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DET_IMPO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_IMPO_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PICODUID))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DET_IMPO')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and PICODUID="+cp_ToStrODBC(&i_TN.->PICODUID)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DET_IMPO')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and PICODUID=&i_TN.->PICODUID;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_PICODUID))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and PICODUID="+cp_ToStrODBC(&i_TN.->PICODUID)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and PICODUID=&i_TN.->PICODUID;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace PICODUID with this.w_PICODUID
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DET_IMPO
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DET_IMPO')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PIDATFIN="+cp_ToStrODBC(this.w_PIDATFIN)+;
                     ",PIDATINI="+cp_ToStrODBC(this.w_PIDATINI)+;
                     ",PINUMPRI="+cp_ToStrODBC(this.w_PINUMPRI)+;
                     ",PIDATIMP="+cp_ToStrODBC(this.w_PIDATIMP)+;
                     ",PI_STATO="+cp_ToStrODBC(this.w_PI_STATO)+;
                     ",PICATEGO="+cp_ToStrODBC(this.w_PICATEGO)+;
                     ",PIOGGETT="+cp_ToStrODBC(this.w_PIOGGETT)+;
                     ",PIDESCRI="+cp_ToStrODBC(this.w_PIDESCRI)+;
                     ",PILOCALI="+cp_ToStrODBC(this.w_PILOCALI)+;
                     ",PICODUID="+cp_ToStrODBC(this.w_PICODUID)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and PICODUID="+cp_ToStrODBC(PICODUID)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DET_IMPO')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PIDATFIN=this.w_PIDATFIN;
                     ,PIDATINI=this.w_PIDATINI;
                     ,PINUMPRI=this.w_PINUMPRI;
                     ,PIDATIMP=this.w_PIDATIMP;
                     ,PI_STATO=this.w_PI_STATO;
                     ,PICATEGO=this.w_PICATEGO;
                     ,PIOGGETT=this.w_PIOGGETT;
                     ,PIDESCRI=this.w_PIDESCRI;
                     ,PILOCALI=this.w_PILOCALI;
                     ,PICODUID=this.w_PICODUID;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and PICODUID=&i_TN.->PICODUID;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DET_IMPO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DET_IMPO_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PICODUID))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DET_IMPO
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and PICODUID="+cp_ToStrODBC(&i_TN.->PICODUID)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and PICODUID=&i_TN.->PICODUID;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PICODUID))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DET_IMPO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DET_IMPO_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPI_STATO_2_6.RadioValue()==this.w_PI_STATO)
      this.oPgFrm.Page1.oPag.oPI_STATO_2_6.SetRadio()
      replace t_PI_STATO with this.oPgFrm.Page1.oPag.oPI_STATO_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPICATEGO_2_7.RadioValue()==this.w_PICATEGO)
      this.oPgFrm.Page1.oPag.oPICATEGO_2_7.SetRadio()
      replace t_PICATEGO with this.oPgFrm.Page1.oPag.oPICATEGO_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIOGGETT_2_8.value==this.w_PIOGGETT)
      this.oPgFrm.Page1.oPag.oPIOGGETT_2_8.value=this.w_PIOGGETT
      replace t_PIOGGETT with this.oPgFrm.Page1.oPag.oPIOGGETT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIDESCRI_2_9.value==this.w_PIDESCRI)
      this.oPgFrm.Page1.oPag.oPIDESCRI_2_9.value=this.w_PIDESCRI
      replace t_PIDESCRI with this.oPgFrm.Page1.oPag.oPIDESCRI_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPILOCALI_2_10.value==this.w_PILOCALI)
      this.oPgFrm.Page1.oPag.oPILOCALI_2_10.value=this.w_PILOCALI
      replace t_PILOCALI with this.oPgFrm.Page1.oPag.oPILOCALI_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPICODUID_2_1.value==this.w_PICODUID)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPICODUID_2_1.value=this.w_PICODUID
      replace t_PICODUID with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPICODUID_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATFIN_2_2.value==this.w_PIDATFIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATFIN_2_2.value=this.w_PIDATFIN
      replace t_PIDATFIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATFIN_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATINI_2_3.value==this.w_PIDATINI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATINI_2_3.value=this.w_PIDATINI
      replace t_PIDATINI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATINI_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPINUMPRI_2_4.value==this.w_PINUMPRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPINUMPRI_2_4.value=this.w_PINUMPRI
      replace t_PINUMPRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPINUMPRI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATIMP_2_5.value==this.w_PIDATIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATIMP_2_5.value=this.w_PIDATIMP
      replace t_PIDATIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIDATIMP_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'DET_IMPO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_PICODUID))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PICODUID)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PICODUID=space(150)
      .w_PIDATFIN=ctot("")
      .w_PIDATINI=ctot("")
      .w_PINUMPRI=0
      .w_PIDATIMP=ctot("")
      .w_PI_STATO=space(1)
      .w_PICATEGO=space(1)
      .w_PIOGGETT=space(254)
      .w_PIDESCRI=space(0)
      .w_PILOCALI=space(30)
      .DoRTCalc(1,6,.f.)
        .w_PI_STATO = 'N'
        .w_PICATEGO = ' '
    endwith
    this.DoRTCalc(9,11,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PICODUID = t_PICODUID
    this.w_PIDATFIN = t_PIDATFIN
    this.w_PIDATINI = t_PIDATINI
    this.w_PINUMPRI = t_PINUMPRI
    this.w_PIDATIMP = t_PIDATIMP
    this.w_PI_STATO = this.oPgFrm.Page1.oPag.oPI_STATO_2_6.RadioValue(.t.)
    this.w_PICATEGO = this.oPgFrm.Page1.oPag.oPICATEGO_2_7.RadioValue(.t.)
    this.w_PIOGGETT = t_PIOGGETT
    this.w_PIDESCRI = t_PIDESCRI
    this.w_PILOCALI = t_PILOCALI
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PICODUID with this.w_PICODUID
    replace t_PIDATFIN with this.w_PIDATFIN
    replace t_PIDATINI with this.w_PIDATINI
    replace t_PINUMPRI with this.w_PINUMPRI
    replace t_PIDATIMP with this.w_PIDATIMP
    replace t_PI_STATO with this.oPgFrm.Page1.oPag.oPI_STATO_2_6.ToRadio()
    replace t_PICATEGO with this.oPgFrm.Page1.oPag.oPICATEGO_2_7.ToRadio()
    replace t_PIOGGETT with this.w_PIOGGETT
    replace t_PIDESCRI with this.w_PIDESCRI
    replace t_PILOCALI with this.w_PILOCALI
    if i_srv='A'
      replace PICODUID with this.w_PICODUID
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mpiPag1 as StdContainer
  Width  = 644
  height = 321
  stdWidth  = 644
  stdheight = 321
  resizeXpos=199
  resizeYpos=99
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=2, width=629,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="PICODUID",Label1="Uid",Field2="PIDATFIN",Label2="Data iniziale",Field3="PIDATINI",Label3="Data finale",Field4="PINUMPRI",Label4="Priorit�",Field5="PIDATIMP",Label5="Data importazione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 99810694

  add object oStr_1_2 as StdString with uid="RCZGMNSMLD",Visible=.t., Left=17, Top=182,;
    Alignment=1, Width=70, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="OGVOTRAYAV",Visible=.t., Left=17, Top=215,;
    Alignment=1, Width=70, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="JXDSEQACRJ",Visible=.t., Left=16, Top=244,;
    Alignment=1, Width=70, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="UAESANBHBF",Visible=.t., Left=380, Top=215,;
    Alignment=1, Width=53, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="QZGEPQPXUD",Visible=.t., Left=292, Top=182,;
    Alignment=1, Width=70, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=20,;
    width=628+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=21,width=627+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPI_STATO_2_6.Refresh()
      this.Parent.oPICATEGO_2_7.Refresh()
      this.Parent.oPIOGGETT_2_8.Refresh()
      this.Parent.oPIDESCRI_2_9.Refresh()
      this.Parent.oPILOCALI_2_10.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oPI_STATO_2_6 as StdTrsCombo with uid="QBKZWXTJSE",rtrep=.t.,;
    cFormVar="w_PI_STATO", RowSource=""+"Cancellato,"+"Modificato,"+"Nuovo,"+"Escludi" , enabled=.f.,;
    HelpContextID = 36759739,;
    Height=25, Width=154, Left=91, Top=182,;
    cTotal="", cQueryName = "PI_STATO",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPI_STATO_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PI_STATO,&i_cF..t_PI_STATO),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'M',;
    iif(xVal =3,'N',;
    iif(xVal =4,'E',;
    space(1))))))
  endfunc
  func oPI_STATO_2_6.GetRadio()
    this.Parent.oContained.w_PI_STATO = this.RadioValue()
    return .t.
  endfunc

  func oPI_STATO_2_6.ToRadio()
    this.Parent.oContained.w_PI_STATO=trim(this.Parent.oContained.w_PI_STATO)
    return(;
      iif(this.Parent.oContained.w_PI_STATO=='C',1,;
      iif(this.Parent.oContained.w_PI_STATO=='M',2,;
      iif(this.Parent.oContained.w_PI_STATO=='N',3,;
      iif(this.Parent.oContained.w_PI_STATO=='E',4,;
      0)))))
  endfunc

  func oPI_STATO_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPICATEGO_2_7 as StdTrsCombo with uid="KGWZDVZLDS",rtrep=.t.,;
    cFormVar="w_PICATEGO", RowSource=""+"Generico,"+"Udienze,"+"Appuntamento,"+"Cose da fare,"+"Note,"+"Sessione telefonica,"+"Assenze,"+"Da inserimento prestazioni,"+"Nessuna" , enabled=.f.,;
    HelpContextID = 239380667,;
    Height=25, Width=147, Left=370, Top=183,;
    cTotal="", cQueryName = "PICATEGO",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPICATEGO_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PICATEGO,&i_cF..t_PICATEGO),this.value)
    return(iif(xVal =1,'G',;
    iif(xVal =2,'U',;
    iif(xVal =3,'S',;
    iif(xVal =4,'D',;
    iif(xVal =5,'M',;
    iif(xVal =6,'T',;
    iif(xVal =7,'A',;
    iif(xVal =8,'Z',;
    iif(xVal =9,' ',;
    space(1)))))))))))
  endfunc
  func oPICATEGO_2_7.GetRadio()
    this.Parent.oContained.w_PICATEGO = this.RadioValue()
    return .t.
  endfunc

  func oPICATEGO_2_7.ToRadio()
    this.Parent.oContained.w_PICATEGO=trim(this.Parent.oContained.w_PICATEGO)
    return(;
      iif(this.Parent.oContained.w_PICATEGO=='G',1,;
      iif(this.Parent.oContained.w_PICATEGO=='U',2,;
      iif(this.Parent.oContained.w_PICATEGO=='S',3,;
      iif(this.Parent.oContained.w_PICATEGO=='D',4,;
      iif(this.Parent.oContained.w_PICATEGO=='M',5,;
      iif(this.Parent.oContained.w_PICATEGO=='T',6,;
      iif(this.Parent.oContained.w_PICATEGO=='A',7,;
      iif(this.Parent.oContained.w_PICATEGO=='Z',8,;
      iif(this.Parent.oContained.w_PICATEGO=='',9,;
      0))))))))))
  endfunc

  func oPICATEGO_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPIOGGETT_2_8 as StdTrsField with uid="PQBEBSOIHN",rtseq=9,rtrep=.t.,;
    cFormVar="w_PIOGGETT",value=space(254),;
    HelpContextID = 252569782,;
    cTotal="", bFixedPos=.t., cQueryName = "PIOGGETT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=91, Top=213, InputMask=replicate('X',254)

  add object oPIDESCRI_2_9 as StdTrsMemo with uid="AIUSOHTFPD",rtseq=10,rtrep=.t.,;
    cFormVar="w_PIDESCRI",value=space(0),;
    HelpContextID = 263153471,;
    cTotal="", bFixedPos=.t., cQueryName = "PIDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=70, Width=547, Left=89, Top=246

  add object oPILOCALI_2_10 as StdTrsField with uid="UEAJBLNWXG",rtseq=11,rtrep=.t.,;
    cFormVar="w_PILOCALI",value=space(30),;
    HelpContextID = 213509951,;
    cTotal="", bFixedPos=.t., cQueryName = "PILOCALI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=197, Left=439, Top=213, InputMask=replicate('X',30)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsag_mpiBodyRow as CPBodyRowCnt
  Width=618
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPICODUID_2_1 as StdTrsField with uid="XYTWILRVJI",rtseq=2,rtrep=.t.,;
    cFormVar="w_PICODUID",value=space(150),isprimarykey=.t.,;
    HelpContextID = 255240390,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=222, Left=-2, Top=0, InputMask=replicate('X',150)

  add object oPIDATFIN_2_2 as StdTrsField with uid="LGPCTXROKP",rtseq=3,rtrep=.t.,;
    cFormVar="w_PIDATFIN",value=ctot(""),;
    HelpContextID = 222599356,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=223, Top=0

  add object oPIDATINI_2_3 as StdTrsField with uid="JXFFQBCXAF",rtseq=4,rtrep=.t.,;
    cFormVar="w_PIDATINI",value=ctot(""),;
    HelpContextID = 96167743,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=335, Top=0

  add object oPINUMPRI_2_4 as StdTrsField with uid="JYJMJULJAV",rtseq=5,rtrep=.t.,;
    cFormVar="w_PINUMPRI",value=0,;
    HelpContextID = 207619903,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=447, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oPIDATIMP_2_5 as StdTrsField with uid="LWBINMQNXY",rtseq=6,rtrep=.t.,;
    cFormVar="w_PIDATIMP",value=ctot(""),;
    HelpContextID = 96167750,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=502, Top=0
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPICODUID_2_1.When()
    return(.t.)
  proc oPICODUID_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPICODUID_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mpi','DET_IMPO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PICODPRO=DET_IMPO.PICODPRO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
