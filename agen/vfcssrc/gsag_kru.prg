* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kru                                                        *
*              Rubrica                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-20                                                      *
* Last revis.: 2014-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsag_kru
* Controllo presenza delle ocx
IF ProvaOcx('cp_ctContact')
   Ah_ErrorMsg("Errore nella gestione di ctContact.OCX")
   RETURN
ENDIF
* --- Fine Area Manuale
return(createobject("tgsag_kru",oParentObject))

* --- Class definition
define class tgsag_kru as StdForm
  Top    = -1
  Left   = 5

  * --- Standard Properties
  Width  = 804
  Height = 536+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-21"
  HelpContextID=116008809
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=78

  * --- Constant Properties
  _IDX = 0
  GRU_NOMI_IDX = 0
  ORI_NOMI_IDX = 0
  cpusers_IDX = 0
  DIPENDEN_IDX = 0
  CAT_ATTR_IDX = 0
  TIP_CATT_IDX = 0
  cPrg = "gsag_kru"
  cComment = "Rubrica"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODICE = space(20)
  w_OrderBy = space(1)
  w_ClnIndiri = 0
  w_ClnTelefono = 0
  w_ClnFax = 0
  w_ClnCellulare = 0
  w_ClnEmail = 0
  w_ClnEmPEC = 0
  w_ClnSito = 0
  w_DimScheda = 0
  w_ClnRagSoc = 0
  w_TipoPersona = space(1)
  w_INDIRIZZO = space(50)
  w_AN___CAP = space(8)
  w_ANLOCALI = space(30)
  w_ANPROVIN = space(2)
  w_NOTE = space(0)
  w_PDATINIZ = ctod('  /  /  ')
  w_obsodat1 = space(1)
  w_TIPONOME = space(1)
  w_TIPONOME = space(1)
  w_NOCODGRU = space(5)
  w_NOCODORI = space(5)
  w_NORIFINT = space(5)
  o_NORIFINT = space(5)
  w_CodUte = 0
  w_CONS_DATI = space(1)
  w_DESGRU = space(35)
  w_FILTDENOM = space(100)
  w_FILTRIFERI = space(100)
  w_PartiDa = space(100)
  o_PartiDa = space(100)
  w_FLDenom = space(1)
  w_FLRiferi = space(1)
  w_FLEml = space(1)
  w_FLCoEml = space(1)
  w_FLPEC = space(1)
  w_FLCoPEC = space(1)
  w_FLTele = space(1)
  w_FLCotele = space(1)
  w_RAGSOC = space(100)
  w_FILTEML = space(100)
  w_FILTPEC = space(100)
  w_FILTCOEML = space(100)
  w_FILTCOPEC = space(100)
  w_FILTTELE = space(100)
  w_FILTCONT = space(100)
  w_ClnLocali = 0
  w_TIPNOM = space(1)
  w_DESUTE = space(20)
  w_DESORI = space(35)
  w_DESRIFINT = space(35)
  w_COGNOME = space(10)
  w_NOME = space(10)
  w_DESCAT1 = space(35)
  w_DESATT1 = space(35)
  w_DESCAT2 = space(35)
  w_DESATT2 = space(35)
  w_DESCAT3 = space(35)
  w_DESATT3 = space(35)
  w_RACODAT1 = space(10)
  w_RACODAT2 = space(10)
  w_RACODAT3 = space(10)
  w_TIPCAT1 = space(5)
  w_FLIndirizzo = space(10)
  w_TIPCAT2 = space(5)
  w_FLLocali = space(10)
  w_TIPCAT3 = space(5)
  w_FLTelefono = space(10)
  w_FLFax = space(10)
  w_FLCellulare = space(10)
  w_FLEmail = space(10)
  w_ClnSocieta = 0
  w_ClnTelefo2 = 0
  w_FLSito = space(10)
  w_FLPersone = space(10)
  w_OB_TEST = ctod('  /  /  ')
  w_NumElem = 0
  w_CodCliente = space(20)
  w_FLEMPEC = space(10)
  w_ctRubrica = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kruPag1","gsag_kru",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Scheda")
      .Pages(2).addobject("oPag","tgsag_kruPag2","gsag_kru",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLDenom_1_51
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ctRubrica = this.oPgFrm.Pages(1).oPag.ctRubrica
    DoDefault()
    proc Destroy()
      this.w_ctRubrica = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='GRU_NOMI'
    this.cWorkTables[2]='ORI_NOMI'
    this.cWorkTables[3]='cpusers'
    this.cWorkTables[4]='DIPENDEN'
    this.cWorkTables[5]='CAT_ATTR'
    this.cWorkTables[6]='TIP_CATT'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODICE=space(20)
      .w_OrderBy=space(1)
      .w_ClnIndiri=0
      .w_ClnTelefono=0
      .w_ClnFax=0
      .w_ClnCellulare=0
      .w_ClnEmail=0
      .w_ClnEmPEC=0
      .w_ClnSito=0
      .w_DimScheda=0
      .w_ClnRagSoc=0
      .w_TipoPersona=space(1)
      .w_INDIRIZZO=space(50)
      .w_AN___CAP=space(8)
      .w_ANLOCALI=space(30)
      .w_ANPROVIN=space(2)
      .w_NOTE=space(0)
      .w_PDATINIZ=ctod("  /  /  ")
      .w_obsodat1=space(1)
      .w_TIPONOME=space(1)
      .w_TIPONOME=space(1)
      .w_NOCODGRU=space(5)
      .w_NOCODORI=space(5)
      .w_NORIFINT=space(5)
      .w_CodUte=0
      .w_CONS_DATI=space(1)
      .w_DESGRU=space(35)
      .w_FILTDENOM=space(100)
      .w_FILTRIFERI=space(100)
      .w_PartiDa=space(100)
      .w_FLDenom=space(1)
      .w_FLRiferi=space(1)
      .w_FLEml=space(1)
      .w_FLCoEml=space(1)
      .w_FLPEC=space(1)
      .w_FLCoPEC=space(1)
      .w_FLTele=space(1)
      .w_FLCotele=space(1)
      .w_RAGSOC=space(100)
      .w_FILTEML=space(100)
      .w_FILTPEC=space(100)
      .w_FILTCOEML=space(100)
      .w_FILTCOPEC=space(100)
      .w_FILTTELE=space(100)
      .w_FILTCONT=space(100)
      .w_ClnLocali=0
      .w_TIPNOM=space(1)
      .w_DESUTE=space(20)
      .w_DESORI=space(35)
      .w_DESRIFINT=space(35)
      .w_COGNOME=space(10)
      .w_NOME=space(10)
      .w_DESCAT1=space(35)
      .w_DESATT1=space(35)
      .w_DESCAT2=space(35)
      .w_DESATT2=space(35)
      .w_DESCAT3=space(35)
      .w_DESATT3=space(35)
      .w_RACODAT1=space(10)
      .w_RACODAT2=space(10)
      .w_RACODAT3=space(10)
      .w_TIPCAT1=space(5)
      .w_FLIndirizzo=space(10)
      .w_TIPCAT2=space(5)
      .w_FLLocali=space(10)
      .w_TIPCAT3=space(5)
      .w_FLTelefono=space(10)
      .w_FLFax=space(10)
      .w_FLCellulare=space(10)
      .w_FLEmail=space(10)
      .w_ClnSocieta=0
      .w_ClnTelefo2=0
      .w_FLSito=space(10)
      .w_FLPersone=space(10)
      .w_OB_TEST=ctod("  /  /  ")
      .w_NumElem=0
      .w_CodCliente=space(20)
      .w_FLEMPEC=space(10)
      .oPgFrm.Page1.oPag.ctRubrica.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_OrderBy = '2'
          .DoRTCalc(3,9,.f.)
        .w_DimScheda = 0
          .DoRTCalc(11,11,.f.)
        .w_TipoPersona = 'P'
          .DoRTCalc(13,17,.f.)
        .w_PDATINIZ = i_datsys
        .w_obsodat1 = 'N'
        .w_TIPONOME = 'X'
        .w_TIPONOME = 'X'
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_NOCODGRU))
          .link_2_11('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_NOCODORI))
          .link_2_12('Full')
        endif
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_NORIFINT))
          .link_2_13('Full')
        endif
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_CodUte))
          .link_2_14('Full')
        endif
        .w_CONS_DATI = ''
          .DoRTCalc(27,30,.f.)
        .w_FLDenom = 'N'
        .w_FLRiferi = 'N'
        .w_FLEml = 'N'
        .w_FLCoEml = 'N'
        .w_FLPEC = 'N'
        .w_FLCoPEC = 'N'
        .w_FLTele = 'N'
        .w_FLCotele = 'N'
          .DoRTCalc(39,46,.f.)
        .w_TIPNOM = IIF(.w_TIPONOME='X','',.w_TIPONOME)
          .DoRTCalc(48,49,.f.)
        .w_DESRIFINT = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .DoRTCalc(51,59,.f.)
        if not(empty(.w_RACODAT1))
          .link_2_40('Full')
        endif
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_RACODAT2))
          .link_2_41('Full')
        endif
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_RACODAT3))
          .link_2_42('Full')
        endif
        .DoRTCalc(62,62,.f.)
        if not(empty(.w_TIPCAT1))
          .link_2_43('Full')
        endif
        .w_FLIndirizzo = 'S'
        .DoRTCalc(64,64,.f.)
        if not(empty(.w_TIPCAT2))
          .link_2_45('Full')
        endif
        .w_FLLocali = 'S'
        .DoRTCalc(66,66,.f.)
        if not(empty(.w_TIPCAT3))
          .link_2_47('Full')
        endif
        .w_FLTelefono = 'S'
        .w_FLFax = 'S'
        .w_FLCellulare = 'S'
        .w_FLEmail = 'S'
          .DoRTCalc(71,72,.f.)
        .w_FLSito = 'S'
        .w_FLPersone = 'S'
        .w_OB_TEST = i_INIDAT
        .w_NumElem = .w_ctRubrica.HeaderCount()
          .DoRTCalc(77,77,.f.)
        .w_FLEMPEC = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_66.enabled = this.oPgFrm.Page1.oPag.oBtn_1_66.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_59.enabled = this.oPgFrm.Page2.oPag.oBtn_2_59.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ctRubrica.Calculate()
        if .o_PartiDa<>.w_PartiDa
          .Calculate_NBLZPBCIDM()
        endif
        .DoRTCalc(1,46,.t.)
            .w_TIPNOM = IIF(.w_TIPONOME='X','',.w_TIPONOME)
        .DoRTCalc(48,49,.t.)
        if .o_NORIFINT<>.w_NORIFINT
            .w_DESRIFINT = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        endif
        .DoRTCalc(51,61,.t.)
          .link_2_43('Full')
        .DoRTCalc(63,63,.t.)
          .link_2_45('Full')
        .DoRTCalc(65,65,.t.)
          .link_2_47('Full')
        .DoRTCalc(67,75,.t.)
            .w_NumElem = .w_ctRubrica.HeaderCount()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(77,78,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ctRubrica.Calculate()
    endwith
  return

  proc Calculate_ETQKPRWYHB()
    with this
          * --- Init check
          .w_FLDenom = 'S'
    endwith
  endproc
  proc Calculate_WXWXRTBVWT()
    with this
          * --- Doppio click - entra in modifica
          GSAG_BRU(this;
              ,"MODIFICA";
             )
    endwith
  endproc
  proc Calculate_QSQRLIGDTD()
    with this
          * --- Aggiorna
          GSAG_BRU(this;
              ,"RICERCA";
             )
    endwith
  endproc
  proc Calculate_NBLZPBCIDM()
    with this
          * --- A_partire_da
          GSAG_BRU(this;
              ,"PARTIDA";
             )
    endwith
  endproc
  proc Calculate_HALNTOGBAZ()
    with this
          * --- Ingrandisce la scheda
          GSAG_BRU(this;
              ,"INGRANDISCI";
             )
    endwith
  endproc
  proc Calculate_ZSDRQRGJJG()
    with this
          * --- Riduce la scheda
          GSAG_BRU(this;
              ,"RIDUCI";
             )
    endwith
  endproc
  proc Calculate_OGZRORHRJT()
    with this
          * --- Avvalora i filtri dopo le modifiche a ragsoc
          .w_FiltDenom = IIF(.w_FLDENOM='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTRIFERI = IIF(.w_FLRIFERI='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTEML = IIF(.w_FLEML='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTPEC = IIF(.w_FLPEC='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTCOEML = IIF(.w_FLCOEML='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTCOPEC = IIF(.w_FLCOPEC='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTTELE = IIF(.w_FLTELE='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTCONT = IIF(.w_FLCOTELE='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
    endwith
  endproc
  proc Calculate_QOIZEZZGPK()
    with this
          * --- Aggiorna dopo le modifiche a ragsoc
          GSAG_BRU(this;
              ,"RICERCA";
             )
    endwith
  endproc
  proc Calculate_AFCSWEUSAV()
    with this
          * --- Avvalora i filtri dopo il cambio di un check
          .w_FiltDenom = IIF(.w_FLDENOM='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTRIFERI = IIF(.w_FLRIFERI='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTEML = IIF(.w_FLEML='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTPEC = IIF(.w_FLPEC='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTCOEML = IIF(.w_FLCOEML='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTCOPEC = IIF(.w_FLCOPEC='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTTELE = IIF(.w_FLTELE='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
          .w_FILTCONT = IIF(.w_FLCOTELE='S' AND !EMPTY(.w_RAGSOC),.w_RAGSOC,'')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_20.visible=!this.oPgFrm.Page1.oPag.oBtn_1_20.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_23.visible=!this.oPgFrm.Page1.oPag.oBtn_1_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_24.visible=!this.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_25.visible=!this.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_27.visible=!this.oPgFrm.Page1.oPag.oBtn_1_27.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_28.visible=!this.oPgFrm.Page1.oPag.oBtn_1_28.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_30.visible=!this.oPgFrm.Page1.oPag.oBtn_1_30.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_31.visible=!this.oPgFrm.Page1.oPag.oBtn_1_31.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_32.visible=!this.oPgFrm.Page1.oPag.oBtn_1_32.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_33.visible=!this.oPgFrm.Page1.oPag.oBtn_1_33.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_34.visible=!this.oPgFrm.Page1.oPag.oBtn_1_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_35.visible=!this.oPgFrm.Page1.oPag.oBtn_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_37.visible=!this.oPgFrm.Page1.oPag.oBtn_1_37.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_38.visible=!this.oPgFrm.Page1.oPag.oBtn_1_38.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_39.visible=!this.oPgFrm.Page1.oPag.oBtn_1_39.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_40.visible=!this.oPgFrm.Page1.oPag.oBtn_1_40.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_41.visible=!this.oPgFrm.Page1.oPag.oBtn_1_41.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_42.visible=!this.oPgFrm.Page1.oPag.oBtn_1_42.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_43.visible=!this.oPgFrm.Page1.oPag.oBtn_1_43.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_44.visible=!this.oPgFrm.Page1.oPag.oBtn_1_44.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_45.visible=!this.oPgFrm.Page1.oPag.oBtn_1_45.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_46.visible=!this.oPgFrm.Page1.oPag.oBtn_1_46.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_47.visible=!this.oPgFrm.Page1.oPag.oBtn_1_47.mHide()
    this.oPgFrm.Page2.oPag.oTIPONOME_2_9.visible=!this.oPgFrm.Page2.oPag.oTIPONOME_2_9.mHide()
    this.oPgFrm.Page2.oPag.oTIPONOME_2_10.visible=!this.oPgFrm.Page2.oPag.oTIPONOME_2_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_ETQKPRWYHB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Modifica")
          .Calculate_WXWXRTBVWT()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ctRubrica.Event(cEvent)
        if lower(cEvent)==lower("Ricerca")
          .Calculate_QSQRLIGDTD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Ingrandisci")
          .Calculate_HALNTOGBAZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Riduci")
          .Calculate_ZSDRQRGJJG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_RAGSOC Searching")
          .Calculate_OGZRORHRJT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_RAGSOC Searching")
          .Calculate_QOIZEZZGPK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_FLCoEml Changed") or lower(cEvent)==lower("w_FLCoPEC Changed") or lower(cEvent)==lower("w_FLCotele Changed") or lower(cEvent)==lower("w_FLDenom Changed") or lower(cEvent)==lower("w_FLEml Changed") or lower(cEvent)==lower("w_FLPEC Changed") or lower(cEvent)==lower("w_FLRiferi Changed") or lower(cEvent)==lower("w_FLTele Changed")
          .Calculate_AFCSWEUSAV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NOCODGRU
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_NOCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_NOCODGRU))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODGRU)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" GNDESCRI like "+cp_ToStrODBC(trim(this.w_NOCODGRU)+"%");

            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GNDESCRI like "+cp_ToStr(trim(this.w_NOCODGRU)+"%");

            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oNOCODGRU_2_11'),i_cWhere,'',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_NOCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_NOCODGRU)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODGRU = NVL(_Link_.GNCODICE,space(5))
      this.w_DESGRU = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODGRU = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCODORI
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AON',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_NOCODORI)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_NOCODORI))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODORI)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ONDESCRI like "+cp_ToStrODBC(trim(this.w_NOCODORI)+"%");

            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ONDESCRI like "+cp_ToStr(trim(this.w_NOCODORI)+"%");

            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODORI) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oNOCODORI_2_12'),i_cWhere,'GSAR_AON',"Origine",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_NOCODORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_NOCODORI)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODORI = NVL(_Link_.ONCODICE,space(5))
      this.w_DESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODORI = space(5)
      endif
      this.w_DESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NORIFINT
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NORIFINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoPersona;
                     ,'DPCODICE',trim(this.w_NORIFINT))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NORIFINT)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoPersona);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoPersona);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NORIFINT) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oNORIFINT_2_13'),i_cWhere,'',"Riferimenti interni",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoPersona<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NORIFINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_NORIFINT);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoPersona);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoPersona;
                       ,'DPCODICE',this.w_NORIFINT)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NORIFINT = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOME = NVL(_Link_.DPCOGNOM,space(10))
      this.w_NOME = NVL(_Link_.DPNOME,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_NORIFINT = space(5)
      endif
      this.w_COGNOME = space(10)
      this.w_NOME = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NORIFINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodUte
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodUte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CodUte);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CodUte)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CodUte) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','CODE',cp_AbsName(oSource.parent,'oCodUte_2_14'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodUte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CodUte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CodUte)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodUte = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CodUte = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodUte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT1
  func Link_2_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT1))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT1)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT1) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT1_2_40'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT1)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT1 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT1 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT1 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT1 = space(10)
      endif
      this.w_DESATT1 = space(35)
      this.w_TIPCAT1 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT2
  func Link_2_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT2))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT2)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT2) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT2_2_41'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT2)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT2 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT2 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT2 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT2 = space(10)
      endif
      this.w_DESATT2 = space(35)
      this.w_TIPCAT2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RACODAT3
  func Link_2_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_ATTR_IDX,3]
    i_lTable = "CAT_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2], .t., this.CAT_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RACODAT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_aot',True,'CAT_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_RACODAT3)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_RACODAT3))
          select CTCODICE,CTDESCRI,CTTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RACODAT3)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RACODAT3) and !this.bDontReportError
            deferred_cp_zoom('CAT_ATTR','*','CTCODICE',cp_AbsName(oSource.parent,'oRACODAT3_2_42'),i_cWhere,'gsar_aot',"Categorie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RACODAT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI,CTTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_RACODAT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_RACODAT3)
            select CTCODICE,CTDESCRI,CTTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RACODAT3 = NVL(_Link_.CTCODICE,space(10))
      this.w_DESATT3 = NVL(_Link_.CTDESCRI,space(35))
      this.w_TIPCAT3 = NVL(_Link_.CTTIPCAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RACODAT3 = space(10)
      endif
      this.w_DESATT3 = space(35)
      this.w_TIPCAT3 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RACODAT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT1
  func Link_2_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT1)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT1 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT1 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT1 = space(5)
      endif
      this.w_DESCAT1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT2
  func Link_2_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT2)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT2 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT2 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT2 = space(5)
      endif
      this.w_DESCAT2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPCAT3
  func Link_2_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CATT_IDX,3]
    i_lTable = "TIP_CATT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2], .t., this.TIP_CATT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPCAT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPCAT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TIPCAT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TIPCAT3)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPCAT3 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAT3 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPCAT3 = space(5)
      endif
      this.w_DESCAT3 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CATT_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CATT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPCAT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page2.oPag.oINDIRIZZO_2_2.value==this.w_INDIRIZZO)
      this.oPgFrm.Page2.oPag.oINDIRIZZO_2_2.value=this.w_INDIRIZZO
    endif
    if not(this.oPgFrm.Page2.oPag.oAN___CAP_2_3.value==this.w_AN___CAP)
      this.oPgFrm.Page2.oPag.oAN___CAP_2_3.value=this.w_AN___CAP
    endif
    if not(this.oPgFrm.Page2.oPag.oANLOCALI_2_4.value==this.w_ANLOCALI)
      this.oPgFrm.Page2.oPag.oANLOCALI_2_4.value=this.w_ANLOCALI
    endif
    if not(this.oPgFrm.Page2.oPag.oANPROVIN_2_5.value==this.w_ANPROVIN)
      this.oPgFrm.Page2.oPag.oANPROVIN_2_5.value=this.w_ANPROVIN
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTE_2_6.value==this.w_NOTE)
      this.oPgFrm.Page2.oPag.oNOTE_2_6.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oPDATINIZ_2_7.value==this.w_PDATINIZ)
      this.oPgFrm.Page2.oPag.oPDATINIZ_2_7.value=this.w_PDATINIZ
    endif
    if not(this.oPgFrm.Page2.oPag.oobsodat1_2_8.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page2.oPag.oobsodat1_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPONOME_2_9.RadioValue()==this.w_TIPONOME)
      this.oPgFrm.Page2.oPag.oTIPONOME_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPONOME_2_10.RadioValue()==this.w_TIPONOME)
      this.oPgFrm.Page2.oPag.oTIPONOME_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCODGRU_2_11.value==this.w_NOCODGRU)
      this.oPgFrm.Page2.oPag.oNOCODGRU_2_11.value=this.w_NOCODGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCODORI_2_12.value==this.w_NOCODORI)
      this.oPgFrm.Page2.oPag.oNOCODORI_2_12.value=this.w_NOCODORI
    endif
    if not(this.oPgFrm.Page2.oPag.oNORIFINT_2_13.value==this.w_NORIFINT)
      this.oPgFrm.Page2.oPag.oNORIFINT_2_13.value=this.w_NORIFINT
    endif
    if not(this.oPgFrm.Page2.oPag.oCodUte_2_14.value==this.w_CodUte)
      this.oPgFrm.Page2.oPag.oCodUte_2_14.value=this.w_CodUte
    endif
    if not(this.oPgFrm.Page2.oPag.oCONS_DATI_2_15.RadioValue()==this.w_CONS_DATI)
      this.oPgFrm.Page2.oPag.oCONS_DATI_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU_2_20.value==this.w_DESGRU)
      this.oPgFrm.Page2.oPag.oDESGRU_2_20.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDenom_1_51.RadioValue()==this.w_FLDenom)
      this.oPgFrm.Page1.oPag.oFLDenom_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLRiferi_1_52.RadioValue()==this.w_FLRiferi)
      this.oPgFrm.Page1.oPag.oFLRiferi_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLEml_1_53.RadioValue()==this.w_FLEml)
      this.oPgFrm.Page1.oPag.oFLEml_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCoEml_1_54.RadioValue()==this.w_FLCoEml)
      this.oPgFrm.Page1.oPag.oFLCoEml_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPEC_1_55.RadioValue()==this.w_FLPEC)
      this.oPgFrm.Page1.oPag.oFLPEC_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCoPEC_1_56.RadioValue()==this.w_FLCoPEC)
      this.oPgFrm.Page1.oPag.oFLCoPEC_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTele_1_57.RadioValue()==this.w_FLTele)
      this.oPgFrm.Page1.oPag.oFLTele_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCotele_1_58.RadioValue()==this.w_FLCotele)
      this.oPgFrm.Page1.oPag.oFLCotele_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOC_1_59.value==this.w_RAGSOC)
      this.oPgFrm.Page1.oPag.oRAGSOC_1_59.value=this.w_RAGSOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESUTE_2_24.value==this.w_DESUTE)
      this.oPgFrm.Page2.oPag.oDESUTE_2_24.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESORI_2_26.value==this.w_DESORI)
      this.oPgFrm.Page2.oPag.oDESORI_2_26.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIFINT_2_29.value==this.w_DESRIFINT)
      this.oPgFrm.Page2.oPag.oDESRIFINT_2_29.value=this.w_DESRIFINT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT1_2_34.value==this.w_DESCAT1)
      this.oPgFrm.Page2.oPag.oDESCAT1_2_34.value=this.w_DESCAT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT1_2_35.value==this.w_DESATT1)
      this.oPgFrm.Page2.oPag.oDESATT1_2_35.value=this.w_DESATT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT2_2_36.value==this.w_DESCAT2)
      this.oPgFrm.Page2.oPag.oDESCAT2_2_36.value=this.w_DESCAT2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT2_2_37.value==this.w_DESATT2)
      this.oPgFrm.Page2.oPag.oDESATT2_2_37.value=this.w_DESATT2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT3_2_38.value==this.w_DESCAT3)
      this.oPgFrm.Page2.oPag.oDESCAT3_2_38.value=this.w_DESCAT3
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATT3_2_39.value==this.w_DESATT3)
      this.oPgFrm.Page2.oPag.oDESATT3_2_39.value=this.w_DESATT3
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT1_2_40.value==this.w_RACODAT1)
      this.oPgFrm.Page2.oPag.oRACODAT1_2_40.value=this.w_RACODAT1
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT2_2_41.value==this.w_RACODAT2)
      this.oPgFrm.Page2.oPag.oRACODAT2_2_41.value=this.w_RACODAT2
    endif
    if not(this.oPgFrm.Page2.oPag.oRACODAT3_2_42.value==this.w_RACODAT3)
      this.oPgFrm.Page2.oPag.oRACODAT3_2_42.value=this.w_RACODAT3
    endif
    if not(this.oPgFrm.Page2.oPag.oFLIndirizzo_2_44.RadioValue()==this.w_FLIndirizzo)
      this.oPgFrm.Page2.oPag.oFLIndirizzo_2_44.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLLocali_2_46.RadioValue()==this.w_FLLocali)
      this.oPgFrm.Page2.oPag.oFLLocali_2_46.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLTelefono_2_48.RadioValue()==this.w_FLTelefono)
      this.oPgFrm.Page2.oPag.oFLTelefono_2_48.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLFax_2_50.RadioValue()==this.w_FLFax)
      this.oPgFrm.Page2.oPag.oFLFax_2_50.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLCellulare_2_52.RadioValue()==this.w_FLCellulare)
      this.oPgFrm.Page2.oPag.oFLCellulare_2_52.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLEmail_2_54.RadioValue()==this.w_FLEmail)
      this.oPgFrm.Page2.oPag.oFLEmail_2_54.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLSito_2_55.RadioValue()==this.w_FLSito)
      this.oPgFrm.Page2.oPag.oFLSito_2_55.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLPersone_2_56.RadioValue()==this.w_FLPersone)
      this.oPgFrm.Page2.oPag.oFLPersone_2_56.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLEMPEC_2_58.RadioValue()==this.w_FLEMPEC)
      this.oPgFrm.Page2.oPag.oFLEMPEC_2_58.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PDATINIZ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPDATINIZ_2_7.SetFocus()
            i_bnoObbl = !empty(.w_PDATINIZ)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NORIFINT = this.w_NORIFINT
    this.o_PartiDa = this.w_PartiDa
    return

enddefine

* --- Define pages as container
define class tgsag_kruPag1 as StdContainer
  Width  = 800
  height = 536
  stdWidth  = 800
  stdheight = 536
  resizeXpos=796
  resizeYpos=488
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ctRubrica as cp_ctContact with uid="TXEKTATYPH",left=2, top=84, width=798,height=455,;
    caption='ctRubrica',;
   bGlobalFont=.t.,;
    FontName="MsSansSerif",Visible=.t.,FontSize=8,Serial="w_CODICE,w_CodCliente",Ncolumn=4,HeaderFillType=2,;
    nPag=1;
    , HelpContextID = 240092775


  add object oBtn_1_20 as StdButton with uid="FJXTEQBGEM",left=5, top=8, width=36,height=25,;
    caption="1-9", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione da un numero";
    , HelpContextID = 115762938;
  , bGlobalFont=.t.

    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","0","123456789")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="TDDTPYMASP",left=47, top=8, width=23,height=25,;
    caption="A", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'A'";
    , HelpContextID = 116007674;
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="LBOSOVGTVK",left=76, top=8, width=23,height=25,;
    caption="B", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'B'";
    , HelpContextID = 116007658;
  , bGlobalFont=.t.

    proc oBtn_1_23.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","B")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="XSEHVPGUQS",left=105, top=8, width=23,height=25,;
    caption="C", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'C'";
    , HelpContextID = 116007642;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="SZNCOOPPLW",left=134, top=8, width=23,height=25,;
    caption="D", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'D'";
    , HelpContextID = 116007626;
  , bGlobalFont=.t.

    proc oBtn_1_25.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="BSXQVKSGBT",left=163, top=8, width=23,height=25,;
    caption="E", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'E'";
    , HelpContextID = 116007610;
  , bGlobalFont=.t.

    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_27 as StdButton with uid="VJLDKMZVWK",left=192, top=8, width=23,height=25,;
    caption="F", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'F'";
    , HelpContextID = 116007594;
  , bGlobalFont=.t.

    proc oBtn_1_27.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","F")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="VCCOYHNLVE",left=221, top=8, width=23,height=25,;
    caption="G", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'G'";
    , HelpContextID = 116007578;
  , bGlobalFont=.t.

    proc oBtn_1_28.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_29 as StdButton with uid="QUWAUTTECW",left=250, top=8, width=23,height=25,;
    caption="H", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'H'";
    , HelpContextID = 116007562;
  , bGlobalFont=.t.

    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","H")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_30 as StdButton with uid="SLXBFDYBMD",left=279, top=8, width=23,height=25,;
    caption="I", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'I'";
    , HelpContextID = 116007546;
  , bGlobalFont=.t.

    proc oBtn_1_30.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="TZAXHDPWVY",left=308, top=8, width=23,height=25,;
    caption="J", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'J'";
    , HelpContextID = 116007530;
  , bGlobalFont=.t.

    proc oBtn_1_31.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","J")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_32 as StdButton with uid="RSINNUPARS",left=337, top=8, width=23,height=25,;
    caption="K", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'K'";
    , HelpContextID = 116007514;
  , bGlobalFont=.t.

    proc oBtn_1_32.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","K")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_33 as StdButton with uid="BXIXBPXJHB",left=366, top=8, width=23,height=25,;
    caption="L", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'L'";
    , HelpContextID = 116007498;
  , bGlobalFont=.t.

    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","L")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="YZPSWZRTIU",left=395, top=8, width=23,height=25,;
    caption="M", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'M'";
    , HelpContextID = 116007482;
  , bGlobalFont=.t.

    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","M")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_35 as StdButton with uid="YLMSTCFEUC",left=424, top=8, width=23,height=25,;
    caption="N", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'N'";
    , HelpContextID = 116007466;
  , bGlobalFont=.t.

    proc oBtn_1_35.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="IDTPBEKSQF",left=453, top=8, width=23,height=25,;
    caption="O", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'O'";
    , HelpContextID = 116007450;
  , bGlobalFont=.t.

    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","O")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="TSTLPOITPF",left=482, top=8, width=23,height=25,;
    caption="P", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'P'";
    , HelpContextID = 116007434;
  , bGlobalFont=.t.

    proc oBtn_1_37.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="OLMFGVTDQE",left=511, top=8, width=23,height=25,;
    caption="Q", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'Q'";
    , HelpContextID = 116007418;
  , bGlobalFont=.t.

    proc oBtn_1_38.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","Q")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_39 as StdButton with uid="VIVOCYHHGA",left=540, top=8, width=23,height=25,;
    caption="R", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'R'";
    , HelpContextID = 116007402;
  , bGlobalFont=.t.

    proc oBtn_1_39.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_40 as StdButton with uid="JBFHNYEMVX",left=569, top=8, width=23,height=25,;
    caption="S", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'S'";
    , HelpContextID = 116007386;
  , bGlobalFont=.t.

    proc oBtn_1_40.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_41 as StdButton with uid="YGRHTMEOXJ",left=598, top=8, width=23,height=25,;
    caption="T", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'T'";
    , HelpContextID = 116007370;
  , bGlobalFont=.t.

    proc oBtn_1_41.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","T")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_41.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_42 as StdButton with uid="NVCMMCPGDU",left=627, top=8, width=23,height=25,;
    caption="U", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'U'";
    , HelpContextID = 116007354;
  , bGlobalFont=.t.

    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","U")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_42.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_43 as StdButton with uid="SVFDSABHYY",left=656, top=8, width=23,height=25,;
    caption="V", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'V'";
    , HelpContextID = 116007338;
  , bGlobalFont=.t.

    proc oBtn_1_43.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_43.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_44 as StdButton with uid="RMAUZATEGY",left=685, top=8, width=23,height=25,;
    caption="W", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'W'";
    , HelpContextID = 116007322;
  , bGlobalFont=.t.

    proc oBtn_1_44.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","W")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_44.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_45 as StdButton with uid="WJYCRSUXUB",left=714, top=8, width=23,height=25,;
    caption="X", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'X'";
    , HelpContextID = 116007306;
  , bGlobalFont=.t.

    proc oBtn_1_45.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","X")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_45.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_46 as StdButton with uid="BUVRFBQHZD",left=743, top=8, width=23,height=25,;
    caption="Y", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'Y'";
    , HelpContextID = 116007290;
  , bGlobalFont=.t.

    proc oBtn_1_46.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","Y")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_46.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc


  add object oBtn_1_47 as StdButton with uid="POVVQSUMTT",left=772, top=8, width=23,height=25,;
    caption="Z", nPag=1;
    , ToolTipText = "Premere per iniziare la visualizzazione dalla lettera 'Z'";
    , HelpContextID = 116007274;
  , bGlobalFont=.t.

    proc oBtn_1_47.Click()
      with this.Parent.oContained
        GSAG_BRU(this.Parent.oContained,"CERCA","Z")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NumElem=0)
     endwith
    endif
  endfunc

  add object oFLDenom_1_51 as StdCheck with uid="SJCDEGVUCP",rtseq=31,rtrep=.f.,left=200, top=37, caption="Denominazione",;
    ToolTipText = "Se attivo, esegue la ricerca nella denominazione, nel nome o nel cognome",;
    HelpContextID = 257911126,;
    cFormVar="w_FLDenom", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDenom_1_51.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLDenom_1_51.GetRadio()
    this.Parent.oContained.w_FLDenom = this.RadioValue()
    return .t.
  endfunc

  func oFLDenom_1_51.SetRadio()
    this.Parent.oContained.w_FLDenom=trim(this.Parent.oContained.w_FLDenom)
    this.value = ;
      iif(this.Parent.oContained.w_FLDenom=='S',1,;
      0)
  endfunc

  add object oFLRiferi_1_52 as StdCheck with uid="UOFYBFVXJL",rtseq=32,rtrep=.f.,left=200, top=62, caption="Persone",;
    ToolTipText = "Se attivo, esegue la ricerca nel riferimento persone",;
    HelpContextID = 82069951,;
    cFormVar="w_FLRiferi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLRiferi_1_52.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLRiferi_1_52.GetRadio()
    this.Parent.oContained.w_FLRiferi = this.RadioValue()
    return .t.
  endfunc

  func oFLRiferi_1_52.SetRadio()
    this.Parent.oContained.w_FLRiferi=trim(this.Parent.oContained.w_FLRiferi)
    this.value = ;
      iif(this.Parent.oContained.w_FLRiferi=='S',1,;
      0)
  endfunc

  add object oFLEml_1_53 as StdCheck with uid="QJBNHBBZFH",rtseq=33,rtrep=.f.,left=336, top=37, caption="E-mail",;
    ToolTipText = "Se attivo, esegue la ricerca nell'indirizzo e-mail",;
    HelpContextID = 263751338,;
    cFormVar="w_FLEml", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLEml_1_53.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLEml_1_53.GetRadio()
    this.Parent.oContained.w_FLEml = this.RadioValue()
    return .t.
  endfunc

  func oFLEml_1_53.SetRadio()
    this.Parent.oContained.w_FLEml=trim(this.Parent.oContained.w_FLEml)
    this.value = ;
      iif(this.Parent.oContained.w_FLEml=='S',1,;
      0)
  endfunc

  add object oFLCoEml_1_54 as StdCheck with uid="JYEWJAKAUD",rtseq=34,rtrep=.f.,left=336, top=62, caption="E-mail persone",;
    ToolTipText = "Se attivo, esegue la ricerca nell'indirizzo e-mail delle persone",;
    HelpContextID = 86419114,;
    cFormVar="w_FLCoEml", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCoEml_1_54.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLCoEml_1_54.GetRadio()
    this.Parent.oContained.w_FLCoEml = this.RadioValue()
    return .t.
  endfunc

  func oFLCoEml_1_54.SetRadio()
    this.Parent.oContained.w_FLCoEml=trim(this.Parent.oContained.w_FLCoEml)
    this.value = ;
      iif(this.Parent.oContained.w_FLCoEml=='S',1,;
      0)
  endfunc

  add object oFLPEC_1_55 as StdCheck with uid="GAOUJRJRMN",rtseq=35,rtrep=.f.,left=471, top=37, caption="PEC",;
    ToolTipText = "Se attivo, esegue la ricerca nell'indirizzo PEC",;
    HelpContextID = 40883882,;
    cFormVar="w_FLPEC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLPEC_1_55.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLPEC_1_55.GetRadio()
    this.Parent.oContained.w_FLPEC = this.RadioValue()
    return .t.
  endfunc

  func oFLPEC_1_55.SetRadio()
    this.Parent.oContained.w_FLPEC=trim(this.Parent.oContained.w_FLPEC)
    this.value = ;
      iif(this.Parent.oContained.w_FLPEC=='S',1,;
      0)
  endfunc

  add object oFLCoPEC_1_56 as StdCheck with uid="DWXEJABSWA",rtseq=36,rtrep=.f.,left=471, top=62, caption="PEC persone",;
    ToolTipText = "Se attivo, esegue la ricerca nell'indirizzo PEC delle persone",;
    HelpContextID = 59332950,;
    cFormVar="w_FLCoPEC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCoPEC_1_56.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLCoPEC_1_56.GetRadio()
    this.Parent.oContained.w_FLCoPEC = this.RadioValue()
    return .t.
  endfunc

  func oFLCoPEC_1_56.SetRadio()
    this.Parent.oContained.w_FLCoPEC=trim(this.Parent.oContained.w_FLCoPEC)
    this.value = ;
      iif(this.Parent.oContained.w_FLCoPEC=='S',1,;
      0)
  endfunc

  add object oFLTele_1_57 as StdCheck with uid="HENLOCLUIJ",rtseq=37,rtrep=.f.,left=606, top=37, caption="Telefoni",;
    ToolTipText = "Se attivo, esegue la ricerca nei numeri di telefono",;
    HelpContextID = 88107350,;
    cFormVar="w_FLTele", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLTele_1_57.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLTele_1_57.GetRadio()
    this.Parent.oContained.w_FLTele = this.RadioValue()
    return .t.
  endfunc

  func oFLTele_1_57.SetRadio()
    this.Parent.oContained.w_FLTele=trim(this.Parent.oContained.w_FLTele)
    this.value = ;
      iif(this.Parent.oContained.w_FLTele=='S',1,;
      0)
  endfunc

  add object oFLCotele_1_58 as StdCheck with uid="MXKFHDNEIU",rtseq=38,rtrep=.f.,left=606, top=62, caption="Telefoni persone",;
    ToolTipText = "Se attivo, esegue la ricerca nei numeri di telefono delle persone",;
    HelpContextID = 171353669,;
    cFormVar="w_FLCotele", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCotele_1_58.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLCotele_1_58.GetRadio()
    this.Parent.oContained.w_FLCotele = this.RadioValue()
    return .t.
  endfunc

  func oFLCotele_1_58.SetRadio()
    this.Parent.oContained.w_FLCotele=trim(this.Parent.oContained.w_FLCotele)
    this.value = ;
      iif(this.Parent.oContained.w_FLCotele=='S',1,;
      0)
  endfunc

  add object oRAGSOC_1_59 as AH_SEARCHFLD with uid="CBKKYKLBHC",rtseq=39,rtrep=.f.,;
    cFormVar = "w_RAGSOC", cQueryName = "RAGSOC",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Filtro per ricerca contestuale",;
    HelpContextID = 22908694,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=67, Top=40, InputMask=replicate('X',100)


  add object oBtn_1_66 as StdButton with uid="BQJHHOQZPU",left=743, top=45, width=23,height=25,;
    CpPicture="bmp\refresh.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per annullare i filtri di selezione";
    , HelpContextID = 206711558;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_66.Click()
      with this.Parent.oContained
        .Blankrec()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_67 as StdButton with uid="PTFJHVGUTY",left=772, top=45, width=23,height=25,;
    CpPicture="tzoom.bmp", caption="", nPag=1;
    , ToolTipText = "Aggiorna la visualizzazione";
    , HelpContextID = 116008714;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_67.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="AUXASRUDLA",Visible=.t., Left=1, Top=45,;
    Alignment=1, Width=58, Height=18,;
    Caption="Trova:"  ;
  , bGlobalFont=.t.

  add object oBox_1_5 as StdBox with uid="LOIZABPOHX",left=64, top=37, width=118,height=28
enddefine
define class tgsag_kruPag2 as StdContainer
  Width  = 800
  height = 536
  stdWidth  = 800
  stdheight = 536
  resizeXpos=290
  resizeYpos=502
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oINDIRIZZO_2_2 as AH_SEARCHFLD with uid="IORAKYZOEG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_INDIRIZZO", cQueryName = "INDIRIZZO",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo",;
    HelpContextID = 142380848,;
   bGlobalFont=.t.,;
    Height=21, Width=426, Left=125, Top=23, InputMask=replicate('X',50)

  add object oAN___CAP_2_3 as StdField with uid="SROGUQVEVX",rtseq=14,rtrep=.f.,;
    cFormVar = "w_AN___CAP", cQueryName = "AN___CAP",;
    bObbl = .f. , nPag = 2, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P.",;
    HelpContextID = 40573782,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=590, Top=23, InputMask=replicate('X',8)

  add object oANLOCALI_2_4 as AH_SEARCHFLD with uid="LTRRSDDYFO",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ANLOCALI", cQueryName = "ANLOCALI",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit�",;
    HelpContextID = 23467185,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=125, Top=53, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oANLOCALI_2_4.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_ANLOCALI",".w_ANPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANPROVIN_2_5 as StdField with uid="GOJLCTWZPB",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ANPROVIN", cQueryName = "ANPROVIN",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia",;
    HelpContextID = 195220652,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=524, Top=53, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  add object oNOTE_2_6 as AH_SEARCHMEMO with uid="AXNLDTQQRX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Annotazioni o parte di esse",;
    HelpContextID = 111121194,;
   bGlobalFont=.t.,;
    Height=21, Width=534, Left=125, Top=84

  add object oPDATINIZ_2_7 as StdField with uid="KXZLKOTRZF",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PDATINIZ", cQueryName = "PDATINIZ",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 67227056,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=125, Top=114

  add object oobsodat1_2_8 as StdCheck with uid="TANZHKUOWP",rtseq=19,rtrep=.f.,left=221, top=114, caption="Solo obsoleti",;
    ToolTipText = "Stampa solo obsoleti",;
    HelpContextID = 13398551,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oobsodat1_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_2_8.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_2_8.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc


  add object oTIPONOME_2_9 as StdCombo with uid="XBBFEWCVIO",rtseq=20,rtrep=.f.,left=419,top=115,width=188,height=22;
    , ToolTipText = "Visualizza la tipologia di nominativo selezionata";
    , HelpContextID = 45471877;
    , cFormVar="w_TIPONOME",RowSource=""+"Solo clienti,"+"Solo fornitori,"+"Solo nominativi,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPONOME_2_9.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'T',;
    iif(this.value =4,'X',;
    space(1))))))
  endfunc
  func oTIPONOME_2_9.GetRadio()
    this.Parent.oContained.w_TIPONOME = this.RadioValue()
    return .t.
  endfunc

  func oTIPONOME_2_9.SetRadio()
    this.Parent.oContained.w_TIPONOME=trim(this.Parent.oContained.w_TIPONOME)
    this.value = ;
      iif(this.Parent.oContained.w_TIPONOME=='C',1,;
      iif(this.Parent.oContained.w_TIPONOME=='F',2,;
      iif(this.Parent.oContained.w_TIPONOME=='T',3,;
      iif(this.Parent.oContained.w_TIPONOME=='X',4,;
      0))))
  endfunc

  func oTIPONOME_2_9.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oTIPONOME_2_10 as StdCheck with uid="ITZQIPTSXH",rtseq=21,rtrep=.f.,left=419, top=114, caption="Solo clienti",;
    ToolTipText = "Se attivo, visualizza solo i clienti",;
    HelpContextID = 45471877,;
    cFormVar="w_TIPONOME", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTIPONOME_2_10.RadioValue()
    return(iif(this.value =1,'C',;
    space(1)))
  endfunc
  func oTIPONOME_2_10.GetRadio()
    this.Parent.oContained.w_TIPONOME = this.RadioValue()
    return .t.
  endfunc

  func oTIPONOME_2_10.SetRadio()
    this.Parent.oContained.w_TIPONOME=trim(this.Parent.oContained.w_TIPONOME)
    this.value = ;
      iif(this.Parent.oContained.w_TIPONOME=='C',1,;
      0)
  endfunc

  func oTIPONOME_2_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oNOCODGRU_2_11 as StdField with uid="VAYPMELLUY",rtseq=22,rtrep=.f.,;
    cFormVar = "w_NOCODGRU", cQueryName = "NOCODGRU",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo",;
    HelpContextID = 78208299,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=125, Top=144, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", oKey_1_1="GNCODICE", oKey_1_2="this.w_NOCODGRU"

  func oNOCODGRU_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODGRU_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODGRU_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oNOCODGRU_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi nominativi",'',this.parent.oContained
  endproc

  add object oNOCODORI_2_12 as StdField with uid="OJETTPZXIQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_NOCODORI", cQueryName = "NOCODORI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine",;
    HelpContextID = 212426015,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=125, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", cZoomOnZoom="GSAR_AON", oKey_1_1="ONCODICE", oKey_1_2="this.w_NOCODORI"

  func oNOCODORI_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODORI_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODORI_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oNOCODORI_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AON',"Origine",'',this.parent.oContained
  endproc
  proc oNOCODORI_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AON()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ONCODICE=this.parent.oContained.w_NOCODORI
     i_obj.ecpSave()
  endproc

  add object oNORIFINT_2_13 as StdField with uid="TQTOGVNAZN",rtseq=24,rtrep=.f.,;
    cFormVar = "w_NORIFINT", cQueryName = "NORIFINT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento interno",;
    HelpContextID = 154907350,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=125, Top=202, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoPersona", oKey_2_1="DPCODICE", oKey_2_2="this.w_NORIFINT"

  func oNORIFINT_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oNORIFINT_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNORIFINT_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoPersona)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoPersona)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oNORIFINT_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Riferimenti interni",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oCodUte_2_14 as StdField with uid="PIUVUYSBHD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CodUte", cQueryName = "CodUte",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente ultima modifica",;
    HelpContextID = 95521830,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=125, Top=232, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_CodUte"

  func oCodUte_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodUte_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodUte_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','CODE',cp_AbsName(this.parent,'oCodUte_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oCONS_DATI_2_15 as StdRadio with uid="CSXZBUPOVR",rtseq=26,rtrep=.f.,left=658, top=186, width=116,height=49;
    , ToolTipText = "Selezionare lo stato relativo al consenso del trattamento dati personali";
    , cFormVar="w_CONS_DATI", ButtonCount=3, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oCONS_DATI_2_15.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Non ricevuto"
      this.Buttons(1).HelpContextID = 56496394
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Ricevuto"
      this.Buttons(2).HelpContextID = 56496394
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Tutti"
      this.Buttons(3).HelpContextID = 56496394
      this.Buttons(3).Top=30
      this.SetAll("Width",114)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Selezionare lo stato relativo al consenso del trattamento dati personali")
      StdRadio::init()
    endproc

  func oCONS_DATI_2_15.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oCONS_DATI_2_15.GetRadio()
    this.Parent.oContained.w_CONS_DATI = this.RadioValue()
    return .t.
  endfunc

  func oCONS_DATI_2_15.SetRadio()
    this.Parent.oContained.w_CONS_DATI=trim(this.Parent.oContained.w_CONS_DATI)
    this.value = ;
      iif(this.Parent.oContained.w_CONS_DATI=='N',1,;
      iif(this.Parent.oContained.w_CONS_DATI=='S',2,;
      iif(this.Parent.oContained.w_CONS_DATI=='',3,;
      0)))
  endfunc

  add object oDESGRU_2_20 as StdField with uid="QFFZKUTNXC",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 58872374,;
   bGlobalFont=.t.,;
    Height=21, Width=386, Left=221, Top=144, InputMask=replicate('X',35)

  add object oDESUTE_2_24 as StdField with uid="JXGTZBFHCK",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 61887030,;
   bGlobalFont=.t.,;
    Height=21, Width=386, Left=221, Top=232, InputMask=replicate('X',20)

  add object oDESORI_2_26 as StdField with uid="HIDPUISWDJ",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 126505526,;
   bGlobalFont=.t.,;
    Height=21, Width=386, Left=221, Top=173, InputMask=replicate('X',35)

  add object oDESRIFINT_2_29 as StdField with uid="QIOIXUSSHJ",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESRIFINT", cQueryName = "DESRIFINT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 201500732,;
   bGlobalFont=.t.,;
    Height=21, Width=386, Left=221, Top=202, InputMask=replicate('X',35)

  add object oDESCAT1_2_34 as StdField with uid="RQHPFELTYB",rtseq=53,rtrep=.f.,;
    cFormVar = "w_DESCAT1", cQueryName = "DESCAT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24007222,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=236, Top=312, InputMask=replicate('X',35)

  add object oDESATT1_2_35 as StdField with uid="BNZQCLINZM",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DESATT1", cQueryName = "DESATT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43799094,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=419, Top=312, InputMask=replicate('X',35)

  add object oDESCAT2_2_36 as StdField with uid="WJCRSTIJKM",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DESCAT2", cQueryName = "DESCAT2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24007222,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=236, Top=337, InputMask=replicate('X',35)

  add object oDESATT2_2_37 as StdField with uid="ZBFJSTINNA",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESATT2", cQueryName = "DESATT2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43799094,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=419, Top=337, InputMask=replicate('X',35)

  add object oDESCAT3_2_38 as StdField with uid="NKTPRZLJCA",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESCAT3", cQueryName = "DESCAT3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione tipo categoria",;
    HelpContextID = 24007222,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=236, Top=362, InputMask=replicate('X',35)

  add object oDESATT3_2_39 as StdField with uid="TBHEYGIQXL",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DESATT3", cQueryName = "DESATT3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione attributo",;
    HelpContextID = 43799094,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=419, Top=362, InputMask=replicate('X',35)

  add object oRACODAT1_2_40 as StdField with uid="QBQJLRJLTX",rtseq=59,rtrep=.f.,;
    cFormVar = "w_RACODAT1", cQueryName = "RACODAT1",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 245976903,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=132, Top=312, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT1"

  func oRACODAT1_2_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT1_2_40.ecpDrop(oSource)
    this.Parent.oContained.link_2_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT1_2_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT1_2_40'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT1_2_40.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT1
     i_obj.ecpSave()
  endproc

  add object oRACODAT2_2_41 as StdField with uid="ZGTNSJFFHG",rtseq=60,rtrep=.f.,;
    cFormVar = "w_RACODAT2", cQueryName = "RACODAT2",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 245976904,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=132, Top=337, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT2"

  func oRACODAT2_2_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT2_2_41.ecpDrop(oSource)
    this.Parent.oContained.link_2_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT2_2_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT2_2_41'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT2_2_41.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT2
     i_obj.ecpSave()
  endproc

  add object oRACODAT3_2_42 as StdField with uid="YAADXVOHNX",rtseq=61,rtrep=.f.,;
    cFormVar = "w_RACODAT3", cQueryName = "RACODAT3",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice attributo",;
    HelpContextID = 245976905,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=132, Top=362, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAT_ATTR", cZoomOnZoom="gsar_aot", oKey_1_1="CTCODICE", oKey_1_2="this.w_RACODAT3"

  func oRACODAT3_2_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oRACODAT3_2_42.ecpDrop(oSource)
    this.Parent.oContained.link_2_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRACODAT3_2_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_ATTR','*','CTCODICE',cp_AbsName(this.parent,'oRACODAT3_2_42'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_aot',"Categorie attributi",'',this.parent.oContained
  endproc
  proc oRACODAT3_2_42.mZoomOnZoom
    local i_obj
    i_obj=gsar_aot()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_RACODAT3
     i_obj.ecpSave()
  endproc

  add object oFLIndirizzo_2_44 as StdCheck with uid="UPPPKSTOYB",rtseq=63,rtrep=.f.,left=68, top=424, caption="Visualizza indirizzo",;
    ToolTipText = "Se attivo, visualizza l'indirizzo",;
    HelpContextID = 147860319,;
    cFormVar="w_FLIndirizzo", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLIndirizzo_2_44.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oFLIndirizzo_2_44.GetRadio()
    this.Parent.oContained.w_FLIndirizzo = this.RadioValue()
    return .t.
  endfunc

  func oFLIndirizzo_2_44.SetRadio()
    this.Parent.oContained.w_FLIndirizzo=trim(this.Parent.oContained.w_FLIndirizzo)
    this.value = ;
      iif(this.Parent.oContained.w_FLIndirizzo=='S',1,;
      0)
  endfunc

  add object oFLLocali_2_46 as StdCheck with uid="JEPFBEYUFY",rtseq=65,rtrep=.f.,left=268, top=424, caption="Visualizza cap, localit� e provincia",;
    ToolTipText = "Se attivo, visualizza cap, localit� e provincia",;
    HelpContextID = 256251457,;
    cFormVar="w_FLLocali", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLLocali_2_46.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oFLLocali_2_46.GetRadio()
    this.Parent.oContained.w_FLLocali = this.RadioValue()
    return .t.
  endfunc

  func oFLLocali_2_46.SetRadio()
    this.Parent.oContained.w_FLLocali=trim(this.Parent.oContained.w_FLLocali)
    this.value = ;
      iif(this.Parent.oContained.w_FLLocali=='S',1,;
      0)
  endfunc

  add object oFLTelefono_2_48 as StdCheck with uid="FVGXHPWYIU",rtseq=67,rtrep=.f.,left=523, top=424, caption="Visualizza numero di telefono",;
    ToolTipText = "Se attivo, visualizza il numero di telefono",;
    HelpContextID = 180297819,;
    cFormVar="w_FLTelefono", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLTelefono_2_48.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oFLTelefono_2_48.GetRadio()
    this.Parent.oContained.w_FLTelefono = this.RadioValue()
    return .t.
  endfunc

  func oFLTelefono_2_48.SetRadio()
    this.Parent.oContained.w_FLTelefono=trim(this.Parent.oContained.w_FLTelefono)
    this.value = ;
      iif(this.Parent.oContained.w_FLTelefono=='S',1,;
      0)
  endfunc

  add object oFLFax_2_50 as StdCheck with uid="ZTKDZHEMWK",rtseq=68,rtrep=.f.,left=68, top=452, caption="Visualizza numero di fax",;
    ToolTipText = "Se attivo, visualizza il numero di fax",;
    HelpContextID = 251950762,;
    cFormVar="w_FLFax", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLFax_2_50.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oFLFax_2_50.GetRadio()
    this.Parent.oContained.w_FLFax = this.RadioValue()
    return .t.
  endfunc

  func oFLFax_2_50.SetRadio()
    this.Parent.oContained.w_FLFax=trim(this.Parent.oContained.w_FLFax)
    this.value = ;
      iif(this.Parent.oContained.w_FLFax=='S',1,;
      0)
  endfunc

  add object oFLCellulare_2_52 as StdCheck with uid="SONQKJEONZ",rtseq=69,rtrep=.f.,left=268, top=452, caption="Visualizza numero di cellulare",;
    ToolTipText = "Se attivo, visualizza il numero di cellulare",;
    HelpContextID = 62512686,;
    cFormVar="w_FLCellulare", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLCellulare_2_52.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oFLCellulare_2_52.GetRadio()
    this.Parent.oContained.w_FLCellulare = this.RadioValue()
    return .t.
  endfunc

  func oFLCellulare_2_52.SetRadio()
    this.Parent.oContained.w_FLCellulare=trim(this.Parent.oContained.w_FLCellulare)
    this.value = ;
      iif(this.Parent.oContained.w_FLCellulare=='S',1,;
      0)
  endfunc

  add object oFLEmail_2_54 as StdCheck with uid="WSTYDQUMQL",rtseq=70,rtrep=.f.,left=523, top=452, caption="Visualizza indirizzo di e-mail",;
    ToolTipText = "Se attivo, visualizza l'e-mail",;
    HelpContextID = 124290730,;
    cFormVar="w_FLEmail", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLEmail_2_54.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oFLEmail_2_54.GetRadio()
    this.Parent.oContained.w_FLEmail = this.RadioValue()
    return .t.
  endfunc

  func oFLEmail_2_54.SetRadio()
    this.Parent.oContained.w_FLEmail=trim(this.Parent.oContained.w_FLEmail)
    this.value = ;
      iif(this.Parent.oContained.w_FLEmail=='S',1,;
      0)
  endfunc

  add object oFLSito_2_55 as StdCheck with uid="ZUZITZQXAY",rtseq=73,rtrep=.f.,left=68, top=480, caption="Visualizza sito",;
    ToolTipText = "Se attivo, visualizza il sito",;
    HelpContextID = 264526166,;
    cFormVar="w_FLSito", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLSito_2_55.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oFLSito_2_55.GetRadio()
    this.Parent.oContained.w_FLSito = this.RadioValue()
    return .t.
  endfunc

  func oFLSito_2_55.SetRadio()
    this.Parent.oContained.w_FLSito=trim(this.Parent.oContained.w_FLSito)
    this.value = ;
      iif(this.Parent.oContained.w_FLSito=='S',1,;
      0)
  endfunc

  add object oFLPersone_2_56 as StdCheck with uid="MRYLAXGYJS",rtseq=74,rtrep=.f.,left=268, top=480, caption="Visualizza le persone",;
    ToolTipText = "Se attivo, visualizza le persone",;
    HelpContextID = 60829716,;
    cFormVar="w_FLPersone", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLPersone_2_56.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLPersone_2_56.GetRadio()
    this.Parent.oContained.w_FLPersone = this.RadioValue()
    return .t.
  endfunc

  func oFLPersone_2_56.SetRadio()
    this.Parent.oContained.w_FLPersone=trim(this.Parent.oContained.w_FLPersone)
    this.value = ;
      iif(this.Parent.oContained.w_FLPersone=='S',1,;
      0)
  endfunc

  add object oFLEMPEC_2_58 as StdCheck with uid="WJRREMZVAP",rtseq=78,rtrep=.f.,left=523, top=480, caption="Visualizza indirizzo PEC",;
    ToolTipText = "Se attivo, visualizza l'e-mail PEC",;
    HelpContextID = 57112918,;
    cFormVar="w_FLEMPEC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLEMPEC_2_58.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oFLEMPEC_2_58.GetRadio()
    this.Parent.oContained.w_FLEMPEC = this.RadioValue()
    return .t.
  endfunc

  func oFLEMPEC_2_58.SetRadio()
    this.Parent.oContained.w_FLEMPEC=trim(this.Parent.oContained.w_FLEMPEC)
    this.value = ;
      iif(this.Parent.oContained.w_FLEMPEC=='S',1,;
      0)
  endfunc


  add object oBtn_2_59 as StdButton with uid="TMRCZGTRFE",left=731, top=23, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=2;
    , ToolTipText = "Aggiorna la visualizzazione";
    , HelpContextID = 60913430;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_59.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_16 as StdString with uid="RBICKTOIZY",Visible=.t., Left=49, Top=23,;
    Alignment=1, Width=71, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="ZGFGSXQHJO",Visible=.t., Left=554, Top=23,;
    Alignment=1, Width=32, Height=18,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="KMPITYQKJP",Visible=.t., Left=49, Top=53,;
    Alignment=1, Width=71, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="ZEAKVIUCVR",Visible=.t., Left=465, Top=53,;
    Alignment=1, Width=52, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="FNCYFRUTUH",Visible=.t., Left=21, Top=144,;
    Alignment=1, Width=99, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="ELGVYCEQRO",Visible=.t., Left=4, Top=114,;
    Alignment=1, Width=116, Height=18,;
    Caption="Data di controllo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="EIENBMKRMG",Visible=.t., Left=21, Top=232,;
    Alignment=1, Width=99, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="ZPREBAWLMV",Visible=.t., Left=21, Top=172,;
    Alignment=1, Width=99, Height=18,;
    Caption="Origine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="CVVHFUWPFJ",Visible=.t., Left=7, Top=201,;
    Alignment=1, Width=113, Height=18,;
    Caption="Riferimento interno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="XQIYOIBOJG",Visible=.t., Left=125, Top=283,;
    Alignment=0, Width=95, Height=18,;
    Caption="Attributi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_49 as StdString with uid="PMBUJPPQBN",Visible=.t., Left=46, Top=84,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="UQAGSEBNEY",Visible=.t., Left=616, Top=156,;
    Alignment=1, Width=159, Height=18,;
    Caption="Consenso trattamento dati"  ;
  , bGlobalFont=.t.

  add object oBox_2_33 as StdBox with uid="UFZFHCAVGL",left=125, top=302, width=484,height=91

  add object oBox_2_53 as StdBox with uid="BXCGNKBEAI",left=640, top=174, width=145,height=80
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kru','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
