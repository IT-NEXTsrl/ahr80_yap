* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bri                                                        *
*              Posponi il promemoria dell'attivit�                             *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-06-11                                                      *
* Last revis.: 2009-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pATSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bri",oParentObject,m.pATSERIAL)
return(i_retval)

define class tgsag_bri as StdBatch
  * --- Local variables
  pATSERIAL = space(20)
  w_ATSERIAL = space(15)
  w_ATCAUATT = space(20)
  w_ATOGGETT = space(254)
  w_ATNOTPIA = space(0)
  w_ATCODPRA = space(20)
  w_DESCAN = space(100)
  w_ATDATINI = ctot("")
  w_ATDATFIN = ctot("")
  w_ATPROMEM = ctot("")
  w_DATINI = ctot("")
  w_DATFIN = ctot("")
  w_DATAPRO = ctot("")
  w_ORAINI = space(2)
  w_ORAFIN = space(2)
  w_ORAPRO = space(2)
  w_MININI = space(2)
  w_MINFIN = space(2)
  w_MINPRO = space(2)
  w_AGGIORNA = space(1)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  CAN_TIER_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rinvio attivit�
    this.w_ATSERIAL = this.pATSERIAL
    * --- Read from OFF_ATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATDATINI,ATDATFIN,ATPROMEM,ATCAUATT,ATOGGETT,ATNOTPIA,ATCODPRA"+;
        " from "+i_cTable+" OFF_ATTI where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATDATINI,ATDATFIN,ATPROMEM,ATCAUATT,ATOGGETT,ATNOTPIA,ATCODPRA;
        from (i_cTable) where;
            ATSERIAL = this.w_ATSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ATDATINI = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
      this.w_ATDATFIN = NVL((_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
      this.w_ATPROMEM = NVL((_read_.ATPROMEM),cp_NullValue(_read_.ATPROMEM))
      this.w_ATCAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
      this.w_ATOGGETT = NVL(cp_ToDate(_read_.ATOGGETT),cp_NullValue(_read_.ATOGGETT))
      this.w_ATNOTPIA = NVL(cp_ToDate(_read_.ATNOTPIA),cp_NullValue(_read_.ATNOTPIA))
      this.w_ATCODPRA = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAN_TIER
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CNDESCAN"+;
        " from "+i_cTable+" CAN_TIER where ";
            +"CNCODCAN = "+cp_ToStrODBC(this.w_ATCODPRA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CNDESCAN;
        from (i_cTable) where;
            CNCODCAN = this.w_ATCODPRA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESCAN = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Variabili utilizzate dalla maschera
    this.w_DATINI = CP_TODATE( this.w_ATDATINI )
    this.w_DATFIN = CP_TODATE( this.w_ATDATFIN )
    this.w_DATAPRO = CP_TODATE( this.w_ATPROMEM )
    this.w_ORAINI = RIGHT( "00" + ALLTRIM( STR( HOUR( this.w_ATDATINI ) ) ) , 2 )
    this.w_ORAFIN = RIGHT( "00" + ALLTRIM( STR( HOUR( this.w_ATDATFIN ) ) ) , 2 )
    this.w_ORAPRO = RIGHT( "00" + ALLTRIM( STR( HOUR( this.w_ATPROMEM ) ) ) , 2 )
    this.w_MININI = RIGHT( "00" + ALLTRIM( STR( MINUTE( this.w_ATDATINI ) ) ) , 2 )
    this.w_MINFIN = RIGHT( "00" + ALLTRIM( STR( MINUTE( this.w_ATDATFIN ) ) ) , 2 )
    this.w_MINPRO = RIGHT( "00" + ALLTRIM( STR( MINUTE( this.w_ATPROMEM ) ) ) , 2 )
    * --- w_AGGIORNA viene impostato a "S" dalla maschera
    this.w_AGGIORNA = "N"
    do GSAG_KRI with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_AGGIORNA = "S" AND !EMPTY( this.w_DATAPRO ) AND !EMPTY( this.w_ORAPRO ) AND !EMPTY( this.w_MINPRO )
      this.w_DATAPRO = DATETIME( YEAR( this.w_DATAPRO ), MONTH( this.w_DATAPRO ), DAY( this.w_DATAPRO ) ,VAL( this.w_ORAPRO ), VAL( this.w_MINPRO ) )
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATPROMEM ="+cp_NullLink(cp_ToStrODBC(this.w_DATAPRO),'OFF_ATTI','ATPROMEM');
            +i_ccchkf ;
        +" where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
               )
      else
        update (i_cTable) set;
            ATPROMEM = this.w_DATAPRO;
            &i_ccchkf. ;
         where;
            ATSERIAL = this.w_ATSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  proc Init(oParentObject,pATSERIAL)
    this.pATSERIAL=pATSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='CAN_TIER'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pATSERIAL"
endproc
