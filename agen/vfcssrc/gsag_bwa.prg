* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bwa                                                        *
*              Gestione agenda                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-18                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bwa",oParentObject,m.pAZIONE)
return(i_retval)

define class tgsag_bwa as StdBatch
  * --- Local variables
  pAZIONE = space(10)
  w_OKPART = .f.
  w_PADRE = .NULL.
  w_Data1 = ctot("")
  w_Data2 = ctot("")
  w_OBJ = .NULL.
  w_RESULT = space(254)
  w_Serial = space(20)
  w_UMCODPER = space(5)
  w_UMCODICE = space(20)
  w_NumRec = 0
  w_MaxRowOrd = 0
  * --- WorkFile variables
  CAU_UTEN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Wizard caricamento attivit� e gestione tariffe utente
    this.w_OKPART = .F.
    this.w_PADRE = This.oparentobject
    do case
      case this.pAzione=="PREV"
        * --- Pagina precedente
        this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP - 1
      case this.pAzione=="NEXT"
        * --- Pagina successiva
        this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP + 1
      case this.pAzione=="INS_ATT"
        * --- Inserimento attivit�
        this.oParentObject.w_RESCHK = 0
        * --- Non utilizziamo come oggetto "ROUTINE" per evitare che il primo parametro passato sia "this"
        if EMPTY(this.oParentObject.w_ATCAUATT)
          ah_ErrorMsg("Attenzione, � necessario specificare un tipo attivit�!")
          i_retcode = 'stop'
          return
        endif
        GSAG_BCO(this.w_PADRE, "W")
        if this.oParentObject.w_RESCHK<>0
          * --- C'� stato un errore in GSAG_BCO
          i_retcode = 'stop'
          return
        endif
        this.w_OBJ = CREATEOBJECT("ActivityWriter",this,"OFF_ATTI")
        * --- Disabilito l'invio di messaggi
        this.w_OBJ.bSendMSG = .f.
        this.w_OBJ.w_ATCAUATT = this.oParentObject.w_ATCAUATT
        this.w_OBJ.w_ATOGGETT = this.oParentObject.w_ATOGGETT
        this.w_OBJ.w_DATINI = this.oParentObject.w_DATINI
        this.w_OBJ.w_ORAINI = this.oParentObject.w_OREINI
        this.w_OBJ.w_MININI = this.oParentObject.w_MININI
        this.w_OBJ.w_DATFIN = this.oParentObject.w_DATFIN
        this.w_OBJ.w_ORAFIN = this.oParentObject.w_OREFIN
        this.w_OBJ.w_MINFIN = this.oParentObject.w_MINFIN
        this.w_OBJ.w_ATCODNOM = this.oParentObject.w_ATCODNOM
        this.w_OBJ.w_ATCODPRA = this.oParentObject.w_ATCODPRA
        this.w_OBJ.w_ATNUMPRI = this.oParentObject.w_ATNUMPRI
        this.w_OBJ.w_AT__ENTE = this.oParentObject.w_AT__ENTE
        this.w_OBJ.w_ATUFFICI = this.oParentObject.w_ATUFFICI
        this.w_OBJ.w_ATLOCALI = this.oParentObject.w_ATLOCALI
        this.w_OBJ.w_ATSTATUS = this.oParentObject.w_ATSTATUS
        this.w_OBJ.w_ATCONTAT = this.oParentObject.w_ATCONTAT
        this.w_OBJ.w_ATPERSON = this.oParentObject.w_ATPERSON
        this.w_OBJ.w_ATTELEFO = this.oParentObject.w_ATTELEFO
        this.w_OBJ.w_ATCELLUL = this.oParentObject.w_ATCELLUL
        this.w_OBJ.w_AT_EMAIL = this.oParentObject.w_AT_EMAIL
        this.w_OBJ.w_AT_EMPEC = this.oParentObject.w_AT_EMPEC
        this.w_OBJ.w_ATGGPREA = this.oParentObject.w_ATGGPREA
        this.w_OBJ.w_ATPROMEM = this.oParentObject.w_ATPROMEM
        this.w_OBJ.w_ATFLPROM = this.oParentObject.w_ATFLPROM
        this.w_OBJ.w_ATPERCOM = this.oParentObject.w_ATPERCOM
        this.w_OBJ.w_ATFLNOTI = this.oParentObject.w_ATFLNOTI
        this.w_OBJ.w_ATSTAATT = this.oParentObject.w_ATSTAATT
        this.w_OBJ.w_ATCODATT = this.oParentObject.w_ATCODATT
        this.w_OBJ.w_ATCENCOS = this.oParentObject.w_ATCENCOS
        this.w_OBJ.w_ATPUBWEB = this.oParentObject.w_ATPUBWEB
        this.w_OBJ.w_ATTIPRIS = NULL
        this.w_OBJ.w_ATNOTPIA = this.oParentObject.w_ATNOTPIA
        this.w_OBJ.w_ATFLATRI = this.oParentObject.w_ATFLATRI
        this.w_OBJ.w_ATCODLIS = this.oParentObject.w_ATCODLIS
        this.w_OBJ.w_ATCODVAL = this.oParentObject.w_ATCODVAL
        this.w_OBJ.w_UTCC = i_CodUte
        this.w_OBJ.w_UTDC = SetInfoDate( g_CALUTD )
        * --- Inseriamo in un cursore i partecipanti selezionati
        SELECT * FROM (this.oParentObject.w_ZOOMPART.ccursor) WHERE XCHK=1 INTO CURSOR TMP_PART 
        if Used("TMP_PART")
          SELECT TMP_PART
          SCAN
          this.w_OBJ.AddPart(0,0,DPTIPRIS,DPCODICE,DPGRUPRE)     
          this.w_OBJ.w_OFF_PART.SaveCurrentRecord()     
          this.w_OKPART = .T.
          ENDSCAN
          USE IN SELECT("TMP_PART")
        endif
        if ! this.w_OKPART
          Ah_errormsg("Attenzione, nessun partecipante selezionato")
          this.w_OBJ = .Null.
          i_retcode = 'stop'
          return
        endif
        this.w_RESULT = this.w_OBJ.CreateActivity()
        if EMPTY(this.w_RESULT)
          * --- Creazione attivit� riuscita
          this.w_Serial = this.w_OBJ.w_ATSERIAL
        else
          ah_ErrorMsg("%1","","",Alltrim(this.w_RESULT))
        endif
        this.w_OBJ = .Null.
      case this.pAzione=="CAU_UTE"
        * --- Inserisce gli utenti nelle causali
        SELECT * FROM (this.oParentObject.w_ZOOM_UTE.ccursor) WHERE XCHK=1 INTO CURSOR TMP_PART 
        if !Used("TMP_PART") OR RECCOUNT()=0
          * --- Nessun partecipante selezionato
          Ah_errormsg("Attenzione, nessun partecipante selezionato")
          USE IN SELECT("TMP_PART")
          i_retcode = 'stop'
          return
        endif
        SELECT * FROM (this.oParentObject.w_ZOOM_CAU.ccursor) WHERE XCHK=1 INTO CURSOR TMP_CAU
        if !Used("TMP_CAU") OR RECCOUNT()=0
          * --- Nessun partecipante selezionato
          Ah_errormsg("Attenzione, nessuna tipologia attivit� selezionata")
          USE IN SELECT("TMP_PART")
          USE IN SELECT("TMP_CAU")
          i_retcode = 'stop'
          return
        endif
        * --- Se siamo qui, significa che ci sono sia partecipanti che tipi attivit�
        if this.oParentObject.w_FL_DELETE="S"
          * --- Se � attivo il flag, deve eliminare le tariffe precedentemente inserite per l'utente
          SELECT TMP_PART
          SCAN
          this.w_UMCODPER = DPCODICE
          * --- Delete from CAU_UTEN
          i_nConn=i_TableProp[this.CAU_UTEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAU_UTEN_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"UMCODPER = "+cp_ToStrODBC(this.w_UMCODPER);
                   )
          else
            delete from (i_cTable) where;
                  UMCODPER = this.w_UMCODPER;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          ENDSCAN
        endif
        * --- Adesso scandiamo le tariffe: per ciascuna di essere preleviamo il max del CPROWNUM e procediamo con l'inserimento
        SELECT TMP_CAU
        SCAN
        this.w_UMCODICE = CACODICE
        * --- Leggiamo il max CPROWNUM della tabella
        this.w_MaxRowOrd = 0
        * --- Select from CAU_UTEN
        i_nConn=i_TableProp[this.CAU_UTEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAU_UTEN_idx,2],.t.,this.CAU_UTEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MAX(CPROWORD) AS MAXROWORD  from "+i_cTable+" CAU_UTEN ";
              +" where UMCODICE="+cp_ToStrODBC(this.w_UMCODICE)+"";
               ,"_Curs_CAU_UTEN")
        else
          select MAX(CPROWORD) AS MAXROWORD from (i_cTable);
           where UMCODICE=this.w_UMCODICE;
            into cursor _Curs_CAU_UTEN
        endif
        if used('_Curs_CAU_UTEN')
          select _Curs_CAU_UTEN
          locate for 1=1
          do while not(eof())
          this.w_MaxRowOrd = NVL(MAXROWORD,0)
            select _Curs_CAU_UTEN
            continue
          enddo
          use
        endif
        * --- Adesso scandiamo gli utenti e li carichiamo
        SELECT TMP_PART
        SCAN
        this.w_UMCODPER = DPCODICE
        this.w_MaxRowOrd = this.w_MaxRowOrd+10
        * --- Try
        local bErr_03A13788
        bErr_03A13788=bTrsErr
        this.Try_03A13788()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Non fa nulla, il record era gi� presente. Deve decurtare il cproword in modo da non lasciare buchi
          this.w_MaxRowOrd = this.w_MaxRowOrd-10
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03A13788
        * --- End
        ENDSCAN
        SELECT TMP_CAU
        * --- Torniamo al record di partenza
        ENDSCAN
        USE IN SELECT("TMP_PART")
        USE IN SELECT("TMP_CAU")
      case this.pAzione=="VARIATA_PRAT"
        * --- E' stata cambiata la pratica
        if this.oParentObject.w_PATIPRIS="X"
          * --- Visualizza le risorse della pratica ed � stata variata la pratica
          this.w_PADRE.NotifyEvent("Aggiorna")     
        endif
      case this.pAzione=="SELEZIONA" OR this.pAzione=="DESELEZIONA" OR this.pAzione=="INVSELEZ"
        * --- Seleziona/deseleziona tutti i partecipanti
        CURSORE = this.oParentObject.w_ZOOMPART.cCursor
        UPDATE (CURSORE) SET XCHK = ICASE(this.pAzione="SELEZIONA", 1, this.pAzione="DESELEZIONA", 0, IIF(XCHK=1, 0, 1))
      case this.pAzione=="INIT_PAGE_FRAME"
        * --- All'init page frame imposta il focus sul campo ATCAUATT
        this.w_OBJ = this.w_PADRE.Getctrl("w_ATCAUATT")
        this.w_OBJ.Setfocus()     
    endcase
    if this.pAzione=="PREV" OR this.pAzione=="NEXT"
      * --- Cambia pagina
      this.w_PADRE.oPGFRM.ActivePage=this.oParentObject.w_CUR_STEP
    endif
    if this.pAzione=="INS_ATT" OR this.pAzione=="CAU_UTE"
      * --- Ha portato a termine l'inserimento
      if !AH_YESNO( "Operazione completata%0Procedere con un nuovo inserimento?")
        this.w_PADRE.EcpQuit()     
      else
        this.w_PADRE.BlankRec()     
        this.w_PADRE.oPGFRM.ActivePage=1
      endif
    endif
  endproc
  proc Try_03A13788()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Tenta di inserire il record in CAU_UTE
    * --- Insert into CAU_UTEN
    i_nConn=i_TableProp[this.CAU_UTEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_UTEN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_UTEN_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"UMCODICE"+",UMCODPER"+",CPROWORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_UMCODICE),'CAU_UTEN','UMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UMCODPER),'CAU_UTEN','UMCODPER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MaxRowOrd),'CAU_UTEN','CPROWORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'UMCODICE',this.w_UMCODICE,'UMCODPER',this.w_UMCODPER,'CPROWORD',this.w_MaxRowOrd)
      insert into (i_cTable) (UMCODICE,UMCODPER,CPROWORD &i_ccchkf. );
         values (;
           this.w_UMCODICE;
           ,this.w_UMCODPER;
           ,this.w_MaxRowOrd;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAZIONE)
    this.pAZIONE=pAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAU_UTEN'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CAU_UTEN')
      use in _Curs_CAU_UTEN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIONE"
endproc
