* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kca                                                        *
*              Caricamento automatico resoconto giornaliero                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-04                                                      *
* Last revis.: 2013-05-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kca",oParentObject))

* --- Class definition
define class tgsag_kca as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 482
  Height = 167
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-22"
  HelpContextID=169203863
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  cPrg = "gsag_kca"
  cComment = "Caricamento automatico resoconto giornaliero"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TipoRisorsa = space(1)
  w_CODPERS = space(5)
  o_CODPERS = space(5)
  w_COGNOME = space(40)
  o_COGNOME = space(40)
  w_NOME = space(40)
  o_NOME = space(40)
  w_DESCRIZ = space(80)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_OB_TEST = ctod('  /  /  ')
  w_STATOATT = space(1)
  w_FLSINGPRE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kcaPag1","gsag_kca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODPERS_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DIPENDEN'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TipoRisorsa=space(1)
      .w_CODPERS=space(5)
      .w_COGNOME=space(40)
      .w_NOME=space(40)
      .w_DESCRIZ=space(80)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_OB_TEST=ctod("  /  /  ")
      .w_STATOATT=space(1)
      .w_FLSINGPRE=space(1)
        .w_TipoRisorsa = 'P'
        .w_CODPERS = ReadDipend(i_codute, "C")
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODPERS))
          .link_1_3('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_DESCRIZ = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .w_DATINI = i_datsys
        .w_DATFIN = i_datsys
        .w_OB_TEST = i_INIDAT
        .w_STATOATT = 'K'
        .w_FLSINGPRE = IIF(ISALT(), 'S', 'N')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CODPERS<>.w_CODPERS.or. .o_COGNOME<>.w_COGNOME.or. .o_NOME<>.w_NOME
            .w_DESCRIZ = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLSINGPRE_1_14.visible=!this.oPgFrm.Page1.oPag.oFLSINGPRE_1_14.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODPERS
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPERS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_CODPERS))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPERS)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPERS) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCODPERS_1_3'),i_cWhere,'GSAR_BDZ',"Persone",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPERS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPERS);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_CODPERS)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPERS = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOME = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODPERS = space(5)
      endif
      this.w_COGNOME = space(40)
      this.w_NOME = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPERS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODPERS_1_3.value==this.w_CODPERS)
      this.oPgFrm.Page1.oPag.oCODPERS_1_3.value=this.w_CODPERS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRIZ_1_6.value==this.w_DESCRIZ)
      this.oPgFrm.Page1.oPag.oDESCRIZ_1_6.value=this.w_DESCRIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_9.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_9.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_10.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_10.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATOATT_1_12.RadioValue()==this.w_STATOATT)
      this.oPgFrm.Page1.oPag.oSTATOATT_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSINGPRE_1_14.RadioValue()==this.w_FLSINGPRE)
      this.oPgFrm.Page1.oPag.oFLSINGPRE_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODPERS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPERS_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODPERS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODPERS = this.w_CODPERS
    this.o_COGNOME = this.w_COGNOME
    this.o_NOME = this.w_NOME
    return

enddefine

* --- Define pages as container
define class tgsag_kcaPag1 as StdContainer
  Width  = 478
  height = 167
  stdWidth  = 478
  stdheight = 167
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODPERS_1_3 as StdField with uid="UDZFBXMUCT",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODPERS", cQueryName = "CODPERS",nZero=5,;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona",;
    HelpContextID = 12217382,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=65, Top=29, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_CODPERS"

  func oCODPERS_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPERS_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPERS_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCODPERS_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oCODPERS_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TipoRisorsa
     i_obj.w_DPCODICE=this.parent.oContained.w_CODPERS
     i_obj.ecpSave()
  endproc

  add object oDESCRIZ_1_6 as StdField with uid="KMZNHYKWWA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCRIZ", cQueryName = "DESCRIZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 142496310,;
   bGlobalFont=.t.,;
    Height=21, Width=337, Left=133, Top=29, InputMask=replicate('X',80)

  add object oDATINI_1_9 as StdField with uid="VAJTUNAFFS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di inizio selezione",;
    HelpContextID = 129737162,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=65, Top=63

  func oDATINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_10 as StdField with uid="VRHGJGBHQK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data di fine selezione",;
    HelpContextID = 51290570,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=164, Top=63

  func oDATFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc


  add object oSTATOATT_1_12 as StdCombo with uid="QNRAJXORLA",rtseq=9,rtrep=.f.,left=349,top=63,width=121,height=21;
    , HelpContextID = 6177402;
    , cFormVar="w_STATOATT",RowSource=""+"Tutte,"+"Evase o completate", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATOATT_1_12.RadioValue()
    return(iif(this.value =1,'K',;
    iif(this.value =2,'B',;
    space(1))))
  endfunc
  func oSTATOATT_1_12.GetRadio()
    this.Parent.oContained.w_STATOATT = this.RadioValue()
    return .t.
  endfunc

  func oSTATOATT_1_12.SetRadio()
    this.Parent.oContained.w_STATOATT=trim(this.Parent.oContained.w_STATOATT)
    this.value = ;
      iif(this.Parent.oContained.w_STATOATT=='K',1,;
      iif(this.Parent.oContained.w_STATOATT=='B',2,;
      0))
  endfunc

  add object oFLSINGPRE_1_14 as StdCheck with uid="YPZNASSILE",rtseq=10,rtrep=.f.,left=65, top=93, caption="Inserisci una riga per ogni prestazione",;
    ToolTipText = "Se attivo, per le attivit� provenienti da inserimento provvisorio inserisce una riga per ogni prestazione con la relativa descrizione.",;
    HelpContextID = 163291656,;
    cFormVar="w_FLSINGPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSINGPRE_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLSINGPRE_1_14.GetRadio()
    this.Parent.oContained.w_FLSINGPRE = this.RadioValue()
    return .t.
  endfunc

  func oFLSINGPRE_1_14.SetRadio()
    this.Parent.oContained.w_FLSINGPRE=trim(this.Parent.oContained.w_FLSINGPRE)
    this.value = ;
      iif(this.Parent.oContained.w_FLSINGPRE=='S',1,;
      0)
  endfunc

  func oFLSINGPRE_1_14.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc


  add object oBtn_1_15 as StdButton with uid="MZNLQBMNEX",left=369, top=116, width=48,height=45,;
    CpPicture="bmp\Controlla.ico", caption="", nPag=1;
    , ToolTipText = "Premere per importare le attivit� presenti in agenda";
    , HelpContextID = 95177350;
    , Caption='\<Importa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSAG_BRE(this.Parent.oContained,"M", .w_DATINI, .w_DATFIN, .w_CODPERS, .w_STATOATT, .w_FLSINGPRE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="TKCMXZXILU",left=422, top=115, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176521286;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="DCOEKCFVVD",Visible=.t., Left=10, Top=30,;
    Alignment=1, Width=51, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="MTWJWPCNSZ",Visible=.t., Left=23, Top=64,;
    Alignment=1, Width=38, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="XIZCGGXJOW",Visible=.t., Left=137, Top=64,;
    Alignment=1, Width=23, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="VMSHXWIZOP",Visible=.t., Left=264, Top=64,;
    Alignment=1, Width=82, Height=18,;
    Caption="Importa attivit�:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kca','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
