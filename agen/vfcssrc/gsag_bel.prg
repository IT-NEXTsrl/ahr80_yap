* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bel                                                        *
*              Elabora attivita da menu                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-27                                                      *
* Last revis.: 2015-07-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFUNZ,pCURSORE,pAUTOCLOSE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bel",oParentObject,m.pFUNZ,m.pCURSORE,m.pAUTOCLOSE)
return(i_retval)

define class tgsag_bel as StdBatch
  * --- Local variables
  pFUNZ = space(1)
  pCURSORE = space(10)
  pAUTOCLOSE = .f.
  w_PADRE = .NULL.
  w_GRAPAT = space(1)
  w_DECTOT = 0
  w_DECUNI = 0
  w_OBJECT = .NULL.
  w_KEYATT = space(0)
  w_CONFERMA = space(1)
  w_ATFLPROM = space(1)
  w_ATPROMEM = ctot("")
  w_DataIniAttivita = ctot("")
  w_DataFinAttivita = ctot("")
  w_CodPraAttivita = space(15)
  w_RETVAL = space(254)
  w_MsgWarning = .f.
  w_Ric_CODATT = space(20)
  w_Ric_QTAMOV = 0
  w_Ric_UNIMIS = space(3)
  w_PrzCalcolato = 0
  w_ATCAUDOC = space(5)
  w_ATCAUACQ = space(5)
  w_FLPRES = space(1)
  w_DataCambiata = .f.
  w_DATINI = ctod("  /  /  ")
  w_ORAINI = space(2)
  w_MININI = space(2)
  w_DATFIN = ctod("  /  /  ")
  w_ORAFIN = space(2)
  w_MINFIN = space(2)
  w_DATINIOR = ctod("  /  /  ")
  w_OREINIOR = space(2)
  w_MININIOR = space(2)
  w_DATFINOR = ctod("  /  /  ")
  w_OREFINOR = space(2)
  w_MINFINOR = space(2)
  w_ATOGGETTOR = space(254)
  w_ATCAUATT = space(20)
  w_ATFLNOTI = space(1)
  w_ATSTATUS = space(1)
  w_ATCAITER = space(20)
  w_ATCODPRA = space(1)
  w_SERIAL = space(20)
  w_DATA_INI = ctot("")
  w_DATA_FIN = ctot("")
  w_CAITER = space(20)
  w_DESC_ATT = space(254)
  w_DES_PRAT = space(100)
  w_STATUS = space(1)
  w_RESCHK = 0
  w_ATFLRICO = space(1)
  w_OGGETTOR = space(254)
  w_ATANNDOC = space(4)
  w_ATALFDOC = space(10)
  w_ATDATFIN = ctod("  /  /  ")
  w_OKSPOSTA = .f.
  w_RETVAL = space(254)
  w_ATRIFMOV = space(10)
  w_ATRIFDOC = space(10)
  w_SERDOC = space(10)
  w_ATNUMDOC = 0
  w_CODSES = space(15)
  w_FLRESTO = .f.
  w_CAUNOT = space(5)
  w_OKGEN = .f.
  w_SCRIVINOTE = space(1)
  w_DATARINV = ctod("  /  /  ")
  w_ORERINV = space(2)
  w_MINRINV = space(2)
  w_Continua = .f.
  Prosegui = .f.
  w_DataFinAttivita = ctot("")
  w_OBJECT = .NULL.
  w_ATFLNOTI = space(1)
  w_InviaMail = .f.
  w_okinv = .f.
  w_CAUDOC = space(5)
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_FLDANA = space(1)
  w_DAVOCCOS = space(15)
  w_DAVOCRIC = space(15)
  w_DACENCOS = space(15)
  w_CPROWORD = 0
  w_DACODCOM = space(15)
  w_DAATTIVI = space(15)
  w_CAUACQ = space(5)
  w_FLACQ = space(1)
  w_FLACOMACQ = space(1)
  w_VOCECR = space(1)
  w_CODPRA = space(15)
  w_ATCAUDOC = space(5)
  w_ATCAUACQ = space(5)
  w_NONSOGGPRE = space(1)
  w_CODNOM = space(15)
  w_CACHKNOM = space(1)
  w_PARAME = space(2)
  w_CAUCON = space(5)
  w_FLCOSE = space(1)
  w_FLACOMAQ = space(1)
  w_NOCODCLI = space(15)
  w_NOTIPO = space(1)
  w_FLINTE = space(1)
  w_OLDPER = 0
  w_OLDDAT = ctod("  /  /  ")
  w_OLDSTAT = space(1)
  w_CAMPO = space(0)
  w_ACQUISTO = space(10)
  w_CODCLI = space(15)
  w_CHKDATAPRE = space(1)
  w_GG_PRE = 0
  w_GG_SUC = 0
  w_ContaPrest = 0
  w_CAUNOT = space(5)
  w_GENPRE = space(1)
  w_CHECKRINV = space(1)
  w_ATTIPRIS = space(1)
  w_CAUATT = space(20)
  w_SERIAL = space(20)
  w_ATCODNOM = space(20)
  w_ATCONTAT = space(5)
  w_ATOPERAT = 0
  w_ATOGGETT = space(254)
  w_ATCODATT = space(5)
  w_ATNOTPIA = space(0)
  w_ATCODESI = space(5)
  w_ATNUMPRI = 0
  w_ATDATINI = ctot("")
  w_ATOPPOFF = space(10)
  w_ATCODPRA = space(15)
  w_ATGGPREA = 0
  w_ATLOCALI = space(30)
  w_AT__ENTE = space(10)
  w_ATUFFICI = space(10)
  w_ATFLATRI = space(1)
  w_ATPERSON = space(60)
  w_ATTELEFO = space(18)
  w_AT___FAX = space(18)
  w_AT_EMAIL = space(0)
  w_AT_EMPEC = space(0)
  w_ATCELLUL = space(18)
  w_ATCODLIS = space(5)
  w_ATCODVAL = space(3)
  w_ATSTAATT = 0
  FL_NON_SOG_PRE = space(1)
  w_Durata = 0
  w_FLNOTI = space(1)
  NUMRIGA = 0
  w_CODRIS = space(5)
  w_PAGRURIS = space(5)
  w_CNFLAPON = space(1)
  w_CNFLVALO = space(1)
  w_CNFLVALO1 = space(1)
  w_CNIMPORT = 0
  w_CNCALDIR = space(1)
  w_CNCOECAL = 0
  w_CNTARTEM = space(1)
  w_CNTARCON = 0
  w_CONTRACN = space(5)
  w_TIPCONCO = space(1)
  w_LISCOLCO = space(5)
  w_STATUSCO = space(1)
  w_PARASS = 0
  w_FLAMPA = space(1)
  COD_PART = space(5)
  w_ENTE = space(10)
  w_TIPOPRAT = space(10)
  w_FLGIUD = space(1)
  w_CNMATOBB = space(1)
  w_CNASSCTP = space(1)
  w_CNCOMPLX = space(1)
  w_CNPROFMT = space(1)
  w_CNESIPOS = space(1)
  w_CNPERPLX = 0
  w_CNPERPOS = 0
  w_CNFLVMLQ = space(1)
  w_NUMDECUNI = 0
  w_Continua = .f.
  w_NUMRIG = 0
  w_CODSER = space(20)
  w_CODART = space(20)
  w_UNIMIS = space(3)
  w_TIPART = space(2)
  w_DESPREST = space(40)
  w_DESSUP = space(0)
  w_COSORA = 0
  FLDEFF = space(1)
  QUANTITA = 0
  PREZZO_UN = 0
  w_TATIPPRE = space(1)
  w_ONORARBI = space(1)
  w_FLPRPE = space(1)
  w_NOTIFICA = space(1)
  w_MANSION = space(5)
  w_TAIMPORT = 0
  PREZZO_MIN = 0
  PREZZO_MAX = 0
  w_CALPRZ = 0
  w_DAGAZUFF = space(6)
  w_ENTE_CAL = space(10)
  w_TAPARAM = space(1)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  CAUMATTI_idx=0
  OFFDATTI_idx=0
  OFF_PART_idx=0
  CAN_TIER_idx=0
  CAU_ATTI_idx=0
  ART_ICOL_idx=0
  DIPENDEN_idx=0
  PRA_ENTI_idx=0
  KEY_ARTI_idx=0
  TIP_DOCU_idx=0
  CDC_MANU_idx=0
  VALUTE_idx=0
  OFF_NOMI_idx=0
  ATT_DCOL_idx=0
  RIS_ESTR_idx=0
  PRA_CONT_idx=0
  PAR_PRAT_idx=0
  LISTINI_idx=0
  CAU_CONT_idx=0
  PAR_ALTE_idx=0
  PAR_AGEN_idx=0
  PRA_TIPI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ELABORA ATTIVITA DA MENU (attivabile dal bottone Elabora in Elenco Attivit�)
    this.pCURSORE = IIF(VARTYPE(this.pCURSORE)<>"C", "", this.pCURSORE)
    this.w_PADRE = this.oParentObject
    * --- Zoom delle Attivit�
    this.w_CONFERMA = "S"
    this.w_RETVAL = ""
    * --- Dichiarazione e inizializzazione w_DataCambiata, necessario per GSAG_BAP
    this.w_DataCambiata = .T.
    do case
      case this.pFUNZ="COMPLETA"
        * --- Rende le attivit� selezionate come "Completate"
        if USED(this.pCURSORE)
          * --- Read from PAR_PRAT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_PRAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_PRAT_idx,2],.t.,this.PAR_PRAT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PACAUDOC,PACAUNOT,PAGENPRE"+;
              " from "+i_cTable+" PAR_PRAT where ";
                  +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PACAUDOC,PACAUNOT,PAGENPRE;
              from (i_cTable) where;
                  PACODAZI = i_codazi;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CAUDOC = NVL(cp_ToDate(_read_.PACAUDOC),cp_NullValue(_read_.PACAUDOC))
            this.w_CAUNOT = NVL(cp_ToDate(_read_.PACAUNOT),cp_NullValue(_read_.PACAUNOT))
            this.w_GENPRE = NVL(cp_ToDate(_read_.PAGENPRE),cp_NullValue(_read_.PAGENPRE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from PAR_ALTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PAGENPRE"+;
              " from "+i_cTable+" PAR_ALTE where ";
                  +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PAGENPRE;
              from (i_cTable) where;
                  PACODAZI = i_codazi;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_GENPRE = NVL(cp_ToDate(_read_.PAGENPRE),cp_NullValue(_read_.PAGENPRE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from PAR_AGEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PAFLPRES"+;
              " from "+i_cTable+" PAR_AGEN where ";
                  +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PAFLPRES;
              from (i_cTable) where;
                  PACODAZI = i_codazi;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLPRES = NVL(cp_ToDate(_read_.PAFLPRES),cp_NullValue(_read_.PAFLPRES))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT (this.pCURSORE)
          GO TOP
          SCAN
          this.w_KEYATT = ATSERIAL
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if !EMPTY(this.w_RETVAL)
            exit
          endif
          SELECT (this.pCURSORE)
          ENDSCAN
        else
          this.w_RETVAL = ah_MsgFormat("Cursore non trovato")
        endif
        i_retcode = 'stop'
        i_retval = this.w_RETVAL
        return
      case this.pFUNZ="SPOSTA"
        * --- Spostamento data iniziale e finale delle attivit� selezionate
        if USED(this.pCURSORE)
          this.w_ATOGGETTOR = IIF(VARTYPE(this.w_PADRE.w_ATOGGETTOR)<>"U", this.w_PADRE.w_ATOGGETTOR, "")
          this.w_ATCAUATT = IIF(VARTYPE(this.w_PADRE.w_ATCAUATT)<>"U", this.w_PADRE.w_ATCAUATT, "")
          this.w_DATINIOR = IIF(VARTYPE(this.w_PADRE.w_DATINIOR)<>"U", this.w_PADRE.w_DATINIOR, cp_CharToDate("  -  -    "))
          this.w_OREINIOR = IIF(VARTYPE(this.w_PADRE.w_OREINIOR)<>"U", this.w_PADRE.w_OREINIOR , "  ")
          this.w_MININIOR = IIF(VARTYPE(this.w_PADRE.w_MININIOR)<>"U", this.w_PADRE.w_MININIOR , "  ")
          this.w_DATFINOR = IIF(VARTYPE(this.w_PADRE.w_DATFINOR)<>"U", this.w_PADRE.w_DATFINOR, cp_CharToDate("  -  -    "))
          this.w_OREFINOR = IIF(VARTYPE(this.w_PADRE.w_OREFINOR)<>"U", this.w_PADRE.w_OREFINOR , "  ")
          this.w_MINFINOR = IIF(VARTYPE(this.w_PADRE.w_MINFINOR)<>"U", this.w_PADRE.w_MINFINOR , "  ")
          this.w_STATUS = IIF(VARTYPE(this.w_PADRE.w_ATSTATUS)<>"U", this.w_PADRE.w_ATSTATUS , "")
          this.w_CONFERMA = "N"
          this.w_DATINI = ctod("  -  -  ")
          this.w_ORAINI = IIF(EMPTY(this.w_OREINIOR), "09", this.w_OREINIOR )
          this.w_MININI = IIF(EMPTY(this.w_MININIOR), "00", this.w_MININIOR)
          this.w_DATFIN = ctod("  -  -  ")
          this.w_ORAFIN = IIF(EMPTY(this.w_OREFINOR), "09", this.w_OREFINOR)
          this.w_MINFIN = IIF(EMPTY(this.w_MINFINOR), "00", this.w_MINFINOR)
          do GSAG_KSP with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_OKGEN = .T.
          if this.w_CONFERMA="S" and Empty(this.w_RETVAL)
            if this.w_RESCHK=0
              SELECT (this.pCURSORE)
              GO TOP
              SCAN
              this.w_RETVAL = ""
              this.w_RESCHK = 0
              this.w_KEYATT = ATSERIAL
              * --- Read from OFF_ATTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ATFLNOTI,ATSTATUS,ATDATINI,ATCAITER,ATCODPRA,ATOGGETT,ATCAUATT,ATDATFIN,ATRIFDOC,ATRIFMOV,ATFLPROM,ATPROMEM,ATFLRICO"+;
                  " from "+i_cTable+" OFF_ATTI where ";
                      +"ATSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ATFLNOTI,ATSTATUS,ATDATINI,ATCAITER,ATCODPRA,ATOGGETT,ATCAUATT,ATDATFIN,ATRIFDOC,ATRIFMOV,ATFLPROM,ATPROMEM,ATFLRICO;
                  from (i_cTable) where;
                      ATSERIAL = this.w_KEYATT;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_ATFLNOTI = NVL(cp_ToDate(_read_.ATFLNOTI),cp_NullValue(_read_.ATFLNOTI))
                this.w_ATSTATUS = NVL(cp_ToDate(_read_.ATSTATUS),cp_NullValue(_read_.ATSTATUS))
                this.w_ATDATINI = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
                this.w_ATCAITER = NVL(cp_ToDate(_read_.ATCAITER),cp_NullValue(_read_.ATCAITER))
                this.w_ATCODPRA = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
                this.w_OGGETTOR = NVL(cp_ToDate(_read_.ATOGGETT),cp_NullValue(_read_.ATOGGETT))
                this.w_ATCAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
                this.w_DataIniAttivita = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
                this.w_DataFinAttivita = NVL((_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
                this.w_CodPraAttivita = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
                this.w_ATRIFDOC = NVL(cp_ToDate(_read_.ATRIFDOC),cp_NullValue(_read_.ATRIFDOC))
                this.w_ATRIFMOV = NVL(cp_ToDate(_read_.ATRIFMOV),cp_NullValue(_read_.ATRIFMOV))
                this.w_ATDATFIN = NVL(cp_ToDate(_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
                this.w_ATFLPROM = NVL(cp_ToDate(_read_.ATFLPROM),cp_NullValue(_read_.ATFLPROM))
                this.w_ATPROMEM = NVL((_read_.ATPROMEM),cp_NullValue(_read_.ATPROMEM))
                this.w_ATFLRICO = NVL(cp_ToDate(_read_.ATFLRICO),cp_NullValue(_read_.ATFLRICO))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_ATFLRICO="S" AND this.w_DATINI<>this.w_DATFIN
                this.w_RETVAL = "Attivit� ricorrente, la data iniziale deve essere uguale dalla data finale"
                this.w_RESCHK = -1
              endif
              if Empty(this.w_RETVAL)
                this.w_DATINIOR = IIF(VARTYPE(this.w_DataIniAttivita)="T",TTOD(this.w_DataIniAttivita),this.w_DataIniAttivita)
                this.w_OREINIOR = right("00"+ALLTRIM(STR(HOUR(this.w_DataIniAttivita),2,0)),2)
                this.w_MININIOR = right("00"+ALLTRIM(STR(MINUTE(this.w_DataIniAttivita),2,0)),2)
                this.w_DATFINOR = IIF(VARTYPE(this.w_DataFinAttivita)="T",TTOD(this.w_DataFinAttivita),this.w_DataFinAttivita)
                this.w_OREFINOR = right("00"+ALLTRIM(STR(HOUR(this.w_DataFinAttivita),2,0)),2)
                this.w_MINFINOR = right("00"+ALLTRIM(STR(MINUTE(this.w_DataFinAttivita),2,0)),2)
                * --- Cicla sul cursore dello zoom relativo alle ATTIVITA'  ed esamina i records selezionati
                * --- Date di inizio e fine ottenute
                this.w_DATA_INI = cp_CharToDatetime(DTOC(this.w_DATINI)+" "+this.w_ORAINI+":"+this.w_MININI+":00")
                this.w_DATA_FIN = cp_CharToDatetime(DTOC(this.w_DATFIN)+" "+this.w_ORAFIN+":"+this.w_MINFIN+":00")
                this.w_ATANNDOC = STR(YEAR(CP_TODATE(this.w_DATFIN)), 4, 0)
                * --- Controllo data iniziale
                if this.oParentObject.w_PACHKCAR<>"N"
                  this.w_RESCHK = ChkDatCal(this.w_KEYATT,This.Oparentobject,this.w_DATA_INI,this.w_DATA_FIN,.F.)
                endif
                if this.w_RESCHK=0 
                  if this.w_DATA_INI>this.w_DATA_FIN
                    * --- Ora inizio minore di ora finale
                    this.w_DATA_FIN = this.w_DATA_INI
                    this.w_DATFIN = this.w_DATINI
                    this.w_ORAFIN = this.w_ORAINI
                    this.w_MINFIN = this.w_MININI
                  endif
                  * --- Nell'attivit� aggiorna le date iniziale e finale.
                  this.w_OKSPOSTA = .t.
                  * --- Try
                  local bErr_030F01F8
                  bErr_030F01F8=bTrsErr
                  this.Try_030F01F8()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- rollback
                    bTrsErr=.t.
                    cp_EndTrs(.t.)
                  endif
                  bTrsErr=bTrsErr or bErr_030F01F8
                  * --- End
                  if ! Isahe() and this.w_OKSPOSTA and Not Empty(this.w_ATRIFMOV) AND this.w_DATFIN<>this.w_ATDATFIN and Empty(this.w_RETVAL)
                    * --- Try
                    local bErr_048C0A90
                    bErr_048C0A90=bTrsErr
                    this.Try_048C0A90()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      this.w_RETVAL = "Errore nell'aggiornamento movimento di analitica associato"
                      this.w_RESCHK = -1
                    endif
                    bTrsErr=bTrsErr or bErr_048C0A90
                    * --- End
                  endif
                  if this.w_RESCHK=0
                    if Empty(this.w_RETVAL)
                      if this.w_ATFLPROM=="S" AND !EMPTY(this.w_ATPROMEM)
                        * --- C'� promemoria, deve essere shiftato dello stesso tempo che intercorre tra il "vecchio" atdatini ed il nuovo ricalcolato
                        this.w_ATPROMEM = this.w_ATPROMEM+(this.w_DATA_INI-this.w_DataIniAttivita)
                      else
                        this.w_ATFLPROM = "N"
                        this.w_ATPROMEM = cp_CharToDateTime("  -  -       :  :  ")
                      endif
                      * --- Try
                      local bErr_030EDFD8
                      bErr_030EDFD8=bTrsErr
                      this.Try_030EDFD8()
                      * --- Catch
                      if !empty(i_Error)
                        i_ErrMsg=i_Error
                        i_Error=''
                        * --- rollback
                        bTrsErr=.t.
                        cp_EndTrs(.t.)
                      endif
                      bTrsErr=bTrsErr or bErr_030EDFD8
                      * --- End
                      if this.w_ATSTATUS= "P" AND this.w_DATFIN<>IIF(VARTYPE(this.w_ATDATFIN)="T",TTOD(this.w_ATDATFIN),this.w_ATDATFIN)
                        * --- Generazione documenti interni dell'attivit�
                        * --- Read from RIS_ESTR
                        i_nOldArea=select()
                        if used('_read_')
                          select _read_
                          use
                        endif
                        i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select "+;
                            "SRCODSES"+;
                            " from "+i_cTable+" RIS_ESTR where ";
                                +"SRCODPRA = "+cp_ToStrODBC(this.w_ATCODPRA);
                                 ,"_read_")
                          i_Rows=iif(used('_read_'),reccount(),0)
                        else
                          select;
                            SRCODSES;
                            from (i_cTable) where;
                                SRCODPRA = this.w_ATCODPRA;
                             into cursor _read_
                          i_Rows=_tally
                        endif
                        if used('_read_')
                          locate for 1=1
                          this.w_CODSES = NVL(cp_ToDate(_read_.SRCODSES),cp_NullValue(_read_.SRCODSES))
                          use
                        else
                          * --- Error: sql sentence error.
                          i_Error = MSG_READ_ERROR
                          return
                        endif
                        select (i_nOldArea)
                        if Isalt() and Not Empty(this.w_CODSES)
                          this.w_FLRESTO = .t.
                          * --- Read from PAR_PRAT
                          i_nOldArea=select()
                          if used('_read_')
                            select _read_
                            use
                          endif
                          i_nConn=i_TableProp[this.PAR_PRAT_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PAR_PRAT_idx,2],.t.,this.PAR_PRAT_idx)
                          if i_nConn<>0
                            cp_sqlexec(i_nConn,"select "+;
                              "PACAUDOC"+;
                              " from "+i_cTable+" PAR_PRAT where ";
                                  +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                                   ,"_read_")
                            i_Rows=iif(used('_read_'),reccount(),0)
                          else
                            select;
                              PACAUDOC;
                              from (i_cTable) where;
                                  PACODAZI = i_codazi;
                               into cursor _read_
                            i_Rows=_tally
                          endif
                          if used('_read_')
                            locate for 1=1
                            this.w_CAUDOC = NVL(cp_ToDate(_read_.PACAUDOC),cp_NullValue(_read_.PACAUDOC))
                            use
                          else
                            * --- Error: sql sentence error.
                            i_Error = MSG_READ_ERROR
                            return
                          endif
                          select (i_nOldArea)
                          * --- Read from TIP_DOCU
                          i_nOldArea=select()
                          if used('_read_')
                            select _read_
                            use
                          endif
                          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
                          if i_nConn<>0
                            cp_sqlexec(i_nConn,"select "+;
                              "TDFLINTE"+;
                              " from "+i_cTable+" TIP_DOCU where ";
                                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
                                   ,"_read_")
                            i_Rows=iif(used('_read_'),reccount(),0)
                          else
                            select;
                              TDFLINTE;
                              from (i_cTable) where;
                                  TDTIPDOC = this.w_CAUDOC;
                               into cursor _read_
                            i_Rows=_tally
                          endif
                          if used('_read_')
                            locate for 1=1
                            this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
                            use
                          else
                            * --- Error: sql sentence error.
                            i_Error = MSG_READ_ERROR
                            return
                          endif
                          select (i_nOldArea)
                          if this.w_FLINTE <> "N" and Not Empty(this.w_ATCODPRA) 
                            * --- Try
                            local bErr_030ECB08
                            bErr_030ECB08=bTrsErr
                            this.Try_030ECB08()
                            * --- Catch
                            if !empty(i_Error)
                              i_ErrMsg=i_Error
                              i_Error=''
                              * --- rollback
                              bTrsErr=.t.
                              cp_EndTrs(.t.)
                            endif
                            bTrsErr=bTrsErr or bErr_030ECB08
                            * --- End
                          else
                            this.w_RETVAL = "Generazione documento non ultimata, attivit� con pratica associata a ripartizione e causale documento senza intestatario"
                            this.w_RESCHK = -1
                          endif
                        else
                          this.w_RETVAL = GSAG_BGE(this, this.w_KEYATT,SPACE(10),Space(15),0,.f.,"D", .F.)
                          if Isalt() and Empty(this.w_RETVAL)
                            * --- Read from PAR_PRAT
                            i_nOldArea=select()
                            if used('_read_')
                              select _read_
                              use
                            endif
                            i_nConn=i_TableProp[this.PAR_PRAT_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PAR_PRAT_idx,2],.t.,this.PAR_PRAT_idx)
                            if i_nConn<>0
                              cp_sqlexec(i_nConn,"select "+;
                                "PACAUNOT"+;
                                " from "+i_cTable+" PAR_PRAT where ";
                                    +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                                     ,"_read_")
                              i_Rows=iif(used('_read_'),reccount(),0)
                            else
                              select;
                                PACAUNOT;
                                from (i_cTable) where;
                                    PACODAZI = i_codazi;
                                 into cursor _read_
                              i_Rows=_tally
                            endif
                            if used('_read_')
                              locate for 1=1
                              this.w_CAUNOT = NVL(cp_ToDate(_read_.PACAUNOT),cp_NullValue(_read_.PACAUNOT))
                              use
                            else
                              * --- Error: sql sentence error.
                              i_Error = MSG_READ_ERROR
                              return
                            endif
                            select (i_nOldArea)
                            if Not Empty(this.w_CAUNOT) and Empty(this.w_RETVAL)
                              * --- Genero documento pre nota spese
                              this.w_RETVAL = GSAG_BGE(this, this.w_KEYATT,SPACE(10),Space(15),0,.f.,"N", .F.)
                            endif
                          endif
                          this.w_RESCHK = IIF(Not Empty(this.w_RETVAL),-1,0)
                        endif
                      endif
                    endif
                    if this.w_RESCHK=0
                      * --- Invio post-it o e-mail di segnalazione modifica
                      if this.w_ATFLNOTI<>"N"
                        GSAG_BAP(this,"M",this.w_KEYATT)
                        if i_retcode='stop' or !empty(i_Error)
                          return
                        endif
                      endif
                      * --- Se l'attivit� � completata e se � stata modificata la relativa data iniziale
                      * --- Se l'attivit� � stata generata da un iter e se � stata modificata la relativa data iniziale
                      if not empty(this.w_ATCAITER) and this.w_DATINI<>IIF(VARTYPE(this.w_ATDATINI)="T",TTOD(this.w_ATDATINI),this.w_ATDATINI) and this.w_ATFLRICO<> "S" and Upper(This.oparentobject.class)="TGSZM_BAT"
                        this.w_SERIAL = this.w_KEYATT
                        this.w_CAITER = this.w_ATCAITER
                        this.w_DESC_ATT = this.w_OGGETTOR
                        * --- Read from CAN_TIER
                        i_nOldArea=select()
                        if used('_read_')
                          select _read_
                          use
                        endif
                        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select "+;
                            "CNDESCAN"+;
                            " from "+i_cTable+" CAN_TIER where ";
                                +"CNCODCAN = "+cp_ToStrODBC(this.w_CodPraAttivita);
                                 ,"_read_")
                          i_Rows=iif(used('_read_'),reccount(),0)
                        else
                          select;
                            CNDESCAN;
                            from (i_cTable) where;
                                CNCODCAN = this.w_CodPraAttivita;
                             into cursor _read_
                          i_Rows=_tally
                        endif
                        if used('_read_')
                          locate for 1=1
                          this.w_DES_PRAT = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
                          use
                        else
                          * --- Error: sql sentence error.
                          i_Error = MSG_READ_ERROR
                          return
                        endif
                        select (i_nOldArea)
                        do GSAG_KAD with this
                        if i_retcode='stop' or !empty(i_Error)
                          return
                        endif
                      endif
                    endif
                  endif
                endif
              endif
              if Not Empty(this.w_RETVAL) 
                * --- Registro problema in spostamento
                this.oParentObject.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Attenzione, impossibile spostare attivit�:%0  %1 del %3 %0 %2", Alltrim(this.w_OGGETTOR),this.w_RETVAL,TTOC(this.w_ATDATINI)))     
                this.w_OKGEN = .F.
              endif
               
 SELECT (this.pCURSORE) 
 ENDSCAN
            endif
          else
            this.oParentObject.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Operazione annullata"))     
            this.w_OKGEN = .F.
          endif
        else
          this.oParentObject.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Cursore non trovato"))     
          this.w_OKGEN = .F.
        endif
        i_retcode = 'stop'
        i_retval = this.w_OKGEN
        return
      case this.pFUNZ="RINVIO"
        * --- RINVIO ATTIVITA' SELEZIONATE
        this.w_SCRIVINOTE = "S"
        this.w_CONFERMA = "N"
        this.w_DATARINV = ctod("  -  -  ")
        this.w_ORERINV = "09"
        this.w_MINRINV = "00"
        if USED(this.pCURSORE)
          do GSAG_KRC with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_CONFERMA="S"
            ah_ErrorMsg("Il completamento delle attivit� da rinviare dovr� essere eseguito successivamente.")
            * --- Cicla sul cursore dello zoom relativo alle ATTIVITA'  ed esamina i records selezionati
            SELECT (this.pCURSORE)
            GO TOP
            SCAN
            this.w_KEYATT = ATSERIAL
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_Durata = this.w_DataFinAttivita-this.w_ATDATINI
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            SELECT (this.pCURSORE)
            ENDSCAN
          else
            this.w_RETVAL = ah_MsgFormat("Operazione annullata")
          endif
        else
          this.w_RETVAL = ah_MsgFormat("Cursore non trovato")
        endif
        i_retcode = 'stop'
        i_retval = this.w_RETVAL
        return
      case this.pFUNZ="ELIMINA" or this.pFUNZ="ELIMSIN" 
        * --- GSAG_BDA
        this.w_okinv = .t.
        this.w_InviaMail = .f.
        if USED(this.pCURSORE)
          SELECT (this.pCURSORE)
          GO TOP
          SCAN
          this.w_KEYATT = ATSERIAL
          if this.pFUNZ="ELIMSIN" 
            * --- Deve modificare cancellare l'attivit�
            * --- Instanzio l'anagrafica
            this.w_OBJECT = GSAG_AAT()
            * --- Controllo se ha passato il test di accesso
            if !(this.w_OBJECT.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- Cerco il record selezionato
            this.w_OBJECT.ECPFILTER()     
            this.w_OBJECT.w_ATSERIAL = this.w_keyatt
            this.w_OBJECT.ECPSAVE()     
            * --- Cancello
            this.w_OBJECT.ECPDELETE()     
            this.w_OBJECT.ECPQUIT()     
            this.w_OBJECT = .null.
          else
            * --- Read from OFF_ATTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ATFLNOTI"+;
                " from "+i_cTable+" OFF_ATTI where ";
                    +"ATSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ATFLNOTI;
                from (i_cTable) where;
                    ATSERIAL = this.w_KEYATT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ATFLNOTI = NVL(cp_ToDate(_read_.ATFLNOTI),cp_NullValue(_read_.ATFLNOTI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_ATFLNOTI<>"N"
              if this.w_okinv
                this.w_InviaMail = Ah_YesNo("Attenzione: nell'elenco  sono presenti attivit� che prevedono l'invio di avvisi. Si desidera procedere all'invio?")
                this.w_okinv = .f.
              endif
              if this.w_InviaMail
                GSAG_BAP(this,"D", this.w_KEYATT,.T.)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
            this.w_RETVAL = GSAG_BDA(this, this.w_KEYATT)
            if !EMPTY(this.w_RETVAL)
              exit
            endif
          endif
          SELECT (this.pCURSORE)
          ENDSCAN
        else
          this.w_RETVAL = ah_MsgFormat("Cursore non trovato")
        endif
        i_retcode = 'stop'
        i_retval = this.w_RETVAL
        return
    endcase
    this.w_PADRE = .Null.
  endproc
  proc Try_030F01F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from ATT_DCOL
    i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2],.t.,this.ATT_DCOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ADSERIAL,ADTIPDOC,ADSERDOC  from "+i_cTable+" ATT_DCOL ";
          +" where ADSERIAL="+cp_ToStrODBC(this.w_KEYATT)+" and (ADTIPDOC='A' or ADTIPDOC='V')";
           ,"_Curs_ATT_DCOL")
    else
      select ADSERIAL,ADTIPDOC,ADSERDOC from (i_cTable);
       where ADSERIAL=this.w_KEYATT and (ADTIPDOC="A" or ADTIPDOC="V");
        into cursor _Curs_ATT_DCOL
    endif
    if used('_Curs_ATT_DCOL')
      select _Curs_ATT_DCOL
      locate for 1=1
      do while not(eof())
      this.w_SERDOC = Nvl(_Curs_ATT_DCOL.ADSERDOC," ")
      this.w_RETVAL = gsar_bdk(.null.,_Curs_ATT_DCOL.ADSERDOC)
      if Not Empty(this.w_RETVAL)
        this.w_RESCHK = -1
        * --- Raise
        i_Error=this.w_RETVAL
        return
      else
        * --- Delete from ATT_DCOL
        i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"ADSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
                +" and ADSERDOC = "+cp_ToStrODBC(this.w_SERDOC);
                 )
        else
          delete from (i_cTable) where;
                ADSERIAL = this.w_KEYATT;
                and ADSERDOC = this.w_SERDOC;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
        select _Curs_ATT_DCOL
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_048C0A90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CDC_MANU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CDC_MANU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CDC_MANU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CDC_MANU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CMDATREG ="+cp_NullLink(cp_ToStrODBC(this.w_ATDATFIN),'CDC_MANU','CMDATREG');
      +",CMDATDOC ="+cp_NullLink(cp_ToStrODBC(this.w_ATDATFIN),'CDC_MANU','CMDATDOC');
          +i_ccchkf ;
      +" where ";
          +"CMCODICE = "+cp_ToStrODBC(this.w_ATRIFMOV);
             )
    else
      update (i_cTable) set;
          CMDATREG = this.w_ATDATFIN;
          ,CMDATDOC = this.w_ATDATFIN;
          &i_ccchkf. ;
       where;
          CMCODICE = this.w_ATRIFMOV;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_030EDFD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_ATNUMDOC = 0
    i_Conn=i_TableProp[this.OFF_ATTI_IDX, 3]
    cp_NextTableProg(this, i_Conn, "NUATT", "i_CODAZI,w_ATANNDOC,w_ATALFDOC,w_ATNUMDOC", .T. )
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_DATA_INI),'OFF_ATTI','ATDATINI');
      +",ATDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_DATA_FIN),'OFF_ATTI','ATDATFIN');
      +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
      +",ATPROMEM ="+cp_NullLink(cp_ToStrODBC(this.w_ATPROMEM),'OFF_ATTI','ATPROMEM');
      +",ATFLPROM ="+cp_NullLink(cp_ToStrODBC(this.w_ATFLPROM),'OFF_ATTI','ATFLPROM');
      +",ATDATDOC ="+cp_NullLink(cp_ToStrODBC(this.w_DATA_FIN),'OFF_ATTI','ATDATDOC');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_ATTI','UTCV');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'OFF_ATTI','UTDV');
      +",ATANNDOC ="+cp_NullLink(cp_ToStrODBC(this.w_ATANNDOC),'OFF_ATTI','ATANNDOC');
      +",ATNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_ATNUMDOC),'OFF_ATTI','ATNUMDOC');
          +i_ccchkf ;
      +" where ";
          +"ATSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
             )
    else
      update (i_cTable) set;
          ATDATINI = this.w_DATA_INI;
          ,ATDATFIN = this.w_DATA_FIN;
          ,ATDATOUT = DATETIME();
          ,ATPROMEM = this.w_ATPROMEM;
          ,ATFLPROM = this.w_ATFLPROM;
          ,ATDATDOC = this.w_DATA_FIN;
          ,UTCV = i_CODUTE;
          ,UTDV = SetInfoDate( g_CALUTD );
          ,ATANNDOC = this.w_ATANNDOC;
          ,ATNUMDOC = this.w_ATNUMDOC;
          &i_ccchkf. ;
       where;
          ATSERIAL = this.w_KEYATT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_030ECB08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from RIS_ESTR
    i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select SRCODPRA,SRCODSES,SRPARAME  from "+i_cTable+" RIS_ESTR ";
          +" where SRCODPRA="+cp_ToStrODBC(this.w_ATCODPRA)+"";
          +" order by SRPARAME Desc";
           ,"_Curs_RIS_ESTR")
    else
      select SRCODPRA,SRCODSES,SRPARAME from (i_cTable);
       where SRCODPRA=this.w_ATCODPRA;
       order by SRPARAME Desc;
        into cursor _Curs_RIS_ESTR
    endif
    if used('_Curs_RIS_ESTR')
      select _Curs_RIS_ESTR
      locate for 1=1
      do while not(eof())
      this.w_RETVAL = GSAG_BGE(.null.,this.w_KEYATT,space(10),_Curs_RIS_ESTR.SRCODSES,_Curs_RIS_ESTR.SRPARAME,this.w_FLRESTO,"R", .F.)
      this.w_FLRESTO = .f.
      if Not Empty(this.w_RETVAL)
        exit
      endif
        select _Curs_RIS_ESTR
        continue
      enddo
      use
    endif
    * --- Genero documento pre nota spese
    if Empty(this.w_RETVAL)
      this.w_RETVAL = GSAG_BGE(this, this.w_KEYATT,SPACE(10),Space(15),0,.f.,"N", .F.)
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_Continua = .T.
    * --- Read from OFF_ATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATCAUATT,ATCODPRA,ATSTATUS,ATDATOUT,ATPERCOM,ATCAUDOC,ATCAUACQ,ATCODNOM"+;
        " from "+i_cTable+" OFF_ATTI where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATCAUATT,ATCODPRA,ATSTATUS,ATDATOUT,ATPERCOM,ATCAUDOC,ATCAUACQ,ATCODNOM;
        from (i_cTable) where;
            ATSERIAL = this.w_KEYATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
      this.w_CODPRA = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
      this.w_OLDSTAT = NVL(cp_ToDate(_read_.ATSTATUS),cp_NullValue(_read_.ATSTATUS))
      this.w_OLDDAT = NVL(cp_ToDate(_read_.ATDATOUT),cp_NullValue(_read_.ATDATOUT))
      this.w_OLDPER = NVL(cp_ToDate(_read_.ATPERCOM),cp_NullValue(_read_.ATPERCOM))
      this.w_ATCAUDOC = NVL(cp_ToDate(_read_.ATCAUDOC),cp_NullValue(_read_.ATCAUDOC))
      this.w_ATCAUACQ = NVL(cp_ToDate(_read_.ATCAUACQ),cp_NullValue(_read_.ATCAUACQ))
      this.w_CODNOM = NVL(cp_ToDate(_read_.ATCODNOM),cp_NullValue(_read_.ATCODNOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAUMATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CAFLNSAP,CACAUDOC,CAFLANAL,CACAUACQ,CACHKNOM"+;
        " from "+i_cTable+" CAUMATTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CAUATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CAFLNSAP,CACAUDOC,CAFLANAL,CACAUACQ,CACHKNOM;
        from (i_cTable) where;
            CACODICE = this.w_CAUATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NONSOGGPRE = NVL(cp_ToDate(_read_.CAFLNSAP),cp_NullValue(_read_.CAFLNSAP))
      this.w_CAUDOC = NVL(cp_ToDate(_read_.CACAUDOC),cp_NullValue(_read_.CACAUDOC))
      this.w_FLANAL = NVL(cp_ToDate(_read_.CAFLANAL),cp_NullValue(_read_.CAFLANAL))
      this.w_CAUACQ = NVL(cp_ToDate(_read_.CACAUACQ),cp_NullValue(_read_.CACAUACQ))
      this.w_CACHKNOM = NVL(cp_ToDate(_read_.CACHKNOM),cp_NullValue(_read_.CACHKNOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDFLINTE,TDCATDOC,TDCAUCON"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDFLINTE,TDCATDOC,TDCAUCON;
        from (i_cTable) where;
            TDTIPDOC = this.w_CAUDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
      this.w_PARAME = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
      this.w_CAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCFLCOSE"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.w_CAUCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCFLCOSE;
        from (i_cTable) where;
            CCCODICE = this.w_CAUCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLCOSE = NVL(cp_ToDate(_read_.CCFLCOSE),cp_NullValue(_read_.CCFLCOSE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CAUDOC = IIF(NOT EMPTY(this.w_ATCAUDOC), this.w_ATCAUDOC,this.w_CAUDOC)
    this.w_CAUACQ = this.w_ATCAUACQ
    this.w_FLGCOM = Docgesana(this.w_CAUDOC,"C")
    this.w_FLDANA = Docgesana(this.w_CAUDOC,"A")
    this.w_FLACQ = Docgesana(this.w_CAUACQ,"A")
    this.w_FLACOMACQ = Docgesana(this.w_ATCAUACQ,"C")
    this.w_VOCECR = Docgesana(this.w_CAUDOC,"V")
    * --- Controllo se tipo attivit� � soggetto a prestazione
    if this.w_NONSOGGPRE = "S"
      this.w_RETVAL = ah_MsgFormat("Operazione non consentita: il tipo dell'attivit� non � soggetto a prestazione.")
      this.w_Continua = .F.
    endif
    if this.w_Continua AND this.w_OLDSTAT<>"P" AND (((this.w_FLINTE <> "N" AND this.w_PARAME<>"RF") OR (this.w_FLCOSE = "S" AND this.w_PARAME="RF") ) AND this.w_CACHKNOM<>"P" AND EMPTY(this.w_CODNOM) and Not Empty(this.w_CAUDOC))
      this.w_RETVAL = ah_MsgFormat("Attenzione: il documento da generare prevede l'intestatario. Occorre indicare un nominativo!")
      this.w_Continua = .F.
    endif
    if !Isalt()
      if this.w_Continua AND Empty(this.w_ATCAUDOC)
        * --- Select from OFFDATTI
        i_nConn=i_TableProp[this.OFFDATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2],.t.,this.OFFDATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select DASERIAL  from "+i_cTable+" OFFDATTI ";
              +" where DASERIAL="+cp_ToStrODBC(this.w_KEYATT)+" AND (DATIPRIG='E' OR DATIPRIG='V') OR (DATIPRI2='E' OR DATIPRI2='V')";
               ,"_Curs_OFFDATTI")
        else
          select DASERIAL from (i_cTable);
           where DASERIAL=this.w_KEYATT AND (DATIPRIG="E" OR DATIPRIG="V") OR (DATIPRI2="E" OR DATIPRI2="V");
            into cursor _Curs_OFFDATTI
        endif
        if used('_Curs_OFFDATTI')
          select _Curs_OFFDATTI
          locate for 1=1
          do while not(eof())
          this.w_CAMPO = DASERIAL
            select _Curs_OFFDATTI
            continue
          enddo
          use
        endif
        if !EMPTY(this.w_CAMPO)
          this.w_RETVAL = ah_MsgFormat("Attenzione per generare il documento di vendita � necessario indicare la causale specifica nei dati di testata.")
          this.w_Continua = .F.
        endif
      endif
      if this.w_Continua AND ((Empty(this.w_ATCAUACQ) AND this.w_FLANAL<>"S") OR this.w_FLANAL="S")
        * --- Select from OFFDATTI
        i_nConn=i_TableProp[this.OFFDATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2],.t.,this.OFFDATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" OFFDATTI ";
              +" where DASERIAL="+cp_ToStrODBC(this.w_KEYATT)+" AND (DATIPRIG='E' OR DATIPRIG='A') ";
               ,"_Curs_OFFDATTI")
        else
          select * from (i_cTable);
           where DASERIAL=this.w_KEYATT AND (DATIPRIG="E" OR DATIPRIG="A") ;
            into cursor _Curs_OFFDATTI
        endif
        if used('_Curs_OFFDATTI')
          select _Curs_OFFDATTI
          locate for 1=1
          do while not(eof())
          this.w_ACQUISTO = DASERIAL
            select _Curs_OFFDATTI
            continue
          enddo
          use
        endif
        if !EMPTY(this.w_ACQUISTO)
          this.w_RETVAL = ah_MsgFormat("Attenzione per generare il documento di acquisto � necessario indicare la causale specifica nei dati di testata.")
          this.w_Continua = .F.
        endif
      endif
    endif
    * --- Controllo pratica
    if this.w_Continua AND (EMPTY(this.w_CODPRA) OR ISNULL(this.w_CODPRA)) AND Isalt()
      this.w_RETVAL = ah_MsgFormat("Impossibile completare l'attivit�: pratica mancante.")
      this.w_Continua = .F.
    endif
    if this.w_Continua
      * --- Read from OFF_NOMI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NOCODCLI,NOTIPNOM"+;
          " from "+i_cTable+" OFF_NOMI where ";
              +"NOCODICE = "+cp_ToStrODBC(this.w_ATCODNOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NOCODCLI,NOTIPNOM;
          from (i_cTable) where;
              NOCODICE = this.w_ATCODNOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NOCODCLI = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
        this.w_NOTIPO = NVL(cp_ToDate(_read_.NOTIPNOM),cp_NullValue(_read_.NOTIPNOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_NOCODCLI = NVL( this.w_NOCODCLI , SPACE( 15 ) )
      if EMPTY( this.w_NOCODCLI ) and this.w_FLINTE <> "N"
        if AH_YESNO( "Il nominativo selezionato non � cliente%0Si desidera trasformarlo?" )
          * --- Write into OFF_NOMI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NOTIPNOM ="+cp_NullLink(cp_ToStrODBC("C"),'OFF_NOMI','NOTIPNOM');
                +i_ccchkf ;
            +" where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_NOCODCLI);
                   )
          else
            update (i_cTable) set;
                NOTIPNOM = "C";
                &i_ccchkf. ;
             where;
                NOCODICE = this.w_NOCODCLI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_CODCLI = GSAR_BO1( THIS , this.w_ATCODNOM , this.w_NOTIPO )
          if empty(this.w_CODCLI)
            this.w_Continua = .F.
            ah_ErrorMsg("Impossibile confermare attivit� con nominativo non trasformato in cliente")
          endif
        else
          this.w_Continua = .F.
          ah_ErrorMsg("Impossibile confermare attivit� con nominativo non trasformato in cliente")
        endif
      endif
    endif
    if this.w_Continua and (g_PERCCR="S" or g_PERCCM="S")
      * --- Controllo presenza dati di analitica\gestione progetti
      * --- Select from gsag_bel
      do vq_exec with 'gsag_bel',this,'_Curs_gsag_bel','',.f.,.t.
      if used('_Curs_gsag_bel')
        select _Curs_gsag_bel
        locate for 1=1
        do while not(eof())
        if this.w_continua
          if !Isahe()
            if g_PERCCR="S" and (_Curs_GSAG_BEL.DATIPRIG $ "A-E" OR (_Curs_GSAG_BEL.DATIPRI2 $ "A-E" and Isalt()) ) or (this.w_FLANAL="S" and Nvl(_Curs_GSAG_BEL.DACOSINT,0)>0)
              do case
                case Empty(Nvl(_Curs_GSAG_BEL.DAVOCCOS,Space(15))) AND ((this.w_FLANAL="S" or (this.w_VOCECR="C" AND (this.w_FLACQ="S" or this.w_FLACOMAQ="S")))) 
                  ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza voce di costo.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                  this.w_Continua = .F.
                case Empty(Nvl(_Curs_GSAG_BEL.DACENCOS,Space(15))) AND ((this.w_FLACQ="S" or this.w_FLANAL="S")) 
                  ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza centro di costo.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                  this.w_Continua = .F.
              endcase
              if this.w_FLACOMAQ="S" or this.w_FLANAL="S" 
                do case
                  case Empty(Nvl(_Curs_GSAG_BEL.DACODCOM,Space(15))) 
                    ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza commessa costi.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                    this.w_Continua = .F.
                  case Empty(Nvl(_Curs_GSAG_BEL.DAATTIVI,Space(15)))
                    ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza attivit� costi.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                    this.w_Continua = .F.
                endcase
              endif
            endif
            do case
              case Empty(Nvl(_Curs_GSAG_BEL.DAVOCRIC,Space(15))) and (this.w_FLDANA="S" or this.w_FLGCOM="S")
                ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza voce di ricavo.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                this.w_Continua = .F.
              case Empty(Nvl(_Curs_GSAG_BEL.DACENRIC,Space(15))) and this.w_FLDANA="S"
                ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza centro di ricavo.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                this.w_Continua = .F.
            endcase
          else
            if g_PERCCM="S" AND !(w_BUFLANAL<>"S" and not empty(g_CODBUN))
              if (this.w_FLACQ $ "S-I" and _Curs_GSAG_BEL.DATIPRIG $ "A-E") 
                do case
                  case Empty(Nvl(_Curs_GSAG_BEL.DAVOCCOS,Space(15))) 
                    ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza voce di costo.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                    this.w_Continua = .F.
                  case Empty(Nvl(_Curs_GSAG_BEL.DACENCOS,Space(15)))
                    ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza centro di costo.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                    this.w_Continua = .F.
                endcase
              endif
              if (_Curs_GSAG_BEL.DATIPRIG $ "A-E" and this.w_FLACOMAQ<> "N") 
                do case
                  case Empty(Nvl(_Curs_GSAG_BEL.DACODCOM,Space(15))) 
                    ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza commessa costi.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                    this.w_Continua = .F.
                  case Empty(Nvl(_Curs_GSAG_BEL.DAATTIVI,Space(15)))
                    ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza attivit� costi.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                    this.w_Continua = .F.
                endcase
              endif
              if g_CACQ="S" and Not Empty(this.w_ATCAUACQ) and (_Curs_GSAG_BEL.DATIPRIG $ "D-E" OR _Curs_GSAG_BEL.DATIPRI2 $ "D-E") and this.w_RESCHK <> -1 AND this.w_FLDANA $ "S-I" 
                * --- Controllo dati analitici acquisti
                do case
                  case Empty(Nvl(_Curs_GSAG_BEL.DAVOCRIC,Space(15))) 
                    ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza voce di ricavo.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                    this.w_Continua = .F.
                  case Empty(Nvl(_Curs_GSAG_BEL.DACENRIC,Space(15))) 
                    ah_ErrorMsg("Impossibile completare l'attivit�:%0riga %1 senza centro di ricavo.","!","", Alltrim(STR(_Curs_GSAG_BEL.CPROWORD)))
                    this.w_Continua = .F.
                endcase
              endif
            endif
          endif
        endif
        if Not this.w_Continua
          exit
        endif
          select _Curs_gsag_bel
          continue
        enddo
        use
      endif
    endif
    if this.w_Continua
      * --- Controllo presenza prestazioni e relativo responsabile
      * --- Select from OFFDATTI
      i_nConn=i_TableProp[this.OFFDATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2],.t.,this.OFFDATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" OFFDATTI ";
            +" where DASERIAL="+cp_ToStrODBC(this.w_KEYATT)+"";
             ,"_Curs_OFFDATTI")
      else
        select * from (i_cTable);
         where DASERIAL=this.w_KEYATT;
          into cursor _Curs_OFFDATTI
      endif
      if used('_Curs_OFFDATTI')
        select _Curs_OFFDATTI
        locate for 1=1
        do while not(eof())
        if this.w_Continua
          if EMPTY(_Curs_OFFDATTI.DACODRES) OR ISNULL(_Curs_OFFDATTI.DACODRES)
            this.w_RETVAL = ah_MsgFormat("Impossibile completare l'attivit�: tutte le prestazioni devono avere responsabile.")
            this.w_Continua = .F.
          endif
          * --- Se � attivo il check Controllo su ins./variaz. prestazioni nei Parametri attivit� e se l'utente non � amministratore
          if this.w_Continua AND IsAlt() AND this.w_FLPRES="S" AND NOT Cp_IsAdministrator()
            * --- Read from DIPENDEN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DIPENDEN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DPCTRPRE,DPGG_PRE,DPGG_SUC"+;
                " from "+i_cTable+" DIPENDEN where ";
                    +"DPCODICE = "+cp_ToStrODBC(_Curs_OFFDATTI.DACODRES);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DPCTRPRE,DPGG_PRE,DPGG_SUC;
                from (i_cTable) where;
                    DPCODICE = _Curs_OFFDATTI.DACODRES;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CHKDATAPRE = NVL(cp_ToDate(_read_.DPCTRPRE),cp_NullValue(_read_.DPCTRPRE))
              this.w_GG_PRE = NVL(cp_ToDate(_read_.DPGG_PRE),cp_NullValue(_read_.DPGG_PRE))
              this.w_GG_SUC = NVL(cp_ToDate(_read_.DPGG_SUC),cp_NullValue(_read_.DPGG_SUC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Se attivo il check Controllo su prestazioni (nelle Persone) e se la data della prestazione � al di fuori dell'intervallo definito per il relativo responsabile
            if this.w_CHKDATAPRE="S" AND (TTOD(this.w_ATDATINI) < date() - this.w_GG_PRE OR TTOD(this.w_ATDATINI) > date() + this.w_GG_SUC)
              this.w_RETVAL = ah_MsgFormat("Data non consentita dai controlli relativi al responsabile.")
              this.w_Continua = .F.
            endif
          endif
        endif
          select _Curs_OFFDATTI
          continue
        enddo
        use
      endif
      if this.w_Continua
        this.w_MsgWarning = .F.
        if IsAhr()
          * --- Per AHR deve confrontare con il prezzo ricalcolato
          * --- Select from ..\agen\exe\query\gsag1bel.vqr
          do vq_exec with '..\agen\exe\query\gsag1bel.vqr',this,'_Curs__d__d__agen_exe_query_gsag1bel_d_vqr','',.f.,.t.
          if used('_Curs__d__d__agen_exe_query_gsag1bel_d_vqr')
            select _Curs__d__d__agen_exe_query_gsag1bel_d_vqr
            locate for 1=1
            do while not(eof())
            this.w_Ric_CODATT = DACODATT
            this.w_Ric_QTAMOV = DAQTAMOV
            this.w_Ric_UNIMIS = DAUNIMIS
            this.w_PrzCalcolato = GSAR_BPZ(this,this.w_Ric_CODATT,this.w_NOCODCLI, "C", this.w_ATCODLIS, this.w_Ric_QTAMOV, this.w_ATCODVAL, this.w_ATDATINI, this.w_Ric_UNIMIS)
            if this.w_PrzCalcolato=0
              if !AH_YESNO( "Attenzione! Nel dettaglio prestazioni sono presenti servizi di tipo 'a valore' che avranno prezzo a zero sul documento generato! Prosegui ugualmente?" )
                this.w_RETVAL = ah_MsgFormat("Impossibile completare l'attivit�: dettaglio prestazioni con servizi che avranno prezzo zero.")
                this.w_Continua = .F.
              endif
            endif
              select _Curs__d__d__agen_exe_query_gsag1bel_d_vqr
              continue
            enddo
            use
          endif
        else
          * --- Write into OFFDATTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.OFFDATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="DASERIAL,CPROWNUM"
            do vq_exec with '..\agen\exe\query\gsag1bel',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OFFDATTI_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
                    +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DATIPRIG ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRIG');
            +",DATIPRI2 ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRI2');
                +i_ccchkf;
                +" from "+i_cTable+" OFFDATTI, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
                    +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFFDATTI, "+i_cQueryTable+" _t2 set ";
            +"OFFDATTI.DATIPRIG ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRIG');
            +",OFFDATTI.DATIPRI2 ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRI2');
                +Iif(Empty(i_ccchkf),"",",OFFDATTI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="OFFDATTI.DASERIAL = t2.DASERIAL";
                    +" and "+"OFFDATTI.CPROWNUM = t2.CPROWNUM";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFFDATTI set (";
                +"DATIPRIG,";
                +"DATIPRI2";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRIG')+",";
                +cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRI2')+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="OFFDATTI.DASERIAL = _t2.DASERIAL";
                    +" and "+"OFFDATTI.CPROWNUM = _t2.CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" OFFDATTI set ";
            +"DATIPRIG ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRIG');
            +",DATIPRI2 ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRI2');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".DASERIAL = "+i_cQueryTable+".DASERIAL";
                    +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DATIPRIG ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRIG');
            +",DATIPRI2 ="+cp_NullLink(cp_ToStrODBC("N"),'OFFDATTI','DATIPRI2');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        this.w_ContaPrest = 0
        * --- Select from OFFDATTI
        i_nConn=i_TableProp[this.OFFDATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2],.t.,this.OFFDATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" OFFDATTI ";
              +" where DASERIAL="+cp_ToStrODBC(this.w_KEYATT)+"";
               ,"_Curs_OFFDATTI")
        else
          select COUNT(*) AS CONTA from (i_cTable);
           where DASERIAL=this.w_KEYATT;
            into cursor _Curs_OFFDATTI
        endif
        if used('_Curs_OFFDATTI')
          select _Curs_OFFDATTI
          locate for 1=1
          do while not(eof())
          this.w_ContaPrest = NVL(_Curs_OFFDATTI.CONTA, 0)
            select _Curs_OFFDATTI
            continue
          enddo
          use
        endif
        if this.w_ContaPrest = 0
          this.w_RETVAL = ah_MsgFormat("Impossibile completare l'attivit�: deve essere presente almeno una prestazione.")
          this.w_Continua = .F.
        endif
      endif
    endif
    if this.w_Continua
      * --- begin transaction
      cp_BeginTrs()
      * --- Try
      local bErr_049272F0
      bErr_049272F0=bTrsErr
      this.Try_049272F0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        if EMPTY(this.w_RETVAL)
          this.w_RETVAL = ah_MsgFormat("Errore aggiornamento attivit�%0%1","","",this.w_RETVAL)
        else
          this.w_RETVAL = ah_MsgFormat("Errore aggiornamento attivit�%0%1",this.w_RETVAL)
        endif
        * --- Write into OFF_ATTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ATSTATUS ="+cp_NullLink(cp_ToStrODBC(this.w_OLDSTAT),'OFF_ATTI','ATSTATUS');
          +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(this.w_OLDDAT),'OFF_ATTI','ATDATOUT');
          +",ATPERCOM ="+cp_NullLink(cp_ToStrODBC(this.w_OLDPER),'OFF_ATTI','ATPERCOM');
          +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_ATTI','UTCV');
          +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'OFF_ATTI','UTDV');
              +i_ccchkf ;
          +" where ";
              +"ATSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
                 )
        else
          update (i_cTable) set;
              ATSTATUS = this.w_OLDSTAT;
              ,ATDATOUT = this.w_OLDDAT;
              ,ATPERCOM = this.w_OLDPER;
              ,UTCV = i_CODUTE;
              ,UTDV = SetInfoDate( g_CALUTD );
              &i_ccchkf. ;
           where;
              ATSERIAL = this.w_KEYATT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_049272F0
      * --- End
    endif
  endproc
  proc Try_049272F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Nell'attivit� pone lo stato come 'completata'.
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATSTATUS ="+cp_NullLink(cp_ToStrODBC("P"),'OFF_ATTI','ATSTATUS');
      +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
      +",ATPERCOM ="+cp_NullLink(cp_ToStrODBC(100),'OFF_ATTI','ATPERCOM');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_ATTI','UTCV');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'OFF_ATTI','UTDV');
          +i_ccchkf ;
      +" where ";
          +"ATSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
             )
    else
      update (i_cTable) set;
          ATSTATUS = "P";
          ,ATDATOUT = DATETIME();
          ,ATPERCOM = 100;
          ,UTCV = i_CODUTE;
          ,UTDV = SetInfoDate( g_CALUTD );
          &i_ccchkf. ;
       where;
          ATSERIAL = this.w_KEYATT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_GENPRE="S"
      this.w_RETVAL = Alltrim(GSAG_BGE(this, this.w_KEYATT,SPACE(10),Space(15),0,.f.,"D", .T.))
    else
      * --- Generazione documenti interni dell'attivit�
      * --- Read from RIS_ESTR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SRCODSES"+;
          " from "+i_cTable+" RIS_ESTR where ";
              +"SRCODPRA = "+cp_ToStrODBC(this.w_ATCODPRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SRCODSES;
          from (i_cTable) where;
              SRCODPRA = this.w_ATCODPRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODSES = NVL(cp_ToDate(_read_.SRCODSES),cp_NullValue(_read_.SRCODSES))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Isalt() and Not Empty(this.w_CODSES)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLINTE"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLINTE;
            from (i_cTable) where;
                TDTIPDOC = this.w_CAUDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_FLINTE <> "N" and Not Empty(this.w_CODPRA) 
          * --- Select from RIS_ESTR
          i_nConn=i_TableProp[this.RIS_ESTR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTR_idx,2],.t.,this.RIS_ESTR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SRCODPRA,SRCODSES,SRPARAME  from "+i_cTable+" RIS_ESTR ";
                +" where SRCODPRA="+cp_ToStrODBC(this.w_ATCODPRA)+"";
                +" order by SRPARAME Desc";
                 ,"_Curs_RIS_ESTR")
          else
            select SRCODPRA,SRCODSES,SRPARAME from (i_cTable);
             where SRCODPRA=this.w_ATCODPRA;
             order by SRPARAME Desc;
              into cursor _Curs_RIS_ESTR
          endif
          if used('_Curs_RIS_ESTR')
            select _Curs_RIS_ESTR
            locate for 1=1
            do while not(eof())
            this.w_RETVAL = GSAG_BGE(.null.,this.w_KEYATT,space(10),_Curs_RIS_ESTR.SRCODSES,_Curs_RIS_ESTR.SRPARAME,this.w_FLRESTO,"R", .F.)
            this.w_FLRESTO = .f.
            if Not Empty(this.w_RETVAL)
              exit
            endif
              select _Curs_RIS_ESTR
              continue
            enddo
            use
          endif
          if Empty(this.w_RETVAL)
            this.w_RETVAL = Alltrim(GSAG_BGE(this, this.w_KEYATT,SPACE(10),Space(15),0,.f.,"N", .F.))
          endif
        else
          this.w_RETVAL = "generazione documento non ultimata, attivit� con pratica associata a ripartizione e causale documento senza intestatario"
        endif
      else
        this.w_RETVAL = Alltrim(GSAG_BGE(this, this.w_KEYATT,SPACE(10),Space(15),0,.f.,"D", .F.))
        if Isalt()
          * --- Read from PAR_PRAT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_PRAT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_PRAT_idx,2],.t.,this.PAR_PRAT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PACAUNOT"+;
              " from "+i_cTable+" PAR_PRAT where ";
                  +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PACAUNOT;
              from (i_cTable) where;
                  PACODAZI = i_codazi;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CAUNOT = NVL(cp_ToDate(_read_.PACAUNOT),cp_NullValue(_read_.PACAUNOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Not Empty(this.w_CAUNOT) and Empty(this.w_RETVAL)
            * --- Genero documento pre nota spese
            this.w_RETVAL = Alltrim(GSAG_BGE(this, this.w_KEYATT,SPACE(10),Space(15),0,.f.,"N", .F.))
          endif
        endif
      endif
    endif
    if EMPTY(this.w_RETVAL) and !bTrsErr
      * --- commit
      cp_EndTrs(.t.)
    else
      * --- Raise
      i_Error=this.w_RETVAL
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.FL_NON_SOG_PRE = space(1)
    this.w_ATSTAATT = 0
    this.w_ATCODVAL = space(3)
    this.w_ATCODLIS = space(5)
    this.w_ATCELLUL = space(18)
    this.w_AT_EMAIL = space(0)
    this.w_AT_EMPEC = space(0)
    this.w_AT___FAX = space(18)
    this.w_ATTELEFO = space(18)
    this.w_ATPERSON = space(60)
    this.w_ATFLATRI = space(1)
    this.w_ATUFFICI = space(10)
    this.w_AT__ENTE = space(10)
    this.w_ATLOCALI = space(30)
    this.w_ATPROMEM = cp_CharToDatetime("  -  -       :  :  ")
    this.w_ATGGPREA = 0
    this.w_ATCODPRA = space(15)
    this.w_ATOPPOFF = space(10)
    this.w_ATDATINI = cp_CharToDatetime("  -  -       :  :  ")
    this.w_ATNUMPRI = 0
    this.w_ATCODESI = space(5)
    this.w_SERIAL = space(20)
    this.w_CHECKRINV = space(1)
    this.w_ATTIPRIS = space(1)
    this.w_CAUATT = space(20)
    this.w_ATCODNOM = space(20)
    this.w_ATCONTAT = space(5)
    this.w_ATOPERAT = 0
    this.w_ATOGGETT = space(254)
    this.w_ATCODATT = space(5)
    this.w_ATNOTPIA = space(0)
    * --- Legge i campi dell'attivit� da rinviare
    * --- Read from OFF_ATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" OFF_ATTI where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            ATSERIAL = this.w_KEYATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
      this.w_ATTIPRIS = NVL(cp_ToDate(_read_.ATTIPRIS),cp_NullValue(_read_.ATTIPRIS))
      this.w_ATCODNOM = NVL(cp_ToDate(_read_.ATCODNOM),cp_NullValue(_read_.ATCODNOM))
      this.w_ATCONTAT = NVL(cp_ToDate(_read_.ATCONTAT),cp_NullValue(_read_.ATCONTAT))
      this.w_ATOPERAT = NVL(cp_ToDate(_read_.ATOPERAT),cp_NullValue(_read_.ATOPERAT))
      this.w_ATOGGETT = NVL(cp_ToDate(_read_.ATOGGETT),cp_NullValue(_read_.ATOGGETT))
      this.w_ATCODATT = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
      this.w_ATNOTPIA = NVL(cp_ToDate(_read_.ATNOTPIA),cp_NullValue(_read_.ATNOTPIA))
      this.w_ATCODESI = NVL(cp_ToDate(_read_.ATCODESI),cp_NullValue(_read_.ATCODESI))
      this.w_ATNUMPRI = NVL(cp_ToDate(_read_.ATNUMPRI),cp_NullValue(_read_.ATNUMPRI))
      this.w_ATDATINI = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
      this.w_ATOPPOFF = NVL(cp_ToDate(_read_.ATOPPOFF),cp_NullValue(_read_.ATOPPOFF))
      this.w_ATCODPRA = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
      this.w_ATGGPREA = NVL(cp_ToDate(_read_.ATGGPREA),cp_NullValue(_read_.ATGGPREA))
      this.w_ATPROMEM = NVL((_read_.ATPROMEM),cp_NullValue(_read_.ATPROMEM))
      this.w_ATLOCALI = NVL(cp_ToDate(_read_.ATLOCALI),cp_NullValue(_read_.ATLOCALI))
      this.w_AT__ENTE = NVL(cp_ToDate(_read_.AT__ENTE),cp_NullValue(_read_.AT__ENTE))
      this.w_ATUFFICI = NVL(cp_ToDate(_read_.ATUFFICI),cp_NullValue(_read_.ATUFFICI))
      this.w_ATFLATRI = NVL(cp_ToDate(_read_.ATFLATRI),cp_NullValue(_read_.ATFLATRI))
      this.w_ATPERSON = NVL(cp_ToDate(_read_.ATPERSON),cp_NullValue(_read_.ATPERSON))
      this.w_ATTELEFO = NVL(cp_ToDate(_read_.ATTELEFO),cp_NullValue(_read_.ATTELEFO))
      this.w_AT___FAX = NVL(cp_ToDate(_read_.AT___FAX),cp_NullValue(_read_.AT___FAX))
      this.w_AT_EMAIL = NVL(cp_ToDate(_read_.AT_EMAIL),cp_NullValue(_read_.AT_EMAIL))
      this.w_AT_EMPEC = NVL(cp_ToDate(_read_.AT_EMPEC),cp_NullValue(_read_.AT_EMPEC))
      this.w_ATCELLUL = NVL(cp_ToDate(_read_.ATCELLUL),cp_NullValue(_read_.ATCELLUL))
      this.w_ATCODLIS = NVL(cp_ToDate(_read_.ATCODLIS),cp_NullValue(_read_.ATCODLIS))
      this.w_ATCODVAL = NVL(cp_ToDate(_read_.ATCODVAL),cp_NullValue(_read_.ATCODVAL))
      this.w_ATSTAATT = NVL(cp_ToDate(_read_.ATSTAATT),cp_NullValue(_read_.ATSTAATT))
      this.w_ATFLPROM = NVL(cp_ToDate(_read_.ATFLPROM),cp_NullValue(_read_.ATFLPROM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAUMATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CAFLRINV"+;
        " from "+i_cTable+" CAUMATTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CAUATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CAFLRINV;
        from (i_cTable) where;
            CACODICE = this.w_CAUATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CHECKRINV = NVL(cp_ToDate(_read_.CAFLRINV),cp_NullValue(_read_.CAFLRINV))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_Continua = .T.
    * --- Controllo se tipo attivit� � soggetto a prestazione
    if this.w_CHECKRINV # "S"
      ah_ErrorMsg("Operazione non consentita: il tipo dell'attivit� non gestisce il rinvio.")
      this.w_Continua = .F.
    endif
    if this.w_Continua
      this.Prosegui = .T.
      * --- begin transaction
      cp_BeginTrs()
      * --- Try
      local bErr_049E9570
      bErr_049E9570=bTrsErr
      this.Try_049E9570()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.Prosegui = .F.
      endif
      bTrsErr=bTrsErr or bErr_049E9570
      * --- End
      if this.Prosegui
        if this.w_ATFLPROM=="S" AND !EMPTY(this.w_ATPROMEM)
          * --- C'� promemoria, deve essere shiftato dello stesso tempo che intercorre tra il "vecchio" atdatini ed il nuovo ricalcolato
          this.w_ATPROMEM = this.w_ATPROMEM+(cp_CharToDatetime(DTOC(this.w_DATARINV)+" "+this.w_ORERINV+":"+this.w_MINRINV+":00")-this.w_ATDATINI)
        else
          this.w_ATPROMEM = cp_CharToDateTime("  -  -       :  :  ")
          this.w_ATFLPROM = "N"
        endif
        * --- begin transaction
        cp_BeginTrs()
        * --- Try
        local bErr_049EAA70
        bErr_049EAA70=bTrsErr
        this.Try_049EAA70()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_049EAA70
        * --- End
      endif
    endif
  endproc
  proc Try_049E9570()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Nell'attivit� aggiorna la data e l'ora di rinvio e pone lo stato come 'Evasa' .
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATDATRIN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDatetime(DTOC(this.w_DATARINV)+" "+this.w_ORERINV+":"+this.w_MINRINV+":00")),'OFF_ATTI','ATDATRIN');
      +",ATSTATUS ="+cp_NullLink(cp_ToStrODBC("F"),'OFF_ATTI','ATSTATUS');
      +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_ATTI','UTCV');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'OFF_ATTI','UTDV');
          +i_ccchkf ;
      +" where ";
          +"ATSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
             )
    else
      update (i_cTable) set;
          ATDATRIN = cp_CharToDatetime(DTOC(this.w_DATARINV)+" "+this.w_ORERINV+":"+this.w_MINRINV+":00");
          ,ATSTATUS = "F";
          ,ATDATOUT = DATETIME();
          ,UTCV = i_CODUTE;
          ,UTDV = SetInfoDate( g_CALUTD );
          &i_ccchkf. ;
       where;
          ATSERIAL = this.w_KEYATT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- In presenza di un'attivit� di tipo riserva
    if NOT EMPTY(NVL(this.w_ATTIPRIS, "" ))
      * --- Azzera il tipo riserva
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATTIPRIS ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'OFF_ATTI','ATTIPRIS');
        +",ATDATOUT ="+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
        +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_ATTI','UTCV');
        +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'OFF_ATTI','UTDV');
            +i_ccchkf ;
        +" where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
               )
      else
        update (i_cTable) set;
            ATTIPRIS = SPACE(1);
            ,ATDATOUT = DATETIME();
            ,UTCV = i_CODUTE;
            ,UTDV = SetInfoDate( g_CALUTD );
            &i_ccchkf. ;
         where;
            ATSERIAL = this.w_KEYATT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_049EAA70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_Conn=i_TableProp[this.OFF_ATTI_IDX, 3]
    * --- Legge il progressivo, lo incrementa di 1 (ponendo il valore ottenuto in w_SERIAL) ed aggiorna la tabella
    cp_NextTableProg(this, i_Conn, "SEATT", "i_codazi,w_SERIAL")
    * --- Inserisce Attivit�
    * --- Insert into OFF_ATTI
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ATSERIAL"+",ATCODNOM"+",ATCONTAT"+",ATOPERAT"+",ATOGGETT"+",ATCODATT"+",ATNOTPIA"+",AT_ESITO"+",ATCODESI"+",ATNUMPRI"+",ATDATINI"+",ATDATFIN"+",ATSTATUS"+",ATOPPOFF"+",ATCAUATT"+",ATCODPRA"+",ATGGPREA"+",ATPROMEM"+",ATPERCOM"+",ATLOCALI"+",AT__ENTE"+",ATUFFICI"+",ATFLATRI"+",ATTIPRIS"+",ATCAITER"+",ATDATRIN"+",ATDATPRO"+",ATPERSON"+",ATTELEFO"+",AT___FAX"+",AT_EMAIL"+",AT_EMPEC"+",ATCELLUL"+",ATOREEFF"+",ATMINEFF"+",ATKEYOUT"+",ATRIFDOC"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",ATSTAATT"+",ATCODLIS"+",ATCODVAL"+",ATRIFSER"+",ATNUMGIO"+",ATDATOUT"+",ATFLPROM"+",ATFLNOTI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'OFF_ATTI','ATSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCODNOM),'OFF_ATTI','ATCODNOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCONTAT),'OFF_ATTI','ATCONTAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATOPERAT),'OFF_ATTI','ATOPERAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATOGGETT),'OFF_ATTI','ATOGGETT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCODATT),'OFF_ATTI','ATCODATT');
      +","+cp_NullLink(cp_ToStrODBC(iif(this.w_SCRIVINOTE="S", this.w_ATNOTPIA, space(0))),'OFF_ATTI','ATNOTPIA');
      +","+cp_NullLink(cp_ToStrODBC(space(0)),'OFF_ATTI','AT_ESITO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCODESI),'OFF_ATTI','ATCODESI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATNUMPRI),'OFF_ATTI','ATNUMPRI');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDatetime(DTOC(this.w_DATARINV)+" "+this.w_ORERINV+":"+this.w_MINRINV+":00")),'OFF_ATTI','ATDATINI');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDatetime(DTOC(this.w_DATARINV)+" "+this.w_ORERINV+":"+this.w_MINRINV+":00")+this.w_Durata),'OFF_ATTI','ATDATFIN');
      +","+cp_NullLink(cp_ToStrODBC("D"),'OFF_ATTI','ATSTATUS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATOPPOFF),'OFF_ATTI','ATOPPOFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAUATT),'OFF_ATTI','ATCAUATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCODPRA),'OFF_ATTI','ATCODPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATGGPREA),'OFF_ATTI','ATGGPREA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATPROMEM),'OFF_ATTI','ATPROMEM');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','ATPERCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATLOCALI),'OFF_ATTI','ATLOCALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AT__ENTE),'OFF_ATTI','AT__ENTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATUFFICI),'OFF_ATTI','ATUFFICI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATFLATRI),'OFF_ATTI','ATFLATRI');
      +","+cp_NullLink(cp_ToStrODBC(space(1)),'OFF_ATTI','ATTIPRIS');
      +","+cp_NullLink(cp_ToStrODBC(space(20)),'OFF_ATTI','ATCAITER');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDatetime("  -  -       :  :  ")),'OFF_ATTI','ATDATRIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATDATINI),'OFF_ATTI','ATDATPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATPERSON),'OFF_ATTI','ATPERSON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATTELEFO),'OFF_ATTI','ATTELEFO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AT___FAX),'OFF_ATTI','AT___FAX');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AT_EMAIL),'OFF_ATTI','AT_EMAIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AT_EMPEC),'OFF_ATTI','AT_EMPEC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCELLUL),'OFF_ATTI','ATCELLUL');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','ATOREEFF');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','ATMINEFF');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','ATKEYOUT');
      +","+cp_NullLink(cp_ToStrODBC(space(10)),'OFF_ATTI','ATRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_ATTI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'OFF_ATTI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'OFF_ATTI','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATSTAATT),'OFF_ATTI','ATSTAATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCODLIS),'OFF_ATTI','ATCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCODVAL),'OFF_ATTI','ATCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(space(20)),'OFF_ATTI','ATRIFSER');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','ATNUMGIO');
      +","+cp_NullLink(cp_ToStrODBC(DATETIME()),'OFF_ATTI','ATDATOUT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATFLPROM),'OFF_ATTI','ATFLPROM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLNOTI),'OFF_ATTI','ATFLNOTI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ATSERIAL',this.w_SERIAL,'ATCODNOM',this.w_ATCODNOM,'ATCONTAT',this.w_ATCONTAT,'ATOPERAT',this.w_ATOPERAT,'ATOGGETT',this.w_ATOGGETT,'ATCODATT',this.w_ATCODATT,'ATNOTPIA',iif(this.w_SCRIVINOTE="S", this.w_ATNOTPIA, space(0)),'AT_ESITO',space(0),'ATCODESI',this.w_ATCODESI,'ATNUMPRI',this.w_ATNUMPRI,'ATDATINI',cp_CharToDatetime(DTOC(this.w_DATARINV)+" "+this.w_ORERINV+":"+this.w_MINRINV+":00"),'ATDATFIN',cp_CharToDatetime(DTOC(this.w_DATARINV)+" "+this.w_ORERINV+":"+this.w_MINRINV+":00")+this.w_Durata)
      insert into (i_cTable) (ATSERIAL,ATCODNOM,ATCONTAT,ATOPERAT,ATOGGETT,ATCODATT,ATNOTPIA,AT_ESITO,ATCODESI,ATNUMPRI,ATDATINI,ATDATFIN,ATSTATUS,ATOPPOFF,ATCAUATT,ATCODPRA,ATGGPREA,ATPROMEM,ATPERCOM,ATLOCALI,AT__ENTE,ATUFFICI,ATFLATRI,ATTIPRIS,ATCAITER,ATDATRIN,ATDATPRO,ATPERSON,ATTELEFO,AT___FAX,AT_EMAIL,AT_EMPEC,ATCELLUL,ATOREEFF,ATMINEFF,ATKEYOUT,ATRIFDOC,UTCC,UTDC,UTCV,UTDV,ATSTAATT,ATCODLIS,ATCODVAL,ATRIFSER,ATNUMGIO,ATDATOUT,ATFLPROM,ATFLNOTI &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,this.w_ATCODNOM;
           ,this.w_ATCONTAT;
           ,this.w_ATOPERAT;
           ,this.w_ATOGGETT;
           ,this.w_ATCODATT;
           ,iif(this.w_SCRIVINOTE="S", this.w_ATNOTPIA, space(0));
           ,space(0);
           ,this.w_ATCODESI;
           ,this.w_ATNUMPRI;
           ,cp_CharToDatetime(DTOC(this.w_DATARINV)+" "+this.w_ORERINV+":"+this.w_MINRINV+":00");
           ,cp_CharToDatetime(DTOC(this.w_DATARINV)+" "+this.w_ORERINV+":"+this.w_MINRINV+":00")+this.w_Durata;
           ,"D";
           ,this.w_ATOPPOFF;
           ,this.w_CAUATT;
           ,this.w_ATCODPRA;
           ,this.w_ATGGPREA;
           ,this.w_ATPROMEM;
           ,0;
           ,this.w_ATLOCALI;
           ,this.w_AT__ENTE;
           ,this.w_ATUFFICI;
           ,this.w_ATFLATRI;
           ,space(1);
           ,space(20);
           ,cp_CharToDatetime("  -  -       :  :  ");
           ,this.w_ATDATINI;
           ,this.w_ATPERSON;
           ,this.w_ATTELEFO;
           ,this.w_AT___FAX;
           ,this.w_AT_EMAIL;
           ,this.w_AT_EMPEC;
           ,this.w_ATCELLUL;
           ,0;
           ,0;
           ,0;
           ,space(10);
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           ,this.w_ATSTAATT;
           ,this.w_ATCODLIS;
           ,this.w_ATCODVAL;
           ,space(20);
           ,0;
           ,DATETIME();
           ,this.w_ATFLPROM;
           ,this.w_FLNOTI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Inserisce Partecipanti attivit�
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Legge il check "Non soggetta a prestazione" del tipo relativo all'attivit� da rinviare
    * --- Read from CAUMATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CAFLNSAP"+;
        " from "+i_cTable+" CAUMATTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CAUATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CAFLNSAP;
        from (i_cTable) where;
            CACODICE = this.w_CAUATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.FL_NON_SOG_PRE = NVL(cp_ToDate(_read_.CAFLNSAP),cp_NullValue(_read_.CAFLNSAP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se il tipo ha il check "Non soggetta a prestazione" disattivato
    if this.FL_NON_SOG_PRE # "S"
      * --- Inserisce Prestazioni attivit�
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento partecipanti singola attivit�
    *     (la pagina � eseguita sotto transazione - vedi pag. 3)
    this.NUMRIGA = 0
    * --- Legge i partecipanti dell'attivit� da rinviare
    * --- Select from OFF_PART
    i_nConn=i_TableProp[this.OFF_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2],.t.,this.OFF_PART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PACODRIS,PAGRURIS  from "+i_cTable+" OFF_PART ";
          +" where PASERIAL="+cp_ToStrODBC(this.w_KEYATT)+"";
           ,"_Curs_OFF_PART")
    else
      select PACODRIS,PAGRURIS from (i_cTable);
       where PASERIAL=this.w_KEYATT;
        into cursor _Curs_OFF_PART
    endif
    if used('_Curs_OFF_PART')
      select _Curs_OFF_PART
      locate for 1=1
      do while not(eof())
      this.NUMRIGA = this.NUMRIGA + 1
      this.w_CODRIS = _Curs_OFF_PART.PACODRIS
      this.w_PAGRURIS = NVL(_Curs_OFF_PART.PAGRURIS, SPACE(5))
      * --- Inserisce partecipante
      * --- Insert into OFF_PART
      i_nConn=i_TableProp[this.OFF_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_PART_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PASERIAL"+",CPROWNUM"+",CPROWORD"+",PACODRIS"+",PAGRURIS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'OFF_PART','PASERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.NUMRIGA),'OFF_PART','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.NUMRIGA * 10),'OFF_PART','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIS),'OFF_PART','PACODRIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PAGRURIS),'OFF_PART','PAGRURIS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PASERIAL',this.w_SERIAL,'CPROWNUM',this.NUMRIGA,'CPROWORD',this.NUMRIGA * 10,'PACODRIS',this.w_CODRIS,'PAGRURIS',this.w_PAGRURIS)
        insert into (i_cTable) (PASERIAL,CPROWNUM,CPROWORD,PACODRIS,PAGRURIS &i_ccchkf. );
           values (;
             this.w_SERIAL;
             ,this.NUMRIGA;
             ,this.NUMRIGA * 10;
             ,this.w_CODRIS;
             ,this.w_PAGRURIS;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_OFF_PART
        continue
      enddo
      use
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserimento prestazioni singola attivit�
    *     (la pagina � eseguita sotto transazione - vedi pag. 3)
    * --- Numero decimali per valori unitari
    this.w_CNFLAPON = space(1)
    this.w_CNFLVALO = space(1)
    this.w_CNIMPORT = 0
    this.w_CNCALDIR = space(1)
    this.w_CNCOECAL = 0
    this.w_CNTARTEM = space(1)
    this.w_CNTARCON = 0
    this.w_CONTRACN = space(1)
    this.w_TIPCONCO = space(1)
    this.w_LISCOLCO = space(1)
    this.w_STATUSCO = space(1)
    this.w_PARASS = 0
    this.w_FLAMPA = "N"
    if NOT(EMPTY(this.w_ATCODPRA) OR ISNULL(this.w_ATCODPRA))
      * --- Legge i dati relativi alla pratica dell'attivit� da rinviare
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CN__ENTE,CNTARTEM,CNTARCON,CNCONTRA,CNFLDIND,CNTIPPRA,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNFLVMLQ"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.w_ATCODPRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CN__ENTE,CNTARTEM,CNTARCON,CNCONTRA,CNFLDIND,CNTIPPRA,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNFLVMLQ;
          from (i_cTable) where;
              CNCODCAN = this.w_ATCODPRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CNFLAPON = NVL(cp_ToDate(_read_.CNFLAPON),cp_NullValue(_read_.CNFLAPON))
        this.w_CNFLVALO = NVL(cp_ToDate(_read_.CNFLVALO),cp_NullValue(_read_.CNFLVALO))
        this.w_CNIMPORT = NVL(cp_ToDate(_read_.CNIMPORT),cp_NullValue(_read_.CNIMPORT))
        this.w_CNCALDIR = NVL(cp_ToDate(_read_.CNCALDIR),cp_NullValue(_read_.CNCALDIR))
        this.w_CNCOECAL = NVL(cp_ToDate(_read_.CNCOECAL),cp_NullValue(_read_.CNCOECAL))
        this.w_PARASS = NVL(cp_ToDate(_read_.CNPARASS),cp_NullValue(_read_.CNPARASS))
        this.w_ENTE = NVL(cp_ToDate(_read_.CN__ENTE),cp_NullValue(_read_.CN__ENTE))
        this.w_CNTARTEM = NVL(cp_ToDate(_read_.CNTARTEM),cp_NullValue(_read_.CNTARTEM))
        this.w_CNTARCON = NVL(cp_ToDate(_read_.CNTARCON),cp_NullValue(_read_.CNTARCON))
        this.w_CONTRACN = NVL(cp_ToDate(_read_.CNCONTRA),cp_NullValue(_read_.CNCONTRA))
        this.w_CNFLVALO1 = NVL(cp_ToDate(_read_.CNFLDIND),cp_NullValue(_read_.CNFLDIND))
        this.w_TIPOPRAT = NVL(cp_ToDate(_read_.CNTIPPRA),cp_NullValue(_read_.CNTIPPRA))
        this.w_CNMATOBB = NVL(cp_ToDate(_read_.CNMATOBB),cp_NullValue(_read_.CNMATOBB))
        this.w_CNASSCTP = NVL(cp_ToDate(_read_.CNASSCTP),cp_NullValue(_read_.CNASSCTP))
        this.w_CNCOMPLX = NVL(cp_ToDate(_read_.CNCOMPLX),cp_NullValue(_read_.CNCOMPLX))
        this.w_CNPROFMT = NVL(cp_ToDate(_read_.CNPROFMT),cp_NullValue(_read_.CNPROFMT))
        this.w_CNESIPOS = NVL(cp_ToDate(_read_.CNESIPOS),cp_NullValue(_read_.CNESIPOS))
        this.w_CNPERPLX = NVL(cp_ToDate(_read_.CNPERPLX),cp_NullValue(_read_.CNPERPLX))
        this.w_CNPERPOS = NVL(cp_ToDate(_read_.CNPERPOS),cp_NullValue(_read_.CNPERPOS))
        this.w_CNFLVMLQ = NVL(cp_ToDate(_read_.CNFLVMLQ),cp_NullValue(_read_.CNFLVMLQ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Legge la classificazione del tipo pratica
      * --- Read from PRA_TIPI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRA_TIPI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRA_TIPI_idx,2],.t.,this.PRA_TIPI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TPFLGIUD"+;
          " from "+i_cTable+" PRA_TIPI where ";
              +"TPCODICE = "+cp_ToStrODBC(this.w_TIPOPRAT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TPFLGIUD;
          from (i_cTable) where;
              TPCODICE = this.w_TIPOPRAT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLGIUD = NVL(cp_ToDate(_read_.TPFLGIUD),cp_NullValue(_read_.TPFLGIUD))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT(EMPTY(this.w_CONTRACN) OR ISNULL(this.w_CONTRACN))
        * --- Legge i dati relativi al contratto della pratica dell'attivit� da rinviare
        * --- Read from PRA_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRA_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRA_CONT_idx,2],.t.,this.PRA_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CPTIPCON,CPLISCOL,CPSTATUS"+;
            " from "+i_cTable+" PRA_CONT where ";
                +"CPCODCON = "+cp_ToStrODBC(this.w_CONTRACN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CPTIPCON,CPLISCOL,CPSTATUS;
            from (i_cTable) where;
                CPCODCON = this.w_CONTRACN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPCONCO = NVL(cp_ToDate(_read_.CPTIPCON),cp_NullValue(_read_.CPTIPCON))
          this.w_LISCOLCO = NVL(cp_ToDate(_read_.CPLISCOL),cp_NullValue(_read_.CPLISCOL))
          this.w_STATUSCO = NVL(cp_ToDate(_read_.CPSTATUS),cp_NullValue(_read_.CPSTATUS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- Numero riga
    * --- Costo orario responsabile prestazione
    * --- Legge il numero di decimali per valori unitari
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECUNI"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_ATCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECUNI;
        from (i_cTable) where;
            VACODVAL = this.w_ATCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NUMDECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_NUMRIG = 0
    * --- Ciclo sulle prestazioni del tipo
    * --- Select from CAU_ATTI
    i_nConn=i_TableProp[this.CAU_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_ATTI_idx,2],.t.,this.CAU_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CACODSER,CADESSER,CADESAGG,CAFLDEFF,CAFLRESP  from "+i_cTable+" CAU_ATTI ";
          +" where CACODICE = "+cp_ToStrODBC(this.w_CAUATT)+"";
           ,"_Curs_CAU_ATTI")
    else
      select CACODSER,CADESSER,CADESAGG,CAFLDEFF,CAFLRESP from (i_cTable);
       where CACODICE = this.w_CAUATT;
        into cursor _Curs_CAU_ATTI
    endif
    if used('_Curs_CAU_ATTI')
      select _Curs_CAU_ATTI
      locate for 1=1
      do while not(eof())
      this.w_CODSER = _Curs_CAU_ATTI.CACODSER
      this.w_DESPREST = _Curs_CAU_ATTI.CADESSER
      this.w_DESSUP = _Curs_CAU_ATTI.CADESAGG
      this.FLDEFF = _Curs_CAU_ATTI.CAFLDEFF
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODSER);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART;
          from (i_cTable) where;
              CACODICE = this.w_CODSER;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARTIPART,ARPRESTA,ARUNMIS1,ARSTASUP,ARSTACOD,ARFLPRPE,ARPARAME"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARTIPART,ARPRESTA,ARUNMIS1,ARSTASUP,ARSTACOD,ARFLPRPE,ARPARAME;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
        this.w_TATIPPRE = NVL(cp_ToDate(_read_.ARPRESTA),cp_NullValue(_read_.ARPRESTA))
        this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
        this.w_ONORARBI = NVL(cp_ToDate(_read_.ARSTASUP),cp_NullValue(_read_.ARSTASUP))
        this.w_NOTIFICA = NVL(cp_ToDate(_read_.ARSTACOD),cp_NullValue(_read_.ARSTACOD))
        this.w_FLPRPE = NVL(cp_ToDate(_read_.ARFLPRPE),cp_NullValue(_read_.ARFLPRPE))
        this.w_TAPARAM = NVL(cp_ToDate(_read_.ARPARAME),cp_NullValue(_read_.ARPARAME))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_UNIMIS = NVL(this.w_UNIMIS, SPACE(3))
      this.QUANTITA = IIF(this.w_TIPART="DE", 0, 1)
      * --- Se � un diritto con valore pratica indeterminabile viene considerato il check
      *     "Calcolo dei diritti per valore indeterminabile" presente sulla pratica.
      if this.w_TATIPPRE $ "RC" AND this.w_CNFLVALO $ "IPS"
        this.w_CNFLVALO = this.w_CNFLVALO1
        this.w_CNCALDIR = "M"
        this.w_CNCOECAL = 50
      endif
      * --- Se attivo il check "Prestazione ripetuta"
      if _Curs_CAU_ATTI.CAFLRESP = "S"
        * --- Legge i partecipanti dell'attivit� da rinviare
        * --- Select from OFF_PART
        i_nConn=i_TableProp[this.OFF_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2],.t.,this.OFF_PART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PACODRIS  from "+i_cTable+" OFF_PART ";
              +" where PASERIAL="+cp_ToStrODBC(this.w_KEYATT)+"";
               ,"_Curs_OFF_PART")
        else
          select PACODRIS from (i_cTable);
           where PASERIAL=this.w_KEYATT;
            into cursor _Curs_OFF_PART
        endif
        if used('_Curs_OFF_PART')
          select _Curs_OFF_PART
          locate for 1=1
          do while not(eof())
          this.w_NUMRIG = this.w_NUMRIG+1
          this.COD_PART = _Curs_OFF_PART.PACODRIS
          * --- Lettura costo orario e mansione del responsabile della prestazione
          * --- Read from DIPENDEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIPENDEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPCOSORA,DPMANSION"+;
              " from "+i_cTable+" DIPENDEN where ";
                  +"DPCODICE = "+cp_ToStrODBC(this.COD_PART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPCOSORA,DPMANSION;
              from (i_cTable) where;
                  DPCODICE = this.COD_PART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COSORA = NVL(cp_ToDate(_read_.DPCOSORA),cp_NullValue(_read_.DPCOSORA))
            this.w_MANSION = NVL(cp_ToDate(_read_.DPMANSION),cp_NullValue(_read_.DPMANSION))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Valore Pratica
          do case
            case this.w_CNFLVALO="D"
              this.w_TAIMPORT = this.w_CNIMPORT
            case this.w_CNFLVALO="I"
              this.w_TAIMPORT = -1
            case this.w_CNFLVALO$"PS"
              this.w_TAIMPORT = -2
          endcase
          * --- Se sono valorizzati il listino e la pratica all'interno dell'attivit� da rinviare
          if NOT(EMPTY(this.w_ATCODPRA) OR ISNULL(this.w_ATCODPRA)) AND NOT(EMPTY(this.w_ATCODLIS) OR ISNULL(this.w_ATCODLIS))
            * --- Dichiarazione array prezzi
            DECLARE ARRPRE (4,1)
            * --- Azzero l'Array che verr� riempito dalla Funzione
            ARRPRE(1)=0
            this.w_ENTE_CAL = ""
            if !empty(this.w_ENTE)
              * --- Read from PRA_ENTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2],.t.,this.PRA_ENTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "EPCALTAR"+;
                  " from "+i_cTable+" PRA_ENTI where ";
                      +"EPCODICE = "+cp_ToStrODBC(this.w_ENTE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  EPCALTAR;
                  from (i_cTable) where;
                      EPCODICE = this.w_ENTE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_ENTE_CAL = NVL(cp_ToDate(_read_.EPCALTAR),cp_NullValue(_read_.EPCALTAR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if ALLTRIM(NVL(this.w_ENTE_CAL, "")) == ""
                this.w_ENTE_CAL = this.w_ENTE
              endif
            endif
            * --- Calcolo del prezzo unitario
            if IsAlt()
              do case
                case this.w_TATIPPRE="P" AND (this.w_CNTARTEM="C" OR (this.w_CNTARTEM="D" AND this.w_TIPCONCO<>"L" AND this.w_STATUSCO="I"))
                  * --- Se Prestazione a tempo e (Tipo di tariffazione a tempo uguale a Tariffa Concordata o 
                  *     (Tipo di tariffazione a tempo uguale a Tariffa Da Contratto e Tipo condizioni del contratto diverso da monte ore/listino collegato)):
                  *     applicazione tariffa concordata
                  this.PREZZO_UN = this.w_CNTARCON
                  this.PREZZO_MIN = this.w_CNTARCON
                  this.PREZZO_MAX = this.w_CNTARCON
                  this.w_DAGAZUFF = ""
                case this.w_TATIPPRE="P" AND this.w_CNTARTEM="D" AND this.w_TIPCONCO="L" AND this.w_STATUSCO="I"
                  * --- Se Prestazione a tempo e Tipo di tariffazione a tempo uguale a Tariffa Da Contratto e Tipo condizioni del contratto uguale a Da monte ore/listino collegato
                  *     applicazione listino collegato (da contratto)
                  this.w_GRAPAT = "N"
                  this.w_CALPRZ = CALPRAT(this.w_CODART, this.w_TATIPPRE, this.w_ONORARBI, this.w_FLPRPE, this.w_NOTIFICA, this.w_CNFLAPON, this.w_LISCOLCO, this.w_GRAPAT, this.w_ENTE_CAL, this.COD_PART, this.w_MANSION, this.w_DATARINV, this.w_CNFLVALO, this.w_TAIMPORT, this.w_CNCALDIR, this.w_CNCOECAL, this.w_PARASS, this.w_FLAMPA, @ARRPRE,this.w_NUMDECUNI, .F., "*NNNNN"+this.w_CNFLVMLQ+this.w_TAPARAM, 0, 0 )
                  this.PREZZO_UN = ARRPRE(1)
                  this.PREZZO_MIN = ARRPRE(2)
                  this.PREZZO_MAX = ARRPRE(3)
                  this.w_DAGAZUFF = ARRPRE(4)
                otherwise
                  * --- Caso normale
                  *     applicazione listino (della pratica)
                  * --- Read from LISTINI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.LISTINI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "LSGRAPAT"+;
                      " from "+i_cTable+" LISTINI where ";
                          +"LSCODLIS = "+cp_ToStrODBC(this.w_ATCODLIS);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      LSGRAPAT;
                      from (i_cTable) where;
                          LSCODLIS = this.w_ATCODLIS;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_GRAPAT = NVL(cp_ToDate(_read_.LSGRAPAT),cp_NullValue(_read_.LSGRAPAT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_CALPRZ = CALPRAT(this.w_CODART, this.w_TATIPPRE, this.w_ONORARBI, this.w_FLPRPE, this.w_NOTIFICA, this.w_CNFLAPON, this.w_ATCODLIS, this.w_GRAPAT, this.w_ENTE_CAL, this.COD_PART, this.w_MANSION, this.w_DATARINV, this.w_CNFLVALO, this.w_TAIMPORT, this.w_CNCALDIR, this.w_CNCOECAL, this.w_PARASS, this.w_FLAMPA, @ARRPRE,this.w_NUMDECUNI, .F., this.w_FLGIUD+this.w_CNMATOBB+this.w_CNASSCTP+this.w_CNCOMPLX+this.w_CNPROFMT+this.w_CNESIPOS+this.w_CNFLVMLQ+this.w_TAPARAM, this.w_CNPERPLX,this.w_CNPERPOS )
                  this.PREZZO_UN = ARRPRE(1)
                  this.PREZZO_MIN = ARRPRE(2)
                  this.PREZZO_MAX = ARRPRE(3)
                  this.w_DAGAZUFF = ARRPRE(4)
              endcase
              if this.w_TATIPPRE="P" AND this.w_CNTARTEM="C"
                * --- Se Prestazione a tempo e Tipo di tariffazione a tempo uguale a Tariffa Concordata
              else
              endif
            else
              this.PREZZO_UN = 0
              this.PREZZO_MIN = 0
              this.PREZZO_MAX = 0
              this.w_DAGAZUFF = ""
            endif
          else
            this.PREZZO_UN = 0
            this.PREZZO_MIN = 0
            this.PREZZO_MAX = 0
            this.w_DAGAZUFF = ""
          endif
          * --- Inserisce prestazione
          * --- Insert into OFFDATTI
          i_nConn=i_TableProp[this.OFFDATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFFDATTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DASERIAL"+",CPROWNUM"+",CPROWORD"+",DACODATT"+",DADESATT"+",DADESAGG"+",DACODRES"+",DACODOPE"+",DADATMOD"+",DAUNIMIS"+",DAQTAMOV"+",DAPREZZO"+",DAVALRIG"+",DAOREEFF"+",DAMINEFF"+",DACOSINT"+",DAFLDEFF"+",DAPREMIN"+",DAPREMAX"+",DAGAZUFF"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'OFFDATTI','DASERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'OFFDATTI','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG * 10),'OFFDATTI','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODSER),'OFFDATTI','DACODATT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESPREST),'OFFDATTI','DADESATT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'OFFDATTI','DADESAGG');
            +","+cp_NullLink(cp_ToStrODBC(this.COD_PART),'OFFDATTI','DACODRES');
            +","+cp_NullLink(cp_ToStrODBC(i_codute),'OFFDATTI','DACODOPE');
            +","+cp_NullLink(cp_ToStrODBC(i_datsys),'OFFDATTI','DADATMOD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'OFFDATTI','DAUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.QUANTITA),'OFFDATTI','DAQTAMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.PREZZO_UN),'OFFDATTI','DAPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(this.QUANTITA * this.PREZZO_UN),'OFFDATTI','DAVALRIG');
            +","+cp_NullLink(cp_ToStrODBC(0),'OFFDATTI','DAOREEFF');
            +","+cp_NullLink(cp_ToStrODBC(0),'OFFDATTI','DAMINEFF');
            +","+cp_NullLink(cp_ToStrODBC(0),'OFFDATTI','DACOSINT');
            +","+cp_NullLink(cp_ToStrODBC(this.FLDEFF),'OFFDATTI','DAFLDEFF');
            +","+cp_NullLink(cp_ToStrODBC(this.PREZZO_MIN),'OFFDATTI','DAPREMIN');
            +","+cp_NullLink(cp_ToStrODBC(this.PREZZO_MAX),'OFFDATTI','DAPREMAX');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DAGAZUFF),'OFFDATTI','DAGAZUFF');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DASERIAL',this.w_SERIAL,'CPROWNUM',this.w_NUMRIG,'CPROWORD',this.w_NUMRIG * 10,'DACODATT',this.w_CODSER,'DADESATT',this.w_DESPREST,'DADESAGG',this.w_DESSUP,'DACODRES',this.COD_PART,'DACODOPE',i_codute,'DADATMOD',i_datsys,'DAUNIMIS',this.w_UNIMIS,'DAQTAMOV',this.QUANTITA,'DAPREZZO',this.PREZZO_UN)
            insert into (i_cTable) (DASERIAL,CPROWNUM,CPROWORD,DACODATT,DADESATT,DADESAGG,DACODRES,DACODOPE,DADATMOD,DAUNIMIS,DAQTAMOV,DAPREZZO,DAVALRIG,DAOREEFF,DAMINEFF,DACOSINT,DAFLDEFF,DAPREMIN,DAPREMAX,DAGAZUFF &i_ccchkf. );
               values (;
                 this.w_SERIAL;
                 ,this.w_NUMRIG;
                 ,this.w_NUMRIG * 10;
                 ,this.w_CODSER;
                 ,this.w_DESPREST;
                 ,this.w_DESSUP;
                 ,this.COD_PART;
                 ,i_codute;
                 ,i_datsys;
                 ,this.w_UNIMIS;
                 ,this.QUANTITA;
                 ,this.PREZZO_UN;
                 ,this.QUANTITA * this.PREZZO_UN;
                 ,0;
                 ,0;
                 ,0;
                 ,this.FLDEFF;
                 ,this.PREZZO_MIN;
                 ,this.PREZZO_MAX;
                 ,this.w_DAGAZUFF;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
            select _Curs_OFF_PART
            continue
          enddo
          use
        endif
      else
        * --- Se non � attivo il check "Prestazione ripetuta"
        this.w_Continua = .T.
        this.COD_PART = space(5)
        * --- Legge il primo partecipante dell'attivit� da rinviare
        * --- Select from OFF_PART
        i_nConn=i_TableProp[this.OFF_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2],.t.,this.OFF_PART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PACODRIS  from "+i_cTable+" OFF_PART ";
              +" where PASERIAL="+cp_ToStrODBC(this.w_KEYATT)+"";
               ,"_Curs_OFF_PART")
        else
          select PACODRIS from (i_cTable);
           where PASERIAL=this.w_KEYATT;
            into cursor _Curs_OFF_PART
        endif
        if used('_Curs_OFF_PART')
          select _Curs_OFF_PART
          locate for 1=1
          do while not(eof())
          if this.w_Continua
            this.COD_PART = _Curs_OFF_PART.PACODRIS
            this.w_Continua = .F.
          endif
            select _Curs_OFF_PART
            continue
          enddo
          use
        endif
        this.w_NUMRIG = this.w_NUMRIG+1
        this.PREZZO_UN = 0
        this.PREZZO_MIN = 0
        this.PREZZO_MAX = 0
        this.w_DAGAZUFF = ""
        * --- Inserisce prestazione
        * --- Insert into OFFDATTI
        i_nConn=i_TableProp[this.OFFDATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFFDATTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DASERIAL"+",CPROWNUM"+",CPROWORD"+",DACODATT"+",DADESATT"+",DADESAGG"+",DACODRES"+",DACODOPE"+",DADATMOD"+",DAUNIMIS"+",DAQTAMOV"+",DAPREZZO"+",DAVALRIG"+",DAOREEFF"+",DAMINEFF"+",DACOSINT"+",DAFLDEFF"+",DAPREMIN"+",DAPREMAX"+",DAGAZUFF"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'OFFDATTI','DASERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'OFFDATTI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG * 10),'OFFDATTI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODSER),'OFFDATTI','DACODATT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DESPREST),'OFFDATTI','DADESATT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'OFFDATTI','DADESAGG');
          +","+cp_NullLink(cp_ToStrODBC(this.COD_PART),'OFFDATTI','DACODRES');
          +","+cp_NullLink(cp_ToStrODBC(i_codute),'OFFDATTI','DACODOPE');
          +","+cp_NullLink(cp_ToStrODBC(i_datsys),'OFFDATTI','DADATMOD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'OFFDATTI','DAUNIMIS');
          +","+cp_NullLink(cp_ToStrODBC(this.QUANTITA),'OFFDATTI','DAQTAMOV');
          +","+cp_NullLink(cp_ToStrODBC(this.PREZZO_UN),'OFFDATTI','DAPREZZO');
          +","+cp_NullLink(cp_ToStrODBC(this.QUANTITA * this.PREZZO_UN),'OFFDATTI','DAVALRIG');
          +","+cp_NullLink(cp_ToStrODBC(0),'OFFDATTI','DAOREEFF');
          +","+cp_NullLink(cp_ToStrODBC(0),'OFFDATTI','DAMINEFF');
          +","+cp_NullLink(cp_ToStrODBC(0),'OFFDATTI','DACOSINT');
          +","+cp_NullLink(cp_ToStrODBC(this.FLDEFF),'OFFDATTI','DAFLDEFF');
          +","+cp_NullLink(cp_ToStrODBC(this.PREZZO_MIN),'OFFDATTI','DAPREMIN');
          +","+cp_NullLink(cp_ToStrODBC(this.PREZZO_MAX),'OFFDATTI','DAPREMAX');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DAGAZUFF),'OFFDATTI','DAGAZUFF');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DASERIAL',this.w_SERIAL,'CPROWNUM',this.w_NUMRIG,'CPROWORD',this.w_NUMRIG * 10,'DACODATT',this.w_CODSER,'DADESATT',this.w_DESPREST,'DADESAGG',this.w_DESSUP,'DACODRES',this.COD_PART,'DACODOPE',i_codute,'DADATMOD',i_datsys,'DAUNIMIS',this.w_UNIMIS,'DAQTAMOV',this.QUANTITA,'DAPREZZO',this.PREZZO_UN)
          insert into (i_cTable) (DASERIAL,CPROWNUM,CPROWORD,DACODATT,DADESATT,DADESAGG,DACODRES,DACODOPE,DADATMOD,DAUNIMIS,DAQTAMOV,DAPREZZO,DAVALRIG,DAOREEFF,DAMINEFF,DACOSINT,DAFLDEFF,DAPREMIN,DAPREMAX,DAGAZUFF &i_ccchkf. );
             values (;
               this.w_SERIAL;
               ,this.w_NUMRIG;
               ,this.w_NUMRIG * 10;
               ,this.w_CODSER;
               ,this.w_DESPREST;
               ,this.w_DESSUP;
               ,this.COD_PART;
               ,i_codute;
               ,i_datsys;
               ,this.w_UNIMIS;
               ,this.QUANTITA;
               ,this.PREZZO_UN;
               ,this.QUANTITA * this.PREZZO_UN;
               ,0;
               ,0;
               ,0;
               ,this.FLDEFF;
               ,this.PREZZO_MIN;
               ,this.PREZZO_MAX;
               ,this.w_DAGAZUFF;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
        select _Curs_CAU_ATTI
        continue
      enddo
      use
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura sull'attivi� comune alle pag 2 e 3
    * --- Read from OFF_ATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" OFF_ATTI where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_KEYATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            ATSERIAL = this.w_KEYATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
      this.w_ATTIPRIS = NVL(cp_ToDate(_read_.ATTIPRIS),cp_NullValue(_read_.ATTIPRIS))
      this.w_ATCODNOM = NVL(cp_ToDate(_read_.ATCODNOM),cp_NullValue(_read_.ATCODNOM))
      this.w_ATCONTAT = NVL(cp_ToDate(_read_.ATCONTAT),cp_NullValue(_read_.ATCONTAT))
      this.w_ATOPERAT = NVL(cp_ToDate(_read_.ATOPERAT),cp_NullValue(_read_.ATOPERAT))
      this.w_ATOGGETT = NVL(cp_ToDate(_read_.ATOGGETT),cp_NullValue(_read_.ATOGGETT))
      this.w_ATCODATT = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
      this.w_ATNOTPIA = NVL(cp_ToDate(_read_.ATNOTPIA),cp_NullValue(_read_.ATNOTPIA))
      this.w_ATCODESI = NVL(cp_ToDate(_read_.ATCODESI),cp_NullValue(_read_.ATCODESI))
      this.w_ATNUMPRI = NVL(cp_ToDate(_read_.ATNUMPRI),cp_NullValue(_read_.ATNUMPRI))
      this.w_ATDATINI = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
      this.w_DataFinAttivita = NVL((_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
      this.w_ATOPPOFF = NVL(cp_ToDate(_read_.ATOPPOFF),cp_NullValue(_read_.ATOPPOFF))
      this.w_ATCODPRA = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
      this.w_ATGGPREA = NVL(cp_ToDate(_read_.ATGGPREA),cp_NullValue(_read_.ATGGPREA))
      this.w_ATPROMEM = NVL((_read_.ATPROMEM),cp_NullValue(_read_.ATPROMEM))
      this.w_ATLOCALI = NVL(cp_ToDate(_read_.ATLOCALI),cp_NullValue(_read_.ATLOCALI))
      this.w_AT__ENTE = NVL(cp_ToDate(_read_.AT__ENTE),cp_NullValue(_read_.AT__ENTE))
      this.w_ATUFFICI = NVL(cp_ToDate(_read_.ATUFFICI),cp_NullValue(_read_.ATUFFICI))
      this.w_ATFLATRI = NVL(cp_ToDate(_read_.ATFLATRI),cp_NullValue(_read_.ATFLATRI))
      this.w_ATPERSON = NVL(cp_ToDate(_read_.ATPERSON),cp_NullValue(_read_.ATPERSON))
      this.w_ATTELEFO = NVL(cp_ToDate(_read_.ATTELEFO),cp_NullValue(_read_.ATTELEFO))
      this.w_AT___FAX = NVL(cp_ToDate(_read_.AT___FAX),cp_NullValue(_read_.AT___FAX))
      this.w_AT_EMAIL = NVL(cp_ToDate(_read_.AT_EMAIL),cp_NullValue(_read_.AT_EMAIL))
      this.w_AT_EMPEC = NVL(cp_ToDate(_read_.AT_EMPEC),cp_NullValue(_read_.AT_EMPEC))
      this.w_ATCELLUL = NVL(cp_ToDate(_read_.ATCELLUL),cp_NullValue(_read_.ATCELLUL))
      this.w_ATCODLIS = NVL(cp_ToDate(_read_.ATCODLIS),cp_NullValue(_read_.ATCODLIS))
      this.w_ATCODVAL = NVL(cp_ToDate(_read_.ATCODVAL),cp_NullValue(_read_.ATCODVAL))
      this.w_ATSTAATT = NVL(cp_ToDate(_read_.ATSTAATT),cp_NullValue(_read_.ATSTAATT))
      this.w_ATFLPROM = NVL(cp_ToDate(_read_.ATFLPROM),cp_NullValue(_read_.ATFLPROM))
      this.w_FLNOTI = NVL(cp_ToDate(_read_.ATFLNOTI),cp_NullValue(_read_.ATFLNOTI))
      this.w_ATCAUACQ = NVL(cp_ToDate(_read_.ATCAUACQ),cp_NullValue(_read_.ATCAUACQ))
      this.w_ATCAUDOC = NVL(cp_ToDate(_read_.ATCAUDOC),cp_NullValue(_read_.ATCAUDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pFUNZ,pCURSORE,pAUTOCLOSE)
    this.pFUNZ=pFUNZ
    this.pCURSORE=pCURSORE
    this.pAUTOCLOSE=pAUTOCLOSE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,23)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='CAUMATTI'
    this.cWorkTables[3]='OFFDATTI'
    this.cWorkTables[4]='OFF_PART'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='CAU_ATTI'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='DIPENDEN'
    this.cWorkTables[9]='PRA_ENTI'
    this.cWorkTables[10]='KEY_ARTI'
    this.cWorkTables[11]='TIP_DOCU'
    this.cWorkTables[12]='CDC_MANU'
    this.cWorkTables[13]='VALUTE'
    this.cWorkTables[14]='OFF_NOMI'
    this.cWorkTables[15]='ATT_DCOL'
    this.cWorkTables[16]='RIS_ESTR'
    this.cWorkTables[17]='PRA_CONT'
    this.cWorkTables[18]='PAR_PRAT'
    this.cWorkTables[19]='LISTINI'
    this.cWorkTables[20]='CAU_CONT'
    this.cWorkTables[21]='PAR_ALTE'
    this.cWorkTables[22]='PAR_AGEN'
    this.cWorkTables[23]='PRA_TIPI'
    return(this.OpenAllTables(23))

  proc CloseCursors()
    if used('_Curs_ATT_DCOL')
      use in _Curs_ATT_DCOL
    endif
    if used('_Curs_RIS_ESTR')
      use in _Curs_RIS_ESTR
    endif
    if used('_Curs_OFFDATTI')
      use in _Curs_OFFDATTI
    endif
    if used('_Curs_OFFDATTI')
      use in _Curs_OFFDATTI
    endif
    if used('_Curs_gsag_bel')
      use in _Curs_gsag_bel
    endif
    if used('_Curs_OFFDATTI')
      use in _Curs_OFFDATTI
    endif
    if used('_Curs__d__d__agen_exe_query_gsag1bel_d_vqr')
      use in _Curs__d__d__agen_exe_query_gsag1bel_d_vqr
    endif
    if used('_Curs_OFFDATTI')
      use in _Curs_OFFDATTI
    endif
    if used('_Curs_RIS_ESTR')
      use in _Curs_RIS_ESTR
    endif
    if used('_Curs_OFF_PART')
      use in _Curs_OFF_PART
    endif
    if used('_Curs_CAU_ATTI')
      use in _Curs_CAU_ATTI
    endif
    if used('_Curs_OFF_PART')
      use in _Curs_OFF_PART
    endif
    if used('_Curs_OFF_PART')
      use in _Curs_OFF_PART
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFUNZ,pCURSORE,pAUTOCLOSE"
endproc
