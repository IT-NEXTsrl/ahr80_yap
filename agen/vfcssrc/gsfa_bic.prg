* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bic                                                        *
*              Importazione telefonate da centralino blues                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-01                                                      *
* Last revis.: 2010-04-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bic",oParentObject,m.pParam)
return(i_retval)

define class tgsfa_bic as StdBatch
  * --- Local variables
  pParam = space(10)
  w_NCONN = 0
  w_ListaCampi = space(10)
  w_FltBlues = space(10)
  w_StrConn = space(0)
  w_CODICE = 0
  w_PADRE = .NULL.
  w_ERR_MESS = .NULL.
  w_FLERRORS = .f.
  w_BLPATH = space(254)
  w_BLUSID = space(100)
  w_BLPSWD = space(100)
  w_EVCURNAM = space(10)
  w_UTDESUTE = space(254)
  w_DATETIME = space(14)
  w_EV__NOTE = space(0)
  w_ELENNUM = space(254)
  w_PATH = space(254)
  * --- WorkFile variables
  BLUESCON_idx=0
  UTE_NTI_idx=0
  cpusers_idx=0
  DIPENDEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CODICE = iif(this.oParentObject.w_UTCODICE<>0, this.oParentObject.w_UTCODICE, i_CODUTE)
    this.w_PADRE = this.oParentObject
    this.w_ERR_MESS = CREATEOBJECT("ah_ErrorLog")
    * --- Read from BLUESCON
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.BLUESCON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BLUESCON_idx,2],.t.,this.BLUESCON_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IBNAMFIL,IBCODUID,IB___PWD"+;
        " from "+i_cTable+" BLUESCON where ";
            +"IBCODICE = "+cp_ToStrODBC(1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IBNAMFIL,IBCODUID,IB___PWD;
        from (i_cTable) where;
            IBCODICE = 1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_BLPATH = NVL(cp_ToDate(_read_.IBNAMFIL),cp_NullValue(_read_.IBNAMFIL))
      this.w_BLUSID = NVL(cp_ToDate(_read_.IBCODUID),cp_NullValue(_read_.IBCODUID))
      this.w_BLPSWD = NVL(cp_ToDate(_read_.IB___PWD),cp_NullValue(_read_.IB___PWD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_EVCURNAM = SYS(2015)
    vq_exec("..\agen\exe\QUERY\GSFA_BIM.VQR",this,this.w_EVCURNAM)
    this.w_DATETIME = TTOC(DATETIME())
    * --- Read from cpusers
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.cpusers_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.cpusers_idx,2],.t.,this.cpusers_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "name"+;
        " from "+i_cTable+" cpusers where ";
            +"code = "+cp_ToStrODBC(i_CODUTE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        name;
        from (i_cTable) where;
            code = i_CODUTE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_UTDESUTE = NVL(cp_ToDate(_read_.name),cp_NullValue(_read_.name))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.pParam="SALVA"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    if EMPTY(NVL(this.w_BLPATH,""))
      ah_errormsg("Mancanco parametri di connessione al database di BLUES")
      i_retcode = 'stop'
      return
    endif
    this.oParentObject.w_Msg = ""
    this.w_StrConn = "DRIVER=Microsoft Access Driver (*.mdb);DriverId=281;FIL=MS Access;MaxBufferSize=2048;PageTimeout=5;"
    this.w_StrConn = this.w_StrConn + "DBQ=" + alltrim(this.w_BLPATH)
    if !empty(NVL(this.w_BLUSID,""))
      this.w_BLPSWD = decrypt_pwd(this.w_BLPSWD)
      this.w_StrConn = this.w_StrConn + ";uid=" + alltrim(this.w_BLUSID) + ";pwd=" + alltrim(this.w_BLPSWD)
    endif
    this.w_NCONN = SQLStringConnect(this.w_StrConn)
    if this.w_NCONN > 0
      addmsgnl("Importazione lanciata dall'operatore %1", this.w_PADRE, this.w_UTDESUTE )
      addmsgnl("Per le telefonate:", this.w_PADRE )
      * --- Indicata le chiamate risposte
      this.w_FltBlues = "sEsitoChiamata_c='1'"
      * --- Formato data di ACCESS: #2003-12-09# per il 9 dicembre 2003, ore #14:30#
      if NOT EMPTY(this.oParentObject.w_PHDTIM)
        addmsgnl("Dalla data %1", this.w_PADRE, TTOC(this.oParentObject.w_PHDTIM) )
        this.w_FltBlues = this.w_FltBlues + " and (dData>#"+left(DTOS(this.oParentObject.w_PHDTIM),4)+"-"+substr(DTOS(this.oParentObject.w_PHDTIM),5,2)+"-"+right(DTOS(this.oParentObject.w_PHDTIM),2)+"#"
        this.w_FltBlues = this.w_FltBlues + " or (dData=#"+left(DTOS(this.oParentObject.w_PHDTIM),4)+"-"+substr(DTOS(this.oParentObject.w_PHDTIM),5,2)+"-"+right(DTOS(this.oParentObject.w_PHDTIM),2)+"#"+" and dOra>#"+TTOC(this.oParentObject.w_PHDTIM,2)+"#))"
      endif
      if NOT EMPTY(this.oParentObject.w_PHDTIMFI)
        addmsgnl("Fino alla data %1", this.w_PADRE, TTOC(this.oParentObject.w_PHDTIMFI) )
        this.w_FltBlues = this.w_FltBlues + " and (dData<#"+left(DTOS(this.oParentObject.w_PHDTIMFI),4)+"-"+substr(DTOS(this.oParentObject.w_PHDTIMFI),5,2)+"-"+right(DTOS(this.oParentObject.w_PHDTIMFI),2)+"#"
        this.w_FltBlues = this.w_FltBlues + " or (dData=#"+left(DTOS(this.oParentObject.w_PHDTIMFI),4)+"-"+substr(DTOS(this.oParentObject.w_PHDTIMFI),5,2)+"-"+right(DTOS(this.oParentObject.w_PHDTIMFI),2)+"#"+" and dOra<=#"+TTOC(this.oParentObject.w_PHDTIMFI,2)+"#))"
      endif
      * --- Durata nel formato #ore:minuti:secondi#
      if this.oParentObject.w_ORE+this.oParentObject.w_MINUTI+this.oParentObject.w_SECONDI<>0
        addmsgnl("Durate pi� di ore %1 minuti %2 secondi %3", this.w_PADRE, ALLTRIM(STR(this.oParentObject.w_ORE)), ALLTRIM(STR(this.oParentObject.w_MINUTI)), ALLTRIM(STR(this.oParentObject.w_SECONDI)))
      endif
      this.w_FltBlues = this.w_FltBlues + " and dDurata>=#"+right("00"+alltrim(str(this.oParentObject.w_ORE)),2)+":"+right("00"+alltrim(str(this.oParentObject.w_MINUTI)),2)+":"+right("00"+alltrim(str(this.oParentObject.w_SECONDI)),2)+"#"
      * --- Operatore
      if !empty(this.oParentObject.w_TELEFO) OR !empty(this.oParentObject.w_TELEF2)
        if NOT EMPTY(NVL(this.oParentObject.w_TELEFO,""))
          this.w_ELENNUM = this.w_ELENNUM+cp_tostrodbc(alltrim(this.oParentObject.w_TELEFO))+","
        endif
        if NOT EMPTY(NVL(this.oParentObject.w_TELEF2,""))
          this.w_ELENNUM = this.w_ELENNUM+cp_tostrodbc(alltrim(this.oParentObject.w_TELEF2))+","
        endif
      else
        if this.oParentObject.w_SELUTPE="U"
          * --- Select from UTE_NTI
          i_nConn=i_TableProp[this.UTE_NTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2],.t.,this.UTE_NTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select UTTELEFO, UTTELEF2  from "+i_cTable+" UTE_NTI ";
                 ,"_Curs_UTE_NTI")
          else
            select UTTELEFO, UTTELEF2 from (i_cTable);
              into cursor _Curs_UTE_NTI
          endif
          if used('_Curs_UTE_NTI')
            select _Curs_UTE_NTI
            locate for 1=1
            do while not(eof())
            if NOT EMPTY(NVL(UTTELEFO,""))
              this.w_ELENNUM = this.w_ELENNUM+cp_tostrodbc(alltrim(UTTELEFO))+","
            endif
            if NOT EMPTY(NVL(UTTELEF2,""))
              this.w_ELENNUM = this.w_ELENNUM+cp_tostrodbc(alltrim(UTTELEF2))+","
            endif
              select _Curs_UTE_NTI
              continue
            enddo
            use
          endif
        else
          * --- Select from DIPENDEN
          i_nConn=i_TableProp[this.DIPENDEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select DPTELEF1, DPTELEF2  from "+i_cTable+" DIPENDEN ";
                 ,"_Curs_DIPENDEN")
          else
            select DPTELEF1, DPTELEF2 from (i_cTable);
              into cursor _Curs_DIPENDEN
          endif
          if used('_Curs_DIPENDEN')
            select _Curs_DIPENDEN
            locate for 1=1
            do while not(eof())
            if NOT EMPTY(NVL(DPTELEF1,""))
              this.w_ELENNUM = this.w_ELENNUM+cp_tostrodbc(alltrim(DPTELEF1))+","
            endif
            if NOT EMPTY(NVL(DPTELEF2,""))
              this.w_ELENNUM = this.w_ELENNUM+cp_tostrodbc(alltrim(DPTELEF2))+","
            endif
              select _Curs_DIPENDEN
              continue
            enddo
            use
          endif
        endif
      endif
      addmsgnl("Elenco numeri %1", this.w_PADRE, left(alltrim(this.w_ELENNUM),len(alltrim(this.w_ELENNUM))-1))
      this.w_ELENNUM = " and sDerivato in (" + left(alltrim(this.w_ELENNUM),len(alltrim(this.w_ELENNUM))-1) + ")"
      if this.w_ELENNUM=" and sDerivato in )"
        ah_errormsg("Mancanco numeri di telefono da estrarre dal database di BLUES")
        i_retcode = 'stop'
        return
      else
        this.w_FltBlues = this.w_FltBlues + this.w_ELENNUM
      endif
      if this.oParentObject.w_SELTRA = "N"
        addmsgnl("Considera solo trasferite", this.w_PADRE)
        * --- Filtro: considera solo le non trasferite
        this.w_FltBlues = this.w_FltBlues + " and iStato=0"
      endif
      if this.oParentObject.w_SELSNM = "N"
        * --- Filtro: considera solo quelle con avvalorato sNumero
        * --- w_FltBlues + ".and.!isNull(sNumero)"
        addmsgnl("Escludi senza numero", this.w_PADRE)
        this.w_FltBlues = this.w_FltBlues + " and sNumero is not NULL"
      endif
      if this.oParentObject.w_DIRCHI<>"T"
        if this.oParentObject.w_DIRCHI="I"
          addmsgnl("Solo chiamate in ingresso", this.w_PADRE)
        else
          addmsgnl("Solo chiamate in uscita", this.w_PADRE)
        endif
        this.w_FltBlues = this.w_FltBlues + " and sTipo="+cp_tostrodbc(this.oParentObject.w_DIRCHI)
      endif
      * --- Estraggo nella vista BLUES i records che soddisfano i filtri
      this.w_ListaCampi = "*"
      this.w_ListaCampi = "lPkid, dData, dOra, dDurata, sNumero, sDerivato, sTipo, iStato, sNome_c, sEsitoChiamata_c, cCosto1_c, sCommessa"
      this.w_PADRE.oPGFRM.ActivePage = 2
      addmsgnl("%0Inizio importazione eventi %1", this.w_PADRE, this.w_DATETIME )
      cp_SQLExec(this.w_NCONN, "select "+ this.w_ListaCampi + " from chiamate where "+this.w_FltBlues, "CursBLUES")
      if used("CursBLUES")
        addmsgnl("Record estratti da BLUES %1", this.w_PADRE, str(RECCOUNT("CursBLUES")) )
        addmsgnl("Creazione cursore %1", this.w_PADRE, TTOC(DATETIME()) )
        Select * into cursor CursBLUES from CursBLUES order by sTipo 
 SCAN
        this.w_EV__NOTE = IIF(not empty(NVL(CursBLUES.sCommessa,"")), iif(isalt(), ah_msgformat("Pratica %1", CursBLUES.sCommessa), ah_msgformat("Commessa %1", CursBLUES.sCommessa)), "")
        this.w_EV__NOTE = this.w_EV__NOTE + iif(not empty(this.w_EV__NOTE), Ah_MsgFormat("%0"), "")
        this.w_EV__NOTE = this.w_EV__NOTE + iif(nvl(CursBLUES.cCosto1_c,0)<>0, ah_msgformat("Costo chiamata %1", transform(CursBLUES.cCosto1_c,"@T")), "")
        this.w_EV__NOTE = this.w_EV__NOTE + iif(not empty(this.w_EV__NOTE), Ah_MsgFormat("%0%0"), "")
        this.w_EV__NOTE = this.w_EV__NOTE + Ah_MsgFormat("Importazione effettuata da %1 il %2", iif(not empty(this.w_UTDESUTE), alltrim(this.w_UTDESUTE), alltrim(str(i_CODUTE))), this.w_DATETIME)
        INSERT INTO ( this.w_EVCURNAM ) (EVSERIAL, EVTIPEVE, EVDIREVE, EV_PHONE, EVTELINT, EVDATINI, EVOREEFF, EVMINEFF, EVCODPRA, EV__NOTE) ; 
 VALUES (right("0000000000"+alltrim(STR(CursBLUES.lPkid,10)),10), iif(CursBLUES.sTipo="I", this.oParentObject.w_TIPEVEI, this.oParentObject.w_TIPEVEO), iif(CursBLUES.sTipo="I", "E", "U"),; 
 left(alltrim(CursBLUES.sNumero),18), left(alltrim(CursBLUES.sDerivato),18), ; 
 datetime(year(CursBLUES.dData), month(CursBLUES.dData), day(CursBLUES.dData), hour(CursBLUES.dOra), minute(CursBLUES.dOra), sec(CursBLUES.dOra)), ; 
 hour(CursBLUES.dDurata), minute(CursBLUES.dDurata), CursBLUES.sCommessa, this.w_EV__NOTE)
        Select CursBLUES 
 ENDSCAN
        addmsgnl("Generazione eventi %1", this.w_PADRE, TTOC(DATETIME()) )
        this.w_FLERRORS = this.w_FLERRORS OR !GSFA_BEV(this, this.w_EVCURNAM , "", this.w_ERR_MESS, "X", .t.)
        addmsgnl("%1", this.w_PADRE, this.w_ERR_MESS.GetLog())
        * --- Ci sono records estratti
        USE IN CursBLUES
        * --- Write into UTE_NTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.UTE_NTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.UTE_NTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"UTPHDTIM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PHDTIMFI),'UTE_NTI','UTPHDTIM');
              +i_ccchkf ;
          +" where ";
              +"UTCODICE = "+cp_ToStrODBC(this.w_CODICE);
                 )
        else
          update (i_cTable) set;
              UTPHDTIM = this.oParentObject.w_PHDTIMFI;
              &i_ccchkf. ;
           where;
              UTCODICE = this.w_CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      addmsgnl("Operazione completata %1", this.w_PADRE, TTOC(DATETIME()) )
      ah_errormsg("Operazione completata", 64)
    else
      * --- Connessione non riuscita
      ah_errormsg("Connessione al database di BLUES non riuscita")
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Salvataggio log
    this.w_PATH = "..\pcon\log\IMPBLUES_UT"+alltrim(str(i_CODUTE))+"_"+DTOS(DATE())+"_"+strtran(time(),":","")+".LOG"
    this.w_PATH = PUTFILE("Salva log", this.w_PATH, "LOG")
    if NOT EMPTY(this.w_PATH)
      this.w_ERR_MESS.SaveLog("Log importazione", "")     
      =STRTOFILE(strtran(strtran(this.oParentObject.w_Msg,CHR(13)+CHR(10),CHR(13)),CHR(13),CHR(13)+CHR(10)), this.w_PATH)
      ah_ErrorMsg("Salvataggio completato", 64)
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='BLUESCON'
    this.cWorkTables[2]='UTE_NTI'
    this.cWorkTables[3]='cpusers'
    this.cWorkTables[4]='DIPENDEN'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_UTE_NTI')
      use in _Curs_UTE_NTI
    endif
    if used('_Curs_DIPENDEN')
      use in _Curs_DIPENDEN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
