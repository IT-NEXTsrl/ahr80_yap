* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bcp                                                        *
*              Calendario di pi� partecipanti                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-01-06                                                      *
* Last revis.: 2015-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bcp",oParentObject,m.pAZIONE)
return(i_retval)

define class tgsag_bcp as StdBatch
  * --- Local variables
  pAZIONE = space(10)
  w_nBaseCal = 0
  w_CntDip = 0
  w_CntRis = 0
  w_CodDip = space(5)
  w_DPCOGNOM = space(40)
  w_DPNOME = space(40)
  w_DPDESCRI = space(40)
  w_DPTIPRIS = space(40)
  w_TxtItem = space(100)
  ind = 0
  indPart = 0
  w_StatusItem = 0
  w_ListCount = 0
  w_ListCount_Ris = 0
  w_IdxCode = 0
  w_ColCode = 0
  w_CalColumns = 0
  w_MaxDipAllowed = 0
  w_ColAttiva = 0
  idApp = 0
  w_Text = space(254)
  w_PartSerial = space(20)
  w_LockTimes = .f.
  w_ColSca = 0
  w_TextPart = space(254)
  w_ADatIni = ctot("")
  w_OraAtti = 0
  w_DurAtti = 0
  i_idx = 0
  w_PATIPRIS = space(1)
  w_CURDIR = space(254)
  w_Title = space(254)
  * --- WorkFile variables
  DIPENDEN_idx=0
  PAR_AGEN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione LIST partecipanti
    * --- Gestione ct1Day e caricamento appuntamenti nel DB
    this.w_nBaseCal = VAL(SYS( 11 , cp_CharToDate("01-01-1900") ))
    this.w_MaxDipAllowed = 10
    * --- Filtro per GSAG_MPA
    * --- Leggo dicitura da sostituire a riservata ( in progress diverr� una variabile pubblica)
    * --- Read from PAR_AGEN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PADESRIS"+;
        " from "+i_cTable+" PAR_AGEN where ";
            +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PADESRIS;
        from (i_cTable) where;
            PACODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_RISMSG = NVL(cp_ToDate(_read_.PADESRIS),cp_NullValue(_read_.PADESRIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pAZIONE=="INIT"
        * --- Inizializziamo i campi ctList
        * --- Inizializza il tipo di default
        * --- Read from PAR_AGEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PACAUATT"+;
            " from "+i_cTable+" PAR_AGEN where ";
                +"PACODAZI = "+cp_ToStrODBC(i_codazi);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PACAUATT;
            from (i_cTable) where;
                PACODAZI = i_codazi;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_CauDefault = NVL(cp_ToDate(_read_.PACAUATT),cp_NullValue(_read_.PACAUATT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_ct1Day.SetVisible(.T.)     
        if NVL(ALLTRIM(this.oParentObject.w_CauDefault),"")==""
          this.oParentObject.w_CauDefault = SPACE(20)
          this.oParentObject.w_AskCau = 1
        endif
      case this.pAZIONE=="AGGIORNACT1DAY"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAZIONE=="STAMPA"
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="CHECKATTIVO" OR this.pAzione=="CHECKDISATTIVO"OR this.pAzione=="RIS_CHECKATTIVO" OR this.pAzione=="RIS_CHECKDISATTIVO"
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAzione=="SELEZIONA" OR this.pAzione=="DESELEZIONA"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pAZIONE=="INGRANDISCI"
        this.oParentObject.w_DimScheda = 0
        this.oParentObject.w_ctSchedParte.Top = this.oParentObject.w_ctSchedParte.Top-55
        this.oParentObject.w_ctSchedParte.Height = this.oParentObject.w_ctSchedParte.Height+55
      case this.pAZIONE=="RIDUCI"
        this.oParentObject.w_DimScheda = 1
        this.oParentObject.w_ctSchedParte.Top = this.oParentObject.w_ctSchedParte.Top+55
        this.oParentObject.w_ctSchedParte.Height = this.oParentObject.w_ctSchedParte.Height-55
      case this.pAzione=="PREPARACODPART"
        * --- Prepara il codice del partecipante per aggiungere un appuntamento: dato il "code" della colonna, preleva il codice della risorsa da ctList.
        *     Possiamo usare la CheckOk perch� sicuramente il check � attivo, altrimenti non ci sarebbe la colonna
        this.oParentObject.w_CODPART = this.oParentObject.w_ctList_Parte.CheckOn(this.oParentObject.w_ColumnSel - 1, this.oParentObject.w_ClnCheck)
        this.oParentObject.w_GRUPART = ""
        this.oParentObject.w_PATIPRIS = "P"
      case this.pAzione=="+"
        this.oParentObject.w_DataSel = this.oParentObject.w_DataSel + 1
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_DatiAttivita = ""
        this.oParentObject.w_DatiPratica = ""
      case this.pAzione=="-"
        this.oParentObject.w_DataSel = this.oParentObject.w_DataSel - 1
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_DatiAttivita = ""
        this.oParentObject.w_DatiPratica = ""
      case this.pAzione=="OGGI"
        * --- Imposta data odierna
        this.oParentObject.w_DataSel = i_DatSys
        this.oParentObject.w_ctDate.SetDate(this.oParentObject.w_dataSel)     
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializziamo le date
    this.oParentObject.w_DataSel = i_DatSys
    this.oParentObject.w_ct1Day.SetColAppType(1)     
    this.oParentObject.w_ct1Day.SetDate(this.oParentObject.w_DataSel)     
    this.oParentObject.w_ctDate.SetDate(this.oParentObject.w_dataSel)     
    this.oParentObject.w_Data_Ini = DTOT(this.oParentObject.w_DataSel)
    this.oParentObject.w_Data_Fin = DTOT(this.oParentObject.w_DataSel)+3600*24-1
    * --- Inizializziamo i campi CtList aggiungendo le colonne con i vari campi
    *     I parametri sono cos� determinati
    *     
    *     1 - titolo colonna, 2 - larghezza,
    *     3 - tipo della colonna (0 - Text, 1 - Integer, 2 - Real, 3 - Date / Time)
    *     4 - tipo del check box, (0 - nessuno, 1 - bidimensionale, 2 - tridimensionale, 3 - User Defined)
    *     5 - Lock larghezza colonna, 6 - Sort abilitato T/F
    *     7 - Allineamento del testo (0 - Left Justified, 1 - Right Justified, 2 - Centered, 3 - Default)
    * --- ctList_Parte
    this.w_PATIPRIS = "P"
    if this.oParentObject.Visible
      * --- Se GSAG_KCP � visibile, nascondiamo prima tutti gli elementi per poi visualizzarli nuovamente
      this.oParentObject.w_ctList_Parte.SetVisible(.F.)     
    else
      * --- DESKMENU-la maschera � aggiornata dopo F10 del calendario-non nascondiamo gli elementi
      this.oParentObject.w_ctList_Parte.ClearList()     
    endif
    * --- Check
    this.oParentObject.w_ClnCheck = this.oParentObject.w_ctList_Parte.AddColumn(" ",20,0,2,.F.,.F.,2)
    * --- Descrizione risorsa
    this.oParentObject.w_ClnDesRis = this.oParentObject.w_ctList_Parte.AddColumn(AH_MSGFORMAT("Persone"),170,0,0,.F.,.F.,0)
    * --- Decodifichiamo l'utente attivo come risorsa interna
    this.w_CodDip = ReadDipend(i_codUte, "C")
    * --- Inseriamo le risorse interne in ctList_Parte
    if this.oParentObject.w_ClnCheck>0 and this.oParentObject.w_ClnDesRis>0
      * --- Se tutti i valori sono maggiori di zero, � segno che tutte le colonne sono state correttamente inserite
      this.oParentObject.w_ctList_Parte.SetVisible(.T.)     
      Dimension aText(2) 
 store "" to aText(1),aText(2)
      * --- Estraiamo le persone disponibili
      * --- Select from ..\AGEN\EXE\QUERY\GSAG_MPA
      do vq_exec with '..\AGEN\EXE\QUERY\GSAG_MPA',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA','',.f.,.t.
      if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA')
        select _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
        locate for 1=1
        do while not(eof())
        * --- Inseriamo ogni elemento in CtList
        * --- Check
        aText( this.oParentObject.w_ClnCheck ) = " "
        * --- Oggetto
        aText( this.oParentObject.w_ClnDesRis ) = ALLTRIM( DESCRIZIONE )
        * --- Componiamo il testo dell'item
        this.w_TxtItem = aText(1)+CHR(10)+aText(2)
        this.indPart = this.oParentObject.w_ctList_Parte.AddItem(this.w_TxtItem, DPCODICE)
        this.w_StatusItem = 0
        if DPCODICE=this.w_CodDip
          * --- L'utente attivo � l'unico selezionato
          this.w_StatusItem = 1
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Avvaloriamo il check
        this.oParentObject.w_ctList_Parte.CellCheck(this.IndPart, this.oParentObject.w_ClnCheck, this.w_StatusItem)     
          select _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
          continue
        enddo
        use
      endif
    endif
    if this.oParentObject.Visible
      * --- Se GSAG_KCP � visibile, nascondiamo prima tutti gli elementi per poi visualizzarli nuovamente
      this.oParentObject.w_ctList_Risorse.SetVisible(.F.)     
    else
      * --- DESKMENU-la maschera � aggiornata dopo F10 del calendario-non nascondiamo gli elementi
      this.oParentObject.w_ctList_Risorse.ClearList()     
    endif
    * --- Check
    this.oParentObject.w_ClnCheck_Ris = this.oParentObject.w_ctList_Risorse.AddColumn(" ",20,0,2,.F.,.F.,2)
    * --- Descrizione risorsa
    this.oParentObject.w_ClnDesRis_Ris = this.oParentObject.w_ctList_Risorse.AddColumn(AH_MSGFORMAT("Risorse"),170,0,0,.F.,.F.,0)
    * --- Inseriamo le risorse interne in ctList_Risorse
    if this.oParentObject.w_ClnCheck_Ris>0 and this.oParentObject.w_ClnDesRis_Ris>0
      * --- Se tutti i valori sono maggiori di zero, � segno che tutte le colonne sono state correttamente inserite
      this.oParentObject.w_ctList_Risorse.SetVisible(.T.)     
      Dimension aText(2) 
 store "" to aText(1),aText(2)
      * --- Estraiamo le risorse disponibili
      this.w_PATIPRIS = "R"
      * --- Select from ..\AGEN\EXE\QUERY\GSAG_MPA
      do vq_exec with '..\AGEN\EXE\QUERY\GSAG_MPA',this,'_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA','',.f.,.t.
      if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA')
        select _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
        locate for 1=1
        do while not(eof())
        * --- Inseriamo ogni elemento in CtList
        * --- Check
        aText( this.oParentObject.w_ClnCheck_Ris ) = " "
        * --- Oggetto
        aText( this.oParentObject.w_ClnDesRis_Ris ) = ALLTRIM( DESCRIZIONE )
        * --- Componiamo il testo dell'item
        this.w_TxtItem = aText(1)+CHR(10)+aText(2)
        this.indPart = this.oParentObject.w_ctList_Risorse.AddItem(this.w_TxtItem, DPCODICE)
        this.w_StatusItem = 0
        * --- Avvaloriamo il check
        this.oParentObject.w_ctList_Risorse.CellCheck(this.IndPart, this.oParentObject.w_ClnCheck, this.w_StatusItem)     
          select _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
          continue
        enddo
        use
      endif
    endif
    this.oParentObject.w_ctDate.CAL.Font.Name = g_GioStdFontName
    this.oParentObject.w_ctDate.CAL.Font.Size = g_GioStdFontSize
    this.oParentObject.w_ctDate.CAL.Font.Italic = g_bGioStdFontItalic
    this.oParentObject.w_ctDate.CAL.Font.Bold = g_bGioStdFontBold
    this.oParentObject.w_ctDate.CAL.DayFont1.Name = g_GioImpFontName
    this.oParentObject.w_ctDate.CAL.DayFont1.Size = g_GioImpFontSize
    this.oParentObject.w_ctDate.CAL.DayFont1.Italic = g_bGioImpFontItalic
    this.oParentObject.w_ctDate.CAL.DayFont1.Bold = g_bGioImpFontBold
    this.oParentObject.w_ct1Day.CAL.heightOffset = g_AgeDayHeightOffset
    if !oParentObject.oPgFrm.ActivePage = 1
      oParentObject.oPgFrm.ActivePage = 1
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Popoliamo Ct1Day estraendo solo i dipendenti che in ctList hanno il check attivo
    * --- Popoliamo ct1Day estraendo solo i dipendenti che in ctList hanno il check attivo
    this.oParentObject.w_Data_Ini = DTOT(this.oParentObject.w_DataSel)
    this.oParentObject.w_Data_Fin = DTOT(this.oParentObject.w_DataSel)+3600*24-1
    this.oParentObject.w_ct1Day.ClearCal()     
    this.oParentObject.w_ct1Day.ClearHeaders()     
    this.oParentObject.w_ct1Day.SetSortWithKey()     
    this.oParentObject.w_ct1Day.SetDate(this.oParentObject.w_DataSel)     
    this.w_ListCount = this.oParentObject.w_ctList_Parte.ListCount()
    this.w_CntDip = 0
    this.oParentObject.w_CntDipSel = 0
    * --- Persone
    do while this.w_CntDip <= this.w_ListCount
      * --- Cicliamo su tutti gli elementi di ctList_Parte e per ciascuno controlliamo il valore del check.
      *     La procedura CheckOn ritorna il codice della risorsa nel caso in cui il check sia attivo, vuoto altrimenti
      this.w_CodDip = this.oParentObject.w_ctList_Parte.CheckOn(this.w_CntDip, this.oParentObject.w_ClnCheck)
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CntDip = this.w_CntDip + 1
    enddo
    * --- Risorse
    *     w_CntDip � utilizzato a pag. 5 quindi dobbiamo utilizzare un altro contatore
    this.w_CntRis = 0
    this.w_ListCount_Ris = this.oParentObject.w_ctList_Risorse.ListCount()
    do while this.w_CntRis <= this.w_ListCount_Ris
      * --- Cicliamo su tutti gli elementi di ctList_Parte e per ciascuno controlliamo il valore del check.
      *     La procedura CheckOn ritorna il codice della risorsa nel caso in cui il check sia attivo, vuoto altrimenti
      this.w_CodDip = this.oParentObject.w_ctList_Risorse.CheckOn(this.w_CntRis, this.oParentObject.w_ClnCheck)
      this.w_CntDip = this.w_CntRis + this.w_ListCount
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_CntRis = this.w_CntRis + 1
    enddo
    if !oParentObject.oPgFrm.ActivePage = 1
      oParentObject.oPgFrm.ActivePage = 1
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Deve aggiornare lo schedule della risorsa il cui seriale � passato come parametro
    if !EMPTY(this.oParentObject.w_DipCodice)
      * --- Come progressivo fa sempre fede l'ordinamento in ctList, quindi sia per la selezione che per la deselezione il codice da associare alla risorsa � sempre lo stesso.
      this.w_CntDip = this.oParentObject.w_ItemSel
      this.w_CodDip = this.oParentObject.w_DipCodice
      if this.pAzione=="RIS_CHECKATTIVO" OR this.pAzione=="RIS_CHECKDISATTIVO"
        * --- Selezione di una risorsa, deve sommare al numero del'item il totale di item delle persone
        this.w_CntDip = this.oParentObject.w_ItemSel + this.oParentObject.w_CtList_Parte.ListCount()
      endif
      if this.pAzione=="CHECKDISATTIVO" OR this.pAzione=="RIS_CHECKDISATTIVO" 
        * --- Dobbiamo cancellare l'header da ct1Day
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.ind # 0
          * --- Ha trovato la colonna: rimuoviamo prima tutti gli appuntamenti che hanno per CODE ind e poi cancelliamo l'header.
          *     Il progressivo della colonna e w_IdxCode-1
          if this.oParentObject.w_ct1Day.GetClmnApps( this.w_IdxCode - 1)>0
            * --- Ci sono appuntamenti per quella risorsa, dobbiamo cancellarli
            this.i_idx = this.oParentObject.w_ct1day.GetApptNumb()
            do while this.i_idx > 0
              if this.oParentObject.w_ct1Day.GetAppCode( this.i_idx ) = this.ind
                * --- Abbiamo trovato un appuntamento della risorsa
                this.oParentObject.w_ct1Day.DelAppoint(this.i_idx)     
              endif
              this.i_idx = this.i_idx - 1
            enddo
          endif
          this.oParentObject.w_ct1Day.DeleteCodeHeader(this.ind)     
          this.oParentObject.w_CntDipSel = this.oParentObject.w_CntDipSel - 1
          * --- Se non c'� pi� nessuna risorsa selezionata cancella tutti gli header
          if this.oParentObject.w_CntDipSel = 0
            this.oParentObject.w_ct1Day.ClearHeaders()     
          endif
        endif
      else
        * --- pAzione=='CHECKATTIVO'
        *     Aggiungiamo l'item con le relative attivit�
        if this.oParentObject.w_CntDipSel<this.w_MaxDipAllowed
          * --- Check attivo e le risorse sono corrette
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Check disattivo
          wait wind "Raggiunto numero massimo di utenti selezionabili"
          this.w_StatusItem = 0
          this.oParentObject.w_ctList_Parte.CellCheck(this.w_CntDip, this.oParentObject.w_ClnCheck, this.w_StatusItem)     
        endif
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestisce la risorsa inserendo una colonna per ciascun responsabile ed inserendo le attivit� di questo
    if !EMPTY(this.w_CodDip)
      * --- E' necessario ripristinare a blank CODPART perch� pu� essere stato avvalorato caricando un appuntamento con doppio click.
      *     Questo causa filtri non necessari in GSAR2BIA
      this.oParentObject.w_CODPART = ""
      * --- Read from DIPENDEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS"+;
          " from "+i_cTable+" DIPENDEN where ";
              +"DPCODICE = "+cp_ToStrODBC(this.w_CodDip);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DPCOGNOM,DPNOME,DPDESCRI,DPTIPRIS;
          from (i_cTable) where;
              DPCODICE = this.w_CodDip;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DPCOGNOM = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
        this.w_DPNOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
        this.w_DPDESCRI = NVL(cp_ToDate(_read_.DPDESCRI),cp_NullValue(_read_.DPDESCRI))
        this.w_DPTIPRIS = NVL(cp_ToDate(_read_.DPTIPRIS),cp_NullValue(_read_.DPTIPRIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Prima di aggiungere dovremmo controllare se � gi� stato inserito
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.ind = 0
        this.oParentObject.w_CntDipSel = this.oParentObject.w_CntDipSel + 1
        * --- Non esiste ancora la colonna, la aggiunge
        this.ind = this.oParentObject.w_ct1Day.AddHeader( this.w_CntDip+1, IIF(this.w_DPTIPRIS=="P",ALLTRIM(ALLTRIM(this.w_DPCOGNOM)+" "+ALLTRIM(this.w_DPNOME)),ALLTRIM(this.w_DPDESCRI)))
      endif
      if this.ind # 0
        * --- Select from QUERY\GSAG_BCP
        do vq_exec with 'QUERY\GSAG_BCP',this,'_Curs_QUERY_GSAG_BCP','',.f.,.t.
        if used('_Curs_QUERY_GSAG_BCP')
          select _Curs_QUERY_GSAG_BCP
          locate for 1=1
          do while not(eof())
          * --- Gestione data attivit�
          this.w_Text = ALLTRIM(ATOGGETT)
          this.w_LockTimes = .F.
          * --- Aggiorna data
          this.w_ADatIni = VAL(SYS(11,this.oParentObject.w_Datasel)) - this.w_nBaseCal
          if ATDATINI < this.oParentObject.w_Data_Ini
            * --- L'appuntamento inizia in precedenza
            this.w_OraAtti = 0
            this.w_LockTimes = .T.
          else
            this.w_OraAtti = HOUR(ATDATINI)*60+MINUTE(ATDATINI)
          endif
          * --- 3600*24-1
          this.w_DurAtti = 0
          if !ISNULL(ATDATFIN) AND !EMPTY(ATDATFIN)
            * --- calcoliamo la durata in minuti
            this.w_DurAtti = ( ATDATFIN-ATDATINI ) / 60 
          endif
          if this.w_DurAtti = 0
            this.w_DurAtti = 1
          endif
          * --- Se superiamo la mezzanotte, facciamo arrivare l'appuntamento a mezzanotte.
          *     In questo modo, gestiamo correttamente anche gli appuntamenti che finiscono il giorno successivo
          if this.w_OraAtti + this.w_DurAtti > (60*24)-1
            this.w_DurAtti = (60*24)-1 - this.w_OraAtti
            this.w_LockTimes = .T.
          endif
          * --- Dobbiamo inserire anche i partecipanti
          this.w_PartSerial = ATSERIAL
          this.w_TextPart = ""
          * --- Select from QUERY\GSAG6BAG.VQR
          do vq_exec with 'QUERY\GSAG6BAG.VQR',this,'_Curs_QUERY_GSAG6BAG_d_VQR','',.f.,.t.
          if used('_Curs_QUERY_GSAG6BAG_d_VQR')
            select _Curs_QUERY_GSAG6BAG_d_VQR
            locate for 1=1
            do while not(eof())
            do case
              case DPTIPRIS <> "R"
                this.w_TextPart = this.w_TextPart+ ALLTRIM(DESCR_DIP)+","+SPACE(1)
            endcase
              select _Curs_QUERY_GSAG6BAG_d_VQR
              continue
            enddo
            use
          endif
          if !EMPTY(this.w_TextPart)
            this.w_TextPart = SPACE(1)+"["+SUBSTR(this.w_TextPart,1, LEN(this.w_TextPart)-2 )+"]"
            this.w_Text = this.w_Text+this.w_TextPart
          endif
          * --- AddAppointment
          this.idApp = this.oParentObject.w_ct1Day.AddKeyAppointment(this.w_OraAtti, this.w_OraAtti+this.w_DurAtti, this.w_ADatIni, this.w_Text, this.w_PartSerial, this.w_LockTimes, _Curs_QUERY_GSAG_BCP.DISPOCOL, _Curs_QUERY_GSAG_BCP.RIGACOL)
          this.oParentObject.w_ct1Day.SetAppCode(this.IdApp, this.w_CntDip+1)     
          this.oParentObject.w_ct1Day.AppointHorzLock(this.idApp)     
          if _Curs_QUERY_GSAG_BCP.ATSTATUS=="T"
            * --- Attivit� provvisoria
            this.oParentObject.w_ct1Day.SetAppBackColor(this.ind, g_ColAttConf)     
          endif
          * --- Se attivit� riservata la rendo immodificabile...
          if Not IsRisAtt( _Curs_QUERY_GSAG_BCP.ATSERIAL , _Curs_QUERY_GSAG_BCP.ATFLATRI ) 
            * --- Se attivit� riservata maschero alcuni campi...
            *     (in progress la lettura dai parametri potr� essere sostituita da una var. pubblica letta all'avvio)
            this.oParentObject.w_ct1Day.cal.AppointReadOnly( this.idApp ) = .t.
            this.oParentObject.w_ct1Day.cal.AppointLockTimes( this.idApp ) = .t.
          endif
            select _Curs_QUERY_GSAG_BCP
            continue
          enddo
          use
        endif
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa
    this.w_CURDIR = SYS(5)+CURDIR()
    if TTOD(this.oParentObject.w_Data_Ini)=TTOD(this.oParentObject.w_DATA_FIN)
      this.w_Title = Ah_MsgFormat("Di")+" "+ALLTRIM(g_GIORNO[DOW(this.oParentObject.w_Data_Ini)])+" "+ALLTRIM(STR(DAY(this.oParentObject.w_Data_Ini)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Ini)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Ini)))
    else
      this.w_Title = Ah_MsgFormat("Da")+" "+ALLTRIM(g_GIORNO[DOW(this.oParentObject.w_Data_Ini)])+" "+ALLTRIM(STR(DAY(this.oParentObject.w_Data_Ini)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Ini)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Ini)))+" "+Ah_MsgFormat("a")+" "+ALLTRIM(g_GIORNO[DOW(this.oParentObject.w_Data_Fin)])+" "+ALLTRIM(STR(DAY(this.oParentObject.w_Data_Fin)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_Data_Fin)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_Data_Fin)))
    endif
    this.oParentObject.w_ctSchedParte.PrintTitles(Ah_MsgFormat("Stampa disponibilit� delle persone"), this.w_Title)     
    this.oParentObject.w_ctSchedParte.PrintSchedule()     
    cd (this.w_CURDIR)
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona/deseleziona tutti
    this.w_ListCount = this.oParentObject.w_ctList_Parte.ListCount()
    this.w_CntDip = 0
    if this.pAzione=="SELEZIONA"
      * --- Check attivo
      this.oParentObject.w_CntDipSel = this.w_MaxDipAllowed
    else
      * --- Check disattivo
      this.oParentObject.w_CntDipSel = 0
    endif
    do while this.w_CntDip <= this.w_ListCount-1
      * --- Cicliamo su tutti gli elementi di ctSchedParte ed impostiamo il check
      if this.pAzione=="SELEZIONA" AND this.w_CntDip<this.w_MaxDipAllowed
        * --- Check attivo
        this.w_StatusItem = 1
      else
        * --- Check disattivo
        this.w_StatusItem = 0
      endif
      this.oParentObject.w_ctList_Parte.CellCheck(this.w_CntDip, this.oParentObject.w_ClnCheck, this.w_StatusItem)     
      this.w_CntDip = this.w_CntDip + 1
    enddo
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dato il progressivo del dipendente in ctList_parte, restituisce l'id dell'header in Ct1Day
    * --- la var. w_CntDip � gestita altrove
    this.ind = 0
    this.w_ColCode = 0
    this.w_IdxCode = 0
    this.w_CalColumns = this.oParentObject.w_ct1Day.GetVirtualClmn()
    do while this.w_IdxCode <= this.w_CalColumns AND this.ind=0
      * --- Cerchiamo se esiste gi� la colonna della risorsa
      this.w_ColCode = this.oParentObject.w_ct1Day.FindColCode( this.w_IdxCode )
      if this.w_ColCode = this.w_CntDip + 1
        this.ind = this.w_ColCode
      endif
      this.w_IdxCode = this.w_IdxCode + 1
    enddo
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAZIONE)
    this.pAZIONE=pAZIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='PAR_AGEN'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
    endif
    if used('_Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA')
      use in _Curs__d__d__AGEN_EXE_QUERY_GSAG_MPA
    endif
    if used('_Curs_QUERY_GSAG_BCP')
      use in _Curs_QUERY_GSAG_BCP
    endif
    if used('_Curs_QUERY_GSAG6BAG_d_VQR')
      use in _Curs_QUERY_GSAG6BAG_d_VQR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAZIONE"
endproc
