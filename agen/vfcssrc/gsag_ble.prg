* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ble                                                        *
*              Esegue link ID richiesta                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-19                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pIDRICH,pNOMESER,pOggetto,pNOMENOM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_ble",oParentObject,m.pIDRICH,m.pNOMESER,m.pOggetto,m.pNOMENOM)
return(i_retval)

define class tgsag_ble as StdBatch
  * --- Local variables
  pIDRICH = space(15)
  pNOMESER = space(10)
  pOggetto = .NULL.
  pNOMENOM = space(10)
  w_EVSERIAL = space(10)
  w_EV__ANNO = space(4)
  w_CODNOM = space(15)
  w_EVLOCALI = space(30)
  w_EV_EMAIL = space(254)
  w_EV_EMPEC = space(254)
  w_EVNOMINA = space(15)
  w_EVCODPAR = space(5)
  w_EVRIFPER = space(254)
  w_EV_PHONE = space(18)
  w_EVNUMCEL = space(20)
  w_EVCODPRA = space(15)
  w_DESNOM = space(60)
  w_TIPNOM = space(1)
  w_ATTIPCLI = space(1)
  w_CODCLI = space(15)
  * --- WorkFile variables
  ANEVENTI_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Not Empty(this.pIDRICH)
      * --- Read from ANEVENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ANEVENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2],.t.,this.ANEVENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "EVSERIAL,EV__ANNO,EVNOMINA,EVLOCALI,EV_EMAIL,EV_EMPEC,EVCODPAR,EVRIFPER,EV_PHONE,EVNUMCEL,EVCODPRA"+;
          " from "+i_cTable+" ANEVENTI where ";
              +"EVIDRICH = "+cp_ToStrODBC(this.pIDRICH);
              +" and EVFLIDPA = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          EVSERIAL,EV__ANNO,EVNOMINA,EVLOCALI,EV_EMAIL,EV_EMPEC,EVCODPAR,EVRIFPER,EV_PHONE,EVNUMCEL,EVCODPRA;
          from (i_cTable) where;
              EVIDRICH = this.pIDRICH;
              and EVFLIDPA = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_EVSERIAL = NVL(cp_ToDate(_read_.EVSERIAL),cp_NullValue(_read_.EVSERIAL))
        this.w_EV__ANNO = NVL(cp_ToDate(_read_.EV__ANNO),cp_NullValue(_read_.EV__ANNO))
        this.w_CODNOM = NVL(cp_ToDate(_read_.EVNOMINA),cp_NullValue(_read_.EVNOMINA))
        this.w_EVLOCALI = NVL(cp_ToDate(_read_.EVLOCALI),cp_NullValue(_read_.EVLOCALI))
        this.w_EV_EMAIL = NVL(cp_ToDate(_read_.EV_EMAIL),cp_NullValue(_read_.EV_EMAIL))
        this.w_EV_EMPEC = NVL(cp_ToDate(_read_.EV_EMPEC),cp_NullValue(_read_.EV_EMPEC))
        this.w_EVNOMINA = NVL(cp_ToDate(_read_.EVNOMINA),cp_NullValue(_read_.EVNOMINA))
        this.w_EVCODPAR = NVL(cp_ToDate(_read_.EVCODPAR),cp_NullValue(_read_.EVCODPAR))
        this.w_EVRIFPER = NVL(cp_ToDate(_read_.EVRIFPER),cp_NullValue(_read_.EVRIFPER))
        this.w_EV_PHONE = NVL(cp_ToDate(_read_.EV_PHONE),cp_NullValue(_read_.EV_PHONE))
        this.w_EVNUMCEL = NVL(cp_ToDate(_read_.EVNUMCEL),cp_NullValue(_read_.EVNUMCEL))
        this.w_EVCODPRA = NVL(cp_ToDate(_read_.EVCODPRA),cp_NullValue(_read_.EVCODPRA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_ROWS=0
        this.w_EVSERIAL = .null.
        this.w_EV__ANNO = .null.
        this.w_CODNOM = .null.
      endif
      this.pNOMESER = Strtran(this.pNOMESER,".","")
      this.pOggetto.w_ATEVANNO = this.w_EV__ANNO
      if Upper(this.pOggetto.class) ="TGSAG_AAT"
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NODESCRI,NOTIPNOM,NOTIPCLI,NOCODCLI"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_EVNOMINA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NODESCRI,NOTIPNOM,NOTIPCLI,NOCODCLI;
            from (i_cTable) where;
                NOCODICE = this.w_EVNOMINA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESNOM = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
          this.w_TIPNOM = NVL(cp_ToDate(_read_.NOTIPNOM),cp_NullValue(_read_.NOTIPNOM))
          this.w_ATTIPCLI = NVL(cp_ToDate(_read_.NOTIPCLI),cp_NullValue(_read_.NOTIPCLI))
          this.w_CODCLI = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.pOggetto.w_ATCELLUL = left(this.w_EVNUMCEL,18)
        this.pOggetto.w_ATCODNOM = this.w_EVNOMINA
        this.pOggetto.o_ATCODNOM = this.w_EVNOMINA
        this.pOggetto.w_DESNOMIN = this.w_DESNOM
        this.pOggetto.w_ATLOCALI = this.w_EVLOCALI
        this.pOggetto.w_AT_EMAIL = this.w_EV_EMAIL
        this.pOggetto.w_AT_EMPEC = this.w_EV_EMPEC
        this.pOggetto.w_ATCONTAT = this.w_EVCODPAR
        this.pOggetto.w_ATPERSON = LEFT(this.w_EVRIFPER,60)
        this.pOggetto.w_ATTELEFO = left(this.w_EV_PHONE,18)
        this.pOggetto.w_ATCELLUL = left(this.w_EVNUMCEL,18)
        this.pOggetto.w_TIPNOM = this.w_TIPNOM
        this.pOggetto.w_ATCODPRA = this.w_EVCODPRA
        this.pOggetto.w_ATCOMRIC = this.w_EVCODPRA
        this.pOggetto.w_ATTIPCLI = this.w_ATTIPCLI
        this.pOggetto.w_CODCLI = this.w_CODCLI
        SETVALUELINKED("M", this.pOggetto,"w_CODCLI",this.w_CODCLI, this.w_ATTIPCLI)
      endif
      SETVALUELINKED("M", this.pOggetto, Alltrim(this.pNOMESER),this.w_EVSERIAL,this.w_EV__ANNO)
      if Left(Upper(this.pOggetto.class),4) ="TGSF"
        this.pOggetto.w_EVIDRICH = this.pOggetto.w_IDRICH
        this.pOggetto.w_EVSTARIC = this.pOggetto.w_STARIC
      endif
      if Vartype(this.pNOMENOM)="C" and Not Empty(this.pNOMENOM)
        this.pNOMENOM = Strtran(this.pNOMENOM,".","")
         
 NC=Alltrim(this.pNOMENOM) 
 NOMIN=this.pOggetto.&NC
        if this.w_CODNOM <> NOMIN
          SETVALUELINKED("M", this.pOggetto, Alltrim(this.pNOMENOM),this.w_CODNOM)
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pIDRICH,pNOMESER,pOggetto,pNOMENOM)
    this.pIDRICH=pIDRICH
    this.pNOMESER=pNOMESER
    this.pOggetto=pOggetto
    this.pNOMENOM=pNOMENOM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ANEVENTI'
    this.cWorkTables[2]='OFF_NOMI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pIDRICH,pNOMESER,pOggetto,pNOMENOM"
endproc
