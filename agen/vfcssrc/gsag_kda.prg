* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kda                                                        *
*              Dati di riga                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-15                                                      *
* Last revis.: 2013-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kda",oParentObject))

* --- Class definition
define class tgsag_kda as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 672
  Height = 320+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-12-05"
  HelpContextID=185981079
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=68

  * --- Constant Properties
  _IDX = 0
  VOC_COST_IDX = 0
  CAN_TIER_IDX = 0
  CENCOST_IDX = 0
  ATTIVITA_IDX = 0
  TIP_DOCU_IDX = 0
  CON_TRAM_IDX = 0
  CON_TRAS_IDX = 0
  MOD_ELEM_IDX = 0
  IMP_MAST_IDX = 0
  IMP_DETT_IDX = 0
  ELE_CONT_IDX = 0
  KEY_ARTI_IDX = 0
  cPrg = "gsag_kda"
  cComment = "Dati di riga"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CLASSE = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_DAVOCCOS = space(15)
  w_DAVOCRIC = space(15)
  w_DACENCOS = space(15)
  w_DACENRIC = space(15)
  w_TIPATT = space(10)
  w_DACODCOM = space(15)
  w_DAATTIVI = space(15)
  w_DA_SEGNO = space(1)
  w_CCDESPIA = space(40)
  w_DAINICOM = ctod('  /  /  ')
  w_DAFINCOM = ctod('  /  /  ')
  w_DESVOC = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_TIPVOC = space(1)
  w_CODCOS = space(5)
  w_VOCOBSO = ctod('  /  /  ')
  w_DESVOR = space(40)
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_VOCTIP = space(1)
  w_TIPVOR = space(1)
  w_FLDANA = space(1)
  w_CENOBSO = ctod('  /  /  ')
  w_DAATTIVI = space(15)
  w_VOCECR = space(1)
  w_DASCONT1 = 0
  w_DASCONT2 = 0
  w_DASCONT3 = 0
  w_DASCONT4 = 0
  w_TIPCON = space(1)
  w_DACONCOD = space(15)
  w_CODESCON = space(50)
  w_OLCONCOD = space(15)
  w_DACONTRA = space(10)
  w_DACODMOD = space(10)
  w_DACODIMP = space(10)
  w_DACOCOMP = 0
  w_DARINNOV = 0
  w_CODESCON = space(50)
  w_MODESCRI = space(60)
  w_IMDESCRI = space(50)
  w_IMDESCON = space(50)
  w_IMROWORD = 0
  w_DAELMCNT = space(10)
  w_ELQTAMOV = 0
  w_ELQTACON = 0
  w_QTA__RES = 0
  w_CODCLI = space(15)
  w_DACODATT = space(20)
  w_DAUNIMIS = space(3)
  w_DADATINI = ctod('  /  /  ')
  w_DAPREZZO = 0
  w_DACODSER = space(20)
  w_FLUPDRIG = .F.
  w_DADESATT = space(40)
  w_AECODSER = space(41)
  w_AEDESART = space(40)
  w_ARCODSER = space(20)
  w_ARDESART = space(40)
  w_ATTEXIST = .F.
  w_COTIPATT = space(20)
  w_DOCEXIST = .F.
  w_COTIPDOC = space(20)
  w_CODATINI = ctod('  /  /  ')
  w_CODATFIN = ctod('  /  /  ')
  w_CCDESRIC = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kdaPag1","gsag_kda",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati di riga")
      .Pages(2).addobject("oPag","tgsag_kdaPag2","gsag_kda",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Elemento contratto")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDAVOCCOS_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='VOC_COST'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='CENCOST'
    this.cWorkTables[4]='ATTIVITA'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='CON_TRAM'
    this.cWorkTables[7]='CON_TRAS'
    this.cWorkTables[8]='MOD_ELEM'
    this.cWorkTables[9]='IMP_MAST'
    this.cWorkTables[10]='IMP_DETT'
    this.cWorkTables[11]='ELE_CONT'
    this.cWorkTables[12]='KEY_ARTI'
    return(this.OpenAllTables(12))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CLASSE=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_DAVOCCOS=space(15)
      .w_DAVOCRIC=space(15)
      .w_DACENCOS=space(15)
      .w_DACENRIC=space(15)
      .w_TIPATT=space(10)
      .w_DACODCOM=space(15)
      .w_DAATTIVI=space(15)
      .w_DA_SEGNO=space(1)
      .w_CCDESPIA=space(40)
      .w_DAINICOM=ctod("  /  /  ")
      .w_DAFINCOM=ctod("  /  /  ")
      .w_DESVOC=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPVOC=space(1)
      .w_CODCOS=space(5)
      .w_VOCOBSO=ctod("  /  /  ")
      .w_DESVOR=space(40)
      .w_FLANAL=space(1)
      .w_FLGCOM=space(1)
      .w_VOCTIP=space(1)
      .w_TIPVOR=space(1)
      .w_FLDANA=space(1)
      .w_CENOBSO=ctod("  /  /  ")
      .w_DAATTIVI=space(15)
      .w_VOCECR=space(1)
      .w_DASCONT1=0
      .w_DASCONT2=0
      .w_DASCONT3=0
      .w_DASCONT4=0
      .w_TIPCON=space(1)
      .w_DACONCOD=space(15)
      .w_CODESCON=space(50)
      .w_OLCONCOD=space(15)
      .w_DACONTRA=space(10)
      .w_DACODMOD=space(10)
      .w_DACODIMP=space(10)
      .w_DACOCOMP=0
      .w_DARINNOV=0
      .w_CODESCON=space(50)
      .w_MODESCRI=space(60)
      .w_IMDESCRI=space(50)
      .w_IMDESCON=space(50)
      .w_IMROWORD=0
      .w_DAELMCNT=space(10)
      .w_ELQTAMOV=0
      .w_ELQTACON=0
      .w_QTA__RES=0
      .w_CODCLI=space(15)
      .w_DACODATT=space(20)
      .w_DAUNIMIS=space(3)
      .w_DADATINI=ctod("  /  /  ")
      .w_DAPREZZO=0
      .w_DACODSER=space(20)
      .w_FLUPDRIG=.f.
      .w_DADESATT=space(40)
      .w_AECODSER=space(41)
      .w_AEDESART=space(40)
      .w_ARCODSER=space(20)
      .w_ARDESART=space(40)
      .w_ATTEXIST=.f.
      .w_COTIPATT=space(20)
      .w_DOCEXIST=.f.
      .w_COTIPDOC=space(20)
      .w_CODATINI=ctod("  /  /  ")
      .w_CODATFIN=ctod("  /  /  ")
      .w_CCDESRIC=space(40)
      .w_DAVOCCOS=oParentObject.w_DAVOCCOS
      .w_DAVOCRIC=oParentObject.w_DAVOCRIC
      .w_DACENCOS=oParentObject.w_DACENCOS
      .w_DACENRIC=oParentObject.w_DACENRIC
      .w_DACODCOM=oParentObject.w_DACODCOM
      .w_DAATTIVI=oParentObject.w_DAATTIVI
      .w_DA_SEGNO=oParentObject.w_DA_SEGNO
      .w_DAINICOM=oParentObject.w_DAINICOM
      .w_DAFINCOM=oParentObject.w_DAFINCOM
      .w_FLANAL=oParentObject.w_FLANAL
      .w_FLGCOM=oParentObject.w_FLGCOM
      .w_FLDANA=oParentObject.w_FLDANA
      .w_DAATTIVI=oParentObject.w_DAATTIVI
      .w_VOCECR=oParentObject.w_VOCECR
      .w_DASCONT1=oParentObject.w_DASCONT1
      .w_DASCONT2=oParentObject.w_DASCONT2
      .w_DASCONT3=oParentObject.w_DASCONT3
      .w_DASCONT4=oParentObject.w_DASCONT4
      .w_DACONCOD=oParentObject.w_DACONCOD
      .w_DACONTRA=oParentObject.w_DACONTRA
      .w_DACODMOD=oParentObject.w_DACODMOD
      .w_DACODIMP=oParentObject.w_DACODIMP
      .w_DACOCOMP=oParentObject.w_DACOCOMP
      .w_DARINNOV=oParentObject.w_DARINNOV
      .w_CODCLI=oParentObject.w_CODCLI
      .w_DACODATT=oParentObject.w_DACODATT
      .w_DAUNIMIS=oParentObject.w_DAUNIMIS
      .w_DAPREZZO=oParentObject.w_DAPREZZO
      .w_DADESATT=oParentObject.w_DADESATT
        .w_CLASSE = This.oparentobject.class
        .w_OBTEST = i_DATSYS
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_DAVOCCOS))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_DAVOCRIC))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_DACENCOS))
          .link_1_5('Full')
        endif
        .w_DACENRIC = .w_DACENRIC
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_DACENRIC))
          .link_1_6('Full')
        endif
        .w_TIPATT = 'A'
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_DACODCOM))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_DAATTIVI))
          .link_1_9('Full')
        endif
        .w_DA_SEGNO = 'A'
        .DoRTCalc(11,26,.f.)
        if not(empty(.w_DAATTIVI))
          .link_1_34('Full')
        endif
          .DoRTCalc(27,31,.f.)
        .w_TIPCON = 'C'
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_DACONCOD))
          .link_1_49('Full')
        endif
          .DoRTCalc(34,34,.f.)
        .w_OLCONCOD = .w_DACONCOD
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_DACONTRA))
          .link_2_10('Full')
        endif
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_DACODMOD))
          .link_2_11('Full')
        endif
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_DACODIMP))
          .link_2_12('Full')
        endif
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_DACOCOMP))
          .link_2_13('Full')
        endif
          .DoRTCalc(40,45,.f.)
        .w_DAELMCNT = .w_DACONTRA
        .DoRTCalc(46,46,.f.)
        if not(empty(.w_DAELMCNT))
          .link_2_20('Full')
        endif
          .DoRTCalc(47,48,.f.)
        .w_QTA__RES = .w_ELQTAMOV - .w_ELQTACON
          .DoRTCalc(50,52,.f.)
        .w_DADATINI = this.oParentObject.w_OB_TEST
          .DoRTCalc(54,54,.f.)
        .w_DACODSER = IIF(isAhe(), this.oParentObject.w_DACODICE, this.oParentObject.w_DACODATT )
        .w_FLUPDRIG = .F.
          .DoRTCalc(57,57,.f.)
        .w_AECODSER = IIF(isAhe(), this.oParentObject.w_DACODICE, this.oParentObject.w_DACODATT )
        .DoRTCalc(58,58,.f.)
        if not(empty(.w_AECODSER))
          .link_2_36('Full')
        endif
          .DoRTCalc(59,59,.f.)
        .w_ARCODSER = .w_DACODATT
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_ARCODSER))
          .link_2_38('Full')
        endif
      .oPgFrm.Page2.oPag.oObj_2_50.Calculate()
    endwith
    this.DoRTCalc(61,68,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_9.enabled = this.oPgFrm.Page2.oPag.oBtn_2_9.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_40.enabled = this.oPgFrm.Page2.oPag.oBtn_2_40.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_41.enabled = this.oPgFrm.Page2.oPag.oBtn_2_41.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_42.enabled = this.oPgFrm.Page2.oPag.oBtn_2_42.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_43.enabled = this.oPgFrm.Page2.oPag.oBtn_2_43.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DAVOCCOS=.w_DAVOCCOS
      .oParentObject.w_DAVOCRIC=.w_DAVOCRIC
      .oParentObject.w_DACENCOS=.w_DACENCOS
      .oParentObject.w_DACENRIC=.w_DACENRIC
      .oParentObject.w_DACODCOM=.w_DACODCOM
      .oParentObject.w_DAATTIVI=.w_DAATTIVI
      .oParentObject.w_DA_SEGNO=.w_DA_SEGNO
      .oParentObject.w_DAINICOM=.w_DAINICOM
      .oParentObject.w_DAFINCOM=.w_DAFINCOM
      .oParentObject.w_FLANAL=.w_FLANAL
      .oParentObject.w_FLGCOM=.w_FLGCOM
      .oParentObject.w_FLDANA=.w_FLDANA
      .oParentObject.w_DAATTIVI=.w_DAATTIVI
      .oParentObject.w_VOCECR=.w_VOCECR
      .oParentObject.w_DASCONT1=.w_DASCONT1
      .oParentObject.w_DASCONT2=.w_DASCONT2
      .oParentObject.w_DASCONT3=.w_DASCONT3
      .oParentObject.w_DASCONT4=.w_DASCONT4
      .oParentObject.w_DACONCOD=.w_DACONCOD
      .oParentObject.w_DACONTRA=.w_DACONTRA
      .oParentObject.w_DACODMOD=.w_DACODMOD
      .oParentObject.w_DACODIMP=.w_DACODIMP
      .oParentObject.w_DACOCOMP=.w_DACOCOMP
      .oParentObject.w_DARINNOV=.w_DARINNOV
      .oParentObject.w_CODCLI=.w_CODCLI
      .oParentObject.w_DACODATT=.w_DACODATT
      .oParentObject.w_DAUNIMIS=.w_DAUNIMIS
      .oParentObject.w_DAPREZZO=.w_DAPREZZO
      .oParentObject.w_DADESATT=.w_DADESATT
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- gsag_kda
    with this
       if .w_FLUPDRIG
         .oParentObject.NotifyEvent('AggiornaAhr')
       endif
       if EMPTY(.oParentObject.w_DAPREZZO)
         .oParentObject.w_DAPREZZO = .w_DAPREZZO
       endif
    endwith
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CLASSE = This.oparentobject.class
            .w_OBTEST = i_DATSYS
        .DoRTCalc(3,6,.t.)
            .w_TIPATT = 'A'
        .DoRTCalc(8,35,.t.)
          .link_2_10('Full')
          .link_2_11('Full')
          .link_2_12('Full')
          .link_2_13('Full')
        .DoRTCalc(40,45,.t.)
            .w_DAELMCNT = .w_DACONTRA
          .link_2_20('Full')
        .DoRTCalc(47,48,.t.)
            .w_QTA__RES = .w_ELQTAMOV - .w_ELQTACON
        .DoRTCalc(50,52,.t.)
            .w_DADATINI = this.oParentObject.w_OB_TEST
        .DoRTCalc(54,54,.t.)
            .w_DACODSER = IIF(isAhe(), this.oParentObject.w_DACODICE, this.oParentObject.w_DACODATT )
        .DoRTCalc(56,57,.t.)
            .w_AECODSER = IIF(isAhe(), this.oParentObject.w_DACODICE, this.oParentObject.w_DACODATT )
          .link_2_36('Full')
        .DoRTCalc(59,59,.t.)
            .w_ARCODSER = .w_DACODATT
          .link_2_38('Full')
        .oPgFrm.Page2.oPag.oObj_2_50.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(61,68,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_50.Calculate()
    endwith
  return

  proc Calculate_COEYXLDVLH()
    with this
          * --- Aggiorna prezzo
          gsag_bdd(this;
              ,'D';
              ,"";
             )
    endwith
  endproc
  proc Calculate_WXHVPRYZFW()
    with this
          * --- Elimina informazioni abbinamento elemento contratto
          .w_DACONTRA = Space(10)
          .link_2_10('Full')
          .w_DACODMOD = Space(10)
          .link_2_11('Full')
          .w_DACODIMP = Space(10)
          .link_2_12('Full')
          .w_DACOCOMP = 0
          .link_2_13('Full')
          .w_DARINNOV = 0
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDAVOCCOS_1_3.enabled = this.oPgFrm.Page1.oPag.oDAVOCCOS_1_3.mCond()
    this.oPgFrm.Page1.oPag.oDAVOCRIC_1_4.enabled = this.oPgFrm.Page1.oPag.oDAVOCRIC_1_4.mCond()
    this.oPgFrm.Page1.oPag.oDACENCOS_1_5.enabled = this.oPgFrm.Page1.oPag.oDACENCOS_1_5.mCond()
    this.oPgFrm.Page1.oPag.oDACODCOM_1_8.enabled = this.oPgFrm.Page1.oPag.oDACODCOM_1_8.mCond()
    this.oPgFrm.Page1.oPag.oDAATTIVI_1_9.enabled = this.oPgFrm.Page1.oPag.oDAATTIVI_1_9.mCond()
    this.oPgFrm.Page1.oPag.oDAATTIVI_1_34.enabled = this.oPgFrm.Page1.oPag.oDAATTIVI_1_34.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_24.enabled = this.oPgFrm.Page2.oPag.oBtn_2_24.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_42.enabled = this.oPgFrm.Page2.oPag.oBtn_2_42.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_43.enabled = this.oPgFrm.Page2.oPag.oBtn_2_43.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(Upper(this.w_CLASSE)='TGSAG_KTM' OR Isalt())
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Elemento contratto"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.oPgFrm.Page1.oPag.oDACODCOM_1_8.visible=!this.oPgFrm.Page1.oPag.oDACODCOM_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDAATTIVI_1_9.visible=!this.oPgFrm.Page1.oPag.oDAATTIVI_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oDAATTIVI_1_34.visible=!this.oPgFrm.Page1.oPag.oDAATTIVI_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oDASCONT1_1_40.visible=!this.oPgFrm.Page1.oPag.oDASCONT1_1_40.mHide()
    this.oPgFrm.Page1.oPag.oDASCONT2_1_41.visible=!this.oPgFrm.Page1.oPag.oDASCONT2_1_41.mHide()
    this.oPgFrm.Page1.oPag.oDASCONT3_1_42.visible=!this.oPgFrm.Page1.oPag.oDASCONT3_1_42.mHide()
    this.oPgFrm.Page1.oPag.oDASCONT4_1_43.visible=!this.oPgFrm.Page1.oPag.oDASCONT4_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oDACONCOD_1_49.visible=!this.oPgFrm.Page1.oPag.oDACONCOD_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oCODESCON_1_51.visible=!this.oPgFrm.Page1.oPag.oCODESCON_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_9.visible=!this.oPgFrm.Page2.oPag.oBtn_2_9.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_24.visible=!this.oPgFrm.Page2.oPag.oBtn_2_24.mHide()
    this.oPgFrm.Page2.oPag.oAECODSER_2_36.visible=!this.oPgFrm.Page2.oPag.oAECODSER_2_36.mHide()
    this.oPgFrm.Page2.oPag.oAEDESART_2_37.visible=!this.oPgFrm.Page2.oPag.oAEDESART_2_37.mHide()
    this.oPgFrm.Page2.oPag.oARCODSER_2_38.visible=!this.oPgFrm.Page2.oPag.oARCODSER_2_38.mHide()
    this.oPgFrm.Page2.oPag.oARDESART_2_39.visible=!this.oPgFrm.Page2.oPag.oARDESART_2_39.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_42.visible=!this.oPgFrm.Page2.oPag.oBtn_2_42.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_43.visible=!this.oPgFrm.Page2.oPag.oBtn_2_43.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Update End")
          .Calculate_COEYXLDVLH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Disabbina")
          .Calculate_WXHVPRYZFW()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_50.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DAVOCCOS
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAVOCCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_DAVOCCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_DAVOCCOS))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAVOCCOS)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAVOCCOS) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oDAVOCCOS_1_3'),i_cWhere,'GSCA_AVC',"Voci di costo o ricavo",'GSVE_KDA.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAVOCCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_DAVOCCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_DAVOCCOS)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAVOCCOS = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(5))
      this.w_VOCOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DAVOCCOS = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_TIPVOC = space(1)
      this.w_CODCOS = space(5)
      this.w_VOCOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOC='C' AND ((.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO) and isahe()) or (! isahe() and CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
        endif
        this.w_DAVOCCOS = space(15)
        this.w_DESVOC = space(40)
        this.w_TIPVOC = space(1)
        this.w_CODCOS = space(5)
        this.w_VOCOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAVOCCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAVOCRIC
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAVOCRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_DAVOCRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_DAVOCRIC))
          select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAVOCRIC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAVOCRIC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oDAVOCRIC_1_4'),i_cWhere,'GSCA_AVC',"Voci di costo o ricavo",'GSVE_KDA.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAVOCRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_DAVOCRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_DAVOCRIC)
            select VCCODICE,VCDESCRI,VCTIPVOC,VCTIPCOS,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAVOCRIC = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOR = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOR = NVL(_Link_.VCTIPVOC,space(1))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(5))
      this.w_VOCOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DAVOCRIC = space(15)
      endif
      this.w_DESVOR = space(40)
      this.w_TIPVOR = space(1)
      this.w_CODCOS = space(5)
      this.w_VOCOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOR='R' AND ((.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO) and isahe()) or (! isahe() and CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
        endif
        this.w_DAVOCRIC = space(15)
        this.w_DESVOR = space(40)
        this.w_TIPVOR = space(1)
        this.w_CODCOS = space(5)
        this.w_VOCOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAVOCRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACENCOS
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_DACENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_DACENCOS))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oDACENCOS_1_5'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_DACENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_DACENCOS)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACENCOS = space(15)
      endif
      this.w_CCDESPIA = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO) and isahe()) or (!isahe() and CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DACENCOS = space(15)
        this.w_CCDESPIA = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACENRIC
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACENRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_DACENRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_DACENRIC))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACENRIC)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACENRIC) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oDACENRIC_1_6'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACENRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_DACENRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_DACENRIC)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACENRIC = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESRIC = NVL(_Link_.CCDESPIA,space(40))
      this.w_CENOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACENRIC = space(15)
      endif
      this.w_CCDESRIC = space(40)
      this.w_CENOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DACENRIC = space(15)
        this.w_CCDESRIC = space(40)
        this.w_CENOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACENRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODCOM
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_DACODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_DACODCOM))
          select CNCODCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oDACODCOM_1_8'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_DACODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_DACODCOM)
            select CNCODCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACODCOM = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO) and isahe()) OR ( ! Isahe() and CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endif
        this.w_DACODCOM = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAATTIVI
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAATTIVI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_DAATTIVI)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DACODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_DACODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_DAATTIVI))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAATTIVI)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAATTIVI) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oDAATTIVI_1_9'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DACODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_DACODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAATTIVI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_DAATTIVI);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DACODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_DACODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_DAATTIVI)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAATTIVI = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_DAATTIVI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAATTIVI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAATTIVI
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAATTIVI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_DAATTIVI)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DACODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_DACODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_DAATTIVI))
          select ATCODCOM,ATTIPATT,ATCODATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAATTIVI)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DAATTIVI) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oDAATTIVI_1_34'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DACODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_DACODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAATTIVI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_DAATTIVI);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_DACODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_DACODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_DAATTIVI)
            select ATCODCOM,ATTIPATT,ATCODATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAATTIVI = NVL(_Link_.ATCODATT,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_DAATTIVI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAATTIVI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACONCOD
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACONCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_DACONCOD)+"%");
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COTIPCLF,CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COTIPCLF',this.w_TIPCON;
                     ,'CONUMERO',trim(this.w_DACONCOD))
          select COTIPCLF,CONUMERO,CODESCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COTIPCLF,CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACONCOD)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACONCOD) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(oSource.parent,'oDACONCOD_1_49'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select COTIPCLF,CONUMERO,CODESCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',oSource.xKey(1);
                       ,'CONUMERO',oSource.xKey(2))
            select COTIPCLF,CONUMERO,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACONCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COTIPCLF,CONUMERO,CODESCON";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_DACONCOD);
                   +" and COTIPCLF="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COTIPCLF',this.w_TIPCON;
                       ,'CONUMERO',this.w_DACONCOD)
            select COTIPCLF,CONUMERO,CODESCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACONCOD = NVL(_Link_.CONUMERO,space(15))
      this.w_CODESCON = NVL(_Link_.CODESCON,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_DACONCOD = space(15)
      endif
      this.w_CODESCON = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.COTIPCLF,1)+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACONCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACONTRA
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAS_IDX,3]
    i_lTable = "CON_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2], .t., this.CON_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COSERIAL,CODESCON,CODATINI,CODATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where COSERIAL="+cp_ToStrODBC(this.w_DACONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COSERIAL',this.w_DACONTRA)
            select COSERIAL,CODESCON,CODATINI,CODATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACONTRA = NVL(_Link_.COSERIAL,space(10))
      this.w_CODESCON = NVL(_Link_.CODESCON,space(50))
      this.w_CODATINI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_CODATFIN = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DACONTRA = space(10)
      endif
      this.w_CODESCON = space(50)
      this.w_CODATINI = ctod("  /  /  ")
      this.w_CODATFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.COSERIAL,1)
      cp_ShowWarn(i_cKey,this.CON_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODMOD
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_ELEM_IDX,3]
    i_lTable = "MOD_ELEM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2], .t., this.MOD_ELEM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCODICE,MODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_DACODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCODICE',this.w_DACODMOD)
            select MOCODICE,MODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODMOD = NVL(_Link_.MOCODICE,space(10))
      this.w_MODESCRI = NVL(_Link_.MODESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_DACODMOD = space(10)
      endif
      this.w_MODESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_ELEM_IDX,2])+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_ELEM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODIMP
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_MAST_IDX,3]
    i_lTable = "IMP_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2], .t., this.IMP_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,IMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where IMCODICE="+cp_ToStrODBC(this.w_DACODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_DACODIMP)
            select IMCODICE,IMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODIMP = NVL(_Link_.IMCODICE,space(10))
      this.w_IMDESCRI = NVL(_Link_.IMDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_DACODIMP = space(10)
      endif
      this.w_IMDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_MAST_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)
      cp_ShowWarn(i_cKey,this.IMP_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACOCOMP
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.IMP_DETT_IDX,3]
    i_lTable = "IMP_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2], .t., this.IMP_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACOCOMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACOCOMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IMCODICE,CPROWNUM,IMDESCON,CPROWORD";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_DACOCOMP);
                   +" and IMCODICE="+cp_ToStrODBC(this.w_DACODIMP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IMCODICE',this.w_DACODIMP;
                       ,'CPROWNUM',this.w_DACOCOMP)
            select IMCODICE,CPROWNUM,IMDESCON,CPROWORD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACOCOMP = NVL(_Link_.CPROWNUM,0)
      this.w_IMDESCON = NVL(_Link_.IMDESCON,space(50))
      this.w_IMROWORD = NVL(_Link_.CPROWORD,0)
    else
      if i_cCtrl<>'Load'
        this.w_DACOCOMP = 0
      endif
      this.w_IMDESCON = space(50)
      this.w_IMROWORD = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.IMP_DETT_IDX,2])+'\'+cp_ToStr(_Link_.IMCODICE,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.IMP_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACOCOMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DAELMCNT
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ELE_CONT_IDX,3]
    i_lTable = "ELE_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2], .t., this.ELE_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAELMCNT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAELMCNT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODMOD,ELCODIMP,ELCODCOM,ELRINNOV,ELCONTRA,ELQTAMOV,ELQTACON";
                   +" from "+i_cTable+" "+i_lTable+" where ELCONTRA="+cp_ToStrODBC(this.w_DAELMCNT);
                   +" and ELCODMOD="+cp_ToStrODBC(this.w_DACODMOD);
                   +" and ELCODIMP="+cp_ToStrODBC(this.w_DACODIMP);
                   +" and ELCODCOM="+cp_ToStrODBC(this.w_DACOCOMP);
                   +" and ELRINNOV="+cp_ToStrODBC(this.w_DARINNOV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODMOD',this.w_DACODMOD;
                       ,'ELCODIMP',this.w_DACODIMP;
                       ,'ELCODCOM',this.w_DACOCOMP;
                       ,'ELRINNOV',this.w_DARINNOV;
                       ,'ELCONTRA',this.w_DAELMCNT)
            select ELCODMOD,ELCODIMP,ELCODCOM,ELRINNOV,ELCONTRA,ELQTAMOV,ELQTACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAELMCNT = NVL(_Link_.ELCONTRA,space(10))
      this.w_ELQTAMOV = NVL(_Link_.ELQTAMOV,0)
      this.w_ELQTACON = NVL(_Link_.ELQTACON,0)
    else
      if i_cCtrl<>'Load'
        this.w_DAELMCNT = space(10)
      endif
      this.w_ELQTAMOV = 0
      this.w_ELQTACON = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ELE_CONT_IDX,2])+'\'+cp_ToStr(_Link_.ELCODMOD,1)+'\'+cp_ToStr(_Link_.ELCODIMP,1)+'\'+cp_ToStr(_Link_.ELCODCOM,1)+'\'+cp_ToStr(_Link_.ELRINNOV,1)+'\'+cp_ToStr(_Link_.ELCONTRA,1)
      cp_ShowWarn(i_cKey,this.ELE_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAELMCNT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AECODSER
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AECODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AECODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_AECODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_AECODSER)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AECODSER = NVL(_Link_.CACODICE,space(41))
      this.w_AEDESART = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_AECODSER = space(41)
      endif
      this.w_AEDESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AECODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCODSER
  func Link_2_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODSER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODSER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_ARCODSER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_ARCODSER)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODSER = NVL(_Link_.CACODICE,space(20))
      this.w_ARDESART = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODSER = space(20)
      endif
      this.w_ARDESART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODSER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDAVOCCOS_1_3.value==this.w_DAVOCCOS)
      this.oPgFrm.Page1.oPag.oDAVOCCOS_1_3.value=this.w_DAVOCCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oDAVOCRIC_1_4.value==this.w_DAVOCRIC)
      this.oPgFrm.Page1.oPag.oDAVOCRIC_1_4.value=this.w_DAVOCRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDACENCOS_1_5.value==this.w_DACENCOS)
      this.oPgFrm.Page1.oPag.oDACENCOS_1_5.value=this.w_DACENCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oDACENRIC_1_6.value==this.w_DACENRIC)
      this.oPgFrm.Page1.oPag.oDACENRIC_1_6.value=this.w_DACENRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODCOM_1_8.value==this.w_DACODCOM)
      this.oPgFrm.Page1.oPag.oDACODCOM_1_8.value=this.w_DACODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDAATTIVI_1_9.value==this.w_DAATTIVI)
      this.oPgFrm.Page1.oPag.oDAATTIVI_1_9.value=this.w_DAATTIVI
    endif
    if not(this.oPgFrm.Page1.oPag.oDA_SEGNO_1_10.RadioValue()==this.w_DA_SEGNO)
      this.oPgFrm.Page1.oPag.oDA_SEGNO_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA_1_11.value==this.w_CCDESPIA)
      this.oPgFrm.Page1.oPag.oCCDESPIA_1_11.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oDAINICOM_1_12.value==this.w_DAINICOM)
      this.oPgFrm.Page1.oPag.oDAINICOM_1_12.value=this.w_DAINICOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDAFINCOM_1_13.value==this.w_DAFINCOM)
      this.oPgFrm.Page1.oPag.oDAFINCOM_1_13.value=this.w_DAFINCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVOC_1_16.value==this.w_DESVOC)
      this.oPgFrm.Page1.oPag.oDESVOC_1_16.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVOR_1_21.value==this.w_DESVOR)
      this.oPgFrm.Page1.oPag.oDESVOR_1_21.value=this.w_DESVOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDAATTIVI_1_34.value==this.w_DAATTIVI)
      this.oPgFrm.Page1.oPag.oDAATTIVI_1_34.value=this.w_DAATTIVI
    endif
    if not(this.oPgFrm.Page1.oPag.oDASCONT1_1_40.value==this.w_DASCONT1)
      this.oPgFrm.Page1.oPag.oDASCONT1_1_40.value=this.w_DASCONT1
    endif
    if not(this.oPgFrm.Page1.oPag.oDASCONT2_1_41.value==this.w_DASCONT2)
      this.oPgFrm.Page1.oPag.oDASCONT2_1_41.value=this.w_DASCONT2
    endif
    if not(this.oPgFrm.Page1.oPag.oDASCONT3_1_42.value==this.w_DASCONT3)
      this.oPgFrm.Page1.oPag.oDASCONT3_1_42.value=this.w_DASCONT3
    endif
    if not(this.oPgFrm.Page1.oPag.oDASCONT4_1_43.value==this.w_DASCONT4)
      this.oPgFrm.Page1.oPag.oDASCONT4_1_43.value=this.w_DASCONT4
    endif
    if not(this.oPgFrm.Page1.oPag.oDACONCOD_1_49.value==this.w_DACONCOD)
      this.oPgFrm.Page1.oPag.oDACONCOD_1_49.value=this.w_DACONCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_1_51.value==this.w_CODESCON)
      this.oPgFrm.Page1.oPag.oCODESCON_1_51.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDACONTRA_2_10.value==this.w_DACONTRA)
      this.oPgFrm.Page2.oPag.oDACONTRA_2_10.value=this.w_DACONTRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDACODMOD_2_11.value==this.w_DACODMOD)
      this.oPgFrm.Page2.oPag.oDACODMOD_2_11.value=this.w_DACODMOD
    endif
    if not(this.oPgFrm.Page2.oPag.oDACODIMP_2_12.value==this.w_DACODIMP)
      this.oPgFrm.Page2.oPag.oDACODIMP_2_12.value=this.w_DACODIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oDARINNOV_2_14.value==this.w_DARINNOV)
      this.oPgFrm.Page2.oPag.oDARINNOV_2_14.value=this.w_DARINNOV
    endif
    if not(this.oPgFrm.Page2.oPag.oCODESCON_2_15.value==this.w_CODESCON)
      this.oPgFrm.Page2.oPag.oCODESCON_2_15.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oMODESCRI_2_16.value==this.w_MODESCRI)
      this.oPgFrm.Page2.oPag.oMODESCRI_2_16.value=this.w_MODESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oIMDESCRI_2_17.value==this.w_IMDESCRI)
      this.oPgFrm.Page2.oPag.oIMDESCRI_2_17.value=this.w_IMDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oIMDESCON_2_18.value==this.w_IMDESCON)
      this.oPgFrm.Page2.oPag.oIMDESCON_2_18.value=this.w_IMDESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oIMROWORD_2_19.value==this.w_IMROWORD)
      this.oPgFrm.Page2.oPag.oIMROWORD_2_19.value=this.w_IMROWORD
    endif
    if not(this.oPgFrm.Page2.oPag.oELQTAMOV_2_21.value==this.w_ELQTAMOV)
      this.oPgFrm.Page2.oPag.oELQTAMOV_2_21.value=this.w_ELQTAMOV
    endif
    if not(this.oPgFrm.Page2.oPag.oELQTACON_2_22.value==this.w_ELQTACON)
      this.oPgFrm.Page2.oPag.oELQTACON_2_22.value=this.w_ELQTACON
    endif
    if not(this.oPgFrm.Page2.oPag.oQTA__RES_2_23.value==this.w_QTA__RES)
      this.oPgFrm.Page2.oPag.oQTA__RES_2_23.value=this.w_QTA__RES
    endif
    if not(this.oPgFrm.Page2.oPag.oDAUNIMIS_2_28.value==this.w_DAUNIMIS)
      this.oPgFrm.Page2.oPag.oDAUNIMIS_2_28.value=this.w_DAUNIMIS
    endif
    if not(this.oPgFrm.Page2.oPag.oAECODSER_2_36.value==this.w_AECODSER)
      this.oPgFrm.Page2.oPag.oAECODSER_2_36.value=this.w_AECODSER
    endif
    if not(this.oPgFrm.Page2.oPag.oAEDESART_2_37.value==this.w_AEDESART)
      this.oPgFrm.Page2.oPag.oAEDESART_2_37.value=this.w_AEDESART
    endif
    if not(this.oPgFrm.Page2.oPag.oARCODSER_2_38.value==this.w_ARCODSER)
      this.oPgFrm.Page2.oPag.oARCODSER_2_38.value=this.w_ARCODSER
    endif
    if not(this.oPgFrm.Page2.oPag.oARDESART_2_39.value==this.w_ARDESART)
      this.oPgFrm.Page2.oPag.oARDESART_2_39.value=this.w_ARDESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESRIC_1_55.value==this.w_CCDESRIC)
      this.oPgFrm.Page1.oPag.oCCDESRIC_1_55.value=this.w_CCDESRIC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPVOC='C' AND ((.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO) and isahe()) or (! isahe() and CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.))))  and (.w_FLANAL='S' OR (.w_FLDANA='S' or .w_FLGCOM='S') )  and not(empty(.w_DAVOCCOS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAVOCCOS_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
          case   not(.w_TIPVOR='R' AND ((.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO) and isahe()) or (! isahe() and CHKDTOBS(.w_VOCOBSO,.w_OBTEST,"Voce di Ricavo obsoleta!",.F.))))  and (.w_FLDANA='S' OR .w_FLGCOM='S' or Isalt())  and not(empty(.w_DAVOCRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAVOCRIC_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di ricavo incongruente o obsoleto")
          case   not((.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO) and isahe()) or (!isahe() and CHKDTOBS(.w_CENOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.)))  and ((.w_FLANAL='S' or .w_FLDANA='S' ))  and not(empty(.w_DACENCOS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACENCOS_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CENOBSO>.w_OBTEST OR EMPTY(.w_CENOBSO))  and not(empty(.w_DACENRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACENRIC_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((.w_VOCOBSO>.w_OBTEST OR EMPTY(.w_VOCOBSO) and isahe()) OR ( ! Isahe() and CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.))))  and not(Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP')  and (((g_PERCAN='S' AND (.w_FLANAL='S' or .w_FLDANA='S' )) OR (g_COMM='S' AND .w_FLGCOM='S')))  and not(empty(.w_DACODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACODCOM_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa incongruente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_kdaPag1 as StdContainer
  Width  = 668
  height = 320
  stdWidth  = 668
  stdheight = 320
  resizeXpos=418
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDAVOCCOS_1_3 as StdField with uid="BXYHJRRPSK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DAVOCCOS", cQueryName = "DAVOCCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di ricavo incongruente o obsoleto",;
    ToolTipText = "Codice voce di costo",;
    HelpContextID = 224756087,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=148, Top=16, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_DAVOCCOS"

  func oDAVOCCOS_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLANAL='S' OR (.w_FLDANA='S' or .w_FLGCOM='S') )
    endwith
   endif
  endfunc

  proc oDAVOCCOS_1_3.mBefore
    with this.Parent.oContained
      .w_VOCTIP='C'
    endwith
  endproc

  func oDAVOCCOS_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAVOCCOS_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAVOCCOS_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oDAVOCCOS_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo o ricavo",'GSVE_KDA.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oDAVOCCOS_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_DAVOCCOS
     i_obj.ecpSave()
  endproc

  add object oDAVOCRIC_1_4 as StdField with uid="QXWDUFCHVC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DAVOCRIC", cQueryName = "DAVOCRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di ricavo incongruente o obsoleto",;
    ToolTipText = "Codice voce di costo",;
    HelpContextID = 26902137,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=148, Top=47, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_DAVOCRIC"

  func oDAVOCRIC_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDANA='S' OR .w_FLGCOM='S' or Isalt())
    endwith
   endif
  endfunc

  proc oDAVOCRIC_1_4.mBefore
    with this.Parent.oContained
      .w_VOCTIP='R'
    endwith
  endproc

  func oDAVOCRIC_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAVOCRIC_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAVOCRIC_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oDAVOCRIC_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo o ricavo",'GSVE_KDA.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oDAVOCRIC_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_DAVOCRIC
     i_obj.ecpSave()
  endproc

  add object oDACENCOS_1_5 as StdField with uid="GWCBQUFVPT",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DACENCOS", cQueryName = "DACENCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 213954935,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=148, Top=77, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_DACENCOS"

  func oDACENCOS_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_FLANAL='S' or .w_FLDANA='S' ))
    endwith
   endif
  endfunc

  func oDACENCOS_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACENCOS_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACENCOS_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oDACENCOS_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oDACENCOS_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_DACENCOS
     i_obj.ecpSave()
  endproc

  add object oDACENRIC_1_6 as StdField with uid="SLDUOBYZOH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DACENRIC", cQueryName = "DACENRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 37703289,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=148, Top=106, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_DACENRIC"

  func oDACENRIC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACENRIC_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACENRIC_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oDACENRIC_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oDACENRIC_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_DACENRIC
     i_obj.ecpSave()
  endproc

  add object oDACODCOM_1_8 as StdField with uid="CCYDSSPMTJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DACODCOM", cQueryName = "DACODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente o obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 223785341,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=148, Top=145, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_DACODCOM"

  func oDACODCOM_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (((g_PERCAN='S' AND (.w_FLANAL='S' or .w_FLDANA='S' )) OR (g_COMM='S' AND .w_FLGCOM='S')))
    endwith
   endif
  endfunc

  func oDACODCOM_1_8.mHide()
    with this.Parent.oContained
      return (Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP')
    endwith
  endfunc

  func oDACODCOM_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
      if .not. empty(.w_DAATTIVI)
        bRes2=.link_1_9('Full')
      endif
      if .not. empty(.w_DAATTIVI)
        bRes2=.link_1_34('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDACODCOM_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODCOM_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oDACODCOM_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oDACODCOM_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_DACODCOM
     i_obj.ecpSave()
  endproc

  add object oDAATTIVI_1_9 as StdField with uid="LTBYOWJTQX",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DAATTIVI", cQueryName = "DAATTIVI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 162410111,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=536, Top=145, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_DACODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_DAATTIVI"

  func oDAATTIVI_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DACODCOM) AND g_COMM='S' AND .w_FLGCOM='S' )
    endwith
   endif
  endfunc

  func oDAATTIVI_1_9.mHide()
    with this.Parent.oContained
      return (Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP')
    endwith
  endfunc

  func oDAATTIVI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAATTIVI_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAATTIVI_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_DACODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_DACODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oDAATTIVI_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oDAATTIVI_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_DACODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_DAATTIVI
     i_obj.ecpSave()
  endproc


  add object oDA_SEGNO_1_10 as StdCombo with uid="LQUSYTRUTR",rtseq=10,rtrep=.f.,left=536,top=177,width=128,height=21;
    , ToolTipText = "Segno importo del movimento di analitica";
    , HelpContextID = 155251067;
    , cFormVar="w_DA_SEGNO",RowSource=""+"Dare,"+"Avere", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDA_SEGNO_1_10.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oDA_SEGNO_1_10.GetRadio()
    this.Parent.oContained.w_DA_SEGNO = this.RadioValue()
    return .t.
  endfunc

  func oDA_SEGNO_1_10.SetRadio()
    this.Parent.oContained.w_DA_SEGNO=trim(this.Parent.oContained.w_DA_SEGNO)
    this.value = ;
      iif(this.Parent.oContained.w_DA_SEGNO=='D',1,;
      iif(this.Parent.oContained.w_DA_SEGNO=='A',2,;
      0))
  endfunc

  add object oCCDESPIA_1_11 as StdField with uid="OQBJBQEOLA",rtseq=11,rtrep=.t.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 9396327,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=279, Top=77, InputMask=replicate('X',40)

  add object oDAINICOM_1_12 as StdField with uid="ZDLRGEENLV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DAINICOM", cQueryName = "DAINICOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio competenza",;
    HelpContextID = 218583421,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=148, Top=176

  add object oDAFINCOM_1_13 as StdField with uid="UETVXIOEZO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DAFINCOM", cQueryName = "DAFINCOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine competenza",;
    HelpContextID = 213680509,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=294, Top=176

  add object oDESVOC_1_16 as StdField with uid="RRXGULMIJI",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 211725770,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=279, Top=16, InputMask=replicate('X',40)

  add object oDESVOR_1_21 as StdField with uid="TBESVEIPFD",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESVOR", cQueryName = "DESVOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 228502986,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=279, Top=47, InputMask=replicate('X',40)

  add object oDAATTIVI_1_34 as StdField with uid="FVFYUHJGST",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DAATTIVI", cQueryName = "DAATTIVI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit� di commessa",;
    HelpContextID = 162410111,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=148, Top=145, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_DACODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_DAATTIVI"

  func oDAATTIVI_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DACODCOM) AND g_COMM='S' AND (.w_FLGCOM='S' or isAhe()))
    endwith
   endif
  endfunc

  func oDAATTIVI_1_34.mHide()
    with this.Parent.oContained
      return (Upper(.w_CLASSE)<>'TCGSAG_MPR' OR Upper(.w_CLASSE)<>'TCGSAG_MPP')
    endwith
  endfunc

  func oDAATTIVI_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAATTIVI_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAATTIVI_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_DACODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_DACODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oDAATTIVI_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oDAATTIVI_1_34.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_DACODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_DAATTIVI
     i_obj.ecpSave()
  endproc


  add object oBtn_1_37 as StdButton with uid="ERHOXVEEGI",left=561, top=272, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Ok";
    , HelpContextID = 186009830;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_38 as StdButton with uid="TKCMXZXILU",left=616, top=272, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193298502;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDASCONT1_1_40 as StdField with uid="KFPXATWEED",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DASCONT1", cQueryName = "DASCONT1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 240012903,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=148, Top=207, cSayPict='"999.99"', cGetPict='"999.99"'

  func oDASCONT1_1_40.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oDASCONT2_1_41 as StdField with uid="VYHZKSXGRH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DASCONT2", cQueryName = "DASCONT2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 240012904,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=214, Top=207, cSayPict='"999.99"', cGetPict='"999.99"'

  func oDASCONT2_1_41.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oDASCONT3_1_42 as StdField with uid="WAROBDGRJZ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DASCONT3", cQueryName = "DASCONT3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 240012905,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=280, Top=207, cSayPict='"999.99"', cGetPict='"999.99"'

  func oDASCONT3_1_42.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oDASCONT4_1_43 as StdField with uid="LCUWGTVUDY",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DASCONT4", cQueryName = "DASCONT4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 240012906,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=346, Top=207, cSayPict='"999.99"', cGetPict='"999.99"'

  func oDASCONT4_1_43.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oDACONCOD_1_49 as StdField with uid="YBAEYYKJZA",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DACONCOD", cQueryName = "DACONCOD",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 213299590,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=148, Top=233, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CON_TRAM", oKey_1_1="COTIPCLF", oKey_1_2="this.w_TIPCON", oKey_2_1="CONUMERO", oKey_2_2="this.w_DACONCOD"

  func oDACONCOD_1_49.mHide()
    with this.Parent.oContained
      return (Isalt() OR Upper(.w_CLASSE)='TGSAG_KTM')
    endwith
  endfunc

  func oDACONCOD_1_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACONCOD_1_49.ecpDrop(oSource)
    this.Parent.oContained.link_1_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACONCOD_1_49.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_TRAM_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"COTIPCLF="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CON_TRAM','*','COTIPCLF,CONUMERO',cp_AbsName(this.parent,'oDACONCOD_1_49'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCODESCON_1_51 as StdField with uid="ZSUWGAJJHR",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208704396,;
   bGlobalFont=.t.,;
    Height=21, Width=382, Left=282, Top=233, InputMask=replicate('X',50)

  func oCODESCON_1_51.mHide()
    with this.Parent.oContained
      return (Isalt() OR Upper(.w_CLASSE)='TGSAG_KTM')
    endwith
  endfunc

  add object oCCDESRIC_1_55 as StdField with uid="TZYJTUVYJB",rtseq=68,rtrep=.t.,;
    cFormVar = "w_CCDESRIC", cQueryName = "CCDESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42950761,;
   bGlobalFont=.t.,;
    Height=21, Width=385, Left=279, Top=106, InputMask=replicate('X',40)

  add object oStr_1_14 as StdString with uid="FRRXLKVZKC",Visible=.t., Left=51, Top=48,;
    Alignment=1, Width=91, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="JJMWTYGWMS",Visible=.t., Left=51, Top=18,;
    Alignment=1, Width=91, Height=18,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="NDNECIPGFN",Visible=.t., Left=51, Top=79,;
    Alignment=1, Width=91, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="HTOLFADPTS",Visible=.t., Left=51, Top=147,;
    Alignment=1, Width=91, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP' or Not Isalt())
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="WOCUVZREIT",Visible=.t., Left=44, Top=178,;
    Alignment=1, Width=98, Height=18,;
    Caption="Competenza da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="FDIWWVGPUH",Visible=.t., Left=252, Top=178,;
    Alignment=1, Width=38, Height=18,;
    Caption="a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="TSGTDCZUUU",Visible=.t., Left=441, Top=178,;
    Alignment=1, Width=91, Height=18,;
    Caption="Segno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="THZJZYTFBJ",Visible=.t., Left=441, Top=147,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (Upper(.w_CLASSE)='TCGSAG_MPR' OR Upper(.w_CLASSE)='TCGSAG_MPP')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="QWXKAAKSPF",Visible=.t., Left=51, Top=147,;
    Alignment=1, Width=91, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (Upper(.w_CLASSE)<>'TCGSAG_MPR' OR Upper(.w_CLASSE)<>'TCGSAG_MPP')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="GDTZWOLXII",Visible=.t., Left=51, Top=147,;
    Alignment=1, Width=91, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (Upper(.w_CLASSE)='TCGSAG_MPR'  OR Upper(.w_CLASSE)='TCGSAG_MPP' or Isalt())
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="EAXHGGLWIU",Visible=.t., Left=44, Top=209,;
    Alignment=1, Width=98, Height=18,;
    Caption="Sconti/magg.:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="JCASMVYLRE",Visible=.t., Left=205, Top=209,;
    Alignment=0, Width=9, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="YPMLMBCNYO",Visible=.t., Left=271, Top=209,;
    Alignment=0, Width=9, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="GUKRHQXEOL",Visible=.t., Left=337, Top=209,;
    Alignment=0, Width=9, Height=18,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="NKCVDYAYHS",Visible=.t., Left=27, Top=235,;
    Alignment=1, Width=115, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (Isalt() OR Upper(.w_CLASSE)='TGSAG_KTM')
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="FGAPOOVTHK",Visible=.t., Left=3, Top=379,;
    Alignment=0, Width=346, Height=19,;
    Caption="Lanciata da GSAG_MDA, GSAG_MPR,GSAG_KTM"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_56 as StdString with uid="JEGGTPWUNR",Visible=.t., Left=39, Top=109,;
    Alignment=1, Width=104, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsag_kdaPag2 as StdContainer
  Width  = 668
  height = 320
  stdWidth  = 668
  stdheight = 320
  resizeXpos=381
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_2_9 as StdButton with uid="XULRZUMNRR",left=251, top=213, width=48,height=45,;
    CpPicture="BMP\abbina.BMP", caption="", nPag=2;
    , ToolTipText = "Permette di abbinare un elemento contratto alla prestazione attuale";
    , HelpContextID = 211460346;
    , caption='\<Abbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_9.Click()
      with this.Parent.oContained
        GSAG_BDE(this.Parent.oContained,"ABBIN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!EMPTY(.w_DACONTRA))
     endwith
    endif
  endfunc

  add object oDACONTRA_2_10 as StdField with uid="SIASNDMDBU",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DACONTRA", cQueryName = "DACONTRA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 196522377,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=11, InputMask=replicate('X',10), cLinkFile="CON_TRAS", oKey_1_1="COSERIAL", oKey_1_2="this.w_DACONTRA"

  func oDACONTRA_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDACODMOD_2_11 as StdField with uid="BYGAMPTQED",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DACODMOD", cQueryName = "DACODMOD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 56013190,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=36, InputMask=replicate('X',10), cLinkFile="MOD_ELEM", oKey_1_1="MOCODICE", oKey_1_2="this.w_DACODMOD"

  func oDACODMOD_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DAELMCNT)
        bRes2=.link_2_20('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDACODIMP_2_12 as StdField with uid="LFGJEDROAJ",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DACODIMP", cQueryName = "DACODIMP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 123122042,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=61, InputMask=replicate('X',10), cLinkFile="IMP_MAST", oKey_1_1="IMCODICE", oKey_1_2="this.w_DACODIMP"

  func oDACODIMP_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DACOCOMP)
        bRes2=.link_2_13('Full')
      endif
      if .not. empty(.w_DAELMCNT)
        bRes2=.link_2_20('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDARINNOV_2_14 as StdField with uid="QSCQLZNPJR",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DARINNOV", cQueryName = "DARINNOV",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 29081972,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=111

  func oDARINNOV_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_DAELMCNT)
        bRes2=.link_2_20('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODESCON_2_15 as StdField with uid="PTSZYUUGUV",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208704396,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=231, Top=11, InputMask=replicate('X',50)

  add object oMODESCRI_2_16 as StdField with uid="BIZHFQLCSX",rtseq=42,rtrep=.f.,;
    cFormVar = "w_MODESCRI", cQueryName = "MODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 208704241,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=231, Top=36, InputMask=replicate('X',60)

  add object oIMDESCRI_2_17 as StdField with uid="HSYGHCCPDU",rtseq=43,rtrep=.f.,;
    cFormVar = "w_IMDESCRI", cQueryName = "IMDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208704817,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=231, Top=61, InputMask=replicate('X',50)

  add object oIMDESCON_2_18 as StdField with uid="JQMBWCWMKC",rtseq=44,rtrep=.f.,;
    cFormVar = "w_IMDESCON", cQueryName = "IMDESCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208704812,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=231, Top=86, InputMask=replicate('X',50)

  add object oIMROWORD_2_19 as StdField with uid="EJXDUJHYSZ",rtseq=45,rtrep=.f.,;
    cFormVar = "w_IMROWORD", cQueryName = "IMROWORD",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 2471222,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=86

  add object oELQTAMOV_2_21 as StdField with uid="NIAHKQTYTL",rtseq=47,rtrep=.f.,;
    cFormVar = "w_ELQTAMOV", cQueryName = "ELQTAMOV",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 58771044,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=144, Top=186, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  add object oELQTACON_2_22 as StdField with uid="GWTFWEPNKA",rtseq=48,rtrep=.f.,;
    cFormVar = "w_ELQTACON", cQueryName = "ELQTACON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 226543212,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=144, Top=211, cSayPict='"99999999.999"', cGetPict='"99999999.999"'

  add object oQTA__RES_2_23 as StdField with uid="YQXDOMDMXE",rtseq=49,rtrep=.f.,;
    cFormVar = "w_QTA__RES", cQueryName = "QTA__RES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 57229913,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=144, Top=236, cSayPict='"99999999.999"', cGetPict='"99999999.999"'


  add object oBtn_2_24 as StdButton with uid="TYFCXGJXKY",left=251, top=213, width=48,height=45,;
    CpPicture="BMP\disabbina.BMP", caption="", nPag=2;
    , ToolTipText = "Permette di disabbinare un elemento contratto alla prestazione attuale";
    , HelpContextID = 60718260;
    , caption='\<Disabbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_24.Click()
      this.parent.oContained.NotifyEvent("Disabbina")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (oParentObject.cFunction<>'Query')
      endwith
    endif
  endfunc

  func oBtn_2_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_DACONTRA))
     endwith
    endif
  endfunc

  add object oDAUNIMIS_2_28 as StdField with uid="RWLHCOWLKL",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DAUNIMIS", cQueryName = "DAUNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 217673353,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=161, InputMask=replicate('X',3)

  add object oAECODSER_2_36 as StdField with uid="ZVYPGPACJJ",rtseq=58,rtrep=.f.,;
    cFormVar = "w_AECODSER", cQueryName = "AECODSER",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(41), bMultilanguage =  .f.,;
    HelpContextID = 44651096,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=144, Top=136, InputMask=replicate('X',41), cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_AECODSER"

  func oAECODSER_2_36.mHide()
    with this.Parent.oContained
      return (!isAHE())
    endwith
  endfunc

  func oAECODSER_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oAEDESART_2_37 as StdField with uid="FMHTGZQYZP",rtseq=59,rtrep=.f.,;
    cFormVar = "w_AEDESART", cQueryName = "AEDESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 242261414,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=362, Top=136, InputMask=replicate('X',40)

  func oAEDESART_2_37.mHide()
    with this.Parent.oContained
      return (!isAHE())
    endwith
  endfunc

  add object oARCODSER_2_38 as StdField with uid="WSNSINAFTN",rtseq=60,rtrep=.f.,;
    cFormVar = "w_ARCODSER", cQueryName = "ARCODSER",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 44654424,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=144, Top=136, InputMask=replicate('X',20), cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_ARCODSER"

  func oARCODSER_2_38.mHide()
    with this.Parent.oContained
      return (isAHE())
    endwith
  endfunc

  func oARCODSER_2_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oARDESART_2_39 as StdField with uid="ZPVIHFHZDA",rtseq=61,rtrep=.f.,;
    cFormVar = "w_ARDESART", cQueryName = "ARDESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 242258086,;
   bGlobalFont=.t.,;
    Height=21, Width=362, Left=302, Top=136, InputMask=replicate('X',40)

  func oARDESART_2_39.mHide()
    with this.Parent.oContained
      return (isAHE())
    endwith
  endfunc


  add object oBtn_2_40 as StdButton with uid="LINCLCRJWI",left=561, top=236, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=2;
    , ToolTipText = "Ok";
    , HelpContextID = 186009830;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_40.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_41 as StdButton with uid="XMHKVPONCQ",left=616, top=236, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193298502;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_41.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_42 as StdButton with uid="KSUWNHATXF",left=561, top=184, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla visualizzazione delle attivit� generate";
    , HelpContextID = 228778096;
    , Caption='\<Vis. Att.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_42.Click()
      with this.Parent.oContained
        GSAG2BZM(this.Parent.oContained,"AO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_42.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_DACONTRA))
      endwith
    endif
  endfunc

  func oBtn_2_42.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_ATTEXIST)
     endwith
    endif
  endfunc


  add object oBtn_2_43 as StdButton with uid="CGHHCAFYSP",left=616, top=183, width=48,height=45,;
    CpPicture="bmp\dettagli.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla visualizzazione dei documenti generati";
    , HelpContextID = 228778096;
    , Caption='\<Vis. Doc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_43.Click()
      with this.Parent.oContained
        GSAG2BZM(this.Parent.oContained,"DO") 
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_DACONTRA))
      endwith
    endif
  endfunc

  func oBtn_2_43.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!.w_DOCEXIST)
     endwith
    endif
  endfunc


  add object oObj_2_50 as cp_runprogram with uid="WMCMDQULXA",left=332, top=309, width=220,height=19,;
    caption='GSAG2BZM(XR)',;
   bGlobalFont=.t.,;
    prg="GSAG2BZM('XR')",;
    cEvent = "Blank",;
    nPag=2;
    , ToolTipText = "Aggiorna gli elementi contratto";
    , HelpContextID = 11516723

  add object oStr_2_1 as StdString with uid="XGNOHOVNMK",Visible=.t., Left=5, Top=14,;
    Alignment=1, Width=134, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="ELZSHFBFOO",Visible=.t., Left=5, Top=37,;
    Alignment=1, Width=134, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="XSKMMQIAMO",Visible=.t., Left=5, Top=62,;
    Alignment=1, Width=134, Height=18,;
    Caption="Impianto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="LAMDYYUUEA",Visible=.t., Left=5, Top=88,;
    Alignment=1, Width=134, Height=18,;
    Caption="Componente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="ITQURESLBJ",Visible=.t., Left=5, Top=113,;
    Alignment=1, Width=134, Height=18,;
    Caption="Rinnovi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="LNBFHYAABV",Visible=.t., Left=5, Top=188,;
    Alignment=1, Width=134, Height=18,;
    Caption="Quantit� totale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="ABFZLPRRLP",Visible=.t., Left=5, Top=213,;
    Alignment=1, Width=134, Height=18,;
    Caption="Quantit� consumata:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="LPDEAPTJSU",Visible=.t., Left=5, Top=238,;
    Alignment=1, Width=134, Height=18,;
    Caption="Quantit� residua:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="UQBPADTBGU",Visible=.t., Left=5, Top=138,;
    Alignment=1, Width=134, Height=18,;
    Caption="Servizio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="QLFGKLYHSP",Visible=.t., Left=5, Top=163,;
    Alignment=1, Width=134, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kda','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
