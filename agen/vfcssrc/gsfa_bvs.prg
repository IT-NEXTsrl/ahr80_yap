* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsfa_bvs                                                        *
*              Gestione treeview                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-30                                                      *
* Last revis.: 2015-01-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOp
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsfa_bvs",oParentObject,m.pTipOp)
return(i_retval)

define class tgsfa_bvs as StdBatch
  * --- Local variables
  pTipOp = space(10)
  w_COUTCURS = space(100)
  w_CRIFTABLE = space(100)
  w_EXPKEY = space(100)
  w_EXPFIELD = space(100)
  w_INPCURS = space(100)
  w_CEXPTABLE = space(100)
  w_REPKEY = space(100)
  w_OTHERFLD = space(100)
  w_RIFKEY = space(100)
  w_POSIZ = 0
  w_LVLKEY = space(200)
  w_LASTPOINT = 0
  w_CODATTCUR = space(15)
  w_RELAZ = .NULL.
  w_TIPSTRLOC = space(1)
  w_CODCLI = space(15)
  w_NUMOCC = space(10)
  w_IDRICH = space(15)
  w_OBJ_ACE = .NULL.
  w_LEVSERIAL = space(10)
  w_EVNOMINA = space(15)
  w_EVRIFPER = space(254)
  w_EVSTATO = space(1)
  w_EVCODPRA = space(15)
  w_EVDATFIN = ctot("")
  w_EVDATINI = ctot("")
  w_EVORAEFF = ctot("")
  w_EVMINEFF = space(2)
  w_LEV__ANNO = space(4)
  w_EV__NOTE = space(0)
  w_EVIDRICH = space(15)
  w_EVSTARIC = space(1)
  w_OK = .f.
  w_RETVAL = 0
  w_SELMULT = .f.
  w_SELEZIONE = space(1)
  w_CODPRA = space(15)
  w_CURSORE = space(10)
  w_PADRE = .NULL.
  w_NOATT = .f.
  w_NONOM = .f.
  w_OK = .f.
  w_ATCAUATT = space(10)
  w_EVLOCALI = space(30)
  w_EV_PHONE = space(18)
  w_EVNUMCEL = space(20)
  w_EVCODPAR = space(5)
  w_EVRIFPER = space(254)
  w_EV_EMAIL = space(15)
  w_EV_EMPEC = space(15)
  w_EV__ANNO = space(4)
  w_ATSEREVE = space(10)
  w_PROG = .NULL.
  w_CODRAGG = space(10)
  w_TEPADEVE = space(5)
  w_OBJEVE = .NULL.
  w_MSG = space(0)
  w_oERRORLOG = .NULL.
  w_LOGERR = .f.
  w_oERRORLOG = .NULL.
  w_TMPC = space(100)
  w_AECODPRA = space(15)
  w_AEORIFIL = space(100)
  w_AEOGGETT = space(220)
  w_AE_TESTO = space(0)
  w_FL_SAVE = .f.
  w_AEPATHFI = space(254)
  w_TIPOALL = space(5)
  w_CLASALL = space(5)
  w_AETIALCP = space(1)
  w_AECLALCP = space(1)
  w_AETYPEAL = space(1)
  w_AENOMEAL = space(1)
  w_AEDESPRA = space(50)
  w_CNCODCAN = space(15)
  w_LSELMUL = .f.
  w_LGFNOMEFILE = space(20)
  w_CNDESCAN = space(100)
  w_CODPRE = space(20)
  w_RAGPRE = space(10)
  w_EVFLIDPA = space(1)
  w_MESS = space(100)
  w_OK = .f.
  * --- WorkFile variables
  TMP_STR_idx=0
  TIPEVENT_idx=0
  ANEVENTI_idx=0
  TIP_RAGG_idx=0
  ASS_ATEV_idx=0
  OFF_ATTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea il cursore da passare alla TreeView (da GSFA_KCE)
    do case
      case this.pTipop="Reload"
        * --- Aggiorna la Treeview
        if EMPTY(this.oParentObject.w_CURSORNA)
          this.oParentObject.w_CURSORNA = SYS(2015)
        endif
        L_NomeCursore = this.oParentObject.w_CURSORNA
        do case
          case this.oParentObject.w_TIPOPE="C"
            if GSfa_BPT(this, this.oParentObject.w_CURSORNA, this.oParentObject.w_ROOT,this.oParentObject.w_EVGRUPPO,this.oParentObject.w_EVPERSON)>0
              if USED("__tmp__")
                SELECT "__tmp__" 
 USE
              endif
              * --- Riempio la Treeview
              Select ( this.oParentObject.w_CURSORNA ) 
 go top
               
 replace cpbmpname with Alltrim(TEFILBMP) FOR Not Empty(Nvl(TEFILBMP," "))
              this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
              this.oParentObject.notifyevent("Esegui")
            else
              * --- Se la prima commessa specificata non ha elementi
              if Not Used ( this.oParentObject.w_CURSORNA )
                ah_ErrorMsg("Nessun tipo evento riscontrato",,"")
                i_retcode = 'stop'
                return
              endif
              Select ( this.oParentObject.w_CURSORNA )
              Zap
              ah_ErrorMsg("Nessun tipo evento riscontrato",,"")
              * --- Riempio la Treeview
              this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
              this.oParentObject.notifyevent("Esegui")
            endif
          case this.oParentObject.w_TIPOPE="I"
            * --- Nodi
            *     1) Nominativo
            *     2) ID
            *     3) Gruppo
            *     4) Persona
             
 CREATE CURSOR &L_NomeCursore (NODO1 C(30), NODO2 C(30), NODO3 C(30), ; 
 NODO4 C(30), FOGLIA C(30), NODO C(200), ; 
 DESCRI1 C(40), DESCRI2 C(40), DESCRI3 C(40), DESCRI4 C(40), TEDESCRI C(40), ; 
 NOMINAT C(30), IDRICH C(15), GRUPPO C(15), PERSONA C(15),TETIPEVE C(10),TEFLGANT C(1), CPBMPNAME C(40), NUMLIV N(2))
            ah_Msg("Fase 1: elaborazione pratiche per cliente...",.T.)
            VQ_EXEC("..\AGEN\EXE\QUERY\GSFA2BVT.VQR",this,"CursDati")
            ah_Msg("Fase 2: creazione struttura...",.T.)
            do while Not Eof( "CursDati" )
              do case
                case this.oParentObject.w_TIPOTREE="N"
                   
 INSERT INTO &L_NomeCursore (NODO1,NODO2,NODO3,NODO4,FOGLIA, ; 
 NODO, DESCRI1, DESCRI2, DESCRI3, DESCRI4, TEDESCRI, NOMINAT, IDRICH, GRUPPO, PERSONA, CPBMPNAME, TETIPEVE,TEFLGANT,NUMLIV) ; 
 VALUES (CursDati.EVNOMINA,CursDati.EVIDRICH ,CursDati.EVGRUPPO , CursDati.EVPERSON,CursDati.EVPERSON," ",CursDati.NODESCRI, ; 
 CursDati.EVIDRICH, CursDati.DESGRU,CursDati.DESPERS,CursDati.DESPERS,"","","","","",CursDati.TETIPEVE,CursDati.TEFLGANT,0)
                case this.oParentObject.w_TIPOTREE="G"
                   
 INSERT INTO &L_NomeCursore (NODO1,NODO2,NODO3,NODO4,FOGLIA, ; 
 NODO, DESCRI1, DESCRI2, DESCRI3, DESCRI4, TEDESCRI, NOMINAT, IDRICH, GRUPPO, PERSONA, CPBMPNAME, TETIPEVE,TEFLGANT,NUMLIV) ; 
 VALUES (CursDati.EVNOMINA ,CursDati.EVGRUPPO , CursDati.EVPERSON,CursDati.EVIDRICH,CursDati.EVIDRICH," ",CursDati.NODESCRI, ; 
 CursDati.DESGRU,CursDati.DESPERS,CursDati.EVIDRICH,CursDati.EVIDRICH,"","","","","",CursDati.TETIPEVE,CursDati.TEFLGANT,0)
                case this.oParentObject.w_TIPOTREE="P"
                   
 INSERT INTO &L_NomeCursore (NODO1,NODO2,NODO3,NODO4,FOGLIA, ; 
 NODO, DESCRI1, DESCRI2, DESCRI3, DESCRI4, TEDESCRI, NOMINAT, IDRICH, GRUPPO, PERSONA, CPBMPNAME, TETIPEVE,TEFLGANT,NUMLIV) ; 
 VALUES (CursDati.EVGRUPPO,CursDati.EVPERSON ,CursDati.EVNOMINA , CursDati.EVIDRICH,CursDati.EVIDRICH," ",CursDati.DESGRU, ; 
 CursDati.DESPERS, CursDati.NODESCRI,CursDati.EVIDRICH,CursDati.EVIDRICH,"","","","","",CursDati.TETIPEVE,CursDati.TEFLGANT,0)
              endcase
              if Not Eof( "CursDati" )
                 
 Select CursDati 
 Skip
              endif
            enddo
            USE IN SELECT("CursDati") 
            SELECT (L_NomeCursore)
            GO TOP
            cp_Level1(L_NomeCursore,"NODO1,NODO2,NODO3","")
            * --- Organizzo il cursore nella logica di visualizzazione nell'albero per 3 livelli
            do case
              case this.oParentObject.w_TIPOTREE="N"
                * --- Valorizzo il livello 1
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..TEDESCRI=alltrim(&L_NomeCursore..DESCRI1) ; 
 ,&L_NomeCursore..NOMINAT=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..NUMLIV=1 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+1,1)<>"."
                * --- Valorizzo il livello 2
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI2) ; 
 , &L_NomeCursore..TEDESCRI=&L_NomeCursore..DESCRI2 ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..IDRICH=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..NUMLIV=2 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+1
                * --- Valorizzo il livello 3
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..TEDESCRI=&L_NomeCursore..DESCRI3 ; 
 , &L_NomeCursore..NOMINAT =&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..IDRICH=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..GRUPPO=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..NUMLIV=3 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*2+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4+1
                * --- Valorizzo il livello 4
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..TEDESCRI=&L_NomeCursore..DESCRI4 ; 
 , &L_NomeCursore..NOMINAT =&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..IDRICH=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..GRUPPO=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..PERSONA=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..NUMLIV=4 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*3+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*2+1
                 
 SELECT (L_NomeCursore) 
 Replace CpBmpName With "BMP\Utepos2.ico" For NUMLIV=1 
 Replace CpBmpName With "BMP\Note.ico" For NUMLIV=2 
 Replace CpBmpName With "BMP\BMP\Kit.ico" For NUMLIV=3 
 Replace CpBmpName With "BMP\Utepos.ico" For NUMLIV=4
              case this.oParentObject.w_TIPOTREE="G"
                * --- Valorizzo il livello 1
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..TEDESCRI=alltrim(&L_NomeCursore..DESCRI1) ; 
 ,&L_NomeCursore..NOMINAT=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..NUMLIV=1 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+1,1)<>"."
                * --- Valorizzo il livello 2
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI2) ; 
 , &L_NomeCursore..TEDESCRI=&L_NomeCursore..DESCRI2 ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..GRUPPO=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..NUMLIV=2 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+1
                * --- Valorizzo il livello 3
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..TEDESCRI=&L_NomeCursore..DESCRI3 ; 
 , &L_NomeCursore..NOMINAT =&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..GRUPPO=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..PERSONA=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..NUMLIV=3 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*2+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4+1
                * --- Valorizzo il livello 4
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..TEDESCRI=&L_NomeCursore..DESCRI4 ; 
 , &L_NomeCursore..NOMINAT =&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..GRUPPO=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..PERSONA=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..IDRICH=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..NUMLIV=4 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*3+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*2+1
                 
 SELECT (L_NomeCursore) 
 Replace CpBmpName With "BMP\Utepos2.ico" For NUMLIV=1 
 Replace CpBmpName With "BMP\kit.ico" For NUMLIV=2 
 Replace CpBmpName With "BMP\Utepos.ico" For NUMLIV=3 
 Replace CpBmpName With "BMP\Note.ico" For NUMLIV=4
              case this.oParentObject.w_TIPOTREE="P"
                * --- Valorizzo il livello 1
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI1) ; 
 , &L_NomeCursore..TEDESCRI=alltrim(&L_NomeCursore..DESCRI1) ; 
 ,&L_NomeCursore..GRUPPO=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..NUMLIV=1 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+1,1)<>"."
                * --- Valorizzo il livello 2
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=alltrim(&L_NomeCursore..DESCRI2) ; 
 , &L_NomeCursore..TEDESCRI=&L_NomeCursore..DESCRI2 ; 
 , &L_NomeCursore..GRUPPO=&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..PERSONA=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..NUMLIV=2 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+1
                * --- Valorizzo il livello 3
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..TEDESCRI=&L_NomeCursore..DESCRI3 ; 
 , &L_NomeCursore..GRUPPO =&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..PERSONA=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..NUMLIV=3 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*2+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4+1
                * --- Valorizzo il livello 4
                UPDATE &L_NomeCursore SET &L_NomeCursore..NODO=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..TEDESCRI=&L_NomeCursore..DESCRI4 ; 
 , &L_NomeCursore..GRUPPO =&L_NomeCursore..NODO1 ; 
 , &L_NomeCursore..PERSONA=&L_NomeCursore..NODO2 ; 
 , &L_NomeCursore..NOMINAT=&L_NomeCursore..NODO3 ; 
 , &L_NomeCursore..IDRICH=&L_NomeCursore..FOGLIA ; 
 , &L_NomeCursore..NUMLIV=4 ; 
 WHERE SUBSTR(&L_NomeCursore..LVLKEY,10+4*3+1,1)<>"." ; 
 AND LEN(RTRIM(&L_NomeCursore..LVLKEY))>10+4*2+1
                 
 SELECT (L_NomeCursore) 
 Replace CpBmpName With "BMP\Kit.ico" For NUMLIV=1 
 Replace CpBmpName With "BMP\Utepos.ico" For NUMLIV=2 
 Replace CpBmpName With "BMP\Utepos2.ico" For NUMLIV=3 
 Replace CpBmpName With "BMP\Note.ico" For NUMLIV=4
            endcase
            SELECT (L_NomeCursore)
            GO TOP
            this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_CURSORNA
            this.oParentObject.w_TREEVIEW.FillTree()     
        endcase
      case this.pTipop="Filteve"
        if this.oParentObject.w_TIPOPE<>"C"
          this.w_NUMOCC = occur(".",This.oparentobject.w_TREEVIEW.GETVAR("lvlkey"))
          this.oParentObject.w_ZNOMINAT = This.oparentobject.w_TREEVIEW.GETVAR("NOMINAT")
          this.oParentObject.w_ZIDRICH = This.oparentobject.w_TREEVIEW.GETVAR("IDRICH")
          this.oParentObject.w_ZGRUPPO = This.oparentobject.w_TREEVIEW.GETVAR("GRUPPO")
          this.oParentObject.w_ZPERSON = This.oparentobject.w_TREEVIEW.GETVAR("PERSONA")
           
 Select (this.oparentobject.w_EVENT.ccursor) 
 ZAP 
 this.oparentobject.w_EVENT.grd.refresh()
           
 this.oparentobject.Notifyevent("Eventi")
          do case
            case this.oParentObject.w_TIPOTREE="N"
               Delete from (this.oparentobject.w_EVENT.ccursor) where this.oParentObject.w_ZNOMINAT<>Nvl(EVNOMINA," ") or (this.oParentObject.w_ZIDRICH<>Nvl(EVIDRICH," ") and this.w_NUMOCC>0) or (this.oParentObject.w_ZGRUPPO<>Nvl(EVGRUPPO," ") ; 
 and this.w_NUMOCC>1) or (this.oParentObject.w_ZPERSON<>Nvl(EVPERSON," ") and this.w_NUMOCC>2) 
            case this.oParentObject.w_TIPOTREE="G"
               Delete from (this.oparentobject.w_EVENT.ccursor) where this.oParentObject.w_ZNOMINAT<>Nvl(EVNOMINA," ") or (this.oParentObject.w_ZIDRICH<>Nvl(EVIDRICH," ") and this.w_NUMOCC>2) or (this.oParentObject.w_ZGRUPPO<>Nvl(EVGRUPPO," ") ; 
 and this.w_NUMOCC>0) or (this.oParentObject.w_ZPERSON<>Nvl(EVPERSON," ") and this.w_NUMOCC>1) or Empty(Nvl(EVIDRICH," ")) 
            case this.oParentObject.w_TIPOTREE="P"
               Delete from (this.oparentobject.w_EVENT.ccursor) where this.oParentObject.w_ZGRUPPO<>Nvl(EVGRUPPO," ") or (this.oParentObject.w_ZIDRICH<>Nvl(EVIDRICH," ") and this.w_NUMOCC>2) or (this.oParentObject.w_ZNOMINAT<>Nvl(EVNOMINA," ") ; 
 and this.w_NUMOCC>1) or (this.oParentObject.w_ZPERSON<>Nvl(EVPERSON," ") and this.w_NUMOCC>0) or Empty(Nvl(EVIDRICH," ")) 
          endcase
           
 Select (this.oparentobject.w_EVENT.ccursor) 
 Go top 
 this.oparentobject.w_EVENT.Refresh() 
 
        endif
      case this.pTipop="Close"
        * --- Elimina i cursori
        if used("query")
          select "query"
          use
        endif
        * --- Drop temporary table TMP_STR
        i_nIdx=cp_GetTableDefIdx('TMP_STR')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_STR')
        endif
        if used((this.oParentObject.w_CURSORNA))
          select ( this.oParentObject.w_CURSORNA )
          use
        endif
      case this.pTipop="Aggiorna"
        this.w_OBJ_ACE = This.oparentobject.GSFA_ACE
        * --- Aggiorno informazioni eventi selezionati
         
 Select (This.oparentobject.w_EVENT.cCursor)
        if RECCOUNT()>0
           
 punt=recno() 
 Scan for XCHK=1
          this.w_LEVSERIAL = EVSERIAL
          this.w_LEV__ANNO = EV__ANNO
          this.w_EVNOMINA = iif(this.w_OBJ_ACE.w_FLNOMIN="S",this.w_OBJ_ACE.w_EVNOMINA,this.oParentObject.w_NOMINA)
          this.w_EVRIFPER = iif(this.w_OBJ_ACE.w_FLRIFP="S",this.w_OBJ_ACE.w_EVRIFPER,this.oParentObject.w_RIFPER)
          this.w_EVSTATO = iif(this.w_OBJ_ACE.w_FLSTATO="S",this.w_OBJ_ACE.w_EV_STATO,this.oParentObject.w_STATO)
          this.w_EVCODPRA = iif(this.w_OBJ_ACE.w_FLPRAT="S",this.w_OBJ_ACE.w_EVCODPRA,this.w_CODPRA)
          this.w_EVDATFIN = iif(this.w_OBJ_ACE.w_FLDATFI="S",this.w_OBJ_ACE.w_EVDATFIN,this.oParentObject.w_EDATFIN)
          this.w_EVDATINI = iif(this.w_OBJ_ACE.w_FLDATIN="S",this.w_OBJ_ACE.w_EVDATINI,this.oParentObject.w_EDATINI)
          this.w_EVORAEFF = iif(this.w_OBJ_ACE.w_FLORE="S",this.w_OBJ_ACE.w_EVOREEFF,this.oParentObject.w_ORAEFF)
          this.w_EVMINEFF = iif(this.w_OBJ_ACE.w_FLMIN="S",this.w_OBJ_ACE.w_EVMINEFF,this.oParentObject.w_MINEFF)
          this.w_EV__NOTE = this.w_OBJ_ACE.w_EV__NOTE
          this.w_EVIDRICH = this.w_OBJ_ACE.w_EVIDRICH
          this.w_EVSTARIC = iif(this.w_OBJ_ACE.w_FLSTID="S",this.w_OBJ_ACE.w_EVSTARIC,this.oParentObject.w_STARIC)
          this.w_EVFLIDPA = this.w_OBJ_ACE.w_EVFLIDPA
          * --- Try
          local bErr_03918828
          bErr_03918828=bTrsErr
          this.Try_03918828()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03918828
          * --- End
          this.w_OK = .t.
          Endscan
          * --- Resetto flag
          if this.w_OK
            this.w_OBJ_ACE.w_FLRIFP = "N"
            this.w_OBJ_ACE.w_FLNOMIN = "N"
            this.w_OBJ_ACE.w_FLSTATO = "N"
            this.w_OBJ_ACE.w_FLPRAT = "N"
            this.w_OBJ_ACE.w_FLDATFI = "N"
            this.w_OBJ_ACE.w_FLDATIN = "N"
            this.w_OBJ_ACE.w_FLORE = "N"
            this.w_OBJ_ACE.w_FLMIN = "N"
            this.w_OBJ_ACE.w_FLNOTE = "N"
            this.w_OBJ_ACE.w_FLSTID = "N"
            Ah_errormsg("Aggiornamento completato","i")
            This.oParentobject.Notifyevent("Eventi")
          else
            Ah_errormsg("Attenzione nessun evento selezionato")
            Select (This.oparentobject.w_EVENT.cCursor) 
 Goto punt
          endif
        endif
      case this.pTipop="Rightclick" or this.pTipop="Deleve"
        * --- Aggiungo voci  bottone nuovo
        if this.pTipop="Deleve"
          this.w_RETVAL = 7
        else
          if g_INVIO="O" AND Isalt()
            DIMENSION ARRVOCI(6)
          else
            DIMENSION ARRVOCI(5)
          endif
           
 ARRVOCI[1]="Attivit�" 
 ARRVOCI[2]="Attivit� collegate" 
 ARRVOCI[3]="Evento" 
 ARRVOCI[4]=iif(This.oParentObject.GSFA_ACE.oPGFRM.ActivePage = 5,"Post-IN con allegati indici","Post-in") 
 ARRVOCI[5]="E-mail"
          if g_INVIO="O" AND Isalt()
            ARRVOCI[6]="E-mail da archiviare"
          endif
          this.w_RETVAL = Addmenu(@ARRVOCI)
        endif
        if this.w_RETVAL>0
          this.w_CURSORE = Sys(2015)
          if ! Isalt() and this.w_RETVAL<3
             
 Select MAX(EVIDRICH) as MAXID,Min(EVIDRICH) as MINID, MAX(NVL(EVNOMINA," ")) as MAXNOM, MIN(NVL(EVNOMINA," ")) as MINNOM From(This.oparentobject.w_EVENT.cCursor) where XCHK=1 into Cursor TMP_EVEN
            this.w_NOATT = TMP_EVEN.MAXID<>TMP_EVEN.MINID
            this.w_NONOM = TMP_EVEN.MAXNOM<>TMP_EVEN.MINNOM
            if this.w_NOATT OR this.w_NONOM
              Ah_errormsg("Attenzione! Nella selezione effettuata esistono eventi con ID richiesta e/o nominativo diversi! Impossibile generare attivit�")
              i_retcode = 'stop'
              return
            endif
            Use in TMP_EVEN
          endif
          this.w_PADRE = This.oparentobject
           
 Select EVSERIAL,EV__ANNO,EVFLIDPA,EVRIFPER,EV_EMAIL,EV_EMPEC,EVPERSON,EV_PHONE, EV__NOTE,EVLOCALI,EVCODPAR,EVNUMCEL from (this.w_PADRE.w_EVENT.cCursor) where XCHK=1 into cursor (this.w_CURSORE)
          this.w_CODPRA = this.w_PADRE.gsfa_ace.w_EVCODPRA
          this.w_OK = .t.
          if (this.w_RETVAL = 1 or this.w_RETVAL = 2 or this.w_RETVAL = 4 or this.w_RETVAL = 7) and Reccount((this.w_CURSORE))=0
            if this.w_RETVAL = 7
              Ah_errormsg("Nessun evento selezionato! Impossibile eliminare.")
              this.w_OK = .F.
            else
              this.w_OK = Ah_Yesno("Nessun evento selezionato, vuoi proseguire nel caricamento di un'attivit�/post-in senza nessun legame con un evento?")
            endif
          endif
          this.w_EVIDRICH = this.w_PADRE.gsfa_ace.w_EVIDRICH
          do case
            case this.w_RETVAL = 1 and this.w_OK
              this.w_ATCAUATT = Gettipass(this.oParentObject.w_TETIPEVE)
              this.w_EVNOMINA = this.w_PADRE.gsfa_ace.w_EVNOMINA
              this.w_EVLOCALI = this.w_PADRE.gsfa_ace.w_EVLOCALI
              this.w_EV_PHONE = this.w_PADRE.gsfa_ace.w_EV_PHONE
              this.w_EVNUMCEL = this.w_PADRE.gsfa_ace.w_EVNUMCEL
              this.w_EVCODPAR = this.w_PADRE.gsfa_ace.w_EVCODPAR
              this.w_EVRIFPER = this.w_PADRE.gsfa_ace.w_EVRIFPER
              this.w_EV_EMAIL = this.w_PADRE.gsfa_ace.w_EV_EMAIL
              this.w_EV_EMPEC = this.w_PADRE.gsfa_ace.w_EV_EMPEC
              this.w_EV__ANNO = this.w_PADRE.gsfa_ace.w_EV__ANNO
              if Not Empty(this.w_EVIDRICH)
                if Nvl(EVFLIDPA,"N")="S"
                  this.w_ATSEREVE = this.w_PADRE.gsfa_ace.w_EVSERIAL
                else
                  * --- Se non evento padre dell'ID lo ricerco
                  * --- Read from ANEVENTI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ANEVENTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2],.t.,this.ANEVENTI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "EVSERIAL"+;
                      " from "+i_cTable+" ANEVENTI where ";
                          +"EVIDRICH = "+cp_ToStrODBC(this.w_IDRICH);
                          +" and EVFLIDPA = "+cp_ToStrODBC("S");
                          +" and EV__ANNO = "+cp_ToStrODBC(this.oParentObject.w_EV__ANNO);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      EVSERIAL;
                      from (i_cTable) where;
                          EVIDRICH = this.w_IDRICH;
                          and EVFLIDPA = "S";
                          and EV__ANNO = this.oParentObject.w_EV__ANNO;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_ATSEREVE = NVL(cp_ToDate(_read_.EVSERIAL),cp_NullValue(_read_.EVSERIAL))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
              endif
              gsag_bra(this,"E","A",this.w_CURSORE)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_RETVAL = 2 and this.w_OK
              * --- Read from TIP_RAGG
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.TIP_RAGG_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIP_RAGG_idx,2],.t.,this.TIP_RAGG_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "RECODRAG"+;
                  " from "+i_cTable+" TIP_RAGG where ";
                      +"RETIPEVE = "+cp_ToStrODBC(this.w_PADRE.gsfa_ace.w_EVTIPEVE);
                      +" and REFLPREF = "+cp_ToStrODBC("S");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  RECODRAG;
                  from (i_cTable) where;
                      RETIPEVE = this.w_PADRE.gsfa_ace.w_EVTIPEVE;
                      and REFLPREF = "S";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODRAGG = NVL(cp_ToDate(_read_.RECODRAG),cp_NullValue(_read_.RECODRAG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_PROG = GSAG_KIT()
              GSUT_BVP(this,this.w_PROG, "w_CODITER", "["+this.w_CODRAGG+"]")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PROG.Notifyevent("w_CODITER Changed")     
              GSUT_BVP(this,this.w_PROG, "w_TIPOEVENTO", "["+iif(not empty(this.w_PADRE.w_TETIPEVE),this.w_PADRE.w_TETIPEVE, this.w_PADRE.w_EVTIPEVE) +"]")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              GSUT_BVP(this,this.w_PROG, "w_CODPRAT", "["+this.w_PADRE.gsfa_ace.w_EVCODPRA+"]")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PROG.Notifyevent("w_CODPRAT Changed")     
              GSUT_BVP(this,this.w_PROG, "w_CODNOM", "["+this.w_PADRE.gsfa_ace.w_EVNOMINA+"]")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PROG.w_CUR_EVE = this.w_CURSORE
            case this.w_RETVAL = 3
              * --- Read from TIPEVENT
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.TIPEVENT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TIPEVENT_idx,2],.t.,this.TIPEVENT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "TEPADEVE"+;
                  " from "+i_cTable+" TIPEVENT where ";
                      +"TETIPEVE = "+cp_ToStrODBC(this.oParentObject.w_TETIPEVE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  TEPADEVE;
                  from (i_cTable) where;
                      TETIPEVE = this.oParentObject.w_TETIPEVE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_TEPADEVE = NVL(cp_ToDate(_read_.TEPADEVE),cp_NullValue(_read_.TEPADEVE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_PROG = GSFA_AEV()
              this.w_PROG.Ecpload()     
              if Not EMpty(this.w_TEPADEVE)
                GSUT_BVP(this,this.w_PROG, "w_EVTIPEVE", "["+this.oParentObject.w_TETIPEVE+"]")
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              GSUT_BVP(this,this.w_PROG, "w_EVNOMINA", "["+this.w_PADRE.w_EVNOMINA+"]")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              GSUT_BVP(this,this.w_PROG, "w_EVCODPRA", "["+this.w_PADRE.w_CODPRA+"]")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              gsag_ble(this,this.w_EVIDRICH,"w_ATSEREVE",this.w_PROG,"w_EVNOMINA")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PROG.w_EVFLIDPA = "N"
            case this.w_RETVAL = 4 and this.w_OK
              * --- Nuovo Postin
              if This.oParentObject.GSFA_ACE.oPGFRM.ActivePage = 5
                Select IDSERIAL from (this.w_PADRE.GSFA_ACE.w_ZOOMIND.cCursor) where XCHK=1 into cursor (this.w_CURSORE)
                if Reccount((this.w_CURSORE)) >0 
                  GSUT_BPT(this,this.w_CURSORE, "GSUT_AID")
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              else
                Select EV__ANNO, EVSERIAL FROM (this.w_CURSORE) INTO cursor __TMPPOST__
                GSUT_BPT(this,"__TMPPOST__", "GSFA_AEV")
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                if USED("__TMPPOST__")
                  USE IN "__TMPPOST__"
                endif
              endif
            case this.w_RETVAL = 5
              * --- Nuova email
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            case this.w_RETVAL = 7 AND this.w_OK
              if Ah_Yesno("Attenzione! L'eliminazione degli eventi selezionati comporta la cancellazione degli eventuali indici documento collegati.%0Si desidera procedere ugualmente?")
                * --- Eseguo cancellazione massiva eventi selezionati
                this.w_oERRORLOG=createobject("AH_ErrorLog")
                g_scheduler="S" 
 g_MSG=" "
                this.w_OBJEVE = GSFA_AEV()
                Select (this.w_CURSORE) 
 Go Top 
 Scan
                this.w_LEV__ANNO = EV__ANNO
                this.w_LEVSERIAL = EVSERIAL
                this.w_OBJEVE.EcpFIlter()     
                this.w_OBJEVE.w_EVSERIAL = this.w_LEVSERIAL
                this.w_OBJEVE.w_EV__ANNO = this.w_LEV__ANNO
                this.w_OBJEVE.EcpSave()     
                * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
                if !(this.w_OBJEVE.bSec1)
                  i_retcode = 'stop'
                  return
                endif
                g_MSG=" "
                btrserr=.f.
                this.w_OBJEVE.ECPDELETE()     
                if btrserr
                  this.w_oERRORLOG.AddMsgLogPartNoTrans(space(4), "Cancellazione evento %1 del %2 non avvenuta", this.w_LEVSERIAL,this.w_LEV__ANNO)     
                  this.w_oERRORLOG.AddMsgLogNoTranslate(g_msg)     
                  this.w_LOGERR = .t.
                endif
                g_scheduler="S"
                 
 Endscan
                AddMsgNL("Fine elaborazione %1",this,Time())
                g_scheduler="N"
                this.w_OBJEVE.EcpQuit()     
                this.w_OBJEVE = Null
                if this.w_LOGERR
                  this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
                endif
                this.w_PADRE.Notifyevent("Eventi")     
                this.w_oERRORLOG = null
              endif
            case this.w_RETVAL = 6
              this.w_CNCODCAN = This.oparentobject.GSFA_ACE.w_EVCODPRA
              this.w_AECODPRA = ALLTRIM(this.w_CNCODCAN)
              this.w_AEORIFIL = ""
              this.w_AEOGGETT = ""
              this.w_AE_TESTO = ""
              this.w_AEPATHFI = ""
              this.w_FL_SAVE = .F.
              this.w_TIPOALL = ""
              this.w_CLASALL = ""
              this.w_AETIALCP = "N"
              this.w_AECLALCP = "N"
              this.w_AETYPEAL = "O"
              this.w_AENOMEAL = "O"
              this.w_CNDESCAN = This.oparentobject.GSFA_ACE.w_DESCAN
              do gspr_kae with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_FL_SAVE
                if Isalt() and g_AGEN="S" and (Not Empty(this.w_CODPRE) or Not Empty(this.w_RAGPRE)) and Not Empty(this.w_AECODPRA)
                  GSAG_BPI(this,this.w_AECODPRA,this.w_CODPRE,this.w_RAGPRE,"")
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
          endcase
          this.w_PROG = Null
          this.w_PADRE = .Null.
           
 Release ARRVOCI
        endif
      case this.pTipop="AggEventi"
        this.w_EVIDRICH = this.oparentobject.w_EVIDRICH
        Ah_msg("Aggiornamento ID evento eseguito correttamente")
        * --- Try
        local bErr_039C0188
        bErr_039C0188=bTrsErr
        this.Try_039C0188()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_039C0188
        * --- End
        * --- Aggiorno attivit� collegate
        * --- Try
        local bErr_039C0CC8
        bErr_039C0CC8=bTrsErr
        this.Try_039C0CC8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_039C0CC8
        * --- End
      case this.pTipop="Apri"
        if g_OFFE="S"
          DIMENSION ARRVOCI(5)
        else
          DIMENSION ARRVOCI(4)
        endif
         
 ARRVOCI[1]="Visualizza documenti" 
 ARRVOCI[2]="Disponibilit� persone e risorse" 
 ARRVOCI[3]="Elenco attivit�" 
 ARRVOCI[4]="Evento"
        if g_OFFE="S"
           
 ARRVOCI[5]="Ricerca offerte"
        endif
        this.w_RETVAL = Addmenu(@ARRVOCI)
        this.w_PADRE = This.oparentobject
        this.w_EVNOMINA = this.w_PADRE.gsfa_ace.w_EVNOMINA
        this.w_CODCLI = this.w_PADRE.gsfa_ace.w_CODCLI
        do case
          case this.w_RETVAL=1
            this.w_PROG = gsve_szm()
            this.w_PROG.w_MAGCLI = "C"
            this.w_PROG.w_CLISEL = this.w_CODCLI
            GSUT_BVP(this,this.w_PROG, "w_CLISEL", "["+this.w_CODCLI+"]")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PROG.Notifyevent("Esegui")     
          case this.w_RETVAL=2
            this.w_PROG = gsag_kdp()
          case this.w_RETVAL=3
            this.w_PROG = gsag_kra()
            GSUT_BVP(this,this.w_PROG, "w_CODNOM", "["+this.w_EVNOMINA+"]")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PROG.Notifyevent("Ricerca")     
          case this.w_RETVAL=4
            =Opengest("O","GSFA_AEV","EV__ANNO",this.oParentObject.w_EV__ANNO,"EVSERIAL",this.oParentObject.w_EVSERIAL)
          case this.w_RETVAL=5
            this.w_PROG = gsof_kro()
            GSUT_BVP(this,this.w_PROG, "w_ROCODNOM", "["+this.w_EVNOMINA+"]")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PROG.Notifyevent("Ricerca")     
        endcase
        this.w_PROG = Null
        this.w_PADRE = .Null.
         
 Release ARRVOCI
      case this.pTipop="Elim"
        this.w_EVFLIDPA = this.oparentobject.w_EVFLIDPA
        this.w_EVIDRICH = this.oparentobject.w_EVIDRICH
        this.w_OK = .T.
        * --- Read from ASS_ATEV
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ASS_ATEV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATEV_idx,2],.t.,this.ASS_ATEV_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" ASS_ATEV where ";
                +"AECODEVE = "+cp_ToStrODBC(this.oParentObject.w_EVSERIAL);
                +" and AE__ANNO = "+cp_ToStrODBC(this.oParentObject.w_EV__ANNO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                AECODEVE = this.oParentObject.w_EVSERIAL;
                and AE__ANNO = this.oParentObject.w_EV__ANNO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows<>0
          this.w_MESS = Ah_msgformat("Attenzione evento legato ad attivit�, impossibile eliminare")
          this.w_OK = .F.
        endif
        if this.w_EVFLIDPA="S" AND this.w_OK
          * --- Select from ANEVENTI
          i_nConn=i_TableProp[this.ANEVENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2],.t.,this.ANEVENTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select EVIDRICH,EVSERIAL,EV__ANNO  from "+i_cTable+" ANEVENTI ";
                +" where EVIDRICH="+cp_ToStrODBC(this.w_EVIDRICH)+" and (EV__ANNO <> "+cp_ToStrODBC(this.oParentObject.w_EV__ANNO)+" or EVSERIAL <> "+cp_ToStrODBC(this.oParentObject.w_EVSERIAL)+" )";
                 ,"_Curs_ANEVENTI")
          else
            select EVIDRICH,EVSERIAL,EV__ANNO from (i_cTable);
             where EVIDRICH=this.w_EVIDRICH and (EV__ANNO <> this.oParentObject.w_EV__ANNO or EVSERIAL <> this.oParentObject.w_EVSERIAL );
              into cursor _Curs_ANEVENTI
          endif
          if used('_Curs_ANEVENTI')
            select _Curs_ANEVENTI
            locate for 1=1
            do while not(eof())
            this.w_MESS = Ah_msgformat("Attenzione, evento padre di altri eventi, impossibile eliminare")
            this.w_OK = .F.
              select _Curs_ANEVENTI
              continue
            enddo
            use
          endif
        endif
        if !this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pTipop="Aggsta"
        * --- Try
        local bErr_039E1058
        bErr_039E1058=bTrsErr
        this.Try_039E1058()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_039E1058
        * --- End
      case this.pTipop="NewEvt"
        * --- Visualizza men� popup
        if i_VisualTheme = -1
          L_RETVAL=0
          DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT
          DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Evento")
          DEFINE BAR 2 OF popCmd PROMPT ah_MsgFormat("Prestazione")
          ON SELECTION BAR 1 OF popCmd L_RETVAL = 1
          ON SELECTION BAR 2 OF popCmd L_RETVAL = 2
          ACTIVATE POPUP popCmd
          DEACTIVATE POPUP popCmd
          RELEASE POPUPS popCmd
          this.w_RETVAL = L_RETVAL
          Release L_RETVAL
        else
          local oBtnMenu, oMenuItem
          oBtnMenu = CreateObject("cbPopupMenu")
          oBtnMenu.oParentObject = This
          oBtnMenu.oPopupMenu.cOnClickProc="ExecScript(NAMEPROC)"
          oMenuItem = oBtnMenu.AddMenuItem()
          oMenuItem.Caption = Ah_Msgformat("Evento")
          oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=1"
          oMenuItem.Visible=.T.
          oMenuItem = oBtnMenu.AddMenuItem()
          oMenuItem.Caption = Ah_Msgformat("Prestazione")
          oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=2"
          oMenuItem.Visible=.T.
          oBtnMenu.InitMenu()
          oBtnMenu.ShowMenu()
          oBtnMenu = .NULL.
        endif
        do case
          case this.w_RETVAL = 1
            * --- Apre l'anagrafica eventi in caricamento
            this.w_PROG = GSFA_AEV()
            this.w_PROG.Ecpload()     
            if !EMPTY(this.w_CNCODCAN)
              GSUT_BVP(this,this.w_PROG, "w_CODPRAT", "["+this.w_CNCODCAN+"]")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          case this.w_RETVAL = 2
            * --- Inserimento provvisorio delle prestazioni
            do GSAG_KPR with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
    endcase
  endproc
  proc Try_03918828()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ANEVENTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ANEVENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ANEVENTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"EVNOMINA ="+cp_NullLink(cp_ToStrODBC(this.w_EVNOMINA),'ANEVENTI','EVNOMINA');
      +",EVRIFPER ="+cp_NullLink(cp_ToStrODBC(this.w_EVRIFPER),'ANEVENTI','EVRIFPER');
      +",EV_STATO ="+cp_NullLink(cp_ToStrODBC(this.w_EVSTATO),'ANEVENTI','EV_STATO');
      +",EVCODPRA ="+cp_NullLink(cp_ToStrODBC(this.w_EVCODPRA),'ANEVENTI','EVCODPRA');
      +",EVDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_EVDATFIN),'ANEVENTI','EVDATFIN');
      +",EVDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_EVDATINI),'ANEVENTI','EVDATINI');
      +",EVOREEFF ="+cp_NullLink(cp_ToStrODBC(this.w_EVORAEFF),'ANEVENTI','EVOREEFF');
      +",EVMINEFF ="+cp_NullLink(cp_ToStrODBC(this.w_EVMINEFF),'ANEVENTI','EVMINEFF');
      +",EVIDRICH ="+cp_NullLink(cp_ToStrODBC(this.w_EVIDRICH),'ANEVENTI','EVIDRICH');
      +",EVSTARIC ="+cp_NullLink(cp_ToStrODBC(this.w_EVSTARIC),'ANEVENTI','EVSTARIC');
          +i_ccchkf ;
      +" where ";
          +"EVSERIAL = "+cp_ToStrODBC(this.w_LEVSERIAL);
          +" and EV__ANNO = "+cp_ToStrODBC(this.w_LEV__ANNO);
             )
    else
      update (i_cTable) set;
          EVNOMINA = this.w_EVNOMINA;
          ,EVRIFPER = this.w_EVRIFPER;
          ,EV_STATO = this.w_EVSTATO;
          ,EVCODPRA = this.w_EVCODPRA;
          ,EVDATFIN = this.w_EVDATFIN;
          ,EVDATINI = this.w_EVDATINI;
          ,EVOREEFF = this.w_EVORAEFF;
          ,EVMINEFF = this.w_EVMINEFF;
          ,EVIDRICH = this.w_EVIDRICH;
          ,EVSTARIC = this.w_EVSTARIC;
          &i_ccchkf. ;
       where;
          EVSERIAL = this.w_LEVSERIAL;
          and EV__ANNO = this.w_LEV__ANNO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_OBJ_ACE.w_FLNOTE="S"
      * --- Write separata per non appesantire zoom con campo note old
      * --- Write into ANEVENTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ANEVENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ANEVENTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"EV__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_EV__NOTE),'ANEVENTI','EV__NOTE');
            +i_ccchkf ;
        +" where ";
            +"EVSERIAL = "+cp_ToStrODBC(this.w_LEVSERIAL);
            +" and EV__ANNO = "+cp_ToStrODBC(this.w_LEV__ANNO);
               )
      else
        update (i_cTable) set;
            EV__NOTE = this.w_EV__NOTE;
            &i_ccchkf. ;
         where;
            EVSERIAL = this.w_LEVSERIAL;
            and EV__ANNO = this.w_LEV__ANNO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_EVFLIDPA="S" and this.w_OBJ_ACE.w_FLSTID="S"
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return
  proc Try_039C0188()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ANEVENTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ANEVENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ANEVENTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"EVIDRICH ="+cp_NullLink(cp_ToStrODBC(this.w_EVIDRICH),'ANEVENTI','EVIDRICH');
      +",EVFLIDPA ="+cp_NullLink(cp_ToStrODBC("S"),'ANEVENTI','EVFLIDPA');
      +",EVSTARIC ="+cp_NullLink(cp_ToStrODBC("A"),'ANEVENTI','EVSTARIC');
          +i_ccchkf ;
      +" where ";
          +"EV__ANNO = "+cp_ToStrODBC(this.oParentObject.w_EV__ANNO);
          +" and EVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_EVSERIAL);
             )
    else
      update (i_cTable) set;
          EVIDRICH = this.w_EVIDRICH;
          ,EVFLIDPA = "S";
          ,EVSTARIC = "A";
          &i_ccchkf. ;
       where;
          EV__ANNO = this.oParentObject.w_EV__ANNO;
          and EVSERIAL = this.oParentObject.w_EVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_039C0CC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from ASS_ATEV
    i_nConn=i_TableProp[this.ASS_ATEV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATEV_idx,2],.t.,this.ASS_ATEV_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ASS_ATEV ";
          +" where AE__ANNO="+cp_ToStrODBC(this.oParentObject.w_EV__ANNO)+" and AECODEVE="+cp_ToStrODBC(this.oParentObject.w_EVSERIAL)+"";
           ,"_Curs_ASS_ATEV")
    else
      select * from (i_cTable);
       where AE__ANNO=this.oParentObject.w_EV__ANNO and AECODEVE=this.oParentObject.w_EVSERIAL;
        into cursor _Curs_ASS_ATEV
    endif
    if used('_Curs_ASS_ATEV')
      select _Curs_ASS_ATEV
      locate for 1=1
      do while not(eof())
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATSEREVE ="+cp_NullLink(cp_ToStrODBC(_Curs_ASS_ATEV.AECODEVE),'OFF_ATTI','ATSEREVE');
            +i_ccchkf ;
        +" where ";
            +"ATSERIAL = "+cp_ToStrODBC(_Curs_ASS_ATEV.AECODATT);
               )
      else
        update (i_cTable) set;
            ATSEREVE = _Curs_ASS_ATEV.AECODEVE;
            &i_ccchkf. ;
         where;
            ATSERIAL = _Curs_ASS_ATEV.AECODATT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_ASS_ATEV
        continue
      enddo
      use
    endif
    return
  proc Try_039E1058()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_EVFLIDPA = this.oparentobject.w_EVFLIDPA
    if this.w_EVFLIDPA="S"
      this.w_EVSTARIC = This.oparentobject.w_EVSTARIC
      this.w_EVIDRICH = This.oparentobject.w_EVIDRICH
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_AEDESPRA = ALLTRIM(This.oparentobject.GSFA_ACE.w_DESCAN)
    this.w_AECODPRA = ALLTRIM(This.oparentobject.GSFA_ACE.w_EVCODPRA)
    DIMENSION L_INFOARCH(9)
    if Isalt()
      L_INFOARCH(1) = "CAN_TIER"
    else
      L_INFOARCH(1) = ""
    endif
    L_INFOARCH(2) = this.w_AECODPRA
    L_INFOARCH(3) = this.w_AEPATHFI
    L_INFOARCH(4) = this.w_AEORIFIL
    L_INFOARCH(5) = this.w_AEOGGETT
    L_INFOARCH(6) = ""
    L_INFOARCH(7) = ""
    L_INFOARCH(8) = ""
    L_INFOARCH(9) = this.w_AEDESPRA
     
 Dimension ARRAYALLEG[1] 
 NumAlleg=1 
 ARRAYALLEG(NumAlleg) = " " 
 w_RET = EMAIL( @ARRAYALLEG , g_UEDIAL, .F., "", "", g_UEFIRM, .F., .F., @L_INFOARCH)
    * Variabili publiche per mail 
 i_EMAIL = " " 
 i_EMAILSUBJECT = " " 
 i_EMAILTEXT = " "
    release L_INFOARCH
    if not w_RET
      this.w_TMPC = "Impossibile inviare via email il documento selezionato"
      ah_ErrorMSG(this.w_TMPC,48)
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna stato eventi collegati
    * --- Write into ANEVENTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ANEVENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANEVENTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ANEVENTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"EVSTARIC ="+cp_NullLink(cp_ToStrODBC(this.w_EVSTARIC),'ANEVENTI','EVSTARIC');
          +i_ccchkf ;
      +" where ";
          +"EVIDRICH = "+cp_ToStrODBC(this.w_EVIDRICH);
             )
    else
      update (i_cTable) set;
          EVSTARIC = this.w_EVSTARIC;
          &i_ccchkf. ;
       where;
          EVIDRICH = this.w_EVIDRICH;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipOp)
    this.pTipOp=pTipOp
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='*TMP_STR'
    this.cWorkTables[2]='TIPEVENT'
    this.cWorkTables[3]='ANEVENTI'
    this.cWorkTables[4]='TIP_RAGG'
    this.cWorkTables[5]='ASS_ATEV'
    this.cWorkTables[6]='OFF_ATTI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_ASS_ATEV')
      use in _Curs_ASS_ATEV
    endif
    if used('_Curs_ANEVENTI')
      use in _Curs_ANEVENTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOp"
endproc
