* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bdc                                                        *
*              Genera documenti dai contratti                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-25                                                      *
* Last revis.: 2015-10-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bdc",oParentObject,m.pPARAM)
return(i_retval)

define class tgsag_bdc as StdBatch
  * --- Local variables
  pPARAM = space(1)
  w_oERRORMESS = .NULL.
  w_oMESS1 = .NULL.
  w_oPART1 = .NULL.
  w_OBJ = .NULL.
  w_IMDESCON = space(50)
  w_IMDESCRI = space(50)
  w_ELDATINI = ctod("  /  /  ")
  w_ELDATFIN = ctod("  /  /  ")
  w_DESCRIZIONE = space(40)
  w_ELGIOATT = 0
  w_ELINICOA = ctod("  /  /  ")
  w_MOCHKDDO = space(1)
  w_MODESDOC = space(0)
  w_ELFINCOA = ctod("  /  /  ")
  w_CHIAVE = space(10)
  w_DBRK = space(10)
  w_RESULT = space(254)
  w_SERIALE = space(10)
  w_DATATT = ctod("  /  /  ")
  w_TIPODOC = space(20)
  w_CLIENTE = space(15)
  w_NEWDATE = ctod("  /  /  ")
  w_PERIODO = space(3)
  w_ROWNUM = 0
  w_ELECONT = .null.
  w_OGGETTO = space(254)
  w_INIZIO = ctod("  /  /  ")
  w_NOMINATIVO = space(15)
  w_CONTAELEM = 0
  w_CONTADOC = 0
  w_ERROR = 0
  w_CODIMP = space(10)
  w_TIPART = space(2)
  w_PART = .f.
  w_TIPAPL = space(1)
  w_CODLIS = space(5)
  w_CODPAG = space(5)
  w_SEDE = space(5)
  w_CODVAL = space(3)
  w_CONTRATTO = space(10)
  w_OLDCONTRA = space(10)
  w_CODESSUP = space(254)
  w_CODESCON = space(50)
  w_COCODCON = space(15)
  w_CO__NOTE = space(0)
  w_COTIPDOC = space(5)
  w_COGRUPAR = space(5)
  w_COTIPATT = space(20)
  w_COCODPAG = space(5)
  w_CODATSTI = ctod("  /  /  ")
  w_CODATINI = ctod("  /  /  ")
  w_CODATFIN = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_PADESSUP = space(254)
  w_COCODPAG = space(5)
  w_DESSUP = space(0)
  w_TIPRIG = space(1)
  w_FLSCOR = space(1)
  w_CODIVA = space(5)
  w_TIPSED = space(5)
  w_TIPOPE = space(10)
  w_ANTIPOPE = space(10)
  w_ARTIPOPE = space(10)
  w_ELINICOD = ctod("  /  /  ")
  w_ELFINCOD = ctod("  /  /  ")
  w_FLINTE = space(1)
  w_COMPONENTE = 0
  w_CODMOD = space(10)
  w_RINNOVO = 0
  w_MVFLVABD = space(1)
  w_MVBENDEP = space(1)
  w_CODORN = space(15)
  w_BUFLANAL = space(1)
  w_BENDEPAZ = space(1)
  w_AZFLVEBD = space(1)
  w_oERRORMESS = .NULL.
  w_CPROWNUM = 0
  w_XCONORN = space(15)
  w_VALRIG = 0
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_RETVAL = space(254)
  w_SERDOC = space(10)
  w_CONTRATTO = space(10)
  w_MODELLO = space(10)
  w_IMPIANTO = space(10)
  w_KEYCOMP = 0
  w_OLDSERDOC = space(10)
  w_CONTA = 0
  w_RINNOV = 0
  w_RINNOV = 0
  w_CODICEIMPIANTO = space(10)
  w_CODICECOMPONENTE = space(20)
  w_WARN = space(0)
  w_DATATOLL = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_NEWCOMPATT1 = ctod("  /  /  ")
  w_NEWCOMPATT2 = ctod("  /  /  ")
  * --- WorkFile variables
  DET_PIAN_idx=0
  ELE_CONT_idx=0
  DET_GEN_idx=0
  DOC_GENE_idx=0
  DET_DOCU_idx=0
  PAR_AGEN_idx=0
  TIP_DOCU_idx=0
  CAM_AGAZ_idx=0
  GES_DOCU_idx=0
  BUSIUNIT_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Documenti (lanciato da GSAG_AGD)
    this.w_oERRORMESS=createobject("AH_ErrorLog")
    this.w_oMESS1=createobject("ah_message")
    * --- G  GENERA DOCUMENTI
    *     D  CANCELLA FIGLI COLLEGATI
    if this.pPARAM="G"
      this.w_DBRK = "##zz##"
      this.w_OLDCONTRA = "##ZZ##ZZ##"
      this.w_CODAZI = i_codazi
      this.w_ROWNUM = 0
      this.w_CONTAELEM = 0
      this.w_CONTADOC = 0
      this.w_ERROR = 0
      * --- Read from PAR_AGEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PADESSUP"+;
          " from "+i_cTable+" PAR_AGEN where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PADESSUP;
          from (i_cTable) where;
              PACODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PADESSUP = NVL(cp_ToDate(_read_.PADESSUP),cp_NullValue(_read_.PADESSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if isAhe()
        * --- Read from BUSIUNIT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.BUSIUNIT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BUSIUNIT_idx,2],.t.,this.BUSIUNIT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BUFLANAL"+;
            " from "+i_cTable+" BUSIUNIT where ";
                +"BUCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and BUCODICE = "+cp_ToStrODBC(g_CODBUN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BUFLANAL;
            from (i_cTable) where;
                BUCODAZI = i_CODAZI;
                and BUCODICE = g_CODBUN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_BUFLANAL = NVL(cp_ToDate(_read_.BUFLANAL),cp_NullValue(_read_.BUFLANAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZBENDEP"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZBENDEP;
            from (i_cTable) where;
                AZCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_BENDEPAZ = NVL(cp_ToDate(_read_.AZBENDEP),cp_NullValue(_read_.AZBENDEP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVBENDEP = this.w_BENDEPAZ
      else
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZFLVEBD"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZFLVEBD;
            from (i_cTable) where;
                AZCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_AZFLVEBD = NVL(cp_ToDate(_read_.AZFLVEBD),cp_NullValue(_read_.AZFLVEBD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MVFLVABD = this.w_AZFLVEBD
      endif
      * --- Creo l'oggetto per scrivere le attivit�
      this.w_oERRORMESS=createobject("AH_ErrorLog")
      this.w_OBJ = CREATEOBJECT("DocumWriter",this,"DOC_MAST")
      * --- Select from GSAG_BDD
      do vq_exec with 'GSAG_BDD',this,'_Curs_GSAG_BDD','',.f.,.t.
      if used('_Curs_GSAG_BDD')
        select _Curs_GSAG_BDD
        locate for 1=1
        do while not(eof())
        this.w_OBJ.cRetLogWarn = "S"
        this.w_TIPODOC = _Curs_GSAG_BDD.ELCAUDOC
        this.w_IMDESCON = _Curs_GSAG_BDD.IMDESCON
        this.w_TIPAPL = _Curs_GSAG_BDD.MOTIPAPL
        this.w_CLIENTE = _Curs_GSAG_BDD.COCODCON
        this.w_CODORN = _Curs_GSAG_BDD.ANCODORN
        this.w_PERIODO = _Curs_GSAG_BDD.ELPERFAT
        this.w_MOCHKDDO = _Curs_GSAG_BDD.MOCHKDDO
        this.w_MODESDOC = _Curs_GSAG_BDD.MODESDOC
        this.w_DATATT = CP_TODATE(_Curs_GSAG_BDD.ELDATFAT)
        this.w_CODIMP = _Curs_GSAG_BDD.ELCODIMP
        this.w_TIPART = _Curs_GSAG_BDD.ARTIPART
        this.w_TIPRIG = iif(Nvl(_Curs_GSAG_BDD.ARTIPART," ")="FO","F","M")
        this.w_CODLIS = Nvl(_Curs_GSAG_BDD.ELCODLIS,space(5))
        this.w_CODPAG = _Curs_GSAG_BDD.ELCODPAG
        this.w_SEDE = _Curs_GSAG_BDD.IM__SEDE
        this.w_CODVAL = _Curs_GSAG_BDD.ELCODVAL
        this.w_CONTRATTO = _Curs_GSAG_BDD.ELCONTRA
        this.w_CODESCON = _Curs_GSAG_BDD.CODESCON
        this.w_COCODCON = _Curs_GSAG_BDD.COCODCON
        this.w_CO__NOTE = _Curs_GSAG_BDD.CO__NOTE
        this.w_IMDESCRI = _Curs_GSAG_BDD.IMDESCRI
        this.w_COTIPDOC = _Curs_GSAG_BDD.COTIPDOC
        this.w_COGRUPAR = _Curs_GSAG_BDD.COGRUPAR
        this.w_COTIPATT = _Curs_GSAG_BDD.COTIPATT
        this.w_COCODPAG = _Curs_GSAG_BDD.COCODPAG
        this.w_CODATSTI = _Curs_GSAG_BDD.CODATSTI
        this.w_CODATINI = _Curs_GSAG_BDD.CODATINI
        this.w_CODATFIN = _Curs_GSAG_BDD.CODATFIN
        this.w_COCODPAG = _Curs_GSAG_BDD.COCODPAG
        this.w_ELGIOATT = _Curs_GSAG_BDD.ELGIOATT
        this.w_FLSCOR = _Curs_GSAG_BDD.ANSCORPO
        this.w_CODIVA = _Curs_GSAG_BDD.ARCODIVA
        this.w_ARTIPOPE = _Curs_GSAG_BDD.ARTIPOPE
        this.w_ELINICOA = CP_TODATE(_Curs_GSAG_BDD.ELINICOA)
        this.w_ELFINCOA = CP_TODATE(_Curs_GSAG_BDD.ELFINCOA)
        this.w_CODESSUP = alltrim(IIF(NOT EMPTY(_Curs_GSAG_BDD.CODESSUP),_Curs_GSAG_BDD.CODESSUP, this.w_PADESSUP))
        this.w_TIPSED = _Curs_GSAG_BDD.TIPSED
        this.w_TIPOPE = _Curs_GSAG_BDD.TIPOPE
        this.w_ANTIPOPE = _Curs_GSAG_BDD.ANTIPOPE
        this.w_ELINICOD = _Curs_GSAG_BDD.ELINICOD
        this.w_ELDATINI = CP_TODATE(_Curs_GSAG_BDD.ELDATINI)
        this.w_ELDATFIN = CP_TODATE(_Curs_GSAG_BDD.ELDATFIN)
        this.w_ELFINCOD = _Curs_GSAG_BDD.ELFINCOD
        this.w_COMPONENTE = _Curs_GSAG_BDD.ELCODCOM
        this.w_CODMOD = _Curs_GSAG_BDD.ELCODMOD
        this.w_DESSUP = _Curs_GSAG_BDD.ARDESSUP
        this.w_DESCRIZIONE = _Curs_GSAG_BDD.ARDESART
        this.w_RINNOVO = _Curs_GSAG_BDD.ELRINNOV
        this.w_CHIAVE = DTOC(this.w_DATATT)+NVL(this.w_TIPODOC, SPACE(5))+NVL(this.w_CLIENTE, SPACE(15))+NVL(this.w_CODLIS,SPACE(5))+NVL(this.w_CODPAG,SPACE(5))+NVL(this.w_TIPSED,SPACE(1))
        if MOCHKDDO="S" AND this.oParentObject.w_DTFLRAGG<>"S"
          CREATE CURSOR APPOMESS ;
          (SERDES C(40),SERDE2 C(254), CONCOD C(10), CONDES C(50), IMPCOD C(10), IMPDES C(50), IMPCOM C(50),PERIOD C(3),DATDOC D(8),DATVAD D(8), DATVAA D(8), DATTOL N(4,0), DATCOD D(8),DATCOA D(8))
          * --- Inserimento nel cursore di tutti i dati necessari
          INSERT INTO APPOMESS (SERDES,SERDE2, CONCOD, CONDES, IMPCOD, IMPDES, IMPCOM,PERIOD,DATDOC,DATVAD, DATVAA, DATTOL, DATCOD,DATCOA) VALUES;
          (nvl(alltrim(this.w_DESCRIZIONE),""),NVL(alltrim(this.w_DESSUP),""),nvl(alltrim(this.w_CONTRATTO),""),nvl(alltrim(this.w_CODESCON),""),nvl(alltrim(this.w_CODIMP),""),nvl(alltrim(this.w_IMDESCRI),""),nvl(alltrim(this.w_IMDESCON),""),alltrim(this.w_PERIODO),NVL ( this.w_DATATT,("  /  /  ")),NVL ( this.w_ELDATINI,("  /  /  ")),NVL ( this.w_ELDATFIN,("  /  /  ")),this.w_ELGIOATT,NVL ( this.w_ELINICOA,("  /  /  ")),NVL ( this.w_ELFINCOA,("  /  /  ")))
          this.w_MODESDOC = CUR_TO_MSG( "APPOMESS" , "" , this.w_MODESDOC, "" )
          this.w_DESCRIZIONE = strtran(SUBSTR(ltrim(this.w_MODESDOC),1,40),CHR(13)+CHR(10),space(2))
          this.w_DESSUP = IIF(len(alltrim(this.w_MODESDOC)) >40,SUBSTR(ltrim(this.w_MODESDOC),41,LEN(ALLTRIM(this.w_MODESDOC))),this.w_DESSUP)
        endif
        if this.w_CHIAVE<>this.w_DBRK
          this.w_PART = .T.
          this.w_CPROWNUM = 0
          this.w_CONTAELEM = this.w_CONTAELEM+1
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_OBJ.Blank()     
          this.w_OBJ.w_MVTIPDOC = this.w_TIPODOC
          this.w_OBJ.w_MVTIPCON = "C"
          this.w_OBJ.w_MVCODCON = this.w_CLIENTE
          this.w_OBJ.w_MVTIPORN = "C"
          this.w_OBJ.w_MVCODORN = this.w_CODORN
          this.w_OBJ.w_MVFLPROV = this.oParentObject.w_DTFLPROV
          this.w_OBJ.w_MVDATREG = CP_TODATE(_Curs_GSAG_BDD.ELDATFAT)
          this.w_OBJ.w_MVDATDOC = CP_TODATE(_Curs_GSAG_BDD.ELDATFAT)
          this.w_OBJ.w_MVTIPOPE = this.w_TIPOPE
          this.w_OBJ.w_MVCATOPE = "OP"
          if Isahe()
            this.w_OBJ.w_MVSERIAL = "0000000001"
            if Nvl(_Curs_GSAG_BDD.TDFLGLIS," ")="S"
              this.w_OBJ.w_MVTCOLIS = _Curs_GSAG_BDD.ELTCOLIS
              this.w_OBJ.w_MVTSCLIS = _Curs_GSAG_BDD.ELSCOLIS
              this.w_OBJ.w_MVTPROLI = _Curs_GSAG_BDD.ELPROLIS
              this.w_OBJ.w_MVTPROSC = _Curs_GSAG_BDD.ELPROSCO
            endif
            this.w_OBJ.w_MVBENDEP = this.w_BENDEPAZ
          else
            this.w_OBJ.w_MVSERIAL = Space(10)
            this.w_OBJ.w_MVTCOLIS = this.w_CODLIS
            this.w_OBJ.w_MVFLVABD = this.w_AZFLVEBD
          endif
          this.w_OBJ.w_MVCODPAG = this.w_CODPAG
          this.w_OBJ.w_MVCODDES = this.w_TIPSED
          this.w_OBJ.w_MVCODVAL = this.w_CODVAL
          this.w_OBJ.w_MVFLSCOR = this.w_FLSCOR
          this.w_OBJ.w_MVCODDES = this.w_TIPSED
        endif
        this.w_FLINTE = _Curs_GSAG_BDD.TDFLINTE
        if this.oParentObject.w_DTFLRAGG="R" AND NOT EMPTY(this.w_CODESSUP) AND (this.w_CHIAVE<>this.w_DBRK OR this.w_CONTRATTO <> this.w_OLDCONTRA)
           
 DIMENSION ARPARAM[11,2] 
 ARPARAM[1,1]="DESCON" 
 ARPARAM[1,2]=ALLTRIM(this.w_CODESCON) 
 ARPARAM[2,1]="CODCON" 
 ARPARAM[2,2]=this.w_COCODCON 
 ARPARAM[3,1]="NOTE" 
 ARPARAM[3,2]=ALLTRIM(this.w_CO__NOTE) 
 ARPARAM[4,1]="TIPDOC" 
 ARPARAM[4,2]=this.w_COTIPDOC 
 ARPARAM[5,1]="GRUPAR" 
 ARPARAM[5,2]=ALLTRIM(this.w_COGRUPAR) 
 ARPARAM[6,1]="TIPATT" 
 ARPARAM[6,2]=ALLTRIM(this.w_COTIPATT) 
 ARPARAM[7,1]="CODPAG" 
 ARPARAM[7,2]=ALLTRIM(this.w_COCODPAG) 
 ARPARAM[8,1]="DATSTI" 
 ARPARAM[8,2]=DTOC(this.w_CODATSTI) 
 ARPARAM[9,1]="DATINI" 
 ARPARAM[9,2]=DTOC(this.w_CODATINI) 
 ARPARAM[10,1]="DATFIN" 
 ARPARAM[10,2]=DTOC(this.w_CODATFIN) 
 ARPARAM[11,1]="CODICE" 
 ARPARAM[11,2]=ALLTRIM(this.w_CONTRATTO)
          this.w_CPROWNUM = this.w_CPROWNUM+1
          this.w_OBJ.AddDetail(this.w_CPROWNUM,0,g_ARTDES,SPACE(3),0,0,"D")     
          this.w_OBJ.w_DOC_DETT.MVDESART = CALDESPA(this.w_CODESSUP,@ARPARAM,.T.)
          this.w_OBJ.w_DOC_DETT.MVDATGEN = .NULL.
          this.w_OBJ.w_DOC_DETT.MVEFFEVA = .NULL.
          this.w_OBJ.w_DOC_DETT.MVDATEVA = .NULL.
          if isahe()
            this.w_OBJ.w_DOC_DETT.MVSERIAL = "0000000001"
            this.w_OBJ.w_DOC_DETT.MVDATRCO = .NULL.
            this.w_OBJ.w_DOC_DETT.MVDAFICO = this.w_ELECONT.ELINICOD
            this.w_OBJ.w_DOC_DETT.MVDAINCO = this.w_ELECONT.ELFINCOD
          else
            this.w_OBJ.w_DOC_DETT.MVSERIAL = Space(10)
          endif
          this.w_OBJ.w_DOC_DETT.MVINICOM = this.w_ELECONT.ELINICOD
          this.w_OBJ.w_DOC_DETT.MVFINCOM = this.w_ELECONT.ELFINCOD
          if len(alltrim(this.w_OBJ.w_DOC_DETT.MVDESART)) >40
            * --- Se la lunghezza del Riferimento descrittivo � maggiore di 40 caratteri,
            *     inserisco nella descrizione articolo MVDESART i caratteri fino all'ultima occorrenza 
            *     dello spazio nei primi 40 caratteri.
            *     Il resto viene inserito nella descrizione supplementare.
            this.w_OBJ.w_DOC_DETT.MVDESSUP = SUBSTR(this.w_OBJ.w_DOC_DETT.MVDESART,(RATC(" ",LEFT(this.w_OBJ.w_DOC_DETT.MVDESART,40))+1))
            this.w_OBJ.w_DOC_DETT.MVDESART = LEFT(this.w_OBJ.w_DOC_DETT.MVDESART, RATC(" ", LEFT(this.w_OBJ.w_DOC_DETT.MVDESART,40)))
          endif
          this.w_OBJ.w_DOC_DETT.SaveCurrentRecord()     
          this.w_OLDCONTRA = this.w_CONTRATTO
        endif
        if this.w_CHIAVE<>this.w_DBRK
          this.w_DBRK = this.w_CHIAVE
        endif
        * --- Inserisco i dati in un memory cursor con gli stessi record della query GSAG_BDD
        *     per avere sempre il dettaglio elementi sempre a disposizione.
        this.w_ELECONT.AppendBlank()     
        this.w_ELECONT.ELCODCOM = _Curs_GSAG_BDD.ELCODCOM
        this.w_ELECONT.ELRINNOV = _Curs_GSAG_BDD.ELRINNOV 
        this.w_ELECONT.ELCODIMP = _Curs_GSAG_BDD.ELCODIMP
        this.w_ELECONT.ELCODMOD = _Curs_GSAG_BDD.ELCODMOD
        this.w_ELECONT.ELCONTRA = _Curs_GSAG_BDD.ELCONTRA
        this.w_ELECONT.ELDATFAT = _Curs_GSAG_BDD.ELDATFAT
        this.w_ELECONT.ELPERFAT = _Curs_GSAG_BDD.ELPERFAT
        this.w_ELECONT.ELGIODOC = _Curs_GSAG_BDD.ELGIODOC
        this.w_ELECONT.ELDATDOC = _Curs_GSAG_BDD.ELDATDOC
        this.w_ELECONT.ELUNIMIS = _Curs_GSAG_BDD.ELUNIMIS
        this.w_ELECONT.ELQTAMOV = _Curs_GSAG_BDD.ELQTAMOV
        this.w_ELECONT.ELDATFIN = _Curs_GSAG_BDD.ELDATFIN
        this.w_ELECONT.ELTCOLIS = _Curs_GSAG_BDD.ELTCOLIS
        this.w_ELECONT.ELSCOLIS = _Curs_GSAG_BDD.ELSCOLIS
        this.w_ELECONT.ELPROLIS = _Curs_GSAG_BDD.ELPROLIS
        this.w_ELECONT.ELPROSCO = _Curs_GSAG_BDD.ELPROSCO
        this.w_ELECONT.ELCONCOD = Nvl(_Curs_GSAG_BDD.ELCONCOD,"")
        this.w_ELECONT.ELINICOD = _Curs_GSAG_BDD.ELINICOD
        this.w_ELECONT.ELFINCOD = _Curs_GSAG_BDD.ELFINCOD
        this.w_XCONORN = IIF(!Empty(NVL(this.w_CODORN, " ")) And g_XCONDI = "S",this.w_CODORN,this.w_CLIENTE)
        if this.w_TIPAPL="C"
          * --- Se applicazione prezzo immediata
          this.w_ELECONT.ELPREZZO = Nvl(_Curs_GSAG_BDD.ELPREZZO,0)
          this.w_ELECONT.ELSCONT1 = Nvl(_Curs_GSAG_BDD.ELSCONT1,0)
          this.w_ELECONT.ELSCONT2 = Nvl(_Curs_GSAG_BDD.ELSCONT2,0)
          this.w_ELECONT.ELSCONT3 = Nvl(_Curs_GSAG_BDD.ELSCONT3,0)
          this.w_ELECONT.ELSCONT4 = Nvl(_Curs_GSAG_BDD.ELSCONT4,0)
        else
          * --- Se applicazione prezzo differita
          DECLARE L_ArrRet (16,1)
          this.w_ELECONT.ELPREZZO = gsar_bpz(this,Nvl(_Curs_GSAG_BDD.ELCODART,Space(20)),this.w_XCONORN,"C",Nvl(_Curs_GSAG_BDD.ELCODLIS,space(5)),Nvl(_Curs_GSAG_BDD.ELQTAMOV,0),Nvl(_Curs_GSAG_BDD.ELCODVAL,space(5)),this.w_OBJ.w_MVDATREG,Nvl(_Curs_GSAG_BDD.ELUNIMIS,space(3)),_Curs_GSAG_BDD.ELCONCOD)
          this.w_ELECONT.ELSCONT1 = L_ArrRet[1,1]
          this.w_ELECONT.ELSCONT2 = L_ArrRet[2,1]
          this.w_ELECONT.ELSCONT3 = L_ArrRet[3,1]
          this.w_ELECONT.ELSCONT4 = L_ArrRet[4,1]
          this.w_ELECONT.ELCONCOD = L_ArrRet[9,1]
          this.w_VALRIG = CAVALRIG(this.w_ELECONT.ELPREZZO,Nvl(_Curs_GSAG_BDD.ELQTAMOV,0), this.w_ELECONT.ELSCONT1,this.w_ELECONT.ELSCONT2,this.w_ELECONT.ELSCONT3,this.w_ELECONT.ELSCONT4, g_PERPVL)
          RELEASE L_ArrRet
        endif
        this.w_ELECONT.ELCODART = _Curs_GSAG_BDD.ELCODART
        this.w_ELECONT.SaveCurrentRecord()     
        * --- Se raggruppo per prestazione, cerco se esiste nel memorycursor una riga
        *     che ho gi� inserito.
        *     Se la trovo, aggiungo nulla ma sommo le quantit�
        * --- Occorre ignorare il check "Raggruppa per prestazione" per i servizi di tipo "a valore". 
        *     Tali servizi non sono raggruppabili perch� le quantit� non sono cumulabili 
        *     (ogni riga avr� sempre come quantit� 1): per ognuno di essi deve essere
        *     creata una riga distinta, mentre per gli altri servizi potr� essere effettuato 
        *     il raggruppamento (quelli a quantit� e valore). 
        if this.oParentObject.w_DTFLRAGG="S" AND this.w_TIPART="FM"
           
 Select (this.w_OBJ.w_DOC_DETT.ccursor) 
 locate for Alltrim(this.w_ELECONT.ELCODART)=Alltrim(MVCODICE) and this.w_ELECONT.ELUNIMIS=MVUNIMIS and nvl(this.w_ELECONT.ELSCONT1,0)=MVSCONT1 and nvl(this.w_ELECONT.ELSCONT2,0)=MVSCONT2 and nvl(this.w_ELECONT.ELSCONT3,0)=MVSCONT3 and nvl(this.w_ELECONT.ELSCONT4,0)=MVSCONT4; 
 and nvl(this.w_ELECONT.ELCODCEN,space(15))=MVCODCEN and nvl(this.w_ELECONT.ELVOCCEN, space(15))=MVVOCCEN and nvl(this.w_ELECONT.ELCOCOMM,space(15))= MVCODCOM and nvl(this.w_ELECONT.ELCODATT, space(15))=MVCODATT and(this.w_ELECONT.ELPREZZO=MVPREZZO or Isahe())
          if ! found()
            this.w_CPROWNUM = this.w_CPROWNUM+1
            this.w_OBJ.AddDetail(this.w_CPROWNUM,0,NVL(_Curs_GSAG_BDD.ELCODART,SPACE(20)),NVL(_Curs_GSAG_BDD.ELUNIMIS,SPACE(5)),NVL(_Curs_GSAG_BDD.ELQTAMOV,0),IIF(Isahe(),0,NVL(this.w_ELECONT.ELPREZZO,0)),this.w_TIPRIG)     
          else
            this.w_OBJ.w_DOC_DETT.ReadCurrentRecord()     
            this.w_OBJ.w_DOC_DETT.MVQTAMOV = this.w_ELECONT.ELQTAMOV+this.w_OBJ.w_DOC_DETT.MVQTAMOV
          endif
        else
          this.w_CPROWNUM = this.w_CPROWNUM+1
          this.w_OBJ.AddDetail(this.w_CPROWNUM,0,NVL(_Curs_GSAG_BDD.ELCODART,SPACE(20)),NVL(_Curs_GSAG_BDD.ELUNIMIS,SPACE(5)),NVL(_Curs_GSAG_BDD.ELQTAMOV,0),iif(Isahe(),0,NVL(this.w_ELECONT.ELPREZZO,0)),this.w_TIPRIG)     
        endif
        if Isahe() and Nvl(_Curs_GSAG_BDD.TDFLGLIS," ")<>"S"
          * --- Assegno listini di riga
          this.w_OBJ.w_DOC_DETT.MVCODLIS = this.w_ELECONT.ELTCOLIS
          this.w_OBJ.w_DOC_DETT.MVSCOLIS = this.w_ELECONT.ELSCOLIS
          this.w_OBJ.w_DOC_DETT.MVPROLIS = this.w_ELECONT.ELPROLIS
          this.w_OBJ.w_DOC_DETT.MVPROSCO = this.w_ELECONT.ELPROSCO
        endif
        this.w_OBJ.w_DOC_DETT.MVCODIVA = CALCODIV(_Curs_GSAG_BDD.ELCODART, "C", this.w_CLIENTE, this.w_TIPOPE, this.w_OBJ.w_MVDATREG , this.w_ANTIPOPE, this.w_CODIVA, this.w_ARTIPOPE)
        this.w_OBJ.w_DOC_DETT.MVVOCCEN = SearchVoc(this,"C",_Curs_GSAG_BDD.ELCODART,"C",this.w_CLIENTE,this.w_TIPODOC,_Curs_GSAG_BDD.NOCODICE)
        this.w_OBJ.w_DOC_DETT.MVDATGEN = .NULL.
        this.w_OBJ.w_DOC_DETT.MVEFFEVA = .NULL.
        this.w_OBJ.w_DOC_DETT.MVDATEVA = .NULL.
        this.w_OBJ.w_DOC_DETT.MVSCONT1 = this.w_ELECONT.ELSCONT1
        this.w_OBJ.w_DOC_DETT.MVSCONT2 = this.w_ELECONT.ELSCONT2
        this.w_OBJ.w_DOC_DETT.MVSCONT3 = this.w_ELECONT.ELSCONT3
        this.w_OBJ.w_DOC_DETT.MVSCONT4 = this.w_ELECONT.ELSCONT4
        this.w_OBJ.w_DOC_DETT.MVVALRIG = this.w_VALRIG
        this.w_OBJ.w_DOC_DETT.MVDESART = this.w_DESCRIZIONE
        this.w_OBJ.w_DOC_DETT.MVDESSUP = this.w_DESSUP
        if this.w_TIPAPL<>"C"
          this.w_OBJ.w_DOC_DETT.MVCONTRA = this.w_ELECONT.ELCONCOD
        endif
        if isahe()
          this.w_OBJ.w_DOC_DETT.MVSERIAL = "0000000001"
          this.w_OBJ.w_DOC_DETT.MVDATRCO = .NULL.
          this.w_OBJ.w_DOC_DETT.MVDAINCO = this.w_ELECONT.ELINICOD
          this.w_OBJ.w_DOC_DETT.MVDAFICO = this.w_ELECONT.ELFINCOD
        else
          this.w_OBJ.w_DOC_DETT.MVSERIAL = Space(10)
        endif
        this.w_OBJ.w_DOC_DETT.MVINICOM = this.w_ELECONT.ELINICOD
        this.w_OBJ.w_DOC_DETT.MVFINCOM = this.w_ELECONT.ELFINCOD
        * --- Analitica
        * --- leggo se il documento gestisce l'analitica
        if isAhe()
          this.w_FLANAL = docgesana( this.w_TIPODOC, "A")
          this.w_FLGCOM = docgesana( this.w_TIPODOC, "C")
        else
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLANAL,TDFLCOMM"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPODOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLANAL,TDFLCOMM;
              from (i_cTable) where;
                  TDTIPDOC = this.w_TIPODOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
            this.w_FLGCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if !isAhe()
          if g_PERCCR="S" AND this.w_FLANAL="S" 
            if NOT EMPTY(NVL(_Curs_GSAG_BDD.ELCODCEN,SPACE(15)))
              this.w_OBJ.w_DOC_DETT.MVCODCEN = _Curs_GSAG_BDD.ELCODCEN
            else
              this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Riga %1: centro di ricavo non definito")
              this.w_oPART1.addParam(ALLTRIM(STR(this.w_CPROWNUM * 10)))     
            endif
            if NOT EMPTY(NVL(_Curs_GSAG_BDD.ELVOCCEN,SPACE(15)))
              this.w_OBJ.w_DOC_DETT.MVVOCCEN = _Curs_GSAG_BDD.ELVOCCEN
            else
              this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Riga %1: voce di ricavo non definita")
              this.w_oPART1.addParam(ALLTRIM(STR(this.w_CPROWNUM * 10)))     
            endif
          endif
          if g_PERCAN="S" AND this.w_FLGCOM="S"
            if NOT EMPTY(NVL(_Curs_GSAG_BDD.ELCOCOMM,SPACE(15)))
              this.w_OBJ.w_DOC_DETT.MVCODCOM = _Curs_GSAG_BDD.ELCOCOMM
            else
              this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Riga: %1: commessa non definita")
              this.w_oPART1.addParam(ALLTRIM(STR(this.w_CPROWNUM)))     
            endif
            if NOT EMPTY(NVL(_Curs_GSAG_BDD.ELCODATT,SPACE(15)))
              this.w_OBJ.w_DOC_DETT.MVCODATT = _Curs_GSAG_BDD.ELCODATT
            else
              this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Riga: %1: attivit� di commessa non definita")
              this.w_oPART1.addParam(ALLTRIM(STR(this.w_CPROWNUM)))     
            endif
          endif
        else
          if NOT EMPTY(NVL(_Curs_GSAG_BDD.ELCODCEN,SPACE(15)))
            this.w_OBJ.w_DOC_DETT.MVCODCEN = _Curs_GSAG_BDD.ELCODCEN
          else
            this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Riga %1: centro di ricavo non definito")
            this.w_oPART1.addParam(ALLTRIM(STR(this.w_CPROWNUM * 10)))     
          endif
          if NOT EMPTY(NVL(_Curs_GSAG_BDD.ELVOCCEN,SPACE(15)))
            this.w_OBJ.w_DOC_DETT.MVVOCCEN = _Curs_GSAG_BDD.ELVOCCEN
          else
            this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Riga %1: voce di ricavo non definita")
            this.w_oPART1.addParam(ALLTRIM(STR(this.w_CPROWNUM * 10)))     
          endif
          if g_PERCAN="S" OR g_COMM="S" 
            if NOT EMPTY(NVL(_Curs_GSAG_BDD.ELCOCOMM,SPACE(15)))
              this.w_OBJ.w_DOC_DETT.MVCODCOM = _Curs_GSAG_BDD.ELCOCOMM
            else
              this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Riga: %1: commessa non definita")
              this.w_oPART1.addParam(ALLTRIM(STR(this.w_CPROWNUM*10)))     
            endif
            if NOT EMPTY(NVL(_Curs_GSAG_BDD.ELCODATT,SPACE(15)))
              this.w_OBJ.w_DOC_DETT.MVCODATT = _Curs_GSAG_BDD.ELCODATT
            else
              this.w_oPART1 = this.w_oMESS1.addmsgpartNL("Riga: %1: attivit� di commessa non definita")
              this.w_oPART1.addParam(ALLTRIM(STR(this.w_CPROWNUM*10)))     
            endif
          endif
        endif
        this.w_OBJ.w_DOC_DETT.SaveCurrentRecord()     
          select _Curs_GSAG_BDD
          continue
        enddo
        use
      endif
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Insert into DET_DOCU
      i_nConn=i_TableProp[this.DET_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DET_DOCU_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSAG2BDD",this.DET_DOCU_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Inserisco i dati dei documenti generati
      this.w_OBJ = .NULL.
      if this.w_CONTAELEM=0
        ah_ErrorMsg("Per le selezioni impostate, non ci sono dati da elaborare")
      else
        * --- oggetto per mess. incrementali
        if this.w_CONTAELEM<>0 
          this.w_oMESS=createobject("ah_message")
          this.w_oPART = this.w_oMESS.addmsgpartNL("Generati n. %1 documenti su n. %2 documenti da generare")
          this.w_oPART.addParam(ALLTRIM(STR(this.w_CONTADOC)))     
          this.w_oPART.addParam(ALLTRIM(STR(this.w_CONTAELEM)))     
        endif
        this.w_oMESS.ah_ErrorMsg()     
        if this.w_oERRORMESS.IsFullLog()
          this.w_oERRORMESS.PrintLog(this,"Errori /warning riscontrati")     
        endif
      endif
    else
      this.w_OLDSERDOC = "@@@@@@@@@@@"
      this.oParentObject.w_HASEVENT = Ah_yesno("Si desidera cancellare il piano di generazione documenti?")
      if this.oParentObject.w_HASEVENT
        * --- Select from GSAG4BDC
        do vq_exec with 'GSAG4BDC',this,'_Curs_GSAG4BDC','',.f.,.t.
        if used('_Curs_GSAG4BDC')
          select _Curs_GSAG4BDC
          locate for 1=1
          do while not(eof())
          this.w_CONTA = NVL(CONTA,0)
            select _Curs_GSAG4BDC
            continue
          enddo
          use
        endif
        if this.w_CONTA>0
          Ah_ErrorMsg("Impossibile cancellare: esiste almeno un altro piano di generazione documenti successivo")
          this.oParentObject.w_HASEVENT = .F.
        else
          * --- Try
          local bErr_04C25298
          bErr_04C25298=bTrsErr
          this.Try_04C25298()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          bTrsErr=bTrsErr or bErr_04C25298
          * --- End
        endif
      endif
      this.w_oERRORMESS.PrintLog(this,"Errori /warning riscontrati")     
    endif
  endproc
  proc Try_04C25298()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from DET_DOCU
    i_nConn=i_TableProp[this.DET_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_DOCU_idx,2],.t.,this.DET_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DET_DOCU ";
          +" where DDSERIAL="+cp_ToStrODBC(this.oParentObject.w_DTSERIAL)+"";
          +" order by DDSERDOC";
           ,"_Curs_DET_DOCU")
    else
      select * from (i_cTable);
       where DDSERIAL=this.oParentObject.w_DTSERIAL;
       order by DDSERDOC;
        into cursor _Curs_DET_DOCU
    endif
    if used('_Curs_DET_DOCU')
      select _Curs_DET_DOCU
      locate for 1=1
      do while not(eof())
      this.w_SERDOC = _Curs_DET_DOCU.DDSERDOC
      if this.w_OLDSERDOC <> this.w_SERDOC
        this.w_RETVAL = GSAR_BDK(this,this.w_SERDOC)
      endif
      if EMPTY(this.w_RETVAL)
        * --- Select from DOC_GENE
        i_nConn=i_TableProp[this.DOC_GENE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_GENE_idx,2],.t.,this.DOC_GENE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_GENE ";
              +" where DDSERIAL="+cp_ToStrODBC(this.oParentObject.w_DTSERIAL)+" AND DDSERATT="+cp_ToStrODBC(this.w_SERDOC)+"";
              +" order by DDSERIAL desc";
               ,"_Curs_DOC_GENE")
        else
          select * from (i_cTable);
           where DDSERIAL=this.oParentObject.w_DTSERIAL AND DDSERATT=this.w_SERDOC;
           order by DDSERIAL desc;
            into cursor _Curs_DOC_GENE
        endif
        if used('_Curs_DOC_GENE')
          select _Curs_DOC_GENE
          locate for 1=1
          do while not(eof())
          this.w_CONTRATTO = _Curs_DOC_GENE.DDCONTRA
          this.w_MODELLO = _Curs_DOC_GENE.DDCODMOD
          this.w_IMPIANTO = _Curs_DOC_GENE.DDCODIMP
          this.w_KEYCOMP = _Curs_DOC_GENE.DDCOMPON
          this.w_RINNOV = Nvl(_Curs_DOC_GENE.DDRINNOV,0)
          * --- Write into ELE_CONT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ELE_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ELDATFAT ="+cp_NullLink(cp_ToStrODBC(_Curs_DOC_GENE.DDOLDDAT),'ELE_CONT','ELDATFAT');
            +",ELINICOD ="+cp_NullLink(cp_ToStrODBC(_Curs_DOC_GENE.DDINICOM),'ELE_CONT','ELINICOD');
            +",ELFINCOD ="+cp_NullLink(cp_ToStrODBC(_Curs_DOC_GENE.DDFINCOM),'ELE_CONT','ELFINCOD');
                +i_ccchkf ;
            +" where ";
                +"ELCONTRA = "+cp_ToStrODBC(this.w_CONTRATTO);
                +" and ELCODMOD = "+cp_ToStrODBC(this.w_MODELLO);
                +" and ELCODIMP = "+cp_ToStrODBC(this.w_IMPIANTO);
                +" and ELCODCOM = "+cp_ToStrODBC(this.w_KEYCOMP);
                +" and ELRINNOV = "+cp_ToStrODBC(this.w_RINNOV);
                   )
          else
            update (i_cTable) set;
                ELDATFAT = _Curs_DOC_GENE.DDOLDDAT;
                ,ELINICOD = _Curs_DOC_GENE.DDINICOM;
                ,ELFINCOD = _Curs_DOC_GENE.DDFINCOM;
                &i_ccchkf. ;
             where;
                ELCONTRA = this.w_CONTRATTO;
                and ELCODMOD = this.w_MODELLO;
                and ELCODIMP = this.w_IMPIANTO;
                and ELCODCOM = this.w_KEYCOMP;
                and ELRINNOV = this.w_RINNOV;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Delete from DOC_GENE
          i_nConn=i_TableProp[this.DOC_GENE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_GENE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"DDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DTSERIAL);
                  +" and DDSERATT = "+cp_ToStrODBC(this.w_SERDOC);
                   )
          else
            delete from (i_cTable) where;
                  DDSERIAL = this.oParentObject.w_DTSERIAL;
                  and DDSERATT = this.w_SERDOC;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
            select _Curs_DOC_GENE
            continue
          enddo
          use
        endif
        * --- Delete from DET_DOCU
        i_nConn=i_TableProp[this.DET_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DET_DOCU_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"DDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DTSERIAL);
                +" and DDSERDOC = "+cp_ToStrODBC(this.w_SERDOC);
                 )
        else
          delete from (i_cTable) where;
                DDSERIAL = this.oParentObject.w_DTSERIAL;
                and DDSERDOC = this.w_SERDOC;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        this.w_oERRORMESS.AddMsgLogPartNoTrans(space(4), "Riscontrato errore in cancellazione: %0%1", space(4)+ALLTRIM(this.w_RETVAL))     
        this.oParentObject.w_HASEVENT = .F.
      endif
      this.w_OLDSERDOC = this.w_SERDOC
        select _Curs_DET_DOCU
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_DBRK<>"##zz##"
      * --- Try
      local bErr_04C3C688
      bErr_04C3C688=bTrsErr
      this.Try_04C3C688()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_ERROR = this.w_ERROR+1
        this.w_RESULT = iif(Empty(this.w_RESULT),Message(),this.w_RESULT)
        this.w_oERRORMESS.AddMsgLogPartNoTrans("Riscontrato errore su generazione.", space(4)+ALLTRIM(this.w_RESULT))     
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_04C3C688
      * --- End
    endif
  endproc
  proc Try_04C3C688()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_OBJ.bwarning = .t.
    if this.w_FLINTE="N"
      this.w_CODICEIMPIANTO = iif(not empty(this.w_CODIMP),this.w_CODIMP,"Non presente")
      this.w_CODICECOMPONENTE = IIF(this.w_COMPONENTE<>0,this.w_COMPONENTE,"Non presente")
      this.w_oPART1 = this.w_oMESS1.addmsgpartNL(AH_MSGFORMAT("La causale documento non prevede intestatario!%0Impossibile generare il documento per l'elemento contratto:%0Modello: %1, Contratto: %2, Impianto: %3, Componente: %4, Rinnovo: %5 del cliente: %6",ALLTRIM(this.w_CODMOD),ALLTRIM(this.w_CONTRATTO),ALLTRIM(this.w_CODICEIMPIANTO),this.w_CODICECOMPONENTE,this.w_RINNOVO,ALLTRIM(this.w_CLIENTE)))
      this.w_RESULT = this.w_oMESS1.ComposeMessage()
      * --- Raise
      i_Error="La causale documento non prevede intestatario"
      return
    endif
    this.w_OBJ.bRicPro = .t.
    this.w_RESULT = this.w_OBJ.CreateDocument()
    if Isahe()
      this.w_OBJ.btrsok = this.w_OBJ.cRetLogWarn<>"E"
    endif
    if this.w_OBJ.btrsok
      this.w_SERIALE = this.w_OBJ.w_MVSERIAL
      if this.w_OBJ.cRetLogWarn="W"
        this.w_oPART1 = this.w_oMESS1.addmsgpartNL("%1")
        this.w_oPART1.addParam(ALLTRIM(this.w_result))     
        * --- Azzero perch� si tratta di warning
        this.w_RESULT = " "
      endif
      this.w_WARN = this.w_oMESS1.ComposeMessage()
      if NOT EMPTY(this.w_WARN) AND Not Empty(this.w_SERIALE)
        this.w_oERRORMESS.AddMsgLog("Riscontrato warning su generazione doc. seriale %1:%0%2", this.w_SERIALE, this.w_WARN)     
      endif
      this.w_ELECONT.GoTop()     
      do while not this.w_ELECONT.Eof()
        if NOT EMPTY(this.w_ELECONT.ELPERFAT)
          this.w_NEWDATE = NextTime(this.w_ELECONT.ELPERFAT, this.w_ELECONT.ELDATFAT, .F.,.T.)
          this.w_DATFIN = IIF(Type("w_ELECONT.ELDATFIN")="T",TTOD(this.w_ELECONT.ELDATFIN),cp_todate(this.w_ELECONT.ELDATFIN))
          this.w_DATATOLL = IIF(this.w_NEWDATE>this.w_DATFIN+this.w_ELECONT.ELGIODOC, CP_CHARTODATE("  -  -  "), this.w_NEWDATE)
          this.w_NEWCOMPATT1 = IIF(EMPTY(this.w_DATATOLL),CP_CHARTODATE("  -  -  "),CP_TODATE(this.w_ELECONT.ELFINCOD)+1)
          this.w_NEWCOMPATT2 = IIF(EMPTY(this.w_DATATOLL),CP_CHARTODATE("  -  -  "),NEXTTIME( this.w_ELECONT.ELPERFAT, CP_TODATE(this.w_NEWCOMPATT1)-1,.F.,.T. ))
          * --- Write into ELE_CONT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ELE_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ELE_CONT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ELE_CONT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ELDATFAT ="+cp_NullLink(cp_ToStrODBC(this.w_DATATOLL),'ELE_CONT','ELDATFAT');
            +",ELINICOD ="+cp_NullLink(cp_ToStrODBC(this.w_NEWCOMPATT1),'ELE_CONT','ELINICOD');
            +",ELFINCOD ="+cp_NullLink(cp_ToStrODBC(this.w_NEWCOMPATT2),'ELE_CONT','ELFINCOD');
                +i_ccchkf ;
            +" where ";
                +"ELCONTRA = "+cp_ToStrODBC(this.w_ELECONT.ELCONTRA);
                +" and ELCODMOD = "+cp_ToStrODBC(this.w_ELECONT.ELCODMOD);
                +" and ELCODIMP = "+cp_ToStrODBC(this.w_ELECONT.ELCODIMP);
                +" and ELCODCOM = "+cp_ToStrODBC(this.w_ELECONT.ELCODCOM);
                +" and ELPERFAT = "+cp_ToStrODBC(this.w_ELECONT.ELPERFAT);
                +" and ELDATFAT = "+cp_ToStrODBC(this.w_ELECONT.ELDATFAT);
                +" and ELRINNOV = "+cp_ToStrODBC(this.w_ELECONT.ELRINNOV);
                   )
          else
            update (i_cTable) set;
                ELDATFAT = this.w_DATATOLL;
                ,ELINICOD = this.w_NEWCOMPATT1;
                ,ELFINCOD = this.w_NEWCOMPATT2;
                &i_ccchkf. ;
             where;
                ELCONTRA = this.w_ELECONT.ELCONTRA;
                and ELCODMOD = this.w_ELECONT.ELCODMOD;
                and ELCODIMP = this.w_ELECONT.ELCODIMP;
                and ELCODCOM = this.w_ELECONT.ELCODCOM;
                and ELPERFAT = this.w_ELECONT.ELPERFAT;
                and ELDATFAT = this.w_ELECONT.ELDATFAT;
                and ELRINNOV = this.w_ELECONT.ELRINNOV;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Insert into DOC_GENE
        i_nConn=i_TableProp[this.DOC_GENE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_GENE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_GENE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DDSERIAL"+",DDSERATT"+",DDCONTRA"+",DDCODMOD"+",DDCODIMP"+",DDCOMPON"+",DDRINNOV"+",DDOLDDAT"+",DDDATDOC"+",DDINICOM"+",DDFINCOM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DTSERIAL),'DOC_GENE','DDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SERIALE),'DOC_GENE','DDSERATT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELCONTRA),'DOC_GENE','DDCONTRA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELCODMOD),'DOC_GENE','DDCODMOD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELCODIMP),'DOC_GENE','DDCODIMP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELCODCOM),'DOC_GENE','DDCOMPON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELRINNOV),'DOC_GENE','DDRINNOV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELDATFAT),'DOC_GENE','DDOLDDAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELDATDOC),'DOC_GENE','DDDATDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELINICOD),'DOC_GENE','DDINICOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ELECONT.ELFINCOD),'DOC_GENE','DDFINCOM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.oParentObject.w_DTSERIAL,'DDSERATT',this.w_SERIALE,'DDCONTRA',this.w_ELECONT.ELCONTRA,'DDCODMOD',this.w_ELECONT.ELCODMOD,'DDCODIMP',this.w_ELECONT.ELCODIMP,'DDCOMPON',this.w_ELECONT.ELCODCOM,'DDRINNOV',this.w_ELECONT.ELRINNOV,'DDOLDDAT',this.w_ELECONT.ELDATFAT,'DDDATDOC',this.w_ELECONT.ELDATDOC,'DDINICOM',this.w_ELECONT.ELINICOD,'DDFINCOM',this.w_ELECONT.ELFINCOD)
          insert into (i_cTable) (DDSERIAL,DDSERATT,DDCONTRA,DDCODMOD,DDCODIMP,DDCOMPON,DDRINNOV,DDOLDDAT,DDDATDOC,DDINICOM,DDFINCOM &i_ccchkf. );
             values (;
               this.oParentObject.w_DTSERIAL;
               ,this.w_SERIALE;
               ,this.w_ELECONT.ELCONTRA;
               ,this.w_ELECONT.ELCODMOD;
               ,this.w_ELECONT.ELCODIMP;
               ,this.w_ELECONT.ELCODCOM;
               ,this.w_ELECONT.ELRINNOV;
               ,this.w_ELECONT.ELDATFAT;
               ,this.w_ELECONT.ELDATDOC;
               ,this.w_ELECONT.ELINICOD;
               ,this.w_ELECONT.ELFINCOD;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_ELECONT.Delete()     
        this.w_ELECONT.Skip(1)     
      enddo
      if Not EMPTY(this.w_RESULT)
        this.w_oERRORMESS.AddMsgLogPartNoTrans("Riscontrato errore su generazione.", space(4)+ALLTRIM(this.w_RESULT))     
      endif
      this.w_CONTADOC = this.w_CONTADOC+1
      * --- commit
      cp_EndTrs(.t.)
    else
      * --- Se non viene generato il documento per un qualunque motivo
      *     cancello il contenuto del memory cursor.
      do while not this.w_ELECONT.Eof()
        this.w_ELECONT.Delete()     
        this.w_ELECONT.Skip(1)     
      enddo
      * --- Raise
      i_Error="operazione fallita"
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    this.w_ELECONT=createobject("cp_MemoryCursor","ELE_CONT",this)
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='DET_PIAN'
    this.cWorkTables[2]='ELE_CONT'
    this.cWorkTables[3]='DET_GEN'
    this.cWorkTables[4]='DOC_GENE'
    this.cWorkTables[5]='DET_DOCU'
    this.cWorkTables[6]='PAR_AGEN'
    this.cWorkTables[7]='TIP_DOCU'
    this.cWorkTables[8]='CAM_AGAZ'
    this.cWorkTables[9]='GES_DOCU'
    this.cWorkTables[10]='BUSIUNIT'
    this.cWorkTables[11]='AZIENDA'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_GSAG_BDD')
      use in _Curs_GSAG_BDD
    endif
    if used('_Curs_GSAG4BDC')
      use in _Curs_GSAG4BDC
    endif
    if used('_Curs_DET_DOCU')
      use in _Curs_DET_DOCU
    endif
    if used('_Curs_DOC_GENE')
      use in _Curs_DOC_GENE
    endif
    return
  proc RemoveMemoryCursors()
    this.w_ELECONT.Done()
  endproc
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
