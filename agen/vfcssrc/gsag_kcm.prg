* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kcm                                                        *
*              Nuova attivit� per pi� pratiche                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-31                                                      *
* Last revis.: 2014-01-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kcm",oParentObject))

* --- Class definition
define class tgsag_kcm as StdForm
  Top    = 3
  Left   = 6

  * --- Standard Properties
  Width  = 656
  Height = 565+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-01-31"
  HelpContextID=169203863
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=82

  * --- Constant Properties
  _IDX = 0
  CAUMATTI_IDX = 0
  PAR_AGEN_IDX = 0
  OFF_NOMI_IDX = 0
  PRA_OGGE_IDX = 0
  DIPENDEN_IDX = 0
  PRA_DIPA_IDX = 0
  CAT_SOGG_IDX = 0
  RUO_SOGI_IDX = 0
  CPUSERS_IDX = 0
  PRA_SEDI_IDX = 0
  cPrg = "gsag_kcm"
  cComment = "Nuova attivit� per pi� pratiche"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_LetturaParAgen = space(10)
  w_TIPOATT = space(20)
  o_TIPOATT = space(20)
  w_OGGE_ATT = space(254)
  w_CAORASYS = space(1)
  w_PACHKFES = space(1)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_OREINI = space(2)
  o_OREINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_OREFIN = space(2)
  w_MINFIN = space(2)
  w_GIOINI = space(10)
  w_GIOFIN = space(10)
  w_ATDURORE = 0
  w_ATDURMIN = 0
  w_CADTIN = ctot('')
  w_FLNSAP = space(1)
  w_STATO_ATT = space(1)
  w_STATO_ATT = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_NOTE_ATT = space(0)
  w_CODNOMIN = space(20)
  w_DESCAN = space(100)
  w_DESOGG = space(60)
  w_PRAAPE = space(15)
  w_RIFGIU = space(30)
  w_FLAPE = space(1)
  w_FLARCH = space(1)
  w_CACHKNOM = space(1)
  w_DESNOMIN = space(40)
  w_COMODO = space(10)
  w_TIPNOM = space(1)
  w_OFDATDOC = ctod('  /  /  ')
  w_TipoRisorsa = space(1)
  w_FLVALO = space(1)
  w_DESSTA = space(60)
  w_CODOGG = space(10)
  w_RIFERI = space(30)
  w_PRACHI = space(15)
  w_NOTE = space(0)
  w_CODNOM = space(20)
  o_CODNOM = space(20)
  w_CODCAT = space(10)
  w_CODNOM1 = space(20)
  o_CODNOM1 = space(20)
  w_CODCAT1 = space(10)
  w_CODNOM2 = space(20)
  o_CODNOM2 = space(20)
  w_CODCAT2 = space(10)
  w_CodRis = space(5)
  o_CodRis = space(5)
  w_RuolRis = space(5)
  w_CodRis1 = space(5)
  o_CodRis1 = space(5)
  w_RuolRis1 = space(5)
  w_CodUte = 0
  w_CODSED = space(5)
  w_CODDIP = space(5)
  w_DATINIAPE = ctod('  /  /  ')
  w_DATFINAPE = ctod('  /  /  ')
  w_DATINICHI = ctod('  /  /  ')
  w_DATFINCHI = ctod('  /  /  ')
  w_VALORE = space(1)
  o_VALORE = space(1)
  w_VALUGUALE = 0
  w_VALMIN = 0
  w_VALMAX = 0
  w_IMPEGN = 0
  w_VALMAGGIORE = 0
  w_DESNOM1 = space(40)
  w_DESNOM2 = space(40)
  w_DESNOM = space(40)
  w_DESRIS = space(80)
  w_DESUTE = space(80)
  w_DESRIS1 = space(80)
  w_COGN_CODRIS = space(40)
  w_NOME_CODRIS = space(40)
  w_COGN_CODRIS1 = space(40)
  w_NOME_CODRIS1 = space(40)
  w_OB_TEST = ctod('  /  /  ')
  w_DESCAT = space(30)
  w_TIPCAT = space(1)
  w_TIPNOM1 = space(1)
  w_DESCAT1 = space(30)
  w_TIPCAT1 = space(1)
  w_DESCAT2 = space(30)
  w_TIPCAT2 = space(1)
  w_TIPNOM2 = space(1)
  w_AGKCM_SZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kcmPag1","gsag_kcm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(2).addobject("oPag","tgsag_kcmPag2","gsag_kcm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezione pratiche")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOATT_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGKCM_SZOOM = this.oPgFrm.Pages(1).oPag.AGKCM_SZOOM
    DoDefault()
    proc Destroy()
      this.w_AGKCM_SZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='CAUMATTI'
    this.cWorkTables[2]='PAR_AGEN'
    this.cWorkTables[3]='OFF_NOMI'
    this.cWorkTables[4]='PRA_OGGE'
    this.cWorkTables[5]='DIPENDEN'
    this.cWorkTables[6]='PRA_DIPA'
    this.cWorkTables[7]='CAT_SOGG'
    this.cWorkTables[8]='RUO_SOGI'
    this.cWorkTables[9]='CPUSERS'
    this.cWorkTables[10]='PRA_SEDI'
    return(this.OpenAllTables(10))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAG_BCM with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LetturaParAgen=space(10)
      .w_TIPOATT=space(20)
      .w_OGGE_ATT=space(254)
      .w_CAORASYS=space(1)
      .w_PACHKFES=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_OREINI=space(2)
      .w_MININI=space(2)
      .w_DATFIN=ctod("  /  /  ")
      .w_OREFIN=space(2)
      .w_MINFIN=space(2)
      .w_GIOINI=space(10)
      .w_GIOFIN=space(10)
      .w_ATDURORE=0
      .w_ATDURMIN=0
      .w_CADTIN=ctot("")
      .w_FLNSAP=space(1)
      .w_STATO_ATT=space(1)
      .w_STATO_ATT=space(1)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
      .w_NOTE_ATT=space(0)
      .w_CODNOMIN=space(20)
      .w_DESCAN=space(100)
      .w_DESOGG=space(60)
      .w_PRAAPE=space(15)
      .w_RIFGIU=space(30)
      .w_FLAPE=space(1)
      .w_FLARCH=space(1)
      .w_CACHKNOM=space(1)
      .w_DESNOMIN=space(40)
      .w_COMODO=space(10)
      .w_TIPNOM=space(1)
      .w_OFDATDOC=ctod("  /  /  ")
      .w_TipoRisorsa=space(1)
      .w_FLVALO=space(1)
      .w_DESSTA=space(60)
      .w_CODOGG=space(10)
      .w_RIFERI=space(30)
      .w_PRACHI=space(15)
      .w_NOTE=space(0)
      .w_CODNOM=space(20)
      .w_CODCAT=space(10)
      .w_CODNOM1=space(20)
      .w_CODCAT1=space(10)
      .w_CODNOM2=space(20)
      .w_CODCAT2=space(10)
      .w_CodRis=space(5)
      .w_RuolRis=space(5)
      .w_CodRis1=space(5)
      .w_RuolRis1=space(5)
      .w_CodUte=0
      .w_CODSED=space(5)
      .w_CODDIP=space(5)
      .w_DATINIAPE=ctod("  /  /  ")
      .w_DATFINAPE=ctod("  /  /  ")
      .w_DATINICHI=ctod("  /  /  ")
      .w_DATFINCHI=ctod("  /  /  ")
      .w_VALORE=space(1)
      .w_VALUGUALE=0
      .w_VALMIN=0
      .w_VALMAX=0
      .w_IMPEGN=0
      .w_VALMAGGIORE=0
      .w_DESNOM1=space(40)
      .w_DESNOM2=space(40)
      .w_DESNOM=space(40)
      .w_DESRIS=space(80)
      .w_DESUTE=space(80)
      .w_DESRIS1=space(80)
      .w_COGN_CODRIS=space(40)
      .w_NOME_CODRIS=space(40)
      .w_COGN_CODRIS1=space(40)
      .w_NOME_CODRIS1=space(40)
      .w_OB_TEST=ctod("  /  /  ")
      .w_DESCAT=space(30)
      .w_TIPCAT=space(1)
      .w_TIPNOM1=space(1)
      .w_DESCAT1=space(30)
      .w_TIPCAT1=space(1)
      .w_DESCAT2=space(30)
      .w_TIPCAT2=space(1)
      .w_TIPNOM2=space(1)
        .w_LetturaParAgen = i_CodAzi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_LetturaParAgen))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_TIPOATT))
          .link_1_3('Full')
        endif
          .DoRTCalc(3,5,.f.)
        .w_DATINI = i_datSys
        .w_OREINI = PADL(ALLTRIM(STR(HOUR(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        .w_MININI = PADL(ALLTRIM(STR(MINUTE(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        .w_DATFIN = .w_DATINI+INT( (VAL(.w_OREINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)) / 24)
        .w_OREFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_OREINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)),24))),2)
        .w_MINFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_MININI)+.w_ATDURMIN), 60),2)),2)
        .w_GIOINI = IIF(NOT EMPTY(.w_DATINI),LEFT(g_GIORNO[DOW(.w_DATINI)],3), '   ')
        .w_GIOFIN = IIF(NOT EMPTY(.w_DATFIN),LEFT(g_GIORNO[DOW(.w_DATFIN)],3), '   ')
          .DoRTCalc(14,19,.f.)
        .w_DTBSO_CAUMATTI = i_DatSys
        .DoRTCalc(21,22,.f.)
        if not(empty(.w_CODNOMIN))
          .link_1_32('Full')
        endif
        .w_DESCAN = ''
          .DoRTCalc(24,26,.f.)
        .w_FLAPE = 'T'
        .w_FLARCH = 'T'
      .oPgFrm.Page1.oPag.AGKCM_SZOOM.Calculate()
          .DoRTCalc(29,32,.f.)
        .w_OFDATDOC = i_datsys
        .w_TipoRisorsa = 'P'
        .w_FLVALO = IIF( .w_VALORE$'IPS' OR EMPTY(.w_VALORE),.w_VALORE,'D')
        .DoRTCalc(36,37,.f.)
        if not(empty(.w_CODOGG))
          .link_2_4('Full')
        endif
        .DoRTCalc(38,41,.f.)
        if not(empty(.w_CODNOM))
          .link_2_8('Full')
        endif
        .w_CODCAT = ''
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_CODCAT))
          .link_2_9('Full')
        endif
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_CODNOM1))
          .link_2_10('Full')
        endif
        .w_CODCAT1 = ''
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_CODCAT1))
          .link_2_11('Full')
        endif
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_CODNOM2))
          .link_2_12('Full')
        endif
        .w_CODCAT2 = ''
        .DoRTCalc(46,46,.f.)
        if not(empty(.w_CODCAT2))
          .link_2_13('Full')
        endif
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_CodRis))
          .link_2_14('Full')
        endif
        .w_RuolRis = SPACE(5)
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_RuolRis))
          .link_2_15('Full')
        endif
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_CodRis1))
          .link_2_16('Full')
        endif
        .w_RuolRis1 = SPACE(5)
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_RuolRis1))
          .link_2_17('Full')
        endif
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_CodUte))
          .link_2_18('Full')
        endif
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_CODSED))
          .link_2_19('Full')
        endif
          .DoRTCalc(53,58,.f.)
        .w_VALUGUALE = IIF(.w_VALORE='U', .w_VALUGUALE, 0)
        .w_VALMIN = IIF(.w_VALORE='N', .w_VALMIN, 0)
        .w_VALMAX = IIF(.w_VALORE='C', .w_VALMAX, 0)
          .DoRTCalc(62,62,.f.)
        .w_VALMAGGIORE = IIF(.w_VALORE='X', .w_VALMAGGIORE, 0)
          .DoRTCalc(64,66,.f.)
        .w_DESRIS = ALLTRIM(.w_COGN_CODRIS)+' '+ALLTRIM(.w_NOME_CODRIS)
          .DoRTCalc(68,68,.f.)
        .w_DESRIS1 = ALLTRIM(.w_COGN_CODRIS1)+' '+ALLTRIM(.w_NOME_CODRIS1)
          .DoRTCalc(70,73,.f.)
        .w_OB_TEST = i_INIDAT
    endwith
    this.DoRTCalc(75,82,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_47.enabled = this.oPgFrm.Page2.oPag.oBtn_2_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_56.enabled = this.oPgFrm.Page1.oPag.oBtn_1_56.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_LetturaParAgen = i_CodAzi
          .link_1_1('Full')
        .DoRTCalc(2,6,.t.)
        if .o_TIPOATT<>.w_TIPOATT
            .w_OREINI = PADL(ALLTRIM(STR(HOUR(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        endif
        if .o_TIPOATT<>.w_TIPOATT
            .w_MININI = PADL(ALLTRIM(STR(MINUTE(IIF(.w_CAORASYS='S',DATETIME(),.w_CADTIN)))),2,'0')
        endif
        if .o_DATINI<>.w_DATINI.or. .o_TIPOATT<>.w_TIPOATT.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_DATFIN = .w_DATINI+INT( (VAL(.w_OREINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)) / 24)
        endif
        if .o_TIPOATT<>.w_TIPOATT.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_OREFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_OREINI)+.w_ATDURORE+INT((VAL(.w_MININI)+.w_ATDURMIN)/60)),24))),2)
        endif
        if .o_TIPOATT<>.w_TIPOATT.or. .o_OREINI<>.w_OREINI.or. .o_MININI<>.w_MININI
            .w_MINFIN = RIGHT('00'+ALLTRIM(STR(MOD( (VAL(.w_MININI)+.w_ATDURMIN), 60),2)),2)
        endif
        if .o_DATINI<>.w_DATINI
            .w_GIOINI = IIF(NOT EMPTY(.w_DATINI),LEFT(g_GIORNO[DOW(.w_DATINI)],3), '   ')
        endif
        if .o_DATFIN<>.w_DATFIN
            .w_GIOFIN = IIF(NOT EMPTY(.w_DATFIN),LEFT(g_GIORNO[DOW(.w_DATFIN)],3), '   ')
        endif
        .oPgFrm.Page1.oPag.AGKCM_SZOOM.Calculate()
        .DoRTCalc(14,34,.t.)
        if .o_VALORE<>.w_VALORE
            .w_FLVALO = IIF( .w_VALORE$'IPS' OR EMPTY(.w_VALORE),.w_VALORE,'D')
        endif
        .DoRTCalc(36,41,.t.)
        if .o_CODNOM<>.w_CODNOM
            .w_CODCAT = ''
          .link_2_9('Full')
        endif
        .DoRTCalc(43,43,.t.)
        if .o_CODNOM1<>.w_CODNOM1
            .w_CODCAT1 = ''
          .link_2_11('Full')
        endif
        .DoRTCalc(45,45,.t.)
        if .o_CODNOM2<>.w_CODNOM2
            .w_CODCAT2 = ''
          .link_2_13('Full')
        endif
        .DoRTCalc(47,58,.t.)
        if .o_VALORE<>.w_VALORE
            .w_VALUGUALE = IIF(.w_VALORE='U', .w_VALUGUALE, 0)
        endif
        if .o_VALORE<>.w_VALORE
            .w_VALMIN = IIF(.w_VALORE='N', .w_VALMIN, 0)
        endif
        if .o_VALORE<>.w_VALORE
            .w_VALMAX = IIF(.w_VALORE='C', .w_VALMAX, 0)
        endif
        .DoRTCalc(62,62,.t.)
        if .o_VALORE<>.w_VALORE
            .w_VALMAGGIORE = IIF(.w_VALORE='X', .w_VALMAGGIORE, 0)
        endif
        .DoRTCalc(64,66,.t.)
        if .o_CodRis<>.w_CodRis
            .w_DESRIS = ALLTRIM(.w_COGN_CODRIS)+' '+ALLTRIM(.w_NOME_CODRIS)
        endif
        .DoRTCalc(68,68,.t.)
        if .o_CodRis1<>.w_CodRis1
            .w_DESRIS1 = ALLTRIM(.w_COGN_CODRIS1)+' '+ALLTRIM(.w_NOME_CODRIS1)
        endif
        if .o_TIPOATT<>.w_TIPOATT
          .Calculate_OCXFWUSOFQ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(70,82,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AGKCM_SZOOM.Calculate()
    endwith
  return

  proc Calculate_DOBRBAQOOH()
    with this
          * --- Attiva la prima pagina
          .oPgFrm.ActivePage = 1
    endwith
  endproc
  proc Calculate_OCXFWUSOFQ()
    with this
          * --- Azzera w_CODNOMIN
          .w_CODNOMIN = IIF(EMPTY(.w_CACHKNOM), space(20), .w_CODNOMIN)
          .link_1_32('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSTATO_ATT_1_27.enabled = this.oPgFrm.Page1.oPag.oSTATO_ATT_1_27.mCond()
    this.oPgFrm.Page1.oPag.oSTATO_ATT_1_28.enabled = this.oPgFrm.Page1.oPag.oSTATO_ATT_1_28.mCond()
    this.oPgFrm.Page1.oPag.oCODNOMIN_1_32.enabled = this.oPgFrm.Page1.oPag.oCODNOMIN_1_32.mCond()
    this.oPgFrm.Page2.oPag.oCODCAT_2_9.enabled = this.oPgFrm.Page2.oPag.oCODCAT_2_9.mCond()
    this.oPgFrm.Page2.oPag.oCODCAT1_2_11.enabled = this.oPgFrm.Page2.oPag.oCODCAT1_2_11.mCond()
    this.oPgFrm.Page2.oPag.oCODCAT2_2_13.enabled = this.oPgFrm.Page2.oPag.oCODCAT2_2_13.mCond()
    this.oPgFrm.Page2.oPag.oRuolRis_2_15.enabled = this.oPgFrm.Page2.oPag.oRuolRis_2_15.mCond()
    this.oPgFrm.Page2.oPag.oRuolRis1_2_17.enabled = this.oPgFrm.Page2.oPag.oRuolRis1_2_17.mCond()
    this.oPgFrm.Page2.oPag.oVALUGUALE_2_29.enabled = this.oPgFrm.Page2.oPag.oVALUGUALE_2_29.mCond()
    this.oPgFrm.Page2.oPag.oVALMIN_2_31.enabled = this.oPgFrm.Page2.oPag.oVALMIN_2_31.mCond()
    this.oPgFrm.Page2.oPag.oVALMAX_2_32.enabled = this.oPgFrm.Page2.oPag.oVALMAX_2_32.mCond()
    this.oPgFrm.Page2.oPag.oVALMAGGIORE_2_39.enabled = this.oPgFrm.Page2.oPag.oVALMAGGIORE_2_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_56.enabled = this.oPgFrm.Page1.oPag.oBtn_1_56.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page1.oPag.oCODNOMIN_1_32
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSTATO_ATT_1_27.visible=!this.oPgFrm.Page1.oPag.oSTATO_ATT_1_27.mHide()
    this.oPgFrm.Page1.oPag.oSTATO_ATT_1_28.visible=!this.oPgFrm.Page1.oPag.oSTATO_ATT_1_28.mHide()
    this.oPgFrm.Page1.oPag.oFLARCH_1_40.visible=!this.oPgFrm.Page1.oPag.oFLARCH_1_40.mHide()
    this.oPgFrm.Page2.oPag.oCODCAT_2_9.visible=!this.oPgFrm.Page2.oPag.oCODCAT_2_9.mHide()
    this.oPgFrm.Page2.oPag.oCODCAT1_2_11.visible=!this.oPgFrm.Page2.oPag.oCODCAT1_2_11.mHide()
    this.oPgFrm.Page2.oPag.oCODCAT2_2_13.visible=!this.oPgFrm.Page2.oPag.oCODCAT2_2_13.mHide()
    this.oPgFrm.Page2.oPag.oRuolRis_2_15.visible=!this.oPgFrm.Page2.oPag.oRuolRis_2_15.mHide()
    this.oPgFrm.Page2.oPag.oRuolRis1_2_17.visible=!this.oPgFrm.Page2.oPag.oRuolRis1_2_17.mHide()
    this.oPgFrm.Page2.oPag.oVALUGUALE_2_29.visible=!this.oPgFrm.Page2.oPag.oVALUGUALE_2_29.mHide()
    this.oPgFrm.Page2.oPag.oVALMIN_2_31.visible=!this.oPgFrm.Page2.oPag.oVALMIN_2_31.mHide()
    this.oPgFrm.Page2.oPag.oVALMAX_2_32.visible=!this.oPgFrm.Page2.oPag.oVALMAX_2_32.mHide()
    this.oPgFrm.Page2.oPag.oVALMAGGIORE_2_39.visible=!this.oPgFrm.Page2.oPag.oVALMAGGIORE_2_39.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_49.visible=!this.oPgFrm.Page2.oPag.oStr_2_49.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_52.visible=!this.oPgFrm.Page2.oPag.oStr_2_52.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_57.visible=!this.oPgFrm.Page2.oPag.oStr_2_57.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_60.visible=!this.oPgFrm.Page2.oPag.oStr_2_60.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AGKCM_SZOOM.Event(cEvent)
        if lower(cEvent)==lower("Ricerca")
          .Calculate_DOBRBAQOOH()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LetturaParAgen
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LetturaParAgen) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LetturaParAgen)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACHKFES";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LetturaParAgen);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LetturaParAgen)
            select PACODAZI,PACHKFES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LetturaParAgen = NVL(_Link_.PACODAZI,space(10))
      this.w_PACHKFES = NVL(_Link_.PACHKFES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LetturaParAgen = space(10)
      endif
      this.w_PACHKFES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LetturaParAgen Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOATT
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAG_MCA',True,'CAUMATTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_TIPOATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAORASYS,CADURORE,CADURMIN,CADATINI,CAFLNSAP,CASTAATT,CA__NOTE,CACHKNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_TIPOATT))
          select CACODICE,CADESCRI,CAORASYS,CADURORE,CADURMIN,CADATINI,CAFLNSAP,CASTAATT,CA__NOTE,CACHKNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPOATT) and !this.bDontReportError
            deferred_cp_zoom('CAUMATTI','*','CACODICE',cp_AbsName(oSource.parent,'oTIPOATT_1_3'),i_cWhere,'GSAG_MCA',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAORASYS,CADURORE,CADURMIN,CADATINI,CAFLNSAP,CASTAATT,CA__NOTE,CACHKNOM";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCRI,CAORASYS,CADURORE,CADURMIN,CADATINI,CAFLNSAP,CASTAATT,CA__NOTE,CACHKNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCRI,CAORASYS,CADURORE,CADURMIN,CADATINI,CAFLNSAP,CASTAATT,CA__NOTE,CACHKNOM";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_TIPOATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_TIPOATT)
            select CACODICE,CADESCRI,CAORASYS,CADURORE,CADURMIN,CADATINI,CAFLNSAP,CASTAATT,CA__NOTE,CACHKNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOATT = NVL(_Link_.CACODICE,space(20))
      this.w_OGGE_ATT = NVL(_Link_.CADESCRI,space(254))
      this.w_CAORASYS = NVL(_Link_.CAORASYS,space(1))
      this.w_ATDURORE = NVL(_Link_.CADURORE,0)
      this.w_ATDURMIN = NVL(_Link_.CADURMIN,0)
      this.w_CADTIN = NVL(_Link_.CADATINI,ctot(""))
      this.w_FLNSAP = NVL(_Link_.CAFLNSAP,space(1))
      this.w_STATO_ATT = NVL(_Link_.CASTAATT,space(1))
      this.w_NOTE_ATT = NVL(_Link_.CA__NOTE,space(0))
      this.w_CACHKNOM = NVL(_Link_.CACHKNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOATT = space(20)
      endif
      this.w_OGGE_ATT = space(254)
      this.w_CAORASYS = space(1)
      this.w_ATDURORE = 0
      this.w_ATDURMIN = 0
      this.w_CADTIN = ctot("")
      this.w_FLNSAP = space(1)
      this.w_STATO_ATT = space(1)
      this.w_NOTE_ATT = space(0)
      this.w_CACHKNOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOMIN
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOMIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOMIN)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOMIN))
          select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOMIN)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOMIN)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOMIN)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOMIN)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOMIN)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOMIN) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOMIN_1_32'),i_cWhere,'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOMIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOMIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOMIN)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOMIN = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOMIN = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(10))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOMIN = space(20)
      endif
      this.w_DESNOMIN = space(40)
      this.w_COMODO = space(10)
      this.w_TIPNOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOMIN = space(20)
        this.w_DESNOMIN = space(40)
        this.w_COMODO = space(10)
        this.w_TIPNOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOMIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODOGG
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_OGGE_IDX,3]
    i_lTable = "PRA_OGGE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_OGGE_IDX,2], .t., this.PRA_OGGE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_OGGE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODOGG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_AOP',True,'PRA_OGGE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OPCODICE like "+cp_ToStrODBC(trim(this.w_CODOGG)+"%");

          i_ret=cp_SQL(i_nConn,"select OPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OPCODICE',trim(this.w_CODOGG))
          select OPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODOGG)==trim(_Link_.OPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_CODOGG)+"%");

            i_ret=cp_SQL(i_nConn,"select OPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_CODOGG)+"%");

            select OPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODOGG) and !this.bDontReportError
            deferred_cp_zoom('PRA_OGGE','*','OPCODICE',cp_AbsName(oSource.parent,'oCODOGG_2_4'),i_cWhere,'GSPR_AOP',"Oggetti pratica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where OPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OPCODICE',oSource.xKey(1))
            select OPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODOGG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where OPCODICE="+cp_ToStrODBC(this.w_CODOGG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OPCODICE',this.w_CODOGG)
            select OPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODOGG = NVL(_Link_.OPCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODOGG = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_OGGE_IDX,2])+'\'+cp_ToStr(_Link_.OPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_OGGE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODOGG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM))
          select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM_2_8'),i_cWhere,'GSAR_ANO',"Elenco nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOM = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(10))
      this.w_TIPNOM = NVL(_Link_.NOTIPNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM = space(20)
      endif
      this.w_DESNOM = space(40)
      this.w_COMODO = space(10)
      this.w_TIPNOM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM<>'G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM = space(20)
        this.w_DESNOM = space(40)
        this.w_COMODO = space(10)
        this.w_TIPNOM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SOGG_IDX,3]
    i_lTable = "CAT_SOGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2], .t., this.CAT_SOGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACS',True,'CAT_SOGG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCAT like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODCAT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODCAT',trim(this.w_CODCAT))
          select CSCODCAT,CSDESCAT,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODCAT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.CSCODCAT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStr(trim(this.w_CODCAT)+"%");

            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CAT_SOGG','*','CSCODCAT',cp_AbsName(oSource.parent,'oCODCAT_2_9'),i_cWhere,'GSPR_ACS',"Categorie soggetti esterni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',oSource.xKey(1))
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',this.w_CODCAT)
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.CSCODCAT,space(10))
      this.w_DESCAT = NVL(_Link_.CSDESCAT,space(30))
      this.w_TIPCAT = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(10)
      endif
      this.w_DESCAT = space(30)
      this.w_TIPCAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCAT,1)
      cp_ShowWarn(i_cKey,this.CAT_SOGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM1
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM1)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM1))
          select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM1)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM1)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM1)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM1)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM1)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM1) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM1_2_10'),i_cWhere,'GSAR_ANO',"Elenco nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM1)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM1 = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOM1 = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(10))
      this.w_TIPNOM1 = NVL(_Link_.NOTIPNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM1 = space(20)
      endif
      this.w_DESNOM1 = space(40)
      this.w_COMODO = space(10)
      this.w_TIPNOM1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM1<>'G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM1 = space(20)
        this.w_DESNOM1 = space(40)
        this.w_COMODO = space(10)
        this.w_TIPNOM1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT1
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SOGG_IDX,3]
    i_lTable = "CAT_SOGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2], .t., this.CAT_SOGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACS',True,'CAT_SOGG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCAT like "+cp_ToStrODBC(trim(this.w_CODCAT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODCAT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODCAT',trim(this.w_CODCAT1))
          select CSCODCAT,CSDESCAT,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODCAT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT1)==trim(_Link_.CSCODCAT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStrODBC(trim(this.w_CODCAT1)+"%");

            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStr(trim(this.w_CODCAT1)+"%");

            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT1) and !this.bDontReportError
            deferred_cp_zoom('CAT_SOGG','*','CSCODCAT',cp_AbsName(oSource.parent,'oCODCAT1_2_11'),i_cWhere,'GSPR_ACS',"Categorie soggetti esterni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',oSource.xKey(1))
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(this.w_CODCAT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',this.w_CODCAT1)
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT1 = NVL(_Link_.CSCODCAT,space(10))
      this.w_DESCAT1 = NVL(_Link_.CSDESCAT,space(30))
      this.w_TIPCAT1 = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT1 = space(10)
      endif
      this.w_DESCAT1 = space(30)
      this.w_TIPCAT1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCAT,1)
      cp_ShowWarn(i_cKey,this.CAT_SOGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODNOM2
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODNOM2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANO',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_CODNOM2)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_CODNOM2))
          select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODNOM2)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_CODNOM2)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStr(trim(this.w_CODNOM2)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStrODBC(trim(this.w_CODNOM2)+"%");

            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NODESCR2 like "+cp_ToStr(trim(this.w_CODNOM2)+"%");

            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODNOM2) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oCODNOM2_2_12'),i_cWhere,'GSAR_ANO',"Elenco nominativi",'GSOF3QNO.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODNOM2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_CODNOM2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_CODNOM2)
            select NOCODICE,NODESCRI,NODESCR2,NOTIPNOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODNOM2 = NVL(_Link_.NOCODICE,space(20))
      this.w_DESNOM2 = NVL(_Link_.NODESCRI,space(40))
      this.w_COMODO = NVL(_Link_.NODESCR2,space(10))
      this.w_TIPNOM2 = NVL(_Link_.NOTIPNOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODNOM2 = space(20)
      endif
      this.w_DESNOM2 = space(40)
      this.w_COMODO = space(10)
      this.w_TIPNOM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPNOM2<>'G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODNOM2 = space(20)
        this.w_DESNOM2 = space(40)
        this.w_COMODO = space(10)
        this.w_TIPNOM2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODNOM2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT2
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SOGG_IDX,3]
    i_lTable = "CAT_SOGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2], .t., this.CAT_SOGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACS',True,'CAT_SOGG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODCAT like "+cp_ToStrODBC(trim(this.w_CODCAT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODCAT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODCAT',trim(this.w_CODCAT2))
          select CSCODCAT,CSDESCAT,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODCAT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT2)==trim(_Link_.CSCODCAT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStrODBC(trim(this.w_CODCAT2)+"%");

            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CSDESCAT like "+cp_ToStr(trim(this.w_CODCAT2)+"%");

            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT2) and !this.bDontReportError
            deferred_cp_zoom('CAT_SOGG','*','CSCODCAT',cp_AbsName(oSource.parent,'oCODCAT2_2_13'),i_cWhere,'GSPR_ACS',"Categorie soggetti esterni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',oSource.xKey(1))
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODCAT,CSDESCAT,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODCAT="+cp_ToStrODBC(this.w_CODCAT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODCAT',this.w_CODCAT2)
            select CSCODCAT,CSDESCAT,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT2 = NVL(_Link_.CSCODCAT,space(10))
      this.w_DESCAT2 = NVL(_Link_.CSDESCAT,space(30))
      this.w_TIPCAT2 = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT2 = space(10)
      endif
      this.w_DESCAT2 = space(30)
      this.w_TIPCAT2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SOGG_IDX,2])+'\'+cp_ToStr(_Link_.CSCODCAT,1)
      cp_ShowWarn(i_cKey,this.CAT_SOGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodRis
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodRis) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_CodRis))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CodRis)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CodRis)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CodRis) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCodRis_2_14'),i_cWhere,'',"Risorse interne",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodRis)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CodRis);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_CodRis)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodRis = NVL(_Link_.DPCODICE,space(5))
      this.w_COGN_CODRIS = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME_CODRIS = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CodRis = space(5)
      endif
      this.w_COGN_CODRIS = space(40)
      this.w_NOME_CODRIS = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodRis Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RuolRis
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RUO_SOGI_IDX,3]
    i_lTable = "RUO_SOGI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2], .t., this.RUO_SOGI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RuolRis) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ARI',True,'RUO_SOGI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RSCODRUO like "+cp_ToStrODBC(trim(this.w_RuolRis)+"%");

          i_ret=cp_SQL(i_nConn,"select RSCODRUO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RSCODRUO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RSCODRUO',trim(this.w_RuolRis))
          select RSCODRUO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RSCODRUO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RuolRis)==trim(_Link_.RSCODRUO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RuolRis) and !this.bDontReportError
            deferred_cp_zoom('RUO_SOGI','*','RSCODRUO',cp_AbsName(oSource.parent,'oRuolRis_2_15'),i_cWhere,'GSPR_ARI',"Ruoli soggetti interni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RSCODRUO";
                     +" from "+i_cTable+" "+i_lTable+" where RSCODRUO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RSCODRUO',oSource.xKey(1))
            select RSCODRUO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RuolRis)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RSCODRUO";
                   +" from "+i_cTable+" "+i_lTable+" where RSCODRUO="+cp_ToStrODBC(this.w_RuolRis);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RSCODRUO',this.w_RuolRis)
            select RSCODRUO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RuolRis = NVL(_Link_.RSCODRUO,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RuolRis = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2])+'\'+cp_ToStr(_Link_.RSCODRUO,1)
      cp_ShowWarn(i_cKey,this.RUO_SOGI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RuolRis Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodRis1
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodRis1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CodRis1)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_CodRis1))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CodRis1)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CodRis1)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CodRis1)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CodRis1)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CodRis1)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CodRis1) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCodRis1_2_16'),i_cWhere,'',"Risorse interne",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodRis1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CodRis1);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_CodRis1)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodRis1 = NVL(_Link_.DPCODICE,space(5))
      this.w_COGN_CODRIS1 = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME_CODRIS1 = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CodRis1 = space(5)
      endif
      this.w_COGN_CODRIS1 = space(40)
      this.w_NOME_CODRIS1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodRis1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RuolRis1
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RUO_SOGI_IDX,3]
    i_lTable = "RUO_SOGI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2], .t., this.RUO_SOGI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RuolRis1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ARI',True,'RUO_SOGI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RSCODRUO like "+cp_ToStrODBC(trim(this.w_RuolRis1)+"%");

          i_ret=cp_SQL(i_nConn,"select RSCODRUO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RSCODRUO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RSCODRUO',trim(this.w_RuolRis1))
          select RSCODRUO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RSCODRUO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RuolRis1)==trim(_Link_.RSCODRUO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RuolRis1) and !this.bDontReportError
            deferred_cp_zoom('RUO_SOGI','*','RSCODRUO',cp_AbsName(oSource.parent,'oRuolRis1_2_17'),i_cWhere,'GSPR_ARI',"Ruoli soggetti interni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RSCODRUO";
                     +" from "+i_cTable+" "+i_lTable+" where RSCODRUO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RSCODRUO',oSource.xKey(1))
            select RSCODRUO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RuolRis1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RSCODRUO";
                   +" from "+i_cTable+" "+i_lTable+" where RSCODRUO="+cp_ToStrODBC(this.w_RuolRis1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RSCODRUO',this.w_RuolRis1)
            select RSCODRUO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RuolRis1 = NVL(_Link_.RSCODRUO,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_RuolRis1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RUO_SOGI_IDX,2])+'\'+cp_ToStr(_Link_.RSCODRUO,1)
      cp_ShowWarn(i_cKey,this.RUO_SOGI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RuolRis1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CodUte
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CodUte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CodUte);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CodUte)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CodUte) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oCodUte_2_18'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CodUte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CodUte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CodUte)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CodUte = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_CodUte = 0
      endif
      this.w_DESUTE = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CodUte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSED
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_SEDI_IDX,3]
    i_lTable = "PRA_SEDI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_SEDI_IDX,2], .t., this.PRA_SEDI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_SEDI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ADP',True,'PRA_SEDI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SPCODICE like "+cp_ToStrODBC(trim(this.w_CODSED)+"%");

          i_ret=cp_SQL(i_nConn,"select SPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SPCODICE',trim(this.w_CODSED))
          select SPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSED)==trim(_Link_.SPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSED) and !this.bDontReportError
            deferred_cp_zoom('PRA_SEDI','*','SPCODICE',cp_AbsName(oSource.parent,'oCODSED_2_19'),i_cWhere,'GSPR_ADP',"Dipartimenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODICE',oSource.xKey(1))
            select SPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SPCODICE="+cp_ToStrODBC(this.w_CODSED);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODICE',this.w_CODSED)
            select SPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSED = NVL(_Link_.SPCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODSED = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_SEDI_IDX,2])+'\'+cp_ToStr(_Link_.SPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_SEDI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOATT_1_3.value==this.w_TIPOATT)
      this.oPgFrm.Page1.oPag.oTIPOATT_1_3.value=this.w_TIPOATT
    endif
    if not(this.oPgFrm.Page1.oPag.oOGGE_ATT_1_5.value==this.w_OGGE_ATT)
      this.oPgFrm.Page1.oPag.oOGGE_ATT_1_5.value=this.w_OGGE_ATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_10.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_10.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oOREINI_1_11.value==this.w_OREINI)
      this.oPgFrm.Page1.oPag.oOREINI_1_11.value=this.w_OREINI
    endif
    if not(this.oPgFrm.Page1.oPag.oMININI_1_12.value==this.w_MININI)
      this.oPgFrm.Page1.oPag.oMININI_1_12.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_13.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_13.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOREFIN_1_14.value==this.w_OREFIN)
      this.oPgFrm.Page1.oPag.oOREFIN_1_14.value=this.w_OREFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMINFIN_1_15.value==this.w_MINFIN)
      this.oPgFrm.Page1.oPag.oMINFIN_1_15.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOINI_1_20.value==this.w_GIOINI)
      this.oPgFrm.Page1.oPag.oGIOINI_1_20.value=this.w_GIOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOFIN_1_21.value==this.w_GIOFIN)
      this.oPgFrm.Page1.oPag.oGIOFIN_1_21.value=this.w_GIOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_ATT_1_27.RadioValue()==this.w_STATO_ATT)
      this.oPgFrm.Page1.oPag.oSTATO_ATT_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATO_ATT_1_28.RadioValue()==this.w_STATO_ATT)
      this.oPgFrm.Page1.oPag.oSTATO_ATT_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_ATT_1_31.value==this.w_NOTE_ATT)
      this.oPgFrm.Page1.oPag.oNOTE_ATT_1_31.value=this.w_NOTE_ATT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODNOMIN_1_32.value==this.w_CODNOMIN)
      this.oPgFrm.Page1.oPag.oCODNOMIN_1_32.value=this.w_CODNOMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_35.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_35.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESOGG_1_36.value==this.w_DESOGG)
      this.oPgFrm.Page1.oPag.oDESOGG_1_36.value=this.w_DESOGG
    endif
    if not(this.oPgFrm.Page1.oPag.oPRAAPE_1_37.value==this.w_PRAAPE)
      this.oPgFrm.Page1.oPag.oPRAAPE_1_37.value=this.w_PRAAPE
    endif
    if not(this.oPgFrm.Page1.oPag.oRIFGIU_1_38.value==this.w_RIFGIU)
      this.oPgFrm.Page1.oPag.oRIFGIU_1_38.value=this.w_RIFGIU
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAPE_1_39.RadioValue()==this.w_FLAPE)
      this.oPgFrm.Page1.oPag.oFLAPE_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLARCH_1_40.RadioValue()==this.w_FLARCH)
      this.oPgFrm.Page1.oPag.oFLARCH_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOMIN_1_51.value==this.w_DESNOMIN)
      this.oPgFrm.Page1.oPag.oDESNOMIN_1_51.value=this.w_DESNOMIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSTA_2_3.value==this.w_DESSTA)
      this.oPgFrm.Page2.oPag.oDESSTA_2_3.value=this.w_DESSTA
    endif
    if not(this.oPgFrm.Page2.oPag.oCODOGG_2_4.value==this.w_CODOGG)
      this.oPgFrm.Page2.oPag.oCODOGG_2_4.value=this.w_CODOGG
    endif
    if not(this.oPgFrm.Page2.oPag.oRIFERI_2_5.value==this.w_RIFERI)
      this.oPgFrm.Page2.oPag.oRIFERI_2_5.value=this.w_RIFERI
    endif
    if not(this.oPgFrm.Page2.oPag.oPRACHI_2_6.value==this.w_PRACHI)
      this.oPgFrm.Page2.oPag.oPRACHI_2_6.value=this.w_PRACHI
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTE_2_7.value==this.w_NOTE)
      this.oPgFrm.Page2.oPag.oNOTE_2_7.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oCODNOM_2_8.value==this.w_CODNOM)
      this.oPgFrm.Page2.oPag.oCODNOM_2_8.value=this.w_CODNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCAT_2_9.RadioValue()==this.w_CODCAT)
      this.oPgFrm.Page2.oPag.oCODCAT_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODNOM1_2_10.value==this.w_CODNOM1)
      this.oPgFrm.Page2.oPag.oCODNOM1_2_10.value=this.w_CODNOM1
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCAT1_2_11.RadioValue()==this.w_CODCAT1)
      this.oPgFrm.Page2.oPag.oCODCAT1_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODNOM2_2_12.value==this.w_CODNOM2)
      this.oPgFrm.Page2.oPag.oCODNOM2_2_12.value=this.w_CODNOM2
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCAT2_2_13.RadioValue()==this.w_CODCAT2)
      this.oPgFrm.Page2.oPag.oCODCAT2_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCodRis_2_14.value==this.w_CodRis)
      this.oPgFrm.Page2.oPag.oCodRis_2_14.value=this.w_CodRis
    endif
    if not(this.oPgFrm.Page2.oPag.oRuolRis_2_15.RadioValue()==this.w_RuolRis)
      this.oPgFrm.Page2.oPag.oRuolRis_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCodRis1_2_16.value==this.w_CodRis1)
      this.oPgFrm.Page2.oPag.oCodRis1_2_16.value=this.w_CodRis1
    endif
    if not(this.oPgFrm.Page2.oPag.oRuolRis1_2_17.RadioValue()==this.w_RuolRis1)
      this.oPgFrm.Page2.oPag.oRuolRis1_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCodUte_2_18.value==this.w_CodUte)
      this.oPgFrm.Page2.oPag.oCodUte_2_18.value=this.w_CodUte
    endif
    if not(this.oPgFrm.Page2.oPag.oCODSED_2_19.RadioValue()==this.w_CODSED)
      this.oPgFrm.Page2.oPag.oCODSED_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODDIP_2_20.RadioValue()==this.w_CODDIP)
      this.oPgFrm.Page2.oPag.oCODDIP_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINIAPE_2_24.value==this.w_DATINIAPE)
      this.oPgFrm.Page2.oPag.oDATINIAPE_2_24.value=this.w_DATINIAPE
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFINAPE_2_25.value==this.w_DATFINAPE)
      this.oPgFrm.Page2.oPag.oDATFINAPE_2_25.value=this.w_DATFINAPE
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINICHI_2_26.value==this.w_DATINICHI)
      this.oPgFrm.Page2.oPag.oDATINICHI_2_26.value=this.w_DATINICHI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFINCHI_2_27.value==this.w_DATFINCHI)
      this.oPgFrm.Page2.oPag.oDATFINCHI_2_27.value=this.w_DATFINCHI
    endif
    if not(this.oPgFrm.Page2.oPag.oVALORE_2_28.RadioValue()==this.w_VALORE)
      this.oPgFrm.Page2.oPag.oVALORE_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVALUGUALE_2_29.value==this.w_VALUGUALE)
      this.oPgFrm.Page2.oPag.oVALUGUALE_2_29.value=this.w_VALUGUALE
    endif
    if not(this.oPgFrm.Page2.oPag.oVALMIN_2_31.value==this.w_VALMIN)
      this.oPgFrm.Page2.oPag.oVALMIN_2_31.value=this.w_VALMIN
    endif
    if not(this.oPgFrm.Page2.oPag.oVALMAX_2_32.value==this.w_VALMAX)
      this.oPgFrm.Page2.oPag.oVALMAX_2_32.value=this.w_VALMAX
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPEGN_2_36.RadioValue()==this.w_IMPEGN)
      this.oPgFrm.Page2.oPag.oIMPEGN_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVALMAGGIORE_2_39.value==this.w_VALMAGGIORE)
      this.oPgFrm.Page2.oPag.oVALMAGGIORE_2_39.value=this.w_VALMAGGIORE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOM1_2_43.value==this.w_DESNOM1)
      this.oPgFrm.Page2.oPag.oDESNOM1_2_43.value=this.w_DESNOM1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOM2_2_44.value==this.w_DESNOM2)
      this.oPgFrm.Page2.oPag.oDESNOM2_2_44.value=this.w_DESNOM2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOM_2_51.value==this.w_DESNOM)
      this.oPgFrm.Page2.oPag.oDESNOM_2_51.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIS_2_53.value==this.w_DESRIS)
      this.oPgFrm.Page2.oPag.oDESRIS_2_53.value=this.w_DESRIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESUTE_2_55.value==this.w_DESUTE)
      this.oPgFrm.Page2.oPag.oDESUTE_2_55.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIS1_2_58.value==this.w_DESRIS1)
      this.oPgFrm.Page2.oPag.oDESRIS1_2_58.value=this.w_DESRIS1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_OGGE_ATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOGGE_ATT_1_5.SetFocus()
            i_bnoObbl = !empty(.w_OGGE_ATT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_OREINI)) or not(VAL(.w_OREINI) < 24))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREINI_1_11.SetFocus()
            i_bnoObbl = !empty(.w_OREINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   ((empty(.w_MININI)) or not(VAL(.w_MININI) < 60))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMININI_1_12.SetFocus()
            i_bnoObbl = !empty(.w_MININI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   (empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_OREFIN)) or not(VAL(.w_OREFIN) < 24))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREFIN_1_14.SetFocus()
            i_bnoObbl = !empty(.w_OREFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   ((empty(.w_MINFIN)) or not(VAL(.w_MINFIN) < 60))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINFIN_1_15.SetFocus()
            i_bnoObbl = !empty(.w_MINFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   ((empty(.w_CODNOMIN) and (.w_CACHKNOM='P')) or not(.w_TIPNOM<>'G' AND .w_TIPNOM<>'F'))  and (NOT EMPTY(.w_CACHKNOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODNOMIN_1_32.SetFocus()
            i_bnoObbl = !empty(.w_CODNOMIN) or !(.w_CACHKNOM='P')
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPNOM<>'G')  and not(empty(.w_CODNOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODNOM_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPNOM1<>'G')  and not(empty(.w_CODNOM1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODNOM1_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPNOM2<>'G')  and not(empty(.w_CODNOM2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODNOM2_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINIAPE<=.w_DATFINAPE OR EMPTY(.w_DATFINAPE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATINIAPE_2_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data apertura iniziale � superiore alla finale")
          case   not(.w_DATINIAPE<=.w_DATFINAPE OR EMPTY(.w_DATINIAPE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATFINAPE_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data apertura iniziale � superiore alla finale")
          case   not(.w_DATINICHI<=.w_DATFINCHI OR EMPTY(.w_DATFINCHI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATINICHI_2_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data chiusura iniziale � superiore alla finale")
          case   not(.w_DATINICHI<=.w_DATFINCHI OR EMPTY(.w_DATINICHI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATFINCHI_2_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data chiusura iniziale � superiore alla finale")
          case   not(.w_VALMIN >= 0 AND ( EMPTY(.w_VALMAX) OR .w_VALMIN<=.w_VALMAX ))  and not(! .w_VALORE$'NC')  and (.w_VALORE='N' OR .w_VALORE='C')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVALMIN_2_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare un valore iniziale maggiore di zero e minore di quello finale")
          case   not(EMPTY(.w_VALMIN) OR ( .w_VALMAX >= 0 AND .w_VALMIN<=.w_VALMAX ))  and not(.w_VALORE<>'C')  and (.w_VALORE='C')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVALMAX_2_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare un valore finale maggiore di zero e del valore iniziale")
          case   not(.w_VALMAGGIORE >= 0)  and not(.w_VALORE<>'X')  and (.w_VALORE='X')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVALMAGGIORE_2_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Specificare un valore maggiore di zero")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOATT = this.w_TIPOATT
    this.o_DATINI = this.w_DATINI
    this.o_OREINI = this.w_OREINI
    this.o_MININI = this.w_MININI
    this.o_DATFIN = this.w_DATFIN
    this.o_CODNOM = this.w_CODNOM
    this.o_CODNOM1 = this.w_CODNOM1
    this.o_CODNOM2 = this.w_CODNOM2
    this.o_CodRis = this.w_CodRis
    this.o_CodRis1 = this.w_CodRis1
    this.o_VALORE = this.w_VALORE
    return

enddefine

* --- Define pages as container
define class tgsag_kcmPag1 as StdContainer
  Width  = 652
  height = 569
  stdWidth  = 652
  stdheight = 569
  resizeXpos=412
  resizeYpos=497
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPOATT_1_3 as StdField with uid="XXWINSOSQV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TIPOATT", cQueryName = "TIPOATT",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il tipo da attribuire alle attivit� da caricare",;
    HelpContextID = 226875594,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=85, Top=7, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="CAUMATTI", cZoomOnZoom="GSAG_MCA", oKey_1_1="CACODICE", oKey_1_2="this.w_TIPOATT"

  func oTIPOATT_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPOATT_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOATT_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAUMATTI','*','CACODICE',cp_AbsName(this.parent,'oTIPOATT_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAG_MCA',"Tipi attivit�",'NOT_OBSO.CAUMATTI_VZM',this.parent.oContained
  endproc
  proc oTIPOATT_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAG_MCA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_TIPOATT
     i_obj.ecpSave()
  endproc

  add object oOGGE_ATT_1_5 as StdField with uid="WVVDOTKMVZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_OGGE_ATT", cQueryName = "OGGE_ATT",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto da attribuire alle attivit� da caricare",;
    HelpContextID = 246442694,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=85, Top=31, InputMask=replicate('X',254)

  add object oDATINI_1_10 as StdField with uid="VAJTUNAFFS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio attivit�",;
    HelpContextID = 129737162,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=134, Top=55

  func oDATINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_PACHKFES='N' OR Chkfestivi(.w_DATINI,.w_PACHKFES))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oOREINI_1_11 as StdField with uid="WOKDXXPUIP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_OREINI", cQueryName = "OREINI",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora inizio attivit�",;
    HelpContextID = 129794074,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=289, Top=55, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREINI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_1_12 as StdField with uid="QMRKNBACQP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "MININI",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti inizio attivit�",;
    HelpContextID = 129759546,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=326, Top=55, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_13 as StdField with uid="VRHGJGBHQK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine attivit�",;
    HelpContextID = 51290570,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=134, Top=79

  func oDATFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(.w_PACHKFES='N' OR Chkfestivi(.w_DATINI,.w_PACHKFES))
         bRes=(cp_WarningMsg(thisform.msgFmt("")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oOREFIN_1_14 as StdField with uid="VKJCJSQZFK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_OREFIN", cQueryName = "OREFIN",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora fine attivit�",;
    HelpContextID = 51347482,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=289, Top=79, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oOREFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_OREFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_1_15 as StdField with uid="GIWPXXGMLC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "MINFIN",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti fine attivit�",;
    HelpContextID = 51312954,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=326, Top=79, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc

  add object oGIOINI_1_20 as StdField with uid="FDLKJPVEHW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_GIOINI", cQueryName = "GIOINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 129755546,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=85, Top=55, InputMask=replicate('X',10)

  add object oGIOFIN_1_21 as StdField with uid="GWEHUCPCHB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_GIOFIN", cQueryName = "GIOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 51308954,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=85, Top=79, InputMask=replicate('X',10)


  add object oSTATO_ATT_1_27 as StdCombo with uid="VPBFCMIITW",rtseq=18,rtrep=.f.,left=465,top=7,width=115,height=22;
    , ToolTipText = "Stato da attribuire alle attivit� da caricare";
    , HelpContextID = 27375686;
    , cFormVar="w_STATO_ATT",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa,"+"Completata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_ATT_1_27.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oSTATO_ATT_1_27.GetRadio()
    this.Parent.oContained.w_STATO_ATT = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_ATT_1_27.SetRadio()
    this.Parent.oContained.w_STATO_ATT=trim(this.Parent.oContained.w_STATO_ATT)
    this.value = ;
      iif(this.Parent.oContained.w_STATO_ATT=='T',1,;
      iif(this.Parent.oContained.w_STATO_ATT=='D',2,;
      iif(this.Parent.oContained.w_STATO_ATT=='I',3,;
      iif(this.Parent.oContained.w_STATO_ATT=='F',4,;
      iif(this.Parent.oContained.w_STATO_ATT=='P',5,;
      0)))))
  endfunc

  func oSTATO_ATT_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP<>'S')
    endwith
   endif
  endfunc

  func oSTATO_ATT_1_27.mHide()
    with this.Parent.oContained
      return (.w_FLNSAP='S')
    endwith
  endfunc


  add object oSTATO_ATT_1_28 as StdCombo with uid="YQBXQUQHJD",rtseq=19,rtrep=.f.,left=465,top=7,width=115,height=22;
    , ToolTipText = "Stato da attribuire alle attivit� da caricare";
    , HelpContextID = 27375686;
    , cFormVar="w_STATO_ATT",RowSource=""+"Provvisoria,"+"Da svolgere,"+"In corso,"+"Evasa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATO_ATT_1_28.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'F',;
    space(1))))))
  endfunc
  func oSTATO_ATT_1_28.GetRadio()
    this.Parent.oContained.w_STATO_ATT = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_ATT_1_28.SetRadio()
    this.Parent.oContained.w_STATO_ATT=trim(this.Parent.oContained.w_STATO_ATT)
    this.value = ;
      iif(this.Parent.oContained.w_STATO_ATT=='T',1,;
      iif(this.Parent.oContained.w_STATO_ATT=='D',2,;
      iif(this.Parent.oContained.w_STATO_ATT=='I',3,;
      iif(this.Parent.oContained.w_STATO_ATT=='F',4,;
      0))))
  endfunc

  func oSTATO_ATT_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNSAP='S')
    endwith
   endif
  endfunc

  func oSTATO_ATT_1_28.mHide()
    with this.Parent.oContained
      return (.w_FLNSAP<>'S')
    endwith
  endfunc

  add object oNOTE_ATT_1_31 as StdMemo with uid="OCPYIQBPTF",rtseq=21,rtrep=.f.,;
    cFormVar = "w_NOTE_ATT", cQueryName = "NOTE_ATT",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note",;
    HelpContextID = 246387414,;
   bGlobalFont=.t.,;
    Height=54, Width=496, Left=85, Top=104

  add object oCODNOMIN_1_32 as StdField with uid="LDORSNJAGM",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODNOMIN", cQueryName = "CODNOMIN",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo da attribuire alle attivit� da caricare",;
    HelpContextID = 207121524,;
   bGlobalFont=.t.,;
    Height=21, Width=147, Left=85, Top=161, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOMIN"

  func oCODNOMIN_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CACHKNOM))
    endwith
   endif
  endfunc

  func oCODNOMIN_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  func oCODNOMIN_1_32.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_CACHKNOM='P'
    endwith
    return i_bres
  endfunc

  proc oCODNOMIN_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOMIN_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOMIN_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Soggetti esterni",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOMIN_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOMIN
     i_obj.ecpSave()
  endproc

  add object oDESCAN_1_35 as AH_SEARCHFLD with uid="CBKKYKLBHC",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Nome pratica o parte di esso (ricerca contestuale)",;
    HelpContextID = 59878858,;
   bGlobalFont=.t.,;
    Height=21, Width=523, Left=57, Top=228, InputMask=replicate('X',100)

  add object oDESOGG_1_36 as AH_SEARCHFLD with uid="VKDZYNTERH",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESOGG", cQueryName = "DESOGG",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione oggetto o parte di essa",;
    HelpContextID = 170241482,;
   bGlobalFont=.t.,;
    Height=21, Width=432, Left=57, Top=257, InputMask=replicate('X',60), bHasZoom = .t. 

  proc oDESOGG_1_36.mZoom
      with this.Parent.oContained
        GSPR_BZA(this.Parent.oContained,"5")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPRAAPE_1_37 as AH_SEARCHFLD with uid="LEFUSAGYNV",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PRAAPE", cQueryName = "PRAAPE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice o numero della pratica o parte di esso (ricerca contestuale)",;
    HelpContextID = 195346442,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=57, Top=285, InputMask=replicate('X',15)

  add object oRIFGIU_1_38 as AH_SEARCHFLD with uid="UYETPYXEJI",rtseq=26,rtrep=.f.,;
    cFormVar = "w_RIFGIU", cQueryName = "RIFGIU",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Numero di ruolo generale attribuito dall'autorit� competente o parte di esso",;
    HelpContextID = 202275050,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=266, Top=285, InputMask=replicate('X',30)

  add object oFLAPE_1_39 as StdRadio with uid="MLIGHJJTAM",rtseq=27,rtrep=.f.,left=494, top=262, width=61,height=50;
    , ToolTipText = "Selezione su apertura/chiusura";
    , cFormVar="w_FLAPE", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oFLAPE_1_39.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Aperte"
      this.Buttons(1).HelpContextID = 247085398
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Chiuse"
      this.Buttons(2).HelpContextID = 247085398
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 247085398
      this.Buttons(3).Top=32
      this.SetAll("Width",59)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Selezione su apertura/chiusura")
      StdRadio::init()
    endproc

  func oFLAPE_1_39.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLAPE_1_39.GetRadio()
    this.Parent.oContained.w_FLAPE = this.RadioValue()
    return .t.
  endfunc

  func oFLAPE_1_39.SetRadio()
    this.Parent.oContained.w_FLAPE=trim(this.Parent.oContained.w_FLAPE)
    this.value = ;
      iif(this.Parent.oContained.w_FLAPE=='A',1,;
      iif(this.Parent.oContained.w_FLAPE=='C',2,;
      iif(this.Parent.oContained.w_FLAPE=='T',3,;
      0)))
  endfunc


  add object oFLARCH_1_40 as StdCombo with uid="ACDUZJFCZT",rtseq=28,rtrep=.f.,left=552,top=275,width=94,height=21;
    , ToolTipText = "Selezione su archiviazione";
    , HelpContextID = 157533866;
    , cFormVar="w_FLARCH",RowSource=""+"Archiviate,"+"Non archiviate,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLARCH_1_40.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLARCH_1_40.GetRadio()
    this.Parent.oContained.w_FLARCH = this.RadioValue()
    return .t.
  endfunc

  func oFLARCH_1_40.SetRadio()
    this.Parent.oContained.w_FLARCH=trim(this.Parent.oContained.w_FLARCH)
    this.value = ;
      iif(this.Parent.oContained.w_FLARCH=='A',1,;
      iif(this.Parent.oContained.w_FLARCH=='N',2,;
      iif(this.Parent.oContained.w_FLARCH=='T',3,;
      0)))
  endfunc

  func oFLARCH_1_40.mHide()
    with this.Parent.oContained
      return (.w_FLAPE<>'C')
    endwith
  endfunc


  add object oBtn_1_41 as StdButton with uid="PHEWMSSLEL",left=598, top=224, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca pratiche";
    , HelpContextID = 190744810;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AGKCM_SZOOM as cp_szoombox with uid="NEVIFHDGHN",left=45, top=312, width=605,height=207,;
    caption='AGKCM_SZOOM',;
   bGlobalFont=.t.,;
    cTable="CAN_TIER",cZoomFile="GSAG_KCM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Ricerca,w_DESCAN Searching,w_PRAAPE Searching",;
    nPag=1;
    , HelpContextID = 30214064

  add object oDESNOMIN_1_51 as StdField with uid="YBKTMCLSXL",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESNOMIN", cQueryName = "DESNOMIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 207180420,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=235, Top=161, InputMask=replicate('X',40)


  add object oBtn_1_56 as StdButton with uid="ERHOXVEEGI",left=544, top=524, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per generare le attivit�";
    , HelpContextID = 169232614;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_56.Click()
      with this.Parent.oContained
        do GSAG_BCM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_56.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_TIPOATT))
      endwith
    endif
  endfunc


  add object oBtn_1_57 as StdButton with uid="TKCMXZXILU",left=598, top=524, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176521286;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_57.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="QVTFQRDCTZ",Visible=.t., Left=13, Top=8,;
    Alignment=1, Width=67, Height=18,;
    Caption="Tipo attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="JWKHPZTOVT",Visible=.t., Left=34, Top=31,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="CWHSMLEESK",Visible=.t., Left=49, Top=55,;
    Alignment=1, Width=31, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="SANFLJMTRS",Visible=.t., Left=53, Top=78,;
    Alignment=1, Width=27, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="EWZSPAOTVW",Visible=.t., Left=225, Top=55,;
    Alignment=1, Width=59, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="CTAHWBKCXZ",Visible=.t., Left=225, Top=79,;
    Alignment=1, Width=59, Height=18,;
    Caption="Alle ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="DAAABZBARA",Visible=.t., Left=321, Top=55,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="BTYOAEACQT",Visible=.t., Left=321, Top=78,;
    Alignment=1, Width=4, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="EQMBEZKWZU",Visible=.t., Left=425, Top=8,;
    Alignment=1, Width=31, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="NQCAFRSLVY",Visible=.t., Left=51, Top=103,;
    Alignment=1, Width=29, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="USTLTLLNCM",Visible=.t., Left=12, Top=199,;
    Alignment=0, Width=101, Height=18,;
    Caption="Selezione pratiche"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="AUXASRUDLA",Visible=.t., Left=7, Top=228,;
    Alignment=1, Width=44, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="DZLNPVNPKJ",Visible=.t., Left=5, Top=257,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="FORHCITHWP",Visible=.t., Left=208, Top=285,;
    Alignment=1, Width=52, Height=18,;
    Caption="N.ruolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="PXYZUVRXPL",Visible=.t., Left=9, Top=286,;
    Alignment=1, Width=42, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="RCYDFBJYOO",Visible=.t., Left=9, Top=163,;
    Alignment=1, Width=71, Height=18,;
    Caption="Nominativo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_33 as StdBox with uid="ZQVGTIYJYE",left=8, top=215, width=640,height=1

  add object oBox_1_45 as StdBox with uid="LOIZABPOHX",left=54, top=225, width=530,height=28

  add object oBox_1_48 as StdBox with uid="FLCSSPYUYM",left=54, top=282, width=125,height=28
enddefine
define class tgsag_kcmPag2 as StdContainer
  Width  = 652
  height = 569
  stdWidth  = 652
  stdheight = 569
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDESSTA_2_3 as AH_SEARCHFLD with uid="GGUMADAQJN",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESSTA", cQueryName = "DESSTA",;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione stato o parte di essa",;
    HelpContextID = 257011146,;
   bGlobalFont=.t.,;
    Height=21, Width=370, Left=105, Top=25, InputMask=replicate('X',60), bHasZoom = .t. 

  proc oDESSTA_2_3.mZoom
      with this.Parent.oContained
        GSPR_BZA(this.Parent.oContained,"6")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCODOGG_2_4 as StdField with uid="CDYUTILVFY",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CODOGG", cQueryName = "CODOGG",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice oggetto (codice della domanda per l'iscrizione della causa a ruolo)",;
    HelpContextID = 170300378,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=105, Top=52, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PRA_OGGE", cZoomOnZoom="GSPR_AOP", oKey_1_1="OPCODICE", oKey_1_2="this.w_CODOGG"

  func oCODOGG_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODOGG_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODOGG_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PRA_OGGE','*','OPCODICE',cp_AbsName(this.parent,'oCODOGG_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPR_AOP',"Oggetti pratica",'',this.parent.oContained
  endproc
  proc oCODOGG_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSPR_AOP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OPCODICE=this.parent.oContained.w_CODOGG
     i_obj.ecpSave()
  endproc

  add object oRIFERI_2_5 as AH_SEARCHFLD with uid="DDZBKPQAYM",rtseq=38,rtrep=.f.,;
    cFormVar = "w_RIFERI", cQueryName = "RIFERI",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento della pratica attribuito dal cliente o parte di esso",;
    HelpContextID = 125860074,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=105, Top=79, InputMask=replicate('X',30)

  add object oPRACHI_2_6 as AH_SEARCHFLD with uid="YEGZEZOCOX",rtseq=39,rtrep=.f.,;
    cFormVar = "w_PRACHI", cQueryName = "PRACHI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice o numero di archiviazione o parte di esso",;
    HelpContextID = 136495114,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=532, Top=79, InputMask=replicate('X',15)

  add object oNOTE_2_7 as AH_SEARCHMEMO with uid="AXNLDTQQRX",rtseq=40,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Annotazioni o parte di esse",;
    HelpContextID = 174091478,;
   bGlobalFont=.t.,;
    Height=21, Width=545, Left=105, Top=107

  add object oCODNOM_2_8 as StdField with uid="CBKJUSKXHD",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CODNOM", cQueryName = "CODNOM",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice soggetti esterno",;
    HelpContextID = 61314010,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=105, Top=135, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM"

  func oCODNOM_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Elenco nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM
     i_obj.ecpSave()
  endproc


  add object oCODCAT_2_9 as StdTableCombo with uid="AXUHUESMBU",rtseq=42,rtrep=.f.,left=517,top=136,width=134,height=21;
    , ToolTipText = "Ruolo del nominativo (soggetti esterni)";
    , HelpContextID = 227709914;
    , cFormVar="w_CODCAT",tablefilter="", bObbl = .f. , nPag = 2;
    , sErrorMsg = "Codice categoria soggetti inesistente o di tipo cliente";
    , cLinkFile="CAT_SOGG";
    , cTable='CAT_SOGG',cKey='CSCODCAT',cValue='CSDESCAT',cOrderBy='CSDESCAT',xDefault=space(10);
  , bGlobalFont=.t.


  func oCODCAT_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODNOM))
    endwith
   endif
  endfunc

  func oCODCAT_2_9.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODNOM))
    endwith
  endfunc

  func oCODCAT_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oCODNOM1_2_10 as StdField with uid="OYBHNZTGWH",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CODNOM1", cQueryName = "CODNOM1",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice altro soggetto esterno",;
    HelpContextID = 61314010,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=105, Top=163, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM1"

  func oCODNOM1_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM1_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM1_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM1_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Elenco nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM1_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM1
     i_obj.ecpSave()
  endproc


  add object oCODCAT1_2_11 as StdTableCombo with uid="MBKKIKQOGZ",rtseq=44,rtrep=.f.,left=517,top=164,width=134,height=21;
    , ToolTipText = "Ruolo del nominativo (soggetti esterni)";
    , HelpContextID = 227709914;
    , cFormVar="w_CODCAT1",tablefilter="", bObbl = .f. , nPag = 2;
    , sErrorMsg = "Codice categoria soggetti inesistente o di tipo cliente";
    , cLinkFile="CAT_SOGG";
    , cTable='CAT_SOGG',cKey='CSCODCAT',cValue='CSDESCAT',cOrderBy='CSDESCAT',xDefault=space(10);
  , bGlobalFont=.t.


  func oCODCAT1_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODNOM1))
    endwith
   endif
  endfunc

  func oCODCAT1_2_11.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODNOM1))
    endwith
  endfunc

  func oCODCAT1_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT1_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oCODNOM2_2_12 as StdField with uid="QCHJXNPPMC",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CODNOM2", cQueryName = "CODNOM2",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice altro soggetto esterno",;
    HelpContextID = 61314010,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=105, Top=191, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", cZoomOnZoom="GSAR_ANO", oKey_1_1="NOCODICE", oKey_1_2="this.w_CODNOM2"

  func oCODNOM2_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODNOM2_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODNOM2_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oCODNOM2_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANO',"Elenco nominativi",'GSOF3QNO.OFF_NOMI_VZM',this.parent.oContained
  endproc
  proc oCODNOM2_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NOCODICE=this.parent.oContained.w_CODNOM2
     i_obj.ecpSave()
  endproc


  add object oCODCAT2_2_13 as StdTableCombo with uid="VRKBCUMKIE",rtseq=46,rtrep=.f.,left=517,top=192,width=134,height=21;
    , ToolTipText = "Ruolo dell'altro nominativo (soggetti esterni)";
    , HelpContextID = 227709914;
    , cFormVar="w_CODCAT2",tablefilter="", bObbl = .f. , nPag = 2;
    , sErrorMsg = "Codice categoria soggetti inesistente o di tipo cliente";
    , cLinkFile="CAT_SOGG";
    , cTable='CAT_SOGG',cKey='CSCODCAT',cValue='CSDESCAT',cOrderBy='CSDESCAT',xDefault=space(10);
  , bGlobalFont=.t.


  func oCODCAT2_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODNOM2))
    endwith
   endif
  endfunc

  func oCODCAT2_2_13.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODNOM2))
    endwith
  endfunc

  func oCODCAT2_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT2_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oCodRis_2_14 as StdField with uid="DOBBSYFCFJ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CodRis", cQueryName = "CodRis",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice Soggetto interno",;
    HelpContextID = 201421786,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=105, Top=219, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_CodRis"

  func oCodRis_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodRis_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodRis_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCodRis_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse interne",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc


  add object oRuolRis_2_15 as StdTableCombo with uid="HKRTBWAVCA",rtseq=48,rtrep=.f.,left=517,top=219,width=134,height=21;
    , ToolTipText = "Ruolo risorsa interna";
    , HelpContextID = 123124970;
    , cFormVar="w_RuolRis",tablefilter="", bObbl = .f. , nPag = 2;
    , cLinkFile="RUO_SOGI";
    , cTable='RUO_SOGI',cKey='RSCODRUO',cValue='RSDESRUO',cOrderBy='RSDESRUO',xDefault=space(5);
  , bGlobalFont=.t.


  func oRuolRis_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CodRis))
    endwith
   endif
  endfunc

  func oRuolRis_2_15.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CodRis))
    endwith
  endfunc

  func oRuolRis_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oRuolRis_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oCodRis1_2_16 as StdField with uid="OEILHJLMQL",rtseq=49,rtrep=.f.,;
    cFormVar = "w_CodRis1", cQueryName = "CodRis1",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice altro soggetto interno",;
    HelpContextID = 201421786,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=105, Top=247, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_CodRis1"

  func oCodRis1_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodRis1_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodRis1_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCodRis1_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Risorse interne",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc


  add object oRuolRis1_2_17 as StdTableCombo with uid="WNELYXPUTB",rtseq=50,rtrep=.f.,left=517,top=247,width=134,height=21;
    , ToolTipText = "Ruolo risorsa interna";
    , HelpContextID = 123124921;
    , cFormVar="w_RuolRis1",tablefilter="", bObbl = .f. , nPag = 2;
    , cLinkFile="RUO_SOGI";
    , cTable='RUO_SOGI',cKey='RSCODRUO',cValue='RSDESRUO',cOrderBy='RSDESRUO',xDefault=space(5);
  , bGlobalFont=.t.


  func oRuolRis1_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_CodRis1))
    endwith
   endif
  endfunc

  func oRuolRis1_2_17.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CodRis1))
    endwith
  endfunc

  func oRuolRis1_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oRuolRis1_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oCodUte_2_18 as StdField with uid="PIUVUYSBHD",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CodUte", cQueryName = "CodUte",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente che ha eseguito l'ultima modifica",;
    HelpContextID = 156136410,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=105, Top=275, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_CodUte"

  func oCodUte_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCodUte_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCodUte_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oCodUte_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc


  add object oCODSED_2_19 as StdTableCombo with uid="BIGGOMHXKV",rtseq=52,rtrep=.f.,left=105,top=302,width=181,height=21;
    , ToolTipText = "Sede";
    , HelpContextID = 222467034;
    , cFormVar="w_CODSED",tablefilter="", bObbl = .f. , nPag = 2;
    , cLinkFile="PRA_SEDI";
    , cTable='PRA_SEDI',cKey='SPCODICE',cValue='SPDESCRI',cOrderBy='SPDESCRI',xDefault=space(5);
  , bGlobalFont=.t.


  func oCODSED_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSED_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oCODDIP_2_20 as StdTableCombo with uid="KFHPFIWVAQ",rtseq=53,rtrep=.f.,left=470,top=302,width=181,height=21;
    , ToolTipText = "Dipartimento";
    , HelpContextID = 17929178;
    , cFormVar="w_CODDIP",tablefilter="", bObbl = .f. , nPag = 2;
    , cTable='PRA_DIPA',cKey='DPCODICE',cValue='DPDESCRI',cOrderBy='DPDESCRI',xDefault=space(5);
  , bGlobalFont=.t.


  add object oDATINIAPE_2_24 as StdField with uid="ZYMWVZJHPD",rtseq=54,rtrep=.f.,;
    cFormVar = "w_DATINIAPE", cQueryName = "DATINIAPE",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data apertura iniziale � superiore alla finale",;
    ToolTipText = "Data apertura di inizio selezione",;
    HelpContextID = 129735978,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=105, Top=330

  func oDATINIAPE_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINIAPE<=.w_DATFINAPE OR EMPTY(.w_DATFINAPE))
    endwith
    return bRes
  endfunc

  add object oDATFINAPE_2_25 as StdField with uid="CWKYCMQTHA",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DATFINAPE", cQueryName = "DATFINAPE",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data apertura iniziale � superiore alla finale",;
    ToolTipText = "Data apertura di fine selezione",;
    HelpContextID = 51289386,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=303, Top=330

  func oDATFINAPE_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINIAPE<=.w_DATFINAPE OR EMPTY(.w_DATINIAPE))
    endwith
    return bRes
  endfunc

  add object oDATINICHI_2_26 as StdField with uid="YEPKGTKYRM",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DATINICHI", cQueryName = "DATINICHI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data chiusura iniziale � superiore alla finale",;
    ToolTipText = "Data chiusura di inizio selezione",;
    HelpContextID = 129735922,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=105, Top=358

  func oDATINICHI_2_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINICHI<=.w_DATFINCHI OR EMPTY(.w_DATFINCHI))
    endwith
    return bRes
  endfunc

  add object oDATFINCHI_2_27 as StdField with uid="OFIHFHTUSQ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DATFINCHI", cQueryName = "DATFINCHI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data chiusura iniziale � superiore alla finale",;
    ToolTipText = "Data chiusura di fine selezione",;
    HelpContextID = 51289330,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=303, Top=358

  func oDATFINCHI_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINICHI<=.w_DATFINCHI OR EMPTY(.w_DATINICHI))
    endwith
    return bRes
  endfunc


  add object oVALORE_2_28 as StdCombo with uid="IRLUKJCDJB",value=1,rtseq=58,rtrep=.f.,left=105,top=386,width=139,height=21;
    , ToolTipText = "Valore";
    , HelpContextID = 192290986;
    , cFormVar="w_VALORE",RowSource=""+"Tutti,"+"Uguale a,"+"Minore di,"+"Maggiore di,"+"Compreso tra,"+"Indeterminabile,"+"Partic. import. e ind.", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oVALORE_2_28.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'U',;
    iif(this.value =3,'X',;
    iif(this.value =4,'N',;
    iif(this.value =5,'C',;
    iif(this.value =6,'I',;
    iif(this.value =7,'P',;
    space(1)))))))))
  endfunc
  func oVALORE_2_28.GetRadio()
    this.Parent.oContained.w_VALORE = this.RadioValue()
    return .t.
  endfunc

  func oVALORE_2_28.SetRadio()
    this.Parent.oContained.w_VALORE=trim(this.Parent.oContained.w_VALORE)
    this.value = ;
      iif(this.Parent.oContained.w_VALORE=='',1,;
      iif(this.Parent.oContained.w_VALORE=='U',2,;
      iif(this.Parent.oContained.w_VALORE=='X',3,;
      iif(this.Parent.oContained.w_VALORE=='N',4,;
      iif(this.Parent.oContained.w_VALORE=='C',5,;
      iif(this.Parent.oContained.w_VALORE=='I',6,;
      iif(this.Parent.oContained.w_VALORE=='P',7,;
      0)))))))
  endfunc

  add object oVALUGUALE_2_29 as StdField with uid="WUBSRSNXJO",rtseq=59,rtrep=.f.,;
    cFormVar = "w_VALUGUALE", cQueryName = "VALUGUALE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore di uguaglianza",;
    HelpContextID = 203430926,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=290, Top=386, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oVALUGUALE_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORE='U')
    endwith
   endif
  endfunc

  func oVALUGUALE_2_29.mHide()
    with this.Parent.oContained
      return (.w_VALORE<>'U')
    endwith
  endfunc

  add object oVALMIN_2_31 as StdField with uid="VQBLYERKET",rtseq=60,rtrep=.f.,;
    cFormVar = "w_VALMIN", cQueryName = "VALMIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Specificare un valore iniziale maggiore di zero e minore di quello finale",;
    ToolTipText = "Valore da superare",;
    HelpContextID = 50864298,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=290, Top=386, cSayPict='"@Z "+v_PV(40+VVL)', cGetPict='"@Z "+v_GV(40+VVL)'

  func oVALMIN_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORE='N' OR .w_VALORE='C')
    endwith
   endif
  endfunc

  func oVALMIN_2_31.mHide()
    with this.Parent.oContained
      return (! .w_VALORE$'NC')
    endwith
  endfunc

  func oVALMIN_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VALMIN >= 0 AND ( EMPTY(.w_VALMAX) OR .w_VALMIN<=.w_VALMAX ))
    endwith
    return bRes
  endfunc

  add object oVALMAX_2_32 as StdField with uid="BETAZFVKHN",rtseq=61,rtrep=.f.,;
    cFormVar = "w_VALMAX", cQueryName = "VALMAX",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Specificare un valore finale maggiore di zero e del valore iniziale",;
    ToolTipText = "Valore da non superare",;
    HelpContextID = 159916202,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=457, Top=386, cSayPict='"@Z "+v_PV(40+VVL)', cGetPict='"@Z "+v_GV(40+VVL)'

  func oVALMAX_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORE='C')
    endwith
   endif
  endfunc

  func oVALMAX_2_32.mHide()
    with this.Parent.oContained
      return (.w_VALORE<>'C')
    endwith
  endfunc

  func oVALMAX_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_VALMIN) OR ( .w_VALMAX >= 0 AND .w_VALMIN<=.w_VALMAX ))
    endwith
    return bRes
  endfunc


  add object oIMPEGN_2_36 as StdCombo with uid="HPCXRCGRJY",value=1,rtseq=62,rtrep=.f.,left=105,top=414,width=139,height=21;
    , ToolTipText = "Impegno";
    , HelpContextID = 53466490;
    , cFormVar="w_IMPEGN",RowSource=""+"Tutti,"+"Molto facile,"+"Facile,"+"Medio,"+"Difficile,"+"Molto difficile", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oIMPEGN_2_36.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    iif(this.value =5,4,;
    iif(this.value =6,5,;
    0)))))))
  endfunc
  func oIMPEGN_2_36.GetRadio()
    this.Parent.oContained.w_IMPEGN = this.RadioValue()
    return .t.
  endfunc

  func oIMPEGN_2_36.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_IMPEGN==0,1,;
      iif(this.Parent.oContained.w_IMPEGN==1,2,;
      iif(this.Parent.oContained.w_IMPEGN==2,3,;
      iif(this.Parent.oContained.w_IMPEGN==3,4,;
      iif(this.Parent.oContained.w_IMPEGN==4,5,;
      iif(this.Parent.oContained.w_IMPEGN==5,6,;
      0))))))
  endfunc

  add object oVALMAGGIORE_2_39 as StdField with uid="VYKJLEOWSJ",rtseq=63,rtrep=.f.,;
    cFormVar = "w_VALMAGGIORE", cQueryName = "VALMAGGIORE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Specificare un valore maggiore di zero",;
    ToolTipText = "Valore da non superare",;
    HelpContextID = 176388465,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=290, Top=386, cSayPict='"@Z "+v_PV(40+VVL)', cGetPict='"@Z "+v_GV(40+VVL)'

  func oVALMAGGIORE_2_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORE='X')
    endwith
   endif
  endfunc

  func oVALMAGGIORE_2_39.mHide()
    with this.Parent.oContained
      return (.w_VALORE<>'X')
    endwith
  endfunc

  func oVALMAGGIORE_2_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VALMAGGIORE >= 0)
    endwith
    return bRes
  endfunc

  add object oDESNOM1_2_43 as StdField with uid="JVOVPBGUDU",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESNOM1", cQueryName = "DESNOM1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61255114,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=225, Top=163, InputMask=replicate('X',40)

  add object oDESNOM2_2_44 as StdField with uid="NZPYZMUBRY",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESNOM2", cQueryName = "DESNOM2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61255114,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=225, Top=191, InputMask=replicate('X',40)


  add object oBtn_2_47 as StdButton with uid="TMRCZGTRFE",left=598, top=18, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=2;
    , ToolTipText = "Ricerca pratiche";
    , HelpContextID = 190744810;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_47.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESNOM_2_51 as StdField with uid="LQSKBILBEO",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61255114,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=225, Top=135, InputMask=replicate('X',40)

  add object oDESRIS_2_53 as StdField with uid="WDOPCYWMCC",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESRIS", cQueryName = "DESRIS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 235056586,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=174, Top=219, InputMask=replicate('X',80)

  add object oDESUTE_2_55 as StdField with uid="JXGTZBFHCK",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 189771210,;
   bGlobalFont=.t.,;
    Height=21, Width=263, Left=163, Top=275, InputMask=replicate('X',80)

  add object oDESRIS1_2_58 as StdField with uid="GIYLPMDKPV",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DESRIS1", cQueryName = "DESRIS1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 235056586,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=174, Top=247, InputMask=replicate('X',80)

  add object oStr_2_21 as StdString with uid="EQKNSJJSWT",Visible=.t., Left=-14, Top=81,;
    Alignment=1, Width=113, Height=18,;
    Caption="Riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="ODRGHHELGZ",Visible=.t., Left=-14, Top=305,;
    Alignment=1, Width=113, Height=18,;
    Caption="Sede:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="VWXRDJPTZG",Visible=.t., Left=348, Top=304,;
    Alignment=1, Width=117, Height=18,;
    Caption="Dipartimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="APCFEBALIA",Visible=.t., Left=-14, Top=333,;
    Alignment=1, Width=113, Height=18,;
    Caption="Da data apertura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="TYPXZXAQVL",Visible=.t., Left=-14, Top=361,;
    Alignment=1, Width=113, Height=18,;
    Caption="Da data chiusura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="NHWEGUMFNK",Visible=.t., Left=187, Top=332,;
    Alignment=1, Width=108, Height=18,;
    Caption="A data apertura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="KSDVYIVYOY",Visible=.t., Left=187, Top=360,;
    Alignment=1, Width=108, Height=18,;
    Caption="A data chiusura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="QPFKBGBOPP",Visible=.t., Left=-14, Top=417,;
    Alignment=1, Width=113, Height=18,;
    Caption="Impegno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="MJHDWIJNDV",Visible=.t., Left=-14, Top=389,;
    Alignment=1, Width=113, Height=18,;
    Caption="Valore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="PGLYDLWGZU",Visible=.t., Left=365, Top=81,;
    Alignment=1, Width=163, Height=18,;
    Caption="Codice di archiviazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="NTEASDKVOM",Visible=.t., Left=41, Top=52,;
    Alignment=1, Width=58, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="PMBUJPPQBN",Visible=.t., Left=-14, Top=109,;
    Alignment=1, Width=113, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="BKMOKHBYCE",Visible=.t., Left=-14, Top=165,;
    Alignment=1, Width=113, Height=18,;
    Caption="Altro soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="CEGYTBHVDT",Visible=.t., Left=-14, Top=137,;
    Alignment=1, Width=113, Height=18,;
    Caption="Soggetto esterno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="JCJQAHOUWU",Visible=.t., Left=-14, Top=193,;
    Alignment=1, Width=113, Height=18,;
    Caption="Altro soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_49 as StdString with uid="IGOMELRWKZ",Visible=.t., Left=460, Top=165,;
    Alignment=1, Width=52, Height=18,;
    Caption="Ruolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_49.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODNOM1))
    endwith
  endfunc

  add object oStr_2_50 as StdString with uid="RWBDVKUEMR",Visible=.t., Left=460, Top=193,;
    Alignment=1, Width=52, Height=18,;
    Caption="Ruolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODNOM2))
    endwith
  endfunc

  add object oStr_2_52 as StdString with uid="GOADFKEWFS",Visible=.t., Left=460, Top=137,;
    Alignment=1, Width=52, Height=18,;
    Caption="Ruolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_52.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODNOM))
    endwith
  endfunc

  add object oStr_2_54 as StdString with uid="HRAEFMYVHV",Visible=.t., Left=-14, Top=221,;
    Alignment=1, Width=113, Height=18,;
    Caption="Soggetto interno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_56 as StdString with uid="EIENBMKRMG",Visible=.t., Left=-14, Top=277,;
    Alignment=1, Width=113, Height=18,;
    Caption="Modificato da:"  ;
  , bGlobalFont=.t.

  add object oStr_2_57 as StdString with uid="TGWAUCUBXZ",Visible=.t., Left=460, Top=221,;
    Alignment=1, Width=52, Height=18,;
    Caption="Ruolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_57.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CodRis))
    endwith
  endfunc

  add object oStr_2_59 as StdString with uid="JLVSNKQSVK",Visible=.t., Left=-14, Top=249,;
    Alignment=1, Width=113, Height=18,;
    Caption="Altro soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_60 as StdString with uid="MWACYMDSMZ",Visible=.t., Left=460, Top=249,;
    Alignment=1, Width=52, Height=18,;
    Caption="Ruolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_60.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CodRis1))
    endwith
  endfunc

  add object oStr_2_61 as StdString with uid="TPTXDIYFOG",Visible=.t., Left=-14, Top=25,;
    Alignment=1, Width=113, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kcm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
