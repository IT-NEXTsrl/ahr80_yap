* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bi1                                                        *
*              Importa componenti                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-20                                                      *
* Last revis.: 2012-11-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bi1",oParentObject)
return(i_retval)

define class tgsag_bi1 as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Ricerca da Import componenti (da GSAG_KIM)
    this.w_PADRE = This.oParentObject
    CREATE CURSOR GRUPPI ( IMCODICE C(10), NUMRIG N(4), ROWORD N(5), IMDESCON C(50), IMMODATR C(20), IMLEGAME N(4))
    wrcursor("GRUPPI")
    * --- Lancia la Query
    ah_Msg("Ricerca documenti da importare...")
    this.w_PADRE.NotifyEvent("Calcola")     
    WAIT CLEAR
     
 SELECT ( this.oParentObject.w_ZoomMast.cCursor ) 
 GO TOP
    this.w_PADRE.NotifyEvent("CalcRig")     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
