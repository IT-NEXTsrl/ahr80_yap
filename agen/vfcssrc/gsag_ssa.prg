* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_ssa                                                        *
*              Stampa singola attivit�                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-09-09                                                      *
* Last revis.: 2010-05-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_ssa",oParentObject))

* --- Class definition
define class tgsag_ssa as StdForm
  Top    = 4
  Left   = 147

  * --- Standard Properties
  Width  = 508
  Height = 234
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-05-28"
  HelpContextID=90842985
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  cPrg = "gsag_ssa"
  cComment = "Stampa singola attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TipoRisorsa = space(1)
  w_CODPERS = space(5)
  o_CODPERS = space(5)
  w_DESRIS = space(80)
  w_STAPREST = space(1)
  w_STARISOR = space(1)
  w_STAIMPIAN = space(1)
  o_STAIMPIAN = space(1)
  w_STAQTA = space(1)
  w_STAPREZZO = space(1)
  w_STAPREZZO = space(1)
  w_STAUM = space(1)
  w_STADUREFF = space(1)
  w_STAIMP = space(1)
  w_STANOTE = space(1)
  w_STAMPATTRIB = space(1)
  w_STADESAGG = space(1)
  w_STAPRAT = space(1)
  w_STAPRAT = space(1)
  w_SERIAL = space(20)
  w_COGN = space(40)
  w_NOME = space(40)
  w_ONUME = 0
  w_ONUM = 0
  w_OB_TEST = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_ssaPag1","gsag_ssa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODPERS_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DIPENDEN'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TipoRisorsa=space(1)
      .w_CODPERS=space(5)
      .w_DESRIS=space(80)
      .w_STAPREST=space(1)
      .w_STARISOR=space(1)
      .w_STAIMPIAN=space(1)
      .w_STAQTA=space(1)
      .w_STAPREZZO=space(1)
      .w_STAPREZZO=space(1)
      .w_STAUM=space(1)
      .w_STADUREFF=space(1)
      .w_STAIMP=space(1)
      .w_STANOTE=space(1)
      .w_STAMPATTRIB=space(1)
      .w_STADESAGG=space(1)
      .w_STAPRAT=space(1)
      .w_STAPRAT=space(1)
      .w_SERIAL=space(20)
      .w_COGN=space(40)
      .w_NOME=space(40)
      .w_ONUME=0
      .w_ONUM=0
      .w_OB_TEST=ctod("  /  /  ")
      .w_SERIAL=oParentObject.w_SERIAL
        .w_TipoRisorsa = 'P'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODPERS))
          .link_1_2('Full')
        endif
        .w_DESRIS = ALLTRIM(.w_COGN)+' '+ALLTRIM(.w_NOME)
        .w_STAPREST = 'S'
        .w_STARISOR = 'S'
        .w_STAIMPIAN = IIF(ISALT(), 'N', 'S')
        .w_STAQTA = 'N'
        .w_STAPREZZO = 'N'
        .w_STAPREZZO = 'N'
        .w_STAUM = 'N'
        .w_STADUREFF = 'N'
        .w_STAIMP = 'N'
        .w_STANOTE = 'S'
        .w_STAMPATTRIB = IIF(ISALT(), 'N', 'S')
        .w_STADESAGG = 'N'
        .w_STAPRAT = 'S'
        .w_STAPRAT = 'S'
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
          .DoRTCalc(18,22,.f.)
        .w_OB_TEST = i_INIDAT
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SERIAL=.w_SERIAL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CODPERS<>.w_CODPERS
            .w_DESRIS = ALLTRIM(.w_COGN)+' '+ALLTRIM(.w_NOME)
        endif
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        if .o_STAIMPIAN<>.w_STAIMPIAN
          .Calculate_TCCQSGFTHF()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
    endwith
  return

  proc Calculate_TCCQSGFTHF()
    with this
          * --- Disattiva Dettaglio attributi
          .w_STAMPATTRIB = IIF(.w_STAIMPIAN='N', 'N', .w_STAMPATTRIB)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSTAMPATTRIB_1_14.enabled = this.oPgFrm.Page1.oPag.oSTAMPATTRIB_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODPERS_1_2.visible=!this.oPgFrm.Page1.oPag.oCODPERS_1_2.mHide()
    this.oPgFrm.Page1.oPag.oDESRIS_1_3.visible=!this.oPgFrm.Page1.oPag.oDESRIS_1_3.mHide()
    this.oPgFrm.Page1.oPag.oSTAIMPIAN_1_6.visible=!this.oPgFrm.Page1.oPag.oSTAIMPIAN_1_6.mHide()
    this.oPgFrm.Page1.oPag.oSTAQTA_1_7.visible=!this.oPgFrm.Page1.oPag.oSTAQTA_1_7.mHide()
    this.oPgFrm.Page1.oPag.oSTAPREZZO_1_8.visible=!this.oPgFrm.Page1.oPag.oSTAPREZZO_1_8.mHide()
    this.oPgFrm.Page1.oPag.oSTAPREZZO_1_9.visible=!this.oPgFrm.Page1.oPag.oSTAPREZZO_1_9.mHide()
    this.oPgFrm.Page1.oPag.oSTAUM_1_10.visible=!this.oPgFrm.Page1.oPag.oSTAUM_1_10.mHide()
    this.oPgFrm.Page1.oPag.oSTADUREFF_1_11.visible=!this.oPgFrm.Page1.oPag.oSTADUREFF_1_11.mHide()
    this.oPgFrm.Page1.oPag.oSTAIMP_1_12.visible=!this.oPgFrm.Page1.oPag.oSTAIMP_1_12.mHide()
    this.oPgFrm.Page1.oPag.oSTAMPATTRIB_1_14.visible=!this.oPgFrm.Page1.oPag.oSTAMPATTRIB_1_14.mHide()
    this.oPgFrm.Page1.oPag.oSTADESAGG_1_15.visible=!this.oPgFrm.Page1.oPag.oSTADESAGG_1_15.mHide()
    this.oPgFrm.Page1.oPag.oSTAPRAT_1_16.visible=!this.oPgFrm.Page1.oPag.oSTAPRAT_1_16.mHide()
    this.oPgFrm.Page1.oPag.oSTAPRAT_1_17.visible=!this.oPgFrm.Page1.oPag.oSTAPRAT_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODPERS
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPERS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_CODPERS))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPERS)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_CODPERS)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPERS) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oCODPERS_1_2'),i_cWhere,'',"Persone",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPERS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODPERS);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_CODPERS)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPERS = NVL(_Link_.DPCODICE,space(5))
      this.w_COGN = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODPERS = space(5)
      endif
      this.w_COGN = space(40)
      this.w_NOME = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPERS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODPERS_1_2.value==this.w_CODPERS)
      this.oPgFrm.Page1.oPag.oCODPERS_1_2.value=this.w_CODPERS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIS_1_3.value==this.w_DESRIS)
      this.oPgFrm.Page1.oPag.oDESRIS_1_3.value=this.w_DESRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAPREST_1_4.RadioValue()==this.w_STAPREST)
      this.oPgFrm.Page1.oPag.oSTAPREST_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTARISOR_1_5.RadioValue()==this.w_STARISOR)
      this.oPgFrm.Page1.oPag.oSTARISOR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAIMPIAN_1_6.RadioValue()==this.w_STAIMPIAN)
      this.oPgFrm.Page1.oPag.oSTAIMPIAN_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAQTA_1_7.RadioValue()==this.w_STAQTA)
      this.oPgFrm.Page1.oPag.oSTAQTA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAPREZZO_1_8.RadioValue()==this.w_STAPREZZO)
      this.oPgFrm.Page1.oPag.oSTAPREZZO_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAPREZZO_1_9.RadioValue()==this.w_STAPREZZO)
      this.oPgFrm.Page1.oPag.oSTAPREZZO_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAUM_1_10.RadioValue()==this.w_STAUM)
      this.oPgFrm.Page1.oPag.oSTAUM_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTADUREFF_1_11.RadioValue()==this.w_STADUREFF)
      this.oPgFrm.Page1.oPag.oSTADUREFF_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAIMP_1_12.RadioValue()==this.w_STAIMP)
      this.oPgFrm.Page1.oPag.oSTAIMP_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTANOTE_1_13.RadioValue()==this.w_STANOTE)
      this.oPgFrm.Page1.oPag.oSTANOTE_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPATTRIB_1_14.RadioValue()==this.w_STAMPATTRIB)
      this.oPgFrm.Page1.oPag.oSTAMPATTRIB_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTADESAGG_1_15.RadioValue()==this.w_STADESAGG)
      this.oPgFrm.Page1.oPag.oSTADESAGG_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAPRAT_1_16.RadioValue()==this.w_STAPRAT)
      this.oPgFrm.Page1.oPag.oSTAPRAT_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAPRAT_1_17.RadioValue()==this.w_STAPRAT)
      this.oPgFrm.Page1.oPag.oSTAPRAT_1_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODPERS = this.w_CODPERS
    this.o_STAIMPIAN = this.w_STAIMPIAN
    return

enddefine

* --- Define pages as container
define class tgsag_ssaPag1 as StdContainer
  Width  = 504
  height = 234
  stdWidth  = 504
  stdheight = 234
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODPERS_1_2 as StdField with uid="DOBBSYFCFJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODPERS", cQueryName = "CODPERS",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona",;
    HelpContextID = 20605990,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=77, Top=24, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_CODPERS"

  func oCODPERS_1_2.mHide()
    with this.Parent.oContained
      return ((NOT ISAHE() AND .w_ONUME<>1) OR (ISAHE() AND .w_ONUM<>1))
    endwith
  endfunc

  func oCODPERS_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPERS_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPERS_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oCODPERS_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Persone",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oDESRIS_1_3 as StdField with uid="WDOPCYWMCC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESRIS", cQueryName = "DESRIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 226667978,;
   bGlobalFont=.t.,;
    Height=21, Width=345, Left=146, Top=24, InputMask=replicate('X',80)

  func oDESRIS_1_3.mHide()
    with this.Parent.oContained
      return ((NOT ISAHE() AND .w_ONUME<>1) OR (ISAHE() AND .w_ONUM<>1))
    endwith
  endfunc

  add object oSTAPREST_1_4 as StdCheck with uid="FGAGECIDAQ",rtseq=4,rtrep=.f.,left=33, top=61, caption="Dettaglio attivit�",;
    ToolTipText = "Se attivo, vengono stampate le prestazioni",;
    HelpContextID = 84558458,;
    cFormVar="w_STAPREST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAPREST_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAPREST_1_4.GetRadio()
    this.Parent.oContained.w_STAPREST = this.RadioValue()
    return .t.
  endfunc

  func oSTAPREST_1_4.SetRadio()
    this.Parent.oContained.w_STAPREST=trim(this.Parent.oContained.w_STAPREST)
    this.value = ;
      iif(this.Parent.oContained.w_STAPREST=='S',1,;
      0)
  endfunc

  add object oSTARISOR_1_5 as StdCheck with uid="GUJZOAOBRM",rtseq=5,rtrep=.f.,left=33, top=87, caption="Dettaglio risorse",;
    ToolTipText = "Se attivo, vengono stampate le risorse",;
    HelpContextID = 226737544,;
    cFormVar="w_STARISOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTARISOR_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTARISOR_1_5.GetRadio()
    this.Parent.oContained.w_STARISOR = this.RadioValue()
    return .t.
  endfunc

  func oSTARISOR_1_5.SetRadio()
    this.Parent.oContained.w_STARISOR=trim(this.Parent.oContained.w_STARISOR)
    this.value = ;
      iif(this.Parent.oContained.w_STARISOR=='S',1,;
      0)
  endfunc

  add object oSTAIMPIAN_1_6 as StdCheck with uid="JQFDKCKPIN",rtseq=6,rtrep=.f.,left=33, top=113, caption="Dettaglio impianti/comp.",;
    ToolTipText = "Se attivo, vengono stampati gli impianti/componenti",;
    HelpContextID = 263407431,;
    cFormVar="w_STAIMPIAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAIMPIAN_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAIMPIAN_1_6.GetRadio()
    this.Parent.oContained.w_STAIMPIAN = this.RadioValue()
    return .t.
  endfunc

  func oSTAIMPIAN_1_6.SetRadio()
    this.Parent.oContained.w_STAIMPIAN=trim(this.Parent.oContained.w_STAIMPIAN)
    this.value = ;
      iif(this.Parent.oContained.w_STAIMPIAN=='S',1,;
      0)
  endfunc

  func oSTAIMPIAN_1_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oSTAQTA_1_7 as StdCheck with uid="CLPOJOVQGG",rtseq=7,rtrep=.f.,left=200, top=61, caption="Quantit�",;
    ToolTipText = "Se attivo, stampa la quantit� delle prestazioni",;
    HelpContextID = 248823258,;
    cFormVar="w_STAQTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAQTA_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAQTA_1_7.GetRadio()
    this.Parent.oContained.w_STAQTA = this.RadioValue()
    return .t.
  endfunc

  func oSTAQTA_1_7.SetRadio()
    this.Parent.oContained.w_STAQTA=trim(this.Parent.oContained.w_STAQTA)
    this.value = ;
      iif(this.Parent.oContained.w_STAQTA=='S',1,;
      0)
  endfunc

  func oSTAQTA_1_7.mHide()
    with this.Parent.oContained
      return (.w_STAPREST='N')
    endwith
  endfunc

  add object oSTAPREZZO_1_8 as StdCheck with uid="FOYTNYQTUN",rtseq=8,rtrep=.f.,left=323, top=61, caption="Tariffa",;
    ToolTipText = "Se attivo, stampa la tariffa delle prestazioni",;
    HelpContextID = 84559728,;
    cFormVar="w_STAPREZZO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAPREZZO_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAPREZZO_1_8.GetRadio()
    this.Parent.oContained.w_STAPREZZO = this.RadioValue()
    return .t.
  endfunc

  func oSTAPREZZO_1_8.SetRadio()
    this.Parent.oContained.w_STAPREZZO=trim(this.Parent.oContained.w_STAPREZZO)
    this.value = ;
      iif(this.Parent.oContained.w_STAPREZZO=='S',1,;
      0)
  endfunc

  func oSTAPREZZO_1_8.mHide()
    with this.Parent.oContained
      return (.w_STAPREST='N' OR NOT ISALT())
    endwith
  endfunc

  add object oSTAPREZZO_1_9 as StdCheck with uid="JUDJOHPFES",rtseq=9,rtrep=.f.,left=323, top=61, caption="Prezzo",;
    ToolTipText = "Se attivo, stampa il prezzo delle prestazioni",;
    HelpContextID = 84559728,;
    cFormVar="w_STAPREZZO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAPREZZO_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAPREZZO_1_9.GetRadio()
    this.Parent.oContained.w_STAPREZZO = this.RadioValue()
    return .t.
  endfunc

  func oSTAPREZZO_1_9.SetRadio()
    this.Parent.oContained.w_STAPREZZO=trim(this.Parent.oContained.w_STAPREZZO)
    this.value = ;
      iif(this.Parent.oContained.w_STAPREZZO=='S',1,;
      0)
  endfunc

  func oSTAPREZZO_1_9.mHide()
    with this.Parent.oContained
      return (.w_STAPREST='N' OR ISALT())
    endwith
  endfunc

  add object oSTAUM_1_10 as StdCheck with uid="VSFGIGQLTS",rtseq=10,rtrep=.f.,left=429, top=61, caption="U.M.",;
    ToolTipText = "Se attivo, stampa l'unit� di misura delle prestazioni",;
    HelpContextID = 4242906,;
    cFormVar="w_STAUM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAUM_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAUM_1_10.GetRadio()
    this.Parent.oContained.w_STAUM = this.RadioValue()
    return .t.
  endfunc

  func oSTAUM_1_10.SetRadio()
    this.Parent.oContained.w_STAUM=trim(this.Parent.oContained.w_STAUM)
    this.value = ;
      iif(this.Parent.oContained.w_STAUM=='S',1,;
      0)
  endfunc

  func oSTAUM_1_10.mHide()
    with this.Parent.oContained
      return (.w_STAPREST='N')
    endwith
  endfunc

  add object oSTADUREFF_1_11 as StdCheck with uid="JDJSSVKQKZ",rtseq=11,rtrep=.f.,left=200, top=87, caption="Durata eff.",;
    ToolTipText = "Se attivo, stampa la durata effettiva delle prestazioni",;
    HelpContextID = 36587212,;
    cFormVar="w_STADUREFF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTADUREFF_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTADUREFF_1_11.GetRadio()
    this.Parent.oContained.w_STADUREFF = this.RadioValue()
    return .t.
  endfunc

  func oSTADUREFF_1_11.SetRadio()
    this.Parent.oContained.w_STADUREFF=trim(this.Parent.oContained.w_STADUREFF)
    this.value = ;
      iif(this.Parent.oContained.w_STADUREFF=='S',1,;
      0)
  endfunc

  func oSTADUREFF_1_11.mHide()
    with this.Parent.oContained
      return (.w_STAPREST='N')
    endwith
  endfunc

  add object oSTAIMP_1_12 as StdCheck with uid="AQDPJXOHDB",rtseq=12,rtrep=.f.,left=323, top=87, caption="Importo di riga",;
    ToolTipText = "Se attivo, stampa l'importo di riga delle prestazioni",;
    HelpContextID = 5029338,;
    cFormVar="w_STAIMP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAIMP_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAIMP_1_12.GetRadio()
    this.Parent.oContained.w_STAIMP = this.RadioValue()
    return .t.
  endfunc

  func oSTAIMP_1_12.SetRadio()
    this.Parent.oContained.w_STAIMP=trim(this.Parent.oContained.w_STAIMP)
    this.value = ;
      iif(this.Parent.oContained.w_STAIMP=='S',1,;
      0)
  endfunc

  func oSTAIMP_1_12.mHide()
    with this.Parent.oContained
      return (.w_STAPREST='N')
    endwith
  endfunc

  add object oSTANOTE_1_13 as StdCheck with uid="NWPKQFPIKE",rtseq=13,rtrep=.f.,left=429, top=87, caption="Note",;
    ToolTipText = "Se attivo, stampa la note",;
    HelpContextID = 64504358,;
    cFormVar="w_STANOTE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTANOTE_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTANOTE_1_13.GetRadio()
    this.Parent.oContained.w_STANOTE = this.RadioValue()
    return .t.
  endfunc

  func oSTANOTE_1_13.SetRadio()
    this.Parent.oContained.w_STANOTE=trim(this.Parent.oContained.w_STANOTE)
    this.value = ;
      iif(this.Parent.oContained.w_STANOTE=='S',1,;
      0)
  endfunc

  add object oSTAMPATTRIB_1_14 as StdCheck with uid="FQKVGOUWZX",rtseq=14,rtrep=.f.,left=200, top=113, caption="Dettaglio attributi",;
    ToolTipText = "Se attivo, vengono stampati gli attributi per ciascun componente",;
    HelpContextID = 15446170,;
    cFormVar="w_STAMPATTRIB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPATTRIB_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPATTRIB_1_14.GetRadio()
    this.Parent.oContained.w_STAMPATTRIB = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPATTRIB_1_14.SetRadio()
    this.Parent.oContained.w_STAMPATTRIB=trim(this.Parent.oContained.w_STAMPATTRIB)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPATTRIB=='S',1,;
      0)
  endfunc

  func oSTAMPATTRIB_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAIMPIAN = 'S')
    endwith
   endif
  endfunc

  func oSTAMPATTRIB_1_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oSTADESAGG_1_15 as StdCheck with uid="BIKGXKOVTS",rtseq=15,rtrep=.f.,left=200, top=113, caption="Descr. agg.",;
    ToolTipText = "Se attivo, stampa la descrizione aggiuntiva delle prestazioni",;
    HelpContextID = 231848227,;
    cFormVar="w_STADESAGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTADESAGG_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTADESAGG_1_15.GetRadio()
    this.Parent.oContained.w_STADESAGG = this.RadioValue()
    return .t.
  endfunc

  func oSTADESAGG_1_15.SetRadio()
    this.Parent.oContained.w_STADESAGG=trim(this.Parent.oContained.w_STADESAGG)
    this.value = ;
      iif(this.Parent.oContained.w_STADESAGG=='S',1,;
      0)
  endfunc

  func oSTADESAGG_1_15.mHide()
    with this.Parent.oContained
      return (.w_STAPREST='N' OR NOT ISALT())
    endwith
  endfunc

  add object oSTAPRAT_1_16 as StdCheck with uid="CRELHXIUCL",rtseq=16,rtrep=.f.,left=323, top=113, caption="Pratica",;
    ToolTipText = "Se attivo, stampa la pratica",;
    HelpContextID = 17449510,;
    cFormVar="w_STAPRAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAPRAT_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAPRAT_1_16.GetRadio()
    this.Parent.oContained.w_STAPRAT = this.RadioValue()
    return .t.
  endfunc

  func oSTAPRAT_1_16.SetRadio()
    this.Parent.oContained.w_STAPRAT=trim(this.Parent.oContained.w_STAPRAT)
    this.value = ;
      iif(this.Parent.oContained.w_STAPRAT=='S',1,;
      0)
  endfunc

  func oSTAPRAT_1_16.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oSTAPRAT_1_17 as StdCheck with uid="IDAGWPTAMG",rtseq=17,rtrep=.f.,left=323, top=113, caption="Commessa",;
    ToolTipText = "Se attivo, stampa la commessa",;
    HelpContextID = 17449510,;
    cFormVar="w_STAPRAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAPRAT_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAPRAT_1_17.GetRadio()
    this.Parent.oContained.w_STAPRAT = this.RadioValue()
    return .t.
  endfunc

  func oSTAPRAT_1_17.SetRadio()
    this.Parent.oContained.w_STAPRAT=trim(this.Parent.oContained.w_STAPRAT)
    this.value = ;
      iif(this.Parent.oContained.w_STAPRAT=='S',1,;
      0)
  endfunc

  func oSTAPRAT_1_17.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc


  add object oObj_1_19 as cp_outputCombo with uid="ZLERXIVPWT",left=112, top=149, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 87154662


  add object oBtn_1_20 as StdButton with uid="MBXAIGGBZE",left=395, top=182, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 217488858;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="ISDICPRYHO",left=445, top=182, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 83525562;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_18 as StdString with uid="JKTSRVRZDE",Visible=.t., Left=19, Top=149,;
    Alignment=1, Width=85, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="SFZUMBCFIR",Visible=.t., Left=19, Top=25,;
    Alignment=1, Width=50, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return ((NOT ISAHE() AND .w_ONUME<>1) OR (ISAHE() AND .w_ONUM<>1))
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_ssa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
