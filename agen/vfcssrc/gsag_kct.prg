* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_kct                                                        *
*              Cancellazione tipi attivit�                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-11-29                                                      *
* Last revis.: 2013-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsag_kct",oParentObject))

* --- Class definition
define class tgsag_kct as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 784
  Height = 535
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-10-08"
  HelpContextID=99231593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsag_kct"
  cComment = "Cancellazione tipi attivit�"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CodCateg = space(1)
  w_SELRAGG = space(1)
  w_SoloPrest = space(1)
  w_DTBSO_CAUMATTI = ctod('  /  /  ')
  w_AGKCT_SZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_kctPag1","gsag_kct",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCodCateg_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGKCT_SZOOM = this.oPgFrm.Pages(1).oPag.AGKCT_SZOOM
    DoDefault()
    proc Destroy()
      this.w_AGKCT_SZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAG_BCT with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CodCateg=space(1)
      .w_SELRAGG=space(1)
      .w_SoloPrest=space(1)
      .w_DTBSO_CAUMATTI=ctod("  /  /  ")
        .w_CodCateg = ' '
        .w_SELRAGG = 'N'
        .w_SoloPrest = 'N'
      .oPgFrm.Page1.oPag.AGKCT_SZOOM.Calculate()
        .w_DTBSO_CAUMATTI = i_DatSys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.AGKCT_SZOOM.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AGKCT_SZOOM.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSELRAGG_1_3.visible=!this.oPgFrm.Page1.oPag.oSELRAGG_1_3.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AGKCT_SZOOM.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCodCateg_1_2.RadioValue()==this.w_CodCateg)
      this.oPgFrm.Page1.oPag.oCodCateg_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELRAGG_1_3.RadioValue()==this.w_SELRAGG)
      this.oPgFrm.Page1.oPag.oSELRAGG_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSoloPrest_1_4.RadioValue()==this.w_SoloPrest)
      this.oPgFrm.Page1.oPag.oSoloPrest_1_4.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsag_kctPag1 as StdContainer
  Width  = 780
  height = 535
  stdWidth  = 780
  stdheight = 535
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCodCateg_1_2 as StdCombo with uid="PJHRCAEYRX",value=9,rtseq=1,rtrep=.f.,left=75,top=24,width=162,height=21;
    , ToolTipText = "Selezionare la categoria";
    , HelpContextID = 74419341;
    , cFormVar="w_CodCateg",RowSource=""+"Generica,"+"Udienza,"+"Appuntamento,"+"Cosa da fare,"+"Nota,"+"Sessione telefonica,"+"Assenza,"+"Da inserimento prestazioni,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCodCateg_1_2.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'U',;
    iif(this.value =3,'S',;
    iif(this.value =4,'D',;
    iif(this.value =5,'M',;
    iif(this.value =6,'T',;
    iif(this.value =7,'A',;
    iif(this.value =8,'Z',;
    iif(this.value =9,' ',;
    space(1)))))))))))
  endfunc
  func oCodCateg_1_2.GetRadio()
    this.Parent.oContained.w_CodCateg = this.RadioValue()
    return .t.
  endfunc

  func oCodCateg_1_2.SetRadio()
    this.Parent.oContained.w_CodCateg=trim(this.Parent.oContained.w_CodCateg)
    this.value = ;
      iif(this.Parent.oContained.w_CodCateg=='G',1,;
      iif(this.Parent.oContained.w_CodCateg=='U',2,;
      iif(this.Parent.oContained.w_CodCateg=='S',3,;
      iif(this.Parent.oContained.w_CodCateg=='D',4,;
      iif(this.Parent.oContained.w_CodCateg=='M',5,;
      iif(this.Parent.oContained.w_CodCateg=='T',6,;
      iif(this.Parent.oContained.w_CodCateg=='A',7,;
      iif(this.Parent.oContained.w_CodCateg=='Z',8,;
      iif(this.Parent.oContained.w_CodCateg=='',9,;
      0)))))))))
  endfunc

  add object oSELRAGG_1_3 as StdCheck with uid="DAMEBQISBD",rtseq=2,rtrep=.f.,left=301, top=24, caption="Raggruppamenti",;
    ToolTipText = "Se attivo, visualizza anche i tipi attivit� presenti nei raggruppamenti ed eliminando un tipo attivit� saranno eliminati anche i raggruppamenti collegati.",;
    HelpContextID = 92070694,;
    cFormVar="w_SELRAGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSELRAGG_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSELRAGG_1_3.GetRadio()
    this.Parent.oContained.w_SELRAGG = this.RadioValue()
    return .t.
  endfunc

  func oSELRAGG_1_3.SetRadio()
    this.Parent.oContained.w_SELRAGG=trim(this.Parent.oContained.w_SELRAGG)
    this.value = ;
      iif(this.Parent.oContained.w_SELRAGG=='S',1,;
      0)
  endfunc

  func oSELRAGG_1_3.mHide()
    with this.Parent.oContained
      return (.w_SoloPrest = 'S')
    endwith
  endfunc

  add object oSoloPrest_1_4 as StdCheck with uid="MHSUMVWJHT",rtseq=3,rtrep=.f.,left=462, top=24, caption="Cancella solo le prestazioni nei tipi attivit�",;
    ToolTipText = "Se attivo, vengono eliminate soltanto le prestazioni nei tipi attivit�",;
    HelpContextID = 25957593,;
    cFormVar="w_SoloPrest", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSoloPrest_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSoloPrest_1_4.GetRadio()
    this.Parent.oContained.w_SoloPrest = this.RadioValue()
    return .t.
  endfunc

  func oSoloPrest_1_4.SetRadio()
    this.Parent.oContained.w_SoloPrest=trim(this.Parent.oContained.w_SoloPrest)
    this.value = ;
      iif(this.Parent.oContained.w_SoloPrest=='S',1,;
      0)
  endfunc


  add object oBtn_1_5 as StdButton with uid="PHEWMSSLEL",left=722, top=14, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca tipi attivit�";
    , HelpContextID = 77690646;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object AGKCT_SZOOM as cp_szoombox with uid="NEVIFHDGHN",left=3, top=65, width=778,height=414,;
    caption='AGKCT_SZOOM',;
   bGlobalFont=.t.,;
    cTable="CAUMATTI",cZoomFile="GSAG_KCT",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "Ricerca,w_SELRAGG Changed,w_SoloPrest Changed",;
    nPag=1;
    , HelpContextID = 245561424


  add object oBtn_1_7 as StdButton with uid="ERHOXVEEGI",left=670, top=487, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare i tipi attivit� selezionati";
    , HelpContextID = 99202842;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        do GSAG_BCT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="TKCMXZXILU",left=722, top=487, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91914170;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="ULXWEPQBKV",Visible=.t., Left=6, Top=24,;
    Alignment=1, Width=63, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_kct','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
