* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_mpr                                                        *
*              Inserimento prestazioni                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-15                                                      *
* Last revis.: 2014-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsag_mpr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsag_mpr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsag_mpr")
  return

* --- Class definition
define class tgsag_mpr as StdPCForm
  Width  = 827
  Height = 440
  Top    = 7
  Left   = 11
  cComment = "Inserimento prestazioni"
  cPrg = "gsag_mpr"
  HelpContextID=147466089
  add object cnt as tcgsag_mpr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsag_mpr as PCContext
  w_ISAHE = space(1)
  w_FLGLIS = space(1)
  w_DACODRES = space(5)
  w_KEYLISTB = space(10)
  w_MVCODVAL = space(3)
  w_LetturaParAlte = space(5)
  w_NOEDDES = space(1)
  w_CODRES = space(5)
  w_DAGRURES = space(5)
  w_CAUACQ = space(10)
  w_CAUDOC = space(5)
  w_APPOAZI = space(5)
  w_CODLIS = space(5)
  w_COST_ORA = 0
  w_CAUATT = space(20)
  w_VALPAR = space(3)
  w_CodPratica = space(20)
  w_READAZI = space(10)
  w_CAUSALE = space(20)
  w_OLDCOM = space(15)
  w_FLQRIO = space(1)
  w_FLPREV = space(1)
  w_PRZVAC = space(1)
  w_GRUPART = space(5)
  w_DATLIS = space(8)
  w_MVFLVEAC = space(3)
  w_TIPRIS = space(1)
  w_LISACQ = space(5)
  w_FLACQ = space(1)
  w_LISACQ = space(5)
  w_FLACOMAQ = space(10)
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_FLDANA = space(1)
  w_CODLIN = space(3)
  w_DATINI = space(8)
  w_DATFIN = space(8)
  w_CODSER = space(20)
  w_DESDSER = space(40)
  w_EditSelez = space(1)
  w_EDITCODPRA = space(1)
  w_OB_TEST = space(8)
  w_CODBUN = space(10)
  w_BUFLANAL = space(1)
  w_DASERIAL = space(20)
  w_FL_BUP = space(1)
  w_DESEL_BUP = 0
  w_FLCCRAUTO = space(10)
  w_PR__DATA = space(8)
  w_DATALIST = space(8)
  w_CENCOS = space(15)
  w_DCODCEN = space(15)
  w_OLDCEN = space(15)
  w_DACODICE = space(41)
  w_PRNUMPRA = space(15)
  w_ECOD1ATT = space(20)
  w_RCOD1ATT = space(20)
  w_DATIPRIG = space(1)
  w_DATIPRI2 = space(1)
  w_ECACODART = space(20)
  w_RCACODART = space(20)
  w_DACODATT = space(20)
  w_CACODART = space(20)
  w_ARTIPRIG = space(1)
  w_ARTIPRI2 = space(1)
  w_COD1ATT = space(20)
  w_NOTIFICA = space(1)
  w_PRDESPRE = space(40)
  w_PRUNIMIS = space(3)
  w_NUMLIS = space(5)
  w_PRQTAMOV = 0
  w_NCODLIS = space(5)
  w_DAPREZZO = 0
  w_PRCODVAL = space(3)
  w_PRCODLIS = space(5)
  w_ATIPRIG = space(10)
  w_ATIPRI2 = space(10)
  w_ETIPRIG = space(10)
  w_ETIPRI2 = space(10)
  w_RTIPRIG = space(10)
  w_RTIPRI2 = space(10)
  w_DACODSED = space(5)
  w_DECTOT = 0
  w_DECUNI = 0
  w_CALCPICU = 0
  w_CALCPICT = 0
  w_PRDESAGG = space(10)
  w_PRCODOPE = 0
  w_PRDATMOD = space(8)
  w_PRVALRIG = 0
  w_LICOSTO = 0
  w_PROREEFF = 0
  w_PRMINEFF = 0
  w_DACOSUNI = 0
  w_PRCOSINT = 0
  w_DAFLDEFF = space(1)
  w_DAPREMIN = 0
  w_DAPREMAX = 0
  w_DAGAZUFF = space(6)
  w_TIPART = space(2)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_FLSERG = space(1)
  w_DESUNI = space(35)
  w_NOMEPRA = space(100)
  w_UNIMIS = space(3)
  w_LCODVAL = space(3)
  w_VALCOM = space(3)
  w_EUNMIS3 = space(3)
  w_RUNMIS3 = space(3)
  w_OBTEST = space(8)
  w_UNMIS3 = space(3)
  w_VALLIS = space(3)
  w_FLAPON = space(1)
  w_ENTE = space(10)
  w_UFFICI = space(10)
  w_FLVALO = space(1)
  w_IMPORT = 0
  w_CALDIR = space(1)
  w_COECAL = 0
  w_PARASS = 0
  w_FLAMPA = space(1)
  w_CAOVAL = 0
  w_DATOBSO = space(8)
  w_TIPOENTE = space(1)
  w_CHKTEMP = space(1)
  w_DUR_ORE = 0
  w_ECADTOBSO = space(8)
  w_RCADTOBSO = space(8)
  w_CADTOBSO = space(8)
  w_DACODCOM = space(15)
  w_DAVOCRIC = space(15)
  w_DAINICOM = space(8)
  w_DAFINCOM = space(8)
  w_DA_SEGNO = space(1)
  w_DACENCOS = space(15)
  w_DAATTIVI = space(15)
  w_NOTIPCLI = space(1)
  w_DTOBSNOM = space(8)
  w_NOCODCLI = space(15)
  w_NOTIPNOM = space(1)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_OFDATDOC = space(8)
  w_EMOLTI3 = 0
  w_RMOLTI3 = 0
  w_QTAUM1 = 0
  w_MOLTI3 = 0
  w_FLUSEP = space(1)
  w_OPERAT = space(1)
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(1)
  w_MOLTIP = 0
  w_PREZUM = space(1)
  w_ARTIPART = space(2)
  w_SCOLIS = space(1)
  w_DAVOCCOS = space(15)
  w_CCODLIS = space(5)
  w_CICLO = space(1)
  w_EOPERA3 = space(1)
  w_ROPERA3 = space(1)
  w_OPERA3 = space(1)
  w_ETIPSER = space(1)
  w_RTIPSER = space(1)
  w_TIPSER = space(1)
  w_DACODLIS = space(5)
  w_DAPROLIS = space(5)
  w_DAPROSCO = space(5)
  w_DASCOLIS = space(5)
  w_DASCONT1 = 0
  w_DASCONT2 = 0
  w_DASCONT3 = 0
  w_DASCONT4 = 0
  w_DACOSUNI = 0
  w_DACOMRIC = space(15)
  w_DAATTRIC = space(15)
  w_DACENRIC = space(15)
  w_FL_FRAZ = space(1)
  w_DALISACQ = space(5)
  w_VALACQ = space(3)
  w_INIACQ = space(8)
  w_TIPACQ = space(1)
  w_FLSACQ = space(1)
  w_IVAACQ = space(1)
  w_FLGACQ = space(1)
  w_FINACQ = space(8)
  w_DATCOINI = space(8)
  w_DATCOFIN = space(8)
  w_DATRIINI = space(8)
  w_DATRIFIN = space(8)
  w_FLSCOR = space(1)
  w_TARTEM = space(1)
  w_TARCON = 0
  w_FLVALO1 = space(1)
  w_VOCECR = space(1)
  w_TOTQTA = 0
  w_TOTORE = 0
  w_TOTMIN = 0
  w_TOTQTAM = 0
  w_CONTRACN = space(5)
  w_TIPCONCO = space(1)
  w_LISCOLCO = space(5)
  w_STATUSCO = space(1)
  w_FLCOMP = space(10)
  w_FLSPAN = space(1)
  w_DARIFPRE = 0
  w_DARIGPRE = space(1)
  w_PRESTA = space(1)
  w_DACONCOD = space(15)
  w_CEN_CauDoc = space(15)
  w_OLDCENRIC = space(15)
  w_CENRIC = space(15)
  w_DACONTRA = space(10)
  w_DACODMOD = space(10)
  w_DACODIMP = space(10)
  w_DACOCOMP = 0
  w_DARINNOV = 0
  w_DAUNIMIS = space(3)
  w_CODCLI = space(15)
  w_DADESATT = space(40)
  w_FLUNIV = space(1)
  w_NUMPRE = 0
  w_LOCALI = space(30)
  w_TOTDURMIN = 0
  w_TOTDURQTA = 0
  w_TOTDURQTAM = 0
  w_TOTALORE = 0
  w_TOTALMIN = 0
  w_TOTDURORE = 0
  w_TOTQTAORE = 0
  w_TOTQTAMIN = 0
  w_NUMSCO = 0
  w_FLDTRP = space(10)
  w_DATMAX = space(8)
  w_FLGZER = space(1)
  w_DALISACQ = space(5)
  w_FLSCOAC = space(1)
  w_CNDATFIN = space(8)
  w_CATCOM = space(3)
  w_DACODNOM = space(15)
  w_PRCODSPE = space(20)
  w_PRCODANT = space(20)
  w_PRIMPANT = 0
  w_PRIMPSPE = 0
  w_GENPRE = space(10)
  w_DTIPRIG = space(1)
  w_TIPRIGCA = space(1)
  w_TIPRI2CA = space(1)
  w_VISPRESTAZ = space(1)
  w_CNTIPPRA = space(10)
  w_CNMATOBB = space(1)
  w_CNASSCTP = space(1)
  w_CNCOMPLX = space(1)
  w_CNPROFMT = space(1)
  w_CNESIPOS = space(1)
  w_CNPERPLX = 0
  w_CNPERPOS = 0
  w_CNFLVMLQ = space(1)
  proc Save(i_oFrom)
    this.w_ISAHE = i_oFrom.w_ISAHE
    this.w_FLGLIS = i_oFrom.w_FLGLIS
    this.w_DACODRES = i_oFrom.w_DACODRES
    this.w_KEYLISTB = i_oFrom.w_KEYLISTB
    this.w_MVCODVAL = i_oFrom.w_MVCODVAL
    this.w_LetturaParAlte = i_oFrom.w_LetturaParAlte
    this.w_NOEDDES = i_oFrom.w_NOEDDES
    this.w_CODRES = i_oFrom.w_CODRES
    this.w_DAGRURES = i_oFrom.w_DAGRURES
    this.w_CAUACQ = i_oFrom.w_CAUACQ
    this.w_CAUDOC = i_oFrom.w_CAUDOC
    this.w_APPOAZI = i_oFrom.w_APPOAZI
    this.w_CODLIS = i_oFrom.w_CODLIS
    this.w_COST_ORA = i_oFrom.w_COST_ORA
    this.w_CAUATT = i_oFrom.w_CAUATT
    this.w_VALPAR = i_oFrom.w_VALPAR
    this.w_CodPratica = i_oFrom.w_CodPratica
    this.w_READAZI = i_oFrom.w_READAZI
    this.w_CAUSALE = i_oFrom.w_CAUSALE
    this.w_OLDCOM = i_oFrom.w_OLDCOM
    this.w_FLQRIO = i_oFrom.w_FLQRIO
    this.w_FLPREV = i_oFrom.w_FLPREV
    this.w_PRZVAC = i_oFrom.w_PRZVAC
    this.w_GRUPART = i_oFrom.w_GRUPART
    this.w_DATLIS = i_oFrom.w_DATLIS
    this.w_MVFLVEAC = i_oFrom.w_MVFLVEAC
    this.w_TIPRIS = i_oFrom.w_TIPRIS
    this.w_LISACQ = i_oFrom.w_LISACQ
    this.w_FLACQ = i_oFrom.w_FLACQ
    this.w_LISACQ = i_oFrom.w_LISACQ
    this.w_FLACOMAQ = i_oFrom.w_FLACOMAQ
    this.w_FLANAL = i_oFrom.w_FLANAL
    this.w_FLGCOM = i_oFrom.w_FLGCOM
    this.w_FLDANA = i_oFrom.w_FLDANA
    this.w_CODLIN = i_oFrom.w_CODLIN
    this.w_DATINI = i_oFrom.w_DATINI
    this.w_DATFIN = i_oFrom.w_DATFIN
    this.w_CODSER = i_oFrom.w_CODSER
    this.w_DESDSER = i_oFrom.w_DESDSER
    this.w_EditSelez = i_oFrom.w_EditSelez
    this.w_EDITCODPRA = i_oFrom.w_EDITCODPRA
    this.w_OB_TEST = i_oFrom.w_OB_TEST
    this.w_CODBUN = i_oFrom.w_CODBUN
    this.w_BUFLANAL = i_oFrom.w_BUFLANAL
    this.w_DASERIAL = i_oFrom.w_DASERIAL
    this.w_FL_BUP = i_oFrom.w_FL_BUP
    this.w_DESEL_BUP = i_oFrom.w_DESEL_BUP
    this.w_FLCCRAUTO = i_oFrom.w_FLCCRAUTO
    this.w_PR__DATA = i_oFrom.w_PR__DATA
    this.w_DATALIST = i_oFrom.w_DATALIST
    this.w_CENCOS = i_oFrom.w_CENCOS
    this.w_DCODCEN = i_oFrom.w_DCODCEN
    this.w_OLDCEN = i_oFrom.w_OLDCEN
    this.w_DACODICE = i_oFrom.w_DACODICE
    this.w_PRNUMPRA = i_oFrom.w_PRNUMPRA
    this.w_ECOD1ATT = i_oFrom.w_ECOD1ATT
    this.w_RCOD1ATT = i_oFrom.w_RCOD1ATT
    this.w_DATIPRIG = i_oFrom.w_DATIPRIG
    this.w_DATIPRI2 = i_oFrom.w_DATIPRI2
    this.w_ECACODART = i_oFrom.w_ECACODART
    this.w_RCACODART = i_oFrom.w_RCACODART
    this.w_DACODATT = i_oFrom.w_DACODATT
    this.w_CACODART = i_oFrom.w_CACODART
    this.w_ARTIPRIG = i_oFrom.w_ARTIPRIG
    this.w_ARTIPRI2 = i_oFrom.w_ARTIPRI2
    this.w_COD1ATT = i_oFrom.w_COD1ATT
    this.w_NOTIFICA = i_oFrom.w_NOTIFICA
    this.w_PRDESPRE = i_oFrom.w_PRDESPRE
    this.w_PRUNIMIS = i_oFrom.w_PRUNIMIS
    this.w_NUMLIS = i_oFrom.w_NUMLIS
    this.w_PRQTAMOV = i_oFrom.w_PRQTAMOV
    this.w_NCODLIS = i_oFrom.w_NCODLIS
    this.w_DAPREZZO = i_oFrom.w_DAPREZZO
    this.w_PRCODVAL = i_oFrom.w_PRCODVAL
    this.w_PRCODLIS = i_oFrom.w_PRCODLIS
    this.w_ATIPRIG = i_oFrom.w_ATIPRIG
    this.w_ATIPRI2 = i_oFrom.w_ATIPRI2
    this.w_ETIPRIG = i_oFrom.w_ETIPRIG
    this.w_ETIPRI2 = i_oFrom.w_ETIPRI2
    this.w_RTIPRIG = i_oFrom.w_RTIPRIG
    this.w_RTIPRI2 = i_oFrom.w_RTIPRI2
    this.w_DACODSED = i_oFrom.w_DACODSED
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_DECUNI = i_oFrom.w_DECUNI
    this.w_CALCPICU = i_oFrom.w_CALCPICU
    this.w_CALCPICT = i_oFrom.w_CALCPICT
    this.w_PRDESAGG = i_oFrom.w_PRDESAGG
    this.w_PRCODOPE = i_oFrom.w_PRCODOPE
    this.w_PRDATMOD = i_oFrom.w_PRDATMOD
    this.w_PRVALRIG = i_oFrom.w_PRVALRIG
    this.w_LICOSTO = i_oFrom.w_LICOSTO
    this.w_PROREEFF = i_oFrom.w_PROREEFF
    this.w_PRMINEFF = i_oFrom.w_PRMINEFF
    this.w_DACOSUNI = i_oFrom.w_DACOSUNI
    this.w_PRCOSINT = i_oFrom.w_PRCOSINT
    this.w_DAFLDEFF = i_oFrom.w_DAFLDEFF
    this.w_DAPREMIN = i_oFrom.w_DAPREMIN
    this.w_DAPREMAX = i_oFrom.w_DAPREMAX
    this.w_DAGAZUFF = i_oFrom.w_DAGAZUFF
    this.w_TIPART = i_oFrom.w_TIPART
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_FLSERG = i_oFrom.w_FLSERG
    this.w_DESUNI = i_oFrom.w_DESUNI
    this.w_NOMEPRA = i_oFrom.w_NOMEPRA
    this.w_UNIMIS = i_oFrom.w_UNIMIS
    this.w_LCODVAL = i_oFrom.w_LCODVAL
    this.w_VALCOM = i_oFrom.w_VALCOM
    this.w_EUNMIS3 = i_oFrom.w_EUNMIS3
    this.w_RUNMIS3 = i_oFrom.w_RUNMIS3
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_UNMIS3 = i_oFrom.w_UNMIS3
    this.w_VALLIS = i_oFrom.w_VALLIS
    this.w_FLAPON = i_oFrom.w_FLAPON
    this.w_ENTE = i_oFrom.w_ENTE
    this.w_UFFICI = i_oFrom.w_UFFICI
    this.w_FLVALO = i_oFrom.w_FLVALO
    this.w_IMPORT = i_oFrom.w_IMPORT
    this.w_CALDIR = i_oFrom.w_CALDIR
    this.w_COECAL = i_oFrom.w_COECAL
    this.w_PARASS = i_oFrom.w_PARASS
    this.w_FLAMPA = i_oFrom.w_FLAMPA
    this.w_CAOVAL = i_oFrom.w_CAOVAL
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_TIPOENTE = i_oFrom.w_TIPOENTE
    this.w_CHKTEMP = i_oFrom.w_CHKTEMP
    this.w_DUR_ORE = i_oFrom.w_DUR_ORE
    this.w_ECADTOBSO = i_oFrom.w_ECADTOBSO
    this.w_RCADTOBSO = i_oFrom.w_RCADTOBSO
    this.w_CADTOBSO = i_oFrom.w_CADTOBSO
    this.w_DACODCOM = i_oFrom.w_DACODCOM
    this.w_DAVOCRIC = i_oFrom.w_DAVOCRIC
    this.w_DAINICOM = i_oFrom.w_DAINICOM
    this.w_DAFINCOM = i_oFrom.w_DAFINCOM
    this.w_DA_SEGNO = i_oFrom.w_DA_SEGNO
    this.w_DACENCOS = i_oFrom.w_DACENCOS
    this.w_DAATTIVI = i_oFrom.w_DAATTIVI
    this.w_NOTIPCLI = i_oFrom.w_NOTIPCLI
    this.w_DTOBSNOM = i_oFrom.w_DTOBSNOM
    this.w_NOCODCLI = i_oFrom.w_NOCODCLI
    this.w_NOTIPNOM = i_oFrom.w_NOTIPNOM
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_OFDATDOC = i_oFrom.w_OFDATDOC
    this.w_EMOLTI3 = i_oFrom.w_EMOLTI3
    this.w_RMOLTI3 = i_oFrom.w_RMOLTI3
    this.w_QTAUM1 = i_oFrom.w_QTAUM1
    this.w_MOLTI3 = i_oFrom.w_MOLTI3
    this.w_FLUSEP = i_oFrom.w_FLUSEP
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_FLFRAZ1 = i_oFrom.w_FLFRAZ1
    this.w_MODUM2 = i_oFrom.w_MODUM2
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_PREZUM = i_oFrom.w_PREZUM
    this.w_ARTIPART = i_oFrom.w_ARTIPART
    this.w_SCOLIS = i_oFrom.w_SCOLIS
    this.w_DAVOCCOS = i_oFrom.w_DAVOCCOS
    this.w_CCODLIS = i_oFrom.w_CCODLIS
    this.w_CICLO = i_oFrom.w_CICLO
    this.w_EOPERA3 = i_oFrom.w_EOPERA3
    this.w_ROPERA3 = i_oFrom.w_ROPERA3
    this.w_OPERA3 = i_oFrom.w_OPERA3
    this.w_ETIPSER = i_oFrom.w_ETIPSER
    this.w_RTIPSER = i_oFrom.w_RTIPSER
    this.w_TIPSER = i_oFrom.w_TIPSER
    this.w_DACODLIS = i_oFrom.w_DACODLIS
    this.w_DAPROLIS = i_oFrom.w_DAPROLIS
    this.w_DAPROSCO = i_oFrom.w_DAPROSCO
    this.w_DASCOLIS = i_oFrom.w_DASCOLIS
    this.w_DASCONT1 = i_oFrom.w_DASCONT1
    this.w_DASCONT2 = i_oFrom.w_DASCONT2
    this.w_DASCONT3 = i_oFrom.w_DASCONT3
    this.w_DASCONT4 = i_oFrom.w_DASCONT4
    this.w_DACOSUNI = i_oFrom.w_DACOSUNI
    this.w_DACOMRIC = i_oFrom.w_DACOMRIC
    this.w_DAATTRIC = i_oFrom.w_DAATTRIC
    this.w_DACENRIC = i_oFrom.w_DACENRIC
    this.w_FL_FRAZ = i_oFrom.w_FL_FRAZ
    this.w_DALISACQ = i_oFrom.w_DALISACQ
    this.w_VALACQ = i_oFrom.w_VALACQ
    this.w_INIACQ = i_oFrom.w_INIACQ
    this.w_TIPACQ = i_oFrom.w_TIPACQ
    this.w_FLSACQ = i_oFrom.w_FLSACQ
    this.w_IVAACQ = i_oFrom.w_IVAACQ
    this.w_FLGACQ = i_oFrom.w_FLGACQ
    this.w_FINACQ = i_oFrom.w_FINACQ
    this.w_DATCOINI = i_oFrom.w_DATCOINI
    this.w_DATCOFIN = i_oFrom.w_DATCOFIN
    this.w_DATRIINI = i_oFrom.w_DATRIINI
    this.w_DATRIFIN = i_oFrom.w_DATRIFIN
    this.w_FLSCOR = i_oFrom.w_FLSCOR
    this.w_TARTEM = i_oFrom.w_TARTEM
    this.w_TARCON = i_oFrom.w_TARCON
    this.w_FLVALO1 = i_oFrom.w_FLVALO1
    this.w_VOCECR = i_oFrom.w_VOCECR
    this.w_TOTQTA = i_oFrom.w_TOTQTA
    this.w_TOTORE = i_oFrom.w_TOTORE
    this.w_TOTMIN = i_oFrom.w_TOTMIN
    this.w_TOTQTAM = i_oFrom.w_TOTQTAM
    this.w_CONTRACN = i_oFrom.w_CONTRACN
    this.w_TIPCONCO = i_oFrom.w_TIPCONCO
    this.w_LISCOLCO = i_oFrom.w_LISCOLCO
    this.w_STATUSCO = i_oFrom.w_STATUSCO
    this.w_FLCOMP = i_oFrom.w_FLCOMP
    this.w_FLSPAN = i_oFrom.w_FLSPAN
    this.w_DARIFPRE = i_oFrom.w_DARIFPRE
    this.w_DARIGPRE = i_oFrom.w_DARIGPRE
    this.w_PRESTA = i_oFrom.w_PRESTA
    this.w_DACONCOD = i_oFrom.w_DACONCOD
    this.w_CEN_CauDoc = i_oFrom.w_CEN_CauDoc
    this.w_OLDCENRIC = i_oFrom.w_OLDCENRIC
    this.w_CENRIC = i_oFrom.w_CENRIC
    this.w_DACONTRA = i_oFrom.w_DACONTRA
    this.w_DACODMOD = i_oFrom.w_DACODMOD
    this.w_DACODIMP = i_oFrom.w_DACODIMP
    this.w_DACOCOMP = i_oFrom.w_DACOCOMP
    this.w_DARINNOV = i_oFrom.w_DARINNOV
    this.w_DAUNIMIS = i_oFrom.w_DAUNIMIS
    this.w_CODCLI = i_oFrom.w_CODCLI
    this.w_DADESATT = i_oFrom.w_DADESATT
    this.w_FLUNIV = i_oFrom.w_FLUNIV
    this.w_NUMPRE = i_oFrom.w_NUMPRE
    this.w_LOCALI = i_oFrom.w_LOCALI
    this.w_TOTDURMIN = i_oFrom.w_TOTDURMIN
    this.w_TOTDURQTA = i_oFrom.w_TOTDURQTA
    this.w_TOTDURQTAM = i_oFrom.w_TOTDURQTAM
    this.w_TOTALORE = i_oFrom.w_TOTALORE
    this.w_TOTALMIN = i_oFrom.w_TOTALMIN
    this.w_TOTDURORE = i_oFrom.w_TOTDURORE
    this.w_TOTQTAORE = i_oFrom.w_TOTQTAORE
    this.w_TOTQTAMIN = i_oFrom.w_TOTQTAMIN
    this.w_NUMSCO = i_oFrom.w_NUMSCO
    this.w_FLDTRP = i_oFrom.w_FLDTRP
    this.w_DATMAX = i_oFrom.w_DATMAX
    this.w_FLGZER = i_oFrom.w_FLGZER
    this.w_DALISACQ = i_oFrom.w_DALISACQ
    this.w_FLSCOAC = i_oFrom.w_FLSCOAC
    this.w_CNDATFIN = i_oFrom.w_CNDATFIN
    this.w_CATCOM = i_oFrom.w_CATCOM
    this.w_DACODNOM = i_oFrom.w_DACODNOM
    this.w_PRCODSPE = i_oFrom.w_PRCODSPE
    this.w_PRCODANT = i_oFrom.w_PRCODANT
    this.w_PRIMPANT = i_oFrom.w_PRIMPANT
    this.w_PRIMPSPE = i_oFrom.w_PRIMPSPE
    this.w_GENPRE = i_oFrom.w_GENPRE
    this.w_DTIPRIG = i_oFrom.w_DTIPRIG
    this.w_TIPRIGCA = i_oFrom.w_TIPRIGCA
    this.w_TIPRI2CA = i_oFrom.w_TIPRI2CA
    this.w_VISPRESTAZ = i_oFrom.w_VISPRESTAZ
    this.w_CNTIPPRA = i_oFrom.w_CNTIPPRA
    this.w_CNMATOBB = i_oFrom.w_CNMATOBB
    this.w_CNASSCTP = i_oFrom.w_CNASSCTP
    this.w_CNCOMPLX = i_oFrom.w_CNCOMPLX
    this.w_CNPROFMT = i_oFrom.w_CNPROFMT
    this.w_CNESIPOS = i_oFrom.w_CNESIPOS
    this.w_CNPERPLX = i_oFrom.w_CNPERPLX
    this.w_CNPERPOS = i_oFrom.w_CNPERPOS
    this.w_CNFLVMLQ = i_oFrom.w_CNFLVMLQ
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_ISAHE = this.w_ISAHE
    i_oTo.w_FLGLIS = this.w_FLGLIS
    i_oTo.w_DACODRES = this.w_DACODRES
    i_oTo.w_KEYLISTB = this.w_KEYLISTB
    i_oTo.w_MVCODVAL = this.w_MVCODVAL
    i_oTo.w_LetturaParAlte = this.w_LetturaParAlte
    i_oTo.w_NOEDDES = this.w_NOEDDES
    i_oTo.w_CODRES = this.w_CODRES
    i_oTo.w_DAGRURES = this.w_DAGRURES
    i_oTo.w_CAUACQ = this.w_CAUACQ
    i_oTo.w_CAUDOC = this.w_CAUDOC
    i_oTo.w_APPOAZI = this.w_APPOAZI
    i_oTo.w_CODLIS = this.w_CODLIS
    i_oTo.w_COST_ORA = this.w_COST_ORA
    i_oTo.w_CAUATT = this.w_CAUATT
    i_oTo.w_VALPAR = this.w_VALPAR
    i_oTo.w_CodPratica = this.w_CodPratica
    i_oTo.w_READAZI = this.w_READAZI
    i_oTo.w_CAUSALE = this.w_CAUSALE
    i_oTo.w_OLDCOM = this.w_OLDCOM
    i_oTo.w_FLQRIO = this.w_FLQRIO
    i_oTo.w_FLPREV = this.w_FLPREV
    i_oTo.w_PRZVAC = this.w_PRZVAC
    i_oTo.w_GRUPART = this.w_GRUPART
    i_oTo.w_DATLIS = this.w_DATLIS
    i_oTo.w_MVFLVEAC = this.w_MVFLVEAC
    i_oTo.w_TIPRIS = this.w_TIPRIS
    i_oTo.w_LISACQ = this.w_LISACQ
    i_oTo.w_FLACQ = this.w_FLACQ
    i_oTo.w_LISACQ = this.w_LISACQ
    i_oTo.w_FLACOMAQ = this.w_FLACOMAQ
    i_oTo.w_FLANAL = this.w_FLANAL
    i_oTo.w_FLGCOM = this.w_FLGCOM
    i_oTo.w_FLDANA = this.w_FLDANA
    i_oTo.w_CODLIN = this.w_CODLIN
    i_oTo.w_DATINI = this.w_DATINI
    i_oTo.w_DATFIN = this.w_DATFIN
    i_oTo.w_CODSER = this.w_CODSER
    i_oTo.w_DESDSER = this.w_DESDSER
    i_oTo.w_EditSelez = this.w_EditSelez
    i_oTo.w_EDITCODPRA = this.w_EDITCODPRA
    i_oTo.w_OB_TEST = this.w_OB_TEST
    i_oTo.w_CODBUN = this.w_CODBUN
    i_oTo.w_BUFLANAL = this.w_BUFLANAL
    i_oTo.w_DASERIAL = this.w_DASERIAL
    i_oTo.w_FL_BUP = this.w_FL_BUP
    i_oTo.w_DESEL_BUP = this.w_DESEL_BUP
    i_oTo.w_FLCCRAUTO = this.w_FLCCRAUTO
    i_oTo.w_PR__DATA = this.w_PR__DATA
    i_oTo.w_DATALIST = this.w_DATALIST
    i_oTo.w_CENCOS = this.w_CENCOS
    i_oTo.w_DCODCEN = this.w_DCODCEN
    i_oTo.w_OLDCEN = this.w_OLDCEN
    i_oTo.w_DACODICE = this.w_DACODICE
    i_oTo.w_PRNUMPRA = this.w_PRNUMPRA
    i_oTo.w_ECOD1ATT = this.w_ECOD1ATT
    i_oTo.w_RCOD1ATT = this.w_RCOD1ATT
    i_oTo.w_DATIPRIG = this.w_DATIPRIG
    i_oTo.w_DATIPRI2 = this.w_DATIPRI2
    i_oTo.w_ECACODART = this.w_ECACODART
    i_oTo.w_RCACODART = this.w_RCACODART
    i_oTo.w_DACODATT = this.w_DACODATT
    i_oTo.w_CACODART = this.w_CACODART
    i_oTo.w_ARTIPRIG = this.w_ARTIPRIG
    i_oTo.w_ARTIPRI2 = this.w_ARTIPRI2
    i_oTo.w_COD1ATT = this.w_COD1ATT
    i_oTo.w_NOTIFICA = this.w_NOTIFICA
    i_oTo.w_PRDESPRE = this.w_PRDESPRE
    i_oTo.w_PRUNIMIS = this.w_PRUNIMIS
    i_oTo.w_NUMLIS = this.w_NUMLIS
    i_oTo.w_PRQTAMOV = this.w_PRQTAMOV
    i_oTo.w_NCODLIS = this.w_NCODLIS
    i_oTo.w_DAPREZZO = this.w_DAPREZZO
    i_oTo.w_PRCODVAL = this.w_PRCODVAL
    i_oTo.w_PRCODLIS = this.w_PRCODLIS
    i_oTo.w_ATIPRIG = this.w_ATIPRIG
    i_oTo.w_ATIPRI2 = this.w_ATIPRI2
    i_oTo.w_ETIPRIG = this.w_ETIPRIG
    i_oTo.w_ETIPRI2 = this.w_ETIPRI2
    i_oTo.w_RTIPRIG = this.w_RTIPRIG
    i_oTo.w_RTIPRI2 = this.w_RTIPRI2
    i_oTo.w_DACODSED = this.w_DACODSED
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_DECUNI = this.w_DECUNI
    i_oTo.w_CALCPICU = this.w_CALCPICU
    i_oTo.w_CALCPICT = this.w_CALCPICT
    i_oTo.w_PRDESAGG = this.w_PRDESAGG
    i_oTo.w_PRCODOPE = this.w_PRCODOPE
    i_oTo.w_PRDATMOD = this.w_PRDATMOD
    i_oTo.w_PRVALRIG = this.w_PRVALRIG
    i_oTo.w_LICOSTO = this.w_LICOSTO
    i_oTo.w_PROREEFF = this.w_PROREEFF
    i_oTo.w_PRMINEFF = this.w_PRMINEFF
    i_oTo.w_DACOSUNI = this.w_DACOSUNI
    i_oTo.w_PRCOSINT = this.w_PRCOSINT
    i_oTo.w_DAFLDEFF = this.w_DAFLDEFF
    i_oTo.w_DAPREMIN = this.w_DAPREMIN
    i_oTo.w_DAPREMAX = this.w_DAPREMAX
    i_oTo.w_DAGAZUFF = this.w_DAGAZUFF
    i_oTo.w_TIPART = this.w_TIPART
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_FLSERG = this.w_FLSERG
    i_oTo.w_DESUNI = this.w_DESUNI
    i_oTo.w_NOMEPRA = this.w_NOMEPRA
    i_oTo.w_UNIMIS = this.w_UNIMIS
    i_oTo.w_LCODVAL = this.w_LCODVAL
    i_oTo.w_VALCOM = this.w_VALCOM
    i_oTo.w_EUNMIS3 = this.w_EUNMIS3
    i_oTo.w_RUNMIS3 = this.w_RUNMIS3
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_UNMIS3 = this.w_UNMIS3
    i_oTo.w_VALLIS = this.w_VALLIS
    i_oTo.w_FLAPON = this.w_FLAPON
    i_oTo.w_ENTE = this.w_ENTE
    i_oTo.w_UFFICI = this.w_UFFICI
    i_oTo.w_FLVALO = this.w_FLVALO
    i_oTo.w_IMPORT = this.w_IMPORT
    i_oTo.w_CALDIR = this.w_CALDIR
    i_oTo.w_COECAL = this.w_COECAL
    i_oTo.w_PARASS = this.w_PARASS
    i_oTo.w_FLAMPA = this.w_FLAMPA
    i_oTo.w_CAOVAL = this.w_CAOVAL
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_TIPOENTE = this.w_TIPOENTE
    i_oTo.w_CHKTEMP = this.w_CHKTEMP
    i_oTo.w_DUR_ORE = this.w_DUR_ORE
    i_oTo.w_ECADTOBSO = this.w_ECADTOBSO
    i_oTo.w_RCADTOBSO = this.w_RCADTOBSO
    i_oTo.w_CADTOBSO = this.w_CADTOBSO
    i_oTo.w_DACODCOM = this.w_DACODCOM
    i_oTo.w_DAVOCRIC = this.w_DAVOCRIC
    i_oTo.w_DAINICOM = this.w_DAINICOM
    i_oTo.w_DAFINCOM = this.w_DAFINCOM
    i_oTo.w_DA_SEGNO = this.w_DA_SEGNO
    i_oTo.w_DACENCOS = this.w_DACENCOS
    i_oTo.w_DAATTIVI = this.w_DAATTIVI
    i_oTo.w_NOTIPCLI = this.w_NOTIPCLI
    i_oTo.w_DTOBSNOM = this.w_DTOBSNOM
    i_oTo.w_NOCODCLI = this.w_NOCODCLI
    i_oTo.w_NOTIPNOM = this.w_NOTIPNOM
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_OFDATDOC = this.w_OFDATDOC
    i_oTo.w_EMOLTI3 = this.w_EMOLTI3
    i_oTo.w_RMOLTI3 = this.w_RMOLTI3
    i_oTo.w_QTAUM1 = this.w_QTAUM1
    i_oTo.w_MOLTI3 = this.w_MOLTI3
    i_oTo.w_FLUSEP = this.w_FLUSEP
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_FLFRAZ1 = this.w_FLFRAZ1
    i_oTo.w_MODUM2 = this.w_MODUM2
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_PREZUM = this.w_PREZUM
    i_oTo.w_ARTIPART = this.w_ARTIPART
    i_oTo.w_SCOLIS = this.w_SCOLIS
    i_oTo.w_DAVOCCOS = this.w_DAVOCCOS
    i_oTo.w_CCODLIS = this.w_CCODLIS
    i_oTo.w_CICLO = this.w_CICLO
    i_oTo.w_EOPERA3 = this.w_EOPERA3
    i_oTo.w_ROPERA3 = this.w_ROPERA3
    i_oTo.w_OPERA3 = this.w_OPERA3
    i_oTo.w_ETIPSER = this.w_ETIPSER
    i_oTo.w_RTIPSER = this.w_RTIPSER
    i_oTo.w_TIPSER = this.w_TIPSER
    i_oTo.w_DACODLIS = this.w_DACODLIS
    i_oTo.w_DAPROLIS = this.w_DAPROLIS
    i_oTo.w_DAPROSCO = this.w_DAPROSCO
    i_oTo.w_DASCOLIS = this.w_DASCOLIS
    i_oTo.w_DASCONT1 = this.w_DASCONT1
    i_oTo.w_DASCONT2 = this.w_DASCONT2
    i_oTo.w_DASCONT3 = this.w_DASCONT3
    i_oTo.w_DASCONT4 = this.w_DASCONT4
    i_oTo.w_DACOSUNI = this.w_DACOSUNI
    i_oTo.w_DACOMRIC = this.w_DACOMRIC
    i_oTo.w_DAATTRIC = this.w_DAATTRIC
    i_oTo.w_DACENRIC = this.w_DACENRIC
    i_oTo.w_FL_FRAZ = this.w_FL_FRAZ
    i_oTo.w_DALISACQ = this.w_DALISACQ
    i_oTo.w_VALACQ = this.w_VALACQ
    i_oTo.w_INIACQ = this.w_INIACQ
    i_oTo.w_TIPACQ = this.w_TIPACQ
    i_oTo.w_FLSACQ = this.w_FLSACQ
    i_oTo.w_IVAACQ = this.w_IVAACQ
    i_oTo.w_FLGACQ = this.w_FLGACQ
    i_oTo.w_FINACQ = this.w_FINACQ
    i_oTo.w_DATCOINI = this.w_DATCOINI
    i_oTo.w_DATCOFIN = this.w_DATCOFIN
    i_oTo.w_DATRIINI = this.w_DATRIINI
    i_oTo.w_DATRIFIN = this.w_DATRIFIN
    i_oTo.w_FLSCOR = this.w_FLSCOR
    i_oTo.w_TARTEM = this.w_TARTEM
    i_oTo.w_TARCON = this.w_TARCON
    i_oTo.w_FLVALO1 = this.w_FLVALO1
    i_oTo.w_VOCECR = this.w_VOCECR
    i_oTo.w_TOTQTA = this.w_TOTQTA
    i_oTo.w_TOTORE = this.w_TOTORE
    i_oTo.w_TOTMIN = this.w_TOTMIN
    i_oTo.w_TOTQTAM = this.w_TOTQTAM
    i_oTo.w_CONTRACN = this.w_CONTRACN
    i_oTo.w_TIPCONCO = this.w_TIPCONCO
    i_oTo.w_LISCOLCO = this.w_LISCOLCO
    i_oTo.w_STATUSCO = this.w_STATUSCO
    i_oTo.w_FLCOMP = this.w_FLCOMP
    i_oTo.w_FLSPAN = this.w_FLSPAN
    i_oTo.w_DARIFPRE = this.w_DARIFPRE
    i_oTo.w_DARIGPRE = this.w_DARIGPRE
    i_oTo.w_PRESTA = this.w_PRESTA
    i_oTo.w_DACONCOD = this.w_DACONCOD
    i_oTo.w_CEN_CauDoc = this.w_CEN_CauDoc
    i_oTo.w_OLDCENRIC = this.w_OLDCENRIC
    i_oTo.w_CENRIC = this.w_CENRIC
    i_oTo.w_DACONTRA = this.w_DACONTRA
    i_oTo.w_DACODMOD = this.w_DACODMOD
    i_oTo.w_DACODIMP = this.w_DACODIMP
    i_oTo.w_DACOCOMP = this.w_DACOCOMP
    i_oTo.w_DARINNOV = this.w_DARINNOV
    i_oTo.w_DAUNIMIS = this.w_DAUNIMIS
    i_oTo.w_CODCLI = this.w_CODCLI
    i_oTo.w_DADESATT = this.w_DADESATT
    i_oTo.w_FLUNIV = this.w_FLUNIV
    i_oTo.w_NUMPRE = this.w_NUMPRE
    i_oTo.w_LOCALI = this.w_LOCALI
    i_oTo.w_TOTDURMIN = this.w_TOTDURMIN
    i_oTo.w_TOTDURQTA = this.w_TOTDURQTA
    i_oTo.w_TOTDURQTAM = this.w_TOTDURQTAM
    i_oTo.w_TOTALORE = this.w_TOTALORE
    i_oTo.w_TOTALMIN = this.w_TOTALMIN
    i_oTo.w_TOTDURORE = this.w_TOTDURORE
    i_oTo.w_TOTQTAORE = this.w_TOTQTAORE
    i_oTo.w_TOTQTAMIN = this.w_TOTQTAMIN
    i_oTo.w_NUMSCO = this.w_NUMSCO
    i_oTo.w_FLDTRP = this.w_FLDTRP
    i_oTo.w_DATMAX = this.w_DATMAX
    i_oTo.w_FLGZER = this.w_FLGZER
    i_oTo.w_DALISACQ = this.w_DALISACQ
    i_oTo.w_FLSCOAC = this.w_FLSCOAC
    i_oTo.w_CNDATFIN = this.w_CNDATFIN
    i_oTo.w_CATCOM = this.w_CATCOM
    i_oTo.w_DACODNOM = this.w_DACODNOM
    i_oTo.w_PRCODSPE = this.w_PRCODSPE
    i_oTo.w_PRCODANT = this.w_PRCODANT
    i_oTo.w_PRIMPANT = this.w_PRIMPANT
    i_oTo.w_PRIMPSPE = this.w_PRIMPSPE
    i_oTo.w_GENPRE = this.w_GENPRE
    i_oTo.w_DTIPRIG = this.w_DTIPRIG
    i_oTo.w_TIPRIGCA = this.w_TIPRIGCA
    i_oTo.w_TIPRI2CA = this.w_TIPRI2CA
    i_oTo.w_VISPRESTAZ = this.w_VISPRESTAZ
    i_oTo.w_CNTIPPRA = this.w_CNTIPPRA
    i_oTo.w_CNMATOBB = this.w_CNMATOBB
    i_oTo.w_CNASSCTP = this.w_CNASSCTP
    i_oTo.w_CNCOMPLX = this.w_CNCOMPLX
    i_oTo.w_CNPROFMT = this.w_CNPROFMT
    i_oTo.w_CNESIPOS = this.w_CNESIPOS
    i_oTo.w_CNPERPLX = this.w_CNPERPLX
    i_oTo.w_CNPERPOS = this.w_CNPERPOS
    i_oTo.w_CNFLVMLQ = this.w_CNFLVMLQ
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsag_mpr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 827
  Height = 440
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-05"
  HelpContextID=147466089
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=258

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  RAP_PRES_IDX = 0
  CAN_TIER_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  DIPENDEN_IDX = 0
  VALUTE_IDX = 0
  LISTINI_IDX = 0
  PAR_AGEN_IDX = 0
  NOT_ARTI_IDX = 0
  KEY_ARTI_IDX = 0
  PRA_ENTI_IDX = 0
  CENCOST_IDX = 0
  CAUMATTI_IDX = 0
  TIP_DOCU_IDX = 0
  OFF_NOMI_IDX = 0
  DES_DIVE_IDX = 0
  CONTI_IDX = 0
  PRA_CONT_IDX = 0
  PAR_ALTE_IDX = 0
  BUSIUNIT_IDX = 0
  PRA_TIPI_IDX = 0
  cFile = "RAP_PRES"
  cKeySelect = "DACODRES,DAGRURES"
  cKeyWhere  = "DACODRES=this.w_DACODRES and DAGRURES=this.w_DAGRURES"
  cKeyDetail  = "DACODRES=this.w_DACODRES and DAGRURES=this.w_DAGRURES"
  cKeyWhereODBC = '"DACODRES="+cp_ToStrODBC(this.w_DACODRES)';
      +'+" and DAGRURES="+cp_ToStrODBC(this.w_DAGRURES)';

  cKeyDetailWhereODBC = '"DACODRES="+cp_ToStrODBC(this.w_DACODRES)';
      +'+" and DAGRURES="+cp_ToStrODBC(this.w_DAGRURES)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"RAP_PRES.DACODRES="+cp_ToStrODBC(this.w_DACODRES)';
      +'+" and RAP_PRES.DAGRURES="+cp_ToStrODBC(this.w_DAGRURES)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'RAP_PRES.PR__DATA,RAP_PRES.PRNUMPRA,RAP_PRES.PRCODVAL,RAP_PRES.PRCODLIS'
  cPrg = "gsag_mpr"
  cComment = "Inserimento prestazioni"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 9
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ISAHE = .F.
  w_FLGLIS = space(1)
  w_DACODRES = space(5)
  o_DACODRES = space(5)
  w_KEYLISTB = space(10)
  w_MVCODVAL = space(3)
  w_LetturaParAlte = space(5)
  o_LetturaParAlte = space(5)
  w_NOEDDES = space(1)
  w_CODRES = space(5)
  w_DAGRURES = space(5)
  w_CAUACQ = space(10)
  w_CAUDOC = space(5)
  w_APPOAZI = space(5)
  w_CODLIS = space(5)
  o_CODLIS = space(5)
  w_COST_ORA = 0
  w_CAUATT = space(20)
  w_VALPAR = space(3)
  w_CodPratica = space(20)
  w_READAZI = space(10)
  w_CAUSALE = space(20)
  w_OLDCOM = space(15)
  w_FLQRIO = space(1)
  w_FLPREV = space(1)
  w_PRZVAC = space(1)
  w_GRUPART = space(5)
  w_DATLIS = ctod('  /  /  ')
  w_MVFLVEAC = space(3)
  w_TIPRIS = space(1)
  w_LISACQ = space(5)
  w_FLACQ = space(1)
  w_LISACQ = space(5)
  w_FLACOMAQ = space(10)
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_FLDANA = space(1)
  w_CODLIN = space(3)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CODSER = space(20)
  w_DESDSER = space(40)
  w_EditSelez = space(1)
  w_EDITCODPRA = .F.
  w_OB_TEST = ctod('  /  /  ')
  w_CODBUN = space(10)
  w_BUFLANAL = space(1)
  w_DASERIAL = space(20)
  w_FL_BUP = space(1)
  w_DESEL_BUP = 0
  w_FLCCRAUTO = space(10)
  w_PR__DATA = ctod('  /  /  ')
  o_PR__DATA = ctod('  /  /  ')
  w_DATALIST = ctod('  /  /  ')
  w_CENCOS = space(15)
  o_CENCOS = space(15)
  w_DCODCEN = space(15)
  o_DCODCEN = space(15)
  w_OLDCEN = space(15)
  w_DACODICE = space(41)
  o_DACODICE = space(41)
  w_PRNUMPRA = space(15)
  o_PRNUMPRA = space(15)
  w_ECOD1ATT = space(20)
  w_RCOD1ATT = space(20)
  w_DATIPRIG = space(1)
  w_DATIPRI2 = space(1)
  w_ECACODART = space(20)
  w_RCACODART = space(20)
  w_DACODATT = space(20)
  o_DACODATT = space(20)
  w_CACODART = space(20)
  w_ARTIPRIG = space(1)
  w_ARTIPRI2 = space(1)
  w_COD1ATT = space(20)
  w_NOTIFICA = space(1)
  w_PRDESPRE = space(40)
  w_PRUNIMIS = space(3)
  o_PRUNIMIS = space(3)
  w_NUMLIS = space(5)
  w_PRQTAMOV = 0
  o_PRQTAMOV = 0
  w_NCODLIS = space(5)
  w_DAPREZZO = 0
  o_DAPREZZO = 0
  w_PRCODVAL = space(3)
  o_PRCODVAL = space(3)
  w_PRCODLIS = space(5)
  w_ATIPRIG = space(10)
  o_ATIPRIG = space(10)
  w_ATIPRI2 = space(10)
  o_ATIPRI2 = space(10)
  w_ETIPRIG = space(10)
  o_ETIPRIG = space(10)
  w_ETIPRI2 = space(10)
  o_ETIPRI2 = space(10)
  w_RTIPRIG = space(10)
  o_RTIPRIG = space(10)
  w_RTIPRI2 = space(10)
  o_RTIPRI2 = space(10)
  w_DACODSED = space(5)
  w_DECTOT = 0
  w_DECUNI = 0
  w_CALCPICU = 0
  w_CALCPICT = 0
  w_PRDESAGG = space(0)
  w_PRCODOPE = 0
  w_PRDATMOD = ctod('  /  /  ')
  w_PRVALRIG = 0
  w_LICOSTO = 0
  w_PROREEFF = 0
  o_PROREEFF = 0
  w_PRMINEFF = 0
  o_PRMINEFF = 0
  w_DACOSUNI = 0
  o_DACOSUNI = 0
  w_PRCOSINT = 0
  w_DAFLDEFF = space(1)
  w_DAPREMIN = 0
  w_DAPREMAX = 0
  w_DAGAZUFF = space(6)
  w_TIPART = space(2)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_FLSERG = space(1)
  w_DESUNI = space(35)
  w_NOMEPRA = space(100)
  w_UNIMIS = space(3)
  w_LCODVAL = space(3)
  w_VALCOM = space(3)
  w_EUNMIS3 = space(3)
  w_RUNMIS3 = space(3)
  w_OBTEST = ctod('  /  /  ')
  w_UNMIS3 = space(3)
  w_VALLIS = space(3)
  w_FLAPON = space(1)
  w_ENTE = space(10)
  w_UFFICI = space(10)
  w_FLVALO = space(1)
  w_IMPORT = 0
  w_CALDIR = space(1)
  w_COECAL = 0
  w_PARASS = 0
  w_FLAMPA = space(1)
  w_CAOVAL = 0
  w_DATOBSO = ctod('  /  /  ')
  w_TIPOENTE = space(1)
  w_CHKTEMP = space(1)
  w_DUR_ORE = 0
  w_ECADTOBSO = ctod('  /  /  ')
  w_RCADTOBSO = ctod('  /  /  ')
  w_CADTOBSO = ctod('  /  /  ')
  w_DACODCOM = space(15)
  o_DACODCOM = space(15)
  w_DAVOCRIC = space(15)
  w_DAINICOM = ctod('  /  /  ')
  w_DAFINCOM = ctod('  /  /  ')
  w_DA_SEGNO = space(1)
  w_DACENCOS = space(15)
  o_DACENCOS = space(15)
  w_DAATTIVI = space(15)
  w_NOTIPCLI = space(1)
  w_DTOBSNOM = ctod('  /  /  ')
  w_NOCODCLI = space(15)
  w_NOTIPNOM = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_OFDATDOC = ctod('  /  /  ')
  w_EMOLTI3 = 0
  w_RMOLTI3 = 0
  w_QTAUM1 = 0
  w_MOLTI3 = 0
  w_FLUSEP = space(1)
  w_OPERAT = space(1)
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(1)
  w_MOLTIP = 0
  w_PREZUM = space(1)
  w_ARTIPART = space(2)
  w_SCOLIS = space(1)
  w_DAVOCCOS = space(15)
  w_CCODLIS = space(5)
  o_CCODLIS = space(5)
  w_CICLO = space(1)
  w_EOPERA3 = space(1)
  w_ROPERA3 = space(1)
  w_OPERA3 = space(1)
  w_ETIPSER = space(1)
  w_RTIPSER = space(1)
  w_TIPSER = space(1)
  w_DACODLIS = space(5)
  w_DAPROLIS = space(5)
  w_DAPROSCO = space(5)
  w_DASCOLIS = space(5)
  w_DASCONT1 = 0
  w_DASCONT2 = 0
  w_DASCONT3 = 0
  w_DASCONT4 = 0
  w_DACOSUNI = 0
  w_DACOMRIC = space(15)
  w_DAATTRIC = space(15)
  w_DACENRIC = space(15)
  o_DACENRIC = space(15)
  w_FL_FRAZ = space(1)
  w_DALISACQ = space(5)
  w_VALACQ = space(3)
  w_INIACQ = ctod('  /  /  ')
  w_TIPACQ = space(1)
  w_FLSACQ = space(1)
  w_IVAACQ = space(1)
  w_FLGACQ = space(1)
  w_FINACQ = ctod('  /  /  ')
  w_DATCOINI = ctod('  /  /  ')
  w_DATCOFIN = ctod('  /  /  ')
  w_DATRIINI = ctod('  /  /  ')
  w_DATRIFIN = ctod('  /  /  ')
  w_FLSCOR = space(1)
  w_TARTEM = space(1)
  w_TARCON = 0
  w_FLVALO1 = space(1)
  w_VOCECR = space(1)
  w_TOTQTA = 0
  w_TOTORE = 0
  w_TOTMIN = 0
  w_TOTQTAM = 0
  w_CONTRACN = space(5)
  w_TIPCONCO = space(1)
  w_LISCOLCO = space(5)
  w_STATUSCO = space(1)
  w_FLCOMP = space(10)
  w_FLSPAN = space(1)
  w_DARIFPRE = 0
  w_DARIGPRE = space(1)
  w_PRESTA = space(1)
  w_DACONCOD = space(15)
  w_CEN_CauDoc = space(15)
  w_OLDCENRIC = space(15)
  w_CENRIC = space(15)
  w_DACONTRA = space(10)
  w_DACODMOD = space(10)
  w_DACODIMP = space(10)
  w_DACOCOMP = 0
  w_DARINNOV = 0
  w_DAUNIMIS = space(3)
  w_CODCLI = space(15)
  w_DADESATT = space(40)
  w_FLUNIV = space(1)
  w_NUMPRE = 0
  w_LOCALI = space(30)
  w_TOTDURMIN = 0
  w_TOTDURQTA = 0
  o_TOTDURQTA = 0
  w_TOTDURQTAM = 0
  o_TOTDURQTAM = 0
  w_TOTALORE = 0
  o_TOTALORE = 0
  w_TOTALMIN = 0
  o_TOTALMIN = 0
  w_TOTDURORE = 0
  w_TOTQTAORE = 0
  w_TOTQTAMIN = 0
  w_NUMSCO = 0
  w_FLDTRP = space(10)
  w_DATMAX = ctod('  /  /  ')
  w_FLGZER = space(1)
  w_DALISACQ = space(5)
  w_FLSCOAC = space(1)
  w_CNDATFIN = ctod('  /  /  ')
  w_CATCOM = space(3)
  w_DACODNOM = space(15)
  o_DACODNOM = space(15)
  w_PRCODSPE = space(20)
  w_PRCODANT = space(20)
  w_PRIMPANT = 0
  w_PRIMPSPE = 0
  w_GENPRE = space(10)
  w_DTIPRIG = space(1)
  w_TIPRIGCA = space(1)
  w_TIPRI2CA = space(1)
  w_VISPRESTAZ = space(1)
  w_CNTIPPRA = space(10)
  w_CNMATOBB = space(1)
  w_CNASSCTP = space(1)
  w_CNCOMPLX = space(1)
  w_CNPROFMT = space(1)
  w_CNESIPOS = space(1)
  w_CNPERPLX = 0
  w_CNPERPOS = 0
  w_CNFLVMLQ = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsag_mprPag1","gsag_mpr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[21]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='DIPENDEN'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='LISTINI'
    this.cWorkTables[7]='PAR_AGEN'
    this.cWorkTables[8]='NOT_ARTI'
    this.cWorkTables[9]='KEY_ARTI'
    this.cWorkTables[10]='PRA_ENTI'
    this.cWorkTables[11]='CENCOST'
    this.cWorkTables[12]='CAUMATTI'
    this.cWorkTables[13]='TIP_DOCU'
    this.cWorkTables[14]='OFF_NOMI'
    this.cWorkTables[15]='DES_DIVE'
    this.cWorkTables[16]='CONTI'
    this.cWorkTables[17]='PRA_CONT'
    this.cWorkTables[18]='PAR_ALTE'
    this.cWorkTables[19]='BUSIUNIT'
    this.cWorkTables[20]='PRA_TIPI'
    this.cWorkTables[21]='RAP_PRES'
    * --- Area Manuale = Open Work Table
    * --- gsag_mpr
      * colora le righe collegate a spese\anticipazioni
       This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor= ;
       "Icase(NVL(t_FLSPAN,'N')='S' and NVL(t_DARIGPRE,'N')='N',RGB(255,128,128) ,NVL(t_DARIFPRE,0)=0 and NVL(t_DARIGPRE,'N')<>'S',RGB(255,255,255),RGB(MOD(214+iif(NVL(t_DARIGPRE,'N')='S',"+;
       "NVL(CPROWNUM,0),NVL(t_DARIFPRE,0))*10,255), 150, MOD(174+iif(NVL(t_DARIGPRE,'N')='S',NVL(CPROWNUM,0),NVL(t_DARIFPRE,0))*40,255)))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(21))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RAP_PRES_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RAP_PRES_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsag_mpr'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_2_15_joined
    link_2_15_joined=.f.
    local link_2_22_joined
    link_2_22_joined=.f.
    local link_2_27_joined
    link_2_27_joined=.f.
    local link_2_196_joined
    link_2_196_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from RAP_PRES where DACODRES=KeySet.DACODRES
    *                            and DAGRURES=KeySet.DAGRURES
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsag_mpr
      this.cKeyWhereODBCqualified=;
           '"RAP_PRES.DACODRES="+cp_ToStrODBC(this.w_DACODRES)';
            +'+" and RAP_PRES.DAGRURES="+cp_ToStrODBC(this.w_DAGRURES)'
         IF Not empty(this.oParentobject.w_codpratica)
           this.cKeyWhereODBCqualified=this.cKeyWhereODBCqualified+;
             +'+" AND RAP_PRES.PRNUMPRA="+cp_ToStrODBC(this.oParentobject.w_codpratica)'
         Endif
         IF Not empty(this.oParentobject.w_CODNIMIN)
           this.cKeyWhereODBCqualified=this.cKeyWhereODBCqualified+;
             +'+" AND RAP_PRES.DACODNOM="+cp_ToStrODBC(this.oParentobject.w_CODNIMIN)'
         Endif
         If Not empty(this.oParentobject.w_datini)
             this.cKeyWhereODBCqualified=this.cKeyWhereODBCqualified+;
             +'+" AND RAP_PRES.PR__DATA>="+cp_ToStrODBC(this.oParentobject.w_DATINI)'
         Endif
         If Not empty(this.oParentobject.w_datfin)
           this.cKeyWhereODBCqualified=this.cKeyWhereODBCqualified+;
            +'+" AND RAP_PRES.PR__DATA<="+cp_ToStrODBC(this.oParentobject.w_DATFIN)'
         endif
           
      IF NOT EMPTY(this.oParentobject.w_CODSER)
         IF IsAhe()
           this.cKeyWhereODBCqualified=this.cKeyWhereODBCqualified+;
           +'+" AND RAP_PRES.DACODICE like"+cp_ToStrODBC("%"+ALLTRIM(this.oParentobject.w_CODSER)+"%")'
         ELSE
           this.cKeyWhereODBCqualified=this.cKeyWhereODBCqualified+;
           +'+" AND RAP_PRES.DACODATT like"+cp_ToStrODBC("%"+ALLTRIM(this.oParentobject.w_CODSER)+"%")'
         ENDIF
      ENDIF 
      IF NOT EMPTY(this.oParentobject.w_DESSER)
         this.cKeyWhereODBCqualified=this.cKeyWhereODBCqualified+;
         +'+" AND RAP_PRES.PRDESPRE like "+cp_ToStrODBC("%"+ALLTRIM(this.oParentobject.w_DESSER)+"%")'
      ENDIF 
      IF NOT EMPTY(this.oParentobject.w_Ordinamento) AND this.oParentobject.w_Ordinamento<>'N'
         DO CASE
           CASE this.oParentobject.w_Ordinamento='P'
             * Ordinamento per pratica
             i_cOrder = 'order by RAP_PRES.PRNUMPRA,RAP_PRES.PR__DATA,RAP_PRES.PRCODVAL,RAP_PRES.PRCODLIS'
           CASE this.oParentobject.w_Ordinamento='D'
             * Ordinamento per data
             i_cOrder = 'order by RAP_PRES.PRDESPRE,RAP_PRES.DACODATT,RAP_PRES.PR__DATA,RAP_PRES.PRNUMPRA,RAP_PRES.PRCODVAL,RAP_PRES.PRCODLIS'
           OTHERWISE
             * Ordinamento per prestazione
             i_cOrder = 'order by RAP_PRES.DACODATT,RAP_PRES.PR__DATA,RAP_PRES.PRNUMPRA,RAP_PRES.PRCODVAL,RAP_PRES.PRCODLIS'
         ENDCASE   
      ENDIF 
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.RAP_PRES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAP_PRES_IDX,2],this.bLoadRecFilter,this.RAP_PRES_IDX,"gsag_mpr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RAP_PRES')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RAP_PRES.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RAP_PRES '
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_15_joined=this.AddJoinedLink_2_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_22_joined=this.AddJoinedLink_2_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_27_joined=this.AddJoinedLink_2_27(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_196_joined=this.AddJoinedLink_2_196(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DACODRES',this.w_DACODRES  ,'DAGRURES',this.w_DAGRURES  )
      select * from (i_cTable) RAP_PRES where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ISAHE = isahe()
        .w_KEYLISTB = This.oparentobject .w_KEYLISTB
        .w_NOEDDES = space(1)
        .w_APPOAZI = i_codazi
        .w_CODLIS = space(5)
        .w_COST_ORA = 0
        .w_CAUATT = space(20)
        .w_VALPAR = space(3)
        .w_CodPratica = space(20)
        .w_READAZI = i_CODAZI
        .w_CAUSALE = space(20)
        .w_FLQRIO = space(1)
        .w_FLPREV = space(1)
        .w_LISACQ = space(5)
        .w_CODLIN = space(3)
        .w_DATINI = this.oParentObject.w_DATINI
        .w_DATFIN = this.oParentObject.w_DATFIN
        .w_CODSER = this.oParentObject.w_CODSER
        .w_DESDSER = this.oParentObject.w_DESSER
        .w_EditSelez = 'S'
        .w_OB_TEST = i_Datsys
        .w_CODBUN = g_CODBUN
        .w_BUFLANAL = space(1)
        .w_DASERIAL = space(20)
        .w_FL_BUP = 'N'
        .w_DESEL_BUP = 0
        .w_CENRIC = space(15)
        .w_TOTDURQTA = 0
        .w_TOTDURQTAM = 0
        .w_TOTALORE = 0
        .w_TOTALMIN = 0
        .w_DATMAX = ctod("  /  /  ")
        .w_FLGZER = space(1)
        .w_GENPRE = space(10)
        .w_DTIPRIG = this.oParentObject.w_DTIPRIG
        .w_VISPRESTAZ = space(1)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_FLGLIS = 'N'
        .w_DACODRES = NVL(DACODRES,space(5))
        .w_MVCODVAL = g_PERVAL
        .w_LetturaParAlte = i_CodAzi
          .link_1_7('Load')
        .w_CODRES = This.oparentobject .w_CODRESP
          .link_1_9('Load')
        .w_DAGRURES = NVL(DAGRURES,space(5))
        .w_CAUACQ = This.oparentobject .w_CAUACQ
        .w_CAUDOC = This.oparentobject .w_CAUDOC
          .link_1_13('Load')
          .link_1_14('Load')
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
          .link_1_35('Load')
          .link_1_36('Load')
        .w_OLDCOM = iif(Not Empty(.w_PRNUMPRA),.w_PRNUMPRA,.w_DACODCOM)
        .w_PRZVAC = This.oparentobject .w_PRZVAC
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .w_GRUPART = This.oparentobject .w_GRUPART
        .w_DATLIS = This.oparentobject .w_DATLIS
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .w_MVFLVEAC = this.oparentobject.w_MVFLVEAC
        .w_TIPRIS = This.oparentobject .w_DPTIPRIS
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .w_FLACQ = IIF(Not Empty(.w_CAUACQ),Docgesana(.w_CAUACQ,'A'),.w_FLANAL)
        .w_LISACQ = This.oparentobject .w_LISACQ
        .w_FLACOMAQ = Docgesana(.w_CAUACQ,'C')
        .w_FLANAL = This.oparentobject .w_FLANAL
        .w_FLGCOM = This.oparentobject .w_FLGCOM
        .w_FLDANA = Docgesana(.w_CAUDOC,'A')
        .w_EDITCODPRA = This.oparentobject .w_EDITCODPRA
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
          .link_1_70('Load')
        .w_FLCCRAUTO = This.oparentobject .w_FLCCRAUTO
        .w_DATALIST = .w_PR__DATA
        .w_OLDCENRIC = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENCOS),.w_CENCOS,.w_DACENCOS)
        .w_CODCLI = .w_DACODNOM
        .w_TOTDURMIN = MOD(.w_TOTALMIN,60)
        .w_TOTDURORE = .w_TOTALORE+INT(.w_TOTALMIN/60)
        .w_TOTQTAORE = .w_TOTDURQTA+INT(.w_TOTDURQTAM/60)
        .w_TOTQTAMIN = Mod(.w_TOTDURQTAM,60)
        .w_NUMSCO = this.oparentobject.w_NUMSCO
        .w_FLDTRP = This.oparentobject .w_FLDTRP
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        cp_LoadRecExtFlds(this,'RAP_PRES')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTDURQTA = 0
      this.w_TOTDURQTAM = 0
      this.w_TOTALORE = 0
      this.w_TOTALMIN = 0
      scan
        with this
          .w_DCODCEN = space(15)
          .w_ECOD1ATT = space(20)
          .w_RCOD1ATT = space(20)
          .w_ECACODART = space(20)
          .w_RCACODART = space(20)
          .w_ARTIPRIG = space(1)
          .w_ARTIPRI2 = space(1)
          .w_NOTIFICA = space(1)
          .w_NUMLIS = space(5)
          .w_NCODLIS = space(5)
          .w_DECTOT = 0
          .w_DECUNI = 0
          .w_LICOSTO = 0
          .w_TIPART = space(2)
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_FLSERG = space(1)
          .w_DESUNI = space(35)
          .w_NOMEPRA = space(100)
          .w_UNIMIS = space(3)
          .w_LCODVAL = space(3)
          .w_VALCOM = space(3)
          .w_EUNMIS3 = space(3)
          .w_RUNMIS3 = space(3)
        .w_OBTEST = i_datsys
          .w_VALLIS = space(3)
          .w_FLAPON = space(1)
          .w_ENTE = space(10)
          .w_UFFICI = space(10)
          .w_FLVALO = space(1)
          .w_IMPORT = 0
          .w_CALDIR = space(1)
          .w_COECAL = 0
          .w_PARASS = 0
          .w_FLAMPA = space(1)
          .w_DATOBSO = ctod("  /  /  ")
          .w_TIPOENTE = space(1)
          .w_CHKTEMP = space(1)
          .w_DUR_ORE = 0
          .w_ECADTOBSO = ctod("  /  /  ")
          .w_RCADTOBSO = ctod("  /  /  ")
          .w_NOTIPCLI = space(1)
          .w_DTOBSNOM = ctod("  /  /  ")
          .w_NOCODCLI = space(15)
          .w_NOTIPNOM = space(1)
        .w_OBTEST = i_datsys
          .w_DATOBSO = ctod("  /  /  ")
        .w_OFDATDOC = i_DatSys
          .w_EMOLTI3 = 0
          .w_RMOLTI3 = 0
          .w_FLUSEP = space(1)
          .w_OPERAT = space(1)
          .w_FLFRAZ1 = space(1)
          .w_MODUM2 = space(1)
          .w_MOLTIP = 0
          .w_PREZUM = space(1)
          .w_ARTIPART = space(2)
          .w_SCOLIS = space(1)
          .w_CCODLIS = space(5)
        .w_CICLO = 'E'
          .w_EOPERA3 = space(1)
          .w_ROPERA3 = space(1)
          .w_ETIPSER = space(1)
          .w_RTIPSER = space(1)
          .w_FL_FRAZ = space(1)
          .w_VALACQ = space(3)
          .w_INIACQ = ctod("  /  /  ")
          .w_TIPACQ = space(1)
          .w_FLSACQ = space(1)
          .w_IVAACQ = space(1)
          .w_FLGACQ = space(1)
          .w_FINACQ = ctod("  /  /  ")
          .w_FLSCOR = space(1)
          .w_TARTEM = space(1)
          .w_TARCON = 0
          .w_FLVALO1 = space(1)
          .w_CONTRACN = space(5)
          .w_TIPCONCO = space(1)
          .w_LISCOLCO = space(5)
          .w_STATUSCO = space(1)
        .w_FLCOMP = 'S'
          .w_FLSPAN = space(1)
          .w_PRESTA = space(1)
          .w_CEN_CauDoc = space(15)
          .w_FLUNIV = space(1)
          .w_NUMPRE = 0
          .w_LOCALI = space(30)
          .w_FLSCOAC = space(1)
          .w_CNDATFIN = ctod("  /  /  ")
          .w_CATCOM = space(3)
          .w_TIPRIGCA = space(1)
          .w_TIPRI2CA = space(1)
          .w_CNTIPPRA = space(10)
          .w_CNMATOBB = space(1)
          .w_CNASSCTP = space(1)
          .w_CNCOMPLX = space(1)
          .w_CNPROFMT = space(1)
          .w_CNESIPOS = space(1)
          .w_CNPERPLX = 0
          .w_CNPERPOS = 0
          .w_CNFLVMLQ = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_PR__DATA = NVL(cp_ToDate(PR__DATA),ctod("  /  /  "))
        .w_CENCOS = This.oparentobject .w_CENCOS
        .w_OLDCEN = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENCOS),.w_CENCOS,.w_DACENCOS)
          .w_DACODICE = NVL(DACODICE,space(41))
          if link_2_6_joined
            this.w_DACODICE = NVL(CACODICE206,NVL(this.w_DACODICE,space(41)))
            this.w_PRDESPRE = NVL(CADESART206,space(40))
            this.w_PRDESAGG = NVL(CADESSUP206,space(0))
            this.w_ECOD1ATT = NVL(CACODART206,space(20))
            this.w_EUNMIS3 = NVL(CAUNIMIS206,space(3))
            this.w_ECACODART = NVL(CACODART206,space(20))
            this.w_ECADTOBSO = NVL(cp_ToDate(CADTOBSO206),ctod("  /  /  "))
            this.w_ETIPSER = NVL(CA__TIPO206,space(1))
            this.w_EOPERA3 = NVL(CAOPERAT206,space(1))
            this.w_EMOLTI3 = NVL(CAMOLTIP206,0)
          else
          .link_2_6('Load')
          endif
          .w_PRNUMPRA = NVL(PRNUMPRA,space(15))
          if link_2_7_joined
            this.w_PRNUMPRA = NVL(CNCODCAN207,NVL(this.w_PRNUMPRA,space(15)))
            this.w_NOMEPRA = NVL(CNDESCAN207,space(100))
            this.w_LCODVAL = NVL(CNCODVAL207,space(3))
            this.w_VALCOM = NVL(CNCODVAL207,space(3))
            this.w_CCODLIS = NVL(CNCODLIS207,space(5))
            this.w_FLAPON = NVL(CNFLAPON207,space(1))
            this.w_ENTE = NVL(CN__ENTE207,space(10))
            this.w_UFFICI = NVL(CNUFFICI207,space(10))
            this.w_FLVALO = NVL(CNFLVALO207,space(1))
            this.w_IMPORT = NVL(CNIMPORT207,0)
            this.w_CALDIR = NVL(CNCALDIR207,space(1))
            this.w_COECAL = NVL(CNCOECAL207,0)
            this.w_PARASS = NVL(CNPARASS207,0)
            this.w_FLAMPA = NVL(CNFLAMPA207,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CNDTOBSO207),ctod("  /  /  "))
            this.w_TARTEM = NVL(CNTARTEM207,space(1))
            this.w_TARCON = NVL(CNTARCON207,0)
            this.w_FLVALO1 = NVL(CNFLDIND207,space(1))
            this.w_CONTRACN = NVL(CNCONTRA207,space(5))
            this.w_LOCALI = NVL(CNLOCALI207,space(30))
            this.w_CNDATFIN = NVL(cp_ToDate(CNDATFIN207),ctod("  /  /  "))
            this.w_CNTIPPRA = NVL(CNTIPPRA207,space(10))
            this.w_CNMATOBB = NVL(CNMATOBB207,space(1))
            this.w_CNASSCTP = NVL(CNASSCTP207,space(1))
            this.w_CNCOMPLX = NVL(CNCOMPLX207,space(1))
            this.w_CNPROFMT = NVL(CNPROFMT207,space(1))
            this.w_CNESIPOS = NVL(CNESIPOS207,space(1))
            this.w_CNPERPLX = NVL(CNPERPLX207,0)
            this.w_CNPERPOS = NVL(CNPERPOS207,0)
            this.w_CNFLVMLQ = NVL(CNFLVMLQ207,space(1))
          else
          .link_2_7('Load')
          endif
          .w_DATIPRIG = NVL(DATIPRIG,space(1))
          .w_DATIPRI2 = NVL(DATIPRI2,space(1))
          .w_DACODATT = NVL(DACODATT,space(20))
          if link_2_15_joined
            this.w_DACODATT = NVL(CACODICE215,NVL(this.w_DACODATT,space(20)))
            this.w_PRDESPRE = NVL(CADESART215,space(40))
            this.w_PRDESAGG = NVL(CADESSUP215,space(0))
            this.w_RCOD1ATT = NVL(CACODART215,space(20))
            this.w_RUNMIS3 = NVL(CAUNIMIS215,space(3))
            this.w_RCACODART = NVL(CACODART215,space(20))
            this.w_RCADTOBSO = NVL(cp_ToDate(CADTOBSO215),ctod("  /  /  "))
            this.w_RTIPSER = NVL(CA__TIPO215,space(1))
            this.w_ROPERA3 = NVL(CAOPERAT215,space(1))
            this.w_RMOLTI3 = NVL(CAMOLTIP215,0)
          else
          .link_2_15('Load')
          endif
        .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
        .w_COD1ATT = IIF(.w_ISAHE,.w_ECOD1ATT,.w_RCOD1ATT)
          .w_PRDESPRE = NVL(PRDESPRE,space(40))
          .w_PRUNIMIS = NVL(PRUNIMIS,space(3))
          if link_2_22_joined
            this.w_PRUNIMIS = NVL(UMCODICE222,NVL(this.w_PRUNIMIS,space(3)))
            this.w_DESUNI = NVL(UMDESCRI222,space(35))
            this.w_CHKTEMP = NVL(UMFLTEMP222,space(1))
            this.w_DUR_ORE = NVL(UMDURORE222,0)
            this.w_FL_FRAZ = NVL(UMFLFRAZ222,space(1))
          else
          .link_2_22('Load')
          endif
          .w_PRQTAMOV = NVL(PRQTAMOV,0)
          .w_DAPREZZO = NVL(DAPREZZO,0)
          .w_PRCODVAL = NVL(PRCODVAL,space(3))
          if link_2_27_joined
            this.w_PRCODVAL = NVL(VACODVAL227,NVL(this.w_PRCODVAL,space(3)))
            this.w_DECTOT = NVL(VADECTOT227,0)
            this.w_DECUNI = NVL(VADECUNI227,0)
          else
          .link_2_27('Load')
          endif
          .w_PRCODLIS = NVL(PRCODLIS,space(5))
          .link_2_28('Load')
        .w_ATIPRIG = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N', Not empty(.w_DATIPRIG),.w_DATIPRIG,LEFT(ALLTRIM(.w_ARTIPRIG)+'D',1))
        .w_ATIPRI2 = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', Not empty(.w_DATIPRI2),.w_DATIPRI2,LEFT(ALLTRIM(.w_ARTIPRI2)+'D',1))
        .w_ETIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_ETIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,'D')
          .w_DACODSED = NVL(DACODSED,space(5))
          * evitabile
          *.link_2_35('Load')
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
          .w_PRDESAGG = NVL(PRDESAGG,space(0))
          .w_PRCODOPE = NVL(PRCODOPE,0)
          .w_PRDATMOD = NVL(cp_ToDate(PRDATMOD),ctod("  /  /  "))
          .w_PRVALRIG = NVL(PRVALRIG,0)
          .w_PROREEFF = NVL(PROREEFF,0)
          .w_PRMINEFF = NVL(PRMINEFF,0)
          .w_DACOSUNI = NVL(DACOSUNI,0)
          .w_PRCOSINT = NVL(PRCOSINT,0)
          .w_DAFLDEFF = NVL(DAFLDEFF,space(1))
          .w_DAPREMIN = NVL(DAPREMIN,0)
          .w_DAPREMAX = NVL(DAPREMAX,0)
          .w_DAGAZUFF = NVL(DAGAZUFF,space(6))
        .w_UNMIS3 = IIF(.w_ISAHE,.w_EUNMIS3,.w_RUNMIS3)
          .link_2_68('Load')
        .w_CAOVAL = IIF(.w_CAOVAL=1 OR .w_CAOVAL=0 OR .w_PRCODVAL=g_CODEUR, GETCAM(.w_PRCODVAL, .w_PR__DATA, 7),.w_CAOVAL)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_79.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_80.Calculate()
        .w_CADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
          .w_DACODCOM = NVL(DACODCOM,space(15))
          .w_DAVOCRIC = NVL(DAVOCRIC,space(15))
          .w_DAINICOM = NVL(cp_ToDate(DAINICOM),ctod("  /  /  "))
          .w_DAFINCOM = NVL(cp_ToDate(DAFINCOM),ctod("  /  /  "))
          .w_DA_SEGNO = NVL(DA_SEGNO,space(1))
          .w_DACENCOS = NVL(DACENCOS,space(15))
          .w_DAATTIVI = NVL(DAATTIVI,space(15))
          .link_2_97('Load')
        .w_QTAUM1 = IIF(.w_DATIPRIG='F', 1, CALQTA(.w_PRQTAMOV,.w_PRUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ' ,.w_UNMIS3, .w_OPERA3, .w_MOLTI3))
        .w_MOLTI3 = IIF(.w_ISAHE,.w_EMOLTI3,.w_RMOLTI3)
          .w_DAVOCCOS = NVL(DAVOCCOS,space(15))
        .w_OPERA3 = IIF(.w_ISAHE,.w_EOPERA3,.w_ROPERA3)
        .w_TIPSER = IIF(.w_ISAHE,.w_ETIPSER,.w_RTIPSER)
          .w_DACODLIS = NVL(DACODLIS,space(5))
          .w_DAPROLIS = NVL(DAPROLIS,space(5))
          .w_DAPROSCO = NVL(DAPROSCO,space(5))
          .w_DASCOLIS = NVL(DASCOLIS,space(5))
          .w_DASCONT1 = NVL(DASCONT1,0)
          .w_DASCONT2 = NVL(DASCONT2,0)
          .w_DASCONT3 = NVL(DASCONT3,0)
          .w_DASCONT4 = NVL(DASCONT4,0)
          .w_DACOSUNI = NVL(DACOSUNI,0)
          .w_DACOMRIC = NVL(DACOMRIC,space(15))
          .w_DAATTRIC = NVL(DAATTRIC,space(15))
          .w_DACENRIC = NVL(DACENRIC,space(15))
          .w_DALISACQ = NVL(DALISACQ,space(5))
          .w_DATCOINI = NVL(cp_ToDate(DATCOINI),ctod("  /  /  "))
          .w_DATCOFIN = NVL(cp_ToDate(DATCOFIN),ctod("  /  /  "))
          .w_DATRIINI = NVL(cp_ToDate(DATRIINI),ctod("  /  /  "))
          .w_DATRIFIN = NVL(cp_ToDate(DATRIFIN),ctod("  /  /  "))
        .w_VOCECR = Docgesana(.w_CAUDOC,'V')
        .w_TOTQTA = iif(.w_CHKTEMP='S',INT(.w_PRQTAMOV*.w_DUR_ORE),0)  
        .w_TOTORE = INT(.w_PROREEFF)
        .w_TOTMIN = .w_PRMINEFF
        .w_TOTQTAM = IIF(.w_CHKTEMP='S', INT(cp_round((.w_DUR_ORE * .w_PRQTAMOV - INT(.w_DUR_ORE * .w_PRQTAMOV)) * 60,0)), 0)
          .link_2_160('Load')
          .w_DARIFPRE = NVL(DARIFPRE,0)
          .w_DARIGPRE = NVL(DARIGPRE,space(1))
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_170.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_171.Calculate()
          .w_DACONCOD = NVL(DACONCOD,space(15))
          .w_DACONTRA = NVL(DACONTRA,space(10))
          .w_DACODMOD = NVL(DACODMOD,space(10))
          .w_DACODIMP = NVL(DACODIMP,space(10))
          .w_DACOCOMP = NVL(DACOCOMP,0)
          .w_DARINNOV = NVL(DARINNOV,0)
        .w_DAUNIMIS = .w_PRUNIMIS
        .w_DADESATT = .w_PRDESPRE
          .w_DALISACQ = NVL(DALISACQ,space(5))
          .w_DACODNOM = NVL(DACODNOM,space(15))
          if link_2_196_joined
            this.w_DACODNOM = NVL(NOCODICE396,NVL(this.w_DACODNOM,space(15)))
            this.w_NOCODCLI = NVL(NOCODCLI396,space(15))
            this.w_DTOBSNOM = NVL(cp_ToDate(NODTOBSO396),ctod("  /  /  "))
            this.w_NOTIPNOM = NVL(NOTIPNOM396,space(1))
            this.w_NOTIPCLI = NVL(NOTIPCLI396,space(1))
            this.w_CODLIN = NVL(NOCODLIN396,space(3))
            this.w_NCODLIS = NVL(NONUMLIS396,space(5))
          else
          .link_2_196('Load')
          endif
          .w_PRCODSPE = NVL(PRCODSPE,space(20))
          .w_PRCODANT = NVL(PRCODANT,space(20))
          .w_PRIMPANT = NVL(PRIMPANT,0)
          .w_PRIMPSPE = NVL(PRIMPSPE,0)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_212.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTDURQTA = .w_TOTDURQTA+.w_TOTQTA
          .w_TOTDURQTAM = .w_TOTDURQTAM+.w_TOTQTAM
          .w_TOTALORE = .w_TOTALORE+.w_TOTORE
          .w_TOTALMIN = .w_TOTALMIN+.w_TOTMIN
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_FLGLIS = 'N'
        .w_MVCODVAL = g_PERVAL
        .w_LetturaParAlte = i_CodAzi
        .w_CODRES = This.oparentobject .w_CODRESP
        .w_CAUACQ = This.oparentobject .w_CAUACQ
        .w_CAUDOC = This.oparentobject .w_CAUDOC
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .w_OLDCOM = iif(Not Empty(.w_PRNUMPRA),.w_PRNUMPRA,.w_DACODCOM)
        .w_PRZVAC = This.oparentobject .w_PRZVAC
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .w_GRUPART = This.oparentobject .w_GRUPART
        .w_DATLIS = This.oparentobject .w_DATLIS
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .w_MVFLVEAC = this.oparentobject.w_MVFLVEAC
        .w_TIPRIS = This.oparentobject .w_DPTIPRIS
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .w_FLACQ = IIF(Not Empty(.w_CAUACQ),Docgesana(.w_CAUACQ,'A'),.w_FLANAL)
        .w_LISACQ = This.oparentobject .w_LISACQ
        .w_FLACOMAQ = Docgesana(.w_CAUACQ,'C')
        .w_FLANAL = This.oparentobject .w_FLANAL
        .w_FLGCOM = This.oparentobject .w_FLGCOM
        .w_FLDANA = Docgesana(.w_CAUDOC,'A')
        .w_EDITCODPRA = This.oparentobject .w_EDITCODPRA
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .w_FLCCRAUTO = This.oparentobject .w_FLCCRAUTO
        .w_TOTDURMIN = MOD(.w_TOTALMIN,60)
        .w_TOTDURORE = .w_TOTALORE+INT(.w_TOTALMIN/60)
        .w_TOTQTAORE = .w_TOTDURQTA+INT(.w_TOTDURQTAM/60)
        .w_TOTQTAMIN = Mod(.w_TOTDURQTAM,60)
        .w_NUMSCO = this.oparentobject.w_NUMSCO
        .w_FLDTRP = This.oparentobject .w_FLDTRP
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_64.enabled = .oPgFrm.Page1.oPag.oBtn_1_64.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_65.enabled = .oPgFrm.Page1.oPag.oBtn_1_65.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_93.enabled = .oPgFrm.Page1.oPag.oBtn_2_93.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
    this.Calculate_VYQPPPBUZV()
    this.Calculate_UEVIUTMRUX()
    this.Calculate_EVQATLEGXV()
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_ISAHE=.f.
      .w_FLGLIS=space(1)
      .w_DACODRES=space(5)
      .w_KEYLISTB=space(10)
      .w_MVCODVAL=space(3)
      .w_LetturaParAlte=space(5)
      .w_NOEDDES=space(1)
      .w_CODRES=space(5)
      .w_DAGRURES=space(5)
      .w_CAUACQ=space(10)
      .w_CAUDOC=space(5)
      .w_APPOAZI=space(5)
      .w_CODLIS=space(5)
      .w_COST_ORA=0
      .w_CAUATT=space(20)
      .w_VALPAR=space(3)
      .w_CodPratica=space(20)
      .w_READAZI=space(10)
      .w_CAUSALE=space(20)
      .w_OLDCOM=space(15)
      .w_FLQRIO=space(1)
      .w_FLPREV=space(1)
      .w_PRZVAC=space(1)
      .w_GRUPART=space(5)
      .w_DATLIS=ctod("  /  /  ")
      .w_MVFLVEAC=space(3)
      .w_TIPRIS=space(1)
      .w_LISACQ=space(5)
      .w_FLACQ=space(1)
      .w_LISACQ=space(5)
      .w_FLACOMAQ=space(10)
      .w_FLANAL=space(1)
      .w_FLGCOM=space(1)
      .w_FLDANA=space(1)
      .w_CODLIN=space(3)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CODSER=space(20)
      .w_DESDSER=space(40)
      .w_EditSelez=space(1)
      .w_EDITCODPRA=.f.
      .w_OB_TEST=ctod("  /  /  ")
      .w_CODBUN=space(10)
      .w_BUFLANAL=space(1)
      .w_DASERIAL=space(20)
      .w_FL_BUP=space(1)
      .w_DESEL_BUP=0
      .w_FLCCRAUTO=space(10)
      .w_PR__DATA=ctod("  /  /  ")
      .w_DATALIST=ctod("  /  /  ")
      .w_CENCOS=space(15)
      .w_DCODCEN=space(15)
      .w_OLDCEN=space(15)
      .w_DACODICE=space(41)
      .w_PRNUMPRA=space(15)
      .w_ECOD1ATT=space(20)
      .w_RCOD1ATT=space(20)
      .w_DATIPRIG=space(1)
      .w_DATIPRI2=space(1)
      .w_ECACODART=space(20)
      .w_RCACODART=space(20)
      .w_DACODATT=space(20)
      .w_CACODART=space(20)
      .w_ARTIPRIG=space(1)
      .w_ARTIPRI2=space(1)
      .w_COD1ATT=space(20)
      .w_NOTIFICA=space(1)
      .w_PRDESPRE=space(40)
      .w_PRUNIMIS=space(3)
      .w_NUMLIS=space(5)
      .w_PRQTAMOV=0
      .w_NCODLIS=space(5)
      .w_DAPREZZO=0
      .w_PRCODVAL=space(3)
      .w_PRCODLIS=space(5)
      .w_ATIPRIG=space(10)
      .w_ATIPRI2=space(10)
      .w_ETIPRIG=space(10)
      .w_ETIPRI2=space(10)
      .w_RTIPRIG=space(10)
      .w_RTIPRI2=space(10)
      .w_DACODSED=space(5)
      .w_DECTOT=0
      .w_DECUNI=0
      .w_CALCPICU=0
      .w_CALCPICT=0
      .w_PRDESAGG=space(0)
      .w_PRCODOPE=0
      .w_PRDATMOD=ctod("  /  /  ")
      .w_PRVALRIG=0
      .w_LICOSTO=0
      .w_PROREEFF=0
      .w_PRMINEFF=0
      .w_DACOSUNI=0
      .w_PRCOSINT=0
      .w_DAFLDEFF=space(1)
      .w_DAPREMIN=0
      .w_DAPREMAX=0
      .w_DAGAZUFF=space(6)
      .w_TIPART=space(2)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_FLSERG=space(1)
      .w_DESUNI=space(35)
      .w_NOMEPRA=space(100)
      .w_UNIMIS=space(3)
      .w_LCODVAL=space(3)
      .w_VALCOM=space(3)
      .w_EUNMIS3=space(3)
      .w_RUNMIS3=space(3)
      .w_OBTEST=ctod("  /  /  ")
      .w_UNMIS3=space(3)
      .w_VALLIS=space(3)
      .w_FLAPON=space(1)
      .w_ENTE=space(10)
      .w_UFFICI=space(10)
      .w_FLVALO=space(1)
      .w_IMPORT=0
      .w_CALDIR=space(1)
      .w_COECAL=0
      .w_PARASS=0
      .w_FLAMPA=space(1)
      .w_CAOVAL=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPOENTE=space(1)
      .w_CHKTEMP=space(1)
      .w_DUR_ORE=0
      .w_ECADTOBSO=ctod("  /  /  ")
      .w_RCADTOBSO=ctod("  /  /  ")
      .w_CADTOBSO=ctod("  /  /  ")
      .w_DACODCOM=space(15)
      .w_DAVOCRIC=space(15)
      .w_DAINICOM=ctod("  /  /  ")
      .w_DAFINCOM=ctod("  /  /  ")
      .w_DA_SEGNO=space(1)
      .w_DACENCOS=space(15)
      .w_DAATTIVI=space(15)
      .w_NOTIPCLI=space(1)
      .w_DTOBSNOM=ctod("  /  /  ")
      .w_NOCODCLI=space(15)
      .w_NOTIPNOM=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_OFDATDOC=ctod("  /  /  ")
      .w_EMOLTI3=0
      .w_RMOLTI3=0
      .w_QTAUM1=0
      .w_MOLTI3=0
      .w_FLUSEP=space(1)
      .w_OPERAT=space(1)
      .w_FLFRAZ1=space(1)
      .w_MODUM2=space(1)
      .w_MOLTIP=0
      .w_PREZUM=space(1)
      .w_ARTIPART=space(2)
      .w_SCOLIS=space(1)
      .w_DAVOCCOS=space(15)
      .w_CCODLIS=space(5)
      .w_CICLO=space(1)
      .w_EOPERA3=space(1)
      .w_ROPERA3=space(1)
      .w_OPERA3=space(1)
      .w_ETIPSER=space(1)
      .w_RTIPSER=space(1)
      .w_TIPSER=space(1)
      .w_DACODLIS=space(5)
      .w_DAPROLIS=space(5)
      .w_DAPROSCO=space(5)
      .w_DASCOLIS=space(5)
      .w_DASCONT1=0
      .w_DASCONT2=0
      .w_DASCONT3=0
      .w_DASCONT4=0
      .w_DACOSUNI=0
      .w_DACOMRIC=space(15)
      .w_DAATTRIC=space(15)
      .w_DACENRIC=space(15)
      .w_FL_FRAZ=space(1)
      .w_DALISACQ=space(5)
      .w_VALACQ=space(3)
      .w_INIACQ=ctod("  /  /  ")
      .w_TIPACQ=space(1)
      .w_FLSACQ=space(1)
      .w_IVAACQ=space(1)
      .w_FLGACQ=space(1)
      .w_FINACQ=ctod("  /  /  ")
      .w_DATCOINI=ctod("  /  /  ")
      .w_DATCOFIN=ctod("  /  /  ")
      .w_DATRIINI=ctod("  /  /  ")
      .w_DATRIFIN=ctod("  /  /  ")
      .w_FLSCOR=space(1)
      .w_TARTEM=space(1)
      .w_TARCON=0
      .w_FLVALO1=space(1)
      .w_VOCECR=space(1)
      .w_TOTQTA=0
      .w_TOTORE=0
      .w_TOTMIN=0
      .w_TOTQTAM=0
      .w_CONTRACN=space(5)
      .w_TIPCONCO=space(1)
      .w_LISCOLCO=space(5)
      .w_STATUSCO=space(1)
      .w_FLCOMP=space(10)
      .w_FLSPAN=space(1)
      .w_DARIFPRE=0
      .w_DARIGPRE=space(1)
      .w_PRESTA=space(1)
      .w_DACONCOD=space(15)
      .w_CEN_CauDoc=space(15)
      .w_OLDCENRIC=space(15)
      .w_CENRIC=space(15)
      .w_DACONTRA=space(10)
      .w_DACODMOD=space(10)
      .w_DACODIMP=space(10)
      .w_DACOCOMP=0
      .w_DARINNOV=0
      .w_DAUNIMIS=space(3)
      .w_CODCLI=space(15)
      .w_DADESATT=space(40)
      .w_FLUNIV=space(1)
      .w_NUMPRE=0
      .w_LOCALI=space(30)
      .w_TOTDURMIN=0
      .w_TOTDURQTA=0
      .w_TOTDURQTAM=0
      .w_TOTALORE=0
      .w_TOTALMIN=0
      .w_TOTDURORE=0
      .w_TOTQTAORE=0
      .w_TOTQTAMIN=0
      .w_NUMSCO=0
      .w_FLDTRP=space(10)
      .w_DATMAX=ctod("  /  /  ")
      .w_FLGZER=space(1)
      .w_DALISACQ=space(5)
      .w_FLSCOAC=space(1)
      .w_CNDATFIN=ctod("  /  /  ")
      .w_CATCOM=space(3)
      .w_DACODNOM=space(15)
      .w_PRCODSPE=space(20)
      .w_PRCODANT=space(20)
      .w_PRIMPANT=0
      .w_PRIMPSPE=0
      .w_GENPRE=space(10)
      .w_DTIPRIG=space(1)
      .w_TIPRIGCA=space(1)
      .w_TIPRI2CA=space(1)
      .w_VISPRESTAZ=space(1)
      .w_CNTIPPRA=space(10)
      .w_CNMATOBB=space(1)
      .w_CNASSCTP=space(1)
      .w_CNCOMPLX=space(1)
      .w_CNPROFMT=space(1)
      .w_CNESIPOS=space(1)
      .w_CNPERPLX=0
      .w_CNPERPOS=0
      .w_CNFLVMLQ=space(1)
      if .cFunction<>"Filter"
        .w_ISAHE = isahe()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_FLGLIS = 'N'
        .w_DACODRES = .w_CODRES
        .w_KEYLISTB = This.oparentobject .w_KEYLISTB
        .w_MVCODVAL = g_PERVAL
        .w_LetturaParAlte = i_CodAzi
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_LetturaParAlte))
         .link_1_7('Full')
        endif
        .DoRTCalc(7,7,.f.)
        .w_CODRES = This.oparentobject .w_CODRESP
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODRES))
         .link_1_9('Full')
        endif
        .w_DAGRURES = .w_GRUPART
        .w_CAUACQ = This.oparentobject .w_CAUACQ
        .w_CAUDOC = This.oparentobject .w_CAUDOC
        .w_APPOAZI = i_codazi
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_APPOAZI))
         .link_1_13('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODLIS))
         .link_1_14('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .DoRTCalc(14,17,.f.)
        .w_READAZI = i_CODAZI
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_READAZI))
         .link_1_35('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CAUSALE))
         .link_1_36('Full')
        endif
        .w_OLDCOM = iif(Not Empty(.w_PRNUMPRA),.w_PRNUMPRA,.w_DACODCOM)
        .DoRTCalc(21,22,.f.)
        .w_PRZVAC = This.oparentobject .w_PRZVAC
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .w_GRUPART = This.oparentobject .w_GRUPART
        .w_DATLIS = This.oparentobject .w_DATLIS
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .w_MVFLVEAC = this.oparentobject.w_MVFLVEAC
        .w_TIPRIS = This.oparentobject .w_DPTIPRIS
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .DoRTCalc(28,28,.f.)
        .w_FLACQ = IIF(Not Empty(.w_CAUACQ),Docgesana(.w_CAUACQ,'A'),.w_FLANAL)
        .w_LISACQ = This.oparentobject .w_LISACQ
        .w_FLACOMAQ = Docgesana(.w_CAUACQ,'C')
        .w_FLANAL = This.oparentobject .w_FLANAL
        .w_FLGCOM = This.oparentobject .w_FLGCOM
        .w_FLDANA = Docgesana(.w_CAUDOC,'A')
        .DoRTCalc(35,35,.f.)
        .w_DATINI = this.oParentObject.w_DATINI
        .w_DATFIN = this.oParentObject.w_DATFIN
        .w_CODSER = this.oParentObject.w_CODSER
        .w_DESDSER = this.oParentObject.w_DESSER
        .w_EditSelez = 'S'
        .w_EDITCODPRA = This.oparentobject .w_EDITCODPRA
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .w_OB_TEST = i_Datsys
        .w_CODBUN = g_CODBUN
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_CODBUN))
         .link_1_70('Full')
        endif
        .DoRTCalc(44,45,.f.)
        .w_FL_BUP = 'N'
        .w_DESEL_BUP = 0
        .w_FLCCRAUTO = This.oparentobject .w_FLCCRAUTO
        .w_PR__DATA = IIF(.w_FLDTRP='S',NVL(.w_DATMAX,i_datsys),i_datsys)
        .w_DATALIST = .w_PR__DATA
        .w_CENCOS = This.oparentobject .w_CENCOS
        .DoRTCalc(52,52,.f.)
        .w_OLDCEN = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENCOS),.w_CENCOS,.w_DACENCOS)
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_DACODICE))
         .link_2_6('Full')
        endif
        .w_PRNUMPRA = .w_CodPratica
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_PRNUMPRA))
         .link_2_7('Full')
        endif
        .DoRTCalc(56,62,.f.)
        if not(empty(.w_DACODATT))
         .link_2_15('Full')
        endif
        .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
        .DoRTCalc(64,65,.f.)
        .w_COD1ATT = IIF(.w_ISAHE,.w_ECOD1ATT,.w_RCOD1ATT)
        .DoRTCalc(67,69,.f.)
        if not(empty(.w_PRUNIMIS))
         .link_2_22('Full')
        endif
        .DoRTCalc(70,70,.f.)
        .w_PRQTAMOV = 1
        .DoRTCalc(72,73,.f.)
        .w_PRCODVAL = iif(Not empty(.w_LCODVAL),.w_LCODVAL,g_PERVAL)
        .DoRTCalc(74,74,.f.)
        if not(empty(.w_PRCODVAL))
         .link_2_27('Full')
        endif
        .w_PRCODLIS = .w_CCODLIS
        .DoRTCalc(75,75,.f.)
        if not(empty(.w_PRCODLIS))
         .link_2_28('Full')
        endif
        .w_ATIPRIG = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N', Not empty(.w_DATIPRIG),.w_DATIPRIG,LEFT(ALLTRIM(.w_ARTIPRIG)+'D',1))
        .w_ATIPRI2 = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', Not empty(.w_DATIPRI2),.w_DATIPRI2,LEFT(ALLTRIM(.w_ARTIPRI2)+'D',1))
        .w_ETIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_ETIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,'D')
        .w_DACODSED = SPACE(5)
        .DoRTCalc(82,82,.f.)
        if not(empty(.w_DACODSED))
         .link_2_35('Full')
        endif
        .DoRTCalc(83,84,.f.)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(87,87,.f.)
        .w_PRCODOPE = i_codute
        .w_PRDATMOD = i_datsys
        .w_PRVALRIG = cp_round(.w_PRQTAMOV*.w_DAPREZZO, IIF(.w_NOTIFICA='S' , 0, .w_DECTOT))
        .DoRTCalc(91,91,.f.)
        .w_PROREEFF = min(IIF(.w_CHKTEMP='S', INT(.w_DUR_ORE * .w_PRQTAMOV), 0),999)
        .w_PRMINEFF = IIF(.w_CHKTEMP='S', INT(cp_round((.w_DUR_ORE * .w_PRQTAMOV - INT(.w_DUR_ORE * .w_PRQTAMOV)) * 60,0)), 0)
        .DoRTCalc(94,94,.f.)
        .w_PRCOSINT = IIF(.w_LICOSTO>0 and .w_COST_ORA=0 ,.w_DACOSUNI*.w_PRQTAMOV,(.w_PROREEFF+(.w_PRMINEFF/60))*.w_DACOSUNI)
        .w_DAFLDEFF = "N"
        .DoRTCalc(97,110,.f.)
        .w_OBTEST = i_datsys
        .w_UNMIS3 = IIF(.w_ISAHE,.w_EUNMIS3,.w_RUNMIS3)
        .DoRTCalc(113,115,.f.)
        if not(empty(.w_ENTE))
         .link_2_68('Full')
        endif
        .DoRTCalc(116,122,.f.)
        .w_CAOVAL = IIF(.w_CAOVAL=1 OR .w_CAOVAL=0 OR .w_PRCODVAL=g_CODEUR, GETCAM(.w_PRCODVAL, .w_PR__DATA, 7),.w_CAOVAL)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_79.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_80.Calculate()
        .DoRTCalc(124,129,.f.)
        .w_CADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
        .w_DACODCOM = .w_PRNUMPRA
        .w_DAVOCRIC = iif(Not empty(.w_COD1ATT) ,SearchVoc(this,'R',.w_COD1ATT,.w_NOTIPCLI,.w_NOCODCLI,.w_CAUDOC,.w_DACODNOM),SPACE(15))
        .DoRTCalc(133,134,.f.)
        .w_DA_SEGNO = 'D'
        .DoRTCalc(136,136,.f.)
        .w_DAATTIVI = space(15)
        .DoRTCalc(138,140,.f.)
        if not(empty(.w_NOCODCLI))
         .link_2_97('Full')
        endif
        .DoRTCalc(141,141,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(143,143,.f.)
        .w_OFDATDOC = i_DatSys
        .DoRTCalc(145,146,.f.)
        .w_QTAUM1 = IIF(.w_DATIPRIG='F', 1, CALQTA(.w_PRQTAMOV,.w_PRUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ' ,.w_UNMIS3, .w_OPERA3, .w_MOLTI3))
        .w_MOLTI3 = IIF(.w_ISAHE,.w_EMOLTI3,.w_RMOLTI3)
        .DoRTCalc(149,156,.f.)
        .w_DAVOCCOS = iif(Not empty(.w_COD1ATT) ,SearchVoc(this,'C',.w_COD1ATT,.w_NOTIPCLI,.w_NOCODCLI,.w_CAUDOC,.w_DACODNOM),SPACE(15))
        .DoRTCalc(158,158,.f.)
        .w_CICLO = 'E'
        .DoRTCalc(160,161,.f.)
        .w_OPERA3 = IIF(.w_ISAHE,.w_EOPERA3,.w_ROPERA3)
        .DoRTCalc(163,164,.f.)
        .w_TIPSER = IIF(.w_ISAHE,.w_ETIPSER,.w_RTIPSER)
        .DoRTCalc(166,178,.f.)
        .w_DALISACQ = .w_LISACQ
        .DoRTCalc(180,194,.f.)
        .w_VOCECR = Docgesana(.w_CAUDOC,'V')
        .w_TOTQTA = iif(.w_CHKTEMP='S',INT(.w_PRQTAMOV*.w_DUR_ORE),0)  
        .w_TOTORE = INT(.w_PROREEFF)
        .w_TOTMIN = .w_PRMINEFF
        .w_TOTQTAM = IIF(.w_CHKTEMP='S', INT(cp_round((.w_DUR_ORE * .w_PRQTAMOV - INT(.w_DUR_ORE * .w_PRQTAMOV)) * 60,0)), 0)
        .DoRTCalc(200,200,.f.)
        if not(empty(.w_CONTRACN))
         .link_2_160('Full')
        endif
        .DoRTCalc(201,203,.f.)
        .w_FLCOMP = 'S'
        .DoRTCalc(205,206,.f.)
        .w_DARIGPRE = 'N'
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_170.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_171.Calculate()
        .DoRTCalc(208,210,.f.)
        .w_OLDCENRIC = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENCOS),.w_CENCOS,.w_DACENCOS)
        .DoRTCalc(212,217,.f.)
        .w_DAUNIMIS = .w_PRUNIMIS
        .w_CODCLI = .w_DACODNOM
        .w_DADESATT = .w_PRDESPRE
        .DoRTCalc(221,223,.f.)
        .w_TOTDURMIN = MOD(.w_TOTALMIN,60)
        .DoRTCalc(225,228,.f.)
        .w_TOTDURORE = .w_TOTALORE+INT(.w_TOTALMIN/60)
        .w_TOTQTAORE = .w_TOTDURQTA+INT(.w_TOTDURQTAM/60)
        .w_TOTQTAMIN = Mod(.w_TOTDURQTAM,60)
        .w_NUMSCO = this.oparentobject.w_NUMSCO
        .w_FLDTRP = This.oparentobject .w_FLDTRP
        .DoRTCalc(234,240,.f.)
        if not(empty(.w_DACODNOM))
         .link_2_196('Full')
        endif
        .DoRTCalc(241,245,.f.)
        .w_DTIPRIG = this.oParentObject.w_DTIPRIG
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_212.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'RAP_PRES')
    this.DoRTCalc(247,258,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_64.enabled = this.oPgFrm.Page1.oPag.oBtn_1_64.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_65.enabled = this.oPgFrm.Page1.oPag.oBtn_1_65.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_93.enabled = this.oPgFrm.Page1.oPag.oBtn_2_93.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
    this.Calculate_QSUZXKUJIT()
    this.Calculate_GIQYYQFYNY()
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPRCODLIS_2_28.enabled = i_bVal
      .Page1.oPag.oATIPRIG_2_29.enabled = i_bVal
      .Page1.oPag.oATIPRI2_2_30.enabled = i_bVal
      .Page1.oPag.oETIPRIG_2_31.enabled = i_bVal
      .Page1.oPag.oRTIPRIG_2_33.enabled = i_bVal
      .Page1.oPag.oPRDESAGG_2_40.enabled = i_bVal
      .Page1.oPag.oPROREEFF_2_45.enabled = i_bVal
      .Page1.oPag.oPRMINEFF_2_46.enabled = i_bVal
      .Page1.oPag.oPRCOSINT_2_48.enabled = i_bVal
      .Page1.oPag.oPRIMPANT_2_199.enabled = i_bVal
      .Page1.oPag.oPRIMPSPE_2_200.enabled = i_bVal
      .Page1.oPag.oBtn_1_64.enabled = .Page1.oPag.oBtn_1_64.mCond()
      .Page1.oPag.oBtn_1_65.enabled = .Page1.oPag.oBtn_1_65.mCond()
      .Page1.oPag.oBtn_2_93.enabled = .Page1.oPag.oBtn_2_93.mCond()
      .Page1.oPag.oBtn_2_188.enabled = i_bVal
      .Page1.oPag.oBtn_2_189.enabled = i_bVal
      .Page1.oPag.oObj_1_18.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      .Page1.oPag.oObj_1_42.enabled = i_bVal
      .Page1.oPag.oObj_1_44.enabled = i_bVal
      .Page1.oPag.oObj_1_47.enabled = i_bVal
      .Page1.oPag.oObj_1_51.enabled = i_bVal
      .Page1.oPag.oObj_1_68.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_79.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_80.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_170.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_171.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_212.enabled = i_bVal
      .Page1.oPag.oObj_1_92.enabled = i_bVal
      .Page1.oPag.oObj_1_93.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'RAP_PRES',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RAP_PRES_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DACODRES,"DACODRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DAGRURES,"DAGRURES",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PR__DATA D(8);
      ,t_DACODICE C(41);
      ,t_PRNUMPRA C(15);
      ,t_DACODATT C(20);
      ,t_PRDESPRE C(40);
      ,t_PRUNIMIS C(3);
      ,t_PRQTAMOV N(12,3);
      ,t_DAPREZZO N(18,5);
      ,t_PRCODVAL C(3);
      ,t_PRCODLIS C(5);
      ,t_ATIPRIG N(3);
      ,t_ATIPRI2 N(3);
      ,t_ETIPRIG N(3);
      ,t_RTIPRIG N(3);
      ,t_RTIPRI2 C(10);
      ,t_PRDESAGG M(10);
      ,t_PRCODOPE N(4);
      ,t_PRDATMOD D(8);
      ,t_PRVALRIG N(18,4);
      ,t_PROREEFF N(3);
      ,t_PRMINEFF N(2);
      ,t_PRCOSINT N(18,4);
      ,t_DAPREMIN N(18,5);
      ,t_DAPREMAX N(18,5);
      ,t_DAGAZUFF C(6);
      ,t_DESUNI C(35);
      ,t_NOMEPRA C(100);
      ,t_FLCOMP N(3);
      ,t_PRIMPANT N(18,5);
      ,t_PRIMPSPE N(18,5);
      ,CPROWNUM N(10);
      ,t_CENCOS C(15);
      ,t_DCODCEN C(15);
      ,t_OLDCEN C(15);
      ,t_ECOD1ATT C(20);
      ,t_RCOD1ATT C(20);
      ,t_DATIPRIG C(1);
      ,t_DATIPRI2 C(1);
      ,t_ECACODART C(20);
      ,t_RCACODART C(20);
      ,t_CACODART C(20);
      ,t_ARTIPRIG C(1);
      ,t_ARTIPRI2 C(1);
      ,t_COD1ATT C(20);
      ,t_NOTIFICA C(1);
      ,t_NUMLIS C(5);
      ,t_NCODLIS C(5);
      ,t_ETIPRI2 C(10);
      ,t_DACODSED C(5);
      ,t_DECTOT N(1);
      ,t_DECUNI N(1);
      ,t_CALCPICU N(1);
      ,t_CALCPICT N(1);
      ,t_LICOSTO N(18,5);
      ,t_DACOSUNI N(18,5);
      ,t_DAFLDEFF C(1);
      ,t_TIPART C(2);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_FLSERG C(1);
      ,t_UNIMIS C(3);
      ,t_LCODVAL C(3);
      ,t_VALCOM C(3);
      ,t_EUNMIS3 C(3);
      ,t_RUNMIS3 C(3);
      ,t_OBTEST D(8);
      ,t_UNMIS3 C(3);
      ,t_VALLIS C(3);
      ,t_FLAPON C(1);
      ,t_ENTE C(10);
      ,t_UFFICI C(10);
      ,t_FLVALO C(1);
      ,t_IMPORT N(18,4);
      ,t_CALDIR C(1);
      ,t_COECAL N(10);
      ,t_PARASS N(3);
      ,t_FLAMPA C(1);
      ,t_CAOVAL N(12,7);
      ,t_DATOBSO D(8);
      ,t_TIPOENTE C(1);
      ,t_CHKTEMP C(1);
      ,t_DUR_ORE N(9,5);
      ,t_ECADTOBSO D(8);
      ,t_RCADTOBSO D(8);
      ,t_CADTOBSO D(8);
      ,t_DACODCOM C(15);
      ,t_DAVOCRIC C(15);
      ,t_DAINICOM D(8);
      ,t_DAFINCOM D(8);
      ,t_DA_SEGNO C(1);
      ,t_DACENCOS C(15);
      ,t_DAATTIVI C(15);
      ,t_NOTIPCLI C(1);
      ,t_DTOBSNOM D(8);
      ,t_NOCODCLI C(15);
      ,t_NOTIPNOM C(1);
      ,t_OFDATDOC D(8);
      ,t_EMOLTI3 N(10,4);
      ,t_RMOLTI3 N(10,4);
      ,t_QTAUM1 N(12,3);
      ,t_MOLTI3 N(10,4);
      ,t_FLUSEP C(1);
      ,t_OPERAT C(1);
      ,t_FLFRAZ1 C(1);
      ,t_MODUM2 C(1);
      ,t_MOLTIP N(10,4);
      ,t_PREZUM C(1);
      ,t_ARTIPART C(2);
      ,t_SCOLIS C(1);
      ,t_DAVOCCOS C(15);
      ,t_CCODLIS C(5);
      ,t_CICLO C(1);
      ,t_EOPERA3 C(1);
      ,t_ROPERA3 C(1);
      ,t_OPERA3 C(1);
      ,t_ETIPSER C(1);
      ,t_RTIPSER C(1);
      ,t_TIPSER C(1);
      ,t_DACODLIS C(5);
      ,t_DAPROLIS C(5);
      ,t_DAPROSCO C(5);
      ,t_DASCOLIS C(5);
      ,t_DASCONT1 N(6,2);
      ,t_DASCONT2 N(6,2);
      ,t_DASCONT3 N(6,2);
      ,t_DASCONT4 N(6,2);
      ,t_DACOMRIC C(15);
      ,t_DAATTRIC C(15);
      ,t_DACENRIC C(15);
      ,t_FL_FRAZ C(1);
      ,t_DALISACQ C(5);
      ,t_VALACQ C(3);
      ,t_INIACQ D(8);
      ,t_TIPACQ C(1);
      ,t_FLSACQ C(1);
      ,t_IVAACQ C(1);
      ,t_FLGACQ C(1);
      ,t_FINACQ D(8);
      ,t_DATCOINI D(8);
      ,t_DATCOFIN D(8);
      ,t_DATRIINI D(8);
      ,t_DATRIFIN D(8);
      ,t_FLSCOR C(1);
      ,t_TARTEM C(1);
      ,t_TARCON N(18,4);
      ,t_FLVALO1 C(1);
      ,t_VOCECR C(1);
      ,t_TOTQTA N(15);
      ,t_TOTORE N(3);
      ,t_TOTMIN N(3);
      ,t_TOTQTAM N(15);
      ,t_CONTRACN C(5);
      ,t_TIPCONCO C(1);
      ,t_LISCOLCO C(5);
      ,t_STATUSCO C(1);
      ,t_FLSPAN C(1);
      ,t_DARIFPRE N(5);
      ,t_DARIGPRE C(1);
      ,t_PRESTA C(1);
      ,t_DACONCOD C(15);
      ,t_CEN_CauDoc C(15);
      ,t_DACONTRA C(10);
      ,t_DACODMOD C(10);
      ,t_DACODIMP C(10);
      ,t_DACOCOMP N(4);
      ,t_DARINNOV N(6);
      ,t_DAUNIMIS C(3);
      ,t_DADESATT C(40);
      ,t_FLUNIV C(1);
      ,t_NUMPRE N(4);
      ,t_LOCALI C(30);
      ,t_FLSCOAC C(1);
      ,t_CNDATFIN D(8);
      ,t_CATCOM C(3);
      ,t_DACODNOM C(15);
      ,t_PRCODSPE C(20);
      ,t_PRCODANT C(20);
      ,t_TIPRIGCA C(1);
      ,t_TIPRI2CA C(1);
      ,t_CNTIPPRA C(10);
      ,t_CNMATOBB C(1);
      ,t_CNASSCTP C(1);
      ,t_CNCOMPLX C(1);
      ,t_CNPROFMT C(1);
      ,t_CNESIPOS C(1);
      ,t_CNPERPLX N(3);
      ,t_CNPERPOS N(3);
      ,t_CNFLVMLQ C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsag_mprbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPR__DATA_2_1.controlsource=this.cTrsName+'.t_PR__DATA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_6.controlsource=this.cTrsName+'.t_DACODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRNUMPRA_2_7.controlsource=this.cTrsName+'.t_PRNUMPRA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_15.controlsource=this.cTrsName+'.t_DACODATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRDESPRE_2_21.controlsource=this.cTrsName+'.t_PRDESPRE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRUNIMIS_2_22.controlsource=this.cTrsName+'.t_PRUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMOV_2_24.controlsource=this.cTrsName+'.t_PRQTAMOV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDAPREZZO_2_26.controlsource=this.cTrsName+'.t_DAPREZZO'
    this.oPgFRm.Page1.oPag.oPRCODVAL_2_27.controlsource=this.cTrsName+'.t_PRCODVAL'
    this.oPgFRm.Page1.oPag.oPRCODLIS_2_28.controlsource=this.cTrsName+'.t_PRCODLIS'
    this.oPgFRm.Page1.oPag.oATIPRIG_2_29.controlsource=this.cTrsName+'.t_ATIPRIG'
    this.oPgFRm.Page1.oPag.oATIPRI2_2_30.controlsource=this.cTrsName+'.t_ATIPRI2'
    this.oPgFRm.Page1.oPag.oETIPRIG_2_31.controlsource=this.cTrsName+'.t_ETIPRIG'
    this.oPgFRm.Page1.oPag.oRTIPRIG_2_33.controlsource=this.cTrsName+'.t_RTIPRIG'
    this.oPgFRm.Page1.oPag.oRTIPRI2_2_34.controlsource=this.cTrsName+'.t_RTIPRI2'
    this.oPgFRm.Page1.oPag.oPRDESAGG_2_40.controlsource=this.cTrsName+'.t_PRDESAGG'
    this.oPgFRm.Page1.oPag.oPRCODOPE_2_41.controlsource=this.cTrsName+'.t_PRCODOPE'
    this.oPgFRm.Page1.oPag.oPRDATMOD_2_42.controlsource=this.cTrsName+'.t_PRDATMOD'
    this.oPgFRm.Page1.oPag.oPRVALRIG_2_43.controlsource=this.cTrsName+'.t_PRVALRIG'
    this.oPgFRm.Page1.oPag.oPROREEFF_2_45.controlsource=this.cTrsName+'.t_PROREEFF'
    this.oPgFRm.Page1.oPag.oPRMINEFF_2_46.controlsource=this.cTrsName+'.t_PRMINEFF'
    this.oPgFRm.Page1.oPag.oPRCOSINT_2_48.controlsource=this.cTrsName+'.t_PRCOSINT'
    this.oPgFRm.Page1.oPag.oDAPREMIN_2_50.controlsource=this.cTrsName+'.t_DAPREMIN'
    this.oPgFRm.Page1.oPag.oDAPREMAX_2_51.controlsource=this.cTrsName+'.t_DAPREMAX'
    this.oPgFRm.Page1.oPag.oDAGAZUFF_2_52.controlsource=this.cTrsName+'.t_DAGAZUFF'
    this.oPgFRm.Page1.oPag.oDESUNI_2_57.controlsource=this.cTrsName+'.t_DESUNI'
    this.oPgFRm.Page1.oPag.oNOMEPRA_2_58.controlsource=this.cTrsName+'.t_NOMEPRA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMP_2_166.controlsource=this.cTrsName+'.t_FLCOMP'
    this.oPgFRm.Page1.oPag.oPRIMPANT_2_199.controlsource=this.cTrsName+'.t_PRIMPANT'
    this.oPgFRm.Page1.oPag.oPRIMPSPE_2_200.controlsource=this.cTrsName+'.t_PRIMPSPE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(20)
    this.AddVLine(90)
    this.AddVLine(217)
    this.AddVLine(338)
    this.AddVLine(582)
    this.AddVLine(620)
    this.AddVLine(689)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPR__DATA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RAP_PRES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAP_PRES_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- gsag_mpr
    i_nCntLine=aggrownum(this.w_DACODRES,this.w_DAGRURES,i_nCntLine)
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RAP_PRES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAP_PRES_IDX,2])
      *
      * insert into RAP_PRES
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RAP_PRES')
        i_extval=cp_InsertValODBCExtFlds(this,'RAP_PRES')
        i_cFldBody=" "+;
                  "(DACODRES,DAGRURES,PR__DATA,DACODICE,PRNUMPRA"+;
                  ",DATIPRIG,DATIPRI2,DACODATT,PRDESPRE,PRUNIMIS"+;
                  ",PRQTAMOV,DAPREZZO,PRCODVAL,PRCODLIS,DACODSED"+;
                  ",PRDESAGG,PRCODOPE,PRDATMOD,PRVALRIG,PROREEFF"+;
                  ",PRMINEFF,DACOSUNI,PRCOSINT,DAFLDEFF,DAPREMIN"+;
                  ",DAPREMAX,DAGAZUFF,DACODCOM,DAVOCRIC,DAINICOM"+;
                  ",DAFINCOM,DA_SEGNO,DACENCOS,DAATTIVI,DAVOCCOS"+;
                  ",DACODLIS,DAPROLIS,DAPROSCO,DASCOLIS,DASCONT1"+;
                  ",DASCONT2,DASCONT3,DASCONT4,DACOMRIC,DAATTRIC"+;
                  ",DACENRIC,DALISACQ,DATCOINI,DATCOFIN,DATRIINI"+;
                  ",DATRIFIN,DARIFPRE,DARIGPRE,DACONCOD,DACONTRA"+;
                  ",DACODMOD,DACODIMP,DACOCOMP,DARINNOV,DACODNOM"+;
                  ",PRCODSPE,PRCODANT,PRIMPANT,PRIMPSPE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DACODRES)+","+cp_ToStrODBC(this.w_DAGRURES)+","+cp_ToStrODBC(this.w_PR__DATA)+","+cp_ToStrODBCNull(this.w_DACODICE)+","+cp_ToStrODBCNull(this.w_PRNUMPRA)+;
             ","+cp_ToStrODBC(this.w_DATIPRIG)+","+cp_ToStrODBC(this.w_DATIPRI2)+","+cp_ToStrODBCNull(this.w_DACODATT)+","+cp_ToStrODBC(this.w_PRDESPRE)+","+cp_ToStrODBCNull(this.w_PRUNIMIS)+;
             ","+cp_ToStrODBC(this.w_PRQTAMOV)+","+cp_ToStrODBC(this.w_DAPREZZO)+","+cp_ToStrODBCNull(this.w_PRCODVAL)+","+cp_ToStrODBCNull(this.w_PRCODLIS)+","+cp_ToStrODBCNull(this.w_DACODSED)+;
             ","+cp_ToStrODBC(this.w_PRDESAGG)+","+cp_ToStrODBC(this.w_PRCODOPE)+","+cp_ToStrODBC(this.w_PRDATMOD)+","+cp_ToStrODBC(this.w_PRVALRIG)+","+cp_ToStrODBC(this.w_PROREEFF)+;
             ","+cp_ToStrODBC(this.w_PRMINEFF)+","+cp_ToStrODBC(this.w_DACOSUNI)+","+cp_ToStrODBC(this.w_PRCOSINT)+","+cp_ToStrODBC(this.w_DAFLDEFF)+","+cp_ToStrODBC(this.w_DAPREMIN)+;
             ","+cp_ToStrODBC(this.w_DAPREMAX)+","+cp_ToStrODBC(this.w_DAGAZUFF)+","+cp_ToStrODBC(this.w_DACODCOM)+","+cp_ToStrODBC(this.w_DAVOCRIC)+","+cp_ToStrODBC(this.w_DAINICOM)+;
             ","+cp_ToStrODBC(this.w_DAFINCOM)+","+cp_ToStrODBC(this.w_DA_SEGNO)+","+cp_ToStrODBC(this.w_DACENCOS)+","+cp_ToStrODBC(this.w_DAATTIVI)+","+cp_ToStrODBC(this.w_DAVOCCOS)+;
             ","+cp_ToStrODBC(this.w_DACODLIS)+","+cp_ToStrODBC(this.w_DAPROLIS)+","+cp_ToStrODBC(this.w_DAPROSCO)+","+cp_ToStrODBC(this.w_DASCOLIS)+","+cp_ToStrODBC(this.w_DASCONT1)+;
             ","+cp_ToStrODBC(this.w_DASCONT2)+","+cp_ToStrODBC(this.w_DASCONT3)+","+cp_ToStrODBC(this.w_DASCONT4)+","+cp_ToStrODBC(this.w_DACOMRIC)+","+cp_ToStrODBC(this.w_DAATTRIC)+;
             ","+cp_ToStrODBC(this.w_DACENRIC)+","+cp_ToStrODBC(this.w_DALISACQ)+","+cp_ToStrODBC(this.w_DATCOINI)+","+cp_ToStrODBC(this.w_DATCOFIN)+","+cp_ToStrODBC(this.w_DATRIINI)+;
             ","+cp_ToStrODBC(this.w_DATRIFIN)+","+cp_ToStrODBC(this.w_DARIFPRE)+","+cp_ToStrODBC(this.w_DARIGPRE)+","+cp_ToStrODBC(this.w_DACONCOD)+","+cp_ToStrODBC(this.w_DACONTRA)+;
             ","+cp_ToStrODBC(this.w_DACODMOD)+","+cp_ToStrODBC(this.w_DACODIMP)+","+cp_ToStrODBC(this.w_DACOCOMP)+","+cp_ToStrODBC(this.w_DARINNOV)+","+cp_ToStrODBCNull(this.w_DACODNOM)+;
             ","+cp_ToStrODBC(this.w_PRCODSPE)+","+cp_ToStrODBC(this.w_PRCODANT)+","+cp_ToStrODBC(this.w_PRIMPANT)+","+cp_ToStrODBC(this.w_PRIMPSPE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RAP_PRES')
        i_extval=cp_InsertValVFPExtFlds(this,'RAP_PRES')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DACODRES',this.w_DACODRES,'DAGRURES',this.w_DAGRURES)
        INSERT INTO (i_cTable) (;
                   DACODRES;
                  ,DAGRURES;
                  ,PR__DATA;
                  ,DACODICE;
                  ,PRNUMPRA;
                  ,DATIPRIG;
                  ,DATIPRI2;
                  ,DACODATT;
                  ,PRDESPRE;
                  ,PRUNIMIS;
                  ,PRQTAMOV;
                  ,DAPREZZO;
                  ,PRCODVAL;
                  ,PRCODLIS;
                  ,DACODSED;
                  ,PRDESAGG;
                  ,PRCODOPE;
                  ,PRDATMOD;
                  ,PRVALRIG;
                  ,PROREEFF;
                  ,PRMINEFF;
                  ,DACOSUNI;
                  ,PRCOSINT;
                  ,DAFLDEFF;
                  ,DAPREMIN;
                  ,DAPREMAX;
                  ,DAGAZUFF;
                  ,DACODCOM;
                  ,DAVOCRIC;
                  ,DAINICOM;
                  ,DAFINCOM;
                  ,DA_SEGNO;
                  ,DACENCOS;
                  ,DAATTIVI;
                  ,DAVOCCOS;
                  ,DACODLIS;
                  ,DAPROLIS;
                  ,DAPROSCO;
                  ,DASCOLIS;
                  ,DASCONT1;
                  ,DASCONT2;
                  ,DASCONT3;
                  ,DASCONT4;
                  ,DACOMRIC;
                  ,DAATTRIC;
                  ,DACENRIC;
                  ,DALISACQ;
                  ,DATCOINI;
                  ,DATCOFIN;
                  ,DATRIINI;
                  ,DATRIFIN;
                  ,DARIFPRE;
                  ,DARIGPRE;
                  ,DACONCOD;
                  ,DACONTRA;
                  ,DACODMOD;
                  ,DACODIMP;
                  ,DACOCOMP;
                  ,DARINNOV;
                  ,DACODNOM;
                  ,PRCODSPE;
                  ,PRCODANT;
                  ,PRIMPANT;
                  ,PRIMPSPE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DACODRES;
                  ,this.w_DAGRURES;
                  ,this.w_PR__DATA;
                  ,this.w_DACODICE;
                  ,this.w_PRNUMPRA;
                  ,this.w_DATIPRIG;
                  ,this.w_DATIPRI2;
                  ,this.w_DACODATT;
                  ,this.w_PRDESPRE;
                  ,this.w_PRUNIMIS;
                  ,this.w_PRQTAMOV;
                  ,this.w_DAPREZZO;
                  ,this.w_PRCODVAL;
                  ,this.w_PRCODLIS;
                  ,this.w_DACODSED;
                  ,this.w_PRDESAGG;
                  ,this.w_PRCODOPE;
                  ,this.w_PRDATMOD;
                  ,this.w_PRVALRIG;
                  ,this.w_PROREEFF;
                  ,this.w_PRMINEFF;
                  ,this.w_DACOSUNI;
                  ,this.w_PRCOSINT;
                  ,this.w_DAFLDEFF;
                  ,this.w_DAPREMIN;
                  ,this.w_DAPREMAX;
                  ,this.w_DAGAZUFF;
                  ,this.w_DACODCOM;
                  ,this.w_DAVOCRIC;
                  ,this.w_DAINICOM;
                  ,this.w_DAFINCOM;
                  ,this.w_DA_SEGNO;
                  ,this.w_DACENCOS;
                  ,this.w_DAATTIVI;
                  ,this.w_DAVOCCOS;
                  ,this.w_DACODLIS;
                  ,this.w_DAPROLIS;
                  ,this.w_DAPROSCO;
                  ,this.w_DASCOLIS;
                  ,this.w_DASCONT1;
                  ,this.w_DASCONT2;
                  ,this.w_DASCONT3;
                  ,this.w_DASCONT4;
                  ,this.w_DACOMRIC;
                  ,this.w_DAATTRIC;
                  ,this.w_DACENRIC;
                  ,this.w_DALISACQ;
                  ,this.w_DATCOINI;
                  ,this.w_DATCOFIN;
                  ,this.w_DATRIINI;
                  ,this.w_DATRIFIN;
                  ,this.w_DARIFPRE;
                  ,this.w_DARIGPRE;
                  ,this.w_DACONCOD;
                  ,this.w_DACONTRA;
                  ,this.w_DACODMOD;
                  ,this.w_DACODIMP;
                  ,this.w_DACOCOMP;
                  ,this.w_DARINNOV;
                  ,this.w_DACODNOM;
                  ,this.w_PRCODSPE;
                  ,this.w_PRCODANT;
                  ,this.w_PRIMPANT;
                  ,this.w_PRIMPSPE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.RAP_PRES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAP_PRES_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT(Empty(t_PR__DATA)) AND (NOT(Empty(t_DACODATT)) or NOT(Empty(t_DACODICE)))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'RAP_PRES')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'RAP_PRES')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT(Empty(t_PR__DATA)) AND (NOT(Empty(t_DACODATT)) or NOT(Empty(t_DACODICE)))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update RAP_PRES
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'RAP_PRES')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PR__DATA="+cp_ToStrODBC(this.w_PR__DATA)+;
                     ",DACODICE="+cp_ToStrODBCNull(this.w_DACODICE)+;
                     ",PRNUMPRA="+cp_ToStrODBCNull(this.w_PRNUMPRA)+;
                     ",DATIPRIG="+cp_ToStrODBC(this.w_DATIPRIG)+;
                     ",DATIPRI2="+cp_ToStrODBC(this.w_DATIPRI2)+;
                     ",DACODATT="+cp_ToStrODBCNull(this.w_DACODATT)+;
                     ",PRDESPRE="+cp_ToStrODBC(this.w_PRDESPRE)+;
                     ",PRUNIMIS="+cp_ToStrODBCNull(this.w_PRUNIMIS)+;
                     ",PRQTAMOV="+cp_ToStrODBC(this.w_PRQTAMOV)+;
                     ",DAPREZZO="+cp_ToStrODBC(this.w_DAPREZZO)+;
                     ",PRCODVAL="+cp_ToStrODBCNull(this.w_PRCODVAL)+;
                     ",PRCODLIS="+cp_ToStrODBCNull(this.w_PRCODLIS)+;
                     ",DACODSED="+cp_ToStrODBCNull(this.w_DACODSED)+;
                     ",PRDESAGG="+cp_ToStrODBC(this.w_PRDESAGG)+;
                     ",PRCODOPE="+cp_ToStrODBC(this.w_PRCODOPE)+;
                     ",PRDATMOD="+cp_ToStrODBC(this.w_PRDATMOD)+;
                     ",PRVALRIG="+cp_ToStrODBC(this.w_PRVALRIG)+;
                     ",PROREEFF="+cp_ToStrODBC(this.w_PROREEFF)+;
                     ",PRMINEFF="+cp_ToStrODBC(this.w_PRMINEFF)+;
                     ",DACOSUNI="+cp_ToStrODBC(this.w_DACOSUNI)+;
                     ",PRCOSINT="+cp_ToStrODBC(this.w_PRCOSINT)+;
                     ",DAFLDEFF="+cp_ToStrODBC(this.w_DAFLDEFF)+;
                     ",DAPREMIN="+cp_ToStrODBC(this.w_DAPREMIN)+;
                     ",DAPREMAX="+cp_ToStrODBC(this.w_DAPREMAX)+;
                     ",DAGAZUFF="+cp_ToStrODBC(this.w_DAGAZUFF)+;
                     ",DACODCOM="+cp_ToStrODBC(this.w_DACODCOM)+;
                     ",DAVOCRIC="+cp_ToStrODBC(this.w_DAVOCRIC)+;
                     ",DAINICOM="+cp_ToStrODBC(this.w_DAINICOM)+;
                     ",DAFINCOM="+cp_ToStrODBC(this.w_DAFINCOM)+;
                     ",DA_SEGNO="+cp_ToStrODBC(this.w_DA_SEGNO)+;
                     ",DACENCOS="+cp_ToStrODBC(this.w_DACENCOS)+;
                     ",DAATTIVI="+cp_ToStrODBC(this.w_DAATTIVI)+;
                     ",DAVOCCOS="+cp_ToStrODBC(this.w_DAVOCCOS)+;
                     ",DACODLIS="+cp_ToStrODBC(this.w_DACODLIS)+;
                     ",DAPROLIS="+cp_ToStrODBC(this.w_DAPROLIS)+;
                     ",DAPROSCO="+cp_ToStrODBC(this.w_DAPROSCO)+;
                     ",DASCOLIS="+cp_ToStrODBC(this.w_DASCOLIS)+;
                     ",DASCONT1="+cp_ToStrODBC(this.w_DASCONT1)+;
                     ",DASCONT2="+cp_ToStrODBC(this.w_DASCONT2)+;
                     ",DASCONT3="+cp_ToStrODBC(this.w_DASCONT3)+;
                     ",DASCONT4="+cp_ToStrODBC(this.w_DASCONT4)+;
                     ",DACOMRIC="+cp_ToStrODBC(this.w_DACOMRIC)+;
                     ",DAATTRIC="+cp_ToStrODBC(this.w_DAATTRIC)+;
                     ",DACENRIC="+cp_ToStrODBC(this.w_DACENRIC)+;
                     ",DALISACQ="+cp_ToStrODBC(this.w_DALISACQ)+;
                     ",DATCOINI="+cp_ToStrODBC(this.w_DATCOINI)+;
                     ",DATCOFIN="+cp_ToStrODBC(this.w_DATCOFIN)+;
                     ",DATRIINI="+cp_ToStrODBC(this.w_DATRIINI)+;
                     ",DATRIFIN="+cp_ToStrODBC(this.w_DATRIFIN)+;
                     ",DARIFPRE="+cp_ToStrODBC(this.w_DARIFPRE)+;
                     ",DARIGPRE="+cp_ToStrODBC(this.w_DARIGPRE)+;
                     ",DACONCOD="+cp_ToStrODBC(this.w_DACONCOD)+;
                     ",DACONTRA="+cp_ToStrODBC(this.w_DACONTRA)+;
                     ",DACODMOD="+cp_ToStrODBC(this.w_DACODMOD)+;
                     ",DACODIMP="+cp_ToStrODBC(this.w_DACODIMP)+;
                     ",DACOCOMP="+cp_ToStrODBC(this.w_DACOCOMP)+;
                     ",DARINNOV="+cp_ToStrODBC(this.w_DARINNOV)+;
                     ",DACODNOM="+cp_ToStrODBCNull(this.w_DACODNOM)+;
                     ",PRCODSPE="+cp_ToStrODBC(this.w_PRCODSPE)+;
                     ",PRCODANT="+cp_ToStrODBC(this.w_PRCODANT)+;
                     ",PRIMPANT="+cp_ToStrODBC(this.w_PRIMPANT)+;
                     ",PRIMPSPE="+cp_ToStrODBC(this.w_PRIMPSPE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'RAP_PRES')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PR__DATA=this.w_PR__DATA;
                     ,DACODICE=this.w_DACODICE;
                     ,PRNUMPRA=this.w_PRNUMPRA;
                     ,DATIPRIG=this.w_DATIPRIG;
                     ,DATIPRI2=this.w_DATIPRI2;
                     ,DACODATT=this.w_DACODATT;
                     ,PRDESPRE=this.w_PRDESPRE;
                     ,PRUNIMIS=this.w_PRUNIMIS;
                     ,PRQTAMOV=this.w_PRQTAMOV;
                     ,DAPREZZO=this.w_DAPREZZO;
                     ,PRCODVAL=this.w_PRCODVAL;
                     ,PRCODLIS=this.w_PRCODLIS;
                     ,DACODSED=this.w_DACODSED;
                     ,PRDESAGG=this.w_PRDESAGG;
                     ,PRCODOPE=this.w_PRCODOPE;
                     ,PRDATMOD=this.w_PRDATMOD;
                     ,PRVALRIG=this.w_PRVALRIG;
                     ,PROREEFF=this.w_PROREEFF;
                     ,PRMINEFF=this.w_PRMINEFF;
                     ,DACOSUNI=this.w_DACOSUNI;
                     ,PRCOSINT=this.w_PRCOSINT;
                     ,DAFLDEFF=this.w_DAFLDEFF;
                     ,DAPREMIN=this.w_DAPREMIN;
                     ,DAPREMAX=this.w_DAPREMAX;
                     ,DAGAZUFF=this.w_DAGAZUFF;
                     ,DACODCOM=this.w_DACODCOM;
                     ,DAVOCRIC=this.w_DAVOCRIC;
                     ,DAINICOM=this.w_DAINICOM;
                     ,DAFINCOM=this.w_DAFINCOM;
                     ,DA_SEGNO=this.w_DA_SEGNO;
                     ,DACENCOS=this.w_DACENCOS;
                     ,DAATTIVI=this.w_DAATTIVI;
                     ,DAVOCCOS=this.w_DAVOCCOS;
                     ,DACODLIS=this.w_DACODLIS;
                     ,DAPROLIS=this.w_DAPROLIS;
                     ,DAPROSCO=this.w_DAPROSCO;
                     ,DASCOLIS=this.w_DASCOLIS;
                     ,DASCONT1=this.w_DASCONT1;
                     ,DASCONT2=this.w_DASCONT2;
                     ,DASCONT3=this.w_DASCONT3;
                     ,DASCONT4=this.w_DASCONT4;
                     ,DACOMRIC=this.w_DACOMRIC;
                     ,DAATTRIC=this.w_DAATTRIC;
                     ,DACENRIC=this.w_DACENRIC;
                     ,DALISACQ=this.w_DALISACQ;
                     ,DATCOINI=this.w_DATCOINI;
                     ,DATCOFIN=this.w_DATCOFIN;
                     ,DATRIINI=this.w_DATRIINI;
                     ,DATRIFIN=this.w_DATRIFIN;
                     ,DARIFPRE=this.w_DARIFPRE;
                     ,DARIGPRE=this.w_DARIGPRE;
                     ,DACONCOD=this.w_DACONCOD;
                     ,DACONTRA=this.w_DACONTRA;
                     ,DACODMOD=this.w_DACODMOD;
                     ,DACODIMP=this.w_DACODIMP;
                     ,DACOCOMP=this.w_DACOCOMP;
                     ,DARINNOV=this.w_DARINNOV;
                     ,DACODNOM=this.w_DACODNOM;
                     ,PRCODSPE=this.w_PRCODSPE;
                     ,PRCODANT=this.w_PRCODANT;
                     ,PRIMPANT=this.w_PRIMPANT;
                     ,PRIMPSPE=this.w_PRIMPSPE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsag_mpr
     This.oparentobject.o_GRUPART= This.oparentobject.w_GRUPART
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RAP_PRES_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAP_PRES_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT(Empty(t_PR__DATA)) AND (NOT(Empty(t_DACODATT)) or NOT(Empty(t_DACODICE)))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete RAP_PRES
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT(Empty(t_PR__DATA)) AND (NOT(Empty(t_DACODATT)) or NOT(Empty(t_DACODICE)))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RAP_PRES_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAP_PRES_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,1,.t.)
          .w_FLGLIS = 'N'
          .w_DACODRES = .w_CODRES
        .DoRTCalc(4,4,.t.)
          .w_MVCODVAL = g_PERVAL
        if .o_LetturaParalte<>.w_LetturaParalte
          .w_LetturaParAlte = i_CodAzi
          .link_1_7('Full')
        endif
        .DoRTCalc(7,7,.t.)
          .w_CODRES = This.oparentobject .w_CODRESP
          .link_1_9('Full')
          .w_DAGRURES = .w_GRUPART
          .w_CAUACQ = This.oparentobject .w_CAUACQ
          .w_CAUDOC = This.oparentobject .w_CAUDOC
          .link_1_13('Full')
          .link_1_14('Full')
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .DoRTCalc(14,17,.t.)
          .link_1_35('Full')
          .link_1_36('Full')
          .w_OLDCOM = iif(Not Empty(.w_PRNUMPRA),.w_PRNUMPRA,.w_DACODCOM)
        .DoRTCalc(21,22,.t.)
          .w_PRZVAC = This.oparentobject .w_PRZVAC
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
          .w_GRUPART = This.oparentobject .w_GRUPART
          .w_DATLIS = This.oparentobject .w_DATLIS
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
          .w_MVFLVEAC = this.oparentobject.w_MVFLVEAC
          .w_TIPRIS = This.oparentobject .w_DPTIPRIS
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .DoRTCalc(28,28,.t.)
          .w_FLACQ = IIF(Not Empty(.w_CAUACQ),Docgesana(.w_CAUACQ,'A'),.w_FLANAL)
          .w_LISACQ = This.oparentobject .w_LISACQ
          .w_FLACOMAQ = Docgesana(.w_CAUACQ,'C')
          .w_FLANAL = This.oparentobject .w_FLANAL
          .w_FLGCOM = This.oparentobject .w_FLGCOM
          .w_FLDANA = Docgesana(.w_CAUDOC,'A')
        .DoRTCalc(35,40,.t.)
          .w_EDITCODPRA = This.oparentobject .w_EDITCODPRA
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .DoRTCalc(42,42,.t.)
          .link_1_70('Full')
        .DoRTCalc(44,47,.t.)
          .w_FLCCRAUTO = This.oparentobject .w_FLCCRAUTO
        .DoRTCalc(49,49,.t.)
        if .o_PR__DATA<>.w_PR__DATA
          .w_DATALIST = .w_PR__DATA
        endif
          .w_CENCOS = This.oparentobject .w_CENCOS
        .DoRTCalc(52,52,.t.)
        if .o_DCODCEN<>.w_DCODCEN.or. .o_CENCOS<>.w_CENCOS.or. .o_DACENCOS<>.w_DACENCOS
          .w_OLDCEN = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENCOS),.w_CENCOS,.w_DACENCOS)
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE
          .Calculate_DIJYXDBEQS()
        endif
        .DoRTCalc(54,62,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
        endif
        .DoRTCalc(64,65,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_COD1ATT = IIF(.w_ISAHE,.w_ECOD1ATT,.w_RCOD1ATT)
        endif
        .DoRTCalc(67,73,.t.)
        if .o_PRNUMPRA<>.w_PRNUMPRA
          .w_PRCODVAL = iif(Not empty(.w_LCODVAL),.w_LCODVAL,g_PERVAL)
          .link_2_27('Full')
        endif
        if .o_PRNUMPRA<>.w_PRNUMPRA.or. .o_PRCODVAL<>.w_PRCODVAL.or. .o_CODLIS<>.w_CODLIS.or. .o_CCODLIS<>.w_CCODLIS.or. .o_DACODNOM<>.w_DACODNOM
          .w_PRCODLIS = .w_CCODLIS
          .link_2_28('Full')
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DAPREZZO<>.w_DAPREZZO
          .w_ATIPRIG = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N', Not empty(.w_DATIPRIG),.w_DATIPRIG,LEFT(ALLTRIM(.w_ARTIPRIG)+'D',1))
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DAPREZZO<>.w_DAPREZZO
          .w_ATIPRI2 = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', Not empty(.w_DATIPRI2),.w_DATIPRI2,LEFT(ALLTRIM(.w_ARTIPRI2)+'D',1))
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DAPREZZO<>.w_DAPREZZO
          .w_ETIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DAPREZZO<>.w_DAPREZZO
          .w_ETIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        endif
        if .o_DACODICE<>.w_DACODICE.or. .o_DAPREZZO<>.w_DAPREZZO
          .w_RTIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        endif
        if .o_DACODICE<>.w_DACODICE.or. .o_DAPREZZO<>.w_DAPREZZO
          .w_RTIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,'D')
        endif
        if .o_DACODNOM<>.w_DACODNOM
          .w_DACODSED = SPACE(5)
          .link_2_35('Full')
        endif
        .DoRTCalc(83,84,.t.)
        if .o_PRCODVAL<>.w_PRCODVAL
          .w_CALCPICU = DEFPIU(.w_DECUNI)
        endif
        if .o_PRCODVAL<>.w_PRCODVAL
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(87,87,.t.)
          .w_PRCODOPE = i_codute
          .w_PRDATMOD = i_datsys
          .w_PRVALRIG = cp_round(.w_PRQTAMOV*.w_DAPREZZO, IIF(.w_NOTIFICA='S' , 0, .w_DECTOT))
        .DoRTCalc(91,91,.t.)
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE.or. .o_PRUNIMIS<>.w_PRUNIMIS.or. .o_PRQTAMOV<>.w_PRQTAMOV
          .w_PROREEFF = min(IIF(.w_CHKTEMP='S', INT(.w_DUR_ORE * .w_PRQTAMOV), 0),999)
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE.or. .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_PRUNIMIS<>.w_PRUNIMIS
          .w_PRMINEFF = IIF(.w_CHKTEMP='S', INT(cp_round((.w_DUR_ORE * .w_PRQTAMOV - INT(.w_DUR_ORE * .w_PRQTAMOV)) * 60,0)), 0)
        endif
        .DoRTCalc(94,94,.t.)
        if .o_PROREEFF<>.w_PROREEFF.or. .o_PRMINEFF<>.w_PRMINEFF.or. .o_DACOSUNI<>.w_DACOSUNI
          .w_PRCOSINT = IIF(.w_LICOSTO>0 and .w_COST_ORA=0 ,.w_DACOSUNI*.w_PRQTAMOV,(.w_PROREEFF+(.w_PRMINEFF/60))*.w_DACOSUNI)
        endif
          .w_DAFLDEFF = "N"
        .DoRTCalc(97,111,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_UNMIS3 = IIF(.w_ISAHE,.w_EUNMIS3,.w_RUNMIS3)
        endif
        .DoRTCalc(113,114,.t.)
          .link_2_68('Full')
        .DoRTCalc(116,122,.t.)
        if .o_PRCODVAL<>.w_PRCODVAL
          .w_CAOVAL = IIF(.w_CAOVAL=1 OR .w_CAOVAL=0 OR .w_PRCODVAL=g_CODEUR, GETCAM(.w_PRCODVAL, .w_PR__DATA, 7),.w_CAOVAL)
        endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_79.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_80.Calculate()
        .DoRTCalc(124,129,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_CADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
        endif
        if .o_PRNUMPRA<>.w_PRNUMPRA
          .w_DACODCOM = .w_PRNUMPRA
        endif
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE
          .w_DAVOCRIC = iif(Not empty(.w_COD1ATT) ,SearchVoc(this,'R',.w_COD1ATT,.w_NOTIPCLI,.w_NOCODCLI,.w_CAUDOC,.w_DACODNOM),SPACE(15))
        endif
        .DoRTCalc(133,136,.t.)
        if .o_DACODCOM<>.w_DACODCOM
          .w_DAATTIVI = space(15)
        endif
        .DoRTCalc(138,139,.t.)
          .link_2_97('Full')
        .DoRTCalc(141,146,.t.)
          .w_QTAUM1 = IIF(.w_DATIPRIG='F', 1, CALQTA(.w_PRQTAMOV,.w_PRUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ' ,.w_UNMIS3, .w_OPERA3, .w_MOLTI3))
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_MOLTI3 = IIF(.w_ISAHE,.w_EMOLTI3,.w_RMOLTI3)
        endif
        .DoRTCalc(149,156,.t.)
        if .o_DACODATT<>.w_DACODATT.or. .o_DACODICE<>.w_DACODICE
          .w_DAVOCCOS = iif(Not empty(.w_COD1ATT) ,SearchVoc(this,'C',.w_COD1ATT,.w_NOTIPCLI,.w_NOCODCLI,.w_CAUDOC,.w_DACODNOM),SPACE(15))
        endif
        .DoRTCalc(158,161,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_OPERA3 = IIF(.w_ISAHE,.w_EOPERA3,.w_ROPERA3)
        endif
        .DoRTCalc(163,164,.t.)
        if .o_DACODICE<>.w_DACODICE.or. .o_DACODATT<>.w_DACODATT
          .w_TIPSER = IIF(.w_ISAHE,.w_ETIPSER,.w_RTIPSER)
        endif
        .DoRTCalc(166,194,.t.)
          .w_VOCECR = Docgesana(.w_CAUDOC,'V')
        if .o_ATIPRIG<>.w_ATIPRIG.or. .o_ETIPRIG<>.w_ETIPRIG.or. .o_RTIPRIG<>.w_RTIPRIG.or. .o_ATIPRI2<>.w_ATIPRI2.or. .o_ETIPRI2<>.w_ETIPRI2.or. .o_RTIPRI2<>.w_RTIPRI2
          .Calculate_URGRYMFFIM()
        endif
        if .o_DAPREZZO<>.w_DAPREZZO.or. .o_DACODRES<>.w_DACODRES
          .Calculate_PLUJZKETBZ()
        endif
        if .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_PRUNIMIS<>.w_PRUNIMIS
          .w_TOTDURQTA = .w_TOTDURQTA-.w_totqta
          .w_TOTQTA = iif(.w_CHKTEMP='S',INT(.w_PRQTAMOV*.w_DUR_ORE),0)  
          .w_TOTDURQTA = .w_TOTDURQTA+.w_totqta
        endif
        if .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_PRUNIMIS<>.w_PRUNIMIS.or. .o_PROREEFF<>.w_PROREEFF
          .w_TOTALORE = .w_TOTALORE-.w_totore
          .w_TOTORE = INT(.w_PROREEFF)
          .w_TOTALORE = .w_TOTALORE+.w_totore
        endif
        if .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_PRUNIMIS<>.w_PRUNIMIS.or. .o_PRMINEFF<>.w_PRMINEFF
          .w_TOTALMIN = .w_TOTALMIN-.w_totmin
          .w_TOTMIN = .w_PRMINEFF
          .w_TOTALMIN = .w_TOTALMIN+.w_totmin
        endif
        if .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_PRUNIMIS<>.w_PRUNIMIS
          .w_TOTDURQTAM = .w_TOTDURQTAM-.w_totqtam
          .w_TOTQTAM = IIF(.w_CHKTEMP='S', INT(cp_round((.w_DUR_ORE * .w_PRQTAMOV - INT(.w_DUR_ORE * .w_PRQTAMOV)) * 60,0)), 0)
          .w_TOTDURQTAM = .w_TOTDURQTAM+.w_totqtam
        endif
          .link_2_160('Full')
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_170.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_171.Calculate()
        if .o_DACODRES<>.w_DACODRES.or. .o_DACODATT<>.w_DACODATT
          .Calculate_AZIXBNPXNY()
        endif
        .DoRTCalc(201,210,.t.)
        if .o_DCODCEN<>.w_DCODCEN.or. .o_CENCOS<>.w_CENCOS.or. .o_DACENRIC<>.w_DACENRIC.or. .o_DACODRES<>.w_DACODRES
          .w_OLDCENRIC = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENCOS),.w_CENCOS,.w_DACENCOS)
        endif
        .DoRTCalc(212,217,.t.)
          .w_DAUNIMIS = .w_PRUNIMIS
          .w_CODCLI = .w_DACODNOM
          .w_DADESATT = .w_PRDESPRE
        .DoRTCalc(221,223,.t.)
        if .o_PRMINEFF<>.w_PRMINEFF.or. .o_PRUNIMIS<>.w_PRUNIMIS.or. .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_TOTALMIN<>.w_TOTALMIN
          .w_TOTDURMIN = MOD(.w_TOTALMIN,60)
        endif
        .DoRTCalc(225,228,.t.)
        if .o_PROREEFF<>.w_PROREEFF.or. .o_PRUNIMIS<>.w_PRUNIMIS.or. .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_TOTALMIN<>.w_TOTALMIN.or. .o_TOTALORE<>.w_TOTALORE
          .w_TOTDURORE = .w_TOTALORE+INT(.w_TOTALMIN/60)
        endif
        if .o_PRUNIMIS<>.w_PRUNIMIS.or. .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_TOTDURQTA<>.w_TOTDURQTA
          .w_TOTQTAORE = .w_TOTDURQTA+INT(.w_TOTDURQTAM/60)
        endif
        if .o_PRUNIMIS<>.w_PRUNIMIS.or. .o_PRQTAMOV<>.w_PRQTAMOV.or. .o_TOTDURQTAM<>.w_TOTDURQTAM
          .w_TOTQTAMIN = Mod(.w_TOTDURQTAM,60)
        endif
          .w_NUMSCO = this.oparentobject.w_NUMSCO
          .w_FLDTRP = This.oparentobject .w_FLDTRP
        .DoRTCalc(234,239,.t.)
          .link_2_196('Full')
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_212.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        this.Calculate_TITGOTIGZK()
      endwith
      this.DoRTCalc(241,258,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CENCOS with this.w_CENCOS
      replace t_DCODCEN with this.w_DCODCEN
      replace t_OLDCEN with this.w_OLDCEN
      replace t_ECOD1ATT with this.w_ECOD1ATT
      replace t_RCOD1ATT with this.w_RCOD1ATT
      replace t_DATIPRIG with this.w_DATIPRIG
      replace t_DATIPRI2 with this.w_DATIPRI2
      replace t_ECACODART with this.w_ECACODART
      replace t_RCACODART with this.w_RCACODART
      replace t_CACODART with this.w_CACODART
      replace t_ARTIPRIG with this.w_ARTIPRIG
      replace t_ARTIPRI2 with this.w_ARTIPRI2
      replace t_COD1ATT with this.w_COD1ATT
      replace t_NOTIFICA with this.w_NOTIFICA
      replace t_NUMLIS with this.w_NUMLIS
      replace t_NCODLIS with this.w_NCODLIS
      replace t_ETIPRI2 with this.w_ETIPRI2
      replace t_DACODSED with this.w_DACODSED
      replace t_DECTOT with this.w_DECTOT
      replace t_DECUNI with this.w_DECUNI
      replace t_CALCPICU with this.w_CALCPICU
      replace t_CALCPICT with this.w_CALCPICT
      replace t_LICOSTO with this.w_LICOSTO
      replace t_DACOSUNI with this.w_DACOSUNI
      replace t_DAFLDEFF with this.w_DAFLDEFF
      replace t_TIPART with this.w_TIPART
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_FLSERG with this.w_FLSERG
      replace t_UNIMIS with this.w_UNIMIS
      replace t_LCODVAL with this.w_LCODVAL
      replace t_VALCOM with this.w_VALCOM
      replace t_EUNMIS3 with this.w_EUNMIS3
      replace t_RUNMIS3 with this.w_RUNMIS3
      replace t_OBTEST with this.w_OBTEST
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_VALLIS with this.w_VALLIS
      replace t_FLAPON with this.w_FLAPON
      replace t_ENTE with this.w_ENTE
      replace t_UFFICI with this.w_UFFICI
      replace t_FLVALO with this.w_FLVALO
      replace t_IMPORT with this.w_IMPORT
      replace t_CALDIR with this.w_CALDIR
      replace t_COECAL with this.w_COECAL
      replace t_PARASS with this.w_PARASS
      replace t_FLAMPA with this.w_FLAMPA
      replace t_CAOVAL with this.w_CAOVAL
      replace t_DATOBSO with this.w_DATOBSO
      replace t_TIPOENTE with this.w_TIPOENTE
      replace t_CHKTEMP with this.w_CHKTEMP
      replace t_DUR_ORE with this.w_DUR_ORE
      replace t_ECADTOBSO with this.w_ECADTOBSO
      replace t_RCADTOBSO with this.w_RCADTOBSO
      replace t_CADTOBSO with this.w_CADTOBSO
      replace t_DACODCOM with this.w_DACODCOM
      replace t_DAVOCRIC with this.w_DAVOCRIC
      replace t_DAINICOM with this.w_DAINICOM
      replace t_DAFINCOM with this.w_DAFINCOM
      replace t_DA_SEGNO with this.w_DA_SEGNO
      replace t_DACENCOS with this.w_DACENCOS
      replace t_DAATTIVI with this.w_DAATTIVI
      replace t_NOTIPCLI with this.w_NOTIPCLI
      replace t_DTOBSNOM with this.w_DTOBSNOM
      replace t_NOCODCLI with this.w_NOCODCLI
      replace t_NOTIPNOM with this.w_NOTIPNOM
      replace t_OBTEST with this.w_OBTEST
      replace t_DATOBSO with this.w_DATOBSO
      replace t_OFDATDOC with this.w_OFDATDOC
      replace t_EMOLTI3 with this.w_EMOLTI3
      replace t_RMOLTI3 with this.w_RMOLTI3
      replace t_QTAUM1 with this.w_QTAUM1
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_FLUSEP with this.w_FLUSEP
      replace t_OPERAT with this.w_OPERAT
      replace t_FLFRAZ1 with this.w_FLFRAZ1
      replace t_MODUM2 with this.w_MODUM2
      replace t_MOLTIP with this.w_MOLTIP
      replace t_PREZUM with this.w_PREZUM
      replace t_ARTIPART with this.w_ARTIPART
      replace t_SCOLIS with this.w_SCOLIS
      replace t_DAVOCCOS with this.w_DAVOCCOS
      replace t_CCODLIS with this.w_CCODLIS
      replace t_CICLO with this.w_CICLO
      replace t_EOPERA3 with this.w_EOPERA3
      replace t_ROPERA3 with this.w_ROPERA3
      replace t_OPERA3 with this.w_OPERA3
      replace t_ETIPSER with this.w_ETIPSER
      replace t_RTIPSER with this.w_RTIPSER
      replace t_TIPSER with this.w_TIPSER
      replace t_DACODLIS with this.w_DACODLIS
      replace t_DAPROLIS with this.w_DAPROLIS
      replace t_DAPROSCO with this.w_DAPROSCO
      replace t_DASCOLIS with this.w_DASCOLIS
      replace t_DASCONT1 with this.w_DASCONT1
      replace t_DASCONT2 with this.w_DASCONT2
      replace t_DASCONT3 with this.w_DASCONT3
      replace t_DASCONT4 with this.w_DASCONT4
      replace t_DACOSUNI with this.w_DACOSUNI
      replace t_DACOMRIC with this.w_DACOMRIC
      replace t_DAATTRIC with this.w_DAATTRIC
      replace t_DACENRIC with this.w_DACENRIC
      replace t_FL_FRAZ with this.w_FL_FRAZ
      replace t_DALISACQ with this.w_DALISACQ
      replace t_VALACQ with this.w_VALACQ
      replace t_INIACQ with this.w_INIACQ
      replace t_TIPACQ with this.w_TIPACQ
      replace t_FLSACQ with this.w_FLSACQ
      replace t_IVAACQ with this.w_IVAACQ
      replace t_FLGACQ with this.w_FLGACQ
      replace t_FINACQ with this.w_FINACQ
      replace t_DATCOINI with this.w_DATCOINI
      replace t_DATCOFIN with this.w_DATCOFIN
      replace t_DATRIINI with this.w_DATRIINI
      replace t_DATRIFIN with this.w_DATRIFIN
      replace t_FLSCOR with this.w_FLSCOR
      replace t_TARTEM with this.w_TARTEM
      replace t_TARCON with this.w_TARCON
      replace t_FLVALO1 with this.w_FLVALO1
      replace t_VOCECR with this.w_VOCECR
      replace t_TOTQTA with this.w_TOTQTA
      replace t_TOTORE with this.w_TOTORE
      replace t_TOTMIN with this.w_TOTMIN
      replace t_TOTQTAM with this.w_TOTQTAM
      replace t_CONTRACN with this.w_CONTRACN
      replace t_TIPCONCO with this.w_TIPCONCO
      replace t_LISCOLCO with this.w_LISCOLCO
      replace t_STATUSCO with this.w_STATUSCO
      replace t_FLSPAN with this.w_FLSPAN
      replace t_DARIFPRE with this.w_DARIFPRE
      replace t_DARIGPRE with this.w_DARIGPRE
      replace t_PRESTA with this.w_PRESTA
      replace t_DACONCOD with this.w_DACONCOD
      replace t_CEN_CauDoc with this.w_CEN_CauDoc
      replace t_DACONTRA with this.w_DACONTRA
      replace t_DACODMOD with this.w_DACODMOD
      replace t_DACODIMP with this.w_DACODIMP
      replace t_DACOCOMP with this.w_DACOCOMP
      replace t_DARINNOV with this.w_DARINNOV
      replace t_DAUNIMIS with this.w_DAUNIMIS
      replace t_DADESATT with this.w_DADESATT
      replace t_FLUNIV with this.w_FLUNIV
      replace t_NUMPRE with this.w_NUMPRE
      replace t_LOCALI with this.w_LOCALI
      replace t_DALISACQ with this.w_DALISACQ
      replace t_FLSCOAC with this.w_FLSCOAC
      replace t_CNDATFIN with this.w_CNDATFIN
      replace t_CATCOM with this.w_CATCOM
      replace t_DACODNOM with this.w_DACODNOM
      replace t_PRCODSPE with this.w_PRCODSPE
      replace t_PRCODANT with this.w_PRCODANT
      replace t_TIPRIGCA with this.w_TIPRIGCA
      replace t_TIPRI2CA with this.w_TIPRI2CA
      replace t_CNTIPPRA with this.w_CNTIPPRA
      replace t_CNMATOBB with this.w_CNMATOBB
      replace t_CNASSCTP with this.w_CNASSCTP
      replace t_CNCOMPLX with this.w_CNCOMPLX
      replace t_CNPROFMT with this.w_CNPROFMT
      replace t_CNESIPOS with this.w_CNESIPOS
      replace t_CNPERPLX with this.w_CNPERPLX
      replace t_CNPERPOS with this.w_CNPERPOS
      replace t_CNFLVMLQ with this.w_CNFLVMLQ
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_79.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_80.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_170.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_171.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_212.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_79.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_80.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_170.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_171.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_212.Calculate()
    endwith
  return
  proc Calculate_QSUZXKUJIT()
    with this
          * --- Valorizzo listino di riga
          .w_PRCODLIS = .w_CODLIS
          .link_2_28('Full')
    endwith
  endproc
  proc Calculate_VYQPPPBUZV()
    with this
          * --- Aggiorna rownum
          GSAG_BCK(this;
              ,'R';
             )
    endwith
  endproc
  proc Calculate_HSUFWAGNQY()
    with this
          * --- Deseleziona tutte le prestazioni gi� inserite
          gsag_bck(this;
              ,'D';
             )
    endwith
  endproc
  proc Calculate_GZPILQVQEQ()
    with this
          * --- Aggiorna spunta righe collegate
          GSAG_BCK(this;
              ,'G';
             )
    endwith
  endproc
  proc Calculate_NNUSMMZHPX()
    with this
          * --- Aggiorna tipo righe collegate
          GSAG_BCK(this;
              ,'H';
             )
    endwith
  endproc
  proc Calculate_HACHIGEILQ()
    with this
          * --- Deseleziona le prime righe
          GSAG_BCK(this;
              ,'B';
             )
    endwith
  endproc
  proc Calculate_DIJYXDBEQS()
    with this
          * --- Init tipo documento
          .w_DATIPRIG = ''
          .w_DATIPRI2 = ''
    endwith
  endproc
  proc Calculate_GIQYYQFYNY()
    with this
          * --- Inizializza con valori di default
          GSAG_BP1(this;
             )
    endwith
  endproc
  proc Calculate_URGRYMFFIM()
    with this
          * --- Ricalcolo tipo documento al cambiare delle combo collegate
          .w_DATIPRIG = ICASE(.w_ISAHE,.w_ETIPRIG,Isahr(),.w_RTIPRIG,.w_ATIPRIG)
          .w_DATIPRI2 = ICASE(.w_Isahe,.w_ETIPRI2,Isahr(),.w_RTIPRI2,.w_ATIPRI2)
          .w_ATIPRIG = .w_DATIPRIG
          .w_ATIPRI2 = .w_DATIPRI2
    endwith
  endproc
  proc Calculate_PLUJZKETBZ()
    with this
          * --- Ricalcolo tipo documento al cambiare del prezzo
          .w_ARTIPRIG = IIF(.w_DAPREZZO <>0 AND .w_TIPRIGCA='D',.w_TIPRIGCA,'N')
          .w_ARTIPRI2 = IIF(.w_DAPREZZO <>0 AND .w_TIPRI2CA='D',.w_TIPRI2CA,'N')
          .w_DATIPRIG = ICASE(.w_Isahe,.w_ETIPRIG,Isahr(),.w_RTIPRIG,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER<>'S', 'N', .w_DTIPRIG='N', 'N',.w_DAPREZZO<>0 ,LEFT(ALLTRIM(.w_ARTIPRIG)+'D',1), Not empty(.w_DATIPRIG),.w_DATIPRIG,LEFT(ALLTRIM(.w_ARTIPRIG)+'D',1)))
          .w_DATIPRI2 = ICASE(.w_Isahe,.w_ETIPRI2,Isahr(),.w_RTIPRI2,iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER<>'S', 'N',.w_DAPREZZO<>0 ,LEFT(ALLTRIM(.w_ARTIPRI2)+'D',1), Not empty(.w_DATIPRI2),.w_DATIPRI2,LEFT(ALLTRIM(.w_ARTIPRI2)+'D',1)))
          .w_ATIPRIG = .w_DATIPRIG
          .w_ATIPRI2 = .w_DATIPRI2
    endwith
  endproc
  proc Calculate_TITGOTIGZK()
    with this
          * --- Aggiornamento totali  necesssario ...
          .w_TOTDURMIN = MOD(.w_TOTALMIN,60)
          .w_TOTDURORE = .w_TOTALORE+INT(.w_TOTALMIN/60)
    endwith
  endproc
  proc Calculate_UEVIUTMRUX()
    with this
          * --- Valorizzo pratica
          .w_CODPRATICA = .oparentobject.w_codpratica
          .w_PRNUMPRA = iif(Not empty(.w_CodPratica),.w_CodPratica,.w_PRNUMPRA)
          .link_2_7('Full')
    endwith
  endproc
  proc Calculate_AZIXBNPXNY()
    with this
          * --- Centri di costo e ricavo
          .w_CEN_CauDoc = IIF(.w_ISAHE AND This.oparentobject.w_FLCCRAUTO='S',SearchVoc(this,'T',.w_DACODICE,'','','',''),SPACE(15))
          .w_DACENCOS = ICASE(!EMPTY(.w_CEN_CauDoc),.w_CEN_CauDoc,!.w_ISAHE,.w_OLDCEN,Space(15))
          .w_DACENRIC = Iif(!Empty(.w_Cen_Caudoc),.w_Cen_Caudoc,.w_Oldcen)
    endwith
  endproc
  proc Calculate_NWWRTSZWQS()
    with this
          * --- Aggiorno commessa ricavi
          .w_DACOMRIC = IIF(Empty(.w_DACOMRIC),.w_DACODCOM,.w_DACOMRIC)
    endwith
  endproc
  proc Calculate_LDKASKAOZX()
    with this
          * --- Ricalcolo durata eff. (ore e minuti)
          .w_PROREEFF = Min(IIF(.w_CHKTEMP='S', INT(.w_DUR_ORE * .w_PRQTAMOV), .w_PROREEFF),999)
          .w_PRMINEFF = IIF(.w_CHKTEMP='S', INT(cp_round((.w_DUR_ORE * .w_PRQTAMOV - INT(.w_DUR_ORE * .w_PRQTAMOV)) * 60,0)), .w_PRMINEFF)
          .w_DACOSUNI = IIF(.w_LICOSTO>0 OR (.w_COST_ORA=0 and .w_CHKTEMP<>'S'),.w_LICOSTO,.w_COST_ORA)
          .w_PRCOSINT = IIF(.w_LICOSTO>0 and .w_COST_ORA=0 ,.w_DACOSUNI*.w_PRQTAMOV,(.w_PROREEFF+(.w_PRMINEFF/60))*.w_DACOSUNI)
    endwith
  endproc
  proc Calculate_MLJYOOTGXS()
    with this
          * --- Valorizzo Totali
          .w_TOTQTAORE = .w_TOTDURQTA+INT(.w_TOTDURQTAM/60)
          .w_TOTQTAMIN = Mod(.w_TOTDURQTAM,60)
          .w_TOTDURORE = .w_TOTALORE+INT(.w_TOTALMIN/60)
          .w_TOTDURMIN = MOD(.w_TOTALMIN,60)
    endwith
  endproc
  proc Calculate_EVQATLEGXV()
    with this
          * --- Determina data massima
          GSAG_BCK(this;
              ,'M';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRNUMPRA_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRNUMPRA_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDACODATT_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDACODATT_2_15.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRDESPRE_2_21.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRDESPRE_2_21.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRUNIMIS_2_22.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRUNIMIS_2_22.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRQTAMOV_2_24.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRQTAMOV_2_24.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDAPREZZO_2_26.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDAPREZZO_2_26.mCond()
    this.oPgFrm.Page1.oPag.oPRCODLIS_2_28.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPRCODLIS_2_28.mCond()
    this.oPgFrm.Page1.oPag.oATIPRIG_2_29.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oATIPRIG_2_29.mCond()
    this.oPgFrm.Page1.oPag.oATIPRI2_2_30.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oATIPRI2_2_30.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFLCOMP_2_166.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oFLCOMP_2_166.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_88.visible=!this.oPgFrm.Page1.oPag.oStr_1_88.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_6.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_6.mHide()
    this.oPgFrm.Page1.oPag.oETIPRIG_2_31.visible=!this.oPgFrm.Page1.oPag.oETIPRIG_2_31.mHide()
    this.oPgFrm.Page1.oPag.oRTIPRIG_2_33.visible=!this.oPgFrm.Page1.oPag.oRTIPRIG_2_33.mHide()
    this.oPgFrm.Page1.oPag.oRTIPRI2_2_34.visible=!this.oPgFrm.Page1.oPag.oRTIPRI2_2_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_93.visible=!this.oPgFrm.Page1.oPag.oBtn_2_93.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_188.visible=!this.oPgFrm.Page1.oPag.oBtn_2_188.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_189.visible=!this.oPgFrm.Page1.oPag.oBtn_2_189.mHide()
    this.oPgFrm.Page1.oPag.oPRIMPANT_2_199.visible=!this.oPgFrm.Page1.oPag.oPRIMPANT_2_199.mHide()
    this.oPgFrm.Page1.oPag.oPRIMPSPE_2_200.visible=!this.oPgFrm.Page1.oPag.oPRIMPSPE_2_200.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsag_mpr
    If cEvent='w_DACODATT Changed'
       this.NotifyEvent('Link')
    Endif
    
    If cEvent='w_PRNUMPRA Changed'
       this.NotifyEvent('TarConc')
    Endif
    
    If INLIST(cEvent, 'w_DACODATT Changed', 'w_PRNUMPRA Changed', 'w_PRCODLIS Changed', 'w_PRCODVAL Changed') 
       * this.w_PRQTAMOV=IIF(this.w_TIPART='DE', 0, 1)
       this.NotifyEvent('Calcola')
    endif
    
    If (cEvent='w_PRQTAMOV Changed' or cEvent='w_PRUNIMIS Changed') 
       this.NotifyEvent('CalcNot')
    endif
    
    if cevent='w_PR__DATA Changed' and (This.w_DARIFPRE>0 or This.w_DARIGPRE='S')
       Ah_errormsg("Attenzione, riga collegata! Impossibile modificare la data")
       This.w_PR__DATA=This.o_PR__DATA
    endif
    if cevent='w_DAPREZZO Changed' and (This.w_TIPART='FO' and This.w_DARIGPRE='S' and This.w_DAPREZZO=0)
       Ah_errormsg("Attenzione, riga collegata! Impossibile azzerare la tariffa")
       This.w_DAPREZZO=This.o_DAPREZZO
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_68.Event(cEvent)
        if lower(cEvent)==lower("DeselezionaPresenti")
          .Calculate_HSUFWAGNQY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_FLCOMP Changed")
          .Calculate_GZPILQVQEQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ATIPRIG Changed")
          .Calculate_NNUSMMZHPX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Deseleziona_BUP")
          .Calculate_HACHIGEILQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init Row")
          .Calculate_DIJYXDBEQS()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_79.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_80.Event(cEvent)
        if lower(cEvent)==lower("DefaultValue")
          .Calculate_GIQYYQFYNY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Aggpra")
          .Calculate_UEVIUTMRUX()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_170.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_171.Event(cEvent)
        if lower(cEvent)==lower("w_PRNUMPRA Changed")
          .Calculate_NWWRTSZWQS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Aggcosto")
          .Calculate_LDKASKAOZX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Row deleted")
          .Calculate_MLJYOOTGXS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Calcdatamax") or lower(cEvent)==lower("w_PR__DATA Changed")
          .Calculate_EVQATLEGXV()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_212.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_92.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_93.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsag_mpr
     If cEvent='w_DACODATT Changed' AND this.w_FL_BUP='N'
          * Nel caso in cui il flag FL_BUP sia attivo non deve notificare 
          * poich� stiamo forzando l'inserimento delle prestazioni
          * da duplicazione o da F23
          this.NotifyEvent('CalcPrest')
     endif 
     If cEvent='w_DACODATT Changed' or cEvent='w_DACODICE Changed' or cEvent='w_PRQTAMOV Changed' OR cEvent='w_PRUNIMIS Changed'
         this.NotifyEvent('Aggcosto')
     endif
     If cevent='Riapplica' AND Ah_YesNo('Attenzione! Spese ed anticipazioni inserite in precedenza saranno eliminate. Continuare?')
        This.Notifyevent('Elispese')
        This.set('w_DARIGPRE' , 'N', .T. ,.T.)
        This.Notifyevent('CalcPrest')
        This.Refresh()
     Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LetturaParAlte
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LetturaParAlte) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LetturaParAlte)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PANOEDES,PAFLGZER,PAGENPRE,PAVISPRE";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LetturaParAlte);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LetturaParAlte)
            select PACODAZI,PANOEDES,PAFLGZER,PAGENPRE,PAVISPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LetturaParAlte = NVL(_Link_.PACODAZI,space(5))
      this.w_NOEDDES = NVL(_Link_.PANOEDES,space(1))
      this.w_FLGZER = NVL(_Link_.PAFLGZER,space(1))
      this.w_GENPRE = NVL(_Link_.PAGENPRE,space(10))
      this.w_VISPRESTAZ = NVL(_Link_.PAVISPRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LetturaParAlte = space(5)
      endif
      this.w_NOEDDES = space(1)
      this.w_FLGZER = space(1)
      this.w_GENPRE = space(10)
      this.w_VISPRESTAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LetturaParAlte Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRES
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPCODICE,DPCOSORA";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_CODRES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPCODICE',this.w_CODRES)
            select DPCODICE,DPCOSORA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRES = NVL(_Link_.DPCODICE,space(5))
      this.w_COST_ORA = NVL(_Link_.DPCOSORA,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODRES = space(5)
      endif
      this.w_COST_ORA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=APPOAZI
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_APPOAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_APPOAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACAUATT,PACODLIS";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_APPOAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_APPOAZI)
            select PACODAZI,PACAUATT,PACODLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_APPOAZI = NVL(_Link_.PACODAZI,space(5))
      this.w_CAUATT = NVL(_Link_.PACAUATT,space(20))
      this.w_CODLIS = NVL(_Link_.PACODLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_APPOAZI = space(5)
      endif
      this.w_CAUATT = space(20)
      this.w_CODLIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_APPOAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLIS
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CODLIS)
            select LSCODLIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_VALPAR = NVL(_Link_.LSVALLIS,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODLIS = space(5)
      endif
      this.w_VALPAR = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_AGEN_IDX,3]
    i_lTable = "PAR_AGEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2], .t., this.PAR_AGEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PACAUPRE,PALISACQ";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_READAZI)
            select PACODAZI,PACAUPRE,PALISACQ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.PACODAZI,space(10))
      this.w_CAUSALE = NVL(_Link_.PACAUPRE,space(20))
      this.w_LISACQ = NVL(_Link_.PALISACQ,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(10)
      endif
      this.w_CAUSALE = space(20)
      this.w_LISACQ = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_AGEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_AGEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSALE
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAUMATTI_IDX,3]
    i_lTable = "CAUMATTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2], .t., this.CAUMATTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSALE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSALE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CAFLANAL,CACAUDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CAUSALE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CAUSALE)
            select CACODICE,CAFLANAL,CACAUDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSALE = NVL(_Link_.CACODICE,space(20))
      this.w_FLANAL = NVL(_Link_.CAFLANAL,space(1))
      this.w_CAUDOC = NVL(_Link_.CACAUDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSALE = space(20)
      endif
      this.w_FLANAL = space(1)
      this.w_CAUDOC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAUMATTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAUMATTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSALE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUN
  func Link_1_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_APPOAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_APPOAZI;
                       ,'BUCODICE',this.w_CODBUN)
            select BUCODAZI,BUCODICE,BUFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUN = NVL(_Link_.BUCODICE,space(10))
      this.w_BUFLANAL = NVL(_Link_.BUFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUN = space(10)
      endif
      this.w_BUFLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODICE
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DACODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DACODICE))
          select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_DACODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_DACODICE)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_DACODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_DACODICE)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDACODICE_2_6'),i_cWhere,'GSMA_BZA',"Prestazioni",'GSAGZKPM.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DACODICE)
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODICE = NVL(_Link_.CACODICE,space(41))
      this.w_PRDESPRE = NVL(_Link_.CADESART,space(40))
      this.w_PRDESAGG = NVL(_Link_.CADESSUP,space(0))
      this.w_ECOD1ATT = NVL(_Link_.CACODART,space(20))
      this.w_EUNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_ECACODART = NVL(_Link_.CACODART,space(20))
      this.w_ECADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_ETIPSER = NVL(_Link_.CA__TIPO,space(1))
      this.w_EOPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_EMOLTI3 = NVL(_Link_.CAMOLTIP,0)
    else
      if i_cCtrl<>'Load'
        this.w_DACODICE = space(41)
      endif
      this.w_PRDESPRE = space(40)
      this.w_PRDESAGG = space(0)
      this.w_ECOD1ATT = space(20)
      this.w_EUNMIS3 = space(3)
      this.w_ECACODART = space(20)
      this.w_ECADTOBSO = ctod("  /  /  ")
      this.w_ETIPSER = space(1)
      this.w_EOPERA3 = space(1)
      this.w_EMOLTI3 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_ECACODART, "FM-FO-DE") AND (EMPTY(.w_ECADTOBSO) OR .w_ECADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_DACODICE = space(41)
        this.w_PRDESPRE = space(40)
        this.w_PRDESAGG = space(0)
        this.w_ECOD1ATT = space(20)
        this.w_EUNMIS3 = space(3)
        this.w_ECACODART = space(20)
        this.w_ECADTOBSO = ctod("  /  /  ")
        this.w_ETIPSER = space(1)
        this.w_EOPERA3 = space(1)
        this.w_EMOLTI3 = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.CACODICE as CACODICE206"+ ",link_2_6.CADESART as CADESART206"+ ",link_2_6.CADESSUP as CADESSUP206"+ ",link_2_6.CACODART as CACODART206"+ ",link_2_6.CAUNIMIS as CAUNIMIS206"+ ",link_2_6.CACODART as CACODART206"+ ",link_2_6.CADTOBSO as CADTOBSO206"+ ",link_2_6.CA__TIPO as CA__TIPO206"+ ",link_2_6.CAOPERAT as CAOPERAT206"+ ",link_2_6.CAMOLTIP as CAMOLTIP206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on RAP_PRES.DACODICE=link_2_6.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and RAP_PRES.DACODICE=link_2_6.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRNUMPRA
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRNUMPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZZ',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_PRNUMPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_PRNUMPRA))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRNUMPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_PRNUMPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_PRNUMPRA)+"%");

            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRNUMPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oPRNUMPRA_2_7'),i_cWhere,'GSAR_BZZ',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRNUMPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_PRNUMPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_PRNUMPRA)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRNUMPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_NOMEPRA = NVL(_Link_.CNDESCAN,space(100))
      this.w_LCODVAL = NVL(_Link_.CNCODVAL,space(3))
      this.w_VALCOM = NVL(_Link_.CNCODVAL,space(3))
      this.w_CCODLIS = NVL(_Link_.CNCODLIS,space(5))
      this.w_FLAPON = NVL(_Link_.CNFLAPON,space(1))
      this.w_ENTE = NVL(_Link_.CN__ENTE,space(10))
      this.w_UFFICI = NVL(_Link_.CNUFFICI,space(10))
      this.w_FLVALO = NVL(_Link_.CNFLVALO,space(1))
      this.w_IMPORT = NVL(_Link_.CNIMPORT,0)
      this.w_CALDIR = NVL(_Link_.CNCALDIR,space(1))
      this.w_COECAL = NVL(_Link_.CNCOECAL,0)
      this.w_PARASS = NVL(_Link_.CNPARASS,0)
      this.w_FLAMPA = NVL(_Link_.CNFLAMPA,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
      this.w_TARTEM = NVL(_Link_.CNTARTEM,space(1))
      this.w_TARCON = NVL(_Link_.CNTARCON,0)
      this.w_FLVALO1 = NVL(_Link_.CNFLDIND,space(1))
      this.w_CONTRACN = NVL(_Link_.CNCONTRA,space(5))
      this.w_LOCALI = NVL(_Link_.CNLOCALI,space(30))
      this.w_CNDATFIN = NVL(cp_ToDate(_Link_.CNDATFIN),ctod("  /  /  "))
      this.w_CNTIPPRA = NVL(_Link_.CNTIPPRA,space(10))
      this.w_CNMATOBB = NVL(_Link_.CNMATOBB,space(1))
      this.w_CNASSCTP = NVL(_Link_.CNASSCTP,space(1))
      this.w_CNCOMPLX = NVL(_Link_.CNCOMPLX,space(1))
      this.w_CNPROFMT = NVL(_Link_.CNPROFMT,space(1))
      this.w_CNESIPOS = NVL(_Link_.CNESIPOS,space(1))
      this.w_CNPERPLX = NVL(_Link_.CNPERPLX,0)
      this.w_CNPERPOS = NVL(_Link_.CNPERPOS,0)
      this.w_CNFLVMLQ = NVL(_Link_.CNFLVMLQ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PRNUMPRA = space(15)
      endif
      this.w_NOMEPRA = space(100)
      this.w_LCODVAL = space(3)
      this.w_VALCOM = space(3)
      this.w_CCODLIS = space(5)
      this.w_FLAPON = space(1)
      this.w_ENTE = space(10)
      this.w_UFFICI = space(10)
      this.w_FLVALO = space(1)
      this.w_IMPORT = 0
      this.w_CALDIR = space(1)
      this.w_COECAL = 0
      this.w_PARASS = 0
      this.w_FLAMPA = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TARTEM = space(1)
      this.w_TARCON = 0
      this.w_FLVALO1 = space(1)
      this.w_CONTRACN = space(5)
      this.w_LOCALI = space(30)
      this.w_CNDATFIN = ctod("  /  /  ")
      this.w_CNTIPPRA = space(10)
      this.w_CNMATOBB = space(1)
      this.w_CNASSCTP = space(1)
      this.w_CNCOMPLX = space(1)
      this.w_CNPROFMT = space(1)
      this.w_CNESIPOS = space(1)
      this.w_CNPERPLX = 0
      this.w_CNPERPOS = 0
      this.w_CNFLVMLQ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_PRNUMPRA = space(15)
        this.w_NOMEPRA = space(100)
        this.w_LCODVAL = space(3)
        this.w_VALCOM = space(3)
        this.w_CCODLIS = space(5)
        this.w_FLAPON = space(1)
        this.w_ENTE = space(10)
        this.w_UFFICI = space(10)
        this.w_FLVALO = space(1)
        this.w_IMPORT = 0
        this.w_CALDIR = space(1)
        this.w_COECAL = 0
        this.w_PARASS = 0
        this.w_FLAMPA = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TARTEM = space(1)
        this.w_TARCON = 0
        this.w_FLVALO1 = space(1)
        this.w_CONTRACN = space(5)
        this.w_LOCALI = space(30)
        this.w_CNDATFIN = ctod("  /  /  ")
        this.w_CNTIPPRA = space(10)
        this.w_CNMATOBB = space(1)
        this.w_CNASSCTP = space(1)
        this.w_CNCOMPLX = space(1)
        this.w_CNPROFMT = space(1)
        this.w_CNESIPOS = space(1)
        this.w_CNPERPLX = 0
        this.w_CNPERPOS = 0
        this.w_CNFLVMLQ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_bRes and i_cCtrl='Drop'
      with this
        if i_bRes and !(!.w_EDITCODPRA OR ((.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) and (.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))))
          i_bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
          if not(i_bRes)
            this.w_PRNUMPRA = space(15)
            this.w_NOMEPRA = space(100)
            this.w_LCODVAL = space(3)
            this.w_VALCOM = space(3)
            this.w_CCODLIS = space(5)
            this.w_FLAPON = space(1)
            this.w_ENTE = space(10)
            this.w_UFFICI = space(10)
            this.w_FLVALO = space(1)
            this.w_IMPORT = 0
            this.w_CALDIR = space(1)
            this.w_COECAL = 0
            this.w_PARASS = 0
            this.w_FLAMPA = space(1)
            this.w_DATOBSO = ctod("  /  /  ")
            this.w_TARTEM = space(1)
            this.w_TARCON = 0
            this.w_FLVALO1 = space(1)
            this.w_CONTRACN = space(5)
            this.w_LOCALI = space(30)
            this.w_CNDATFIN = ctod("  /  /  ")
            this.w_CNTIPPRA = space(10)
            this.w_CNMATOBB = space(1)
            this.w_CNASSCTP = space(1)
            this.w_CNCOMPLX = space(1)
            this.w_CNPROFMT = space(1)
            this.w_CNESIPOS = space(1)
            this.w_CNPERPLX = 0
            this.w_CNPERPOS = 0
            this.w_CNFLVMLQ = space(1)
          endif
        endif
      endwith
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRNUMPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 30 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+30<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.CNCODCAN as CNCODCAN207"+ ",link_2_7.CNDESCAN as CNDESCAN207"+ ",link_2_7.CNCODVAL as CNCODVAL207"+ ",link_2_7.CNCODVAL as CNCODVAL207"+ ",link_2_7.CNCODLIS as CNCODLIS207"+ ",link_2_7.CNFLAPON as CNFLAPON207"+ ",link_2_7.CN__ENTE as CN__ENTE207"+ ",link_2_7.CNUFFICI as CNUFFICI207"+ ",link_2_7.CNFLVALO as CNFLVALO207"+ ",link_2_7.CNIMPORT as CNIMPORT207"+ ",link_2_7.CNCALDIR as CNCALDIR207"+ ",link_2_7.CNCOECAL as CNCOECAL207"+ ",link_2_7.CNPARASS as CNPARASS207"+ ",link_2_7.CNFLAMPA as CNFLAMPA207"+ ",link_2_7.CNDTOBSO as CNDTOBSO207"+ ",link_2_7.CNTARTEM as CNTARTEM207"+ ",link_2_7.CNTARCON as CNTARCON207"+ ",link_2_7.CNFLDIND as CNFLDIND207"+ ",link_2_7.CNCONTRA as CNCONTRA207"+ ",link_2_7.CNLOCALI as CNLOCALI207"+ ",link_2_7.CNDATFIN as CNDATFIN207"+ ",link_2_7.CNTIPPRA as CNTIPPRA207"+ ",link_2_7.CNMATOBB as CNMATOBB207"+ ",link_2_7.CNASSCTP as CNASSCTP207"+ ",link_2_7.CNCOMPLX as CNCOMPLX207"+ ",link_2_7.CNPROFMT as CNPROFMT207"+ ",link_2_7.CNESIPOS as CNESIPOS207"+ ",link_2_7.CNPERPLX as CNPERPLX207"+ ",link_2_7.CNPERPOS as CNPERPOS207"+ ",link_2_7.CNFLVMLQ as CNFLVMLQ207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on RAP_PRES.PRNUMPRA=link_2_7.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+30
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and RAP_PRES.PRNUMPRA=link_2_7.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+30
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DACODATT
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DACODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DACODATT))
          select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODATT)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_DACODATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_DACODATT)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStrODBC(trim(this.w_DACODATT)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESSUP like "+cp_ToStr(trim(this.w_DACODATT)+"%");

            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DACODATT) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDACODATT_2_15'),i_cWhere,'GSMA_BZA',"Prestazioni",''+"Inspre"+'.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DACODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DACODATT)
            select CACODICE,CADESART,CADESSUP,CACODART,CAUNIMIS,CADTOBSO,CA__TIPO,CAOPERAT,CAMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODATT = NVL(_Link_.CACODICE,space(20))
      this.w_PRDESPRE = NVL(_Link_.CADESART,space(40))
      this.w_PRDESAGG = NVL(_Link_.CADESSUP,space(0))
      this.w_RCOD1ATT = NVL(_Link_.CACODART,space(20))
      this.w_RUNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_RCACODART = NVL(_Link_.CACODART,space(20))
      this.w_RCADTOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_RTIPSER = NVL(_Link_.CA__TIPO,space(1))
      this.w_ROPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_RMOLTI3 = NVL(_Link_.CAMOLTIP,0)
    else
      if i_cCtrl<>'Load'
        this.w_DACODATT = space(20)
      endif
      this.w_PRDESPRE = space(40)
      this.w_PRDESAGG = space(0)
      this.w_RCOD1ATT = space(20)
      this.w_RUNMIS3 = space(3)
      this.w_RCACODART = space(20)
      this.w_RCADTOBSO = ctod("  /  /  ")
      this.w_RTIPSER = space(1)
      this.w_ROPERA3 = space(1)
      this.w_RMOLTI3 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        endif
        this.w_DACODATT = space(20)
        this.w_PRDESPRE = space(40)
        this.w_PRDESAGG = space(0)
        this.w_RCOD1ATT = space(20)
        this.w_RUNMIS3 = space(3)
        this.w_RCACODART = space(20)
        this.w_RCADTOBSO = ctod("  /  /  ")
        this.w_RTIPSER = space(1)
        this.w_ROPERA3 = space(1)
        this.w_RMOLTI3 = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_15.CACODICE as CACODICE215"+ ",link_2_15.CADESART as CADESART215"+ ",link_2_15.CADESSUP as CADESSUP215"+ ",link_2_15.CACODART as CACODART215"+ ",link_2_15.CAUNIMIS as CAUNIMIS215"+ ",link_2_15.CACODART as CACODART215"+ ",link_2_15.CADTOBSO as CADTOBSO215"+ ",link_2_15.CA__TIPO as CA__TIPO215"+ ",link_2_15.CAOPERAT as CAOPERAT215"+ ",link_2_15.CAMOLTIP as CAMOLTIP215"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_15 on RAP_PRES.DACODATT=link_2_15.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_15"
          i_cKey=i_cKey+'+" and RAP_PRES.DACODATT=link_2_15.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRUNIMIS
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_PRUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_PRUNIMIS))
          select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oPRUNIMIS_2_22'),i_cWhere,'GSAR_AUM',"Unita di misura",'GSVEUMDV.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_PRUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_PRUNIMIS)
            select UMCODICE,UMDESCRI,UMFLTEMP,UMDURORE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_DESUNI = NVL(_Link_.UMDESCRI,space(35))
      this.w_CHKTEMP = NVL(_Link_.UMFLTEMP,space(1))
      this.w_DUR_ORE = NVL(_Link_.UMDURORE,0)
      this.w_FL_FRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PRUNIMIS = space(3)
      endif
      this.w_DESUNI = space(35)
      this.w_CHKTEMP = space(1)
      this.w_DUR_ORE = 0
      this.w_FL_FRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSERG='S' OR .w_PRUNIMIS=.w_UNMIS1 OR .w_PRUNIMIS=.w_UNMIS2 OR .w_PRUNIMIS=.w_UNMIS3
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire l'unit� di misura principale o secondaria della prestazione")
        endif
        this.w_PRUNIMIS = space(3)
        this.w_DESUNI = space(35)
        this.w_CHKTEMP = space(1)
        this.w_DUR_ORE = 0
        this.w_FL_FRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_22.UMCODICE as UMCODICE222"+ ",link_2_22.UMDESCRI as UMDESCRI222"+ ",link_2_22.UMFLTEMP as UMFLTEMP222"+ ",link_2_22.UMDURORE as UMDURORE222"+ ",link_2_22.UMFLFRAZ as UMFLFRAZ222"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_22 on RAP_PRES.PRUNIMIS=link_2_22.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_22"
          i_cKey=i_cKey+'+" and RAP_PRES.PRUNIMIS=link_2_22.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCODVAL
  func Link_2_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PRCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PRCODVAL)
            select VACODVAL,VADECTOT,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRCODVAL = space(3)
      endif
      this.w_DECTOT = 0
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_27(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_27.VACODVAL as VACODVAL227"+ ",link_2_27.VADECTOT as VADECTOT227"+ ",link_2_27.VADECUNI as VADECUNI227"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_27 on RAP_PRES.PRCODVAL=link_2_27.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_27"
          i_cKey=i_cKey+'+" and RAP_PRES.PRCODVAL=link_2_27.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCODLIS
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_PRCODLIS)+"%");
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_PRCODVAL);

          i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSVALLIS,LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSVALLIS',this.w_PRCODVAL;
                     ,'LSCODLIS',trim(this.w_PRCODLIS))
          select LSVALLIS,LSCODLIS,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSVALLIS,LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PRCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(oSource.parent,'oPRCODLIS_2_28'),i_cWhere,'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PRCODVAL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LSVALLIS,LSCODLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valuta listino incongruente o listino gestito a sconti")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LSVALLIS="+cp_ToStrODBC(this.w_PRCODVAL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',oSource.xKey(1);
                       ,'LSCODLIS',oSource.xKey(2))
            select LSVALLIS,LSCODLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSVALLIS,LSCODLIS,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_PRCODLIS);
                   +" and LSVALLIS="+cp_ToStrODBC(this.w_PRCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSVALLIS',this.w_PRCODVAL;
                       ,'LSCODLIS',this.w_PRCODLIS)
            select LSVALLIS,LSCODLIS,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_SCOLIS = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODLIS = space(5)
      endif
      this.w_VALLIS = space(3)
      this.w_SCOLIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VALLIS=.w_PRCODVAL 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta listino incongruente o listino gestito a sconti")
        endif
        this.w_PRCODLIS = space(5)
        this.w_VALLIS = space(3)
        this.w_SCOLIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSVALLIS,1)+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODSED
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_DACODSED);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_NOTIPCLI);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_NOCODCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_NOTIPCLI;
                       ,'DDCODICE',this.w_NOCODCLI;
                       ,'DDCODDES',this.w_DACODSED)
            select DDTIPCON,DDCODICE,DDCODDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODSED = NVL(_Link_.DDCODDES,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DACODSED = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ENTE
  func Link_2_68(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_ENTI_IDX,3]
    i_lTable = "PRA_ENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2], .t., this.PRA_ENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EPCODICE,EP__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where EPCODICE="+cp_ToStrODBC(this.w_ENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EPCODICE',this.w_ENTE)
            select EPCODICE,EP__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ENTE = NVL(_Link_.EPCODICE,space(10))
      this.w_TIPOENTE = NVL(_Link_.EP__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ENTE = space(10)
      endif
      this.w_TIPOENTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_ENTI_IDX,2])+'\'+cp_ToStr(_Link_.EPCODICE,1)
      cp_ShowWarn(i_cKey,this.PRA_ENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCODCLI
  func Link_2_97(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS,ANCATCOM";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_NOCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_NOTIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_NOTIPCLI;
                       ,'ANCODICE',this.w_NOCODCLI)
            select ANTIPCON,ANCODICE,ANSCORPO,ANNUMLIS,ANCATCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_FLSCOR = NVL(_Link_.ANSCORPO,space(1))
      this.w_NUMLIS = NVL(_Link_.ANNUMLIS,space(5))
      this.w_CATCOM = NVL(_Link_.ANCATCOM,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODCLI = space(15)
      endif
      this.w_FLSCOR = space(1)
      this.w_NUMLIS = space(5)
      this.w_CATCOM = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTRACN
  func Link_2_160(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PRA_CONT_IDX,3]
    i_lTable = "PRA_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PRA_CONT_IDX,2], .t., this.PRA_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PRA_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTRACN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTRACN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPCODCON,CPTIPCON,CPLISCOL,CPSTATUS";
                   +" from "+i_cTable+" "+i_lTable+" where CPCODCON="+cp_ToStrODBC(this.w_CONTRACN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPCODCON',this.w_CONTRACN)
            select CPCODCON,CPTIPCON,CPLISCOL,CPSTATUS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTRACN = NVL(_Link_.CPCODCON,space(5))
      this.w_TIPCONCO = NVL(_Link_.CPTIPCON,space(1))
      this.w_LISCOLCO = NVL(_Link_.CPLISCOL,space(5))
      this.w_STATUSCO = NVL(_Link_.CPSTATUS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONTRACN = space(5)
      endif
      this.w_TIPCONCO = space(1)
      this.w_LISCOLCO = space(5)
      this.w_STATUSCO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PRA_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CPCODCON,1)
      cp_ShowWarn(i_cKey,this.PRA_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTRACN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODNOM
  func Link_2_196(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE,NOCODCLI,NODTOBSO,NOTIPNOM,NOTIPCLI,NOCODLIN,NONUMLIS";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_DACODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_DACODNOM)
            select NOCODICE,NOCODCLI,NODTOBSO,NOTIPNOM,NOTIPCLI,NOCODLIN,NONUMLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODNOM = NVL(_Link_.NOCODICE,space(15))
      this.w_NOCODCLI = NVL(_Link_.NOCODCLI,space(15))
      this.w_DTOBSNOM = NVL(cp_ToDate(_Link_.NODTOBSO),ctod("  /  /  "))
      this.w_NOTIPNOM = NVL(_Link_.NOTIPNOM,space(1))
      this.w_NOTIPCLI = NVL(_Link_.NOTIPCLI,space(1))
      this.w_CODLIN = NVL(_Link_.NOCODLIN,space(3))
      this.w_NCODLIS = NVL(_Link_.NONUMLIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DACODNOM = space(15)
      endif
      this.w_NOCODCLI = space(15)
      this.w_DTOBSNOM = ctod("  /  /  ")
      this.w_NOTIPNOM = space(1)
      this.w_NOTIPCLI = space(1)
      this.w_CODLIN = space(3)
      this.w_NCODLIS = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DTOBSNOM) Or .w_DTOBSNOM >= .w_PR__DATA
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DACODNOM = space(15)
        this.w_NOCODCLI = space(15)
        this.w_DTOBSNOM = ctod("  /  /  ")
        this.w_NOTIPNOM = space(1)
        this.w_NOTIPCLI = space(1)
        this.w_CODLIN = space(3)
        this.w_NCODLIS = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_196(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.OFF_NOMI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_196.NOCODICE as NOCODICE396"+ ",link_2_196.NOCODCLI as NOCODCLI396"+ ",link_2_196.NODTOBSO as NODTOBSO396"+ ",link_2_196.NOTIPNOM as NOTIPNOM396"+ ",link_2_196.NOTIPCLI as NOTIPCLI396"+ ",link_2_196.NOCODLIN as NOCODLIN396"+ ",link_2_196.NONUMLIS as NONUMLIS396"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_196 on RAP_PRES.DACODNOM=link_2_196.NOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_196"
          i_cKey=i_cKey+'+" and RAP_PRES.DACODNOM=link_2_196.NOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPRCODVAL_2_27.value==this.w_PRCODVAL)
      this.oPgFrm.Page1.oPag.oPRCODVAL_2_27.value=this.w_PRCODVAL
      replace t_PRCODVAL with this.oPgFrm.Page1.oPag.oPRCODVAL_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODLIS_2_28.value==this.w_PRCODLIS)
      this.oPgFrm.Page1.oPag.oPRCODLIS_2_28.value=this.w_PRCODLIS
      replace t_PRCODLIS with this.oPgFrm.Page1.oPag.oPRCODLIS_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oATIPRIG_2_29.RadioValue()==this.w_ATIPRIG)
      this.oPgFrm.Page1.oPag.oATIPRIG_2_29.SetRadio()
      replace t_ATIPRIG with this.oPgFrm.Page1.oPag.oATIPRIG_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oATIPRI2_2_30.RadioValue()==this.w_ATIPRI2)
      this.oPgFrm.Page1.oPag.oATIPRI2_2_30.SetRadio()
      replace t_ATIPRI2 with this.oPgFrm.Page1.oPag.oATIPRI2_2_30.value
    endif
    if not(this.oPgFrm.Page1.oPag.oETIPRIG_2_31.RadioValue()==this.w_ETIPRIG)
      this.oPgFrm.Page1.oPag.oETIPRIG_2_31.SetRadio()
      replace t_ETIPRIG with this.oPgFrm.Page1.oPag.oETIPRIG_2_31.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRTIPRIG_2_33.RadioValue()==this.w_RTIPRIG)
      this.oPgFrm.Page1.oPag.oRTIPRIG_2_33.SetRadio()
      replace t_RTIPRIG with this.oPgFrm.Page1.oPag.oRTIPRIG_2_33.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRTIPRI2_2_34.value==this.w_RTIPRI2)
      this.oPgFrm.Page1.oPag.oRTIPRI2_2_34.value=this.w_RTIPRI2
      replace t_RTIPRI2 with this.oPgFrm.Page1.oPag.oRTIPRI2_2_34.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDESAGG_2_40.value==this.w_PRDESAGG)
      this.oPgFrm.Page1.oPag.oPRDESAGG_2_40.value=this.w_PRDESAGG
      replace t_PRDESAGG with this.oPgFrm.Page1.oPag.oPRDESAGG_2_40.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODOPE_2_41.value==this.w_PRCODOPE)
      this.oPgFrm.Page1.oPag.oPRCODOPE_2_41.value=this.w_PRCODOPE
      replace t_PRCODOPE with this.oPgFrm.Page1.oPag.oPRCODOPE_2_41.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATMOD_2_42.value==this.w_PRDATMOD)
      this.oPgFrm.Page1.oPag.oPRDATMOD_2_42.value=this.w_PRDATMOD
      replace t_PRDATMOD with this.oPgFrm.Page1.oPag.oPRDATMOD_2_42.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRVALRIG_2_43.value==this.w_PRVALRIG)
      this.oPgFrm.Page1.oPag.oPRVALRIG_2_43.value=this.w_PRVALRIG
      replace t_PRVALRIG with this.oPgFrm.Page1.oPag.oPRVALRIG_2_43.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPROREEFF_2_45.value==this.w_PROREEFF)
      this.oPgFrm.Page1.oPag.oPROREEFF_2_45.value=this.w_PROREEFF
      replace t_PROREEFF with this.oPgFrm.Page1.oPag.oPROREEFF_2_45.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRMINEFF_2_46.value==this.w_PRMINEFF)
      this.oPgFrm.Page1.oPag.oPRMINEFF_2_46.value=this.w_PRMINEFF
      replace t_PRMINEFF with this.oPgFrm.Page1.oPag.oPRMINEFF_2_46.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCOSINT_2_48.value==this.w_PRCOSINT)
      this.oPgFrm.Page1.oPag.oPRCOSINT_2_48.value=this.w_PRCOSINT
      replace t_PRCOSINT with this.oPgFrm.Page1.oPag.oPRCOSINT_2_48.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPREMIN_2_50.value==this.w_DAPREMIN)
      this.oPgFrm.Page1.oPag.oDAPREMIN_2_50.value=this.w_DAPREMIN
      replace t_DAPREMIN with this.oPgFrm.Page1.oPag.oDAPREMIN_2_50.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDAPREMAX_2_51.value==this.w_DAPREMAX)
      this.oPgFrm.Page1.oPag.oDAPREMAX_2_51.value=this.w_DAPREMAX
      replace t_DAPREMAX with this.oPgFrm.Page1.oPag.oDAPREMAX_2_51.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDAGAZUFF_2_52.value==this.w_DAGAZUFF)
      this.oPgFrm.Page1.oPag.oDAGAZUFF_2_52.value=this.w_DAGAZUFF
      replace t_DAGAZUFF with this.oPgFrm.Page1.oPag.oDAGAZUFF_2_52.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUNI_2_57.value==this.w_DESUNI)
      this.oPgFrm.Page1.oPag.oDESUNI_2_57.value=this.w_DESUNI
      replace t_DESUNI with this.oPgFrm.Page1.oPag.oDESUNI_2_57.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEPRA_2_58.value==this.w_NOMEPRA)
      this.oPgFrm.Page1.oPag.oNOMEPRA_2_58.value=this.w_NOMEPRA
      replace t_NOMEPRA with this.oPgFrm.Page1.oPag.oNOMEPRA_2_58.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDURMIN_3_1.value==this.w_TOTDURMIN)
      this.oPgFrm.Page1.oPag.oTOTDURMIN_3_1.value=this.w_TOTDURMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDURORE_3_6.value==this.w_TOTDURORE)
      this.oPgFrm.Page1.oPag.oTOTDURORE_3_6.value=this.w_TOTDURORE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTQTAORE_3_8.value==this.w_TOTQTAORE)
      this.oPgFrm.Page1.oPag.oTOTQTAORE_3_8.value=this.w_TOTQTAORE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTQTAMIN_3_9.value==this.w_TOTQTAMIN)
      this.oPgFrm.Page1.oPag.oTOTQTAMIN_3_9.value=this.w_TOTQTAMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPRIMPANT_2_199.value==this.w_PRIMPANT)
      this.oPgFrm.Page1.oPag.oPRIMPANT_2_199.value=this.w_PRIMPANT
      replace t_PRIMPANT with this.oPgFrm.Page1.oPag.oPRIMPANT_2_199.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRIMPSPE_2_200.value==this.w_PRIMPSPE)
      this.oPgFrm.Page1.oPag.oPRIMPSPE_2_200.value=this.w_PRIMPSPE
      replace t_PRIMPSPE with this.oPgFrm.Page1.oPag.oPRIMPSPE_2_200.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPR__DATA_2_1.value==this.w_PR__DATA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPR__DATA_2_1.value=this.w_PR__DATA
      replace t_PR__DATA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPR__DATA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_6.value==this.w_DACODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_6.value=this.w_DACODICE
      replace t_DACODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNUMPRA_2_7.value==this.w_PRNUMPRA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNUMPRA_2_7.value=this.w_PRNUMPRA
      replace t_PRNUMPRA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNUMPRA_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_15.value==this.w_DACODATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_15.value=this.w_DACODATT
      replace t_DACODATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRDESPRE_2_21.value==this.w_PRDESPRE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRDESPRE_2_21.value=this.w_PRDESPRE
      replace t_PRDESPRE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRDESPRE_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRUNIMIS_2_22.value==this.w_PRUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRUNIMIS_2_22.value=this.w_PRUNIMIS
      replace t_PRUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRUNIMIS_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMOV_2_24.value==this.w_PRQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMOV_2_24.value=this.w_PRQTAMOV
      replace t_PRQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMOV_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAPREZZO_2_26.value==this.w_DAPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAPREZZO_2_26.value=this.w_DAPREZZO
      replace t_DAPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDAPREZZO_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMP_2_166.RadioValue()==this.w_FLCOMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMP_2_166.SetRadio()
      replace t_FLCOMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMP_2_166.value
    endif
    cp_SetControlsValueExtFlds(this,'RAP_PRES')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(ChkTipArt(.w_ECACODART, "FM-FO-DE") AND (EMPTY(.w_ECADTOBSO) OR .w_ECADTOBSO>=i_datsys)) and not(empty(.w_DACODICE)) and (NOT(Empty(.w_PR__DATA)) AND (NOT(Empty(.w_DACODATT)) or NOT(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODICE_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)) and (.w_EDITCODPRA and ((.w_DARIFPRE=0 and .w_DARIGPRE<>'S'))) and not(empty(.w_PRNUMPRA)) and (NOT(Empty(.w_PR__DATA)) AND (NOT(Empty(.w_DACODATT)) or NOT(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNUMPRA_2_7
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
        case   not(ChkTipArt(.w_RCACODART, "FM-FO-DE") AND (EMPTY(.w_RCADTOBSO) OR .w_RCADTOBSO>=i_datsys)) and ((.w_DARIFPRE=0 and .w_DARIGPRE<>'S' )) and not(empty(.w_DACODATT)) and (NOT(Empty(.w_PR__DATA)) AND (NOT(Empty(.w_DACODATT)) or NOT(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDACODATT_2_15
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Prestazione selezionata inesistente o obsoleta")
        case   not(.w_FLSERG='S' OR .w_PRUNIMIS=.w_UNMIS1 OR .w_PRUNIMIS=.w_UNMIS2 OR .w_PRUNIMIS=.w_UNMIS3) and (.w_TIPART = 'FM' AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S')) and not(empty(.w_PRUNIMIS)) and (NOT(Empty(.w_PR__DATA)) AND (NOT(Empty(.w_DACODATT)) or NOT(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRUNIMIS_2_22
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire l'unit� di misura principale o secondaria della prestazione")
        case   not((.w_PRQTAMOV>0 AND (.w_FL_FRAZ<>'S' OR .w_PRQTAMOV=INT(.w_PRQTAMOV))) OR NOT .w_TIPSER $ 'RM') and (.w_TIPART = 'FM') and (NOT(Empty(.w_PR__DATA)) AND (NOT(Empty(.w_DACODATT)) or NOT(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMOV_2_24
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
        case   not(.w_VALLIS=.w_PRCODVAL ) and ( (.w_DARIFPRE=0 and .w_DARIGPRE<>'S')) and not(empty(.w_PRCODLIS)) and (NOT(Empty(.w_PR__DATA)) AND (NOT(Empty(.w_DACODATT)) or NOT(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oPRCODLIS_2_28
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valuta listino incongruente o listino gestito a sconti")
        case   not((.w_FLANAL='N' OR (.w_RTIPRIG<>'A' AND .w_RTIPRIG<>'E')) AND (!.w_RTIPRIG$'DE' OR !EMPTY(.w_CAUDOC)) AND (!.w_RTIPRIG$'AE' OR !EMPTY(.w_CAUACQ))) and (NOT(Empty(.w_PR__DATA)) AND (NOT(Empty(.w_DACODATT)) or NOT(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oRTIPRIG_2_33
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Selezionare un documento coerente con le causali documenti specificate nel tipo attivit�!")
        case   not(EMPTY(.w_DTOBSNOM) Or .w_DTOBSNOM >= .w_PR__DATA) and not(empty(.w_DACODNOM)) and (NOT(Empty(.w_PR__DATA)) AND (NOT(Empty(.w_DACODATT)) or NOT(Empty(.w_DACODICE))))
          .oNewFocus=.oPgFrm.Page1.oPag.oDACODNOM_2_196
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if NOT(Empty(.w_PR__DATA)) AND (NOT(Empty(.w_DACODATT)) or NOT(Empty(.w_DACODICE)))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DACODRES = this.w_DACODRES
    this.o_LetturaParAlte = this.w_LetturaParAlte
    this.o_CODLIS = this.w_CODLIS
    this.o_PR__DATA = this.w_PR__DATA
    this.o_CENCOS = this.w_CENCOS
    this.o_DCODCEN = this.w_DCODCEN
    this.o_DACODICE = this.w_DACODICE
    this.o_PRNUMPRA = this.w_PRNUMPRA
    this.o_DACODATT = this.w_DACODATT
    this.o_PRUNIMIS = this.w_PRUNIMIS
    this.o_PRQTAMOV = this.w_PRQTAMOV
    this.o_DAPREZZO = this.w_DAPREZZO
    this.o_PRCODVAL = this.w_PRCODVAL
    this.o_ATIPRIG = this.w_ATIPRIG
    this.o_ATIPRI2 = this.w_ATIPRI2
    this.o_ETIPRIG = this.w_ETIPRIG
    this.o_ETIPRI2 = this.w_ETIPRI2
    this.o_RTIPRIG = this.w_RTIPRIG
    this.o_RTIPRI2 = this.w_RTIPRI2
    this.o_PROREEFF = this.w_PROREEFF
    this.o_PRMINEFF = this.w_PRMINEFF
    this.o_DACOSUNI = this.w_DACOSUNI
    this.o_DACODCOM = this.w_DACODCOM
    this.o_DACENCOS = this.w_DACENCOS
    this.o_CCODLIS = this.w_CCODLIS
    this.o_DACENRIC = this.w_DACENRIC
    this.o_TOTDURQTA = this.w_TOTDURQTA
    this.o_TOTDURQTAM = this.w_TOTDURQTAM
    this.o_TOTALORE = this.w_TOTALORE
    this.o_TOTALMIN = this.w_TOTALMIN
    this.o_DACODNOM = this.w_DACODNOM
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT(Empty(t_PR__DATA)) AND (NOT(Empty(t_DACODATT)) or NOT(Empty(t_DACODICE))))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PR__DATA=ctod("  /  /  ")
      .w_CENCOS=space(15)
      .w_DCODCEN=space(15)
      .w_OLDCEN=space(15)
      .w_DACODICE=space(41)
      .w_PRNUMPRA=space(15)
      .w_ECOD1ATT=space(20)
      .w_RCOD1ATT=space(20)
      .w_DATIPRIG=space(1)
      .w_DATIPRI2=space(1)
      .w_ECACODART=space(20)
      .w_RCACODART=space(20)
      .w_DACODATT=space(20)
      .w_CACODART=space(20)
      .w_ARTIPRIG=space(1)
      .w_ARTIPRI2=space(1)
      .w_COD1ATT=space(20)
      .w_NOTIFICA=space(1)
      .w_PRDESPRE=space(40)
      .w_PRUNIMIS=space(3)
      .w_NUMLIS=space(5)
      .w_PRQTAMOV=0
      .w_NCODLIS=space(5)
      .w_DAPREZZO=0
      .w_PRCODVAL=space(3)
      .w_PRCODLIS=space(5)
      .w_ATIPRIG=space(10)
      .w_ATIPRI2=space(10)
      .w_ETIPRIG=space(10)
      .w_ETIPRI2=space(10)
      .w_RTIPRIG=space(10)
      .w_RTIPRI2=space(10)
      .w_DACODSED=space(5)
      .w_DECTOT=0
      .w_DECUNI=0
      .w_CALCPICU=0
      .w_CALCPICT=0
      .w_PRDESAGG=space(0)
      .w_PRCODOPE=0
      .w_PRDATMOD=ctod("  /  /  ")
      .w_PRVALRIG=0
      .w_LICOSTO=0
      .w_PROREEFF=0
      .w_PRMINEFF=0
      .w_DACOSUNI=0
      .w_PRCOSINT=0
      .w_DAFLDEFF=space(1)
      .w_DAPREMIN=0
      .w_DAPREMAX=0
      .w_DAGAZUFF=space(6)
      .w_TIPART=space(2)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_FLSERG=space(1)
      .w_DESUNI=space(35)
      .w_NOMEPRA=space(100)
      .w_UNIMIS=space(3)
      .w_LCODVAL=space(3)
      .w_VALCOM=space(3)
      .w_EUNMIS3=space(3)
      .w_RUNMIS3=space(3)
      .w_OBTEST=ctod("  /  /  ")
      .w_UNMIS3=space(3)
      .w_VALLIS=space(3)
      .w_FLAPON=space(1)
      .w_ENTE=space(10)
      .w_UFFICI=space(10)
      .w_FLVALO=space(1)
      .w_IMPORT=0
      .w_CALDIR=space(1)
      .w_COECAL=0
      .w_PARASS=0
      .w_FLAMPA=space(1)
      .w_CAOVAL=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPOENTE=space(1)
      .w_CHKTEMP=space(1)
      .w_DUR_ORE=0
      .w_ECADTOBSO=ctod("  /  /  ")
      .w_RCADTOBSO=ctod("  /  /  ")
      .w_CADTOBSO=ctod("  /  /  ")
      .w_DACODCOM=space(15)
      .w_DAVOCRIC=space(15)
      .w_DAINICOM=ctod("  /  /  ")
      .w_DAFINCOM=ctod("  /  /  ")
      .w_DA_SEGNO=space(1)
      .w_DACENCOS=space(15)
      .w_DAATTIVI=space(15)
      .w_NOTIPCLI=space(1)
      .w_DTOBSNOM=ctod("  /  /  ")
      .w_NOCODCLI=space(15)
      .w_NOTIPNOM=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_OFDATDOC=ctod("  /  /  ")
      .w_EMOLTI3=0
      .w_RMOLTI3=0
      .w_QTAUM1=0
      .w_MOLTI3=0
      .w_FLUSEP=space(1)
      .w_OPERAT=space(1)
      .w_FLFRAZ1=space(1)
      .w_MODUM2=space(1)
      .w_MOLTIP=0
      .w_PREZUM=space(1)
      .w_ARTIPART=space(2)
      .w_SCOLIS=space(1)
      .w_DAVOCCOS=space(15)
      .w_CCODLIS=space(5)
      .w_CICLO=space(1)
      .w_EOPERA3=space(1)
      .w_ROPERA3=space(1)
      .w_OPERA3=space(1)
      .w_ETIPSER=space(1)
      .w_RTIPSER=space(1)
      .w_TIPSER=space(1)
      .w_DACODLIS=space(5)
      .w_DAPROLIS=space(5)
      .w_DAPROSCO=space(5)
      .w_DASCOLIS=space(5)
      .w_DASCONT1=0
      .w_DASCONT2=0
      .w_DASCONT3=0
      .w_DASCONT4=0
      .w_DACOSUNI=0
      .w_DACOMRIC=space(15)
      .w_DAATTRIC=space(15)
      .w_DACENRIC=space(15)
      .w_FL_FRAZ=space(1)
      .w_DALISACQ=space(5)
      .w_VALACQ=space(3)
      .w_INIACQ=ctod("  /  /  ")
      .w_TIPACQ=space(1)
      .w_FLSACQ=space(1)
      .w_IVAACQ=space(1)
      .w_FLGACQ=space(1)
      .w_FINACQ=ctod("  /  /  ")
      .w_DATCOINI=ctod("  /  /  ")
      .w_DATCOFIN=ctod("  /  /  ")
      .w_DATRIINI=ctod("  /  /  ")
      .w_DATRIFIN=ctod("  /  /  ")
      .w_FLSCOR=space(1)
      .w_TARTEM=space(1)
      .w_TARCON=0
      .w_FLVALO1=space(1)
      .w_VOCECR=space(1)
      .w_TOTQTA=0
      .w_TOTORE=0
      .w_TOTMIN=0
      .w_TOTQTAM=0
      .w_CONTRACN=space(5)
      .w_TIPCONCO=space(1)
      .w_LISCOLCO=space(5)
      .w_STATUSCO=space(1)
      .w_FLCOMP=space(10)
      .w_FLSPAN=space(1)
      .w_DARIFPRE=0
      .w_DARIGPRE=space(1)
      .w_PRESTA=space(1)
      .w_DACONCOD=space(15)
      .w_CEN_CauDoc=space(15)
      .w_DACONTRA=space(10)
      .w_DACODMOD=space(10)
      .w_DACODIMP=space(10)
      .w_DACOCOMP=0
      .w_DARINNOV=0
      .w_DAUNIMIS=space(3)
      .w_DADESATT=space(40)
      .w_FLUNIV=space(1)
      .w_NUMPRE=0
      .w_LOCALI=space(30)
      .w_DALISACQ=space(5)
      .w_FLSCOAC=space(1)
      .w_CNDATFIN=ctod("  /  /  ")
      .w_CATCOM=space(3)
      .w_DACODNOM=space(15)
      .w_PRCODSPE=space(20)
      .w_PRCODANT=space(20)
      .w_PRIMPANT=0
      .w_PRIMPSPE=0
      .w_TIPRIGCA=space(1)
      .w_TIPRI2CA=space(1)
      .w_CNTIPPRA=space(10)
      .w_CNMATOBB=space(1)
      .w_CNASSCTP=space(1)
      .w_CNCOMPLX=space(1)
      .w_CNPROFMT=space(1)
      .w_CNESIPOS=space(1)
      .w_CNPERPLX=0
      .w_CNPERPOS=0
      .w_CNFLVMLQ=space(1)
      .DoRTCalc(1,48,.f.)
        .w_PR__DATA = IIF(.w_FLDTRP='S',NVL(.w_DATMAX,i_datsys),i_datsys)
      .DoRTCalc(50,50,.f.)
        .w_CENCOS = This.oparentobject .w_CENCOS
      .DoRTCalc(52,52,.f.)
        .w_OLDCEN = ICASE(Not Empty(.w_DCODCEN),.w_DCODCEN,Not Empty(.w_CENCOS),.w_CENCOS,.w_DACENCOS)
      .DoRTCalc(54,54,.f.)
      if not(empty(.w_DACODICE))
        .link_2_6('Full')
      endif
        .w_PRNUMPRA = .w_CodPratica
      .DoRTCalc(55,55,.f.)
      if not(empty(.w_PRNUMPRA))
        .link_2_7('Full')
      endif
      .DoRTCalc(56,62,.f.)
      if not(empty(.w_DACODATT))
        .link_2_15('Full')
      endif
        .w_CACODART = IIF(.w_ISAHE,.w_ECACODART,.w_RCACODART)
      .DoRTCalc(64,65,.f.)
        .w_COD1ATT = IIF(.w_ISAHE,.w_ECOD1ATT,.w_RCOD1ATT)
      .DoRTCalc(67,69,.f.)
      if not(empty(.w_PRUNIMIS))
        .link_2_22('Full')
      endif
      .DoRTCalc(70,70,.f.)
        .w_PRQTAMOV = 1
      .DoRTCalc(72,73,.f.)
        .w_PRCODVAL = iif(Not empty(.w_LCODVAL),.w_LCODVAL,g_PERVAL)
      .DoRTCalc(74,74,.f.)
      if not(empty(.w_PRCODVAL))
        .link_2_27('Full')
      endif
        .w_PRCODLIS = .w_CCODLIS
      .DoRTCalc(75,75,.f.)
      if not(empty(.w_PRCODLIS))
        .link_2_28('Full')
      endif
        .w_ATIPRIG = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', .w_DTIPRIG='N', 'N', Not empty(.w_DATIPRIG),.w_DATIPRIG,LEFT(ALLTRIM(.w_ARTIPRIG)+'D',1))
        .w_ATIPRI2 = iCASE(.w_TIPART='FO' and .w_DAPREZZO=0 and .w_FLGZER='N', 'N', Not empty(.w_DATIPRI2),.w_DATIPRI2,LEFT(ALLTRIM(.w_ARTIPRI2)+'D',1))
        .w_ETIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_ETIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRIG = iCASE(Not empty(.w_DATIPRIG),.w_DATIPRIG,icase(Not empty(.w_CAUACQ) and Not empty(.w_CAUDOC),'E',Not empty(.w_CAUACQ),'A','D'))
        .w_RTIPRI2 = iCASE(Not empty(.w_DATIPRI2),.w_DATIPRI2,'D')
        .w_DACODSED = SPACE(5)
      .DoRTCalc(82,82,.f.)
      if not(empty(.w_DACODSED))
        .link_2_35('Full')
      endif
      .DoRTCalc(83,84,.f.)
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
      .DoRTCalc(87,87,.f.)
        .w_PRCODOPE = i_codute
        .w_PRDATMOD = i_datsys
        .w_PRVALRIG = cp_round(.w_PRQTAMOV*.w_DAPREZZO, IIF(.w_NOTIFICA='S' , 0, .w_DECTOT))
      .DoRTCalc(91,91,.f.)
        .w_PROREEFF = min(IIF(.w_CHKTEMP='S', INT(.w_DUR_ORE * .w_PRQTAMOV), 0),999)
        .w_PRMINEFF = IIF(.w_CHKTEMP='S', INT(cp_round((.w_DUR_ORE * .w_PRQTAMOV - INT(.w_DUR_ORE * .w_PRQTAMOV)) * 60,0)), 0)
      .DoRTCalc(94,94,.f.)
        .w_PRCOSINT = IIF(.w_LICOSTO>0 and .w_COST_ORA=0 ,.w_DACOSUNI*.w_PRQTAMOV,(.w_PROREEFF+(.w_PRMINEFF/60))*.w_DACOSUNI)
        .w_DAFLDEFF = "N"
      .DoRTCalc(97,110,.f.)
        .w_OBTEST = i_datsys
        .w_UNMIS3 = IIF(.w_ISAHE,.w_EUNMIS3,.w_RUNMIS3)
      .DoRTCalc(113,115,.f.)
      if not(empty(.w_ENTE))
        .link_2_68('Full')
      endif
      .DoRTCalc(116,122,.f.)
        .w_CAOVAL = IIF(.w_CAOVAL=1 OR .w_CAOVAL=0 OR .w_PRCODVAL=g_CODEUR, GETCAM(.w_PRCODVAL, .w_PR__DATA, 7),.w_CAOVAL)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_79.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_80.Calculate()
      .DoRTCalc(124,129,.f.)
        .w_CADTOBSO = IIF(.w_ISAHE,.w_ECADTOBSO,.w_RCADTOBSO)
        .w_DACODCOM = .w_PRNUMPRA
        .w_DAVOCRIC = iif(Not empty(.w_COD1ATT) ,SearchVoc(this,'R',.w_COD1ATT,.w_NOTIPCLI,.w_NOCODCLI,.w_CAUDOC,.w_DACODNOM),SPACE(15))
      .DoRTCalc(133,134,.f.)
        .w_DA_SEGNO = 'D'
      .DoRTCalc(136,136,.f.)
        .w_DAATTIVI = space(15)
      .DoRTCalc(138,140,.f.)
      if not(empty(.w_NOCODCLI))
        .link_2_97('Full')
      endif
      .DoRTCalc(141,141,.f.)
        .w_OBTEST = i_datsys
      .DoRTCalc(143,143,.f.)
        .w_OFDATDOC = i_DatSys
      .DoRTCalc(145,146,.f.)
        .w_QTAUM1 = IIF(.w_DATIPRIG='F', 1, CALQTA(.w_PRQTAMOV,.w_PRUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, .w_MODUM2, ' ' ,.w_UNMIS3, .w_OPERA3, .w_MOLTI3))
        .w_MOLTI3 = IIF(.w_ISAHE,.w_EMOLTI3,.w_RMOLTI3)
      .DoRTCalc(149,156,.f.)
        .w_DAVOCCOS = iif(Not empty(.w_COD1ATT) ,SearchVoc(this,'C',.w_COD1ATT,.w_NOTIPCLI,.w_NOCODCLI,.w_CAUDOC,.w_DACODNOM),SPACE(15))
      .DoRTCalc(158,158,.f.)
        .w_CICLO = 'E'
      .DoRTCalc(160,161,.f.)
        .w_OPERA3 = IIF(.w_ISAHE,.w_EOPERA3,.w_ROPERA3)
      .DoRTCalc(163,164,.f.)
        .w_TIPSER = IIF(.w_ISAHE,.w_ETIPSER,.w_RTIPSER)
      .DoRTCalc(166,178,.f.)
        .w_DALISACQ = .w_LISACQ
      .DoRTCalc(180,194,.f.)
        .w_VOCECR = Docgesana(.w_CAUDOC,'V')
        .w_TOTQTA = iif(.w_CHKTEMP='S',INT(.w_PRQTAMOV*.w_DUR_ORE),0)  
        .w_TOTORE = INT(.w_PROREEFF)
        .w_TOTMIN = .w_PRMINEFF
        .w_TOTQTAM = IIF(.w_CHKTEMP='S', INT(cp_round((.w_DUR_ORE * .w_PRQTAMOV - INT(.w_DUR_ORE * .w_PRQTAMOV)) * 60,0)), 0)
      .DoRTCalc(200,200,.f.)
      if not(empty(.w_CONTRACN))
        .link_2_160('Full')
      endif
      .DoRTCalc(201,203,.f.)
        .w_FLCOMP = 'S'
      .DoRTCalc(205,206,.f.)
        .w_DARIGPRE = 'N'
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_170.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_171.Calculate()
      .DoRTCalc(208,217,.f.)
        .w_DAUNIMIS = .w_PRUNIMIS
      .DoRTCalc(219,219,.f.)
        .w_DADESATT = .w_PRDESPRE
      .DoRTCalc(221,240,.f.)
      if not(empty(.w_DACODNOM))
        .link_2_196('Full')
      endif
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_212.Calculate()
    endwith
    this.DoRTCalc(241,258,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PR__DATA = t_PR__DATA
    this.w_CENCOS = t_CENCOS
    this.w_DCODCEN = t_DCODCEN
    this.w_OLDCEN = t_OLDCEN
    this.w_DACODICE = t_DACODICE
    this.w_PRNUMPRA = t_PRNUMPRA
    this.w_ECOD1ATT = t_ECOD1ATT
    this.w_RCOD1ATT = t_RCOD1ATT
    this.w_DATIPRIG = t_DATIPRIG
    this.w_DATIPRI2 = t_DATIPRI2
    this.w_ECACODART = t_ECACODART
    this.w_RCACODART = t_RCACODART
    this.w_DACODATT = t_DACODATT
    this.w_CACODART = t_CACODART
    this.w_ARTIPRIG = t_ARTIPRIG
    this.w_ARTIPRI2 = t_ARTIPRI2
    this.w_COD1ATT = t_COD1ATT
    this.w_NOTIFICA = t_NOTIFICA
    this.w_PRDESPRE = t_PRDESPRE
    this.w_PRUNIMIS = t_PRUNIMIS
    this.w_NUMLIS = t_NUMLIS
    this.w_PRQTAMOV = t_PRQTAMOV
    this.w_NCODLIS = t_NCODLIS
    this.w_DAPREZZO = t_DAPREZZO
    this.w_PRCODVAL = t_PRCODVAL
    this.w_PRCODLIS = t_PRCODLIS
    this.w_ATIPRIG = this.oPgFrm.Page1.oPag.oATIPRIG_2_29.RadioValue(.t.)
    this.w_ATIPRI2 = this.oPgFrm.Page1.oPag.oATIPRI2_2_30.RadioValue(.t.)
    this.w_ETIPRIG = this.oPgFrm.Page1.oPag.oETIPRIG_2_31.RadioValue(.t.)
    this.w_ETIPRI2 = t_ETIPRI2
    this.w_RTIPRIG = this.oPgFrm.Page1.oPag.oRTIPRIG_2_33.RadioValue(.t.)
    this.w_RTIPRI2 = t_RTIPRI2
    this.w_DACODSED = t_DACODSED
    this.w_DECTOT = t_DECTOT
    this.w_DECUNI = t_DECUNI
    this.w_CALCPICU = t_CALCPICU
    this.w_CALCPICT = t_CALCPICT
    this.w_PRDESAGG = t_PRDESAGG
    this.w_PRCODOPE = t_PRCODOPE
    this.w_PRDATMOD = t_PRDATMOD
    this.w_PRVALRIG = t_PRVALRIG
    this.w_LICOSTO = t_LICOSTO
    this.w_PROREEFF = t_PROREEFF
    this.w_PRMINEFF = t_PRMINEFF
    this.w_DACOSUNI = t_DACOSUNI
    this.w_PRCOSINT = t_PRCOSINT
    this.w_DAFLDEFF = t_DAFLDEFF
    this.w_DAPREMIN = t_DAPREMIN
    this.w_DAPREMAX = t_DAPREMAX
    this.w_DAGAZUFF = t_DAGAZUFF
    this.w_TIPART = t_TIPART
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_FLSERG = t_FLSERG
    this.w_DESUNI = t_DESUNI
    this.w_NOMEPRA = t_NOMEPRA
    this.w_UNIMIS = t_UNIMIS
    this.w_LCODVAL = t_LCODVAL
    this.w_VALCOM = t_VALCOM
    this.w_EUNMIS3 = t_EUNMIS3
    this.w_RUNMIS3 = t_RUNMIS3
    this.w_OBTEST = t_OBTEST
    this.w_UNMIS3 = t_UNMIS3
    this.w_VALLIS = t_VALLIS
    this.w_FLAPON = t_FLAPON
    this.w_ENTE = t_ENTE
    this.w_UFFICI = t_UFFICI
    this.w_FLVALO = t_FLVALO
    this.w_IMPORT = t_IMPORT
    this.w_CALDIR = t_CALDIR
    this.w_COECAL = t_COECAL
    this.w_PARASS = t_PARASS
    this.w_FLAMPA = t_FLAMPA
    this.w_CAOVAL = t_CAOVAL
    this.w_DATOBSO = t_DATOBSO
    this.w_TIPOENTE = t_TIPOENTE
    this.w_CHKTEMP = t_CHKTEMP
    this.w_DUR_ORE = t_DUR_ORE
    this.w_ECADTOBSO = t_ECADTOBSO
    this.w_RCADTOBSO = t_RCADTOBSO
    this.w_CADTOBSO = t_CADTOBSO
    this.w_DACODCOM = t_DACODCOM
    this.w_DAVOCRIC = t_DAVOCRIC
    this.w_DAINICOM = t_DAINICOM
    this.w_DAFINCOM = t_DAFINCOM
    this.w_DA_SEGNO = t_DA_SEGNO
    this.w_DACENCOS = t_DACENCOS
    this.w_DAATTIVI = t_DAATTIVI
    this.w_NOTIPCLI = t_NOTIPCLI
    this.w_DTOBSNOM = t_DTOBSNOM
    this.w_NOCODCLI = t_NOCODCLI
    this.w_NOTIPNOM = t_NOTIPNOM
    this.w_OBTEST = t_OBTEST
    this.w_DATOBSO = t_DATOBSO
    this.w_OFDATDOC = t_OFDATDOC
    this.w_EMOLTI3 = t_EMOLTI3
    this.w_RMOLTI3 = t_RMOLTI3
    this.w_QTAUM1 = t_QTAUM1
    this.w_MOLTI3 = t_MOLTI3
    this.w_FLUSEP = t_FLUSEP
    this.w_OPERAT = t_OPERAT
    this.w_FLFRAZ1 = t_FLFRAZ1
    this.w_MODUM2 = t_MODUM2
    this.w_MOLTIP = t_MOLTIP
    this.w_PREZUM = t_PREZUM
    this.w_ARTIPART = t_ARTIPART
    this.w_SCOLIS = t_SCOLIS
    this.w_DAVOCCOS = t_DAVOCCOS
    this.w_CCODLIS = t_CCODLIS
    this.w_CICLO = t_CICLO
    this.w_EOPERA3 = t_EOPERA3
    this.w_ROPERA3 = t_ROPERA3
    this.w_OPERA3 = t_OPERA3
    this.w_ETIPSER = t_ETIPSER
    this.w_RTIPSER = t_RTIPSER
    this.w_TIPSER = t_TIPSER
    this.w_DACODLIS = t_DACODLIS
    this.w_DAPROLIS = t_DAPROLIS
    this.w_DAPROSCO = t_DAPROSCO
    this.w_DASCOLIS = t_DASCOLIS
    this.w_DASCONT1 = t_DASCONT1
    this.w_DASCONT2 = t_DASCONT2
    this.w_DASCONT3 = t_DASCONT3
    this.w_DASCONT4 = t_DASCONT4
    this.w_DACOSUNI = t_DACOSUNI
    this.w_DACOMRIC = t_DACOMRIC
    this.w_DAATTRIC = t_DAATTRIC
    this.w_DACENRIC = t_DACENRIC
    this.w_FL_FRAZ = t_FL_FRAZ
    this.w_DALISACQ = t_DALISACQ
    this.w_VALACQ = t_VALACQ
    this.w_INIACQ = t_INIACQ
    this.w_TIPACQ = t_TIPACQ
    this.w_FLSACQ = t_FLSACQ
    this.w_IVAACQ = t_IVAACQ
    this.w_FLGACQ = t_FLGACQ
    this.w_FINACQ = t_FINACQ
    this.w_DATCOINI = t_DATCOINI
    this.w_DATCOFIN = t_DATCOFIN
    this.w_DATRIINI = t_DATRIINI
    this.w_DATRIFIN = t_DATRIFIN
    this.w_FLSCOR = t_FLSCOR
    this.w_TARTEM = t_TARTEM
    this.w_TARCON = t_TARCON
    this.w_FLVALO1 = t_FLVALO1
    this.w_VOCECR = t_VOCECR
    this.w_TOTQTA = t_TOTQTA
    this.w_TOTORE = t_TOTORE
    this.w_TOTMIN = t_TOTMIN
    this.w_TOTQTAM = t_TOTQTAM
    this.w_CONTRACN = t_CONTRACN
    this.w_TIPCONCO = t_TIPCONCO
    this.w_LISCOLCO = t_LISCOLCO
    this.w_STATUSCO = t_STATUSCO
    this.w_FLCOMP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMP_2_166.RadioValue(.t.)
    this.w_FLSPAN = t_FLSPAN
    this.w_DARIFPRE = t_DARIFPRE
    this.w_DARIGPRE = t_DARIGPRE
    this.w_PRESTA = t_PRESTA
    this.w_DACONCOD = t_DACONCOD
    this.w_CEN_CauDoc = t_CEN_CauDoc
    this.w_DACONTRA = t_DACONTRA
    this.w_DACODMOD = t_DACODMOD
    this.w_DACODIMP = t_DACODIMP
    this.w_DACOCOMP = t_DACOCOMP
    this.w_DARINNOV = t_DARINNOV
    this.w_DAUNIMIS = t_DAUNIMIS
    this.w_DADESATT = t_DADESATT
    this.w_FLUNIV = t_FLUNIV
    this.w_NUMPRE = t_NUMPRE
    this.w_LOCALI = t_LOCALI
    this.w_DALISACQ = t_DALISACQ
    this.w_FLSCOAC = t_FLSCOAC
    this.w_CNDATFIN = t_CNDATFIN
    this.w_CATCOM = t_CATCOM
    this.w_DACODNOM = t_DACODNOM
    this.w_PRCODSPE = t_PRCODSPE
    this.w_PRCODANT = t_PRCODANT
    this.w_PRIMPANT = t_PRIMPANT
    this.w_PRIMPSPE = t_PRIMPSPE
    this.w_TIPRIGCA = t_TIPRIGCA
    this.w_TIPRI2CA = t_TIPRI2CA
    this.w_CNTIPPRA = t_CNTIPPRA
    this.w_CNMATOBB = t_CNMATOBB
    this.w_CNASSCTP = t_CNASSCTP
    this.w_CNCOMPLX = t_CNCOMPLX
    this.w_CNPROFMT = t_CNPROFMT
    this.w_CNESIPOS = t_CNESIPOS
    this.w_CNPERPLX = t_CNPERPLX
    this.w_CNPERPOS = t_CNPERPOS
    this.w_CNFLVMLQ = t_CNFLVMLQ
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PR__DATA with this.w_PR__DATA
    replace t_CENCOS with this.w_CENCOS
    replace t_DCODCEN with this.w_DCODCEN
    replace t_OLDCEN with this.w_OLDCEN
    replace t_DACODICE with this.w_DACODICE
    replace t_PRNUMPRA with this.w_PRNUMPRA
    replace t_ECOD1ATT with this.w_ECOD1ATT
    replace t_RCOD1ATT with this.w_RCOD1ATT
    replace t_DATIPRIG with this.w_DATIPRIG
    replace t_DATIPRI2 with this.w_DATIPRI2
    replace t_ECACODART with this.w_ECACODART
    replace t_RCACODART with this.w_RCACODART
    replace t_DACODATT with this.w_DACODATT
    replace t_CACODART with this.w_CACODART
    replace t_ARTIPRIG with this.w_ARTIPRIG
    replace t_ARTIPRI2 with this.w_ARTIPRI2
    replace t_COD1ATT with this.w_COD1ATT
    replace t_NOTIFICA with this.w_NOTIFICA
    replace t_PRDESPRE with this.w_PRDESPRE
    replace t_PRUNIMIS with this.w_PRUNIMIS
    replace t_NUMLIS with this.w_NUMLIS
    replace t_PRQTAMOV with this.w_PRQTAMOV
    replace t_NCODLIS with this.w_NCODLIS
    replace t_DAPREZZO with this.w_DAPREZZO
    replace t_PRCODVAL with this.w_PRCODVAL
    replace t_PRCODLIS with this.w_PRCODLIS
    replace t_ATIPRIG with this.oPgFrm.Page1.oPag.oATIPRIG_2_29.ToRadio()
    replace t_ATIPRI2 with this.oPgFrm.Page1.oPag.oATIPRI2_2_30.ToRadio()
    replace t_ETIPRIG with this.oPgFrm.Page1.oPag.oETIPRIG_2_31.ToRadio()
    replace t_ETIPRI2 with this.w_ETIPRI2
    replace t_RTIPRIG with this.oPgFrm.Page1.oPag.oRTIPRIG_2_33.ToRadio()
    replace t_RTIPRI2 with this.w_RTIPRI2
    replace t_DACODSED with this.w_DACODSED
    replace t_DECTOT with this.w_DECTOT
    replace t_DECUNI with this.w_DECUNI
    replace t_CALCPICU with this.w_CALCPICU
    replace t_CALCPICT with this.w_CALCPICT
    replace t_PRDESAGG with this.w_PRDESAGG
    replace t_PRCODOPE with this.w_PRCODOPE
    replace t_PRDATMOD with this.w_PRDATMOD
    replace t_PRVALRIG with this.w_PRVALRIG
    replace t_LICOSTO with this.w_LICOSTO
    replace t_PROREEFF with this.w_PROREEFF
    replace t_PRMINEFF with this.w_PRMINEFF
    replace t_DACOSUNI with this.w_DACOSUNI
    replace t_PRCOSINT with this.w_PRCOSINT
    replace t_DAFLDEFF with this.w_DAFLDEFF
    replace t_DAPREMIN with this.w_DAPREMIN
    replace t_DAPREMAX with this.w_DAPREMAX
    replace t_DAGAZUFF with this.w_DAGAZUFF
    replace t_TIPART with this.w_TIPART
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_FLSERG with this.w_FLSERG
    replace t_DESUNI with this.w_DESUNI
    replace t_NOMEPRA with this.w_NOMEPRA
    replace t_UNIMIS with this.w_UNIMIS
    replace t_LCODVAL with this.w_LCODVAL
    replace t_VALCOM with this.w_VALCOM
    replace t_EUNMIS3 with this.w_EUNMIS3
    replace t_RUNMIS3 with this.w_RUNMIS3
    replace t_OBTEST with this.w_OBTEST
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_VALLIS with this.w_VALLIS
    replace t_FLAPON with this.w_FLAPON
    replace t_ENTE with this.w_ENTE
    replace t_UFFICI with this.w_UFFICI
    replace t_FLVALO with this.w_FLVALO
    replace t_IMPORT with this.w_IMPORT
    replace t_CALDIR with this.w_CALDIR
    replace t_COECAL with this.w_COECAL
    replace t_PARASS with this.w_PARASS
    replace t_FLAMPA with this.w_FLAMPA
    replace t_CAOVAL with this.w_CAOVAL
    replace t_DATOBSO with this.w_DATOBSO
    replace t_TIPOENTE with this.w_TIPOENTE
    replace t_CHKTEMP with this.w_CHKTEMP
    replace t_DUR_ORE with this.w_DUR_ORE
    replace t_ECADTOBSO with this.w_ECADTOBSO
    replace t_RCADTOBSO with this.w_RCADTOBSO
    replace t_CADTOBSO with this.w_CADTOBSO
    replace t_DACODCOM with this.w_DACODCOM
    replace t_DAVOCRIC with this.w_DAVOCRIC
    replace t_DAINICOM with this.w_DAINICOM
    replace t_DAFINCOM with this.w_DAFINCOM
    replace t_DA_SEGNO with this.w_DA_SEGNO
    replace t_DACENCOS with this.w_DACENCOS
    replace t_DAATTIVI with this.w_DAATTIVI
    replace t_NOTIPCLI with this.w_NOTIPCLI
    replace t_DTOBSNOM with this.w_DTOBSNOM
    replace t_NOCODCLI with this.w_NOCODCLI
    replace t_NOTIPNOM with this.w_NOTIPNOM
    replace t_OBTEST with this.w_OBTEST
    replace t_DATOBSO with this.w_DATOBSO
    replace t_OFDATDOC with this.w_OFDATDOC
    replace t_EMOLTI3 with this.w_EMOLTI3
    replace t_RMOLTI3 with this.w_RMOLTI3
    replace t_QTAUM1 with this.w_QTAUM1
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_FLUSEP with this.w_FLUSEP
    replace t_OPERAT with this.w_OPERAT
    replace t_FLFRAZ1 with this.w_FLFRAZ1
    replace t_MODUM2 with this.w_MODUM2
    replace t_MOLTIP with this.w_MOLTIP
    replace t_PREZUM with this.w_PREZUM
    replace t_ARTIPART with this.w_ARTIPART
    replace t_SCOLIS with this.w_SCOLIS
    replace t_DAVOCCOS with this.w_DAVOCCOS
    replace t_CCODLIS with this.w_CCODLIS
    replace t_CICLO with this.w_CICLO
    replace t_EOPERA3 with this.w_EOPERA3
    replace t_ROPERA3 with this.w_ROPERA3
    replace t_OPERA3 with this.w_OPERA3
    replace t_ETIPSER with this.w_ETIPSER
    replace t_RTIPSER with this.w_RTIPSER
    replace t_TIPSER with this.w_TIPSER
    replace t_DACODLIS with this.w_DACODLIS
    replace t_DAPROLIS with this.w_DAPROLIS
    replace t_DAPROSCO with this.w_DAPROSCO
    replace t_DASCOLIS with this.w_DASCOLIS
    replace t_DASCONT1 with this.w_DASCONT1
    replace t_DASCONT2 with this.w_DASCONT2
    replace t_DASCONT3 with this.w_DASCONT3
    replace t_DASCONT4 with this.w_DASCONT4
    replace t_DACOSUNI with this.w_DACOSUNI
    replace t_DACOMRIC with this.w_DACOMRIC
    replace t_DAATTRIC with this.w_DAATTRIC
    replace t_DACENRIC with this.w_DACENRIC
    replace t_FL_FRAZ with this.w_FL_FRAZ
    replace t_DALISACQ with this.w_DALISACQ
    replace t_VALACQ with this.w_VALACQ
    replace t_INIACQ with this.w_INIACQ
    replace t_TIPACQ with this.w_TIPACQ
    replace t_FLSACQ with this.w_FLSACQ
    replace t_IVAACQ with this.w_IVAACQ
    replace t_FLGACQ with this.w_FLGACQ
    replace t_FINACQ with this.w_FINACQ
    replace t_DATCOINI with this.w_DATCOINI
    replace t_DATCOFIN with this.w_DATCOFIN
    replace t_DATRIINI with this.w_DATRIINI
    replace t_DATRIFIN with this.w_DATRIFIN
    replace t_FLSCOR with this.w_FLSCOR
    replace t_TARTEM with this.w_TARTEM
    replace t_TARCON with this.w_TARCON
    replace t_FLVALO1 with this.w_FLVALO1
    replace t_VOCECR with this.w_VOCECR
    replace t_TOTQTA with this.w_TOTQTA
    replace t_TOTORE with this.w_TOTORE
    replace t_TOTMIN with this.w_TOTMIN
    replace t_TOTQTAM with this.w_TOTQTAM
    replace t_CONTRACN with this.w_CONTRACN
    replace t_TIPCONCO with this.w_TIPCONCO
    replace t_LISCOLCO with this.w_LISCOLCO
    replace t_STATUSCO with this.w_STATUSCO
    replace t_FLCOMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFLCOMP_2_166.ToRadio()
    replace t_FLSPAN with this.w_FLSPAN
    replace t_DARIFPRE with this.w_DARIFPRE
    replace t_DARIGPRE with this.w_DARIGPRE
    replace t_PRESTA with this.w_PRESTA
    replace t_DACONCOD with this.w_DACONCOD
    replace t_CEN_CauDoc with this.w_CEN_CauDoc
    replace t_DACONTRA with this.w_DACONTRA
    replace t_DACODMOD with this.w_DACODMOD
    replace t_DACODIMP with this.w_DACODIMP
    replace t_DACOCOMP with this.w_DACOCOMP
    replace t_DARINNOV with this.w_DARINNOV
    replace t_DAUNIMIS with this.w_DAUNIMIS
    replace t_DADESATT with this.w_DADESATT
    replace t_FLUNIV with this.w_FLUNIV
    replace t_NUMPRE with this.w_NUMPRE
    replace t_LOCALI with this.w_LOCALI
    replace t_DALISACQ with this.w_DALISACQ
    replace t_FLSCOAC with this.w_FLSCOAC
    replace t_CNDATFIN with this.w_CNDATFIN
    replace t_CATCOM with this.w_CATCOM
    replace t_DACODNOM with this.w_DACODNOM
    replace t_PRCODSPE with this.w_PRCODSPE
    replace t_PRCODANT with this.w_PRCODANT
    replace t_PRIMPANT with this.w_PRIMPANT
    replace t_PRIMPSPE with this.w_PRIMPSPE
    replace t_TIPRIGCA with this.w_TIPRIGCA
    replace t_TIPRI2CA with this.w_TIPRI2CA
    replace t_CNTIPPRA with this.w_CNTIPPRA
    replace t_CNMATOBB with this.w_CNMATOBB
    replace t_CNASSCTP with this.w_CNASSCTP
    replace t_CNCOMPLX with this.w_CNCOMPLX
    replace t_CNPROFMT with this.w_CNPROFMT
    replace t_CNESIPOS with this.w_CNESIPOS
    replace t_CNPERPLX with this.w_CNPERPLX
    replace t_CNPERPOS with this.w_CNPERPOS
    replace t_CNFLVMLQ with this.w_CNFLVMLQ
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTDURQTA = .w_TOTDURQTA-.w_totqta
        .w_TOTALORE = .w_TOTALORE-.w_totore
        .w_TOTALMIN = .w_TOTALMIN-.w_totmin
        .w_TOTDURQTAM = .w_TOTDURQTAM-.w_totqtam
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsag_mprPag1 as StdContainer
  Width  = 823
  height = 440
  stdWidth  = 823
  stdheight = 440
  resizeXpos=551
  resizeYpos=38
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=1, width=813,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="FLCOMP",Label1="",Field2="PR__DATA",Label2="Data",Field3="PRNUMPRA",Label3="Pratica",Field4="DACODICE",Label4="Codice",Field5="DACODATT",Label5="Codice",Field6="PRDESPRE",Label6="Descrizione",Field7="PRUNIMIS",Label7="U.M.",Field8="PRQTAMOV",Label8="Quantit�",Field9="DAPREZZO",Label9="Tariffa",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168624762


  add object oObj_1_18 as cp_runprogram with uid="ZDBNDAIRKT",left=5, top=471, width=211,height=19,;
    caption='gsag_bcd(1)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('1')",;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 261498442


  add object oObj_1_31 as cp_runprogram with uid="IHIDYBDKXP",left=5, top=492, width=212,height=19,;
    caption='gsag_bcd(2)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('2')",;
    cEvent = "CalcNot",;
    nPag=1;
    , HelpContextID = 261498698


  add object oObj_1_42 as cp_runprogram with uid="EPEDLIMMWQ",left=5, top=513, width=211,height=19,;
    caption='gsag_bcd(3)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('3')",;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 261498954


  add object oObj_1_44 as cp_runprogram with uid="BPVSMQVCVU",left=5, top=450, width=211,height=19,;
    caption='gsag_bcd(4)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('4')",;
    cEvent = "Impprez",;
    nPag=1;
    , HelpContextID = 261499210


  add object oObj_1_47 as cp_runprogram with uid="VMEZHRHNWS",left=5, top=534, width=211,height=19,;
    caption='gsag_bcd(7)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('7')",;
    cEvent = "LisRig",;
    nPag=1;
    , ToolTipText = "Applico  listinin al cambiare dei dati di riga";
    , HelpContextID = 261499978


  add object oObj_1_51 as cp_runprogram with uid="KYGMASALXX",left=4, top=555, width=211,height=19,;
    caption='gsag_bcd(8)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('8')",;
    cEvent = "w_DALISACQ Changed",;
    nPag=1;
    , ToolTipText = "Calcolo costo interno";
    , HelpContextID = 261500234


  add object oBtn_1_64 as StdButton with uid="PGDSTCLPNB",left=6, top=198, width=23,height=23,;
    CpPicture="bmp\Check_small.bmp", caption="", nPag=1;
    , ToolTipText = "Seleziona tutte le prestazioni";
    , HelpContextID = 147465994;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_64.Click()
      with this.Parent.oContained
        GSAG_BCK(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_65 as StdButton with uid="LQWGAAWXAW",left=30, top=198, width=23,height=23,;
    CpPicture="bmp\UnCheck_small.bmp", caption="", nPag=1;
    , ToolTipText = "Deseleziona tutte le prestazioni";
    , HelpContextID = 147465994;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_65.Click()
      with this.Parent.oContained
        GSAG_BCK(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_68 as cp_runprogram with uid="TDFEAXIAEE",left=227, top=539, width=218,height=27,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAG_BCK(w_SELEZI)",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 30531558


  add object oObj_1_92 as cp_runprogram with uid="UYNOOUWJSB",left=5, top=845, width=211,height=19,;
    caption='GSAG_BVR',;
   bGlobalFont=.t.,;
    prg="gsag_bvr('LETTURA')",;
    cEvent = "Link",;
    nPag=1;
    , HelpContextID = 259080376


  add object oObj_1_93 as cp_runprogram with uid="WGHOZZEMFU",left=5, top=878, width=211,height=19,;
    caption='GSAG_BVR',;
   bGlobalFont=.t.,;
    prg="gsag_bvr('LEGGETARCONC')",;
    cEvent = "TarConc",;
    nPag=1;
    , HelpContextID = 259080376

  add object oStr_1_15 as StdString with uid="ZOHGBHZIYU",Visible=.t., Left=348, Top=201,;
    Alignment=1, Width=90, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="YZMQBGQDPB",Visible=.t., Left=580, Top=201,;
    Alignment=1, Width=94, Height=18,;
    Caption="Importo di riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="EVHOHPKEQZ",Visible=.t., Left=260, Top=201,;
    Alignment=1, Width=31, Height=18,;
    Caption="G.U.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="DPASEYXUXB",Visible=.t., Left=148, Top=282,;
    Alignment=0, Width=241, Height=18,;
    Caption="Descrizione aggiuntiva"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="IPXDPIQXPJ",Visible=.t., Left=11, Top=377,;
    Alignment=1, Width=81, Height=18,;
    Caption="Modificato da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="IFSNVFIKXY",Visible=.t., Left=30, Top=408,;
    Alignment=1, Width=27, Height=18,;
    Caption="Il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="HVIBWDTHAX",Visible=.t., Left=589, Top=377,;
    Alignment=1, Width=81, Height=18,;
    Caption="Costo interno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="KRFRWINQZV",Visible=.t., Left=418, Top=377,;
    Alignment=1, Width=90, Height=18,;
    Caption="Durata effettiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="PVSFOKOSXX",Visible=.t., Left=551, Top=377,;
    Alignment=0, Width=5, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="MYAHANWBDP",Visible=.t., Left=173, Top=227,;
    Alignment=1, Width=84, Height=18,;
    Caption="Nome pratica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ZQRNFFTDQA",Visible=.t., Left=373, Top=408,;
    Alignment=1, Width=69, Height=18,;
    Caption="Tariffa min.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="RSGBCNUZVS",Visible=.t., Left=48, Top=201,;
    Alignment=1, Width=47, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="PGAEKERPSC",Visible=.t., Left=147, Top=201,;
    Alignment=1, Width=44, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YXHEGBXQSL",Visible=.t., Left=597, Top=408,;
    Alignment=1, Width=73, Height=18,;
    Caption="Tariffa max.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="VORHYFINWA",Visible=.t., Left=600, Top=227,;
    Alignment=1, Width=74, Height=18,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="MQORHDNVZT",Visible=.t., Left=600, Top=253,;
    Alignment=1, Width=74, Height=18,;
    Caption="Nota spese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="RZWQDSKSRT",Visible=.t., Left=173, Top=253,;
    Alignment=1, Width=84, Height=18,;
    Caption="Anticipazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (.w_GENPRE<>'S' or .w_DARIGPRE<>'S')
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="TMIXFRITER",Visible=.t., Left=407, Top=253,;
    Alignment=1, Width=40, Height=18,;
    Caption="Spesa:"  ;
  , bGlobalFont=.t.

  func oStr_1_88.mHide()
    with this.Parent.oContained
      return (.w_GENPRE<>'S' or .w_DARIGPRE<>'S')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=20,;
    width=809+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=21,width=808+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|CAN_TIER|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPRCODVAL_2_27.Refresh()
      this.Parent.oPRCODLIS_2_28.Refresh()
      this.Parent.oATIPRIG_2_29.Refresh()
      this.Parent.oATIPRI2_2_30.Refresh()
      this.Parent.oETIPRIG_2_31.Refresh()
      this.Parent.oRTIPRIG_2_33.Refresh()
      this.Parent.oRTIPRI2_2_34.Refresh()
      this.Parent.oPRDESAGG_2_40.Refresh()
      this.Parent.oPRCODOPE_2_41.Refresh()
      this.Parent.oPRDATMOD_2_42.Refresh()
      this.Parent.oPRVALRIG_2_43.Refresh()
      this.Parent.oPROREEFF_2_45.Refresh()
      this.Parent.oPRMINEFF_2_46.Refresh()
      this.Parent.oPRCOSINT_2_48.Refresh()
      this.Parent.oDAPREMIN_2_50.Refresh()
      this.Parent.oDAPREMAX_2_51.Refresh()
      this.Parent.oDAGAZUFF_2_52.Refresh()
      this.Parent.oDESUNI_2_57.Refresh()
      this.Parent.oNOMEPRA_2_58.Refresh()
      this.Parent.oPRIMPANT_2_199.Refresh()
      this.Parent.oPRIMPSPE_2_200.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oDACODICE_2_6
      case cFile='CAN_TIER'
        oDropInto=this.oBodyCol.oRow.oPRNUMPRA_2_7
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oDACODATT_2_15
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oPRUNIMIS_2_22
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oPRCODVAL_2_27 as StdTrsField with uid="TXOWYCASXL",rtseq=74,rtrep=.t.,;
    cFormVar="w_PRCODVAL",value=space(3),enabled=.f.,;
    ToolTipText = "Codice valuta",;
    HelpContextID = 29974594,;
    cTotal="", bFixedPos=.t., cQueryName = "PRCODVAL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=98, Top=198, InputMask=replicate('X',3), TabStop=.f., cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PRCODVAL"

  func oPRCODVAL_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_PRCODLIS)
        bRes2=.link_2_28('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oPRCODLIS_2_28 as StdTrsField with uid="ZJHXBCOYLK",rtseq=75,rtrep=.t.,;
    cFormVar="w_PRCODLIS",value=space(5),;
    ToolTipText = "Codice listino",;
    HelpContextID = 130637897,;
    cTotal="", bFixedPos=.t., cQueryName = "PRCODLIS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valuta listino incongruente o listino gestito a sconti",;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=194, Top=198, InputMask=replicate('X',5), bHasZoom = .t. , TabStop=.f., cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSVALLIS", oKey_1_2="this.w_PRCODVAL", oKey_2_1="LSCODLIS", oKey_2_2="this.w_PRCODLIS"

  func oPRCODLIS_2_28.mCond()
    with this.Parent.oContained
      return ( (.w_DARIFPRE=0 and .w_DARIGPRE<>'S'))
    endwith
  endfunc

  func oPRCODLIS_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODLIS_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPRCODLIS_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LISTINI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStrODBC(this.Parent.oContained.w_PRCODVAL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LSVALLIS="+cp_ToStr(this.Parent.oContained.w_PRCODVAL)
    endif
    do cp_zoom with 'LISTINI','*','LSVALLIS,LSCODLIS',cp_AbsName(this.parent,'oPRCODLIS_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oPRCODLIS_2_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LSVALLIS=w_PRCODVAL
     i_obj.w_LSCODLIS=this.parent.oContained.w_PRCODLIS
    i_obj.ecpSave()
  endproc

  add object oATIPRIG_2_29 as StdTrsCombo with uid="AGEBLOUZJC",rtrep=.t.,;
    cFormVar="w_ATIPRIG", RowSource=""+"Non fatturabile,"+"Fatturabile" , ;
    ToolTipText = "Tipologia riga per la generazione del documento.",;
    HelpContextID = 95076614,;
    Height=25, Width=107, Left=677, Top=224,;
    cTotal="", cQueryName = "ATIPRIG",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oATIPRIG_2_29.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATIPRIG,&i_cF..t_ATIPRIG),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    space(10))))
  endfunc
  func oATIPRIG_2_29.GetRadio()
    this.Parent.oContained.w_ATIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oATIPRIG_2_29.ToRadio()
    this.Parent.oContained.w_ATIPRIG=trim(this.Parent.oContained.w_ATIPRIG)
    return(;
      iif(this.Parent.oContained.w_ATIPRIG=='N',1,;
      iif(this.Parent.oContained.w_ATIPRIG=='D',2,;
      0)))
  endfunc

  func oATIPRIG_2_29.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oATIPRIG_2_29.mCond()
    with this.Parent.oContained
      return (NOT(.w_ARTIPART='FO' and (.w_DAPREZZO=0 and .w_FLGZER='N')))
    endwith
  endfunc

  add object oATIPRI2_2_30 as StdTrsCombo with uid="RLLREJZLJF",rtrep=.t.,;
    cFormVar="w_ATIPRI2", RowSource=""+"Senza nota spese,"+"Con nota spese" , ;
    ToolTipText = "Tipologia riga per la generazione della nota spese",;
    HelpContextID = 95076614,;
    Height=25, Width=107, Left=677, Top=251,;
    cTotal="", cQueryName = "ATIPRI2",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oATIPRI2_2_30.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ATIPRI2,&i_cF..t_ATIPRI2),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    space(10))))
  endfunc
  func oATIPRI2_2_30.GetRadio()
    this.Parent.oContained.w_ATIPRI2 = this.RadioValue()
    return .t.
  endfunc

  func oATIPRI2_2_30.ToRadio()
    this.Parent.oContained.w_ATIPRI2=trim(this.Parent.oContained.w_ATIPRI2)
    return(;
      iif(this.Parent.oContained.w_ATIPRI2=='N',1,;
      iif(this.Parent.oContained.w_ATIPRI2=='D',2,;
      0)))
  endfunc

  func oATIPRI2_2_30.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oATIPRI2_2_30.mCond()
    with this.Parent.oContained
      return (NOT(.w_ARTIPART='FO' and (.w_DAPREZZO=0 and .w_FLGZER='N')))
    endwith
  endfunc

  add object oETIPRIG_2_31 as StdTrsCombo with uid="WDOQTSAWKZ",rtrep=.t.,;
    cFormVar="w_ETIPRIG", RowSource=""+"Nessuno,"+"Doc. vendite,"+"Doc. Acquisti,"+"Entrambi" , ;
    ToolTipText = "Tipologia riga per la generazione del documento.",;
    HelpContextID = 95076678,;
    Height=25, Width=107, Left=677, Top=224,;
    cTotal="", cQueryName = "ETIPRIG",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oETIPRIG_2_31.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ETIPRIG,&i_cF..t_ETIPRIG),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    iif(xVal =3,'A',;
    iif(xVal =4,'E',;
    space(10))))))
  endfunc
  func oETIPRIG_2_31.GetRadio()
    this.Parent.oContained.w_ETIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oETIPRIG_2_31.ToRadio()
    this.Parent.oContained.w_ETIPRIG=trim(this.Parent.oContained.w_ETIPRIG)
    return(;
      iif(this.Parent.oContained.w_ETIPRIG=='N',1,;
      iif(this.Parent.oContained.w_ETIPRIG=='D',2,;
      iif(this.Parent.oContained.w_ETIPRIG=='A',3,;
      iif(this.Parent.oContained.w_ETIPRIG=='E',4,;
      0)))))
  endfunc

  func oETIPRIG_2_31.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oETIPRIG_2_31.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISAHE)
    endwith
    endif
  endfunc

  add object oRTIPRIG_2_33 as StdTrsCombo with uid="XKDCKNPLAZ",rtrep=.t.,;
    cFormVar="w_RTIPRIG", RowSource=""+"Nessuno,"+"Doc. vendite,"+"Doc. Acquisti,"+"Entrambi" , ;
    ToolTipText = "Tipologia riga per la generazione del documento.",;
    HelpContextID = 95076886,;
    Height=25, Width=107, Left=677, Top=224,;
    cTotal="", cQueryName = "RTIPRIG",;
    bObbl = .f. , nPag=2  , sErrorMsg = "Selezionare un documento coerente con le causali documenti specificate nel tipo attivit�!";
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oRTIPRIG_2_33.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RTIPRIG,&i_cF..t_RTIPRIG),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'D',;
    iif(xVal =3,'A',;
    iif(xVal =4,'E',;
    space(10))))))
  endfunc
  func oRTIPRIG_2_33.GetRadio()
    this.Parent.oContained.w_RTIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oRTIPRIG_2_33.ToRadio()
    this.Parent.oContained.w_RTIPRIG=trim(this.Parent.oContained.w_RTIPRIG)
    return(;
      iif(this.Parent.oContained.w_RTIPRIG=='N',1,;
      iif(this.Parent.oContained.w_RTIPRIG=='D',2,;
      iif(this.Parent.oContained.w_RTIPRIG=='A',3,;
      iif(this.Parent.oContained.w_RTIPRIG=='E',4,;
      0)))))
  endfunc

  func oRTIPRIG_2_33.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oRTIPRIG_2_33.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!Isahr())
    endwith
    endif
  endfunc

  func oRTIPRIG_2_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_FLANAL='N' OR (.w_RTIPRIG<>'A' AND .w_RTIPRIG<>'E')) AND (!.w_RTIPRIG$'DE' OR !EMPTY(.w_CAUDOC)) AND (!.w_RTIPRIG$'AE' OR !EMPTY(.w_CAUACQ)))
    endwith
    return bRes
  endfunc

  add object oRTIPRI2_2_34 as StdTrsField with uid="KPLGRXLJLH",rtseq=81,rtrep=.t.,;
    cFormVar="w_RTIPRI2",value=space(10),enabled=.f.,;
    ToolTipText = "Tipologia riga per la generazione della nota spese",;
    HelpContextID = 95076886,;
    cTotal="", bFixedPos=.t., cQueryName = "RTIPRI2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=1101, Top=675, InputMask=replicate('X',10)

  func oRTIPRI2_2_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!Isahr())
    endwith
    endif
  endfunc

  add object oPRDESAGG_2_40 as StdTrsMemo with uid="DFASHLLYMF",rtseq=87,rtrep=.t.,;
    cFormVar="w_PRDESAGG",value=space(0),;
    ToolTipText = "Descrizione aggiuntiva",;
    HelpContextID = 229601341,;
    cTotal="", bFixedPos=.t., cQueryName = "PRDESAGG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=58, Width=669, Left=148, Top=299

  add object oPRCODOPE_2_41 as StdTrsField with uid="EPBSGZJLOX",rtseq=88,rtrep=.t.,;
    cFormVar="w_PRCODOPE",value=0,enabled=.f.,;
    HelpContextID = 87465925,;
    cTotal="", bFixedPos=.t., cQueryName = "PRCODOPE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=96, Top=376, cSayPict=["9999"], cGetPict=["9999"]

  add object oPRDATMOD_2_42 as StdTrsField with uid="NPHVJHJVMO",rtseq=89,rtrep=.t.,;
    cFormVar="w_PRDATMOD",value=ctod("  /  /  "),enabled=.f.,;
    HelpContextID = 105156550,;
    cTotal="", bFixedPos=.t., cQueryName = "PRDATMOD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=61, Top=406

  add object oPRVALRIG_2_43 as StdTrsField with uid="HSYACXQYFK",rtseq=90,rtrep=.t.,;
    cFormVar="w_PRVALRIG",value=0,enabled=.f.,;
    HelpContextID = 238850109,;
    cTotal="", bFixedPos=.t., cQueryName = "PRVALRIG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=677, Top=198, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oPROREEFF_2_45 as StdTrsField with uid="CTHIMPLPRQ",rtseq=92,rtrep=.t.,;
    cFormVar="w_PROREEFF",value=0,;
    ToolTipText = "Durata effettiva dell'attivit� (in ore)",;
    HelpContextID = 14491708,;
    cTotal="", bFixedPos=.t., cQueryName = "PROREEFF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=513, Top=376, cSayPict=["999"], cGetPict=["999"]

  add object oPRMINEFF_2_46 as StdTrsField with uid="CZQKVVXLBC",rtseq=93,rtrep=.t.,;
    cFormVar="w_PRMINEFF",value=0,;
    ToolTipText = "Durata effettiva dell'attivit� (in minuti)",;
    HelpContextID = 23330876,;
    cTotal="", bFixedPos=.t., cQueryName = "PRMINEFF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=559, Top=376, cSayPict=["99"], cGetPict=["99"]

  add object oPRCOSINT_2_48 as StdTrsField with uid="WKHJDOZMCD",rtseq=95,rtrep=.t.,;
    cFormVar="w_PRCOSINT",value=0,;
    ToolTipText = "Costo interno",;
    HelpContextID = 172400566,;
    cTotal="", bFixedPos=.t., cQueryName = "PRCOSINT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=677, Top=376, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oDAPREMIN_2_50 as StdTrsField with uid="FFRNXBUORC",rtseq=97,rtrep=.t.,;
    cFormVar="w_DAPREMIN",value=0,enabled=.f.,;
    HelpContextID = 148708996,;
    cTotal="", bFixedPos=.t., cQueryName = "DAPREMIN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=447, Top=406, cSayPict=[v_PV(38+VVU)], cGetPict=[v_GV(38+VVU)]

  add object oDAPREMAX_2_51 as StdTrsField with uid="AZHPGYOFIK",rtseq=98,rtrep=.t.,;
    cFormVar="w_DAPREMAX",value=0,enabled=.f.,;
    HelpContextID = 119726450,;
    cTotal="", bFixedPos=.t., cQueryName = "DAPREMAX",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=677, Top=406, cSayPict=[v_PV(38+VVU)], cGetPict=[v_GV(38+VVU)]

  add object oDAGAZUFF_2_52 as StdTrsField with uid="ZWEELJBSHG",rtseq=99,rtrep=.t.,;
    cFormVar="w_DAGAZUFF",value=space(6),enabled=.f.,;
    HelpContextID = 35360380,;
    cTotal="", bFixedPos=.t., cQueryName = "DAGAZUFF",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=293, Top=198, InputMask=replicate('X',6)

  add object oDESUNI_2_57 as StdTrsField with uid="CWGETYRRIZ",rtseq=104,rtrep=.t.,;
    cFormVar="w_DESUNI",value=space(35),enabled=.f.,;
    ToolTipText = "Descrizione unit� di misura",;
    HelpContextID = 177188298,;
    cTotal="", bFixedPos=.t., cQueryName = "DESUNI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=148, Left=441, Top=198, InputMask=replicate('X',35)

  add object oNOMEPRA_2_58 as StdTrsField with uid="ZRHJGDQFPH",rtseq=105,rtrep=.t.,;
    cFormVar="w_NOMEPRA",value=space(100),enabled=.f.,;
    HelpContextID = 25166634,;
    cTotal="", bFixedPos=.t., cQueryName = "NOMEPRA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=327, Left=262, Top=224, InputMask=replicate('X',100)

  add object oBtn_2_93 as StdButton with uid="FSSIIAFLPV",width=48,height=45,;
   left=6, top=227,;
    CpPicture="bmp\carica.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per caricare dati analitica";
    , HelpContextID = 231186055;
    , TabStop=.f.,Caption='\<Analitica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_93.Click()
      do GSAG_KDA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_93.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COAN <> 'S')
    endwith
   endif
  endfunc

  add object oBtn_2_188 as StdButton with uid="ZKTFGYXQCP",width=48,height=45,;
   left=56, top=227,;
    CpPicture="bmp\pagat.ico", caption="", nPag=2;
    , ToolTipText = "Premere per inserire la spesa e l'anticipazione relativa alla prestazione selezionata";
    , HelpContextID = 33579482;
    , TabStop=.f.,Caption='\<Spese';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_188.Click()
      this.parent.oContained.NotifyEvent("Riapplica")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_188.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLSPAN<>'S' )
    endwith
   endif
  endfunc

  add object oBtn_2_189 as StdButton with uid="WFHMFPDPLN",width=48,height=45,;
   left=106, top=227,;
    CpPicture="bmp\legami.ico", caption="", nPag=2;
    , ToolTipText = "Premere per collegare la spesa o l'anticipazione ad una prestazione";
    , HelpContextID = 83430438;
    , TabStop=.f.,Caption='\<Collega';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_189.Click()
      with this.Parent.oContained
        GSAL_BSA(this.Parent.oContained,"P","L")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_189.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_DACODATT) or .w_DARIFPRE>0 or .w_DAPREZZO=0 or .w_GENPRE='S')
    endwith
   endif
  endfunc

  add object oPRIMPANT_2_199 as StdTrsField with uid="PMWLPNCCZW",rtseq=243,rtrep=.t.,;
    cFormVar="w_PRIMPANT",value=0,;
    ToolTipText = "importo anticipazioni",;
    HelpContextID = 41435062,;
    cTotal="", bFixedPos=.t., cQueryName = "PRIMPANT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=262, Top=251

  func oPRIMPANT_2_199.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GENPRE<>'S' or .w_DARIGPRE<>'S')
    endwith
    endif
  endfunc

  add object oPRIMPSPE_2_200 as StdTrsField with uid="DZMTNGOGZG",rtseq=244,rtrep=.t.,;
    cFormVar="w_PRIMPSPE",value=0,;
    ToolTipText = "Importo spesa",;
    HelpContextID = 7880645,;
    cTotal="", bFixedPos=.t., cQueryName = "PRIMPSPE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=450, Top=251

  func oPRIMPSPE_2_200.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GENPRE<>'S' or .w_DARIGPRE<>'S')
    endwith
    endif
  endfunc

  add object oBox_2_94 as StdBox with uid="JKVGKAADMJ",left=801, top=22, width=1,height=172

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTDURMIN_3_1 as StdField with uid="JDQGAXQJOG",rtseq=224,rtrep=.f.,;
    cFormVar="w_TOTDURMIN",value=0,enabled=.f.,;
    ToolTipText = "Totale durata effettiva in minuti",;
    HelpContextID = 19959201,;
    cQueryName = "TOTDURMIN",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=311, Top=406

  add object oTOTDURORE_3_6 as StdField with uid="TVENFGPNBH",rtseq=229,rtrep=.f.,;
    cFormVar="w_TOTDURORE",value=0,enabled=.f.,;
    ToolTipText = "Totale durata effettiva",;
    HelpContextID = 19959336,;
    cQueryName = "TOTDURORE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=261, Top=406

  add object oTOTQTAORE_3_8 as StdField with uid="EBCHRTMXJV",rtseq=230,rtrep=.f.,;
    cFormVar="w_TOTQTAORE",value=0,enabled=.f.,;
    ToolTipText = "Totale durata in ore",;
    HelpContextID = 36933160,;
    cQueryName = "TOTQTAORE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=261, Top=376

  add object oTOTQTAMIN_3_9 as StdField with uid="WOLMPNEBCQ",rtseq=231,rtrep=.f.,;
    cFormVar="w_TOTQTAMIN",value=0,enabled=.f.,;
    ToolTipText = "Totale durata in minuti",;
    HelpContextID = 36933025,;
    cQueryName = "TOTQTAMIN",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=311, Top=376

  add object oStr_3_7 as StdString with uid="YWNVWNACCV",Visible=.t., Left=182, Top=377,;
    Alignment=1, Width=75, Height=18,;
    Caption="Totale durata:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="KEFNRZPVYT",Visible=.t., Left=175, Top=406,;
    Alignment=1, Width=82, Height=18,;
    Caption="Tot. durata eff.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="PCRYJWORIG",Visible=.t., Left=305, Top=377,;
    Alignment=0, Width=8, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="HZZGQWLOZY",Visible=.t., Left=305, Top=408,;
    Alignment=0, Width=8, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsag_mprBodyRow as CPBodyRowCnt
  Width=799
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPR__DATA_2_1 as StdTrsField with uid="BESISPKPGD",rtseq=49,rtrep=.t.,;
    cFormVar="w_PR__DATA",value=ctod("  /  /  "),;
    ToolTipText = "Data prestazione",;
    HelpContextID = 215687223,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=14, Top=0

  add object oDACODICE_2_6 as StdTrsField with uid="QPTASIFTTJ",rtseq=54,rtrep=.t.,;
    cFormVar="w_DACODICE",value=space(41),;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 80301691,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=212, Top=0, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_DACODICE"

  func oDACODICE_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_ISAHE)
    endwith
    endif
  endfunc

  func oDACODICE_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODICE_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDACODICE_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDACODICE_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",'GSAGZKPM.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDACODICE_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DACODICE
    i_obj.ecpSave()
  endproc

  add object oPRNUMPRA_2_7 as StdTrsField with uid="AIMYJPYJWW",rtseq=55,rtrep=.t.,;
    cFormVar="w_PRNUMPRA",value=space(15),;
    ToolTipText = "Numero pratica",;
    HelpContextID = 207622199,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=125, Left=84, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_BZZ", oKey_1_1="CNCODCAN", oKey_1_2="this.w_PRNUMPRA"

  func oPRNUMPRA_2_7.mCond()
    with this.Parent.oContained
      return (.w_EDITCODPRA and ((.w_DARIFPRE=0 and .w_DARIGPRE<>'S')))
    endwith
  endfunc

  func oPRNUMPRA_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
      if bRes and !(!.w_EDITCODPRA OR ((.w_CNDATFIN>.w_OBTEST OR EMPTY(.w_CNDATFIN)) and (.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))))
         bRes=(cp_WarningMsg(thisform.msgFmt("Attenzione pratica chiusa")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oPRNUMPRA_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPRNUMPRA_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oPRNUMPRA_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZZ',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oPRNUMPRA_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_PRNUMPRA
    i_obj.ecpSave()
  endproc

  add object oDACODATT_2_15 as StdTrsField with uid="WEWIRPNUDD",rtseq=62,rtrep=.t.,;
    cFormVar="w_DACODATT",value=space(20),;
    ToolTipText = "Codice della prestazione",;
    HelpContextID = 214519434,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Prestazione selezionata inesistente o obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=212, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_DACODATT"

  func oDACODATT_2_15.mCond()
    with this.Parent.oContained
      return ((.w_DARIFPRE=0 and .w_DARIGPRE<>'S' ))
    endwith
  endfunc

  func oDACODATT_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODATT_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDACODATT_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDACODATT_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Prestazioni",''+"Inspre"+'.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDACODATT_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DACODATT
    i_obj.ecpSave()
  endproc

  add object oPRDESPRE_2_21 as StdTrsField with uid="GFELYFKMLX",rtseq=68,rtrep=.t.,;
    cFormVar="w_PRDESPRE",value=space(40),;
    ToolTipText = "Descrizione prestazione",;
    HelpContextID = 212824123,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=241, Left=333, Top=0, InputMask=replicate('X',40)

  func oPRDESPRE_2_21.mCond()
    with this.Parent.oContained
      return ((.w_NOEDDES='N' or cp_IsAdministrator()))
    endwith
  endfunc

  add object oPRUNIMIS_2_22 as StdTrsField with uid="QQPJJQONLC",rtseq=69,rtrep=.t.,;
    cFormVar="w_PRUNIMIS",value=space(3),;
    HelpContextID = 152666185,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire l'unit� di misura principale o secondaria della prestazione",;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=576, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , TabStop=.f., cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_PRUNIMIS"

  func oPRUNIMIS_2_22.mCond()
    with this.Parent.oContained
      return (.w_TIPART = 'FM' AND (NOT EMPTY(.w_UNMIS2) OR NOT EMPTY(.w_UNMIS3) OR .w_FLSERG='S'))
    endwith
  endfunc

  func oPRUNIMIS_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRUNIMIS_2_22.ecpDrop(oSource)
    this.Parent.oContained.link_2_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPRUNIMIS_2_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oPRUNIMIS_2_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'GSVEUMDV.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oPRUNIMIS_2_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_PRUNIMIS
    i_obj.ecpSave()
  endproc

  add object oPRQTAMOV_2_24 as StdTrsField with uid="BDKVJURQBJ",rtseq=71,rtrep=.t.,;
    cFormVar="w_PRQTAMOV",value=0,;
    HelpContextID = 123781044,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=68, Left=614, Top=0, cSayPict=['@Z '+v_PQ(11)], cGetPict=['@Z '+v_GQ(11)], bHasZoom = .t. 

  func oPRQTAMOV_2_24.mCond()
    with this.Parent.oContained
      return (.w_TIPART = 'FM')
    endwith
  endfunc

  func oPRQTAMOV_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PRQTAMOV>0 AND (.w_FL_FRAZ<>'S' OR .w_PRQTAMOV=INT(.w_PRQTAMOV))) OR NOT .w_TIPSER $ 'RM')
    endwith
    return bRes
  endfunc

  proc oPRQTAMOV_2_24.mZoom
      with this.Parent.oContained
        GSAG_BOM(this.Parent.oContained,"I")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDAPREZZO_2_26 as StdTrsField with uid="FLBKTLCMZH",rtseq=73,rtrep=.t.,;
    cFormVar="w_DAPREZZO",value=0,;
    HelpContextID = 170058107,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=111, Left=683, Top=0, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)], bHasZoom = .t. 

  func oDAPREZZO_2_26.mCond()
    with this.Parent.oContained
      return (.w_TIPART<>'DE')
    endwith
  endfunc

  proc oDAPREZZO_2_26.mZoom
      with this.Parent.oContained
        gsag_bmp(this.Parent.oContained,"I",.w_DAPREZZO,.w_DAPREMAX,.w_DAPREMIN,.w_RCACODART)
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oObj_2_79 as cp_runprogram with uid="SLLMVQZRYJ",width=494,height=19,;
   left=222, top=432,;
    caption='Controllo congruit� con tipologia ente - GSAG_BDD(A, PRA)',;
   bGlobalFont=.t.,;
    prg="GSAG_BDD('A', 'PRA')",;
    cEvent = "w_PRNUMPRA Changed",;
    nPag=2;
    , HelpContextID = 94986782

  add object oObj_2_80 as cp_runprogram with uid="FZVGEATLHK",width=494,height=19,;
   left=222, top=453,;
    caption='Controllo congruit� con tipologia ente - GSAG_BDD(A, PRE)',;
   bGlobalFont=.t.,;
    prg="GSAG_BDD('A', 'PRE')",;
    cEvent = "w_DACODATT Changed,w_DACODICE Changed",;
    nPag=2;
    , HelpContextID = 94986778

  add object oFLCOMP_2_166 as StdTrsCheck with uid="KPXCZSNQLM",rtrep=.t.,;
    cFormVar="w_FLCOMP",  caption="",;
    ToolTipText = "Prestazioni da completare",;
    HelpContextID = 61253290,;
    Left=-2, Top=0, Width=15,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oFLCOMP_2_166.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLCOMP,&i_cF..t_FLCOMP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oFLCOMP_2_166.GetRadio()
    this.Parent.oContained.w_FLCOMP = this.RadioValue()
    return .t.
  endfunc

  func oFLCOMP_2_166.ToRadio()
    this.Parent.oContained.w_FLCOMP=trim(this.Parent.oContained.w_FLCOMP)
    return(;
      iif(this.Parent.oContained.w_FLCOMP=='S',1,;
      0))
  endfunc

  func oFLCOMP_2_166.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oFLCOMP_2_166.mCond()
    with this.Parent.oContained
      return (.w_EditSelez='S')
    endwith
  endfunc

  add object oObj_2_170 as cp_runprogram with uid="GTPHAFKRYK",width=219,height=20,;
   left=221, top=475,;
    caption='GSAL_BSA',;
   bGlobalFont=.t.,;
    prg="gsal_bsa('P','C')",;
    cEvent = "CalcPrest",;
    nPag=2;
    , ToolTipText = "Creazione righe prestazioni collegate";
    , HelpContextID = 259408039

  add object oObj_2_171 as cp_runprogram with uid="OTLXPLRLTQ",width=219,height=19,;
   left=221, top=495,;
    caption='GSAL_BSA',;
   bGlobalFont=.t.,;
    prg="gsal_bsa('P','E')",;
    cEvent = "HasEvent,Elispese",;
    nPag=2;
    , ToolTipText = "Eliminazione righe prestazioni collegate";
    , HelpContextID = 259408039

  add object oObj_2_212 as cp_runprogram with uid="TGHUTOBZYF",width=232,height=19,;
   left=457, top=721,;
    caption='gsag_bcd(5)',;
   bGlobalFont=.t.,;
    prg="gsag_bcd('5')",;
    cEvent = "w_PR__DATA Changed",;
    nPag=2;
    , HelpContextID = 261499466
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPR__DATA_2_1.When()
    return(.t.)
  proc oPR__DATA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPR__DATA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=8
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsag_mpr','RAP_PRES','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DACODRES=RAP_PRES.DACODRES";
  +" and "+i_cAliasName2+".DAGRURES=RAP_PRES.DAGRURES";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
