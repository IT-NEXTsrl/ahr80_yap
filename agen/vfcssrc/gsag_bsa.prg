* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsag_bsa                                                        *
*              Stampa Agenda                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-20                                                      *
* Last revis.: 2009-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsag_bsa",oParentObject)
return(i_retval)

define class tgsag_bsa as StdBatch
  * --- Local variables
  w_CONTA = 0
  w_DATI = space(254)
  w_DATI = space(254)
  w_DATI = space(254)
  w_DATI = space(254)
  w_DATI = space(254)
  w_DATI = space(254)
  w_DATI = space(254)
  w_ORAINI = 0
  w_ORAFIN = 0
  w_DURATA = 0
  w_DATINI = ctot("")
  w_DATFIN = ctot("")
  w_GIORNO = space(12)
  w_RECORD = 0
  w_CONTA_LUN = 0
  w_CONTA_MAR = 0
  w_CONTA_MER = 0
  w_CONTA_GIO = 0
  w_CONTA_VEN = 0
  w_CONTA_SAB = 0
  w_CONTA_DOM = 0
  w_TIPREC = space(1)
  w_STAMPA = space(10)
  w_ORA = 0
  w_OLDORA = 0
  w_ATSERIAL = space(20)
  w_PARTECIP = space(10)
  w_DIFFE = 0
  w_GIORNOSQL = space(12)
  * --- WorkFile variables
  DIPENDEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia la stampa dell'oggetto Agenda (Gsag_kag)
    this.w_CONTA_LUN = 1
    this.w_CONTA_MAR = 1
    this.w_CONTA_MER = 1
    this.w_CONTA_GIO = 1
    this.w_CONTA_VEN = 1
    this.w_CONTA_SAB = 1
    this.w_CONTA_DOM = 1
    this.w_CONTA = 1
    this.w_RECORD = 1
    this.w_OLDORA = 99
    if this.oParentObject.w_CALATTIVO=1
      this.w_GIORNO = DOW(this.oParentObject.w_DATA_INI)
      if CP_DBTYPE="SQLServer"
        if this.w_GIORNO=7
          this.w_GIORNO = 1
        else
          this.w_GIORNO = this.w_GIORNO+1
        endif
      endif
      this.w_GIORNOSQL = g_GIORNO[this.w_GIORNO]
    endif
    CREATE CURSOR TMPFILTRO (TMPSERIAL C(20))
    CREATE CURSOR GIORNO (GIORNO C(12), OGG_LUN M(10), OGG_MAR M(10), OGG_MER M(10), OGG_GIO M(10), OGG_VEN M(10), OGG_SAB M(10), OGG_DOM M(10), ATDATINI D(8), ATDATFIN D(8), TIPREC C(1), ATOGGETT C(254), ATPERSON C(60),ATSERIAL C(20))
    INSERT INTO GIORNO (GIORNO,OGG_LUN,OGG_MAR,OGG_MER,OGG_GIO,OGG_VEN,OGG_SAB,OGG_DOM,ATDATINI,ATDATFIN,TIPREC,ATOGGETT,ATPERSON,ATSERIAL); 
 VALUES("","","","","","","","",cp_chartodate("  -  -  "), cp_chartodate("  -  -  "),"","","","")
    vq_exec("query\GSAGSKAG", this, "AGENDA")
     
 SELECT AGENDA 
 GO TOP
    do while NOT EOF() 
      this.w_GIORNO = GIORNO
      this.w_DATI = DATI
      this.w_DATINI = iif(ATDATINI<this.oParentObject.w_DATA_INI,this.oParentObject.w_DATA_INI,ATDATINI)
      this.w_DATFIN = iif(ATDATFIN>this.oParentObject.w_DATA_FIN,this.oParentObject.w_DATA_FIN,ATDATFIN)
      this.w_ATSERIAL = ATSERIAL
      * --- lancio la funzione per leggere i partecipanti
      this.w_PARTECIP = AH_DIPENDEN(this.w_ATSERIAL)
      this.w_DIFFE = TTOD(this.w_DATFIN)-TTOD(this.w_DATINI) + 1
      this.w_TIPREC = TIPREC
      this.w_ORA = HOUR(this.w_DATINI)
      * --- effettuo il ciclo per gestire la attivit� su pi� giorni
      do while this.w_DIFFE>0
        if this.oParentObject.w_CALATTIVO=3
          this.w_GIORNO = DOW(this.w_DATINI)
          if CP_DBTYPE="SQLServer"
            if this.w_GIORNO=7
              this.w_GIORNO = 1
            else
              this.w_GIORNO = this.w_GIORNO+1
            endif
          endif
          this.w_GIORNOSQL = g_GIORNO[dow(this.w_DATINI)]
        endif
         
 SELECT GIORNO 
 GO TOP
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_DATINI = this.w_DATINI+3600*24
        this.w_DIFFE = this.w_DIFFE-1
      enddo
      this.w_OLDORA = this.w_ORA
       
 SELECT AGENDA 
 SKIP(1)
    enddo
    if this.oParentObject.w_CALATTIVO=3
      vq_exec("query\GSAGSKAG_3", this, "RIEPILOGO")
      Select * FROM RIEPILOGO WHERE ATSERIAL NOT IN (SELECT * FROM TMPFILTRO) into cursor RIEPILOGO
      if RECCOUNT("RIEPILOGO")>0
        Select * From GIORNO union all Select * From RIEPILOGO Order By TIPREC,ATDATFIN,ATDATINI into cursor __TMP__
      else
        Select * From GIORNO Order By TIPREC,ATDATFIN,ATDATINI into cursor __TMP__
      endif
      use in RIEPILOGO
      use in TMPFILTRO
    else
      Select * From GIORNO Order By TIPREC,ATDATFIN,ATDATINI into cursor __TMP__
    endif
    L_DATINI=this.oParentObject.w_DATA_INI
    L_DATFIN=this.oParentObject.w_DATA_FIN
    L_DATA=ALLTRIM(g_GIORNO[DOW(this.oParentObject.w_DataSel)])+" "+ALLTRIM(STR(DAY(this.oParentObject.w_DataSel)))+" "+ALLTRIM(g_MESE[MONTH(this.oParentObject.w_DataSel)])+" "+ALLTRIM(STR(YEAR(this.oParentObject.w_DataSel)))
    if this.oParentObject.w_calAttivo=3
       cp_chprn("QUERY\GSAGSKAG", " ", this)
    else
       cp_chprn("QUERY\GSAG_SKAG", " ", this)
    endif
    use in AGENDA
    use in GIORNO
    use in __TMP__
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_GIORNOSQL="Luned�" and ((this.w_CONTA_LUN<=15 AND this.oParentObject.w_CALATTIVO=3 ) OR this.oParentObject.w_CALATTIVO=1 )
        this.w_STAMPA = IIF(this.oParentObject.w_CALATTIVO=1 AND this.w_OLDORA=this.w_ORA, REPL(" ",2*this.w_CONTA_LUN),"")
        REPLACE GIORNO.OGG_LUN WITH alltrim(GIORNO.OGG_LUN)+IIF(this.oParentObject.w_CALATTIVO=3,LEFT(; 
 iif(empty(GIORNO.ogg_lun),"",CHR(13))+this.w_STAMPA+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+" "+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ",50),iif(empty(GIORNO.ogg_lun),"",CHR(13))+this.w_STAMPA+PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+; 
 ":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+; 
 " "+PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ")+IIF(this.w_CONTA_LUN=15 AND this.oParentObject.w_CALATTIVO=3, chr(13)+space(100)+"Segue->","")
        REPLACE GIORNO.TIPREC WITH this.w_TIPREC
        insert into TMPFILTRO (TMPSERIAL) VALUES (this.w_ATSERIAL)
        this.w_CONTA_LUN = this.w_CONTA_LUN+1
      case this.w_GIORNOSQL="Marted�" and ((this.w_CONTA_MAR<=15 AND this.oParentObject.w_CALATTIVO=3 ) OR this.oParentObject.w_CALATTIVO=1 )
        this.w_STAMPA = IIF(this.oParentObject.w_CALATTIVO=1 AND this.w_OLDORA=this.w_ORA, REPL(" ",2*this.w_CONTA_MAR),"")
        REPLACE GIORNO.OGG_MAR WITH alltrim(GIORNO.OGG_MAR)+IIF(this.oParentObject.w_CALATTIVO=3,LEFT(; 
 iif(empty(GIORNO.ogg_mar),"",CHR(13))+this.w_STAMPA+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+" "+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ",50),iif(empty(GIORNO.ogg_mar),"",CHR(13))+this.w_STAMPA+PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+; 
 ":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+; 
 " "+PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ")+IIF(this.w_CONTA_MAR=15 AND this.oParentObject.w_CALATTIVO=3, chr(13)+space(100)+"Segue->","")
        REPLACE GIORNO.TIPREC WITH this.w_TIPREC
        insert into TMPFILTRO (TMPSERIAL) VALUES (this.w_ATSERIAL)
        this.w_CONTA_MAR = this.w_CONTA_MAR+1
      case this.w_GIORNOSQL="Mercoled�" and ((this.w_CONTA_MER<=15 AND this.oParentObject.w_CALATTIVO=3 ) OR this.oParentObject.w_CALATTIVO=1 )
        this.w_STAMPA = IIF(this.oParentObject.w_CALATTIVO=1 AND this.w_OLDORA=this.w_ORA, REPL(" ",2*this.w_CONTA_MER),"")
        REPLACE GIORNO.OGG_MER WITH alltrim(GIORNO.OGG_MER)+IIF(this.oParentObject.w_CALATTIVO=3,LEFT(; 
 iif(empty(GIORNO.ogg_mer),"",CHR(13))+this.w_STAMPA+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+" "+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ",50),iif(empty(GIORNO.ogg_mer),"",CHR(13))+this.w_STAMPA+PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+; 
 ":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+; 
 " "+PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ")+IIF(this.w_CONTA_MER=15 AND this.oParentObject.w_CALATTIVO=3, chr(13)+space(100)+"Segue->","")
        REPLACE GIORNO.TIPREC WITH this.w_TIPREC
        insert into TMPFILTRO (TMPSERIAL) VALUES (this.w_ATSERIAL)
        this.w_CONTA_MER = this.w_CONTA_MER+1
      case this.w_GIORNOSQL="Gioved�" and ((this.w_CONTA_GIO<=15 AND this.oParentObject.w_CALATTIVO=3 ) OR this.oParentObject.w_CALATTIVO=1 )
        this.w_STAMPA = IIF(this.oParentObject.w_CALATTIVO=1 AND this.w_OLDORA=this.w_ORA,REPL(" ",2*this.w_CONTA_GIO),"")
        REPLACE GIORNO.OGG_GIO WITH alltrim(GIORNO.OGG_GIO)+IIF(this.oParentObject.w_CALATTIVO=3,LEFT(; 
 iif(empty(GIORNO.ogg_gio),"",CHR(13))+this.w_STAMPA+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+" "+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ",50),iif(empty(GIORNO.ogg_gio),"",CHR(13))+this.w_STAMPA+PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+; 
 ":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+; 
 " "+PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ")+IIF(this.w_CONTA_GIO=15 AND this.oParentObject.w_CALATTIVO=3, chr(13)+space(100)+"Segue->","")
        REPLACE GIORNO.TIPREC WITH this.w_TIPREC
        insert into TMPFILTRO (TMPSERIAL) VALUES (this.w_ATSERIAL)
        this.w_CONTA_GIO = this.w_CONTA_GIO+1
      case this.w_GIORNOSQL="Venerd�" and ((this.w_CONTA_VEN<=15 AND this.oParentObject.w_CALATTIVO=3 ) OR this.oParentObject.w_CALATTIVO=1 )
        this.w_STAMPA = IIF(this.oParentObject.w_CALATTIVO=1 AND this.w_OLDORA=this.w_ORA, REPL(" ",2*this.w_CONTA_VEN),"")
        REPLACE GIORNO.OGG_VEN WITH alltrim(GIORNO.OGG_VEN)+IIF(this.oParentObject.w_CALATTIVO=3,LEFT(; 
 iif(empty(GIORNO.ogg_ven),"",CHR(13))+this.w_STAMPA+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+" "+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ",50),iif(empty(GIORNO.ogg_ven),"",CHR(13))+this.w_STAMPA+PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+; 
 ":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+" "+PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+" ")+IIF(this.w_CONTA_VEN=15 AND this.oParentObject.w_CALATTIVO=3, chr(13)+space(100)+"Segue->","")
        REPLACE GIORNO.TIPREC WITH this.w_TIPREC
        insert into TMPFILTRO (TMPSERIAL) VALUES (this.w_ATSERIAL)
        this.w_CONTA_VEN = this.w_CONTA_VEN+1
      case this.w_GIORNOSQL="Sabato" and ((this.w_CONTA_SAB<=15 AND this.oParentObject.w_CALATTIVO=3 ) OR this.oParentObject.w_CALATTIVO=1 )
        this.w_STAMPA = IIF(this.oParentObject.w_CALATTIVO=1 AND this.w_OLDORA=this.w_ORA, REPL(" ",2*this.w_CONTA_SAB),"")
        REPLACE GIORNO.OGG_SAB WITH alltrim(GIORNO.OGG_SAB)+IIF(this.oParentObject.w_CALATTIVO=3,LEFT(; 
 iif(empty(GIORNO.ogg_sab),"",CHR(13))+this.w_STAMPA+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+" "+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ",50),iif(empty(GIORNO.ogg_sab),"",CHR(13))+this.w_STAMPA+PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+; 
 ":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+; 
 " "+PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ")+IIF(this.w_CONTA_SAB=15 AND this.oParentObject.w_CALATTIVO=3, chr(13)+space(100)+"Segue->","")
        REPLACE GIORNO.TIPREC WITH this.w_TIPREC
        insert into TMPFILTRO (TMPSERIAL) VALUES (this.w_ATSERIAL)
        this.w_CONTA_SAB = this.w_CONTA_SAB+1
      case this.w_GIORNOSQL="Domenica" and ((this.w_CONTA_DOM<=15 AND this.oParentObject.w_CALATTIVO=3 ) OR this.oParentObject.w_CALATTIVO=1 )
        this.w_STAMPA = IIF(this.oParentObject.w_CALATTIVO=1 AND this.w_OLDORA=this.w_ORA, REPL(" ",2*this.w_CONTA_DOM),"")
        REPLACE GIORNO.OGG_DOM WITH alltrim(GIORNO.OGG_DOM)+IIF(this.oParentObject.w_CALATTIVO=3,LEFT(; 
 iif(empty(GIORNO.ogg_dom),"",CHR(13))+this.w_STAMPA+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+" "+; 
 PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ",50),iif(empty(GIORNO.ogg_dom),"",CHR(13))+this.w_STAMPA+PADL(ALLTRIM(STR(HOUR(this.w_DATINI),2)),2,"0")+; 
 ":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATINI),2)),2,"0")+; 
 " "+PADL(ALLTRIM(STR(HOUR(this.w_DATFIN),2)),2,"0")+":"+PADL(ALLTRIM(STR(MINUTE(this.w_DATFIN),2)),2,"0")+" "+; 
 " "+alltrim(this.w_DATI)+" "+IIF(EMPTY(this.w_PARTECIP),"","[")+ALLTRIM(this.w_PARTECIP)+IIF(EMPTY(this.w_PARTECIP),"","]")+; 
 " ")+IIF(this.w_CONTA_DOM=15 AND this.oParentObject.w_CALATTIVO=3, chr(13)+space(100)+"Segue->","")
        REPLACE GIORNO.TIPREC WITH this.w_TIPREC
        insert into TMPFILTRO (TMPSERIAL) VALUES (this.w_ATSERIAL)
        this.w_CONTA_DOM = this.w_CONTA_DOM+1
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DIPENDEN'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
